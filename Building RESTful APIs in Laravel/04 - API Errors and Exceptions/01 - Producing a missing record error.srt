1
00:00:00,50 --> 00:00:03,40
- [Instructor] A missing record error, or 404,

2
00:00:03,40 --> 00:00:06,20
is the classic file not found error page

3
00:00:06,20 --> 00:00:09,80
you've seen probably thousands of times surfing the web.

4
00:00:09,80 --> 00:00:13,30
The 404 is the response for us to inform the user

5
00:00:13,30 --> 00:00:15,80
that whatever they were looking for at this URL,

6
00:00:15,80 --> 00:00:17,60
is not to be found.

7
00:00:17,60 --> 00:00:21,80
In our API, the goal of the 404 is to provide for us

8
00:00:21,80 --> 00:00:24,60
a response when a resource isn't available

9
00:00:24,60 --> 00:00:26,90
with that particular ID.

10
00:00:26,90 --> 00:00:30,00
Now in Laravel we have two ways to solve this.

11
00:00:30,00 --> 00:00:32,50
We'll look at the simple way to solve it

12
00:00:32,50 --> 00:00:35,20
and then we'll go ahead and build out a custom solution

13
00:00:35,20 --> 00:00:36,80
in case you need it.

14
00:00:36,80 --> 00:00:41,60
Begin by opening up your route's api.php file.

15
00:00:41,60 --> 00:00:45,70
Now in this file, you'll note that put and delete URLs

16
00:00:45,70 --> 00:00:47,80
using the poll parameter.

17
00:00:47,80 --> 00:00:52,30
This uses the ID passed in to identify the poll

18
00:00:52,30 --> 00:00:54,70
with that particular primary key.

19
00:00:54,70 --> 00:00:59,60
And if that poll isn't found, it returns an automatic 404.

20
00:00:59,60 --> 00:01:02,40
We've seen this before, but just to verify,

21
00:01:02,40 --> 00:01:03,90
let's try it out.

22
00:01:03,90 --> 00:01:07,70
We'll open up Insomnia, remembering to have started

23
00:01:07,70 --> 00:01:10,40
your Laravel server at this point.

24
00:01:10,40 --> 00:01:12,40
We'll go to our delete method

25
00:01:12,40 --> 00:01:15,40
and we'll try to delete a random poll.

26
00:01:15,40 --> 00:01:19,40
In this case, let's try deleting 34567,

27
00:01:19,40 --> 00:01:23,10
which is a poll that clearly doesn't exist in our database.

28
00:01:23,10 --> 00:01:27,80
If we send our request, we're going to get a 404 response code

29
00:01:27,80 --> 00:01:33,20
and we're going to see Laravel's 404 standard response page.

30
00:01:33,20 --> 00:01:36,20
If you click on the Timeline button in Insomnia

31
00:01:36,20 --> 00:01:39,30
you'll see that first thing that's returned

32
00:01:39,30 --> 00:01:42,10
is a 404 Not Found.

33
00:01:42,10 --> 00:01:47,00
This is that http response code I was talking about earlier.

34
00:01:47,00 --> 00:01:49,80
So this is the easy way for us to build in

35
00:01:49,80 --> 00:01:53,00
a 404 response into our API.

36
00:01:53,00 --> 00:01:55,20
However, note what this does.

37
00:01:55,20 --> 00:01:57,90
This is returning an HTML response page.

38
00:01:57,90 --> 00:01:59,50
We may not want that

39
00:01:59,50 --> 00:02:01,60
or we may want to return something differently

40
00:02:01,60 --> 00:02:04,10
and or we may need to perform our actions

41
00:02:04,10 --> 00:02:09,70
in a different way then Laravel's magical 404 works.

42
00:02:09,70 --> 00:02:13,30
To do this, let's open up our polls controller.

43
00:02:13,30 --> 00:02:15,50
So go back to our code editor

44
00:02:15,50 --> 00:02:22,70
and open up App, Http, Controllers, PollsController.php.

45
00:02:22,70 --> 00:02:26,30
And here we're going to work with our show method,

46
00:02:26,30 --> 00:02:28,60
since it takes in an id.

47
00:02:28,60 --> 00:02:30,70
Rather than using the find method,

48
00:02:30,70 --> 00:02:33,10
we're going to use find or fail.

49
00:02:33,10 --> 00:02:38,50
So on line 17, we'll replace Poll::find with findOrFail.

50
00:02:38,50 --> 00:02:41,90
At this point we don't need to do anything else.

51
00:02:41,90 --> 00:02:45,40
This findOrFail method will cause the API endpoint

52
00:02:45,40 --> 00:02:48,40
to attempt to find the poll matching that id

53
00:02:48,40 --> 00:02:50,70
and when it doesn't Laravel

54
00:02:50,70 --> 00:02:53,50
will throw its standard 404 error page.

55
00:02:53,50 --> 00:02:56,00
Now let's try this out.

56
00:02:56,00 --> 00:02:57,70
We'll go back to Insomnia.

57
00:02:57,70 --> 00:03:00,40
We'll copy the get poll method

58
00:03:00,40 --> 00:03:06,10
and this time we'll call it get poll does not exist.

59
00:03:06,10 --> 00:03:09,40
In this case we'll again try to get some random poll.

60
00:03:09,40 --> 00:03:13,40
Let's try poll 5634.

61
00:03:13,40 --> 00:03:18,10
We'll send the request and we'll get our 404 response page,

62
00:03:18,10 --> 00:03:21,00
much like we did with our delete request.

63
00:03:21,00 --> 00:03:23,00
Now there is a third way to solve this

64
00:03:23,00 --> 00:03:25,50
that we're also going to demonstrate.

65
00:03:25,50 --> 00:03:29,70
This way we're not going to return Laravel's 404 page.

66
00:03:29,70 --> 00:03:33,10
Let's go back to the polls controller in our code editor.

67
00:03:33,10 --> 00:03:37,60
And in our show method above our response to json line,

68
00:03:37,60 --> 00:03:40,30
line 17, we'll add a new line

69
00:03:40,30 --> 00:03:48,10
and in here we'll call poll is equal to Poll::find

70
00:03:48,10 --> 00:03:50,20
and pass in our id.

71
00:03:50,20 --> 00:03:54,40
So this finds our poll and sets it to the poll variable.

72
00:03:54,40 --> 00:04:00,50
Now we can add on line 18 if is_null

73
00:04:00,50 --> 00:04:03,10
to check if our poll instance is null,

74
00:04:03,10 --> 00:04:05,30
so we'll pass in poll.

75
00:04:05,30 --> 00:04:07,40
And then inside of our if statement

76
00:04:07,40 --> 00:04:15,90
we'll return response arrow json(null, 404).

77
00:04:15,90 --> 00:04:18,70
So this'll return a null json response

78
00:04:18,70 --> 00:04:23,50
with the http response code 404.

79
00:04:23,50 --> 00:04:24,80
Save this.

80
00:04:24,80 --> 00:04:28,30
We'll go back to Insomnia and try it one more time.

81
00:04:28,30 --> 00:04:33,90
So again, we're going to try accessing API poll 5634.

82
00:04:33,90 --> 00:04:39,10
And we get a null json response and a 404 response code.

83
00:04:39,10 --> 00:04:40,00
Pretty cool.

84
00:04:40,00 --> 00:04:41,80
We've now build three different ways

85
00:04:41,80 --> 00:04:51,00
to deal with missing records in our Laravel API.

