1
00:00:00,60 --> 00:00:01,70
- [Instructor] Layerbel includes

2
00:00:01,70 --> 00:00:04,30
a powerful validation layer that we can build

3
00:00:04,30 --> 00:00:08,20
on top of to validate data sent to our API.

4
00:00:08,20 --> 00:00:12,40
And then properly ensure it matches whatever rules we need.

5
00:00:12,40 --> 00:00:14,60
Let's get started and build some code.

6
00:00:14,60 --> 00:00:19,30
We'll open up the polls controller in app, http,

7
00:00:19,30 --> 00:00:23,00
controllers, polls controller.php.

8
00:00:23,00 --> 00:00:27,10
In this stored method, on line 24, we need to add some

9
00:00:27,10 --> 00:00:30,40
logic to first build up a basic rule to ensure we have

10
00:00:30,40 --> 00:00:33,00
a title value pass and has to be, say,

11
00:00:33,00 --> 00:00:35,30
less than 10 characters in length.

12
00:00:35,30 --> 00:00:37,30
To do this, we need to first add

13
00:00:37,30 --> 00:00:40,40
the validator facade to the clause.

14
00:00:40,40 --> 00:00:46,60
On line six, add a new line, and enter in use, validator.

15
00:00:46,60 --> 00:00:49,20
Now, unlike how you might have done validation in the past,

16
00:00:49,20 --> 00:00:51,50
where you just added the validation rules and called

17
00:00:51,50 --> 00:00:55,00
the validate method on the request object passed in,

18
00:00:55,00 --> 00:00:58,40
that doesn't quite work for APIs in layerbel.

19
00:00:58,40 --> 00:01:01,80
First we're going to need to build up an array of rules.

20
00:01:01,80 --> 00:01:05,60
On line 27, inside of our store method,

21
00:01:05,60 --> 00:01:08,30
we're going to add an array of rules.

22
00:01:08,30 --> 00:01:12,50
To this, we'll add a key value pair with the key of

23
00:01:12,50 --> 00:01:14,90
title and then the value being

24
00:01:14,90 --> 00:01:22,00
the string of required, bar, max, colon 10.

25
00:01:22,00 --> 00:01:24,40
This says that title is required and

26
00:01:24,40 --> 00:01:27,40
it can only be a max of 10 characters long.

27
00:01:27,40 --> 00:01:33,50
Now after this, we'll add validator is going to be equal to

28
00:01:33,50 --> 00:01:35,70
a new instance of our validator class.

29
00:01:35,70 --> 00:01:40,90
So validator, colon, colon, make, we'll call in our request

30
00:01:40,90 --> 00:01:46,40
object, all to pass in all the data from our request.

31
00:01:46,40 --> 00:01:49,80
And then pass in the rules we want to check it against.

32
00:01:49,80 --> 00:01:53,20
So we create a new instance of the validator clause,

33
00:01:53,20 --> 00:01:55,60
including our data that we're going to pass in

34
00:01:55,60 --> 00:01:58,90
and the rules we want to validate that data against.

35
00:01:58,90 --> 00:02:00,40
After this, we can test for the

36
00:02:00,40 --> 00:02:04,40
validation failure, using validator fails.

37
00:02:04,40 --> 00:02:07,70
For now, we'll just do DD for debug

38
00:02:07,70 --> 00:02:12,80
and die, validator, arrow, fails.

39
00:02:12,80 --> 00:02:14,40
So we can see what kind of response

40
00:02:14,40 --> 00:02:16,40
we get back at this point.

41
00:02:16,40 --> 00:02:18,40
Now what do we need to do at this point?

42
00:02:18,40 --> 00:02:20,10
First, remember to start up your

43
00:02:20,10 --> 00:02:23,40
layerbel app with php artisan serve.

44
00:02:23,40 --> 00:02:27,50
The open up insomnia, and we'll go to our post request

45
00:02:27,50 --> 00:02:30,00
where we're adding a poll, and we'll

46
00:02:30,00 --> 00:02:32,70
set the title to be a really long value.

47
00:02:32,70 --> 00:02:39,20
For instance, we'll set it as the best course on layerbel.

48
00:02:39,20 --> 00:02:42,80
We'll send this request, and we'll see that in fact

49
00:02:42,80 --> 00:02:46,40
what we get back is a true bullion response.

50
00:02:46,40 --> 00:02:48,20
So our validator did fail, and it

51
00:02:48,20 --> 00:02:51,30
returns true in the case of a failure.

52
00:02:51,30 --> 00:02:54,80
Let's go back to our code and we can finish this up now.

53
00:02:54,80 --> 00:02:57,80
Now we want to replace our DD validator fails,

54
00:02:57,80 --> 00:03:01,60
instead with calling if and inside of this

55
00:03:01,60 --> 00:03:07,80
if we want to return, a response, arrow, json.

56
00:03:07,80 --> 00:03:11,00
And then we'll pass in validator and then we'll call

57
00:03:11,00 --> 00:03:14,40
the errors method on the validator class.

58
00:03:14,40 --> 00:03:18,70
And then we'll return an http status code of 400.

59
00:03:18,70 --> 00:03:21,40
And that's it, we now have validation that works

60
00:03:21,40 --> 00:03:24,80
and responds with the correct error code on this route.

61
00:03:24,80 --> 00:03:28,70
If we try it out, we can go back to insomnia, send our

62
00:03:28,70 --> 00:03:32,50
request, and we'll get back a json body that tells

63
00:03:32,50 --> 00:03:37,60
us that the title cannot be greater than 10 characters long.

64
00:03:37,60 --> 00:03:42,20
Now if we go back and change our title validation rules

65
00:03:42,20 --> 00:03:46,70
to instead allow 255 characters long, so go back to your

66
00:03:46,70 --> 00:03:53,50
code editor and edit line 28 to read max colon 255.

67
00:03:53,50 --> 00:03:57,50
Save this and rerun our request, and we'll see that we did

68
00:03:57,50 --> 00:03:59,50
in fact create our new poll with

69
00:03:59,50 --> 00:04:02,20
the title the best course on layerbel.

70
00:04:02,20 --> 00:04:04,30
Now do recall, we've only solved

71
00:04:04,30 --> 00:04:06,40
this problem for our post request.

72
00:04:06,40 --> 00:04:08,30
We didn't update our put request,

73
00:04:08,30 --> 00:04:10,60
so puts are still unvalidated.

74
00:04:10,60 --> 00:04:13,10
You probably should design a more consistent system

75
00:04:13,10 --> 00:04:15,80
for doing validation, but that's kind of

76
00:04:15,80 --> 00:04:18,10
outside of the scope of this course.

77
00:04:18,10 --> 00:04:20,20
But for now at least, you know how

78
00:04:20,20 --> 00:04:24,00
to build validation in layerbel for your API.

