1
00:00:00,60 --> 00:00:01,90
- [Male Voice] So now you've seen the basics

2
00:00:01,90 --> 00:00:06,00
of dealing with basic 404 errors and validation errors.

3
00:00:06,00 --> 00:00:08,50
But all APIs are going to be a little bit different,

4
00:00:08,50 --> 00:00:10,90
and have unique needs and choices

5
00:00:10,90 --> 00:00:13,60
for you to build out your API.

6
00:00:13,60 --> 00:00:15,60
Sometimes you have to process and present

7
00:00:15,60 --> 00:00:17,50
different types of errors.

8
00:00:17,50 --> 00:00:19,50
How can you do this?

9
00:00:19,50 --> 00:00:22,60
To begin, we are going to build an endpoint.

10
00:00:22,60 --> 00:00:24,50
This of course will be a very unique thing

11
00:00:24,50 --> 00:00:26,30
that you have never done before.

12
00:00:26,30 --> 00:00:29,10
This endpoint is just going to throw errors.

13
00:00:29,10 --> 00:00:31,60
So we can try out a few different variations

14
00:00:31,60 --> 00:00:34,20
of processing and displaying errors.

15
00:00:34,20 --> 00:00:40,30
We'll begin by opening up our routes file at routesapi.php

16
00:00:40,30 --> 00:00:42,60
On line 24 well add a new route,

17
00:00:42,60 --> 00:00:46,80
which will be route::any.

18
00:00:46,80 --> 00:00:49,60
This any method allows this route

19
00:00:49,60 --> 00:00:52,20
to respond to any type of request

20
00:00:52,20 --> 00:00:54,30
at this particular endpoint.

21
00:00:54,30 --> 00:00:57,20
In this case our end point will be errors.

22
00:00:57,20 --> 00:01:00,40
And we'll have the PollsController

23
00:01:00,40 --> 00:01:03,50
and the errors function handle it.

24
00:01:03,50 --> 00:01:08,20
Now open up our PollsController in app, http,

25
00:01:08,20 --> 00:01:12,00
Controllers, PollsController.php.

26
00:01:12,00 --> 00:01:14,10
We'll scroll all the way to the bottom,

27
00:01:14,10 --> 00:01:19,50
and at the end of it we'll add a new public function errors.

28
00:01:19,50 --> 00:01:23,20
Here, for our first example of dealing with errors,

29
00:01:23,20 --> 00:01:25,40
we're going to do what we did earlier,

30
00:01:25,40 --> 00:01:27,40
which is we handle a special error case

31
00:01:27,40 --> 00:01:29,20
by returning a json response,

32
00:01:29,20 --> 00:01:32,40
and the particular error code that should be returned.

33
00:01:32,40 --> 00:01:38,40
On line 52 add return, response, arrow json,

34
00:01:38,40 --> 00:01:40,50
and we'll return an empty array,

35
00:01:40,50 --> 00:01:43,10
and then the error code 501.

36
00:01:43,10 --> 00:01:46,60
This http status code is the error code

37
00:01:46,60 --> 00:01:48,40
for when the server doesn't implement

38
00:01:48,40 --> 00:01:51,30
the code needed to fulfill the request.

39
00:01:51,30 --> 00:01:53,80
You're basically asking the server to do something

40
00:01:53,80 --> 00:01:56,50
that the server doesn't know how to do.

41
00:01:56,50 --> 00:01:58,30
Now at this point, you should start up

42
00:01:58,30 --> 00:02:01,70
your laervo application with php artisan serv.

43
00:02:01,70 --> 00:02:03,50
Then open up insomnia.

44
00:02:03,50 --> 00:02:08,70
Well create a new request called get errors.

45
00:02:08,70 --> 00:02:10,70
And this request is going to talk to

46
00:02:10,70 --> 00:02:24,70
http://127.0.0.1 port 8000 api errors.

47
00:02:24,70 --> 00:02:26,60
And we can send this request.

48
00:02:26,60 --> 00:02:29,10
And we see we get back an empty array.

49
00:02:29,10 --> 00:02:32,40
And we get back our 501 error code.

50
00:02:32,40 --> 00:02:35,80
Okay, now let's try it a different way.

51
00:02:35,80 --> 00:02:38,10
Let's go back to our editor,

52
00:02:38,10 --> 00:02:41,50
and this time let's set up an error message.

53
00:02:41,50 --> 00:02:43,80
So inside of our empty array,

54
00:02:43,80 --> 00:02:47,60
we'll return the message key

55
00:02:47,60 --> 00:02:52,50
and we'll send a value of payment is required.

56
00:02:52,50 --> 00:02:56,40
We'll save this, and we'll go back to insomnia,

57
00:02:56,40 --> 00:02:58,70
and we'll resend our request.

58
00:02:58,70 --> 00:03:01,50
And we'll see that this time we get a message back.

59
00:03:01,50 --> 00:03:02,70
So there you go.

60
00:03:02,70 --> 00:03:04,70
We can set up custom error responses

61
00:03:04,70 --> 00:03:08,20
including both the content and the http error code.

62
00:03:08,20 --> 00:03:10,00
This is pretty easy.

