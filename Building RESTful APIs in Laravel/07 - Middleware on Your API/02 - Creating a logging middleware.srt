1
00:00:00,60 --> 00:00:03,40
- Let's build a simple logging middleware layer

2
00:00:03,40 --> 00:00:04,20
to our application.

3
00:00:04,20 --> 00:00:06,10
And add to it to log all

4
00:00:06,10 --> 00:00:09,00
requests through our API layer.

5
00:00:09,00 --> 00:00:12,60
We'll first run a PHP artisan make command with

6
00:00:12,60 --> 00:00:13,50
php

7
00:00:13,50 --> 00:00:14,60
artisan

8
00:00:14,60 --> 00:00:15,40
make

9
00:00:15,40 --> 00:00:16,40
colon

10
00:00:16,40 --> 00:00:18,30
middleware

11
00:00:18,30 --> 00:00:21,60
and then we'll call this middleware "Logging".

12
00:00:21,60 --> 00:00:24,00
This will create a new middleware class for us

13
00:00:24,00 --> 00:00:26,80
called Logging. We'll look at it now by

14
00:00:26,80 --> 00:00:28,80
opening up our code editor

15
00:00:28,80 --> 00:00:30,70
and opening up in App,

16
00:00:30,70 --> 00:00:32,10
HTTP

17
00:00:32,10 --> 00:00:33,20
Middleware

18
00:00:33,20 --> 00:00:35,70
and Logging.php

19
00:00:35,70 --> 00:00:38,70
You'll notice this middleware has a handle function

20
00:00:38,70 --> 00:00:40,10
that takes a request

21
00:00:40,10 --> 00:00:41,90
and then the next instance.

22
00:00:41,90 --> 00:00:43,50
The next instance of course being

23
00:00:43,50 --> 00:00:45,90
the next middleware class to call.

24
00:00:45,90 --> 00:00:48,70
So now we need to build in our logging feature.

25
00:00:48,70 --> 00:00:51,80
Widen line to include our logging class on line six

26
00:00:51,80 --> 00:00:52,60
with

27
00:00:52,60 --> 00:00:53,60
use

28
00:00:53,60 --> 00:00:56,30
Illuminate

29
00:00:56,30 --> 00:00:58,90
backslash Support

30
00:00:58,90 --> 00:01:01,10
backslash Facades

31
00:01:01,10 --> 00:01:03,60
backslash Log

32
00:01:03,60 --> 00:01:07,80
And on line 19, widen new line that what said has

33
00:01:07,80 --> 00:01:09,80
Log, colon colon,

34
00:01:09,80 --> 00:01:11,10
debug

35
00:01:11,10 --> 00:01:13,40
request

36
00:01:13,40 --> 00:01:15,30
arrow, method.

37
00:01:15,30 --> 00:01:19,70
So this will just log the HTTP method for the request.

38
00:01:19,70 --> 00:01:21,40
Now we can see it in action.

39
00:01:21,40 --> 00:01:23,90
To do so, we need to first add the middleware layer

40
00:01:23,90 --> 00:01:26,10
to our application.

41
00:01:26,10 --> 00:01:29,50
To do this, open up the file Kernel.php

42
00:01:29,50 --> 00:01:33,30
in the App HTTP directory.

43
00:01:33,30 --> 00:01:35,30
Laravel provides for us middleware groups

44
00:01:35,30 --> 00:01:37,20
for the different routes.

45
00:01:37,20 --> 00:01:40,40
You'll note there's a middleware group for web on line 30

46
00:01:40,40 --> 00:01:43,00
and API on line 40.

47
00:01:43,00 --> 00:01:46,50
We'll add to this by adding into the array for API

48
00:01:46,50 --> 00:01:48,40
our middleware class.

49
00:01:48,40 --> 00:01:49,20
Which will be backslash

50
00:01:49,20 --> 00:01:51,20
App

51
00:01:51,20 --> 00:01:53,30
backslash Http

52
00:01:53,30 --> 00:01:54,30
backslash

53
00:01:54,30 --> 00:01:56,80
Middleware

54
00:01:56,80 --> 00:01:59,70
backslash Logging.

55
00:01:59,70 --> 00:02:02,20
Colon colon, class.

56
00:02:02,20 --> 00:02:03,60
Now we can save this

57
00:02:03,60 --> 00:02:05,90
and we'll need to serve up our Laravel application

58
00:02:05,90 --> 00:02:09,20
by going back to our terminal and running the command

59
00:02:09,20 --> 00:02:10,00
"php

60
00:02:10,00 --> 00:02:11,70
artisan

61
00:02:11,70 --> 00:02:12,50
serve".

62
00:02:12,50 --> 00:02:14,70
Now let's open up Insomnia

63
00:02:14,70 --> 00:02:17,40
And we'll send any random request.

64
00:02:17,40 --> 00:02:20,50
We'll do our Get Polls

65
00:02:20,50 --> 00:02:21,90
and then we'll also

66
00:02:21,90 --> 00:02:24,70
delete a new poll.

67
00:02:24,70 --> 00:02:26,90
Now let's go back to our text editor

68
00:02:26,90 --> 00:02:28,20
and we'll look in

69
00:02:28,20 --> 00:02:30,60
where our logs are stored at

70
00:02:30,60 --> 00:02:32,20
Stored

71
00:02:32,20 --> 00:02:33,70
Logs

72
00:02:33,70 --> 00:02:36,30
and Laravel.log

73
00:02:36,30 --> 00:02:39,50
If we scroll all the way to the bottom

74
00:02:39,50 --> 00:02:42,60
we'll see we see two lines in our logging file.

75
00:02:42,60 --> 00:02:44,50
One that says it's a Get request and

76
00:02:44,50 --> 00:02:47,20
one that says it's a Delete request.

77
00:02:47,20 --> 00:02:50,00
So we were able to log the different request methods

78
00:02:50,00 --> 00:02:52,70
that we used for our two different requests.

79
00:02:52,70 --> 00:02:54,10
What about the reverse?

80
00:02:54,10 --> 00:02:56,70
What happens if we want to log something about a response

81
00:02:56,70 --> 00:02:58,60
as opposed to the request?

82
00:02:58,60 --> 00:03:02,10
To do this, let's go back to our logging middleware class

83
00:03:02,10 --> 00:03:05,50
and now we need to add a new function in.

84
00:03:05,50 --> 00:03:09,10
We'll add on line 23,

85
00:03:09,10 --> 00:03:10,20
public

86
00:03:10,20 --> 00:03:11,20
function

87
00:03:11,20 --> 00:03:13,30
terminate

88
00:03:13,30 --> 00:03:15,50
This is going to take a request

89
00:03:15,50 --> 00:03:18,40
and a response.

90
00:03:18,40 --> 00:03:20,90
On line 25, we can add

91
00:03:20,90 --> 00:03:22,40
Log colon colon

92
00:03:22,40 --> 00:03:24,90
debug

93
00:03:24,90 --> 00:03:26,10
response

94
00:03:26,10 --> 00:03:26,90
arrow

95
00:03:26,90 --> 00:03:29,60
status

96
00:03:29,60 --> 00:03:31,60
Now let's go back to Insomnia

97
00:03:31,60 --> 00:03:36,10
and we'll send another request.

98
00:03:36,10 --> 00:03:37,90
And we'll go back to our text editor

99
00:03:37,90 --> 00:03:41,20
and look at the log again and we'll see the Get line

100
00:03:41,20 --> 00:03:45,20
for the request and then 200 for the response.

101
00:03:45,20 --> 00:03:48,20
Now clearly this isn't a very useful logging feature.

102
00:03:48,20 --> 00:03:50,10
But you should be able to take this and build in a

103
00:03:50,10 --> 00:03:52,70
real logging capability if you need to.

104
00:03:52,70 --> 00:03:55,30
But at the very least, the concept of middleware

105
00:03:55,30 --> 00:03:57,10
and how you go about building it

106
00:03:57,10 --> 00:03:59,00
should be more clear at this point.

