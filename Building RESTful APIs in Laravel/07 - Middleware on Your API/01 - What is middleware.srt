1
00:00:00,50 --> 00:00:02,40
- [Instructor] So what is Middleware?

2
00:00:02,40 --> 00:00:06,30
To answer this, we're going to start on a bit of a detour.

3
00:00:06,30 --> 00:00:08,80
PHP has a community-based organization

4
00:00:08,80 --> 00:00:13,00
that proposes standards outside of the core PHP team.

5
00:00:13,00 --> 00:00:16,20
This group, it's called the PHP FIG,

6
00:00:16,20 --> 00:00:17,90
or Framework Inter-op Group.

7
00:00:17,90 --> 00:00:21,00
Their goal is to propose common standards

8
00:00:21,00 --> 00:00:25,60
for PHP projects and tools to work better together.

9
00:00:25,60 --> 00:00:26,90
These standards are called

10
00:00:26,90 --> 00:00:31,00
PHP Standards Recommendations or PSR.

11
00:00:31,00 --> 00:00:33,60
You may have seen or used one or two PSRs

12
00:00:33,60 --> 00:00:36,30
without ever realizing it.

13
00:00:36,30 --> 00:00:39,60
For instance, autoloading that you use with composer

14
00:00:39,60 --> 00:00:44,00
and name spaces uses the PSR-4 standard.

15
00:00:44,00 --> 00:00:46,80
Oh golly. So we're going to talk a bunch of fun stuff

16
00:00:46,80 --> 00:00:50,50
about committees and whatnot, sounds boring.

17
00:00:50,50 --> 00:00:52,60
Okay, well let's get to the reason

18
00:00:52,60 --> 00:00:54,80
we're actually discussing this.

19
00:00:54,80 --> 00:00:59,00
PSR-7 is a standard dealing with HTTP messages

20
00:00:59,00 --> 00:01:02,30
or the request and responses we send and receive

21
00:01:02,30 --> 00:01:04,10
when writing in a PI.

22
00:01:04,10 --> 00:01:07,80
PSR-7 includes the ability to write software

23
00:01:07,80 --> 00:01:10,80
called Middleware. What is Middleware?

24
00:01:10,80 --> 00:01:12,70
Well at it's most basic level,

25
00:01:12,70 --> 00:01:16,00
Middleware is the idea of software that sits in the middle

26
00:01:16,00 --> 00:01:19,40
between your application and the end user.

27
00:01:19,40 --> 00:01:21,80
Middleware gives us the ability to interact

28
00:01:21,80 --> 00:01:24,50
with either a request or a response

29
00:01:24,50 --> 00:01:27,00
and either modify or stop the request

30
00:01:27,00 --> 00:01:29,40
or the response if we need to.

31
00:01:29,40 --> 00:01:32,70
One of powerful aspects of Middleware is the ability

32
00:01:32,70 --> 00:01:35,40
to have Middleware act like the layers of an onion

33
00:01:35,40 --> 00:01:37,50
in which you can stack Middleware

34
00:01:37,50 --> 00:01:40,70
on top of one after the other.

35
00:01:40,70 --> 00:01:43,00
The center of the onion is your application.

36
00:01:43,00 --> 00:01:45,50
Each layer is another piece of Middleware

37
00:01:45,50 --> 00:01:48,20
that has one precise job.

38
00:01:48,20 --> 00:01:50,70
One thing to recognize with what we are doing.

39
00:01:50,70 --> 00:01:54,70
In PSR-7 the requests and responses are immutable.

40
00:01:54,70 --> 00:01:58,60
That is to say, every time we modify the request

41
00:01:58,60 --> 00:02:02,60
or response, we create a new instance of the request

42
00:02:02,60 --> 00:02:05,10
and the corresponding response.

43
00:02:05,10 --> 00:02:07,60
This ensures that any operation

44
00:02:07,60 --> 00:02:10,10
that modifies the request or the response

45
00:02:10,10 --> 00:02:14,70
always creates a new instance of the HTTP message.

46
00:02:14,70 --> 00:02:17,30
So as these messages are passed around

47
00:02:17,30 --> 00:02:19,30
from each layer in our onion,

48
00:02:19,30 --> 00:02:23,50
we create whole new messages at each point potentially.

49
00:02:23,50 --> 00:02:26,40
Well okay, but what might I use Middleware for?

50
00:02:26,40 --> 00:02:28,80
Here's some rough ideas. Logging.

51
00:02:28,80 --> 00:02:31,70
Since your Middleware will see every request and response,

52
00:02:31,70 --> 00:02:34,90
logging is pretty easy to do at the Middleware layer.

53
00:02:34,90 --> 00:02:37,90
You can do rate limit or throttling since

54
00:02:37,90 --> 00:02:41,00
if the Middleware sees the request coming in over its limit,

55
00:02:41,00 --> 00:02:44,40
it's fairly easy for us to stop the request in its tracks.

56
00:02:44,40 --> 00:02:48,40
And you don't have to spin up the rest of your application.

57
00:02:48,40 --> 00:02:52,40
Authentication and authorization has the same idea.

58
00:02:52,40 --> 00:02:54,00
What about adding a specific set

59
00:02:54,00 --> 00:02:55,60
of HTTP headers to a request?

60
00:02:55,60 --> 00:02:57,30
Or if you need to change the date format

61
00:02:57,30 --> 00:02:59,00
for some particular client.

62
00:02:59,00 --> 00:03:01,40
Middleware can do all these things and more.

63
00:03:01,40 --> 00:03:05,20
Recall our idea of Middleware acting as an onion.

64
00:03:05,20 --> 00:03:07,90
One of the advantages with Middleware over building

65
00:03:07,90 --> 00:03:10,70
these features in your application's code is two.

66
00:03:10,70 --> 00:03:14,40
First, the code. Because it's based around a PSR standard

67
00:03:14,40 --> 00:03:16,90
it's movable between frameworks.

68
00:03:16,90 --> 00:03:20,30
So if you decide to switch from Laravel to Symphony,

69
00:03:20,30 --> 00:03:23,10
or some other framework, your Middleware layer,

70
00:03:23,10 --> 00:03:27,10
in theory, should be moved with almost no work.

71
00:03:27,10 --> 00:03:30,20
Secondarily, the Middleware concept means you don't

72
00:03:30,20 --> 00:03:32,10
have to spin up the entire application

73
00:03:32,10 --> 00:03:34,60
to process some request.

74
00:03:34,60 --> 00:03:36,50
Going back to our rate limit idea,

75
00:03:36,50 --> 00:03:39,50
in this case, any request that is over the rate limit

76
00:03:39,50 --> 00:03:42,00
doesn't even hit our Laravel application

77
00:03:42,00 --> 00:03:44,60
and thus it doesn't need a spin-up.

78
00:03:44,60 --> 00:03:47,40
Instead, the Middleware layer deals with it.

79
00:03:47,40 --> 00:03:50,00
The common idea in Middleware is you

80
00:03:50,00 --> 00:03:52,40
have a request and a response object.

81
00:03:52,40 --> 00:03:55,70
In our PHP version, these are specific interfaces

82
00:03:55,70 --> 00:03:59,60
that PSR-7 defines. The request or response

83
00:03:59,60 --> 00:04:01,80
is then returned depending on which part

84
00:04:01,80 --> 00:04:05,30
of the HTTP message you are working with.

85
00:04:05,30 --> 00:04:07,10
That's pretty much all it is.

86
00:04:07,10 --> 00:04:10,10
To chain our Middleware, we added Next parameter

87
00:04:10,10 --> 00:04:12,90
that takes the callable instance of the next Middleware

88
00:04:12,90 --> 00:04:15,70
in the chain and we keep that chain going.

89
00:04:15,70 --> 00:04:18,40
Now remember, at any point we can break the chain

90
00:04:18,40 --> 00:04:20,60
if we need to so Middleware doesn't

91
00:04:20,60 --> 00:04:23,90
have to always call the next Middleware layer.

92
00:04:23,90 --> 00:04:26,00
Here's an example of a Middleware

93
00:04:26,00 --> 00:04:27,90
that just modifies the response

94
00:04:27,90 --> 00:04:30,60
to include a custom HTTP header.

95
00:04:30,60 --> 00:04:33,10
In this case, X-JOBS, where the text

96
00:04:33,10 --> 00:04:36,80
of where to apply for a job at this mythical company.

97
00:04:36,80 --> 00:04:40,20
Here's the same concept of our Middleware layer this time,

98
00:04:40,20 --> 00:04:42,10
however with this time, the Middleware

99
00:04:42,10 --> 00:04:44,20
calls our next Middleware layer.

100
00:04:44,20 --> 00:04:45,90
This is pretty easy, right?

101
00:04:45,90 --> 00:04:48,80
That's almost all the code you'll write for Middleware.

102
00:04:48,80 --> 00:04:50,80
A method you can call that will interact

103
00:04:50,80 --> 00:04:52,70
with the request or the response

104
00:04:52,70 --> 00:04:55,80
and then call the next Middleware in the chain.

105
00:04:55,80 --> 00:04:58,10
So that's Middleware, we have an application

106
00:04:58,10 --> 00:04:59,90
that's the center of an onion.

107
00:04:59,90 --> 00:05:02,20
And with each of these layers of our Middleware,

108
00:05:02,20 --> 00:05:05,90
each layer can view the request and the response.

109
00:05:05,90 --> 00:05:08,90
Each layer can also modify the request

110
00:05:08,90 --> 00:05:11,10
and the corresponding response.

111
00:05:11,10 --> 00:05:12,90
And that's pretty much it.

112
00:05:12,90 --> 00:05:14,70
Middleware is a pretty powerful concept,

113
00:05:14,70 --> 00:05:17,00
especially if you want to write tools that

114
00:05:17,00 --> 00:05:21,00
are framework agnostic, but it's not terribly complicated.

