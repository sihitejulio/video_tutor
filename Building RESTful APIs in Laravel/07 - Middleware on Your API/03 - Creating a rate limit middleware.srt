1
00:00:00,50 --> 00:00:02,20
- [Instructor] Now let's build a slightly

2
00:00:02,20 --> 00:00:03,90
more complex middleware.

3
00:00:03,90 --> 00:00:06,60
This time a layer that modifies our response

4
00:00:06,60 --> 00:00:09,40
to add a custom http header.

5
00:00:09,40 --> 00:00:12,20
This is one of the most common uses for middleware

6
00:00:12,20 --> 00:00:13,50
on the response side.

7
00:00:13,50 --> 00:00:15,20
So it's a good demonstration of

8
00:00:15,20 --> 00:00:17,80
a classic middleware type problem.

9
00:00:17,80 --> 00:00:20,50
To begin we need to make a middleware class.

10
00:00:20,50 --> 00:00:23,70
So we'll run the command P-H-P artisan

11
00:00:23,70 --> 00:00:28,10
make colon middleware

12
00:00:28,10 --> 00:00:31,60
and we'll name this middleware http headers.

13
00:00:31,60 --> 00:00:33,30
Now we can open up our file by

14
00:00:33,30 --> 00:00:35,20
opening up our code editor.

15
00:00:35,20 --> 00:00:37,20
Then opening up the file at app,

16
00:00:37,20 --> 00:00:42,60
http, middleware, http headers dot P-H-P.

17
00:00:42,60 --> 00:00:44,80
To call our middleware after the response

18
00:00:44,80 --> 00:00:46,90
is being passed back, we need to replace

19
00:00:46,90 --> 00:00:49,30
the content of our handle method.

20
00:00:49,30 --> 00:00:51,60
First on line 18,

21
00:00:51,60 --> 00:00:55,00
we'll replace response equal to

22
00:00:55,00 --> 00:00:59,90
next request, and after this we can modify the response

23
00:00:59,90 --> 00:01:04,60
and we'll return the response on line 20.

24
00:01:04,60 --> 00:01:08,20
So that is we're going to be taking the request,

25
00:01:08,20 --> 00:01:10,50
passing it to our next middleware,

26
00:01:10,50 --> 00:01:13,70
we're going to be storing that as a response object,

27
00:01:13,70 --> 00:01:16,10
and then we're going to be returning the response.

28
00:01:16,10 --> 00:01:20,20
And in between we're going to modify the response.

29
00:01:20,20 --> 00:01:22,20
So on line 19, we'll add

30
00:01:22,20 --> 00:01:25,10
response,

31
00:01:25,10 --> 00:01:28,20
arrow header,

32
00:01:28,20 --> 00:01:31,10
pass in the name for our header that we're going to set,

33
00:01:31,10 --> 00:01:34,50
in this case we'll call it X dash jobs.

34
00:01:34,50 --> 00:01:36,00
This X dash

35
00:01:36,00 --> 00:01:38,00
is used commonly

36
00:01:38,00 --> 00:01:41,90
for custom http headers, though it isn't required.

37
00:01:41,90 --> 00:01:44,20
We'll then pass in a string of

38
00:01:44,20 --> 00:01:45,20
come

39
00:01:45,20 --> 00:01:47,90
work with us.

40
00:01:47,90 --> 00:01:50,00
Now we need a repeat like we did last time

41
00:01:50,00 --> 00:01:52,20
by adding our middleware to our route.

42
00:01:52,20 --> 00:01:55,20
We'll open up the file kernel dot P-H-P

43
00:01:55,20 --> 00:01:58,60
in app http.

44
00:01:58,60 --> 00:02:02,10
We'll add this again into our A-P-I group

45
00:02:02,10 --> 00:02:06,60
so on line 44 we'll add backslash app,

46
00:02:06,60 --> 00:02:11,10
backslash http, backslash middleware,

47
00:02:11,10 --> 00:02:14,20
backslash http

48
00:02:14,20 --> 00:02:16,00
headers,

49
00:02:16,00 --> 00:02:19,10
colon, colon, class.

50
00:02:19,10 --> 00:02:21,60
Now let's test and verify that this works.

51
00:02:21,60 --> 00:02:23,80
We need to go back to our terminal

52
00:02:23,80 --> 00:02:26,10
and start our Laravel application

53
00:02:26,10 --> 00:02:29,60
with P-H-P artisan serve.

54
00:02:29,60 --> 00:02:32,90
And now we can go to Insomnia and send a request.

55
00:02:32,90 --> 00:02:34,20
And if we look at the headers,

56
00:02:34,20 --> 00:02:36,90
we'll see we have an X jobs headers

57
00:02:36,90 --> 00:02:39,90
that has our come work with us string.

58
00:02:39,90 --> 00:02:41,70
So this was pretty easy.

59
00:02:41,70 --> 00:02:44,40
What happens if we want to pass a flag to our middleware?

60
00:02:44,40 --> 00:02:47,70
For instance we want to pass in a custom string.

61
00:02:47,70 --> 00:02:49,30
How might that work?

62
00:02:49,30 --> 00:02:52,30
To do this, let's go back to our kernel file

63
00:02:52,30 --> 00:02:55,80
in our text editor, and let's modify our line

64
00:02:55,80 --> 00:02:59,20
for setting the http headers middleware class.

65
00:02:59,20 --> 00:03:01,80
In this case, we'll add a key here

66
00:03:01,80 --> 00:03:04,40
with a single string attached.

67
00:03:04,40 --> 00:03:08,00
Our string will be http headers.

68
00:03:08,00 --> 00:03:11,00
So line 44 reads as http headers

69
00:03:11,00 --> 00:03:14,40
is equal to the instance of the http headers class.

70
00:03:14,40 --> 00:03:18,90
Now what we need to do is take this line,

71
00:03:18,90 --> 00:03:21,50
we'll copy it, remove it,

72
00:03:21,50 --> 00:03:22,80
and we're going to place it into

73
00:03:22,80 --> 00:03:25,90
our route middleware instance.

74
00:03:25,90 --> 00:03:27,80
On line 62.

75
00:03:27,80 --> 00:03:30,80
Now we can take our key http headers

76
00:03:30,80 --> 00:03:33,30
and add that as line 44,

77
00:03:33,30 --> 00:03:35,80
add a colon inside of the string,

78
00:03:35,80 --> 00:03:38,70
and pass in the string that we want to pass in.

79
00:03:38,70 --> 00:03:43,00
In this case, we'll add in come work for us

80
00:03:43,00 --> 00:03:45,20
with an exclamation mark.

81
00:03:45,20 --> 00:03:46,90
So notice what we did.

82
00:03:46,90 --> 00:03:51,30
We created a key for our http headers middleware

83
00:03:51,30 --> 00:03:52,90
so we have a name for it,

84
00:03:52,90 --> 00:03:57,20
and then we binded it into our A-P-I middleware group

85
00:03:57,20 --> 00:04:01,00
along with passing in a custom text value.

86
00:04:01,00 --> 00:04:02,80
We save this kernel and now we

87
00:04:02,80 --> 00:04:05,40
open up http headers.

88
00:04:05,40 --> 00:04:07,40
And we need to modify our handle function

89
00:04:07,40 --> 00:04:10,00
to accept this custom parameter.

90
00:04:10,00 --> 00:04:12,60
So our handle function on line 16

91
00:04:12,60 --> 00:04:15,10
is going to be updated to add a string,

92
00:04:15,10 --> 00:04:19,40
text, and we'll set a default value of an empty string.

93
00:04:19,40 --> 00:04:22,70
Unlike 19, we can replace the come work with us

94
00:04:22,70 --> 00:04:24,70
with our text variable.

95
00:04:24,70 --> 00:04:29,00
Save this, open up Insomnia and resend our request,

96
00:04:29,00 --> 00:04:31,70
and we'll see we get our X jobs header

97
00:04:31,70 --> 00:04:35,70
with the new custom string that we passed in.

98
00:04:35,70 --> 00:04:38,00
So this is a much more advanced middleware layer

99
00:04:38,00 --> 00:04:41,10
that we're able to build inside of Laravel.

100
00:04:41,10 --> 00:04:42,70
This is about as advanced as you're

101
00:04:42,70 --> 00:04:45,60
typically going to need for most middleware.

102
00:04:45,60 --> 00:04:47,50
There's more ability to do more advanced

103
00:04:47,50 --> 00:04:49,70
things with middleware, but these basic

104
00:04:49,70 --> 00:04:51,70
patterns that we've seen demonstrated,

105
00:04:51,70 --> 00:04:53,60
are going to be how you're going to build out

106
00:04:53,60 --> 00:04:57,00
any future middleware inside of your Laravel application.

