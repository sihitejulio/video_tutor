1
00:00:00,50 --> 00:00:03,10
- [Instructor] Now we all have a solid understanding

2
00:00:03,10 --> 00:00:06,40
and a framework to build your next great API.

3
00:00:06,40 --> 00:00:09,50
We figured out the basics of an API in Laravel,

4
00:00:09,50 --> 00:00:13,40
adding records, editing records, validating the data,

5
00:00:13,40 --> 00:00:15,70
and even returning errors.

6
00:00:15,70 --> 00:00:17,60
We worked through building middleware

7
00:00:17,60 --> 00:00:20,20
and transforming our data in our API

8
00:00:20,20 --> 00:00:21,70
as well as ensuring we only allow

9
00:00:21,70 --> 00:00:25,10
certain authorized access to our API.

10
00:00:25,10 --> 00:00:27,40
The library contains a wealth of information

11
00:00:27,40 --> 00:00:31,10
on building APIs and the design of APIs.

12
00:00:31,10 --> 00:00:33,00
You should also consider taking a look

13
00:00:33,00 --> 00:00:36,90
at the Laravel documentation site at laravel.com.

14
00:00:36,90 --> 00:00:39,00
This includes a ton of information

15
00:00:39,00 --> 00:00:41,10
both on Laravel itself as well as

16
00:00:41,10 --> 00:00:44,20
building and working with APIs in Laravel.

17
00:00:44,20 --> 00:00:46,30
If you want to get more general knowledge

18
00:00:46,30 --> 00:00:48,50
about building APIs itself,

19
00:00:48,50 --> 00:00:51,90
take a look at the book, Build APIs You Won't Hate.

20
00:00:51,90 --> 00:00:54,20
This book has a wealth of information

21
00:00:54,20 --> 00:00:57,20
to give you the ability to build an API

22
00:00:57,20 --> 00:00:59,60
while avoiding the most common pratfalls

23
00:00:59,60 --> 00:01:01,80
in building an API.

24
00:01:01,80 --> 00:01:06,00
Now have fun out there writing some new and awesome APIs.

