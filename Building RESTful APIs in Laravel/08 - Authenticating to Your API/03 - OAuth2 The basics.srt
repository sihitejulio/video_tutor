1
00:00:00,60 --> 00:00:04,70
- [Instructor] OAuth is a standard developed back in 2007.

2
00:00:04,70 --> 00:00:07,00
The goal with OAuth was to provide

3
00:00:07,00 --> 00:00:10,60
for the ability for tools like Twitter, Google and similar

4
00:00:10,60 --> 00:00:13,20
to permit access to an API authenticating

5
00:00:13,20 --> 00:00:15,60
as a certain user while at the same time

6
00:00:15,60 --> 00:00:18,00
not handing out your username and password

7
00:00:18,00 --> 00:00:20,00
to every site that might want

8
00:00:20,00 --> 00:00:24,50
to use your API credentials like you would with basic Auth.

9
00:00:24,50 --> 00:00:29,50
OAuth 2 was a reworking of OAuth 1, developed in 2012.

10
00:00:29,50 --> 00:00:33,40
One of the key changes was OAuth 2 is more like a framework

11
00:00:33,40 --> 00:00:37,90
or a pattern to approach towards building API access.

12
00:00:37,90 --> 00:00:41,70
There are some very standard OAuth 2 implementations.

13
00:00:41,70 --> 00:00:45,90
We'll use one, but some companies modify the OAuth 2 flow

14
00:00:45,90 --> 00:00:48,30
for their particular needs.

15
00:00:48,30 --> 00:00:50,50
What is the OAuth 2 flow?

16
00:00:50,50 --> 00:00:53,90
The flow is the mechanism by which you, a user,

17
00:00:53,90 --> 00:00:56,60
grant an external service API credentials

18
00:00:56,60 --> 00:00:59,00
to talk to whatever API.

19
00:00:59,00 --> 00:01:01,30
A more concrete example here is when

20
00:01:01,30 --> 00:01:03,20
you give your Twitter client access

21
00:01:03,20 --> 00:01:05,70
to the Twitter API to log in as you

22
00:01:05,70 --> 00:01:07,30
and to tweet as you.

23
00:01:07,30 --> 00:01:10,70
That's what's going on, you're using OAuth 2.

24
00:01:10,70 --> 00:01:12,80
The client is getting your credentials

25
00:01:12,80 --> 00:01:15,10
to talk to the Twitter API.

26
00:01:15,10 --> 00:01:16,60
So, here is what happens.

27
00:01:16,60 --> 00:01:18,30
You try to use some client,

28
00:01:18,30 --> 00:01:20,70
sticking with our concept of a Twitter client,

29
00:01:20,70 --> 00:01:22,70
you try to use a Twitter client.

30
00:01:22,70 --> 00:01:25,50
The Twitter client says, okay, for you to use me,

31
00:01:25,50 --> 00:01:29,30
I need access to your credentials for your Twitter login.

32
00:01:29,30 --> 00:01:31,90
So, I direct you to an Authorization Server

33
00:01:31,90 --> 00:01:33,40
or where you can log in

34
00:01:33,40 --> 00:01:36,70
and authorize the client to talk to the Twitter API.

35
00:01:36,70 --> 00:01:38,60
Then there is a redirect back,

36
00:01:38,60 --> 00:01:41,30
passing some tokens that the client can then use

37
00:01:41,30 --> 00:01:43,70
to talk to the API Server.

38
00:01:43,70 --> 00:01:47,40
The details of this pattern may differ around the edges,

39
00:01:47,40 --> 00:01:49,50
but the basic pattern, open a client,

40
00:01:49,50 --> 00:01:52,10
client redirects you to log in,

41
00:01:52,10 --> 00:01:55,10
and then you get directed back to the client

42
00:01:55,10 --> 00:01:58,00
and now the client can talk to the API.

43
00:01:58,00 --> 00:02:00,30
The idea here, again, is that OAuth 2

44
00:02:00,30 --> 00:02:02,40
provides this framework towards

45
00:02:02,40 --> 00:02:05,60
building an implementation rather than a very strict

46
00:02:05,60 --> 00:02:07,50
and rigorous standard.

47
00:02:07,50 --> 00:02:10,30
One of the distinct advantages with OAuth, you'll note,

48
00:02:10,30 --> 00:02:13,90
is that both the API and the user can control the clients

49
00:02:13,90 --> 00:02:16,10
with access to their account.

50
00:02:16,10 --> 00:02:17,90
If you've used Twitter clients before

51
00:02:17,90 --> 00:02:20,30
or hooked up services to your Google account,

52
00:02:20,30 --> 00:02:22,20
you can see the list of services

53
00:02:22,20 --> 00:02:23,80
that have access to your account

54
00:02:23,80 --> 00:02:26,90
and remove their access without going through the site,

55
00:02:26,90 --> 00:02:29,60
entrusting that external client.

56
00:02:29,60 --> 00:02:32,00
The same is true of the API service.

57
00:02:32,00 --> 00:02:34,70
If Twitter finds out some client is acting nefarious

58
00:02:34,70 --> 00:02:36,00
and breaking the rules,

59
00:02:36,00 --> 00:02:38,40
they can disable every API client

60
00:02:38,40 --> 00:02:40,40
that client has ever created

61
00:02:40,40 --> 00:02:43,40
and block new tokens from being created.

62
00:02:43,40 --> 00:02:47,00
We're going to explore OAuth 2 in a little more detail now.

63
00:02:47,00 --> 00:02:50,70
First up, we're going to explore the grant types.

64
00:02:50,70 --> 00:02:51,90
Grant types are the way

65
00:02:51,90 --> 00:02:55,50
in which we grant a client access to an API.

66
00:02:55,50 --> 00:02:57,70
Authorization code is where the service

67
00:02:57,70 --> 00:03:00,20
passes a code back to the application

68
00:03:00,20 --> 00:03:04,20
which it then uses to authorize itself with the API.

69
00:03:04,20 --> 00:03:05,50
Password works the same,

70
00:03:05,50 --> 00:03:08,90
except rather than logging into the application itself,

71
00:03:08,90 --> 00:03:12,00
you give your client the username and password

72
00:03:12,00 --> 00:03:14,50
and it logs you into the API Server,

73
00:03:14,50 --> 00:03:17,00
but you still use a token.

74
00:03:17,00 --> 00:03:20,30
Finally, client credentials are used when the client needs

75
00:03:20,30 --> 00:03:22,90
to access the API as itself.

76
00:03:22,90 --> 00:03:25,00
We're not going to worry about this at all.

77
00:03:25,00 --> 00:03:27,60
It's a pretty minimal use case.

78
00:03:27,60 --> 00:03:30,40
OAuth also provides for a field called scope.

79
00:03:30,40 --> 00:03:32,70
You've seen this before when you've used a Twitter app

80
00:03:32,70 --> 00:03:34,00
and Twitter tells you exactly

81
00:03:34,00 --> 00:03:36,80
what the application will be able to do,

82
00:03:36,80 --> 00:03:37,80
or for Google.

83
00:03:37,80 --> 00:03:40,60
You may have a scope that says this application can read

84
00:03:40,60 --> 00:03:42,20
my contacts and my emails,

85
00:03:42,20 --> 00:03:44,80
but he can't access my Google Docs.

86
00:03:44,80 --> 00:03:47,20
So, you can grant an application access

87
00:03:47,20 --> 00:03:50,90
to manage your contacts, but not your Google Docs.

88
00:03:50,90 --> 00:03:52,90
That is the point of scope.

89
00:03:52,90 --> 00:03:54,90
This can also neatly tie directly

90
00:03:54,90 --> 00:03:58,00
to an API endpoint if we desire.

91
00:03:58,00 --> 00:03:59,60
So, this is what happens.

92
00:03:59,60 --> 00:04:03,00
We're on a site and we want to use an API with it.

93
00:04:03,00 --> 00:04:06,00
We first click a link similar to this,

94
00:04:06,00 --> 00:04:08,40
pointing us to the Authorization Server

95
00:04:08,40 --> 00:04:10,40
where we're going to log in.

96
00:04:10,40 --> 00:04:12,00
This passes a client ID,

97
00:04:12,00 --> 00:04:15,00
which is a secret used to identify the client,

98
00:04:15,00 --> 00:04:17,00
we pass a redirect URL,

99
00:04:17,00 --> 00:04:19,90
which says this is where to go after we're done,

100
00:04:19,90 --> 00:04:22,10
the scope if we need to pass in a scope,

101
00:04:22,10 --> 00:04:24,00
and finally, a state value,

102
00:04:24,00 --> 00:04:27,60
which is a random string, basically used as a nonce,

103
00:04:27,60 --> 00:04:29,20
or a one-time value,

104
00:04:29,20 --> 00:04:31,60
to verify that this isn't being replayed

105
00:04:31,60 --> 00:04:34,20
or done by a nefarious actor.

106
00:04:34,20 --> 00:04:36,80
Now the user logs into the application

107
00:04:36,80 --> 00:04:41,20
and then the app will redirect us back to the client.

108
00:04:41,20 --> 00:04:44,00
When they get redirected, they pass back the code

109
00:04:44,00 --> 00:04:46,90
that we'll use as our token when logging in

110
00:04:46,90 --> 00:04:49,50
and that's basically OAuth 2.

111
00:04:49,50 --> 00:04:51,80
OAuth 2 can be much more complicated

112
00:04:51,80 --> 00:04:54,20
and there are a lot of tricky things to get right,

113
00:04:54,20 --> 00:04:56,50
so it's important to do a few things.

114
00:04:56,50 --> 00:04:58,70
First is you should almost never write

115
00:04:58,70 --> 00:05:00,90
your own implementation of OAuth 2.

116
00:05:00,90 --> 00:05:03,30
It's a world of hassle.

117
00:05:03,30 --> 00:05:05,30
Second, keep it simple.

118
00:05:05,30 --> 00:05:06,50
Before trying to do a bunch

119
00:05:06,50 --> 00:05:09,00
of complicated things with OAuth,

120
00:05:09,00 --> 00:05:12,80
build out a standard, simple application with OAuth

121
00:05:12,80 --> 00:05:15,90
and then let your application grow into new use cases

122
00:05:15,90 --> 00:05:17,00
as you need it.

