1
00:00:00,50 --> 00:00:01,90
- [Instructor] Now we'll do the second half

2
00:00:01,90 --> 00:00:04,30
of our OAuth application.

3
00:00:04,30 --> 00:00:07,40
First, we need to create a client application

4
00:00:07,40 --> 00:00:09,60
that we're going to be able to get the tokens

5
00:00:09,60 --> 00:00:12,60
to be able to talk to our API server.

6
00:00:12,60 --> 00:00:16,40
We'll create a new layer of L client application.

7
00:00:16,40 --> 00:00:17,60
What you need to do

8
00:00:17,60 --> 00:00:20,20
is you need to go up at directory

9
00:00:20,20 --> 00:00:24,40
in your terminal application from where community poll lives

10
00:00:24,40 --> 00:00:26,50
and we're going to create a new project.

11
00:00:26,50 --> 00:00:27,90
So composer

12
00:00:27,90 --> 00:00:31,00
create dash project

13
00:00:31,00 --> 00:00:32,10
dash dash

14
00:00:32,10 --> 00:00:35,10
prefer dash D I S T

15
00:00:35,10 --> 00:00:36,60
laravel

16
00:00:36,60 --> 00:00:40,30
forward slash laravel and then we'll

17
00:00:40,30 --> 00:00:45,50
pass the name of the project being client dash server.

18
00:00:45,50 --> 00:00:47,50
Now we'll need to move into the directory

19
00:00:47,50 --> 00:00:50,10
for our client server by changing directory

20
00:00:50,10 --> 00:00:52,20
into client server.

21
00:00:52,20 --> 00:00:54,30
We need the add a new composer dependency,

22
00:00:54,30 --> 00:00:56,70
in this case it's going to be guzzle.

23
00:00:56,70 --> 00:00:59,70
So inside of client server, we'll run composer

24
00:00:59,70 --> 00:01:01,00
require

25
00:01:01,00 --> 00:01:02,40
guzzle

26
00:01:02,40 --> 00:01:07,10
H T T P forward slash guzzle.

27
00:01:07,10 --> 00:01:09,40
At this point, we need to create two routes

28
00:01:09,40 --> 00:01:12,40
for our client application, a redirect URL

29
00:01:12,40 --> 00:01:14,80
and a callback URL.

30
00:01:14,80 --> 00:01:18,00
We'll open up our text editor.

31
00:01:18,00 --> 00:01:22,20
We need to open up our client server application,

32
00:01:22,20 --> 00:01:25,00
and inside of here, we need to open up the routes

33
00:01:25,00 --> 00:01:27,10
and the web dot PHP,

34
00:01:27,10 --> 00:01:29,80
as these routes need to be accessible from the web.

35
00:01:29,80 --> 00:01:31,90
They're not API routes.

36
00:01:31,90 --> 00:01:35,20
At this point, open up the snippets.txt file

37
00:01:35,20 --> 00:01:38,20
provided in the exercise files for this lesson.

38
00:01:38,20 --> 00:01:41,10
You want to copy an paste this entire file.

39
00:01:41,10 --> 00:01:43,30
Go back to our text editor

40
00:01:43,30 --> 00:01:47,20
and paste this in at the bottom of the web.php file

41
00:01:47,20 --> 00:01:50,70
inside of client server.

42
00:01:50,70 --> 00:01:54,00
Now we need to go back to our terminal application.

43
00:01:54,00 --> 00:01:59,40
We need to go to the directory for our community poll lab

44
00:01:59,40 --> 00:02:02,80
and inside of here, we need to run the command PHP

45
00:02:02,80 --> 00:02:07,10
artisan passport client, 'cause we're going to create

46
00:02:07,10 --> 00:02:08,80
a new client.

47
00:02:08,80 --> 00:02:11,50
It will ask what we want to assign the user ID,

48
00:02:11,50 --> 00:02:14,80
in this case, we'll assign it to the user ID of one.

49
00:02:14,80 --> 00:02:16,90
It will ask what we want to name the client.

50
00:02:16,90 --> 00:02:18,90
Feel free to name it whatever you want.

51
00:02:18,90 --> 00:02:22,40
I'm going to name it just Client Server.

52
00:02:22,40 --> 00:02:24,10
It'll then ask us where we want to

53
00:02:24,10 --> 00:02:26,00
redirect the requests to.

54
00:02:26,00 --> 00:02:28,50
In this case, we'll want to redirect it to

55
00:02:28,50 --> 00:02:31,70
H T T P colon, forward slash, forward slash,

56
00:02:31,70 --> 00:02:35,80
one two seven, point zero, point zero, point one,

57
00:02:35,80 --> 00:02:39,40
and then port eight thousand and one,

58
00:02:39,40 --> 00:02:41,40
forward slash call back.

59
00:02:41,40 --> 00:02:43,70
So this is going to redirect our request

60
00:02:43,70 --> 00:02:46,60
after it's authorized back to our client server,

61
00:02:46,60 --> 00:02:50,20
which is going to be running on port 8001.

62
00:02:50,20 --> 00:02:52,30
After this, it'll print out some information,

63
00:02:52,30 --> 00:02:54,90
including a client secret.

64
00:02:54,90 --> 00:02:56,80
You'll need to copy this value,

65
00:02:56,80 --> 00:03:00,40
the client secret, at this time.

66
00:03:00,40 --> 00:03:05,20
Now let's go back to our code editor, and at this point,

67
00:03:05,20 --> 00:03:08,70
we want to open up in our Client Server application

68
00:03:08,70 --> 00:03:13,60
the routes, web.php file that we just edited.

69
00:03:13,60 --> 00:03:17,90
In here there's a client secret field

70
00:03:17,90 --> 00:03:19,40
on line 36.

71
00:03:19,40 --> 00:03:21,00
We need to replace this

72
00:03:21,00 --> 00:03:23,30
with your client secret.

73
00:03:23,30 --> 00:03:27,20
This client secret is going to be different for each client.

74
00:03:27,20 --> 00:03:30,90
We also need a pass-in for whatever the client ID is.

75
00:03:30,90 --> 00:03:34,80
In my case, it's five, in your case, it may be three.

76
00:03:34,80 --> 00:03:37,00
If you also updated the client ID,

77
00:03:37,00 --> 00:03:40,00
you also need to update it on line 21.

78
00:03:40,00 --> 00:03:43,20
Now we need to go back to our terminal

79
00:03:43,20 --> 00:03:45,50
and at this time we need to start up

80
00:03:45,50 --> 00:03:47,80
our community poll application.

81
00:03:47,80 --> 00:03:52,50
Go ahead and start it up with P H P artisan serve.

82
00:03:52,50 --> 00:03:54,90
So that's our community poll application.

83
00:03:54,90 --> 00:03:57,40
We now need to create a new

84
00:03:57,40 --> 00:04:00,60
instance of our terminal,

85
00:04:00,60 --> 00:04:03,90
go to our directory

86
00:04:03,90 --> 00:04:07,70
for our client server, and we need to start this one up

87
00:04:07,70 --> 00:04:12,40
with P H P artisan serve, and we need to pass-in

88
00:04:12,40 --> 00:04:14,40
a port parameter

89
00:04:14,40 --> 00:04:17,40
of 8001.

90
00:04:17,40 --> 00:04:19,90
So our standard API

91
00:04:19,90 --> 00:04:23,70
community poll is running on port 8000,

92
00:04:23,70 --> 00:04:28,20
and our client application is running on 8001.

93
00:04:28,20 --> 00:04:29,90
Now we need to go to our browser

94
00:04:29,90 --> 00:04:32,00
and we're going to try it all out.

95
00:04:32,00 --> 00:04:34,60
We'll go to first our

96
00:04:34,60 --> 00:04:39,00
client server at port 8001 and we'll go to the URL

97
00:04:39,00 --> 00:04:41,10
and redirect.

98
00:04:41,10 --> 00:04:44,50
Redirect is going to ask us to login to

99
00:04:44,50 --> 00:04:47,30
our community poll application.

100
00:04:47,30 --> 00:04:51,50
Notice, we're on port 8000 forward slash login.

101
00:04:51,50 --> 00:04:55,80
Now we need to login using a username and password.

102
00:04:55,80 --> 00:04:59,80
Recall, the password is secret and we can get the username

103
00:04:59,80 --> 00:05:03,20
by opening up Insomnia and copying the username

104
00:05:03,20 --> 00:05:06,20
from our basic auth requests.

105
00:05:06,20 --> 00:05:08,90
We'll log in.

106
00:05:08,90 --> 00:05:10,80
Now we'll get redirected here

107
00:05:10,80 --> 00:05:13,30
to an authorization request page.

108
00:05:13,30 --> 00:05:16,20
Our community poll application is asking us

109
00:05:16,20 --> 00:05:19,40
do we want the client server to be able to access

110
00:05:19,40 --> 00:05:21,10
our account?

111
00:05:21,10 --> 00:05:24,30
In this case, yes, we do want it to be able to access it,

112
00:05:24,30 --> 00:05:28,40
so we'll authorize it and now we'll get directed

113
00:05:28,40 --> 00:05:31,20
to a page that displays a bunch of JSON.

114
00:05:31,20 --> 00:05:34,10
The main part of this is it's going to include

115
00:05:34,10 --> 00:05:36,50
an access token field.

116
00:05:36,50 --> 00:05:40,10
With this, we can pass it as a bearer authorization token

117
00:05:40,10 --> 00:05:42,60
in the authorization HTTP header,

118
00:05:42,60 --> 00:05:45,40
and we'll get access to the API.

119
00:05:45,40 --> 00:05:49,60
Now to do this, we need to go back to our code editor,

120
00:05:49,60 --> 00:05:54,50
we need to open up app HTTP kernel dot P H P

121
00:05:54,50 --> 00:05:56,90
inside of our community poll application.

122
00:05:56,90 --> 00:06:00,10
So be sure you're editing your community poll application

123
00:06:00,10 --> 00:06:04,80
and open up app HTTP kernel dot P H P.

124
00:06:04,80 --> 00:06:06,80
You'll know you're for sure in the right one

125
00:06:06,80 --> 00:06:09,70
when you look at the API Middlewear group,

126
00:06:09,70 --> 00:06:12,70
and you'll see it has the Basic Auth and the HTTP headers

127
00:06:12,70 --> 00:06:14,50
Middlewear group.

128
00:06:14,50 --> 00:06:21,80
On line 45, replace the Basic Auth with the string

129
00:06:21,80 --> 00:06:22,90
auth

130
00:06:22,90 --> 00:06:25,20
colon API.

131
00:06:25,20 --> 00:06:29,70
This says for the routing API, use the configured API

132
00:06:29,70 --> 00:06:34,40
authentication setup, which recall was passport.

133
00:06:34,40 --> 00:06:37,00
Now open up Insomnia and go to Requests

134
00:06:37,00 --> 00:06:39,30
and try to send a request.

135
00:06:39,30 --> 00:06:42,90
In this case, with our basic authentication in place,

136
00:06:42,90 --> 00:06:45,50
we'll send it, and you'll note it directs us

137
00:06:45,50 --> 00:06:47,40
to a login page.

138
00:06:47,40 --> 00:06:51,50
That's because we no longer accept basic authentication.

139
00:06:51,50 --> 00:06:56,20
Now in this case, we'll change it to accept a bearer token

140
00:06:56,20 --> 00:06:58,50
and we'll need to paste in a token.

141
00:06:58,50 --> 00:07:01,30
To get the token, go back to the browser

142
00:07:01,30 --> 00:07:07,50
and copy the access token field.

143
00:07:07,50 --> 00:07:11,50
Paste in the token, and send the request again.

144
00:07:11,50 --> 00:07:14,70
And now, you'll see that we were able to use our tokens

145
00:07:14,70 --> 00:07:17,40
to be able to log into our API.

146
00:07:17,40 --> 00:07:20,40
So we're able to build a client application

147
00:07:20,40 --> 00:07:25,30
that is able to access our API using this bearer token.

148
00:07:25,30 --> 00:07:28,00
What you would need to do if you were building a client

149
00:07:28,00 --> 00:07:30,50
is you would need to take this JSON

150
00:07:30,50 --> 00:07:34,10
and save the access tokens along with the token type.

151
00:07:34,10 --> 00:07:37,00
And the Expires In field tells you how long

152
00:07:37,00 --> 00:07:39,30
this token will last for.

153
00:07:39,30 --> 00:07:41,40
In this case, our token's going to

154
00:07:41,40 --> 00:07:43,70
last for a fairly long period of time.

155
00:07:43,70 --> 00:07:46,70
We're probably not going to need to refresh it.

156
00:07:46,70 --> 00:07:48,70
And there are some other edge cases that you may

157
00:07:48,70 --> 00:07:51,60
need to deal with if you're needing to build out scope

158
00:07:51,60 --> 00:07:54,30
or any of this other features OAuth.

159
00:07:54,30 --> 00:07:56,80
At this point, however, this is pretty basic

160
00:07:56,80 --> 00:07:59,20
and this is probably mostly what you're going to use

161
00:07:59,20 --> 00:08:03,30
when you build out OAuth authentication in your API.

162
00:08:03,30 --> 00:08:06,10
So you should be able to build out the OAuth

163
00:08:06,10 --> 00:08:08,40
authentication, implementation

164
00:08:08,40 --> 00:08:12,20
as well as understand how to build a client to be able to

165
00:08:12,20 --> 00:08:14,00
access and build out those tokens.

