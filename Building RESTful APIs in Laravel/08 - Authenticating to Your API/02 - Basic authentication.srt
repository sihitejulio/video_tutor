1
00:00:00,60 --> 00:00:03,00
- [Narrator] Basic authentication is fairly similar

2
00:00:03,00 --> 00:00:05,30
to our token authentication.

3
00:00:05,30 --> 00:00:07,70
Basic authentication is a standard way

4
00:00:07,70 --> 00:00:12,50
to send a username and a password as an HTTP header.

5
00:00:12,50 --> 00:00:15,80
This is implemented by browsers and API tools.

6
00:00:15,80 --> 00:00:17,80
If you've added where you get a dialogue box

7
00:00:17,80 --> 00:00:19,80
from the browser before hitting a site,

8
00:00:19,80 --> 00:00:21,00
that is basic auth,

9
00:00:21,00 --> 00:00:24,40
implemented at the HTTP webserver layer.

10
00:00:24,40 --> 00:00:26,60
The way basic authentication works,

11
00:00:26,60 --> 00:00:28,90
is it adds an authorization header

12
00:00:28,90 --> 00:00:34,10
with the text basic, space, and then base64 encoded,

13
00:00:34,10 --> 00:00:37,50
the username, colon, and then a password.

14
00:00:37,50 --> 00:00:40,50
Basic authentication gives us the advantage

15
00:00:40,50 --> 00:00:42,60
of providing some basic security around

16
00:00:42,60 --> 00:00:44,20
a username and a password.

17
00:00:44,20 --> 00:00:47,10
But if you aren't using SSL on your request,

18
00:00:47,10 --> 00:00:50,60
the username and password are essentially in plain text,

19
00:00:50,60 --> 00:00:52,40
and anyone can read it.

20
00:00:52,40 --> 00:00:55,20
Basic authentication is an okay mechanism

21
00:00:55,20 --> 00:00:57,30
to pass this information around,

22
00:00:57,30 --> 00:01:00,20
but it's not the best solution we have today.

23
00:01:00,20 --> 00:01:02,30
But it is pretty easy to build.

24
00:01:02,30 --> 00:01:04,30
So, we're going to go ahead and do that now.

25
00:01:04,30 --> 00:01:07,00
Let's open up our terminal application at this time,

26
00:01:07,00 --> 00:01:11,50
and we'll run the command PHP, artisan, make, colon,

27
00:01:11,50 --> 00:01:13,00
middleware,

28
00:01:13,00 --> 00:01:17,00
and we'll make a middleware called basic auth.

29
00:01:17,00 --> 00:01:19,90
Now let's open up our code editor.

30
00:01:19,90 --> 00:01:24,80
And we'll open up our middleware at app, HTTP, middleware,

31
00:01:24,80 --> 00:01:27,50
basic auth, dot PHP.

32
00:01:27,50 --> 00:01:30,00
Laravel actually provides for us the pre-built

33
00:01:30,00 --> 00:01:31,90
middleware layer to protect our API

34
00:01:31,90 --> 00:01:34,10
with basic authentication.

35
00:01:34,10 --> 00:01:38,50
To do this, all we need to do is rewrite line 18

36
00:01:38,50 --> 00:01:39,90
to read as,

37
00:01:39,90 --> 00:01:42,30
return,

38
00:01:42,30 --> 00:01:46,80
we'll call the class auth, colon colon, once basic,

39
00:01:46,80 --> 00:01:51,30
and then question mark, colon, and then next,

40
00:01:51,30 --> 00:01:54,00
and then pass in the request.

41
00:01:54,00 --> 00:01:56,30
This says to validate our request

42
00:01:56,30 --> 00:01:58,20
using basic authentication,

43
00:01:58,20 --> 00:02:00,20
and if it validates, we'll go ahead

44
00:02:00,20 --> 00:02:02,30
and pass on to the next request.

45
00:02:02,30 --> 00:02:04,00
Else, will return false.

46
00:02:04,00 --> 00:02:07,40
Now we need to add in our facade to access auth.

47
00:02:07,40 --> 00:02:10,90
So, under our used closure line, we'll add use,

48
00:02:10,90 --> 00:02:17,70
illuminate, backslash, support, backslash, facades,

49
00:02:17,70 --> 00:02:19,10
backslash auth.

50
00:02:19,10 --> 00:02:20,70
And then we need to register our middleware

51
00:02:20,70 --> 00:02:22,60
as we've done in the past.

52
00:02:22,60 --> 00:02:26,30
We'll open up our HTTP colonel add app, HTTP

53
00:02:26,30 --> 00:02:28,60
colonel dot PHP.

54
00:02:28,60 --> 00:02:31,90
We'll scroll down, and replace line 44,

55
00:02:31,90 --> 00:02:35,40
where we set up our token auth with basic auth.

56
00:02:35,40 --> 00:02:38,00
And now we'll need to find a set of user credentials

57
00:02:38,00 --> 00:02:39,90
for our application to use.

58
00:02:39,90 --> 00:02:43,30
We'll need to open up our database in DB browser.

59
00:02:43,30 --> 00:02:47,10
In here, we can look at our user's table,

60
00:02:47,10 --> 00:02:48,90
and we'll see we have some emails,

61
00:02:48,90 --> 00:02:50,30
which are going to be our usernames,

62
00:02:50,30 --> 00:02:53,10
and then we have passwords.

63
00:02:53,10 --> 00:02:57,20
First, we'll select the user,

64
00:02:57,20 --> 00:02:58,90
copy their username,

65
00:02:58,90 --> 00:03:03,10
in this case, our username is going to be the Emo address.

66
00:03:03,10 --> 00:03:05,60
We'll open up Insomnia,

67
00:03:05,60 --> 00:03:09,60
and we'll add in an authentication header to our request.

68
00:03:09,60 --> 00:03:13,20
So, we'll select auth, select basic auth,

69
00:03:13,20 --> 00:03:15,40
we'll paste in the username,

70
00:03:15,40 --> 00:03:18,30
and then we need to fill in the password.

71
00:03:18,30 --> 00:03:21,30
Now, for the password, Laravel uses a standard password

72
00:03:21,30 --> 00:03:24,80
when it builds up the users that it generates.

73
00:03:24,80 --> 00:03:26,30
In this case, it's secret.

74
00:03:26,30 --> 00:03:30,50
That's S E C R E T.

75
00:03:30,50 --> 00:03:33,50
Now we need to start up our Laravel application,

76
00:03:33,50 --> 00:03:35,30
by going back to our terminal editor,

77
00:03:35,30 --> 00:03:38,10
and writing PHP, artisan, serve.

78
00:03:38,10 --> 00:03:40,20
Now if we go back to Insomnia,

79
00:03:40,20 --> 00:03:44,10
and send our request, we'll see that it works just fine.

80
00:03:44,10 --> 00:03:47,00
So let's validate that this doesn't actually work

81
00:03:47,00 --> 00:03:49,10
with an invalid password.

82
00:03:49,10 --> 00:03:51,20
So if we change our password to be "test"

83
00:03:51,20 --> 00:03:52,80
and send a request,

84
00:03:52,80 --> 00:03:55,40
we'll see that we get a 401 error.

85
00:03:55,40 --> 00:03:58,60
After all, we're no longer authorized to access it,

86
00:03:58,60 --> 00:04:01,10
because we passed in the wrong password.

87
00:04:01,10 --> 00:04:05,90
And if we change it back to secret, we're back in.

88
00:04:05,90 --> 00:04:09,20
So, basic auth is pretty easy to set up and get running.

89
00:04:09,20 --> 00:04:11,90
Notice it permits you to tie user account

90
00:04:11,90 --> 00:04:14,50
to being able to access an API.

91
00:04:14,50 --> 00:04:16,40
But again, it requires two things.

92
00:04:16,40 --> 00:04:18,60
First, the username and password

93
00:04:18,60 --> 00:04:20,50
has to be used by the service,

94
00:04:20,50 --> 00:04:24,70
which means that if anyone is able to see the request

95
00:04:24,70 --> 00:04:29,30
when it's not backed with SSL, your ISP, your router, etc.,

96
00:04:29,30 --> 00:04:31,10
all get to see the request,

97
00:04:31,10 --> 00:04:33,60
including your username and password.

98
00:04:33,60 --> 00:04:37,60
Secondarily, if you need to expose this in some way,

99
00:04:37,60 --> 00:04:40,20
say, put it into a JavaScript application,

100
00:04:40,20 --> 00:04:42,00
or hand out for other services

101
00:04:42,00 --> 00:04:44,20
to access the API as yourself,

102
00:04:44,20 --> 00:04:46,50
you're handing out your username and password

103
00:04:46,50 --> 00:04:48,50
to your account.

104
00:04:48,50 --> 00:04:51,40
What we're going to discuss next is called 0auth,

105
00:04:51,40 --> 00:04:54,10
which as of today is the standard for protecting

106
00:04:54,10 --> 00:04:58,00
and serving up authentication information for APIs.

