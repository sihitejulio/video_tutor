1
00:00:00,50 --> 00:00:03,80
- [Instructor] Let's discuss token authentication.

2
00:00:03,80 --> 00:00:06,10
Authentication is the process of checking

3
00:00:06,10 --> 00:00:08,80
that someone is who they say they are.

4
00:00:08,80 --> 00:00:11,10
It identifies a user,

5
00:00:11,10 --> 00:00:14,10
so it's the basic username and password form field

6
00:00:14,10 --> 00:00:16,80
that you see with most websites.

7
00:00:16,80 --> 00:00:20,80
Authorization on the other hand identifies

8
00:00:20,80 --> 00:00:24,80
that a user has access to parts of the application.

9
00:00:24,80 --> 00:00:30,10
It authorizes access as opposed to identifying a user.

10
00:00:30,10 --> 00:00:31,10
We aren't going to build

11
00:00:31,10 --> 00:00:33,70
the authorization mechanism for your API,

12
00:00:33,70 --> 00:00:37,10
but we will however build an authentication layer.

13
00:00:37,10 --> 00:00:40,30
We'll explore a few different variations of this.

14
00:00:40,30 --> 00:00:43,50
The first one is token authentication.

15
00:00:43,50 --> 00:00:46,70
Token authentication is where we pass a token

16
00:00:46,70 --> 00:00:50,50
either as a header or in the URL parameters.

17
00:00:50,50 --> 00:00:53,00
The token is assigned to us via the site

18
00:00:53,00 --> 00:00:56,10
and it enables us to access the API.

19
00:00:56,10 --> 00:00:59,40
Note that where the token comes from is a secondary problem

20
00:00:59,40 --> 00:01:01,80
that you'll need to solve depending on how it matters

21
00:01:01,80 --> 00:01:04,00
for your particular site.

22
00:01:04,00 --> 00:01:06,00
The advantage with token authentication

23
00:01:06,00 --> 00:01:08,60
is that it's fast and easy to set up,

24
00:01:08,60 --> 00:01:12,10
but the disadvantage is tokens can be shared,

25
00:01:12,10 --> 00:01:13,70
typically they aren't encrypted

26
00:01:13,70 --> 00:01:16,60
which means that anyone who can read the API request

27
00:01:16,60 --> 00:01:19,90
can duplicate the token and send their own request.

28
00:01:19,90 --> 00:01:22,20
However, it's a good starting point for us

29
00:01:22,20 --> 00:01:25,70
to build out an authentication layer in our API.

30
00:01:25,70 --> 00:01:28,60
To begin, we'll actually use a middleware layer

31
00:01:28,60 --> 00:01:36,10
so we'll run the command php artisan make:middleware

32
00:01:36,10 --> 00:01:38,80
and we'll name it TokenAuth.

33
00:01:38,80 --> 00:01:41,50
Now let's open up our TokenAuth middleware class

34
00:01:41,50 --> 00:01:43,40
by opening up our text editor

35
00:01:43,40 --> 00:01:49,50
and opening up app, http, middleware, TokenAuth.php.

36
00:01:49,50 --> 00:01:52,30
So what we need to do is we need to check

37
00:01:52,30 --> 00:01:54,90
our request for a certain header

38
00:01:54,90 --> 00:01:57,00
and if that header is not available

39
00:01:57,00 --> 00:01:59,30
or set to the correct value,

40
00:01:59,30 --> 00:02:01,90
we want to stop our request.

41
00:02:01,90 --> 00:02:04,50
So what we'll add is before line 18

42
00:02:04,50 --> 00:02:12,00
we'll add in token is equal to request arrow header

43
00:02:12,00 --> 00:02:14,70
so we're going to get a header from our request.

44
00:02:14,70 --> 00:02:22,10
In this case, we'll ask for the header of X-API-TOKEN

45
00:02:22,10 --> 00:02:26,00
so this is our token grabbed from the X-API-TOKEN header.

46
00:02:26,00 --> 00:02:29,00
Now we can compare it to a predetermined value

47
00:02:29,00 --> 00:02:30,20
and if it doesn't match,

48
00:02:30,20 --> 00:02:33,60
we want to abort and throw an exception.

49
00:02:33,60 --> 00:02:36,00
So we'll add on line 19

50
00:02:36,00 --> 00:02:43,00
if test-value is not equal to token.

51
00:02:43,00 --> 00:02:44,30
Inside of our if,

52
00:02:44,30 --> 00:02:49,50
we want to execute an abort 401

53
00:02:49,50 --> 00:02:54,60
and then pass the string Auth Token not found.

54
00:02:54,60 --> 00:02:57,60
So note here we are testing against a custom string,

55
00:02:57,60 --> 00:02:58,90
but there's no reason

56
00:02:58,90 --> 00:03:01,70
we couldn't either pass in a custom value,

57
00:03:01,70 --> 00:03:04,50
as in when we set up our middleware,

58
00:03:04,50 --> 00:03:06,40
or we could call out to the database

59
00:03:06,40 --> 00:03:09,60
and see if that token matches a value in the database

60
00:03:09,60 --> 00:03:13,40
or any of a hundred other ways to validate the token.

61
00:03:13,40 --> 00:03:15,90
The main point here is we check for a token

62
00:03:15,90 --> 00:03:17,40
and if it doesn't exist,

63
00:03:17,40 --> 00:03:19,70
we cause the request to fail.

64
00:03:19,70 --> 00:03:23,10
Now we need to add this middleware to our application.

65
00:03:23,10 --> 00:03:25,10
So we open up the kernel file

66
00:03:25,10 --> 00:03:30,50
located at app, http, kernel.php.

67
00:03:30,50 --> 00:03:33,30
We'll scroll down and into our middleware section,

68
00:03:33,30 --> 00:03:36,00
we'll add a new line for our array

69
00:03:36,00 --> 00:03:39,90
and we'll set this to the class instance of TokenAuth

70
00:03:39,90 --> 00:03:52,10
which will be \App\Http\Middleware\TokenAuth::class.

71
00:03:52,10 --> 00:03:55,20
Now let's save this and we'll serve up our application

72
00:03:55,20 --> 00:04:01,30
by opening up our terminal and running php artisan serve.

73
00:04:01,30 --> 00:04:03,30
Now we can open up Insomnia

74
00:04:03,30 --> 00:04:05,20
and we'll send a request

75
00:04:05,20 --> 00:04:07,20
and we'll note our request fails here

76
00:04:07,20 --> 00:04:09,80
because our request was unauthorized.

77
00:04:09,80 --> 00:04:11,00
Now what we need to do

78
00:04:11,00 --> 00:04:13,90
is we need to add in a custom header for our request

79
00:04:13,90 --> 00:04:16,20
so we'll select the header option,

80
00:04:16,20 --> 00:04:18,30
add a new header,

81
00:04:18,30 --> 00:04:24,00
in this case we'll call our header X-API-TOKEN,

82
00:04:24,00 --> 00:04:29,00
and then we'll set the value to test-value.

83
00:04:29,00 --> 00:04:33,20
We'll send our request and now our request once again works.

84
00:04:33,20 --> 00:04:35,50
So this is how we can build in some basic

85
00:04:35,50 --> 00:04:39,60
and easy token authentication into our API.

86
00:04:39,60 --> 00:04:43,90
Again note, our header is not encrypted in any way

87
00:04:43,90 --> 00:04:47,20
and therefore anyone who is able to read this request

88
00:04:47,20 --> 00:04:50,20
is able to duplicate the request and resend it

89
00:04:50,20 --> 00:04:52,60
and pretend that they are the user,

90
00:04:52,60 --> 00:04:55,20
that they're authenticating as that user

91
00:04:55,20 --> 00:04:59,00
who has this API token of test-value.

