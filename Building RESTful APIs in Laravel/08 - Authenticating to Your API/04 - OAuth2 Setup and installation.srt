1
00:00:00,60 --> 00:00:02,20
- [Presenter] Laravel, as you might guess,

2
00:00:02,20 --> 00:00:04,80
provides for us a variety of tools to get started

3
00:00:04,80 --> 00:00:07,30
rather Quickly with OAuth 2.0.

4
00:00:07,30 --> 00:00:11,00
To begin, we need to install the auth-scaffold Laravel

5
00:00:11,00 --> 00:00:15,20
provides for us so we can login to the Laravel application.

6
00:00:15,20 --> 00:00:16,10
Run the command,

7
00:00:16,10 --> 00:00:21,30
php artisan make:auth

8
00:00:21,30 --> 00:00:24,60
in your terminal application so it'll generate

9
00:00:24,60 --> 00:00:27,40
the authentication scaffolding.

10
00:00:27,40 --> 00:00:29,30
Now we need to add a composer package

11
00:00:29,30 --> 00:00:32,50
for our site for supporting OAuth 2.0.

12
00:00:32,50 --> 00:00:35,70
This package is going to be, composer

13
00:00:35,70 --> 00:00:42,20
require laravel/passport

14
00:00:42,20 --> 00:00:44,90
After this installs, we need to run some migrations

15
00:00:44,90 --> 00:00:47,40
to ensure we add some new database fields.

16
00:00:47,40 --> 00:00:52,50
So we'll run, php artisan migrate.

17
00:00:52,50 --> 00:00:54,30
Now passport is also going to install

18
00:00:54,30 --> 00:00:57,50
a custom artisan command to install itself.

19
00:00:57,50 --> 00:00:58,40
So we can run,

20
00:00:58,40 --> 00:01:04,00
php artisan passport:install.

21
00:01:04,00 --> 00:01:07,10
You'll note it generates two clients for us,

22
00:01:07,10 --> 00:01:10,80
each of which include an ID and a secret.

23
00:01:10,80 --> 00:01:12,70
In this case, we don't necessarily care

24
00:01:12,70 --> 00:01:14,70
what kind of clients they created.

25
00:01:14,70 --> 00:01:16,80
Just understand that they created a client

26
00:01:16,80 --> 00:01:20,40
with a corresponding ID and a corresponding secret.

27
00:01:20,40 --> 00:01:23,00
Now let's open up the user model class.

28
00:01:23,00 --> 00:01:29,10
We'll open up our text editor, and open up app, user.php.

29
00:01:29,10 --> 00:01:30,80
Here we can include the trait

30
00:01:30,80 --> 00:01:34,30
for including our API tokens into the model.

31
00:01:34,30 --> 00:01:39,10
On line 5, we'll add, use Laravel\

32
00:01:39,10 --> 00:01:44,30
Passport\HasApiTokens;

33
00:01:44,30 --> 00:01:46,10
And then on line 11,

34
00:01:46,10 --> 00:01:49,90
we can add HasApiTokens

35
00:01:49,90 --> 00:01:52,80
to our included traits.

36
00:01:52,80 --> 00:01:55,70
After this, we'll need to add the passport routes provider

37
00:01:55,70 --> 00:01:58,40
to our list of service providers.

38
00:01:58,40 --> 00:02:01,30
So open up the route service provider located in

39
00:02:01,30 --> 00:02:05,30
app, Providers, RouteServiceProvider.

40
00:02:05,30 --> 00:02:09,00
As usual, we need to import a class into our namespace,

41
00:02:09,00 --> 00:02:11,80
so we'll add, on line 5,

42
00:02:11,80 --> 00:02:16,30
use Laravel\

43
00:02:16,30 --> 00:02:20,10
Passport\Passport.

44
00:02:20,10 --> 00:02:23,40
And inside of line 42,

45
00:02:23,40 --> 00:02:25,70
inside of our map() function,

46
00:02:25,70 --> 00:02:31,70
we'll add Passport::routes.

47
00:02:31,70 --> 00:02:33,70
Okay, we're getting closer.

48
00:02:33,70 --> 00:02:35,80
We've added Passport, which is a wrapper

49
00:02:35,80 --> 00:02:37,90
for OAuth 2.0 implementation.

50
00:02:37,90 --> 00:02:40,30
We added it so that the user model knows

51
00:02:40,30 --> 00:02:42,50
how to use the tokens generated.

52
00:02:42,50 --> 00:02:46,50
And we added the routes for the service to our application.

53
00:02:46,50 --> 00:02:49,30
Now we need to add to our auth configuration,

54
00:02:49,30 --> 00:02:51,70
that we're using Passport.

55
00:02:51,70 --> 00:02:57,30
Open up the auth config file, located in config,

56
00:02:57,30 --> 00:03:01,60
and we'll scroll down to line 45

57
00:03:01,60 --> 00:03:05,50
where we have the api authentication guards,

58
00:03:05,50 --> 00:03:08,20
and we'll change the driver for it

59
00:03:08,20 --> 00:03:12,70
to rather than being token, to being passport.

60
00:03:12,70 --> 00:03:14,50
This is pretty much the entire back end

61
00:03:14,50 --> 00:03:18,80
that we need to build out for OAuth in Laravel.

62
00:03:18,80 --> 00:03:22,20
For the front end, we'll set it up in the next video.

63
00:03:22,20 --> 00:03:25,30
The Laravel documentation also includes information

64
00:03:25,30 --> 00:03:28,40
on deploying this with a real view-based front end,

65
00:03:28,40 --> 00:03:30,10
although we're not going to cover it.

66
00:03:30,10 --> 00:03:31,90
But feel free to take a look at it

67
00:03:31,90 --> 00:03:48,00
if you feel that you need a real front end.

