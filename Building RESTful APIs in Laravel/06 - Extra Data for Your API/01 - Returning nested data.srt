1
00:00:00,50 --> 00:00:01,60
- [Instructor] Nesting data

2
00:00:01,60 --> 00:00:03,60
is where we nest the related data

3
00:00:03,60 --> 00:00:06,90
for a class inside of the objects response.

4
00:00:06,90 --> 00:00:08,60
For our poll this would mean

5
00:00:08,60 --> 00:00:11,40
we would include the key of questions

6
00:00:11,40 --> 00:00:14,40
that loads in the related questions for our poll.

7
00:00:14,40 --> 00:00:16,60
This is what we're going to build out now.

8
00:00:16,60 --> 00:00:18,90
Let's start by opening up the polls controller

9
00:00:18,90 --> 00:00:25,30
in App, Http, Controllers, PollsController.php.

10
00:00:25,30 --> 00:00:27,80
We'll modify our find our fail call

11
00:00:27,80 --> 00:00:30,70
to also eagerly load the questions

12
00:00:30,70 --> 00:00:34,00
by modifying on line 23

13
00:00:34,00 --> 00:00:36,90
to read poll finder fail

14
00:00:36,90 --> 00:00:40,70
to instead read as poll with,

15
00:00:40,70 --> 00:00:42,60
with being a function call.

16
00:00:42,60 --> 00:00:44,50
We'll pass in questions

17
00:00:44,50 --> 00:00:46,90
to say we need to load in the questions,

18
00:00:46,90 --> 00:00:49,80
arrow, findOrFail.

19
00:00:49,80 --> 00:00:53,20
Now let's also modify this to clean it up a little bit.

20
00:00:53,20 --> 00:00:56,70
We'll take this poll with questions findOrFail,

21
00:00:56,70 --> 00:00:59,40
we'll move that onto its own line on 23

22
00:00:59,40 --> 00:01:01,80
where poll will be equal to poll

23
00:01:01,80 --> 00:01:04,20
with questions findOrFail,

24
00:01:04,20 --> 00:01:05,90
and then we can pass in poll

25
00:01:05,90 --> 00:01:07,70
to our poll resource.

26
00:01:07,70 --> 00:01:10,10
We can save this, and now we need to modify

27
00:01:10,10 --> 00:01:11,70
our poll resource class

28
00:01:11,70 --> 00:01:13,70
to display the questions.

29
00:01:13,70 --> 00:01:15,70
So we'll open up the poll resource class

30
00:01:15,70 --> 00:01:20,80
at App Http Resources Poll.php.

31
00:01:20,80 --> 00:01:21,90
Here all we need to do

32
00:01:21,90 --> 00:01:23,60
is just call the parent function.

33
00:01:23,60 --> 00:01:26,30
So we're going to replace this return title

34
00:01:26,30 --> 00:01:32,50
with just return parent::toArray

35
00:01:32,50 --> 00:01:34,40
and pass in the request.

36
00:01:34,40 --> 00:01:36,40
Save this and let's test it out

37
00:01:36,40 --> 00:01:38,00
in Insomnia.

38
00:01:38,00 --> 00:01:40,00
We'll go to get a poll

39
00:01:40,00 --> 00:01:42,40
and this time pull off that questions

40
00:01:42,40 --> 00:01:44,50
that we added in the prior video

41
00:01:44,50 --> 00:01:48,40
and just ask for the polls at ID four.

42
00:01:48,40 --> 00:01:50,20
And there we see our poll

43
00:01:50,20 --> 00:01:52,00
with the full title,

44
00:01:52,00 --> 00:01:53,90
and then notice we have a new key

45
00:01:53,90 --> 00:01:56,20
of questions, and included in there

46
00:01:56,20 --> 00:01:58,70
is all the questions for that poll.

47
00:01:58,70 --> 00:02:03,10
If we swap this to a different ID, say ID of seven,

48
00:02:03,10 --> 00:02:05,00
we'll see we get the same thing again.

49
00:02:05,00 --> 00:02:07,30
We get our poll with ID of seven,

50
00:02:07,30 --> 00:02:09,40
its title and other attributes,

51
00:02:09,40 --> 00:02:12,30
and we get the questions for that poll.

52
00:02:12,30 --> 00:02:15,50
Notice how easy it was to build in this functionality.

53
00:02:15,50 --> 00:02:18,00
Nesting data is going to be a very quick way

54
00:02:18,00 --> 00:02:21,10
to present related models in your API.

55
00:02:21,10 --> 00:02:24,20
However, it isn't always the right solution

56
00:02:24,20 --> 00:02:26,40
for your particular need.

57
00:02:26,40 --> 00:02:28,80
There are a different way of loading this in

58
00:02:28,80 --> 00:02:33,00
called side loading that we'll cover next.

