1
00:00:00,50 --> 00:00:02,70
- [Instructor] Accepting file uploads is a little bit

2
00:00:02,70 --> 00:00:06,40
trickier, but relatively quick given our tool chain.

3
00:00:06,40 --> 00:00:12,80
To begin, we want a new route, so open up routes, API.php.

4
00:00:12,80 --> 00:00:15,10
And we'll add at the end of our file,

5
00:00:15,10 --> 00:00:20,40
Route::post, files, create,

6
00:00:20,40 --> 00:00:23,70
and this will access the FilesController,

7
00:00:23,70 --> 00:00:26,40
and the method it will call is create.

8
00:00:26,40 --> 00:00:31,30
Now, let's open up our FilesController at app, Http,

9
00:00:31,30 --> 00:00:34,90
Controllers, FilesController.php.

10
00:00:34,90 --> 00:00:40,00
Want a new function, public function create,

11
00:00:40,00 --> 00:00:41,80
this will take a request object,

12
00:00:41,80 --> 00:00:45,80
so Request, Request, and in here,

13
00:00:45,80 --> 00:00:49,80
we can save our file with path is going to be equal

14
00:00:49,80 --> 00:00:54,50
to request, arrow, file, photo,

15
00:00:54,50 --> 00:00:56,90
and then arrow, store,

16
00:00:56,90 --> 00:00:59,40
and then the string testing.

17
00:00:59,40 --> 00:01:00,90
Now, what will this do?

18
00:01:00,90 --> 00:01:03,60
This will take a file past via a post

19
00:01:03,60 --> 00:01:05,90
multi-part body request,

20
00:01:05,90 --> 00:01:08,50
with the file paths in the photo key,

21
00:01:08,50 --> 00:01:11,40
and move the file to our testing bucket,

22
00:01:11,40 --> 00:01:15,50
and return the path to the file with the path variable.

23
00:01:15,50 --> 00:01:17,60
At this point, we can do any number of things

24
00:01:17,60 --> 00:01:20,90
with the file and modify to our heart's content.

25
00:01:20,90 --> 00:01:23,80
In this case, we'll just display the path of the file

26
00:01:23,80 --> 00:01:25,70
in a json response.

27
00:01:25,70 --> 00:01:32,80
We'll make like 17 read as return response, arrow, json.

28
00:01:32,80 --> 00:01:36,70
We'll paths in the ray and we'll add a key value pair

29
00:01:36,70 --> 00:01:41,00
of path, which will be equal to our path variable,

30
00:01:41,00 --> 00:01:45,20
and then our Http response code will be 200,

31
00:01:45,20 --> 00:01:46,70
and we'll save this.

32
00:01:46,70 --> 00:01:49,10
Now, remember to start up your layer bell application

33
00:01:49,10 --> 00:01:51,30
with php artisan serve.

34
00:01:51,30 --> 00:01:53,10
We'll open up Insomnia.

35
00:01:53,10 --> 00:01:56,90
We'll Duplicate our GET FILE request.

36
00:01:56,90 --> 00:02:00,50
We'll rename it to CREATE FILE,

37
00:02:00,50 --> 00:02:04,10
change our request to be a post request,

38
00:02:04,10 --> 00:02:09,30
change the URL to be create, api/files/create.

39
00:02:09,30 --> 00:02:13,40
For the body, we'll select multipart form,

40
00:02:13,40 --> 00:02:14,70
we'll add a new field.

41
00:02:14,70 --> 00:02:17,40
In this case, the field will be photo,

42
00:02:17,40 --> 00:02:20,00
and then we'll select file,

43
00:02:20,00 --> 00:02:22,70
and now we'll just choose a random file to upload.

44
00:02:22,70 --> 00:02:26,70
In this case, I'll upload our readme.

45
00:02:26,70 --> 00:02:28,90
Now, I'll send the request,

46
00:02:28,90 --> 00:02:32,60
and I'll see I get my file testing,

47
00:02:32,60 --> 00:02:36,20
and then stored at a random path .html.

48
00:02:36,20 --> 00:02:38,00
Note you can add more work to this.

49
00:02:38,00 --> 00:02:40,20
You can, for instance, validate the file,

50
00:02:40,20 --> 00:02:43,40
or you can serve the file back to the user of the API,

51
00:02:43,40 --> 00:02:45,70
but the basics at this point should be pretty clear.

52
00:02:45,70 --> 00:02:48,50
You can both return and store files with an API

53
00:02:48,50 --> 00:02:50,00
at this point.

