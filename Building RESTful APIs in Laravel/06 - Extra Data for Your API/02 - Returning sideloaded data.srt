1
00:00:00,60 --> 00:00:01,90
- [Instructor] Side loaded data

2
00:00:01,90 --> 00:00:05,40
is when we load two different models of data in our API.

3
00:00:05,40 --> 00:00:08,80
We want to side load that other related data

4
00:00:08,80 --> 00:00:11,60
as a second record in our API keys,

5
00:00:11,60 --> 00:00:13,90
where it's the last time we nested the questions

6
00:00:13,90 --> 00:00:15,40
inside of the poll.

7
00:00:15,40 --> 00:00:17,00
What if we wanted to return questions

8
00:00:17,00 --> 00:00:20,50
as a second top level key in our API response?

9
00:00:20,50 --> 00:00:22,30
This is going to take a little bit longer

10
00:00:22,30 --> 00:00:23,70
but not by a lot.

11
00:00:23,70 --> 00:00:25,90
First, we'll open up the poll model

12
00:00:25,90 --> 00:00:29,50
from app Poll.phP in our editor.

13
00:00:29,50 --> 00:00:31,70
First we'll want to hide the questions attribute

14
00:00:31,70 --> 00:00:34,20
from our model's json response.

15
00:00:34,20 --> 00:00:36,20
To do this after our fillable attribute,

16
00:00:36,20 --> 00:00:38,20
we'll add a new attribute

17
00:00:38,20 --> 00:00:42,20
that will be protected, and will be the variable hidden,

18
00:00:42,20 --> 00:00:44,10
and will pass in an array,

19
00:00:44,10 --> 00:00:47,10
and inside of this array, will pass in the key

20
00:00:47,10 --> 00:00:48,30
that we want to hide.

21
00:00:48,30 --> 00:00:50,90
In this case, we'll hide questions.

22
00:00:50,90 --> 00:00:53,30
Now we'll want to open up our poll's controller,

23
00:00:53,30 --> 00:00:59,50
at app, Http, Controllers, Pollscontroller.php.

24
00:00:59,50 --> 00:01:02,60
Now, let's update our show method.

25
00:01:02,60 --> 00:01:04,40
First what we're going to do, is we're going to

26
00:01:04,40 --> 00:01:10,60
replace line 19, our poll find, with just line 23.

27
00:01:10,60 --> 00:01:11,80
We're actually just going to get rid of

28
00:01:11,80 --> 00:01:15,60
everything from line 20 through 23.

29
00:01:15,60 --> 00:01:19,00
So we have a poll, we pass it to the poll resource,

30
00:01:19,00 --> 00:01:20,90
we save it as a response, and then we

31
00:01:20,90 --> 00:01:22,90
set that response back out.

32
00:01:22,90 --> 00:01:24,20
Now what we're going to do is we're going to

33
00:01:24,20 --> 00:01:27,00
create a new response and we're going to move

34
00:01:27,00 --> 00:01:30,50
poll and questions into their own top level keys.

35
00:01:30,50 --> 00:01:36,40
So first off, on line 20, create a response, poll,

36
00:01:36,40 --> 00:01:39,20
that's going to be equal to our poll instance.

37
00:01:39,20 --> 00:01:44,20
Now we'll add on line 21, response, questions,

38
00:01:44,20 --> 00:01:48,70
is going to be equal to poll, arrow, questions.

39
00:01:48,70 --> 00:01:51,20
So we'll set this new response object

40
00:01:51,20 --> 00:01:54,40
to have a key of poll and a key of questions.

41
00:01:54,40 --> 00:01:56,30
The poll will contain our poll,

42
00:01:56,30 --> 00:01:59,50
and questions will contain the questions for that poll.

43
00:01:59,50 --> 00:02:03,30
We'll edit line 22, new poll resource,

44
00:02:03,30 --> 00:02:07,90
to be new poll resource with the response object instead.

45
00:02:07,90 --> 00:02:10,30
Save that as our modified response,

46
00:02:10,30 --> 00:02:13,20
and then return it with our response json.

47
00:02:13,20 --> 00:02:14,90
Now we can save this.

48
00:02:14,90 --> 00:02:17,40
We'll repeat like we did in the last video,

49
00:02:17,40 --> 00:02:19,60
start up your Laravel application

50
00:02:19,60 --> 00:02:24,00
and go to Insomnia, and we'll view our get poll response.

51
00:02:24,00 --> 00:02:26,20
Now, this rose a 500 error.

52
00:02:26,20 --> 00:02:28,10
The reason is is because of the way

53
00:02:28,10 --> 00:02:30,10
that we've modified our response.

54
00:02:30,10 --> 00:02:33,70
We can't actually send it to our poll resource class,

55
00:02:33,70 --> 00:02:35,60
so we'll go back to our editor,

56
00:02:35,60 --> 00:02:38,20
and we'll delete line 22.

57
00:02:38,20 --> 00:02:40,60
I wanted to show this off so that you understand

58
00:02:40,60 --> 00:02:43,80
that our resource classes are transformers,

59
00:02:43,80 --> 00:02:46,70
are only designed to work with instances

60
00:02:46,70 --> 00:02:48,70
of our Laravel models.

61
00:02:48,70 --> 00:02:51,10
If we pass in an instance of an array,

62
00:02:51,10 --> 00:02:52,90
like actually we're doing here

63
00:02:52,90 --> 00:02:54,70
where we're passing in that response poll

64
00:02:54,70 --> 00:02:58,80
and that response questions, it can't actually work.

65
00:02:58,80 --> 00:03:02,50
So line 22 went away, and we just returned the response,

66
00:03:02,50 --> 00:03:05,50
so we update, and now we just return the response

67
00:03:05,50 --> 00:03:08,30
as a json response object.

68
00:03:08,30 --> 00:03:12,30
Save this, go back to Insomnia, and re-run the request.

69
00:03:12,30 --> 00:03:14,30
And there we see, we have our poll,

70
00:03:14,30 --> 00:03:16,20
and we have our questions.

71
00:03:16,20 --> 00:03:18,60
Now why might you do this way

72
00:03:18,60 --> 00:03:21,10
where we have the data side loaded

73
00:03:21,10 --> 00:03:23,30
as opposed to nesting the data?

74
00:03:23,30 --> 00:03:25,50
There are some good reasons for each.

75
00:03:25,50 --> 00:03:28,50
In nesting our data, we demonstrate the relationship

76
00:03:28,50 --> 00:03:31,30
between the poll and the related questions,

77
00:03:31,30 --> 00:03:33,80
however side loading, notice, give us the

78
00:03:33,80 --> 00:03:37,10
ability to access questions directly.

79
00:03:37,10 --> 00:03:40,10
This could be important in the design of your API.

80
00:03:40,10 --> 00:03:41,60
Side loading can make sense,

81
00:03:41,60 --> 00:03:43,80
especially when dealing with collections.

82
00:03:43,80 --> 00:03:45,60
For instance, if we're getting the collection

83
00:03:45,60 --> 00:03:47,60
of questions for a single poll,

84
00:03:47,60 --> 00:03:49,70
we're acting on the questions route.

85
00:03:49,70 --> 00:03:51,20
Side loading can make sense

86
00:03:51,20 --> 00:03:54,40
especially when dealing with collections of data,

87
00:03:54,40 --> 00:03:57,00
for instance, if we're getting the collection

88
00:03:57,00 --> 00:03:59,50
of questions for a single poll,

89
00:03:59,50 --> 00:04:01,80
we're acting on the questions route.

90
00:04:01,80 --> 00:04:03,90
But we might want to side load the poll

91
00:04:03,90 --> 00:04:05,80
associated with all of these questions

92
00:04:05,80 --> 00:04:09,00
as opposed to nesting that same data,

93
00:04:09,00 --> 00:04:12,70
that same poll, with every single question.

94
00:04:12,70 --> 00:04:15,10
This is when side loading can make more sense

95
00:04:15,10 --> 00:04:16,00
over nesting.

