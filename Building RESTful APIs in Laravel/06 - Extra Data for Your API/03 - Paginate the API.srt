1
00:00:00,90 --> 00:00:02,90
- [Instructor] Paginating is a common problem

2
00:00:02,90 --> 00:00:05,50
in web applications, and it's pretty easy

3
00:00:05,50 --> 00:00:08,00
to solve with a modern web framework.

4
00:00:08,00 --> 00:00:09,60
But what about for an API?

5
00:00:09,60 --> 00:00:11,80
How do we paginate an API?

6
00:00:11,80 --> 00:00:14,20
And doesn't it seem like it'd be really hard?

7
00:00:14,20 --> 00:00:16,70
Well, luckily Laravel provides a few tricks

8
00:00:16,70 --> 00:00:18,40
for us to get this working.

9
00:00:18,40 --> 00:00:20,60
Let's open up our polls controller class

10
00:00:20,60 --> 00:00:26,20
in app Http Controllers PollsController.php.

11
00:00:26,20 --> 00:00:30,40
Let's change the get call on line 14 to paginate.

12
00:00:30,40 --> 00:00:33,40
So now our line reads return response

13
00:00:33,40 --> 00:00:36,90
json Poll paginate and a 200.

14
00:00:36,90 --> 00:00:40,50
At this point now we can save this file,

15
00:00:40,50 --> 00:00:43,40
we can start up our PHP artisan server,

16
00:00:43,40 --> 00:00:48,80
open up Insomnia, and we'll call our get polls method.

17
00:00:48,80 --> 00:00:50,50
Notice our polls are displayed

18
00:00:50,50 --> 00:00:54,10
along with the meta data about the pagination response.

19
00:00:54,10 --> 00:00:56,90
We have a current page, we have a data,

20
00:00:56,90 --> 00:00:59,10
which includes all of our polls,

21
00:00:59,10 --> 00:01:02,20
and at the bottom we have the first_page_url

22
00:01:02,20 --> 00:01:05,50
from last_page, last_page_url, next page,

23
00:01:05,50 --> 00:01:10,20
the path, the previous page, the to, the total.

24
00:01:10,20 --> 00:01:13,30
This shows us the meta data that we need,

25
00:01:13,30 --> 00:01:16,20
i.e., the data about the data itself

26
00:01:16,20 --> 00:01:18,30
that we need to be able to paginate through

27
00:01:18,30 --> 00:01:21,80
each of our responses and paginate through our polls.

28
00:01:21,80 --> 00:01:23,20
Let's go back to our controller

29
00:01:23,20 --> 00:01:25,80
and see how we can actually paginate through.

30
00:01:25,80 --> 00:01:27,30
We'll update Poll paginate

31
00:01:27,30 --> 00:01:29,90
to only return one response at a time

32
00:01:29,90 --> 00:01:34,00
by passing into our paginate method the value one.

33
00:01:34,00 --> 00:01:36,80
This'll only show one record at a time.

34
00:01:36,80 --> 00:01:38,70
Save this and go back to Insomnia,

35
00:01:38,70 --> 00:01:41,50
and resend the request, and we'll see here

36
00:01:41,50 --> 00:01:43,50
we only get back one poll.

37
00:01:43,50 --> 00:01:45,80
Notice we get back information to tell us

38
00:01:45,80 --> 00:01:47,80
how to get to the next page.

39
00:01:47,80 --> 00:01:51,20
We can pass it in with adding in

40
00:01:51,20 --> 00:01:54,80
?page=2.

41
00:01:54,80 --> 00:01:57,90
We send this and we get back our second poll,

42
00:01:57,90 --> 00:01:59,40
and notice it tells us how to get

43
00:01:59,40 --> 00:02:02,60
to the next page or even the last page

44
00:02:02,60 --> 00:02:05,20
by passing in page is equal to 12.

45
00:02:05,20 --> 00:02:07,60
So we get back to our last page.

46
00:02:07,60 --> 00:02:08,60
Notice it even shows us

47
00:02:08,60 --> 00:02:10,90
how to get to the previous page URL

48
00:02:10,90 --> 00:02:13,10
with page is equal to 11.

49
00:02:13,10 --> 00:02:15,60
So we can keep paginating through our API

50
00:02:15,60 --> 00:02:17,70
as often as we need to.

51
00:02:17,70 --> 00:02:21,00
Notice it should be easy for someone to access your API

52
00:02:21,00 --> 00:02:24,00
and use that next_page_url meta data field

53
00:02:24,00 --> 00:02:25,70
to page through the API

54
00:02:25,70 --> 00:02:28,60
and pull up as many records as they need,

55
00:02:28,60 --> 00:02:30,40
or use any of the other meta data

56
00:02:30,40 --> 00:02:32,90
provided in the pagination response

57
00:02:32,90 --> 00:02:34,00
to be able to figure out

58
00:02:34,00 --> 00:02:37,00
exactly how to get the data that they need.

