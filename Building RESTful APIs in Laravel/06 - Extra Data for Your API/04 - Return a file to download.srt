1
00:00:00,60 --> 00:00:02,50
- [Instructor] In building an API, you may need

2
00:00:02,50 --> 00:00:05,80
to provide an endpoint to be able to download a file.

3
00:00:05,80 --> 00:00:08,20
This might be as simple as a place to get an image

4
00:00:08,20 --> 00:00:09,70
or something more complicated,

5
00:00:09,70 --> 00:00:13,60
like providing a custom one-time only generated PDF.

6
00:00:13,60 --> 00:00:15,20
We're going to ignore the problem

7
00:00:15,20 --> 00:00:17,10
of generating a file to download.

8
00:00:17,10 --> 00:00:19,50
That's not really in scope for this course.

9
00:00:19,50 --> 00:00:22,30
Instead, we'll focus on providing you an endpoint

10
00:00:22,30 --> 00:00:24,40
to be able to download a file.

11
00:00:24,40 --> 00:00:25,90
How is this going to work?

12
00:00:25,90 --> 00:00:27,80
First, like we've done in the past,

13
00:00:27,80 --> 00:00:30,20
we need an endpoint that we can reach.

14
00:00:30,20 --> 00:00:32,50
We'll start off by creating a custom controller

15
00:00:32,50 --> 00:00:34,50
and a corresponding route for this.

16
00:00:34,50 --> 00:00:36,50
Let's make a new controller

17
00:00:36,50 --> 00:00:38,90
by opening up our terminal application

18
00:00:38,90 --> 00:00:45,30
and running the command php artisan make:controller

19
00:00:45,30 --> 00:00:46,90
and we'll create a new controller

20
00:00:46,90 --> 00:00:49,30
called the FilesController.

21
00:00:49,30 --> 00:00:52,20
At this point, now open up your text editor

22
00:00:52,20 --> 00:00:57,90
and open up the corresponding routes at routes, api.php.

23
00:00:57,90 --> 00:01:02,50
We'll add a new route, Route:: get

24
00:01:02,50 --> 00:01:08,70
and the path that we'll be accessing is files/get

25
00:01:08,70 --> 00:01:13,30
and it will access the FilesController, and our show action.

26
00:01:13,30 --> 00:01:18,50
Now, we can open up our FilesController at app, Http,

27
00:01:18,50 --> 00:01:22,50
Controllers, FilesController.php.

28
00:01:22,50 --> 00:01:24,40
On line nine, we'll replace this

29
00:01:24,40 --> 00:01:28,50
and add in a new public function show.

30
00:01:28,50 --> 00:01:32,00
Now, Laravel provides for us a simple and easy one-liner

31
00:01:32,00 --> 00:01:34,80
to return a file in the HTTP response

32
00:01:34,80 --> 00:01:36,90
and trigger a download.

33
00:01:36,90 --> 00:01:44,40
This line reads as return response()->download.

34
00:01:44,40 --> 00:01:47,10
Download takes two options.

35
00:01:47,10 --> 00:01:49,80
First is a pathToFile.

36
00:01:49,80 --> 00:01:51,50
The second is the name of the file

37
00:01:51,50 --> 00:01:54,50
that we want to have it download as.

38
00:01:54,50 --> 00:01:56,40
There's an optional third parameter

39
00:01:56,40 --> 00:02:00,50
which is any other HTTP headers to include in the request.

40
00:02:00,50 --> 00:02:02,80
We don't need it for this case.

41
00:02:02,80 --> 00:02:04,60
Now we need a file to send.

42
00:02:04,60 --> 00:02:09,00
If you open up the Exercise Files for this lesson,

43
00:02:09,00 --> 00:02:10,80
you'll notice there's a PDF.

44
00:02:10,80 --> 00:02:13,70
This PDF is an image of Grace Hopper.

45
00:02:13,70 --> 00:02:16,20
Grace Hopper was a computer scientist

46
00:02:16,20 --> 00:02:18,70
and a rear admiral in the US Navy.

47
00:02:18,70 --> 00:02:21,40
She was one of the early proponents of compilers

48
00:02:21,40 --> 00:02:23,40
or the ability to write computer code

49
00:02:23,40 --> 00:02:26,90
in a programming language that is closer to English

50
00:02:26,90 --> 00:02:29,20
than it is to machine code.

51
00:02:29,20 --> 00:02:33,20
Her work in this area led directly to the creation of COBOL.

52
00:02:33,20 --> 00:02:36,00
An important person to the field of computer science,

53
00:02:36,00 --> 00:02:39,70
especially if you prefer writing PHP over Assembly

54
00:02:39,70 --> 00:02:41,80
or using punched cards.

55
00:02:41,80 --> 00:02:44,60
This is going to be the file we're going to serve up.

56
00:02:44,60 --> 00:02:47,60
To do this, we need to copy this file,

57
00:02:47,60 --> 00:02:50,80
open up our directory where our application lives,

58
00:02:50,80 --> 00:02:54,00
place it in storage, app,

59
00:02:54,00 --> 00:02:56,60
and paste our file right in here.

60
00:02:56,60 --> 00:02:58,50
Now, let's go back to our FilesController

61
00:02:58,50 --> 00:03:01,40
and pass in the path to the file.

62
00:03:01,40 --> 00:03:06,50
We'll replace pathToFile with storage_path,

63
00:03:06,50 --> 00:03:08,10
which is a function,

64
00:03:08,10 --> 00:03:11,50
and we can pass in the path to the file at this point,

65
00:03:11,50 --> 00:03:16,30
app, forward slash if you're on Mac OS X or Linux,

66
00:03:16,30 --> 00:03:23,10
backslash if you're on Windows, and then GraceHopper.pdf.

67
00:03:23,10 --> 00:03:25,40
And now we can pass in the name for the file.

68
00:03:25,40 --> 00:03:27,30
In this case, we'll pass in the name

69
00:03:27,30 --> 00:03:30,80
that is Grace Hopper's corresponding nickname,

70
00:03:30,80 --> 00:03:33,50
which is Amazing Grace.

71
00:03:33,50 --> 00:03:34,60
Save this,

72
00:03:34,60 --> 00:03:38,50
and now, let's open up Insomnia and test this out.

73
00:03:38,50 --> 00:03:43,40
We'll create a new request, label it GET FILE.

74
00:03:43,40 --> 00:03:45,70
It will be accessing our normal application

75
00:03:45,70 --> 00:04:02,20
at http://127.0.0.1:8000/api/files/get

76
00:04:02,20 --> 00:04:04,40
and we'll send the request now.

77
00:04:04,40 --> 00:04:07,10
Well, it didn't look like it worked and why is that?

78
00:04:07,10 --> 00:04:09,90
Well, we might have forgotten to start up our server,

79
00:04:09,90 --> 00:04:12,60
so let's go back to our terminal application

80
00:04:12,60 --> 00:04:17,40
and start up our server with php artisan serve.

81
00:04:17,40 --> 00:04:19,90
And now, let's try this out one more time,

82
00:04:19,90 --> 00:04:21,90
and there we see our PDF.

83
00:04:21,90 --> 00:04:23,10
Please note that if you have

84
00:04:23,10 --> 00:04:25,00
an issue with this at this point

85
00:04:25,00 --> 00:04:27,40
and you're on Windows, you will need to ensure

86
00:04:27,40 --> 00:04:31,60
that the PHP fileinfo extension, which is already built

87
00:04:31,60 --> 00:04:34,40
and installed for your PHP installation,

88
00:04:34,40 --> 00:04:39,00
needs to be added to your php.ini file.

89
00:04:39,00 --> 00:04:47,80
You'll need to add the line extension = php_fileinfo.dll

90
00:04:47,80 --> 00:04:52,70
to your php.ini file anywhere in the file.

91
00:04:52,70 --> 00:04:55,80
Once you've done this, this should work.

92
00:04:55,80 --> 00:04:57,80
If we click on the Headers,

93
00:04:57,80 --> 00:05:00,50
we'll notice we get the name of the file,

94
00:05:00,50 --> 00:05:04,80
we see the content type of the file, the size of the file.

95
00:05:04,80 --> 00:05:08,40
All of this was built for free for us thanks to Laravel.

96
00:05:08,40 --> 00:05:10,90
Sending files is pretty easy to build in.

97
00:05:10,90 --> 00:05:14,40
Just have a file available at some path on your file system

98
00:05:14,40 --> 00:05:16,80
and then you can serve it straight up.

99
00:05:16,80 --> 00:05:18,00
Feel free to see if this works

100
00:05:18,00 --> 00:04:50,00
just as easily with images or any other type of file.

