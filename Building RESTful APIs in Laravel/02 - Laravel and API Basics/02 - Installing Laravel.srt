1
00:00:00,50 --> 00:00:02,50
- [Narrator] Laravel is simple and easy

2
00:00:02,50 --> 00:00:05,60
for us to get installed onto our machine.

3
00:00:05,60 --> 00:00:08,30
To begin, all we need is to have composer,

4
00:00:08,30 --> 00:00:11,20
and PHP version 7.0 or newer,

5
00:00:11,20 --> 00:00:14,00
installed in our development machine.

6
00:00:14,00 --> 00:00:17,00
You can use the course video: Composer Overview,

7
00:00:17,00 --> 00:00:18,50
from the course Advanced PHP,

8
00:00:18,50 --> 00:00:22,60
to install Composer if you need to at this time.

9
00:00:22,60 --> 00:00:26,60
If you need to update PHP to a more recent version,

10
00:00:26,60 --> 00:00:30,80
look at the course: Installing Apache, MySQL, and PHP

11
00:00:30,80 --> 00:00:33,60
to find detailed instructions for your machine

12
00:00:33,60 --> 00:00:36,90
to install PHP 7.0 or above.

13
00:00:36,90 --> 00:00:40,40
So you have Composer and PHP 7.0 or greater.

14
00:00:40,40 --> 00:00:42,00
The next step is pretty easy.

15
00:00:42,00 --> 00:00:45,20
We need to open up a terminal application.

16
00:00:45,20 --> 00:00:47,30
Once you have your terminal application opened up,

17
00:00:47,30 --> 00:00:50,20
navigate to a directory where you'll be working with

18
00:00:50,20 --> 00:00:53,90
our code base, and PHP, and Composer is both available

19
00:00:53,90 --> 00:00:57,40
and able to be run from the command line.

20
00:00:57,40 --> 00:00:59,70
At this point, you'll want to enter the command,

21
00:00:59,70 --> 00:01:10,30
composer create-project --prefer-dist

22
00:01:10,30 --> 00:01:16,90
Laravel/Laravel community poll.

23
00:01:16,90 --> 00:01:20,30
This command creates a new directory: Community Poll,

24
00:01:20,30 --> 00:01:21,80
and installs Laravel

25
00:01:21,80 --> 00:01:24,20
and it's dependencies into that directory,

26
00:01:24,20 --> 00:01:26,50
and creates for us a boot strapped

27
00:01:26,50 --> 00:01:29,00
Laravel application in it's place,

28
00:01:29,00 --> 00:01:31,90
with the majority of settings and all of the setup

29
00:01:31,90 --> 00:01:36,50
already done for us at this point.

30
00:01:36,50 --> 00:01:39,00
Now let's navigate into that directory

31
00:01:39,00 --> 00:01:44,80
with the terminal command cd community poll.

32
00:01:44,80 --> 00:01:47,30
We're here in our Laravel application now.

33
00:01:47,30 --> 00:01:51,50
Feel free to explore what's here in the Laravel directory,

34
00:01:51,50 --> 00:01:52,80
although we're going to cover some

35
00:01:52,80 --> 00:01:55,10
of the basics in a few videos.

36
00:01:55,10 --> 00:01:56,40
We need to first verify that

37
00:01:56,40 --> 00:01:58,40
everything is working correctly.

38
00:01:58,40 --> 00:02:00,80
From here, in our terminal application,

39
00:02:00,80 --> 00:02:05,90
run the command PHP artisan serve.

40
00:02:05,90 --> 00:02:07,20
Remember this command.

41
00:02:07,20 --> 00:02:10,70
We're going to be running this command an awful lot.

42
00:02:10,70 --> 00:02:12,40
You should see something to the effect

43
00:02:12,40 --> 00:02:15,40
of Laravel development servers started,

44
00:02:15,40 --> 00:02:18,60
with the URL either local host 8000,

45
00:02:18,60 --> 00:02:25,80
or 127.0.0.1 port 8000.

46
00:02:25,80 --> 00:02:27,90
This means our Laravel application

47
00:02:27,90 --> 00:02:31,60
is being served via our local PHP instance.

48
00:02:31,60 --> 00:02:34,10
If we copy this URL

49
00:02:34,10 --> 00:02:37,50
and paste it into our web browser,

50
00:02:37,50 --> 00:02:40,10
we'll see that we can get to the Laravel home page

51
00:02:40,10 --> 00:02:41,80
for our application.

52
00:02:41,80 --> 00:02:44,60
Right now it's just a Laravel default home page.

53
00:02:44,60 --> 00:02:46,90
It includes links to Laravel's documentation

54
00:02:46,90 --> 00:02:50,70
and some various places to get help if you need it.

55
00:02:50,70 --> 00:02:53,00
At this point, you should have Laravel installed,

56
00:02:53,00 --> 00:02:54,40
and we have it running.

57
00:02:54,40 --> 00:02:58,00
This is going to be a solid start to our project at this point.

