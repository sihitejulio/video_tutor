1
00:00:00,70 --> 00:00:02,30
- [Instructor] At this point, we need to get Laravel

2
00:00:02,30 --> 00:00:04,70
working with our database.

3
00:00:04,70 --> 00:00:09,60
To begin with there is a SQLite file, database.sqlite,

4
00:00:09,60 --> 00:00:14,30
in the exercise files for this project, for this video.

5
00:00:14,30 --> 00:00:15,90
You want to move this file

6
00:00:15,90 --> 00:00:19,10
into the community-poll directory.

7
00:00:19,10 --> 00:00:21,30
After this, we need to get the file path

8
00:00:21,30 --> 00:00:23,40
of this particular location.

9
00:00:23,40 --> 00:00:26,60
If you open up your terminal application,

10
00:00:26,60 --> 00:00:29,50
and navigate to the directory with the file.

11
00:00:29,50 --> 00:00:32,10
If you're using Mac OS or Linux

12
00:00:32,10 --> 00:00:36,30
you can run the command pwd.

13
00:00:36,30 --> 00:00:42,10
For those of you on Windows, the command is cd.

14
00:00:42,10 --> 00:00:45,20
Run the appropriate command for your machine

15
00:00:45,20 --> 00:00:48,60
and this will give you the current path to the directory

16
00:00:48,60 --> 00:00:51,80
with the database.sqlite file.

17
00:00:51,80 --> 00:00:55,90
You're going to copy this file path.

18
00:00:55,90 --> 00:00:58,90
And now we're going to want to open up our .env file

19
00:00:58,90 --> 00:01:03,10
in your code editor of choice.

20
00:01:03,10 --> 00:01:07,40
Here in our editor we need to edit the db connection value

21
00:01:07,40 --> 00:01:08,80
to sqlite.

22
00:01:08,80 --> 00:01:11,30
This is on line eight.

23
00:01:11,30 --> 00:01:12,30
At the end of this,

24
00:01:12,30 --> 00:01:15,10
we need to include the directory separator.

25
00:01:15,10 --> 00:01:17,80
For Windows that is a backslash.

26
00:01:17,80 --> 00:01:22,20
And for Linux and Mac OS people, that's a forward slash.

27
00:01:22,20 --> 00:01:26,50
And then the database.sqlite for the name of the file.

28
00:01:26,50 --> 00:01:29,90
There are two other points to note at this time.

29
00:01:29,90 --> 00:01:31,90
If you are on Windows, you'll need to

30
00:01:31,90 --> 00:01:35,80
enclose the file path in a pair of double quotes.

31
00:01:35,80 --> 00:01:37,90
Also, if you are following along

32
00:01:37,90 --> 00:01:40,10
by using the exercise files,

33
00:01:40,10 --> 00:01:43,20
you'll need to edit the .env file

34
00:01:43,20 --> 00:01:46,20
with the path to the database.sqlite file

35
00:01:46,20 --> 00:01:49,80
for every single video throughout this course.

36
00:01:49,80 --> 00:01:53,80
On line 11, the DB_DATABASE value

37
00:01:53,80 --> 00:01:57,80
will be set to the path for the database file,

38
00:01:57,80 --> 00:02:00,80
which is going to be the path to the directory,

39
00:02:00,80 --> 00:02:03,10
which we already have.

40
00:02:03,10 --> 00:02:05,90
And then we need to include the directory separator.

41
00:02:05,90 --> 00:02:08,00
For Windows, this is a backslash.

42
00:02:08,00 --> 00:02:12,20
For Linux and Mac OS, that's the forward slash.

43
00:02:12,20 --> 00:02:16,10
And then database.sqlite.

44
00:02:16,10 --> 00:02:19,10
Now save this updated .env file.

45
00:02:19,10 --> 00:02:20,20
Now we can verify

46
00:02:20,20 --> 00:02:22,40
that Laravel is able to talk to the database

47
00:02:22,40 --> 00:02:25,60
and install the base data for our application.

48
00:02:25,60 --> 00:02:29,80
We're going to run the command in our terminal application.

49
00:02:29,80 --> 00:02:38,90
php artisan migrate:fresh --seed.

50
00:02:38,90 --> 00:02:40,80
At this point, you should see that it creates

51
00:02:40,80 --> 00:02:45,90
the users_table and creates the password_resets_table.

52
00:02:45,90 --> 00:02:49,00
At this point we want to install our own migrations

53
00:02:49,00 --> 00:02:51,00
and our own seeds.

54
00:02:51,00 --> 00:02:54,50
Migrations are the way in which we version our database.

55
00:02:54,50 --> 00:02:56,70
And seeding is the way in which we put in place

56
00:02:56,70 --> 00:03:01,40
basic data for our application into the database.

57
00:03:01,40 --> 00:03:03,80
There are three different files we'll be adding

58
00:03:03,80 --> 00:03:07,60
from our exercise files for this lesson.

59
00:03:07,60 --> 00:03:12,40
If you go back to the exercise files folder,

60
00:03:12,40 --> 00:03:17,30
you'll see that there is a community_polls.php file,

61
00:03:17,30 --> 00:03:23,40
a DatabaseSeeder.php and a PollFactory.php.

62
00:03:23,40 --> 00:03:29,00
First, we'll move the community_polls.php file

63
00:03:29,00 --> 00:03:34,30
into community-poll, database, migrations.

64
00:03:34,30 --> 00:03:37,00
Just copy it straight in here.

65
00:03:37,00 --> 00:03:41,00
The next file is the PollFactory.

66
00:03:41,00 --> 00:03:43,60
We're going to take the PollFactory

67
00:03:43,60 --> 00:03:48,00
and put it into database, factories.

68
00:03:48,00 --> 00:03:53,30
And, finally, we'll copy DatabaseSeeder.php,

69
00:03:53,30 --> 00:03:57,50
and this'll go into the database, seeds directory.

70
00:03:57,50 --> 00:03:58,50
And we're going to replace

71
00:03:58,50 --> 00:04:03,40
the existing DatabaseSeeder.php file in the directory.

72
00:04:03,40 --> 00:04:06,10
Now we can run our new migrations.

73
00:04:06,10 --> 00:04:09,30
We're going to go back to our terminal application

74
00:04:09,30 --> 00:04:17,00
and run the command php artisan migrate and refresh.

75
00:04:17,00 --> 00:04:19,40
At this point we have some database tables,

76
00:04:19,40 --> 00:04:22,20
but what we really need to create are some model classes

77
00:04:22,20 --> 00:04:25,10
for the polls, questions and answers.

78
00:04:25,10 --> 00:04:27,20
To do this, we'll run the command

79
00:04:27,20 --> 00:04:35,40
php artisan make:model Poll.

80
00:04:35,40 --> 00:04:38,30
We'll repeat this process for questions.

81
00:04:38,30 --> 00:04:41,30
In many terminal editors, you can use the up arrow

82
00:04:41,30 --> 00:04:43,30
to get the last command run.

83
00:04:43,30 --> 00:04:50,30
Backspace out the Poll and instead put in Question.

84
00:04:50,30 --> 00:04:54,10
And we'll repeat this again a third time for Answer.

85
00:04:54,10 --> 00:04:56,70
This creates the Laravel model classes

86
00:04:56,70 --> 00:05:00,60
for our Poll, Question and Answer tables.

87
00:05:00,60 --> 00:05:03,10
At this point finish seeding our database

88
00:05:03,10 --> 00:05:10,40
with the command php artisan db:seed.

89
00:05:10,40 --> 00:05:12,40
Now we have a database migrated.

90
00:05:12,40 --> 00:05:13,80
And inside of this database

91
00:05:13,80 --> 00:05:17,70
we have faked dummy data in place for polls,

92
00:05:17,70 --> 00:05:20,10
questions for those corresponding polls

93
00:05:20,10 --> 00:05:24,00
and answers to those exact same questions.

94
00:05:24,00 --> 00:05:26,40
If you have a SQLite database tool,

95
00:05:26,40 --> 00:05:30,30
such as DB Browser, which you can download

96
00:05:30,30 --> 00:05:35,70
from sqlitebrowser.org,

97
00:05:35,70 --> 00:05:40,70
you can see the data that we have in our database.

98
00:05:40,70 --> 00:05:44,70
So we can see here, we have some answers.

99
00:05:44,70 --> 00:05:48,00
We have different polls.

100
00:05:48,00 --> 00:05:51,00
And we have questions.

101
00:05:51,00 --> 00:05:53,40
Feel free to explore around this database more

102
00:05:53,40 --> 00:05:18,00
if you desire at this point.

