1
00:00:00,50 --> 00:00:03,70
Before we dive into building our Laravel API,

2
00:00:03,70 --> 00:00:07,60
let's talk about the basic ideas of what is an API,

3
00:00:07,60 --> 00:00:11,20
what we use it for, and when you might need one.

4
00:00:11,20 --> 00:00:16,20
An API stands for an Application Programming Interface.

5
00:00:16,20 --> 00:00:20,40
It's a tool to allow other programmers to build off of,

6
00:00:20,40 --> 00:00:22,60
to build their own software.

7
00:00:22,60 --> 00:00:24,10
APIs can be used by other

8
00:00:24,10 --> 00:00:27,80
in house teams or by teams outside of your own company.

9
00:00:27,80 --> 00:00:30,90
Sometimes you may even both build the API yourself

10
00:00:30,90 --> 00:00:33,40
and implement the API.

11
00:00:33,40 --> 00:00:36,30
APIs are designed to allow you to separate the front end

12
00:00:36,30 --> 00:00:39,80
implementation details from the back end.

13
00:00:39,80 --> 00:00:43,30
An API built this way will allow you to support multiple

14
00:00:43,30 --> 00:00:47,20
implementers or multiple teams to implement an application

15
00:00:47,20 --> 00:00:49,50
in their own specific way.

16
00:00:49,50 --> 00:00:52,50
Twitter, for instance, has an API that allows multiple

17
00:00:52,50 --> 00:00:54,30
Twitter clients to exist.

18
00:00:54,30 --> 00:00:57,00
They all implement the same Twitter API,

19
00:00:57,00 --> 00:01:00,40
but each implements it in their own unique way.

20
00:01:00,40 --> 00:01:02,10
You can even build API clients

21
00:01:02,10 --> 00:01:04,90
that only use part of the API.

22
00:01:04,90 --> 00:01:07,60
In Twitter's case, you may make a Twitter client,

23
00:01:07,60 --> 00:01:10,20
for instance, that only supports direct messages,

24
00:01:10,20 --> 00:01:11,80
but nothing else.

25
00:01:11,80 --> 00:01:16,00
APIs also shouldn't just be BREAD and CRUD operations.

26
00:01:16,00 --> 00:01:19,60
Or they shouldn't just implement an index, an add, an edit,

27
00:01:19,60 --> 00:01:22,00
a view, and a delete operation.

28
00:01:22,00 --> 00:01:23,80
The API can, and should,

29
00:01:23,80 --> 00:01:27,80
hide the actual underlying database and any possibly tricky

30
00:01:27,80 --> 00:01:33,50
logic behind an easy to read and program against interface.

31
00:01:33,50 --> 00:01:37,20
APIs can and do support multiple standards such as

32
00:01:37,20 --> 00:01:42,00
REST, RPC, GraphQL, SOAP, and basically anything you want

33
00:01:42,00 --> 00:01:46,10
the API to do, you can write your API to support.

34
00:01:46,10 --> 00:01:50,10
You can support multiple formats of data transmission.

35
00:01:50,10 --> 00:01:53,30
JSON, XML, JSON API, et cetera.

36
00:01:53,30 --> 00:01:56,50
And you can even provide multiple language support.

37
00:01:56,50 --> 00:01:59,60
Many APIs that are designed to be consumed by the general

38
00:01:59,60 --> 00:02:03,10
public will provide libraries to drop into your project to

39
00:02:03,10 --> 00:02:06,40
provide a basic implementation in any of a dozen

40
00:02:06,40 --> 00:02:08,50
different programming languages.

41
00:02:08,50 --> 00:02:11,30
Here's some examples of different APIs that each have

42
00:02:11,30 --> 00:02:13,20
different use cases.

43
00:02:13,20 --> 00:02:16,50
Stripe is used to perform payments on the web using both the

44
00:02:16,50 --> 00:02:21,60
back end HTTP JSON API to white label complete payment

45
00:02:21,60 --> 00:02:25,20
process as well as a JavaScript driven only front end

46
00:02:25,20 --> 00:02:27,70
for on page payments.

47
00:02:27,70 --> 00:02:31,20
Twitter has an API that is used both publicly for the web

48
00:02:31,20 --> 00:02:33,70
interface version of the Twitter website,

49
00:02:33,70 --> 00:02:36,10
as well as an API that is used by the hundreds

50
00:02:36,10 --> 00:02:40,60
of Twitter clients on multiple devices and platforms.

51
00:02:40,60 --> 00:02:43,90
Google Maps here can be used in multiple formats as well.

52
00:02:43,90 --> 00:02:47,30
We can display maps, validate addresses, get directions,

53
00:02:47,30 --> 00:02:50,20
and dozens of other use cases.

54
00:02:50,20 --> 00:02:53,20
The point here is that APIs come in all flavors

55
00:02:53,20 --> 00:02:55,50
and different sizes.

56
00:02:55,50 --> 00:02:58,00
We're going to build a simple API

57
00:02:58,00 --> 00:03:01,40
to support creating and managing polls.

58
00:03:01,40 --> 00:03:03,60
We'll also manage poll questions

59
00:03:03,60 --> 00:03:06,00
and their corresponding answers.

60
00:03:06,00 --> 00:03:07,80
We won't build a front end interface

61
00:03:07,80 --> 00:03:11,60
for this API, but let's explore a bit of what our API might

62
00:03:11,60 --> 00:03:15,50
look like if we implemented a front end for it.

63
00:03:15,50 --> 00:03:18,20
We'll have an API endpoint to build that interface for

64
00:03:18,20 --> 00:03:21,20
viewing questions for a particular poll.

65
00:03:21,20 --> 00:03:23,00
You'll be able to build an endpoint

66
00:03:23,00 --> 00:03:25,80
to be able to add answers for those questions.

67
00:03:25,80 --> 00:03:28,40
So you'll be able to create polls with their

68
00:03:28,40 --> 00:03:31,00
corresponding questions and you can then answer those

69
00:03:31,00 --> 00:03:33,50
questions to complete a poll.

70
00:03:33,50 --> 00:03:36,80
We'll finally build in some authentication for our API

71
00:03:36,80 --> 00:03:38,70
so we could assign the answers for our poll

72
00:03:38,70 --> 00:03:41,00
to a certain user if we wanted to.

73
00:03:41,00 --> 00:03:43,00
Or you could build the API to allow only

74
00:03:43,00 --> 00:03:46,10
certain users to access certain polls.

75
00:03:46,10 --> 00:03:49,20
You should keep in mind with building out an API,

76
00:03:49,20 --> 00:03:51,90
they typically are only one part of a tool,

77
00:03:51,90 --> 00:03:54,00
but not the whole tool.

78
00:03:54,00 --> 00:03:57,50
Sometimes you'll want or need to provide other tools.

79
00:03:57,50 --> 00:04:00,50
If you're building an API to power an internal front end,

80
00:04:00,50 --> 00:04:03,40
you'll still need to write that particular front end.

81
00:04:03,40 --> 00:04:06,70
If the API is designed for external teams to use and

82
00:04:06,70 --> 00:04:10,30
consume, you might want to build out a library for the API.

83
00:04:10,30 --> 00:04:12,60
That way it's easier for people to get started

84
00:04:12,60 --> 00:04:14,70
using your API.

85
00:04:14,70 --> 00:04:17,50
APIs are a complicated topic.

86
00:04:17,50 --> 00:04:19,40
You might want to look at a few other courses

87
00:04:19,40 --> 00:04:21,30
if you feel uncomfortable with the idea of

88
00:04:21,30 --> 00:04:24,40
forging ahead with this course right away.

89
00:04:24,40 --> 00:04:25,40
My course covering

90
00:04:25,40 --> 00:04:28,70
Consuming RESTful APIs in PHP with Guzzle

91
00:04:28,70 --> 00:04:31,40
may give you some basic ideas around working with

92
00:04:31,40 --> 00:04:35,50
APIs as a consumer and why you might use them.

93
00:04:35,50 --> 00:04:38,20
The courses on Designing RESTful APIs and

94
00:04:38,20 --> 00:04:40,90
API Testing and Validation can be helpful

95
00:04:40,90 --> 00:04:44,00
to understand some of the finer details around designing

96
00:04:44,00 --> 00:04:46,10
and testing APIs.

97
00:04:46,10 --> 00:04:49,00
There are also many other API building courses in the

98
00:04:49,00 --> 00:04:52,10
library if you want to take a look at building APIs with

99
00:04:52,10 --> 00:05:16,00
tools other than Laravel.

