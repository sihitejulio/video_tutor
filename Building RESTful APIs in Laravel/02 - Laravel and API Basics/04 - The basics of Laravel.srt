1
00:00:00,50 --> 00:00:01,50
- [Instructor] In this video,

2
00:00:01,50 --> 00:00:05,70
we're going to explore the basics of working with Laravel.

3
00:00:05,70 --> 00:00:09,50
Laravel 5.5 is a modern PHP 7

4
00:00:09,50 --> 00:00:13,20
MVC or Model View Controller framework

5
00:00:13,20 --> 00:00:15,90
focused around providing an elegant,

6
00:00:15,90 --> 00:00:19,70
beautiful, simple-to-read framework.

7
00:00:19,70 --> 00:00:21,50
Laravel's particular focus

8
00:00:21,50 --> 00:00:25,30
is on making the code easy to read and to work with.

9
00:00:25,30 --> 00:00:28,90
Laravel's tag line even is, "Love beautiful code?

10
00:00:28,90 --> 00:00:31,80
"We do too."

11
00:00:31,80 --> 00:00:34,70
Now this course is not going to cover Laravel 5

12
00:00:34,70 --> 00:00:37,00
in any great detail.

13
00:00:37,00 --> 00:00:40,20
We're going to be using Laravel and working with it.

14
00:00:40,20 --> 00:00:41,80
But if you need to spend some time

15
00:00:41,80 --> 00:00:45,20
getting some essential training on Laravel 5,

16
00:00:45,20 --> 00:00:47,40
inside of the library there is a course

17
00:00:47,40 --> 00:00:52,70
Laravel 5 Essential Training that I would recommend.

18
00:00:52,70 --> 00:00:55,30
Let's explore the folder structure of Laravel

19
00:00:55,30 --> 00:00:57,50
and then we'll look at a concrete example

20
00:00:57,50 --> 00:01:00,70
of Laravel's easy-to-read code.

21
00:01:00,70 --> 00:01:03,80
Laravel's main top-level directory is here

22
00:01:03,80 --> 00:01:06,00
where we install the application.

23
00:01:06,00 --> 00:01:07,90
It contains an app directory

24
00:01:07,90 --> 00:01:11,60
that contains all of our core application code.

25
00:01:11,60 --> 00:01:13,30
Bootstrap, on the other hand,

26
00:01:13,30 --> 00:01:16,90
is where our application gets booted.

27
00:01:16,90 --> 00:01:18,80
Config is where, take a guess,

28
00:01:18,80 --> 00:01:22,10
the configuration of our application occurs.

29
00:01:22,10 --> 00:01:24,00
Here in the config directory,

30
00:01:24,00 --> 00:01:26,60
you'll see that the config system isn't designed

31
00:01:26,60 --> 00:01:29,20
to give you just one file to edit.

32
00:01:29,20 --> 00:01:31,80
Instead, we split the configuration,

33
00:01:31,80 --> 00:01:34,60
making it easier to isolate and understand

34
00:01:34,60 --> 00:01:39,00
exactly where you need to add or edit your configuration

35
00:01:39,00 --> 00:01:42,60
to edit any particular part of the application.

36
00:01:42,60 --> 00:01:45,40
The database folder is where we store our factories,

37
00:01:45,40 --> 00:01:49,00
migrations and seeds as we saw earlier.

38
00:01:49,00 --> 00:01:51,60
Public is where the application is hosted from

39
00:01:51,60 --> 00:01:53,90
when you push your application live.

40
00:01:53,90 --> 00:01:56,20
This is where the first PHP file

41
00:01:56,20 --> 00:01:58,40
that your visitors to your site will hit

42
00:01:58,40 --> 00:02:01,70
as well as where the minified CSS and JavaScript files

43
00:02:01,70 --> 00:02:04,60
your application uses live.

44
00:02:04,60 --> 00:02:07,90
Resources is where we place the translations,

45
00:02:07,90 --> 00:02:10,70
unminified CSS and JavaScript

46
00:02:10,70 --> 00:02:13,90
as well as the view files for your application.

47
00:02:13,90 --> 00:02:15,40
The routes directory

48
00:02:15,40 --> 00:02:18,20
contains the routes for our application.

49
00:02:18,20 --> 00:02:21,90
This is the stuff that says, "On this URL for this site,

50
00:02:21,90 --> 00:02:25,30
"it points to this controller and this action."

51
00:02:25,30 --> 00:02:29,20
Storage is for placing logs and other various cache files

52
00:02:29,20 --> 00:02:32,90
and any other long-term storage.

53
00:02:32,90 --> 00:02:34,70
Tests of course is the directory

54
00:02:34,70 --> 00:02:37,70
where we'll add and write tests.

55
00:02:37,70 --> 00:02:39,20
And vendor is for a composer

56
00:02:39,20 --> 00:02:41,80
to host its particular packages.

57
00:02:41,80 --> 00:02:43,70
If we look at the app directory,

58
00:02:43,70 --> 00:02:46,30
this is where we'll spend most of our time.

59
00:02:46,30 --> 00:02:49,70
The user class here in the root directory

60
00:02:49,70 --> 00:02:53,20
is our model class for the user table.

61
00:02:53,20 --> 00:02:54,60
Now in Laravel,

62
00:02:54,60 --> 00:02:57,80
the default is you don't have a model directory.

63
00:02:57,80 --> 00:02:59,80
However, if this bothers you,

64
00:02:59,80 --> 00:03:02,40
you can always change it down the road.

65
00:03:02,40 --> 00:03:05,30
Our models class in model view controller

66
00:03:05,30 --> 00:03:07,40
is where we talk to the database.

67
00:03:07,40 --> 00:03:10,80
We both save data as well as retrieve it.

68
00:03:10,80 --> 00:03:13,60
If we look at the user class for a bit,

69
00:03:13,60 --> 00:03:16,10
we'll notice it's all modern PHP.

70
00:03:16,10 --> 00:03:18,50
We have a class that extends another class.

71
00:03:18,50 --> 00:03:20,40
We're going to use a trait

72
00:03:20,40 --> 00:03:23,30
and it's going to have some different properties.

73
00:03:23,30 --> 00:03:25,80
In this case, fillable are those properties

74
00:03:25,80 --> 00:03:30,00
that we can assign when setting form fields in mass.

75
00:03:30,00 --> 00:03:32,20
Hidden, on the other hand, are those properties

76
00:03:32,20 --> 00:03:34,70
that are hidden when dumping the object

77
00:03:34,70 --> 00:03:38,90
or returning it for instance as part of an API response.

78
00:03:38,90 --> 00:03:41,90
What about where our controllers live?

79
00:03:41,90 --> 00:03:45,60
The controllers live in the HTTP directory.

80
00:03:45,60 --> 00:03:49,40
There we have a controllers and a middleware directory.

81
00:03:49,40 --> 00:03:51,90
Controllers act in between our model

82
00:03:51,90 --> 00:03:54,90
where we talk to the database and the views,

83
00:03:54,90 --> 00:03:57,20
the stuff the end user sees.

84
00:03:57,20 --> 00:04:01,20
The controller coordinates between the model and the view.

85
00:04:01,20 --> 00:04:04,70
When we view a page, we hit the controller to retrieve data.

86
00:04:04,70 --> 00:04:07,00
We pass that data to the view

87
00:04:07,00 --> 00:04:10,30
which formats that data for the end user.

88
00:04:10,30 --> 00:04:12,50
When you submit a form on the other hand,

89
00:04:12,50 --> 00:04:15,80
we hit the controller which passes that data to the model

90
00:04:15,80 --> 00:04:17,60
to be saved and updated

91
00:04:17,60 --> 00:04:21,80
and then redirects it presumably to a new view.

92
00:04:21,80 --> 00:04:24,00
Middleware is something we'll discuss later,

93
00:04:24,00 --> 00:04:26,00
but the basic idea of middleware

94
00:04:26,00 --> 00:04:29,70
is to sit in between our application and the end user

95
00:04:29,70 --> 00:04:32,70
and perform things like ensuring a user is logged in

96
00:04:32,70 --> 00:04:36,30
or adding any HTTP headers that we need.

97
00:04:36,30 --> 00:04:38,60
If we go back up to our app directory,

98
00:04:38,60 --> 00:04:41,90
we'll see a providers directory.

99
00:04:41,90 --> 00:04:46,30
Inside of this providers directory are service providers.

100
00:04:46,30 --> 00:04:49,60
Service providers are a somewhat unique feature to Laravel

101
00:04:49,60 --> 00:04:51,70
in the PHP framework world.

102
00:04:51,70 --> 00:04:54,10
They make it easy for you to register

103
00:04:54,10 --> 00:04:58,80
and add core services to the framework at runtime.

104
00:04:58,80 --> 00:05:02,00
Feel free to keep exploring the Laravel app at this point.

