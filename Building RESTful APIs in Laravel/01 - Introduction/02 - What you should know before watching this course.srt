1
00:00:00,50 --> 00:00:01,40
- [Instructor] There's some certain things

2
00:00:01,40 --> 00:00:06,00
that you should know about before you begin this course.

3
00:00:06,00 --> 00:00:07,70
To follow along, you should have

4
00:00:07,70 --> 00:00:10,40
a solid working knowledge of PHP.

5
00:00:10,40 --> 00:00:13,50
You will need to have PHP 7.0 or newer

6
00:00:13,50 --> 00:00:17,10
installed and accessible from the command line.

7
00:00:17,10 --> 00:00:20,00
Including PHP 7.0 or newer installed,

8
00:00:20,00 --> 00:00:24,30
you also need some PHP packages installed.

9
00:00:24,30 --> 00:00:27,70
These are required for Laravel to work.

10
00:00:27,70 --> 00:00:29,80
You also need Composer installed

11
00:00:29,80 --> 00:00:33,10
and accessible from your terminal application.

12
00:00:33,10 --> 00:00:35,50
This course is going to assume you at least have

13
00:00:35,50 --> 00:00:37,40
a rough understanding of Laravel

14
00:00:37,40 --> 00:00:39,80
and working within the framework.

15
00:00:39,80 --> 00:00:42,70
That being said, nothing should be too difficult

16
00:00:42,70 --> 00:00:46,00
for you to accomplish if you don't already know Laravel.

17
00:00:46,00 --> 00:00:48,20
But we aren't going to learn Laravel

18
00:00:48,20 --> 00:00:50,20
specifically in this course.

19
00:00:50,20 --> 00:00:54,30
We're focusing on APIs, not Laravel.

20
00:00:54,30 --> 00:00:56,10
And finally we'll run some commands

21
00:00:56,10 --> 00:00:59,20
from the terminal application on a regular basis.

22
00:00:59,20 --> 00:01:01,80
So you should know how to navigate around the terminal

23
00:01:01,80 --> 00:01:03,70
and use it to run commands.

24
00:01:03,70 --> 00:01:07,00
Everything else we'll work through using the course content.

