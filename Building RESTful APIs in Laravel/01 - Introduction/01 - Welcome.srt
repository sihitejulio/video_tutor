1
00:00:00,50 --> 00:00:02,60
- [Justin] Hello, I'm Justin Yost,

2
00:00:02,60 --> 00:00:06,50
and welcome to Building RESTful APIs in Laravel.

3
00:00:06,50 --> 00:00:10,40
Building RESTful APIs in Laravel is designed to give you

4
00:00:10,40 --> 00:00:13,90
the ability to spin up your own custom API,

5
00:00:13,90 --> 00:00:17,40
driven with the beautiful framework Laravel.

6
00:00:17,40 --> 00:00:20,10
In this course, we're going to cover the basics

7
00:00:20,10 --> 00:00:23,80
of building a BREAD, or browse, read,

8
00:00:23,80 --> 00:00:28,10
edit, add, and delete API in Laravel.

9
00:00:28,10 --> 00:00:30,80
You may have also heard this as CRUD,

10
00:00:30,80 --> 00:00:34,80
for create, read, update, and delete.

11
00:00:34,80 --> 00:00:37,50
We'll then dive into customizing that API

12
00:00:37,50 --> 00:00:40,80
to make your API shine above the rest.

13
00:00:40,80 --> 00:00:43,40
I'll start with a basic API and then we'll go

14
00:00:43,40 --> 00:00:46,60
into building custom API responses and building out

15
00:00:46,60 --> 00:00:51,10
middleware to transform your API responses and requests.

16
00:00:51,10 --> 00:00:54,10
We'll finish up with covering the basics of ensuring

17
00:00:54,10 --> 00:00:58,10
you only let authenticated users access your API.

18
00:00:58,10 --> 00:00:59,80
And then we'll explore the basic

19
00:00:59,80 --> 00:01:02,70
testing techniques in an API.

20
00:01:02,70 --> 00:01:06,00
Let's dive in to Building APIs using Laravel.

