1
00:00:00,50 --> 00:00:02,60
- [Narrator] If you have access to the exercise files

2
00:00:02,60 --> 00:00:05,20
for this course you can download them to your desktop

3
00:00:05,20 --> 00:00:06,90
as I've done here.

4
00:00:06,90 --> 00:00:10,80
The exercise files are organized into folders,

5
00:00:10,80 --> 00:00:14,70
divided by chapter, and then movie number.

6
00:00:14,70 --> 00:00:17,30
Inside each folder is the collection of code

7
00:00:17,30 --> 00:00:19,70
we'll be creating throughout the course,

8
00:00:19,70 --> 00:00:22,90
as well as any additional files or snippets needed

9
00:00:22,90 --> 00:00:25,20
for that specific video.

10
00:00:25,20 --> 00:00:27,50
If you don't have access to the exercise files,

11
00:00:27,50 --> 00:00:30,20
you can still follow along by creating your own files

12
00:00:30,20 --> 00:00:31,70
while watching.

13
00:00:31,70 --> 00:00:34,30
Some of the particular movies include a separate file

14
00:00:34,30 --> 00:00:35,90
for that specific video.

15
00:00:35,90 --> 00:00:38,00
We'll cover its use as we need it.

