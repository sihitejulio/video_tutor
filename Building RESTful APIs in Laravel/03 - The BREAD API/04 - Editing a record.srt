1
00:00:00,90 --> 00:00:02,80
- [Instructor] Editing a record is going to look similar

2
00:00:02,80 --> 00:00:04,40
to adding a record.

3
00:00:04,40 --> 00:00:09,00
In this case however, we'll use a put HTTP request

4
00:00:09,00 --> 00:00:11,90
on the polls ID endpoint.

5
00:00:11,90 --> 00:00:14,30
Put is used to put a new resource

6
00:00:14,30 --> 00:00:17,40
onto the specific resource or URL.

7
00:00:17,40 --> 00:00:21,10
So we put a new poll to replace the current poll

8
00:00:21,10 --> 00:00:23,10
at the current resource.

9
00:00:23,10 --> 00:00:26,10
You can also use patch to do this same thing.

10
00:00:26,10 --> 00:00:29,00
There's a lot of discussion amongst REST nerds

11
00:00:29,00 --> 00:00:30,40
about put and patch

12
00:00:30,40 --> 00:00:33,30
and when to use one versus when to use the other.

13
00:00:33,30 --> 00:00:35,60
For now, I'm just not going to worry about that.

14
00:00:35,60 --> 00:00:37,90
That's getting a little deep into REST

15
00:00:37,90 --> 00:00:40,60
and we don't really need to deal with that at this point.

16
00:00:40,60 --> 00:00:43,70
Just know we're going to use put and it'll be fine.

17
00:00:43,70 --> 00:00:46,80
Feel free to use patch if you want to however.

18
00:00:46,80 --> 00:00:48,70
We'll begin like we did last time,

19
00:00:48,70 --> 00:00:50,90
and as we're going to do pretty frequently,

20
00:00:50,90 --> 00:00:52,50
by adding a new route.

21
00:00:52,50 --> 00:00:56,60
We'll open up our routes api.php

22
00:00:56,60 --> 00:01:00,10
and we'll add a new route on line 22

23
00:01:00,10 --> 00:01:08,50
that will read route::put and then the string polls/poll.

24
00:01:08,50 --> 00:01:11,50
This poll is going to be in curly braces.

25
00:01:11,50 --> 00:01:13,90
This permits us to use the ID value

26
00:01:13,90 --> 00:01:15,80
and thanks to Laravel's magic

27
00:01:15,80 --> 00:01:18,80
have that automatically find the correct instance

28
00:01:18,80 --> 00:01:21,40
of the poll record in our database.

29
00:01:21,40 --> 00:01:23,90
Now for the corresponding controller and action,

30
00:01:23,90 --> 00:01:26,90
it will of course be PollsController

31
00:01:26,90 --> 00:01:29,00
and the action will be update.

32
00:01:29,00 --> 00:01:31,10
Now to add the controller logic.

33
00:01:31,10 --> 00:01:36,00
We'll open up PollsController contained in app, HTTP,

34
00:01:36,00 --> 00:01:39,40
controllers, PollsContoller.php.

35
00:01:39,40 --> 00:01:44,00
On line 26, we'll add a new public function udpate.

36
00:01:44,00 --> 00:01:46,60
This function is going to take two parameters.

37
00:01:46,60 --> 00:01:49,70
The first is going to be a request object

38
00:01:49,70 --> 00:01:53,10
so we'll pass in request and request.

39
00:01:53,10 --> 00:01:56,60
Second will be the instance of the poll object

40
00:01:56,60 --> 00:02:00,30
that Laravel populates from the ID in the URL

41
00:02:00,30 --> 00:02:04,40
so we'll add Poll and the variable name will be poll.

42
00:02:04,40 --> 00:02:09,60
Now we can update the record using poll update request all

43
00:02:09,60 --> 00:02:13,90
similar to how we did on line 22 in our store method

44
00:02:13,90 --> 00:02:18,50
so line 28 will read poll arrow update

45
00:02:18,50 --> 00:02:20,30
to call the update method

46
00:02:20,30 --> 00:02:22,60
and request all.

47
00:02:22,60 --> 00:02:26,50
So we're going to pass in all of the data from our request,

48
00:02:26,50 --> 00:02:31,90
pass it into our poll object and update that poll instance.

49
00:02:31,90 --> 00:02:33,40
Pretty simple, right?

50
00:02:33,40 --> 00:02:36,20
Line 21, we're going to do pretty much the same thing

51
00:02:36,20 --> 00:02:37,50
that we've done already.

52
00:02:37,50 --> 00:02:44,80
We're going to return response arrow json poll and then 200.

53
00:02:44,80 --> 00:02:47,60
Now why 200 instead of 201?

54
00:02:47,60 --> 00:02:50,60
Well keep in mind, we're only editing the object.

55
00:02:50,60 --> 00:02:53,40
We're not actually creating a new poll.

56
00:02:53,40 --> 00:02:57,00
201 is for when we have a whole new object.

57
00:02:57,00 --> 00:03:00,50
In this case, we're simply editing so we return 200

58
00:03:00,50 --> 00:03:02,90
to say everything worked out fine.

59
00:03:02,90 --> 00:03:04,60
Now we're going to test this out.

60
00:03:04,60 --> 00:03:07,70
Recall to start your server with PHP artisan server

61
00:03:07,70 --> 00:03:09,80
if you haven't yet already.

62
00:03:09,80 --> 00:03:14,50
Go to Insomnia and let's duplicate the post request.

63
00:03:14,50 --> 00:03:16,90
We'll edit the name of the post request

64
00:03:16,90 --> 00:03:20,80
and instead we'll have it be put poll.

65
00:03:20,80 --> 00:03:25,70
We'll change the post HTTP verb to instead be put.

66
00:03:25,70 --> 00:03:30,60
And for api/polls, we instead need to pass in the ID

67
00:03:30,60 --> 00:03:33,20
for the poll that we're going to be modifying,

68
00:03:33,20 --> 00:03:36,40
so in this case we'll modify poll 11.

69
00:03:36,40 --> 00:03:39,80
Now let's change the title to rather than be testing,

70
00:03:39,80 --> 00:03:42,10
we'll call it test change

71
00:03:42,10 --> 00:03:45,60
so we can verify that our poll actually is changing.

72
00:03:45,60 --> 00:03:48,20
We'll send the request and sure enough,

73
00:03:48,20 --> 00:03:51,20
our poll was changed from the title of testing

74
00:03:51,20 --> 00:03:52,70
to test change.

75
00:03:52,70 --> 00:03:56,40
If you get a 404 record not found error at this point,

76
00:03:56,40 --> 00:03:59,80
please confirm that you have a record for this ID.

77
00:03:59,80 --> 00:04:03,40
You can do so by doing our get poll request

78
00:04:03,40 --> 00:04:05,10
on that matching ID.

79
00:04:05,10 --> 00:04:07,10
If it doesn't return a matching record,

80
00:04:07,10 --> 00:04:09,00
you'll need to use a different ID

81
00:04:09,00 --> 00:04:12,90
such as two, five, six, or seven.

82
00:04:12,90 --> 00:04:15,80
If we look in our get poll method

83
00:04:15,80 --> 00:04:18,10
and we call get poll 11,

84
00:04:18,10 --> 00:04:20,60
we can see and verify in fact

85
00:04:20,60 --> 00:04:22,90
that our poll has in fact changed.

86
00:04:22,90 --> 00:04:24,20
This is pretty awesome.

87
00:04:24,20 --> 00:04:27,00
At this point, we have four out of five parts

88
00:04:27,00 --> 00:04:09,00
of our bread API already done.

