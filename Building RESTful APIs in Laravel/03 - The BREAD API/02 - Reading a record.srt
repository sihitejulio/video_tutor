1
00:00:00,50 --> 00:00:02,30
- [Instructor] The first part of our API

2
00:00:02,30 --> 00:00:04,50
was building an endpoint to get all

3
00:00:04,50 --> 00:00:06,80
of the polls in our database.

4
00:00:06,80 --> 00:00:10,50
What we want to do now is to get only a single poll.

5
00:00:10,50 --> 00:00:14,70
In other words, we last asked our API for every single poll.

6
00:00:14,70 --> 00:00:18,20
This time, we want to ask for just a single poll.

7
00:00:18,20 --> 00:00:21,40
We want to read for just a single record.

8
00:00:21,40 --> 00:00:23,70
First of all, let's install a tool

9
00:00:23,70 --> 00:00:27,70
to work with our API, as opposed to just using the browser,

10
00:00:27,70 --> 00:00:31,40
which works fine but isn't the best tool available to us.

11
00:00:31,40 --> 00:00:36,70
Open up your browser to the URL insomnia.rest.

12
00:00:36,70 --> 00:00:41,40
Here you can download a cross-platform, free API client.

13
00:00:41,40 --> 00:00:44,20
There are others, including tools that let you share

14
00:00:44,20 --> 00:00:48,50
API environments in a cloud service with your coworkers.

15
00:00:48,50 --> 00:00:52,40
But this tool is going to work fine for us and for our needs.

16
00:00:52,40 --> 00:00:54,80
Download and install the application,

17
00:00:54,80 --> 00:00:56,60
and come back when you're done.

18
00:00:56,60 --> 00:00:58,80
Now we want to open up Insomnia,

19
00:00:58,80 --> 00:01:01,80
and at this point, we want to create a new request.

20
00:01:01,80 --> 00:01:05,20
We're going to label the request set or get polls

21
00:01:05,20 --> 00:01:22,00
to get the URL at http://127.0.0.1port8000/API/polls.

22
00:01:22,00 --> 00:01:23,80
And if we try to send the request,

23
00:01:23,80 --> 00:01:25,30
we'll see that it didn't work.

24
00:01:25,30 --> 00:01:26,80
Well why is that?

25
00:01:26,80 --> 00:01:29,50
Oh right, we didn't start our server yet.

26
00:01:29,50 --> 00:01:32,00
We'll go back to our terminal application

27
00:01:32,00 --> 00:01:36,30
and start up our server with php artisan serve.

28
00:01:36,30 --> 00:01:39,80
Now let's go back to Insomnia and make sure this worked.

29
00:01:39,80 --> 00:01:43,10
We'll send the request again, and it did indeed

30
00:01:43,10 --> 00:01:45,20
pull back all of our polls.

31
00:01:45,20 --> 00:01:48,40
At this point, we want to make a new request.

32
00:01:48,40 --> 00:01:51,50
We'll duplicate the request, as most of the request

33
00:01:51,50 --> 00:01:53,60
is going to look exactly the same.

34
00:01:53,60 --> 00:01:56,80
Right-click, and there's a duplicate ability.

35
00:01:56,80 --> 00:02:00,70
We'll edit the name for this, as it'll be just get poll,

36
00:02:00,70 --> 00:02:02,30
without the plural.

37
00:02:02,30 --> 00:02:05,20
Now, if you ran the seed, you should have 10 polls.

38
00:02:05,20 --> 00:02:08,60
So we can technically send any ID from 1 through 10,

39
00:02:08,60 --> 00:02:10,50
and send off our request.

40
00:02:10,50 --> 00:02:16,40
So we'll change our API to instead ask for API/polls/

41
00:02:16,40 --> 00:02:18,40
in this case, I'll use 3.

42
00:02:18,40 --> 00:02:22,50
And we'll try sending it, and we'll get a 404 response.

43
00:02:22,50 --> 00:02:24,50
This is because our API isn't set up

44
00:02:24,50 --> 00:02:26,40
to respond to this endpoint.

45
00:02:26,40 --> 00:02:28,70
So we need to change that first.

46
00:02:28,70 --> 00:02:32,60
We'll go to our text editor, and open up the api file

47
00:02:32,60 --> 00:02:34,50
in the routes directory.

48
00:02:34,50 --> 00:02:39,00
Now we'll copy line 19 and paste it as line 20.

49
00:02:39,00 --> 00:02:40,90
Now we need to change this a bit,

50
00:02:40,90 --> 00:02:44,50
as it's going to expect an ID value for the URL,

51
00:02:44,50 --> 00:02:47,20
and to send it to a different controller action.

52
00:02:47,20 --> 00:02:51,10
So first, on line 20, we'll replace polls

53
00:02:51,10 --> 00:02:56,10
with polls/ and in curly braces, ID.

54
00:02:56,10 --> 00:02:59,20
This tells Laravel our route will contain an ID

55
00:02:59,20 --> 00:03:01,30
in the URL path.

56
00:03:01,30 --> 00:03:03,60
And now we change polls at index

57
00:03:03,60 --> 00:03:06,60
to read as pollsController@show,

58
00:03:06,60 --> 00:03:11,00
show being the Laravel standard for the view request action.

59
00:03:11,00 --> 00:03:14,50
At this point, we of course need to modify the controller.

60
00:03:14,50 --> 00:03:19,10
We'll open up the polls controller at app, Http,

61
00:03:19,10 --> 00:03:22,40
Controllers, PollsController.php.

62
00:03:22,40 --> 00:03:25,50
After our index function, we can add a new function

63
00:03:25,50 --> 00:03:29,70
on line 15, public function show.

64
00:03:29,70 --> 00:03:33,90
This show method is going to have a parameter of id.

65
00:03:33,90 --> 00:03:37,10
On line 17, we'll call Eloquent's find method,

66
00:03:37,10 --> 00:03:41,50
which takes either an array of IDs or a single ID value.

67
00:03:41,50 --> 00:03:47,60
So this will read as return response, as a function call,

68
00:03:47,60 --> 00:03:51,20
arrow, json, much like we did before,

69
00:03:51,20 --> 00:03:58,00
poll::find, pass in our ID, and then 200,

70
00:03:58,00 --> 00:04:00,70
for the 200 response code.

71
00:04:00,70 --> 00:04:02,50
Now save all this.

72
00:04:02,50 --> 00:04:04,40
Go back to Insomnia.

73
00:04:04,40 --> 00:04:06,80
And attempt to rerun our request.

74
00:04:06,80 --> 00:04:10,20
And this time, we'll get back a single poll result.

75
00:04:10,20 --> 00:04:11,40
Pretty cool, right?

76
00:04:11,40 --> 00:04:13,10
We've already built two out of the five pieces

77
00:04:13,10 --> 00:04:15,00
of our bread API.

78
00:04:15,00 --> 00:04:17,00
We have browse and read.

79
00:04:17,00 --> 00:04:19,00
Next, we're going to work on adding.

