1
00:00:00,50 --> 00:00:02,60
- [Instructor] Our first goal with our API

2
00:00:02,60 --> 00:00:06,60
is to build the ability to get the records for an endpoint.

3
00:00:06,60 --> 00:00:10,60
In this case, we want to get all of the polls in our database.

4
00:00:10,60 --> 00:00:12,10
In this case, it's just going to be

5
00:00:12,10 --> 00:00:15,50
a simple API endpoint to get all the polls.

6
00:00:15,50 --> 00:00:17,70
To begin with, we need a controller,

7
00:00:17,70 --> 00:00:20,80
so we'll make a new controller to manage polls with.

8
00:00:20,80 --> 00:00:24,00
In this case, using Laravel's make command.

9
00:00:24,00 --> 00:00:27,40
To run this, open up your terminal application

10
00:00:27,40 --> 00:00:30,60
into the community-poll directory and run the command

11
00:00:30,60 --> 00:00:37,50
php artisan make:controller PollsController.

12
00:00:37,50 --> 00:00:39,80
After this, we'll need to build a route for us

13
00:00:39,80 --> 00:00:42,20
to be able to hit the PollsController.

14
00:00:42,20 --> 00:00:44,50
We'll open up our code editor

15
00:00:44,50 --> 00:00:47,50
and in here, we'll open up the routes directory

16
00:00:47,50 --> 00:00:50,80
and open up the api.php file.

17
00:00:50,80 --> 00:00:56,90
We'll add on line 19 Route::get,

18
00:00:56,90 --> 00:00:59,90
pass the string polls,

19
00:00:59,90 --> 00:01:05,00
and then the string PollsController@index.

20
00:01:05,00 --> 00:01:05,90
This says when we send

21
00:01:05,90 --> 00:01:10,20
a GET HTTP request to the polls' route,

22
00:01:10,20 --> 00:01:12,90
route the action to the PollsController.

23
00:01:12,90 --> 00:01:15,50
And then the index meant that they're in.

24
00:01:15,50 --> 00:01:19,60
Since this is in the API call rather than the web file,

25
00:01:19,60 --> 00:01:23,60
our routes will all have a prefix of API applied to them.

26
00:01:23,60 --> 00:01:27,10
Now, let's see what happens if we start our application.

27
00:01:27,10 --> 00:01:29,10
Go back to your terminal application

28
00:01:29,10 --> 00:01:33,20
and run the command php artisan serve.

29
00:01:33,20 --> 00:01:44,40
In our browser, we'll go to 127.0.0.1:8000/api/polls.

30
00:01:44,40 --> 00:01:46,60
At this point, we get an error page

31
00:01:46,60 --> 00:01:48,70
and that's because we don't have an index method

32
00:01:48,70 --> 00:01:51,10
in our PollsController class.

33
00:01:51,10 --> 00:01:53,20
Now, we're going to go create one.

34
00:01:53,20 --> 00:01:56,80
We'll open up the PollsController file in our text editor,

35
00:01:56,80 --> 00:02:03,10
which is at app, Http, Controllers, PollsController.php.

36
00:02:03,10 --> 00:02:05,30
On line nine, we'll replace the comment

37
00:02:05,30 --> 00:02:09,10
with a new empty public function index.

38
00:02:09,10 --> 00:02:13,30
If we save this

39
00:02:13,30 --> 00:02:15,90
and refresh the page in our browser,

40
00:02:15,90 --> 00:02:19,10
we no longer at least having the error page displayed,

41
00:02:19,10 --> 00:02:20,80
but of course we don't see anything

42
00:02:20,80 --> 00:02:24,40
as the controller method isn't actually doing anything.

43
00:02:24,40 --> 00:02:25,30
So to finish up,

44
00:02:25,30 --> 00:02:27,90
we need to load in the polls and display them.

45
00:02:27,90 --> 00:02:31,80
Go back to our file, and in the index function,

46
00:02:31,80 --> 00:02:35,40
add the line return response,

47
00:02:35,40 --> 00:02:38,30
which is going to be a function call, arrow,

48
00:02:38,30 --> 00:02:43,80
json will call Poll::get(), 200,

49
00:02:43,80 --> 00:02:46,00
with a semicolon at the end.

50
00:02:46,00 --> 00:02:48,10
This says to return a response

51
00:02:48,10 --> 00:02:51,30
that is a JSON formatted response

52
00:02:51,30 --> 00:02:54,80
with the HTTP response code 200,

53
00:02:54,80 --> 00:02:58,80
and it's going to get the polls from our database.

54
00:02:58,80 --> 00:03:02,90
At this point, we also need to add before line five,

55
00:03:02,90 --> 00:03:05,50
use App\Poll,

56
00:03:05,50 --> 00:03:09,20
so we include the Poll object into our class.

57
00:03:09,20 --> 00:03:11,90
Now we can go back to our browser and refresh,

58
00:03:11,90 --> 00:03:16,00
and there you'll see all of our polls formatted in JSON

59
00:03:16,00 --> 00:03:18,20
with the 200 response code.

60
00:03:18,20 --> 00:03:20,60
And that's how easy it was for Laravel

61
00:03:20,60 --> 00:03:24,00
and for us to build in a basic endpoint.

