1
00:00:00,00 --> 00:00:03,10
- [Narrator] Adding a record is going to be the first time

2
00:00:03,10 --> 00:00:06,10
that we're going to submit data to our API.

3
00:00:06,10 --> 00:00:08,50
So at this point it might be the time at which

4
00:00:08,50 --> 00:00:11,20
you're starting to wonder about how we're going to deal

5
00:00:11,20 --> 00:00:13,50
with data formatting or how to handle

6
00:00:13,50 --> 00:00:16,00
error responses and validation.

7
00:00:16,00 --> 00:00:17,80
I'm going to make it simple for you.

8
00:00:17,80 --> 00:00:20,00
Today for this video, we're just

9
00:00:20,00 --> 00:00:22,40
not going to worry about that about that stuff.

10
00:00:22,40 --> 00:00:24,20
We are going to build this out

11
00:00:24,20 --> 00:00:26,70
but it's going to be later on in the course.

12
00:00:26,70 --> 00:00:29,70
We're going to build our our API progressively.

13
00:00:29,70 --> 00:00:34,00
So that means today, no validation, no error responses,

14
00:00:34,00 --> 00:00:35,70
and no real formatting.

15
00:00:35,70 --> 00:00:38,30
We're just going to pass in straight JACE on data

16
00:00:38,30 --> 00:00:40,70
that we're going to we're going to process blindly.

17
00:00:40,70 --> 00:00:42,30
As we've gone up to this point,

18
00:00:42,30 --> 00:00:44,80
we need to first build a route for this request.

19
00:00:44,80 --> 00:00:49,90
Open up the API.php file in the routes folder.

20
00:00:49,90 --> 00:00:56,00
Here on line 21, we'll add Route::post()

21
00:00:56,00 --> 00:00:58,40
to define that we're going to add a new route

22
00:00:58,40 --> 00:01:00,90
that accepts only post requests.

23
00:01:00,90 --> 00:01:03,30
We'll pass in the string polls,

24
00:01:03,30 --> 00:01:06,20
and then for the corresponding controller and action

25
00:01:06,20 --> 00:01:11,80
it'll be PollsController@store for the store method

26
00:01:11,80 --> 00:01:13,30
in the polls controller.

27
00:01:13,30 --> 00:01:17,30
We'll now open up our polls controller in app,

28
00:01:17,30 --> 00:01:22,00
HTTP, Controllers, PollsControler.php.

29
00:01:22,00 --> 00:01:25,20
And we should add a new method on line 20

30
00:01:25,20 --> 00:01:28,50
called public function store().

31
00:01:28,50 --> 00:01:30,60
Now this store method is going to need

32
00:01:30,60 --> 00:01:32,90
to access the request object.

33
00:01:32,90 --> 00:01:34,90
The request object is going to have our

34
00:01:34,90 --> 00:01:37,50
post data stored inside of it.

35
00:01:37,50 --> 00:01:42,00
To do this we'll pass in a parameter of Request

36
00:01:42,00 --> 00:01:44,40
and the variable request.

37
00:01:44,40 --> 00:01:50,20
This request object is our Illuminate Http Request clause

38
00:01:50,20 --> 00:01:52,40
that we defined on line six.

39
00:01:52,40 --> 00:01:59,00
Now on line 22, Poll::create and then we'll call the

40
00:01:59,00 --> 00:02:03,70
request object and it's method all.

41
00:02:03,70 --> 00:02:08,00
Which loads in all of the post data into that create method.

42
00:02:08,00 --> 00:02:11,40
Add a ; of course, and on line 23

43
00:02:11,40 --> 00:02:15,10
we'll return response, guess how this is going to go,

44
00:02:15,10 --> 00:02:19,30
calling json, and then we'll pass in our poll,

45
00:02:19,30 --> 00:02:22,40
and then for our http response code,

46
00:02:22,40 --> 00:02:24,70
it'll be 201.

47
00:02:24,70 --> 00:02:28,00
Now the reason for the 201 is because the 200 is simply

48
00:02:28,00 --> 00:02:30,40
for when everything is fine.

49
00:02:30,40 --> 00:02:32,90
The 201 on the other hand reports that

50
00:02:32,90 --> 00:02:35,80
the request created a new resource.

51
00:02:35,80 --> 00:02:38,00
In this case, our new poll.

52
00:02:38,00 --> 00:02:41,10
Now to create our new poll we'll go to Insomnia,

53
00:02:41,10 --> 00:02:46,50
we'll create a new request called POST POLL.

54
00:02:46,50 --> 00:02:48,80
Our URL for our request will be

55
00:02:48,80 --> 00:03:01,80
http://127.0.0.1:8000/api/polls.

56
00:03:01,80 --> 00:03:05,70
Our http verb for this request will be post.

57
00:03:05,70 --> 00:03:08,80
So we'll have to change it from get to post.

58
00:03:08,80 --> 00:03:13,70
And now we'll add a post json body, so we'll select body.

59
00:03:13,70 --> 00:03:18,50
Select json, add in curly braces, we'll pass in the string

60
00:03:18,50 --> 00:03:22,70
title, : and then the string testing.

61
00:03:22,70 --> 00:03:25,00
And that'll be our post json body.

62
00:03:25,00 --> 00:03:26,90
Now we can try this out.

63
00:03:26,90 --> 00:03:29,20
Remember to start up your API server

64
00:03:29,20 --> 00:03:33,00
if you haven't already with php artisan surf.

65
00:03:33,00 --> 00:03:35,10
And let's send the request.

66
00:03:35,10 --> 00:03:37,20
And the request is going to fail.

67
00:03:37,20 --> 00:03:40,00
Layer bell has this feature called guard

68
00:03:40,00 --> 00:03:42,20
to prevent assigning fields like this.

69
00:03:42,20 --> 00:03:44,90
Where we aren't picking and choosing the fields to set.

70
00:03:44,90 --> 00:03:46,20
We're going to have to change this

71
00:03:46,20 --> 00:03:48,50
by opening up our poll model.

72
00:03:48,50 --> 00:03:51,10
To do this, go back to your code editor

73
00:03:51,10 --> 00:03:55,20
and open up in app, the poll.php file.

74
00:03:55,20 --> 00:04:00,40
And now we need to add on line 9 protected fillable

75
00:04:00,40 --> 00:04:03,50
is going to be equal to, and then all of the fields

76
00:04:03,50 --> 00:04:06,40
that we want to be able to pass into from our API

77
00:04:06,40 --> 00:04:08,60
and be able to save an update.

78
00:04:08,60 --> 00:04:11,90
In this case it'll just be the field title.

79
00:04:11,90 --> 00:04:15,30
We'll save this and we'll go back to Insomnia

80
00:04:15,30 --> 00:04:17,00
and re-run our request.

81
00:04:17,00 --> 00:04:18,60
And now it works.

82
00:04:18,60 --> 00:04:20,60
We see our new poll instance.

83
00:04:20,60 --> 00:04:23,10
If we try sending this another time,

84
00:04:23,10 --> 00:04:26,60
notice this time our ID on our poll is 11

85
00:04:26,60 --> 00:04:28,30
and if we send it a second time,

86
00:04:28,30 --> 00:04:31,00
we'll get an ID of 12.

87
00:04:31,00 --> 00:04:35,30
So even though the name is duplicated, we get a new poll.

88
00:04:35,30 --> 00:04:37,50
Since we don't have any validation logic

89
00:04:37,50 --> 00:04:39,30
this should make sense to you.

90
00:04:39,30 --> 00:04:41,90
Feel free to play around with this a little bit further

91
00:04:41,90 --> 00:04:45,00
but this is basics of how we're going to be able to create

92
00:04:45,00 --> 00:04:47,00
new records with our API.

