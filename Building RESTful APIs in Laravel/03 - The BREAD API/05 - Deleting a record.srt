1
00:00:00,50 --> 00:00:01,60
- [Instructor] Deleting a record

2
00:00:01,60 --> 00:00:03,30
is going to be pretty simple.

3
00:00:03,30 --> 00:00:05,80
We simply need to send a delete request

4
00:00:05,80 --> 00:00:08,10
to the end point with the matching URL

5
00:00:08,10 --> 00:00:09,20
for our poll.

6
00:00:09,20 --> 00:00:12,30
We'll begin like we've done so many times already

7
00:00:12,30 --> 00:00:13,70
with a new route.

8
00:00:13,70 --> 00:00:17,50
We'll open up routes api.php,

9
00:00:17,50 --> 00:00:20,70
and now we'll add a new route on line 23

10
00:00:20,70 --> 00:00:24,20
that will be Route::delete

11
00:00:24,20 --> 00:00:28,60
polls/ and then in a curly braces poll.

12
00:00:28,60 --> 00:00:30,90
Our controller, an action for this,

13
00:00:30,90 --> 00:00:34,90
will be PollsController@delete.

14
00:00:34,90 --> 00:00:36,70
Now to add our controller logic,

15
00:00:36,70 --> 00:00:38,20
we'll open up PollsController

16
00:00:38,20 --> 00:00:43,80
in App Http Controllers PollsController.php.

17
00:00:43,80 --> 00:00:46,20
At the bottom of our controller on line 32

18
00:00:46,20 --> 00:00:49,50
we'll add public function delete

19
00:00:49,50 --> 00:00:54,20
passing in our request and our Poll instance.

20
00:00:54,20 --> 00:00:55,90
Now what we need to do is just call

21
00:00:55,90 --> 00:00:58,40
the delete method on our poll object.

22
00:00:58,40 --> 00:01:03,20
So we'll call poll arrow delete on line 34.

23
00:01:03,20 --> 00:01:05,90
At this point we want to return a null response,

24
00:01:05,90 --> 00:01:07,00
a null response since

25
00:01:07,00 --> 00:01:09,50
there's no longer a poll object to return.

26
00:01:09,50 --> 00:01:13,20
We'll also return this with a 204 status code.

27
00:01:13,20 --> 00:01:16,70
204 is the status code for no response.

28
00:01:16,70 --> 00:01:19,00
This status code is going to let us know

29
00:01:19,00 --> 00:01:22,10
that our resource was successfully deleted.

30
00:01:22,10 --> 00:01:26,80
So we'll return response arrow json

31
00:01:26,80 --> 00:01:30,30
null and then 204.

32
00:01:30,30 --> 00:01:33,10
Now to test this out, recall the start your server

33
00:01:33,10 --> 00:01:35,40
with PHP artisan serve.

34
00:01:35,40 --> 00:01:37,40
We'll open up Insomnia.

35
00:01:37,40 --> 00:01:40,70
We'll duplicate the get poll request.

36
00:01:40,70 --> 00:01:44,00
We'll edit it to be delete poll

37
00:01:44,00 --> 00:01:45,80
for the title of the request.

38
00:01:45,80 --> 00:01:49,30
We'll change our verb here to be delete,

39
00:01:49,30 --> 00:01:51,20
and we'll send our request.

40
00:01:51,20 --> 00:01:53,60
And you'll see we get no response back

41
00:01:53,60 --> 00:01:56,10
with the 204 status code.

42
00:01:56,10 --> 00:01:58,40
Now if we try this a second time

43
00:01:58,40 --> 00:02:01,10
to delete a poll already deleted

44
00:02:01,10 --> 00:02:03,40
we should expect a 404,

45
00:02:03,40 --> 00:02:05,20
and that's exactly what we get.

46
00:02:05,20 --> 00:02:07,20
So inside of these few videos,

47
00:02:07,20 --> 00:02:10,80
we've already created a complete bread API for polls.

48
00:02:10,80 --> 00:02:13,80
We can browse or get all of our polls.

49
00:02:13,80 --> 00:02:17,40
We can read, add, edit, and delete

50
00:02:17,40 --> 00:02:19,30
a single poll at a time.

51
00:02:19,30 --> 00:02:22,30
Most APIs are going to start out this way.

52
00:02:22,30 --> 00:02:25,50
They're going to be simple bread or crud APIs.

53
00:02:25,50 --> 00:02:27,90
The magic for whatever API you're building

54
00:02:27,90 --> 00:02:30,30
is what you're going to do beyond this,

55
00:02:30,30 --> 00:02:33,00
and that's where we're going to start moving into next.

