1
00:00:00,50 --> 00:00:03,70
Now that we've seen how to build a basic API in Laravel,

2
00:00:03,70 --> 00:00:06,70
let's see how to load in subresources.

3
00:00:06,70 --> 00:00:08,70
What's a subresource?

4
00:00:08,70 --> 00:00:11,70
In our case, a subresource is going to be a way

5
00:00:11,70 --> 00:00:14,80
to get the questions related to a poll.

6
00:00:14,80 --> 00:00:17,80
We want to build an endpoint at polls,

7
00:00:17,80 --> 00:00:21,50
forward slash, poll ID, forward slash questions,

8
00:00:21,50 --> 00:00:25,20
to return the questions associated with the poll.

9
00:00:25,20 --> 00:00:27,10
We'll begin like we always do

10
00:00:27,10 --> 00:00:29,70
with the first step of building a route.

11
00:00:29,70 --> 00:00:34,50
So, we'll open the routes file at routes api.php

12
00:00:34,50 --> 00:00:37,90
and we can add under line 26

13
00:00:37,90 --> 00:00:41,40
route, colon colon, get,

14
00:00:41,40 --> 00:00:44,80
and we are going to access polls, forward slash

15
00:00:44,80 --> 00:00:49,70
and then in curly braces poll and then questions.

16
00:00:49,70 --> 00:00:52,30
This will be talking to the polls controller

17
00:00:52,30 --> 00:00:55,90
and the method it will call will be questions.

18
00:00:55,90 --> 00:00:57,60
Now to build our corresponding function

19
00:00:57,60 --> 00:00:59,60
in the polls controller, we'll open up

20
00:00:59,60 --> 00:01:06,20
app, http, controllers, polls controller.php.

21
00:01:06,20 --> 00:01:07,60
All the way on the bottom,

22
00:01:07,60 --> 00:01:13,30
we'll add a new function called public function questions.

23
00:01:13,30 --> 00:01:15,80
This function is going to take two parameters.

24
00:01:15,80 --> 00:01:18,10
The first, of course, is going to be our request,

25
00:01:18,10 --> 00:01:22,10
the second will be a poll instance.

26
00:01:22,10 --> 00:01:24,90
So we have a poll and we have a request.

27
00:01:24,90 --> 00:01:27,70
Now how do we get the questions for our poll?

28
00:01:27,70 --> 00:01:29,50
That's actually pretty easy.

29
00:01:29,50 --> 00:01:32,10
We can call questions, is going

30
00:01:32,10 --> 00:01:36,30
to be equal to poll, arrow, questions.

31
00:01:36,30 --> 00:01:38,20
If you've used Laravel in the past,

32
00:01:38,20 --> 00:01:40,50
you know that the poll model has access

33
00:01:40,50 --> 00:01:43,30
to the ability to get this subresources,

34
00:01:43,30 --> 00:01:45,50
in this case, our questions.

35
00:01:45,50 --> 00:01:49,60
Now on line 60, add return, response,

36
00:01:49,60 --> 00:01:56,50
arrow, json, and we'll return our questions and a 200.

37
00:01:56,50 --> 00:01:58,90
And now we can open up Insomnia,

38
00:01:58,90 --> 00:02:02,50
and we'll want to edit our get single poll

39
00:02:02,50 --> 00:02:06,00
and we'll add questions to the end of our URL.

40
00:02:06,00 --> 00:02:10,20
We'll send this request, and we'll get nothing, actually.

41
00:02:10,20 --> 00:02:11,70
Now, why is that?

42
00:02:11,70 --> 00:02:13,60
If you know Laravel, you should know the answer

43
00:02:13,60 --> 00:02:14,50
at this point.

44
00:02:14,50 --> 00:02:17,00
We didn't yet define any relationships

45
00:02:17,00 --> 00:02:19,20
in our Laravel model.

46
00:02:19,20 --> 00:02:21,80
So let's go back to our code editor

47
00:02:21,80 --> 00:02:26,70
and we'll open up our poll class in app, poll.php,

48
00:02:26,70 --> 00:02:29,20
and we need to add a new relationship

49
00:02:29,20 --> 00:02:32,20
for our questions model.

50
00:02:32,20 --> 00:02:34,40
So we'll add a new function,

51
00:02:34,40 --> 00:02:38,30
that's public function, questions,

52
00:02:38,30 --> 00:02:42,90
and then here we'll have it return, this, arrow,

53
00:02:42,90 --> 00:02:48,40
has many, app, backslash, question.

54
00:02:48,40 --> 00:02:49,50
Save this.

55
00:02:49,50 --> 00:02:51,20
Let's go back to Insomnia

56
00:02:51,20 --> 00:02:53,30
and try it out one more time.

57
00:02:53,30 --> 00:02:55,80
And there, we get all of our questions,

58
00:02:55,80 --> 00:02:59,80
notice with our poll ID is equal to 4.

59
00:02:59,80 --> 00:03:03,90
So now we built out an API to be able to get subresources

60
00:03:03,90 --> 00:03:05,70
off of that API.

61
00:03:05,70 --> 00:03:08,60
We can talk to a poll and get the questions related

62
00:03:08,60 --> 00:03:10,00
to that poll.

