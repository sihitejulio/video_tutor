1
00:00:00,50 --> 00:00:02,30
- [Instructor] Data transformers,

2
00:00:02,30 --> 00:00:05,50
they give us the ability to manipulate our responses.

3
00:00:05,50 --> 00:00:07,30
We can do something as simple as simply

4
00:00:07,30 --> 00:00:10,50
capitalize all the letters in an abbreviation,

5
00:00:10,50 --> 00:00:13,20
or we can ensure we don't reveal sensitive information,

6
00:00:13,20 --> 00:00:15,30
like a password,

7
00:00:15,30 --> 00:00:18,20
or even perform some more challenging tasks,

8
00:00:18,20 --> 00:00:21,00
like ensuring data matches some prescribed format

9
00:00:21,00 --> 00:00:22,50
or standard.

10
00:00:22,50 --> 00:00:25,60
Laravel provides us a suite of transformation tools

11
00:00:25,60 --> 00:00:29,60
for our APIs, using a class called resource.

12
00:00:29,60 --> 00:00:34,10
We'll begin by making a resource class for the poll model.

13
00:00:34,10 --> 00:00:36,50
We'll want to open up our terminal application

14
00:00:36,50 --> 00:00:39,80
and inside of the directory, where our API lives,

15
00:00:39,80 --> 00:00:46,60
run the command php artisan make:resource

16
00:00:46,60 --> 00:00:49,40
capital P for Poll.

17
00:00:49,40 --> 00:00:53,30
This creates a resource folder in the http directory

18
00:00:53,30 --> 00:00:55,20
with a poll class.

19
00:00:55,20 --> 00:00:57,00
Let's open up that now.

20
00:00:57,00 --> 00:00:59,00
We'll go to our code editor

21
00:00:59,00 --> 00:01:06,00
and we'll open up app, Http, Resources, Poll.php.

22
00:01:06,00 --> 00:01:08,60
You'll see it currently has a two array method

23
00:01:08,60 --> 00:01:10,40
that just calls the parent method,

24
00:01:10,40 --> 00:01:13,70
which in this case doesn't change our response in any way.

25
00:01:13,70 --> 00:01:16,50
Now what we're going to do is we're going to replace

26
00:01:16,50 --> 00:01:19,00
this return parent::toArrary method

27
00:01:19,00 --> 00:01:21,50
with just call it return

28
00:01:21,50 --> 00:01:23,50
and then we'll return an array

29
00:01:23,50 --> 00:01:26,80
and we'll return a key value pair.

30
00:01:26,80 --> 00:01:28,60
In this case our key will be title

31
00:01:28,60 --> 00:01:35,70
and our value will mb_strimwidth

32
00:01:35,70 --> 00:01:38,50
to return just the width of the string

33
00:01:38,50 --> 00:01:40,70
that's going to be five characters long.

34
00:01:40,70 --> 00:01:44,00
So we're going to trim our title to be five characters long.

35
00:01:44,00 --> 00:01:48,00
Our string is going to be this arrow title,

36
00:01:48,00 --> 00:01:51,70
because our this object is going to be the instance

37
00:01:51,70 --> 00:01:53,20
of our poll class.

38
00:01:53,20 --> 00:01:59,10
The start point will be zero, the width of it will be five

39
00:01:59,10 --> 00:02:02,90
and then we'll add in ellipses to show that it's longer

40
00:02:02,90 --> 00:02:05,50
than these first few five characters.

41
00:02:05,50 --> 00:02:08,90
So we'll add in as our fourth parameter to this,

42
00:02:08,90 --> 00:02:11,20
an ellipse of three periods.

43
00:02:11,20 --> 00:02:15,50
So this say to trim our title to five characters long

44
00:02:15,50 --> 00:02:17,80
using an ellipse to show that it's longer than

45
00:02:17,80 --> 00:02:20,60
the couple characters that we're going to see.

46
00:02:20,60 --> 00:02:23,00
So how do we use this transformer in our API.

47
00:02:23,00 --> 00:02:27,10
Well begin by opening up your polls controller class

48
00:02:27,10 --> 00:02:33,10
in app, Http, Controllers, PollsController.php.

49
00:02:33,10 --> 00:02:38,40
We now need to replace our return response json on line 22

50
00:02:38,40 --> 00:02:42,10
to first call in our poll resource class.

51
00:02:42,10 --> 00:02:46,50
So line 22, we'll erase the return response json

52
00:02:46,50 --> 00:02:50,70
and we'll instead set response is going to be equal to

53
00:02:50,70 --> 00:02:53,90
new PollResource.

54
00:02:53,90 --> 00:02:56,50
This says to get the poll, transform it

55
00:02:56,50 --> 00:02:58,60
using the PollResource class

56
00:02:58,60 --> 00:03:01,00
and store that transformed response

57
00:03:01,00 --> 00:03:03,30
as a response variable.

58
00:03:03,30 --> 00:03:05,90
On line 23, we can then return that response

59
00:03:05,90 --> 00:03:08,70
the same way we've done all these other times

60
00:03:08,70 --> 00:03:18,10
with return response arrow json response 200.

61
00:03:18,10 --> 00:03:20,70
Now we also need to note that we need to use

62
00:03:20,70 --> 00:03:22,40
our Poll Resource class.

63
00:03:22,40 --> 00:03:25,20
Now we'll also add on line six,

64
00:03:25,20 --> 00:03:34,20
use App\Http\Resources\Poll as PollResource.

65
00:03:34,20 --> 00:03:37,10
We can save this and now we need to start our

66
00:03:37,10 --> 00:03:40,90
php artisan server by going to our terminal

67
00:03:40,90 --> 00:03:44,40
and running php artisan serve.

68
00:03:44,40 --> 00:03:46,20
Now we can go to Insomnia

69
00:03:46,20 --> 00:03:49,20
and now we need to run our show method.

70
00:03:49,20 --> 00:03:51,90
In this case it's our Get Poll request.

71
00:03:51,90 --> 00:03:55,70
So we'll run our Get Poll request.

72
00:03:55,70 --> 00:03:57,80
And this case it's going to return a 404

73
00:03:57,80 --> 00:04:00,70
because we deleted the poll with id 11.

74
00:04:00,70 --> 00:04:04,40
So instead, let's try it with id four.

75
00:04:04,40 --> 00:04:05,60
We'll send this request

76
00:04:05,60 --> 00:04:10,10
and notice we only get two characters of our title

77
00:04:10,10 --> 00:04:11,60
and then we get the ellipses.

78
00:04:11,60 --> 00:04:14,30
You'll note we also don't see any other fields

79
00:04:14,30 --> 00:04:16,60
in our data response.

80
00:04:16,60 --> 00:04:19,90
You can repeat this process in any number of ways

81
00:04:19,90 --> 00:04:23,70
to modify the data before passing it out of your API

82
00:04:23,70 --> 00:04:27,40
to ensure you only display either the fields desired

83
00:04:27,40 --> 00:04:30,20
or to ensure that the data matches

84
00:04:30,20 --> 00:04:32,90
some particular standard or format.

85
00:04:32,90 --> 00:04:36,20
Feel free to play around with this resource class.

86
00:04:36,20 --> 00:04:37,80
It should be pretty easy for you

87
00:04:37,80 --> 00:04:40,20
to manipulate as you desire.

88
00:04:40,20 --> 00:04:44,20
One tricky thing to note here is that the resource class

89
00:04:44,20 --> 00:04:47,30
only works on a single instance of polls.

90
00:04:47,30 --> 00:04:51,50
To manipulate a collection of polls, or other objects,

91
00:04:51,50 --> 00:04:54,50
you have to create a resource collection class

92
00:04:54,50 --> 00:04:57,60
to add metadata about the collection of objects.

93
00:04:57,60 --> 00:05:00,40
The pattern is almost exactly the same,

94
00:05:00,40 --> 00:05:02,20
but it's important to be aware of this

95
00:05:02,20 --> 00:05:04,00
at this point in time.

96
00:05:04,00 --> 00:05:05,80
We're not going to see it for a bit,

97
00:05:05,80 --> 00:05:08,20
but we will use a resource collection

98
00:05:08,20 --> 00:05:10,00
later on in the course.

