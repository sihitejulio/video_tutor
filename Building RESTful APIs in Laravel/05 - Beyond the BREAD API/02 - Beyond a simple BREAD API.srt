1
00:00:00,60 --> 00:00:02,60
- [Narrator] Now that we've stepped piece by piece

2
00:00:02,60 --> 00:00:05,70
to build a basic API for a single resource,

3
00:00:05,70 --> 00:00:07,50
in our case polls.

4
00:00:07,50 --> 00:00:10,90
Let's see the very fast way to build an API with Layer Bell.

5
00:00:10,90 --> 00:00:12,60
This time for questions.

6
00:00:12,60 --> 00:00:16,20
After all, every poll needs some questions to answer.

7
00:00:16,20 --> 00:00:18,40
So open up your terminal application

8
00:00:18,40 --> 00:00:24,40
and run the command php artisan make:controller

9
00:00:24,40 --> 00:00:34,00
QuestionsController --resource -- model=Question

10
00:00:34,00 --> 00:00:36,60
from the root of your Layer Bell application.

11
00:00:36,60 --> 00:00:39,70
This command is going to build us a questions controller

12
00:00:39,70 --> 00:00:42,60
with all the routes we need to handle questions.

13
00:00:42,60 --> 00:00:45,00
At this point we need to add of course,

14
00:00:45,00 --> 00:00:46,70
the routes for the API.

15
00:00:46,70 --> 00:00:50,50
To do so we need to first open up our text editor.

16
00:00:50,50 --> 00:00:51,90
So let's open that up.

17
00:00:51,90 --> 00:00:57,20
And then open our routes file at routes, api.php.

18
00:00:57,20 --> 00:00:58,70
And now we can add a new line.

19
00:00:58,70 --> 00:01:06,80
Route::apiResource and then the string questions

20
00:01:06,80 --> 00:01:09,20
and then for the controller that this will answer

21
00:01:09,20 --> 00:01:11,90
QuestionsController.

22
00:01:11,90 --> 00:01:15,50
This says to build an API managed end point for questions.

23
00:01:15,50 --> 00:01:17,90
Using the questions controller given

24
00:01:17,90 --> 00:01:20,60
a Layer Bell standard resource controller.

25
00:01:20,60 --> 00:01:23,20
Which is what we built using make.

26
00:01:23,20 --> 00:01:25,40
So we built a controller and we built it's

27
00:01:25,40 --> 00:01:28,40
corresponding routes in just a minute or two.

28
00:01:28,40 --> 00:01:32,80
Let's go back to our terminal and we need to run the command

29
00:01:32,80 --> 00:01:35,80
php artisan serve.

30
00:01:35,80 --> 00:01:38,00
And we'll see what exactly we get from here.

31
00:01:38,00 --> 00:01:43,40
Let's open up Insomnia and duplicate the get polls request

32
00:01:43,40 --> 00:01:48,90
we'll rename it to get questions, and rather than

33
00:01:48,90 --> 00:01:53,60
accessing api polls, we'll instead access api questions.

34
00:01:53,60 --> 00:01:55,40
We'll send this request,

35
00:01:55,40 --> 00:01:58,00
and we run it and we get back bupkus.

36
00:01:58,00 --> 00:02:00,10
Well what's going on here?

37
00:02:00,10 --> 00:02:02,60
So where should we begin?

38
00:02:02,60 --> 00:02:05,20
The controller seems like a good place to look.

39
00:02:05,20 --> 00:02:07,60
Let's open up the questions controller,

40
00:02:07,60 --> 00:02:11,10
by going to our text editor and opening up app,

41
00:02:11,10 --> 00:02:16,30
http, controllers, QuestionsController.php.

42
00:02:16,30 --> 00:02:18,40
And look at that, there is no actual code

43
00:02:18,40 --> 00:02:20,40
in our controller methods.

44
00:02:20,40 --> 00:02:24,30
Layer Bell's make artisan command creates the basic

45
00:02:24,30 --> 00:02:26,50
structure of the controller, but doesn't actually

46
00:02:26,50 --> 00:02:28,10
populate the controller with the

47
00:02:28,10 --> 00:02:30,50
actual logic for each of the methods.

48
00:02:30,50 --> 00:02:33,20
That logic is still left up to you to add.

49
00:02:33,20 --> 00:02:35,40
So we're going to have to do a little bit of work,

50
00:02:35,40 --> 00:02:37,40
but don't worry it'll go pretty fast.

51
00:02:37,40 --> 00:02:39,10
We now know how to do this all.

52
00:02:39,10 --> 00:02:43,50
So first on line 17 we'll replace the comment with

53
00:02:43,50 --> 00:02:51,80
return response ->json(question::get(),)

54
00:02:51,80 --> 00:02:55,30
and then 200 for the response code that we'll return.

55
00:02:55,30 --> 00:02:58,40
We'll save this and that's pretty much it.

56
00:02:58,40 --> 00:03:00,80
Now we just built out our index function

57
00:03:00,80 --> 00:03:04,20
and now we can add in our show function.

58
00:03:04,20 --> 00:03:06,70
So we'll go down to line 49.

59
00:03:06,70 --> 00:03:09,50
We'll replace the comment on line 49 with

60
00:03:09,50 --> 00:03:17,90
return response () ->json($question,) and then 200 again.

61
00:03:17,90 --> 00:03:20,00
Inside of our store function

62
00:03:20,00 --> 00:03:23,00
we'll need to replace line 38 with

63
00:03:23,00 --> 00:03:37,70
return response()->json(question::create($request->all()))

64
00:03:37,70 --> 00:03:41,00
and then for the response code will be 201

65
00:03:41,00 --> 00:03:44,30
since we're creating a new question here.

66
00:03:44,30 --> 00:03:47,30
Notice we're not going to add any validation at this point.

67
00:03:47,30 --> 00:03:49,70
We're just going to blindly add our questions.

68
00:03:49,70 --> 00:03:52,10
Again something you want to customize but for now

69
00:03:52,10 --> 00:03:53,90
we'll leave it alone.

70
00:03:53,90 --> 00:03:58,70
Finally we can update our update method on line 72.

71
00:03:58,70 --> 00:04:00,70
And this will read as

72
00:04:00,70 --> 00:04:10,00
return response->jason($question->update)

73
00:04:10,00 --> 00:04:11,80
so we're going to update the instance of question.

74
00:04:11,80 --> 00:04:16,70
We're going to pass in the request->all

75
00:04:16,70 --> 00:04:20,50
and then of course our response code will be a 200.

76
00:04:20,50 --> 00:04:23,60
Finally we need to build out our destroy method.

77
00:04:23,60 --> 00:04:26,20
This is also going to be a one line method.

78
00:04:26,20 --> 00:04:29,00
On line 83, we'll replace it with

79
00:04:29,00 --> 00:04:37,50
return response->json($question->delete)

80
00:04:37,50 --> 00:04:40,20
and then 204 for the response code.

81
00:04:40,20 --> 00:04:42,10
We've one last thing we need to do

82
00:04:42,10 --> 00:04:43,80
to get this really working.

83
00:04:43,80 --> 00:04:47,40
We need to be able to add or edit our fields in a question.

84
00:04:47,40 --> 00:04:51,50
We need to update the fillable fields list in the model.

85
00:04:51,50 --> 00:04:57,20
So open up the question model, in app, and question.php.

86
00:04:57,20 --> 00:04:59,60
And on line nine, we'll add

87
00:04:59,60 --> 00:05:05,00
protected $fillable = an empty array.

88
00:05:05,00 --> 00:05:09,30
Inside of this array we'll pass in the keys for the fields

89
00:05:09,30 --> 00:05:12,40
that we want to be able to fill in from our API.

90
00:05:12,40 --> 00:05:20,60
In this case title, question, and poll_id.

91
00:05:20,60 --> 00:05:22,10
Now save this.

92
00:05:22,10 --> 00:05:24,00
We'll go back to Insomnia,

93
00:05:24,00 --> 00:05:28,60
and we'll re-run our API for getting questions.

94
00:05:28,60 --> 00:05:31,00
And we'll see that indeed it does work.

95
00:05:31,00 --> 00:05:34,10
Now feel free to play around inside of Insomnia

96
00:05:34,10 --> 00:05:36,00
and you'll see that the rest of the question API

97
00:05:36,00 --> 00:05:39,60
we just built and you can verify that all of it works.

98
00:05:39,60 --> 00:05:42,50
After this we're going to improve the API

99
00:05:42,50 --> 00:05:46,00
by providing a way to deal with sub-resources.

