1
00:00:00,60 --> 00:00:03,00
- [Instructor] There are a variety of ways to test our API

2
00:00:03,00 --> 00:00:06,00
but in this case we're going to use a tool called Behat

3
00:00:06,00 --> 00:00:09,40
which will permit us to write what we call behavior tests

4
00:00:09,40 --> 00:00:13,10
or tests that test a defined behavior of our software.

5
00:00:13,10 --> 00:00:16,10
In this way we can test a specific API call

6
00:00:16,10 --> 00:00:18,80
and verify it works as we expect.

7
00:00:18,80 --> 00:00:21,50
To begin we'll need to install BeHat.

8
00:00:21,50 --> 00:00:23,60
We'll open up our Terminal application

9
00:00:23,60 --> 00:00:26,10
and inside of our community-poll directory

10
00:00:26,10 --> 00:00:28,70
we'll run the command composer

11
00:00:28,70 --> 00:00:29,90
require

12
00:00:29,90 --> 00:00:31,50
- -dev

13
00:00:31,50 --> 00:00:32,80
because this is only going to be a

14
00:00:32,80 --> 00:00:35,20
development level composer requirement

15
00:00:35,20 --> 00:00:39,70
and behat/behat.

16
00:00:39,70 --> 00:00:46,60
Now we can run the command vendor/bin/behat --init.

17
00:00:46,60 --> 00:00:49,20
This command will set up BeHat to run in our project

18
00:00:49,20 --> 00:00:51,50
including creating a features directory

19
00:00:51,50 --> 00:00:54,10
where we'll write feature tests.

20
00:00:54,10 --> 00:00:58,60
Notice I'm running vendor/bin/behat.

21
00:00:58,60 --> 00:01:00,20
If you're on Windows it'll need to be

22
00:01:00,20 --> 00:01:03,80
vendor\bin\behat.

23
00:01:03,80 --> 00:01:06,20
Now to begin pretty fast we want to be able

24
00:01:06,20 --> 00:01:08,50
to get into our first BeHat test.

25
00:01:08,50 --> 00:01:10,90
We'll write some logic to manage our tests.

26
00:01:10,90 --> 00:01:13,30
This is called a feature context

27
00:01:13,30 --> 00:01:17,00
which is used to handle the roles of our BeHat test.

28
00:01:17,00 --> 00:01:19,40
Inside of the exercise files included in

29
00:01:19,40 --> 00:01:22,50
this project is a snippets.txt

30
00:01:22,50 --> 00:01:23,30
file.

31
00:01:23,30 --> 00:01:25,00
You want to open that now,

32
00:01:25,00 --> 00:01:28,60
copy all the contents of the snippets.txt file,

33
00:01:28,60 --> 00:01:30,60
open up your text editor,

34
00:01:30,60 --> 00:01:34,10
and open up the file located in features,

35
00:01:34,10 --> 00:01:37,00
bootstrap, FeatureContext.php

36
00:01:37,00 --> 00:01:41,70
and paste in the contents from your snippets.txt file here.

37
00:01:41,70 --> 00:01:45,30
This file is our basic work to query our API,

38
00:01:45,30 --> 00:01:46,90
verify we get a response,

39
00:01:46,90 --> 00:01:49,80
and that the response is a JSON document.

40
00:01:49,80 --> 00:01:52,90
We'll write some new logic for it in the later videos

41
00:01:52,90 --> 00:01:54,90
but for now this will at least let us test

42
00:01:54,90 --> 00:01:58,10
and verify that we can in fact write a test.

43
00:01:58,10 --> 00:01:59,90
One change you'll need to make

44
00:01:59,90 --> 00:02:01,90
on the top of this file

45
00:02:01,90 --> 00:02:03,30
at line 21

46
00:02:03,30 --> 00:02:05,30
is where we set up the bearerToken

47
00:02:05,30 --> 00:02:08,40
to be able to authenticate to our API.

48
00:02:08,40 --> 00:02:10,40
You want to open up Insomnia

49
00:02:10,40 --> 00:02:12,00
and copy out the bearerToken that

50
00:02:12,00 --> 00:02:16,20
you passed in for your authentication in the last video.

51
00:02:16,20 --> 00:02:18,40
Now let's write a simple test.

52
00:02:18,40 --> 00:02:21,70
We'll create a new file in the features directory.

53
00:02:21,70 --> 00:02:26,50
We'll label this file basic.feature.

54
00:02:26,50 --> 00:02:28,30
At the top of the file on line one

55
00:02:28,30 --> 00:02:33,40
we're going to add feature colon Sample Tests.

56
00:02:33,40 --> 00:02:36,00
This says what feature we're going to be testing.

57
00:02:36,00 --> 00:02:39,30
In this case we're going to test the feature Sample Tests.

58
00:02:39,30 --> 00:02:40,90
After all we just want to prove we

59
00:02:40,90 --> 00:02:43,20
can get some tests working here.

60
00:02:43,20 --> 00:02:45,20
On line two add

61
00:02:45,20 --> 00:02:46,70
in order

62
00:02:46,70 --> 00:02:48,00
to test

63
00:02:48,00 --> 00:02:49,90
the API.

64
00:02:49,90 --> 00:02:54,30
This in order line defines what we're testing basically.

65
00:02:54,30 --> 00:02:56,20
Line three is going to look pretty similar

66
00:02:56,20 --> 00:02:57,50
with I

67
00:02:57,50 --> 00:02:58,30
need

68
00:02:58,30 --> 00:02:59,30
to be able

69
00:02:59,30 --> 00:03:00,40
to test

70
00:03:00,40 --> 00:03:02,00
the API.

71
00:03:02,00 --> 00:03:04,70
Now most of this is pretty basic stuff of BeHat

72
00:03:04,70 --> 00:03:07,10
and mostly I want us to be able to get a test going.

73
00:03:07,10 --> 00:03:10,20
So we'll ignore a lot of what this stuff is.

74
00:03:10,20 --> 00:03:12,40
If you want to look further into this you can

75
00:03:12,40 --> 00:03:13,90
and you probably should to see how to

76
00:03:13,90 --> 00:03:15,60
write tests that give you a closer to

77
00:03:15,60 --> 00:03:17,80
English level approach to tests

78
00:03:17,80 --> 00:03:19,30
as well as the ability to write tests

79
00:03:19,30 --> 00:03:21,70
that cover a suite of features of your code

80
00:03:21,70 --> 00:03:24,30
as opposed to testing just a controller action

81
00:03:24,30 --> 00:03:26,00
or a single method.

82
00:03:26,00 --> 00:03:27,40
Mostly what I'm trying to do

83
00:03:27,40 --> 00:03:29,30
is to demonstrate the ease at which

84
00:03:29,30 --> 00:03:31,70
you can test the entire feature of an API

85
00:03:31,70 --> 00:03:33,10
as opposed to just testing

86
00:03:33,10 --> 00:03:35,70
the controller methods themselves.

87
00:03:35,70 --> 00:03:37,50
Now we'll add a blank line

88
00:03:37,50 --> 00:03:38,50
and another blank line

89
00:03:38,50 --> 00:03:41,60
and on line five we'll add a scenario.

90
00:03:41,60 --> 00:03:43,80
A scenario is going to be our actual

91
00:03:43,80 --> 00:03:45,20
test that we're going to run.

92
00:03:45,20 --> 00:03:47,60
In this case we'll do scenario colon

93
00:03:47,60 --> 00:03:49,60
get questions.

94
00:03:49,60 --> 00:03:51,40
So we're testing a feature.

95
00:03:51,40 --> 00:03:54,50
In this case our feature is called Sample Tests.

96
00:03:54,50 --> 00:03:57,40
We're building up a scenario to test that feature.

97
00:03:57,40 --> 00:03:59,80
In this case get questions.

98
00:03:59,80 --> 00:04:02,90
Now we're going to define the scenario on line six

99
00:04:02,90 --> 00:04:05,20
and we're going to add in given

100
00:04:05,20 --> 00:04:06,20
I have

101
00:04:06,20 --> 00:04:07,90
the payload

102
00:04:07,90 --> 00:04:09,20
colon

103
00:04:09,20 --> 00:04:11,80
and our payload in this case for getting questions

104
00:04:11,80 --> 00:04:14,00
is going to be just an empty payload.

105
00:04:14,00 --> 00:04:17,90
So we'll add three double quotes on line seven

106
00:04:17,90 --> 00:04:20,50
and three double quotes on line eight.

107
00:04:20,50 --> 00:04:22,80
And on line nine we'll write out

108
00:04:22,80 --> 00:04:24,00
when

109
00:04:24,00 --> 00:04:25,10
I

110
00:04:25,10 --> 00:04:26,50
request

111
00:04:26,50 --> 00:04:27,40
and then in quotes

112
00:04:27,40 --> 00:04:32,50
GET /api/questions.

113
00:04:32,50 --> 00:04:34,60
Notice this follows the format of the

114
00:04:34,60 --> 00:04:37,00
feature context file we filled in.

115
00:04:37,00 --> 00:04:39,40
If we look back at that file

116
00:04:39,40 --> 00:04:42,90
we'll notice when we scroll down to line 35

117
00:04:42,90 --> 00:04:47,30
we have an iRequest passing in an HTTP method

118
00:04:47,30 --> 00:04:50,10
and then some argument that it's going to request.

119
00:04:50,10 --> 00:04:52,60
Correspondingly, on line 55,

120
00:04:52,60 --> 00:04:55,10
we have IGetAResponse.

121
00:04:55,10 --> 00:04:58,20
65, theResponseIsJson.

122
00:04:58,20 --> 00:05:00,50
Notice how close this is to English.

123
00:05:00,50 --> 00:05:03,40
We format our feature test in English language

124
00:05:03,40 --> 00:05:06,90
and then write PHP code to turn that English language

125
00:05:06,90 --> 00:05:09,60
into an actual functional test.

126
00:05:09,60 --> 00:05:11,50
Let's go back to the basic feature

127
00:05:11,50 --> 00:05:13,40
and we'll finish this out.

128
00:05:13,40 --> 00:05:14,50
On line 10

129
00:05:14,50 --> 00:05:15,30
add

130
00:05:15,30 --> 00:05:16,20
then

131
00:05:16,20 --> 00:05:17,00
the

132
00:05:17,00 --> 00:05:19,10
response is JSON.

133
00:05:19,10 --> 00:05:23,50
So we're ensuring that we get a JSON response from our API.

134
00:05:23,50 --> 00:05:25,20
Now at this point we just want to verify

135
00:05:25,20 --> 00:05:26,60
we get JSON back.

136
00:05:26,60 --> 00:05:28,70
That means we know the API is working

137
00:05:28,70 --> 00:05:31,00
and we can talk to it as we expect.

138
00:05:31,00 --> 00:05:32,80
We'll save all this,

139
00:05:32,80 --> 00:05:35,20
we'll go back to our Terminal application,

140
00:05:35,20 --> 00:05:39,00
and now we can run the command vendor/bin/.

141
00:05:39,00 --> 00:05:40,50
Remember if you're on Windows it's

142
00:05:40,50 --> 00:05:43,50
going to be backlash not forward slash.

143
00:05:43,50 --> 00:05:44,50
Behat.

144
00:05:44,50 --> 00:05:46,30
And my test failed.

145
00:05:46,30 --> 00:05:48,30
Notice what error I get back here.

146
00:05:48,30 --> 00:05:50,40
I get back a cURL error seven

147
00:05:50,40 --> 00:05:56,50
because I failed to connect to one 127.0.0.1 port 8000.

148
00:05:56,50 --> 00:05:57,90
That probably means that we didn't

149
00:05:57,90 --> 00:05:59,90
start up our application yet.

150
00:05:59,90 --> 00:06:01,40
We'll need to open up a new

151
00:06:01,40 --> 00:06:04,70
window in our Terminal application,

152
00:06:04,70 --> 00:06:08,30
change directory to our community poll application,

153
00:06:08,30 --> 00:06:11,70
start up our Artisan server,

154
00:06:11,70 --> 00:06:14,00
and now we can re-run our tests.

155
00:06:14,00 --> 00:06:16,20
And we see here our test passed.

156
00:06:16,20 --> 00:06:18,30
Notice we execute a feature

157
00:06:18,30 --> 00:06:20,10
and then we execute a corresponding

158
00:06:20,10 --> 00:06:22,20
scenario for that feature.

159
00:06:22,20 --> 00:06:24,20
It tells us that our scenario passed

160
00:06:24,20 --> 00:06:27,00
and that there were three steps in our scenario.

161
00:06:27,00 --> 00:06:29,90
This is a fairly simple and basic BeHat test.

162
00:06:29,90 --> 00:06:32,90
We just verified that we were able to talk to the API

163
00:06:32,90 --> 00:06:35,20
and that we got back JSON.

164
00:06:35,20 --> 00:06:36,80
Next we're going to actually write

165
00:06:36,80 --> 00:06:38,20
some real logic to this

166
00:06:38,20 --> 00:06:39,70
so you can learn to write more of

167
00:06:39,70 --> 00:06:43,00
your own complex tests for your API.

