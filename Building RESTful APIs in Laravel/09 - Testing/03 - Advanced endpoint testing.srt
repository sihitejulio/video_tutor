1
00:00:00,70 --> 00:00:03,10
- [Narrator] Our final behat test is going to submit

2
00:00:03,10 --> 00:00:06,50
a new question and ensure it is added properly.

3
00:00:06,50 --> 00:00:10,50
Let's open up our basic.feature file located in the

4
00:00:10,50 --> 00:00:14,50
features directory, and we're going to copy the lines

5
00:00:14,50 --> 00:00:16,90
for the Scenario: Get Questions.

6
00:00:16,90 --> 00:00:19,80
Which is lines 5 through 11.

7
00:00:19,80 --> 00:00:24,70
We'll then paste these lines as lines 13 through 19.

8
00:00:24,70 --> 00:00:28,20
Let's edit the title of this scenario from Get Questions

9
00:00:28,20 --> 00:00:32,80
on line 13 to be Add Question.

10
00:00:32,80 --> 00:00:34,80
We're still going to be passing a payload

11
00:00:34,80 --> 00:00:38,60
only this time our payload will have a real payload in it.

12
00:00:38,60 --> 00:00:40,80
We're going to pass a json object.

13
00:00:40,80 --> 00:00:47,00
So on lines 16 we'll add curly braces for our json object.

14
00:00:47,00 --> 00:00:48,30
We'll add three keys.

15
00:00:48,30 --> 00:00:51,40
A title, a question, and a poll ID.

16
00:00:51,40 --> 00:00:56,90
So first on line 17 we'll add a title key.

17
00:00:56,90 --> 00:00:59,90
We'll set the title value to be Behat

18
00:00:59,90 --> 00:01:01,80
and add a comma at the end.

19
00:01:01,80 --> 00:01:08,40
On line 19 we'll add a question, colon, and we'll pass in

20
00:01:08,40 --> 00:01:13,70
the question value of "is it awesome?"

21
00:01:13,70 --> 00:01:15,90
and then another comma.

22
00:01:15,90 --> 00:01:22,10
Finally on line 19 we'll add in the poll ID, colon,

23
00:01:22,10 --> 00:01:26,30
and then we'll pass in a poll ID of the integer two.

24
00:01:26,30 --> 00:01:29,80
We don't need a closing comma here

25
00:01:29,80 --> 00:01:31,80
because we're doing a json object

26
00:01:31,80 --> 00:01:33,90
which doesn't have a closing comma.

27
00:01:33,90 --> 00:01:36,10
And this is also an integer two

28
00:01:36,10 --> 00:01:37,50
not the string two.

29
00:01:37,50 --> 00:01:40,60
So we don't need to enclose it in our double quotes.

30
00:01:40,60 --> 00:01:43,30
Now we'll edit line 22 for the request

31
00:01:43,30 --> 00:01:48,10
to be a post request rather than a get request.

32
00:01:48,10 --> 00:01:51,20
After we set up our request to be a post request

33
00:01:51,20 --> 00:01:53,30
we're going to need to edit the line for that

34
00:01:53,30 --> 00:01:55,40
the response contains a question.

35
00:01:55,40 --> 00:01:59,10
Instead what we're going to do is ensure that our question

36
00:01:59,10 --> 00:02:00,60
contains a title.

37
00:02:00,60 --> 00:02:02,90
And in this case we want to ensure the title matches

38
00:02:02,90 --> 00:02:05,80
our value that we pass in, Behat.

39
00:02:05,80 --> 00:02:07,60
So we'll just write this out.

40
00:02:07,60 --> 00:02:13,60
So line 24 will turn into then the question contains a title

41
00:02:13,60 --> 00:02:17,80
of and then in double quotes our value, Behat.

42
00:02:17,80 --> 00:02:20,40
We can swap over to our terminal application now

43
00:02:20,40 --> 00:02:22,50
and run our Behat command again

44
00:02:22,50 --> 00:02:28,90
with vendor/bin/behat.

45
00:02:28,90 --> 00:02:31,40
Now it asks us to choose the context to generate the

46
00:02:31,40 --> 00:02:32,80
snippet for this file.

47
00:02:32,80 --> 00:02:35,50
And in this case we'll again choose our context

48
00:02:35,50 --> 00:02:38,70
of one for our feature context.

49
00:02:38,70 --> 00:02:41,80
We'll copy this snippet of code that it generates for us,

50
00:02:41,80 --> 00:02:44,20
go back over to our text editor,

51
00:02:44,20 --> 00:02:47,70
and open up our FeatureContext.php file.

52
00:02:47,70 --> 00:02:50,90
We'll scroll down to the bottom and paste this in

53
00:02:50,90 --> 00:02:53,30
at lines 88 and below.

54
00:02:53,30 --> 00:02:55,20
Now what we need to do at this point,

55
00:02:55,20 --> 00:02:58,20
is we'll need to decode our response body again.

56
00:02:58,20 --> 00:03:05,60
So we can copy line 79 and paste it in as line 93.

57
00:03:05,60 --> 00:03:10,00
And then all we need to do is verify that our title

58
00:03:10,00 --> 00:03:13,20
matches the data title property.

59
00:03:13,20 --> 00:03:24,30
So we'll add in if data->title==arg1

60
00:03:24,30 --> 00:03:28,70
everything's fine.

61
00:03:28,70 --> 00:03:33,60
Else we want to through our exception.

62
00:03:33,60 --> 00:03:38,00
So we'll copy our throw new exception line from 84

63
00:03:38,00 --> 00:03:40,60
and paste it in on line 98.

64
00:03:40,60 --> 00:03:44,30
And in this case our exception message will be

65
00:03:44,30 --> 00:03:48,50
the title does not match.

66
00:03:48,50 --> 00:03:50,60
And we can delete line 100

67
00:03:50,60 --> 00:03:54,00
with our throw new pending exception.

68
00:03:54,00 --> 00:03:59,30
Now save this and go back to our terminal application

69
00:03:59,30 --> 00:04:06,70
and we'll re-run our vendor/bin/behat command.

70
00:04:06,70 --> 00:04:08,40
And our test passed.

71
00:04:08,40 --> 00:04:11,30
So now we know how to do a basic test of the end point,

72
00:04:11,30 --> 00:04:15,20
as well as verify that when we submit data to our API

73
00:04:15,20 --> 00:04:17,80
that the response matches what we expect.

74
00:04:17,80 --> 00:04:21,30
From this you should be able to easily expand this

75
00:04:21,30 --> 00:04:25,50
into writing your own test of this API or any other API

76
00:04:25,50 --> 00:04:26,70
pretty easily.

77
00:04:26,70 --> 00:04:30,90
This will give you the ability to verify that features

78
00:04:30,90 --> 00:02:58,00
of your API work as you expect them.

