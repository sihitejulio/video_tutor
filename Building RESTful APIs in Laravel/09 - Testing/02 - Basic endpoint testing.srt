1
00:00:00,60 --> 00:00:02,10
- [Instructor] Now, let's actually test

2
00:00:02,10 --> 00:00:04,50
and verify our endpoint, and this time,

3
00:00:04,50 --> 00:00:07,00
we'll attempt to verify that the question's endpoint

4
00:00:07,00 --> 00:00:09,70
contains 50 different questions in it.

5
00:00:09,70 --> 00:00:12,70
To begin, open up the basic.feature file

6
00:00:12,70 --> 00:00:16,80
located in features, basic.feature.

7
00:00:16,80 --> 00:00:19,70
We'll add a new step to our test on line 11.

8
00:00:19,70 --> 00:00:23,40
This step will read then the response

9
00:00:23,40 --> 00:00:27,00
contains 50 records.

10
00:00:27,00 --> 00:00:29,00
This gives us a new test step that we can write

11
00:00:29,00 --> 00:00:32,80
to ensure our response contains a set number of records.

12
00:00:32,80 --> 00:00:34,40
Now, to build up this test,

13
00:00:34,40 --> 00:00:36,40
we'll need to run in our terminal application,

14
00:00:36,40 --> 00:00:38,70
so open up your terminal application,

15
00:00:38,70 --> 00:00:44,00
and run the command vendor/bin/behat.

16
00:00:44,00 --> 00:00:46,70
It will note that our test has an undefined step.

17
00:00:46,70 --> 00:00:48,40
It will ask us where we want to place

18
00:00:48,40 --> 00:00:50,60
the context for this step.

19
00:00:50,60 --> 00:00:55,00
In this case, we will select one for the FeatureContext.

20
00:00:55,00 --> 00:00:56,30
It now prints out the snippet

21
00:00:56,30 --> 00:00:59,30
that we can use to define that test.

22
00:00:59,30 --> 00:01:01,20
So, we'll copy this.

23
00:01:01,20 --> 00:01:04,00
And now, we need to open up our FeatureContext file.

24
00:01:04,00 --> 00:01:06,60
Go back to your text editor,

25
00:01:06,60 --> 00:01:10,60
and open up the file at features, bootstrap, FeatureContext.

26
00:01:10,60 --> 00:01:13,30
We'll scroll to the bottom.

27
00:01:13,30 --> 00:01:18,60
And on line 74, we'll paste in the content that we received.

28
00:01:18,60 --> 00:01:20,10
Now, we need to update this

29
00:01:20,10 --> 00:01:23,40
to actually perform the steps that we want to test.

30
00:01:23,40 --> 00:01:26,40
To do so, we need to JSON decode the body

31
00:01:26,40 --> 00:01:28,60
and then count the number of records

32
00:01:28,60 --> 00:01:29,90
and verify that our count

33
00:01:29,90 --> 00:01:32,80
is equal to the argument that we passed in.

34
00:01:32,80 --> 00:01:34,80
We'll replace line 79

35
00:01:34,80 --> 00:01:47,60
with $data=json_decode($this->responseBody).

36
00:01:47,60 --> 00:01:49,80
responseBody is a property of the class

37
00:01:49,80 --> 00:01:52,80
that we set up earlier in this file

38
00:01:52,80 --> 00:01:55,60
when we return the response.

39
00:01:55,60 --> 00:01:58,10
Now, we can get the count for the number of records

40
00:01:58,10 --> 00:02:03,40
with $count = count($data).

41
00:02:03,40 --> 00:02:05,30
And finally, we can write our test

42
00:02:05,30 --> 00:02:11,80
with return ($count == $arg1).

43
00:02:11,80 --> 00:02:13,50
Now, if we run our test,

44
00:02:13,50 --> 00:02:16,90
again, with vendor/bin/behat,

45
00:02:16,90 --> 00:02:19,10
we'll see that our test passed.

46
00:02:19,10 --> 00:02:21,30
We could fill this test out more.

47
00:02:21,30 --> 00:02:22,10
For instance, we could ensure

48
00:02:22,10 --> 00:02:25,30
that every record is a question by verifying that we have

49
00:02:25,30 --> 00:02:28,20
the standard property use of a question.

50
00:02:28,20 --> 00:02:30,70
Notice also now we can build this test out

51
00:02:30,70 --> 00:02:32,40
and mix and match the test together

52
00:02:32,40 --> 00:02:35,10
to build other more complex tests.

53
00:02:35,10 --> 00:02:37,30
For now, this is where we'll leave it off.

54
00:02:37,30 --> 00:02:39,70
We built a pretty basic and simple test

55
00:02:39,70 --> 00:02:43,00
to verify our API for questions.

