1
00:00:00,40 --> 00:00:02,30
- So here's our finished solution.

2
00:00:02,30 --> 00:00:04,00
How about we run it together.

3
00:00:04,00 --> 00:00:06,20
Just take a look to see how long it works.

4
00:00:06,20 --> 00:00:07,90
We are going to do this the proper way

5
00:00:07,90 --> 00:00:09,40
where we install it.

6
00:00:09,40 --> 00:00:11,40
And so that just got installed in my BIN folder

7
00:00:11,40 --> 00:00:13,00
and if I opened that up I would see it.

8
00:00:13,00 --> 00:00:15,70
And I can prove that just by going 29_finished,

9
00:00:15,70 --> 00:00:16,80
and it runs.

10
00:00:16,80 --> 00:00:18,80
So it's now processing all of the files.

11
00:00:18,80 --> 00:00:21,90
How long does it take to do all the processing?

12
00:00:21,90 --> 00:00:23,60
And this is something I want to point out.

13
00:00:23,60 --> 00:00:27,20
If I open my activity monitor we can see that i'm

14
00:00:27,20 --> 00:00:32,90
using 1,453% of the CPU capacity on this computer.

15
00:00:32,90 --> 00:00:36,70
So that's 1,500% of a total 1,600%.

16
00:00:36,70 --> 00:00:39,10
And down here you can see my CPU load

17
00:00:39,10 --> 00:00:41,00
is off the charts.

18
00:00:41,00 --> 00:00:44,50
There you can see I'm reaching the end of the processing

19
00:00:44,50 --> 00:00:48,40
and through the magic of editing you did not have to sit

20
00:00:48,40 --> 00:00:50,10
there for all three minutes.

21
00:00:50,10 --> 00:00:53,10
But 186 seconds to do all the processing.

22
00:00:53,10 --> 00:00:55,50
And it created the three images for me and it maxed

23
00:00:55,50 --> 00:00:57,70
out my processing on my CPU.

24
00:00:57,70 --> 00:00:59,80
So it was using all the CPUs and you can totally

25
00:00:59,80 --> 00:01:00,80
see that right there.

26
00:01:00,80 --> 00:01:02,00
That's pretty awesome.

