1
00:00:00,00 --> 00:00:02,10
- So the next thing we're going to do is

2
00:00:02,10 --> 00:00:04,20
we're going to add concurrency into our solution.

3
00:00:04,20 --> 00:00:05,80
And concurrency is going to allow us to

4
00:00:05,80 --> 00:00:07,50
take advantage of all the processors,

5
00:00:07,50 --> 00:00:09,40
all the CPUs on our machine.

6
00:00:09,40 --> 00:00:11,60
So that will help our code run faster.

7
00:00:11,60 --> 00:00:13,60
And we're going to try to see if

8
00:00:13,60 --> 00:00:15,20
we can get our code to run faster.

9
00:00:15,20 --> 00:00:16,90
So the very first thing I did was

10
00:00:16,90 --> 00:00:18,70
I wanted to put a time function in.

11
00:00:18,70 --> 00:00:20,70
Once again just to time how long does this take?

12
00:00:20,70 --> 00:00:22,60
And I'm doing it slightly different here.

13
00:00:22,60 --> 00:00:25,00
I'm using time.Since(start) and giving

14
00:00:25,00 --> 00:00:27,00
me the seconds back on that.

15
00:00:27,00 --> 00:00:28,80
Before, you'll remember, we did

16
00:00:28,80 --> 00:00:30,70
this method right here, where in

17
00:00:30,70 --> 00:00:33,90
godoc.org, Standard Library Package, time,

18
00:00:33,90 --> 00:00:35,70
we did sub.

19
00:00:35,70 --> 00:00:37,80
And this time we are using Since.

20
00:00:37,80 --> 00:00:40,00
So I'm going to measure how long did the code take to run?

21
00:00:40,00 --> 00:00:42,10
And if I come over here and I run that,

22
00:00:42,10 --> 00:00:44,20
right here in 12,

23
00:00:44,20 --> 00:00:47,40
we'll see how long it takes.

24
00:00:47,40 --> 00:00:50,30
Now you'll notice that that took 18.38 seconds.

25
00:00:50,30 --> 00:00:52,00
But let's take a look at what we can

26
00:00:52,00 --> 00:00:53,90
do to improve that performance.

27
00:00:53,90 --> 00:00:55,90
So the next thing I did in cleaning up my code,

28
00:00:55,90 --> 00:00:57,70
is I did a little bit of abstraction.

29
00:00:57,70 --> 00:00:59,60
And I created this function getPaths,

30
00:00:59,60 --> 00:01:02,10
where it does the entire thing right here,

31
00:01:02,10 --> 00:01:04,90
and gets all the paths and returns those paths right there.

32
00:01:04,90 --> 00:01:06,90
And then the next thing I did was

33
00:01:06,90 --> 00:01:08,50
I added some concurrency.

34
00:01:08,50 --> 00:01:10,90
So under getting my images, I get all of my paths,

35
00:01:10,90 --> 00:01:13,50
and then I came down here and I created a go routine.

36
00:01:13,50 --> 00:01:15,90
And this is how you create go routines.

37
00:01:15,90 --> 00:01:20,00
So go routines are not operating system CPU threads.

38
00:01:20,00 --> 00:01:22,30
That's like an entirely different thing.

39
00:01:22,30 --> 00:01:24,20
And if you look in what a thread is,

40
00:01:24,20 --> 00:01:26,40
there's hyperthreading and that type of a deal.

41
00:01:26,40 --> 00:01:28,30
But, generally speaking, you can kind of just say

42
00:01:28,30 --> 00:01:30,90
for each CPU there's one thread of instruction

43
00:01:30,90 --> 00:01:32,90
that's running through that CPU.

44
00:01:32,90 --> 00:01:35,10
And with a go routine, it's a very

45
00:01:35,10 --> 00:01:38,90
light-weight data structure, and it's not a thread.

46
00:01:38,90 --> 00:01:40,90
It runs on top of a thread.

47
00:01:40,90 --> 00:01:43,70
And go is going to schedule those go routines.

48
00:01:43,70 --> 00:01:45,00
So they're very light.

49
00:01:45,00 --> 00:01:46,50
You should have no worries about

50
00:01:46,50 --> 00:01:48,80
creating a lot of them and letting them just all run.

51
00:01:48,80 --> 00:01:50,40
But you do have to think about

52
00:01:50,40 --> 00:01:52,50
how do I do concurrent programming?

53
00:01:52,50 --> 00:01:54,90
I just want to take a moment and make a distinction

54
00:01:54,90 --> 00:01:57,60
between concurrency and parallelism.

55
00:01:57,60 --> 00:02:00,70
So concurrency is doing many different things,

56
00:02:00,70 --> 00:02:03,30
but only one of those things at a time.

57
00:02:03,30 --> 00:02:05,80
So I could talk and I can eat,

58
00:02:05,80 --> 00:02:07,90
and I can do those two things concurrently,

59
00:02:07,90 --> 00:02:09,90
but I have to do one or the other.

60
00:02:09,90 --> 00:02:12,20
So I'm eating, I'm talking, that's happening concurrently.

61
00:02:12,20 --> 00:02:13,70
But, I'm going one or the other.

62
00:02:13,70 --> 00:02:16,00
And you can kind of see that in this diagram here.

63
00:02:16,00 --> 00:02:18,80
This routine is running, or that routine is running.

64
00:02:18,80 --> 00:02:20,40
So that's concurrency.

65
00:02:20,40 --> 00:02:23,00
Parallelism is doing both of them at the same time.

66
00:02:23,00 --> 00:02:25,30
So, something that I might do in parallel

67
00:02:25,30 --> 00:02:27,40
is run and listen to music.

68
00:02:27,40 --> 00:02:30,10
I could go for a run and I could also listen to music.

69
00:02:30,10 --> 00:02:32,50
And I could do those things exactly at the same time.

70
00:02:32,50 --> 00:02:33,90
Two separate things being done

71
00:02:33,90 --> 00:02:35,60
at the same time, in parallel.

72
00:02:35,60 --> 00:02:37,20
So I just wanted to make a distinction there

73
00:02:37,20 --> 00:02:39,20
between concurrency and parallelism.

74
00:02:39,20 --> 00:02:40,50
Because sometimes there's

75
00:02:40,50 --> 00:02:42,00
a little bit of confusion about that.

76
00:02:42,00 --> 00:02:44,20
Alright, so I launch a go routine.

77
00:02:44,20 --> 00:02:46,50
Basically what I have here to launch a go routine,

78
00:02:46,50 --> 00:02:48,40
you just give it the key word go.

79
00:02:48,40 --> 00:02:50,90
I have an anonymous, self-executing function.

80
00:02:50,90 --> 00:02:53,00
So this function is anonymous

81
00:02:53,00 --> 00:02:54,70
because it does not have a name.

82
00:02:54,70 --> 00:02:56,70
In our function declaration we'd have our func

83
00:02:56,70 --> 00:03:00,20
and then we'd have our receiver right here, if we had one.

84
00:03:00,20 --> 00:03:02,20
And then we'd have our identifier,

85
00:03:02,20 --> 00:03:03,80
which would be the name of the function.

86
00:03:03,80 --> 00:03:05,40
And then we'd have our parameters.

87
00:03:05,40 --> 00:03:07,30
And then we'd have our returns.

88
00:03:07,30 --> 00:03:09,20
But we do not have an identifier.

89
00:03:09,20 --> 00:03:11,40
We don't have returns, we don't have an identifier.

90
00:03:11,40 --> 00:03:13,10
We don't have a receiver.

91
00:03:13,10 --> 00:03:14,80
We just have an anonymous function.

92
00:03:14,80 --> 00:03:16,40
So that's why it's called anonymous.

93
00:03:16,40 --> 00:03:18,10
So it's an anonymous function,

94
00:03:18,10 --> 00:03:20,40
and as soon as it's defined, which is right here,

95
00:03:20,40 --> 00:03:22,50
I am calling it and executing it.

96
00:03:22,50 --> 00:03:24,50
And I'm passing in one argument.

97
00:03:24,50 --> 00:03:27,00
This is defined with the parameter to take a string.

98
00:03:27,00 --> 00:03:29,40
The string that it takes is a path.

99
00:03:29,40 --> 00:03:32,10
So I'm passing in all of my paths.

100
00:03:32,10 --> 00:03:34,10
And when I pass all those in, I'm

101
00:03:34,10 --> 00:03:35,80
going and getting my pixels.

102
00:03:35,80 --> 00:03:37,20
This makes total sense.

103
00:03:37,20 --> 00:03:39,30
So concurrently, using all my CPUs,

104
00:03:39,30 --> 00:03:42,60
go get the pixels for all of those images.

105
00:03:42,60 --> 00:03:45,10
I want every CPU working on that.

106
00:03:45,10 --> 00:03:47,30
And so that's a great thing to go get.

107
00:03:47,30 --> 00:03:50,10
I do have to do a little bit of locking right here.

108
00:03:50,10 --> 00:03:52,10
And the reason I have to lock is

109
00:03:52,10 --> 00:03:54,10
because I don't want a race condition.

110
00:03:54,10 --> 00:03:56,60
A race condition is where you have multiple routines

111
00:03:56,60 --> 00:03:59,50
running, trying to access the same piece of data.

112
00:03:59,50 --> 00:04:02,20
There's a great diagram of a race condition

113
00:04:02,20 --> 00:04:04,50
over at Bill Kennedy's Ardand Labs

114
00:04:04,50 --> 00:04:06,80
Hard-Core Go Training, let me show that to you.

115
00:04:06,80 --> 00:04:09,80
Here I am at GitHub, and Ardand Labs Go Training.

116
00:04:09,80 --> 00:04:13,00
This is all licensed Apache, anybody can use it.

117
00:04:13,00 --> 00:04:15,40
And I'm looking for go routines.

118
00:04:15,40 --> 00:04:17,20
I'm going to take a look at that.

119
00:04:17,20 --> 00:04:21,00
And inside here, I'm looking for race conditions.

120
00:04:21,00 --> 00:04:23,00
Hmm, I'm not finding it here, so let me dig

121
00:04:23,00 --> 00:04:25,00
around a bit more and see if I do find it.

122
00:04:25,00 --> 00:04:28,10
I'll search for race up at the repository,

123
00:04:28,10 --> 00:04:30,30
and as I scroll through and look at these topics,

124
00:04:30,30 --> 00:04:32,60
Topics data race, that's looking promising.

125
00:04:32,60 --> 00:04:34,50
I'll go to data race.

126
00:04:34,50 --> 00:04:37,90
Perfect, this is the diagram I was looking for.

127
00:04:37,90 --> 00:04:40,40
So here we have two different routines running.

128
00:04:40,40 --> 00:04:43,30
We have one globally shared value between those routines.

129
00:04:43,30 --> 00:04:45,90
If they each pull that value set to zero,

130
00:04:45,90 --> 00:04:47,70
and then increment it and write it

131
00:04:47,70 --> 00:04:50,20
back to the global variable, this should be 2,

132
00:04:50,20 --> 00:04:52,50
but because there is that overlapping

133
00:04:52,50 --> 00:04:55,50
of them both being able to access the same variable

134
00:04:55,50 --> 00:04:58,00
at the same time, we have a race condition.

135
00:04:58,00 --> 00:04:59,90
We didn't get the correct value right here.

136
00:04:59,90 --> 00:05:02,10
So that's a little diagram of a race condition.

137
00:05:02,10 --> 00:05:04,60
Back at our code, we prevent a race condition,

138
00:05:04,60 --> 00:05:06,60
by locking that variable down.

139
00:05:06,60 --> 00:05:08,70
So we say only one routine can

140
00:05:08,70 --> 00:05:10,40
access this variable at a time.

141
00:05:10,40 --> 00:05:12,30
So if something is appending to images,

142
00:05:12,30 --> 00:05:14,70
that is the only routine that can access that.

143
00:05:14,70 --> 00:05:15,90
So that's it.

144
00:05:15,90 --> 00:05:17,30
That's how we create a go routine.

145
00:05:17,30 --> 00:05:19,50
The one last thing to point out which is of note,

146
00:05:19,50 --> 00:05:22,40
is that here I'm passing in this path variable

147
00:05:22,40 --> 00:05:24,20
and receiving it here.

148
00:05:24,20 --> 00:05:26,10
That is creating its own scope.

149
00:05:26,10 --> 00:05:28,00
Go has block level scope.

150
00:05:28,00 --> 00:05:31,50
So the scope of path right here is right to there.

151
00:05:31,50 --> 00:05:34,10
I could have called this a slightly different name,

152
00:05:34,10 --> 00:05:36,00
like maybe I could have called it

153
00:05:36,00 --> 00:05:38,10
just p would've been a good name.

154
00:05:38,10 --> 00:05:39,90
And that way there's a distinction between

155
00:05:39,90 --> 00:05:42,10
what got passed in and that's this variable.

156
00:05:42,10 --> 00:05:44,30
And the p right there which is the path.

157
00:05:44,30 --> 00:05:46,80
And I do that because as these launch,

158
00:05:46,80 --> 00:05:49,20
I want to make sure that the path, the iteration,

159
00:05:49,20 --> 00:05:51,60
that is receiving this is the correct one.

160
00:05:51,60 --> 00:05:54,50
And if I didn't pass this in, and I was just calling path

161
00:05:54,50 --> 00:05:56,70
right here from there, this go routine

162
00:05:56,70 --> 00:05:59,20
will be accessing the incorrect path, right?

163
00:05:59,20 --> 00:06:01,90
So different go routines will be accessing the wrong paths.

164
00:06:01,90 --> 00:06:03,40
So that's how you create a go routine.

165
00:06:03,40 --> 00:06:05,30
Let's watch it run and see how quick it is.

166
00:06:05,30 --> 00:06:06,70
So, it's good to take note.

167
00:06:06,70 --> 00:06:08,60
Our last one took about 18 seconds.

168
00:06:08,60 --> 00:06:10,60
And currently we are in 12.

169
00:06:10,60 --> 00:06:12,70
And we're going to run 14 here,

170
00:06:12,70 --> 00:06:16,60
so change directories to 14.

171
00:06:16,60 --> 00:06:19,70
So we went from 18 seconds down to four seconds.

172
00:06:19,70 --> 00:06:22,00
And the interesting thing is, is while that runs,

173
00:06:22,00 --> 00:06:24,30
I'm going to bring up my activity monitor here.

174
00:06:24,30 --> 00:06:25,80
And I'm going to run that again

175
00:06:25,80 --> 00:06:27,60
and take a look at my activity monitor.

176
00:06:27,60 --> 00:06:29,50
424%.

177
00:06:29,50 --> 00:06:33,60
So at one point, I was using 424% of my CPUs.

178
00:06:33,60 --> 00:06:36,50
The total percentage of power available

179
00:06:36,50 --> 00:06:38,90
on this computer happens to be 1600%.

180
00:06:38,90 --> 00:06:41,20
So you can see down here I kind of spiked a little bit.

181
00:06:41,20 --> 00:06:43,80
But, as we start to do processing into the billions,

182
00:06:43,80 --> 00:06:45,60
we'll see that really come up.

183
00:06:45,60 --> 00:06:47,80
Alright, so let's go and look at our final steps.

184
00:06:47,80 --> 00:06:49,80
Come back to my code.

185
00:06:49,80 --> 00:06:51,50
And I'm done with this file.

186
00:06:51,50 --> 00:06:53,20
And the last two things I'm going to do

187
00:06:53,20 --> 00:06:56,10
is in this file I want to create a new data structure,

188
00:06:56,10 --> 00:06:58,10
and it's going to be an image.

189
00:06:58,10 --> 00:07:00,00
An image will have pixels.

190
00:07:00,00 --> 00:07:01,70
It'll also have a name.

191
00:07:01,70 --> 00:07:03,50
It'll also have a width and a height.

192
00:07:03,50 --> 00:07:05,50
So I'm going to start passing around that image

193
00:07:05,50 --> 00:07:07,70
in here because there'll be times where

194
00:07:07,70 --> 00:07:09,90
as I start to do my processing and comparison,

195
00:07:09,90 --> 00:07:12,40
I need to know the name of the files I'm comparing.

196
00:07:12,40 --> 00:07:14,20
I need to know their width and their height.

197
00:07:14,20 --> 00:07:16,00
And I want all the pixels of that file.

198
00:07:16,00 --> 00:07:18,00
So you can see, I've changed a little bit of

199
00:07:18,00 --> 00:07:20,10
the naming in here for what an image is.

200
00:07:20,10 --> 00:07:23,40
And I've also done a little aliasing up here in my imports.

201
00:07:23,40 --> 00:07:25,70
Because I didn't want to overwrite this,

202
00:07:25,70 --> 00:07:27,30
which is package image.

203
00:07:27,30 --> 00:07:29,30
So I called that standard image from

204
00:07:29,30 --> 00:07:31,10
the standard library, package image.

205
00:07:31,10 --> 00:07:33,80
So I had to rename that so I could call this struct image.

206
00:07:33,80 --> 00:07:35,50
And I also wanted to illustrate

207
00:07:35,50 --> 00:07:37,60
how you do aliasing in go.

208
00:07:37,60 --> 00:07:40,40
So last thing I want to do is I want to start using

209
00:07:40,40 --> 00:07:43,10
channels for doing my concurrency.

210
00:07:43,10 --> 00:07:46,90
You could either have a buffered or an unbuffered channel.

211
00:07:46,90 --> 00:07:49,30
If you're just getting started with concurrency,

212
00:07:49,30 --> 00:07:52,80
I highly recommend you only use unbuffered channels.

213
00:07:52,80 --> 00:07:55,00
And so basically an unbuffered channel

214
00:07:55,00 --> 00:07:57,10
is a way you could communicate

215
00:07:57,10 --> 00:07:59,10
between different go routines.

216
00:07:59,10 --> 00:08:01,30
And there's a little bit of detail to this.

217
00:08:01,30 --> 00:08:03,00
But basically, an unbuffered channel

218
00:08:03,00 --> 00:08:05,30
allows you to put some data onto the channel

219
00:08:05,30 --> 00:08:07,60
and receive data off of the channel.

220
00:08:07,60 --> 00:08:09,40
And you can only put one thing

221
00:08:09,40 --> 00:08:11,20
and take on thing off at a time.

222
00:08:11,20 --> 00:08:13,10
And so, that allows you to

223
00:08:13,10 --> 00:08:15,00
coordinate your different go routines.

224
00:08:15,00 --> 00:08:16,60
And again, like I said, you can take a look

225
00:08:16,60 --> 00:08:18,30
at all this code up here if you want to

226
00:08:18,30 --> 00:08:20,90
understand how we do concurrency in more detail.

227
00:08:20,90 --> 00:08:23,10
Just check out that folder right there.

228
00:08:23,10 --> 00:08:25,00
So I make a new channel.

229
00:08:25,00 --> 00:08:26,50
And here I'm going to push all

230
00:08:26,50 --> 00:08:28,40
of my pixels onto that channel.

231
00:08:28,40 --> 00:08:30,50
And I'm returning this channel up here.

232
00:08:30,50 --> 00:08:32,80
So wherever it was where I called getImages,

233
00:08:32,80 --> 00:08:34,80
that is giving me back a channel.

234
00:08:34,80 --> 00:08:36,10
What do I do with that channel?

235
00:08:36,10 --> 00:08:38,30
I'm passing that channel into xImages.

236
00:08:38,30 --> 00:08:40,40
And what does xImages do?

237
00:08:40,40 --> 00:08:45,00
xImages takes that channel and it ranges over the channel

238
00:08:45,00 --> 00:08:48,80
and appends things to a slice of pointers to an image.

239
00:08:48,80 --> 00:08:50,50
So the image addresses.

240
00:08:50,50 --> 00:08:52,80
And remember now, an image is this struct

241
00:08:52,80 --> 00:08:54,50
that I've created right here.

242
00:08:54,50 --> 00:08:57,10
So that struct is saying here's all the data for each image.

243
00:08:57,10 --> 00:08:59,70
And all I'm passing around is the address,

244
00:08:59,70 --> 00:09:01,20
which is a single word.

245
00:09:01,20 --> 00:09:04,00
On a 64-bit architecture machine, only 64-bits.

246
00:09:04,00 --> 00:09:07,40
So when I call xImages, what I get back are

247
00:09:07,40 --> 00:09:13,00
my images which is just a slice of pointers to an image.

248
00:09:13,00 --> 00:09:16,00
I range over that, and I can see all of the addresses,

249
00:09:16,00 --> 00:09:17,60
dereference them right here,

250
00:09:17,60 --> 00:09:19,00
and I'll get the data structure.

251
00:09:19,00 --> 00:09:23,10
So let's run this code and take a look at it.

252
00:09:23,10 --> 00:09:25,20
Whoa, isn't that crazy?

253
00:09:25,20 --> 00:09:27,00
So I'm just going to hit Control C.

254
00:09:27,00 --> 00:09:31,10
And it's printing out every single image, all of the pixels.

255
00:09:31,10 --> 00:09:33,00
All 87 million of them.

256
00:09:33,00 --> 00:09:34,80
And if we go up here to the top,

257
00:09:34,80 --> 00:09:37,80
we see the beginning of that being printed.

258
00:09:37,80 --> 00:09:39,80
So the first thing right there,

259
00:09:39,80 --> 00:09:42,70
is this part of our struct.

260
00:09:42,70 --> 00:09:44,50
The name.

261
00:09:44,50 --> 00:09:47,20
So the name got printed, right there.

262
00:09:47,20 --> 00:09:49,50
And then the next part is a slice.

263
00:09:49,50 --> 00:09:51,70
And that's a slice of pixel.

264
00:09:51,70 --> 00:09:54,90
And a pixel has a struct, which is RGBA,

265
00:09:54,90 --> 00:09:57,60
and if we scroll down and we looked at how that finishes.

266
00:09:57,60 --> 00:09:59,30
I don't know if we'll be able to find the end

267
00:09:59,30 --> 00:10:01,30
of that data structure, I just saw a flash.

268
00:10:01,30 --> 00:10:03,10
Here's the end of the data structure,

269
00:10:03,10 --> 00:10:04,50
there's the end of the slice.

270
00:10:04,50 --> 00:10:06,30
And then we have 88 by 1975.

271
00:10:06,30 --> 00:10:08,40
And that was the width and the height.

272
00:10:08,40 --> 00:10:13,60
And if we go and we look at these images,

273
00:10:13,60 --> 00:10:15,30
Reveal in Finder.

274
00:10:15,30 --> 00:10:22,50
We can look, 88, that was 888 by 1975.

275
00:10:22,50 --> 00:10:25,10
We can see that was the Statue of Liberty reversed.

276
00:10:25,10 --> 00:10:27,10
888 by 1975.

277
00:10:27,10 --> 00:10:28,60
So we implemented some great

278
00:10:28,60 --> 00:10:30,40
things in this step of our solution.

279
00:10:30,40 --> 00:10:31,90
We've added concurrency in.

280
00:10:31,90 --> 00:10:34,20
And concurrency really speeded up our processing.

281
00:10:34,20 --> 00:10:36,00
In the next couple of videos, we're going

282
00:10:36,00 --> 00:10:38,00
to see how to finish out this solution.

