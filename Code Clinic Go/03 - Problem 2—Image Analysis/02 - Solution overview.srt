1
00:00:00,40 --> 00:00:03,90
- So here's the solution to my image-comparison

2
00:00:03,90 --> 00:00:06,70
and you can see there are quite a few files here.

3
00:00:06,70 --> 00:00:09,00
I've got 32 different folders.

4
00:00:09,00 --> 00:00:13,40
And this was actually a really big problem to solve

5
00:00:13,40 --> 00:00:15,30
and a pretty challenging problem.

6
00:00:15,30 --> 00:00:17,70
Image analysis is not my forte,

7
00:00:17,70 --> 00:00:19,50
there are people who specialize

8
00:00:19,50 --> 00:00:23,80
for their entire careers on doing image analysis

9
00:00:23,80 --> 00:00:27,20
and you can write doctoral theses in image analysis.

10
00:00:27,20 --> 00:00:29,80
As you start researching how do I analyze images,

11
00:00:29,80 --> 00:00:32,40
it's a rabbit hole which gets deep.

12
00:00:32,40 --> 00:00:33,40
Just right off the bat

13
00:00:33,40 --> 00:00:35,30
I want to give the high level overview,

14
00:00:35,30 --> 00:00:36,70
just kind of like step way back

15
00:00:36,70 --> 00:00:38,50
and look at the overall solution

16
00:00:38,50 --> 00:00:41,10
and show you kind of how I went about solving the prob.

17
00:00:41,10 --> 00:00:42,20
And then in the next videos

18
00:00:42,20 --> 00:00:44,80
we'll step into all of the details of the solution

19
00:00:44,80 --> 00:00:47,80
and actually see the mechanics of the Go code

20
00:00:47,80 --> 00:00:51,30
and how I used Go to do an image-comparison.

21
00:00:51,30 --> 00:00:52,50
So the first thing I'll share with you

22
00:00:52,50 --> 00:00:54,70
is my ultimate approach

23
00:00:54,70 --> 00:00:57,30
which was to use sum of absolute differences.

24
00:00:57,30 --> 00:01:00,00
And sum of absolute differences is to

25
00:01:00,00 --> 00:01:03,20
get the data for each pixel and then compare it.

26
00:01:03,20 --> 00:01:04,50
So what does that mean?

27
00:01:04,50 --> 00:01:09,60
Here are the images that I had to work with, right here.

28
00:01:09,60 --> 00:01:10,40
And so each image

29
00:01:10,40 --> 00:01:12,90
as you saw in the intro video for this code clinic

30
00:01:12,90 --> 00:01:14,70
is made up of a bunch of pixels.

31
00:01:14,70 --> 00:01:17,00
So if I take this image right here

32
00:01:17,00 --> 00:01:19,30
and I get that top left pixel,

33
00:01:19,30 --> 00:01:22,20
whatever that data is for that pixel,

34
00:01:22,20 --> 00:01:26,10
and that data ultimately is binary, it's zeros and ones.

35
00:01:26,10 --> 00:01:28,20
But I could come up a level from binary

36
00:01:28,20 --> 00:01:31,20
and I could just look at what is the RGBA,

37
00:01:31,20 --> 00:01:34,00
the Red, the Green, the Blue, the Alpha values

38
00:01:34,00 --> 00:01:35,40
for that one pixel.

39
00:01:35,40 --> 00:01:37,20
And I could take that one pixel

40
00:01:37,20 --> 00:01:41,50
and then I could run through all of the pixels of this file

41
00:01:41,50 --> 00:01:43,70
and anywhere that there's a match,

42
00:01:43,70 --> 00:01:46,80
anywhere that I have that top left pixel

43
00:01:46,80 --> 00:01:49,60
matching some pixel here,

44
00:01:49,60 --> 00:01:51,60
I could then start comparing sequentially.

45
00:01:51,60 --> 00:01:54,70
So I could then say, okay, well this one matched some point

46
00:01:54,70 --> 00:01:56,50
in this file right there.

47
00:01:56,50 --> 00:01:59,30
Can I now start going to the next pixel over,

48
00:01:59,30 --> 00:02:00,80
pixel number two?

49
00:02:00,80 --> 00:02:02,70
And from the matching point,

50
00:02:02,70 --> 00:02:05,40
does it then match the next pixel over?

51
00:02:05,40 --> 00:02:07,80
And just keep going in sequence like that.

52
00:02:07,80 --> 00:02:10,10
And so if the pixels continue to match

53
00:02:10,10 --> 00:02:12,10
then we have a matched image.

54
00:02:12,10 --> 00:02:17,00
Now to implement this I had to determine some threshold

55
00:02:17,00 --> 00:02:20,00
because jpeg is lossy compression.

56
00:02:20,00 --> 00:02:22,30
So lossy and lossless compression,

57
00:02:22,30 --> 00:02:24,30
sometimes when you compress something

58
00:02:24,30 --> 00:02:26,70
you don't lose any data, right.

59
00:02:26,70 --> 00:02:29,10
So it's compressed and there's no data loss,

60
00:02:29,10 --> 00:02:31,60
that's lossless, you haven't lost anything.

61
00:02:31,60 --> 00:02:35,60
Lossy compression means when you change a file

62
00:02:35,60 --> 00:02:37,70
that file is going to lose data.

63
00:02:37,70 --> 00:02:40,00
And so jpeg is lossy compression

64
00:02:40,00 --> 00:02:43,50
and just by the fact that I took a cut out of this image

65
00:02:43,50 --> 00:02:46,10
and then saved it, in saving it

66
00:02:46,10 --> 00:02:50,50
the algorithm which saves a jpeg file is going to slightly,

67
00:02:50,50 --> 00:02:53,20
if not drastically, alter that data.

68
00:02:53,20 --> 00:02:56,00
And whoever saves that file can have the choice

69
00:02:56,00 --> 00:02:58,10
to set different settings.

70
00:02:58,10 --> 00:03:02,30
And so if you look at that on Wikipedia,

71
00:03:02,30 --> 00:03:07,40
I'm going to Google Wikipedia, jpeg.

72
00:03:07,40 --> 00:03:10,00
And now that I'm here at jpeg on Wikipedia

73
00:03:10,00 --> 00:03:12,10
I could scroll down and all the way down here

74
00:03:12,10 --> 00:03:15,10
there's an example of different files

75
00:03:15,10 --> 00:03:16,70
saved at different qualities.

76
00:03:16,70 --> 00:03:19,40
So here it's saved at a quality of a 100

77
00:03:19,40 --> 00:03:22,90
and further down saved at a quality of one,

78
00:03:22,90 --> 00:03:23,80
you know the lowest.

79
00:03:23,80 --> 00:03:26,30
And you can see that the data has drastically been altered

80
00:03:26,30 --> 00:03:28,80
so whoever saves the file can adjust the quality

81
00:03:28,80 --> 00:03:31,00
at which that file is saved.

82
00:03:31,00 --> 00:03:34,00
All right, so we're working with jpeg, it's lossy,

83
00:03:34,00 --> 00:03:36,60
you know there might not be an exact match

84
00:03:36,60 --> 00:03:39,20
because as I saved the excerpt from this file

85
00:03:39,20 --> 00:03:42,40
or whoever saved it, saved the excerpt from this file

86
00:03:42,40 --> 00:03:46,20
to this file, right, the data might have changed some.

87
00:03:46,20 --> 00:03:47,90
They might have changed that setting.

88
00:03:47,90 --> 00:03:49,70
So I needed to build in some threshold

89
00:03:49,70 --> 00:03:53,80
where the sum of absolute differences between the pixels,

90
00:03:53,80 --> 00:03:55,70
right, I had a little bit of wiggle room.

91
00:03:55,70 --> 00:03:58,10
So hey, if the difference was less than a certain amount,

92
00:03:58,10 --> 00:03:59,80
cool, we're going to call it good.

93
00:03:59,80 --> 00:04:02,10
So that was the first thing that I had to come to

94
00:04:02,10 --> 00:04:05,00
in figuring out my solution for this problem.

95
00:04:05,00 --> 00:04:08,00
All right, and then all of these steps here, right,

96
00:04:08,00 --> 00:04:12,50
opening a file, reading a file, decoding the images, right,

97
00:04:12,50 --> 00:04:14,50
and then kind of looking at what I got,

98
00:04:14,50 --> 00:04:16,70
and then working with the file information

99
00:04:16,70 --> 00:04:20,80
and seeing because this was an interesting insight for me

100
00:04:20,80 --> 00:04:24,10
just looking at my files, I was like, wait a minute

101
00:04:24,10 --> 00:04:28,40
this problem's already solved because anything that has A,

102
00:04:28,40 --> 00:04:30,20
right, we've got a match.

103
00:04:30,20 --> 00:04:31,50
So that was actually

104
00:04:31,50 --> 00:04:33,60
probably the easiest solution right there.

105
00:04:33,60 --> 00:04:34,50
It was just to be like,

106
00:04:34,50 --> 00:04:36,40
look these files have the exact same name

107
00:04:36,40 --> 00:04:39,00
except one has an A which means it's a subset of the other,

108
00:04:39,00 --> 00:04:41,30
we're done fantastic.

109
00:04:41,30 --> 00:04:43,60
But that wasn't quite the intent of the challenge.

110
00:04:43,60 --> 00:04:46,00
So I looked at file info for a second,

111
00:04:46,00 --> 00:04:48,90
and then abstracting the code,

112
00:04:48,90 --> 00:04:51,70
and then getting all the pixels into a data structure,

113
00:04:51,70 --> 00:04:54,60
and then adding concurrency in so that we could

114
00:04:54,60 --> 00:04:57,70
really maximize whatever computer I'm running this on,

115
00:04:57,70 --> 00:04:59,70
I could take advantage of all the cores of the computer

116
00:04:59,70 --> 00:05:01,40
to do the processing.

117
00:05:01,40 --> 00:05:03,70
And then building a comparison image.

118
00:05:03,70 --> 00:05:05,20
And so here's the comparison image

119
00:05:05,20 --> 00:05:08,10
and these are close enough matches.

120
00:05:08,10 --> 00:05:09,90
And I hadn't refined it yet

121
00:05:09,90 --> 00:05:12,30
to be just give me the very best match.

122
00:05:12,30 --> 00:05:15,50
So under my threshold, when I ran this, right,

123
00:05:15,50 --> 00:05:18,50
I got a match there and it's like cool, that's the match.

124
00:05:18,50 --> 00:05:21,30
And then this also matched, right, so they're very close

125
00:05:21,30 --> 00:05:24,00
so I had a couple of matches under my threshold

126
00:05:24,00 --> 00:05:26,80
and then I built an image to show that's were it matched.

127
00:05:26,80 --> 00:05:29,60
And so there are those couples and then with the parrot,

128
00:05:29,60 --> 00:05:32,40
right there, right, and then also right there.

129
00:05:32,40 --> 00:05:34,50
So I needed at that point in my solution,

130
00:05:34,50 --> 00:05:37,20
and let me just close those preview files,

131
00:05:37,20 --> 00:05:40,50
to then filter those solutions down to the very best match

132
00:05:40,50 --> 00:05:41,00
out of that.

133
00:05:41,00 --> 00:05:44,00
So I filtered it down and I also overlaid the images

134
00:05:44,00 --> 00:05:45,80
so that you could sort of see,

135
00:05:45,80 --> 00:05:48,00
instead of just giving me that one image,

136
00:05:48,00 --> 00:05:49,40
where it was the black there's the match.

137
00:05:49,40 --> 00:05:51,50
I said, well here's this over here

138
00:05:51,50 --> 00:05:52,80
and then there's that match.

139
00:05:52,80 --> 00:05:55,60
And I got this really kind of trippy looking art

140
00:05:55,60 --> 00:05:57,20
and I actually kind of like it.

141
00:05:57,20 --> 00:05:59,50
But I wanted to sort of like, yeah, show

142
00:05:59,50 --> 00:06:02,40
where in the image did the excerpted image come from

143
00:06:02,40 --> 00:06:04,50
and so I built this image right here to do that.

144
00:06:04,50 --> 00:06:06,90
And the reason that it looks all whacky colors

145
00:06:06,90 --> 00:06:09,40
is I just really sort of degraded the amount of data

146
00:06:09,40 --> 00:06:12,30
I was working with so that I wasn't working with

147
00:06:12,30 --> 00:06:13,60
quite so much data.

148
00:06:13,60 --> 00:06:16,20
Anyhow, there's where we have the match

149
00:06:16,20 --> 00:06:18,10
and then I filter them out.

150
00:06:18,10 --> 00:06:20,70
And then the one thing which I didn't solve yet

151
00:06:20,70 --> 00:06:22,50
was at that point in the solution,

152
00:06:22,50 --> 00:06:26,10
was here we have the Statue of Liberty, fantastic,

153
00:06:26,10 --> 00:06:30,10
right, but then it's an excerpt which is flipped, great.

154
00:06:30,10 --> 00:06:33,20
So now I have an incredible amount of processing

155
00:06:33,20 --> 00:06:34,20
which is occurring.

156
00:06:34,20 --> 00:06:37,70
And we're talking about like 93 billion comparisons

157
00:06:37,70 --> 00:06:40,40
are made when the solution ultimately runs.

158
00:06:40,40 --> 00:06:42,30
And I count all of those somewhere down here,

159
00:06:42,30 --> 00:06:43,90
total-pixels-compared.

160
00:06:43,90 --> 00:06:46,10
So if you run that version it'll show you

161
00:06:46,10 --> 00:06:48,90
we made 93 billion comparisons.

162
00:06:48,90 --> 00:06:52,00
Are you kidding me, 93 billion comparisons?

163
00:06:52,00 --> 00:06:55,50
And so on my machine which has four cores

164
00:06:55,50 --> 00:06:58,70
and it has the entire Hyper-Threading virtual cores,

165
00:06:58,70 --> 00:07:02,50
whatever that is, so it translates to basically eight cores.

166
00:07:02,50 --> 00:07:04,50
On my machine it takes about 15 minutes.

167
00:07:04,50 --> 00:07:07,10
On this machine that I'm recording these lectures on,

168
00:07:07,10 --> 00:07:10,30
that has 16 cores, eight cores, eight of them virtual,

169
00:07:10,30 --> 00:07:12,20
16 cores right.

170
00:07:12,20 --> 00:07:15,40
With 16 cores it takes about three and a half minutes

171
00:07:15,40 --> 00:07:16,40
to run on here.

172
00:07:16,40 --> 00:07:20,00
But 93 billion comparisons, totally crazy.

173
00:07:20,00 --> 00:07:23,70
All right, so the point being was with the Statue of Liberty

174
00:07:23,70 --> 00:07:25,50
and here's this I put it into Photoshop

175
00:07:25,50 --> 00:07:27,70
just to sort of illustrate, all right,

176
00:07:27,70 --> 00:07:30,10
and I don't have Photoshop on this machine apparently.

177
00:07:30,10 --> 00:07:34,20
But here I still take that top left pixel and I compare it

178
00:07:34,20 --> 00:07:38,70
but now I have to, when I get a match, invert this, right.

179
00:07:38,70 --> 00:07:40,60
I have to run my algorithm in reverse

180
00:07:40,60 --> 00:07:43,50
so I then start going this way and as I'm going this way

181
00:07:43,50 --> 00:07:45,80
through this image, once I get a match on that image

182
00:07:45,80 --> 00:07:49,60
I start going this way and I'm comparing in sequence again.

183
00:07:49,60 --> 00:07:50,80
So I had to flip that.

184
00:07:50,80 --> 00:07:52,30
And I ran that against all of my images

185
00:07:52,30 --> 00:07:55,70
so basically doubled the processing that needed to be done.

186
00:07:55,70 --> 00:07:57,30
Because for every time I had a match

187
00:07:57,30 --> 00:07:59,10
I was looking both forward and backwards

188
00:07:59,10 --> 00:08:00,60
to see if there's a match.

189
00:08:00,60 --> 00:08:04,40
So like I said, this is like the brute force approach

190
00:08:04,40 --> 00:08:05,60
to image comparison.

191
00:08:05,60 --> 00:08:07,90
It's probably not the way Google approaches it

192
00:08:07,90 --> 00:08:10,20
but maybe it is just at a higher level.

193
00:08:10,20 --> 00:08:12,10
So kind of interesting stuff.

194
00:08:12,10 --> 00:08:13,90
So that was basically my solve.

195
00:08:13,90 --> 00:08:15,40
When it's all said and done,

196
00:08:15,40 --> 00:08:17,50
we've got some amazing concurrency going on,

197
00:08:17,50 --> 00:08:21,00
and we have four different files, so here's our main file,

198
00:08:21,00 --> 00:08:22,60
and we have our compare file,

199
00:08:22,60 --> 00:08:24,60
our filter file, and our make file.

200
00:08:24,60 --> 00:08:26,00
And that's kind of going to illustrate

201
00:08:26,00 --> 00:08:27,90
how we could work with different files in Go

202
00:08:27,90 --> 00:08:30,10
and have them be part of this same package.

203
00:08:30,10 --> 00:08:32,30
And so they'll show you a little bit of file organization.

204
00:08:32,30 --> 00:08:33,50
So there's some really cool things to show you

205
00:08:33,50 --> 00:08:34,80
about this challenge.

206
00:08:34,80 --> 00:08:36,50
All right, so that's an overview of my solution

207
00:08:36,50 --> 00:08:37,80
that's the high level overview.

208
00:08:37,80 --> 00:08:39,10
In the next couple of videos,

209
00:08:39,10 --> 00:08:40,30
we're going to take a look at

210
00:08:40,30 --> 00:08:42,00
how I actually implemented that

211
00:08:42,00 --> 00:08:44,00
and start learning some cool things about Go code.

