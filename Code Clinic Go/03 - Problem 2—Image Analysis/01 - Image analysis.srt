1
00:00:06,90 --> 00:00:09,10
- Hello, and welcome to Code Clinic.

2
00:00:09,10 --> 00:00:10,90
My name is Todd McLeod.

3
00:00:10,90 --> 00:00:13,20
Code Clinic is a course where a unique problem

4
00:00:13,20 --> 00:00:16,20
is introduced to a collection of Lynda.com authors.

5
00:00:16,20 --> 00:00:19,20
In response, each author will create a solution

6
00:00:19,20 --> 00:00:21,70
using their programming language of choice.

7
00:00:21,70 --> 00:00:23,80
You can learn several things from Code Clinic,

8
00:00:23,80 --> 00:00:25,90
different approaches to solving a problem,

9
00:00:25,90 --> 00:00:28,10
the pros and cons of different languages,

10
00:00:28,10 --> 00:00:29,80
and some tips and tricks

11
00:00:29,80 --> 00:00:33,40
to incorporate into your own coding practices.

12
00:00:33,40 --> 00:00:35,00
In this challenge, we'll work on a problem

13
00:00:35,00 --> 00:00:37,30
centered around Image Analysis.

14
00:00:37,30 --> 00:00:40,30
In one sense, this is simply data analysis.

15
00:00:40,30 --> 00:00:41,90
Images are really nothing more

16
00:00:41,90 --> 00:00:45,80
than specialized and well-defined sets of data.

17
00:00:45,80 --> 00:00:48,20
An image consists of pixels.

18
00:00:48,20 --> 00:00:51,50
Pixels consist of data representing the color of the pixel,

19
00:00:51,50 --> 00:00:54,60
and in some cases, the pixel's transparency.

20
00:00:54,60 --> 00:00:57,70
The pixels are arranged in rows and columns.

21
00:00:57,70 --> 00:00:59,20
When assembled correctly,

22
00:00:59,20 --> 00:01:00,90
they represent an image.

23
00:01:00,90 --> 00:01:03,60
Our brains are very good at recognizing patterns,

24
00:01:03,60 --> 00:01:05,20
but computers are not.

25
00:01:05,20 --> 00:01:07,40
Think about Captcha security devices,

26
00:01:07,40 --> 00:01:09,10
those puzzles you sometimes see

27
00:01:09,10 --> 00:01:11,30
when logging into a website.

28
00:01:11,30 --> 00:01:13,30
The Captcha asks what letters and numbers

29
00:01:13,30 --> 00:01:14,30
are in the image,

30
00:01:14,30 --> 00:01:16,40
information obscured by random lines

31
00:01:16,40 --> 00:01:20,20
sometimes overlapping transparent blocks of color.

32
00:01:20,20 --> 00:01:21,40
All of those intersecting shapes

33
00:01:21,40 --> 00:01:23,30
makes it difficult for a computer program

34
00:01:23,30 --> 00:01:25,00
to separate the background noise

35
00:01:25,00 --> 00:01:27,40
from the actual data.

36
00:01:27,40 --> 00:01:31,00
Another example is the test to determine color blindness.

37
00:01:31,00 --> 00:01:32,40
Letters and numbers are hidden

38
00:01:32,40 --> 00:01:35,10
in a circle filled with different colored dots.

39
00:01:35,10 --> 00:01:36,50
If you're color blind,

40
00:01:36,50 --> 00:01:38,60
you will not be able to see the numbers.

41
00:01:38,60 --> 00:01:39,70
For a computer program,

42
00:01:39,70 --> 00:01:41,30
this can be incredibly difficult,

43
00:01:41,30 --> 00:01:43,30
as it requires detecting an edge,

44
00:01:43,30 --> 00:01:45,80
as well as recognizing the overall shape.

45
00:01:45,80 --> 00:01:49,80
It's difficult even for the most advanced programmer.

46
00:01:49,80 --> 00:01:51,20
In this problem, we're trying to solve

47
00:01:51,20 --> 00:01:53,10
a common problem for many photographers,

48
00:01:53,10 --> 00:01:54,80
plagiarism.

49
00:01:54,80 --> 00:01:56,40
A photographer will take a picture

50
00:01:56,40 --> 00:01:57,50
and post it on the internet,

51
00:01:57,50 --> 00:01:59,80
only to discover someone has stolen their image

52
00:01:59,80 --> 00:02:01,50
and placed a subset of that image

53
00:02:01,50 --> 00:02:03,10
on their website.

54
00:02:03,10 --> 00:02:06,20
For example, here is an image,

55
00:02:06,20 --> 00:02:09,40
and then a cropped version of that image.

56
00:02:09,40 --> 00:02:10,60
It would be extremely handy

57
00:02:10,60 --> 00:02:12,30
if there was a program searching the internet

58
00:02:12,30 --> 00:02:14,50
for cropped versions of an original image,

59
00:02:14,50 --> 00:02:16,90
so that a photographer could protect their rights.

60
00:02:16,90 --> 00:02:20,00
In fact, Google Image Search will do just that.

61
00:02:20,00 --> 00:02:21,30
But we're curious how it works,

62
00:02:21,30 --> 00:02:24,90
and what the required code might look like.

63
00:02:24,90 --> 00:02:26,00
Here's the challenge.

64
00:02:26,00 --> 00:02:27,20
Given two images,

65
00:02:27,20 --> 00:02:28,50
determine if one image

66
00:02:28,50 --> 00:02:31,20
is a subset of the other image.

67
00:02:31,20 --> 00:02:33,20
We'll assume they are both JPEG files,

68
00:02:33,20 --> 00:02:35,10
that the Resolution is the same,

69
00:02:35,10 --> 00:02:37,60
as well as the Bit Depth.

70
00:02:37,60 --> 00:02:39,20
We provided a set of images.

71
00:02:39,20 --> 00:02:40,70
The program should return a table

72
00:02:40,70 --> 00:02:42,70
showing which images are cropped versions

73
00:02:42,70 --> 00:02:45,00
of other images.

74
00:02:45,00 --> 00:02:45,80
Perhaps you want to pause

75
00:02:45,80 --> 00:02:47,70
and create a solution of your own.

76
00:02:47,70 --> 00:02:49,90
How would you solve the problem?

77
00:02:49,90 --> 00:02:50,60
In the next videos,

78
00:02:50,60 --> 00:02:54,00
I'll show you how I solved this challenge.

