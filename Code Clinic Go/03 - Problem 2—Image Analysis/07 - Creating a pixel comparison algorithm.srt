1
00:00:00,40 --> 00:00:03,50
- So the next step in our solution is to start comparing

2
00:00:03,50 --> 00:00:06,20
the pixels between different images.

3
00:00:06,20 --> 00:00:08,20
And we've been able to load the images,

4
00:00:08,20 --> 00:00:10,40
we've been able to get all of the pixel information

5
00:00:10,40 --> 00:00:11,60
for every image out.

6
00:00:11,60 --> 00:00:15,00
We have all that data, we implemented some concurrency

7
00:00:15,00 --> 00:00:17,10
to help us load all the images

8
00:00:17,10 --> 00:00:19,80
and pull out all the pixel data for each image,

9
00:00:19,80 --> 00:00:22,20
but now we need to start comparing those pixels.

10
00:00:22,20 --> 00:00:25,70
So this is going to be a needle in the haystack problem.

11
00:00:25,70 --> 00:00:26,80
What does that mean?

12
00:00:26,80 --> 00:00:28,80
We have a haystack,

13
00:00:28,80 --> 00:00:31,50
and we're looking for a needle within it.

14
00:00:31,50 --> 00:00:33,00
So we're going to take this top-left pixel

15
00:00:33,00 --> 00:00:34,00
and we're going to compare it

16
00:00:34,00 --> 00:00:36,70
to every pixel in the haystack.

17
00:00:36,70 --> 00:00:37,80
And then when we have a match,

18
00:00:37,80 --> 00:00:40,60
we'll start sequencing across every single one

19
00:00:40,60 --> 00:00:43,70
of these pixels, matching the underlined pixels

20
00:00:43,70 --> 00:00:45,80
and see if we continue to have a match.

21
00:00:45,80 --> 00:00:48,40
So to do this, I created a new file,

22
00:00:48,40 --> 00:00:51,10
and this file is still part of package main.

23
00:00:51,10 --> 00:00:54,20
You can see compare.go is part of package main

24
00:00:54,20 --> 00:00:57,40
and then also main.go is part of package main.

25
00:00:57,40 --> 00:00:59,50
And I have both of those files right here.

26
00:00:59,50 --> 00:01:02,50
So to run my code with more than one file

27
00:01:02,50 --> 00:01:05,50
I need to do either go run *.go,

28
00:01:05,50 --> 00:01:08,50
meaning run any file that ends in .go,

29
00:01:08,50 --> 00:01:11,70
or the better way to do it is to build my code,

30
00:01:11,70 --> 00:01:13,60
in which case if I use just go build,

31
00:01:13,60 --> 00:01:15,60
I'm going to say, hey right here in the current directory

32
00:01:15,60 --> 00:01:18,40
if I'm in this directory, run this executable,

33
00:01:18,40 --> 00:01:21,50
or I can install my code, which means it'll build it

34
00:01:21,50 --> 00:01:24,60
and install it into by bin folder in my workspace.

35
00:01:24,60 --> 00:01:26,60
And since I have an environment variable,

36
00:01:26,60 --> 00:01:29,90
a path variable set to my bin folder in my workspace,

37
00:01:29,90 --> 00:01:32,30
I give in at the terminal just type in the name

38
00:01:32,30 --> 00:01:35,00
of the executable, takes the name from the folder it's in

39
00:01:35,00 --> 00:01:37,50
and it'll run that executable.

40
00:01:37,50 --> 00:01:39,70
So let's take a look to see how we're comparing pixels

41
00:01:39,70 --> 00:01:41,60
to get the difference between different pixels.

42
00:01:41,60 --> 00:01:44,20
So the heart of that algorithm is right down here,

43
00:01:44,20 --> 00:01:46,20
where we compare difference in pixels,

44
00:01:46,20 --> 00:01:49,30
so we're getting each pixel, the needle and the haystack,

45
00:01:49,30 --> 00:01:51,50
and each pixel has RGBA values,

46
00:01:51,50 --> 00:01:53,50
so here are those RGBA values.

47
00:01:53,50 --> 00:01:55,90
And with each of those values in the needle,

48
00:01:55,90 --> 00:01:58,50
I'm comparing it to the the value in the haystack.

49
00:01:58,50 --> 00:02:01,10
And I'm then taking the absolute value of that

50
00:02:01,10 --> 00:02:03,60
and I'm summing the difference of all of that.

51
00:02:03,60 --> 00:02:05,20
That's the summation right there

52
00:02:05,20 --> 00:02:08,40
of what the difference is between each pixel

53
00:02:08,40 --> 00:02:10,00
when I'm comparing pixels.

54
00:02:10,00 --> 00:02:13,40
So each pixel is made up of zero

55
00:02:13,40 --> 00:02:17,20
to 65,535 values

56
00:02:17,20 --> 00:02:20,10
because each pixel is using like two bytes,

57
00:02:20,10 --> 00:02:22,30
but it's kind of interesting, if you remember

58
00:02:22,30 --> 00:02:24,70
when we took a look at pixel information,

59
00:02:24,70 --> 00:02:29,70
which we did right down here, in format printing, right.

60
00:02:29,70 --> 00:02:32,60
When we printed out the type, it's a uint 32.

61
00:02:32,60 --> 00:02:36,90
Yet when we look at the values, they're from zero to 65,535.

62
00:02:36,90 --> 00:02:40,00
That tell's me it's using about two bytes of information

63
00:02:40,00 --> 00:02:42,40
or two bytes exactly, of information

64
00:02:42,40 --> 00:02:45,00
for the color information, zero to 65,000.

65
00:02:45,00 --> 00:02:46,40
Well if we're using two bytes,

66
00:02:46,40 --> 00:02:50,30
why aren't we using uint 16 instead of uint 32?

67
00:02:50,30 --> 00:02:52,10
Well the answer to that can be found right here

68
00:02:52,10 --> 00:02:56,10
in the internals, right, the code of the standard library

69
00:02:56,10 --> 00:02:57,80
and that's the answer right there.

70
00:02:57,80 --> 00:02:59,20
So you could pause the video

71
00:02:59,20 --> 00:03:00,90
and you could take a look at that and read that.

72
00:03:00,90 --> 00:03:03,00
But that's just one of the things that was interesting to me

73
00:03:03,00 --> 00:03:04,70
when I was putting the solution together,

74
00:03:04,70 --> 00:03:06,10
it's kind of like, that doesn't make sense,

75
00:03:06,10 --> 00:03:08,40
and it didn't make sense until I finally read this,

76
00:03:08,40 --> 00:03:09,70
right here.

77
00:03:09,70 --> 00:03:12,70
Alright, so let's go back to our algorithm.

78
00:03:12,70 --> 00:03:14,30
So where am I using this

79
00:03:14,30 --> 00:03:15,90
to compare the difference in pixels?

80
00:03:15,90 --> 00:03:17,50
So the first thing I do is I want to come over

81
00:03:17,50 --> 00:03:19,10
and I want to look at main.

82
00:03:19,10 --> 00:03:21,80
So inside main, I'm getting all of my images

83
00:03:21,80 --> 00:03:23,20
and I'm taking those images

84
00:03:23,20 --> 00:03:25,00
and I'm putting them onto a channel.

85
00:03:25,00 --> 00:03:28,70
So getImages returns a pointer to an image

86
00:03:28,70 --> 00:03:30,60
and it puts that onto a channel.

87
00:03:30,60 --> 00:03:33,50
A pointer to an image has the name, the pixels,

88
00:03:33,50 --> 00:03:35,50
the width, and the height.

89
00:03:35,50 --> 00:03:37,10
And then I take that channel and I'm passing it

90
00:03:37,10 --> 00:03:39,10
to another function xImages.

91
00:03:39,10 --> 00:03:42,20
I used x because xImages is going to give me a slice

92
00:03:42,20 --> 00:03:45,00
of images, so xImages, when it comes down here

93
00:03:45,00 --> 00:03:47,40
what does it return?

94
00:03:47,40 --> 00:03:52,20
xImages gives me a slice of pointers to images.

95
00:03:52,20 --> 00:03:54,20
So now that I have that, I'm taking those images

96
00:03:54,20 --> 00:03:55,60
right there, all those images,

97
00:03:55,60 --> 00:03:56,80
and I'm going to compare them.

98
00:03:56,80 --> 00:03:59,90
So let's go look and see what compare does.

99
00:03:59,90 --> 00:04:02,40
The first thing compare does is it starts looping

100
00:04:02,40 --> 00:04:04,10
over all of the images.

101
00:04:04,10 --> 00:04:06,20
I'm going to synchronize my go routines right here

102
00:04:06,20 --> 00:04:08,60
with WaitGroup, and then I'm also creating a channel

103
00:04:08,60 --> 00:04:10,50
to put my results onto.

104
00:04:10,50 --> 00:04:13,10
But then after that, I'm going to loop over all my images.

105
00:04:13,10 --> 00:04:15,40
So for each image, it's a nested loop,

106
00:04:15,40 --> 00:04:17,80
I'm going to compare to all of the other images.

107
00:04:17,80 --> 00:04:20,30
So for each image in my folder,

108
00:04:20,30 --> 00:04:22,00
that'll be the needle, right,

109
00:04:22,00 --> 00:04:23,80
and I'm going to compare it to all other images

110
00:04:23,80 --> 00:04:25,20
which will be the haystack.

111
00:04:25,20 --> 00:04:27,10
And then I'm going to start to eliminate possibilities,

112
00:04:27,10 --> 00:04:30,50
so I don't have to do all of those 87 million comparisons.

113
00:04:30,50 --> 00:04:32,40
So, if the needle's bigger than the haystack,

114
00:04:32,40 --> 00:04:33,90
either in width or height,

115
00:04:33,90 --> 00:04:36,30
then the needle did not come from the haystack.

116
00:04:36,30 --> 00:04:38,20
Stop comparing that one right now.

117
00:04:38,20 --> 00:04:40,40
And if the needle name is equal to the haystack name,

118
00:04:40,40 --> 00:04:43,60
then most likely that's the exact same file, right.

119
00:04:43,60 --> 00:04:45,60
So, once I've eliminated those possiblities,

120
00:04:45,60 --> 00:04:47,50
I'm now going to compare the pixels

121
00:04:47,50 --> 00:04:49,80
for each of those needles and haystacks.

122
00:04:49,80 --> 00:04:51,70
And I pass that in, into this funciton,

123
00:04:51,70 --> 00:04:54,10
and I launch it as its own go routine.

124
00:04:54,10 --> 00:04:55,70
I'm going to go compare those pixels.

125
00:04:55,70 --> 00:04:58,20
What does comparePixels do?

126
00:04:58,20 --> 00:05:00,00
Interesting, and pretty cool,

127
00:05:00,00 --> 00:05:01,90
when I get a match in the pixels,

128
00:05:01,90 --> 00:05:04,40
so when that top-left corner of the needle

129
00:05:04,40 --> 00:05:06,30
matches the pixel in the haystack,

130
00:05:06,30 --> 00:05:08,40
I then go launch another go routine,

131
00:05:08,40 --> 00:05:10,70
which compares them in sequence.

132
00:05:10,70 --> 00:05:13,00
So visually, to see what that looks like,

133
00:05:13,00 --> 00:05:16,20
when this top corner matches somewhere,

134
00:05:16,20 --> 00:05:18,10
it'll then start comparing in sequence

135
00:05:18,10 --> 00:05:19,70
and it'll start running across here

136
00:05:19,70 --> 00:05:22,80
and comparing that to all of the underlying pixels.

137
00:05:22,80 --> 00:05:24,50
And what compareSequence does,

138
00:05:24,50 --> 00:05:27,20
is it's checking to see, hey, do we continue to have a match

139
00:05:27,20 --> 00:05:29,50
between all of these aligned pixels

140
00:05:29,50 --> 00:05:31,60
in the needle and the haystack.

141
00:05:31,60 --> 00:05:33,50
So the cool thing about that is

142
00:05:33,50 --> 00:05:36,10
to compare pixels between two images

143
00:05:36,10 --> 00:05:37,60
and launch a go routine.

144
00:05:37,60 --> 00:05:40,00
And then to compare a sequence between two images,

145
00:05:40,00 --> 00:05:41,70
right, when the pixels match,

146
00:05:41,70 --> 00:05:43,00
I'll launch another go routine.

147
00:05:43,00 --> 00:05:45,00
So I'm launching all of these go routines,

148
00:05:45,00 --> 00:05:47,50
so all these processes are being set in the motion

149
00:05:47,50 --> 00:05:49,90
and that allows my computer to use all of its resources

150
00:05:49,90 --> 00:05:51,90
to start doing that processing.

151
00:05:51,90 --> 00:05:54,20
To do the processing we could get down into the details

152
00:05:54,20 --> 00:05:56,90
a little bit, of what comparing pixels does,

153
00:05:56,90 --> 00:05:59,60
so we call comparePixels and comparePixels

154
00:05:59,60 --> 00:06:02,90
is going to now go through every pixel in the haystack

155
00:06:02,90 --> 00:06:05,10
and it's going to find the x and the y position

156
00:06:05,10 --> 00:06:06,70
of each pixel in the haystack,

157
00:06:06,70 --> 00:06:09,20
and then it's going to compare that to the needle.

158
00:06:09,20 --> 00:06:11,90
So once again we're doing a size elimination here.

159
00:06:11,90 --> 00:06:14,30
So the needle must fit within the haystack,

160
00:06:14,30 --> 00:06:15,50
right there at that comment.

161
00:06:15,50 --> 00:06:17,00
And visually what does that look like?

162
00:06:17,00 --> 00:06:18,20
It looks like this.

163
00:06:18,20 --> 00:06:20,70
So the needle needs to fit within the haystack.

164
00:06:20,70 --> 00:06:23,60
So if I'm comparing the top-left corner of the needle,

165
00:06:23,60 --> 00:06:26,20
I could compare right there, and that needle

166
00:06:26,20 --> 00:06:28,90
still will fit within this haystack.

167
00:06:28,90 --> 00:06:32,20
But if I start comparing the top-left corner of the needle,

168
00:06:32,20 --> 00:06:34,10
right here, well then that needle

169
00:06:34,10 --> 00:06:35,90
will no longer fit in that haystack.

170
00:06:35,90 --> 00:06:38,10
It would be off the haystack image.

171
00:06:38,10 --> 00:06:41,10
So I want to eliminate that possibility, where this needle

172
00:06:41,10 --> 00:06:43,90
and this is the size of the needle on top of the haystack,

173
00:06:43,90 --> 00:06:46,40
this needle fits the haystack right here,

174
00:06:46,40 --> 00:06:49,10
but if I shifted this over one more pixel,

175
00:06:49,10 --> 00:06:50,80
that needle will no longer fit in the haystack.

176
00:06:50,80 --> 00:06:52,60
So let's eliminate that possibility

177
00:06:52,60 --> 00:06:55,10
and when I do eliminate all those possibilities,

178
00:06:55,10 --> 00:06:58,70
I only need to compare the top-left corner of the needle

179
00:06:58,70 --> 00:07:01,10
to these pixels in the haystack.

180
00:07:01,10 --> 00:07:02,80
Because these are the only possiblities

181
00:07:02,80 --> 00:07:06,60
where the needle will match something in the haystack

182
00:07:06,60 --> 00:07:08,00
and still have room to match

183
00:07:08,00 --> 00:07:10,00
all the other pixels beneath it.

184
00:07:10,00 --> 00:07:11,80
So that's what this does right here.

185
00:07:11,80 --> 00:07:14,80
It's comparing the top-left corner of the needle,

186
00:07:14,80 --> 00:07:19,10
to the pixels that make sense within the haystack.

187
00:07:19,10 --> 00:07:21,30
So here's where the pixel comparison occurs.

188
00:07:21,30 --> 00:07:22,40
I get a difference between

189
00:07:22,40 --> 00:07:23,80
that top-left corner of the needle

190
00:07:23,80 --> 00:07:26,40
to whatever pixel in the haystack we are in.

191
00:07:26,40 --> 00:07:28,80
And if that difference is beneath the threshold,

192
00:07:28,80 --> 00:07:31,80
then we're going to go and launch compareSequence,

193
00:07:31,80 --> 00:07:33,20
where we start comparing it.

194
00:07:33,20 --> 00:07:34,50
Well what is threshold?

195
00:07:34,50 --> 00:07:38,70
So, again we are looking at each pixel is a uint 32,

196
00:07:38,70 --> 00:07:42,00
but really there are 65,535 values

197
00:07:42,00 --> 00:07:44,10
for each RGBA.

198
00:07:44,10 --> 00:07:48,60
And so each pixel has four times 65,535,

199
00:07:48,60 --> 00:07:51,00
or a quarter million possible values.

200
00:07:51,00 --> 00:07:52,60
So at one end of the spectrum,

201
00:07:52,60 --> 00:07:56,80
we could have zero, zero, zero, zero,

202
00:07:56,80 --> 00:07:58,50
and at the other end of the spectrum we could have

203
00:07:58,50 --> 00:08:01,60
65,535, 65,535,

204
00:08:01,60 --> 00:08:05,90
65,535, and guess what I'm going to say again (laughs),

205
00:08:05,90 --> 00:08:08,70
65,535, or a quarter million.

206
00:08:08,70 --> 00:08:11,60
So we have somewhere between zero and a quarter of a million

207
00:08:11,60 --> 00:08:14,30
approximately possible values, and so,

208
00:08:14,30 --> 00:08:18,90
if the difference is only 500 or this small fraction

209
00:08:18,90 --> 00:08:21,40
of a difference in the images,

210
00:08:21,40 --> 00:08:23,60
then we're going to say, okay that's the threshold.

211
00:08:23,60 --> 00:08:25,80
And we're allowing for some fuzziness here,

212
00:08:25,80 --> 00:08:27,70
so we could adjust that if I wanted to.

213
00:08:27,70 --> 00:08:29,50
I could try well 1,000 or 2,000

214
00:08:29,50 --> 00:08:31,80
and see what that difference is.

215
00:08:31,80 --> 00:08:33,80
So, if that difference is low enough,

216
00:08:33,80 --> 00:08:35,60
let's go compare that sequence.

217
00:08:35,60 --> 00:08:38,20
And now the first thing I want to do, in compareSequence,

218
00:08:38,20 --> 00:08:39,90
is I want to fail quickly.

219
00:08:39,90 --> 00:08:43,00
So if this isn't matching, I want to get out

220
00:08:43,00 --> 00:08:45,00
as fast as I can, so I don't have to do

221
00:08:45,00 --> 00:08:46,60
any additional comparison.

222
00:08:46,60 --> 00:08:47,90
So I'm going to start to go through

223
00:08:47,90 --> 00:08:49,50
every pixel in the needle,

224
00:08:49,50 --> 00:08:51,00
and for every pixel in the needle

225
00:08:51,00 --> 00:08:53,90
I'm going to find the difference with every underlined,

226
00:08:53,90 --> 00:08:56,40
corresponding pixel in the haystack.

227
00:08:56,40 --> 00:08:58,00
And as I calculate those differences,

228
00:08:58,00 --> 00:09:00,50
I'm looking for these three things right here.

229
00:09:00,50 --> 00:09:02,90
So, did the previous row we just calculate,

230
00:09:02,90 --> 00:09:05,90
have less than 10 good matches,

231
00:09:05,90 --> 00:09:07,50
then we're under the threshold.

232
00:09:07,50 --> 00:09:09,70
And if we're in a new row, I need to reset the counter

233
00:09:09,70 --> 00:09:12,10
where I'm counting how many matches are in each row,

234
00:09:12,10 --> 00:09:14,20
and if this pixel's beneath the threshold,

235
00:09:14,20 --> 00:09:16,00
then I'm going to increment that counter.

236
00:09:16,00 --> 00:09:17,70
So that's one of the things I'm doing right there

237
00:09:17,70 --> 00:09:19,10
just to make sure every row

238
00:09:19,10 --> 00:09:21,10
has sufficient number of matches.

239
00:09:21,10 --> 00:09:22,70
And so if we do get enough matches,

240
00:09:22,70 --> 00:09:24,50
and each row has more than 10,

241
00:09:24,50 --> 00:09:26,50
and this is something where you play with it

242
00:09:26,50 --> 00:09:29,20
and see what is the match, then I'm going to say okay,

243
00:09:29,20 --> 00:09:31,30
that was a match, if we get through the entire image

244
00:09:31,30 --> 00:09:34,60
and none of the rows are less than 10 good differences,

245
00:09:34,60 --> 00:09:36,50
then I'm going to say we have a match.

246
00:09:36,50 --> 00:09:39,80
I'm going to take that result and put it onto a channel.

247
00:09:39,80 --> 00:09:40,80
What is result?

248
00:09:40,80 --> 00:09:42,20
If we come over here to main,

249
00:09:42,20 --> 00:09:44,10
we could see that result is a struct

250
00:09:44,10 --> 00:09:46,30
where I'm storing a needle and a haystack,

251
00:09:46,30 --> 00:09:48,00
they're both pointers to images.

252
00:09:48,00 --> 00:09:50,70
I'm storing the pixel in the haystack,

253
00:09:50,70 --> 00:09:52,30
where we first had our match,

254
00:09:52,30 --> 00:09:54,00
so that's kind of like my anchor point,

255
00:09:54,00 --> 00:09:56,30
and I'm also storing the average difference

256
00:09:56,30 --> 00:09:59,40
between all the pixels for that entire image.

257
00:09:59,40 --> 00:10:02,40
And you can see average difference was calculated right here

258
00:10:02,40 --> 00:10:04,50
where I accumulated all the differences

259
00:10:04,50 --> 00:10:07,50
and I divided by all of the pixels in the needle

260
00:10:07,50 --> 00:10:10,00
and that gave me my average difference per pixel.

261
00:10:10,00 --> 00:10:15,00
So let's run this file 17 and see what it actually does.

262
00:10:15,00 --> 00:10:16,70
There it goes, it's running.

263
00:10:16,70 --> 00:10:18,40
And it printed out these results.

264
00:10:18,40 --> 00:10:19,40
That's pretty great.

265
00:10:19,40 --> 00:10:20,50
So what are those results?

266
00:10:20,50 --> 00:10:22,70
If I come back and I look at my results,

267
00:10:22,70 --> 00:10:24,50
my results are a needle and a haystack,

268
00:10:24,50 --> 00:10:25,60
and they're going to be addresses

269
00:10:25,60 --> 00:10:27,10
where that information's stored.

270
00:10:27,10 --> 00:10:29,60
Needle, haystack, addresses.

271
00:10:29,60 --> 00:10:32,00
It's going to be the haystack index, where we had a match.

272
00:10:32,00 --> 00:10:34,10
So these are the pixels in the haystack

273
00:10:34,10 --> 00:10:35,50
where we had a match.

274
00:10:35,50 --> 00:10:37,80
And then it's going to be the average difference per pixel.

275
00:10:37,80 --> 00:10:39,50
And so we can see here we have

276
00:10:39,50 --> 00:10:40,80
some pretty low numbers actually,

277
00:10:40,80 --> 00:10:43,10
when you think about that as in comparison to

278
00:10:43,10 --> 00:10:44,60
a quarter of a million.

279
00:10:44,60 --> 00:10:47,70
So that is where I started to build the algorithm

280
00:10:47,70 --> 00:10:50,00
for comparing the differences between pixels

281
00:10:50,00 --> 00:10:52,20
and finding the right matches.

282
00:10:52,20 --> 00:10:54,00
A couple of things we did after that,

283
00:10:54,00 --> 00:10:55,40
or that I'm going to do after that,

284
00:10:55,40 --> 00:10:58,70
is build images to kind of display the results,

285
00:10:58,70 --> 00:11:01,30
and then also just to a little bit more cleanup

286
00:11:01,30 --> 00:11:03,60
to make it all run a little bit better.

287
00:11:03,60 --> 00:11:06,20
So that's the heart of the image comparison algorithm.

288
00:11:06,20 --> 00:11:08,20
We still have a little bit more to look at

289
00:11:08,20 --> 00:11:11,00
and I added in a few more steps here in my solution,

290
00:11:11,00 --> 00:11:14,30
including doing the reverse, and matching on

291
00:11:14,30 --> 00:11:17,50
the Statue of Liberty reversing that compare algorithm

292
00:11:17,50 --> 00:11:21,00
so the Statue of Liberty, when we're looking at that,

293
00:11:21,00 --> 00:11:23,40
right here, we're able to match that one also.

294
00:11:23,40 --> 00:11:25,00
So that's still to come.

