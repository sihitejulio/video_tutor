1
00:00:00,80 --> 00:00:02,00
- So our next step is we're going to do

2
00:00:02,00 --> 00:00:03,90
a little bit more abstraction of our code

3
00:00:03,90 --> 00:00:06,00
just to keep main really lean.

4
00:00:06,00 --> 00:00:07,40
Then we're going to get all the pixel

5
00:00:07,40 --> 00:00:09,30
for all of the images

6
00:00:09,30 --> 00:00:12,80
and then the final thing in this video right here

7
00:00:12,80 --> 00:00:15,10
is that we are going to count all of the pixels.

8
00:00:15,10 --> 00:00:17,10
And so when we count all the pixels

9
00:00:17,10 --> 00:00:19,40
we'll see that we have aggregated together

10
00:00:19,40 --> 00:00:24,60
into a data structure 87, 020, 406 pixels

11
00:00:24,60 --> 00:00:26,10
which is totally amazing to me

12
00:00:26,10 --> 00:00:28,90
that this thing runs in like under 30 seconds

13
00:00:28,90 --> 00:00:33,30
and it get the information, looks at 87,000,000 pixels.

14
00:00:33,30 --> 00:00:35,00
So how do we do the abstraction?

15
00:00:35,00 --> 00:00:36,40
I've got another little challenge for you.

16
00:00:36,40 --> 00:00:37,90
So this is where we left off,

17
00:00:37,90 --> 00:00:38,90
we were loading an image

18
00:00:38,90 --> 00:00:40,20
and we'd abstracted that out

19
00:00:40,20 --> 00:00:43,10
and you can see here in folder 08main.go

20
00:00:43,10 --> 00:00:44,70
same thing, same code.

21
00:00:44,70 --> 00:00:46,20
I can compare between those two

22
00:00:46,20 --> 00:00:48,80
and funk load image does change at all between those two.

23
00:00:48,80 --> 00:00:52,30
But here in 08, I want to abstract some of this.

24
00:00:52,30 --> 00:00:53,80
So what would you take out?

25
00:00:53,80 --> 00:00:55,00
How would you abstract it?

26
00:00:55,00 --> 00:00:56,00
How would you make it run?

27
00:00:56,00 --> 00:00:57,60
Maybe you want to pause the video at this point.

28
00:00:57,60 --> 00:00:58,90
Go right here to this folder,

29
00:00:58,90 --> 00:01:01,40
open main.go and see if you can abstract this code

30
00:01:01,40 --> 00:01:02,60
and look at how you do it.

31
00:01:02,60 --> 00:01:03,90
So here's how I did it.

32
00:01:03,90 --> 00:01:06,00
I created this new function down here.

33
00:01:06,00 --> 00:01:07,30
So here's load image.

34
00:01:07,30 --> 00:01:09,70
We saw that already, but now I have getImages

35
00:01:09,70 --> 00:01:11,20
because that's pretty much what this does,

36
00:01:11,20 --> 00:01:12,90
it goes and it gets the images.

37
00:01:12,90 --> 00:01:15,40
And so Get Images creates a data structure, images,

38
00:01:15,40 --> 00:01:17,40
which is a slice of images.

39
00:01:17,40 --> 00:01:18,50
Just a list of images.

40
00:01:18,50 --> 00:01:19,90
We do our file path walk

41
00:01:19,90 --> 00:01:22,30
and we walk over this entire directory

42
00:01:22,30 --> 00:01:23,70
and each time we walk over,

43
00:01:23,70 --> 00:01:25,30
one of the items in the directory,

44
00:01:25,30 --> 00:01:27,20
and the directory we're walking over's right here.

45
00:01:27,20 --> 00:01:29,10
All these different images right here.

46
00:01:29,10 --> 00:01:31,30
So each time we walk over one of those images,

47
00:01:31,30 --> 00:01:34,30
we are taking that path and we're loading the image.

48
00:01:34,30 --> 00:01:36,60
When we load the image it gives us an image.

49
00:01:36,60 --> 00:01:37,90
That's what load image does.

50
00:01:37,90 --> 00:01:41,40
And then I pin that image to my slice of images.

51
00:01:41,40 --> 00:01:42,20
Perfect.

52
00:01:42,20 --> 00:01:43,90
I return my entire slice of images.

53
00:01:43,90 --> 00:01:45,10
So that's how I abstracted it.

54
00:01:45,10 --> 00:01:46,70
And I ran this right here.

55
00:01:46,70 --> 00:01:50,00
And then that prints out all of the r, g, b, a data for

56
00:01:50,00 --> 00:01:54,60
the very first pixel on each and every image in the folder.

57
00:01:54,60 --> 00:01:56,50
So let's run that and see what it looks like.

58
00:01:56,50 --> 00:01:59,80
Change directories, go run main.go.

59
00:01:59,80 --> 00:02:02,20
Perfect, so it printed it all out.

60
00:02:02,20 --> 00:02:03,60
So what's our next step?

61
00:02:03,60 --> 00:02:05,30
We were able to get all the images,

62
00:02:05,30 --> 00:02:07,10
now we need to get all of the pixels.

63
00:02:07,10 --> 00:02:08,40
So let me show you that function.

64
00:02:08,40 --> 00:02:10,40
So here's get pixels right?

65
00:02:10,40 --> 00:02:13,30
And basically each image is made up of a bunch of pixels.

66
00:02:13,30 --> 00:02:15,50
So we're going to need some sort of a slice

67
00:02:15,50 --> 00:02:17,80
to store all of that pixel data.

68
00:02:17,80 --> 00:02:19,10
And what is a pixel?

69
00:02:19,10 --> 00:02:21,60
Is that a certain type, is it part of the standard library?

70
00:02:21,60 --> 00:02:23,80
A pixel is of type struct.

71
00:02:23,80 --> 00:02:25,70
And it's a type that I've created.

72
00:02:25,70 --> 00:02:28,70
You can create your own types in Go.

73
00:02:28,70 --> 00:02:30,90
I've created a type, it's type pixel.

74
00:02:30,90 --> 00:02:33,20
And the underlying type is that it's a struct, right?

75
00:02:33,20 --> 00:02:34,50
So what is a struct?

76
00:02:34,50 --> 00:02:35,90
If we go look at the language spec,

77
00:02:35,90 --> 00:02:37,50
and here's the language spec.

78
00:02:37,50 --> 00:02:39,10
I can see down here under types

79
00:02:39,10 --> 00:02:41,20
that it's another type that I have to work with.

80
00:02:41,20 --> 00:02:43,40
And specifically, it's an aggregate type

81
00:02:43,40 --> 00:02:45,90
which means that I can aggregate different types

82
00:02:45,90 --> 00:02:47,50
together into this one type.

83
00:02:47,50 --> 00:02:49,30
And this allows you to start to do some

84
00:02:49,30 --> 00:02:52,00
of the object oriented programming type stuff in Go.

85
00:02:52,00 --> 00:02:54,70
We don't really say object oriented in Go.

86
00:02:54,70 --> 00:02:57,80
If you go through this entire language specification,

87
00:02:57,80 --> 00:03:00,30
Command-F, object oriented,

88
00:03:00,30 --> 00:03:01,70
never is it mentioned.

89
00:03:01,70 --> 00:03:04,60
Likewise, if we look through Effective Go

90
00:03:04,60 --> 00:03:06,90
which is like the other sort of go to document

91
00:03:06,90 --> 00:03:08,90
for how are we supposed to do things

92
00:03:08,90 --> 00:03:10,90
by the book with Go programming

93
00:03:10,90 --> 00:03:13,80
and we search for object oriented, nothing.

94
00:03:13,80 --> 00:03:16,70
But however, if we do go to the golang.org website

95
00:03:16,70 --> 00:03:18,20
and we got to documents,

96
00:03:18,20 --> 00:03:20,60
and we scroll down to frequently asked questions,

97
00:03:20,60 --> 00:03:24,90
in here one of the questions they talk about

98
00:03:24,90 --> 00:03:26,40
is Go an object-oriented language.

99
00:03:26,40 --> 00:03:28,70
You can pause this video right here and read that.

100
00:03:28,70 --> 00:03:30,20
And there are aspects of Go

101
00:03:30,20 --> 00:03:31,60
which are very object oriented,

102
00:03:31,60 --> 00:03:33,30
but it's done in a slightly different way.

103
00:03:33,30 --> 00:03:37,00
So we do have structs which allow us to put types together.

104
00:03:37,00 --> 00:03:38,30
So this could kind of be like

105
00:03:38,30 --> 00:03:40,10
type employee could be a struct.

106
00:03:40,10 --> 00:03:42,30
with these different fields in the struct

107
00:03:42,30 --> 00:03:44,20
which would be other data types.

108
00:03:44,20 --> 00:03:46,30
So I'm aggregating different data types together.

109
00:03:46,30 --> 00:03:49,20
So I created a struct and it has r, g, b, a.

110
00:03:49,20 --> 00:03:52,30
And those are all uint32s, each of those fields

111
00:03:52,30 --> 00:03:55,00
and I can declare them all on one line like that,

112
00:03:55,00 --> 00:03:57,90
this variable of this type.

113
00:03:57,90 --> 00:03:59,30
Alright, there's my struct.

114
00:03:59,30 --> 00:04:01,40
Now I have my struct of pixels

115
00:04:01,40 --> 00:04:03,70
and I'm making a slice of pixel.

116
00:04:03,70 --> 00:04:06,40
Right and so for each entry in this slice

117
00:04:06,40 --> 00:04:07,80
I'm going to store a pixel.

118
00:04:07,80 --> 00:04:09,20
So I pass in an image

119
00:04:09,20 --> 00:04:10,70
and then that image is going to be

120
00:04:10,70 --> 00:04:12,80
turned into all of the pixels that it has.

121
00:04:12,80 --> 00:04:15,60
I loop over every single pixel in my image

122
00:04:15,60 --> 00:04:18,10
and I get the xy coordinates

123
00:04:18,10 --> 00:04:20,40
and I use that doing division and modulus

124
00:04:20,40 --> 00:04:22,50
for whatever iteration of the loop I'm on.

125
00:04:22,50 --> 00:04:24,80
And you could take this code right here

126
00:04:24,80 --> 00:04:26,90
and you could run it to see how that determines xy,

127
00:04:26,90 --> 00:04:29,10
but I've also put this little

128
00:04:29,10 --> 00:04:30,40
README file together right here.

129
00:04:30,40 --> 00:04:32,50
And this README file if we had an image

130
00:04:32,50 --> 00:04:35,20
with only these pixels starting at a zero based index,

131
00:04:35,20 --> 00:04:38,00
and I was to do i = 4.

132
00:04:38,00 --> 00:04:40,90
So I was at index position four right here

133
00:04:40,90 --> 00:04:43,20
which would actually be three.

134
00:04:43,20 --> 00:04:45,00
So you could calculate this one your own,

135
00:04:45,00 --> 00:04:46,70
but basically it gives you your row

136
00:04:46,70 --> 00:04:48,40
and your column information.

137
00:04:48,40 --> 00:04:50,90
Alright, and once I have the row and the column information

138
00:04:50,90 --> 00:04:53,30
the x and the y, I could say image at xy

139
00:04:53,30 --> 00:04:55,00
give me the r, g, b, a values.

140
00:04:55,00 --> 00:04:56,80
I could take those r, g, b, a values

141
00:04:56,80 --> 00:05:00,80
and at this entry in my slice so I have pixels

142
00:05:00,80 --> 00:05:05,80
and now I'm accessing by index position, so zero, one, two,

143
00:05:05,80 --> 00:05:09,00
all the way up to the entire index position in my slice.

144
00:05:09,00 --> 00:05:11,20
The slice is giving me a struct.

145
00:05:11,20 --> 00:05:14,10
So that struct that it's giving me is a pixel, right.

146
00:05:14,10 --> 00:05:17,20
And I can access the different members of the pixel,

147
00:05:17,20 --> 00:05:18,60
the different fields of the pixel.

148
00:05:18,60 --> 00:05:21,50
And so the field would be r, g, b, a.

149
00:05:21,50 --> 00:05:23,70
That's what I defined it as, r, g, b, a.

150
00:05:23,70 --> 00:05:26,00
So I'm setting those values for each pixel.

151
00:05:26,00 --> 00:05:27,40
And then once I have all that,

152
00:05:27,40 --> 00:05:29,00
I return my slice of pixel.

153
00:05:29,00 --> 00:05:34,50
Okay, so originally getImages returned a slice of images.

154
00:05:34,50 --> 00:05:37,40
Well now, instead of returning an image here,

155
00:05:37,40 --> 00:05:39,70
we're going to have a slice of pixel

156
00:05:39,70 --> 00:05:41,50
because each image is a bunch of pixels.

157
00:05:41,50 --> 00:05:44,10
And getPixels gives us a slice of pixel.

158
00:05:44,10 --> 00:05:46,30
So instead of returning a slice of image,

159
00:05:46,30 --> 00:05:47,90
we're going to have a slice of pixels

160
00:05:47,90 --> 00:05:50,30
and that's exactly what getImages gives us now,

161
00:05:50,30 --> 00:05:54,60
a slice of images which each image is a bunch of pixels

162
00:05:54,60 --> 00:05:56,10
and we've taken each image

163
00:05:56,10 --> 00:05:58,00
and we've turned it into a slice of pixel.

164
00:05:58,00 --> 00:06:00,90
And so we have a slice of slice of pixel.

165
00:06:00,90 --> 00:06:03,30
So we load our image, we get the image,

166
00:06:03,30 --> 00:06:05,00
and then we get the pixels for each image.

167
00:06:05,00 --> 00:06:07,60
We get the pixels and then we append to images,

168
00:06:07,60 --> 00:06:10,70
all of those pixels and then we return all of our images.

169
00:06:10,70 --> 00:06:13,10
So when we call images, getImages,

170
00:06:13,10 --> 00:06:15,10
it walks this entire directory,

171
00:06:15,10 --> 00:06:16,70
gives us back our slice of slice.

172
00:06:16,70 --> 00:06:19,80
We loop over that and so each time we loop over,

173
00:06:19,80 --> 00:06:23,00
our slice of slice we're down to the slice of pixel.

174
00:06:23,00 --> 00:06:24,50
We loop over the slice of pixel,

175
00:06:24,50 --> 00:06:26,70
it gives us the pixel and we can do this

176
00:06:26,70 --> 00:06:28,90
10 times for each image for the first

177
00:06:28,90 --> 00:06:31,80
10 pixels until we hit 10 and then we break.

178
00:06:31,80 --> 00:06:35,00
So when we run this code,

179
00:06:35,00 --> 00:06:37,40
you can see there it's printing out the resolution

180
00:06:37,40 --> 00:06:40,90
each time, of each image.

181
00:06:40,90 --> 00:06:43,30
And there it's printed out all of the pixel information

182
00:06:43,30 --> 00:06:45,30
for the first 10 pixels in each image.

183
00:06:45,30 --> 00:06:47,10
Pretty amazing, so you can see image zero,

184
00:06:47,10 --> 00:06:50,10
image one, image two, three, four, five, six, seven, eight.

185
00:06:50,10 --> 00:06:51,80
So we did it for eight images.

186
00:06:51,80 --> 00:06:54,30
And there is the resolution for each of them.

187
00:06:54,30 --> 00:06:57,90
One, two, three, four, five, six, seven, eight, nine.

188
00:06:57,90 --> 00:06:59,70
Nine images zero based.

189
00:06:59,70 --> 00:07:01,50
And does that correspond here?

190
00:07:01,50 --> 00:07:05,10
One, two, three, four, five, six, seven, eight, nine.

191
00:07:05,10 --> 00:07:07,80
So that was the big thing to get all of our pixels,

192
00:07:07,80 --> 00:07:09,60
and then the last thing that we did,

193
00:07:09,60 --> 00:07:12,80
was we counted how many pixels were actually touching.

194
00:07:12,80 --> 00:07:14,50
So I created a variable counter

195
00:07:14,50 --> 00:07:17,20
and I'm going to hold down command control G

196
00:07:17,20 --> 00:07:19,50
selects every instance of counter on the page.

197
00:07:19,50 --> 00:07:21,70
And then every time I touch a pixel,

198
00:07:21,70 --> 00:07:24,20
I am just incrementing that counter right here

199
00:07:24,20 --> 00:07:26,20
and then I'm printing that counter out

200
00:07:26,20 --> 00:07:28,10
to see that when this code runs,

201
00:07:28,10 --> 00:07:31,40
I think it was something like 87,000,000 pixel are examined.

202
00:07:31,40 --> 00:07:32,70
And we can see that if we just

203
00:07:32,70 --> 00:07:36,40
change directories and run that.

204
00:07:36,40 --> 00:07:38,00
87,000,000 pixels.

205
00:07:38,00 --> 00:07:40,10
Alright, so the last thing I just have to say,

206
00:07:40,10 --> 00:07:43,00
is that as I was working on this solution,

207
00:07:43,00 --> 00:07:46,30
I consulted with a variety of different people,

208
00:07:46,30 --> 00:07:48,60
some of them really advanced in the field,

209
00:07:48,60 --> 00:07:51,10
and they either said that's your job do your work

210
00:07:51,10 --> 00:07:53,80
or they said this is not trivial.

211
00:07:53,80 --> 00:07:57,90
From very experienced, skilled, talented developers,

212
00:07:57,90 --> 00:08:01,00
they said this code clinic is not trivial.

213
00:08:01,00 --> 00:08:04,20
And I heard that exact phrase from multiple people.

214
00:08:04,20 --> 00:08:05,30
Not trivial.

215
00:08:05,30 --> 00:08:06,60
So I just thought that was interesting

216
00:08:06,60 --> 00:08:08,20
as I sort of talked with other people

217
00:08:08,20 --> 00:08:09,80
and bounced my ideas off other people

218
00:08:09,80 --> 00:08:11,20
and showed them the code I was developing,

219
00:08:11,20 --> 00:08:13,20
they said, this is not trivial.

220
00:08:13,20 --> 00:08:14,90
So if you're feeling like woah,

221
00:08:14,90 --> 00:08:16,10
you're in good company.

222
00:08:16,10 --> 00:08:17,90
There's some really skilled developers out there

223
00:08:17,90 --> 00:08:19,90
who also had a similar experience as you.

224
00:08:19,90 --> 00:08:21,40
But, you know, we're really getting

225
00:08:21,40 --> 00:08:22,70
into some intricacies here

226
00:08:22,70 --> 00:08:24,30
and we're going to continue to do it

227
00:08:24,30 --> 00:08:27,00
because we have some more code to look through here.

