1
00:00:00,50 --> 00:00:03,00
- So I'm just going to jump over a few files here

2
00:00:03,00 --> 00:00:06,20
because I added in some folders in the steps in my process

3
00:00:06,20 --> 00:00:08,90
just so you could see sort of how I was working.

4
00:00:08,90 --> 00:00:10,80
But I just want to sort of stop and highlight

5
00:00:10,80 --> 00:00:12,40
some of the main points, and then we'll just

6
00:00:12,40 --> 00:00:14,20
move right to the solution.

7
00:00:14,20 --> 00:00:15,90
But the things I want to highlight in this video

8
00:00:15,90 --> 00:00:18,50
are filtering down my results,

9
00:00:18,50 --> 00:00:21,00
and then also being able to make another image

10
00:00:21,00 --> 00:00:23,40
which will give me a visual display

11
00:00:23,40 --> 00:00:24,80
of my results so I can see,

12
00:00:24,80 --> 00:00:27,70
ok, a needle matched somewhere in the haystack.

13
00:00:27,70 --> 00:00:29,00
Let me see an image of where

14
00:00:29,00 --> 00:00:30,90
that needle matched in the haystack.

15
00:00:30,90 --> 00:00:32,50
So we're going to create that image,

16
00:00:32,50 --> 00:00:35,40
and it looks like this.

17
00:00:35,40 --> 00:00:36,80
Like that, so that's where that needle

18
00:00:36,80 --> 00:00:38,80
matched in this haystack.

19
00:00:38,80 --> 00:00:40,40
Kind of a crazy looking image.

20
00:00:40,40 --> 00:00:41,50
And then, in addition to that,

21
00:00:41,50 --> 00:00:43,30
I want to show you something that I came across,

22
00:00:43,30 --> 00:00:44,70
which was a little bit interesting

23
00:00:44,70 --> 00:00:46,40
and I didn't totally understand it,

24
00:00:46,40 --> 00:00:48,00
and I explored it a little bit right there.

25
00:00:48,00 --> 00:00:50,10
So we'll take a look at that because, for me,

26
00:00:50,10 --> 00:00:52,00
programming is always kind of like

27
00:00:52,00 --> 00:00:53,80
working with that which you do know

28
00:00:53,80 --> 00:00:55,40
and that which you don't know.

29
00:00:55,40 --> 00:00:56,70
And so here was somewhere where

30
00:00:56,70 --> 00:00:57,90
I hit something I didn't know,

31
00:00:57,90 --> 00:00:59,60
so I just explored it a little bit.

32
00:00:59,60 --> 00:01:01,10
Alright, first of all, let me show you how

33
00:01:01,10 --> 00:01:02,60
I filtered down my results.

34
00:01:02,60 --> 00:01:04,80
To do that I passed in all of my results,

35
00:01:04,80 --> 00:01:06,30
and then I made a map.

36
00:01:06,30 --> 00:01:09,40
A map is a key value data structure,

37
00:01:09,40 --> 00:01:12,70
and I could store in there a name, right?

38
00:01:12,70 --> 00:01:15,80
So the name I'm creating is, for this result

39
00:01:15,80 --> 00:01:18,80
where it's the needle in this haystack,

40
00:01:18,80 --> 00:01:20,20
and I create that name right there

41
00:01:20,20 --> 00:01:21,80
and I'm going to say that's the key,

42
00:01:21,80 --> 00:01:24,90
I'm going to check, is that key already in the map?

43
00:01:24,90 --> 00:01:27,40
And if it is in the map it'll give me my value

44
00:01:27,40 --> 00:01:30,00
and then return a bool here true or false.

45
00:01:30,00 --> 00:01:31,80
I can check that bool right here,

46
00:01:31,80 --> 00:01:34,20
and so if it's already there I'm going to say ok, great.

47
00:01:34,20 --> 00:01:37,30
That value has an average difference.

48
00:01:37,30 --> 00:01:39,40
Is that average difference bigger

49
00:01:39,40 --> 00:01:42,30
than the average difference for the result I'm looping over?

50
00:01:42,30 --> 00:01:45,00
And so if the result is smaller

51
00:01:45,00 --> 00:01:46,90
than what's already stored there,

52
00:01:46,90 --> 00:01:49,50
I'm going to now store that new result,

53
00:01:49,50 --> 00:01:51,90
this one that we're in the iteration or loop on,

54
00:01:51,90 --> 00:01:53,00
into that map.

55
00:01:53,00 --> 00:01:56,30
So it will always store the result

56
00:01:56,30 --> 00:01:58,70
with the smallest avgDiff.

57
00:01:58,70 --> 00:02:01,00
And the else clause here is if

58
00:02:01,00 --> 00:02:02,60
this was not ok, right?

59
00:02:02,60 --> 00:02:05,00
If there was no value already in the map,

60
00:02:05,00 --> 00:02:07,90
then it's going to set at that key this value,

61
00:02:07,90 --> 00:02:09,60
whatever iteration the loop is.

62
00:02:09,60 --> 00:02:11,30
So when I do that inside this map

63
00:02:11,30 --> 00:02:14,60
I will have my smallest avgDiff result.

64
00:02:14,60 --> 00:02:16,50
There you go, and so here's my filtered

65
00:02:16,50 --> 00:02:18,30
list of results and I'm just going to

66
00:02:18,30 --> 00:02:20,60
pend it back to the slice and return that slice.

67
00:02:20,60 --> 00:02:22,00
So that's how I filtered it.

68
00:02:22,00 --> 00:02:24,40
And then to make an image I came over here

69
00:02:24,40 --> 00:02:27,20
and in this file you can see how I made these images,

70
00:02:27,20 --> 00:02:30,10
but the first thing I did is I take

71
00:02:30,10 --> 00:02:33,90
my needle and my haystack and I assign them to n and h.

72
00:02:33,90 --> 00:02:36,20
I'm passing into this function result.

73
00:02:36,20 --> 00:02:39,00
Remember, result has both the needle and the haystack,

74
00:02:39,00 --> 00:02:41,90
right there, so I'm taking the needle and the haystack

75
00:02:41,90 --> 00:02:43,90
and I'm just assigning them to n and h

76
00:02:43,90 --> 00:02:45,20
so I can work with them.

77
00:02:45,20 --> 00:02:47,70
I'm going to open up my haystack,

78
00:02:47,70 --> 00:02:49,50
and so then I'll have an open file,

79
00:02:49,50 --> 00:02:51,60
so I get that open file, the haystack,

80
00:02:51,60 --> 00:02:53,80
and I'm going to create a new file, right here.

81
00:02:53,80 --> 00:02:56,00
So I create a new file, and the name I give it

82
00:02:56,00 --> 00:02:58,10
is this crazy name right here.

83
00:02:58,10 --> 00:02:59,40
And the reason I give it this name

84
00:02:59,40 --> 00:03:02,90
is so that you can see, let me just move this over,

85
00:03:02,90 --> 00:03:05,20
ok, well this file was in that file,

86
00:03:05,20 --> 00:03:06,90
and the difference between them, on average,

87
00:03:06,90 --> 00:03:08,50
for each pixel, is that value, and here are

88
00:03:08,50 --> 00:03:11,20
their Y_X coordinates where they matched, right?

89
00:03:11,20 --> 00:03:13,80
So that's what this entire line right here does,

90
00:03:13,80 --> 00:03:17,20
is it creates this name, kind of a big name.

91
00:03:17,20 --> 00:03:18,50
So I create a new file.

92
00:03:18,50 --> 00:03:20,20
Once I have that new file I have to go about

93
00:03:20,20 --> 00:03:23,10
the business of actually creating a new image.

94
00:03:23,10 --> 00:03:24,60
So I'm going to decode the old file

95
00:03:24,60 --> 00:03:27,50
and I pass in into dof, variable dof,

96
00:03:27,50 --> 00:03:30,70
decoded the old file, and the next thing I want to do

97
00:03:30,70 --> 00:03:34,60
is I want to create pixels in all the areas

98
00:03:34,60 --> 00:03:36,90
which do something visually to show me,

99
00:03:36,90 --> 00:03:40,00
hey, this is where the needle matched in the haystack.

100
00:03:40,00 --> 00:03:44,20
So I'm creating a NewRGBA image from this package.

101
00:03:44,20 --> 00:03:48,80
I aliased the standard library image package to be stdimage.

102
00:03:48,80 --> 00:03:52,20
So I'm creating a NewRGBA of this size

103
00:03:52,20 --> 00:03:54,20
and I'm assigning to m,

104
00:03:54,20 --> 00:03:57,50
and then I'm looping over every pixel in the haystack,

105
00:03:57,50 --> 00:03:59,50
and as I loop over every pixel in the haystack

106
00:03:59,50 --> 00:04:02,10
I get the haystack X and the haystack Y.

107
00:04:02,10 --> 00:04:06,30
And then at each pixel that is in my current haystack,

108
00:04:06,30 --> 00:04:08,60
I'm getting that value for the RGBA

109
00:04:08,60 --> 00:04:12,10
and assigning it to a new variable RGBA.

110
00:04:12,10 --> 00:04:16,70
And then for my new image m I am setting the RGBA

111
00:04:16,70 --> 00:04:21,40
for that new image, the pixel, this XY of that new image,

112
00:04:21,40 --> 00:04:25,50
I'm setting that XY pixel to those RGBA values.

113
00:04:25,50 --> 00:04:29,00
So in effect what I did was from the haystack

114
00:04:29,00 --> 00:04:33,20
I got the RGBA values, these values right here,

115
00:04:33,20 --> 00:04:35,50
and then I assigned them to

116
00:04:35,50 --> 00:04:39,00
the RGBA values in my new image.

117
00:04:39,00 --> 00:04:41,20
So basically my new image has

118
00:04:41,20 --> 00:04:44,50
distorted lower resolution uint8

119
00:04:44,50 --> 00:04:47,90
version of the RGBA values in the original haystack image.

120
00:04:47,90 --> 00:04:49,80
That's why it comes out kind of looking funny,

121
00:04:49,80 --> 00:04:51,50
because I brought the scale down.

122
00:04:51,50 --> 00:04:52,50
So the next thing I need to do is

123
00:04:52,50 --> 00:04:56,50
as I get my needle match I need to change the RGBA values

124
00:04:56,50 --> 00:04:59,20
in my new image to some value different

125
00:04:59,20 --> 00:05:00,30
than what it already is,

126
00:05:00,30 --> 00:05:02,50
so I'm just going to reset it for those areas.

127
00:05:02,50 --> 00:05:04,10
So I need to get some new value

128
00:05:04,10 --> 00:05:05,70
that I'm going to reset those to,

129
00:05:05,70 --> 00:05:08,50
and I'm using the variable d to do that here.

130
00:05:08,50 --> 00:05:11,40
And I'm doing a little calculation between zero and 255

131
00:05:11,40 --> 00:05:13,70
so that the difference in my pixels,

132
00:05:13,70 --> 00:05:15,60
when I divide that by the threshold,

133
00:05:15,60 --> 00:05:19,70
if the difference is zero and the threshold was 500,

134
00:05:19,70 --> 00:05:24,10
then this would be zero, or 1/500 or 2/500.

135
00:05:24,10 --> 00:05:28,70
If the difference was 500 then this would be 500/500 or one.

136
00:05:28,70 --> 00:05:30,70
One minus one goes to zero.

137
00:05:30,70 --> 00:05:32,40
So it's just a little calculation here

138
00:05:32,40 --> 00:05:35,50
to sort of make things go between black and white,

139
00:05:35,50 --> 00:05:36,70
and to give you a visual of that

140
00:05:36,70 --> 00:05:39,90
if RGB is 255 we have white,

141
00:05:39,90 --> 00:05:44,40
and if it's 000

142
00:05:44,40 --> 00:05:45,80
we have black, there's black.

143
00:05:45,80 --> 00:05:47,80
RGB 000 would be black,

144
00:05:47,80 --> 00:05:51,30
so I'm just getting a value between zero and 255

145
00:05:51,30 --> 00:05:55,00
to be some gradations of white to black.

146
00:05:55,00 --> 00:05:58,00
Alright, once I have that I come down here,

147
00:05:58,00 --> 00:06:01,00
and I created a little bit of a frame to go around the image

148
00:06:01,00 --> 00:06:03,60
where d would just be some straightforward value,

149
00:06:03,60 --> 00:06:07,50
and then I reset my new image, the XY values,

150
00:06:07,50 --> 00:06:11,60
for wherever there's a match in the needle to the haystack,

151
00:06:11,60 --> 00:06:14,40
I set those to that sort of black to white

152
00:06:14,40 --> 00:06:16,20
image color right there.

153
00:06:16,20 --> 00:06:19,60
Once that's all done I encode my new jpeg.

154
00:06:19,60 --> 00:06:22,00
So I pass in the new file that I created,

155
00:06:22,00 --> 00:06:23,60
which was up here.

156
00:06:23,60 --> 00:06:26,60
Encode takes a Writer, so I'm going to write to that file.

157
00:06:26,60 --> 00:06:29,30
And file implements the Reader and the Writer interface.

158
00:06:29,30 --> 00:06:32,80
And then I also pass in, Encode also wants the image.

159
00:06:32,80 --> 00:06:34,60
So I pass in that image I created

160
00:06:34,60 --> 00:06:36,20
and it writes it to that file.

161
00:06:36,20 --> 00:06:38,40
So that's how you create an image.

162
00:06:38,40 --> 00:06:41,30
Finally, the last step and the anomaly,

163
00:06:41,30 --> 00:06:42,90
the best thing to do is to see a run,

164
00:06:42,90 --> 00:06:44,90
so I'm just going to come over here to my terminal

165
00:06:44,90 --> 00:06:47,30
and I've got this already up and I'm going to just

166
00:06:47,30 --> 00:06:49,10
do go run main.go.

167
00:06:49,10 --> 00:06:52,80
And I created an image of only 10 pixels wide

168
00:06:52,80 --> 00:06:54,20
and one pixel tall.

169
00:06:54,20 --> 00:06:57,20
And then I did all of my putting the pixels into a slice

170
00:06:57,20 --> 00:07:01,10
and when I loop over it it gives me 11.

171
00:07:01,10 --> 00:07:02,90
And the last pixel was funny,

172
00:07:02,90 --> 00:07:04,50
and so I was just curious why that last picture

173
00:07:04,50 --> 00:07:05,90
was coming out funny.

174
00:07:05,90 --> 00:07:07,60
So I don't have a reason for this,

175
00:07:07,60 --> 00:07:09,10
maybe that's because the Read Write head

176
00:07:09,10 --> 00:07:12,10
is right on the edge of the last pixel,

177
00:07:12,10 --> 00:07:14,30
which would be the next starting pixel.

178
00:07:14,30 --> 00:07:16,90
It's at the beginning even though that pixel's not there,

179
00:07:16,90 --> 00:07:18,80
so it's picking up a little bit of the edge color

180
00:07:18,80 --> 00:07:20,50
or something, I'm not quite sure.

181
00:07:20,50 --> 00:07:24,00
But also, even when I changed that and made it 23,

182
00:07:24,00 --> 00:07:27,50
go run main, I made it 10x2 so now you can see

183
00:07:27,50 --> 00:07:30,70
that this one here is 10x2, this image,

184
00:07:30,70 --> 00:07:33,10
but there's 10, and there's 10,

185
00:07:33,10 --> 00:07:35,00
and then here's my last one right on the edge.

186
00:07:35,00 --> 00:07:36,90
So kind of funny that there's that last pixel

187
00:07:36,90 --> 00:07:38,90
which didn't quite make sense to me.

188
00:07:38,90 --> 00:07:40,90
So you have a visual representation of that.

189
00:07:40,90 --> 00:07:43,00
Here are those images I created.

190
00:07:43,00 --> 00:07:47,00
ten-by-one.jpg, right,

191
00:07:47,00 --> 00:07:48,50
and then also ten-by-two.

192
00:07:48,50 --> 00:07:50,50
So I was just looping over those to check them.

193
00:07:50,50 --> 00:07:52,30
So those were my last steps in the solution.

194
00:07:52,30 --> 00:07:55,00
In the next video we'll see the finished solution.

