1
00:00:00,80 --> 00:00:01,70
- So now we're going to take a look at

2
00:00:01,70 --> 00:00:03,40
these files where we start to work with

3
00:00:03,40 --> 00:00:05,40
the different pieces of data that we're getting from

4
00:00:05,40 --> 00:00:10,00
our images, all those RGBA values, Red, Green, Blue,

5
00:00:10,00 --> 00:00:12,50
Alpha, that's what RGBA stands for.

6
00:00:12,50 --> 00:00:15,90
We are going to look at these four folders right here

7
00:00:15,90 --> 00:00:18,00
and the files in them, before we do that

8
00:00:18,00 --> 00:00:20,50
we're going to do a little bit of housekeeping however,

9
00:00:20,50 --> 00:00:23,50
because you'll notice that the folder names here,

10
00:00:23,50 --> 00:00:26,80
they have changed, this previously went from

11
00:00:26,80 --> 00:00:28,20
one, two, three, four, five,

12
00:00:28,20 --> 00:00:30,40
eight, nine, ten, eleven, twelve,

13
00:00:30,40 --> 00:00:33,00
and then even down here we had 26, 28.

14
00:00:33,00 --> 00:00:35,90
So I've renamed all of these files, and I thought this

15
00:00:35,90 --> 00:00:38,20
would be a cool opportunity to show you how we could

16
00:00:38,20 --> 00:00:41,80
use GitHub and Version Control inside WebStorm.

17
00:00:41,80 --> 00:00:44,00
So I've made some changes to commit that

18
00:00:44,00 --> 00:00:46,50
to my GitHub account, I just come over here to

19
00:00:46,50 --> 00:00:49,00
Version Control, click this link right there,

20
00:00:49,00 --> 00:00:52,60
and I say "changed folder names" as my commit message,

21
00:00:52,60 --> 00:00:54,80
so that was our first bit of housekeeping, and now if I

22
00:00:54,80 --> 00:00:57,30
refresh this, you'll see one, two, three, four, five, six,

23
00:00:57,30 --> 00:00:59,90
seven, eight, nine, and then that 28 and everything,

24
00:00:59,90 --> 00:01:01,20
it's all squared away.

25
00:01:01,20 --> 00:01:02,90
Second piece of housekeeping is in the last video,

26
00:01:02,90 --> 00:01:06,80
I think I didn't finish my thought which I started in that,

27
00:01:06,80 --> 00:01:09,80
when you are programming in Go, a lot of times it's just

28
00:01:09,80 --> 00:01:14,30
int and float64, I started out by saying I was a little

29
00:01:14,30 --> 00:01:16,50
overwhelmed by all these different numeric types,

30
00:01:16,50 --> 00:01:19,10
but really, when you're coding, a lot of times just int

31
00:01:19,10 --> 00:01:21,90
and float64, that's what you're using, so that's the other

32
00:01:21,90 --> 00:01:23,80
piece of housekeeping I just wanted to clean up.

33
00:01:23,80 --> 00:01:26,60
All right, so now we're going to take a look at abstracting

34
00:01:26,60 --> 00:01:29,30
what we've sort of built before, and if we look at the

35
00:01:29,30 --> 00:01:32,90
04 folder, so in 05 we've got the file information,

36
00:01:32,90 --> 00:01:37,10
in 04, we were actually pulling out the RGBA values.

37
00:01:37,10 --> 00:01:40,70
How would you abstract this into its own function?

38
00:01:40,70 --> 00:01:43,70
I want that functionality taken out of Main

39
00:01:43,70 --> 00:01:45,70
and put into its own function.

40
00:01:45,70 --> 00:01:49,30
So if you want, you can download this folder right here, 04,

41
00:01:49,30 --> 00:01:52,30
and try to get it into a function on your own,

42
00:01:52,30 --> 00:01:53,60
and get that function working.

43
00:01:53,60 --> 00:01:55,50
I want to pull this code out of Main.

44
00:01:55,50 --> 00:01:58,70
So from opening a file, two decoding the file, I pulled

45
00:01:58,70 --> 00:02:01,90
that down into this function load image, here I'm opening

46
00:02:01,90 --> 00:02:04,70
the file, and then I'm decoding the file,

47
00:02:04,70 --> 00:02:05,90
and what am I returning?

48
00:02:05,90 --> 00:02:09,00
I'm returning an image, so that's how I did it, so now I can

49
00:02:09,00 --> 00:02:13,90
call in load image, pass in the image I want to load,

50
00:02:13,90 --> 00:02:16,10
the path to that image, and it gives me back an image,

51
00:02:16,10 --> 00:02:20,00
and now I can call image at RGBA and get my RGBA values,

52
00:02:20,00 --> 00:02:22,20
just like I did right there.

53
00:02:22,20 --> 00:02:25,10
So that was me abstracting that functionality, getting it

54
00:02:25,10 --> 00:02:28,10
out of Main, and getting it into its own function.

55
00:02:28,10 --> 00:02:31,00
So the next thing we need to do is we have more than one

56
00:02:31,00 --> 00:02:33,80
image, so here we're only looking at one image,

57
00:02:33,80 --> 00:02:36,60
and I want to look at all of my images, so eventually

58
00:02:36,60 --> 00:02:38,40
I'm going to have to get the image data for all of these

59
00:02:38,40 --> 00:02:40,60
images, which means that somehow I'm going to have

60
00:02:40,60 --> 00:02:42,50
to load each of those images.

61
00:02:42,50 --> 00:02:46,30
Well, in Go there's this really useful function,

62
00:02:46,30 --> 00:02:49,80
it's file path Walk, so there's path, which is a package,

63
00:02:49,80 --> 00:02:53,30
and file path, so file path is a package with a function

64
00:02:53,30 --> 00:02:55,30
Walk, and what does Walk take?

65
00:02:55,30 --> 00:02:58,20
So if we hold down Command, and we click on Walk,

66
00:02:58,20 --> 00:03:01,40
we'll see that Walk takes a route, some straying,

67
00:03:01,40 --> 00:03:03,70
so if I look at how I use that, I passed in,

68
00:03:03,70 --> 00:03:05,90
here's the place I want you to start, the route,

69
00:03:05,90 --> 00:03:08,50
and then it takes a Walk Func.

70
00:03:08,50 --> 00:03:10,40
So this is going to be a little bit complex,

71
00:03:10,40 --> 00:03:13,00
and this is definitely something you'll want to

72
00:03:13,00 --> 00:03:15,30
chew on over time and visit.

73
00:03:15,30 --> 00:03:17,00
If you're an experienced programmer maybe this is

74
00:03:17,00 --> 00:03:19,50
totally going to make sense to you, but I found that

75
00:03:19,50 --> 00:03:23,50
learning to code in Go is kind of like studying Zen,

76
00:03:23,50 --> 00:03:27,00
in the sense that you have cones, they're kind of like

77
00:03:27,00 --> 00:03:29,20
cones, they're these riddles that you have to solve,

78
00:03:29,20 --> 00:03:31,80
and you reflect upon them and then after time

79
00:03:31,80 --> 00:03:33,60
their meaning becomes clear.

80
00:03:33,60 --> 00:03:36,70
So we have Func Walk, we're using file path Walk,

81
00:03:36,70 --> 00:03:41,00
and it takes in its own function, so this is an example

82
00:03:41,00 --> 00:03:45,00
of a callback, and a callback is just a piece of executable

83
00:03:45,00 --> 00:03:47,90
code that's passed as an argument to other code,

84
00:03:47,90 --> 00:03:49,50
which is expected to call back the argument

85
00:03:49,50 --> 00:03:51,00
some convenient time.

86
00:03:51,00 --> 00:03:55,60
So, I am passing in as an argument a function, right,

87
00:03:55,60 --> 00:03:57,80
and it's not a function because that's what this is

88
00:03:57,80 --> 00:04:00,40
named here, it's a function because when you look at

89
00:04:00,40 --> 00:04:05,50
how it's defined, this type, the type is Func, all of those

90
00:04:05,50 --> 00:04:10,10
parameters, and then that return, that is the type.

91
00:04:10,10 --> 00:04:13,70
So functions, you can use it like you use any other type,

92
00:04:13,70 --> 00:04:16,40
so here the language specification, I'm in the language

93
00:04:16,40 --> 00:04:19,50
specification, and we're looking at all the different types

94
00:04:19,50 --> 00:04:22,30
in Go, and one of the types we have in Go obviously

95
00:04:22,30 --> 00:04:25,40
are numeric types, and string types, and slice types,

96
00:04:25,40 --> 00:04:28,20
and you can totally envision those being passed into

97
00:04:28,20 --> 00:04:30,90
a function, being returned from a function, and you can

98
00:04:30,90 --> 00:04:33,10
envision working with them, but then another one

99
00:04:33,10 --> 00:04:36,00
of the types is a function, and that's one of the types,

100
00:04:36,00 --> 00:04:38,50
so just like we could take a numeric type or a string type

101
00:04:38,50 --> 00:04:40,90
and pass that into a function or return it from a function,

102
00:04:40,90 --> 00:04:44,50
we could also pass a function into a function, and we could

103
00:04:44,50 --> 00:04:46,50
also return a function from a function.

104
00:04:46,50 --> 00:04:49,20
So that's what we're doing right here, we are passing

105
00:04:49,20 --> 00:04:54,20
a function into a function, right, so here's the Walk Func,

106
00:04:54,20 --> 00:04:58,40
and Walk Func goes from there all the way down to here,

107
00:04:58,40 --> 00:05:01,30
and that's where it runs, and what we're passing into that,

108
00:05:01,30 --> 00:05:04,50
is this function, and that function runs from here,

109
00:05:04,50 --> 00:05:06,20
all the way to right there.

110
00:05:06,20 --> 00:05:07,40
Well how is that used?

111
00:05:07,40 --> 00:05:10,20
Here you can see I've got path, I've got info,

112
00:05:10,20 --> 00:05:13,90
and I have err, and that's just the way that this function

113
00:05:13,90 --> 00:05:16,70
is defined, a Walk Func, because file path Walk,

114
00:05:16,70 --> 00:05:20,30
just to review this, takes a Walk Func, and a Walk Func

115
00:05:20,30 --> 00:05:23,80
is some function with this signature, so I give it

116
00:05:23,80 --> 00:05:26,80
some function with that signature, so here's my function

117
00:05:26,80 --> 00:05:30,30
with that signature, and then here's the code that

118
00:05:30,30 --> 00:05:33,60
that function is going to run, I put that code in there,

119
00:05:33,60 --> 00:05:36,50
and I'm using some of these arguments that get passed in,

120
00:05:36,50 --> 00:05:40,70
so Walk is going to use this function and it's going to

121
00:05:40,70 --> 00:05:43,10
fill this stuff in, it's going to give me my path,

122
00:05:43,10 --> 00:05:46,60
it's going to give me my OS file info, it'll give me an err

123
00:05:46,60 --> 00:05:48,80
if there's an err, and then I could use path,

124
00:05:48,80 --> 00:05:49,90
I could print that out.

125
00:05:49,90 --> 00:05:52,80
Maybe this'll be a little bit more clear when I run it,

126
00:05:52,80 --> 00:05:55,10
you might want to dig a little bit more into the internals

127
00:05:55,10 --> 00:05:58,20
of this, you might see here's the Walk Func, what does

128
00:05:58,20 --> 00:06:01,20
the Walk Func do, and look through some of this different

129
00:06:01,20 --> 00:06:04,80
stuff, and then also, the Walk here, what does Walk do,

130
00:06:04,80 --> 00:06:07,50
what Walk is going to call Walk, you can start to go

131
00:06:07,50 --> 00:06:09,40
into the internals and see how the people

132
00:06:09,40 --> 00:06:11,90
who wrote Go wrote this function.

133
00:06:11,90 --> 00:06:15,90
That's actually a really good way to learn Go programming

134
00:06:15,90 --> 00:06:18,20
is to look at the standard library and the code

135
00:06:18,20 --> 00:06:20,70
that was used to write the standard library,

136
00:06:20,70 --> 00:06:23,40
because this code here, these are the best examples

137
00:06:23,40 --> 00:06:25,40
and the most accurate examples you can find

138
00:06:25,40 --> 00:06:27,10
as to how to write Go code.

139
00:06:27,10 --> 00:06:28,80
So let's see this run and it'll hopefully become

140
00:06:28,80 --> 00:06:30,40
a little bit more clear.

141
00:06:30,40 --> 00:06:31,50
So the first thing that I'm going to do is

142
00:06:31,50 --> 00:06:37,20
I'm going to go into 07 and then go run main.go

143
00:06:37,20 --> 00:06:39,80
and there it ran and it printed out all of those images,

144
00:06:39,80 --> 00:06:42,50
so I'm accessing those images, so now that I'm

145
00:06:42,50 --> 00:06:44,90
looping over all the images in here recursively,

146
00:06:44,90 --> 00:06:47,50
I could take that and I could pass each image

147
00:06:47,50 --> 00:06:50,50
to load image, and I could start loading each image,

148
00:06:50,50 --> 00:06:52,10
so that's what I'm working towards.

149
00:06:52,10 --> 00:06:54,50
Now, I want to show you this error, so if I put in

150
00:06:54,50 --> 00:06:57,30
the wrong file information here and I didn't have

151
00:06:57,30 --> 00:06:59,80
the right path to that file, what happens?

152
00:06:59,80 --> 00:07:02,30
My program's going to blow up, and it's going to say

153
00:07:02,30 --> 00:07:04,80
"panic runtime error, invalid memory address,

154
00:07:04,80 --> 00:07:08,30
or nil pointer dereference," so I need an underscore

155
00:07:08,30 --> 00:07:11,00
and now it should be running.

156
00:07:11,00 --> 00:07:13,20
Excellent, then the next thing I want to show you is,

157
00:07:13,20 --> 00:07:16,60
I wasn't totally sure how is Walk being implemented,

158
00:07:16,60 --> 00:07:19,20
And so, one thing I wanted to check was to make sure

159
00:07:19,20 --> 00:07:21,70
they weren't using concurrency, and that I wasn't somehow

160
00:07:21,70 --> 00:07:24,40
creating a race condition in my code, which would be

161
00:07:24,40 --> 00:07:27,20
when you have different processes trying to access

162
00:07:27,20 --> 00:07:29,60
the same data at the same time and potentially overriding

163
00:07:29,60 --> 00:07:32,50
each other, so I just ran, just out of curiosity,

164
00:07:32,50 --> 00:07:35,70
this right here, and so that little flag instead of go run

165
00:07:35,70 --> 00:07:39,60
main.go, go run -race main.go, it checks to see

166
00:07:39,60 --> 00:07:42,20
if there's any race condition, and everything's fine,

167
00:07:42,20 --> 00:07:44,30
so I didn't think there would be but that was something

168
00:07:44,30 --> 00:07:46,00
that went through my mind which I also checked.

169
00:07:46,00 --> 00:07:48,00
And then I just want to show you one more deal,

170
00:07:48,00 --> 00:07:52,00
and that is if I bring this up a level.

171
00:07:52,00 --> 00:07:54,40
So I'm just going to go up there and so now I think

172
00:07:54,40 --> 00:07:57,30
I'm looking at my 02 image comparison folder,

173
00:07:57,30 --> 00:08:00,00
and now if I run this file, just go run main,

174
00:08:00,00 --> 00:08:02,60
it's just going to give me my entire directory structure,

175
00:08:02,60 --> 00:08:08,60
02, 01, 00, of everything that was in whatever folder

176
00:08:08,60 --> 00:08:11,60
I was in when I ran that, so you can see how file path Walk

177
00:08:11,60 --> 00:08:14,50
works and you don't have to fully understand it to use it,

178
00:08:14,50 --> 00:08:17,00
but two concepts which are important to take away

179
00:08:17,00 --> 00:08:20,70
is that functions are a type in Go, and we can pass

180
00:08:20,70 --> 00:08:23,70
functions into another function, we can return functions

181
00:08:23,70 --> 00:08:26,40
from a function, just like we could pass a numeric type

182
00:08:26,40 --> 00:08:28,90
or a string type into a function and return those

183
00:08:28,90 --> 00:08:32,00
from a function, we could do the same thing with functions,

184
00:08:32,00 --> 00:08:33,70
passing functions into functions.

185
00:08:33,70 --> 00:08:36,00
So that's one of the main things take away functions

186
00:08:36,00 --> 00:08:37,90
are types, and you can pass them and return them

187
00:08:37,90 --> 00:08:39,70
from functions, and then the other thing is just,

188
00:08:39,70 --> 00:08:42,80
OK, we just put in this Walk Func right here,

189
00:08:42,80 --> 00:08:45,30
and then we could write our own code and start accessing

190
00:08:45,30 --> 00:08:47,70
the file info, the path, so we're going to see

191
00:08:47,70 --> 00:08:50,00
how to use that in the next couple of videos.

