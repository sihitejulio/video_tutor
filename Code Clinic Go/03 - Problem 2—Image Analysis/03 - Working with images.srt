1
00:00:00,50 --> 00:00:04,10
- The first step in figuring out the solution is to first

2
00:00:04,10 --> 00:00:06,30
open a file and then once we have a file opened,

3
00:00:06,30 --> 00:00:07,40
we're going to decode it,

4
00:00:07,40 --> 00:00:09,80
so that we can get the image data out of it.

5
00:00:09,80 --> 00:00:11,30
And then after that, I'm going to show you some

6
00:00:11,30 --> 00:00:13,50
file info, which is one of the steps I took

7
00:00:13,50 --> 00:00:15,10
in putting my solution together.

8
00:00:15,10 --> 00:00:16,80
Didn't turn out to really help me, but I thought

9
00:00:16,80 --> 00:00:18,30
I'd leave it in so you could see it.

10
00:00:18,30 --> 00:00:19,70
So the first thing we do is we open a file,

11
00:00:19,70 --> 00:00:22,20
we use os.Open to do that.

12
00:00:22,20 --> 00:00:24,50
It gives us a pointer to a file.

13
00:00:24,50 --> 00:00:27,30
We have to defer closing that file.

14
00:00:27,30 --> 00:00:29,10
Because we've opened a file, we want to make sure

15
00:00:29,10 --> 00:00:31,70
it closes, and we use defer to do that.

16
00:00:31,70 --> 00:00:33,80
And if we just print this out, we get the address

17
00:00:33,80 --> 00:00:36,10
to the file because it's a pointer to a file,

18
00:00:36,10 --> 00:00:38,80
the address where the file information is kept.

19
00:00:38,80 --> 00:00:40,90
So we do go run main.go.

20
00:00:40,90 --> 00:00:44,20
We see the pointer to the file; we've already seen all that.

21
00:00:44,20 --> 00:00:46,60
The next we do is now that we have our file open,

22
00:00:46,60 --> 00:00:49,00
we want to read the data for that file.

23
00:00:49,00 --> 00:00:51,40
So I use ioutil.ReadAll,

24
00:00:51,40 --> 00:00:54,00
and iouti.ReadAll takes a reader.

25
00:00:54,00 --> 00:00:55,70
I want to take a moment and just revisit how

26
00:00:55,70 --> 00:00:57,70
interfaces work and go.

27
00:00:57,70 --> 00:00:59,80
So, the first thing is that Open gives me

28
00:00:59,80 --> 00:01:01,80
a pointer to a file.

29
00:01:01,80 --> 00:01:05,40
If we go look at the file type, over in godoc.org,

30
00:01:05,40 --> 00:01:08,00
here we have type File, and type File has

31
00:01:08,00 --> 00:01:10,00
this method attached to it.

32
00:01:10,00 --> 00:01:12,60
So in the declaration of a function in Go,

33
00:01:12,60 --> 00:01:15,90
you have the func keyword, then you have the receiver,

34
00:01:15,90 --> 00:01:18,30
if there is one, and then you have the identifier,

35
00:01:18,30 --> 00:01:20,50
the name of the function, and then you have the

36
00:01:20,50 --> 00:01:23,00
parameters that you'll pass arguments into this,

37
00:01:23,00 --> 00:01:24,80
and then here, you have your returns,

38
00:01:24,80 --> 00:01:26,30
and you can have multiple returns.

39
00:01:26,30 --> 00:01:30,10
So, func, receiver, identifier, name of the function,

40
00:01:30,10 --> 00:01:32,00
parameters, returns.

41
00:01:32,00 --> 00:01:35,50
So if it has this receiver, you could put a type in here

42
00:01:35,50 --> 00:01:37,00
as the receiver.

43
00:01:37,00 --> 00:01:40,70
And this type will then attach this function to that type.

44
00:01:40,70 --> 00:01:43,80
So to type File, we have attached this function

45
00:01:43,80 --> 00:01:46,20
which makes this a method of type File.

46
00:01:46,20 --> 00:01:50,20
Since type File has this method, it is implementing

47
00:01:50,20 --> 00:01:51,50
the reader interface.

48
00:01:51,50 --> 00:01:53,90
So the reader interface says, anything that has this

49
00:01:53,90 --> 00:01:58,80
behavior, this method is implementing the reader interface.

50
00:01:58,80 --> 00:02:00,90
I thought I'd just point out, incidentally also this is

51
00:02:00,90 --> 00:02:04,70
the Writer interface, also at godoc.org/io, and

52
00:02:04,70 --> 00:02:06,40
the Writer interface is that.

53
00:02:06,40 --> 00:02:09,70
So question, does type File implement the Writer interface?

54
00:02:09,70 --> 00:02:12,50
Pause this video, look at all these methods,

55
00:02:12,50 --> 00:02:15,90
and see if one of those methods is this method right here.

56
00:02:15,90 --> 00:02:18,80
Is that method right there, also in this list.

57
00:02:18,80 --> 00:02:20,40
And if it is, then type File also

58
00:02:20,40 --> 00:02:22,40
implements the Writer interface.

59
00:02:22,40 --> 00:02:24,40
And indeed, right here, you can see

60
00:02:24,40 --> 00:02:25,80
there's that Write method.

61
00:02:25,80 --> 00:02:27,50
So type File implements both the Writer

62
00:02:27,50 --> 00:02:29,30
and the Reader interface.

63
00:02:29,30 --> 00:02:32,50
This is good to know, because when we have soemthing

64
00:02:32,50 --> 00:02:34,90
like ReadAll, which asks for a reader,

65
00:02:34,90 --> 00:02:38,60
we can now pass a file into that; polymorphism.

66
00:02:38,60 --> 00:02:41,20
So ReadAll returns a slice of byte.

67
00:02:41,20 --> 00:02:43,40
And I get that slice of bytes back,

68
00:02:43,40 --> 00:02:45,40
and when I get it back I just print it out.

69
00:02:45,40 --> 00:02:49,90
So, let's see that run, and change my directories.

70
00:02:49,90 --> 00:02:52,90
And here are all of my bytes being printed out.

71
00:02:52,90 --> 00:02:54,10
So what is a byte?

72
00:02:54,10 --> 00:02:55,30
This is another thing which is really good

73
00:02:55,30 --> 00:02:56,50
to explore in Go.

74
00:02:56,50 --> 00:02:59,10
So I'm at the language spec here, golang.org,

75
00:02:59,10 --> 00:03:02,50
the spec, and I've gone down to look at numeric types.

76
00:03:02,50 --> 00:03:05,70
When I first saw this in Go, I thought, Oh, no!

77
00:03:05,70 --> 00:03:07,80
There are a lot of types there, right?

78
00:03:07,80 --> 00:03:10,10
A lot of different numeric types, and do I have to

79
00:03:10,10 --> 00:03:12,70
really figure out what size I need, when I need it?

80
00:03:12,70 --> 00:03:14,70
I just like to use int and float.

81
00:03:14,70 --> 00:03:17,90
Well, the good news is, is pretty much that's what you do,

82
00:03:17,90 --> 00:03:18,90
you use int.

83
00:03:18,90 --> 00:03:21,80
And so, here's int, it's the same size as a uint.

84
00:03:21,80 --> 00:03:24,70
So that u stands for unsigned.

85
00:03:24,70 --> 00:03:27,10
We have ints and we have unsigned ints.

86
00:03:27,10 --> 00:03:32,00
And so an int is eight bits and it goes from 128 to 127.

87
00:03:32,00 --> 00:03:34,20
It has 256 possibilities.

88
00:03:34,20 --> 00:03:36,40
Why 256 possibilities?

89
00:03:36,40 --> 00:03:40,00
Because if we have 8 bits, we're working in binary,

90
00:03:40,00 --> 00:03:43,20
then two to the power of eight is 256.

91
00:03:43,20 --> 00:03:45,80
Right, that's just the way binary works.

92
00:03:45,80 --> 00:03:48,20
But an unsigned int is going to go, instead of

93
00:03:48,20 --> 00:03:51,20
negative to positive, it just goes from zero forward,

94
00:03:51,20 --> 00:03:53,10
so we got all 256 right there.

95
00:03:53,10 --> 00:03:55,80
So, an int is an unsigned int,

96
00:03:55,80 --> 00:03:59,10
and an unsigned int is either 32 or 64 bits.

97
00:03:59,10 --> 00:04:01,60
What determines if it's 32 or 64 bits?

98
00:04:01,60 --> 00:04:03,50
The architecture of the system.

99
00:04:03,50 --> 00:04:06,50
So if you're on a 64 bit architecture, then you get

100
00:04:06,50 --> 00:04:08,10
to use 64 bits.

101
00:04:08,10 --> 00:04:10,60
If it's 32 bits, then it's 32 bits.

102
00:04:10,60 --> 00:04:13,20
Bill Kennedy, the author of that one book I pointed out,

103
00:04:13,20 --> 00:04:15,90
Go in Action, and also the guy who leads the

104
00:04:15,90 --> 00:04:17,80
Hardcore Go trainings,

105
00:04:17,80 --> 00:04:20,50
he talks about this as mechanical sympathy.

106
00:04:20,50 --> 00:04:22,70
Go has mechanical sympathy.

107
00:04:22,70 --> 00:04:25,00
It looks at the machine it's running on,

108
00:04:25,00 --> 00:04:27,20
and it's sympathetic to that machine;

109
00:04:27,20 --> 00:04:30,80
it works well on that machine.

110
00:04:30,80 --> 00:04:33,00
Alright, so I just wanted to take that quick diversion,

111
00:04:33,00 --> 00:04:35,20
to take a look at what byte is and also to take

112
00:04:35,20 --> 00:04:37,10
a look at the different types here in Go,

113
00:04:37,10 --> 00:04:38,80
and here you can see byte.

114
00:04:38,80 --> 00:04:41,50
And byte is an alias for a uint8.

115
00:04:41,50 --> 00:04:44,40
So uint8, means zero to 255.

116
00:04:44,40 --> 00:04:46,00
And where was it that we saw that?

117
00:04:46,00 --> 00:04:48,10
We saw right here in our code,

118
00:04:48,10 --> 00:04:51,10
that ReadAll gives us a slice of byte, right?

119
00:04:51,10 --> 00:04:53,10
And here is our slice of bytes, and you'll

120
00:04:53,10 --> 00:04:56,40
notice these numbers are all from zero to 255.

121
00:04:56,40 --> 00:05:00,00
And so that's the data that makes up this entire file

122
00:05:00,00 --> 00:05:01,30
when we open this file.

123
00:05:01,30 --> 00:05:02,80
And I find that pretty amazing to be able to see

124
00:05:02,80 --> 00:05:04,60
that data, pretty cool.

125
00:05:04,60 --> 00:05:05,70
The next thing we're going to do is

126
00:05:05,70 --> 00:05:08,50
we're going to look at decoding our image.

127
00:05:08,50 --> 00:05:10,60
Let's open up this file and take a look at it.

128
00:05:10,60 --> 00:05:12,60
There's a bunch of code here, but let me show you

129
00:05:12,60 --> 00:05:14,50
where I started when I was working with this.

130
00:05:14,50 --> 00:05:16,40
I went over to the standard library.

131
00:05:16,40 --> 00:05:17,60
I went to godoc.org.

132
00:05:17,60 --> 00:05:19,80
Actually not the standard library, but godoc.org

133
00:05:19,80 --> 00:05:23,70
where the standard library and third party packages

134
00:05:23,70 --> 00:05:25,20
are all documented.

135
00:05:25,20 --> 00:05:26,80
And I searched for image.

136
00:05:26,80 --> 00:05:29,60
And when I searched for image, I got some documentation

137
00:05:29,60 --> 00:05:32,40
from the standard library, but I also got third party

138
00:05:32,40 --> 00:05:35,10
packages down here; so here are third party packages.

139
00:05:35,10 --> 00:05:37,20
So, I just sort of browsed these when I first

140
00:05:37,20 --> 00:05:38,50
started working here,

141
00:05:38,50 --> 00:05:41,30
and this looks really promising, image and image/jpeg

142
00:05:41,30 --> 00:05:43,00
looks really promising.

143
00:05:43,00 --> 00:05:44,60
So when I came in and looked at image,

144
00:05:44,60 --> 00:05:46,20
I was a little bit overwhelmed, frankly,

145
00:05:46,20 --> 00:05:48,60
because I've never done any image analysis,

146
00:05:48,60 --> 00:05:50,50
and I've never really worked with images.

147
00:05:50,50 --> 00:05:54,20
YCbCr, I had no idea what that was, and quite frankly,

148
00:05:54,20 --> 00:05:55,90
I'm still not totally sure.

149
00:05:55,90 --> 00:05:58,40
RGBA, I recognize that, alright,

150
00:05:58,40 --> 00:06:00,10
but you'll see more about this in a second.

151
00:06:00,10 --> 00:06:01,80
And when I came over to jpeg, I was actually

152
00:06:01,80 --> 00:06:04,50
pretty relieved, because there weren't all that many things

153
00:06:04,50 --> 00:06:06,10
listed here in the index.

154
00:06:06,10 --> 00:06:08,40
And also one of the things listed is Decode,

155
00:06:08,40 --> 00:06:09,80
and Decode takes a Reader.

156
00:06:09,80 --> 00:06:11,20
Well that was really good news to me,

157
00:06:11,20 --> 00:06:15,10
because when I Open my file, which is what I do right here,

158
00:06:15,10 --> 00:06:17,30
when I Open that file, I get a pointer to a file, and

159
00:06:17,30 --> 00:06:20,70
a pointer to File implements the Reader interface, and since

160
00:06:20,70 --> 00:06:25,30
Decode takes a reader, I can pass in that file to Decode.

161
00:06:25,30 --> 00:06:27,30
So one of the things you can do at that point is,

162
00:06:27,30 --> 00:06:29,60
great, I'm just going to Decode that, pass my file in,

163
00:06:29,60 --> 00:06:31,80
it gives me two things back, an image and a error.

164
00:06:31,80 --> 00:06:33,60
I know how to handle an error.

165
00:06:33,60 --> 00:06:36,10
Let me just get my image back and let me just kind of type

166
00:06:36,10 --> 00:06:37,50
with code completion.

167
00:06:37,50 --> 00:06:38,80
This is one of the ways I like to work.

168
00:06:38,80 --> 00:06:41,10
img. and there are some of my options.

169
00:06:41,10 --> 00:06:44,40
I've got At, I've got Bounds, I've got ColorModel.

170
00:06:44,40 --> 00:06:46,30
So that looks promising, and I played with that

171
00:06:46,30 --> 00:06:47,70
a little bit right here.

172
00:06:47,70 --> 00:06:50,00
But I also went in to see what is in Image.

173
00:06:50,00 --> 00:06:51,90
And here you can see I have the exact same thing,

174
00:06:51,90 --> 00:06:54,40
ColorModel, Bounds, and At.

175
00:06:54,40 --> 00:06:56,10
So, with each of these, I sort of explored

176
00:06:56,10 --> 00:06:57,20
them a little further.

177
00:06:57,20 --> 00:06:59,50
In particular, I was kind of curious At(x, y).

178
00:06:59,50 --> 00:07:02,20
So I add a certain point, like okay, a pixel point.

179
00:07:02,20 --> 00:07:04,70
Add a certain pixel point, I get color information.

180
00:07:04,70 --> 00:07:06,20
Well, what's color information?

181
00:07:06,20 --> 00:07:08,30
And when I came here, I was very happy;

182
00:07:08,30 --> 00:07:10,00
I've got RGBA data.

183
00:07:10,00 --> 00:07:11,50
So the next thing was just to figure out how

184
00:07:11,50 --> 00:07:12,90
do I make all of that work.

185
00:07:12,90 --> 00:07:14,90
And I wrote a little code, experimenting with it.

186
00:07:14,90 --> 00:07:17,50
And so Bounds kind of gives you the bounds of the image.

187
00:07:17,50 --> 00:07:20,00
And so here image, you notice when I typed in img.

188
00:07:20,00 --> 00:07:22,20
one of the options was Bounds, right?

189
00:07:22,20 --> 00:07:24,50
So, I just got bounds and what I have available

190
00:07:24,50 --> 00:07:25,70
when I have bounds.?

191
00:07:25,70 --> 00:07:28,50
I've got like, Min, and I've got Max, I've got Add,

192
00:07:28,50 --> 00:07:30,20
I've got At, right?

193
00:07:30,20 --> 00:07:32,10
So, some pretty interesting stuff, ya know,

194
00:07:32,10 --> 00:07:34,70
so I can get a point at a certain pixel,

195
00:07:34,70 --> 00:07:36,30
a certain (x, y) thing.

196
00:07:36,30 --> 00:07:37,50
That's kind of cool.

197
00:07:37,50 --> 00:07:40,10
Once I did bounds.At and I just

198
00:07:40,10 --> 00:07:43,30
passed in 0,0.

199
00:07:43,30 --> 00:07:45,80
And then what do I have available, RGBA.

200
00:07:45,80 --> 00:07:46,70
Isn't that nice?

201
00:07:46,70 --> 00:07:49,40
So, bounds.At give me that RGBA.

202
00:07:49,40 --> 00:07:52,10
That's one of the way you can work with it.

203
00:07:52,10 --> 00:07:53,80
So, you can see me using that right here.

204
00:07:53,80 --> 00:07:56,30
I used bounds.Dx and bounds.Dy and that gave me

205
00:07:56,30 --> 00:07:58,80
my width and my height, and I did a multiplication of

206
00:07:58,80 --> 00:08:01,90
the bounds, like what's the x and what's the y?

207
00:08:01,90 --> 00:08:03,60
And it gave me the total pixels,

208
00:08:03,60 --> 00:08:07,20
and then down here I did img.At(0,0).RGBA().

209
00:08:07,20 --> 00:08:09,70
So right, when I get my image file, what's available to me?

210
00:08:09,70 --> 00:08:13,20
So, I can do image. and then I have At right there,

211
00:08:13,20 --> 00:08:16,90
and if I just pass in another point 0,0,

212
00:08:16,90 --> 00:08:20,30
and then another . RGBA is another choice for me.

213
00:08:20,30 --> 00:08:21,90
So it looked like there was two ways to get

214
00:08:21,90 --> 00:08:23,10
to the same point.

215
00:08:23,10 --> 00:08:26,90
I ended up using this method right here, img.At(0, 0).RGBA()

216
00:08:26,90 --> 00:08:30,20
so I got my first pixel at position 0, 0, which is

217
00:08:30,20 --> 00:08:32,30
the first pixel in an image.

218
00:08:32,30 --> 00:08:34,00
I said give me RGBA,

219
00:08:34,00 --> 00:08:36,90
and that will return four values,

220
00:08:36,90 --> 00:08:39,20
returns RGBA.

221
00:08:39,20 --> 00:08:41,30
And they're all uint32's.

222
00:08:41,30 --> 00:08:43,40
Right, and then I get those and I print them out,

223
00:08:43,40 --> 00:08:45,90
and then I also got my bounds.Dx right here,

224
00:08:45,90 --> 00:08:48,80
so img.At and I did bounds.Dx() which is

225
00:08:48,80 --> 00:08:51,50
the entire x value, and bounds.DY()

226
00:08:51,50 --> 00:08:53,30
which is the entire y value,

227
00:08:53,30 --> 00:08:55,00
give me the RGBA of that.

228
00:08:55,00 --> 00:08:57,50
I got all those RGBA's and I printed out the last pixel,

229
00:08:57,50 --> 00:08:59,60
so let's take a look at that running.

230
00:08:59,60 --> 00:09:03,40
Going to clear my screen, change directories.

231
00:09:03,40 --> 00:09:05,20
There's my RGBA values.

232
00:09:05,20 --> 00:09:09,40
So, the next thing I wanted to do was in file 04,

233
00:09:09,40 --> 00:09:11,00
where I just printed that information out,

234
00:09:11,00 --> 00:09:13,40
so I simplified the code after experimenting,

235
00:09:13,40 --> 00:09:14,90
and I wanted to see the type on that,

236
00:09:14,90 --> 00:09:16,00
which we've already seen.

237
00:09:16,00 --> 00:09:18,50
So I'm printing out the type; I get uint 32.

238
00:09:18,50 --> 00:09:20,80
And print out the values, at this point,

239
00:09:20,80 --> 00:09:22,40
my first pixel right there, and I get the values

240
00:09:22,40 --> 00:09:25,20
of my first pixel, so that's the output of this file.

241
00:09:25,20 --> 00:09:28,40
So, last thing I looked at was just getting some file info,

242
00:09:28,40 --> 00:09:30,50
so you could use once you have a pointer to a file,

243
00:09:30,50 --> 00:09:31,60
you could look at the different methods

244
00:09:31,60 --> 00:09:33,00
that are available to it.

245
00:09:33,00 --> 00:09:36,00
So, I can come over to godoc.org,

246
00:09:36,00 --> 00:09:39,00
and I can go up to my File information,

247
00:09:39,00 --> 00:09:40,30
and I can look at the different methods that

248
00:09:40,30 --> 00:09:43,60
are available to it, and one of the the methods is Stat,

249
00:09:43,60 --> 00:09:45,60
and that gives me back Fileinfo.

250
00:09:45,60 --> 00:09:47,70
And if I look at what Fileinfo is,

251
00:09:47,70 --> 00:09:50,70
it's a interface and it has all these different methods

252
00:09:50,70 --> 00:09:52,50
to it, and so I could look at those different methods

253
00:09:52,50 --> 00:09:54,40
and see what kind of information I get.

254
00:09:54,40 --> 00:09:56,60
And I did that right here.

255
00:09:56,60 --> 00:10:00,00
So, I did f.Stat and gave me fi, and I could call those

256
00:10:00,00 --> 00:10:02,30
different methods to see information.

257
00:10:02,30 --> 00:10:04,10
And I'm going to run that and just take a look

258
00:10:04,10 --> 00:10:05,60
at what I got.

259
00:10:05,60 --> 00:10:08,20
So that last step didn't end up giving me any useful

260
00:10:08,20 --> 00:10:10,00
information for solving my problem,

261
00:10:10,00 --> 00:10:11,90
unless I just sort of wanted to compare file names

262
00:10:11,90 --> 00:10:14,60
or something like that for that really easy solve.

263
00:10:14,60 --> 00:10:17,70
But it was an interesting step to explore, anyhow.

264
00:10:17,70 --> 00:10:20,00
Figuring out our RGBA values was the first

265
00:10:20,00 --> 00:10:21,50
step in creating the solution.

266
00:10:21,50 --> 00:10:23,10
Getting that under our belts, really gets us

267
00:10:23,10 --> 00:10:25,00
on our way to solving this problem.

