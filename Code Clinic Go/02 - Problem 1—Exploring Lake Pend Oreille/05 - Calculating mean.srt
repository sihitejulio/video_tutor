1
00:00:00,50 --> 00:00:03,90
- So let's take a look now at how to calculate our mean,

2
00:00:03,90 --> 00:00:05,50
and then once we calculate our mean,

3
00:00:05,50 --> 00:00:09,50
we're going to abstract that code out into its own function.

4
00:00:09,50 --> 00:00:10,70
So, we'll see how to do that.

5
00:00:10,70 --> 00:00:13,80
And then we'll do a really basic measure of performance,

6
00:00:13,80 --> 00:00:16,90
and we'll use time and just measuring how much time

7
00:00:16,90 --> 00:00:20,60
does our code take to run before it was abstracted

8
00:00:20,60 --> 00:00:22,50
and after it was abstracted.

9
00:00:22,50 --> 00:00:24,60
And then we'll take a look at performance,

10
00:00:24,60 --> 00:00:27,90
and we'll see how much time does it take to run our code

11
00:00:27,90 --> 00:00:31,50
before it was abstracted and after it was abstracted,

12
00:00:31,50 --> 00:00:33,00
and to see if there was any difference.

13
00:00:33,00 --> 00:00:35,30
We'll just do a really basic measure of performance.

14
00:00:35,30 --> 00:00:38,30
Just taking a look at the duration it takes to run

15
00:00:38,30 --> 00:00:40,30
each iteration of code.

16
00:00:40,30 --> 00:00:42,20
So, that's what we're looking at in this video.

17
00:00:42,20 --> 00:00:44,10
Some really interesting stuff.

18
00:00:44,10 --> 00:00:47,60
The first thing to do is to calculate the mean.

19
00:00:47,60 --> 00:00:50,00
And we've already been able to get our data,

20
00:00:50,00 --> 00:00:52,50
and then parse that that into float64.

21
00:00:52,50 --> 00:00:54,80
And so I created a couple of variables.

22
00:00:54,80 --> 00:00:58,00
And here you can see that Go allows you to do

23
00:00:58,00 --> 00:01:01,10
multiple variable assignment and declaration

24
00:01:01,10 --> 00:01:02,40
all at the same time.

25
00:01:02,40 --> 00:01:05,00
And so here I'm just doing the variable declaration.

26
00:01:05,00 --> 00:01:09,50
I'm declaring atTotal, bmTotal, wsTotal, and counter

27
00:01:09,50 --> 00:01:11,60
all as float64s.

28
00:01:11,60 --> 00:01:13,70
Now I'm ranging over all of my records,

29
00:01:13,70 --> 00:01:17,20
and I'm getting my air temperature, my barometric pressure,

30
00:01:17,20 --> 00:01:18,30
my wind speed,

31
00:01:18,30 --> 00:01:22,40
and I'm adding that to my air temperature, my barometric,

32
00:01:22,40 --> 00:01:24,30
and my wind speed totals.

33
00:01:24,30 --> 00:01:27,30
So this plus equals just means add it to this.

34
00:01:27,30 --> 00:01:30,00
And here what we're doing when we use var

35
00:01:30,00 --> 00:01:32,20
versus this shorthand notation,

36
00:01:32,20 --> 00:01:35,30
is that with var we're declaring a variable,

37
00:01:35,30 --> 00:01:38,90
and the variable's being set to its zero value.

38
00:01:38,90 --> 00:01:42,10
So in Go there's a concept of zero value,

39
00:01:42,10 --> 00:01:44,30
And you can go to the language spec

40
00:01:44,30 --> 00:01:47,10
and also to effective Go these two documents.

41
00:01:47,10 --> 00:01:48,70
And you can read about zero value.

42
00:01:48,70 --> 00:01:51,80
It tells you what zero value is for each of the types.

43
00:01:51,80 --> 00:01:56,80
So, var i int is the same thing as var i int equals zero.

44
00:01:56,80 --> 00:01:59,40
So, when we use var like that to declare a variable,

45
00:01:59,40 --> 00:02:01,90
we're setting the variable to its zero value

46
00:02:01,90 --> 00:02:03,90
without doing any type of an assignment here.

47
00:02:03,90 --> 00:02:06,80
So, these are all set to zero dot zero.

48
00:02:06,80 --> 00:02:09,90
And then I'm adding values float64s into that,

49
00:02:09,90 --> 00:02:11,50
and I'm incrementing my counter.

50
00:02:11,50 --> 00:02:14,30
So, now to get my mean all I have to do is take the total

51
00:02:14,30 --> 00:02:18,10
and divide it by the number of items that were added up.

52
00:02:18,10 --> 00:02:20,70
And that's going to give me my mean or my average.

53
00:02:20,70 --> 00:02:22,00
So, very basic.

54
00:02:22,00 --> 00:02:23,70
Now, a question for you.

55
00:02:23,70 --> 00:02:26,50
How would we take this code and abstract it

56
00:02:26,50 --> 00:02:28,30
into its own function?

57
00:02:28,30 --> 00:02:30,50
And that's a really good challenge also.

58
00:02:30,50 --> 00:02:32,30
So, you might want to pause the video right now,

59
00:02:32,30 --> 00:02:34,30
and start with this code right here,

60
00:02:34,30 --> 00:02:37,10
and then abstract it into it's own function,

61
00:02:37,10 --> 00:02:38,90
and get the same functionality.

62
00:02:38,90 --> 00:02:41,00
Get the same result when it runs.

63
00:02:41,00 --> 00:02:42,80
So, let's take a look at how we do that.

64
00:02:42,80 --> 00:02:44,10
But first, before we take a look

65
00:02:44,10 --> 00:02:45,50
at how to abstract this code,

66
00:02:45,50 --> 00:02:47,40
let's watch this file run.

67
00:02:47,40 --> 00:02:49,70
It's always fun to watch the code run.

68
00:02:49,70 --> 00:02:52,30
So, currently, where I am at?

69
00:02:52,30 --> 00:02:54,00
I am at 0202.

70
00:02:54,00 --> 00:02:55,20
I am way down the tree,

71
00:02:55,20 --> 00:02:57,20
so I'm going to come up a couple levels.

72
00:02:57,20 --> 00:02:59,20
And I'll just take look to see where I'm at.

73
00:02:59,20 --> 00:03:00,70
Come up one more level.

74
00:03:00,70 --> 00:03:04,90
And print working directory, I'm in 01 parse weather data.

75
00:03:04,90 --> 00:03:05,80
Perfect.

76
00:03:05,80 --> 00:03:11,80
And now I'm going to go into 07 and go run main.go.

77
00:03:11,80 --> 00:03:14,60
So, there's the results of this calculations.

78
00:03:14,60 --> 00:03:16,70
So, now to abstract the code.

79
00:03:16,70 --> 00:03:19,50
To abstract this code, I created another function,

80
00:03:19,50 --> 00:03:21,20
and I called it mean.

81
00:03:21,20 --> 00:03:24,50
And this function's going to take in multidimensional slice.

82
00:03:24,50 --> 00:03:27,20
So slice with a slice of strings,

83
00:03:27,20 --> 00:03:29,00
because I have all of my records.

84
00:03:29,00 --> 00:03:32,00
And each record is made up of a couple of different fields.

85
00:03:32,00 --> 00:03:34,90
And I'm also going to pass in an index position.

86
00:03:34,90 --> 00:03:37,20
So, again, I'm going to create a total.

87
00:03:37,20 --> 00:03:40,30
I'm going to loop over all of rows,

88
00:03:40,30 --> 00:03:43,10
which will, each time it loops, it'll give me a row.

89
00:03:43,10 --> 00:03:44,50
Which is what I asked for.

90
00:03:44,50 --> 00:03:48,10
And then for each row I'm going to access a piece of data

91
00:03:48,10 --> 00:03:49,70
at a certain index.

92
00:03:49,70 --> 00:03:52,10
So, when I call this mean, I could say,

93
00:03:52,10 --> 00:03:54,30
"Hey, I want the data index position one,"

94
00:03:54,30 --> 00:03:55,90
and that will give me our temperature.

95
00:03:55,90 --> 00:03:58,70
"I want the data at index position two."

96
00:03:58,70 --> 00:04:02,50
So for each record there's fields that make up that record.

97
00:04:02,50 --> 00:04:06,40
And I'm asking for the field at this index position,

98
00:04:06,40 --> 00:04:08,20
which will be barometric pressure.

99
00:04:08,20 --> 00:04:10,90
And then right here, for wind speed it's at that position.

100
00:04:10,90 --> 00:04:13,40
So, I pass in my multidimensional slice,

101
00:04:13,40 --> 00:04:16,00
and I say, "Give me the data at a certain position,"

102
00:04:16,00 --> 00:04:19,50
and I turn that into float64, add it to total.

103
00:04:19,50 --> 00:04:20,70
And then when I get total,

104
00:04:20,70 --> 00:04:23,10
I divide it by the complete count,

105
00:04:23,10 --> 00:04:26,70
which is the length of the rows that I passed in.

106
00:04:26,70 --> 00:04:30,10
And then I subtract one since it's a zero based index.

107
00:04:30,10 --> 00:04:31,80
And that gives me my mean.

108
00:04:31,80 --> 00:04:34,00
So, that's how I abstracted this code.

109
00:04:34,00 --> 00:04:36,10
And I left the length up there just as it is.

110
00:04:36,10 --> 00:04:38,20
So, that was my solution to abstracting the code.

111
00:04:38,20 --> 00:04:40,90
Last thing for us to look at is this code here

112
00:04:40,90 --> 00:04:42,40
any faster than that code.

113
00:04:42,40 --> 00:04:43,20
What do you think?

114
00:04:43,20 --> 00:04:44,50
So, let's go in and take a look.

115
00:04:44,50 --> 00:04:46,20
I'm going to clear out the screen,

116
00:04:46,20 --> 00:04:49,90
go into 09, and then go into 01.

117
00:04:49,90 --> 00:04:53,40
So, we'll run this code go run main.go,

118
00:04:53,40 --> 00:04:54,90
and the first file ran,

119
00:04:54,90 --> 00:04:59,00
and the unabstracted version took 21.5 milliseconds.

120
00:04:59,00 --> 00:05:02,30
And now just to see where I'm at, I'm in 01,

121
00:05:02,30 --> 00:05:05,30
and go up a level and then come back into 02,

122
00:05:05,30 --> 00:05:08,50
and go run this code.

123
00:05:08,50 --> 00:05:12,30
And the abstracted version took two milliseconds longer.

124
00:05:12,30 --> 00:05:14,90
So, let's take a look at how did I abstract that code

125
00:05:14,90 --> 00:05:17,10
and add timing onto it.

126
00:05:17,10 --> 00:05:19,70
So, here I start time.now.

127
00:05:19,70 --> 00:05:21,00
I get the current time.

128
00:05:21,00 --> 00:05:22,10
And once I have the current time,

129
00:05:22,10 --> 00:05:22,90
when I'm all finished,

130
00:05:22,90 --> 00:05:25,60
I could do end time.now.

131
00:05:25,60 --> 00:05:30,40
And then I get the change, the delta end.sub start.

132
00:05:30,40 --> 00:05:34,50
And so, end is, I could click right here to see what now is.

133
00:05:34,50 --> 00:05:36,60
Now returns a time.

134
00:05:36,60 --> 00:05:38,60
And so when I have a time,

135
00:05:38,60 --> 00:05:43,00
there is a method attached to the type time called sub.

136
00:05:43,00 --> 00:05:47,90
So, if I go over to Godoc.org is right here

137
00:05:47,90 --> 00:05:51,00
and here are all the methods associated with time.

138
00:05:51,00 --> 00:05:53,50
Time has a lot of methods associated with it,

139
00:05:53,50 --> 00:05:54,60
and here are some.

140
00:05:54,60 --> 00:05:56,80
So, any time I have something of type time,

141
00:05:56,80 --> 00:05:58,80
I have the method sub, which I could call.

142
00:05:58,80 --> 00:06:02,10
And it returns the duration between two times.

143
00:06:02,10 --> 00:06:05,30
And so that's where that code came from right there

144
00:06:05,30 --> 00:06:06,70
and how to read that.

145
00:06:06,70 --> 00:06:07,30
All right.

146
00:06:07,30 --> 00:06:09,60
There's a little bit about calculating the mean.

147
00:06:09,60 --> 00:06:11,20
In the next video we're going to take a look at

148
00:06:11,20 --> 00:06:13,00
how do we calculate the median,

149
00:06:13,00 --> 00:06:16,00
and then after that we'll see how to use http kit.

