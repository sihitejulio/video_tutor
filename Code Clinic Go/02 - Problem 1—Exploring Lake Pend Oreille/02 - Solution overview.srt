1
00:00:00,60 --> 00:00:03,00
- So here's my solution for parsing weather data

2
00:00:03,00 --> 00:00:04,60
and calculating statistics.

3
00:00:04,60 --> 00:00:06,90
And then all of these folders right here

4
00:00:06,90 --> 00:00:09,70
are the steps I took to reach this solution.

5
00:00:09,70 --> 00:00:11,80
So let's open up my solution and run it

6
00:00:11,80 --> 00:00:13,50
and then we'll take a look at the code.

7
00:00:13,50 --> 00:00:15,10
So here's the solution and I'm going to

8
00:00:15,10 --> 00:00:17,90
go over to my terminal and clear this out

9
00:00:17,90 --> 00:00:20,80
and then just do go run main.go

10
00:00:20,80 --> 00:00:22,80
and I can run that command right there because

11
00:00:22,80 --> 00:00:27,00
I'm already in the 16 error handling finished folder.

12
00:00:27,00 --> 00:00:29,10
So if I look to see where I'm at you can see

13
00:00:29,10 --> 00:00:30,40
I'm in that folder.

14
00:00:30,40 --> 00:00:32,70
When that solution ran you can see it calculated

15
00:00:32,70 --> 00:00:35,60
all the statistics and showed me how many records

16
00:00:35,60 --> 00:00:38,40
it calculated it on and there's my solution.

17
00:00:38,40 --> 00:00:41,40
So let's take a high overview look and see kind

18
00:00:41,40 --> 00:00:44,60
of the general forest of how I put the solution together.

19
00:00:44,60 --> 00:00:46,20
And then in the next videos we'll get down

20
00:00:46,20 --> 00:00:49,00
into the trees and look in more detail and more depth

21
00:00:49,00 --> 00:00:51,00
at each of the different steps I took.

22
00:00:51,00 --> 00:00:53,10
So here's the high level solution.

23
00:00:53,10 --> 00:00:56,90
So when I try to understand code I always

24
00:00:56,90 --> 00:00:58,90
take a look at where does this code start?

25
00:00:58,90 --> 00:01:01,10
And in Go code starts in func main,

26
00:01:01,10 --> 00:01:02,50
like in many languages.

27
00:01:02,50 --> 00:01:05,10
And the first thing I do is I'm doing an http get call

28
00:01:05,10 --> 00:01:07,40
and then I'm going to read that data.

29
00:01:07,40 --> 00:01:09,40
And again, we'll see this in more detail later.

30
00:01:09,40 --> 00:01:12,30
And then down here I have a variety of different functions.

31
00:01:12,30 --> 00:01:16,00
I get the length, I get the mean, and I also get the median.

32
00:01:16,00 --> 00:01:18,30
And what I'm passing in there is all that data that

33
00:01:18,30 --> 00:01:22,30
I got when I did my http get request and asked

34
00:01:22,30 --> 00:01:23,40
for that data.

35
00:01:23,40 --> 00:01:25,30
So each of these functions run and return

36
00:01:25,30 --> 00:01:28,10
the results and then they print the results out.

37
00:01:28,10 --> 00:01:29,40
So the next videos we'll take a look

38
00:01:29,40 --> 00:01:31,00
at the details of the solution.

