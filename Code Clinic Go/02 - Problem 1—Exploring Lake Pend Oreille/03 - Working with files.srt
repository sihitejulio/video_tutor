1
00:00:00,50 --> 00:00:03,60
- So let's take a look at my solution in more depth now.

2
00:00:03,60 --> 00:00:05,00
Before we get to the point where we're actually

3
00:00:05,00 --> 00:00:06,90
going and getting the data

4
00:00:06,90 --> 00:00:09,30
off of the internet using HTTP GET

5
00:00:09,30 --> 00:00:12,90
and culling that data and bringing it down programmatically,

6
00:00:12,90 --> 00:00:14,70
I went and got the file manually.

7
00:00:14,70 --> 00:00:16,70
I downloaded it and I put it right here,

8
00:00:16,70 --> 00:00:18,40
and now we're just going to open the file

9
00:00:18,40 --> 00:00:20,30
and then we're going to read the file.

10
00:00:20,30 --> 00:00:22,30
Something I want to point out before we even

11
00:00:22,30 --> 00:00:25,40
start in doing that, if you want to take a deeper dive

12
00:00:25,40 --> 00:00:28,50
into my thought process behind building this code,

13
00:00:28,50 --> 00:00:30,20
you could open up these README files,

14
00:00:30,20 --> 00:00:32,30
which are included in each section.

15
00:00:32,30 --> 00:00:35,60
And you could see all of my thoughts involved with

16
00:00:35,60 --> 00:00:37,10
how does Open work,

17
00:00:37,10 --> 00:00:38,90
or how do interfaces work,

18
00:00:38,90 --> 00:00:41,90
or what are the different ways I could read a file?

19
00:00:41,90 --> 00:00:43,80
So ultimately to read a file I'm going to choose

20
00:00:43,80 --> 00:00:47,40
the csv.NewReader, comma separated value.

21
00:00:47,40 --> 00:00:49,20
We'll see why in just a second.

22
00:00:49,20 --> 00:00:50,80
So let's start with this first one

23
00:00:50,80 --> 00:00:52,90
where we are opening a file.

24
00:00:52,90 --> 00:00:54,40
And when you choose to open a file

25
00:00:54,40 --> 00:00:57,70
something that's really cool is that here in WebStorm

26
00:00:57,70 --> 00:01:00,30
I could hold down Command on my keyboard

27
00:01:00,30 --> 00:01:02,50
and when I move my mouse over Open

28
00:01:02,50 --> 00:01:04,50
I could click on that, and it's going to

29
00:01:04,50 --> 00:01:07,30
take me to the internals of Go.

30
00:01:07,30 --> 00:01:09,50
This is the standard library right here,

31
00:01:09,50 --> 00:01:12,70
and this is the documentation which is built

32
00:01:12,70 --> 00:01:14,70
for the standard library, the documentation

33
00:01:14,70 --> 00:01:16,50
is built right off of the comments.

34
00:01:16,50 --> 00:01:19,10
And you could see her that I have func Open,

35
00:01:19,10 --> 00:01:21,70
and this comment begins with Open.

36
00:01:21,70 --> 00:01:23,50
And that's Idiomatic Go,

37
00:01:23,50 --> 00:01:25,60
that's the way you write a comment so that it

38
00:01:25,60 --> 00:01:27,90
ends up well in the documentation.

39
00:01:27,90 --> 00:01:30,20
So, the cool thing is, is when I hold down

40
00:01:30,20 --> 00:01:31,90
Command in WebStorm I could go

41
00:01:31,90 --> 00:01:33,80
straight to that documentation.

42
00:01:33,80 --> 00:01:35,30
And I'm not sure if it works that way in

43
00:01:35,30 --> 00:01:37,40
Atom or Sublime or other editors,

44
00:01:37,40 --> 00:01:40,60
but that's one of the things I really love about WebStorm.

45
00:01:40,60 --> 00:01:42,90
So looking at this, the internals of the language,

46
00:01:42,90 --> 00:01:44,50
how's the standard library working,

47
00:01:44,50 --> 00:01:46,90
I cull func Open from the OS package,

48
00:01:46,90 --> 00:01:49,90
and it takes a name, the file I want to open,

49
00:01:49,90 --> 00:01:53,50
and it returns a pointer to a file and an error.

50
00:01:53,50 --> 00:01:56,10
This is another thing which is really cool about Go.

51
00:01:56,10 --> 00:01:58,00
You could have multiple returns,

52
00:01:58,00 --> 00:02:01,40
and so it's really idiomatic, meaning these are the idioms,

53
00:02:01,40 --> 00:02:04,50
the patterns of speech by which we write Go code,

54
00:02:04,50 --> 00:02:07,20
it's very idiomatic to handle your errs

55
00:02:07,20 --> 00:02:09,00
right where they're occurring.

56
00:02:09,00 --> 00:02:11,90
There's not any, like try-catch kind of stuff.

57
00:02:11,90 --> 00:02:13,90
We do have something that sort of approximates that

58
00:02:13,90 --> 00:02:17,10
with panic and recover, but having multiple returns

59
00:02:17,10 --> 00:02:19,70
and handling an err right where it might be occurring,

60
00:02:19,70 --> 00:02:21,50
this is very idiomatic Go ,

61
00:02:21,50 --> 00:02:23,20
this is the way you write Go code.

62
00:02:23,20 --> 00:02:24,60
So I open my file.

63
00:02:24,60 --> 00:02:26,80
Once a file is opened it's going to give me

64
00:02:26,80 --> 00:02:28,90
both a pointer to a file and the err.

65
00:02:28,90 --> 00:02:31,10
I check to see if that err is not nil,

66
00:02:31,10 --> 00:02:32,40
meaning there's an err.

67
00:02:32,40 --> 00:02:34,20
I panic and close the program.

68
00:02:34,20 --> 00:02:37,70
Otherwise, I'm going to defer closing my file.

69
00:02:37,70 --> 00:02:40,00
So when you open a file, if you program,

70
00:02:40,00 --> 00:02:42,20
you're very aware that you can't just continually

71
00:02:42,20 --> 00:02:44,80
open file after file after file.

72
00:02:44,80 --> 00:02:47,50
That will use up all of the resources on your computer.

73
00:02:47,50 --> 00:02:50,00
So when you open a file you want to make sure you close it.

74
00:02:50,00 --> 00:02:52,20
Well there's this really nice thing you can do in Go.

75
00:02:52,20 --> 00:02:54,90
You just say defer and then put in, you know,

76
00:02:54,90 --> 00:02:57,70
here's the file and then a function attached to the file,

77
00:02:57,70 --> 00:03:00,10
method attached to the file, Close.

78
00:03:00,10 --> 00:03:02,00
And that's going to close the file.

79
00:03:02,00 --> 00:03:05,10
What defer does is right before this function executes

80
00:03:05,10 --> 00:03:07,50
it'll run any code inside that function

81
00:03:07,50 --> 00:03:09,00
which has been deferred,

82
00:03:09,00 --> 00:03:12,10
and so this will close the file right before main executes.

83
00:03:12,10 --> 00:03:15,20
And then, finally, what I do is I print to see what is f,

84
00:03:15,20 --> 00:03:17,50
and when I print what is f, this is what I get.

85
00:03:17,50 --> 00:03:19,60
I get a pointer to a memory address

86
00:03:19,60 --> 00:03:21,10
where that file is stored.

87
00:03:21,10 --> 00:03:22,80
So that was the first thing we needed to do.

88
00:03:22,80 --> 00:03:23,80
We opened a file.

89
00:03:23,80 --> 00:03:26,70
The next thing we need to do is we need to read that file.

90
00:03:26,70 --> 00:03:28,20
If I look at this README once again,

91
00:03:28,20 --> 00:03:30,80
here are some different choices I have for reading a file.

92
00:03:30,80 --> 00:03:35,30
I'm going to go with csv.NewReader and see how that works.

93
00:03:35,30 --> 00:03:37,30
You can see I've got my file, deferred the Close,

94
00:03:37,30 --> 00:03:40,50
and then I do csv.NewReader, I get my Reader,

95
00:03:40,50 --> 00:03:42,40
and then I do a rdr.ReadAll.

96
00:03:42,40 --> 00:03:43,90
Again, I can hold down Command

97
00:03:43,90 --> 00:03:45,70
and I could go to the internals and take a look

98
00:03:45,70 --> 00:03:47,80
to see what ReadAll is doing.

99
00:03:47,80 --> 00:03:50,10
So you could see here from the documentation

100
00:03:50,10 --> 00:03:53,60
that ReadAll reads all the remaining records from r.

101
00:03:53,60 --> 00:03:56,30
So r is what was passed in, it's the argument,

102
00:03:56,30 --> 00:03:58,30
and it's a pointer to a Reader.

103
00:03:58,30 --> 00:04:00,90
We took our file, which is right here,

104
00:04:00,90 --> 00:04:03,30
and we made it with csv.NewReader

105
00:04:03,30 --> 00:04:04,70
by passing our file into that,

106
00:04:04,70 --> 00:04:07,90
because NewReader will take an io.Reader,

107
00:04:07,90 --> 00:04:10,40
and an io.Reader is an interface

108
00:04:10,40 --> 00:04:14,00
and a file implements the Reader interface,

109
00:04:14,00 --> 00:04:16,70
and this is how we do polymorphism in Go,

110
00:04:16,70 --> 00:04:18,70
and I know I just said a lot of buzzwords.

111
00:04:18,70 --> 00:04:20,00
And if you're new to programming

112
00:04:20,00 --> 00:04:21,80
that's going to be a little bit mysterious,

113
00:04:21,80 --> 00:04:24,40
and that could two videos in and of itself

114
00:04:24,40 --> 00:04:26,30
talking about interfaces,

115
00:04:26,30 --> 00:04:29,20
but this is how we implement Go code.

116
00:04:29,20 --> 00:04:31,00
And so we pass in that file right here,

117
00:04:31,00 --> 00:04:33,30
and we can do that because NewReader wants a Reader,

118
00:04:33,30 --> 00:04:35,50
and the file implements the Reader interface.

119
00:04:35,50 --> 00:04:37,90
There's certain behavior defined

120
00:04:37,90 --> 00:04:39,80
by methods attached to a file

121
00:04:39,80 --> 00:04:41,70
that allow it to implement that interface.

122
00:04:41,70 --> 00:04:44,50
So we get a Reader and then we do ReadAll.

123
00:04:44,50 --> 00:04:47,00
And when we do ReadAll, we already saw what that does,

124
00:04:47,00 --> 00:04:49,50
it returns a slice of slice of strings.

125
00:04:49,50 --> 00:04:52,10
So each record is a slice of fields.

126
00:04:52,10 --> 00:04:53,20
What does that mean?

127
00:04:53,20 --> 00:04:55,90
If we open up our data, and I'm going to come up here

128
00:04:55,90 --> 00:04:58,50
and I'm going to click right there to get rid of

129
00:04:58,50 --> 00:05:01,40
all this directory over on the left,

130
00:05:01,40 --> 00:05:03,40
and you could see here we have our data.

131
00:05:03,40 --> 00:05:05,90
So each record is a slice of fields.

132
00:05:05,90 --> 00:05:07,40
Well, here we have a record.

133
00:05:07,40 --> 00:05:10,40
A record is one entry in a bunch of data,

134
00:05:10,40 --> 00:05:12,10
so there's one record.

135
00:05:12,10 --> 00:05:14,40
And we saw in the documentation that

136
00:05:14,40 --> 00:05:16,60
each record is a slice of field.

137
00:05:16,60 --> 00:05:19,90
So a slice is a list, so here might be one field,

138
00:05:19,90 --> 00:05:22,20
another field, another field, another field.

139
00:05:22,20 --> 00:05:24,70
So some of the terminology that's being used here

140
00:05:24,70 --> 00:05:27,90
is kind of like from databases, where we have records

141
00:05:27,90 --> 00:05:29,50
and then we have fields,

142
00:05:29,50 --> 00:05:31,70
records made up of a bunch of different fields.

143
00:05:31,70 --> 00:05:34,30
So each record is a slice of fields,

144
00:05:34,30 --> 00:05:36,80
and then if we were to get a whole bunch of records,

145
00:05:36,80 --> 00:05:38,80
and so here you could see different records,

146
00:05:38,80 --> 00:05:41,40
we could create another slice, or another list,

147
00:05:41,40 --> 00:05:43,20
with all of those records in it.

148
00:05:43,20 --> 00:05:46,10
So we would have, first, a list or a slice,

149
00:05:46,10 --> 00:05:48,20
with all those fields, and then we'd have

150
00:05:48,20 --> 00:05:51,70
another list or slice with all those records.

151
00:05:51,70 --> 00:05:55,60
And so what that ends up being is a multidimensional slice.

152
00:05:55,60 --> 00:05:59,60
And so in Go slices are built on top of arrays

153
00:05:59,60 --> 00:06:03,70
and slices are dynamic, they can grow and shrink,

154
00:06:03,70 --> 00:06:05,80
whereas arrays are of thick size,

155
00:06:05,80 --> 00:06:10,30
so we use slices to create dynamic lists of data.

156
00:06:10,30 --> 00:06:12,50
And here we have a multidimensional slice

157
00:06:12,50 --> 00:06:15,90
to hold every single record.

158
00:06:15,90 --> 00:06:17,40
So there's the record, and each record

159
00:06:17,40 --> 00:06:19,70
is made up of a different field.

160
00:06:19,70 --> 00:06:22,80
So that's the data structure I'm getting right there.

161
00:06:22,80 --> 00:06:25,60
So we do ReadAll, we get back all of our rows,

162
00:06:25,60 --> 00:06:27,70
and then I'm going to loop over all my rows.

163
00:06:27,70 --> 00:06:29,80
And the way I get all of my data in Go

164
00:06:29,80 --> 00:06:33,30
is I use the range keyword with the for loop.

165
00:06:33,30 --> 00:06:35,70
I do for, range, and then I list

166
00:06:35,70 --> 00:06:37,80
some sort of iterable object,

167
00:06:37,80 --> 00:06:39,50
something I could iterate over.

168
00:06:39,50 --> 00:06:41,90
Some sort of iterable data structure.

169
00:06:41,90 --> 00:06:44,80
And then range is going to return both,

170
00:06:44,80 --> 00:06:46,40
if it was like a slice it's going to return

171
00:06:46,40 --> 00:06:48,90
the index and the value,

172
00:06:48,90 --> 00:06:51,50
and so if i wanted to I could call it i, v,

173
00:06:51,50 --> 00:06:53,00
like that, index, value.

174
00:06:53,00 --> 00:06:56,60
And if it is a map they return the key and the value,

175
00:06:56,60 --> 00:06:58,50
so I could also do it that way.

176
00:06:58,50 --> 00:07:00,20
But these are just random variable names,

177
00:07:00,20 --> 00:07:03,80
and you'll notice that what I had before was an underscore.

178
00:07:03,80 --> 00:07:05,90
So an underscore means, you know what,

179
00:07:05,90 --> 00:07:08,60
I don't need the index so just throw that away,

180
00:07:08,60 --> 00:07:09,40
I'm not using it.

181
00:07:09,40 --> 00:07:12,20
I'm aware that it's returned, I just don't want to use it.

182
00:07:12,20 --> 00:07:14,00
And so that's how you tell the compiler

183
00:07:14,00 --> 00:07:15,70
I'm not missing anything here,

184
00:07:15,70 --> 00:07:18,20
I haven't declared a variable that I'm not using,

185
00:07:18,20 --> 00:07:19,70
and the data that's coming back

186
00:07:19,70 --> 00:07:22,80
I'm just letting that be thrown away into emptiness.

187
00:07:22,80 --> 00:07:25,20
So I get all my rows and I loop over my rows,

188
00:07:25,20 --> 00:07:26,70
so now when I run this

189
00:07:26,70 --> 00:07:28,00
I'm going to change directories,

190
00:07:28,00 --> 00:07:32,20
go up one level, go run main.

191
00:07:32,20 --> 00:07:34,90
There is all my data printing out on the screen.

192
00:07:34,90 --> 00:07:36,20
And that's beautiful.

193
00:07:36,20 --> 00:07:39,30
So you'll notice here that each record

194
00:07:39,30 --> 00:07:42,70
is completely contained between brackets.

195
00:07:42,70 --> 00:07:46,40
So that tells me that this is all currently one field.

196
00:07:46,40 --> 00:07:48,10
And I'm going to have to break that up

197
00:07:48,10 --> 00:07:51,60
which is what I'm doing in this next file right here.

198
00:07:51,60 --> 00:07:53,30
Down here, where I change the delimiter,

199
00:07:53,30 --> 00:07:54,70
that's where I'm going to break it up.

200
00:07:54,70 --> 00:07:55,90
The last thing I want to show you before

201
00:07:55,90 --> 00:07:58,90
we finish this video is just getting index-access.

202
00:07:58,90 --> 00:08:00,90
So what I've changed in this code right here

203
00:08:00,90 --> 00:08:03,20
is I have now asked for the index,

204
00:08:03,20 --> 00:08:06,00
and I'm going to print the index and the row

205
00:08:06,00 --> 00:08:08,80
and then if the index happens to be 1.

206
00:08:08,80 --> 00:08:12,30
So for that very first record I'm just going to print

207
00:08:12,30 --> 00:08:14,80
give me the data in position 0,

208
00:08:14,80 --> 00:08:16,80
so that's how you buy index, right,

209
00:08:16,80 --> 00:08:19,10
0 based index, that's how you access,

210
00:08:19,10 --> 00:08:22,10
in a slice, the first piece of data.

211
00:08:22,10 --> 00:08:23,70
Let's take a look at that running,

212
00:08:23,70 --> 00:08:26,90
and we'll switch and clear this out.

213
00:08:26,90 --> 00:08:29,00
And, actually, before it runs, I'm going to comment this out

214
00:08:29,00 --> 00:08:31,20
because we don't want to print all the data again.

215
00:08:31,20 --> 00:08:33,70
I just wanted to see that one piece of data.

216
00:08:33,70 --> 00:08:36,80
and go run main, and there we go.

217
00:08:36,80 --> 00:08:40,10
So there's that first record entry, right?

218
00:08:40,10 --> 00:08:42,50
Position 0 if index is 1.

219
00:08:42,50 --> 00:08:44,50
It's actually the second one, and then we break.

220
00:08:44,50 --> 00:08:46,40
Alright, so that's not going to print everything.

221
00:08:46,40 --> 00:08:48,80
So let's see that too.

222
00:08:48,80 --> 00:08:49,80
There we go.

223
00:08:49,80 --> 00:08:53,70
So it prints the first row which is the header, right there,

224
00:08:53,70 --> 00:08:55,30
and then it prints the second one

225
00:08:55,30 --> 00:08:58,10
and then it gives me what's in that second position,

226
00:08:58,10 --> 00:09:01,00
which is position 1 since it's zero-based.

227
00:09:01,00 --> 00:09:02,90
It gives me that data right there.

228
00:09:02,90 --> 00:09:05,20
So pretty cool, we've been able to open a file,

229
00:09:05,20 --> 00:09:07,30
we've been able to read the data,

230
00:09:07,30 --> 00:09:08,90
we were able to pull the data out,

231
00:09:08,90 --> 00:09:10,40
we looped over all the data,

232
00:09:10,40 --> 00:09:12,30
and then we grabbed by index position

233
00:09:12,30 --> 00:09:13,80
a piece of the data to look at it.

234
00:09:13,80 --> 00:09:16,50
In the next video we're going to change the delimiter

235
00:09:16,50 --> 00:09:20,00
and start accessing each individual piece of data.

