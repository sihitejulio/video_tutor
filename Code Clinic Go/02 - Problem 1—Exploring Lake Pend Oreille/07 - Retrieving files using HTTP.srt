1
00:00:00,40 --> 00:00:03,50
- So here is the last step in our solve,

2
00:00:03,50 --> 00:00:04,90
and the very last step in our solve

3
00:00:04,90 --> 00:00:07,90
is to get our data programmatically.

4
00:00:07,90 --> 00:00:10,90
And so we're going to use, from the http package,

5
00:00:10,90 --> 00:00:13,00
the Get function, and that's going to allow us

6
00:00:13,00 --> 00:00:16,00
to go get the data at a certain URL.

7
00:00:16,00 --> 00:00:18,20
When we get the data at a certain URL,

8
00:00:18,20 --> 00:00:20,10
it's going to give us a response.

9
00:00:20,10 --> 00:00:22,60
When we have a response, attached to that response,

10
00:00:22,60 --> 00:00:24,20
we have a Body.

11
00:00:24,20 --> 00:00:27,20
So if we look at http.Get, I'm going to hold down cmd

12
00:00:27,20 --> 00:00:29,70
and go into the standard library and look at that code,

13
00:00:29,70 --> 00:00:33,00
takes in that url, gives us a Response and an error,

14
00:00:33,00 --> 00:00:34,70
and if we look at the Response,

15
00:00:34,70 --> 00:00:37,70
the Response is a struct with these different fields,

16
00:00:37,70 --> 00:00:39,50
including a Body.

17
00:00:39,50 --> 00:00:42,00
So when we get that Response, we have a Body,

18
00:00:42,00 --> 00:00:44,50
and we pass it in to ReadAll.

19
00:00:44,50 --> 00:00:46,80
Well what is it that ReadAll takes?

20
00:00:46,80 --> 00:00:50,30
If I click on that, I see that ReadAll takes an io.Reader,

21
00:00:50,30 --> 00:00:53,70
so something that implements the Reader interface.

22
00:00:53,70 --> 00:00:56,60
Well, it must be that response Body

23
00:00:56,60 --> 00:00:59,90
implements the Reader interface, but can we prove that?

24
00:00:59,90 --> 00:01:01,80
So here's a little bit of a challenge for you.

25
00:01:01,80 --> 00:01:03,80
Maybe pause this video right here,

26
00:01:03,80 --> 00:01:06,30
open up the code or open up the standard library

27
00:01:06,30 --> 00:01:10,00
at GoDoc.org, and once you get into the standard library,

28
00:01:10,00 --> 00:01:14,30
see if you could find the way in which a response Body

29
00:01:14,30 --> 00:01:18,10
implements the io.Reader interface.

30
00:01:18,10 --> 00:01:20,10
OK, so here's how we do it.

31
00:01:20,10 --> 00:01:22,20
So I'm going to go and look at what does Get do,

32
00:01:22,20 --> 00:01:23,30
gives me a Response.

33
00:01:23,30 --> 00:01:24,70
What is my Response?

34
00:01:24,70 --> 00:01:25,90
It has Body,

35
00:01:25,90 --> 00:01:29,10
and a Body is of type io.ReadCloser.

36
00:01:29,10 --> 00:01:31,80
Well what is an io.ReadCloser?

37
00:01:31,80 --> 00:01:34,90
An io.ReadCloser is of type interface,

38
00:01:34,90 --> 00:01:37,80
and so, it's a interface that has these two

39
00:01:37,80 --> 00:01:41,30
other interfaces embedded into it, and so,

40
00:01:41,30 --> 00:01:43,30
this interface here is the Reader interface

41
00:01:43,30 --> 00:01:45,20
and there's the Closer interface.

42
00:01:45,20 --> 00:01:47,80
So it has both a Reader and a Closer interface,

43
00:01:47,80 --> 00:01:49,50
and if I follow those, I see, hey,

44
00:01:49,50 --> 00:01:51,00
there's the Reader interface,

45
00:01:51,00 --> 00:01:52,70
and if I'd followed the other one, I would've seen,

46
00:01:52,70 --> 00:01:54,80
hey, there's the Closer interface.

47
00:01:54,80 --> 00:01:56,60
So that's what ReadCloser gives me,

48
00:01:56,60 --> 00:01:59,10
the Reader and the Closer interface.

49
00:01:59,10 --> 00:02:01,80
So we just saw that a Response

50
00:02:01,80 --> 00:02:05,50
has the Response Body, which is of type io.ReadCloser,

51
00:02:05,50 --> 00:02:08,10
which means the Body implements the Reader

52
00:02:08,10 --> 00:02:10,40
and the Closer interface,

53
00:02:10,40 --> 00:02:14,30
and so since ioutil.ReadAll asks for a Reader,

54
00:02:14,30 --> 00:02:15,80
we can now pass it this

55
00:02:15,80 --> 00:02:18,70
because this implements the Reader interface.

56
00:02:18,70 --> 00:02:21,40
So when we do ReadAll, what does ReadAll give us?

57
00:02:21,40 --> 00:02:23,80
ReadAll gives us a slice of byte,

58
00:02:23,80 --> 00:02:25,70
so we get a slice of byte back,

59
00:02:25,70 --> 00:02:27,80
and I'm assigning that to a variable page,

60
00:02:27,80 --> 00:02:29,20
and then I'm Printing it out.

61
00:02:29,20 --> 00:02:31,30
So let's see what that gives me when I Print it out.

62
00:02:31,30 --> 00:02:34,90
I'm format Printing and I'm saying "Print this as a string."

63
00:02:34,90 --> 00:02:37,60
So I clear out my terminal, and I'm going to see

64
00:02:37,60 --> 00:02:39,50
which directory I'm in, and I'm in 13,

65
00:02:39,50 --> 00:02:41,50
so I'm going to do go run main,

66
00:02:41,50 --> 00:02:43,20
and I print out that string.

67
00:02:43,20 --> 00:02:46,10
So there's all of the HTML data for the homepage

68
00:02:46,10 --> 00:02:47,60
at Lynda.com.

69
00:02:47,60 --> 00:02:50,40
Now you'll notice, if I change this from Printing a string

70
00:02:50,40 --> 00:02:54,00
to just Printing the values, what happens then?

71
00:02:54,00 --> 00:02:55,80
I just get the slice of bytes.

72
00:02:55,80 --> 00:02:57,60
So those are all the bytes that make up

73
00:02:57,60 --> 00:03:00,80
all of the text that I saw before, which was the homepage.

74
00:03:00,80 --> 00:03:03,20
So it's just kind of interesting to take a look at,

75
00:03:03,20 --> 00:03:06,50
and you can compare ReadAll, which we're using right here,

76
00:03:06,50 --> 00:03:10,00
to what we used earlier, which was rdr.ReadAll,

77
00:03:10,00 --> 00:03:12,20
and rdr.ReadAll returns

78
00:03:12,20 --> 00:03:14,60
a multidimensional slice of string.

79
00:03:14,60 --> 00:03:16,90
Interesting, so you could just compare those two,

80
00:03:16,90 --> 00:03:20,10
and think about where you might use one in one situation

81
00:03:20,10 --> 00:03:22,80
versus the other in a different situation.

82
00:03:22,80 --> 00:03:24,70
So now let's take a look at our next example.

83
00:03:24,70 --> 00:03:26,50
We saw in the first example,

84
00:03:26,50 --> 00:03:31,10
how do we get all of the data from a webpage, Lynda.com?

85
00:03:31,10 --> 00:03:35,40
How do we go get all of our data from the military website

86
00:03:35,40 --> 00:03:38,20
with that data we want to run the statistics on?

87
00:03:38,20 --> 00:03:40,70
So I'm going to use http.Get again,

88
00:03:40,70 --> 00:03:42,80
tell it to go get the data at this location,

89
00:03:42,80 --> 00:03:44,20
it gives me a response,

90
00:03:44,20 --> 00:03:46,10
I use ioutil.ReadAll,

91
00:03:46,10 --> 00:03:48,30
ReadAll gives me my slice of bytes,

92
00:03:48,30 --> 00:03:50,60
and I might rename this variable, actually,

93
00:03:50,60 --> 00:03:54,20
instead of page to just bs, which is byte slice.

94
00:03:54,20 --> 00:03:55,50
All right, so there's my byte slice

95
00:03:55,50 --> 00:03:56,90
and I'm going to Print it as a string.

96
00:03:56,90 --> 00:03:59,50
Let's see what happens when I run that.

97
00:03:59,50 --> 00:04:01,40
Change directories, go into 14,

98
00:04:01,40 --> 00:04:03,10
go run main.go.

99
00:04:03,10 --> 00:04:04,20
I'm printing this as a string

100
00:04:04,20 --> 00:04:06,70
so I should see all of that data,

101
00:04:06,70 --> 00:04:07,90
and there it is, printing out.

102
00:04:07,90 --> 00:04:08,90
Pretty cool.

103
00:04:08,90 --> 00:04:11,40
So I'm going to do ctrl + c to end that,

104
00:04:11,40 --> 00:04:13,50
and then cmd + k to clear my screen.

105
00:04:13,50 --> 00:04:15,50
If I wanted to, I could change that and see

106
00:04:15,50 --> 00:04:19,30
those actual bytes by changing the Printing verb to %v,

107
00:04:19,30 --> 00:04:21,40
and then run it again.

108
00:04:21,40 --> 00:04:24,30
There they all are, pretty cool, ctrl + c,

109
00:04:24,30 --> 00:04:26,20
cmd + k,

110
00:04:26,20 --> 00:04:27,90
and we're back here so I'll leave that as that

111
00:04:27,90 --> 00:04:29,80
so it Prints out prettier.

112
00:04:29,80 --> 00:04:32,00
All right, so last step in the process here

113
00:04:32,00 --> 00:04:35,10
is instead of just Printing out all of the results,

114
00:04:35,10 --> 00:04:36,90
I'm going to get all of those results,

115
00:04:36,90 --> 00:04:38,60
so get the response,

116
00:04:38,60 --> 00:04:41,50
and then read that response, put it into a reader,

117
00:04:41,50 --> 00:04:43,70
and then Read it all, and get it into my rows,

118
00:04:43,70 --> 00:04:45,30
and so you'll notice here, I switched back

119
00:04:45,30 --> 00:04:48,10
from ioutil.ReadAll to rdr.ReadAll,

120
00:04:48,10 --> 00:04:49,50
and then that gives me the same data

121
00:04:49,50 --> 00:04:51,00
that I was working with before.

122
00:04:51,00 --> 00:04:52,40
Right there in that position.

123
00:04:52,40 --> 00:04:55,00
So this should all run the same as it was running before

124
00:04:55,00 --> 00:04:59,00
but let's take a look to verify.

125
00:04:59,00 --> 00:05:02,70
So there's, currently, the live figures

126
00:05:02,70 --> 00:05:05,50
for grabbing the data right now at this instant.

127
00:05:05,50 --> 00:05:06,40
Pretty cool.

128
00:05:06,40 --> 00:05:07,80
Well the very last thing I want to do

129
00:05:07,80 --> 00:05:09,80
to make sure my code is clean, there's two things.

130
00:05:09,80 --> 00:05:12,10
I want to make sure all of my errors are handled,

131
00:05:12,10 --> 00:05:14,50
and so everytime something returned an error,

132
00:05:14,50 --> 00:05:16,90
and this is just how you handle errors in Go,

133
00:05:16,90 --> 00:05:18,40
you immediately handle them,

134
00:05:18,40 --> 00:05:20,70
and you do something programmatic right here,

135
00:05:20,70 --> 00:05:23,70
so it's not very complex, my programmatic response,

136
00:05:23,70 --> 00:05:26,20
but I'm just saying, hey, log.Fatal this error,

137
00:05:26,20 --> 00:05:28,40
and exit the program or panic,

138
00:05:28,40 --> 00:05:31,60
exit the program and here's the error that you exited with,

139
00:05:31,60 --> 00:05:33,20
but I'm handling each of those errors

140
00:05:33,20 --> 00:05:34,70
just in case there's an error.

141
00:05:34,70 --> 00:05:36,90
So that's the beginning of error handling.

142
00:05:36,90 --> 00:05:39,20
Just making sure every error that's returned,

143
00:05:39,20 --> 00:05:41,40
you're programmatically doing something with it,

144
00:05:41,40 --> 00:05:43,70
checking it, making sure there isn't an error,

145
00:05:43,70 --> 00:05:44,80
and if there is an error,

146
00:05:44,80 --> 00:05:46,50
you're responding in some way.

147
00:05:46,50 --> 00:05:49,00
So that's one thing I absolutely want to make sure I do

148
00:05:49,00 --> 00:05:52,00
when I'm finishing up a solution for any problem,

149
00:05:52,00 --> 00:05:54,90
and then the other last thing I want to do with my code

150
00:05:54,90 --> 00:05:56,90
is I'm going to go up a level,

151
00:05:56,90 --> 00:06:00,90
and go back to right here, cc, I'm in the cc directory,

152
00:06:00,90 --> 00:06:04,10
I need to go into 01_parse_weather-data,

153
00:06:04,10 --> 00:06:06,20
so I'm going to format my code,

154
00:06:06,20 --> 00:06:09,70
and this will format all of my code to be idiomatic,

155
00:06:09,70 --> 00:06:12,10
and you could see it worked on a couple of files,

156
00:06:12,10 --> 00:06:14,80
so this ./...

157
00:06:14,80 --> 00:06:17,60
means from the current location where I'm at right now,

158
00:06:17,60 --> 00:06:20,30
work your way all the way down through the folders beneath,

159
00:06:20,30 --> 00:06:24,20
so from right here, go through all of these folders,

160
00:06:24,20 --> 00:06:27,70
and then run go fmt on any of the packages

161
00:06:27,70 --> 00:06:30,70
that you find there, and so that formats all of my code

162
00:06:30,70 --> 00:06:33,80
to look appropriate, they'll look the way it should look

163
00:06:33,80 --> 00:06:36,60
to be idiomatic in Go code,

164
00:06:36,60 --> 00:06:38,70
and so I want to run golint along my code.

165
00:06:38,70 --> 00:06:40,50
I'm going to clear out my terminal,

166
00:06:40,50 --> 00:06:43,10
and now I'm going to type in golint

167
00:06:43,10 --> 00:06:45,50
and again, use that notation, do it from where I'm at

168
00:06:45,50 --> 00:06:47,60
all the way down through the directory structure,

169
00:06:47,60 --> 00:06:49,30
and it just checked all my code to make sure

170
00:06:49,30 --> 00:06:53,00
that there weren't any outstanding, egregious formatting

171
00:06:53,00 --> 00:06:55,40
or syntax errors in my code,

172
00:06:55,40 --> 00:06:58,50
or misuses of ways I should be doing things in my code,

173
00:06:58,50 --> 00:07:01,00
so that's what golint does, and you could get golint

174
00:07:01,00 --> 00:07:04,70
by just Googling this repo, golang/lint, github

175
00:07:04,70 --> 00:07:06,80
and right there, you do this code right there

176
00:07:06,80 --> 00:07:08,40
and it brings it down and installs it

177
00:07:08,40 --> 00:07:10,40
so that you could run that golinter.

178
00:07:10,40 --> 00:07:11,80
So those are the things I'd want to do

179
00:07:11,80 --> 00:07:13,00
to finish up my code,

180
00:07:13,00 --> 00:07:14,30
run go fmt,

181
00:07:14,30 --> 00:07:15,50
run go lint,

182
00:07:15,50 --> 00:07:18,10
and then also make sure that all my errors are handled.

183
00:07:18,10 --> 00:07:21,00
So there's my solution for parsing weather data.

184
00:07:21,00 --> 00:07:23,40
That was a great challenge for Go.

185
00:07:23,40 --> 00:07:27,00
Go does this type of processing very well,

186
00:07:27,00 --> 00:07:31,00
and we've got some other amazing challenges coming up.

