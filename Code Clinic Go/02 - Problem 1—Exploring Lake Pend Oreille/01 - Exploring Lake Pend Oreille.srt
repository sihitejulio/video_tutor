1
00:00:06,90 --> 00:00:09,00
- Hello and welcome to Code Clinic.

2
00:00:09,00 --> 00:00:10,70
My name is Todd McLeod.

3
00:00:10,70 --> 00:00:12,80
Code Clinic is a course where you unique problems

4
00:00:12,80 --> 00:00:15,60
is introduced to a collection of lynda.com authors.

5
00:00:15,60 --> 00:00:18,70
In response, each author will create a solution

6
00:00:18,70 --> 00:00:21,20
using their programming language of choice.

7
00:00:21,20 --> 00:00:23,40
You can learn several things from Code Clinic.

8
00:00:23,40 --> 00:00:25,40
Different approaches to solving a problem,

9
00:00:25,40 --> 00:00:27,70
the pros and cons of different languages,

10
00:00:27,70 --> 00:00:29,70
and some tips and tricks to incorporate

11
00:00:29,70 --> 00:00:32,80
into your own coding practices.

12
00:00:32,80 --> 00:00:33,60
In this challenge,

13
00:00:33,60 --> 00:00:36,00
we'll work on a problem in Statistical Analysis

14
00:00:36,00 --> 00:00:37,10
and to some extent,

15
00:00:37,10 --> 00:00:38,80
handling big data.

16
00:00:38,80 --> 00:00:40,60
It's common to use a computer to manipulate

17
00:00:40,60 --> 00:00:43,20
and summarize large amounts of information,

18
00:00:43,20 --> 00:00:45,70
providing important insights on how to improve

19
00:00:45,70 --> 00:00:48,20
or handle a certain situation.

20
00:00:48,20 --> 00:00:50,20
In this problem, we'll use weather data collected

21
00:00:50,20 --> 00:00:53,30
by the US Navy from Lake Pend Oreille in northern Idaho.

22
00:00:53,30 --> 00:00:55,70
Lake Pend Oreille is the fifth deepest

23
00:00:55,70 --> 00:00:58,00
fresh water lake in the United States.

24
00:00:58,00 --> 00:01:00,20
So deep, in fact, that the US Navy

25
00:01:00,20 --> 00:01:02,80
uses it to test submarines.

26
00:01:02,80 --> 00:01:05,10
As part of that testing, the US Navy compiles

27
00:01:05,10 --> 00:01:07,70
an exhaustive list of weather statistics:

28
00:01:07,70 --> 00:01:11,10
wind speed, air temperature, barometric pressure.

29
00:01:11,10 --> 00:01:13,70
You can browse this data by pointing your web browser at

30
00:01:13,70 --> 00:01:20,30
http://lpo.dt.navy.mil

31
00:01:20,30 --> 00:01:23,10
You'll find several weather summaries, a web cam,

32
00:01:23,10 --> 00:01:25,80
and the raw data they collect every five minutes

33
00:01:25,80 --> 00:01:29,60
archived as standard text files.

34
00:01:29,60 --> 00:01:32,20
For anyone living or working on Lake Pend Oreille,

35
00:01:32,20 --> 00:01:35,60
weather statistics are an important part of everyday life.

36
00:01:35,60 --> 00:01:37,80
Average wind speed can be very different

37
00:01:37,80 --> 00:01:39,30
than median wind speed,

38
00:01:39,30 --> 00:01:41,20
especially if you're on a small boat

39
00:01:41,20 --> 00:01:43,10
in the middle of the lake.

40
00:01:43,10 --> 00:01:44,70
In this challenge, each of our authors

41
00:01:44,70 --> 00:01:46,10
will use their favorite language

42
00:01:46,10 --> 00:01:48,00
to calculate the Mean and Median

43
00:01:48,00 --> 00:01:49,90
of the Wind Speed, Air Temperature,

44
00:01:49,90 --> 00:01:53,10
and Barometric Pressure recorded at Deep Moor Station

45
00:01:53,10 --> 00:01:55,80
for a given range of dates.

46
00:01:55,80 --> 00:01:58,80
First, let's briefly review Mean and Median.

47
00:01:58,80 --> 00:02:00,50
These are both statistics.

48
00:02:00,50 --> 00:02:03,60
To explain statistics, we need a set of numbers.

49
00:02:03,60 --> 00:02:06,20
How about 14 readings for Wind Gust

50
00:02:06,20 --> 00:02:10,60
at Deep Moor Weather Station on January 1, 2014.

51
00:02:10,60 --> 00:02:13,50
You can see the data at this website.

52
00:02:13,50 --> 00:02:16,40
The first column is the day wind gust was recorded.

53
00:02:16,40 --> 00:02:18,70
The second column is the time it was recorded.

54
00:02:18,70 --> 00:02:23,10
And the third column is the wind gust in miles per hour.

55
00:02:23,10 --> 00:02:26,10
The Mean is also known as the average.

56
00:02:26,10 --> 00:02:28,20
To calculate the mean of a range of numbers,

57
00:02:28,20 --> 00:02:30,00
simply add the values in the set,

58
00:02:30,00 --> 00:02:32,70
then divide by the number of values.

59
00:02:32,70 --> 00:02:33,70
In this example,

60
00:02:33,70 --> 00:02:36,10
we add 14, 14,

61
00:02:36,10 --> 00:02:40,30
11, 11, 11, 11, 11,

62
00:02:40,30 --> 00:02:44,00
three, seven, seven, seven, seven,

63
00:02:44,00 --> 00:02:45,90
four and eight.

64
00:02:45,90 --> 00:02:48,50
Then divide the sum by 14,

65
00:02:48,50 --> 00:02:50,90
the count of numbers in the set.

66
00:02:50,90 --> 00:02:54,60
In this case, the mean is equal to nine.

67
00:02:54,60 --> 00:02:57,00
The Median is the number halfway between

68
00:02:57,00 --> 00:03:00,70
all the values in a sorted range of values.

69
00:03:00,70 --> 00:03:03,70
Think of the median as in the median strip of the road.

70
00:03:03,70 --> 00:03:06,40
It always marks the center of the road.

71
00:03:06,40 --> 00:03:07,60
To calculate the median,

72
00:03:07,60 --> 00:03:10,30
first sort the numbers from lowest to highest.

73
00:03:10,30 --> 00:03:12,40
If there's an odd number of values,

74
00:03:12,40 --> 00:03:14,60
then just take the middle number.

75
00:03:14,60 --> 00:03:16,30
If there's an even number of values,

76
00:03:16,30 --> 00:03:19,40
then calculate the average of the central two numbers.

77
00:03:19,40 --> 00:03:21,80
In this case, there's an even number of values.

78
00:03:21,80 --> 00:03:23,80
So we sort, then take the average

79
00:03:23,80 --> 00:03:26,50
of the middle two values: eight and 11.

80
00:03:26,50 --> 00:03:29,50
The median is 9.5.

81
00:03:29,50 --> 00:03:30,90
So there's our first challenge.

82
00:03:30,90 --> 00:03:34,00
Pull statistics from a data set available online.

83
00:03:34,00 --> 00:03:35,20
Perhaps you want to pause

84
00:03:35,20 --> 00:03:37,00
and create a solution of your own.

85
00:03:37,00 --> 00:03:38,90
How would you solve the problem?

86
00:03:38,90 --> 00:03:39,90
In the next videos,

87
00:03:39,90 --> 00:03:43,00
I'll show you how I solved this challenge.

