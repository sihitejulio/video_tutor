1
00:00:00,40 --> 00:00:00,80
- All right.

2
00:00:00,80 --> 00:00:03,20
So, now we're going to calculate our median.

3
00:00:03,20 --> 00:00:05,10
And I've included in here in the read me

4
00:00:05,10 --> 00:00:07,70
a little reminder of how do you calculate the median.

5
00:00:07,70 --> 00:00:10,20
So, if we have an odd number of values,

6
00:00:10,20 --> 00:00:12,70
the median is the value right there in the middle.

7
00:00:12,70 --> 00:00:15,10
And if we have an even number of values,

8
00:00:15,10 --> 00:00:17,00
there is no value right in the middle.

9
00:00:17,00 --> 00:00:20,00
So, we take the two values on each side of the middle.

10
00:00:20,00 --> 00:00:20,80
Right there.

11
00:00:20,80 --> 00:00:22,10
The two middle values.

12
00:00:22,10 --> 00:00:24,40
And we add them together, and then divide by two.

13
00:00:24,40 --> 00:00:25,90
That's how we calculate the median.

14
00:00:25,90 --> 00:00:28,40
So, we need to figure out, do we have an odd number

15
00:00:28,40 --> 00:00:32,50
or an even number of values in the data we're working with?

16
00:00:32,50 --> 00:00:34,10
So, let me show you how I did that.

17
00:00:34,10 --> 00:00:36,70
I created a new function called median.

18
00:00:36,70 --> 00:00:39,00
And that function, just like my mean function,

19
00:00:39,00 --> 00:00:41,10
the median function is going to take in

20
00:00:41,10 --> 00:00:42,70
a multidimensional slice.

21
00:00:42,70 --> 00:00:44,80
So, once again, this is all of my records.

22
00:00:44,80 --> 00:00:46,80
And here is one record.

23
00:00:46,80 --> 00:00:48,50
Each record is a slice a fields.

24
00:00:48,50 --> 00:00:50,60
And then it also takes in the index position

25
00:00:50,60 --> 00:00:54,30
of the data I want to access in that slice of fields.

26
00:00:54,30 --> 00:00:58,60
So, the first thing I do is I create a slice of Float64.

27
00:00:58,60 --> 00:01:01,00
Remember all of this data, it's a string.

28
00:01:01,00 --> 00:01:03,30
So, I want to take that piece of data,

29
00:01:03,30 --> 00:01:06,60
and I'm going to turn it into a Float64, so I parse it.

30
00:01:06,60 --> 00:01:09,70
And them I'm going to append it to my slice,

31
00:01:09,70 --> 00:01:12,20
which is holding Float64.

32
00:01:12,20 --> 00:01:14,00
So, I've taken the data, which is a string,

33
00:01:14,00 --> 00:01:17,90
appended it to a slice, which is holding Float64.

34
00:01:17,90 --> 00:01:19,70
The reason I wanted to do that is because

35
00:01:19,70 --> 00:01:22,30
I'm going to work with each piece of data

36
00:01:22,30 --> 00:01:23,60
to do calculations.

37
00:01:23,60 --> 00:01:25,10
Now there's something cool

38
00:01:25,10 --> 00:01:28,40
that I'm very excited to show you about Go.

39
00:01:28,40 --> 00:01:30,60
So, Go has a package called sort.

40
00:01:30,60 --> 00:01:32,60
And if we go and we look at that package,

41
00:01:32,60 --> 00:01:36,00
we can come over to that package here at godoc.org,

42
00:01:36,00 --> 00:01:37,90
we could see some really cool examples about

43
00:01:37,90 --> 00:01:39,60
how to use package sort.

44
00:01:39,60 --> 00:01:41,50
But if we just look at this index

45
00:01:41,50 --> 00:01:43,30
where it lists all the different functions

46
00:01:43,30 --> 00:01:44,60
and all the different types,

47
00:01:44,60 --> 00:01:46,80
and the methods associated with those types,

48
00:01:46,80 --> 00:01:48,20
we see here right at the beginning,

49
00:01:48,20 --> 00:01:51,70
we have func Float64s, func ints.

50
00:01:51,70 --> 00:01:53,30
Well, one of the things about Go,

51
00:01:53,30 --> 00:01:56,00
which the people who built Go were really trying to do,

52
00:01:56,00 --> 00:01:58,90
was to eliminate stutter in code.

53
00:01:58,90 --> 00:02:01,50
To take code and no longer have it be

54
00:02:01,50 --> 00:02:03,90
really verbose or repetitive.

55
00:02:03,90 --> 00:02:07,50
So, let's take a look at how we use this Float64s

56
00:02:07,50 --> 00:02:09,70
right here to sort our data.

57
00:02:09,70 --> 00:02:14,10
So, down below I've called from this package sort Float64s.

58
00:02:14,10 --> 00:02:18,40
And just take a moment to reflect on how easily that reads.

59
00:02:18,40 --> 00:02:21,70
Sort Float64s, I really like that.

60
00:02:21,70 --> 00:02:25,10
Front package sort we're calling the function Float64s.

61
00:02:25,10 --> 00:02:26,20
And what do we want to do?

62
00:02:26,20 --> 00:02:28,10
We want to sort Float64s.

63
00:02:28,10 --> 00:02:31,60
And, so, to do that we pass in a slice of Float64s.

64
00:02:31,60 --> 00:02:33,50
And we could see that right here.

65
00:02:33,50 --> 00:02:37,20
Func Float64s takes a slice of Float64s.

66
00:02:37,20 --> 00:02:41,20
And it sorts a slice of Float64s in increasing order.

67
00:02:41,20 --> 00:02:44,10
So, pretty cool, I think, the way they designed the language

68
00:02:44,10 --> 00:02:46,00
to have it read really well.

69
00:02:46,00 --> 00:02:49,10
That's one of the stated goals of the language.

70
00:02:49,10 --> 00:02:51,20
So, let's take a look at how this code works,

71
00:02:51,20 --> 00:02:53,20
because I'm getting all of my data

72
00:02:53,20 --> 00:02:55,70
into this slice of Float64.

73
00:02:55,70 --> 00:02:57,80
And before sorting it'll show me the data.

74
00:02:57,80 --> 00:03:00,70
And then I sort it and it'll show me the data after sorting.

75
00:03:00,70 --> 00:03:03,80
And it's just returning a value of zero for no reason,

76
00:03:03,80 --> 00:03:05,30
because we need a return.

77
00:03:05,30 --> 00:03:06,90
So, let's watch this code run.

78
00:03:06,90 --> 00:03:10,30
So, I'm in directory 10, go run main.go

79
00:03:10,30 --> 00:03:11,90
and all that code's running.

80
00:03:11,90 --> 00:03:14,60
Before sorting, our values are in any order.

81
00:03:14,60 --> 00:03:18,50
After sorting, the lower values are listed first.

82
00:03:18,50 --> 00:03:20,60
Same thing here for barometric pressure.

83
00:03:20,60 --> 00:03:22,20
Before sorting, any order.

84
00:03:22,20 --> 00:03:25,30
After sorting, lower values are listed first.

85
00:03:25,30 --> 00:03:26,60
So, everything got sorted.

86
00:03:26,60 --> 00:03:27,60
Fantastic.

87
00:03:27,60 --> 00:03:30,10
The next thing we want to to is we want to figure out,

88
00:03:30,10 --> 00:03:32,50
"Hey do we have an even or an odd number

89
00:03:32,50 --> 00:03:34,40
"of items that we're working with?"

90
00:03:34,40 --> 00:03:36,20
So, how do we calculate whether or not we have

91
00:03:36,20 --> 00:03:38,40
and even or an odd number of items?

92
00:03:38,40 --> 00:03:41,00
I did that by inside my median func,

93
00:03:41,00 --> 00:03:44,10
coming in here and finding the length of this slice.

94
00:03:44,10 --> 00:03:45,70
And the way I find that length is

95
00:03:45,70 --> 00:03:48,40
by using the built in length function right there.

96
00:03:48,40 --> 00:03:50,60
And then I take the modulus two of that.

97
00:03:50,60 --> 00:03:53,00
So, modulus two will give me the remainder.

98
00:03:53,00 --> 00:03:55,40
Like that, I'm saying just give me the remainder.

99
00:03:55,40 --> 00:03:58,70
And, so, if I have an even number of items, like this.

100
00:03:58,70 --> 00:04:01,40
And I divide by two, the remainder's going to be zero.

101
00:04:01,40 --> 00:04:03,60
So, anytime I have an even number of items,

102
00:04:03,60 --> 00:04:05,20
the remainder will always be zero.

103
00:04:05,20 --> 00:04:07,00
So, that will tell me, "Hey this is even."

104
00:04:07,00 --> 00:04:08,70
If I have an odd number of items,

105
00:04:08,70 --> 00:04:10,90
the way this works is that here we have,

106
00:04:10,90 --> 00:04:13,70
if this is even, then we're going to return,

107
00:04:13,70 --> 00:04:15,20
and our function exits.

108
00:04:15,20 --> 00:04:18,50
Otherwise, this code never runs, and we return right here.

109
00:04:18,50 --> 00:04:21,50
And we report back we have an odd number of items.

110
00:04:21,50 --> 00:04:24,30
So, I'm just using zero and one to represent even,

111
00:04:24,30 --> 00:04:25,90
zero's even and odd is one.

112
00:04:25,90 --> 00:04:28,40
So, let's see what we have when we run this code.

113
00:04:28,40 --> 00:04:31,30
I'm going to clear everything out and then go up a level.

114
00:04:31,30 --> 00:04:34,70
And go run main.go.

115
00:04:34,70 --> 00:04:35,60
And we have ones.

116
00:04:35,60 --> 00:04:38,00
So, ones tell us we have odd number of items.

117
00:04:38,00 --> 00:04:38,50
Great.

118
00:04:38,50 --> 00:04:39,50
So, we're getting somewhere.

119
00:04:39,50 --> 00:04:41,20
The last thing we want to do is

120
00:04:41,20 --> 00:04:43,40
we want to calculate the median.

121
00:04:43,40 --> 00:04:45,80
So, again, going into our median function,

122
00:04:45,80 --> 00:04:47,40
we're determining whether or not

123
00:04:47,40 --> 00:04:49,10
we have an even number of items,

124
00:04:49,10 --> 00:04:50,60
or we have an odd number of items.

125
00:04:50,60 --> 00:04:52,20
If we have an even number of items,

126
00:04:52,20 --> 00:04:53,60
we got to take those two right there,

127
00:04:53,60 --> 00:04:55,50
add them together, and divide by two.

128
00:04:55,50 --> 00:04:57,90
So, we find the middle, we get length sorted,

129
00:04:57,90 --> 00:05:00,10
we divide by two and that gives us the middle.

130
00:05:00,10 --> 00:05:03,20
And then we say, "Hey, give me the number

131
00:05:03,20 --> 00:05:05,10
"at this position right there."

132
00:05:05,10 --> 00:05:06,90
And then the lower number is going to be

133
00:05:06,90 --> 00:05:08,70
that position minus one.

134
00:05:08,70 --> 00:05:10,00
Now, you might be wondering,

135
00:05:10,00 --> 00:05:12,90
how is it that that is the higher number

136
00:05:12,90 --> 00:05:14,70
and this is the lower number?

137
00:05:14,70 --> 00:05:17,80
Well, slices are zero based indices.

138
00:05:17,80 --> 00:05:19,60
And, so, if I have four items,

139
00:05:19,60 --> 00:05:22,00
And I say, "Give me the length of that."

140
00:05:22,00 --> 00:05:23,90
Well, the length of that is four.

141
00:05:23,90 --> 00:05:26,80
So, I've got four divided by two, that gives me two.

142
00:05:26,80 --> 00:05:28,00
What's the higher number here?

143
00:05:28,00 --> 00:05:31,20
Zero, one, two, position two.

144
00:05:31,20 --> 00:05:32,30
What's the lower number?

145
00:05:32,30 --> 00:05:34,10
Well, it's two minus one.

146
00:05:34,10 --> 00:05:35,80
So, that's how I get the higher number

147
00:05:35,80 --> 00:05:37,40
and the lower number right there.

148
00:05:37,40 --> 00:05:39,70
So, return higher plus lower divided by two.

149
00:05:39,70 --> 00:05:42,00
Otherwise, I just get the length sorted.

150
00:05:42,00 --> 00:05:44,90
And then, once I have the length sorted, I return that one.

151
00:05:44,90 --> 00:05:47,90
So, if we had length three divide that by two,

152
00:05:47,90 --> 00:05:51,20
that's going to give me 1.5 or,

153
00:05:51,20 --> 00:05:53,30
since it's an int when we do this division,

154
00:05:53,30 --> 00:05:54,60
it'll roll back to one.

155
00:05:54,60 --> 00:05:56,90
Which here it's position zero, position one.

156
00:05:56,90 --> 00:05:58,30
So, it gives me one in the middle.

157
00:05:58,30 --> 00:06:00,20
So, that's calculating the median.

158
00:06:00,20 --> 00:06:02,60
And let's go up a level into that directory,

159
00:06:02,60 --> 00:06:07,20
and then go run main.go and watch this code run.

160
00:06:07,20 --> 00:06:07,90
Perfect.

161
00:06:07,90 --> 00:06:09,70
So, total records processed.

162
00:06:09,70 --> 00:06:12,10
Air temperature, barometric, and wind speed.

163
00:06:12,10 --> 00:06:13,30
All of it calculated.

164
00:06:13,30 --> 00:06:14,60
Everything's working great.

165
00:06:14,60 --> 00:06:16,70
And we got all that stuff processed

166
00:06:16,70 --> 00:06:19,00
with this file that we have right here on our computer.

167
00:06:19,00 --> 00:06:20,50
But in the next video I'm going to show you

168
00:06:20,50 --> 00:06:22,50
how to get that file dynamically.

169
00:06:22,50 --> 00:06:23,80
So anytime you run this code,

170
00:06:23,80 --> 00:06:27,00
it's always brought up to date and is current.

