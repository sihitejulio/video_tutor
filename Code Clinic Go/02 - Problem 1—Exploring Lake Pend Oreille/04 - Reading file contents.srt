1
00:00:00,40 --> 00:00:02,80
- So now I'm going to show you how you change the delimiter.

2
00:00:02,80 --> 00:00:07,50
We're using the CSV package and we want to read files from a

3
00:00:07,50 --> 00:00:09,60
CSV stand point, how do we change the limiter?

4
00:00:09,60 --> 00:00:12,20
And I'm going to show you how to access the desired data

5
00:00:12,20 --> 00:00:13,50
because we only want some of the

6
00:00:13,50 --> 00:00:15,60
fields, which are in each record.

7
00:00:15,60 --> 00:00:18,00
And we're going to take those fields because they're strings

8
00:00:18,00 --> 00:00:21,10
and we want to do mathematical computations with them.

9
00:00:21,10 --> 00:00:24,00
And go is very strongly typed, right?

10
00:00:24,00 --> 00:00:26,50
So it's compiled language and it's static,

11
00:00:26,50 --> 00:00:30,40
which means that you have to be very precise about what is

12
00:00:30,40 --> 00:00:33,10
what type and only do calculations with the correct type.

13
00:00:33,10 --> 00:00:34,40
And so we're going to take our

14
00:00:34,40 --> 00:00:36,80
strings and turn them into a float 64.

15
00:00:36,80 --> 00:00:38,50
And then we're going to calculate the mean.

16
00:00:38,50 --> 00:00:40,50
So that's what I'm hoping to show you in this video.

17
00:00:40,50 --> 00:00:42,30
And I also want to show you a

18
00:00:42,30 --> 00:00:44,30
little bit more about interfaces.

19
00:00:44,30 --> 00:00:47,50
Because I realized in the last video that I might be leaving

20
00:00:47,50 --> 00:00:50,30
a few of my very good friends behind

21
00:00:50,30 --> 00:00:52,00
and I don't want to do that.

22
00:00:52,00 --> 00:00:56,20
And so here we have CSV new reader, and I said hey it takes

23
00:00:56,20 --> 00:00:58,60
a file because it implements the reader interface.

24
00:00:58,60 --> 00:01:00,10
Let's take a look at that again.

25
00:01:00,10 --> 00:01:02,50
So new reader, takes an IO reader.

26
00:01:02,50 --> 00:01:03,80
What is an IO reader?

27
00:01:03,80 --> 00:01:07,00
An IO reader is an interface, that its type.

28
00:01:07,00 --> 00:01:12,70
And it means that any type that implements this method

29
00:01:12,70 --> 00:01:15,80
is now implementing the reader interface.

30
00:01:15,80 --> 00:01:19,00
And so this is one place where people who are learning go,

31
00:01:19,00 --> 00:01:21,00
particularly if they come from other programming

32
00:01:21,00 --> 00:01:23,70
backgrounds, get a little bit confused.

33
00:01:23,70 --> 00:01:28,00
Because interfaces are implemented implicitly.

34
00:01:28,00 --> 00:01:30,70
You don't have to explicitly say that

35
00:01:30,70 --> 00:01:34,10
a file implements the reader interface.

36
00:01:34,10 --> 00:01:39,00
If a file has this method, go is smart enough to figure out

37
00:01:39,00 --> 00:01:43,00
okay, that file implements the reader interface.

38
00:01:43,00 --> 00:01:45,20
So if we go and we look at the standard documentation

39
00:01:45,20 --> 00:01:48,30
online, and I could come down here and command F and

40
00:01:48,30 --> 00:01:49,70
search for file, so it highlighted

41
00:01:49,70 --> 00:01:52,00
anywhere file is listed on the page.

42
00:01:52,00 --> 00:01:53,80
And I'm looking for type file,

43
00:01:53,80 --> 00:01:55,60
so here's type file right there.

44
00:01:55,60 --> 00:01:59,60
And that's what I get when, in my code, right here,

45
00:01:59,60 --> 00:02:02,90
I'm saying open gives me a pointer to a file.

46
00:02:02,90 --> 00:02:06,70
So when I have a file, right there type file, if I look at

47
00:02:06,70 --> 00:02:11,20
the methods for file, you'll see right here that this is the

48
00:02:11,20 --> 00:02:14,30
read slice of biH returns in int and an error.

49
00:02:14,30 --> 00:02:18,50
That is the definition for the reader interface.

50
00:02:18,50 --> 00:02:24,40
So since file has this method right there, file implicitly

51
00:02:24,40 --> 00:02:26,90
implements the reader interface.

52
00:02:26,90 --> 00:02:30,00
And by the way, over here on the left, that's called the

53
00:02:30,00 --> 00:02:33,00
receiver, the thing I'm moving my mouse over, and that's how

54
00:02:33,00 --> 00:02:36,60
you attach in your definition of a function, that's how you

55
00:02:36,60 --> 00:02:40,20
attach a function to a type, which then

56
00:02:40,20 --> 00:02:43,20
makes that function a method of that type.

57
00:02:43,20 --> 00:02:46,10
So these are all methods of type file.

58
00:02:46,10 --> 00:02:48,30
And it has this method read, which means

59
00:02:48,30 --> 00:02:50,80
it implements the reader interface, which means that

60
00:02:50,80 --> 00:02:54,40
anywhere any other function asks for a reader, I could pass

61
00:02:54,40 --> 00:02:56,90
in anything implementing the reader interface.

62
00:02:56,90 --> 00:03:00,60
Kabam, kabing, polymorphism, I love it.

63
00:03:00,60 --> 00:03:02,60
Alright, so the first thing we're doing

64
00:03:02,60 --> 00:03:04,90
here is we're changing the delimiter.

65
00:03:04,90 --> 00:03:08,80
And to change the delimiter, that is all we do right there.

66
00:03:08,80 --> 00:03:11,70
So we just do reader dot comma, normally it's a comma,

67
00:03:11,70 --> 00:03:13,20
but I'm saying make it a tab.

68
00:03:13,20 --> 00:03:15,00
So where do we see that definition?

69
00:03:15,00 --> 00:03:18,20
So again if I come back to the go doc dot org and I'm just

70
00:03:18,20 --> 00:03:20,60
going to search for CSV and see if it brings anything up.

71
00:03:20,60 --> 00:03:22,40
And you're like okay, well where did it go?

72
00:03:22,40 --> 00:03:24,90
Right, I just thought you entered the name of the package.

73
00:03:24,90 --> 00:03:27,30
Well, if I go back and I look at my code,

74
00:03:27,30 --> 00:03:30,00
you see I'm importing and coding CSV.

75
00:03:30,00 --> 00:03:34,80
So the first thing I need to do is I need to go to encoding.

76
00:03:34,80 --> 00:03:37,70
And once I get there, all the different packages associated

77
00:03:37,70 --> 00:03:39,90
with it, one of them is CSV.

78
00:03:39,90 --> 00:03:45,00
And so inside CSV, if I come down and I look at okay

79
00:03:45,00 --> 00:03:49,10
CSV new reader and then reader comma, so what is a reader?

80
00:03:49,10 --> 00:03:52,40
And here we have a reader type reader.

81
00:03:52,40 --> 00:03:56,10
And reader has these different fields associated with it,

82
00:03:56,10 --> 00:03:57,80
so it's a struct, which allows us to

83
00:03:57,80 --> 00:04:00,00
aggregate different data together.

84
00:04:00,00 --> 00:04:03,30
And so this is how we might create quote on quote object,

85
00:04:03,30 --> 00:04:06,10
though in go we don't really refer to them as objects,

86
00:04:06,10 --> 00:04:07,60
but we collect different fields of

87
00:04:07,60 --> 00:04:09,50
data together inside of a struct.

88
00:04:09,50 --> 00:04:11,20
And one of them is a comma.

89
00:04:11,20 --> 00:04:14,30
So field delimiter set to comma by new reader.

90
00:04:14,30 --> 00:04:17,20
So we could reset that just like I did right here,

91
00:04:17,20 --> 00:04:22,30
reader dot comma comma and I'm setting it to a tab.

92
00:04:22,30 --> 00:04:25,40
And now I do my reader read all and it broke all of my

93
00:04:25,40 --> 00:04:28,30
fields up if they were separated by tab.

94
00:04:28,30 --> 00:04:30,70
So I range over them and I'm going to print the first

95
00:04:30,70 --> 00:04:33,30
two rows at index position zero

96
00:04:33,30 --> 00:04:35,20
and one for my slice of slices.

97
00:04:35,20 --> 00:04:38,60
And then right here, I'm going to print the second row

98
00:04:38,60 --> 00:04:41,20
again and give me the first piece of data.

99
00:04:41,20 --> 00:04:42,90
So let's take a look at that.

100
00:04:42,90 --> 00:04:47,60
So we'll go to O four and change directories.

101
00:04:47,60 --> 00:04:49,70
O four go run main.

102
00:04:49,70 --> 00:04:51,40
And so here we got the first one.

103
00:04:51,40 --> 00:04:52,60
And we got the second one.

104
00:04:52,60 --> 00:04:55,70
And then it just gave me 19.50, which is right here.

105
00:04:55,70 --> 00:04:59,40
So this was all in position zero of that interslice

106
00:04:59,40 --> 00:05:01,50
and this was in position one.

107
00:05:01,50 --> 00:05:02,50
So how cool is that?

108
00:05:02,50 --> 00:05:03,80
We accessed some data.

109
00:05:03,80 --> 00:05:06,20
Now that we've accessed data,

110
00:05:06,20 --> 00:05:08,80
we can access our desired data.

111
00:05:08,80 --> 00:05:13,70
So when I look at all of my data, what is the desired data?

112
00:05:13,70 --> 00:05:17,00
I have air temperature, I have barometric pressure,

113
00:05:17,00 --> 00:05:19,70
and I also have wind speed.

114
00:05:19,70 --> 00:05:21,40
So those are the fields I'm wanting.

115
00:05:21,40 --> 00:05:24,30
So if this is my first field, this is my second field at

116
00:05:24,30 --> 00:05:26,90
position one, I want my air temp and then I

117
00:05:26,90 --> 00:05:29,10
could figure out the positions of the rest of my fields.

118
00:05:29,10 --> 00:05:33,80
And so ultimately what I came up with was to access desired

119
00:05:33,80 --> 00:05:38,30
data, I need positions one, two, and seven.

120
00:05:38,30 --> 00:05:42,30
And by using format printing, print F, I could go and I

121
00:05:42,30 --> 00:05:44,70
could look at all the different formatting verbs.

122
00:05:44,70 --> 00:05:49,50
So again I'll go over to godoc.org and look at fmt package.

123
00:05:49,50 --> 00:05:52,50
And here are all of my different options for print

124
00:05:52,50 --> 00:05:54,20
formatting with print F.

125
00:05:54,20 --> 00:05:56,10
So these are the formatting verbs.

126
00:05:56,10 --> 00:06:00,50
And I'm going to use percent T, which tells me the type.

127
00:06:00,50 --> 00:06:03,10
So I'm going to see the type of each of these pieces of data

128
00:06:03,10 --> 00:06:05,10
and then I'll also see the value.

129
00:06:05,10 --> 00:06:06,60
So I'll print my first two rows,

130
00:06:06,60 --> 00:06:08,30
the header and the first row of data.

131
00:06:08,30 --> 00:06:10,50
And then I'll print the first row of data again,

132
00:06:10,50 --> 00:06:13,10
but it's pulling out the individual pieces of data.

133
00:06:13,10 --> 00:06:17,70
So let's see that run.

134
00:06:17,70 --> 00:06:19,60
And I got a false, I got a true.

135
00:06:19,60 --> 00:06:21,00
I was also checking something else.

136
00:06:21,00 --> 00:06:22,50
I got the first two rows.

137
00:06:22,50 --> 00:06:24,60
And then I got the type, string, string, string.

138
00:06:24,60 --> 00:06:26,10
And I got the pieces of data.

139
00:06:26,10 --> 00:06:27,00
Very cool.

140
00:06:27,00 --> 00:06:28,80
And where did the false and the true come from?

141
00:06:28,80 --> 00:06:31,70
So right here, the false and the true print line,

142
00:06:31,70 --> 00:06:34,90
reader, trim leaving space, that was the first time.

143
00:06:34,90 --> 00:06:37,90
That was set to false, so I was just trimming the leading

144
00:06:37,90 --> 00:06:40,40
space because I thought oh that's kind of interesting.

145
00:06:40,40 --> 00:06:44,80
In that struct that we saw over at godoc, we only had

146
00:06:44,80 --> 00:06:47,30
comment, we also had trim leading space.

147
00:06:47,30 --> 00:06:49,20
And it's a bool, type boolian.

148
00:06:49,20 --> 00:06:53,10
So by default, it is set to false, but then I set it to true

149
00:06:53,10 --> 00:06:55,70
and it then printed out true.

150
00:06:55,70 --> 00:06:57,60
So I was also able to trim the leading space.

151
00:06:57,60 --> 00:06:59,60
Alright, so this video is getting a little bit long,

152
00:06:59,60 --> 00:07:01,40
so this'll be the last thing we take a look at.

153
00:07:01,40 --> 00:07:04,70
And here we're going to take that string, the data in each

154
00:07:04,70 --> 00:07:08,70
position, and we're going to parse it to a float 64.

155
00:07:08,70 --> 00:07:11,00
And so string convert is a package.

156
00:07:11,00 --> 00:07:13,80
And you can see I'm importing string convert string convert.

157
00:07:13,80 --> 00:07:16,20
And I call the function parse float.

158
00:07:16,20 --> 00:07:19,20
And then I get that piece of data and I take that string

159
00:07:19,20 --> 00:07:22,00
and I convert it into a float 64.

160
00:07:22,00 --> 00:07:24,60
So now when I print out the type of that data, you should

161
00:07:24,60 --> 00:07:27,80
see the air temperature, barometric pressure, and wind speed

162
00:07:27,80 --> 00:07:30,10
are all going to be float 64, data

163
00:07:30,10 --> 00:07:32,40
types I could use to do calculations.

164
00:07:32,40 --> 00:07:34,40
So let's take a look at that.

165
00:07:34,40 --> 00:07:36,20
We can clear out my screen.

166
00:07:36,20 --> 00:07:40,00
Change directories into O Six and then go run main.

167
00:07:40,00 --> 00:07:42,40
There we go, all of my float 64s.

168
00:07:42,40 --> 00:07:45,00
In the next video we'll see how to do our calculations.

