1
00:00:00,50 --> 00:00:01,70
- So now we're going to take a look at

2
00:00:01,70 --> 00:00:03,90
how do we figure out attack vectors.

3
00:00:03,90 --> 00:00:04,90
That's what I like to call this.

4
00:00:04,90 --> 00:00:07,30
What are my angle of attack vectors

5
00:00:07,30 --> 00:00:08,70
and how do we figure them out?

6
00:00:08,70 --> 00:00:11,10
Well, for rows and columns, it's pretty straightforward.

7
00:00:11,10 --> 00:00:12,50
Visually we can see those, right?

8
00:00:12,50 --> 00:00:13,50
That's an attack vector.

9
00:00:13,50 --> 00:00:15,20
They'll be able to attack each other.

10
00:00:15,20 --> 00:00:17,50
These two queens will be able to attack each other.

11
00:00:17,50 --> 00:00:20,10
These two queens will be able to attack each other.

12
00:00:20,10 --> 00:00:22,20
And the mathematical representation of

13
00:00:22,20 --> 00:00:24,50
figuring that out for a row or column is

14
00:00:24,50 --> 00:00:26,70
if these guys, they both have the same column,

15
00:00:26,70 --> 00:00:29,30
you can see the zero there for their coordinates,

16
00:00:29,30 --> 00:00:31,40
great, they're going to be able to attack each other.

17
00:00:31,40 --> 00:00:32,40
They're in the same column.

18
00:00:32,40 --> 00:00:33,30
They're in column zero.

19
00:00:33,30 --> 00:00:34,60
Zero and zero.

20
00:00:34,60 --> 00:00:35,90
And these guys are in the same row.

21
00:00:35,90 --> 00:00:37,80
They're both in row one and one.

22
00:00:37,80 --> 00:00:39,20
They're going to be able to attack each other.

23
00:00:39,20 --> 00:00:42,20
So mathematically, we'd represent that like this.

24
00:00:42,20 --> 00:00:44,40
But what about this one that's not quite so obvious?

25
00:00:44,40 --> 00:00:47,60
To get this formula, and the reason why this formula works,

26
00:00:47,60 --> 00:00:49,90
we're going to use the formula for a slope.

27
00:00:49,90 --> 00:00:52,20
So if you remember back to algebra

28
00:00:52,20 --> 00:00:55,20
we had that y equals mx plus b formula.

29
00:00:55,20 --> 00:00:56,90
The point-slope formula.

30
00:00:56,90 --> 00:01:00,10
Well, m is the slope and m is calculated by

31
00:01:00,10 --> 00:01:04,40
figuring out the absolute difference between rise over run,

32
00:01:04,40 --> 00:01:07,00
and this is going up one and coming over one.

33
00:01:07,00 --> 00:01:08,60
So our slope is one.

34
00:01:08,60 --> 00:01:10,20
There's the visual representation,

35
00:01:10,20 --> 00:01:13,80
delta_y, delta_x of slope is equal to rise over run.

36
00:01:13,80 --> 00:01:15,60
There's the formula for slope,

37
00:01:15,60 --> 00:01:17,60
the difference in y over the difference in x,

38
00:01:17,60 --> 00:01:22,60
and m is the traditional algebraic representation of slope.

39
00:01:22,60 --> 00:01:24,00
But we know our slope is one,

40
00:01:24,00 --> 00:01:25,40
because it's up one, over one,

41
00:01:25,40 --> 00:01:26,90
and one divided by one is one.

42
00:01:26,90 --> 00:01:28,90
So I can set it equal to one like that.

43
00:01:28,90 --> 00:01:31,10
I can then cross-cancel my denominator

44
00:01:31,10 --> 00:01:33,90
by multiplying each side by this amount right here

45
00:01:33,90 --> 00:01:36,20
and that would cancel out and put that over there.

46
00:01:36,20 --> 00:01:38,40
So I could have that and I could also flip it around.

47
00:01:38,40 --> 00:01:41,20
So now let's actually apply that as an example

48
00:01:41,20 --> 00:01:42,70
with this one right here.

49
00:01:42,70 --> 00:01:44,90
And so I'm going to calculate the difference

50
00:01:44,90 --> 00:01:46,40
between two minus x, they're x-values.

51
00:01:46,40 --> 00:01:48,90
I'm using this representation of the formula right here.

52
00:01:48,90 --> 00:01:50,50
And three minus seven, the y-values.

53
00:01:50,50 --> 00:01:51,90
Take the absolute difference.

54
00:01:51,90 --> 00:01:54,40
Four equals four; they are on a diagonal.

55
00:01:54,40 --> 00:01:55,60
They could attack each other.

56
00:01:55,60 --> 00:01:58,20
So where do we solve that in the code?

57
00:01:58,20 --> 00:02:01,40
Well, I have this one function right here, intAbsolute.

58
00:02:01,40 --> 00:02:02,80
That looks pretty promising,

59
00:02:02,80 --> 00:02:04,90
because I'm taking the absolute value of something,

60
00:02:04,90 --> 00:02:06,10
and sure enough, when I look to see

61
00:02:06,10 --> 00:02:07,40
where is that being used,

62
00:02:07,40 --> 00:02:10,00
it's finding the difference between rows and columns.

63
00:02:10,00 --> 00:02:12,10
So that's me checking right there to see

64
00:02:12,10 --> 00:02:14,20
if there's that angle attack vector.

65
00:02:14,20 --> 00:02:16,40
Well, where's checkingValid being used?

66
00:02:16,40 --> 00:02:18,50
So, I scroll up here and I see checkingValid

67
00:02:18,50 --> 00:02:20,30
is being used right here.

68
00:02:20,30 --> 00:02:22,30
Where is getOptions being used?

69
00:02:22,30 --> 00:02:23,10
I scroll up.

70
00:02:23,10 --> 00:02:24,10
So here are my options.

71
00:02:24,10 --> 00:02:25,80
Here's that data structure.

72
00:02:25,80 --> 00:02:28,00
And we're going to get 40,320

73
00:02:28,00 --> 00:02:30,00
different possible permutations.

74
00:02:30,00 --> 00:02:33,10
First, though, is just a slice event with only those values.

75
00:02:33,10 --> 00:02:35,20
We pass that into getOptions

76
00:02:35,20 --> 00:02:38,70
and getOptions is going to give us our permutations.

77
00:02:38,70 --> 00:02:40,80
So let's look see what getOptions does.

78
00:02:40,80 --> 00:02:42,80
getOptions takes that slice event,

79
00:02:42,80 --> 00:02:44,30
which is our data structure,

80
00:02:44,30 --> 00:02:46,30
passes it into permutations.

81
00:02:46,30 --> 00:02:48,80
permutations calls permute,

82
00:02:48,80 --> 00:02:51,80
and permute is where we do this recursive algorithm

83
00:02:51,80 --> 00:02:55,30
to produce this output right here.

84
00:02:55,30 --> 00:02:56,70
That's what permute's going to do.

85
00:02:56,70 --> 00:02:58,90
We're going to look at permute in more detail.

86
00:02:58,90 --> 00:03:00,90
We're going to look at how to figure out permutations,

87
00:03:00,90 --> 00:03:02,30
but not in this video.

88
00:03:02,30 --> 00:03:03,50
In the next video, we'll start looking

89
00:03:03,50 --> 00:03:05,50
at how do we calculate permutations.

90
00:03:05,50 --> 00:03:07,80
So we get all of our permutations.

91
00:03:07,80 --> 00:03:10,80
getOptions gives us back all of our permutations.

92
00:03:10,80 --> 00:03:12,00
Here they are.

93
00:03:12,00 --> 00:03:13,50
It's a slice of slice of int.

94
00:03:13,50 --> 00:03:15,80
That's what permutations gives back to us.

95
00:03:15,80 --> 00:03:17,90
Slice of slice of int, right there.

96
00:03:17,90 --> 00:03:19,20
That's what it gives back to us

97
00:03:19,20 --> 00:03:21,60
and it goes back to here, and then it comes back to here,

98
00:03:21,60 --> 00:03:24,00
and we get back a slice of slice of int.

99
00:03:24,00 --> 00:03:25,70
Why a slice of int?

100
00:03:25,70 --> 00:03:28,90
Because right here we have one single combination.

101
00:03:28,90 --> 00:03:31,70
One possible arrangement of these eight values.

102
00:03:31,70 --> 00:03:34,30
We're going to have 40,230 of those

103
00:03:34,30 --> 00:03:36,30
and they're each going to be a slice of int.

104
00:03:36,30 --> 00:03:40,30
Well, let's store all 40,320 of them

105
00:03:40,30 --> 00:03:43,20
in a slice, and that's exactly what we're doing

106
00:03:43,20 --> 00:03:45,50
when we return right here.

107
00:03:45,50 --> 00:03:50,20
We're taking 40,320 of those slice of ints

108
00:03:50,20 --> 00:03:51,60
are going to be in this slice.

109
00:03:51,60 --> 00:03:53,60
So that's a slice of slice of ints.

110
00:03:53,60 --> 00:03:55,50
And that's a multi-dimensional slice.

111
00:03:55,50 --> 00:03:57,30
Okay, so, where was it

112
00:03:57,30 --> 00:03:59,60
that we got that multi-dimensional slice back?

113
00:03:59,60 --> 00:04:00,50
Right here.

114
00:04:00,50 --> 00:04:02,80
Then we create another multi-dimensional slice

115
00:04:02,80 --> 00:04:04,40
to hold our return.

116
00:04:04,40 --> 00:04:06,40
This is going to be the correct answers.

117
00:04:06,40 --> 00:04:07,30
We're going to check,

118
00:04:07,30 --> 00:04:10,10
we're going to range over our slice of slice int.

119
00:04:10,10 --> 00:04:11,60
So every time we range over it,

120
00:04:11,60 --> 00:04:14,00
we're going to get one of the possible options.

121
00:04:14,00 --> 00:04:16,00
This is one arrangement of the board.

122
00:04:16,00 --> 00:04:19,20
It'll be one combination, one arrangement of the queens

123
00:04:19,20 --> 00:04:20,50
in these rows and columns.

124
00:04:20,50 --> 00:04:22,20
The rows are the index positions.

125
00:04:22,20 --> 00:04:23,60
The columns are the values.

126
00:04:23,60 --> 00:04:25,60
So, we're going to get that each time,

127
00:04:25,60 --> 00:04:27,70
and then we're going to check each possible board

128
00:04:27,70 --> 00:04:28,90
to see if it's valid.

129
00:04:28,90 --> 00:04:31,40
So we come down here to checkValid

130
00:04:31,40 --> 00:04:33,60
and we see that in checkValid,

131
00:04:33,60 --> 00:04:36,20
we're checking to see, do they have a diagonal

132
00:04:36,20 --> 00:04:38,00
where they can attack each other.

133
00:04:38,00 --> 00:04:40,80
So I want you to take a look at this part of the algorithm

134
00:04:40,80 --> 00:04:42,30
and I want you to pause the video

135
00:04:42,30 --> 00:04:44,10
and I want you to reflect on that

136
00:04:44,10 --> 00:04:47,10
and see if you can figure out how is this working.

137
00:04:47,10 --> 00:04:48,90
So, I'm going to give you a little bit of help,

138
00:04:48,90 --> 00:04:51,20
and the piece of help I'm going to give you is right here

139
00:04:51,20 --> 00:04:53,60
in 01-README.md

140
00:04:53,60 --> 00:04:56,40
and I've copied over here the data structure.

141
00:04:56,40 --> 00:04:58,80
Let's say this is just a sample data structure

142
00:04:58,80 --> 00:05:00,50
that we're iterating through this time.

143
00:05:00,50 --> 00:05:02,20
So this is one of the boards that we're doing

144
00:05:02,20 --> 00:05:05,00
and we're now ranging over this slice,

145
00:05:05,00 --> 00:05:08,20
and as we range over this slice, what's going to happen is

146
00:05:08,20 --> 00:05:11,40
we're going to get certain row and column information.

147
00:05:11,40 --> 00:05:13,80
And so I put down here each iteration

148
00:05:13,80 --> 00:05:15,60
and what we'll get for each iteration.

149
00:05:15,60 --> 00:05:18,70
And so in the first iteration, we'll get row1,

150
00:05:18,70 --> 00:05:19,90
which will be zero.

151
00:05:19,90 --> 00:05:23,30
Remember, that index position determines the row number.

152
00:05:23,30 --> 00:05:25,90
And so that will be row zero somewhere.

153
00:05:25,90 --> 00:05:27,40
The column will be seven.

154
00:05:27,40 --> 00:05:29,20
So, it's the exact same thing here.

155
00:05:29,20 --> 00:05:30,60
There will be row zero, column seven.

156
00:05:30,60 --> 00:05:34,40
And then this will be row one, column three.

157
00:05:34,40 --> 00:05:36,60
And so, I just listed each of those out.

158
00:05:36,60 --> 00:05:38,90
So now that you have this first part,

159
00:05:38,90 --> 00:05:41,60
I want you to pause the video again

160
00:05:41,60 --> 00:05:43,70
and try the second attempt to see

161
00:05:43,70 --> 00:05:46,50
what is happening in each iteration.

162
00:05:46,50 --> 00:05:49,20
For each iteration, we do another loop here,

163
00:05:49,20 --> 00:05:51,00
but what is happening with that other loop

164
00:05:51,00 --> 00:05:52,10
and what is it checking?

165
00:05:52,10 --> 00:05:54,10
So give the video a pause and see if you could

166
00:05:54,10 --> 00:05:56,20
complete this README right here,

167
00:05:56,20 --> 00:05:58,90
and I'll put in another README that has an example

168
00:05:58,90 --> 00:06:00,30
which I'll show you just in a minute

169
00:06:00,30 --> 00:06:03,30
after you unpause the video.

170
00:06:03,30 --> 00:06:05,40
Alright, so hopefully you paused the video

171
00:06:05,40 --> 00:06:06,70
and you gave that a crack

172
00:06:06,70 --> 00:06:08,70
and here's the next solution.

173
00:06:08,70 --> 00:06:10,60
And so, what I've done is I've shown

174
00:06:10,60 --> 00:06:12,10
that we're getting these values here.

175
00:06:12,10 --> 00:06:13,70
Row1 and column1,

176
00:06:13,70 --> 00:06:15,80
and then based upon the row1, column1 we get,

177
00:06:15,80 --> 00:06:17,50
that's going to be an outer loop.

178
00:06:17,50 --> 00:06:18,90
We're going to do an inner loop.

179
00:06:18,90 --> 00:06:21,20
There's another loop right here, another for,

180
00:06:21,20 --> 00:06:23,90
and we're going to get another row, column value.

181
00:06:23,90 --> 00:06:25,20
And so that's going to be an inner loop.

182
00:06:25,20 --> 00:06:27,40
So for each of the outer loops,

183
00:06:27,40 --> 00:06:29,40
there's going to be a bunch of inner loops.

184
00:06:29,40 --> 00:06:31,70
And so I like to think of nested loops

185
00:06:31,70 --> 00:06:35,40
as one big outer loop and a bunch of little inner loops,

186
00:06:35,40 --> 00:06:37,90
so it's kind of like this big thing, the big outer loop,

187
00:06:37,90 --> 00:06:39,60
turns once and then the inner loop

188
00:06:39,60 --> 00:06:41,50
iterates for all the times inside.

189
00:06:41,50 --> 00:06:44,40
I know that sounds kind of funny but it helps people.

190
00:06:44,40 --> 00:06:45,80
So we get our inner iterations

191
00:06:45,80 --> 00:06:47,30
and if we do the calculations,

192
00:06:47,30 --> 00:06:48,80
you could see what it's doing.

193
00:06:48,80 --> 00:06:52,50
And every time we move forward in our data structure,

194
00:06:52,50 --> 00:06:54,80
it's doing comparisons against the rest

195
00:06:54,80 --> 00:06:56,80
and the rest get smaller and smaller.

196
00:06:56,80 --> 00:06:58,50
Now the reason we're able to allow our algorithm

197
00:06:58,50 --> 00:07:01,20
to get smaller and smaller and smaller like that

198
00:07:01,20 --> 00:07:04,40
is because if we compare does x equal y.

199
00:07:04,40 --> 00:07:06,70
If we were to compare that, it's redundant for us

200
00:07:06,70 --> 00:07:08,80
to compare does y equal x.

201
00:07:08,80 --> 00:07:10,60
Because we've already compared if x equals y,

202
00:07:10,60 --> 00:07:13,20
so it's redundant to say, does y equals x?

203
00:07:13,20 --> 00:07:14,70
We've already answered that question.

204
00:07:14,70 --> 00:07:16,70
And so that's the thinking behind only doing it

205
00:07:16,70 --> 00:07:19,10
from the point forward where you're at

206
00:07:19,10 --> 00:07:21,70
and we're just eliminating some of our processing there.

207
00:07:21,70 --> 00:07:23,50
Alright, so that allows us to compare

208
00:07:23,50 --> 00:07:24,70
all of the possible solutions.

209
00:07:24,70 --> 00:07:27,40
So we're taking one queen and we're taking one queen,

210
00:07:27,40 --> 00:07:28,60
like in this first loop here.

211
00:07:28,60 --> 00:07:31,80
We're taking that queen, which is represented right here

212
00:07:31,80 --> 00:07:35,00
and we're comparing it against all of the other queens,

213
00:07:35,00 --> 00:07:36,30
which are right here.

214
00:07:36,30 --> 00:07:38,60
And then in the next one, we're taking this queen

215
00:07:38,60 --> 00:07:40,60
right there and we're comparing it

216
00:07:40,60 --> 00:07:42,20
against all of the other queens.

217
00:07:42,20 --> 00:07:44,40
We're taking this queen and we're comparing it

218
00:07:44,40 --> 00:07:45,70
against all of the other queens.

219
00:07:45,70 --> 00:07:48,00
And do we need to now compare three and seven again?

220
00:07:48,00 --> 00:07:49,20
No. We don't.

221
00:07:49,20 --> 00:07:50,80
We already compared three and seven.

222
00:07:50,80 --> 00:07:52,60
That's what's happening in this algorithm.

223
00:07:52,60 --> 00:07:54,60
So that stuff, if you're new to programming,

224
00:07:54,60 --> 00:07:58,00
it takes time to start thinking about that logic

225
00:07:58,00 --> 00:08:00,70
and figuring out how does that actually run?

226
00:08:00,70 --> 00:08:03,00
But I wanted to make sure that we stepped through it

227
00:08:03,00 --> 00:08:05,90
and you got that idea of the process that I used

228
00:08:05,90 --> 00:08:10,30
to break things out and visually just sort of pencil it out.

229
00:08:10,30 --> 00:08:11,80
Just see, how does this computer

230
00:08:11,80 --> 00:08:13,40
actually do these calculations

231
00:08:13,40 --> 00:08:15,10
and is it doing what I want?

232
00:08:15,10 --> 00:08:17,70
And a lot of times, if you're just learning programming,

233
00:08:17,70 --> 00:08:19,80
you find examples like this online.

234
00:08:19,80 --> 00:08:22,90
Like, oh, here's the solve, that Rosetta code,

235
00:08:22,90 --> 00:08:24,50
or wherever you might find it.

236
00:08:24,50 --> 00:08:26,20
And then just stop and try to understand

237
00:08:26,20 --> 00:08:28,00
what somebody else has created.

238
00:08:28,00 --> 00:08:29,50
And after doing that for a while,

239
00:08:29,50 --> 00:08:31,40
you will then be able to see,

240
00:08:31,40 --> 00:08:33,50
okay, I get it, and I'm starting to be able

241
00:08:33,50 --> 00:08:35,00
to write this stuff myself.

242
00:08:35,00 --> 00:08:36,80
Alright, so that's how we calculate

243
00:08:36,80 --> 00:08:39,10
all of our attack vectors

244
00:08:39,10 --> 00:08:40,80
and in the next video we're going to start looking

245
00:08:40,80 --> 00:08:43,80
at how do we start calculating the permutations.

246
00:08:43,80 --> 00:08:45,80
So this is a pretty complex algorithm.

247
00:08:45,80 --> 00:08:49,10
We'll move part of the way towards understanding it.

248
00:08:49,10 --> 00:08:50,50
I don't know if we'll get all the way,

249
00:08:50,50 --> 00:08:52,70
but we're certainly going to take some really big strides

250
00:08:52,70 --> 00:08:55,70
towards understanding how can we calculate permutations

251
00:08:55,70 --> 00:08:58,00
and we'll see that in the next video.

