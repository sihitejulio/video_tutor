1
00:00:00,50 --> 00:00:03,80
- So the first step in understanding our solution

2
00:00:03,80 --> 00:00:05,50
was this data structure,

3
00:00:05,50 --> 00:00:08,00
and we already saw how this data structure eliminates

4
00:00:08,00 --> 00:00:11,40
any conflicts between queens on horizontal

5
00:00:11,40 --> 00:00:12,90
or vertical vectors.

6
00:00:12,90 --> 00:00:15,20
And so a vector is just like a path way,

7
00:00:15,20 --> 00:00:17,30
or an arrow pointing a certain way.

8
00:00:17,30 --> 00:00:19,10
And so, on any Row or any Column,

9
00:00:19,10 --> 00:00:21,20
none of the queens are going to conflict.

10
00:00:21,20 --> 00:00:23,40
And so we have that data structure where none of the queens

11
00:00:23,40 --> 00:00:24,60
are going to conflict,

12
00:00:24,60 --> 00:00:26,90
could we somehow find all of the different

13
00:00:26,90 --> 00:00:28,70
permutations of this data structure?

14
00:00:28,70 --> 00:00:31,20
What do I mean by permutations?

15
00:00:31,20 --> 00:00:34,80
So let's say the data structure only had A and B.

16
00:00:34,80 --> 00:00:37,30
What are the permutations of A and B?

17
00:00:37,30 --> 00:00:38,90
So maybe pause this video

18
00:00:38,90 --> 00:00:40,60
if you need to reflect on that question

19
00:00:40,60 --> 00:00:42,40
because I know I'm a fast talker,

20
00:00:42,40 --> 00:00:44,80
and you need to take this point away.

21
00:00:44,80 --> 00:00:47,00
Step one, we got this data structure

22
00:00:47,00 --> 00:00:50,80
that eliminates Row and Column conflicts.

23
00:00:50,80 --> 00:00:53,20
Now, can we find all the different permutations?

24
00:00:53,20 --> 00:00:56,40
All the different arrangements of these eight digits

25
00:00:56,40 --> 00:00:57,60
in this data structure,

26
00:00:57,60 --> 00:00:59,00
can we find all of the different arrangements

27
00:00:59,00 --> 00:01:00,60
and combinations of that?

28
00:01:00,60 --> 00:01:03,40
Well, step two is to understand permutations.

29
00:01:03,40 --> 00:01:05,40
Because if we could get all of that data structures

30
00:01:05,40 --> 00:01:08,00
with different arrangements, then we could test each one

31
00:01:08,00 --> 00:01:09,70
in some way come up with an algorithm

32
00:01:09,70 --> 00:01:12,40
to see if there's a conflict diagonally.

33
00:01:12,40 --> 00:01:14,80
So step one, that data structure.

34
00:01:14,80 --> 00:01:16,80
Step two, getting permutation.

35
00:01:16,80 --> 00:01:18,70
And so this is the second take away.

36
00:01:18,70 --> 00:01:20,80
What are the permutations of A and B?

37
00:01:20,80 --> 00:01:23,30
Alright, so here are the permutations of A and B.

38
00:01:23,30 --> 00:01:26,40
A and B, and B and A, two permutations.

39
00:01:26,40 --> 00:01:29,20
So maybe now you understand what I mean by permutations.

40
00:01:29,20 --> 00:01:30,70
You ready for the next one?

41
00:01:30,70 --> 00:01:34,40
What are all the possible permutations of this set A, B, C?

42
00:01:34,40 --> 00:01:36,40
So maybe get a pencil and a piece of paper,

43
00:01:36,40 --> 00:01:38,70
try it out on your own and see if you could come up

44
00:01:38,70 --> 00:01:40,80
with all of the different permutations.

45
00:01:40,80 --> 00:01:42,70
Alright, you got it, here they are.

46
00:01:42,70 --> 00:01:45,40
We've got one, two, three, four, five, six,

47
00:01:45,40 --> 00:01:48,70
and the formula for calculating that by the way,

48
00:01:48,70 --> 00:01:50,80
is N!

49
00:01:50,80 --> 00:01:53,00
So if we had 8 items,

50
00:01:53,00 --> 00:01:56,50
they'd be 40320 possible permutations

51
00:01:56,50 --> 00:01:58,10
which is actually what we are looking at

52
00:01:58,10 --> 00:02:00,90
with that one data structure right here.

53
00:02:00,90 --> 00:02:03,70
We are looking at different things, and eight different

54
00:02:03,70 --> 00:02:05,30
that possible permutations are,

55
00:02:05,30 --> 00:02:07,20
that 40,000 number.

56
00:02:07,20 --> 00:02:10,00
This one, we had three, so three factorials,

57
00:02:10,00 --> 00:02:14,20
3 times 2 times 1, which is 6 possible permutations.

58
00:02:14,20 --> 00:02:16,00
Here, we have six possible permutations.

59
00:02:16,00 --> 00:02:17,70
Alright, are you ready for your next challenge?

60
00:02:17,70 --> 00:02:19,00
Here we've got four,

61
00:02:19,00 --> 00:02:22,20
so we've got got 4 times 3 times 2 times 1,

62
00:02:22,20 --> 00:02:24,60
24 possible permutations.

63
00:02:24,60 --> 00:02:26,90
So why don't you try it again if you want.

64
00:02:26,90 --> 00:02:30,00
Find an orderly way to start going through the permutations

65
00:02:30,00 --> 00:02:31,30
of these set right here.

66
00:02:31,30 --> 00:02:32,80
They'll be 24 possibilities.

67
00:02:32,80 --> 00:02:34,50
So maybe you are back from pausing your video

68
00:02:34,50 --> 00:02:35,60
and working that out,

69
00:02:35,60 --> 00:02:37,60
and here is the beginning of those permutations.

70
00:02:37,60 --> 00:02:38,90
And so you can kind of take a look

71
00:02:38,90 --> 00:02:41,50
at the order I was using to figure that out.

72
00:02:41,50 --> 00:02:44,90
And this is really the first step in designing an algorithm.

73
00:02:44,90 --> 00:02:48,20
You sit down if it's complex and you start drawing it out

74
00:02:48,20 --> 00:02:50,30
and writing it out, and thinking out,

75
00:02:50,30 --> 00:02:51,80
"How's everything going to work?"

76
00:02:51,80 --> 00:02:54,70
And you'll see as we even get into the nitty-gritty

77
00:02:54,70 --> 00:02:55,90
detail of this thing.

78
00:02:55,90 --> 00:02:59,20
Here I started mapping out the recursion of the algorithm

79
00:02:59,20 --> 00:03:01,00
which we are going to get into.

80
00:03:01,00 --> 00:03:03,40
And so, I mapped out step by step

81
00:03:03,40 --> 00:03:05,20
what that recursive algorithm

82
00:03:05,20 --> 00:03:07,60
which we'll eventually build is going to do.

83
00:03:07,60 --> 00:03:10,20
And I mapped it out partly so that you can see it.

84
00:03:10,20 --> 00:03:12,10
I really wanted you to be able to walk through

85
00:03:12,10 --> 00:03:14,80
and take a look at what's it's doing.

86
00:03:14,80 --> 00:03:16,50
And what's it's doing right down here

87
00:03:16,50 --> 00:03:18,30
is getting the permutations.

88
00:03:18,30 --> 00:03:20,10
So, the point being is,

89
00:03:20,10 --> 00:03:23,60
figuring out the solutions to a problem, it often starts

90
00:03:23,60 --> 00:03:25,70
with a pencil and a piece of paper at a desk

91
00:03:25,70 --> 00:03:27,10
and just penciling it out

92
00:03:27,10 --> 00:03:28,50
and how can we think about this

93
00:03:28,50 --> 00:03:30,30
and what data structures can we use?

94
00:03:30,30 --> 00:03:32,30
And how do we come up with the best solution?

95
00:03:32,30 --> 00:03:35,40
So for us, the best solution was using this data structure

96
00:03:35,40 --> 00:03:37,30
and then, we are going to find

97
00:03:37,30 --> 00:03:39,00
all the different possible permutations.

98
00:03:39,00 --> 00:03:41,60
Remember here, permutations of that data structure,

99
00:03:41,60 --> 00:03:44,00
we'll find all of the possible different arrangements

100
00:03:44,00 --> 00:03:48,60
and there will be 40,320 different possible arrangements.

101
00:03:48,60 --> 00:03:51,50
Once we have all of those different possible arrangements,

102
00:03:51,50 --> 00:03:54,00
we will go through, and we will check to see

103
00:03:54,00 --> 00:03:55,80
if there are any conflicts.

104
00:03:55,80 --> 00:03:58,60
If any of the attack vectors means that the queens

105
00:03:58,60 --> 00:04:00,60
could hit each other at an angle.

106
00:04:00,60 --> 00:04:02,00
So on the next video I'm going to show you

107
00:04:02,00 --> 00:04:03,80
some of the logic behind,

108
00:04:03,80 --> 00:04:05,70
"How do we calculate whether or not

109
00:04:05,70 --> 00:04:10,00
"there is a conflict on Row, a Column, or on a Diagonal."

