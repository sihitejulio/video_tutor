1
00:00:00,40 --> 00:00:03,50
- So, in this video we are going to explore

2
00:00:03,50 --> 00:00:05,80
how do we calculate permutations?

3
00:00:05,80 --> 00:00:07,40
And here's my challenge for you.

4
00:00:07,40 --> 00:00:09,40
Try writing an algorithm

5
00:00:09,40 --> 00:00:12,50
to calculate the permutations of this set.

6
00:00:12,50 --> 00:00:14,60
And if you just happen to be experimenting with Go,

7
00:00:14,60 --> 00:00:16,80
and you don't have your Go environment up and running,

8
00:00:16,80 --> 00:00:19,00
you can go to the golang playground.

9
00:00:19,00 --> 00:00:20,80
And here is a place online

10
00:00:20,80 --> 00:00:23,60
that you can totally just start writing Go code.

11
00:00:23,60 --> 00:00:26,40
Now, it will automatically do your imports for you

12
00:00:26,40 --> 00:00:29,00
when you click that and then hit Format.

13
00:00:29,00 --> 00:00:30,60
And you can click this to share it.

14
00:00:30,60 --> 00:00:33,30
So, if you get stuck you could go to stack overflow.

15
00:00:33,30 --> 00:00:35,80
Or you could go to the golang bridge forum,

16
00:00:35,80 --> 00:00:37,00
and you can say, "Hey, help me out.

17
00:00:37,00 --> 00:00:38,10
I'm trying to understand this."

18
00:00:38,10 --> 00:00:41,50
This, however, will not do code completion for you.

19
00:00:41,50 --> 00:00:44,30
So, you need to make sure you're syntax is spot on.

20
00:00:44,30 --> 00:00:45,80
And then you could run your code right here.

21
00:00:45,80 --> 00:00:47,00
And I'll show you the results.

22
00:00:47,00 --> 00:00:48,10
Pretty cool.

23
00:00:48,10 --> 00:00:49,50
So, I encourage you to try to figure out

24
00:00:49,50 --> 00:00:51,70
all the permutations of this set.

25
00:00:51,70 --> 00:00:53,70
Write a little program that'll do it.

26
00:00:53,70 --> 00:00:55,70
We are now going to show you how I did it.

27
00:00:55,70 --> 00:00:58,00
And I did it by starting out with just loops.

28
00:00:58,00 --> 00:01:00,10
And so, I have this slice of string,

29
00:01:00,10 --> 00:01:02,60
and I'm going to set it to variable options.

30
00:01:02,60 --> 00:01:04,40
And I'm going to range over options.

31
00:01:04,40 --> 00:01:07,30
So, if I only had that one loop there,

32
00:01:07,30 --> 00:01:09,40
that's going to print ABC.

33
00:01:09,40 --> 00:01:11,40
That's it, it won't print anything right now.

34
00:01:11,40 --> 00:01:12,40
I need a print statement.

35
00:01:12,40 --> 00:01:14,80
But I did print that, it'd print ABC.

36
00:01:14,80 --> 00:01:16,10
Well, now I have an inner loop,

37
00:01:16,10 --> 00:01:18,60
and inside that I have another inner loop.

38
00:01:18,60 --> 00:01:19,40
This is cool.

39
00:01:19,40 --> 00:01:20,90
It's totally understandable.

40
00:01:20,90 --> 00:01:24,10
It's good for just starting out and not too many options.

41
00:01:24,10 --> 00:01:25,90
And this will print all of the permutations.

42
00:01:25,90 --> 00:01:28,00
Let's go look.

43
00:01:28,00 --> 00:01:29,20
Go run main.

44
00:01:29,20 --> 00:01:30,80
There are all the permutations.

45
00:01:30,80 --> 00:01:31,90
And look at how it did it.

46
00:01:31,90 --> 00:01:34,00
So, the outer loop is this first column.

47
00:01:34,00 --> 00:01:35,90
And you could see that outer loop

48
00:01:35,90 --> 00:01:38,20
gave me range over the first option

49
00:01:38,20 --> 00:01:41,90
in options the value A a string to X.

50
00:01:41,90 --> 00:01:43,70
And then that's going to print here.

51
00:01:43,70 --> 00:01:46,20
And then the next loop right here

52
00:01:46,20 --> 00:01:49,10
is going to range over the options

53
00:01:49,10 --> 00:01:51,10
and ranging over the options on the inside.

54
00:01:51,10 --> 00:01:53,50
The first time this goes it'll assign A.

55
00:01:53,50 --> 00:01:55,50
And so that's going to be represented here.

56
00:01:55,50 --> 00:01:57,10
So, this loop goes.

57
00:01:57,10 --> 00:01:59,10
And then the next loop goes, and it's A.

58
00:01:59,10 --> 00:02:01,20
And then the inner loop goes, and it's A.

59
00:02:01,20 --> 00:02:02,90
And then the inner loop

60
00:02:02,90 --> 00:02:05,80
is going to go through all its options ABC.

61
00:02:05,80 --> 00:02:07,30
And then it'll be the second loop

62
00:02:07,30 --> 00:02:09,00
of the middle loop, BBB.

63
00:02:09,00 --> 00:02:10,60
And those go, ABC.

64
00:02:10,60 --> 00:02:12,80
And then CCC, ABC.

65
00:02:12,80 --> 00:02:15,30
And then now it's time for the first one to go again,

66
00:02:15,30 --> 00:02:17,30
BBB, AA ABC.

67
00:02:17,30 --> 00:02:19,40
So, that might be like if you're new to programming

68
00:02:19,40 --> 00:02:20,30
what is that doing?

69
00:02:20,30 --> 00:02:21,80
Let's just do this one.

70
00:02:21,80 --> 00:02:24,10
Take that one out, take that one out.

71
00:02:24,10 --> 00:02:25,50
And then I'll do,

72
00:02:25,50 --> 00:02:27,10
go fmt to format that.

73
00:02:27,10 --> 00:02:28,90
So, it's cleaned up a little bit.

74
00:02:28,90 --> 00:02:31,20
And then I'll do go run main.

75
00:02:31,20 --> 00:02:33,80
And oh, undefined y.

76
00:02:33,80 --> 00:02:36,20
So, we'll take y out.

77
00:02:36,20 --> 00:02:38,10
And clear that and then go run main again.

78
00:02:38,10 --> 00:02:39,60
AA, AB.

79
00:02:39,60 --> 00:02:41,20
So, what's that doing?

80
00:02:41,20 --> 00:02:43,80
We're ranging over all of the options

81
00:02:43,80 --> 00:02:45,20
on this big outer loop.

82
00:02:45,20 --> 00:02:46,40
So, big outer loop,

83
00:02:46,40 --> 00:02:48,00
and each time the big outer loop goes,

84
00:02:48,00 --> 00:02:49,40
the inner loop is going to go

85
00:02:49,40 --> 00:02:51,90
ABC, big outer loop, and then inner loop.

86
00:02:51,90 --> 00:02:56,00
ABC, big outer loop, and then inner loop, ABC.

87
00:02:56,00 --> 00:02:58,10
So, it's going to get A,

88
00:02:58,10 --> 00:03:00,00
and then it's going to go ABC.

89
00:03:00,00 --> 00:03:02,50
So, A and A,

90
00:03:02,50 --> 00:03:04,60
B, C, it'll print those.

91
00:03:04,60 --> 00:03:06,30
And then B,

92
00:03:06,30 --> 00:03:07,50
and then B's here.

93
00:03:07,50 --> 00:03:09,20
ABC,

94
00:03:09,20 --> 00:03:10,90
and then C,

95
00:03:10,90 --> 00:03:12,40
and then C's here,

96
00:03:12,40 --> 00:03:14,00
ABC, right?

97
00:03:14,00 --> 00:03:16,70
And each time it does that ABC, it prints.

98
00:03:16,70 --> 00:03:20,40
And so that's why we get A, ABC,

99
00:03:20,40 --> 00:03:22,10
B, ABC,

100
00:03:22,10 --> 00:03:24,10
C, ABC.

101
00:03:24,10 --> 00:03:25,60
And so that's the big outer loop

102
00:03:25,60 --> 00:03:27,10
and then little inner loop going over

103
00:03:27,10 --> 00:03:29,20
for each loop in the big outer loop.

104
00:03:29,20 --> 00:03:30,00
So, that's how that works.

105
00:03:30,00 --> 00:03:31,80
I'm going to put it back to the way it was.

106
00:03:31,80 --> 00:03:35,10
Let's reformat that.

107
00:03:35,10 --> 00:03:35,80
Go fmt.

108
00:03:35,80 --> 00:03:36,90
Go run main.

109
00:03:36,90 --> 00:03:38,40
And that's all working again.

110
00:03:38,40 --> 00:03:41,20
Make sure I leave it with you in working order.

111
00:03:41,20 --> 00:03:45,00
So, that's the first attempt at doing permutations.

112
00:03:45,00 --> 00:03:47,00
And so, I guess maybe I'll leave you

113
00:03:47,00 --> 00:03:48,80
with one more challenge.

114
00:03:48,80 --> 00:03:50,70
Now, maybe try writing the code

115
00:03:50,70 --> 00:03:52,90
for doing this one.

116
00:03:52,90 --> 00:03:54,40
And see if you could come up with a solution,

117
00:03:54,40 --> 00:03:56,20
and put it here on Go Playground.

118
00:03:56,20 --> 00:04:00,00
And you have this right here to start with.

