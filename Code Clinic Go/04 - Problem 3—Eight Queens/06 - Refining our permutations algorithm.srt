1
00:00:00,40 --> 00:00:03,10
- So we're now going to refine our permutation algorithm.

2
00:00:03,10 --> 00:00:05,40
And we were just looking at this first file right here.

3
00:00:05,40 --> 00:00:08,50
We created a bunch of permutations with nested loops.

4
00:00:08,50 --> 00:00:10,20
And let me just bring that file up.

5
00:00:10,20 --> 00:00:13,30
So it's 01, and then go run main.go.

6
00:00:13,30 --> 00:00:15,30
The challenge that I posed is

7
00:00:15,30 --> 00:00:18,20
find all the permutations of A B C.

8
00:00:18,20 --> 00:00:19,70
So the question I have is,

9
00:00:19,70 --> 00:00:22,30
"Are these all valid permutations?"

10
00:00:22,30 --> 00:00:24,40
And remember, again, the challenge for us

11
00:00:24,40 --> 00:00:27,00
was to find all of the permutations of A B C.

12
00:00:27,00 --> 00:00:29,70
So I'm asking, "Are these all valid permutations?"

13
00:00:29,70 --> 00:00:30,80
And I'll give you a hint.

14
00:00:30,80 --> 00:00:31,90
The answer is no.

15
00:00:31,90 --> 00:00:33,00
But why?

16
00:00:33,00 --> 00:00:35,30
Why are they all not valid permutations?

17
00:00:35,30 --> 00:00:37,30
The reason is because some of them

18
00:00:37,30 --> 00:00:39,20
are using the same letter twice.

19
00:00:39,20 --> 00:00:42,30
So B B, B B B, A C A,

20
00:00:42,30 --> 00:00:43,90
and we only have one A.

21
00:00:43,90 --> 00:00:44,70
We only have one B.

22
00:00:44,70 --> 00:00:46,00
We only have one C.

23
00:00:46,00 --> 00:00:49,70
So only things where each letter is unique are valid.

24
00:00:49,70 --> 00:00:52,50
And we now need to refine our permutation algorithm

25
00:00:52,50 --> 00:00:54,30
to just give us the valid results.

26
00:00:54,30 --> 00:00:56,10
And to do that, I did a little filter.

27
00:00:56,10 --> 00:00:59,00
And the filter I did here was this line right there.

28
00:00:59,00 --> 00:01:00,60
And let me just show you how it does filter it out.

29
00:01:00,60 --> 00:01:03,20
So change directories, 02,

30
00:01:03,20 --> 00:01:04,90
go run main.go.

31
00:01:04,90 --> 00:01:06,70
So there, it filtered everything out.

32
00:01:06,70 --> 00:01:09,90
So why is it that line right here filters out

33
00:01:09,90 --> 00:01:13,70
all of the double A's, triple A's, double B's,

34
00:01:13,70 --> 00:01:15,00
that kind of thing?

35
00:01:15,00 --> 00:01:17,10
And the reason is because

36
00:01:17,10 --> 00:01:19,20
let's say that X is capital A.

37
00:01:19,20 --> 00:01:21,70
And then what happens if Y is capital A?

38
00:01:21,70 --> 00:01:23,30
Well then, throw this one out

39
00:01:23,30 --> 00:01:24,80
because we can't have two A's.

40
00:01:24,80 --> 00:01:26,20
Okay, well let's say X is capital A

41
00:01:26,20 --> 00:01:28,00
but this time Z is capital A.

42
00:01:28,00 --> 00:01:28,90
Well, throw this one out.

43
00:01:28,90 --> 00:01:30,00
We can't have two A's.

44
00:01:30,00 --> 00:01:32,20
And we do that for each and every one of these.

45
00:01:32,20 --> 00:01:34,10
We just make sure that neither of them

46
00:01:34,10 --> 00:01:35,10
is equal to the other.

47
00:01:35,10 --> 00:01:37,50
So we want to compare X to Y and Z.

48
00:01:37,50 --> 00:01:41,50
And we want to compare Y to both X and Z.

49
00:01:41,50 --> 00:01:45,20
And we want to compare Z to both X and Y.

50
00:01:45,20 --> 00:01:46,40
So we just want to make sure each one

51
00:01:46,40 --> 00:01:47,90
is compared to the other two

52
00:01:47,90 --> 00:01:49,70
and that they're never equal.

53
00:01:49,70 --> 00:01:51,50
Because if they are, it means we have two A's

54
00:01:51,50 --> 00:01:52,90
or we have two B's.

55
00:01:52,90 --> 00:01:54,80
So that's the next step in my solution.

56
00:01:54,80 --> 00:01:56,50
And then the third thing I did just because

57
00:01:56,50 --> 00:01:57,90
I thought it was interesting

58
00:01:57,90 --> 00:01:59,50
was to put this all into a data structure

59
00:01:59,50 --> 00:02:00,90
and then to loop over it.

60
00:02:00,90 --> 00:02:04,00
So that was like one other option I added into that.

