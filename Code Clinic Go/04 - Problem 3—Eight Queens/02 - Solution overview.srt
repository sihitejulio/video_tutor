1
00:00:00,40 --> 00:00:01,80
- So the first thing I want to do

2
00:00:01,80 --> 00:00:04,30
is I want to give you the high level overview

3
00:00:04,30 --> 00:00:07,80
of my solution and what we're looking at here

4
00:00:07,80 --> 00:00:10,20
is in the 03_eight-queens folder.

5
00:00:10,20 --> 00:00:11,60
There's 08_solution.

6
00:00:11,60 --> 00:00:13,00
So the first thing I want to do

7
00:00:13,00 --> 00:00:14,90
is I just want to show you this thing run

8
00:00:14,90 --> 00:00:19,20
and come over there to folder 08 in my Terminal

9
00:00:19,20 --> 00:00:21,50
and hit go run main.go.

10
00:00:21,50 --> 00:00:23,60
Run this file, and there it is.

11
00:00:23,60 --> 00:00:26,10
92 possible answers.

12
00:00:26,10 --> 00:00:28,30
And as I scroll up through this, it shows me

13
00:00:28,30 --> 00:00:30,90
all of the possible different iterations

14
00:00:30,90 --> 00:00:32,80
where I could have queens.

15
00:00:32,80 --> 00:00:34,60
So, I think that's pretty cool.

16
00:00:34,60 --> 00:00:36,30
I like the way it all prints out

17
00:00:36,30 --> 00:00:39,20
and we can visually see that on the chessboard.

18
00:00:39,20 --> 00:00:40,40
Pretty darn amazing.

19
00:00:40,40 --> 00:00:42,50
The next thing I want to do is just provide you

20
00:00:42,50 --> 00:00:45,00
with a little bit more detail on depth

21
00:00:45,00 --> 00:00:46,50
to my solution, but still kind of keep it

22
00:00:46,50 --> 00:00:48,20
in the overview level.

23
00:00:48,20 --> 00:00:49,80
So, I don't want to jump in

24
00:00:49,80 --> 00:00:52,20
and start looking at all these hieroglyphics

25
00:00:52,20 --> 00:00:54,70
because this code gets kind of complicated

26
00:00:54,70 --> 00:00:56,00
kind of quickly.

27
00:00:56,00 --> 00:00:57,70
But obviously, you've got the files,

28
00:00:57,70 --> 00:00:59,70
you could jump in and you can start looking through it.

29
00:00:59,70 --> 00:01:02,10
We will get in and talk about some of this code,

30
00:01:02,10 --> 00:01:05,00
but it's going to be in a little bit more depth.

31
00:01:05,00 --> 00:01:07,40
The one thing I will point out in the overview,

32
00:01:07,40 --> 00:01:09,90
is this data structure right here.

33
00:01:09,90 --> 00:01:12,00
So I really like this data structure,

34
00:01:12,00 --> 00:01:15,20
because what this data structure represents, is this.

35
00:01:15,20 --> 00:01:18,50
So here we have a chessboard, we've got eight queens,

36
00:01:18,50 --> 00:01:20,30
we have our columns, and we have our rows

37
00:01:20,30 --> 00:01:22,50
and here's that data structure.

38
00:01:22,50 --> 00:01:24,00
You can kind of see how it's working

39
00:01:24,00 --> 00:01:28,00
so in row zero, the queen is in column seven,

40
00:01:28,00 --> 00:01:31,20
and in row one, index position one,

41
00:01:31,20 --> 00:01:33,50
the queen is in column three.

42
00:01:33,50 --> 00:01:36,90
And in row two, the queen is in column five.

43
00:01:36,90 --> 00:01:40,80
So the index position of this slice, is the row,

44
00:01:40,80 --> 00:01:44,60
and then the value of whatever's stored at that position

45
00:01:44,60 --> 00:01:46,30
is the column.

46
00:01:46,30 --> 00:01:48,70
And so because of this unique data structure,

47
00:01:48,70 --> 00:01:51,90
it's naturally eliminating all of the possibilities

48
00:01:51,90 --> 00:01:54,60
of not having a queen on the same row

49
00:01:54,60 --> 00:01:56,00
or the same column.

50
00:01:56,00 --> 00:01:58,80
So we only have one queen on each row and each column

51
00:01:58,80 --> 00:01:59,90
so you can see that.

52
00:01:59,90 --> 00:02:03,10
Remember our goal is to not have any queens conflict

53
00:02:03,10 --> 00:02:06,00
either diagonally or by our columns.

54
00:02:06,00 --> 00:02:08,10
And we'll get into more detail about some of the thinking

55
00:02:08,10 --> 00:02:11,10
behind this as we go into looking through the solution

56
00:02:11,10 --> 00:02:12,30
a little bit more.

57
00:02:12,30 --> 00:02:13,50
The last thing I want to show you

58
00:02:13,50 --> 00:02:15,10
in my overview of the solution

59
00:02:15,10 --> 00:02:17,50
is just some of the resources, which I have.

60
00:02:17,50 --> 00:02:20,30
So this is in a README file, right there.

61
00:02:20,30 --> 00:02:22,90
And I did some research on this, just to sort of

62
00:02:22,90 --> 00:02:24,10
get different perspectives

63
00:02:24,10 --> 00:02:25,30
and see what different people thought,

64
00:02:25,30 --> 00:02:27,70
and here's some links that I want to share with you.

65
00:02:27,70 --> 00:02:30,10
Here's a slideshow, here's another nice slideshow

66
00:02:30,10 --> 00:02:31,60
which talks about eight queens.

67
00:02:31,60 --> 00:02:34,40
There's a great overview of different solutions at this link

68
00:02:34,40 --> 00:02:36,80
and then Stanford has a wonderful handout on it.

69
00:02:36,80 --> 00:02:38,50
There's also a really nice video

70
00:02:38,50 --> 00:02:40,70
which gave me some very good inspirations.

71
00:02:40,70 --> 00:02:42,20
And then finally there's this great website

72
00:02:42,20 --> 00:02:44,70
I want you to know about, Rosetta Code.

73
00:02:44,70 --> 00:02:47,50
And I have that up here in my browser also.

74
00:02:47,50 --> 00:02:49,00
So here's Rosetta Code,

75
00:02:49,00 --> 00:02:52,80
and it's a website which basically allows the same task

76
00:02:52,80 --> 00:02:55,30
in as many different languages as possible.

77
00:02:55,30 --> 00:02:56,50
And so you can come through here,

78
00:02:56,50 --> 00:02:59,20
when you need to solve a task, and you could look

79
00:02:59,20 --> 00:03:01,00
what are the different tasks

80
00:03:01,00 --> 00:03:02,60
and who solved it in what language.

81
00:03:02,60 --> 00:03:04,50
And so there's an n-queens problem.

82
00:03:04,50 --> 00:03:07,70
Any number of queens, and there's a solution here in Go.

83
00:03:07,70 --> 00:03:09,90
This is actually the first time I'm looking at

84
00:03:09,90 --> 00:03:11,10
this particular one.

85
00:03:11,10 --> 00:03:14,20
But you might take a look at that also as another solution

86
00:03:14,20 --> 00:03:15,90
to the eight queens problem.

87
00:03:15,90 --> 00:03:18,30
Alright, so that's my high overview

88
00:03:18,30 --> 00:03:20,00
to the eight queens problem.

89
00:03:20,00 --> 00:03:21,70
That's an overview of my solution.

90
00:03:21,70 --> 00:03:22,90
And in the next couple of videos

91
00:03:22,90 --> 00:03:24,40
we're going to get into talking about

92
00:03:24,40 --> 00:03:27,00
some of the steps we took to come up with that solution.

