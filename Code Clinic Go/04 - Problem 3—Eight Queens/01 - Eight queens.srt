1
00:00:00,80 --> 00:00:06,70
(Machine whirring)

2
00:00:06,70 --> 00:00:08,90
- Hello, and welcome to Code Clinic.

3
00:00:08,90 --> 00:00:11,00
My name is Todd McLeod.

4
00:00:11,00 --> 00:00:13,20
Code Clinic is a course where a unique problem

5
00:00:13,20 --> 00:00:16,70
is introduced to a collection of lynda.com authors.

6
00:00:16,70 --> 00:00:19,10
In response, each author will create a solution

7
00:00:19,10 --> 00:00:22,00
using their programming language of choice.

8
00:00:22,00 --> 00:00:24,00
You can learn several things from Code Clinic.

9
00:00:24,00 --> 00:00:26,30
Different approaches to solving a problem,

10
00:00:26,30 --> 00:00:28,70
the pros and cons of different languages,

11
00:00:28,70 --> 00:00:31,20
and some tips and tricks to incorporate

12
00:00:31,20 --> 00:00:33,60
into your own coding practices.

13
00:00:33,60 --> 00:00:34,70
- In this challenge,

14
00:00:34,70 --> 00:00:37,20
we're working on a classic computer programming problem

15
00:00:37,20 --> 00:00:39,70
called The Eight Queens.

16
00:00:39,70 --> 00:00:42,20
This famous problem is often used during interviews,

17
00:00:42,20 --> 00:00:45,60
or to demonstrate the utility of a computer language.

18
00:00:45,60 --> 00:00:47,60
It requires an understanding of recursion

19
00:00:47,60 --> 00:00:49,30
and algorithm design,

20
00:00:49,30 --> 00:00:51,30
and it can be quite useful as an exercise

21
00:00:51,30 --> 00:00:52,90
in learning to program solutions

22
00:00:52,90 --> 00:00:55,60
for complex problems.

23
00:00:55,60 --> 00:00:58,60
This problem is proposed by Max Bezzle in 1848,

24
00:00:58,60 --> 00:01:02,10
and solved by Franz Nauck in 1850.

25
00:01:02,10 --> 00:01:03,60
The problem is simple.

26
00:01:03,60 --> 00:01:05,60
Start with a chessboard and eight queens.

27
00:01:05,60 --> 00:01:06,40
Then set up the board

28
00:01:06,40 --> 00:01:09,30
so that no two queens can attack each other.

29
00:01:09,30 --> 00:01:11,20
There is more than one solution,

30
00:01:11,20 --> 00:01:12,90
find them all.

31
00:01:12,90 --> 00:01:15,50
We already know there are 92 possible solutions.

32
00:01:15,50 --> 00:01:17,30
And we already have examples of the solutions

33
00:01:17,30 --> 00:01:19,50
in several computer languages.

34
00:01:19,50 --> 00:01:20,70
If you've never played chess,

35
00:01:20,70 --> 00:01:22,80
you'll need to understand that a queen can attack

36
00:01:22,80 --> 00:01:26,40
by moving an unlimited number of spaces in three directions:

37
00:01:26,40 --> 00:01:29,60
horizontally, vertically, and diagonally.

38
00:01:29,60 --> 00:01:33,30
This means that no two queens can share a row or a column,

39
00:01:33,30 --> 00:01:36,70
nor can they be located diagonally from each other.

40
00:01:36,70 --> 00:01:37,70
- In the following videos,

41
00:01:37,70 --> 00:01:40,50
I'll show you my solution to The Eight Queens problem.

42
00:01:40,50 --> 00:01:42,70
I'd encourage you to also look at the solutions

43
00:01:42,70 --> 00:01:45,70
from other authors in the lynda.com library.

44
00:01:45,70 --> 00:01:47,90
You'll be able to compare different author styles,

45
00:01:47,90 --> 00:01:50,00
and different languages.

