1
00:00:00,50 --> 00:00:01,80
- Alright, so we have our solution

2
00:00:01,80 --> 00:00:03,30
for this code clinic challenge

3
00:00:03,30 --> 00:00:05,50
and one of the main takeaways from this challenge

4
00:00:05,50 --> 00:00:07,50
is being able to work with directories

5
00:00:07,50 --> 00:00:10,00
and recursively go through directory structure

6
00:00:10,00 --> 00:00:11,20
and find the files in it

7
00:00:11,20 --> 00:00:12,80
and then do something with the file,

8
00:00:12,80 --> 00:00:14,40
and along those lines,

9
00:00:14,40 --> 00:00:15,40
I just wanted to share with you

10
00:00:15,40 --> 00:00:17,90
how can you append text to a bunch of files

11
00:00:17,90 --> 00:00:20,70
and how can you rename a bunch of files?

12
00:00:20,70 --> 00:00:22,60
So that's just something I find really useful

13
00:00:22,60 --> 00:00:24,10
and I wanted to pass it along.

14
00:00:24,10 --> 00:00:26,40
So the first thing to do is get all the files

15
00:00:26,40 --> 00:00:28,20
and I just put this in as a test

16
00:00:28,20 --> 00:00:29,80
and if you run this, it prints out

17
00:00:29,80 --> 00:00:31,60
all the files from right there.

18
00:00:31,60 --> 00:00:32,90
So I'm just kind of going through this

19
00:00:32,90 --> 00:00:35,20
in a methodical way to make sure that

20
00:00:35,20 --> 00:00:37,60
I don't accidentally rename or overwrite

21
00:00:37,60 --> 00:00:40,40
files I don't want renamed or overwritten.

22
00:00:40,40 --> 00:00:42,40
Or append text to something I don't want to

23
00:00:42,40 --> 00:00:43,80
the text appended to.

24
00:00:43,80 --> 00:00:45,90
So I just sort of check that right there

25
00:00:45,90 --> 00:00:48,70
and then the next thing I do is I do a test

26
00:00:48,70 --> 00:00:52,10
of appending this text to these files

27
00:00:52,10 --> 00:00:54,90
and I have the files before text is appended to them

28
00:00:54,90 --> 00:00:56,00
and after.

29
00:00:56,00 --> 00:00:58,20
So that if I mess up in my programming

30
00:00:58,20 --> 00:01:00,00
and append things incorrectly

31
00:01:00,00 --> 00:01:01,50
I can come back here and get the files

32
00:01:01,50 --> 00:01:03,30
the way they were and then put them here

33
00:01:03,30 --> 00:01:05,90
and these are both just throw away folders.

34
00:01:05,90 --> 00:01:07,60
I don't care about the files in there

35
00:01:07,60 --> 00:01:09,10
but as I'm testing this to make sure

36
00:01:09,10 --> 00:01:10,80
I've built this algorithm correctly,

37
00:01:10,80 --> 00:01:13,10
I'm going to do it on a folder with files

38
00:01:13,10 --> 00:01:14,70
which I don't care about,

39
00:01:14,70 --> 00:01:16,10
and then once everything is working,

40
00:01:16,10 --> 00:01:19,10
I'm going to apply that to this folder right here.

41
00:01:19,10 --> 00:01:20,90
So when I run the test, how's this working?

42
00:01:20,90 --> 00:01:23,30
How do we append text to a file?

43
00:01:23,30 --> 00:01:26,70
To append text to a file, you need to open your file

44
00:01:26,70 --> 00:01:28,90
with permissions to write.

45
00:01:28,90 --> 00:01:30,70
I want to be able to write to this file

46
00:01:30,70 --> 00:01:32,90
so we're using open file.

47
00:01:32,90 --> 00:01:35,90
Before we were using just os.Open.

48
00:01:35,90 --> 00:01:37,60
Well if we look at the standard library

49
00:01:37,60 --> 00:01:39,00
and the code that makes it up,

50
00:01:39,00 --> 00:01:42,00
you can see os.Open is actually returning

51
00:01:42,00 --> 00:01:45,30
OpenFile and if we go over to godoc.org

52
00:01:45,30 --> 00:01:47,10
we can read about OpenFile.

53
00:01:47,10 --> 00:01:50,40
It takes a name, the string, and then it takes a flag

54
00:01:50,40 --> 00:01:52,30
and it takes permissions,

55
00:01:52,30 --> 00:01:53,50
and the permissions it takes

56
00:01:53,50 --> 00:01:57,50
are like Unix or Posix perimissions.

57
00:01:57,50 --> 00:01:59,50
So file system permissions

58
00:01:59,50 --> 00:02:01,20
and what file system permissions are

59
00:02:01,20 --> 00:02:06,20
is if I did like ls-la it shows me this stuff right here

60
00:02:06,20 --> 00:02:08,20
and we have three sets of permissions,

61
00:02:08,20 --> 00:02:09,50
read, write, and execute.

62
00:02:09,50 --> 00:02:11,00
Read, write and execute,

63
00:02:11,00 --> 00:02:13,20
and read, write, execute right there.

64
00:02:13,20 --> 00:02:16,00
So this has read, write, and execute permissions.

65
00:02:16,00 --> 00:02:18,60
This has read and execute permissions,

66
00:02:18,60 --> 00:02:20,50
and this had read and execute permissions.

67
00:02:20,50 --> 00:02:22,10
Well what's the difference between each of these

68
00:02:22,10 --> 00:02:23,70
three triads?

69
00:02:23,70 --> 00:02:26,10
The first triad is what the owner can do.

70
00:02:26,10 --> 00:02:28,30
The second triad is what the group members can do

71
00:02:28,30 --> 00:02:31,70
and the third triad is what other users can do.

72
00:02:31,70 --> 00:02:33,70
And so the first character, obviously, is read.

73
00:02:33,70 --> 00:02:36,10
The second one is write, the third was execute.

74
00:02:36,10 --> 00:02:40,10
And then we use the numbers four, two, and one

75
00:02:40,10 --> 00:02:41,30
to signify those.

76
00:02:41,30 --> 00:02:43,60
So if we wanted to give the owner permission

77
00:02:43,60 --> 00:02:44,40
to read and write,

78
00:02:44,40 --> 00:02:46,00
we say four and two is six,

79
00:02:46,00 --> 00:02:47,90
so we'd give them permission six

80
00:02:47,90 --> 00:02:49,60
and if wanted to give that same permission

81
00:02:49,60 --> 00:02:52,70
to group member we'd also give them permission six

82
00:02:52,70 --> 00:02:55,50
and since four plus two is six, they'd have permission

83
00:02:55,50 --> 00:02:56,50
to read and write.

84
00:02:56,50 --> 00:03:00,20
So you see things in the standard library like this.

85
00:03:00,20 --> 00:03:02,60
Right here the permission is six, six, six.

86
00:03:02,60 --> 00:03:04,50
So that's what opening a file is

87
00:03:04,50 --> 00:03:06,10
and we're going to open a file

88
00:03:06,10 --> 00:03:08,00
and then give it permissions

89
00:03:08,00 --> 00:03:10,00
so that we can write to that file,

90
00:03:10,00 --> 00:03:12,20
which is located right here.

91
00:03:12,20 --> 00:03:13,70
So we're going to write to that file

92
00:03:13,70 --> 00:03:15,00
and once we have that file

93
00:03:15,00 --> 00:03:16,40
and we're allowed to write to it,

94
00:03:16,40 --> 00:03:19,40
we can do f.WriteString text

95
00:03:19,40 --> 00:03:21,40
and we're going to write that text to the file.

96
00:03:21,40 --> 00:03:23,00
Let's run this and we're going to run it against

97
00:03:23,00 --> 00:03:24,30
sample files after

98
00:03:24,30 --> 00:03:27,00
and we're going to see if our files change.

99
00:03:27,00 --> 00:03:30,20
So I'm just bring that up to show what it looks like before

100
00:03:30,20 --> 00:03:31,40
and now when we run it,

101
00:03:31,40 --> 00:03:34,80
we're going to run 15 02.

102
00:03:34,80 --> 00:03:36,70
So print working directory,

103
00:03:36,70 --> 00:03:40,20
I'm in 15 02, go run main.go.

104
00:03:40,20 --> 00:03:42,50
Really, a moment of truth.

105
00:03:42,50 --> 00:03:44,70
Alright, so just ran and it found

106
00:03:44,70 --> 00:03:46,50
all of these different files

107
00:03:46,50 --> 00:03:48,80
and look at what was added to the end of the file.

108
00:03:48,80 --> 00:03:51,30
Wow, that's totally awesome, I love that,

109
00:03:51,30 --> 00:03:53,90
and we can verify, hey did that work on all the files?

110
00:03:53,90 --> 00:03:55,50
It added to all the files.

111
00:03:55,50 --> 00:03:58,10
So that's a huge time saver right there.

112
00:03:58,10 --> 00:03:58,80
So the next thing I'm going to do

113
00:03:58,80 --> 00:04:01,80
is I'm going to apply this to this entire directory.

114
00:04:01,80 --> 00:04:03,50
So this entire directory, I need to add

115
00:04:03,50 --> 00:04:05,80
that copyright notice to it.

116
00:04:05,80 --> 00:04:08,30
So I'll just leave that closed for a minute

117
00:04:08,30 --> 00:04:09,70
so we can sort of see up there

118
00:04:09,70 --> 00:04:11,40
and then come down here

119
00:04:11,40 --> 00:04:15,70
and I open this up and I'm running it against there,

120
00:04:15,70 --> 00:04:20,30
and then I'm also appending this text

121
00:04:20,30 --> 00:04:22,90
and cross your fingers, it's a big job,

122
00:04:22,90 --> 00:04:24,60
and if we don't do it correctly,

123
00:04:24,60 --> 00:04:26,20
we could mess stuff up,

124
00:04:26,20 --> 00:04:27,40
but I've covered myself

125
00:04:27,40 --> 00:04:29,10
because I have backups and everything.

126
00:04:29,10 --> 00:04:30,90
So I need to change directories.

127
00:04:30,90 --> 00:04:34,10
Go up to 03.

128
00:04:34,10 --> 00:04:35,20
You ready?

129
00:04:35,20 --> 00:04:35,90
Here we go.

130
00:04:35,90 --> 00:04:38,00
So just ran against all of those folders.

131
00:04:38,00 --> 00:04:40,10
Computers are amazing, they can do things so quickly

132
00:04:40,10 --> 00:04:41,50
and let's take a look.

133
00:04:41,50 --> 00:04:43,10
Just randomly open one up

134
00:04:43,10 --> 00:04:44,70
and that looks pretty good.

135
00:04:44,70 --> 00:04:45,70
I'm happy with that.

136
00:04:45,70 --> 00:04:46,70
It's not perfect.

137
00:04:46,70 --> 00:04:48,70
To fix that what would I have done,

138
00:04:48,70 --> 00:04:51,10
maybe change the text, but you know what?

139
00:04:51,10 --> 00:04:52,80
That's good enough for me right now.

140
00:04:52,80 --> 00:04:55,40
So that was the first thing I wanted to share with you.

141
00:04:55,40 --> 00:04:56,90
Then the next one I want to share with you

142
00:04:56,90 --> 00:04:58,90
is being able to rename files,

143
00:04:58,90 --> 00:05:01,10
and so here I'm going to rename files

144
00:05:01,10 --> 00:05:02,90
and the files I'm going to rename,

145
00:05:02,90 --> 00:05:04,20
here are the files before.

146
00:05:04,20 --> 00:05:06,90
Let's put these files also in here

147
00:05:06,90 --> 00:05:08,20
because they'll be the ones we rename.

148
00:05:08,20 --> 00:05:09,70
I'll put them right there.

149
00:05:09,70 --> 00:05:11,80
Alright, and so that will be the files

150
00:05:11,80 --> 00:05:14,40
that I'm going to rename is in test-files.

151
00:05:14,40 --> 00:05:17,10
And to rename a file, I'm going to open up test-files,

152
00:05:17,10 --> 00:05:19,40
so I'm going to read, use ioutil all,

153
00:05:19,40 --> 00:05:20,50
and this is a little bit different,

154
00:05:20,50 --> 00:05:22,10
I'm going to read that directory

155
00:05:22,10 --> 00:05:23,70
and was does ReadDir give me?

156
00:05:23,70 --> 00:05:25,60
ReadDir takes in a Dir name

157
00:05:25,60 --> 00:05:29,30
and returns a slice of os.FileInfo.

158
00:05:29,30 --> 00:05:29,80
Okay?

159
00:05:29,80 --> 00:05:31,80
FileInfo, what is FileInfo?

160
00:05:31,80 --> 00:05:35,90
FileInfo is an interface and it has these different things

161
00:05:35,90 --> 00:05:37,30
defining the interface.

162
00:05:37,30 --> 00:05:39,40
Name, Size, Mode, ModTime.

163
00:05:39,40 --> 00:05:40,80
So how would we use that.

164
00:05:40,80 --> 00:05:43,20
So we've got our files right here

165
00:05:43,20 --> 00:05:45,20
and we get files and we range over them.

166
00:05:45,20 --> 00:05:46,40
So I get my...

167
00:05:46,40 --> 00:05:48,40
each file to pointer to file

168
00:05:48,40 --> 00:05:50,00
and I can print the name out

169
00:05:50,00 --> 00:05:52,30
and so I can say, okay, here's the oldfile,

170
00:05:52,30 --> 00:05:55,10
it's the directory it's in plus the file name,

171
00:05:55,10 --> 00:05:57,40
and then the new file will be the directory

172
00:05:57,40 --> 00:06:01,10
plus, I'm going to convert here my index,

173
00:06:01,10 --> 00:06:02,50
so that'll be some number

174
00:06:02,50 --> 00:06:04,80
and then I'm going to add just, I guess I'll

175
00:06:04,80 --> 00:06:06,60
make it a TXT file

176
00:06:06,60 --> 00:06:08,50
because right now these are GO files

177
00:06:08,50 --> 00:06:12,10
and then format, I'm going to print line oldfile to newfile.

178
00:06:12,10 --> 00:06:14,00
I'm just showing which is being converted

179
00:06:14,00 --> 00:06:18,60
and then I'm going to use os.Rename oldfile, newfile.

180
00:06:18,60 --> 00:06:21,60
So that should rename all of these files in here

181
00:06:21,60 --> 00:06:25,80
to text files numbered with the name of the file

182
00:06:25,80 --> 00:06:27,00
being a number.

183
00:06:27,00 --> 00:06:30,40
Those are JPGs, so I'll leave it as JPG.

184
00:06:30,40 --> 00:06:32,00
So let's take a look at this running,

185
00:06:32,00 --> 00:06:36,90
bring up my terminal and its in 16.

186
00:06:36,90 --> 00:06:40,70
And ls and go run main.go.

187
00:06:40,70 --> 00:06:43,30
So it's telling me that it worked on a couple of files

188
00:06:43,30 --> 00:06:45,40
and you can see here, cropped Macaw

189
00:06:45,40 --> 00:06:49,70
blue feathers test files 3 with 3 cropped Macaw parrot

190
00:06:49,70 --> 00:06:51,10
with yellow and blue feathers

191
00:06:51,10 --> 00:06:54,00
to test-files/3.jpg.

192
00:06:54,00 --> 00:06:56,40
Named from this right here

193
00:06:56,40 --> 00:06:58,10
to this.

194
00:06:58,10 --> 00:06:59,70
So that name right there.

195
00:06:59,70 --> 00:07:01,00
So let's look at them

196
00:07:01,00 --> 00:07:04,20
and there you can see, I've renamed all of those files.

197
00:07:04,20 --> 00:07:06,10
So those are two things which I found useful

198
00:07:06,10 --> 00:07:09,00
and I hope they are useful to you.

