1
00:00:00,90 --> 00:00:06,80
(Machine whirring)

2
00:00:06,80 --> 00:00:08,60
- Hello, and welcome to Code Clinic.

3
00:00:08,60 --> 00:00:10,60
My name is Todd McLeod.

4
00:00:10,60 --> 00:00:12,80
Code Clinic is a course where unique problems

5
00:00:12,80 --> 00:00:15,70
are introduced to a collection of lynda.com authors.

6
00:00:15,70 --> 00:00:18,30
In response, each author will create a solution

7
00:00:18,30 --> 00:00:20,80
using their programming language of choice.

8
00:00:20,80 --> 00:00:22,90
You can learn several things from Code Clinic.

9
00:00:22,90 --> 00:00:24,80
Different approaches to solving a problem,

10
00:00:24,80 --> 00:00:27,00
the pros and cons of different languages,

11
00:00:27,00 --> 00:00:28,90
and some tips and tricks to incorporate

12
00:00:28,90 --> 00:00:31,70
into your own coding practices.

13
00:00:31,70 --> 00:00:35,10
- In this challenge, the problem combines two concepts,

14
00:00:35,10 --> 00:00:38,10
recursion and accessing image data.

15
00:00:38,10 --> 00:00:39,80
Recursion means to repeat something

16
00:00:39,80 --> 00:00:41,40
in a similar way.

17
00:00:41,40 --> 00:00:42,50
In programming,

18
00:00:42,50 --> 00:00:45,60
recursion means a function will actually call itself,

19
00:00:45,60 --> 00:00:47,50
nesting a call to a subroutine

20
00:00:47,50 --> 00:00:50,00
within a call to the same subroutine.

21
00:00:50,00 --> 00:00:51,20
Look for a code like this

22
00:00:51,20 --> 00:00:54,60
in the samples you are about to see from the authors.

23
00:00:54,60 --> 00:00:58,00
JPEG files can contain additional image data,

24
00:00:58,00 --> 00:01:00,90
stored as Exif, or IPTC.

25
00:01:00,90 --> 00:01:05,20
Exif stands for Exchangeable Image File Format,

26
00:01:05,20 --> 00:01:08,00
and is a well documented standard.

27
00:01:08,00 --> 00:01:09,00
If you have a digital camera,

28
00:01:09,00 --> 00:01:11,90
or have taken photos with a newer cellphone camera,

29
00:01:11,90 --> 00:01:15,40
the image probably has Exif data available.

30
00:01:15,40 --> 00:01:16,90
Using a Macintosh,

31
00:01:16,90 --> 00:01:18,70
you can see this metadata information

32
00:01:18,70 --> 00:01:21,10
by opening the image in Preview,

33
00:01:21,10 --> 00:01:23,40
opening Tools, Show Inspector,

34
00:01:23,40 --> 00:01:26,80
and selecting the Exif or IPTC tab.

35
00:01:26,80 --> 00:01:28,90
On Windows, you can see metadata

36
00:01:28,90 --> 00:01:30,20
by right-clicking an image,

37
00:01:30,20 --> 00:01:33,40
and selecting "Properties" in the Details tab.

38
00:01:33,40 --> 00:01:35,60
You'll see things like caption, dimensions,

39
00:01:35,60 --> 00:01:39,60
camera type, colour space, exposure information,

40
00:01:39,60 --> 00:01:41,60
and other details.

41
00:01:41,60 --> 00:01:44,40
Cellphones will also embed geographic location data,

42
00:01:44,40 --> 00:01:47,90
identifying the longitude and latitude.

43
00:01:47,90 --> 00:01:50,00
The challenge is to look through the example files

44
00:01:50,00 --> 00:01:51,50
included with Code Clinic,

45
00:01:51,50 --> 00:01:54,90
find images, extract the description from the metadata,

46
00:01:54,90 --> 00:01:59,50
then reorganise those photos into alphabetical order.

47
00:01:59,50 --> 00:02:01,30
- As always, you may want to take some time

48
00:02:01,30 --> 00:02:03,10
and solve the problem yourself.

49
00:02:03,10 --> 00:02:04,20
In the next videos,

50
00:02:04,20 --> 00:02:07,00
I'll show you how I solved this challenge.

