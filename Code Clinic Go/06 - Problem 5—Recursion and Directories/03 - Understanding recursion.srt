1
00:00:00,40 --> 00:00:02,50
- Let's take a look at how recursion works,

2
00:00:02,50 --> 00:00:04,30
and then, after we've done that, we will take a look

3
00:00:04,30 --> 00:00:07,20
at how recursion is being used to walk

4
00:00:07,20 --> 00:00:08,80
through all of this directory.

5
00:00:08,80 --> 00:00:11,60
We'll see how "filepath-walk" is implemented

6
00:00:11,60 --> 00:00:14,10
in the standard library

7
00:00:14,10 --> 00:00:16,30
and we'll see recursion at use there.

8
00:00:16,30 --> 00:00:19,00
To understand recursion, I think "factorial" is

9
00:00:19,00 --> 00:00:20,90
the best example to begin with.

10
00:00:20,90 --> 00:00:24,00
Here's the basic idea of recursion.

11
00:00:24,00 --> 00:00:27,30
We have "func main" and, in here,

12
00:00:27,30 --> 00:00:29,70
we're calling function, "factorial,"

13
00:00:29,70 --> 00:00:33,00
and factorial's going to return an "int"

14
00:00:33,00 --> 00:00:35,70
and we're passing into at "4" as an argument.

15
00:00:35,70 --> 00:00:39,40
Factorial has a parameter of wanting an int,

16
00:00:39,40 --> 00:00:42,10
and we are passing in 4 as an argument.

17
00:00:42,10 --> 00:00:43,70
Just making a distinction there

18
00:00:43,70 --> 00:00:45,50
between parameter and argument.

19
00:00:45,50 --> 00:00:48,60
What this does is, if x equals "zero,"

20
00:00:48,60 --> 00:00:50,50
then we're going to return "one."

21
00:00:50,50 --> 00:00:52,40
Otherwise, we're going to return "x"

22
00:00:52,40 --> 00:00:56,30
multiplied by "factorial (x-1)."

23
00:00:56,30 --> 00:00:58,80
We're calling factorial again, and we're passing

24
00:00:58,80 --> 00:01:00,70
in a new number, and then that's

25
00:01:00,70 --> 00:01:02,30
going to return a number.

26
00:01:02,30 --> 00:01:04,10
There's functions nested within functions

27
00:01:04,10 --> 00:01:05,70
nested within functions.

28
00:01:05,70 --> 00:01:07,10
When you first come across this,

29
00:01:07,10 --> 00:01:09,20
it's a little bit hard to wrap your head around.

30
00:01:09,20 --> 00:01:12,10
As you get into more difficult recursive algorithms,

31
00:01:12,10 --> 00:01:14,10
it's additionally more difficult

32
00:01:14,10 --> 00:01:15,90
to wrap your head around it.

33
00:01:15,90 --> 00:01:19,60
One thing I want to say is that I have heard

34
00:01:19,60 --> 00:01:23,20
that anything that you do with recursion,

35
00:01:23,20 --> 00:01:27,20
you can also do it just with regular programming.

36
00:01:27,20 --> 00:01:29,30
With loops, with conditional logic,

37
00:01:29,30 --> 00:01:32,30
so you don't necessarily need to use recursion.

38
00:01:32,30 --> 00:01:35,70
That said, some problems lend themselves well

39
00:01:35,70 --> 00:01:38,20
to recursion, and some do not.

40
00:01:38,20 --> 00:01:40,00
By becoming familiar with recursion,

41
00:01:40,00 --> 00:01:43,30
you can then choose the right tool for the right job.

42
00:01:43,30 --> 00:01:45,60
Let me show you something that always seems

43
00:01:45,60 --> 00:01:47,50
to help people understand recursion,

44
00:01:47,50 --> 00:01:50,70
and it definitely helped me when I was first learning it.

45
00:01:50,70 --> 00:01:53,20
Here we have that factorial function.

46
00:01:53,20 --> 00:01:57,10
You can see in my example, I am passing in 4.

47
00:01:57,10 --> 00:01:59,80
If I was to pass in four right here,

48
00:01:59,80 --> 00:02:01,80
four equal to zero, no.

49
00:02:01,80 --> 00:02:04,00
We're going to have four right there,

50
00:02:04,00 --> 00:02:06,60
and then we're going to pass in factorial (4-1).

51
00:02:06,60 --> 00:02:08,50
This is what we're going to return.

52
00:02:08,50 --> 00:02:12,00
If we return that, cool, we're returning that

53
00:02:12,00 --> 00:02:14,80
to wherever it was called; this is where it was called.

54
00:02:14,80 --> 00:02:18,10
But before we can return it, this has to execute.

55
00:02:18,10 --> 00:02:19,90
Let's evaluate that.

56
00:02:19,90 --> 00:02:22,90
Factorial (4-1) is going to be factorial three.

57
00:02:22,90 --> 00:02:25,30
Is three equal to zero? That's not true.

58
00:02:25,30 --> 00:02:29,30
We're going to have three times factorial three minus one.

59
00:02:29,30 --> 00:02:33,10
Ok, we have 3 times factorial (3-1).

60
00:02:33,10 --> 00:02:36,40
Before we can return this, we have to evaluate this one.

61
00:02:36,40 --> 00:02:38,10
Let's evaluate that one.

62
00:02:38,10 --> 00:02:41,90
So factorial two would pass into two, is two equal to zero?

63
00:02:41,90 --> 00:02:44,90
That's not equal to zero, so we're going to come down here.

64
00:02:44,90 --> 00:02:47,40
2 times factorial (2-1).

65
00:02:47,40 --> 00:02:51,30
Before we could return, however, we need to evaluate this.

66
00:02:51,30 --> 00:02:54,20
Let's evaluate that, let's pass in factorial one.

67
00:02:54,20 --> 00:02:56,80
So factorial one, is one equal to zero?

68
00:02:56,80 --> 00:02:58,30
That's not true, so we have

69
00:02:58,30 --> 00:03:00,20
one times factorial one minus one.

70
00:03:00,20 --> 00:03:02,80
1 times factorial (1-1).

71
00:03:02,80 --> 00:03:04,90
Cool, we're getting somewhere;

72
00:03:04,90 --> 00:03:08,50
however, before we can return, we need to evaluate this.

73
00:03:08,50 --> 00:03:12,60
It just keeps going, it's a function calling itself.

74
00:03:12,60 --> 00:03:16,80
Factorial (1-1) evaluates, which is factorial zero.

75
00:03:16,80 --> 00:03:19,20
Is zero equal to zero, true, return one.

76
00:03:19,20 --> 00:03:21,40
Hey, what do you know?

77
00:03:21,40 --> 00:03:23,70
We no longer have to call factorial.

78
00:03:23,70 --> 00:03:25,70
So this is known as the base case.

79
00:03:25,70 --> 00:03:28,50
You can see if we don't have some condition

80
00:03:28,50 --> 00:03:30,60
where eventually we stopped calling our function

81
00:03:30,60 --> 00:03:33,50
from within the function, this would just go on eternally,

82
00:03:33,50 --> 00:03:35,40
and we would have an infinite kind

83
00:03:35,40 --> 00:03:37,80
of recursive algorithm which never finished.

84
00:03:37,80 --> 00:03:40,00
We'd never get a result.

85
00:03:40,00 --> 00:03:41,60
But now that that is all done,

86
00:03:41,60 --> 00:03:44,20
we do the multiplication and we get 24,

87
00:03:44,20 --> 00:03:48,40
and then we return 24 back to here, and it gets printed.

88
00:03:48,40 --> 00:03:51,10
That's the basic idea of recursion.

89
00:03:51,10 --> 00:03:54,30
You can think about it like this, and this is good to know,

90
00:03:54,30 --> 00:03:56,90
because here is the call stack of functions.

91
00:03:56,90 --> 00:03:59,60
We start with "main" and then the first thing

92
00:03:59,60 --> 00:04:02,60
that happens is main calls factorial 4.

93
00:04:02,60 --> 00:04:05,30
Then after factorial 4 is called,

94
00:04:05,30 --> 00:04:07,90
that calls factorial 3.

95
00:04:07,90 --> 00:04:10,80
So we have these functions sort of stacking up on the stack.

96
00:04:10,80 --> 00:04:14,40
Then after factorial 3 is called, it calls factorial 2,

97
00:04:14,40 --> 00:04:16,30
and it calls factorial 1, and then

98
00:04:16,30 --> 00:04:18,80
that finally calls factorial 0 which gives us 1.

99
00:04:18,80 --> 00:04:22,90
Then that factorial 0 returns its value

100
00:04:22,90 --> 00:04:25,90
to the previous call which is factorial 1.

101
00:04:25,90 --> 00:04:28,80
Factorial 1 returns its value to the previous call

102
00:04:28,80 --> 00:04:31,20
which was the function factorial 2.

103
00:04:31,20 --> 00:04:34,40
Factorial 2 returns its value to the previous function

104
00:04:34,40 --> 00:04:38,30
which is factorial 3, and factorial 3 returns its value

105
00:04:38,30 --> 00:04:39,80
to the previous function that called it,

106
00:04:39,80 --> 00:04:41,00
which is factorial 4.

107
00:04:41,00 --> 00:04:44,50
Then factorial 4 returns its value to the function

108
00:04:44,50 --> 00:04:46,80
which called it which was main.

109
00:04:46,80 --> 00:04:49,10
It returns its value to right here

110
00:04:49,10 --> 00:04:52,20
where main, the print line called factorial 4.

111
00:04:52,20 --> 00:04:54,90
We have these functions kind of stacking up

112
00:04:54,90 --> 00:04:57,60
on top of each other, and they build this stack.

113
00:04:57,60 --> 00:05:01,00
Then as they return, the stack decreases.

114
00:05:01,00 --> 00:05:03,10
That's a really good representation

115
00:05:03,10 --> 00:05:05,70
of thinking about all of these functions.

116
00:05:05,70 --> 00:05:07,80
You can see how recursive algorithms

117
00:05:07,80 --> 00:05:09,40
with a lot of functions calling functions

118
00:05:09,40 --> 00:05:11,30
calling functions can start to

119
00:05:11,30 --> 00:05:13,90
become expensive in terms of performance.

120
00:05:13,90 --> 00:05:16,50
You could have thousands, hundreds of thousands,

121
00:05:16,50 --> 00:05:18,60
of functions and it just doesn't make any sense anymore.

122
00:05:18,60 --> 00:05:21,70
It just is too computationally intensive

123
00:05:21,70 --> 00:05:24,50
and expensive to use factorial.

124
00:05:24,50 --> 00:05:26,90
It's nice little way to understand it.

125
00:05:26,90 --> 00:05:30,40
Now, let's just verify that factorial 4 actually gives us

126
00:05:30,40 --> 00:05:32,10
the result that we expected it would give us.

127
00:05:32,10 --> 00:05:34,70
If we look at this, we should be getting 24.

128
00:05:34,70 --> 00:05:37,20
So let's run our code, and we will see

129
00:05:37,20 --> 00:05:39,60
if all goes right, factorial 4.

130
00:05:39,60 --> 00:05:41,40
So I'm going to change directories

131
00:05:41,40 --> 00:05:46,20
and go up into "01_factorial" "go run main.go"

132
00:05:46,20 --> 00:05:47,90
and 24 is my value.

133
00:05:47,90 --> 00:05:49,30
Now I'm going to leave Fibonacci

134
00:05:49,30 --> 00:05:51,20
for you to reason about on your own,

135
00:05:51,20 --> 00:05:53,20
but it's another really nice example.

136
00:05:53,20 --> 00:05:55,40
This one, how those functions call,

137
00:05:55,40 --> 00:05:56,80
I will step you through this.

138
00:05:56,80 --> 00:05:58,80
So fibonacci (4) is going to return

139
00:05:58,80 --> 00:06:00,70
two calls to the function.

140
00:06:00,70 --> 00:06:03,40
So we have two of them, 4-1, 4-2.

141
00:06:03,40 --> 00:06:06,00
Each of these are going to, when they're called,

142
00:06:06,00 --> 00:06:07,60
are going to return to functions.

143
00:06:07,60 --> 00:06:10,40
That one will return these two,

144
00:06:10,40 --> 00:06:13,30
and this one will return those two,

145
00:06:13,30 --> 00:06:16,70
and now each of these is going to return, too.

146
00:06:16,70 --> 00:06:19,30
So that one is going to return these two,

147
00:06:19,30 --> 00:06:23,30
and this one is going to return those two,

148
00:06:23,30 --> 00:06:26,00
and this one is going to return those two,

149
00:06:26,00 --> 00:06:30,00
and this one is going to return those two, all right, cool.

150
00:06:30,00 --> 00:06:32,30
Then when we evaluate all that down,

151
00:06:32,30 --> 00:06:34,30
we do have our base case here,

152
00:06:34,30 --> 00:06:36,10
where if n is less than or equal to one,

153
00:06:36,10 --> 00:06:37,60
we return one.

154
00:06:37,60 --> 00:06:39,10
So when we evaluate all that down,

155
00:06:39,10 --> 00:06:42,00
we get this and then when we evaluate each of these

156
00:06:42,00 --> 00:06:44,10
we end up getting that, so we get five.

157
00:06:44,10 --> 00:06:46,50
So you can sort of look that one over on your own.

158
00:06:46,50 --> 00:06:48,30
Let's take a quick glance at how recursion

159
00:06:48,30 --> 00:06:50,90
is being used in filepath-walk.

160
00:06:50,90 --> 00:06:53,20
If we go to walk and we open that up,

161
00:06:53,20 --> 00:06:55,70
hold down "command," click on the link in webstorm.

162
00:06:55,70 --> 00:06:57,90
Hopefully this works in your editor, too.

163
00:06:57,90 --> 00:07:00,30
I open that up and I see, okay, it does stuff,

164
00:07:00,30 --> 00:07:02,00
and then it calls "walk."

165
00:07:02,00 --> 00:07:03,90
I look at this and here's walk,

166
00:07:03,90 --> 00:07:06,90
and if I hold down Ctrl+Cmd+G,

167
00:07:06,90 --> 00:07:09,10
just selected every instance of "walk" on the page.

168
00:07:09,10 --> 00:07:10,80
If you want to know how to do that,

169
00:07:10,80 --> 00:07:12,60
I've pointed this out once before,

170
00:07:12,60 --> 00:07:14,60
but you can look at the Default Keymap Reference

171
00:07:14,60 --> 00:07:16,90
to see many of the cool little shortcuts.

172
00:07:16,90 --> 00:07:18,80
Just print that out, laminate it, put it on your desk.

173
00:07:18,80 --> 00:07:20,80
If you're using Webstorm, it's nice.

174
00:07:20,80 --> 00:07:23,00
So "walk" is doing it's walking,

175
00:07:23,00 --> 00:07:26,50
it calls walk again right here.

176
00:07:26,50 --> 00:07:28,60
So you can see that this recursive algorithm

177
00:07:28,60 --> 00:07:31,80
is written by somebody who is a black belt coder.

178
00:07:31,80 --> 00:07:33,20
They have been coding for awhile

179
00:07:33,20 --> 00:07:36,00
and they've really reasoned about how recursion works

180
00:07:36,00 --> 00:07:38,10
and they have a solid understanding of it

181
00:07:38,10 --> 00:07:40,10
to put together this type of code.

182
00:07:40,10 --> 00:07:42,40
That's an introduction to recursion.

183
00:07:42,40 --> 00:07:44,80
I think that's one of the most important concepts

184
00:07:44,80 --> 00:07:46,40
from this challenge.

185
00:07:46,40 --> 00:07:49,10
I just wanted to make sure you had a solid grasp on it.

186
00:07:49,10 --> 00:07:50,40
In the next couple of videos,

187
00:07:50,40 --> 00:07:53,00
we'll go into some of the other details of MySolve.

