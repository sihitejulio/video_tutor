1
00:00:00,50 --> 00:00:02,50
- So I'm excited to share my solution

2
00:00:02,50 --> 00:00:04,10
with you for this challenge

3
00:00:04,10 --> 00:00:06,50
because there's some useful stuff in here

4
00:00:06,50 --> 00:00:09,50
and in addition to showing you my solution

5
00:00:09,50 --> 00:00:10,40
for this challenge,

6
00:00:10,40 --> 00:00:12,90
I've also included some stuff which I find useful

7
00:00:12,90 --> 00:00:16,10
in my own life, like if I am in this directory,

8
00:00:16,10 --> 00:00:18,60
right here, and I want to go through

9
00:00:18,60 --> 00:00:21,20
and I want to add something to the end

10
00:00:21,20 --> 00:00:24,50
of every file, like this code right here,

11
00:00:24,50 --> 00:00:27,90
this is all code that I use to teach at Fresno State

12
00:00:27,90 --> 00:00:29,50
and at a variety of other places

13
00:00:29,50 --> 00:00:32,90
and so this code is licensed for everybody.

14
00:00:32,90 --> 00:00:34,10
It's public domain

15
00:00:34,10 --> 00:00:37,10
and I need to add the license to the end

16
00:00:37,10 --> 00:00:39,60
of all of these files and that's a lot of files.

17
00:00:39,60 --> 00:00:41,80
So before I knew programming,

18
00:00:41,80 --> 00:00:43,00
the way I might have done that

19
00:00:43,00 --> 00:00:44,20
would be, I'd come over here

20
00:00:44,20 --> 00:00:45,20
and I'd get the license

21
00:00:45,20 --> 00:00:46,70
and then I'd come over here

22
00:00:46,70 --> 00:00:48,50
and I would paste that in.

23
00:00:48,50 --> 00:00:50,30
That would be very laborious,

24
00:00:50,30 --> 00:00:51,70
that would take forever to go

25
00:00:51,70 --> 00:00:53,20
and do that to all those files.

26
00:00:53,20 --> 00:00:55,40
So I'm going to show you how programmatically

27
00:00:55,40 --> 00:00:58,00
we can go through all of these files

28
00:00:58,00 --> 00:01:01,30
and add that text to the end of every file.

29
00:01:01,30 --> 00:01:02,50
So that's a real time saver,

30
00:01:02,50 --> 00:01:04,20
and I'm excited to share that with you

31
00:01:04,20 --> 00:01:07,20
and you will notice that before I run that code,

32
00:01:07,20 --> 00:01:10,00
I have copied this directory and I have pit it right there,

33
00:01:10,00 --> 00:01:11,70
bup backup.

34
00:01:11,70 --> 00:01:13,20
So that if my code doesn't work,

35
00:01:13,20 --> 00:01:14,80
if I mess up all these files,

36
00:01:14,80 --> 00:01:16,10
I can always just come back

37
00:01:16,10 --> 00:01:17,80
and get the way they were right before.

38
00:01:17,80 --> 00:01:19,30
So that's just thinking ahead.

39
00:01:19,30 --> 00:01:21,70
That's called gaining experience from life.

40
00:01:21,70 --> 00:01:22,90
And then I'm also going to show you

41
00:01:22,90 --> 00:01:24,80
how to do a renamer.

42
00:01:24,80 --> 00:01:27,60
So if I have a directory full of different files

43
00:01:27,60 --> 00:01:28,80
and I want to rename them,

44
00:01:28,80 --> 00:01:30,50
we could do that recursively.

45
00:01:30,50 --> 00:01:31,90
So I'm going to show you how to use that.

46
00:01:31,90 --> 00:01:33,40
So these two things I find really useful

47
00:01:33,40 --> 00:01:34,70
and helpful in my life

48
00:01:34,70 --> 00:01:37,20
and I'm excited just to pass those onto you,

49
00:01:37,20 --> 00:01:38,50
so that you can use them too.

50
00:01:38,50 --> 00:01:39,80
That's the power of programming,

51
00:01:39,80 --> 00:01:42,40
automating something, like using a machine

52
00:01:42,40 --> 00:01:45,00
to do repetitive, menial tasks,

53
00:01:45,00 --> 00:01:46,80
so that we don't have to do it ourselves.

54
00:01:46,80 --> 00:01:48,00
So let's take a look at my solution,

55
00:01:48,00 --> 00:01:50,50
get a high level overview for this challenge

56
00:01:50,50 --> 00:01:53,10
and then we'll move in to taking a look at the details,

57
00:01:53,10 --> 00:01:55,90
but here is the high level overview.

58
00:01:55,90 --> 00:01:58,00
So I'm going to clear out all this stuff in my terminal.

59
00:01:58,00 --> 00:01:59,50
I'm going to take a look, see where I'm at,

60
00:01:59,50 --> 00:02:02,70
what directory I'm in and I'm in 05_dir-recursion.

61
00:02:02,70 --> 00:02:05,60
That's where I want to be, 14_change-names.

62
00:02:05,60 --> 00:02:07,20
14_change-names.

63
00:02:07,20 --> 00:02:08,20
That's where I want to be

64
00:02:08,20 --> 00:02:10,60
and I take a moment to do that

65
00:02:10,60 --> 00:02:12,30
because I kid you not, there've been many times

66
00:02:12,30 --> 00:02:14,70
I'm like, why is my code not working

67
00:02:14,70 --> 00:02:17,50
and I'd be like, I'm running the code here,

68
00:02:17,50 --> 00:02:20,00
when really I've been changing the code here,

69
00:02:20,00 --> 00:02:21,60
fixing it, and then I'm running it here,

70
00:02:21,60 --> 00:02:23,10
and I'm like it's not working.

71
00:02:23,10 --> 00:02:24,70
So you've always got to check.

72
00:02:24,70 --> 00:02:27,50
Alright, go run main.go, let's see what happens.

73
00:02:27,50 --> 00:02:28,90
It's going to get some output here.

74
00:02:28,90 --> 00:02:31,80
So what just happened was my program,

75
00:02:31,80 --> 00:02:33,40
went through started here,

76
00:02:33,40 --> 00:02:35,20
went through all of these directories

77
00:02:35,20 --> 00:02:38,90
found all of the images, pulled out there Exif metadata

78
00:02:38,90 --> 00:02:40,20
or whatever you want to call it

79
00:02:40,20 --> 00:02:42,90
and then found the description

80
00:02:42,90 --> 00:02:47,60
and organized these images by description alphabetically

81
00:02:47,60 --> 00:02:49,10
and put them right there.

82
00:02:49,10 --> 00:02:51,40
That was my solution and we'll take a look at the code

83
00:02:51,40 --> 00:02:52,80
get an overview of how it works

84
00:02:52,80 --> 00:02:54,30
but I just want to point out one thing,

85
00:02:54,30 --> 00:02:56,30
here you can see B comes before C,

86
00:02:56,30 --> 00:02:57,10
that makes sense,

87
00:02:57,10 --> 00:02:58,40
and B comes before M,

88
00:02:58,40 --> 00:02:59,60
that makes sense,

89
00:02:59,60 --> 00:03:00,40
but what is this?

90
00:03:00,40 --> 00:03:02,00
M comes before A?

91
00:03:02,00 --> 00:03:03,80
Hey that doesn't look quite right.

92
00:03:03,80 --> 00:03:06,20
And so I've already mentioned that GO uses

93
00:03:06,20 --> 00:03:09,60
UTF 8 and UTF 8 is a coding scheme.

94
00:03:09,60 --> 00:03:12,10
Coding schemes allow us to translate

95
00:03:12,10 --> 00:03:15,30
from circuits and switches from on and off switches

96
00:03:15,30 --> 00:03:18,60
to some sort of other information

97
00:03:18,60 --> 00:03:21,50
and ASCII used to be this coding scheme

98
00:03:21,50 --> 00:03:22,60
that was for text.

99
00:03:22,60 --> 00:03:25,50
Now UTF 8 is the most popular coding scheme.

100
00:03:25,50 --> 00:03:27,10
So I know I just gave you a lot of information

101
00:03:27,10 --> 00:03:28,90
but let me show you what that all means.

102
00:03:28,90 --> 00:03:31,20
Here is Wikipedia, and we're looking

103
00:03:31,20 --> 00:03:33,40
at the coding scheme for ASCII

104
00:03:33,40 --> 00:03:37,10
and ASCII was popular before UTF 8 came along.

105
00:03:37,10 --> 00:03:40,40
So they took ASCII, and they built it into UTF 8.

106
00:03:40,40 --> 00:03:43,20
So the first part of UTF 8 is ASCII.

107
00:03:43,20 --> 00:03:44,70
So when we have in our computer,

108
00:03:44,70 --> 00:03:47,70
circuits or switches, whatever you want to call them

109
00:03:47,70 --> 00:03:51,00
in this state of on and off,

110
00:03:51,00 --> 00:03:52,70
then this arrangement of binary

111
00:03:52,70 --> 00:03:55,70
translates to the letter A or capital letter A.

112
00:03:55,70 --> 00:03:58,40
Let me clarify, capital A comes before capitial B,

113
00:03:58,40 --> 00:04:01,00
and capital M and then after all those,

114
00:04:01,00 --> 00:04:02,10
we have the lower case.

115
00:04:02,10 --> 00:04:05,40
So in fact, M does come before A.

116
00:04:05,40 --> 00:04:08,00
So high level solution overview of the code.

117
00:04:08,00 --> 00:04:09,90
Let me open up the code here.

118
00:04:09,90 --> 00:04:12,20
We're going to get all of our images to do that

119
00:04:12,20 --> 00:04:14,10
we're going to do filepath.Walk

120
00:04:14,10 --> 00:04:16,00
and walk over all of our directories

121
00:04:16,00 --> 00:04:18,10
so I'm going up a couple levels to get to

122
00:04:18,10 --> 00:04:19,60
wherever I can walk over them,

123
00:04:19,60 --> 00:04:22,30
and then we're going to get the filepath extension,

124
00:04:22,30 --> 00:04:24,10
and we're going to switch on that extension

125
00:04:24,10 --> 00:04:26,10
and if it's JPG or JPEG

126
00:04:26,10 --> 00:04:29,90
because with Exif, it works for JPEG,

127
00:04:29,90 --> 00:04:35,80
JPEG, but is not supported for JPEG 2000, PNG, or GIF.

128
00:04:35,80 --> 00:04:38,50
So I'm only interested in JPEG files

129
00:04:38,50 --> 00:04:40,70
and this is a good thing to read if you're

130
00:04:40,70 --> 00:04:41,70
not familiar with Exifs,

131
00:04:41,70 --> 00:04:43,00
so just pause the video for a second

132
00:04:43,00 --> 00:04:44,70
and take a look at that.

133
00:04:44,70 --> 00:04:46,90
We find, okay, just give me my JPEGs

134
00:04:46,90 --> 00:04:48,20
and then I'm going to open that,

135
00:04:48,20 --> 00:04:50,50
get my file, and then once I have my file

136
00:04:50,50 --> 00:04:52,90
I'm going to pass it into my Exif function

137
00:04:52,90 --> 00:04:54,40
and notice just kind of naming

138
00:04:54,40 --> 00:04:56,50
that's like an art in computer science.

139
00:04:56,50 --> 00:04:57,70
There's two hard things,

140
00:04:57,70 --> 00:04:59,00
there's this famous quote,

141
00:04:59,00 --> 00:05:00,70
"Two hard things in computer science:

142
00:05:00,70 --> 00:05:03,20
"clearing cache, and naming things."

143
00:05:03,20 --> 00:05:05,30
I named this xi(f), pretty creative right?

144
00:05:05,30 --> 00:05:06,30
(laughs)

145
00:05:06,30 --> 00:05:08,40
I passed my file into that

146
00:05:08,40 --> 00:05:11,40
and then I decode my file with this third party package

147
00:05:11,40 --> 00:05:12,90
to get the Exif information

148
00:05:12,90 --> 00:05:15,90
and once I do that I can pull out all the Exif information

149
00:05:15,90 --> 00:05:18,00
and then I can manipulate the string

150
00:05:18,00 --> 00:05:19,40
for the image description,

151
00:05:19,40 --> 00:05:21,30
and once I've manipulated the string

152
00:05:21,30 --> 00:05:22,50
for the image description,

153
00:05:22,50 --> 00:05:26,40
I can then create files for all the images

154
00:05:26,40 --> 00:05:27,80
and just put them in one location

155
00:05:27,80 --> 00:05:30,20
and I could arrange them alphabetically

156
00:05:30,20 --> 00:05:31,30
by using sort.

157
00:05:31,30 --> 00:05:33,20
So that's my high level overview of the solution

158
00:05:33,20 --> 00:05:35,60
and in the next couple of videos,

159
00:05:35,60 --> 00:05:38,00
we're going to take a look at how recursion actually works.

160
00:05:38,00 --> 00:05:40,10
What are the more intimate details

161
00:05:40,10 --> 00:05:41,70
of how I implemented that solution

162
00:05:41,70 --> 00:05:43,90
and then how can you use walk-in directories

163
00:05:43,90 --> 00:05:46,70
and recursion to do some awesome stuff

164
00:05:46,70 --> 00:05:48,00
to save you time.

