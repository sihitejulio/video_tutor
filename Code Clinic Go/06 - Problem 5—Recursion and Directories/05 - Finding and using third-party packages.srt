1
00:00:00,50 --> 00:00:02,10
- So the next thing we're going to do is

2
00:00:02,10 --> 00:00:05,00
we are going to go get our exif information.

3
00:00:05,00 --> 00:00:09,60
The place I begin searching for can go to exif information

4
00:00:09,60 --> 00:00:11,60
is just to go to godoc.org.

5
00:00:11,60 --> 00:00:14,80
And I like godoc.org because it has packages

6
00:00:14,80 --> 00:00:16,60
which are both in Standard Library

7
00:00:16,60 --> 00:00:18,60
and also third party packages.

8
00:00:18,60 --> 00:00:20,70
So I came here and searched for exif.

9
00:00:20,70 --> 00:00:22,00
Then I do a command F.

10
00:00:22,00 --> 00:00:25,40
It launches this Find right there and I search for exif.

11
00:00:25,40 --> 00:00:28,80
So in Chrome, it will highlight every instance of exif

12
00:00:28,80 --> 00:00:32,50
on the page and I can see where is exif being used.

13
00:00:32,50 --> 00:00:33,90
So I think there's some way

14
00:00:33,90 --> 00:00:37,30
that godoc.org ranks these packages.

15
00:00:37,30 --> 00:00:39,60
I'm sure it probably has something to do with popularity.

16
00:00:39,60 --> 00:00:41,30
I have a link into this specific exif

17
00:00:41,30 --> 00:00:43,10
but that's just my intuition

18
00:00:43,10 --> 00:00:44,50
and that's what would make sense

19
00:00:44,50 --> 00:00:45,80
if I was building this site.

20
00:00:45,80 --> 00:00:48,20
So I'm just going to go with the first exif package

21
00:00:48,20 --> 00:00:49,50
which is listed here.

22
00:00:49,50 --> 00:00:52,00
So to get that into my code, I'm going to highlight that

23
00:00:52,00 --> 00:00:54,30
right there and then go into my Terminal

24
00:00:54,30 --> 00:00:56,00
and type in go get

25
00:00:56,00 --> 00:00:58,70
and then paste that in and it's going to go get that code.

26
00:00:58,70 --> 00:01:00,80
If I want to update that code,

27
00:01:00,80 --> 00:01:03,50
I'll do go get -u

28
00:01:03,50 --> 00:01:04,80
and then it would update that code.

29
00:01:04,80 --> 00:01:06,20
I'm not going to do that right now

30
00:01:06,20 --> 00:01:07,70
because my code is running.

31
00:01:07,70 --> 00:01:09,10
I don't know if he's changed it.

32
00:01:09,10 --> 00:01:10,60
If he has, it break the code

33
00:01:10,60 --> 00:01:11,90
and I'm going to have to troubleshoot that

34
00:01:11,90 --> 00:01:13,10
and I don't want to do that.

35
00:01:13,10 --> 00:01:15,40
To learn about the package, I just click on this link

36
00:01:15,40 --> 00:01:18,10
and it brings me to the documentation for the package.

37
00:01:18,10 --> 00:01:20,60
You can also learn about a package

38
00:01:20,60 --> 00:01:23,10
and sometimes find additional information

39
00:01:23,10 --> 00:01:25,80
just by going to the package on GitHub.

40
00:01:25,80 --> 00:01:28,20
So that's another way that you could get information.

41
00:01:28,20 --> 00:01:29,50
I also want to point out.

42
00:01:29,50 --> 00:01:31,60
I didn't see anything from the Standard Library.

43
00:01:31,60 --> 00:01:33,00
This is all GitHub stuff.

44
00:01:33,00 --> 00:01:34,70
That's the name namespacing, right?

45
00:01:34,70 --> 00:01:36,80
If I had searched for os,

46
00:01:36,80 --> 00:01:38,30
these are from the Standard Library.

47
00:01:38,30 --> 00:01:39,90
These are third packages.

48
00:01:39,90 --> 00:01:42,00
So I want to do this one.

49
00:01:42,00 --> 00:01:43,80
And one of the first things I looked for

50
00:01:43,80 --> 00:01:48,10
when I'm at some package is our, their examples.

51
00:01:48,10 --> 00:01:49,30
I want examples.

52
00:01:49,30 --> 00:01:51,40
So again, command F, search for example.

53
00:01:51,40 --> 00:01:53,50
And yeah, we have Examples right up here.

54
00:01:53,50 --> 00:01:56,10
Here's the Example, and fantastic.

55
00:01:56,10 --> 00:01:59,10
I'm able to get LatLong and Date and Time

56
00:01:59,10 --> 00:02:01,90
and there's also some focal stuff.

57
00:02:01,90 --> 00:02:03,40
This looks totally promising

58
00:02:03,40 --> 00:02:05,70
but I don't see anything in there for ImageDescription.

59
00:02:05,70 --> 00:02:06,80
So maybe there will be something else

60
00:02:06,80 --> 00:02:08,10
so it will help me get it.

61
00:02:08,10 --> 00:02:11,80
So this is the first attempt that I made at using exif.

62
00:02:11,80 --> 00:02:13,60
I'm going to open a file.

63
00:02:13,60 --> 00:02:15,00
So I just pass in the file.

64
00:02:15,00 --> 00:02:17,50
And I didn't worry about like, "Hey, recursing,

65
00:02:17,50 --> 00:02:18,90
"let's open all of them at once.

66
00:02:18,90 --> 00:02:20,20
"Let's get all of them."

67
00:02:20,20 --> 00:02:22,50
I was just like, "You know, let's start at one file

68
00:02:22,50 --> 00:02:24,10
"and see if we could get it to work."

69
00:02:24,10 --> 00:02:26,60
And if you start to play around with this stuff,

70
00:02:26,60 --> 00:02:30,00
you should know that not all images have this information

71
00:02:30,00 --> 00:02:32,40
so you just maybe want to try couple of different images.

72
00:02:32,40 --> 00:02:34,20
So this is an image I took with my cellphone

73
00:02:34,20 --> 00:02:36,70
just a few days ago and that's the one we're going to do

74
00:02:36,70 --> 00:02:38,10
so I'm going to open that up.

75
00:02:38,10 --> 00:02:39,70
I'm throwing away my air.

76
00:02:39,70 --> 00:02:42,00
I'm also throwing away my air right here.

77
00:02:42,00 --> 00:02:44,30
This is not production code.

78
00:02:44,30 --> 00:02:46,40
This is keeping it lean so we could focus

79
00:02:46,40 --> 00:02:48,70
on just what is being used right here.

80
00:02:48,70 --> 00:02:50,90
So then we have exif.Decode.

81
00:02:50,90 --> 00:02:51,70
I'm going to decode that.

82
00:02:51,70 --> 00:02:54,00
That's straight from the example and I get an x.

83
00:02:54,00 --> 00:02:55,70
And if x is not nil

84
00:02:55,70 --> 00:02:59,40
because if I hit a file or something that made some error,

85
00:02:59,40 --> 00:03:00,90
maybe it come back as nil.

86
00:03:00,90 --> 00:03:03,70
I look into little bit how's Decode working right there

87
00:03:03,70 --> 00:03:06,40
and it gives me back this which could potentially be nil.

88
00:03:06,40 --> 00:03:08,40
So if x is not nil.

89
00:03:08,40 --> 00:03:10,30
Then the x.DateTime,

90
00:03:10,30 --> 00:03:12,60
the time it was taken that stands for time,

91
00:03:12,60 --> 00:03:16,30
so we have x.LatLong and I get the LatLong.

92
00:03:16,30 --> 00:03:17,80
And that returns three values.

93
00:03:17,80 --> 00:03:19,20
That's pretty interesting, right?

94
00:03:19,20 --> 00:03:22,80
Returns one, two, three different things.

95
00:03:22,80 --> 00:03:24,00
And then I print that out.

96
00:03:24,00 --> 00:03:26,40
So this is file 06, folder 06

97
00:03:26,40 --> 00:03:27,30
and I'm already there.

98
00:03:27,30 --> 00:03:30,20
Let's just go run main and let's see what happens.

99
00:03:30,20 --> 00:03:32,50
So it gave me the time this was taken.

100
00:03:32,50 --> 00:03:33,90
That information wasn't stored.

101
00:03:33,90 --> 00:03:36,20
I did not take this at midnight

102
00:03:36,20 --> 00:03:39,30
on 2001 (laughs) January 1st.

103
00:03:39,30 --> 00:03:41,60
So that was incorrect but the lat and the long.

104
00:03:41,60 --> 00:03:42,90
That's pretty interesting.

105
00:03:42,90 --> 00:03:44,80
I'm just going to try a little experiment here.

106
00:03:44,80 --> 00:03:46,10
I'm going to highlight that

107
00:03:46,10 --> 00:03:49,60
and see what happens if I bring this over to Maps.

108
00:03:49,60 --> 00:03:52,30
And drop this in tag Google Maps.

109
00:03:52,30 --> 00:03:55,80
Just how awesome is Google Maps?

110
00:03:55,80 --> 00:03:57,00
It is that awesome.

111
00:03:57,00 --> 00:03:59,80
That's exactly where I'm at in the world.

112
00:03:59,80 --> 00:04:01,70
So we get the LatLong.

113
00:04:01,70 --> 00:04:02,70
I'm pretty excited.

114
00:04:02,70 --> 00:04:03,90
This is looking good.

115
00:04:03,90 --> 00:04:06,50
My next step in the process was right here.

116
00:04:06,50 --> 00:04:08,00
And so now I'm going to hook it up

117
00:04:08,00 --> 00:04:11,20
where it starts looking for all of my different files.

118
00:04:11,20 --> 00:04:13,20
I'm bringing in the stuff that we did

119
00:04:13,20 --> 00:04:14,90
and these folders right here

120
00:04:14,90 --> 00:04:17,30
where I start finding the extension.

121
00:04:17,30 --> 00:04:19,50
And then I make sure if it is a JPEG,

122
00:04:19,50 --> 00:04:22,00
I'm going to open it and I'll defer that

123
00:04:22,00 --> 00:04:22,80
because it's there.

124
00:04:22,80 --> 00:04:25,50
And then I created this function exif, right?

125
00:04:25,50 --> 00:04:27,50
Perfect, we are to laugh about that one.

126
00:04:27,50 --> 00:04:30,20
And exif.Decode, same thing.

127
00:04:30,20 --> 00:04:31,70
So for each image, I'm going to decode it

128
00:04:31,70 --> 00:04:33,70
and just print out the time in the LatLong.

129
00:04:33,70 --> 00:04:36,20
So all I did was basically the exact same code

130
00:04:36,20 --> 00:04:37,60
that was right here

131
00:04:37,60 --> 00:04:39,90
but now I'm going to do it for every one of my images

132
00:04:39,90 --> 00:04:43,10
that I could find in all of this folder

133
00:04:43,10 --> 00:04:44,70
because I'm going up all the way

134
00:04:44,70 --> 00:04:46,80
to this position right there

135
00:04:46,80 --> 00:04:48,40
so let's see what happens.

136
00:04:48,40 --> 00:04:49,50
Cross your fingers.

137
00:04:49,50 --> 00:04:50,90
I'm on a drama.

138
00:04:50,90 --> 00:04:52,10
All right, here we go.

139
00:04:52,10 --> 00:04:54,50
Whoo, there we go, beautiful.

140
00:04:54,50 --> 00:04:56,80
So we printed out some exif information.

141
00:04:56,80 --> 00:04:58,10
You could see a lot of these stuff,

142
00:04:58,10 --> 00:04:59,20
a lot of these images.

143
00:04:59,20 --> 00:05:00,70
I just didn't get anything

144
00:05:00,70 --> 00:05:03,10
and I am not sure if that's because

145
00:05:03,10 --> 00:05:05,20
that exif package is broken.

146
00:05:05,20 --> 00:05:08,80
I did not write this package nor do I want to

147
00:05:08,80 --> 00:05:11,10
but I would if I was going to put this in the production,

148
00:05:11,10 --> 00:05:12,70
I would test it.

149
00:05:12,70 --> 00:05:14,10
I would make sure it's working right.

150
00:05:14,10 --> 00:05:15,10
And if it's going to be production,

151
00:05:15,10 --> 00:05:16,80
I would look through this code

152
00:05:16,80 --> 00:05:19,40
and try to understand it's the best to my abilities

153
00:05:19,40 --> 00:05:22,40
so that I could see is this providing a backdoor

154
00:05:22,40 --> 00:05:24,50
for somebody into my application

155
00:05:24,50 --> 00:05:26,00
or is this putting some sort of

156
00:05:26,00 --> 00:05:28,00
malicious code on my machine?

157
00:05:28,00 --> 00:05:29,20
Is it all running well?

158
00:05:29,20 --> 00:05:30,70
So there's a little bit of paranoia,

159
00:05:30,70 --> 00:05:32,20
a sea of paranoia and suspicion

160
00:05:32,20 --> 00:05:34,00
that maybe I just planted in your mind

161
00:05:34,00 --> 00:05:35,30
but it's something to think about.

162
00:05:35,30 --> 00:05:36,70
You are taking code from somebody else

163
00:05:36,70 --> 00:05:38,10
and you are running it.

164
00:05:38,10 --> 00:05:40,30
So if this is a serious situation,

165
00:05:40,30 --> 00:05:43,10
you want to make sure you understand all of your code.

166
00:05:43,10 --> 00:05:45,90
It's kind of a tricky job to be a programmer

167
00:05:45,90 --> 00:05:47,00
because I mentioned this before

168
00:05:47,00 --> 00:05:48,50
but to program is to work

169
00:05:48,50 --> 00:05:51,30
with what you know and what you don't know.

170
00:05:51,30 --> 00:05:52,40
And you have to get comfortable

171
00:05:52,40 --> 00:05:55,70
because at a certain point, you can't know everything

172
00:05:55,70 --> 00:05:59,10
and you just have to rest on the assumption that this works.

173
00:05:59,10 --> 00:06:01,20
One of the ways we could bring assurance to our self

174
00:06:01,20 --> 00:06:03,40
that something works is to test it.

175
00:06:03,40 --> 00:06:05,70
So enough flaws finding, enough for my life.

176
00:06:05,70 --> 00:06:08,60
Let's look at the next step, step eight.

177
00:06:08,60 --> 00:06:11,50
And in step eight, what is it that I did differently?

178
00:06:11,50 --> 00:06:13,20
Right here, it sounds like, well, cool.

179
00:06:13,20 --> 00:06:15,90
I got LatLong stuff, at least for one image.

180
00:06:15,90 --> 00:06:17,70
And I need to get more.

181
00:06:17,70 --> 00:06:19,30
I need to get all that other stuff.

182
00:06:19,30 --> 00:06:22,80
And so what I did was if I get my exif.Decode

183
00:06:22,80 --> 00:06:25,40
from that third party package and I get my x,

184
00:06:25,40 --> 00:06:26,80
what do I have available?

185
00:06:26,80 --> 00:06:28,30
And literally to figure this out,

186
00:06:28,30 --> 00:06:32,30
I went x. and I just let my code completion kind of tell me,

187
00:06:32,30 --> 00:06:34,70
"Hey, this is what x has attached to it."

188
00:06:34,70 --> 00:06:36,00
And when I saw a String,

189
00:06:36,00 --> 00:06:37,20
I thought, "That looks nice."

190
00:06:37,20 --> 00:06:39,50
I'm glad there's a String function right there.

191
00:06:39,50 --> 00:06:42,50
So String just generally means take off the data

192
00:06:42,50 --> 00:06:46,50
associated with this data structure, put it into a String.

193
00:06:46,50 --> 00:06:48,10
And so I'm going to run that

194
00:06:48,10 --> 00:06:50,70
and hopefully get some String back right here

195
00:06:50,70 --> 00:06:51,70
and then print it.

196
00:06:51,70 --> 00:06:53,10
Once again, we get the drama.

197
00:06:53,10 --> 00:06:54,20
Is it going to work?

198
00:06:54,20 --> 00:06:56,00
I think you probably know at this point.

199
00:06:56,00 --> 00:06:57,80
I've gone through and I've tested the code

200
00:06:57,80 --> 00:06:59,40
because I would not want to put up

201
00:06:59,40 --> 00:07:00,60
something that didn't work

202
00:07:00,60 --> 00:07:02,70
or if I do put up things that don't work, I tell you.

203
00:07:02,70 --> 00:07:04,40
I say, "This isn't going to work

204
00:07:04,40 --> 00:07:06,40
"but this is what I want you to learn about it."

205
00:07:06,40 --> 00:07:08,00
Bam, we run it.

206
00:07:08,00 --> 00:07:12,40
Sweet glory, now I got a whole bunch of crazy metadata.

207
00:07:12,40 --> 00:07:13,50
And when I look through this,

208
00:07:13,50 --> 00:07:16,00
it's different for different files.

209
00:07:16,00 --> 00:07:17,40
So there's ImageDescription.

210
00:07:17,40 --> 00:07:19,70
I'm going to copy that and then command F

211
00:07:19,70 --> 00:07:22,90
and then V and I don't know what that does to me.

212
00:07:22,90 --> 00:07:25,00
So it brings me through but that's not too helpful.

213
00:07:25,00 --> 00:07:27,10
I think it might highlight like on a browser.

214
00:07:27,10 --> 00:07:29,60
Anyhow, what I want you to know is

215
00:07:29,60 --> 00:07:32,50
that there is no ImageDescription right here

216
00:07:32,50 --> 00:07:34,20
for that one picture I took

217
00:07:34,20 --> 00:07:37,60
because I never added it and it's not added by default.

218
00:07:37,60 --> 00:07:40,40
So not all pictures have ImageDescription

219
00:07:40,40 --> 00:07:42,50
and there's some images that don't have anything

220
00:07:42,50 --> 00:07:43,80
in my Directory Structure.

221
00:07:43,80 --> 00:07:45,30
So the next thing I need to do is

222
00:07:45,30 --> 00:07:47,50
I need to sort of take that String.

223
00:07:47,50 --> 00:07:49,20
I need to look through that String.

224
00:07:49,20 --> 00:07:51,70
Find images that have an ImageDescription.

225
00:07:51,70 --> 00:07:54,50
I need to grab just this piece of String.

226
00:07:54,50 --> 00:07:57,10
I then need to have only the images

227
00:07:57,10 --> 00:07:58,90
that have an ImageDescription

228
00:07:58,90 --> 00:08:00,70
and I need to sort them by this.

229
00:08:00,70 --> 00:08:02,70
Put them into some sort of an order.

230
00:08:02,70 --> 00:08:04,90
So that was the task that laid before me

231
00:08:04,90 --> 00:08:09,20
and I was feeling the love of a unsolved question.

232
00:08:09,20 --> 00:08:10,60
Will I be able to do it?

233
00:08:10,60 --> 00:08:11,80
I finally did get to figure out.

234
00:08:11,80 --> 00:08:14,50
In the next video, we're going to see the next step

235
00:08:14,50 --> 00:08:18,00
and how we take the String and find it

236
00:08:18,00 --> 00:08:20,40
and call it out and get the images all sorted.

237
00:08:20,40 --> 00:08:22,00
All right, let's do it.

