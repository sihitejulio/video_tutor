1
00:00:00,50 --> 00:00:02,20
- Here is the next step

2
00:00:02,20 --> 00:00:05,10
in our solution for making this all happen.

3
00:00:05,10 --> 00:00:07,20
I want to look at these four folders

4
00:00:07,20 --> 00:00:09,30
with the files inside of each of them,

5
00:00:09,30 --> 00:00:11,10
and just point a couple of things out.

6
00:00:11,10 --> 00:00:13,10
But before we do that,

7
00:00:13,10 --> 00:00:15,70
I want to say one more thing about recursion.

8
00:00:15,70 --> 00:00:17,70
So I mentioned that Ivo Balbaert's book

9
00:00:17,70 --> 00:00:19,10
is a great book to read.

10
00:00:19,10 --> 00:00:20,80
Start with it on chapter four.

11
00:00:20,80 --> 00:00:23,80
And here is Ivo's website for that book,

12
00:00:23,80 --> 00:00:26,50
and Ivo has some cool code right here.

13
00:00:26,50 --> 00:00:28,20
So you could take a look at Ivo's code,

14
00:00:28,20 --> 00:00:30,20
and he has some great examples

15
00:00:30,20 --> 00:00:32,10
for doing recursion.

16
00:00:32,10 --> 00:00:34,20
He does the Fibonacci problem

17
00:00:34,20 --> 00:00:36,80
in about ten bazillion different ways,

18
00:00:36,80 --> 00:00:38,70
actually it's probably about 12 different ways,

19
00:00:38,70 --> 00:00:40,70
but he does it with concurrency and everything.

20
00:00:40,70 --> 00:00:41,70
So you could go through and take a look

21
00:00:41,70 --> 00:00:44,60
at all the different ways that Ivo uses recursion

22
00:00:44,60 --> 00:00:47,10
and it's a good tool for you to learn more

23
00:00:47,10 --> 00:00:48,90
about recursion.

24
00:00:48,90 --> 00:00:50,30
So let's take a look at a couple of these things,

25
00:00:50,30 --> 00:00:51,40
here's our first file,

26
00:00:51,40 --> 00:00:52,90
now let's just run it and see what happens,

27
00:00:52,90 --> 00:00:54,80
and then we'll kind of take a look at the functionality

28
00:00:54,80 --> 00:00:55,80
to see what it's doing.

29
00:00:55,80 --> 00:00:57,10
And I'm going to go up a level,

30
00:00:57,10 --> 00:00:59,10
and see where I'm at right there,

31
00:00:59,10 --> 00:01:00,70
and then go up one more,

32
00:01:00,70 --> 00:01:02,50
and see where I'm at right there.

33
00:01:02,50 --> 00:01:05,10
And then I'm going to go into 02,

34
00:01:05,10 --> 00:01:08,20
and go run main dot go.

35
00:01:08,20 --> 00:01:08,80
Whoa.

36
00:01:08,80 --> 00:01:10,40
Printed out a whole bunch of stuff.

37
00:01:10,40 --> 00:01:12,80
So basically what I said was from here,

38
00:01:12,80 --> 00:01:15,30
go up to there, so that's one level up.

39
00:01:15,30 --> 00:01:16,40
Right there.

40
00:01:16,40 --> 00:01:18,50
And then from here, go up to there

41
00:01:18,50 --> 00:01:20,10
and that's the next level up.

42
00:01:20,10 --> 00:01:21,40
And then once you're here,

43
00:01:21,40 --> 00:01:23,00
go through all of these folders

44
00:01:23,00 --> 00:01:24,00
and print them out.

45
00:01:24,00 --> 00:01:25,60
So we've already seen this in action.

46
00:01:25,60 --> 00:01:27,10
And here you could everything got printed,

47
00:01:27,10 --> 00:01:30,50
06, 05, 06, all the different things

48
00:01:30,50 --> 00:01:33,20
and there's 06, and that goes on further up.

49
00:01:33,20 --> 00:01:35,90
So we're using our filepath Walk function here.

50
00:01:35,90 --> 00:01:38,80
And we pass in the Walk func,

51
00:01:38,80 --> 00:01:41,20
so this is a callback, it is a function.

52
00:01:41,20 --> 00:01:44,00
The fuction is taking another function

53
00:01:44,00 --> 00:01:45,30
as an argument.

54
00:01:45,30 --> 00:01:48,20
And then filepath Walk is going to use that.

55
00:01:48,20 --> 00:01:49,60
I do want to take one second here

56
00:01:49,60 --> 00:01:53,70
and just look at the code inside the standard library.

57
00:01:53,70 --> 00:01:55,40
So we have func Walk,

58
00:01:55,40 --> 00:01:56,90
and then Walk does what it does,

59
00:01:56,90 --> 00:01:59,60
and it returns lowercase walk.

60
00:01:59,60 --> 00:02:01,50
So this is something I haven't pointed out yet,

61
00:02:01,50 --> 00:02:03,60
which I do think you need to know about

62
00:02:03,60 --> 00:02:05,40
if you're getting started with Go.

63
00:02:05,40 --> 00:02:07,70
The case determines whether or not

64
00:02:07,70 --> 00:02:10,40
something is visible or not visible.

65
00:02:10,40 --> 00:02:11,30
Another way we say that

66
00:02:11,30 --> 00:02:14,80
is whether or not something is exported or not exported

67
00:02:14,80 --> 00:02:16,00
outside of package.

68
00:02:16,00 --> 00:02:19,30
Go doesn't really use, people who program in Go,

69
00:02:19,30 --> 00:02:21,70
and the language specification, and Effective Go

70
00:02:21,70 --> 00:02:24,40
and golang.org, we don't really use the words

71
00:02:24,40 --> 00:02:25,70
private and public.

72
00:02:25,70 --> 00:02:28,00
Because those words carry a lot of baggage and weight

73
00:02:28,00 --> 00:02:29,80
from other programming languages.

74
00:02:29,80 --> 00:02:32,60
So visible, not visible outside the package,

75
00:02:32,60 --> 00:02:34,80
exported, not exported outside the package,

76
00:02:34,80 --> 00:02:37,10
those are some of the words that we use.

77
00:02:37,10 --> 00:02:38,50
So, this is what we're doing.

78
00:02:38,50 --> 00:02:40,10
We're doing our filepath Walk,

79
00:02:40,10 --> 00:02:42,60
and we're saying Go walk starting at this root,

80
00:02:42,60 --> 00:02:45,20
and then we pass in our callback,

81
00:02:45,20 --> 00:02:46,30
and remember what this does,

82
00:02:46,30 --> 00:02:48,30
when this function executes,

83
00:02:48,30 --> 00:02:50,30
when filepath Walk runs,

84
00:02:50,30 --> 00:02:52,10
it's going to execute this function

85
00:02:52,10 --> 00:02:54,70
for every place it walks to.

86
00:02:54,70 --> 00:02:56,80
So we could put whatever code we want in here,

87
00:02:56,80 --> 00:02:59,30
and what I put in is I said I want to print the path.

88
00:02:59,30 --> 00:03:01,70
And you could also see as I pointed out before,

89
00:03:01,70 --> 00:03:04,20
if you go look at the Walk function down here,

90
00:03:04,20 --> 00:03:05,70
Walk, and this is cool,

91
00:03:05,70 --> 00:03:08,20
about WebStorm holding down that command

92
00:03:08,20 --> 00:03:09,60
to kind of selection everything,

93
00:03:09,60 --> 00:03:14,10
Walk will call Walk, so we have recursion going on there.

94
00:03:14,10 --> 00:03:15,20
The next file I want to look at

95
00:03:15,20 --> 00:03:18,30
is file 03, and so let's see what file 03 does,

96
00:03:18,30 --> 00:03:19,90
and see what the modification is.

97
00:03:19,90 --> 00:03:23,00
So I'm going to go up a level,

98
00:03:23,00 --> 00:03:26,00
and I've printed out file extensions, that's pretty cool.

99
00:03:26,00 --> 00:03:27,10
So that's going to be the first step

100
00:03:27,10 --> 00:03:30,90
for me finding all of my files of a certain type, right?

101
00:03:30,90 --> 00:03:32,20
What is the extension?

102
00:03:32,20 --> 00:03:34,80
And to get the extension, this is all I had to do.

103
00:03:34,80 --> 00:03:37,70
Filepath, extension, and then pass in the path.

104
00:03:37,70 --> 00:03:39,20
So I'm getting the path here

105
00:03:39,20 --> 00:03:40,90
from the function that gets passed in,

106
00:03:40,90 --> 00:03:44,10
the Walk func, the callback, and it gives me my extension

107
00:03:44,10 --> 00:03:46,20
for each file, and then I print that out.

108
00:03:46,20 --> 00:03:49,40
So I think it's worthwhile just to go look at filepath,

109
00:03:49,40 --> 00:03:50,70
what is this package?

110
00:03:50,70 --> 00:03:52,40
So we can see here in the import

111
00:03:52,40 --> 00:03:55,80
we're importing path, filepath from the standard library,

112
00:03:55,80 --> 00:03:58,60
and if we go look at GoDoc, we can see right here,

113
00:03:58,60 --> 00:04:01,00
here's the package filepath.

114
00:04:01,00 --> 00:04:03,30
And package filepath has ext

115
00:04:03,30 --> 00:04:05,00
which gives us an extension.

116
00:04:05,00 --> 00:04:07,30
But this package allows you to do

117
00:04:07,30 --> 00:04:09,40
a bunch of different things for working with files,

118
00:04:09,40 --> 00:04:10,80
so it's super good to know about,

119
00:04:10,80 --> 00:04:13,20
and definitely if you're going to be trying to grab files

120
00:04:13,20 --> 00:04:14,90
you want to go check that out.

121
00:04:14,90 --> 00:04:16,60
So the next step in my solve,

122
00:04:16,60 --> 00:04:18,80
let's see what it does.

123
00:04:18,80 --> 00:04:20,00
We are at my terminal,

124
00:04:20,00 --> 00:04:22,50
and then go up a level,

125
00:04:22,50 --> 00:04:24,80
and just make sure 04, that's the one I want to go to,

126
00:04:24,80 --> 00:04:28,30
that's the next one, go run main dot go,

127
00:04:28,30 --> 00:04:30,00
and I just got my jpgs.

128
00:04:30,00 --> 00:04:32,50
So that's excellent, how did I just get my jpgs?

129
00:04:32,50 --> 00:04:34,50
Well, I used a switch statement to do that.

130
00:04:34,50 --> 00:04:37,60
I said hey, I want to switch on this value,

131
00:04:37,60 --> 00:04:39,40
the ext, it's a string.

132
00:04:39,40 --> 00:04:42,30
And for each case if it's this

133
00:04:42,30 --> 00:04:44,40
or this, then run this code.

134
00:04:44,40 --> 00:04:47,00
Now switch is a little bit different in Go

135
00:04:47,00 --> 00:04:48,40
than in other languages.

136
00:04:48,40 --> 00:04:50,60
And a good place to kind of get a sense

137
00:04:50,60 --> 00:04:52,40
of how to use switches in Go

138
00:04:52,40 --> 00:04:56,80
is to come over to this website gobyexample.com.

139
00:04:56,80 --> 00:04:58,40
So this is a great place just to reference

140
00:04:58,40 --> 00:05:00,00
hey how do I do a slice,

141
00:05:00,00 --> 00:05:03,80
how do I do switch, if/else, for, constance, variables,

142
00:05:03,80 --> 00:05:06,30
closures, recursions, it's a resource.

143
00:05:06,30 --> 00:05:08,00
Consult it, don't rely on it

144
00:05:08,00 --> 00:05:10,20
as the absolute truth, that would be

145
00:05:10,20 --> 00:05:13,10
the language specification or Effective Go

146
00:05:13,10 --> 00:05:14,70
to really sort of make sure that

147
00:05:14,70 --> 00:05:16,70
okay, I'm doing this the way it should be done.

148
00:05:16,70 --> 00:05:18,70
But there's a lot of really great examples in here.

149
00:05:18,70 --> 00:05:20,60
So let's take a look at the switch example,

150
00:05:20,60 --> 00:05:22,40
and you could see here with these switches,

151
00:05:22,40 --> 00:05:24,10
here's one way you could do a switch.

152
00:05:24,10 --> 00:05:25,80
We're going to switch on the value "i"

153
00:05:25,80 --> 00:05:28,80
in case one here is if "i" is one,

154
00:05:28,80 --> 00:05:30,50
case two is if "i" is two,

155
00:05:30,50 --> 00:05:32,40
case three is if "i" is three.

156
00:05:32,40 --> 00:05:35,40
Now one thing to know about switch statements in Go,

157
00:05:35,40 --> 00:05:38,40
is there is no default fall through.

158
00:05:38,40 --> 00:05:40,60
So if you're programming in Java,

159
00:05:40,60 --> 00:05:42,30
you would need to do breaks.

160
00:05:42,30 --> 00:05:45,00
You would have break, break, break.

161
00:05:45,00 --> 00:05:47,20
Well, one of the things that they really wanted to do

162
00:05:47,20 --> 00:05:49,60
when designing Go from everything I've read,

163
00:05:49,60 --> 00:05:51,20
is they wanted to have the language be

164
00:05:51,20 --> 00:05:53,50
as clean and lean as possible.

165
00:05:53,50 --> 00:05:55,90
And so here we have a break, break, break,

166
00:05:55,90 --> 00:05:58,70
totally redundant, and nobody ever really uses fall through,

167
00:05:58,70 --> 00:06:01,00
that's more of like the exception as opposed to the rule,

168
00:06:01,00 --> 00:06:02,40
that you use fall through.

169
00:06:02,40 --> 00:06:07,00
So in Go, you do not need to put in a break statement.

170
00:06:07,00 --> 00:06:08,50
There is no default fall through.

171
00:06:08,50 --> 00:06:10,30
You can if you want, put fall through,

172
00:06:10,30 --> 00:06:12,60
and then you could specify for it to fall through,

173
00:06:12,60 --> 00:06:15,20
you could do that, but there's no default fall through,

174
00:06:15,20 --> 00:06:16,80
so you don't need your break statements.

175
00:06:16,80 --> 00:06:19,30
So this is one example of using switch.

176
00:06:19,30 --> 00:06:21,50
Another example of using switch, right here.

177
00:06:21,50 --> 00:06:23,90
And you could see switching on multiple things right here,

178
00:06:23,90 --> 00:06:26,20
so it's kind of like or this one or that one,

179
00:06:26,20 --> 00:06:28,50
and we're also doing another switch right here.

180
00:06:28,50 --> 00:06:30,60
Where we're just switching on nothing,

181
00:06:30,60 --> 00:06:33,40
and the case is going to be true or false, right?

182
00:06:33,40 --> 00:06:35,40
So it'll just evaluate some expression

183
00:06:35,40 --> 00:06:36,60
and come back with true or false

184
00:06:36,60 --> 00:06:38,00
and it'll run it if it's true.

185
00:06:38,00 --> 00:06:40,40
And you could also do of course a default case.

186
00:06:40,40 --> 00:06:41,90
Okay, so that's the switch statement,

187
00:06:41,90 --> 00:06:43,30
and a really good thing to know about,

188
00:06:43,30 --> 00:06:45,10
and you could see in the code that I wrote,

189
00:06:45,10 --> 00:06:47,70
I was doing jpg or jpeg, and I could add

190
00:06:47,70 --> 00:06:49,50
different files in here if I wanted,

191
00:06:49,50 --> 00:06:51,60
or just give me md's also.

192
00:06:51,60 --> 00:06:53,80
And so if I did that and then run this code again,

193
00:06:53,80 --> 00:06:55,90
I'll also have my md's in here.

194
00:06:55,90 --> 00:06:58,20
So I got a bunch of md's in there.

195
00:06:58,20 --> 00:06:59,40
But I'm going to take that out,

196
00:06:59,40 --> 00:07:02,30
cause I only want jpgs, cause jpegs have exif.

197
00:07:02,30 --> 00:07:03,80
Alright, last step in the process

198
00:07:03,80 --> 00:07:05,20
that I want to show you in this video,

199
00:07:05,20 --> 00:07:06,60
let's run it and see what happens,

200
00:07:06,60 --> 00:07:09,70
it's going to be file 05.

201
00:07:09,70 --> 00:07:12,50
Go run main dot go.

202
00:07:12,50 --> 00:07:15,10
So now I'm printing out some information from each file.

203
00:07:15,10 --> 00:07:16,90
So this is a lot like what we did

204
00:07:16,90 --> 00:07:18,90
when we were doing image comparison.

205
00:07:18,90 --> 00:07:21,00
I'm opening up my file right here.

206
00:07:21,00 --> 00:07:23,60
So okay, hey it's a jpg file, let's open it up,

207
00:07:23,60 --> 00:07:27,00
I give it the path, I get my file, I defer the close.

208
00:07:27,00 --> 00:07:29,40
Just decode that file to an image

209
00:07:29,40 --> 00:07:31,20
and then I could get the rgba information

210
00:07:31,20 --> 00:07:32,40
and print that out.

211
00:07:32,40 --> 00:07:34,70
So a little bit of a summary of what we did before.

212
00:07:34,70 --> 00:07:36,70
In the next video we're going to take a look at

213
00:07:36,70 --> 00:07:40,00
how we grab the exif information and pull that out.

