1
00:00:00,50 --> 00:00:01,70
- So next thing we want to do

2
00:00:01,70 --> 00:00:04,20
is we want to get only our images

3
00:00:04,20 --> 00:00:06,40
that have that description.

4
00:00:06,40 --> 00:00:07,80
So when we ran that last file,

5
00:00:07,80 --> 00:00:10,50
we saw some of the images have this description?

6
00:00:10,50 --> 00:00:12,60
How can we pull this description out?

7
00:00:12,60 --> 00:00:15,60
Well this is one big long string.

8
00:00:15,60 --> 00:00:19,10
And in Go working in strings is very easy.

9
00:00:19,10 --> 00:00:21,30
Go has a lot of really great functionality built in

10
00:00:21,30 --> 00:00:23,40
to the standard library for working with strings,

11
00:00:23,40 --> 00:00:25,50
and that's what we're going to explore in this video,

12
00:00:25,50 --> 00:00:26,60
how do we do that.

13
00:00:26,60 --> 00:00:28,20
So the first thing I want to do is,

14
00:00:28,20 --> 00:00:30,70
all of this stuff up here is exactly the same.

15
00:00:30,70 --> 00:00:33,00
We're getting our file path extension,

16
00:00:33,00 --> 00:00:35,20
we're switching on it, we're opening it up,

17
00:00:35,20 --> 00:00:38,50
and then we're calling xi(f), and xi(f) down here,

18
00:00:38,50 --> 00:00:41,20
we decode all of our xi(f) information.

19
00:00:41,20 --> 00:00:45,00
Then if it's not nil, we get the string of that,

20
00:00:45,00 --> 00:00:48,10
and now we're checking does the string contain

21
00:00:48,10 --> 00:00:49,80
a certain phrase?

22
00:00:49,80 --> 00:00:53,00
The phrase we're looking for is this phrase right here,

23
00:00:53,00 --> 00:00:54,20
ImageDescription.

24
00:00:54,20 --> 00:00:58,10
You could see here strings.Contains, from package strings,

25
00:00:58,10 --> 00:00:59,60
we have Contains.

26
00:00:59,60 --> 00:01:04,50
Contains takes s and a substring, and returns a bool.

27
00:01:04,50 --> 00:01:08,10
So it tells you whether or not this string right here

28
00:01:08,10 --> 00:01:09,80
is in the larger string.

29
00:01:09,80 --> 00:01:11,50
And it tells true or false.

30
00:01:11,50 --> 00:01:13,50
So this will evaluate true or false,

31
00:01:13,50 --> 00:01:16,20
and if it's true, it'll print this information out.

32
00:01:16,20 --> 00:01:19,40
So a little drama here again, will this run?

33
00:01:19,40 --> 00:01:21,30
I think you probably know by now

34
00:01:21,30 --> 00:01:24,90
that I put code samples in that work.

35
00:01:24,90 --> 00:01:27,40
There we go, it printed all that out.

36
00:01:27,40 --> 00:01:29,30
So the next step in the process is we want

37
00:01:29,30 --> 00:01:31,30
to be able to extract,

38
00:01:31,30 --> 00:01:33,30
and I'm just going to go up so I can show it again

39
00:01:33,30 --> 00:01:34,50
and then run that.

40
00:01:34,50 --> 00:01:37,70
We want to be able to extract that ImageDescription,

41
00:01:37,70 --> 00:01:38,20
right there.

42
00:01:38,20 --> 00:01:39,20
So how would you do this?

43
00:01:39,20 --> 00:01:43,00
How would you go about taking from this entire string,

44
00:01:43,00 --> 00:01:46,80
right here, and just getting that stuff, right there.

45
00:01:46,80 --> 00:01:48,20
So let's see how I did it.

46
00:01:48,20 --> 00:01:52,20
I'm going to open up the next file, ten, right here.

47
00:01:52,20 --> 00:01:56,40
I have my string and I do string.Contains now,

48
00:01:56,40 --> 00:01:58,20
so what is string.Contains?

49
00:01:58,20 --> 00:02:00,20
It's the exact same thing.

50
00:02:00,20 --> 00:02:01,50
I have my string right here,

51
00:02:01,50 --> 00:02:03,30
and then I do the string.Contains.

52
00:02:03,30 --> 00:02:07,50
And then the phrase I'm looking for is all of this.

53
00:02:07,50 --> 00:02:10,30
Now in Go you could signify a string

54
00:02:10,30 --> 00:02:13,30
either with double quotes, like I do right here,

55
00:02:13,30 --> 00:02:15,00
or with back tics.

56
00:02:15,00 --> 00:02:18,70
And if you use back tics, it gives you a raw string literal.

57
00:02:18,70 --> 00:02:22,60
What that means is it's going to preserve your formatting,

58
00:02:22,60 --> 00:02:25,80
so I have returns here, it's going to preserve that.

59
00:02:25,80 --> 00:02:29,60
So I have a raw string literal from there to there,

60
00:02:29,60 --> 00:02:32,20
and I have all of this stuff in between.

61
00:02:32,20 --> 00:02:35,30
Comment, marks, returns and everything.

62
00:02:35,30 --> 00:02:38,00
A raw string literal will preserve that.

63
00:02:38,00 --> 00:02:40,50
And I'm using a raw string literal here

64
00:02:40,50 --> 00:02:43,60
because inside of it I want to use double quotes.

65
00:02:43,60 --> 00:02:46,30
So that's what I'm searching for, that entire phrase.

66
00:02:46,30 --> 00:02:49,00
I want to find that right here,

67
00:02:49,00 --> 00:02:50,30
and when I find that it's going to give me

68
00:02:50,30 --> 00:02:53,60
the index position in this entire string

69
00:02:53,60 --> 00:02:55,00
where it found that.

70
00:02:55,00 --> 00:02:57,60
So that will be this position right there.

71
00:02:57,60 --> 00:02:59,90
So when I find this position right here,

72
00:02:59,90 --> 00:03:03,50
to get to there, how would I get from here to there?

73
00:03:03,50 --> 00:03:05,10
And I kind of just showed you the answer.

74
00:03:05,10 --> 00:03:07,30
So once I find this index position,

75
00:03:07,30 --> 00:03:12,00
I need to add this length to it so I end up right here.

76
00:03:12,00 --> 00:03:13,10
And that's what I do.

77
00:03:13,10 --> 00:03:17,00
So I do strings.Index, and I pass in in this string,

78
00:03:17,00 --> 00:03:19,20
I want to find this phrase, right?

79
00:03:19,20 --> 00:03:22,90
And then that's going to give me this position right here,

80
00:03:22,90 --> 00:03:25,00
and then I'm adding to that position

81
00:03:25,00 --> 00:03:26,40
the length of the phrase.

82
00:03:26,40 --> 00:03:29,10
So I'm adding to this position the length of this phrase,

83
00:03:29,10 --> 00:03:31,00
so I should end up right there.

84
00:03:31,00 --> 00:03:33,40
And then that'll be my start position.

85
00:03:33,40 --> 00:03:35,60
So I'm going to print out, did I get the description,

86
00:03:35,60 --> 00:03:39,10
and in Go, you can slice strings.

87
00:03:39,10 --> 00:03:42,00
So there's slicing, and there's slices.

88
00:03:42,00 --> 00:03:47,40
A slice is a data structure, so here is a slice.

89
00:03:47,40 --> 00:03:49,70
So that's a data structure, it's like a list.

90
00:03:49,70 --> 00:03:54,90
Slicing is taking a slice or an array or a string

91
00:03:54,90 --> 00:03:57,00
and saying you know what, I only want part of it.

92
00:03:57,00 --> 00:04:00,00
You slice it by saying get me from this position

93
00:04:00,00 --> 00:04:03,90
in the string or the array or the slice, to this position.

94
00:04:03,90 --> 00:04:05,60
And you pass in some numbers there.

95
00:04:05,60 --> 00:04:07,20
And if you don't have a number,

96
00:04:07,20 --> 00:04:09,60
it goes all the way to the end.

97
00:04:09,60 --> 00:04:12,40
So I have my string, and I want from that string,

98
00:04:12,40 --> 00:04:16,10
from start to the end.

99
00:04:16,10 --> 00:04:18,60
So the end is that little piece right in between.

100
00:04:18,60 --> 00:04:20,70
So that's what I'm doing, let's give it a run,

101
00:04:20,70 --> 00:04:22,80
and see what happens, I'm going to clear this out.

102
00:04:22,80 --> 00:04:26,80
And that is file ten.

103
00:04:26,80 --> 00:04:28,40
And did I get my descrip:

104
00:04:28,40 --> 00:04:30,30
Cropped and flipped statue of liberty,

105
00:04:30,30 --> 00:04:33,40
so it gave me everything from the quote

106
00:04:33,40 --> 00:04:35,30
for it on that description.

107
00:04:35,30 --> 00:04:38,10
And it did that for all the descriptions it found,

108
00:04:38,10 --> 00:04:41,00
so that's pretty great, and that's the first step.

109
00:04:41,00 --> 00:04:43,60
The next step is I need to just get the end.

110
00:04:43,60 --> 00:04:46,20
And we do that here in file 11.

111
00:04:46,20 --> 00:04:48,70
So I have my start, right there I have my start.

112
00:04:48,70 --> 00:04:51,70
Where I'm slicing, but I also need to get my end.

113
00:04:51,70 --> 00:04:55,10
So how do I get from this point all the way up

114
00:04:55,10 --> 00:04:56,20
to that point?

115
00:04:56,20 --> 00:04:58,00
So the solution for that is I need

116
00:04:58,00 --> 00:05:02,10
to find this double quote right there.

117
00:05:02,10 --> 00:05:04,50
And I need to find it by starting right here.

118
00:05:04,50 --> 00:05:07,40
I need to start here and then find that double quote.

119
00:05:07,40 --> 00:05:10,70
And to do that I'm going to use string.Index again.

120
00:05:10,70 --> 00:05:13,50
And I'm going to start with my string right here,

121
00:05:13,50 --> 00:05:14,90
so I'm going to start right there.

122
00:05:14,90 --> 00:05:17,50
I'm going to say give me the first instance of this.

123
00:05:17,50 --> 00:05:19,80
And so that's going to give me that position.

124
00:05:19,80 --> 00:05:24,60
So from here, it will give me the value from here to there.

125
00:05:24,60 --> 00:05:26,30
So it will give me that value.

126
00:05:26,30 --> 00:05:28,60
And to that I need to add everything

127
00:05:28,60 --> 00:05:30,00
from the beginning of the strings.

128
00:05:30,00 --> 00:05:31,90
So everything from the beginning of the string,

129
00:05:31,90 --> 00:05:33,20
wherever it started,

130
00:05:33,20 --> 00:05:35,70
will be way up in the beginning of the string.

131
00:05:35,70 --> 00:05:36,70
Right to there.

132
00:05:36,70 --> 00:05:38,80
And now I have this value.

133
00:05:38,80 --> 00:05:41,20
I'm starting to move stuff, I'm not going to do that.

134
00:05:41,20 --> 00:05:43,20
And now I have this value here too,

135
00:05:43,20 --> 00:05:46,30
which goes to right there, so when I add those together,

136
00:05:46,30 --> 00:05:48,00
I will have the start,

137
00:05:48,00 --> 00:05:50,10
which is the length from the beginning of the string,

138
00:05:50,10 --> 00:05:52,00
to this position right here.

139
00:05:52,00 --> 00:05:55,40
And I will have the length from right here to there.

140
00:05:55,40 --> 00:05:56,80
And when I add those together,

141
00:05:56,80 --> 00:05:58,20
that will be my end position.

142
00:05:58,20 --> 00:05:59,50
So that was the idea.

143
00:05:59,50 --> 00:06:01,40
And I print my start and I print my end,

144
00:06:01,40 --> 00:06:04,40
and then I'm slicing from start to end.

145
00:06:04,40 --> 00:06:08,20
In mathematics, like in algebra, this would be like that.

146
00:06:08,20 --> 00:06:11,10
Because it's start, including start,

147
00:06:11,10 --> 00:06:13,90
end, not including the end.

148
00:06:13,90 --> 00:06:15,10
So it doesn't include the end,

149
00:06:15,10 --> 00:06:16,50
that's just the way it works.

150
00:06:16,50 --> 00:06:17,20
I'm going to run this,

151
00:06:17,20 --> 00:06:22,30
and let's take a look to see what happens.

152
00:06:22,30 --> 00:06:23,10
So this is great,

153
00:06:23,10 --> 00:06:25,20
I got my strings out of each of my files.

154
00:06:25,20 --> 00:06:27,50
and I printed out the start position

155
00:06:27,50 --> 00:06:28,90
and the end position.

156
00:06:28,90 --> 00:06:30,90
Cropped and flipped all those different things.

157
00:06:30,90 --> 00:06:32,30
So now that I have those strings,

158
00:06:32,30 --> 00:06:33,40
I need to put them in some sort

159
00:06:33,40 --> 00:06:35,70
of a data structure and I need to sort it,

160
00:06:35,70 --> 00:06:38,00
and that's what we do in this file right here.

161
00:06:38,00 --> 00:06:41,40
So I changed xi(f) to return a string,

162
00:06:41,40 --> 00:06:43,00
and the string it's going to return

163
00:06:43,00 --> 00:06:46,10
is that extracted string, that description.

164
00:06:46,10 --> 00:06:48,30
And if it hasn't, it'll return that string,

165
00:06:48,30 --> 00:06:51,50
or it will return nothing, just an empty string.

166
00:06:51,50 --> 00:06:54,60
So when I call xi(f) up here, I get a string back.

167
00:06:54,60 --> 00:06:58,80
So for any file that has a .jpg or .jpg extension,

168
00:06:58,80 --> 00:07:01,90
it's going to check, does it have an image description?

169
00:07:01,90 --> 00:07:03,90
If so, it will give me that image description.

170
00:07:03,90 --> 00:07:06,60
If not, it will give me an empty string.

171
00:07:06,60 --> 00:07:10,70
So if the string is not empty,

172
00:07:10,70 --> 00:07:12,60
then we know we have the description.

173
00:07:12,60 --> 00:07:14,10
So what I'm going to do then is I'm going

174
00:07:14,10 --> 00:07:17,30
to take that string which is the description, right here,

175
00:07:17,30 --> 00:07:19,20
and I'm going to add to it the path.

176
00:07:19,20 --> 00:07:21,10
And I'm going to separate those two.

177
00:07:21,10 --> 00:07:23,70
We're concatenating a string together right here.

178
00:07:23,70 --> 00:07:25,10
And we're creating a new string,

179
00:07:25,10 --> 00:07:28,10
and then I'm going to pen to slice,

180
00:07:28,10 --> 00:07:30,60
which is a slice of string, I'm going to pen that.

181
00:07:30,60 --> 00:07:33,60
So that will allow me to have both the phrase,

182
00:07:33,60 --> 00:07:36,10
which I can sort on, and then also the path,

183
00:07:36,10 --> 00:07:38,30
which I can use to get the image.

184
00:07:38,30 --> 00:07:41,00
So I get my slice of string right here,

185
00:07:41,00 --> 00:07:42,90
and when this entire function is done,

186
00:07:42,90 --> 00:07:45,40
I return my slice of string after I've walked

187
00:07:45,40 --> 00:07:47,00
all of the different files,

188
00:07:47,00 --> 00:07:50,70
and I've added to my slice of strings right here.

189
00:07:50,70 --> 00:07:52,60
I'm going to return my slice of strings,

190
00:07:52,60 --> 00:07:55,60
so that gets returned up here from getImages,

191
00:07:55,60 --> 00:07:59,10
and I have my xs, my slice of string.

192
00:07:59,10 --> 00:08:00,50
And I'm going to sort that.

193
00:08:00,50 --> 00:08:01,70
From the sort package,

194
00:08:01,70 --> 00:08:03,90
I could use this function strings,

195
00:08:03,90 --> 00:08:06,10
I sort it, and then I can range over that,

196
00:08:06,10 --> 00:08:07,00
and print it out.

197
00:08:07,00 --> 00:08:09,40
And I should have all of my images

198
00:08:09,40 --> 00:08:11,40
that have an image description

199
00:08:11,40 --> 00:08:14,00
sorted by the image description.

200
00:08:14,00 --> 00:08:20,20
So let's give that a go and see what happens.

201
00:08:20,20 --> 00:08:22,50
There they are, and it looks a little bit funny,

202
00:08:22,50 --> 00:08:24,90
because everything is very big on that screen,

203
00:08:24,90 --> 00:08:27,80
but if I make it smaller it looks more compact.

204
00:08:27,80 --> 00:08:30,00
But that's the next step in my solution,

205
00:08:30,00 --> 00:08:31,20
and in the following videos,

206
00:08:31,20 --> 00:08:34,00
we'll see how I finish out this challenge.

