1
00:00:00,50 --> 00:00:02,20
- So the last thing I need to do

2
00:00:02,20 --> 00:00:05,50
is now get all of my images into a folder

3
00:00:05,50 --> 00:00:09,20
and have those images in some sort of a sorted order.

4
00:00:09,20 --> 00:00:12,30
And the order I want those images sorted in

5
00:00:12,30 --> 00:00:16,20
is alphabetically by that description which I found

6
00:00:16,20 --> 00:00:17,50
in the xf information.

7
00:00:17,50 --> 00:00:19,90
So remember, we're looking through all of these

8
00:00:19,90 --> 00:00:22,40
different folders and files for images.

9
00:00:22,40 --> 00:00:25,20
We're getting all the images that are in this

10
00:00:25,20 --> 00:00:26,50
directory structure.

11
00:00:26,50 --> 00:00:27,70
And then we're pulling out,

12
00:00:27,70 --> 00:00:29,70
we're extracting their xf information.

13
00:00:29,70 --> 00:00:33,50
From the xf information we're getting the description.

14
00:00:33,50 --> 00:00:36,10
We are then sorting on that description

15
00:00:36,10 --> 00:00:37,80
and now we want to get those images

16
00:00:37,80 --> 00:00:39,50
and have them in a directory somewhere

17
00:00:39,50 --> 00:00:42,10
sorted by that description.

18
00:00:42,10 --> 00:00:44,20
That's been our entire task and this is where

19
00:00:44,20 --> 00:00:45,80
we're actually going to create the images

20
00:00:45,80 --> 00:00:48,90
and put them into a folder in a certain order.

21
00:00:48,90 --> 00:00:51,00
And the easiest way to do this, for me,

22
00:00:51,00 --> 00:00:53,00
was just to create new images,

23
00:00:53,00 --> 00:00:54,50
not trying to take the old images

24
00:00:54,50 --> 00:00:56,90
and rename them and put them into the right order.

25
00:00:56,90 --> 00:00:59,40
I will take old images, but also create new images

26
00:00:59,40 --> 00:01:00,50
and give it the name I want

27
00:01:00,50 --> 00:01:02,00
and put it into a new folder.

28
00:01:02,00 --> 00:01:03,50
So that's how I'm going to do that.

29
00:01:03,50 --> 00:01:05,30
And the way I did it is right here

30
00:01:05,30 --> 00:01:06,80
on this first one, it's my first shot,

31
00:01:06,80 --> 00:01:09,50
and here's the refined approach right there.

32
00:01:09,50 --> 00:01:11,10
So let's take a look at this first shot

33
00:01:11,10 --> 00:01:13,00
at creating it the easier solution.

34
00:01:13,00 --> 00:01:17,00
So this is a really cool thing right here strings.Split.

35
00:01:17,00 --> 00:01:19,20
And you'll notice that when I created my string

36
00:01:19,20 --> 00:01:21,90
I originally in this previous example,

37
00:01:21,90 --> 00:01:25,10
I put that little separator in, in the string,

38
00:01:25,10 --> 00:01:27,70
with the image description right there.

39
00:01:27,70 --> 00:01:30,30
I put that little separator in between the string

40
00:01:30,30 --> 00:01:31,20
and the path.

41
00:01:31,20 --> 00:01:33,90
And I did that so that in this next example

42
00:01:33,90 --> 00:01:35,90
I could use strings.Split.

43
00:01:35,90 --> 00:01:37,40
Well what does strings.Split do?

44
00:01:37,40 --> 00:01:41,10
Split will split a string on a delimeter

45
00:01:41,10 --> 00:01:43,30
and it'll give me back a slice of string.

46
00:01:43,30 --> 00:01:45,00
So, wherever the delimeter occur,

47
00:01:45,00 --> 00:01:47,20
there'll be one of my entries and my slice.

48
00:01:47,20 --> 00:01:48,30
And so that's what split does.

49
00:01:48,30 --> 00:01:50,70
And it can read that right here.

50
00:01:50,70 --> 00:01:52,40
So I'm going to split my strings,

51
00:01:52,40 --> 00:01:54,00
which gives me back a slice.

52
00:01:54,00 --> 00:01:57,00
I know there's only one separator in there.

53
00:01:57,00 --> 00:01:58,30
At least that's what I''m hoping.

54
00:01:58,30 --> 00:01:59,90
I've pretty good confidence over my data.

55
00:01:59,90 --> 00:02:01,50
I saw it print out earlier.

56
00:02:01,50 --> 00:02:03,40
And so, if with only one separator,

57
00:02:03,40 --> 00:02:07,40
I'm going to have an entry in my slice in position zero.

58
00:02:07,40 --> 00:02:08,90
So zero based index.

59
00:02:08,90 --> 00:02:10,90
And I'll also have an entry in my slice

60
00:02:10,90 --> 00:02:12,40
in position one.

61
00:02:12,40 --> 00:02:15,50
So remember my slice was right here, right?

62
00:02:15,50 --> 00:02:17,30
I'm going to get this in position zero,

63
00:02:17,30 --> 00:02:19,20
and that in position one.

64
00:02:19,20 --> 00:02:20,30
That was my string.

65
00:02:20,30 --> 00:02:25,00
So, when I use strings.Split right here,

66
00:02:25,00 --> 00:02:28,10
it'll create a slice and put this in position zero

67
00:02:28,10 --> 00:02:30,30
and that in position one.

68
00:02:30,30 --> 00:02:32,70
Alright, so I'm saying give me what's in position one

69
00:02:32,70 --> 00:02:33,90
and that's my path.

70
00:02:33,90 --> 00:02:35,90
And now I want to open that path.

71
00:02:35,90 --> 00:02:37,20
And I catch there and deal with it,

72
00:02:37,20 --> 00:02:38,40
if there isn't there.

73
00:02:38,40 --> 00:02:40,50
I'm going to create a new file.

74
00:02:40,50 --> 00:02:42,60
So os.Create, I'm going to call it new file,

75
00:02:42,60 --> 00:02:44,10
nf for new file.

76
00:02:44,10 --> 00:02:48,30
And then I'm going to use strconv.Itos(i),

77
00:02:48,30 --> 00:02:51,20
and I'm going to take that and the i,

78
00:02:51,20 --> 00:02:53,40
so i is coming from when I call create file,

79
00:02:53,40 --> 00:02:55,00
which is right here.

80
00:02:55,00 --> 00:02:57,70
Call create file, and I'm passing in the index of a loop.

81
00:02:57,70 --> 00:03:00,00
So, whatever (mumbles) we're on,

82
00:03:00,00 --> 00:03:03,30
and then also the value, which will be that string.

83
00:03:03,30 --> 00:03:06,20
So I'm taking that i and I'm going to add to it

84
00:03:06,20 --> 00:03:08,70
through concatenation .jpg.

85
00:03:08,70 --> 00:03:11,90
And so I'll sequentially go through

86
00:03:11,90 --> 00:03:14,90
my sorted slice of strings, right here.

87
00:03:14,90 --> 00:03:17,00
So I sorted my slices strings,

88
00:03:17,00 --> 00:03:19,10
and I'm going to sequentially loop through them

89
00:03:19,10 --> 00:03:20,20
and each time I loop through it,

90
00:03:20,20 --> 00:03:21,60
I'm going to create a file.

91
00:03:21,60 --> 00:03:22,90
And the file I'm going to create,

92
00:03:22,90 --> 00:03:26,80
they're going to be numerically ordered right? By number.

93
00:03:26,80 --> 00:03:30,20
And so it'll be just by the index value that comes through.

94
00:03:30,20 --> 00:03:33,40
Now with my new file, I'm going to use io.Copy

95
00:03:33,40 --> 00:03:37,10
and io.Copy takes a destination and a source.

96
00:03:37,10 --> 00:03:38,60
And it wants a writer, something that

97
00:03:38,60 --> 00:03:40,10
implements the writer interface.

98
00:03:40,10 --> 00:03:42,60
And the source needs to implement the reader interface.

99
00:03:42,60 --> 00:03:46,20
So it need to read the source, and write to the destination.

100
00:03:46,20 --> 00:03:48,60
While a file implements both the writer

101
00:03:48,60 --> 00:03:50,10
and the reader interface,

102
00:03:50,10 --> 00:03:51,80
and we already saw how that works.

103
00:03:51,80 --> 00:03:54,00
So I just put in here my new file,

104
00:03:54,00 --> 00:03:57,40
the destination, my old file, which is the source.

105
00:03:57,40 --> 00:04:00,40
And I do io.Copy and that's everything I need to do,

106
00:04:00,40 --> 00:04:02,60
hopefully, if it all works as planned.

107
00:04:02,60 --> 00:04:06,80
Let's take a look, 13.

108
00:04:06,80 --> 00:04:10,50
And, so it printed out a little bit of information.

109
00:04:10,50 --> 00:04:12,80
We can dig around to see where that print is coming from.

110
00:04:12,80 --> 00:04:14,90
I remember, right here.

111
00:04:14,90 --> 00:04:17,00
And more importantly, look at that.

112
00:04:17,00 --> 00:04:19,60
We've got our images and we have them in sorted order.

113
00:04:19,60 --> 00:04:21,80
But what that doesn't tell me is

114
00:04:21,80 --> 00:04:24,30
okay but, what was the description originally?

115
00:04:24,30 --> 00:04:27,10
Can we see the description where those are sorted?

116
00:04:27,10 --> 00:04:28,40
So I'm going to close this,

117
00:04:28,40 --> 00:04:30,80
and I'm going to delete those images.

118
00:04:30,80 --> 00:04:32,90
Otherwise, I'm going to get redundancy

119
00:04:32,90 --> 00:04:34,10
when I run this code here,

120
00:04:34,10 --> 00:04:35,50
because it'll find the images up there.

121
00:04:35,50 --> 00:04:37,30
And it'll also find them right there.

122
00:04:37,30 --> 00:04:39,90
So now, in 14 what I've done

123
00:04:39,90 --> 00:04:42,30
is I've use a little concatenation again

124
00:04:42,30 --> 00:04:43,80
when I create my new file.

125
00:04:43,80 --> 00:04:45,70
And instead of having it just be

126
00:04:45,70 --> 00:04:49,70
a number and then the jpg right there,

127
00:04:49,70 --> 00:04:52,10
I'm also adding in the description.

128
00:04:52,10 --> 00:04:53,60
And to get the description,

129
00:04:53,60 --> 00:04:56,70
I've don strings.Split in position one

130
00:04:56,70 --> 00:04:58,70
was my path.

131
00:04:58,70 --> 00:05:01,90
I want what's in position zero which is the string,

132
00:05:01,90 --> 00:05:04,50
which was that description that I originally had.

133
00:05:04,50 --> 00:05:07,40
And so I say, alright, the description is in position zero,

134
00:05:07,40 --> 00:05:09,50
and then I can pass the description in right there.

135
00:05:09,50 --> 00:05:13,60
So let's take a look at that and watch that run.

136
00:05:13,60 --> 00:05:15,10
So it all ran.

137
00:05:15,10 --> 00:05:16,10
And then we look here,

138
00:05:16,10 --> 00:05:18,20
and we have all our images printed out.

139
00:05:18,20 --> 00:05:20,60
We've sorted them out alphabetically by description.

140
00:05:20,60 --> 00:05:21,60
So that's pretty great.

141
00:05:21,60 --> 00:05:23,80
This was a fun challenge, I enjoyed it.

142
00:05:23,80 --> 00:05:25,50
There's definitely some things in it

143
00:05:25,50 --> 00:05:26,80
which were a little bit challenging,

144
00:05:26,80 --> 00:05:28,90
but it wasn't over the top.

145
00:05:28,90 --> 00:05:30,40
So is right at that perfect place

146
00:05:30,40 --> 00:05:32,30
where it took a little bit of time,

147
00:05:32,30 --> 00:05:34,30
but it also offered success.

148
00:05:34,30 --> 00:05:37,40
And it wasn't like it took weeks to solve this one.

149
00:05:37,40 --> 00:05:38,50
So I enjoyed this challenge.

150
00:05:38,50 --> 00:05:41,30
In the next video we're going to take a look at

151
00:05:41,30 --> 00:05:43,20
just some of the useful things you can do

152
00:05:43,20 --> 00:05:45,50
using that file path walk function.

153
00:05:45,50 --> 00:05:47,30
And I think these'll be helpful to use.

154
00:05:47,30 --> 00:05:49,00
So I just want to pass them on as

155
00:05:49,00 --> 00:05:50,00
something's that's kind of fun.

