1
00:00:00,30 --> 00:00:01,40
- So next thing we're going to do

2
00:00:01,40 --> 00:00:03,70
is we're going to download and install WebStorm,

3
00:00:03,70 --> 00:00:06,00
so that you'll have a good editor for writing GO code.

4
00:00:06,00 --> 00:00:08,10
WebStorm does cost money,

5
00:00:08,10 --> 00:00:10,40
so if you're a student, you could do

6
00:00:10,40 --> 00:00:12,80
WebStorm education.

7
00:00:12,80 --> 00:00:13,90
Spell that correctly.

8
00:00:13,90 --> 00:00:16,30
There's a link here, free for students,

9
00:00:16,30 --> 00:00:18,90
and so you could apply to use WebStorm for free,

10
00:00:18,90 --> 00:00:20,30
and the company will decide,

11
00:00:20,30 --> 00:00:21,40
JetBrains is the company,

12
00:00:21,40 --> 00:00:23,40
whether or not they'll let you use it.

13
00:00:23,40 --> 00:00:26,30
And if you're not wanting to pay for WebStorm,

14
00:00:26,30 --> 00:00:28,60
I'll also show you how to set up atom.io

15
00:00:28,60 --> 00:00:30,20
in the next video on Windows,

16
00:00:30,20 --> 00:00:33,20
to use Atom to do your editing of GO code.

17
00:00:33,20 --> 00:00:34,90
Alright, so let's do WebStorm.

18
00:00:34,90 --> 00:00:37,00
We're going to download WebStorm,

19
00:00:37,00 --> 00:00:40,50
and so follow this link, JetBrains WebStorm download,

20
00:00:40,50 --> 00:00:43,10
and download.

21
00:00:43,10 --> 00:00:44,40
So now it's downloading.

22
00:00:44,40 --> 00:00:47,60
I'll click this and open it,

23
00:00:47,60 --> 00:00:49,70
and then hit Next,

24
00:00:49,70 --> 00:00:50,90
and Next,

25
00:00:50,90 --> 00:00:53,70
and Next, and Install.

26
00:00:53,70 --> 00:00:54,90
Now I want to run WebStorm,

27
00:00:54,90 --> 00:00:56,80
so I'm going to put this checkmark on right there,

28
00:00:56,80 --> 00:00:59,30
and I'm going to click Finish.

29
00:00:59,30 --> 00:01:02,00
It's going to ask me, do I want to import any settings

30
00:01:02,00 --> 00:01:03,80
if I've been running WebStorm before,

31
00:01:03,80 --> 00:01:06,60
and I do not, so I'm going to click OK.

32
00:01:06,60 --> 00:01:08,20
And then it's going to say, do you want to buy it

33
00:01:08,20 --> 00:01:09,60
or use the evaluation version?

34
00:01:09,60 --> 00:01:13,70
I'll use the evaluation version, and I will accept this.

35
00:01:13,70 --> 00:01:14,30
And then it's saying,

36
00:01:14,30 --> 00:01:16,10
hey how do you want to configure WebStorm?

37
00:01:16,10 --> 00:01:18,00
I'm just going to skip this, because I'm going to show you

38
00:01:18,00 --> 00:01:20,60
how to download themes to configure WebStorm.

39
00:01:20,60 --> 00:01:22,10
So the first thing I'm going to do,

40
00:01:22,10 --> 00:01:25,20
is click on Configure down here, and go to Plugins,

41
00:01:25,20 --> 00:01:26,90
and once here, I'm going to turn off

42
00:01:26,90 --> 00:01:29,00
all of the plugins I do not want to use,

43
00:01:29,00 --> 00:01:31,30
because the fewer plugins I have running,

44
00:01:31,30 --> 00:01:36,70
the fewer resources Webstorm will take up on my computer.

45
00:01:36,70 --> 00:01:37,90
So now I have those turned off,

46
00:01:37,90 --> 00:01:40,40
I'll just kind of let you take a look at that screen,

47
00:01:40,40 --> 00:01:43,50
and this screen right here, and this screen right here.

48
00:01:43,50 --> 00:01:45,90
The next thing I want to do is I want to go get a plugin

49
00:01:45,90 --> 00:01:47,60
to allow me to write GO code.

50
00:01:47,60 --> 00:01:49,50
So I'm going to go back to my browser,

51
00:01:49,50 --> 00:01:53,90
and I'm going to search for webstorm golang plugin,

52
00:01:53,90 --> 00:01:55,20
and then follow this link

53
00:01:55,20 --> 00:01:59,00
to support plugin - Plugin Repository - JetBrains.

54
00:01:59,00 --> 00:02:01,00
And once I get here, I'm not going to

55
00:02:01,00 --> 00:02:03,00
download the top one from the top of the list,

56
00:02:03,00 --> 00:02:04,70
even though it looks like it's the most recent.

57
00:02:04,70 --> 00:02:07,50
I'm going to choose show all updates,

58
00:02:07,50 --> 00:02:10,40
and this is November 2015.

59
00:02:10,40 --> 00:02:13,40
Now it's January 2016, so that's the most current one.

60
00:02:13,40 --> 00:02:15,90
I'm going to choose Download.

61
00:02:15,90 --> 00:02:18,30
And I do not want to unzip this folder.

62
00:02:18,30 --> 00:02:22,70
I want to import this into WebStorm completely unzipped,

63
00:02:22,70 --> 00:02:26,20
so I'm going to choose Install plugin from disk.

64
00:02:26,20 --> 00:02:29,70
I'm going to go to my Downloads folder,

65
00:02:29,70 --> 00:02:33,60
select that, hit OK, and then hit OK.

66
00:02:33,60 --> 00:02:36,40
It's going to ask me to Restart WebStorm.

67
00:02:36,40 --> 00:02:40,40
And the first thing I'm going to do is click Open,

68
00:02:40,40 --> 00:02:42,60
and I'm going to go again to Todd McLeod,

69
00:02:42,60 --> 00:02:45,40
but this time I'm going to go to my Documents,

70
00:02:45,40 --> 00:02:48,10
and to my goworkspace, and into src,

71
00:02:48,10 --> 00:02:51,50
and then github.com, GoesToEleven, lynda, cc,

72
00:02:51,50 --> 00:02:52,70
and I'm going to open up cc

73
00:02:52,70 --> 00:02:55,70
for all of the files for this course.

74
00:02:55,70 --> 00:02:58,70
Saying I Can't start Git: git.exe, Fix it.

75
00:02:58,70 --> 00:03:01,70
So to figure out where git.exe is on Windows,

76
00:03:01,70 --> 00:03:03,20
I'm going to click Fix it right there,

77
00:03:03,20 --> 00:03:04,00
and it's going to want

78
00:03:04,00 --> 00:03:06,40
the Path to Git executable on Windows,

79
00:03:06,40 --> 00:03:08,40
I'm going to go back to my browser.

80
00:03:08,40 --> 00:03:11,00
This is an issue I've solved many times with students,

81
00:03:11,00 --> 00:03:15,80
and I'm going to search for where is git located on windows.

82
00:03:15,80 --> 00:03:17,90
There's a couple of different articles here.

83
00:03:17,90 --> 00:03:20,40
There's one for this Stack Overflow article,

84
00:03:20,40 --> 00:03:23,30
and then this one gave me the solution for Windows 10,

85
00:03:23,30 --> 00:03:25,20
so I'm going to click this one.

86
00:03:25,20 --> 00:03:27,10
See, I've visited those links before.

87
00:03:27,10 --> 00:03:31,40
And I can find it at this location.

88
00:03:31,40 --> 00:03:32,50
So I'm going to copy that,

89
00:03:32,50 --> 00:03:35,80
and go back to Windows Explorer.

90
00:03:35,80 --> 00:03:38,40
Paste that in, change the Username

91
00:03:38,40 --> 00:03:40,30
to my username.

92
00:03:40,30 --> 00:03:41,70
Hit Enter.

93
00:03:41,70 --> 00:03:44,70
There's PortableGit, which is the next step in the process,

94
00:03:44,70 --> 00:03:46,50
and then I'm going to go into Command,

95
00:03:46,50 --> 00:03:48,20
and then there's the git right there.

96
00:03:48,20 --> 00:03:49,30
So now that I have all that,

97
00:03:49,30 --> 00:03:52,60
I'm going to Copy that, come back to WebStorm,

98
00:03:52,60 --> 00:03:55,10
and Paste that in here,

99
00:03:55,10 --> 00:03:56,70
and see if it likes that.

100
00:03:56,70 --> 00:03:58,60
Does not, so I'm going to add

101
00:03:58,60 --> 00:04:02,30
another \git.exe

102
00:04:02,30 --> 00:04:04,90
and see if it likes that,

103
00:04:04,90 --> 00:04:05,70
and it likes it.

104
00:04:05,70 --> 00:04:06,90
Always good news.

105
00:04:06,90 --> 00:04:09,20
Hit OK, hit OK.

106
00:04:09,20 --> 00:04:10,70
So I have that set up.

107
00:04:10,70 --> 00:04:13,60
So the next thing that might be interesting for you

108
00:04:13,60 --> 00:04:17,40
is to change the theme in Webstorm, if you want to do that.

109
00:04:17,40 --> 00:04:18,40
So I'm going to open a file

110
00:04:18,40 --> 00:04:20,30
so we could see what the theme looks like right now.

111
00:04:20,30 --> 00:04:22,00
And soon as I open this file,

112
00:04:22,00 --> 00:04:24,00
it says Project SDK is not defined,

113
00:04:24,00 --> 00:04:26,70
so I'm going to choose Setup SDK,

114
00:04:26,70 --> 00:04:28,50
and it wants the Go SDK,

115
00:04:28,50 --> 00:04:30,90
so this wants the root, wants Go root.

116
00:04:30,90 --> 00:04:32,90
If I just click that, it's going to take me right there.

117
00:04:32,90 --> 00:04:34,30
So that's where Go is located.

118
00:04:34,30 --> 00:04:36,60
Hit OK, and hit OK.

119
00:04:36,60 --> 00:04:38,40
So now that has gone away.

120
00:04:38,40 --> 00:04:39,50
That error's gone away.

121
00:04:39,50 --> 00:04:42,20
This error, you could just click that and close that.

122
00:04:42,20 --> 00:04:44,10
Don't worry about that.

123
00:04:44,10 --> 00:04:44,90
And that's it.

124
00:04:44,90 --> 00:04:48,30
WebStorm is now ready to write GO code,

125
00:04:48,30 --> 00:04:49,80
and I could verify this

126
00:04:49,80 --> 00:04:53,70
just by taking that out and doing fmt.Println,

127
00:04:53,70 --> 00:04:55,70
and you can see, Code Completion is coming up.

128
00:04:55,70 --> 00:04:57,00
I love Code Completion.

129
00:04:57,00 --> 00:04:58,90
So last thing I want to show you about WebStorm,

130
00:04:58,90 --> 00:05:00,90
is that you could come over to your browser,

131
00:05:00,90 --> 00:05:04,40
and you could search for WebStorm themes,

132
00:05:04,40 --> 00:05:07,90
and you could go to color-themes.com,

133
00:05:07,90 --> 00:05:10,20
and in the rest of this training,

134
00:05:10,20 --> 00:05:12,70
I'm using Relax Your Eyes, this theme right here.

135
00:05:12,70 --> 00:05:14,50
So if you like this theme,

136
00:05:14,50 --> 00:05:16,70
you could download this,

137
00:05:16,70 --> 00:05:19,40
click this button.

138
00:05:19,40 --> 00:05:21,10
I'm not going to open that file.

139
00:05:21,10 --> 00:05:23,10
I'm going to come back to WebStorm.

140
00:05:23,10 --> 00:05:25,10
I'm going to hit Command + Z a couple of times

141
00:05:25,10 --> 00:05:27,20
to bring me back to the way the code was before,

142
00:05:27,20 --> 00:05:30,50
and then i'll go File, Import Settings.

143
00:05:30,50 --> 00:05:33,50
So not Settings, but Import Settings.

144
00:05:33,50 --> 00:05:35,60
And I'll go find that file I just downloaded,

145
00:05:35,60 --> 00:05:37,90
so it'll be in Todd McLeod,

146
00:05:37,90 --> 00:05:39,40
Downloads,

147
00:05:39,40 --> 00:05:40,40
Relax Your Eyes.

148
00:05:40,40 --> 00:05:42,80
Just open that up, hit OK.

149
00:05:42,80 --> 00:05:46,20
Webstorm will want to Restart.

150
00:05:46,20 --> 00:05:47,40
And now I have the theme

151
00:05:47,40 --> 00:05:49,90
that I've used in this course, now showing here.

152
00:05:49,90 --> 00:05:50,70
Doesn't look all that different

153
00:05:50,70 --> 00:05:52,00
than the theme we had before.

154
00:05:52,00 --> 00:05:53,40
One last thing I'll show you,

155
00:05:53,40 --> 00:05:55,50
is if you want to change some Settings in WebStorm,

156
00:05:55,50 --> 00:05:56,80
there's a really helpful feature.

157
00:05:56,80 --> 00:05:58,90
You go to File, Settings, and then you can come in here,

158
00:05:58,90 --> 00:06:01,70
and you can search for, like, Font, and it filters it out.

159
00:06:01,70 --> 00:06:04,60
And it'll show like, hey I want to change the Font size,

160
00:06:04,60 --> 00:06:07,40
you could say I want this to be 24.

161
00:06:07,40 --> 00:06:08,70
OK.

162
00:06:08,70 --> 00:06:09,50
So that's it.

163
00:06:09,50 --> 00:06:12,00
That's how you get WebStorm set up on Windows.

