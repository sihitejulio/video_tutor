1
00:00:00,50 --> 00:00:02,60
- So now we're going to get Go set up on Windows,

2
00:00:02,60 --> 00:00:04,60
and to download and install Go,

3
00:00:04,60 --> 00:00:07,40
we're going to go to golang.org,

4
00:00:07,40 --> 00:00:09,80
and at golang.org, big gopher right here,

5
00:00:09,80 --> 00:00:11,50
Download Go, click on that and you could see

6
00:00:11,50 --> 00:00:13,90
here's the download for Windows.

7
00:00:13,90 --> 00:00:15,10
I want you to take a moment, though,

8
00:00:15,10 --> 00:00:16,70
and just reflect on this part right here,

9
00:00:16,70 --> 00:00:19,80
it's a 64-bit processor, so if you scroll down,

10
00:00:19,80 --> 00:00:21,40
you see that you could download Go

11
00:00:21,40 --> 00:00:24,20
for different operating systems and different architectures.

12
00:00:24,20 --> 00:00:25,60
If you have a newer computer,

13
00:00:25,60 --> 00:00:28,10
odds are very good you're on a 64-bit architecture,

14
00:00:28,10 --> 00:00:30,50
and you're just going to use this one right here.

15
00:00:30,50 --> 00:00:31,80
If your computer's a little bit older,

16
00:00:31,80 --> 00:00:34,30
you might want to make sure that you're not 32-bit,

17
00:00:34,30 --> 00:00:36,10
or if you are 32-bit, you want to use

18
00:00:36,10 --> 00:00:38,70
this download right here, this msi download,

19
00:00:38,70 --> 00:00:40,60
32-bit for Windows.

20
00:00:40,60 --> 00:00:42,40
So to check that, we're going to come down

21
00:00:42,40 --> 00:00:44,20
to the right corner, the Start button,

22
00:00:44,20 --> 00:00:46,70
right click it and go to File Explorer,

23
00:00:46,70 --> 00:00:49,60
and inside File Explorer, you could right click This PC

24
00:00:49,60 --> 00:00:52,60
or My Computer, and choose Properties,

25
00:00:52,60 --> 00:00:54,20
and it'll tell you, 64-bit.

26
00:00:54,20 --> 00:00:56,40
So I'm good with the 64-bit version, like I said,

27
00:00:56,40 --> 00:00:58,00
if you see 32-bit right here,

28
00:00:58,00 --> 00:00:59,80
you're going to want the 32-bit one,

29
00:00:59,80 --> 00:01:02,30
so I'm going to download the 64-bit version.

30
00:01:02,30 --> 00:01:03,90
Just download it to my machine.

31
00:01:03,90 --> 00:01:05,90
I'm going to click it and open it,

32
00:01:05,90 --> 00:01:09,50
and then go through this dialog box.

33
00:01:09,50 --> 00:01:11,00
And now, I'll click Finish.

34
00:01:11,00 --> 00:01:12,30
So the next thing you need to do

35
00:01:12,30 --> 00:01:14,40
to get Go working well on your machine

36
00:01:14,40 --> 00:01:15,90
is you need to create a workspace

37
00:01:15,90 --> 00:01:18,30
where you're going to do all of your work in Go.

38
00:01:18,30 --> 00:01:21,00
So go into File Explorer again, go into Documents,

39
00:01:21,00 --> 00:01:24,00
and I've already created one here called goworkspace.

40
00:01:24,00 --> 00:01:26,90
You could call this anything

41
00:01:26,90 --> 00:01:28,90
you want. (laughs)

42
00:01:28,90 --> 00:01:29,90
It could be any name.

43
00:01:29,90 --> 00:01:32,50
I like goworkspace, I'm going to leave it as goworkspace,

44
00:01:32,50 --> 00:01:36,00
and inside goworkspace, you're going to create a bin folder,

45
00:01:36,00 --> 00:01:38,30
a package folder, and a source folder.

46
00:01:38,30 --> 00:01:39,20
I'm going to show you something

47
00:01:39,20 --> 00:01:40,50
really cool here in a moment,

48
00:01:40,50 --> 00:01:43,20
and how this helps us do our Go programming,

49
00:01:43,20 --> 00:01:44,70
but one of the things we need to do

50
00:01:44,70 --> 00:01:47,90
is we need to create a couple of path environment variables

51
00:01:47,90 --> 00:01:51,20
telling our system that hey, we have this goworkspace,

52
00:01:51,20 --> 00:01:53,30
and this is where we're doing all of our Go code,

53
00:01:53,30 --> 00:01:56,60
and then that allows Go to integrate with your workspace

54
00:01:56,60 --> 00:01:58,60
and to do some pretty neat things.

55
00:01:58,60 --> 00:02:00,20
So to set our environment variables,

56
00:02:00,20 --> 00:02:01,70
the first environment variable I want

57
00:02:01,70 --> 00:02:05,30
is right to this workspace, so I'm going to Copy that,

58
00:02:05,30 --> 00:02:07,30
and then I'm going to come down to this right here,

59
00:02:07,30 --> 00:02:10,60
This PC, right click it and choose Properties,

60
00:02:10,60 --> 00:02:13,30
and go to Advanced system settings,

61
00:02:13,30 --> 00:02:15,30
and then I'm going to go to Environment Variables,

62
00:02:15,30 --> 00:02:17,60
and I'm going to create a new System variable,

63
00:02:17,60 --> 00:02:20,00
so this will be for everybody who logs in.

64
00:02:20,00 --> 00:02:21,60
If I wanted to just create an Environment Variable

65
00:02:21,60 --> 00:02:23,30
just for Todd McLeod when he's logged in,

66
00:02:23,30 --> 00:02:24,50
I'd set it up here.

67
00:02:24,50 --> 00:02:26,50
This is my computer, I'm using it,

68
00:02:26,50 --> 00:02:27,70
Im setting it right down here,

69
00:02:27,70 --> 00:02:29,10
and so, it's just going to be set right there,

70
00:02:29,10 --> 00:02:31,30
and I'm going to call this one GOPATH,

71
00:02:31,30 --> 00:02:33,50
and then paste in that URL,

72
00:02:33,50 --> 00:02:35,60
so I've set my GOPATH Variable right there.

73
00:02:35,60 --> 00:02:38,00
So you need that GOPATH Variable set.

74
00:02:38,00 --> 00:02:40,40
One more Environment Variable which I would like to set,

75
00:02:40,40 --> 00:02:42,40
which will be helpful when we're doing your programming

76
00:02:42,40 --> 00:02:44,40
and you'll see me using that, maybe occasionally

77
00:02:44,40 --> 00:02:45,70
throughout the rest of this course,

78
00:02:45,70 --> 00:02:49,00
is one pointing right to this bin inside my workspace,

79
00:02:49,00 --> 00:02:52,20
so I'll Copy that,

80
00:02:52,20 --> 00:02:55,90
and then come back to my Environment Variables,

81
00:02:55,90 --> 00:02:58,60
and I'm going to scroll down to my Path,

82
00:02:58,60 --> 00:03:02,00
and I'm going to Edit that, and you can see here,

83
00:03:02,00 --> 00:03:03,60
that I have C:\Go\bin right there.

84
00:03:03,60 --> 00:03:05,50
I'm going to add one more to that.

85
00:03:05,50 --> 00:03:08,30
Hit a semicolon, ctrl + v,

86
00:03:08,30 --> 00:03:10,30
and I just added this Path right there

87
00:03:10,30 --> 00:03:11,70
to Path Variables here,

88
00:03:11,70 --> 00:03:13,80
so Path Variables is just a generic catchall

89
00:03:13,80 --> 00:03:15,90
for other Paths you might want to include

90
00:03:15,90 --> 00:03:17,30
in your system environment,

91
00:03:17,30 --> 00:03:18,70
so that's where I'm putting that.

92
00:03:18,70 --> 00:03:19,60
Hit OK,

93
00:03:19,60 --> 00:03:20,70
hit OK,

94
00:03:20,70 --> 00:03:21,70
hit OK.

95
00:03:21,70 --> 00:03:23,40
All right, so the next thing we need to do

96
00:03:23,40 --> 00:03:25,60
to make our environment work really well,

97
00:03:25,60 --> 00:03:27,50
is I'm going to be showing you, the rest of this course,

98
00:03:27,50 --> 00:03:29,00
I'm going to be working on a Mac,

99
00:03:29,00 --> 00:03:31,70
and I'm going to be using the terminal shell, Bash,

100
00:03:31,70 --> 00:03:32,80
whatever you want to call it,

101
00:03:32,80 --> 00:03:35,20
I'm going to be using Unix-type commands

102
00:03:35,20 --> 00:03:37,70
for doing my command line interface programming.

103
00:03:37,70 --> 00:03:39,90
If you just use the command line interface,

104
00:03:39,90 --> 00:03:41,60
Command Prompt here on Windows,

105
00:03:41,60 --> 00:03:43,10
you're going to have different commands,

106
00:03:43,10 --> 00:03:45,10
so I want you to be able to have the same commands,

107
00:03:45,10 --> 00:03:47,30
and this will actually help you avoid an error

108
00:03:47,30 --> 00:03:49,00
which sometimes comes up on Windows,

109
00:03:49,00 --> 00:03:51,00
and so to avoid that error,

110
00:03:51,00 --> 00:03:55,30
we're going to get the github download for Desktop.

111
00:03:55,30 --> 00:03:58,00
So we're going to go to GitHub Desktop,

112
00:03:58,00 --> 00:04:00,70
choose to Download it,

113
00:04:00,70 --> 00:04:03,80
and open it up,

114
00:04:03,80 --> 00:04:06,30
and click Install,

115
00:04:06,30 --> 00:04:07,50
and now that this has come up,

116
00:04:07,50 --> 00:04:10,40
I'm going to go up to this little gear icon right here,

117
00:04:10,40 --> 00:04:12,40
and I'm going to choose Options,

118
00:04:12,40 --> 00:04:14,00
and in the Options, you could just enter

119
00:04:14,00 --> 00:04:15,90
your git information, right?

120
00:04:15,90 --> 00:04:18,30
So I'm going to enter whatever I use in my GitHub account,

121
00:04:18,30 --> 00:04:20,70
and I'm also going to come over to Default shell,

122
00:04:20,70 --> 00:04:22,30
and I'm going to choose Get Bash,

123
00:04:22,30 --> 00:04:23,50
so make sure you select that one

124
00:04:23,50 --> 00:04:26,50
if you want to use the same command line interface commands

125
00:04:26,50 --> 00:04:28,10
that I'm using throughout this course.

126
00:04:28,10 --> 00:04:30,10
Make sure you have Get Bash selected,

127
00:04:30,10 --> 00:04:31,80
and then choose Save,

128
00:04:31,80 --> 00:04:33,40
and I'm going to close this,

129
00:04:33,40 --> 00:04:35,10
and now I could come down to all of my apps,

130
00:04:35,10 --> 00:04:38,10
all my programs on my computer right here, All of my apps,

131
00:04:38,10 --> 00:04:40,30
and I could scroll down to the Gs,

132
00:04:40,30 --> 00:04:41,40
there go the Gs,

133
00:04:41,40 --> 00:04:45,00
and here is GitHub, and I can launch Git Shell.

134
00:04:45,00 --> 00:04:48,80
So now on Git Shell, I should be able to do go env,

135
00:04:48,80 --> 00:04:50,60
and that'll tell me my Go environment.

136
00:04:50,60 --> 00:04:53,70
Go's a command we run in Go to do different things,

137
00:04:53,70 --> 00:04:55,80
and you could see here, I have my GOROOT,

138
00:04:55,80 --> 00:04:57,60
and I also have my GOPATH,

139
00:04:57,60 --> 00:04:59,30
and I could also just learn more about Go

140
00:04:59,30 --> 00:05:01,30
by typing in go, and it'll show me

141
00:05:01,30 --> 00:05:03,60
all the different Go commands which are available,

142
00:05:03,60 --> 00:05:06,90
but the command I want to do right now is go get,

143
00:05:06,90 --> 00:05:08,70
and I'm going to go get some code,

144
00:05:08,70 --> 00:05:10,80
and the code that I'm going to get

145
00:05:10,80 --> 00:05:15,10
is github goestoeleven, and that's my username on GitHub,

146
00:05:15,10 --> 00:05:17,20
so if you come here,

147
00:05:17,20 --> 00:05:18,80
and click on Repositories,

148
00:05:18,80 --> 00:05:20,40
and then choose lynda,

149
00:05:20,40 --> 00:05:22,40
and then copy this URL,

150
00:05:22,40 --> 00:05:24,40
gonna ctrl + c,

151
00:05:24,40 --> 00:05:26,90
and then come back to my shell,

152
00:05:26,90 --> 00:05:30,20
and go get, space, ctrl + v to paste it in.

153
00:05:30,20 --> 00:05:31,60
That didn't seem to work for some reason,

154
00:05:31,60 --> 00:05:34,00
so instead, I'll try right clicking and see if that works,

155
00:05:34,00 --> 00:05:35,80
and there we go, there's Paste, and now I have it,

156
00:05:35,80 --> 00:05:37,40
I'm going to hit Enter,

157
00:05:37,40 --> 00:05:40,10
and so what this is doing is this is going and getting

158
00:05:40,10 --> 00:05:41,50
all the code from that repo,

159
00:05:41,50 --> 00:05:43,10
you could see it just brought it all down,

160
00:05:43,10 --> 00:05:44,80
and now, if we go back to our workspace,

161
00:05:44,80 --> 00:05:46,70
I'm going to open up File Explorer,

162
00:05:46,70 --> 00:05:49,20
and go into Documents and go into my workspace,

163
00:05:49,20 --> 00:05:50,90
and this is where the binaries will be kept

164
00:05:50,90 --> 00:05:52,50
after you compile your program,

165
00:05:52,50 --> 00:05:54,20
it'll put he executables right there,

166
00:05:54,20 --> 00:05:55,80
but if I go in here, the source,

167
00:05:55,80 --> 00:05:58,60
this is where we keep all of our source code that we write.

168
00:05:58,60 --> 00:06:01,50
There is now this fantastic, great,

169
00:06:01,50 --> 00:06:03,80
amazing name spacing convention

170
00:06:03,80 --> 00:06:05,40
for keeping different packages,

171
00:06:05,40 --> 00:06:08,70
and you have all of the code for this course downloaded

172
00:06:08,70 --> 00:06:10,00
to your machine.

173
00:06:10,00 --> 00:06:11,70
All right, so that's how it all works,

174
00:06:11,70 --> 00:06:13,90
that's how you set up Windows to do Go programming

175
00:06:13,90 --> 00:06:15,40
and get Go installed,

176
00:06:15,40 --> 00:06:17,40
and I'm going to give you a couple of suggestions

177
00:06:17,40 --> 00:06:19,40
for troubleshooting techniques on Windows.

178
00:06:19,40 --> 00:06:22,20
If the first time you get everything installed

179
00:06:22,20 --> 00:06:24,20
and you try to run go version,

180
00:06:24,20 --> 00:06:26,30
or go get, or you try to do that

181
00:06:26,30 --> 00:06:28,10
and you don't see anything working,

182
00:06:28,10 --> 00:06:30,30
restart your machine, just restart it.

183
00:06:30,30 --> 00:06:31,60
That should fix everything for you

184
00:06:31,60 --> 00:06:33,90
'cause that will refresh all your Environment Variables

185
00:06:33,90 --> 00:06:36,70
and bring up a fresh shell, and they'll all be registered

186
00:06:36,70 --> 00:06:37,50
and then it should work,

187
00:06:37,50 --> 00:06:39,90
so that's something that some people get stuck on sometimes,

188
00:06:39,90 --> 00:06:42,90
they get it all installed, and then go version doesn't work.

189
00:06:42,90 --> 00:06:44,40
Just try restarting your machine

190
00:06:44,40 --> 00:06:47,10
and that should reset everything and make it work fine.

191
00:06:47,10 --> 00:06:51,00
All right, so that's getting Go up and running on Windows.

