1
00:00:00,40 --> 00:00:02,80
- If you really want to get the most out of Code Clinic,

2
00:00:02,80 --> 00:00:04,70
try to create your own solution to the problem

3
00:00:04,70 --> 00:00:06,90
before you watch how I've done it.

4
00:00:06,90 --> 00:00:09,20
If you've spent time working on the question,

5
00:00:09,20 --> 00:00:12,00
you'll have a better idea of some of the difficult paths

6
00:00:12,00 --> 00:00:13,40
and have a better understanding

7
00:00:13,40 --> 00:00:16,10
of why I've made certain choices.

8
00:00:16,10 --> 00:00:18,20
I've included the source code from my solution

9
00:00:18,20 --> 00:00:19,60
in the exercise files.

10
00:00:19,60 --> 00:00:21,50
I'm assuming you've already got the necessary

11
00:00:21,50 --> 00:00:24,90
tools installed to be able to edit and run the code.

12
00:00:24,90 --> 00:00:27,30
If not, I'd recommend you take a look at

13
00:00:27,30 --> 00:00:28,90
Up and Running with Go.

14
00:00:28,90 --> 00:00:31,30
Code Clinic is a unique opportunity

15
00:00:31,30 --> 00:00:33,60
to look over my shoulder as I actually solve

16
00:00:33,60 --> 00:00:35,10
coding problems.

17
00:00:35,10 --> 00:00:37,20
Keep in mind that my solution is not

18
00:00:37,20 --> 00:00:39,10
the only correct solution.

19
00:00:39,10 --> 00:00:41,90
There are always a multitude of ways to solve a problem.

20
00:00:41,90 --> 00:00:44,40
But by watching how I approach the task,

21
00:00:44,40 --> 00:00:46,20
I hope you will learn how to become

22
00:00:46,20 --> 00:00:48,00
a better programmer yourself.

