1
00:00:00,50 --> 00:00:02,50
- [Voiceover] Since Go is a relatively new

2
00:00:02,50 --> 00:00:04,50
programming language, I thought I'd take a moment

3
00:00:04,50 --> 00:00:06,50
to talk about why you might want to choose Go

4
00:00:06,50 --> 00:00:09,10
as a language in which to program.

5
00:00:09,10 --> 00:00:12,00
For me, when I was looking at whether or not to use Go,

6
00:00:12,00 --> 00:00:14,40
one of the strongest arguments which stood out to me

7
00:00:14,40 --> 00:00:17,70
is that Go has amazing credentials.

8
00:00:17,70 --> 00:00:19,10
Go was created at Google,

9
00:00:19,10 --> 00:00:22,60
which to me is one of the greatest, if not the greatest,

10
00:00:22,60 --> 00:00:25,40
software engineering firm to have ever existed.

11
00:00:25,40 --> 00:00:29,10
It was also created by some luminaries in computer science.

12
00:00:29,10 --> 00:00:31,00
So Google was hitting a wall

13
00:00:31,00 --> 00:00:34,10
with all current modern programming languages.

14
00:00:34,10 --> 00:00:36,00
What Google wanted to do,

15
00:00:36,00 --> 00:00:37,80
there were no programming languages out there

16
00:00:37,80 --> 00:00:40,50
which could do it, and so Google created

17
00:00:40,50 --> 00:00:43,20
a new programming language, and that language is Go,

18
00:00:43,20 --> 00:00:46,30
so that they could do the things they wanted to accomplish.

19
00:00:46,30 --> 00:00:48,30
You can learn about Google's reasons

20
00:00:48,30 --> 00:00:50,60
for creating the Go programming language

21
00:00:50,60 --> 00:00:54,10
by going to golang.org, and then once you're here,

22
00:00:54,10 --> 00:00:56,40
click on Documents, and then inside Documents

23
00:00:56,40 --> 00:01:00,50
there's a Frequently Asked Questions.

24
00:01:00,50 --> 00:01:02,10
When you click on Frequently Asked Questions,

25
00:01:02,10 --> 00:01:04,70
you'll find all of these right here.

26
00:01:04,70 --> 00:01:06,00
And so these questions right here,

27
00:01:06,00 --> 00:01:07,90
these are the main things here which talk about

28
00:01:07,90 --> 00:01:10,70
why Google created a new programming language.

29
00:01:10,70 --> 00:01:12,80
So why are you creating a new language?

30
00:01:12,80 --> 00:01:14,40
What is the purpose of the project?

31
00:01:14,40 --> 00:01:16,20
What is the status of the project?

32
00:01:16,20 --> 00:01:19,20
And what are the guiding principles in the design?

33
00:01:19,20 --> 00:01:22,00
So one thing which is interesting right here

34
00:01:22,00 --> 00:01:25,30
is looking at the dates

35
00:01:25,30 --> 00:01:27,70
for when Go was created and released.

36
00:01:27,70 --> 00:01:30,40
So Go started in 2007, it was released

37
00:01:30,40 --> 00:01:33,40
as an open source project in 2009,

38
00:01:33,40 --> 00:01:36,50
and then version one was released in 2012.

39
00:01:36,50 --> 00:01:39,00
So Go is a really young language.

40
00:01:39,00 --> 00:01:41,00
It's only four years old

41
00:01:41,00 --> 00:01:43,40
at the time at which I'm recording this video.

42
00:01:43,40 --> 00:01:46,40
If we compare that to other languages,

43
00:01:46,40 --> 00:01:48,80
C is roughly 43 years old,

44
00:01:48,80 --> 00:01:52,10
C++ is about 32 years old,

45
00:01:52,10 --> 00:01:55,80
Ruby is 20 years old, and JavaScript is also 20 years old.

46
00:01:55,80 --> 00:01:59,30
So Go is only four years old, but again, like I said,

47
00:01:59,30 --> 00:02:02,30
the main reason for me, the most influential reason

48
00:02:02,30 --> 00:02:04,20
that influenced me to choose Go,

49
00:02:04,20 --> 00:02:06,50
is that Go was created at Google

50
00:02:06,50 --> 00:02:09,00
by these luminaries in computer science.

51
00:02:09,00 --> 00:02:12,30
So to create Go, Google enlisted the help of

52
00:02:12,30 --> 00:02:15,40
Ken Thompson, Rob Pike, and Robert Griesemer,

53
00:02:15,40 --> 00:02:16,80
and these are some of the same individuals

54
00:02:16,80 --> 00:02:19,40
who helped create the C programming language,

55
00:02:19,40 --> 00:02:21,60
UNIX, and UTF-8.

56
00:02:21,60 --> 00:02:23,50
That really made me stop and take notice.

57
00:02:23,50 --> 00:02:26,10
I was like, "Woah, the same people who created C,

58
00:02:26,10 --> 00:02:28,60
"helped create C, UNIX, and UTF-8

59
00:02:28,60 --> 00:02:31,00
"have now created a new programming language?"

60
00:02:31,00 --> 00:02:33,40
That was a programming language I wanted to know about.

61
00:02:33,40 --> 00:02:34,90
And then as I learned more about Go,

62
00:02:34,90 --> 00:02:36,20
I really liked what I learned.

63
00:02:36,20 --> 00:02:38,00
It's performant, it really takes advantage

64
00:02:38,00 --> 00:02:41,20
of multiple cores, it does concurrency natively,

65
00:02:41,20 --> 00:02:43,80
it's compiled, it works in a network-distributed

66
00:02:43,80 --> 00:02:47,00
environment, has clean syntax,

67
00:02:47,00 --> 00:02:48,70
it's a powerful standard library,

68
00:02:48,70 --> 00:02:49,80
so it's a low-level language

69
00:02:49,80 --> 00:02:52,30
with a really powerful standard library,

70
00:02:52,30 --> 00:02:54,70
it's garbage collected, it's portable

71
00:02:54,70 --> 00:02:57,60
in that it will compile on many different operating systems,

72
00:02:57,60 --> 00:03:00,00
too many different operating systems.

73
00:03:00,00 --> 00:03:02,80
Of course it's backed by Google, and it's open source.

74
00:03:02,80 --> 00:03:05,60
So all those reasons made me very interested in Go,

75
00:03:05,60 --> 00:03:06,50
and those are the reasons

76
00:03:06,50 --> 00:03:08,80
that ultimately led me to choosing it.

77
00:03:08,80 --> 00:03:10,40
Another question which I asked myself

78
00:03:10,40 --> 00:03:13,20
when looking at Go was, are there any other people

79
00:03:13,20 --> 00:03:15,00
out there using the language?

80
00:03:15,00 --> 00:03:18,70
And you could go to Google, and you could search for

81
00:03:18,70 --> 00:03:21,80
companies using Go, and one of the links which will come up

82
00:03:21,80 --> 00:03:24,60
is a link to this repo right here:

83
00:03:24,60 --> 00:03:28,50
github.com/golang/go/wiki/GoUsers.

84
00:03:28,50 --> 00:03:32,90
And at this repo, you can see many of the different names

85
00:03:32,90 --> 00:03:35,00
of the companies using Go,

86
00:03:35,00 --> 00:03:37,10
and there's some really big names on the list.

87
00:03:37,10 --> 00:03:40,00
You can take a look at that list on your own time.

88
00:03:40,00 --> 00:03:42,70
So when I was looking at whether or not I wanted to use Go,

89
00:03:42,70 --> 00:03:45,60
one question I had for it was, is it any good?

90
00:03:45,60 --> 00:03:48,40
And just looking at its credentials, for me the answer was,

91
00:03:48,40 --> 00:03:51,40
it's got amazing credentials, I'm definitely interested,

92
00:03:51,40 --> 00:03:52,90
and as I've learned more about it,

93
00:03:52,90 --> 00:03:54,50
I've been very impressed by it.

94
00:03:54,50 --> 00:03:57,50
The other question I had is, are other people using Go?

95
00:03:57,50 --> 00:04:00,20
And when I saw the different people who are using Go,

96
00:04:00,20 --> 00:04:02,70
of course that list has grown since I started,

97
00:04:02,70 --> 00:04:05,90
I was also very impressed by all the different companies

98
00:04:05,90 --> 00:04:07,40
that were already using Go.

99
00:04:07,40 --> 00:04:09,50
And the third one was, would there be any jobs out there

100
00:04:09,50 --> 00:04:10,80
for my students?

101
00:04:10,80 --> 00:04:13,00
And I'm happy to say that it's been really rewarding

102
00:04:13,00 --> 00:04:14,50
for me to see some of my students

103
00:04:14,50 --> 00:04:17,30
who haven't been studying Go for very long,

104
00:04:17,30 --> 00:04:19,40
having companies inquire with them,

105
00:04:19,40 --> 00:04:21,50
"Hey we saw your code on github,

106
00:04:21,50 --> 00:04:23,20
"and we like that you're into Go.

107
00:04:23,20 --> 00:04:24,50
"We'd like to talk with you."

108
00:04:24,50 --> 00:04:26,60
So those are some of the reasons I looked at

109
00:04:26,60 --> 00:04:27,30
when choosing Go.

110
00:04:27,30 --> 00:04:29,00
I hope those reasons are helpful to you

111
00:04:29,00 --> 00:04:31,20
in making your own decision about whether or not

112
00:04:31,20 --> 00:04:33,00
Go is something you want to invest in more deeply.

