1
00:00:00,50 --> 00:00:02,40
- What should you know before taking this course?

2
00:00:02,40 --> 00:00:05,20
So, this course is designed for everyone.

3
00:00:05,20 --> 00:00:06,30
From beginners to experts,

4
00:00:06,30 --> 00:00:07,40
I've structured this course

5
00:00:07,40 --> 00:00:09,50
so that everyone will gain value from it.

6
00:00:09,50 --> 00:00:12,20
My helpful resources video provides resources

7
00:00:12,20 --> 00:00:13,80
that every good developer needs.

8
00:00:13,80 --> 00:00:15,70
I'm going to give you a whole bunch of resources,

9
00:00:15,70 --> 00:00:17,70
which as you're learning about Go

10
00:00:17,70 --> 00:00:19,10
and maybe getting into the language

11
00:00:19,10 --> 00:00:21,00
and learning the language, those resources,

12
00:00:21,00 --> 00:00:23,40
regardless of whether or not you're a beginner or an expert,

13
00:00:23,40 --> 00:00:25,60
those are going to help you learn the language

14
00:00:25,60 --> 00:00:27,00
and learn it well.

15
00:00:27,00 --> 00:00:30,20
Because having the right resources is super important.

16
00:00:30,20 --> 00:00:34,10
You need to know where can I look to find good information

17
00:00:34,10 --> 00:00:36,00
and not waste my time, right?

18
00:00:36,00 --> 00:00:37,10
I want good information.

19
00:00:37,10 --> 00:00:38,20
I don't want to waste my time.

20
00:00:38,20 --> 00:00:39,60
Where can I look to get those resources?

21
00:00:39,60 --> 00:00:42,00
So, I'm going to give you a list of the resources

22
00:00:42,00 --> 00:00:44,80
which I have found the most valuable in learning

23
00:00:44,80 --> 00:00:47,00
the Go programming language.

24
00:00:47,00 --> 00:00:48,50
The second thing, the second reason this course

25
00:00:48,50 --> 00:00:51,10
will be great for people who are either beginners or experts

26
00:00:51,10 --> 00:00:54,60
or for everyone is that my codebase of solutions

27
00:00:54,60 --> 00:00:57,60
will provide you with a pathway for learning the language

28
00:00:57,60 --> 00:01:00,60
and for also accomplishing common tasks.

29
00:01:00,60 --> 00:01:03,80
Go can be a little bit enigmatic when you first approach it,

30
00:01:03,80 --> 00:01:06,20
particularly if you're new to programming.

31
00:01:06,20 --> 00:01:08,30
It can be a little bit difficult sometimes to figure out

32
00:01:08,30 --> 00:01:10,70
how do I put different pieces of code together

33
00:01:10,70 --> 00:01:12,00
to get them to work.

34
00:01:12,00 --> 00:01:14,60
If you have examples that show you how

35
00:01:14,60 --> 00:01:15,80
to put that code together,

36
00:01:15,80 --> 00:01:18,00
it's much easier to understand it,

37
00:01:18,00 --> 00:01:19,80
and so that's another one of the great things

38
00:01:19,80 --> 00:01:21,20
you'll get from this course.

39
00:01:21,20 --> 00:01:22,90
So, whether you're a beginner or an expert,

40
00:01:22,90 --> 00:01:25,90
that's going to help you out as you're taking this course.

41
00:01:25,90 --> 00:01:27,10
It was one of my goals

42
00:01:27,10 --> 00:01:29,00
when I was putting this course together

43
00:01:29,00 --> 00:01:31,10
to try to make it as accessible

44
00:01:31,10 --> 00:01:33,50
to as many people as possible.

45
00:01:33,50 --> 00:01:37,30
I'm going to step into a little bit of detail sometimes

46
00:01:37,30 --> 00:01:38,60
to explain something,

47
00:01:38,60 --> 00:01:40,20
which might be a little bit rudimentary

48
00:01:40,20 --> 00:01:41,50
to a seasoned developer,

49
00:01:41,50 --> 00:01:43,30
but I want to make sure that if there are people

50
00:01:43,30 --> 00:01:45,30
who are just learning the language out there

51
00:01:45,30 --> 00:01:47,10
that they can understand some of the concepts

52
00:01:47,10 --> 00:01:49,10
that I'm going to share in this course.

53
00:01:49,10 --> 00:01:50,80
If you are new to the language,

54
00:01:50,80 --> 00:01:52,50
from my own experience,

55
00:01:52,50 --> 00:01:53,90
and from my empathy with people

56
00:01:53,90 --> 00:01:56,80
who are trying to learn how to program by watching videos,

57
00:01:56,80 --> 00:01:58,90
I've been there, I know what it's like,

58
00:01:58,90 --> 00:02:00,80
and I've built this course in such a way

59
00:02:00,80 --> 00:02:02,90
that I'm going to try to bring you along,

60
00:02:02,90 --> 00:02:04,80
even if you're just starting out with programming

61
00:02:04,80 --> 00:02:06,70
so you can really understand all the different examples

62
00:02:06,70 --> 00:02:08,00
I'm going to share with you.

63
00:02:08,00 --> 00:02:09,90
I'm excited to show you the different solutions

64
00:02:09,90 --> 00:02:11,20
I have for Code Clinic

65
00:02:11,20 --> 00:02:13,40
and also to introduce you to what I think

66
00:02:13,40 --> 00:02:15,90
is one of the most amazing if not the most amazing

67
00:02:15,90 --> 00:02:19,00
language out there today with which you can program.

