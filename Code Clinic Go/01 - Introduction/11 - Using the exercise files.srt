1
00:00:00,60 --> 00:00:02,70
- The exercise files for this course are included

2
00:00:02,70 --> 00:00:04,10
with your membership.

3
00:00:04,10 --> 00:00:06,60
They're organized into folders for each problem.

4
00:00:06,60 --> 00:00:09,30
Inside each problem folder there are a number

5
00:00:09,30 --> 00:00:12,10
of other folders representing steps along the way

6
00:00:12,10 --> 00:00:14,30
towards solving each problem.

7
00:00:14,30 --> 00:00:16,20
I've also included some additional information

8
00:00:16,20 --> 00:00:18,40
about my thought processes and some references

9
00:00:18,40 --> 00:00:20,50
to other resources that might help you further

10
00:00:20,50 --> 00:00:23,40
your exploration of the Go language.

11
00:00:23,40 --> 00:00:27,20
If you're new to Go I have also included this folder

12
00:00:27,20 --> 00:00:29,50
right here which includes the code I use to teach

13
00:00:29,50 --> 00:00:31,40
the fundamentals of Go.

14
00:00:31,40 --> 00:00:33,40
During the course I'll use a package management

15
00:00:33,40 --> 00:00:36,20
feature of Go, the Go Get Command to pull

16
00:00:36,20 --> 00:00:39,00
in all of these files form my GitHub repository

17
00:00:39,00 --> 00:00:41,20
and I recommend that you do the same.

18
00:00:41,20 --> 00:00:43,30
Go works very well with GitHub and I've set

19
00:00:43,30 --> 00:00:45,70
up the structure of our project to reflect that.

20
00:00:45,70 --> 00:00:48,00
But if you prefer, you can follow along with the files

21
00:00:48,00 --> 00:00:50,00
you download from the course page instead.

