1
00:00:00,20 --> 00:00:02,20
- The other important thing for us to know

2
00:00:02,20 --> 00:00:04,10
is how to set up an editor,

3
00:00:04,10 --> 00:00:07,60
or an Integrated Development Environment, an IDE,

4
00:00:07,60 --> 00:00:10,70
so that we can more easily write Go code.

5
00:00:10,70 --> 00:00:12,60
There's a couple of editors that are out there

6
00:00:12,60 --> 00:00:16,00
that work really well and really easily with Go.

7
00:00:16,00 --> 00:00:18,20
My favorite is WebStorm.

8
00:00:18,20 --> 00:00:20,40
WebStorm is built by JetBrains,

9
00:00:20,40 --> 00:00:23,70
and JetBrains also builds IntelliJ,

10
00:00:23,70 --> 00:00:26,40
and IntelliJ is that base editor

11
00:00:26,40 --> 00:00:29,20
which was used to create Android Studio.

12
00:00:29,20 --> 00:00:31,70
You can use WebStorm to write Go Code,

13
00:00:31,70 --> 00:00:35,00
you can also use Atom.io to write Go Code.

14
00:00:35,00 --> 00:00:37,50
Atom is a great editor written

15
00:00:37,50 --> 00:00:40,40
by the same people who created GitHub.

16
00:00:40,40 --> 00:00:43,60
You can also use Sublime to write Go Code.

17
00:00:43,60 --> 00:00:44,90
So the first thing I want to show you

18
00:00:44,90 --> 00:00:46,90
for all three of those editors

19
00:00:46,90 --> 00:00:48,80
is which plugins you should grab

20
00:00:48,80 --> 00:00:51,70
so that your editor works well with Go.

21
00:00:51,70 --> 00:00:53,00
If you're using WebStorm,

22
00:00:53,00 --> 00:00:55,90
you want to Google webstorm golang plugin,

23
00:00:55,90 --> 00:00:57,70
and then come right here to support plugin,

24
00:00:57,70 --> 00:00:59,80
and I'm going to show you more about this in a second.

25
00:00:59,80 --> 00:01:01,50
If you're using Atom,

26
00:01:01,50 --> 00:01:04,50
you want to go atom golang plugin and look for go-plus,

27
00:01:04,50 --> 00:01:07,10
and then that'll be the plugin you use with Atom.

28
00:01:07,10 --> 00:01:08,90
If you're using Sublime,

29
00:01:08,90 --> 00:01:11,80
there's something called DisposaBoy/GoSublime.

30
00:01:11,80 --> 00:01:14,00
But I just searched for sublime golang

31
00:01:14,00 --> 00:01:16,90
and then that brought me to DisposaBoy/GoSublime.

32
00:01:16,90 --> 00:01:18,20
And so that's the plugin you'll use

33
00:01:18,20 --> 00:01:20,40
to write Go Code in Sublime.

34
00:01:20,40 --> 00:01:22,70
So now let's come back to WebStorm,

35
00:01:22,70 --> 00:01:24,60
and I'm going to show you in this video

36
00:01:24,60 --> 00:01:27,50
some details about using WebStorm with Go,

37
00:01:27,50 --> 00:01:29,30
and how to use it effectively.

38
00:01:29,30 --> 00:01:31,30
So the first thing I want you to know is that yes,

39
00:01:31,30 --> 00:01:33,00
WebStorm does cost money.

40
00:01:33,00 --> 00:01:34,40
However, there's a caveat.

41
00:01:34,40 --> 00:01:36,10
If you are a student,

42
00:01:36,10 --> 00:01:38,30
you can write to WebStorm, to JetBrains,

43
00:01:38,30 --> 00:01:39,90
and you can let them know, "I am a student,

44
00:01:39,90 --> 00:01:43,00
"and I'd like to use WebStorm for educational purposes."

45
00:01:43,00 --> 00:01:45,00
This means you are not developing commercially.

46
00:01:45,00 --> 00:01:46,80
It is up to JetBrains to decide

47
00:01:46,80 --> 00:01:49,40
whether or not they're going to give you a student license,

48
00:01:49,40 --> 00:01:51,90
which will allow you to use their products for free.

49
00:01:51,90 --> 00:01:54,10
Just click right here, Apply Now.

50
00:01:54,10 --> 00:01:56,80
I went to Google and searched for webstorm education

51
00:01:56,80 --> 00:01:59,00
and followed the link Free for students.

52
00:01:59,00 --> 00:02:00,50
You can submit your information

53
00:02:00,50 --> 00:02:02,10
and maybe they'll give you a license for free.

54
00:02:02,10 --> 00:02:04,00
Often, I imagine what they're looking for

55
00:02:04,00 --> 00:02:06,20
is a educational email address,

56
00:02:06,20 --> 00:02:08,80
are you actually writing from an educational account.

57
00:02:08,80 --> 00:02:09,70
If you have one,

58
00:02:09,70 --> 00:02:11,90
make sure you contact them using that email.

59
00:02:11,90 --> 00:02:13,20
Alright, the second thing to know

60
00:02:13,20 --> 00:02:15,80
about WebStorm is downloading the plugin.

61
00:02:15,80 --> 00:02:17,80
When I search for webstorm golang plugin,

62
00:02:17,80 --> 00:02:19,40
and then click this link,

63
00:02:19,40 --> 00:02:21,50
it's going to take me to a page with plugins.

64
00:02:21,50 --> 00:02:26,10
Notice right here, I've got this one from 11.2015, November.

65
00:02:26,10 --> 00:02:28,60
However, and this is what you want to do,

66
00:02:28,60 --> 00:02:31,90
if you scroll down and click show all updates,

67
00:02:31,90 --> 00:02:33,80
it'll give you a more recent update.

68
00:02:33,80 --> 00:02:36,00
I don't know why this is set up this way,

69
00:02:36,00 --> 00:02:37,70
but I've had trouble before

70
00:02:37,70 --> 00:02:39,10
and some students have had trouble

71
00:02:39,10 --> 00:02:42,30
when they install the one which is at the current top here.

72
00:02:42,30 --> 00:02:45,10
But if we get the most recent one, they don't have trouble.

73
00:02:45,10 --> 00:02:47,00
So download the most recent version.

74
00:02:47,00 --> 00:02:49,10
Once you've downloaded the most recent version,

75
00:02:49,10 --> 00:02:50,40
we're going to go into WebStorm,

76
00:02:50,40 --> 00:02:51,90
and we're going to import that version.

77
00:02:51,90 --> 00:02:53,90
So let me show you how to do that.

78
00:02:53,90 --> 00:02:56,70
I start WebStorm, and this is the first screen I see.

79
00:02:56,70 --> 00:02:58,30
I click on Configure down here,

80
00:02:58,30 --> 00:03:00,60
and there's a link right there for Plugins.

81
00:03:00,60 --> 00:03:02,40
So when you click Plugins,

82
00:03:02,40 --> 00:03:03,50
you'll notice that I have a list

83
00:03:03,50 --> 00:03:06,20
of all the plugins which run in WebStorm.

84
00:03:06,20 --> 00:03:07,90
You will also notice that a lot

85
00:03:07,90 --> 00:03:10,20
of these plugins are unchecked.

86
00:03:10,20 --> 00:03:12,30
So if you're not going to use one of these plugins,

87
00:03:12,30 --> 00:03:15,10
which comes pre-built, you could uncheck it,

88
00:03:15,10 --> 00:03:17,00
and then this is going to limit the amount

89
00:03:17,00 --> 00:03:19,60
of resources WebStorm needs to run.

90
00:03:19,60 --> 00:03:22,50
So it's going to make WebStorm less resource-intensive,

91
00:03:22,50 --> 00:03:24,90
and it's going to help WebStorm run quicker.

92
00:03:24,90 --> 00:03:27,90
So uncheck all of the different plugins you don't need.

93
00:03:27,90 --> 00:03:29,30
Once you have done that,

94
00:03:29,30 --> 00:03:31,80
and first let me just show you quickly the ones I do,

95
00:03:31,80 --> 00:03:33,60
I just keep those checked,

96
00:03:33,60 --> 00:03:35,20
and then those checked,

97
00:03:35,20 --> 00:03:37,60
and those checked, so those are the ones I have checked.

98
00:03:37,60 --> 00:03:39,00
The next thing you do is you choose

99
00:03:39,00 --> 00:03:40,70
install plugin from desk.

100
00:03:40,70 --> 00:03:42,70
You're going to go to your Downloads folder

101
00:03:42,70 --> 00:03:45,10
and look for that zip file you downloaded

102
00:03:45,10 --> 00:03:47,00
from this page right here.

103
00:03:47,00 --> 00:03:49,70
So when I download that, it's going to give me a zip file.

104
00:03:49,70 --> 00:03:52,70
Let me just show you that, so right there.

105
00:03:52,70 --> 00:03:54,90
Do not open this zip file yourself.

106
00:03:54,90 --> 00:03:56,50
Some people run into the problem,

107
00:03:56,50 --> 00:03:59,50
they open it and then they try to import that plugin.

108
00:03:59,50 --> 00:04:01,30
You do not want to open that zip file.

109
00:04:01,30 --> 00:04:02,90
Let WebStorm open it for you.

110
00:04:02,90 --> 00:04:05,30
Just choose the zip file and click OK.

111
00:04:05,30 --> 00:04:07,60
I've already done it, and when you do it,

112
00:04:07,60 --> 00:04:09,80
you'll have Go right there.

113
00:04:09,80 --> 00:04:11,20
So now you're ready to do Go.

114
00:04:11,20 --> 00:04:13,00
It'll ask you to restart WebStorm.

115
00:04:13,00 --> 00:04:14,90
That's fine, just restart WebStorm.

116
00:04:14,90 --> 00:04:17,10
The next thing I really like to do with WebStorm,

117
00:04:17,10 --> 00:04:19,00
is I'm going to change the theme.

118
00:04:19,00 --> 00:04:20,60
And to see how to change the theme,

119
00:04:20,60 --> 00:04:22,10
first I'm going to open a project.

120
00:04:22,10 --> 00:04:24,30
So I click Open,

121
00:04:24,30 --> 00:04:26,50
and then I'm going to go to my Go workspace,

122
00:04:26,50 --> 00:04:28,90
and so that's inside Documents and I called it gowork,

123
00:04:28,90 --> 00:04:31,80
and then go into src, and then my name space,

124
00:04:31,80 --> 00:04:34,40
for all of my code, and it's GoesToEleven.

125
00:04:34,40 --> 00:04:36,60
I have a folder here which I've brought down, lynda,

126
00:04:36,60 --> 00:04:39,50
and I'm just going to open my CC folder for Code Clinic

127
00:04:39,50 --> 00:04:40,80
and click OK.

128
00:04:40,80 --> 00:04:44,30
So now that is opening, I have all those ready to run,

129
00:04:44,30 --> 00:04:46,00
and it's telling me over here that I have

130
00:04:46,00 --> 00:04:48,70
an unregistered VCS root directory.

131
00:04:48,70 --> 00:04:50,20
I'm going to choose Add root,

132
00:04:50,20 --> 00:04:51,80
and this will allow WebStorm to connect

133
00:04:51,80 --> 00:04:53,90
with GitHub really easily for me,

134
00:04:53,90 --> 00:04:56,60
and I can do all of my Git pushes and commits

135
00:04:56,60 --> 00:04:58,70
right here, committing my changes.

136
00:04:58,70 --> 00:05:00,70
So with the click of a button, I can do that.

137
00:05:00,70 --> 00:05:02,20
So that's kind of nice.

138
00:05:02,20 --> 00:05:04,90
The next thing is, when you first open a file,

139
00:05:04,90 --> 00:05:06,70
and we'll see if this sets,

140
00:05:06,70 --> 00:05:08,70
it's gonna ask you to set up your SDK.

141
00:05:08,70 --> 00:05:10,50
So again, this could be a little bit of a hurdle

142
00:05:10,50 --> 00:05:12,40
for some students when they're home working.

143
00:05:12,40 --> 00:05:14,50
If I haven't shown this to them, they're like,

144
00:05:14,50 --> 00:05:15,90
"Well, how do I do this?

145
00:05:15,90 --> 00:05:17,50
"It took me a half hour to research all that."

146
00:05:17,50 --> 00:05:20,30
You just click this link, then click these buttons.

147
00:05:20,30 --> 00:05:22,80
It takes you to the SDK where it's installed.

148
00:05:22,80 --> 00:05:23,90
You click OK, because that's

149
00:05:23,90 --> 00:05:26,20
where Go got installed right there.

150
00:05:26,20 --> 00:05:30,40
You click OK, you hit OK, and now that's all set up.

151
00:05:30,40 --> 00:05:32,40
We've detected some libraries from your GOPATH.

152
00:05:32,40 --> 00:05:33,80
You may want to add extra libraries.

153
00:05:33,80 --> 00:05:35,70
That's fine, you can just close out that message.

154
00:05:35,70 --> 00:05:37,30
That's nothing to worry about.

155
00:05:37,30 --> 00:05:40,40
So the next thing I want to do here inside of WebStorm,

156
00:05:40,40 --> 00:05:42,00
is, I like changing the theme.

157
00:05:42,00 --> 00:05:43,50
The default theme that comes with this,

158
00:05:43,50 --> 00:05:45,00
I don't really care for the colors.

159
00:05:45,00 --> 00:05:48,10
So you could do that by again, going to the amazing web,

160
00:05:48,10 --> 00:05:50,90
and searching for webstorm color theme.

161
00:05:50,90 --> 00:05:54,40
Click on Color Themes, and when you come here,

162
00:05:54,40 --> 00:05:56,40
they present a bunch of different themes

163
00:05:56,40 --> 00:05:58,60
that you could import into WebStorm.

164
00:05:58,60 --> 00:06:00,30
Alright, the one I like is right here.

165
00:06:00,30 --> 00:06:02,80
Relax Your Eyes, that's awesome.

166
00:06:02,80 --> 00:06:04,70
So I'm just going to download that,

167
00:06:04,70 --> 00:06:06,00
and again, when I download that,

168
00:06:06,00 --> 00:06:08,10
it's going to give me this file right here,

169
00:06:08,10 --> 00:06:10,00
so get rid of that one, I don't need that one.

170
00:06:10,00 --> 00:06:10,90
That was an extra.

171
00:06:10,90 --> 00:06:12,70
But Relax Your Eyes.jar,

172
00:06:12,70 --> 00:06:15,30
again, don't open this, but let WebStorm open it.

173
00:06:15,30 --> 00:06:18,20
So you're going to go to File, and then Import Settings,

174
00:06:18,20 --> 00:06:20,00
and you're going to go find that file.

175
00:06:20,00 --> 00:06:22,70
So for me it was in Downloads right there.

176
00:06:22,70 --> 00:06:25,30
And then just choose that file and click OK.

177
00:06:25,30 --> 00:06:26,80
There are a few more Setting tweaks

178
00:06:26,80 --> 00:06:28,30
I've already made to my file.

179
00:06:28,30 --> 00:06:30,10
If you look at my code and wonder

180
00:06:30,10 --> 00:06:32,00
why your code doesn't look like mine,

181
00:06:32,00 --> 00:06:34,60
I've made a few more tweaks inside Preferences.

182
00:06:34,60 --> 00:06:36,60
And Preferences is really cool.

183
00:06:36,60 --> 00:06:39,60
I could come in here, and I could search for for like, wrap,

184
00:06:39,60 --> 00:06:40,90
if I want to change line wrapping,

185
00:06:40,90 --> 00:06:43,20
which is one of the things I changed.

186
00:06:43,20 --> 00:06:45,60
So it gives me all the options for wrapping here,

187
00:06:45,60 --> 00:06:47,60
so I could look under General, and I could see,

188
00:06:47,60 --> 00:06:49,50
okay, how do I want things to wrap,

189
00:06:49,50 --> 00:06:52,10
or here in General, Use soft wraps in editor.

190
00:06:52,10 --> 00:06:54,70
I set that setting right there and I took this one off.

191
00:06:54,70 --> 00:06:56,30
You can also come in here

192
00:06:56,30 --> 00:06:58,80
and change the font size and also the font.

193
00:06:58,80 --> 00:07:02,70
So if I search for font, I can see here under Font.

194
00:07:02,70 --> 00:07:05,50
I changed that to a custom font, right there,

195
00:07:05,50 --> 00:07:07,70
and changed the size to 20.

196
00:07:07,70 --> 00:07:10,20
So those are some of the changes I've made on my editor.

197
00:07:10,20 --> 00:07:11,80
Alright, so that's it.

198
00:07:11,80 --> 00:07:15,60
We've got WebStorm up and running, we have our project open,

199
00:07:15,60 --> 00:07:18,00
you've really learned some amazing things so far

200
00:07:18,00 --> 00:07:20,30
to work with Go, setting up your work environment,

201
00:07:20,30 --> 00:07:23,00
getting your path environment variables set correctly,

202
00:07:23,00 --> 00:07:25,60
creating your workspace, getting an editor,

203
00:07:25,60 --> 00:07:28,40
setting a plugin in the editor to work with Go,

204
00:07:28,40 --> 00:07:31,30
and then opening a file of code so it could be worked with.

205
00:07:31,30 --> 00:07:33,20
You're up and running and ready

206
00:07:33,20 --> 00:07:35,00
to start writing some Go code.

