1
00:00:00,60 --> 00:00:02,80
- Hello and welcome to Code Clinic for Go.

2
00:00:02,80 --> 00:00:04,30
My name is Todd McLeod.

3
00:00:04,30 --> 00:00:06,20
I'm really looking forward to showing you

4
00:00:06,20 --> 00:00:08,30
my solutions for the challenges proposed

5
00:00:08,30 --> 00:00:10,30
to the Code Clinic authors.

6
00:00:10,30 --> 00:00:11,30
In this series,

7
00:00:11,30 --> 00:00:13,90
we'll explore creative and efficient ways to use Go

8
00:00:13,90 --> 00:00:16,20
to solve a range of problems.

9
00:00:16,20 --> 00:00:17,90
I'll describe some unique aspects

10
00:00:17,90 --> 00:00:19,20
of working with Go,

11
00:00:19,20 --> 00:00:22,40
and offer suggestions on how to improve your code.

12
00:00:22,40 --> 00:00:23,70
If you're new to programming,

13
00:00:23,70 --> 00:00:25,50
or if you're a seasoned programmer,

14
00:00:25,50 --> 00:00:28,10
this series of videos will be beneficial to you,

15
00:00:28,10 --> 00:00:29,80
showing you how to effectively use

16
00:00:29,80 --> 00:00:33,10
the Go programming language in real world solutions.

17
00:00:33,10 --> 00:00:34,70
After watching each solution,

18
00:00:34,70 --> 00:00:36,00
you'll come away with patterns

19
00:00:36,00 --> 00:00:37,70
for writing Go programs

20
00:00:37,70 --> 00:00:38,90
as well as reusable code

21
00:00:38,90 --> 00:00:42,20
that you can use in your own projects.

22
00:00:42,20 --> 00:00:43,80
If you search our library for Code Clinic,

23
00:00:43,80 --> 00:00:46,00
you'll find additional courses by different authors

24
00:00:46,00 --> 00:00:47,80
using other languages.

25
00:00:47,80 --> 00:00:49,30
Each Code Clinic author

26
00:00:49,30 --> 00:00:51,40
solves the exact same set of problems,

27
00:00:51,40 --> 00:00:53,40
so you can compare different solutions

28
00:00:53,40 --> 00:00:56,30
as well as the pros and cons of each language.

29
00:00:56,30 --> 00:00:58,30
If you're wanting to learn to program,

30
00:00:58,30 --> 00:01:00,90
if you're looking for tips on better coding,

31
00:01:00,90 --> 00:01:02,50
or if you're trying to decide

32
00:01:02,50 --> 00:01:05,00
on your next new programming language,

33
00:01:05,00 --> 00:01:07,20
you're gonna love Code Clinic.

34
00:01:07,20 --> 00:01:11,00
So let's get started with Code Clinic for Go.

