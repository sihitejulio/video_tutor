1
00:00:00,50 --> 00:00:01,50
- So the first thing I want to do

2
00:00:01,50 --> 00:00:02,90
is I want to give you some resources

3
00:00:02,90 --> 00:00:05,50
to help you learn the Go programming language.

4
00:00:05,50 --> 00:00:07,40
Traditionally, resources are something

5
00:00:07,40 --> 00:00:09,40
that are provided at the end of the course

6
00:00:09,40 --> 00:00:11,40
but I want to provide it at the beginning

7
00:00:11,40 --> 00:00:13,40
so that as you're learning Go

8
00:00:13,40 --> 00:00:16,40
you have these resources upon which you can draw

9
00:00:16,40 --> 00:00:19,50
to further enhance your learning and to gain more knowledge.

10
00:00:19,50 --> 00:00:22,80
So the first resource I want to point out is GitHub.

11
00:00:22,80 --> 00:00:24,90
And if you go to my GitHub account,

12
00:00:24,90 --> 00:00:27,20
I have a bunch of different Repositories here.

13
00:00:27,20 --> 00:00:29,50
One of the Repositories, lynda,

14
00:00:29,50 --> 00:00:32,20
in here you could find a readme file that will have links

15
00:00:32,20 --> 00:00:34,00
to all the resources I'm going to show you,

16
00:00:34,00 --> 00:00:35,20
so you could check out that one.

17
00:00:35,20 --> 00:00:36,80
But the Repository I want to show you right now

18
00:00:36,80 --> 00:00:38,30
is Golang Training

19
00:00:38,30 --> 00:00:40,10
because there's a lot of really great code in here

20
00:00:40,10 --> 00:00:42,20
which you can take a look at to learn the language.

21
00:00:42,20 --> 00:00:46,30
So there's a lot of really good steps in here for

22
00:00:46,30 --> 00:00:49,60
just how does a scope work, how do blank-identifiers work,

23
00:00:49,60 --> 00:00:53,40
all the way up to concurrency which is right here,

24
00:00:53,40 --> 00:00:56,20
go-routines, and parallelism, and what's the difference.

25
00:00:56,20 --> 00:00:58,40
So this is a super valuable resource

26
00:00:58,40 --> 00:00:59,90
and it's additionally valuable,

27
00:00:59,90 --> 00:01:02,00
and not everybody knows this, because you could come in here

28
00:01:02,00 --> 00:01:04,30
and you could search this Repository.

29
00:01:04,30 --> 00:01:08,60
So if I'm in this Repository and I search right there,

30
00:01:08,60 --> 00:01:12,70
I could see all of the instances in that repo and my code

31
00:01:12,70 --> 00:01:14,80
where I've mentioned os.Args.

32
00:01:14,80 --> 00:01:18,30
And so I've mentioned os.Args there, and here, and here.

33
00:01:18,30 --> 00:01:21,60
So as you are learning the language or using the language,

34
00:01:21,60 --> 00:01:23,50
if you need to see how to do something

35
00:01:23,50 --> 00:01:25,20
you could come to me repo right here

36
00:01:25,20 --> 00:01:26,70
and search this Repository and see,

37
00:01:26,70 --> 00:01:29,10
"Hey, have I used os.Args and how did I use it."

38
00:01:29,10 --> 00:01:31,00
And so you could find that.

39
00:01:31,00 --> 00:01:33,10
The next helpful resource for you to know about

40
00:01:33,10 --> 00:01:36,20
is that there are some Forums for asking questions.

41
00:01:36,20 --> 00:01:37,80
So of course, one of the main Forums

42
00:01:37,80 --> 00:01:39,60
for asking questions today, in programming,

43
00:01:39,60 --> 00:01:41,20
is stackoverflow.

44
00:01:41,20 --> 00:01:43,20
I'm going to show you something about stackoverflow

45
00:01:43,20 --> 00:01:46,30
which not a lot of people know about.

46
00:01:46,30 --> 00:01:49,30
Here I am at stackoverflow and when I search for Go,

47
00:01:49,30 --> 00:01:51,30
and I'm going to type in golang.

48
00:01:51,30 --> 00:01:53,30
And I'm going to take a little bit of a tangent here,

49
00:01:53,30 --> 00:01:55,10
for a moment, to talk about the difference between

50
00:01:55,10 --> 00:01:57,30
Go and golang and then we're going to come back

51
00:01:57,30 --> 00:01:58,90
and look at stackoverflow.

52
00:01:58,90 --> 00:02:02,30
But sometimes Go is referred to as Go,

53
00:02:02,30 --> 00:02:03,40
the programming language,

54
00:02:03,40 --> 00:02:04,70
and so you could see here Try Go

55
00:02:04,70 --> 00:02:06,50
and the Go programming language.

56
00:02:06,50 --> 00:02:09,00
And sometimes it's referred to as golang.

57
00:02:09,00 --> 00:02:11,50
So golang.org is the official website

58
00:02:11,50 --> 00:02:13,30
of the Go programming language.

59
00:02:13,30 --> 00:02:17,10
And often, Go is referred to as golang

60
00:02:17,10 --> 00:02:21,40
because if I was to search for go os or go args

61
00:02:21,40 --> 00:02:24,40
that's going to bring up a lot of results not related

62
00:02:24,40 --> 00:02:26,20
to the Go programming language.

63
00:02:26,20 --> 00:02:29,40
However, if I search for golang that makes it more specific.

64
00:02:29,40 --> 00:02:31,20
And so Go and golang are terms

65
00:02:31,20 --> 00:02:33,30
which are sometimes used interchangeably.

66
00:02:33,30 --> 00:02:35,10
All right, so that's the end of our tangent,

67
00:02:35,10 --> 00:02:36,50
now let's come back to stackoverflow.

68
00:02:36,50 --> 00:02:40,00
I've searched for golang and I've gotten some Go results.

69
00:02:40,00 --> 00:02:41,70
You could see these tags here,

70
00:02:41,70 --> 00:02:44,60
as I hover my mouse over one of the tags,

71
00:02:44,60 --> 00:02:47,70
one of the options in this little flyout menu

72
00:02:47,70 --> 00:02:50,70
is that I could subscribe to this topic.

73
00:02:50,70 --> 00:02:53,30
By subscribing to this topic I'm going to receive an e-mail

74
00:02:53,30 --> 00:02:54,90
and I could adjust my preferences,

75
00:02:54,90 --> 00:02:56,60
how often do I want to receive it.

76
00:02:56,60 --> 00:02:59,10
But I'll receive an e-mail of all the questions

77
00:02:59,10 --> 00:03:02,00
that are occurring around the Go programming language.

78
00:03:02,00 --> 00:03:04,60
This is a great way to help learn about the language

79
00:03:04,60 --> 00:03:06,80
because you'll see what are other people asking

80
00:03:06,80 --> 00:03:09,80
and then how are other people answering those questions.

81
00:03:09,80 --> 00:03:11,20
And so you'll be able to take a look

82
00:03:11,20 --> 00:03:14,90
and also ask questions here if you need help.

83
00:03:14,90 --> 00:03:17,10
Another great forum for asking questions

84
00:03:17,10 --> 00:03:18,70
is the golangbridge.

85
00:03:18,70 --> 00:03:20,20
There's not much more to say about that

86
00:03:20,20 --> 00:03:22,30
other than you should take a look at it, and check it out,

87
00:03:22,30 --> 00:03:25,40
and it's a good place to ask and answer questions about Go.

88
00:03:25,40 --> 00:03:26,90
So know let me give you a few books

89
00:03:26,90 --> 00:03:28,50
which I found really valuable.

90
00:03:28,50 --> 00:03:30,60
I've looked at pretty much all of the books

91
00:03:30,60 --> 00:03:33,30
that are out there on the Go programming language

92
00:03:33,30 --> 00:03:35,60
and out of all of the books these are the ones

93
00:03:35,60 --> 00:03:37,40
which I think are the most valuable.

94
00:03:37,40 --> 00:03:40,70
So Caleb Doxsey has two books which are really valuable.

95
00:03:40,70 --> 00:03:43,70
One of them is this one right here, "Introducing Go."

96
00:03:43,70 --> 00:03:46,30
"Introducing Go" is very approachable

97
00:03:46,30 --> 00:03:47,90
and great for beginners.

98
00:03:47,90 --> 00:03:51,30
Caleb also has a book which is offered for free online

99
00:03:51,30 --> 00:03:54,60
at golang-book.com, so it's another really great resource

100
00:03:54,60 --> 00:03:55,80
to take a look at.

101
00:03:55,80 --> 00:03:56,50
Another good book

102
00:03:56,50 --> 00:03:58,60
for people who are just getting started with programming

103
00:03:58,60 --> 00:04:01,40
is, "The Way to Go" by Ivo Balbaert.

104
00:04:01,40 --> 00:04:03,30
While this book is a few years old,

105
00:04:03,30 --> 00:04:05,20
it's still a really valuable book.

106
00:04:05,20 --> 00:04:08,10
All these books I keep on my desk and this is one of them.

107
00:04:08,10 --> 00:04:09,20
So I recommend with this book

108
00:04:09,20 --> 00:04:10,80
jumping over the first few chapters

109
00:04:10,80 --> 00:04:13,80
as they just cover some basic set up information

110
00:04:13,80 --> 00:04:16,40
but some of that information is a little bit dated

111
00:04:16,40 --> 00:04:18,10
but just start with Chapter four.

112
00:04:18,10 --> 00:04:20,80
And the way Ivo describes many of the different concepts

113
00:04:20,80 --> 00:04:23,90
related to Go programming is done very thoroughly

114
00:04:23,90 --> 00:04:25,20
and very well, so again,

115
00:04:25,20 --> 00:04:27,00
if you're just starting out with programming in Go,

116
00:04:27,00 --> 00:04:29,60
I recommend this one.

117
00:04:29,60 --> 00:04:32,20
Another great book is "Go in Action" by Bill Kennedy.

118
00:04:32,20 --> 00:04:35,00
I would categorize this book as an intermediate book

119
00:04:35,00 --> 00:04:36,40
so if you have some programming experience

120
00:04:36,40 --> 00:04:38,90
this is a great book with which to begin.

121
00:04:38,90 --> 00:04:41,30
Chapter two is a bit advanced as is Chapter seven

122
00:04:41,30 --> 00:04:44,20
so consider reading those two chapters last.

123
00:04:44,20 --> 00:04:46,40
Finally, the last book I recommend is,

124
00:04:46,40 --> 00:04:49,40
"The Go Programming Language" by Donovan and Kernighan.

125
00:04:49,40 --> 00:04:52,10
So this is a fantastic book, it's very thorough

126
00:04:52,10 --> 00:04:53,70
but it is more advanced.

127
00:04:53,70 --> 00:04:55,30
So even if you're a seasoned developer

128
00:04:55,30 --> 00:04:57,00
I recommend purchasing this book

129
00:04:57,00 --> 00:04:58,70
with one of the other books mentioned

130
00:04:58,70 --> 00:05:00,20
as it will serve as a supplement

131
00:05:00,20 --> 00:05:03,00
to some of the more advanced concepts.

132
00:05:03,00 --> 00:05:05,30
There are a few free resources which I also

133
00:05:05,30 --> 00:05:08,10
will afford to you as great for learning about Go.

134
00:05:08,10 --> 00:05:10,70
One of them is a YouTube playlist with all of my lectures

135
00:05:10,70 --> 00:05:14,00
so you could check that one out right here on YouTube.

136
00:05:14,00 --> 00:05:16,00
Another great playlist of lectures

137
00:05:16,00 --> 00:05:18,10
is one that Caleb Doxsey did

138
00:05:18,10 --> 00:05:22,30
and his playlist of lectures also has an index of videos

139
00:05:22,30 --> 00:05:25,20
which describes what's happening in each video.

140
00:05:25,20 --> 00:05:27,20
So this is a very thorough playlist

141
00:05:27,20 --> 00:05:29,90
and it introduces the Go programming language

142
00:05:29,90 --> 00:05:33,40
and then teaches you how to do web programming with Go,

143
00:05:33,40 --> 00:05:36,00
so another great resource for learning Go.

144
00:05:36,00 --> 00:05:38,90
And he has a codebase which goes along with that also

145
00:05:38,90 --> 00:05:40,00
which you can take a look at

146
00:05:40,00 --> 00:05:42,70
at this github repo, right here.

147
00:05:42,70 --> 00:05:45,90
One more great resource is there's an in-person training

148
00:05:45,90 --> 00:05:49,20
which is offered by Bill Kennedy which he calls Hardcore Go

149
00:05:49,20 --> 00:05:50,40
so take a look at that website

150
00:05:50,40 --> 00:05:52,40
and see if there's a training near you

151
00:05:52,40 --> 00:05:54,70
because if there is that's a super valuable way

152
00:05:54,70 --> 00:05:57,70
to spend a weekend and to learn more about Go.

153
00:05:57,70 --> 00:05:58,80
And then, of course,

154
00:05:58,80 --> 00:06:02,70
another great resource is Go's official website, golang.org.

155
00:06:02,70 --> 00:06:06,00
Sometimes there is some confusion between golang.org

156
00:06:06,00 --> 00:06:08,80
and godoc.org for my students.

157
00:06:08,80 --> 00:06:10,30
So what is the difference?

158
00:06:10,30 --> 00:06:12,10
Golang.org is the official website

159
00:06:12,10 --> 00:06:13,50
of the Go programming language

160
00:06:13,50 --> 00:06:16,60
and then we have godoc.org which has documentation

161
00:06:16,60 --> 00:06:18,50
for the Go programming language.

162
00:06:18,50 --> 00:06:20,40
This is not the official website

163
00:06:20,40 --> 00:06:21,90
of the Go programming language.

164
00:06:21,90 --> 00:06:24,00
This is the official website,

165
00:06:24,00 --> 00:06:26,50
right here, of the Go programming language.

166
00:06:26,50 --> 00:06:28,50
Inside here we have Packages

167
00:06:28,50 --> 00:06:30,40
and we have the Standard library.

168
00:06:30,40 --> 00:06:31,90
So if I wanted, I could come down here

169
00:06:31,90 --> 00:06:33,30
and I could look at the fmt Package

170
00:06:33,30 --> 00:06:34,90
from the Standard library.

171
00:06:34,90 --> 00:06:37,20
And this is the exact same documentation

172
00:06:37,20 --> 00:06:39,80
which I'd find right here at GoDoc

173
00:06:39,80 --> 00:06:43,10
so here's the fmt documentation from the Standard library.

174
00:06:43,10 --> 00:06:45,90
So what's the difference between these two websites?

175
00:06:45,90 --> 00:06:48,80
Here, this is only going to have documentation

176
00:06:48,80 --> 00:06:51,70
from the Standard library at golang.org

177
00:06:51,70 --> 00:06:53,80
and here at godoc.org

178
00:06:53,80 --> 00:06:56,50
we also have documentation from third-party packages.

179
00:06:56,50 --> 00:06:58,80
So you could search for third-party packages

180
00:06:58,80 --> 00:07:01,80
like uuid, Universal Unique ID.

181
00:07:01,80 --> 00:07:03,90
And so here are some code which somebody else has written

182
00:07:03,90 --> 00:07:06,40
which you might include in a project

183
00:07:06,40 --> 00:07:08,90
and save yourself the time of having to write the code

184
00:07:08,90 --> 00:07:10,60
and you can find it by going there.

185
00:07:10,60 --> 00:07:13,10
So once again, the difference between golang.org

186
00:07:13,10 --> 00:07:14,80
for looking at documentation

187
00:07:14,80 --> 00:07:17,80
and godoc.org for looking at documentation

188
00:07:17,80 --> 00:07:22,30
is GoDoc has the Standard library and third-party packages

189
00:07:22,30 --> 00:07:25,70
and golang only has the Standard library.

190
00:07:25,70 --> 00:07:27,50
Of course something else which is really helpful

191
00:07:27,50 --> 00:07:30,90
at the official website of the Go programming language

192
00:07:30,90 --> 00:07:37,60
is under Documents, you have the Language Specification

193
00:07:37,60 --> 00:07:39,30
which is right here.

194
00:07:39,30 --> 00:07:42,00
And then you also have a Document called Effective Go

195
00:07:42,00 --> 00:07:43,40
and both of those Documents

196
00:07:43,40 --> 00:07:46,20
talk about how to learn the Go programming language

197
00:07:46,20 --> 00:07:48,90
and they could be a little bit technical

198
00:07:48,90 --> 00:07:51,50
and they could be a little bit enigmatic

199
00:07:51,50 --> 00:07:53,60
but they're definitely the official source

200
00:07:53,60 --> 00:07:55,70
for figuring out how does something work

201
00:07:55,70 --> 00:07:58,20
so that's a great place to look.

202
00:07:58,20 --> 00:07:59,50
One more thing to mention

203
00:07:59,50 --> 00:08:01,60
when you're looking for Third-Party Packages

204
00:08:01,60 --> 00:08:03,50
is that in addition to godoc.org

205
00:08:03,50 --> 00:08:06,20
there's also a repo called awesome-go

206
00:08:06,20 --> 00:08:07,30
so you could take a look at that

207
00:08:07,30 --> 00:08:10,80
and it's just a list of really awesome Third-Party Packages

208
00:08:10,80 --> 00:08:12,20
that people use.

209
00:08:12,20 --> 00:08:13,70
Finally, the last thing you could do,

210
00:08:13,70 --> 00:08:15,70
a useful resource for learning the Go programming language

211
00:08:15,70 --> 00:08:17,00
is staying connected with me.

212
00:08:17,00 --> 00:08:20,00
And so these are the different ways you could find me online

213
00:08:20,00 --> 00:08:23,20
to either just follow what I'm posting about Go

214
00:08:23,20 --> 00:08:24,70
or to even ask me a question,

215
00:08:24,70 --> 00:08:26,50
if you have any questions about Go.

216
00:08:26,50 --> 00:08:29,30
So lastly, I just want to say thank you to a few individuals

217
00:08:29,30 --> 00:08:32,10
who have helped me really learn the Go programming language.

218
00:08:32,10 --> 00:08:34,40
And there's a concept to the Lakota Sioux Indians

219
00:08:34,40 --> 00:08:36,80
called Mitakuye Oyasin.

220
00:08:36,80 --> 00:08:39,30
And the idea behind that concept is that

221
00:08:39,30 --> 00:08:40,90
all of your relatives are the ones

222
00:08:40,90 --> 00:08:43,70
who are standing behind you, all of your relations,

223
00:08:43,70 --> 00:08:46,00
they're the ones who helped you get to where you are.

224
00:08:46,00 --> 00:08:48,10
And so it is only with the help of these people,

225
00:08:48,10 --> 00:08:51,30
Bill Kennedy, Caleb Doxsey, Daniel Hoffman, Corey Diehl,

226
00:08:51,30 --> 00:08:53,90
Ray Villalobos, and Zeno Rocha,

227
00:08:53,90 --> 00:08:55,40
it's only with the help of those individuals,

228
00:08:55,40 --> 00:08:58,30
plus many others that I've gotten to where I am

229
00:08:58,30 --> 00:08:59,20
with programming today.

230
00:08:59,20 --> 00:09:00,30
So I just wanted to take a moment

231
00:09:00,30 --> 00:09:02,00
and say thank you to all of them

232
00:09:02,00 --> 00:09:04,10
for all the help they've given me

233
00:09:04,10 --> 00:09:06,50
in becoming a better programmer

234
00:09:06,50 --> 00:09:08,90
so that I can now share that with you.

235
00:09:08,90 --> 00:09:10,50
All right, so hopefully these resources

236
00:09:10,50 --> 00:09:13,00
will be helpful to you on your journey, learning about Go.

