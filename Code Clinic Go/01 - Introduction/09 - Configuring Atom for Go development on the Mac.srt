1
00:00:00,50 --> 00:00:01,40
- So I thought I'd take a moment

2
00:00:01,40 --> 00:00:04,00
and just show you how to use atom.io,

3
00:00:04,00 --> 00:00:05,60
another really nice editor

4
00:00:05,60 --> 00:00:06,80
and the first thing you want to do

5
00:00:06,80 --> 00:00:08,40
is just download that.

6
00:00:08,40 --> 00:00:09,70
So once that's downloaded,

7
00:00:09,70 --> 00:00:11,80
I'm going to go over to my download folder

8
00:00:11,80 --> 00:00:14,00
and you can see I've already extracted it

9
00:00:14,00 --> 00:00:16,70
but we're going to get rid of that file for a second

10
00:00:16,70 --> 00:00:17,70
and then just show you what happens

11
00:00:17,70 --> 00:00:19,10
when I extract this again.

12
00:00:19,10 --> 00:00:20,50
I'm going to double click

13
00:00:20,50 --> 00:00:22,40
and it opens it up

14
00:00:22,40 --> 00:00:25,00
and then I just have that file right here.

15
00:00:25,00 --> 00:00:27,80
If I open that file directly, like this,

16
00:00:27,80 --> 00:00:30,80
it will tell me, "Hey, this is downloaded from the internet.

17
00:00:30,80 --> 00:00:32,00
Are you sure you want to open it?"

18
00:00:32,00 --> 00:00:33,50
This is fine, just click okay.

19
00:00:33,50 --> 00:00:35,00
Open.

20
00:00:35,00 --> 00:00:36,70
But when Atom opens, it tells me

21
00:00:36,70 --> 00:00:38,20
that GOPATH isn't set.

22
00:00:38,20 --> 00:00:41,00
You should try launching Atom using the shell commands.

23
00:00:41,00 --> 00:00:42,70
So launching Atom from the shell commands

24
00:00:42,70 --> 00:00:44,30
is going to get rid of this error.

25
00:00:44,30 --> 00:00:46,30
I'm going to close Atom

26
00:00:46,30 --> 00:00:48,30
and then go to my terminal

27
00:00:48,30 --> 00:00:49,40
and clear all this out

28
00:00:49,40 --> 00:00:51,00
and I can see where I am at.

29
00:00:51,00 --> 00:00:52,30
I'm in my downloads and if I look

30
00:00:52,30 --> 00:00:53,70
at what's in my downloads,

31
00:00:53,70 --> 00:00:54,90
I can see I have Atom.app.

32
00:00:54,90 --> 00:00:57,30
I'm going to type open Atom

33
00:00:57,30 --> 00:00:59,50
and this time there is no error there.

34
00:00:59,50 --> 00:01:02,50
So to get the GO plugin for Atom,

35
00:01:02,50 --> 00:01:04,40
you can just come up here to Atom

36
00:01:04,40 --> 00:01:06,00
and choose Preferences

37
00:01:06,00 --> 00:01:08,90
and inside Preferences you have Install

38
00:01:08,90 --> 00:01:12,30
and in Install you can search for different packages.

39
00:01:12,30 --> 00:01:14,30
I'm going to search for go-plus.

40
00:01:14,30 --> 00:01:16,00
It probably won't find it

41
00:01:16,00 --> 00:01:17,60
because I already have it installed.

42
00:01:17,60 --> 00:01:18,30
There it is.

43
00:01:18,30 --> 00:01:19,80
It found it, go-plus,

44
00:01:19,80 --> 00:01:22,70
and if it wasn't installed I would choose Install.

45
00:01:22,70 --> 00:01:24,70
So I have go-plus installed so I'm ready

46
00:01:24,70 --> 00:01:26,40
to start writing some go code.

47
00:01:26,40 --> 00:01:28,00
Let's see if we can open up all of our code

48
00:01:28,00 --> 00:01:30,10
and see what it looks like.

49
00:01:30,10 --> 00:01:32,60
I'm going to go to File and choose Open

50
00:01:32,60 --> 00:01:35,60
and then go to my Documents, my workspace

51
00:01:35,60 --> 00:01:37,80
and my source

52
00:01:37,80 --> 00:01:40,10
and my name space,

53
00:01:40,10 --> 00:01:41,40
username at github,

54
00:01:41,40 --> 00:01:43,30
and then lynda and then code clinic.

55
00:01:43,30 --> 00:01:44,80
We'll open that one.

56
00:01:44,80 --> 00:01:46,90
Here are all my files ready to be worked with

57
00:01:46,90 --> 00:01:50,10
and I can open up just this first little hello world file

58
00:01:50,10 --> 00:01:51,90
and there we are in Atom and we're ready

59
00:01:51,90 --> 00:01:53,40
to start writing code.

60
00:01:53,40 --> 00:01:56,20
We could see, too, do we have any code completion?

61
00:01:56,20 --> 00:01:57,10
So that's nice.

62
00:01:57,10 --> 00:01:58,30
It's all ready to go.

63
00:01:58,30 --> 00:01:59,70
So that's working in Atom,

64
00:01:59,70 --> 00:02:01,60
some of the basics of getting it setup.

65
00:02:01,60 --> 00:02:03,20
I want to show you one little trick

66
00:02:03,20 --> 00:02:05,30
using a go tool

67
00:02:05,30 --> 00:02:07,00
but I'm going to save that actually

68
00:02:07,00 --> 00:02:08,00
for the next video in case

69
00:02:08,00 --> 00:02:09,30
other people didn't watch this video

70
00:02:09,30 --> 00:02:10,70
because it's only Atom.

71
00:02:10,70 --> 00:02:13,00
So that's getting Atom setup and running.

