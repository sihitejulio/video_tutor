1
00:00:00,50 --> 00:00:01,80
- I want to take a moment to look at

2
00:00:01,80 --> 00:00:04,40
how do we download and install Go

3
00:00:04,40 --> 00:00:06,20
and get Go running on your local machine

4
00:00:06,20 --> 00:00:08,30
because students sometimes have trouble

5
00:00:08,30 --> 00:00:10,00
getting their environment set up correctly.

6
00:00:10,00 --> 00:00:12,30
So I think this really warrants taking a moment

7
00:00:12,30 --> 00:00:14,50
just to look, how do we set up our environment.

8
00:00:14,50 --> 00:00:15,90
So what we're going to see in this video

9
00:00:15,90 --> 00:00:18,40
is we're going to see how to download and install Go,

10
00:00:18,40 --> 00:00:19,60
totally easy.

11
00:00:19,60 --> 00:00:21,50
I'm going to talk about SHA-1 checksums

12
00:00:21,50 --> 00:00:24,20
and just sort of illustrate what that's used for.

13
00:00:24,20 --> 00:00:27,20
Then we'll look how to set up your workspace in Go.

14
00:00:27,20 --> 00:00:29,10
Go definitely has a certain workspace

15
00:00:29,10 --> 00:00:31,50
which is required, I'll show you how to do that.

16
00:00:31,50 --> 00:00:33,40
And then we're going to set some environment variables

17
00:00:33,40 --> 00:00:35,90
and make sure our environment variables are set correctly.

18
00:00:35,90 --> 00:00:37,50
So that's what's on the docket,

19
00:00:37,50 --> 00:00:39,20
and the first thing to do is

20
00:00:39,20 --> 00:00:42,70
to go to the golang.org website.

21
00:00:42,70 --> 00:00:44,10
Once we're here, you can see,

22
00:00:44,10 --> 00:00:46,80
there's a big logo right here and Download Go.

23
00:00:46,80 --> 00:00:48,90
So I'm going to click on Download Go.

24
00:00:48,90 --> 00:00:50,20
And I just choose the platform

25
00:00:50,20 --> 00:00:52,20
which I want to download for.

26
00:00:52,20 --> 00:00:53,60
And right beneath the link to download,

27
00:00:53,60 --> 00:00:57,10
you see right there, SHA-1 and it has a unique number.

28
00:00:57,10 --> 00:00:59,50
We'll come back and take a look at that in one second.

29
00:00:59,50 --> 00:01:00,90
But first, I'm just going to click this link

30
00:01:00,90 --> 00:01:04,50
and download Go.

31
00:01:04,50 --> 00:01:06,20
So now that Go has downloaded,

32
00:01:06,20 --> 00:01:07,70
I could either click on that arrow

33
00:01:07,70 --> 00:01:09,00
and choose Show in Finder,

34
00:01:09,00 --> 00:01:10,60
or I could just choose to open.

35
00:01:10,60 --> 00:01:13,00
So I'm going to show in finder, just take a look at it.

36
00:01:13,00 --> 00:01:15,00
And double-click it right there.

37
00:01:15,00 --> 00:01:18,20
And now I'll walk through these windows to install Go.

38
00:01:18,20 --> 00:01:19,80
So, just Continue,

39
00:01:19,80 --> 00:01:21,90
and then default destination is selected,

40
00:01:21,90 --> 00:01:24,80
and installation type, just continue.

41
00:01:24,80 --> 00:01:27,30
And then it asks me for my password.

42
00:01:27,30 --> 00:01:31,50
Type in my password and then click Install Software.

43
00:01:31,50 --> 00:01:32,80
So far, pretty simple.

44
00:01:32,80 --> 00:01:34,40
Nothing too complicated.

45
00:01:34,40 --> 00:01:35,80
So the next thing I'm going to do is,

46
00:01:35,80 --> 00:01:37,50
after I close this windows, I'm going to check

47
00:01:37,50 --> 00:01:39,30
to see, is Go installed.

48
00:01:39,30 --> 00:01:41,00
I'm on a Apple, so I'm going to hold down command

49
00:01:41,00 --> 00:01:42,60
and hit the spacebar.

50
00:01:42,60 --> 00:01:45,00
And that's going to bring up Spotlight Search

51
00:01:45,00 --> 00:01:47,00
where I could type in terminal,

52
00:01:47,00 --> 00:01:48,20
and I want to bring up my terminal

53
00:01:48,20 --> 00:01:50,60
to run some commands at the terminal.

54
00:01:50,60 --> 00:01:52,10
And so I've got my terminal launched,

55
00:01:52,10 --> 00:01:55,50
and now I'm just going to type in go, space, env.

56
00:01:55,50 --> 00:01:57,90
and It listed all the environment variables.

57
00:01:57,90 --> 00:02:01,40
So, go is a command which you can run at the command line,

58
00:02:01,40 --> 00:02:04,30
at the terminal, when Go is installed.

59
00:02:04,30 --> 00:02:07,50
And I can see some of the environment variables like GOROOT.

60
00:02:07,50 --> 00:02:10,90
So this is where all of the Go program got installed.

61
00:02:10,90 --> 00:02:12,30
usr/local/go,

62
00:02:12,30 --> 00:02:14,10
and then we also have GOPATH.

63
00:02:14,10 --> 00:02:15,70
And GOPATH is not set yet,

64
00:02:15,70 --> 00:02:17,30
but that's going to be set to my workspace

65
00:02:17,30 --> 00:02:20,50
which I'm going to show you how to set up in just a moment.

66
00:02:20,50 --> 00:02:23,10
One more go command which is cool to know about,

67
00:02:23,10 --> 00:02:26,40
I'm going to clear my screen by holding down command K,

68
00:02:26,40 --> 00:02:29,70
I'm going to type in go version.

69
00:02:29,70 --> 00:02:33,50
So it tells me I have version 1.5.2 installed.

70
00:02:33,50 --> 00:02:35,00
And then one more command which I'm going to show you

71
00:02:35,00 --> 00:02:38,20
is just to type in go and press enter,

72
00:02:38,20 --> 00:02:40,30
and it brings up the different commands,

73
00:02:40,30 --> 00:02:42,60
and also different topics that you could use

74
00:02:42,60 --> 00:02:44,10
with the go command.

75
00:02:44,10 --> 00:02:46,90
So the go command takes different arguments.

76
00:02:46,90 --> 00:02:49,50
And you could see, we used Go environment

77
00:02:49,50 --> 00:02:50,60
right here, right?

78
00:02:50,60 --> 00:02:52,40
And we got the environment variables.

79
00:02:52,40 --> 00:02:54,50
In addition, we had different topics,

80
00:02:54,50 --> 00:02:56,10
like, I could type in Go C

81
00:02:56,10 --> 00:02:58,00
and it'll give me information about that topic,

82
00:02:58,00 --> 00:03:00,70
how to call code between Go and C.

83
00:03:00,70 --> 00:03:02,70
Furthermore, I could type in go help

84
00:03:02,70 --> 00:03:04,70
and then enter either a command,

85
00:03:04,70 --> 00:03:07,90
or go help and then enter a topic

86
00:03:07,90 --> 00:03:09,70
and when I do that, it'll give me help

87
00:03:09,70 --> 00:03:12,00
on how to use different commands and different topics.

88
00:03:12,00 --> 00:03:14,80
So, go help env.

89
00:03:14,80 --> 00:03:16,00
So that gave me a little information

90
00:03:16,00 --> 00:03:17,20
about my environment variables.

91
00:03:17,20 --> 00:03:20,40
go help version

92
00:03:20,40 --> 00:03:23,00
gave me a little bit of information about running version.

93
00:03:23,00 --> 00:03:25,40
So that's a little bit about the go command.

94
00:03:25,40 --> 00:03:27,40
So if I just type in env,

95
00:03:27,40 --> 00:03:30,60
you could see, I have environment path variables here.

96
00:03:30,60 --> 00:03:32,50
So, path variables, environment variables

97
00:03:32,50 --> 00:03:34,70
are variables that we set in the computer

98
00:03:34,70 --> 00:03:37,60
to tell the computer where to look for certain things.

99
00:03:37,60 --> 00:03:39,50
So, if I asked my computer to do something,

100
00:03:39,50 --> 00:03:41,70
like if I type in go version,

101
00:03:41,70 --> 00:03:43,60
the computer has to figure out, "Okay,

102
00:03:43,60 --> 00:03:45,10
"you've asked me to run a command,

103
00:03:45,10 --> 00:03:47,20
"that command is go with something else,

104
00:03:47,20 --> 00:03:48,80
"version, added to it.

105
00:03:48,80 --> 00:03:52,70
"Where can I go to get the code to run that command?"

106
00:03:52,70 --> 00:03:54,10
So the computer will then look down

107
00:03:54,10 --> 00:03:56,10
all of these different paths.

108
00:03:56,10 --> 00:03:56,90
Right?

109
00:03:56,90 --> 00:03:58,30
And it'll try to figure out, "Hey, do you know

110
00:03:58,30 --> 00:04:00,00
"how to run Go code?

111
00:04:00,00 --> 00:04:01,30
"I've been given a go command,

112
00:04:01,30 --> 00:04:02,80
"do you know how to run this command?"

113
00:04:02,80 --> 00:04:05,00
So it will look here, it'll look here, here,

114
00:04:05,00 --> 00:04:07,00
it'll look in all those places, right.

115
00:04:07,00 --> 00:04:08,60
And, eventually, it'll come to the place

116
00:04:08,60 --> 00:04:11,30
that knows how to run that command and then it runs it.

117
00:04:11,30 --> 00:04:13,80
So that's what environment variables allow us to do.

118
00:04:13,80 --> 00:04:17,80
All right, so now let's configure our workspace.

119
00:04:17,80 --> 00:04:20,50
To set up our workspace, we're going to need three folders.

120
00:04:20,50 --> 00:04:22,20
So I'm just going to go to Documents

121
00:04:22,20 --> 00:04:25,40
and I'm going to create a new folder.

122
00:04:25,40 --> 00:04:26,90
But, this could be called anything.

123
00:04:26,90 --> 00:04:27,80
So, gowork.

124
00:04:27,80 --> 00:04:29,70
I'll just call it gowork, where I keep my Go work.

125
00:04:29,70 --> 00:04:31,00
And then inside gowork,

126
00:04:31,00 --> 00:04:34,00
I'm going to create three new folders,

127
00:04:34,00 --> 00:04:37,80
one is going to be bin,

128
00:04:37,80 --> 00:04:41,40
another one will be pkg,

129
00:04:41,40 --> 00:04:43,70
and another one will be src.

130
00:04:43,70 --> 00:04:46,10
So, perfect, I've got those three folders in my workspace,

131
00:04:46,10 --> 00:04:48,20
that's all I needed to do.

132
00:04:48,20 --> 00:04:50,20
To get to that location in my terminal,

133
00:04:50,20 --> 00:04:52,00
I'm going to hold down command and tab

134
00:04:52,00 --> 00:04:53,70
and switch to my terminal.

135
00:04:53,70 --> 00:04:55,00
I'm going to clear out my terminal

136
00:04:55,00 --> 00:04:57,50
by holding command K, and then once I'm here,

137
00:04:57,50 --> 00:04:59,90
I'm going to type in print working directory,

138
00:04:59,90 --> 00:05:02,30
to see which working directory I'm in.

139
00:05:02,30 --> 00:05:03,70
And I'm at Users/toddmcleod,

140
00:05:03,70 --> 00:05:07,20
so ls -la to list everything in that directory

141
00:05:07,20 --> 00:05:09,30
and I could see, I have Documents right there

142
00:05:09,30 --> 00:05:11,10
as a folder in that directory.

143
00:05:11,10 --> 00:05:12,80
So, cd to change directories,

144
00:05:12,80 --> 00:05:15,00
go to change directory documents.

145
00:05:15,00 --> 00:05:16,90
Command K to clear my screen.

146
00:05:16,90 --> 00:05:19,30
ls - la to print everything,

147
00:05:19,30 --> 00:05:21,30
and I could see, there's my gowork folder,

148
00:05:21,30 --> 00:05:23,50
I could change directories into gowork.

149
00:05:23,50 --> 00:05:24,60
I could hit tab right there,

150
00:05:24,60 --> 00:05:27,60
and it would default to whatever matches over here,

151
00:05:27,60 --> 00:05:29,10
if there's a match.

152
00:05:29,10 --> 00:05:32,80
And then I hit enter, and that could ls - la,

153
00:05:32,80 --> 00:05:35,50
list, list all, and see that I have bin, pkg,

154
00:05:35,50 --> 00:05:36,70
and src, right there.

155
00:05:36,70 --> 00:05:38,10
So that's my workspace.

156
00:05:38,10 --> 00:05:40,30
So now I need to set my environment variable to that.

157
00:05:40,30 --> 00:05:42,40
How do I go about doing that?

158
00:05:42,40 --> 00:05:44,50
If I go back to my root directory

159
00:05:44,50 --> 00:05:46,30
and I could just type cd.

160
00:05:46,30 --> 00:05:49,60
So, pwd shows me I'm there, print working directory.

161
00:05:49,60 --> 00:05:52,10
cd will take me back to my root

162
00:05:52,10 --> 00:05:53,50
and then I could print right there.

163
00:05:53,50 --> 00:05:57,00
Sometimes you'll see people do cd with a tilde, like that.

164
00:05:57,00 --> 00:05:58,80
And that also takes you right back to your root,

165
00:05:58,80 --> 00:06:00,50
and you can see, I'm still at my root.

166
00:06:00,50 --> 00:06:01,30
So now that I'm there,

167
00:06:01,30 --> 00:06:03,10
I'm going to do a list, list all again.

168
00:06:03,10 --> 00:06:04,30
I'll hold down command K

169
00:06:04,30 --> 00:06:06,60
to bring that command to the top of the screen.

170
00:06:06,60 --> 00:06:07,70
Hit enter.

171
00:06:07,70 --> 00:06:12,60
And I have something in here called bash_profile.

172
00:06:12,60 --> 00:06:15,50
So, inside this bash profile I could put a setting in there

173
00:06:15,50 --> 00:06:17,90
to set a new environment variable.

174
00:06:17,90 --> 00:06:19,90
Now, I'm going to do that by typing nano.

175
00:06:19,90 --> 00:06:22,90
And nano is a text editor right in the terminal.

176
00:06:22,90 --> 00:06:26,30
And I'm going to hit my .bas,

177
00:06:26,30 --> 00:06:27,90
and if I hit tab here,

178
00:06:27,90 --> 00:06:30,40
I'm actually going to match on that and that.

179
00:06:30,40 --> 00:06:31,90
And it'll say, "Which one do you want?

180
00:06:31,90 --> 00:06:33,20
"Do you want this one, bash_history,

181
00:06:33,20 --> 00:06:34,10
"or bash_profile?"

182
00:06:34,10 --> 00:06:35,40
Hit tab a couple of times and it

183
00:06:35,40 --> 00:06:36,90
gave me my two different options.

184
00:06:36,90 --> 00:06:39,60
I'm going to complete pr, so it'll be able to figure out,

185
00:06:39,60 --> 00:06:41,20
I want bash profile.

186
00:06:41,20 --> 00:06:43,70
Hit tab, and then press enter,

187
00:06:43,70 --> 00:06:46,90
and I'm here where I can edit or add environment variables.

188
00:06:46,90 --> 00:06:50,50
So, bash_profile will be run by my computer

189
00:06:50,50 --> 00:06:52,40
anytime I open up the terminal and it'll look at,

190
00:06:52,40 --> 00:06:54,40
like, what are my different environment variables

191
00:06:54,40 --> 00:06:55,40
and it'll allow me to set

192
00:06:55,40 --> 00:06:57,10
my different environment variables.

193
00:06:57,10 --> 00:06:59,20
So, to set an environment variable for Go,

194
00:06:59,20 --> 00:07:00,40
I'm going to put a comment and say,

195
00:07:00,40 --> 00:07:03,10
"for Go Programming."

196
00:07:03,10 --> 00:07:04,80
That's a comment, and then I'm going to do,

197
00:07:04,80 --> 00:07:08,00
export GOPATH, and my GOPATH

198
00:07:08,00 --> 00:07:11,20
is going to be equal to my HOME,

199
00:07:11,20 --> 00:07:14,70
whatever my home is, Documents.

200
00:07:14,70 --> 00:07:17,10
And then I called it gowork.

201
00:07:17,10 --> 00:07:21,30
So that will be my environment variable for my GOPATH.

202
00:07:21,30 --> 00:07:23,50
So that tells the Go sdk that, hey,

203
00:07:23,50 --> 00:07:26,30
this is where my workspace is located

204
00:07:26,30 --> 00:07:28,70
and that's where you're going to find all my Go code.

205
00:07:28,70 --> 00:07:31,40
Now, you can have multiple workspaces,

206
00:07:31,40 --> 00:07:33,20
but if you're just starting out with Go,

207
00:07:33,20 --> 00:07:36,80
I strongly advise you to set up one workspace. (chuckles)

208
00:07:36,80 --> 00:07:38,10
I'm going to leave it at that,

209
00:07:38,10 --> 00:07:39,50
that's all I'm going to say.

210
00:07:39,50 --> 00:07:43,60
Now, I'm going to do control X to save this, hit Yes.

211
00:07:43,60 --> 00:07:45,80
And now, if I restart my terminals,

212
00:07:45,80 --> 00:07:48,40
I'm going to close this with command Q.

213
00:07:48,40 --> 00:07:50,40
And then restart terminal.

214
00:07:50,40 --> 00:07:53,10
And now if I do my go env,

215
00:07:53,10 --> 00:07:54,90
I should see that I not only have GOROOT,

216
00:07:54,90 --> 00:07:57,70
which we saw before, but I also have GOPATH.

217
00:07:57,70 --> 00:08:00,80
And notice, where I had $HOME before

218
00:08:00,80 --> 00:08:04,20
it put in the home directory, or the home location

219
00:08:04,20 --> 00:08:05,60
for this user, right?

220
00:08:05,60 --> 00:08:08,60
So it filled that in from that variable, the $HOME.

221
00:08:08,60 --> 00:08:10,00
And so there's my workspace.

222
00:08:10,00 --> 00:08:12,00
I have one more environment variable I want to configure.

223
00:08:12,00 --> 00:08:15,80
Command K to clear my screen, and then env to show them.

224
00:08:15,80 --> 00:08:17,80
I want to add to my path.

225
00:08:17,80 --> 00:08:20,90
I want to add the location right here, to bin.

226
00:08:20,90 --> 00:08:24,60
So bin is where my binary, B I N, binary, bin,

227
00:08:24,60 --> 00:08:26,30
that's where my binaries will be stored

228
00:08:26,30 --> 00:08:27,90
once I've compiled them.

229
00:08:27,90 --> 00:08:30,30
So when I create a program and I compile it,

230
00:08:30,30 --> 00:08:32,60
the executable would be put right here.

231
00:08:32,60 --> 00:08:35,50
By having a path to that folder,

232
00:08:35,50 --> 00:08:38,40
then I could just type in the executable's name

233
00:08:38,40 --> 00:08:39,60
and the computer will be like, "Okay,

234
00:08:39,60 --> 00:08:41,30
"do you know how to run this executable?

235
00:08:41,30 --> 00:08:42,10
"No.

236
00:08:42,10 --> 00:08:43,50
"Do you know how to run this executable?

237
00:08:43,50 --> 00:08:44,20
"No.

238
00:08:44,20 --> 00:08:45,10
"Do you? No.

239
00:08:45,10 --> 00:08:46,20
"Do you? No."

240
00:08:46,20 --> 00:08:47,70
And finally, when it gets to the setting

241
00:08:47,70 --> 00:08:49,80
that points to that folder, it'll be like,

242
00:08:49,80 --> 00:08:51,30
"Oh, okay, here's that executable,

243
00:08:51,30 --> 00:08:52,50
"I'm going to run it."

244
00:08:52,50 --> 00:08:53,90
So I'm going to add that environment variable

245
00:08:53,90 --> 00:08:55,20
to the bin folder.

246
00:08:55,20 --> 00:08:57,80
To do that I'm going to go back to nano,

247
00:08:57,80 --> 00:09:00,70
well, print working directory.

248
00:09:00,70 --> 00:09:02,10
And then ls -la.

249
00:09:02,10 --> 00:09:04,30
And I want to do my nano

250
00:09:04,30 --> 00:09:07,20
bash_profile.

251
00:09:07,20 --> 00:09:10,10
And I could do this by also just hitting the up arrow

252
00:09:10,10 --> 00:09:11,70
to go to the previous commands,

253
00:09:11,70 --> 00:09:13,30
so I could do it that way.

254
00:09:13,30 --> 00:09:14,50
So now that I'm here, I'm going to add

255
00:09:14,50 --> 00:09:17,20
one more environment variable.

256
00:09:17,20 --> 00:09:21,00
And that variable is going to be export PATH.

257
00:09:21,00 --> 00:09:22,10
When I first started programming,

258
00:09:22,10 --> 00:09:23,30
this looked a little bit funny to me.

259
00:09:23,30 --> 00:09:25,90
I have my PATH, and then there's the $PATH right here.

260
00:09:25,90 --> 00:09:27,70
Basically, what this is doing is it's saying,

261
00:09:27,70 --> 00:09:29,60
"Hey, all of the variables that are already

262
00:09:29,60 --> 00:09:31,70
"in the PATH variable, we're going to

263
00:09:31,70 --> 00:09:34,60
"take those variables, we're going to add this one onto it,

264
00:09:34,60 --> 00:09:36,40
"and then we're going to set all of this,

265
00:09:36,40 --> 00:09:39,30
"once again, equal to what our PATH variable is now."

266
00:09:39,30 --> 00:09:41,40
So, it's just a normal variable assignment

267
00:09:41,40 --> 00:09:43,40
with the variable on the left and the values to which

268
00:09:43,40 --> 00:09:46,20
you want to assign to that variable over on the right.

269
00:09:46,20 --> 00:09:49,90
So I've added that one, control X to save.

270
00:09:49,90 --> 00:09:51,10
Yes.

271
00:09:51,10 --> 00:09:53,20
And then, restart my terminal.

272
00:09:53,20 --> 00:09:54,50
And you have to restart your terminal

273
00:09:54,50 --> 00:09:57,50
to reload the settings, and if I type in env,

274
00:09:57,50 --> 00:09:59,10
I'll see in my PATH environment variables

275
00:09:59,10 --> 00:10:01,60
I now have an environment variable pointing

276
00:10:01,60 --> 00:10:06,60
to Users/toddmcleod/Documents/gowork/bin.

277
00:10:06,60 --> 00:10:08,60
All right, so let's see a couple useful things

278
00:10:08,60 --> 00:10:10,90
we could do with our workspace now that it's set up.

279
00:10:10,90 --> 00:10:12,20
The first thing I'm going to show you

280
00:10:12,20 --> 00:10:15,60
is that I could go and find some third party package

281
00:10:15,60 --> 00:10:17,50
and that could be listed at godoc.org.

282
00:10:17,50 --> 00:10:19,00
So, if I wanted to, I could go,

283
00:10:19,00 --> 00:10:20,90
I want a uuid.

284
00:10:20,90 --> 00:10:24,30
And so, maybe I want to use nu7hatch's uuid.

285
00:10:24,30 --> 00:10:26,20
And, often in these third party packages

286
00:10:26,20 --> 00:10:27,90
they'll say, "Hey, to get this code,

287
00:10:27,90 --> 00:10:29,80
"use the go get command."

288
00:10:29,80 --> 00:10:30,80
It doesn't say it in this one,

289
00:10:30,80 --> 00:10:32,40
but I'll show you how to do it.

290
00:10:32,40 --> 00:10:37,00
So, I copy the path at github to this package.

291
00:10:37,00 --> 00:10:40,10
And this is all really kind of cool the way it works.

292
00:10:40,10 --> 00:10:41,80
So, the first thing I'm going to show you

293
00:10:41,80 --> 00:10:44,20
is in Go, the go commands,

294
00:10:44,20 --> 00:10:45,80
we have go get.

295
00:10:45,80 --> 00:10:48,70
And that will download install packages and dependencies.

296
00:10:48,70 --> 00:10:50,40
So, this is package management.

297
00:10:50,40 --> 00:10:52,70
So I'm going to do go get and then type in

298
00:10:52,70 --> 00:10:54,60
this namespace, right?

299
00:10:54,60 --> 00:10:58,40
At this url, this user, this package.

300
00:10:58,40 --> 00:11:01,60
And it's going and getting all of that code.

301
00:11:01,60 --> 00:11:04,10
And you'll notice that this thing just popped up.

302
00:11:04,10 --> 00:11:06,60
If you haven't done development on your Mac before,

303
00:11:06,60 --> 00:11:10,10
you may be then prompted to install a few tools.

304
00:11:10,10 --> 00:11:11,80
So I'm going to try that again.

305
00:11:11,80 --> 00:11:13,40
Same command, hit enter.

306
00:11:13,40 --> 00:11:15,00
And up here, you can see the terminal working

307
00:11:15,00 --> 00:11:16,00
and it looks like it's all done,

308
00:11:16,00 --> 00:11:18,60
so I'm going to go back and look at my workspace.

309
00:11:18,60 --> 00:11:20,90
And now, inside my workspace,

310
00:11:20,90 --> 00:11:23,40
I have my source, github.com,

311
00:11:23,40 --> 00:11:25,80
nu7hatch, gouuid.

312
00:11:25,80 --> 00:11:27,70
So all of that code has been downloaded.

313
00:11:27,70 --> 00:11:30,00
Now let me show you one other thing which is totally cool.

314
00:11:30,00 --> 00:11:32,20
That package does not need to be on GoDoc.

315
00:11:32,20 --> 00:11:34,70
I could just go straight to github.

316
00:11:34,70 --> 00:11:36,70
And let's say I want to download all of the code

317
00:11:36,70 --> 00:11:38,30
I have at the Golang Training,

318
00:11:38,30 --> 00:11:40,10
which I highly recommend you do.

319
00:11:40,10 --> 00:11:43,60
And then come back here, go get.

320
00:11:43,60 --> 00:11:47,50
And I need to take out that https part.

321
00:11:47,50 --> 00:11:50,60
So, it just downloaded and installed all of that code.

322
00:11:50,60 --> 00:11:53,00
And here, you can see, I have all that code.

323
00:11:53,00 --> 00:11:56,10
And the namespacing for packages here is pretty great.

324
00:11:56,10 --> 00:11:58,30
We have github.com, user name,

325
00:11:58,30 --> 00:12:01,20
and then here is this repo for me and all the code.

326
00:12:01,20 --> 00:12:02,40
User name, a different user,

327
00:12:02,40 --> 00:12:03,80
repo, all the code.

328
00:12:03,80 --> 00:12:05,60
And then when we call that in our programs,

329
00:12:05,60 --> 00:12:08,40
we'll be calling it with pretty much the same namespace.

330
00:12:08,40 --> 00:12:11,80
And that's how packages and namespaces are managed in Go.

331
00:12:11,80 --> 00:12:14,00
And they're tied directly off of your workspace

332
00:12:14,00 --> 00:12:16,70
which needs to have bin, pkg, and src.

333
00:12:16,70 --> 00:12:19,30
So that's how the workspaces all work, right there.

334
00:12:19,30 --> 00:12:20,20
So the next thing I'm going to do,

335
00:12:20,20 --> 00:12:22,60
is I'm just going to create a new folder here, under src,

336
00:12:22,60 --> 00:12:25,30
just temporary, so I'll just call it temp.

337
00:12:25,30 --> 00:12:29,30
And then I'm going to navigate to that at my terminal.

338
00:12:29,30 --> 00:12:34,70
And then inside here, I'm going to create a new file.

339
00:12:34,70 --> 00:12:37,80
And I'm going to first just put a comment.

340
00:12:37,80 --> 00:12:42,10
And then I'll come over to the Golang.org website.

341
00:12:42,10 --> 00:12:43,90
And here's the hello world program,

342
00:12:43,90 --> 00:12:45,70
so I'm going to copy that.

343
00:12:45,70 --> 00:12:48,10
Come back to my terminal,

344
00:12:48,10 --> 00:12:49,10
paste it,

345
00:12:49,10 --> 00:12:53,60
and then control X, and save it.

346
00:12:53,60 --> 00:12:55,50
So I've just written a little piece of code,

347
00:12:55,50 --> 00:12:56,80
right there, hello.go.

348
00:12:56,80 --> 00:12:58,40
So now, at my terminal,

349
00:12:58,40 --> 00:12:59,80
I'm going to change and make sure,

350
00:12:59,80 --> 00:13:01,90
figure out which directory I'm in, I'm in temp,

351
00:13:01,90 --> 00:13:03,50
and I could ls and there it is.

352
00:13:03,50 --> 00:13:05,50
And I'm going to do go build.

353
00:13:05,50 --> 00:13:07,20
And it just built it, and now if I look,

354
00:13:07,20 --> 00:13:12,70
I have hello.go and I also have temp.

355
00:13:12,70 --> 00:13:15,20
And you'll notice it named it after the file,

356
00:13:15,20 --> 00:13:16,20
so there's my temp file.

357
00:13:16,20 --> 00:13:17,80
And I only did go build.

358
00:13:17,80 --> 00:13:20,70
If I do go install,

359
00:13:20,70 --> 00:13:23,00
it'll actually install that binary right there in my temp.

360
00:13:23,00 --> 00:13:25,60
So, go build just builds your file,

361
00:13:25,60 --> 00:13:27,90
and then puts the executable right there.

362
00:13:27,90 --> 00:13:30,30
Go install will install it in your binary.

363
00:13:30,30 --> 00:13:32,20
And now, if I go back to my terminal,

364
00:13:32,20 --> 00:13:34,00
and I type in temp,

365
00:13:34,00 --> 00:13:36,30
Hello, Chinese. (laughs)

366
00:13:36,30 --> 00:13:38,00
So that's our first executable program.

367
00:13:38,00 --> 00:13:40,50
It's running, we have Go up and running.

368
00:13:40,50 --> 00:13:42,70
We've downloaded it, we've installed it,

369
00:13:42,70 --> 00:13:44,00
we configured our workspace,

370
00:13:44,00 --> 00:13:46,10
we configured our environment variables.

371
00:13:46,10 --> 00:13:47,90
The last thing which, I realize, I didn't show you,

372
00:13:47,90 --> 00:13:49,90
was just to check the SHA-1.

373
00:13:49,90 --> 00:13:52,30
So, when we did download Go,

374
00:13:52,30 --> 00:13:58,50
it showed us this SHA-1 code right here.

375
00:13:58,50 --> 00:13:59,60
Right there.

376
00:13:59,60 --> 00:14:01,50
So, when I look at what got downloaded,

377
00:14:01,50 --> 00:14:04,00
if I go to my Download folder, right there.

378
00:14:04,00 --> 00:14:05,90
So I don't need that one.

379
00:14:05,90 --> 00:14:08,20
At my terminal,

380
00:14:08,20 --> 00:14:11,50
I can now run openssl sha1.

381
00:14:11,50 --> 00:14:14,10
And then get that file which is,

382
00:14:14,10 --> 00:14:17,20
let me see what that file name is, first.

383
00:14:17,20 --> 00:14:19,60
And it prints out that SHA-1 value.

384
00:14:19,60 --> 00:14:22,40
And so that SHA-1 value right there is just confirming

385
00:14:22,40 --> 00:14:25,40
that the file I downloaded is the exact same file

386
00:14:25,40 --> 00:14:26,90
that I received.

387
00:14:26,90 --> 00:14:29,70
So, that the file they intended is the file that I received.

388
00:14:29,70 --> 00:14:31,20
So that's just a security check to make sure

389
00:14:31,20 --> 00:14:34,30
the file hasn't been tampered with in transport anywhere.

390
00:14:34,30 --> 00:14:36,20
So, that was a lot of really good information.

391
00:14:36,20 --> 00:14:39,00
I wish you luck in getting your Go environment set up.

