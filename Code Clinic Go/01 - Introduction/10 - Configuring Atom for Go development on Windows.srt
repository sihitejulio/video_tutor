1
00:00:00,40 --> 00:00:02,10
- So now we're going to install Atom.

2
00:00:02,10 --> 00:00:03,80
That's another editor that you can use with Go.

3
00:00:03,80 --> 00:00:07,60
So you could use WebStorm or Atom, whichever your prefer.

4
00:00:07,60 --> 00:00:09,00
To do that, the first step is

5
00:00:09,00 --> 00:00:11,50
we're going to download Windows Installer.

6
00:00:11,50 --> 00:00:12,60
So that downloads.

7
00:00:12,60 --> 00:00:15,10
I'm going to click it to launch it.

8
00:00:15,10 --> 00:00:18,20
And now Atom is being installed.

9
00:00:18,20 --> 00:00:20,10
And the first time you see Atom, you're going

10
00:00:20,10 --> 00:00:22,50
to see this welcome screen, and if you ever want to get back

11
00:00:22,50 --> 00:00:24,00
to it, you could always go to Help,

12
00:00:24,00 --> 00:00:26,20
Welcome Guide to see this.

13
00:00:26,20 --> 00:00:28,30
So now the next step is to install a package,

14
00:00:28,30 --> 00:00:31,40
and we want to install the go-plus package.

15
00:00:31,40 --> 00:00:35,40
So I'm going to search for go-plus.

16
00:00:35,40 --> 00:00:38,00
And the idea here is that there's third party packages

17
00:00:38,00 --> 00:00:39,70
that you could use in your editor, too,

18
00:00:39,70 --> 00:00:42,60
to help make your editor run more effectively.

19
00:00:42,60 --> 00:00:45,20
So where we have go-plus, and I'm installing it.

20
00:00:45,20 --> 00:00:48,10
And while this installs, I want to come over to my workspace

21
00:00:48,10 --> 00:00:50,60
and open that up and look inside Source.

22
00:00:50,60 --> 00:00:54,40
As it installs this go-plus plug-in, it'll grab different

23
00:00:54,40 --> 00:00:57,80
things like gofmt, goimports, gocode, golint.

24
00:00:57,80 --> 00:00:59,70
It should be grabbing all that, and you see it just

25
00:00:59,70 --> 00:01:02,50
dropped some packages into my work space.

26
00:01:02,50 --> 00:01:04,90
So I have those packages there now,

27
00:01:04,90 --> 00:01:06,30
but it didn't find some of them.

28
00:01:06,30 --> 00:01:08,00
One of them it didn't find was

29
00:01:08,00 --> 00:01:11,40
the Lint Tool, nor did it find goimports.

30
00:01:11,40 --> 00:01:14,00
And so, you would have to go do a little research

31
00:01:14,00 --> 00:01:15,90
at this point, which I've done,

32
00:01:15,90 --> 00:01:21,20
and just be like golang goimports, and see what comes up.

33
00:01:21,20 --> 00:01:24,70
Then just read the different links or golang golint.

34
00:01:24,70 --> 00:01:27,40
And the thing is, more than one person out there might have

35
00:01:27,40 --> 00:01:30,70
made a golang linter, so you have to make the distinction,

36
00:01:30,70 --> 00:01:34,60
the determination is which one is Adam wanting

37
00:01:34,60 --> 00:01:39,20
because it's saying the linter hasn't been found.

38
00:01:39,20 --> 00:01:40,90
And generally speaking, what comes up

39
00:01:40,90 --> 00:01:42,90
is the most popular one in the community,

40
00:01:42,90 --> 00:01:44,90
but definitely do a little bit of research

41
00:01:44,90 --> 00:01:46,60
and see if you have the right one.

42
00:01:46,60 --> 00:01:48,40
So here's the one that we want.

43
00:01:48,40 --> 00:01:49,80
I'm just giving you that heads up about how you do

44
00:01:49,80 --> 00:01:52,00
the research in case you end up in that situation,

45
00:01:52,00 --> 00:01:54,90
and it's not for one of the things I've just described.

46
00:01:54,90 --> 00:01:56,60
So we're going to get the go linter, and now we're

47
00:01:56,60 --> 00:01:59,80
going to go not to Atom, but to our terminal.

48
00:01:59,80 --> 00:02:01,40
I'm going to clear out my terminal.

49
00:02:01,40 --> 00:02:04,40
I'm going to do a go get and then paste in this url

50
00:02:04,40 --> 00:02:07,40
because I'm going to go get some code and enter it.

51
00:02:07,40 --> 00:02:09,70
That will go get the linter.

52
00:02:09,70 --> 00:02:11,90
And then we want to do the same thing with imports.

53
00:02:11,90 --> 00:02:14,00
So I have imports here for you.

54
00:02:14,00 --> 00:02:15,80
So copy that and then

55
00:02:15,80 --> 00:02:20,10
come back to the terminal and type that in

56
00:02:20,10 --> 00:02:23,20
and paste then hit enter.

57
00:02:23,20 --> 00:02:24,50
So now we have both of those.

58
00:02:24,50 --> 00:02:26,80
We can come back to Atom.

59
00:02:26,80 --> 00:02:30,50
If I wanted to, I could hit Uninstall and then Reinstall.

60
00:02:30,50 --> 00:02:33,50
So let's just do that and see if those errors go away.

61
00:02:33,50 --> 00:02:36,80
And so now you can see that the lint tool and then also

62
00:02:36,80 --> 00:02:39,10
the other one, goimports, both of those are being

63
00:02:39,10 --> 00:02:41,10
found now, and it's all good.

64
00:02:41,10 --> 00:02:43,70
So we have a godef tool and an oracle tool,

65
00:02:43,70 --> 00:02:45,30
which I'm not going to worry about.

66
00:02:45,30 --> 00:02:48,20
It's not really anything that I want to have run.

67
00:02:48,20 --> 00:02:50,20
I've never used those in my own go development.

68
00:02:50,20 --> 00:02:53,50
So the thing that I want to do at this point is just come in

69
00:02:53,50 --> 00:02:56,70
and open some files and see if our editor is doing

70
00:02:56,70 --> 00:02:59,50
code completion and doing everything we want.

71
00:02:59,50 --> 00:03:01,60
So I'm going to do File, Open Folder,

72
00:03:01,60 --> 00:03:03,90
and then just go to this repository

73
00:03:03,90 --> 00:03:05,60
where I have all the code for this course.

74
00:03:05,60 --> 00:03:06,50
And I'm just going to open that

75
00:03:06,50 --> 00:03:09,50
cc folder where all that code is.

76
00:03:09,50 --> 00:03:11,40
So now I have this folder open and we'll just

77
00:03:11,40 --> 00:03:14,60
open up a basic one, main.go hello.

78
00:03:14,60 --> 00:03:18,90
And we can try to type some code in and see what happens.

79
00:03:18,90 --> 00:03:21,50
And so I have code completion working, and that is

80
00:03:21,50 --> 00:03:24,80
one of my favorite things when I'm writing any kind of code,

81
00:03:24,80 --> 00:03:27,50
so code completion, and that's fantastic.

82
00:03:27,50 --> 00:03:28,60
So I'm just going to do

83
00:03:28,60 --> 00:03:33,00
"Hello Again, Atom.io is working."

84
00:03:33,00 --> 00:03:36,90
And we might try one more thing, so I'm going to take out

85
00:03:36,90 --> 00:03:40,10
this import statement just to see if goimports is working.

86
00:03:40,10 --> 00:03:42,90
And so now when I save my file, you can see

87
00:03:42,90 --> 00:03:45,60
it automatically put the import statement back in.

88
00:03:45,60 --> 00:03:48,50
So this is basic Atom functionality.

89
00:03:48,50 --> 00:03:50,80
You could certainly look around within Atom to see

90
00:03:50,80 --> 00:03:53,50
if there are other plug-ins that you might want to install,

91
00:03:53,50 --> 00:03:55,80
other packages that you want to install to help make writing

92
00:03:55,80 --> 00:04:00,00
your code more effective and make writing code easier.

93
00:04:00,00 --> 00:04:03,00
And you could also, of course, look to see what other tools

94
00:04:03,00 --> 00:04:04,90
are out there that you could download,

95
00:04:04,90 --> 00:04:07,80
what other packages are out there, third party packages

96
00:04:07,80 --> 00:04:11,20
that you could download to help write gocode more easily.

97
00:04:11,20 --> 00:04:15,00
So there's packages that you use in Atom, and then

98
00:04:15,00 --> 00:04:18,80
there are also packages that have been written for gocode,

99
00:04:18,80 --> 00:04:21,60
like goimports, which Atom then uses.

100
00:04:21,60 --> 00:04:22,60
So you can look around, see if

101
00:04:22,60 --> 00:04:24,30
there's more that you want to use.

102
00:04:24,30 --> 00:04:26,40
But that's the basics of getting Atom

103
00:04:26,40 --> 00:04:28,00
up and running on Windows.

