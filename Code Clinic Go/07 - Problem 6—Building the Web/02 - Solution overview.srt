1
00:00:00,50 --> 00:00:02,70
- Here's the overview to my solution,

2
00:00:02,70 --> 00:00:05,00
and you can find that in the code

3
00:00:05,00 --> 00:00:07,50
under 06_web roux-academy.

4
00:00:07,50 --> 00:00:10,10
I've included an additional file right here

5
00:00:10,10 --> 00:00:12,50
which refactors the data, just to show you

6
00:00:12,50 --> 00:00:14,40
hey, there's another way that we could have been

7
00:00:14,40 --> 00:00:16,80
passing our data into our template.

8
00:00:16,80 --> 00:00:18,60
But the full, complete solution

9
00:00:18,60 --> 00:00:20,90
where you see the web page being built

10
00:00:20,90 --> 00:00:24,40
and have the data being merged into the template,

11
00:00:24,40 --> 00:00:28,60
that's all found right here in 11-roux-academy.

12
00:00:28,60 --> 00:00:30,50
So in addition to giving you a solution

13
00:00:30,50 --> 00:00:32,70
overview in this video, I also want to give you

14
00:00:32,70 --> 00:00:34,90
some really valuable resources.

15
00:00:34,90 --> 00:00:36,70
Let's take a look at the solution.

16
00:00:36,70 --> 00:00:39,10
First, we'll just run it, and you'll notice right here

17
00:00:39,10 --> 00:00:42,00
I just have the regular htm file.

18
00:00:42,00 --> 00:00:44,40
If I run this htm file right now,

19
00:00:44,40 --> 00:00:46,00
I'm just going to open it up and run it,

20
00:00:46,00 --> 00:00:47,40
this is what it looks like.

21
00:00:47,40 --> 00:00:49,10
We just have some templating right there.

22
00:00:49,10 --> 00:00:51,00
You can see where I want to put in my fields.

23
00:00:51,00 --> 00:00:53,50
I'm going to run this program.

24
00:00:53,50 --> 00:00:56,00
I've created a program right here; main.go.

25
00:00:56,00 --> 00:00:58,60
In main.go, I've got all of my code

26
00:00:58,60 --> 00:01:01,20
to get the data and put it into the template.

27
00:01:01,20 --> 00:01:03,00
Let me just show you the code running,

28
00:01:03,00 --> 00:01:05,40
and then we'll take a look; get a high level overview

29
00:01:05,40 --> 00:01:07,00
of how that code's working.

30
00:01:07,00 --> 00:01:08,40
I'm going to go to my terminal

31
00:01:08,40 --> 00:01:10,90
and go into 11-roux-academy,

32
00:01:10,90 --> 00:01:12,70
and then go run main.go.

33
00:01:12,70 --> 00:01:14,50
As soon as I hit run here,

34
00:01:14,50 --> 00:01:16,40
I'm quickly going to switch over here,

35
00:01:16,40 --> 00:01:18,60
and we're going to watch right in this folder

36
00:01:18,60 --> 00:01:20,70
because we're going to see a file be created.

37
00:01:20,70 --> 00:01:21,60
Are you ready?

38
00:01:21,60 --> 00:01:22,90
Here we go.

39
00:01:22,90 --> 00:01:24,80
And there's that file, it came up.

40
00:01:24,80 --> 00:01:26,10
All right, so we've got that file.

41
00:01:26,10 --> 00:01:28,40
Let's open it up and preview it in a web browser,

42
00:01:28,40 --> 00:01:30,20
and this is kind of a mysterious thing

43
00:01:30,20 --> 00:01:31,80
about WebStorm, by the way:

44
00:01:31,80 --> 00:01:34,20
this little icon right there.

45
00:01:34,20 --> 00:01:36,30
It isn't there at first, you just kind of have to

46
00:01:36,30 --> 00:01:38,40
know about it, and as you start moving towards it,

47
00:01:38,40 --> 00:01:41,20
it appears and you click it and it launches in your browser.

48
00:01:41,20 --> 00:01:43,70
All right, so the code ran, and it built out

49
00:01:43,70 --> 00:01:45,70
these two tables with all of the data.

50
00:01:45,70 --> 00:01:47,00
So how did we do that?

51
00:01:47,00 --> 00:01:49,10
What's the high level overview?

52
00:01:49,10 --> 00:01:53,50
The high level overview.

53
00:01:53,50 --> 00:01:55,80
I've created a couple of data structures here

54
00:01:55,80 --> 00:01:58,60
to hold data, I created a struct with three fields:

55
00:01:58,60 --> 00:02:01,10
Number, Name and Units, and so that's going to be

56
00:02:01,10 --> 00:02:03,00
where I'm storing my data,

57
00:02:03,00 --> 00:02:04,90
and then I created another struct, semester,

58
00:02:04,90 --> 00:02:07,90
to hold the term, and then a slice of courses,

59
00:02:07,90 --> 00:02:10,50
and so that will just be a bunch of different courses.

60
00:02:10,50 --> 00:02:13,30
Once I have those data structures,

61
00:02:13,30 --> 00:02:14,80
I'm going to go get the data,

62
00:02:14,80 --> 00:02:17,10
and I'm going to parse my templates,

63
00:02:17,10 --> 00:02:19,90
and then I'm going to create an index html file,

64
00:02:19,90 --> 00:02:21,70
and then I'm going to execute my template

65
00:02:21,70 --> 00:02:23,90
and pass the data into it.

66
00:02:23,90 --> 00:02:27,00
So a lot of the code in this is getting the data

67
00:02:27,00 --> 00:02:29,60
and making sure the data comes out in the data structure,

68
00:02:29,60 --> 00:02:31,20
which I want.

69
00:02:31,20 --> 00:02:33,30
That all happens here, academicYear,

70
00:02:33,30 --> 00:02:35,40
where I'm passing in those two files.

71
00:02:35,40 --> 00:02:37,50
One of the first things I did was,

72
00:02:37,50 --> 00:02:40,30
I saw that these csv files had some other kind

73
00:02:40,30 --> 00:02:42,20
of encoding stuff going on in them,

74
00:02:42,20 --> 00:02:44,40
so I just copied that data into a text file,

75
00:02:44,40 --> 00:02:46,80
and that got rid of those weird encoding things

76
00:02:46,80 --> 00:02:48,00
that came out with it.

77
00:02:48,00 --> 00:02:50,80
So a lot of the code here is just coming down

78
00:02:50,80 --> 00:02:54,10
and going through these files and getting that data out.

79
00:02:54,10 --> 00:02:56,00
That's everything I'm doing down here

80
00:02:56,00 --> 00:02:57,90
where I'm working with strings to pull out

81
00:02:57,90 --> 00:02:59,90
the correct data, and then putting it into

82
00:02:59,90 --> 00:03:03,10
the right data structures for passing into my template.

83
00:03:03,10 --> 00:03:05,00
And then, this part right here is where I'm

84
00:03:05,00 --> 00:03:06,50
actually getting the records.

85
00:03:06,50 --> 00:03:08,60
I'm opening a file, I'm reading it,

86
00:03:08,60 --> 00:03:10,80
and then I'm returning all of my records right there,

87
00:03:10,80 --> 00:03:13,00
slice of slice strings.

