1
00:00:00,60 --> 00:00:02,70
- So the next step for us in the process

2
00:00:02,70 --> 00:00:06,30
is to take our data and to be able to pass that

3
00:00:06,30 --> 00:00:08,70
into the template when it executes

4
00:00:08,70 --> 00:00:12,10
so that we can then merge our data with our text,

5
00:00:12,10 --> 00:00:16,30
our form letter, our spam emails, our HTML web pages.

6
00:00:16,30 --> 00:00:18,00
And to do that, we're going to use

7
00:00:18,00 --> 00:00:20,20
this concept of pipelining.

8
00:00:20,20 --> 00:00:22,10
So we're going to be looking at these files,

9
00:00:22,10 --> 00:00:24,30
and right now, we're looking at this main.go

10
00:00:24,30 --> 00:00:26,50
from right here where I have some data structures.

11
00:00:26,50 --> 00:00:27,60
But first, I sort of want to take

12
00:00:27,60 --> 00:00:29,40
a quick look at pipelining.

13
00:00:29,40 --> 00:00:31,80
So pipelining is this concept in Unix

14
00:00:31,80 --> 00:00:33,60
where the output of one process

15
00:00:33,60 --> 00:00:36,00
becomes the input of the next process,

16
00:00:36,00 --> 00:00:37,40
and so there's your pipe, and it's

17
00:00:37,40 --> 00:00:39,40
also like a pipeline because it's

18
00:00:39,40 --> 00:00:41,40
just going down the pipe this way.

19
00:00:41,40 --> 00:00:43,00
So it'll be like, hey the output of this program

20
00:00:43,00 --> 00:00:44,90
becomes the input of this program.

21
00:00:44,90 --> 00:00:45,90
The output of this program

22
00:00:45,90 --> 00:00:47,70
becomes the input of that program,

23
00:00:47,70 --> 00:00:50,70
and so we could see that in action with passing data

24
00:00:50,70 --> 00:00:53,20
into templates where if I have these data structures,

25
00:00:53,20 --> 00:00:56,00
and you know, I pass year in my template

26
00:00:56,00 --> 00:00:58,90
when it executes, years made up of a bunch of semesters,

27
00:00:58,90 --> 00:01:01,30
a semester's made up of a term of a course,

28
00:01:01,30 --> 00:01:04,20
and then I come over here and I could use this dot notation.

29
00:01:04,20 --> 00:01:06,20
So give me the fall, and this is from year,

30
00:01:06,20 --> 00:01:08,50
and then give me the term, right?

31
00:01:08,50 --> 00:01:11,90
So I get fall from that because I passed a year,

32
00:01:11,90 --> 00:01:15,30
or here I'd say, okay, give me fall, and then courses.

33
00:01:15,30 --> 00:01:18,10
And since courses is a slice, I'm going to

34
00:01:18,10 --> 00:01:21,30
range over that, and this is where the loop would end

35
00:01:21,30 --> 00:01:23,40
right here, and as I range over that,

36
00:01:23,40 --> 00:01:27,50
the output of this becomes the input of this.

37
00:01:27,50 --> 00:01:29,60
And so it's kind of like my dot changes

38
00:01:29,60 --> 00:01:33,00
depending upon the context, and that's the thing here.

39
00:01:33,00 --> 00:01:35,80
The output of ranging over this, when I come down here,

40
00:01:35,80 --> 00:01:41,10
I no longer have to do .Fall, .Courses, and then go .Number

41
00:01:41,10 --> 00:01:42,30
If you go to the standard documentation,

42
00:01:42,30 --> 00:01:46,20
here I'm at text template, and if I scroll down,

43
00:01:46,20 --> 00:01:47,90
here's some information about pipelines

44
00:01:47,90 --> 00:01:50,50
and some of the notation, and you could do

45
00:01:50,50 --> 00:01:53,70
a lot of interesting things with it, and if you then

46
00:01:53,70 --> 00:01:57,20
go over and look at instead of text template right here,

47
00:01:57,20 --> 00:01:59,70
if you go over and look at HTML template, you see that

48
00:01:59,70 --> 00:02:02,30
that same pipelining documentation isn't here

49
00:02:02,30 --> 00:02:05,20
and that's because HTML template

50
00:02:05,20 --> 00:02:07,60
builds on top of text template,

51
00:02:07,60 --> 00:02:09,20
so they describe it in text template

52
00:02:09,20 --> 00:02:12,00
and then in HTML template, if you want to understand that,

53
00:02:12,00 --> 00:02:13,40
go look back at text template

54
00:02:13,40 --> 00:02:16,10
because HTML template is built off of that.

55
00:02:16,10 --> 00:02:17,50
And then one thing that I want to point out here,

56
00:02:17,50 --> 00:02:20,50
they do have a little example of that escaping

57
00:02:20,50 --> 00:02:23,30
to prevent cross-site scripting that

58
00:02:23,30 --> 00:02:25,70
I was talking about with HTML template.

59
00:02:25,70 --> 00:02:29,10
If I'm passing in this phrase right here

60
00:02:29,10 --> 00:02:33,20
with some tags in it, and I give that data

61
00:02:33,20 --> 00:02:35,30
when it comes, what's it going to look like?

62
00:02:35,30 --> 00:02:37,30
So just in the most basic escape,

63
00:02:37,30 --> 00:02:39,10
based upon the context, it's gonna

64
00:02:39,10 --> 00:02:40,50
escape it differently.

65
00:02:40,50 --> 00:02:42,40
So if it was in just like, hey, put this data

66
00:02:42,40 --> 00:02:44,00
on the web page, that's how it's going to

67
00:02:44,00 --> 00:02:47,30
put it on the web page or if it's inside an anchor tag,

68
00:02:47,30 --> 00:02:49,50
well then, you'd get a different thing.

69
00:02:49,50 --> 00:02:51,90
Alright, so let's take a look at the basics

70
00:02:51,90 --> 00:02:54,60
of passing data and the templates.

71
00:02:54,60 --> 00:02:56,40
And so here's my first file.

72
00:02:56,40 --> 00:02:59,20
I'm going to parse files, and I'm parsing my template,

73
00:02:59,20 --> 00:03:01,90
and then I get my variable, whatever I'm going to call it,

74
00:03:01,90 --> 00:03:03,40
and then I'm going to execute my template,

75
00:03:03,40 --> 00:03:05,90
so parse it, execute it, and I'm going to say

76
00:03:05,90 --> 00:03:07,20
execute it standard out,

77
00:03:07,20 --> 00:03:09,20
and this is the data I'm passing in.

78
00:03:09,20 --> 00:03:10,20
Five times five.

79
00:03:10,20 --> 00:03:12,40
I could pass in 25, but I just thought

80
00:03:12,40 --> 00:03:14,90
it'd be interesting to pass in five times five.

81
00:03:14,90 --> 00:03:18,10
And so when I pass that in, I'm going to say print it.

82
00:03:18,10 --> 00:03:19,10
That's my current data.

83
00:03:19,10 --> 00:03:21,50
So dot is kind of like the current position.

84
00:03:21,50 --> 00:03:24,00
Alright, so let's run this, and see what it does.

85
00:03:24,00 --> 00:03:25,90
We want 06, 03.

86
00:03:25,90 --> 00:03:29,70
So I'm going to change directories and see where I'm at,

87
00:03:29,70 --> 00:03:33,70
and then change directories again, and then I'm in 06.

88
00:03:33,70 --> 00:03:34,80
I want to go to 03.

89
00:03:34,80 --> 00:03:36,90
Go run main.go.

90
00:03:36,90 --> 00:03:39,60
And so I run it, it did that little calculation,

91
00:03:39,60 --> 00:03:42,30
and then it passed the value n and it printed it out.

92
00:03:42,30 --> 00:03:44,50
So that's super sweet that we were able

93
00:03:44,50 --> 00:03:47,40
to pass data to a template, and have it print.

94
00:03:47,40 --> 00:03:49,10
In the next example, I'm going to pass in

95
00:03:49,10 --> 00:03:52,40
a slice of numbers such as like a list,

96
00:03:52,40 --> 00:03:54,40
and I want to range over this.

97
00:03:54,40 --> 00:03:56,90
So I'm going to range over what I passed in,

98
00:03:56,90 --> 00:04:01,30
and then the output of this becomes the input of that.

99
00:04:01,30 --> 00:04:02,90
So the current element of this

100
00:04:02,90 --> 00:04:05,00
range will be the output of this.

101
00:04:05,00 --> 00:04:07,40
So let's watch that round, and that's 04.

102
00:04:07,40 --> 00:04:10,00
Change directories, 04.

103
00:04:10,00 --> 00:04:11,30
Clear my screen.

104
00:04:11,30 --> 00:04:16,00
Go run main.go, and I have a one, two, three, four, five.

105
00:04:16,00 --> 00:04:17,20
Pretty cool.

106
00:04:17,20 --> 00:04:18,50
It's not that difficult.

107
00:04:18,50 --> 00:04:19,50
It's not that complicated,

108
00:04:19,50 --> 00:04:21,40
and it does take some getting used to.

109
00:04:21,40 --> 00:04:23,30
So this next one, I'm just going to pass in

110
00:04:23,30 --> 00:04:26,40
an empty slice, so it's not going to have any data,

111
00:04:26,40 --> 00:04:28,80
and I'm going to range over it, but I'm going to

112
00:04:28,80 --> 00:04:31,00
use an else, and so if there's nothing there,

113
00:04:31,00 --> 00:04:32,90
then it's going to print out no items.

114
00:04:32,90 --> 00:04:35,40
So this is just showing you the else clause,

115
00:04:35,40 --> 00:04:36,90
and we'll go to 05.

116
00:04:36,90 --> 00:04:39,90
Go run main, and no item prints out.

117
00:04:39,90 --> 00:04:42,00
So that's the basics of pipelining

118
00:04:42,00 --> 00:04:43,90
and passing data into your template.

119
00:04:43,90 --> 00:04:45,80
Got more cool stuff to learn.

120
00:04:45,80 --> 00:04:47,70
In the next couple of videos, we're going to

121
00:04:47,70 --> 00:04:50,80
keep building our solution until we get up to this

122
00:04:50,80 --> 00:04:53,00
awesome Roux Academy example.

