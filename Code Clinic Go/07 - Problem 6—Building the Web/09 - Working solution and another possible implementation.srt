1
00:00:00,30 --> 00:00:02,80
- So we've got all of the hard code written,

2
00:00:02,80 --> 00:00:05,20
and we have our data structure,

3
00:00:05,20 --> 00:00:07,40
and we're getting back our data,

4
00:00:07,40 --> 00:00:09,40
and we're going to parse a template.

5
00:00:09,40 --> 00:00:11,30
And then we're going to Create a file,

6
00:00:11,30 --> 00:00:13,50
and then we're going to execute our template,

7
00:00:13,50 --> 00:00:15,50
and write to that new file we created,

8
00:00:15,50 --> 00:00:18,20
the data, right, so we'll pass the data into that.

9
00:00:18,20 --> 00:00:20,30
And so we'll take the template,

10
00:00:20,30 --> 00:00:22,40
which is just this sort of html,

11
00:00:22,40 --> 00:00:23,60
into which we're going to embed

12
00:00:23,60 --> 00:00:26,30
all of our little curly braces, where the data should go.

13
00:00:26,30 --> 00:00:27,60
That'll be our form letter.

14
00:00:27,60 --> 00:00:29,60
And then we're going to Create a new file,

15
00:00:29,60 --> 00:00:31,60
and we'll call it index.html.

16
00:00:31,60 --> 00:00:33,10
And you'll notice the path here,

17
00:00:33,10 --> 00:00:35,20
I'm just going to drop it into the same directory

18
00:00:35,20 --> 00:00:36,80
where the template is stored.

19
00:00:36,80 --> 00:00:38,00
And I'm putting it there

20
00:00:38,00 --> 00:00:40,30
because there's certain html code

21
00:00:40,30 --> 00:00:43,50
that is referencing things in a directory,

22
00:00:43,50 --> 00:00:46,70
kind of, relative way, that needs to be in that folder.

23
00:00:46,70 --> 00:00:48,10
So I'm going to put it right there.

24
00:00:48,10 --> 00:00:50,60
And then we'll just pass the data in, and execute it.

25
00:00:50,60 --> 00:00:52,20
So we've done a lot of the hard coding,

26
00:00:52,20 --> 00:00:54,00
now all we have to do is go in here,

27
00:00:54,00 --> 00:00:55,60
and look through all of this html,

28
00:00:55,60 --> 00:00:57,70
and find the parts that we want to change.

29
00:00:57,70 --> 00:01:00,60
And so once you've sort of taken a look at the page,

30
00:01:00,60 --> 00:01:01,60
and analyzed the page,

31
00:01:01,60 --> 00:01:03,00
you'll see what's happening

32
00:01:03,00 --> 00:01:06,40
is that a table is being created for each Term.

33
00:01:06,40 --> 00:01:09,70
And so since we're ranging over semesters or Terms,

34
00:01:09,70 --> 00:01:11,50
we'll create a table for each Term,

35
00:01:11,50 --> 00:01:13,00
and then when we're in here,

36
00:01:13,00 --> 00:01:14,70
we're going to print out the Term,

37
00:01:14,70 --> 00:01:16,50
so remember in our data structure

38
00:01:16,50 --> 00:01:19,70
that academic year is going to return

39
00:01:19,70 --> 00:01:21,30
a slice of semester.

40
00:01:21,30 --> 00:01:23,00
So as we range over this,

41
00:01:23,00 --> 00:01:25,10
that'll put a this, becomes a semester.

42
00:01:25,10 --> 00:01:28,50
And then a semester has Term and Courses in it,

43
00:01:28,50 --> 00:01:30,40
so I can now access the output of this,

44
00:01:30,40 --> 00:01:33,80
becomes the current input of the next inner area,

45
00:01:33,80 --> 00:01:36,60
and that means that this is a semester with Term.

46
00:01:36,60 --> 00:01:39,00
I can access Term from a semester,

47
00:01:39,00 --> 00:01:41,20
and I could also access Courses.

48
00:01:41,20 --> 00:01:44,20
So once I've set up, sort of, the top part of the table,

49
00:01:44,20 --> 00:01:47,30
I could range over this Courses part,

50
00:01:47,30 --> 00:01:48,80
and the the output of this

51
00:01:48,80 --> 00:01:51,20
becomes the input of the next stuff.

52
00:01:51,20 --> 00:01:53,40
And so what is the output of a slice of courses?

53
00:01:53,40 --> 00:01:54,90
Well it's going to be a Course.

54
00:01:54,90 --> 00:01:57,80
So as I range over a slice of Courses,

55
00:01:57,80 --> 00:02:00,40
I'm going to have a bunch of Courses that print out.

56
00:02:00,40 --> 00:02:03,90
I'll be able to access the number, and name, and units.

57
00:02:03,90 --> 00:02:06,20
So that's how that pipelining thing is working.

58
00:02:06,20 --> 00:02:08,00
So once I get all that put in there,

59
00:02:08,00 --> 00:02:09,90
it should build all of my html.

60
00:02:09,90 --> 00:02:11,10
So let's watch that work.

61
00:02:11,10 --> 00:02:13,40
And so I'm going to go change directories,

62
00:02:13,40 --> 00:02:15,10
and I think that that does it for me,

63
00:02:15,10 --> 00:02:17,40
and then go run main.go, and when I run this,

64
00:02:17,40 --> 00:02:20,50
I'll see an index.html file show up right there.

65
00:02:20,50 --> 00:02:22,50
And somewhere around line 34,

66
00:02:22,50 --> 00:02:25,60
I'm going to see my code that has been created.

67
00:02:25,60 --> 00:02:28,00
So there it is, there's that big space I had in there,

68
00:02:28,00 --> 00:02:30,20
and it inserted all of that data.

69
00:02:30,20 --> 00:02:31,50
You can see all of the Courses,

70
00:02:31,50 --> 00:02:33,30
and then it started a new table,

71
00:02:33,30 --> 00:02:34,60
and there's the second semester,

72
00:02:34,60 --> 00:02:35,90
and it did all the Courses.

73
00:02:35,90 --> 00:02:37,40
And when I view this,

74
00:02:37,40 --> 00:02:39,80
it looks just like we'd like it to look.

75
00:02:39,80 --> 00:02:42,00
So when the web began,

76
00:02:42,00 --> 00:02:43,60
this is my lament, alright?

77
00:02:43,60 --> 00:02:46,60
When the web began, it was like this awesome place

78
00:02:46,60 --> 00:02:48,90
where anybody could build a webpage.

79
00:02:48,90 --> 00:02:50,30
And that was so cool,

80
00:02:50,30 --> 00:02:52,60
because anybody could go out there and share information.

81
00:02:52,60 --> 00:02:55,80
And you still can, but it's different.

82
00:02:55,80 --> 00:02:58,40
If you want to do it in a way that, hey that looks great,

83
00:02:58,40 --> 00:03:00,30
there's a little bit of complexity.

84
00:03:00,30 --> 00:03:01,80
That's my lament.

85
00:03:01,80 --> 00:03:03,10
I'm done lamenting now.

86
00:03:03,10 --> 00:03:04,40
The last thing I'm going to leave you with,

87
00:03:04,40 --> 00:03:06,90
is just one more thought in this solution.

88
00:03:06,90 --> 00:03:09,40
And one of the places which is always

89
00:03:09,40 --> 00:03:12,70
my sort of, doh moment, or whatever,

90
00:03:12,70 --> 00:03:14,80
right, is like, what data structure

91
00:03:14,80 --> 00:03:16,40
do I want to pass to my template?

92
00:03:16,40 --> 00:03:18,00
And so in that data structure,

93
00:03:18,00 --> 00:03:19,40
it worked to pass in a slice,

94
00:03:19,40 --> 00:03:21,00
and in the slice of semester,

95
00:03:21,00 --> 00:03:22,80
and then we were looping over the semester.

96
00:03:22,80 --> 00:03:25,20
But you might also want to do something like this.

97
00:03:25,20 --> 00:03:26,30
And I put that together

98
00:03:26,30 --> 00:03:28,30
in a little bit of a README file right here.

99
00:03:28,30 --> 00:03:30,80
You could add, like, this struct, and then I thought,

100
00:03:30,80 --> 00:03:32,40
"Well I'm just going to build it and show it to you."

101
00:03:32,40 --> 00:03:33,60
You could add this struct

102
00:03:33,60 --> 00:03:37,50
where you have the different Fall, Spring, Summer semester,

103
00:03:37,50 --> 00:03:39,50
and so once you have that,

104
00:03:39,50 --> 00:03:41,90
you could then say .Fall.Term,

105
00:03:41,90 --> 00:03:43,70
and I could do that in one place,

106
00:03:43,70 --> 00:03:46,70
and then I could do .Spring.Term, and do that in another.

107
00:03:46,70 --> 00:03:48,50
So if I wasn't going to be putting these

108
00:03:48,50 --> 00:03:50,70
right next to each other in my template,

109
00:03:50,70 --> 00:03:52,10
that's how I would do it,

110
00:03:52,10 --> 00:03:53,90
so that they could be in different places.

111
00:03:53,90 --> 00:03:55,60
So when you pass a slice,

112
00:03:55,60 --> 00:03:58,50
like we passed a slice of semester, right,

113
00:03:58,50 --> 00:04:00,10
academicYear gave us semesters,

114
00:04:00,10 --> 00:04:02,30
and semesters was a slice of semester.

115
00:04:02,30 --> 00:04:04,70
When you pass a slice and you get to your template,

116
00:04:04,70 --> 00:04:07,30
you're going to be putting all that data in the same spot,

117
00:04:07,30 --> 00:04:09,10
because you're going to be looping over that slice.

118
00:04:09,10 --> 00:04:11,70
If you want to put data in a different spot,

119
00:04:11,70 --> 00:04:13,60
you need to have some sort of a struct,

120
00:04:13,60 --> 00:04:15,60
so you could call it, like, okay,

121
00:04:15,60 --> 00:04:17,60
I'm just calling Fall stuff,

122
00:04:17,60 --> 00:04:19,20
and it's going in this one area.

123
00:04:19,20 --> 00:04:22,40
I'm calling Spring stuff, and it's going in this other area.

124
00:04:22,40 --> 00:04:24,20
That's the one thing I also wanted to add,

125
00:04:24,20 --> 00:04:26,70
as something for you to think about, and chew on.

126
00:04:26,70 --> 00:04:29,70
And definitely one of the things I had as a stumbling block,

127
00:04:29,70 --> 00:04:31,00
learning all this stuff,

128
00:04:31,00 --> 00:04:33,10
was like what data structure to get the data

129
00:04:33,10 --> 00:04:35,20
where I want it to be, to do what I want it to do?

130
00:04:35,20 --> 00:04:37,10
I was just thinking about it in that way.

131
00:04:37,10 --> 00:04:38,90
I hope that's helpful for you.

132
00:04:38,90 --> 00:04:42,30
I hope that you've enjoyed this Code Clinic Challenge,

133
00:04:42,30 --> 00:04:44,80
and my main goal is just been to help you out,

134
00:04:44,80 --> 00:04:47,20
and that's why I've given you all of these resources,

135
00:04:47,20 --> 00:04:48,50
different places you could see

136
00:04:48,50 --> 00:04:50,20
some really great trainings online

137
00:04:50,20 --> 00:04:52,40
to help you learn web programming with GO.

138
00:04:52,40 --> 00:04:54,10
GO is an awesome language.

139
00:04:54,10 --> 00:04:55,70
It has amazing credentials.

140
00:04:55,70 --> 00:04:57,00
It was built by geniuses.

141
00:04:57,00 --> 00:04:59,50
It was built by one of the best software engineering firms

142
00:04:59,50 --> 00:05:01,10
ever to have existed.

143
00:05:01,10 --> 00:05:02,60
In my opinion,

144
00:05:02,60 --> 00:05:06,00
it is the very best language you can be using today

145
00:05:06,00 --> 00:05:07,10
to do web programming.

146
00:05:07,10 --> 00:05:08,80
This is what it was built for.

147
00:05:08,80 --> 00:05:11,10
And if you use it, if you learn it, and you use it,

148
00:05:11,10 --> 00:05:12,20
it will serve you well.

149
00:05:12,20 --> 00:05:13,90
The one last thought I've got to leave you with,

150
00:05:13,90 --> 00:05:16,40
is anytime somebody

151
00:05:16,40 --> 00:05:18,70
strives to improve themselves,

152
00:05:18,70 --> 00:05:20,60
that's making the world a better place.

153
00:05:20,60 --> 00:05:23,10
So good on you, as the Australians say.

154
00:05:23,10 --> 00:05:25,40
Good on you for taking the time

155
00:05:25,40 --> 00:05:28,40
to invest in improving your knowledge,

156
00:05:28,40 --> 00:05:29,70
and becoming more skilled,

157
00:05:29,70 --> 00:05:32,50
and becoming more capable of helping others

158
00:05:32,50 --> 00:05:33,90
and contributing to the world.

159
00:05:33,90 --> 00:05:35,30
And that's no small feat.

160
00:05:35,30 --> 00:05:37,70
So you're doing something that not everybody does,

161
00:05:37,70 --> 00:05:39,50
and that's why I love education.

162
00:05:39,50 --> 00:05:42,60
I love to see people acquiring new skills,

163
00:05:42,60 --> 00:05:44,40
and just forwarding their skillset and their knowledge,

164
00:05:44,40 --> 00:05:46,40
and then being able to take that out into the world,

165
00:05:46,40 --> 00:05:48,30
and do good things to help people.

166
00:05:48,30 --> 00:05:50,30
So, good luck to you on your journeys.

167
00:05:50,30 --> 00:05:51,20
It's been awesome

168
00:05:51,20 --> 00:05:53,00
being able to share this knowledge with you,

169
00:05:53,00 --> 00:05:55,20
and I had hoped that has an instructor,

170
00:05:55,20 --> 00:05:58,40
I've been able to convey some really useful things to you,

171
00:05:58,40 --> 00:06:01,00
and that your knowledge and skillset with GO

172
00:06:01,00 --> 00:06:04,60
is at a much farther along place than where it was

173
00:06:04,60 --> 00:06:06,20
when we both started out on this journey.

174
00:06:06,20 --> 00:06:08,70
So thanks for joining me, and like I said,

175
00:06:08,70 --> 00:06:10,40
if you build something, tweet me.

176
00:06:10,40 --> 00:06:11,80
Show it to me, say check it out.

177
00:06:11,80 --> 00:06:12,90
I'd love to see it.

178
00:06:12,90 --> 00:06:14,50
Good luck to you on your journeys,

179
00:06:14,50 --> 00:06:17,00
and thanks again, and I wish you well.

