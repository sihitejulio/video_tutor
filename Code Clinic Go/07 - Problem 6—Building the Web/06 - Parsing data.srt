1
00:00:00,40 --> 00:00:02,10
- So I really want to lay the foundations

2
00:00:02,10 --> 00:00:04,20
for how you do web programming in Go

3
00:00:04,20 --> 00:00:05,70
because this is one of the things

4
00:00:05,70 --> 00:00:08,10
that Go does really, really well.

5
00:00:08,10 --> 00:00:10,00
Go is built to operate

6
00:00:10,00 --> 00:00:12,40
in a distributed networked environment

7
00:00:12,40 --> 00:00:14,20
and to be super performant.

8
00:00:14,20 --> 00:00:15,90
That's pretty much what Google does,

9
00:00:15,90 --> 00:00:18,30
you know, software running on the web,

10
00:00:18,30 --> 00:00:21,00
and they built Go to be able to handle that

11
00:00:21,00 --> 00:00:23,20
and to help do that more effectively,

12
00:00:23,20 --> 00:00:25,20
and so, I really want to convey to you

13
00:00:25,20 --> 00:00:28,90
how to do web programming with Go and how easy it is,

14
00:00:28,90 --> 00:00:31,80
and I hope that you take this knowledge that you're gaining

15
00:00:31,80 --> 00:00:33,90
and you build something amazing,

16
00:00:33,90 --> 00:00:36,60
but let me just illustrate here one of the statistics

17
00:00:36,60 --> 00:00:39,70
which was super compelling and is super compelling to me,

18
00:00:39,70 --> 00:00:42,60
and when I was looking around for a server-side language,

19
00:00:42,60 --> 00:00:45,40
should it be Node, should it be Ruby, should it be Python.

20
00:00:45,40 --> 00:00:47,90
Those were all some of the choices I was considering

21
00:00:47,90 --> 00:00:50,80
and Go, it's the thing which is the best,

22
00:00:50,80 --> 00:00:52,90
and so here's one of the case studies.

23
00:00:52,90 --> 00:00:56,70
This appeared July 6, 2015 in the Go blog

24
00:00:56,70 --> 00:01:00,20
but just look at the statistics on this, right here.

25
00:01:00,20 --> 00:01:03,40
It handles 200 million real-time connections

26
00:01:03,40 --> 00:01:06,90
and serves over 10 billion messages daily.

27
00:01:06,90 --> 00:01:09,20
So, wow, that's pretty incredible.

28
00:01:09,20 --> 00:01:11,00
So, that's some of the statistics.

29
00:01:11,00 --> 00:01:13,60
Now what I want to do is actually the meat of this all

30
00:01:13,60 --> 00:01:15,40
and to do that, a large part of our work

31
00:01:15,40 --> 00:01:17,90
is going to be just parsing the data.

32
00:01:17,90 --> 00:01:19,70
We've been given some data here,

33
00:01:19,70 --> 00:01:22,50
and here is that piece of data right there,

34
00:01:22,50 --> 00:01:26,10
and we need to get that out of this csv file.

35
00:01:26,10 --> 00:01:28,20
We need to get that into some sort of data structure,

36
00:01:28,20 --> 00:01:31,30
which we can then use to pass into our templates.

37
00:01:31,30 --> 00:01:32,80
So that's what we're shooting for.

38
00:01:32,80 --> 00:01:34,00
I have two text files.

39
00:01:34,00 --> 00:01:35,30
That one and this one.

40
00:01:35,30 --> 00:01:38,90
I'm going to start off just working with first_semester.csv

41
00:01:38,90 --> 00:01:40,60
and when I first tried to parse this,

42
00:01:40,60 --> 00:01:43,30
it gave me some pretty funny looking characters.

43
00:01:43,30 --> 00:01:46,80
So right here you can see I'm doing data/first_semester.txt.

44
00:01:46,80 --> 00:01:50,00
I'm going to do csv first and let's take a look at that.

45
00:01:50,00 --> 00:01:52,10
So I'm going to come over to my terminal

46
00:01:52,10 --> 00:01:55,70
and I'm in 08_parse-csv/01_csv-Reader/

47
00:01:55,70 --> 00:01:57,90
That's right where I want to be.

48
00:01:57,90 --> 00:02:00,60
Go run main.go and when I run this,

49
00:02:00,60 --> 00:02:01,80
that looks kind of funny.

50
00:02:01,80 --> 00:02:03,20
And so I could try to figure that out

51
00:02:03,20 --> 00:02:04,70
and I could do some research,

52
00:02:04,70 --> 00:02:06,30
or I could try this old trick

53
00:02:06,30 --> 00:02:08,10
and sometimes the simplest solution,

54
00:02:08,10 --> 00:02:10,50
Occam's razor, is the best solution.

55
00:02:10,50 --> 00:02:11,50
Here's that old trick, right?

56
00:02:11,50 --> 00:02:13,40
I get all this text right here

57
00:02:13,40 --> 00:02:16,50
and I just opened up a new text file and I dropped it in,

58
00:02:16,50 --> 00:02:19,20
and when I parse this new text file,

59
00:02:19,20 --> 00:02:21,70
it gets rid of those wacky characters,

60
00:02:21,70 --> 00:02:24,10
so I will run this once more,

61
00:02:24,10 --> 00:02:27,20
and now the wacky characters, I got all of my data.

62
00:02:27,20 --> 00:02:31,20
So that first step was just simply to open the file,

63
00:02:31,20 --> 00:02:33,00
and then parse the csv file

64
00:02:33,00 --> 00:02:35,20
and I use csv.NewReader to do that,

65
00:02:35,20 --> 00:02:36,70
even though it's a text file.

66
00:02:36,70 --> 00:02:37,50
That totally works.

67
00:02:37,50 --> 00:02:39,50
It's just looking for some sort of a delimiter

68
00:02:39,50 --> 00:02:40,40
and if there isn't one,

69
00:02:40,40 --> 00:02:43,10
it drops that whole record into one row.

70
00:02:43,10 --> 00:02:45,90
And then I range over all my records and I get my rows.

71
00:02:45,90 --> 00:02:47,10
There's my data, it's printed out.

72
00:02:47,10 --> 00:02:48,70
So that's step one.

73
00:02:48,70 --> 00:02:52,40
Alright, on to step two, in parsing the data.

74
00:02:52,40 --> 00:02:55,10
And this step, how did I iterate on this code?

75
00:02:55,10 --> 00:02:59,00
I started to use index access to see what could I pull out.

76
00:02:59,00 --> 00:03:00,40
So what am I able to pull out?

77
00:03:00,40 --> 00:03:03,40
So let's watch this run and see what comes out.

78
00:03:03,40 --> 00:03:04,90
Change directories.

79
00:03:04,90 --> 00:03:07,50
Go on to 02.

80
00:03:07,50 --> 00:03:10,30
And now, I was able to pull out different fields in there,

81
00:03:10,30 --> 00:03:11,80
and I just experimented with it.

82
00:03:11,80 --> 00:03:13,10
So at first, I just kind of said,

83
00:03:13,10 --> 00:03:14,70
hey, give me my first field.

84
00:03:14,70 --> 00:03:17,40
And when I ran that, it gave me everything but the numbers.

85
00:03:17,40 --> 00:03:17,90
So then I said,

86
00:03:17,90 --> 00:03:22,00
well, maybe the numbers is the step field in that slice.

87
00:03:22,00 --> 00:03:24,90
And when I ran this one, once again,

88
00:03:24,90 --> 00:03:27,30
it gave me those semester unit numbers at the end,

89
00:03:27,30 --> 00:03:29,80
so I figured okay, there we go, I got my numbers.

90
00:03:29,80 --> 00:03:31,10
So the next step after that

91
00:03:31,10 --> 00:03:33,60
was to start splitting my string up,

92
00:03:33,60 --> 00:03:36,60
and you can see here how I started to do that.

93
00:03:36,60 --> 00:03:38,80
So we could take a look at how SplitN works.

94
00:03:38,80 --> 00:03:43,10
So SplitN is going to take a string, a separator, and n.

95
00:03:43,10 --> 00:03:45,80
And we could see SplitN slices s into substrings

96
00:03:45,80 --> 00:03:48,50
separated by sep and returns a slice of the substrings

97
00:03:48,50 --> 00:03:50,30
between those separators,

98
00:03:50,30 --> 00:03:52,90
and it does it for how ever many right n's

99
00:03:52,90 --> 00:03:54,10
that I want to give to it.

100
00:03:54,10 --> 00:03:57,20
You could read the specification of that, right there.

101
00:03:57,20 --> 00:03:59,30
So, when I pass into 2 right here,

102
00:03:59,30 --> 00:04:01,70
it's going to give me two items, right?

103
00:04:01,70 --> 00:04:04,10
It'll give me one on the left, one on the right,

104
00:04:04,10 --> 00:04:07,10
so two entries, and it gives me a slice of strings.

105
00:04:07,10 --> 00:04:09,20
So I'm going to take this first item.

106
00:04:09,20 --> 00:04:13,50
Now remember that first item was this part right here,

107
00:04:13,50 --> 00:04:14,80
and I'm saying, split that,

108
00:04:14,80 --> 00:04:16,40
and I want to split it into two pieces,

109
00:04:16,40 --> 00:04:18,60
and I want it to be split on a space.

110
00:04:18,60 --> 00:04:20,30
So this will be the first piece

111
00:04:20,30 --> 00:04:21,70
and this will be the second piece.

112
00:04:21,70 --> 00:04:23,20
That's what SplitN does.

113
00:04:23,20 --> 00:04:25,10
And so, I get those two pieces

114
00:04:25,10 --> 00:04:26,90
and so then, I could say, give me that first piece,

115
00:04:26,90 --> 00:04:28,40
and give me that second piece.

116
00:04:28,40 --> 00:04:31,90
So it'll give me this one and then it will give me this one,

117
00:04:31,90 --> 00:04:34,00
and then I'm accessing that last one

118
00:04:34,00 --> 00:04:35,60
the same way I was before.

119
00:04:35,60 --> 00:04:37,00
So that's the idea behind this

120
00:04:37,00 --> 00:04:39,70
and we shouldn't see anything really change.

121
00:04:39,70 --> 00:04:43,10
So I'm going to do 03 on this one, that's 03,

122
00:04:43,10 --> 00:04:44,50
and go run main.

123
00:04:44,50 --> 00:04:46,30
And it should look pretty much the same

124
00:04:46,30 --> 00:04:47,70
but the important thing here is

125
00:04:47,70 --> 00:04:51,70
instead of this just being one entry as it was in that run,

126
00:04:51,70 --> 00:04:54,40
I now have it in as two entries.

127
00:04:54,40 --> 00:04:57,60
Alright, what's the next step in the process?

128
00:04:57,60 --> 00:05:00,20
It's I wanted to be able to access the header row,

129
00:05:00,20 --> 00:05:02,20
because if you look at the data file,

130
00:05:02,20 --> 00:05:05,00
we have here, this header row right there,

131
00:05:05,00 --> 00:05:07,10
and I want to be able to get that header row out,

132
00:05:07,10 --> 00:05:08,80
just so it's separate from the data.

133
00:05:08,80 --> 00:05:09,40
And the way I do that is

134
00:05:09,40 --> 00:05:11,70
if this is my first time through the loop,

135
00:05:11,70 --> 00:05:14,10
then I know that's going to be my header row.

136
00:05:14,10 --> 00:05:17,20
And my header row has two parts that I want.

137
00:05:17,20 --> 00:05:20,20
It has this part and then it also has that part,

138
00:05:20,20 --> 00:05:22,20
and they're separated by a comma.

139
00:05:22,20 --> 00:05:25,40
So, I'm going to split that and I want the two parts,

140
00:05:25,40 --> 00:05:26,90
and they're separated by a comma,

141
00:05:26,90 --> 00:05:29,60
and I just want to be able to have the first part,

142
00:05:29,60 --> 00:05:31,10
which is this right here,

143
00:05:31,10 --> 00:05:34,10
so this, right there, row 0.

144
00:05:34,10 --> 00:05:37,30
Remember the row 0 part was like that part right there.

145
00:05:37,30 --> 00:05:38,70
So it's giving me all of this

146
00:05:38,70 --> 00:05:40,80
because there is no number after it.

147
00:05:40,80 --> 00:05:42,20
It's giving me the first entry.

148
00:05:42,20 --> 00:05:45,40
And then xs will give me the first part of that

149
00:05:45,40 --> 00:05:48,20
when I've split it, which will be from my text file,

150
00:05:48,20 --> 00:05:50,40
it'll be this part right there, alright.

151
00:05:50,40 --> 00:05:52,90
And there's a little bit of experimentation in there.

152
00:05:52,90 --> 00:05:55,60
It's not like I just sat down and instantly wrote that,

153
00:05:55,60 --> 00:05:57,40
and was like, I know how this works.

154
00:05:57,40 --> 00:05:59,40
I understand it but let's see what I'm getting.

155
00:05:59,40 --> 00:06:01,60
How does it interact with my data, my text file?

156
00:06:01,60 --> 00:06:04,20
And so then I print the TERM: out and then down here,

157
00:06:04,20 --> 00:06:06,40
is the same stuff that we saw already before,

158
00:06:06,40 --> 00:06:08,40
and so, if that's not the first row,

159
00:06:08,40 --> 00:06:10,30
then I'm just going to print the rest of the data.

160
00:06:10,30 --> 00:06:12,70
So let's watch this run, this is 04.

161
00:06:12,70 --> 00:06:15,20
I'm going to clear this out, go into 04,

162
00:06:15,20 --> 00:06:17,70
and go run main.

163
00:06:17,70 --> 00:06:19,70
And I got TERM: First Semester,

164
00:06:19,70 --> 00:06:21,20
so that's what I was going for.

165
00:06:21,20 --> 00:06:23,90
And the last thing I did here in getting my data out

166
00:06:23,90 --> 00:06:25,80
was to abstract all this.

167
00:06:25,80 --> 00:06:30,00
I'm going to close these files and then open this up.

168
00:06:30,00 --> 00:06:32,60
And so I turned that into getRecords,

169
00:06:32,60 --> 00:06:35,30
and with getRecords, I came down here and opened the file.

170
00:06:35,30 --> 00:06:37,50
I got the records, turned a slice of strings,

171
00:06:37,50 --> 00:06:40,50
and then over here, I range over my records,

172
00:06:40,50 --> 00:06:42,80
and I print out all the results right there.

173
00:06:42,80 --> 00:06:44,70
So you could compare that to before,

174
00:06:44,70 --> 00:06:46,60
everything was in func main,

175
00:06:46,60 --> 00:06:48,80
but now I've passed all of the getting records

176
00:06:48,80 --> 00:06:49,90
into its own function,

177
00:06:49,90 --> 00:06:51,80
so I did a little bit of abstraction.

178
00:06:51,80 --> 00:06:54,50
And always, it's good to verify that things work.

179
00:06:54,50 --> 00:06:55,50
I want to make sure all this works

180
00:06:55,50 --> 00:06:58,70
before I pass this code onto you.

181
00:06:58,70 --> 00:07:00,30
And it's all working fine.

182
00:07:00,30 --> 00:07:02,10
The first part was to understand

183
00:07:02,10 --> 00:07:03,80
how to do web programming in Go,

184
00:07:03,80 --> 00:07:05,20
and to work with templates.

185
00:07:05,20 --> 00:07:08,00
And then the second part was to take the data,

186
00:07:08,00 --> 00:07:10,60
and start being able to access it.

187
00:07:10,60 --> 00:07:12,80
In the next video, we're going to get that data

188
00:07:12,80 --> 00:07:15,00
and put it into a data structure.

