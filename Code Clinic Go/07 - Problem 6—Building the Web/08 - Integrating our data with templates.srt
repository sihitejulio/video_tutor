1
00:00:00,50 --> 00:00:03,10
- We're getting really close to having our solution here.

2
00:00:03,10 --> 00:00:05,70
In the previous video, we were getting all of our data

3
00:00:05,70 --> 00:00:08,10
and we were just printing it, but in this video,

4
00:00:08,10 --> 00:00:09,60
what we're going to do is we're going to get

5
00:00:09,60 --> 00:00:11,70
all of that data and we'll take a template,

6
00:00:11,70 --> 00:00:13,70
so I'll show you a template we've created

7
00:00:13,70 --> 00:00:15,20
and we'll parse that template,

8
00:00:15,20 --> 00:00:17,50
we'll parse the files on it and then

9
00:00:17,50 --> 00:00:19,00
once we have the template, we'll execute it

10
00:00:19,00 --> 00:00:21,00
and we'll pass that data into it.

11
00:00:21,00 --> 00:00:24,30
In the previous video, all we did is we printed our data,

12
00:00:24,30 --> 00:00:26,20
in this video, we're going to pass our data

13
00:00:26,20 --> 00:00:28,60
into a template and we'll see that template

14
00:00:28,60 --> 00:00:30,30
print it to standard out.

15
00:00:30,30 --> 00:00:32,80
The next step in the process is starting to integrate

16
00:00:32,80 --> 00:00:34,90
our data into a template and we're looking

17
00:00:34,90 --> 00:00:38,00
at these files right in the 10 folder.

18
00:00:38,00 --> 00:00:41,70
I wanted to just build this right into a simple template

19
00:00:41,70 --> 00:00:43,70
before we get into the complex example,

20
00:00:43,70 --> 00:00:47,30
the whole bunch of other HTML, CSS, whatever other

21
00:00:47,30 --> 00:00:49,60
web-programing code is around it.

22
00:00:49,60 --> 00:00:51,50
Can we get this to work on its own,

23
00:00:51,50 --> 00:00:55,20
in a simple file, without obscuring what's going on

24
00:00:55,20 --> 00:00:57,80
with the whole bunch of other HTML kind of code,

25
00:00:57,80 --> 00:00:59,70
that's what I was going for, in this folder,

26
00:00:59,70 --> 00:01:00,90
in this example.

27
00:01:00,90 --> 00:01:03,20
Just to make sure I can work with that data

28
00:01:03,20 --> 00:01:05,10
in a way that makes sense.

29
00:01:05,10 --> 00:01:06,30
So, that's what we're doing,

30
00:01:06,30 --> 00:01:09,80
I'm passing in this semesters variable

31
00:01:09,80 --> 00:01:11,60
and the "semesters variable"

32
00:01:11,60 --> 00:01:14,00
is a slice, so I'm going to range over that

33
00:01:14,00 --> 00:01:16,60
and what is stored in the semesters variable,

34
00:01:16,60 --> 00:01:18,00
you rememeber that we get,

35
00:01:18,00 --> 00:01:20,30
"semesters" comes from "academic year"

36
00:01:20,30 --> 00:01:23,50
and academic year returns a slice of semester.

37
00:01:23,50 --> 00:01:26,50
We have a slice of semester which is being passed in here

38
00:01:26,50 --> 00:01:30,20
so as we range over this, each time we're going to have

39
00:01:30,20 --> 00:01:31,00
a semester.

40
00:01:31,00 --> 00:01:33,90
This is going to range over than and the dot

41
00:01:33,90 --> 00:01:36,60
is going to become a semester, pipelining

42
00:01:36,60 --> 00:01:39,00
this, the output of that into this.

43
00:01:39,00 --> 00:01:40,90
This now becomes a semester.

44
00:01:40,90 --> 00:01:42,50
Now we have a semester, we have term

45
00:01:42,50 --> 00:01:44,10
and courses available.

46
00:01:44,10 --> 00:01:47,40
I can print out term and I can also access courses

47
00:01:47,40 --> 00:01:50,10
which in of itself is a slice.

48
00:01:50,10 --> 00:01:52,10
And since that's a slice, I'm going to loop over that

49
00:01:52,10 --> 00:01:55,60
and the output of this will become the input of that.

50
00:01:55,60 --> 00:01:58,40
Now it's going to print out whatever courses are stored

51
00:01:58,40 --> 00:01:59,40
in this slice.

52
00:01:59,40 --> 00:02:01,00
Sometimes it's hard to say this stuff,

53
00:02:01,00 --> 00:02:03,40
because I should've maybe said "Whatever course",

54
00:02:03,40 --> 00:02:06,60
but it's plural, even though the type is singular

55
00:02:06,60 --> 00:02:08,10
that I created right there.

56
00:02:08,10 --> 00:02:10,70
That's the idea, let's look at it and see what happens

57
00:02:10,70 --> 00:02:11,70
when it runs.

58
00:02:11,70 --> 00:02:15,90
I am in 04-something, I think I need to go up.

59
00:02:15,90 --> 00:02:17,90
M 10, are you there, and there you are,

60
00:02:17,90 --> 00:02:21,90
and go run main, there we go, clear that out.

61
00:02:21,90 --> 00:02:24,70
Nice and so it printed out first semester

62
00:02:24,70 --> 00:02:27,60
and then all of my courses and each one is a struct,

63
00:02:27,60 --> 00:02:29,40
so that's why we have the little curlies around it,

64
00:02:29,40 --> 00:02:31,30
and then the second semester.

65
00:02:31,30 --> 00:02:33,40
That's looking pretty promising.

66
00:02:33,40 --> 00:02:36,40
In the next example here, let's see what did I do

67
00:02:36,40 --> 00:02:38,00
to iterrate on that.

68
00:02:38,00 --> 00:02:40,40
This one printed the standard out and in this one,

69
00:02:40,40 --> 00:02:43,10
I'm just going to do it to a file

70
00:02:43,10 --> 00:02:46,30
and so it's going to be pretty much the exact same code,

71
00:02:46,30 --> 00:02:48,90
the only difference here is going to be in "main",

72
00:02:48,90 --> 00:02:51,00
I'm going to also create a file.

73
00:02:51,00 --> 00:02:53,20
And so I create my file right there and instead

74
00:02:53,20 --> 00:02:56,10
of writing the standard out, I'm going to write to my file.

75
00:02:56,10 --> 00:02:59,00
I deleted the file so that when I run the second one

76
00:02:59,00 --> 00:03:00,50
again, we'll see it run.

77
00:03:00,50 --> 00:03:03,70
I'll clear this out and go into 02.

78
00:03:03,70 --> 00:03:05,10
"Go run main.go"

79
00:03:05,10 --> 00:03:06,40
and it should create that file.

80
00:03:06,40 --> 00:03:08,60
There it is and I can open that up.

81
00:03:08,60 --> 00:03:10,90
And where is my little magic,

82
00:03:10,90 --> 00:03:12,50
here out of nowhere and there it is printed out

83
00:03:12,50 --> 00:03:13,80
as a web page.

84
00:03:13,80 --> 00:03:16,30
All right, that's pretty awesome, pretty amazing,

85
00:03:16,30 --> 00:03:19,10
in the next video, we're going to see how we can take

86
00:03:19,10 --> 00:03:21,40
what we've done in all of these previous videos

87
00:03:21,40 --> 00:03:25,70
and integrate it into the Roux academy website

88
00:03:25,70 --> 00:03:28,20
which looks like this.

89
00:03:28,20 --> 00:03:30,30
We're going to build it so that we will

90
00:03:30,30 --> 00:03:34,20
dynamically fill-in all of that stuff right there.

91
00:03:34,20 --> 00:03:35,90
All right, looking forward to doing that with you

92
00:03:35,90 --> 00:03:37,00
in the next video.

