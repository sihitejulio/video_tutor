1
00:00:06,90 --> 00:00:08,80
- Hello and welcome to Code Clinic.

2
00:00:08,80 --> 00:00:10,90
My name is Todd McLeod.

3
00:00:10,90 --> 00:00:12,90
Code Clinic is a course where a unique problem

4
00:00:12,90 --> 00:00:16,00
is introduced to a collection of lynda.com authors.

5
00:00:16,00 --> 00:00:18,60
In response, each author will create a solution

6
00:00:18,60 --> 00:00:21,30
using their programming language of choice.

7
00:00:21,30 --> 00:00:23,30
You can learn several things from Code Clinic,

8
00:00:23,30 --> 00:00:25,10
different approaches to solving a problem,

9
00:00:25,10 --> 00:00:27,40
the pros and cons of different languages,

10
00:00:27,40 --> 00:00:29,50
and some tips and tricks to incorporate

11
00:00:29,50 --> 00:00:32,30
into your own coding practices.

12
00:00:32,30 --> 00:00:34,00
In this challenge, we're working on a problem

13
00:00:34,00 --> 00:00:37,10
common to anyone building a website or a report,

14
00:00:37,10 --> 00:00:41,20
specifically merging live data with static templates.

15
00:00:41,20 --> 00:00:43,70
Data is only useful when it can effectively communicate

16
00:00:43,70 --> 00:00:45,40
to someone else and to do that,

17
00:00:45,40 --> 00:00:48,20
we need to format it into some sort of meaningful,

18
00:00:48,20 --> 00:00:50,90
visual representation.

19
00:00:50,90 --> 00:00:54,10
Sometimes this takes the form a printed report.

20
00:00:54,10 --> 00:00:57,60
Most likely it means publishing data to a webpage.

21
00:00:57,60 --> 00:01:00,90
In this problem, we have the preformatted HTML webpage

22
00:01:00,90 --> 00:01:04,50
and two data files in CSV format.

23
00:01:04,50 --> 00:01:07,40
Our job is to write code that will insert the data files

24
00:01:07,40 --> 00:01:11,30
into the webpage and apply the proper formatting.

25
00:01:11,30 --> 00:01:12,40
We're going to use the files

26
00:01:12,40 --> 00:01:15,30
created by James Williamson in chapter nine of his course

27
00:01:15,30 --> 00:01:18,70
titled Dreamweaver CC Essential Training.

28
00:01:18,70 --> 00:01:20,80
James provides all of the material we need

29
00:01:20,80 --> 00:01:22,10
for this challenge.

30
00:01:22,10 --> 00:01:25,00
I invite you to download the example files for that course

31
00:01:25,00 --> 00:01:28,40
if you'd like to attempt a solution of your own.

32
00:01:28,40 --> 00:01:29,80
In the next videos, I'll show you how

33
00:01:29,80 --> 00:01:32,00
I solved this challenge.

