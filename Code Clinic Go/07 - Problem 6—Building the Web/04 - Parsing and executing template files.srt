1
00:00:00,30 --> 00:00:01,30
- So here's the next step.

2
00:00:01,30 --> 00:00:03,50
We're going to start using templates.

3
00:00:03,50 --> 00:00:07,30
So we saw, in the previous video, how we can take data

4
00:00:07,30 --> 00:00:09,20
and we can merge that with text.

5
00:00:09,20 --> 00:00:11,10
We did that to stdout

6
00:00:11,10 --> 00:00:13,40
and then we also did it to a file.

7
00:00:13,40 --> 00:00:15,70
Well is there some way that we could take our text

8
00:00:15,70 --> 00:00:17,70
and instead of having our text

9
00:00:17,70 --> 00:00:20,00
be right in our main program,

10
00:00:20,00 --> 00:00:23,00
just one big string of text right here in func main()?

11
00:00:23,00 --> 00:00:24,90
Is there some way we could take that text and

12
00:00:24,90 --> 00:00:28,60
have it in some sort of another file, like a template file?

13
00:00:28,60 --> 00:00:31,10
So that's the idea behind "Let's create a template,"

14
00:00:31,10 --> 00:00:34,00
and it's just going to be standing in its own file.

15
00:00:34,00 --> 00:00:36,90
It'll be this piece of text right here, this string,

16
00:00:36,90 --> 00:00:39,50
with the fields into which we're going to merge data

17
00:00:39,50 --> 00:00:42,20
and instead of having it in func main(),

18
00:00:42,20 --> 00:00:43,90
it'll just be in a file by itself,

19
00:00:43,90 --> 00:00:46,50
which will be better code organization,

20
00:00:46,50 --> 00:00:48,10
and we'll call that a template.

21
00:00:48,10 --> 00:00:50,30
So we're going to create a template and in Go

22
00:00:50,30 --> 00:00:52,50
you could call the template whatever you want

23
00:00:52,50 --> 00:00:54,30
because when you open up a file

24
00:00:54,30 --> 00:00:57,00
you just say "this is the file, there's the extension,

25
00:00:57,00 --> 00:00:59,00
it doesn't matter what the extension is."

26
00:00:59,00 --> 00:01:01,50
You don't have to give it a PHP extension

27
00:01:01,50 --> 00:01:02,90
or an ASP extension,

28
00:01:02,90 --> 00:01:06,50
that's old school web programming where it's file-based.

29
00:01:06,50 --> 00:01:09,00
Now we're doing everything programmatically.

30
00:01:09,00 --> 00:01:12,30
A lot of people would give the extension gohtml.

31
00:01:12,30 --> 00:01:13,90
So, what we're going to go for,

32
00:01:13,90 --> 00:01:16,70
the understanding that I want you to get from this video,

33
00:01:16,70 --> 00:01:19,10
is just this idea of templates

34
00:01:19,10 --> 00:01:21,40
and then after, we've learned and seen,

35
00:01:21,40 --> 00:01:24,00
"okay, I understand template," we're going to look at

36
00:01:24,00 --> 00:01:27,30
starting to merge data into our templates.

37
00:01:27,30 --> 00:01:29,90
To step number one, is, we have main.go

38
00:01:29,90 --> 00:01:31,20
and we have a template

39
00:01:31,20 --> 00:01:33,50
and let's open up the template and take a look at it.

40
00:01:33,50 --> 00:01:35,90
We're not merging data in this step.

41
00:01:35,90 --> 00:01:37,80
We've got some text that's in a file

42
00:01:37,80 --> 00:01:40,00
and then over here, we're going to use

43
00:01:40,00 --> 00:01:44,10
from package template, text template.ParseFiles.

44
00:01:44,10 --> 00:01:47,10
Now there's two template packages in Go

45
00:01:47,10 --> 00:01:49,10
and I'm going to go over and show you those.

46
00:01:49,10 --> 00:01:52,10
So here at my web browser, I tried to look these up

47
00:01:52,10 --> 00:01:55,70
at "godoc.org" but I got "Internal server error."

48
00:01:55,70 --> 00:01:58,10
but you can see here there's a text template

49
00:01:58,10 --> 00:02:00,60
and there's also an HTML template.

50
00:02:00,60 --> 00:02:04,20
So I could also find documentation from the standard library

51
00:02:04,20 --> 00:02:06,30
over at "golang.org".

52
00:02:06,30 --> 00:02:10,70
So here at golang.org, I went to documents

53
00:02:10,70 --> 00:02:14,00
and then once I was in documents I scrolled down

54
00:02:14,00 --> 00:02:16,90
until I saw Package Documentation

55
00:02:16,90 --> 00:02:18,90
and then here I just went to Standard library

56
00:02:18,90 --> 00:02:20,90
and then here all the different packages.

57
00:02:20,90 --> 00:02:24,30
Okay, where's HTML?

58
00:02:24,30 --> 00:02:25,50
There's HTML template

59
00:02:25,50 --> 00:02:27,90
and then I did the same thing for text template.

60
00:02:27,90 --> 00:02:32,50
So HTML template is built on top of text template.

61
00:02:32,50 --> 00:02:34,80
Everything you could do in text template,

62
00:02:34,80 --> 00:02:36,70
you could do in HTML template.

63
00:02:36,70 --> 00:02:39,60
Plus in HTML template you could do a little bit more.

64
00:02:39,60 --> 00:02:42,80
The difference between the two is HTML template

65
00:02:42,80 --> 00:02:46,70
is going to make your code safe for the web.

66
00:02:46,70 --> 00:02:49,40
So it's going to escape dangerous characters,

67
00:02:49,40 --> 00:02:52,30
which might cause cross-site scripting.

68
00:02:52,30 --> 00:02:53,30
You could play with it more

69
00:02:53,30 --> 00:02:54,90
and if you look at those additional resources,

70
00:02:54,90 --> 00:02:58,40
which I gave you, there's examples of the difference between

71
00:02:58,40 --> 00:03:00,70
the text template and HTML template

72
00:03:00,70 --> 00:03:02,50
and cross-site scripting attacks

73
00:03:02,50 --> 00:03:05,10
being thwarted by HTML template.

74
00:03:05,10 --> 00:03:07,20
We're just going to use template.ParseFiles

75
00:03:07,20 --> 00:03:10,40
from text template and I can change this to HTML

76
00:03:10,40 --> 00:03:12,00
and it'll totally still work too

77
00:03:12,00 --> 00:03:14,60
but we're just going to stick with text template for now.

78
00:03:14,60 --> 00:03:17,20
So we do template.ParseFiles, we give it a file

79
00:03:17,20 --> 00:03:19,20
and that parses this files,

80
00:03:19,20 --> 00:03:21,30
it says "okay, now you have a template."

81
00:03:21,30 --> 00:03:24,00
So it took this file and made my template for me

82
00:03:24,00 --> 00:03:26,20
and now I could execute that template.

83
00:03:26,20 --> 00:03:28,30
I say "where do I want to execute it to."

84
00:03:28,30 --> 00:03:30,50
If I look up the definition of this function,

85
00:03:30,50 --> 00:03:33,20
what do you think that this is going to be defined as?

86
00:03:33,20 --> 00:03:35,60
What do you think the type of that parameter is going to be?

87
00:03:35,60 --> 00:03:38,10
And if you answered "Writer," you were right on.

88
00:03:38,10 --> 00:03:41,00
So it takes a Writer and then a data interface.

89
00:03:41,00 --> 00:03:44,20
So this is kind of interesting, this is the empty interface

90
00:03:44,20 --> 00:03:45,60
and I haven't mentioned this yet,

91
00:03:45,60 --> 00:03:47,50
this is definitely critical and essential

92
00:03:47,50 --> 00:03:48,80
to understanding Go.

93
00:03:48,80 --> 00:03:52,40
So, this is the empty interface, that's the type

94
00:03:52,40 --> 00:03:55,20
and it's an interface with absolutely no behavior.

95
00:03:55,20 --> 00:03:57,60
Everything implements the empty interface.

96
00:03:57,60 --> 00:03:59,10
So, that basically means you could pass

97
00:03:59,10 --> 00:04:00,20
that any type you want

98
00:04:00,20 --> 00:04:02,50
and is an argument when you see empty interface.

99
00:04:02,50 --> 00:04:04,20
So that's going to be the data.

100
00:04:04,20 --> 00:04:05,30
So here is where we're going to say,

101
00:04:05,30 --> 00:04:07,90
"okay, this is where I want you to execute my template

102
00:04:07,90 --> 00:04:09,00
and produce the output,

103
00:04:09,00 --> 00:04:10,90
I want you to write it to this location,

104
00:04:10,90 --> 00:04:13,80
and here's the data that I want you to merge

105
00:04:13,80 --> 00:04:14,60
into the template."

106
00:04:14,60 --> 00:04:16,50
So right now I'm not passing anything in,

107
00:04:16,50 --> 00:04:18,10
so I'm just passing in nil.

108
00:04:18,10 --> 00:04:20,00
So it's just going to execute my template

109
00:04:20,00 --> 00:04:21,90
and print it to stdout.

110
00:04:21,90 --> 00:04:25,50
So let's run that and take a look and see what it does.

111
00:04:25,50 --> 00:04:30,20
And that is an 02, 01.

112
00:04:30,20 --> 00:04:31,40
And I should see that pop-up

113
00:04:31,40 --> 00:04:34,10
right here in stdout, like that, perfect.

114
00:04:34,10 --> 00:04:35,80
So here's the next question I have for you,

115
00:04:35,80 --> 00:04:38,10
another challenge and that would be

116
00:04:38,10 --> 00:04:41,70
can you get this same thing, which we did right here,

117
00:04:41,70 --> 00:04:43,60
to be printed to a file?

118
00:04:43,60 --> 00:04:46,30
So I want it to come out into index.html.

119
00:04:46,30 --> 00:04:50,40
So we took this template and our code,

120
00:04:50,40 --> 00:04:53,60
used it and then produced index.html.

121
00:04:53,60 --> 00:04:55,70
So, that would be my challenge for you.

122
00:04:55,70 --> 00:04:57,10
So, here's how you do it.

123
00:04:57,10 --> 00:04:59,80
You do template.ParseFiles, so that's your first step,

124
00:04:59,80 --> 00:05:02,30
and I'm going to create a file index.html.

125
00:05:02,30 --> 00:05:04,20
So just like we did before

126
00:05:04,20 --> 00:05:05,70
and then I'm going to execute my template

127
00:05:05,70 --> 00:05:07,30
and since this takes a Writer

128
00:05:07,30 --> 00:05:09,70
and a file implements the Writer interface.

129
00:05:09,70 --> 00:05:11,50
I'm going to write it to that file

130
00:05:11,50 --> 00:05:14,00
and then I'm not passing in any data.

131
00:05:14,00 --> 00:05:16,10
So when I run "nf", just to verify,

132
00:05:16,10 --> 00:05:17,60
because the proof is in the pudding.

133
00:05:17,60 --> 00:05:19,90
One of my favorite quotes is,

134
00:05:19,90 --> 00:05:24,20
"When the book and the bird differ believe the bird."

135
00:05:24,20 --> 00:05:26,00
So maybe documentation says one thing

136
00:05:26,00 --> 00:05:27,90
but what happens when you run it?

137
00:05:27,90 --> 00:05:29,80
What's the reality?

138
00:05:29,80 --> 00:05:31,50
So let's see it run.

139
00:05:31,50 --> 00:05:33,70
Produced file, so there we go and there's the file

140
00:05:33,70 --> 00:05:36,70
and we could open this and we've got "Hello".

141
00:05:36,70 --> 00:05:37,60
So that's the next step

142
00:05:37,60 --> 00:05:40,10
in understanding web programming with Go

143
00:05:40,10 --> 00:05:42,60
is just making sure that

144
00:05:42,60 --> 00:05:44,90
you get this idea of having templates

145
00:05:44,90 --> 00:05:46,80
so that's going to be, like, the form letter

146
00:05:46,80 --> 00:05:48,60
that we're going to merge data into

147
00:05:48,60 --> 00:05:50,90
and so we just do template.ParseFiles

148
00:05:50,90 --> 00:05:52,60
and we say "parse this one"

149
00:05:52,60 --> 00:05:54,00
and then we do tpl.Execute

150
00:05:54,00 --> 00:05:56,00
and we say where we want it to be written to

151
00:05:56,00 --> 00:05:57,10
and what data we pass in.

152
00:05:57,10 --> 00:05:58,10
So that's the next step

153
00:05:58,10 --> 00:06:01,00
in understanding web programming with Go.

