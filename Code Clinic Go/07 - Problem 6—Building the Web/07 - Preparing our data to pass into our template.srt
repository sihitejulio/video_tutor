1
00:00:00,40 --> 00:00:03,00
- The next step in our process is to take our data

2
00:00:03,00 --> 00:00:05,80
that we've been able to pull out of our files and print.

3
00:00:05,80 --> 00:00:08,90
And we want to get that into some sort of a data structure.

4
00:00:08,90 --> 00:00:11,60
This is what we're going to cover right here in this video,

5
00:00:11,60 --> 00:00:14,60
these files, this next step in the solution.

6
00:00:14,60 --> 00:00:15,90
And this is where we left off.

7
00:00:15,90 --> 00:00:18,70
So right now we're looking at the file we left off with.

8
00:00:18,70 --> 00:00:22,20
And this file here was simply printing the data out.

9
00:00:22,20 --> 00:00:23,90
Well, I want to be able to put that into

10
00:00:23,90 --> 00:00:26,60
some sort of a data structure, so my next step

11
00:00:26,60 --> 00:00:28,80
is going to be this file right here

12
00:00:28,80 --> 00:00:31,50
where instead of just printing those straight out,

13
00:00:31,50 --> 00:00:33,30
I'm going to take them, and I'm going to

14
00:00:33,30 --> 00:00:34,80
assign them to variables.

15
00:00:34,80 --> 00:00:36,50
And then I'm going to print those variables.

16
00:00:36,50 --> 00:00:37,90
So that was my next step.

17
00:00:37,90 --> 00:00:40,10
So now I have all those values and variables

18
00:00:40,10 --> 00:00:43,30
for each record as I loop through all of my records.

19
00:00:43,30 --> 00:00:46,70
My step after that is to create some sort of a structure

20
00:00:46,70 --> 00:00:48,10
that's going to hold all that.

21
00:00:48,10 --> 00:00:51,00
So I did type course struct, which is going to hold

22
00:00:51,00 --> 00:00:52,90
a number, a name, and units.

23
00:00:52,90 --> 00:00:55,40
Three different fields, all of type string.

24
00:00:55,40 --> 00:00:58,00
Incidentally, we talked before about visibility

25
00:00:58,00 --> 00:01:01,00
in packages with Go and how capitalization

26
00:01:01,00 --> 00:01:03,00
determines whether or not something is visible

27
00:01:03,00 --> 00:01:05,50
or not visible outside of a package.

28
00:01:05,50 --> 00:01:07,30
And another way you might talk about this

29
00:01:07,30 --> 00:01:09,40
is whether or not something is exported

30
00:01:09,40 --> 00:01:11,50
or not exported from a package.

31
00:01:11,50 --> 00:01:14,20
But we don't really say public and private in Go programming

32
00:01:14,20 --> 00:01:16,30
because those words carry a lot of baggage.

33
00:01:16,30 --> 00:01:19,20
In order to make sure that this stuff is accessible,

34
00:01:19,20 --> 00:01:21,90
I need to make sure that these fields are capitalized.

35
00:01:21,90 --> 00:01:23,80
Oddly enough, you don't have to worry about

36
00:01:23,80 --> 00:01:25,70
your struct being capitalized.

37
00:01:25,70 --> 00:01:28,70
So generally speaking, keep everything not visible,

38
00:01:28,70 --> 00:01:32,10
not exported, unless you need it to be visible and exported.

39
00:01:32,10 --> 00:01:33,90
In which case, make it capitalized.

40
00:01:33,90 --> 00:01:35,20
So I've created this struct.

41
00:01:35,20 --> 00:01:37,90
And then down here, I'm going to create a variable

42
00:01:37,90 --> 00:01:40,00
and say it's of that type.

43
00:01:40,00 --> 00:01:42,50
And then I'm going to take that variable and say,

44
00:01:42,50 --> 00:01:45,30
"hey, the field for that variable number is equal to this,

45
00:01:45,30 --> 00:01:47,90
"and the field name is equal to that, and the field units

46
00:01:47,90 --> 00:01:50,90
"for that variable c of type course

47
00:01:50,90 --> 00:01:52,80
"is of this other value over here."

48
00:01:52,80 --> 00:01:54,70
So I'm doing those three assignments right there.

49
00:01:54,70 --> 00:01:56,20
And I'm just going to print that whole thing out.

50
00:01:56,20 --> 00:01:57,80
So let's watch that one run.

51
00:01:57,80 --> 00:02:01,10
Change directories, and where am I?

52
00:02:01,10 --> 00:02:04,80
I'm in eight and I need to go up into 09,

53
00:02:04,80 --> 00:02:07,80
and then into 02, and 02 is the struct.

54
00:02:07,80 --> 00:02:11,30
Go run main.

55
00:02:11,30 --> 00:02:13,30
Cool, it did just what we were hoping.

56
00:02:13,30 --> 00:02:15,00
It printed out the "TERM," and then

57
00:02:15,00 --> 00:02:18,80
it created the variable for my struct right here.

58
00:02:18,80 --> 00:02:21,50
And then I added the variables to that struct

59
00:02:21,50 --> 00:02:22,90
and then printed them out.

60
00:02:22,90 --> 00:02:25,80
And so here they all are, they're all printed out.

61
00:02:25,80 --> 00:02:27,60
So the next step in the process was

62
00:02:27,60 --> 00:02:30,10
to create yet one more structure.

63
00:02:30,10 --> 00:02:33,20
It's the next data struct I'm going to create is a semester.

64
00:02:33,20 --> 00:02:35,70
In the semester, I'm going to say, "hey, just keep a string.

65
00:02:35,70 --> 00:02:37,50
"What semester is this?"

66
00:02:37,50 --> 00:02:39,70
I'm also going to, here,

67
00:02:39,70 --> 00:02:42,40
keep all of the courses for that semester.

68
00:02:42,40 --> 00:02:44,50
With all the courses there, I will have a slice

69
00:02:44,50 --> 00:02:46,50
of all of the courses because I have

70
00:02:46,50 --> 00:02:48,50
two semesters with which I need to work:

71
00:02:48,50 --> 00:02:50,90
the first semester and the second semester.

72
00:02:50,90 --> 00:02:53,40
This is the way I came up with to do that.

73
00:02:53,40 --> 00:02:55,50
So what changed in this code?

74
00:02:55,50 --> 00:02:56,90
One of the changes you could see is

75
00:02:56,90 --> 00:02:59,80
instead of just printing out my code,

76
00:02:59,80 --> 00:03:02,40
I am now taking that struct that I created,

77
00:03:02,40 --> 00:03:05,60
and I'm appending it to a slice of c.

78
00:03:05,60 --> 00:03:09,10
And I did my length to zero so that I could just

79
00:03:09,10 --> 00:03:11,20
start appending right at the very beginning.

80
00:03:11,20 --> 00:03:13,20
That's the length of the slice.

81
00:03:13,20 --> 00:03:17,10
And then I did my length and capacity of the slice

82
00:03:17,10 --> 00:03:18,90
to the length of the records.

83
00:03:18,90 --> 00:03:20,90
The capacity makes the underlying array.

84
00:03:20,90 --> 00:03:23,70
So the slice is going to have an underlying array

85
00:03:23,70 --> 00:03:26,10
of the exact size that it needs

86
00:03:26,10 --> 00:03:29,20
to store all the data that's coming in from records.

87
00:03:29,20 --> 00:03:31,10
So I created a slice, and now instead of just

88
00:03:31,10 --> 00:03:34,80
printing out each record, I'm taking each record

89
00:03:34,80 --> 00:03:36,40
and I'm appending it to the slice.

90
00:03:36,40 --> 00:03:39,70
So I will have a slice, a list, of all of my courses.

91
00:03:39,70 --> 00:03:43,00
So these were two of the changes that were in the code.

92
00:03:43,00 --> 00:03:45,00
The next thing that changed is right here,

93
00:03:45,00 --> 00:03:47,80
I'm taking this new semester thing here.

94
00:03:47,80 --> 00:03:49,60
And it has s.Courses, and I'm

95
00:03:49,60 --> 00:03:52,60
setting s.Courses equal to that slice.

96
00:03:52,60 --> 00:03:55,50
So somewhere in here, I'm creating a variable s.

97
00:03:55,50 --> 00:03:57,90
And there it is right here, I'm creating the variable s,

98
00:03:57,90 --> 00:04:00,20
and it is of type semester.

99
00:04:00,20 --> 00:04:04,20
And then down here, I'm saying, "the field in s,"

100
00:04:04,20 --> 00:04:06,20
which is of type semester, "the field Courses

101
00:04:06,20 --> 00:04:08,60
"is equal to that slice of Courses."

102
00:04:08,60 --> 00:04:10,20
And now I could print that out.

103
00:04:10,20 --> 00:04:11,60
So I'm just taking a look at this code,

104
00:04:11,60 --> 00:04:13,20
and all of that's still the same,

105
00:04:13,20 --> 00:04:15,60
and all of this is still the same.

106
00:04:15,60 --> 00:04:17,40
And so these highlighted yellow lines

107
00:04:17,40 --> 00:04:19,80
are really what's different, and this is different.

108
00:04:19,80 --> 00:04:23,00
I've added this in here, so I can now start storing

109
00:04:23,00 --> 00:04:25,80
things like by a semester basis.

110
00:04:25,80 --> 00:04:27,70
So let's run this code and take a look and

111
00:04:27,70 --> 00:04:30,60
see what prints out when I print out the semester.

112
00:04:30,60 --> 00:04:34,70
Change directories and go run main.go.

113
00:04:34,70 --> 00:04:36,10
So there it printed out.

114
00:04:36,10 --> 00:04:37,50
And just kind of take a look at

115
00:04:37,50 --> 00:04:39,20
this data structure that printed out.

116
00:04:39,20 --> 00:04:43,10
So what printed was s, and s has a Term.

117
00:04:43,10 --> 00:04:45,10
And so here's one piece of code also,

118
00:04:45,10 --> 00:04:47,00
which changed which I didn't catch.

119
00:04:47,00 --> 00:04:51,30
And so we added term to s of type semester struct, right?

120
00:04:51,30 --> 00:04:53,50
And so there it printed out that first field.

121
00:04:53,50 --> 00:04:56,30
And then the next field is Courses, which is a slice.

122
00:04:56,30 --> 00:04:58,70
And so here you could see the notation for a slice.

123
00:04:58,70 --> 00:05:02,20
And that slice is a collection of courses.

124
00:05:02,20 --> 00:05:05,40
And so inside here, you could see this list of courses,

125
00:05:05,40 --> 00:05:07,10
all those courses in there.

126
00:05:07,10 --> 00:05:08,90
So we're really making good progress

127
00:05:08,90 --> 00:05:12,40
in getting all of our data into a usable data structure

128
00:05:12,40 --> 00:05:14,70
which we could pass in to our template.

129
00:05:14,70 --> 00:05:18,70
And the final step here is to make a slice of semesters.

130
00:05:18,70 --> 00:05:20,50
And so I'm going to widen my window here

131
00:05:20,50 --> 00:05:22,30
so we see all this on one line.

132
00:05:22,30 --> 00:05:24,20
But now what I'm doing is I'm passing in

133
00:05:24,20 --> 00:05:26,40
two files that I want to process.

134
00:05:26,40 --> 00:05:30,00
And compared to the previous code which we had,

135
00:05:30,00 --> 00:05:32,70
we were doing all of this processing in main.

136
00:05:32,70 --> 00:05:34,10
Well look at main now.

137
00:05:34,10 --> 00:05:36,90
Main only has these two lines in it.

138
00:05:36,90 --> 00:05:38,40
So one of the things it does is it calls

139
00:05:38,40 --> 00:05:40,60
this new function academicYear.

140
00:05:40,60 --> 00:05:44,10
And this is some notation which we haven't talked about yet.

141
00:05:44,10 --> 00:05:47,00
So here is a variadic parameter.

142
00:05:47,00 --> 00:05:50,50
So we're passing in an unlimited number of arguments.

143
00:05:50,50 --> 00:05:53,10
So we could pass in either none

144
00:05:53,10 --> 00:05:56,10
or as many as we want of type string.

145
00:05:56,10 --> 00:05:58,50
So that's what that means, it's a variadic parameter.

146
00:05:58,50 --> 00:06:00,00
And you could look at the language specification

147
00:06:00,00 --> 00:06:01,60
in effective Go to learn

148
00:06:01,60 --> 00:06:04,00
and read a little bit more about variadic parameters.

149
00:06:04,00 --> 00:06:08,20
But that dot, dot, dot notation means pass in as many values

150
00:06:08,20 --> 00:06:10,60
of type string as you would like.

151
00:06:10,60 --> 00:06:13,50
I pass in two strings, and then I need to be able to deal

152
00:06:13,50 --> 00:06:15,80
with those two strings, so those are right there.

153
00:06:15,80 --> 00:06:17,60
And I'm going to range over them.

154
00:06:17,60 --> 00:06:19,80
As I range over them, I'm going to

155
00:06:19,80 --> 00:06:23,20
pretty much do all of that processing that we had before

156
00:06:23,20 --> 00:06:24,50
in the previous file.

157
00:06:24,50 --> 00:06:26,90
But now I have this semesters, which will be a slice

158
00:06:26,90 --> 00:06:29,20
of type semester struct.

159
00:06:29,20 --> 00:06:32,20
And so somewhere in here, and let me just highlight those,

160
00:06:32,20 --> 00:06:34,20
I'm going to be appending each semester

161
00:06:34,20 --> 00:06:37,30
to all of my semesters, and then I'll return my semesters.

162
00:06:37,30 --> 00:06:39,70
When I get my semesters back, I could print those out.

163
00:06:39,70 --> 00:06:43,40
So let's take a look to see what runs on this.

164
00:06:43,40 --> 00:06:45,10
And there we go, so we have

165
00:06:45,10 --> 00:06:47,20
this one big slice when I print it out.

166
00:06:47,20 --> 00:06:48,70
And inside that slice,

167
00:06:48,70 --> 00:06:50,20
we're going to have two data structures.

168
00:06:50,20 --> 00:06:51,90
There's the first data structure,

169
00:06:51,90 --> 00:06:54,20
and that data structure is a semester, right,

170
00:06:54,20 --> 00:06:56,70
because we have a slice of semester,

171
00:06:56,70 --> 00:06:58,30
and that's what we called semesters.

172
00:06:58,30 --> 00:07:00,40
And so here is the first semester.

173
00:07:00,40 --> 00:07:02,80
And a semester has both a Term and Courses,

174
00:07:02,80 --> 00:07:05,70
so there's the Term, and then here's the slice of Courses.

175
00:07:05,70 --> 00:07:07,60
And then here's the second semester.

176
00:07:07,60 --> 00:07:10,50
And it has a Term, and then also the slice of Courses.

177
00:07:10,50 --> 00:07:12,60
That really moved us a long ways

178
00:07:12,60 --> 00:07:16,10
towards getting our data ready to pass into a template.

179
00:07:16,10 --> 00:07:19,70
And Go requires you to really think about what you're doing

180
00:07:19,70 --> 00:07:22,90
that doesn't abstract all of this away like some other frame

181
00:07:22,90 --> 00:07:25,00
where it gives you all of the power.

182
00:07:25,00 --> 00:07:27,30
And so you've got all of the power to do whatever you want,

183
00:07:27,30 --> 00:07:29,80
but you got to know what you're doing to be able to use it.

184
00:07:29,80 --> 00:07:31,40
So we have our data structure

185
00:07:31,40 --> 00:07:33,00
ready to pass into our template.

186
00:07:33,00 --> 00:07:36,90
And in the next videos, what is in store for us?

187
00:07:36,90 --> 00:07:39,50
We are going to start working with that data,

188
00:07:39,50 --> 00:07:41,10
and then we're going to apply that

189
00:07:41,10 --> 00:07:44,60
to our Rue Academy HTML page.

190
00:07:44,60 --> 00:07:46,80
So we're really close to having our solve.

191
00:07:46,80 --> 00:07:49,60
And this is really powerful being able to do this

192
00:07:49,60 --> 00:07:51,90
with Go, and it's not that complex.

193
00:07:51,90 --> 00:07:54,00
In a relatively short period of time,

194
00:07:54,00 --> 00:07:56,00
if you know the language fundamentals,

195
00:07:56,00 --> 00:07:59,00
you're able to take some data and merge it into templates

196
00:07:59,00 --> 00:08:01,80
and then produce that into dynamic web pages.

197
00:08:01,80 --> 00:08:04,50
That's really powerful tool and skill set

198
00:08:04,50 --> 00:08:07,00
to have in today's marketplace.

