1
00:00:00,60 --> 00:00:02,30
- So the first step in understanding

2
00:00:02,30 --> 00:00:03,90
web programming with Go,

3
00:00:03,90 --> 00:00:06,20
is to understand templating.

4
00:00:06,20 --> 00:00:10,00
And templating is the idea of just taking data

5
00:00:10,00 --> 00:00:12,50
and merging it with some sort of an output.

6
00:00:12,50 --> 00:00:15,20
And so, we can have a whole database of customer names,

7
00:00:15,20 --> 00:00:18,00
and we could merge that with form letters.

8
00:00:18,00 --> 00:00:19,90
Or, we could take a data source and we could

9
00:00:19,90 --> 00:00:24,10
merge it with a template for sending an email.

10
00:00:24,10 --> 00:00:25,90
So, I've put together a little bit of a joke,

11
00:00:25,90 --> 00:00:29,10
just to illustrate form letters, or a spam email.

12
00:00:29,10 --> 00:00:30,60
And you can see here, this might be what a

13
00:00:30,60 --> 00:00:32,20
template looks like.

14
00:00:32,20 --> 00:00:35,70
You have data, and for each record in the data storage,

15
00:00:35,70 --> 00:00:37,40
whatever data storage you're using,

16
00:00:37,40 --> 00:00:39,60
for every record in your data source,

17
00:00:39,60 --> 00:00:41,20
it'll take that record and pull out the

18
00:00:41,20 --> 00:00:43,70
field First_Name, and put the first name there.

19
00:00:43,70 --> 00:00:45,80
And then pull out the field First_Weekend_Location

20
00:00:45,80 --> 00:00:46,60
and put it there.

21
00:00:46,60 --> 00:00:48,30
And it'll do that for each record.

22
00:00:48,30 --> 00:00:50,60
So this might read like, "Dear Jennifer",

23
00:00:50,60 --> 00:00:53,80
"Dear James", "Dear Scott", "Dear Stacey",

24
00:00:53,80 --> 00:00:54,60
so there it is.

25
00:00:54,60 --> 00:00:55,50
That's a form letter.

26
00:00:55,50 --> 00:00:56,60
That's templating.

27
00:00:56,60 --> 00:00:58,60
Taking a data source and merging it.

28
00:00:58,60 --> 00:01:00,10
So how do we do that with Go?

29
00:01:00,10 --> 00:01:01,60
Let me just ask you that question.

30
00:01:01,60 --> 00:01:02,70
How would you do that?

31
00:01:02,70 --> 00:01:04,70
How would you take data, and start

32
00:01:04,70 --> 00:01:06,50
merging it with text?

33
00:01:06,50 --> 00:01:07,70
So another way to ask that is,

34
00:01:07,70 --> 00:01:09,70
how can you take a variable and

35
00:01:09,70 --> 00:01:11,20
add it to a string?

36
00:01:11,20 --> 00:01:13,00
So here we have a string.

37
00:01:13,00 --> 00:01:14,50
It's a raw string literal.

38
00:01:14,50 --> 00:01:16,10
And here we have a variable,

39
00:01:16,10 --> 00:01:17,70
and I'm going to combine those two.

40
00:01:17,70 --> 00:01:19,30
And the way you combine one string

41
00:01:19,30 --> 00:01:21,10
with another string, because this variable

42
00:01:21,10 --> 00:01:24,20
has a string, is you do that with concatenation.

43
00:01:24,20 --> 00:01:26,20
And when we run this, it's going to take my name,

44
00:01:26,20 --> 00:01:29,40
and it's going to merge it with this template right here,

45
00:01:29,40 --> 00:01:30,70
where it's asking for a name.

46
00:01:30,70 --> 00:01:32,40
Put the name data right there.

47
00:01:32,40 --> 00:01:34,00
So let's go take a look at this running.

48
00:01:34,00 --> 00:01:36,00
I'm going to clear out my terminal

49
00:01:36,00 --> 00:01:39,30
and go up a level, and then go run main,

50
00:01:39,30 --> 00:01:41,10
and it'll merge all that.

51
00:01:41,10 --> 00:01:42,70
And you can see the text I merged it with

52
00:01:42,70 --> 00:01:44,10
is HTML.

53
00:01:44,10 --> 00:01:45,00
How cool is that?

54
00:01:45,00 --> 00:01:47,20
Well, if I could get this into a file,

55
00:01:47,20 --> 00:01:50,60
I've built a webpage with templating in Go

56
00:01:50,60 --> 00:01:52,50
in less than three minutes.

57
00:01:52,50 --> 00:01:54,70
So, that's the next question I have for you,

58
00:01:54,70 --> 00:01:57,60
is how would you take what we've done right here

59
00:01:57,60 --> 00:02:00,30
and put it into its own file?

60
00:02:00,30 --> 00:02:01,30
Maybe you want to pause the video

61
00:02:01,30 --> 00:02:03,30
and, before I show you the solution,

62
00:02:03,30 --> 00:02:04,80
and see if you could get this to come out

63
00:02:04,80 --> 00:02:06,00
into a file.

64
00:02:06,00 --> 00:02:07,50
Now I'm going to show you the solution.

65
00:02:07,50 --> 00:02:08,80
I'm going to get rid of that file.

66
00:02:08,80 --> 00:02:10,10
I was just testing the code.

67
00:02:10,10 --> 00:02:11,60
Always a good thing to do.

68
00:02:11,60 --> 00:02:13,00
And this is the way you do it.

69
00:02:13,00 --> 00:02:15,40
I have, first of all, my name.

70
00:02:15,40 --> 00:02:17,70
It's just a variable storing a string.

71
00:02:17,70 --> 00:02:19,80
And I'm going to use format Sprint.

72
00:02:19,80 --> 00:02:22,40
So, Sprint is string print.

73
00:02:22,40 --> 00:02:24,40
I am printing to a string.

74
00:02:24,40 --> 00:02:27,10
So, instead of printing to standard out,

75
00:02:27,10 --> 00:02:29,30
like the terminal, I'm going to print

76
00:02:29,30 --> 00:02:30,80
to a string.

77
00:02:30,80 --> 00:02:33,10
And the same exact thing is happening here,

78
00:02:33,10 --> 00:02:35,80
with just concatenating two strings together.

79
00:02:35,80 --> 00:02:37,60
And now I'm going to create a file,

80
00:02:37,60 --> 00:02:40,10
os.Create, and I'm going to use io.Copy,

81
00:02:40,10 --> 00:02:43,60
and the destination is going to be right to the new file.

82
00:02:43,60 --> 00:02:45,10
That's why I called it nf.

83
00:02:45,10 --> 00:02:48,20
And then, source is going to be my string.

84
00:02:48,20 --> 00:02:52,40
Now, io.Copy takes both a reader and a writer.

85
00:02:52,40 --> 00:02:54,30
So if we go look at that, what that means is

86
00:02:54,30 --> 00:02:57,20
io.Copy takes a writer and a reader.

87
00:02:57,20 --> 00:02:59,50
And to make a string, a reader,

88
00:02:59,50 --> 00:03:02,10
you have to wrap it with strings.newreader,

89
00:03:02,10 --> 00:03:04,10
and then that just turns a string into a reader,

90
00:03:04,10 --> 00:03:04,90
and that's it.

91
00:03:04,90 --> 00:03:05,80
That's all you got to do.

92
00:03:05,80 --> 00:03:07,50
So let's take a look at this running.

93
00:03:07,50 --> 00:03:12,90
Change Directory 02, and go run main.go.

94
00:03:12,90 --> 00:03:16,20
It created an index.html, and if I open that up,

95
00:03:16,20 --> 00:03:18,30
yeah, it looks exactly similar, actually,

96
00:03:18,30 --> 00:03:20,70
to what I was getting in my terminal,

97
00:03:20,70 --> 00:03:23,00
my standard out, when I did the previous example.

98
00:03:23,00 --> 00:03:24,90
But I can now preview this in browser,

99
00:03:24,90 --> 00:03:26,00
and there we go.

100
00:03:26,00 --> 00:03:28,50
I have a webpage that has had data

101
00:03:28,50 --> 00:03:31,10
merged into it, using Go.

102
00:03:31,10 --> 00:03:35,00
And that's the beginning of doing web programming with Go.

