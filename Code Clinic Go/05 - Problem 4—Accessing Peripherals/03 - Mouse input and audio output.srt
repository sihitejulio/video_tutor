1
00:00:00,50 --> 00:00:02,20
- So the next step in attempting to come up with

2
00:00:02,20 --> 00:00:05,00
a solution that works was to understand

3
00:00:05,00 --> 00:00:08,00
the simple direct media layer a little bit more.

4
00:00:08,00 --> 00:00:09,70
And to work with a veandco package

5
00:00:09,70 --> 00:00:11,40
and to try to get mouse input

6
00:00:11,40 --> 00:00:13,20
and try to get audio output.

7
00:00:13,20 --> 00:00:14,60
Try to get those things working.

8
00:00:14,60 --> 00:00:16,80
And when I went to the veandco Github,

9
00:00:16,80 --> 00:00:19,40
I searched for examples and fortunately,

10
00:00:19,40 --> 00:00:21,40
they have a folder here with examples.

11
00:00:21,40 --> 00:00:22,60
And there's an audio example

12
00:00:22,60 --> 00:00:24,70
and an events example.

13
00:00:24,70 --> 00:00:26,50
And I just want to take a moment to point out

14
00:00:26,50 --> 00:00:27,50
that down here at the bottom,

15
00:00:27,50 --> 00:00:30,50
they also have a Berkley software development license.

16
00:00:30,50 --> 00:00:32,10
Which means that they're licensing this

17
00:00:32,10 --> 00:00:34,00
for the common good.

18
00:00:34,00 --> 00:00:36,10
And anybody can use this code to learn

19
00:00:36,10 --> 00:00:37,60
or to do what they would like

20
00:00:37,60 --> 00:00:39,30
within the guidelines of that license.

21
00:00:39,30 --> 00:00:41,10
It's important to pay attention to those licenses

22
00:00:41,10 --> 00:00:44,20
because we are in intellectual property business

23
00:00:44,20 --> 00:00:46,40
as programmers and it's just good to be

24
00:00:46,40 --> 00:00:48,10
respectful of other people's property.

25
00:00:48,10 --> 00:00:49,70
So let's take a look at these two programs running

26
00:00:49,70 --> 00:00:51,80
and then we'll do a little cursory walk-through

27
00:00:51,80 --> 00:00:52,70
of the code.

28
00:00:52,70 --> 00:00:54,40
The intent in this challenge for me

29
00:00:54,40 --> 00:00:56,10
isn't to try to teach you something about

30
00:00:56,10 --> 00:00:58,20
binding Go with C.

31
00:00:58,20 --> 00:00:59,70
I am not a C programmer.

32
00:00:59,70 --> 00:01:01,20
That is not my strong point.

33
00:01:01,20 --> 00:01:03,60
The intent is to just share with you in this challenge

34
00:01:03,60 --> 00:01:06,40
what I went through and what the students

35
00:01:06,40 --> 00:01:07,40
who worked with me went through

36
00:01:07,40 --> 00:01:09,40
in trying to come up with our solution

37
00:01:09,40 --> 00:01:11,80
and to show you that we were able to actually solve it,

38
00:01:11,80 --> 00:01:13,80
even if all of us didn't have full grasp

39
00:01:13,80 --> 00:01:15,90
of the material with which we were working.

40
00:01:15,90 --> 00:01:17,40
We were able to get a solution working.

41
00:01:17,40 --> 00:01:19,40
So let's take a look at this running.

42
00:01:19,40 --> 00:01:21,50
So I'm at the first one, the events one.

43
00:01:21,50 --> 00:01:24,10
And I'm going to do a go run main.go.

44
00:01:24,10 --> 00:01:27,30
And it's going to clang away, it's telling me up here.

45
00:01:27,30 --> 00:01:29,50
And then I'm getting some warnings.

46
00:01:29,50 --> 00:01:31,30
And I come down here.

47
00:01:31,30 --> 00:01:33,50
As I move my mouse over this window,

48
00:01:33,50 --> 00:01:36,70
you can see I'm capturing some mouse positions.

49
00:01:36,70 --> 00:01:38,10
X and Y.

50
00:01:38,10 --> 00:01:39,80
You can also see it's a little bit draggy.

51
00:01:39,80 --> 00:01:41,60
I'm getting a little wait time sometimes.

52
00:01:41,60 --> 00:01:42,90
There's some lag there.

53
00:01:42,90 --> 00:01:45,40
Not the best performance for whatever reason

54
00:01:45,40 --> 00:01:47,60
and I don't understand why, but all right.

55
00:01:47,60 --> 00:01:48,90
We've got some X Y values

56
00:01:48,90 --> 00:01:51,00
and X Y values are important.

57
00:01:51,00 --> 00:01:53,40
And then let's take a look at the other one,

58
00:01:53,40 --> 00:01:55,00
where we get some audio.

59
00:01:55,00 --> 00:01:58,80
And it's a lot of code for what you're about to hear.

60
00:01:58,80 --> 00:02:00,70
Again, we get the warnings.

61
00:02:00,70 --> 00:02:03,00
(loud electronic tone)

62
00:02:03,00 --> 00:02:03,80
And that's it.

63
00:02:03,80 --> 00:02:04,80
But, you know what?

64
00:02:04,80 --> 00:02:07,70
That was pretty great because we got mouse X Y,

65
00:02:07,70 --> 00:02:08,90
we captured some input,

66
00:02:08,90 --> 00:02:10,60
and we got some audio output.

67
00:02:10,60 --> 00:02:12,90
So if we're able to combine these two examples

68
00:02:12,90 --> 00:02:15,00
into one program that runs,

69
00:02:15,00 --> 00:02:16,60
we'll have our solution.

70
00:02:16,60 --> 00:02:18,30
And so that was like, okay good.

71
00:02:18,30 --> 00:02:19,60
We're within reaching distance.

72
00:02:19,60 --> 00:02:21,90
We can look at a code pattern that's been laid

73
00:02:21,90 --> 00:02:23,20
before by somebody else,

74
00:02:23,20 --> 00:02:24,90
and blend the two together.

75
00:02:24,90 --> 00:02:27,00
So just to do a cursory overview of this code,

76
00:02:27,00 --> 00:02:28,60
this is Go code.

77
00:02:28,60 --> 00:02:32,10
And so here we're doing variable declaration with Go.

78
00:02:32,10 --> 00:02:34,00
And then down here, we can read this.

79
00:02:34,00 --> 00:02:37,50
Okay, we're just calling a function from some package.

80
00:02:37,50 --> 00:02:38,90
And here's defer.

81
00:02:38,90 --> 00:02:41,60
And so this looks like this got opened

82
00:02:41,60 --> 00:02:43,60
and we need to destroy that.

83
00:02:43,60 --> 00:02:44,60
And we're going to do that

84
00:02:44,60 --> 00:02:46,30
with an entire function exits.

85
00:02:46,30 --> 00:02:47,10
Here's another thing:

86
00:02:47,10 --> 00:02:49,50
we're going to render and we're passing in the window.

87
00:02:49,50 --> 00:02:51,10
Okay, that's all very readable.

88
00:02:51,10 --> 00:02:53,00
And this For loop, totally understand that.

89
00:02:53,00 --> 00:02:54,30
We're setting a variable to True

90
00:02:54,30 --> 00:02:56,50
and we're running this loop while it's true

91
00:02:56,50 --> 00:02:58,30
and at a certain point, we set it to false.

92
00:02:58,30 --> 00:03:00,80
And then we have a switch and we understand this switch.

93
00:03:00,80 --> 00:03:02,10
This is a type switch.

94
00:03:02,10 --> 00:03:03,60
So we're switching on different types.

95
00:03:03,60 --> 00:03:04,90
What types are we switching on?

96
00:03:04,90 --> 00:03:07,60
MouseMotion, MouseButton, MouseWheel,

97
00:03:07,60 --> 00:03:08,80
KeyUpEvent.

98
00:03:08,80 --> 00:03:10,30
And then we're printing out different information

99
00:03:10,30 --> 00:03:11,40
when those occur.

100
00:03:11,40 --> 00:03:12,70
And so this is pretty readable code.

101
00:03:12,70 --> 00:03:15,70
The interesting thing for me is that this is in func run,

102
00:03:15,70 --> 00:03:17,50
right here, and then we come down here

103
00:03:17,50 --> 00:03:20,70
and in main, we have os.Exit and run.

104
00:03:20,70 --> 00:03:22,80
And what run is returning are numbers.

105
00:03:22,80 --> 00:03:24,40
So when run is finished running,

106
00:03:24,40 --> 00:03:25,80
it is going to return a number.

107
00:03:25,80 --> 00:03:29,00
And over here in godoc OS, we can look at Exit

108
00:03:29,00 --> 00:03:31,00
and Exit's going to take in a number.

109
00:03:31,00 --> 00:03:33,70
And conventionally zero indicates success

110
00:03:33,70 --> 00:03:36,90
and non-zero means we have an error when we exit it.

111
00:03:36,90 --> 00:03:39,40
This code isn't too difficult to start wrapping

112
00:03:39,40 --> 00:03:41,70
our heads around, but there's obviously

113
00:03:41,70 --> 00:03:43,10
some things going on here,

114
00:03:43,10 --> 00:03:45,60
which even my editor is not totally sure about.

115
00:03:45,60 --> 00:03:47,50
I haven't seen this coloring before.

116
00:03:47,50 --> 00:03:49,00
And so somewhere in there,

117
00:03:49,00 --> 00:03:53,40
it's using SDL and a non-zero in error.

118
00:03:53,40 --> 00:03:54,80
So that's the basic overview.

119
00:03:54,80 --> 00:03:58,80
The code which is used to capture the mouse events.

120
00:03:58,80 --> 00:04:00,90
The audio code is a little bit more involved

121
00:04:00,90 --> 00:04:02,40
and a little bit more complicated.

122
00:04:02,40 --> 00:04:04,00
So I'm going to talk a little bit about audio

123
00:04:04,00 --> 00:04:06,20
in the next video when we look at our solution.

124
00:04:06,20 --> 00:04:08,50
But, I just want to take a moment to kind of show,

125
00:04:08,50 --> 00:04:10,90
hey, we have some great code samples.

126
00:04:10,90 --> 00:04:12,50
And with these code samples that are running,

127
00:04:12,50 --> 00:04:14,80
can we combine them to build our solution.

128
00:04:14,80 --> 00:04:16,10
And so that's what we did.

129
00:04:16,10 --> 00:04:16,80
And in the next video,

130
00:04:16,80 --> 00:04:19,00
I'm going to show you a little bit about our solution.

