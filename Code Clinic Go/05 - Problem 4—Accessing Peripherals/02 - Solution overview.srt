1
00:00:00,60 --> 00:00:02,90
- So I'm going to do go run main.go

2
00:00:02,90 --> 00:00:05,10
and hit enter,

3
00:00:05,10 --> 00:00:07,50
and you can see it's running up here.

4
00:00:07,50 --> 00:00:09,20
And now we're getting some warnings.

5
00:00:09,20 --> 00:00:10,70
This program is a little bit buggy,

6
00:00:10,70 --> 00:00:12,40
so I'm going to let it totally load.

7
00:00:12,40 --> 00:00:14,00
I've had it crash a couple of times.

8
00:00:14,00 --> 00:00:16,40
It's connecting with a lot of different things,

9
00:00:16,40 --> 00:00:17,60
and now I'm going to push my mouse button

10
00:00:17,60 --> 00:00:19,10
and see what happens.

11
00:00:19,10 --> 00:00:28,90
(electronic beeping noises)

12
00:00:28,90 --> 00:00:29,60
Excellent.

13
00:00:29,60 --> 00:00:31,90
You don't know how many hours of my life that took,

14
00:00:31,90 --> 00:00:34,40
and this was also a project where I drew in

15
00:00:34,40 --> 00:00:35,90
some of the minds of my students

16
00:00:35,90 --> 00:00:38,10
to help me brainstorm on how to make this happen.

17
00:00:38,10 --> 00:00:40,70
So a lot of the effort that was put into this

18
00:00:40,70 --> 00:00:43,40
was also the contribution of many of my students.

19
00:00:43,40 --> 00:00:45,00
That is the solution,

20
00:00:45,00 --> 00:00:47,80
and to give you my solution overview,

21
00:00:47,80 --> 00:00:50,20
I'm going to walk through the process

22
00:00:50,20 --> 00:00:52,40
that we went through to figure out

23
00:00:52,40 --> 00:00:53,90
how to get all of this to run,

24
00:00:53,90 --> 00:00:55,40
and we put in ample notes

25
00:00:55,40 --> 00:00:57,10
to sort of document everything.

26
00:00:57,10 --> 00:00:58,90
And what we're doing is we are connecting

27
00:00:58,90 --> 00:01:01,00
to a third-party package here,

28
00:01:01,00 --> 00:01:02,90
and that third-party package is running

29
00:01:02,90 --> 00:01:05,20
go-sdl2,

30
00:01:05,20 --> 00:01:07,00
Simple DirectMedia Layer 2,

31
00:01:07,00 --> 00:01:08,50
and so we're going to see what that is.

32
00:01:08,50 --> 00:01:10,00
And the process we went through

33
00:01:10,00 --> 00:01:13,00
to figure all this out and to get all work

34
00:01:13,00 --> 00:01:14,80
was this right here, and I'm going to change that

35
00:01:14,80 --> 00:01:17,10
to Our Learning process.

36
00:01:17,10 --> 00:01:18,90
So let's take a look at those different things.

37
00:01:18,90 --> 00:01:21,00
So the first thing that I did was that

38
00:01:21,00 --> 00:01:23,30
I can into GoDoc and I searched for mouse,

39
00:01:23,30 --> 00:01:24,30
and then Command F,

40
00:01:24,30 --> 00:01:25,60
and then look for mouse,

41
00:01:25,60 --> 00:01:27,60
and there's definitely some things in here

42
00:01:27,60 --> 00:01:29,80
related to using the mouse.

43
00:01:29,80 --> 00:01:31,20
And so I dug through that,

44
00:01:31,20 --> 00:01:33,10
and one of the things that really stood out to me

45
00:01:33,10 --> 00:01:35,10
was mobile, and so I did come up

46
00:01:35,10 --> 00:01:36,80
with some examples,

47
00:01:36,80 --> 00:01:38,80
and these are examples from

48
00:01:38,80 --> 00:01:42,00
a cutting edge area of Go development

49
00:01:42,00 --> 00:01:44,50
where they're trying to get GoCode

50
00:01:44,50 --> 00:01:48,90
to be able to write Android and iOS apps,

51
00:01:48,90 --> 00:01:51,80
both Android and iOS with one language,

52
00:01:51,80 --> 00:01:53,30
so how great would that be.

53
00:01:53,30 --> 00:01:55,60
So here are some examples of GoCode.

54
00:01:55,60 --> 00:01:57,30
And these do not run on my phone

55
00:01:57,30 --> 00:01:58,90
for whatever reason, and I'll show you

56
00:01:58,90 --> 00:02:01,00
where I found those examples when I talk about these

57
00:02:01,00 --> 00:02:03,20
and we see them, and these do.

58
00:02:03,20 --> 00:02:04,70
So there are the examples.

59
00:02:04,70 --> 00:02:06,30
As I start about all these different things

60
00:02:06,30 --> 00:02:08,70
that took me into learning Excel

61
00:02:08,70 --> 00:02:11,50
and creating graphical user interfaces.

62
00:02:11,50 --> 00:02:14,30
And Excel is a pretty low-level thing for

63
00:02:14,30 --> 00:02:17,10
creating a basic framework for a GUI environment,

64
00:02:17,10 --> 00:02:19,10
and on Mac, it's XQuartz,

65
00:02:19,10 --> 00:02:21,30
so this rabbit hole gets deep pretty fast.

66
00:02:21,30 --> 00:02:23,40
And then I started to learn about SDL,

67
00:02:23,40 --> 00:02:25,00
Simple DirectMedia Layer.

68
00:02:25,00 --> 00:02:26,80
That was one of the other things I searched for

69
00:02:26,80 --> 00:02:28,20
on GoDoc,

70
00:02:28,20 --> 00:02:29,40
and so here's a little bit about

71
00:02:29,40 --> 00:02:31,30
Simple DirectMedia Layer.

72
00:02:31,30 --> 00:02:32,60
And one of my students has worked with us

73
00:02:32,60 --> 00:02:34,20
quite a bit building games;

74
00:02:34,20 --> 00:02:36,30
it's a cross-platform software development library

75
00:02:36,30 --> 00:02:38,10
designed to provide a low-level hardware

76
00:02:38,10 --> 00:02:40,30
abstraction layer to computer multimedia

77
00:02:40,30 --> 00:02:42,30
hardware components, so you know that's going to be

78
00:02:42,30 --> 00:02:44,80
a couple of late nights to figure that one out.

79
00:02:44,80 --> 00:02:45,90
And so then had to,

80
00:02:45,90 --> 00:02:48,00
alright, if we're going try this out in SDL,

81
00:02:48,00 --> 00:02:50,30
when we searched GoDoc for SDL,

82
00:02:50,30 --> 00:02:53,80
we found this one package right here, veandco.

83
00:02:53,80 --> 00:02:57,10
It's a huge package, not much documentation.

84
00:02:57,10 --> 00:02:59,80
And so we had to try to start getting this running.

85
00:02:59,80 --> 00:03:02,10
I looked at GitHub, found his Repo,

86
00:03:02,10 --> 00:03:03,50
and then came down here,

87
00:03:03,50 --> 00:03:05,90
and there's some instructions at this location.

88
00:03:05,90 --> 00:03:08,50
There wasn't any documentation over at GoDoc.

89
00:03:08,50 --> 00:03:10,40
There were some over here at GitHub.

90
00:03:10,40 --> 00:03:12,80
And that really sort of illustrated one of things

91
00:03:12,80 --> 00:03:15,80
which to me is interesting about Go.

92
00:03:15,80 --> 00:03:17,50
So there are some things where Go

93
00:03:17,50 --> 00:03:19,50
is being developed at a fast pace,

94
00:03:19,50 --> 00:03:20,70
and there's a lot of people working,

95
00:03:20,70 --> 00:03:21,90
and there's just a couple of places

96
00:03:21,90 --> 00:03:23,20
where it's a little bit rough.

97
00:03:23,20 --> 00:03:25,30
One of the places that's rough is mobile,

98
00:03:25,30 --> 00:03:26,70
and they're getting things working,

99
00:03:26,70 --> 00:03:28,30
and they're building things, but

100
00:03:28,30 --> 00:03:30,20
all the documentation isn't necessarily there.

101
00:03:30,20 --> 00:03:31,80
Sometimes it's conflicting.

102
00:03:31,80 --> 00:03:34,10
Also sometimes third-party packages,

103
00:03:34,10 --> 00:03:37,00
there's no description here at godoc.org,

104
00:03:37,00 --> 00:03:38,70
but come over here at GitHub,

105
00:03:38,70 --> 00:03:40,60
and we can find some information about it.

106
00:03:40,60 --> 00:03:43,10
So this is great, and we brought it down

107
00:03:43,10 --> 00:03:45,10
and installed it and tried to get it to run,

108
00:03:45,10 --> 00:03:47,00
but then we need to have SDL installed,

109
00:03:47,00 --> 00:03:48,80
so we installed Homebrew

110
00:03:48,80 --> 00:03:51,60
and Homebrew's package management for the Mac,

111
00:03:51,60 --> 00:03:53,50
and then we did brew install.

112
00:03:53,50 --> 00:03:55,10
So that was bringing it all down,

113
00:03:55,10 --> 00:03:56,70
and then this error came up:

114
00:03:56,70 --> 00:03:59,00
sdl pkg-config not in path.

115
00:03:59,00 --> 00:04:01,30
So do a little bit more research about that

116
00:04:01,30 --> 00:04:03,40
and what we discovered, and these are

117
00:04:03,40 --> 00:04:05,90
the screenshots of Homebrew and then Homebrews,

118
00:04:05,90 --> 00:04:07,40
if you want to learn a little bit more about those,

119
00:04:07,40 --> 00:04:09,30
and again, all of those links

120
00:04:09,30 --> 00:04:12,10
are right here in this README file.

121
00:04:12,10 --> 00:04:14,10
So coming back, just the normal

122
00:04:14,10 --> 00:04:17,10
research process for, "Let's try to get something to work."

123
00:04:17,10 --> 00:04:18,50
So we had that error about

124
00:04:18,50 --> 00:04:20,00
the package thing not working.

125
00:04:20,00 --> 00:04:22,60
So we had to do brew install pkg-config,

126
00:04:22,60 --> 00:04:24,40
and we got that going, but then

127
00:04:24,40 --> 00:04:26,10
we need to set a path variable.

128
00:04:26,10 --> 00:04:28,30
So we came over here

129
00:04:28,30 --> 00:04:31,80
and set our path variable in .bash_profile

130
00:04:31,80 --> 00:04:33,50
that we also needed this.

131
00:04:33,50 --> 00:04:36,50
Once we had done all of that,

132
00:04:36,50 --> 00:04:40,00
we could then go start writing this code,

133
00:04:40,00 --> 00:04:41,70
and playing around with this code,

134
00:04:41,70 --> 00:04:43,10
and trying to get it to work.

135
00:04:43,10 --> 00:04:46,00
So that was the basic overview of our solution.

136
00:04:46,00 --> 00:04:47,90
We did get it to work and in the process,

137
00:04:47,90 --> 00:04:51,10
we discovered some cool things about the Go mobile project,

138
00:04:51,10 --> 00:04:52,40
which I'm going to show you,

139
00:04:52,40 --> 00:04:55,40
and then we also found these two great files here

140
00:04:55,40 --> 00:04:57,60
where they give examples of using

141
00:04:57,60 --> 00:05:01,50
viendco for doing events and audio.

142
00:05:01,50 --> 00:05:03,90
So we have those and then we got the solution working.

143
00:05:03,90 --> 00:05:05,30
That's the high level overview.

144
00:05:05,30 --> 00:05:06,90
In the next couple of movies, we'll drop in

145
00:05:06,90 --> 00:05:09,00
and take a look at some of the details.

