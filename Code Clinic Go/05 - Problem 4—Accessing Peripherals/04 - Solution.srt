1
00:00:00,40 --> 00:00:02,50
- So here's our solution for this challenge.

2
00:00:02,50 --> 00:00:05,80
There's some high end things which are going on in here.

3
00:00:05,80 --> 00:00:07,80
I don't really want to get into the details

4
00:00:07,80 --> 00:00:10,50
of creating a slice header for the data structure

5
00:00:10,50 --> 00:00:13,50
of a slice in Go, from types in C

6
00:00:13,50 --> 00:00:15,70
that are converted then to Go types

7
00:00:15,70 --> 00:00:17,50
by using the reflect package.

8
00:00:17,50 --> 00:00:20,20
I did put a lot of notes into this code.

9
00:00:20,20 --> 00:00:22,40
So depending upon your skill level

10
00:00:22,40 --> 00:00:24,70
if you want to read through here and try to figure out,

11
00:00:24,70 --> 00:00:26,40
"oh, OK, this is what's happening,"

12
00:00:26,40 --> 00:00:28,00
you could take a look at these things.

13
00:00:28,00 --> 00:00:29,90
But what I would like to do is just kind of do

14
00:00:29,90 --> 00:00:32,30
a brief review of some of the things

15
00:00:32,30 --> 00:00:34,60
in the SDL package, which we do use.

16
00:00:34,60 --> 00:00:37,40
And this stuff down here in main is pretty straightforward.

17
00:00:37,40 --> 00:00:39,90
But when we get up into working with sound

18
00:00:39,90 --> 00:00:42,20
like here in func SignWave, which is a callback

19
00:00:42,20 --> 00:00:43,90
which gets passed to C,

20
00:00:43,90 --> 00:00:46,50
and changing that sound based upon mouse movement,

21
00:00:46,50 --> 00:00:49,20
I think that that stuff is all a little bit complex

22
00:00:49,20 --> 00:00:51,50
to try to convey and have it

23
00:00:51,50 --> 00:00:54,10
come across coherently and clearly.

24
00:00:54,10 --> 00:00:55,60
So I'm just going to leave that entire

25
00:00:55,60 --> 00:00:57,90
beast of code right there alone.

26
00:00:57,90 --> 00:00:59,70
But in func main, a lot of this stuff is

27
00:00:59,70 --> 00:01:01,90
really straightforward and understandable.

28
00:01:01,90 --> 00:01:06,10
So from package SDL, we have an initialization with Init.

29
00:01:06,10 --> 00:01:07,90
And there's different things we could do with that.

30
00:01:07,90 --> 00:01:09,50
We don't need to necessarily pass in

31
00:01:09,50 --> 00:01:12,40
any argument, so it takes an optional argument.

32
00:01:12,40 --> 00:01:14,40
But this initializes our system.

33
00:01:14,40 --> 00:01:16,40
And what that does is it scans the system

34
00:01:16,40 --> 00:01:18,80
and figures out what hardware is available.

35
00:01:18,80 --> 00:01:21,20
That's kind of an interesting thing for SDL to do

36
00:01:21,20 --> 00:01:23,20
is figure out what hardware is available.

37
00:01:23,20 --> 00:01:25,00
And then again, we create a window,

38
00:01:25,00 --> 00:01:27,90
all pretty understandable, and we are using defer,

39
00:01:27,90 --> 00:01:30,70
which defers something that line of code

40
00:01:30,70 --> 00:01:33,40
until func main is just about to exit.

41
00:01:33,40 --> 00:01:36,30
And then the last thing func main does before it exits

42
00:01:36,30 --> 00:01:38,70
is it runs that deferred code.

43
00:01:38,70 --> 00:01:40,90
And then we create a data structure down here.

44
00:01:40,90 --> 00:01:45,10
And this data structure is a struct with different fields.

45
00:01:45,10 --> 00:01:48,30
And we're setting different fields for our sound settings

46
00:01:48,30 --> 00:01:50,10
including this callback, and then we're going to take

47
00:01:50,10 --> 00:01:52,10
this spec, and we're going to pass it in

48
00:01:52,10 --> 00:01:55,30
to opening our audio from SDL package.

49
00:01:55,30 --> 00:01:56,90
And so when we pass that in,

50
00:01:56,90 --> 00:01:58,60
OpenAudio will know what to do with that,

51
00:01:58,60 --> 00:02:00,40
and that's going to open the audio

52
00:02:00,40 --> 00:02:02,30
for use within our system.

53
00:02:02,30 --> 00:02:03,70
And when it opens, we're going to say,

54
00:02:03,70 --> 00:02:05,00
"hey, pause that audio,"

55
00:02:05,00 --> 00:02:06,60
because when the program first runs,

56
00:02:06,60 --> 00:02:08,10
we want the audio to be paused,

57
00:02:08,10 --> 00:02:10,00
and then when a mouse button is clicked,

58
00:02:10,00 --> 00:02:13,00
the left mouse button, that's when we want things to run.

59
00:02:13,00 --> 00:02:15,00
So we're going to set this value to true,

60
00:02:15,00 --> 00:02:16,40
and we're going to do a for loop,

61
00:02:16,40 --> 00:02:18,60
which loops just continuously,

62
00:02:18,60 --> 00:02:20,30
but we're going to give it a little bit of a break.

63
00:02:20,30 --> 00:02:22,90
So every 100 milliseconds, that for loop

64
00:02:22,90 --> 00:02:24,30
is going to take a sleep.

65
00:02:24,30 --> 00:02:27,20
And we're using time.Sleep from Go

66
00:02:27,20 --> 00:02:31,30
as opposed to sdl.Delay, which was also in SDL

67
00:02:31,30 --> 00:02:34,90
because sdl.Delay would have delayed the entire thread,

68
00:02:34,90 --> 00:02:37,60
whereas Go will only pause the Go routine,

69
00:02:37,60 --> 00:02:38,90
not the entire thread.

70
00:02:38,90 --> 00:02:41,00
That's going to be better for our system's resources

71
00:02:41,00 --> 00:02:42,70
to do it this way.

72
00:02:42,70 --> 00:02:44,30
And then up here, we have

73
00:02:44,30 --> 00:02:46,40
where we are listening for different events.

74
00:02:46,40 --> 00:02:48,80
So the QuitEvent would quit the program.

75
00:02:48,80 --> 00:02:50,40
Or if we have a MouseMotionEvent,

76
00:02:50,40 --> 00:02:53,20
we're going to be capturing our x, y position.

77
00:02:53,20 --> 00:02:54,70
And you could see that happening right here,

78
00:02:54,70 --> 00:02:56,60
capturing the x, y position.

79
00:02:56,60 --> 00:02:59,30
and then we're passing that to tone and volume.

80
00:02:59,30 --> 00:03:01,30
And so that's going to be changing our sound.

81
00:03:01,30 --> 00:03:04,20
These are global variables, which both the callback

82
00:03:04,20 --> 00:03:06,80
and the code right here have access to.

83
00:03:06,80 --> 00:03:08,90
So if they both try to access that code

84
00:03:08,90 --> 00:03:12,30
at the exact same time, we would have a race condition.

85
00:03:12,30 --> 00:03:14,10
And the reason we'd have a race condition is because

86
00:03:14,10 --> 00:03:16,70
the audio is running on one thread.

87
00:03:16,70 --> 00:03:20,10
And where the callback is being run is on another thread.

88
00:03:20,10 --> 00:03:21,60
So we lock this down,

89
00:03:21,60 --> 00:03:23,50
and that way we prevent a race condition.

90
00:03:23,50 --> 00:03:25,00
But this is where we're changing

91
00:03:25,00 --> 00:03:26,80
our sound based upon our x, y.

92
00:03:26,80 --> 00:03:28,90
And like I said, I'm not a sound person.

93
00:03:28,90 --> 00:03:30,20
I'm no sound expert.

94
00:03:30,20 --> 00:03:32,00
I hardly ever worked with sound in my life.

95
00:03:32,00 --> 00:03:33,70
I'm not going to go into the details

96
00:03:33,70 --> 00:03:35,50
of the understanding of what I got from sound

97
00:03:35,50 --> 00:03:36,80
because it's kind of complicated,

98
00:03:36,80 --> 00:03:38,40
the formulas which are being used.

99
00:03:38,40 --> 00:03:40,70
This is where the sound gets changed

100
00:03:40,70 --> 00:03:43,00
based upon mouse movements.

101
00:03:43,00 --> 00:03:44,70
And then last thing which is being checked here

102
00:03:44,70 --> 00:03:46,90
is whether or not are we clicking the button.

103
00:03:46,90 --> 00:03:48,80
And when we click the button,

104
00:03:48,80 --> 00:03:51,20
then we are going to play sound.

105
00:03:51,20 --> 00:03:54,40
So PauseAudio, if the button is released,

106
00:03:54,40 --> 00:03:57,50
then that's true, pause the audio.

107
00:03:57,50 --> 00:03:59,90
If the button hasn't been released, if it's pressed,

108
00:03:59,90 --> 00:04:02,70
then this is false, and then PauseAudio is false.

109
00:04:02,70 --> 00:04:04,50
We got a double negative, which means that

110
00:04:04,50 --> 00:04:06,50
audio is not paused, it's playing.

111
00:04:06,50 --> 00:04:09,30
Another way you could write that would be right here.

112
00:04:09,30 --> 00:04:11,90
So that's my solution for accessing the mouse

113
00:04:11,90 --> 00:04:13,70
and turning it into a musical instrument

114
00:04:13,70 --> 00:04:15,00
for this code challenge.

115
00:04:15,00 --> 00:04:17,30
And I'm looking forward to the next two challenges

116
00:04:17,30 --> 00:04:18,70
where I get to show you how to do

117
00:04:18,70 --> 00:04:21,20
some really interesting cool stuff with directories

118
00:04:21,20 --> 00:04:25,00
and recursion and then also how to use templating

119
00:04:25,00 --> 00:04:28,00
and to do web programming with Go.

