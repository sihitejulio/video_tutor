1
00:00:06,90 --> 00:00:09,10
- Hello, and welcome to Code Clinic.

2
00:00:09,10 --> 00:00:10,80
My name is Todd McLeod.

3
00:00:10,80 --> 00:00:13,30
Code Clinic is a course where a unique problem

4
00:00:13,30 --> 00:00:16,40
is introduced to a collection of Lynda.com authors.

5
00:00:16,40 --> 00:00:18,90
In response, each author will create a solution

6
00:00:18,90 --> 00:00:21,60
using their programming language of choice.

7
00:00:21,60 --> 00:00:23,70
You can learn several things from Code Clinic,

8
00:00:23,70 --> 00:00:25,70
different approaches to solving a problem,

9
00:00:25,70 --> 00:00:27,90
the pros and cons of different languages,

10
00:00:27,90 --> 00:00:29,50
and some tips and tricks

11
00:00:29,50 --> 00:00:32,90
to incorporate into your own coding practices.

12
00:00:32,90 --> 00:00:34,00
In this challenge,

13
00:00:34,00 --> 00:00:36,00
the problem is to create a musical instrument

14
00:00:36,00 --> 00:00:40,90
controlled by the mouse.

15
00:00:40,90 --> 00:00:43,30
Move the mouse up and down to change the pitch,

16
00:00:43,30 --> 00:00:46,20
move it side to side to change the volume.

17
00:00:46,20 --> 00:00:48,50
The instrument is silent until we click and hold

18
00:00:48,50 --> 00:00:50,90
one of the mouse buttons.

19
00:00:50,90 --> 00:00:53,90
Letting go of the button turns off the musical tone.

20
00:00:53,90 --> 00:00:55,20
It's a simple request,

21
00:00:55,20 --> 00:00:57,30
but because it requires access to the mouse,

22
00:00:57,30 --> 00:01:00,90
it may be difficult or impossible in some languages.

23
00:01:00,90 --> 00:01:01,90
In all cases,

24
00:01:01,90 --> 00:01:03,60
we'll explore some ways to solve the problem

25
00:01:03,60 --> 00:01:06,00
and learn why they might not work.

26
00:01:06,00 --> 00:01:07,30
Thomas Edison observed,

27
00:01:07,30 --> 00:01:09,30
"Negative results are just what I want.

28
00:01:09,30 --> 00:01:11,90
"They're just as valuable to me as positive results.

29
00:01:11,90 --> 00:01:14,40
"I can never find the thing that does the job best

30
00:01:14,40 --> 00:01:16,90
"until I find the ones that don't."

31
00:01:16,90 --> 00:01:18,00
You may want to take some time

32
00:01:18,00 --> 00:01:19,40
and solve the problem yourself.

33
00:01:19,40 --> 00:01:20,50
In the next videos,

34
00:01:20,50 --> 00:01:23,00
I'll explain how I answered this challenge.

