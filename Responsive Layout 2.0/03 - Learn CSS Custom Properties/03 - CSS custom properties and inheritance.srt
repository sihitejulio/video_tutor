1
00:00:00,05 --> 00:00:01,06
- Lets take a closer look at how

2
00:00:01,06 --> 00:00:04,05
inheritance works with custom properties.

3
00:00:04,05 --> 00:00:07,08
It's just like other aspects of CSS inheritance.

4
00:00:07,08 --> 00:00:10,01
However, for whatever reason I find that

5
00:00:10,01 --> 00:00:11,07
people are surprised by the way

6
00:00:11,07 --> 00:00:14,05
custom properties behave in terms of inheritance.

7
00:00:14,05 --> 00:00:16,04
So let's look at an example in a

8
00:00:16,04 --> 00:00:18,07
little bit more detail and in isolation.

9
00:00:18,07 --> 00:00:21,08
So its starting here we have some HTML.

10
00:00:21,08 --> 00:00:23,04
We have an H1.

11
00:00:23,04 --> 00:00:26,06
We have a paragraph and then we have an article.

12
00:00:26,06 --> 00:00:30,04
Inside of that article we have an H2 and another paragraph.

13
00:00:30,04 --> 00:00:32,03
And those two paragraphs are what we are going to

14
00:00:32,03 --> 00:00:34,07
focus on a lot of the time here.

15
00:00:34,07 --> 00:00:37,01
Down in my CSS, as you'll see,

16
00:00:37,01 --> 00:00:38,08
I have my root declaration

17
00:00:38,08 --> 00:00:40,07
and I've set up a variable called the color

18
00:00:40,07 --> 00:00:43,00
and the color is set to red.

19
00:00:43,00 --> 00:00:45,07
And then I have just some empty declarations

20
00:00:45,07 --> 00:00:48,02
right now for the article and the paragraph.

21
00:00:48,02 --> 00:00:49,07
So the first thing that I want to do

22
00:00:49,07 --> 00:00:52,04
is I want to declare that my article

23
00:00:52,04 --> 00:00:56,01
will have a color of red.

24
00:00:56,01 --> 00:00:57,06
In other words, the text color

25
00:00:57,06 --> 00:01:00,00
is going to wind up being red.

26
00:01:00,00 --> 00:01:03,01
So what is going to show as red

27
00:01:03,01 --> 00:01:06,02
on this page as soon as I type in that code?

28
00:01:06,02 --> 00:01:08,03
Think about that for just a second,

29
00:01:08,03 --> 00:01:10,03
what is going to turn red when

30
00:01:10,03 --> 00:01:15,02
I declare that the article has red text?

31
00:01:15,02 --> 00:01:17,00
Okay, so if you've thought about that

32
00:01:17,00 --> 00:01:19,01
we'll simply say the word color

33
00:01:19,01 --> 00:01:22,05
and then we'll say var in parentheses,

34
00:01:22,05 --> 00:01:27,01
double dash, color and a semicolon.

35
00:01:27,01 --> 00:01:29,07
So hopefully you said that both the H2

36
00:01:29,07 --> 00:01:32,02
and the paragraph would be the color of red.

37
00:01:32,02 --> 00:01:35,06
Color property is one of those

38
00:01:35,06 --> 00:01:37,05
that's inherited so it will be

39
00:01:37,05 --> 00:01:40,07
article has that declaration of color,

40
00:01:40,07 --> 00:01:42,08
the H2 inherits the color from the article

41
00:01:42,08 --> 00:01:45,07
of the paragraph inherits that color from the article.

42
00:01:45,07 --> 00:01:48,05
So this is just straightforward CSS,

43
00:01:48,05 --> 00:01:53,04
this is exactly what you'd expect to happen.

44
00:01:53,04 --> 00:01:55,05
Now what would happen if I took

45
00:01:55,05 --> 00:01:57,02
that same line of code that I have

46
00:01:57,02 --> 00:02:00,02
here on line five, the color is the variable of color

47
00:02:00,02 --> 00:02:04,02
and I move that into the paragraph declaration instead.

48
00:02:04,02 --> 00:02:07,09
What will now have the text color of red?

49
00:02:07,09 --> 00:02:10,00
Think about that for a second.

50
00:02:10,00 --> 00:02:15,02
What would you expect to see?

51
00:02:15,02 --> 00:02:17,03
So now I'm going to move that here

52
00:02:17,03 --> 00:02:18,08
into the paragraph and hopefully

53
00:02:18,08 --> 00:02:21,07
you said that both of the paragraphs

54
00:02:21,07 --> 00:02:24,00
would have the color of red,

55
00:02:24,00 --> 00:02:25,07
because of course we said that

56
00:02:25,07 --> 00:02:27,02
the paragraphs will have the red.

57
00:02:27,02 --> 00:02:29,01
And so the paragraph outside the article

58
00:02:29,01 --> 00:02:31,07
has the color of red, that first paragraph

59
00:02:31,07 --> 00:02:33,03
and paragraph two that is

60
00:02:33,03 --> 00:02:37,04
in the article, that is also red.

61
00:02:37,04 --> 00:02:39,08
So now I'm going to do something kind of funky here,

62
00:02:39,08 --> 00:02:42,09
so at the beginning we have

63
00:02:42,09 --> 00:02:45,02
our root declaration with the color red.

64
00:02:45,02 --> 00:02:47,08
Now I'm going to re-declare that variable

65
00:02:47,08 --> 00:02:49,06
here inside of this article.

66
00:02:49,06 --> 00:02:52,07
So I'm going to make this become blue.

67
00:02:52,07 --> 00:02:54,09
And my question to you now is,

68
00:02:54,09 --> 00:02:57,02
what color is paragraph one,

69
00:02:57,02 --> 00:03:00,04
and what color is paragraph two?

70
00:03:00,04 --> 00:03:03,04
When I'm going ahead and make this declaration happen.

71
00:03:03,04 --> 00:03:05,05
So if I declare in my article

72
00:03:05,05 --> 00:03:07,04
that the color will be blue,

73
00:03:07,04 --> 00:03:10,01
and in the following declaration

74
00:03:10,01 --> 00:03:12,00
I have a paragraph declaration there,

75
00:03:12,00 --> 00:03:14,01
and I've just called for a color,

76
00:03:14,01 --> 00:03:19,09
what color is the two paragraphs?

77
00:03:19,09 --> 00:03:22,04
Okay, so in this situation, we have

78
00:03:22,04 --> 00:03:25,00
two paragraphs of different colors.

79
00:03:25,00 --> 00:03:26,07
How on earth does that work?

80
00:03:26,07 --> 00:03:30,00
Well remember that here, in paragraph one,

81
00:03:30,00 --> 00:03:32,00
we're inheriting the color declaration

82
00:03:32,00 --> 00:03:33,06
from the root of the article,

83
00:03:33,06 --> 00:03:35,08
in other words the color is red.

84
00:03:35,08 --> 00:03:39,08
But inside of the second paragraph,

85
00:03:39,08 --> 00:03:42,03
that's inside of the article,

86
00:03:42,03 --> 00:03:45,08
so we have red declared way outside

87
00:03:45,08 --> 00:03:49,05
at the very top of the document object model

88
00:03:49,05 --> 00:03:52,00
that that whole tree of HTML,

89
00:03:52,00 --> 00:03:54,05
then we have the article that re-declares

90
00:03:54,05 --> 00:03:56,09
the color as blue and since the

91
00:03:56,09 --> 00:03:59,03
second paragraph is inside of the article,

92
00:03:59,03 --> 00:04:01,01
it will inherit blue from the article

93
00:04:01,01 --> 00:04:03,03
rather than red from the root.

94
00:04:03,03 --> 00:04:04,08
So we actually can wind up with

95
00:04:04,08 --> 00:04:07,06
two paragraphs of different colors

96
00:04:07,06 --> 00:04:08,09
even though that looks like they

97
00:04:08,09 --> 00:04:11,02
have the same declaration there.

98
00:04:11,02 --> 00:04:13,02
And so this is what I'm really talking about

99
00:04:13,02 --> 00:04:14,07
when I say custom properties

100
00:04:14,07 --> 00:04:18,03
are going to inherit their properties

101
00:04:18,03 --> 00:04:20,05
just the same way regular CSS works.

102
00:04:20,05 --> 00:04:22,01
This is pretty different than what

103
00:04:22,01 --> 00:04:24,03
you normally see with variable declarations

104
00:04:24,03 --> 00:04:27,00
in other programming languages.

