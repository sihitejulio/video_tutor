1
00:00:00,05 --> 00:00:01,09
- [Instructor] As I've talked about calc

2
00:00:01,09 --> 00:00:05,03
and custom properties in my classes and at conferences,

3
00:00:05,03 --> 00:00:08,04
a common question I get is whether it makes sense

4
00:00:08,04 --> 00:00:12,04
to continue using Sass.

5
00:00:12,04 --> 00:00:15,02
Yes, absolutely.

6
00:00:15,02 --> 00:00:17,09
Remember that Sass has lots of features

7
00:00:17,09 --> 00:00:20,05
that aren't yet available in CSS

8
00:00:20,05 --> 00:00:23,03
including mixins and extends.

9
00:00:23,03 --> 00:00:25,09
Remember, these are ways that we can

10
00:00:25,09 --> 00:00:27,09
reuse little bits of code.

11
00:00:27,09 --> 00:00:30,03
We can either extend that code.

12
00:00:30,03 --> 00:00:33,07
In other words, continue to add on to styles with an extend,

13
00:00:33,07 --> 00:00:37,03
or we can have some code where we're passing in values.

14
00:00:37,03 --> 00:00:40,01
That's a thing that we can do with mixins.

15
00:00:40,01 --> 00:00:44,01
There's nesting, one of the most favorite features of Sass

16
00:00:44,01 --> 00:00:45,09
for all the programmers out there.

17
00:00:45,09 --> 00:00:49,06
Nesting makes your Sass a little bit easier to read.

18
00:00:49,06 --> 00:00:51,09
All of those wonderful file management functions

19
00:00:51,09 --> 00:00:55,04
like the partial files where you can split your Sass up

20
00:00:55,04 --> 00:00:57,04
over a whole bunch of different files.

21
00:00:57,04 --> 00:00:59,05
Doesn't matter how many 'cause when you compile,

22
00:00:59,05 --> 00:01:02,02
it compiles down to a single CSS file.

23
00:01:02,02 --> 00:01:04,09
And then all of those wonderful built-in functions

24
00:01:04,09 --> 00:01:07,01
that are available to you with Sass.

25
00:01:07,01 --> 00:01:08,09
Some of my favorites are the lighten

26
00:01:08,09 --> 00:01:10,02
and the darken functions.

27
00:01:10,02 --> 00:01:13,02
Those are just so easy to just access and use

28
00:01:13,02 --> 00:01:14,09
for color manipulation,

29
00:01:14,09 --> 00:01:17,03
or you have properties like rounding

30
00:01:17,03 --> 00:01:20,08
and many, many other built-in functions in Sass.

31
00:01:20,08 --> 00:01:24,02
And of course, you can write your own functions as well.

32
00:01:24,02 --> 00:01:30,01
So when should you use Sass versus custom properties?

33
00:01:30,01 --> 00:01:32,02
Well what I recommend is as follows.

34
00:01:32,02 --> 00:01:36,01
First of all, use your Sass variables, or Sass math,

35
00:01:36,01 --> 00:01:37,07
when you're establishing something

36
00:01:37,07 --> 00:01:40,08
that's going to be universal throughout your document.

37
00:01:40,08 --> 00:01:42,09
For example, it's unlikely you're going to

38
00:01:42,09 --> 00:01:46,08
change your color scheme across your media queries,

39
00:01:46,08 --> 00:01:49,00
or maybe your font families.

40
00:01:49,00 --> 00:01:50,08
Those are all going to be exactly the same

41
00:01:50,08 --> 00:01:53,02
between your mobile and your desktop layouts.

42
00:01:53,02 --> 00:01:56,06
However, you might change a font size

43
00:01:56,06 --> 00:01:59,07
depending on where you are in your media queries,

44
00:01:59,07 --> 00:02:03,07
and that would be better dealt with via custom properties.

45
00:02:03,07 --> 00:02:08,07
Second, you can use custom properties for variables and math

46
00:02:08,07 --> 00:02:12,06
where the values will be changing in the media queries.

47
00:02:12,06 --> 00:02:15,01
For example, you might change your font sizes

48
00:02:15,01 --> 00:02:16,06
for your mobile devices,

49
00:02:16,06 --> 00:02:18,01
and that might be totally different

50
00:02:18,01 --> 00:02:21,06
than what you're doing on a desktop device.

51
00:02:21,06 --> 00:02:24,00
Rather than repeating the same styles

52
00:02:24,00 --> 00:02:26,02
over and over again with the new values

53
00:02:26,02 --> 00:02:28,01
inside of your media queries,

54
00:02:28,01 --> 00:02:31,02
we're going to establish that logic for the layout

55
00:02:31,02 --> 00:02:34,02
outside of the media query as a default.

56
00:02:34,02 --> 00:02:36,08
So in other words, we're going to establish the layout

57
00:02:36,08 --> 00:02:38,02
where there's no media queries.

58
00:02:38,02 --> 00:02:40,01
That's the mobile first approach,

59
00:02:40,01 --> 00:02:42,07
and that's where we're going to put in all of the logic

60
00:02:42,07 --> 00:02:46,00
for how the page should be laid out.

61
00:02:46,00 --> 00:02:50,01
And then inside of the media query,

62
00:02:50,01 --> 00:02:51,09
this is where we're going to change

63
00:02:51,09 --> 00:02:54,00
the values of the custom property

64
00:02:54,00 --> 00:02:58,05
rather than redeclaring our styles over and over again.

65
00:02:58,05 --> 00:03:01,08
If this approach isn't exactly clear to you,

66
00:03:01,08 --> 00:03:04,02
I will be going through this in great detail

67
00:03:04,02 --> 00:03:06,05
in the coming videos.

68
00:03:06,05 --> 00:03:10,03
Remember that Sass can't change its variable values

69
00:03:10,03 --> 00:03:13,07
inside of media queries and impact the design

70
00:03:13,07 --> 00:03:16,06
in the way that custom properties can.

71
00:03:16,06 --> 00:03:20,06
If you want to change a variable value within a media query,

72
00:03:20,06 --> 00:03:25,00
custom properties are always the way to go.

