1
00:00:00,00 --> 00:00:06,02
(upbeat music)

2
00:00:06,02 --> 00:00:08,00
- [Instructor] Remember the type scale challenge

3
00:00:08,00 --> 00:00:09,07
from the calc chapter?

4
00:00:09,07 --> 00:00:11,04
We wrote a whole bunch of code

5
00:00:11,04 --> 00:00:14,04
to use calc in calculating our type scale,

6
00:00:14,04 --> 00:00:17,05
but ultimately we're not really happy with this result.

7
00:00:17,05 --> 00:00:20,09
It's clear how to derive each value in the scale,

8
00:00:20,09 --> 00:00:23,02
but oh my goodness, it's so repetitious,

9
00:00:23,02 --> 00:00:25,01
no one wants to look at this.

10
00:00:25,01 --> 00:00:28,08
So, let's rewrite this type scale using custom properties,

11
00:00:28,08 --> 00:00:31,08
now that you know a little bit more about them.

12
00:00:31,08 --> 00:00:35,03
To get you started on this, I'll give you a few hints.

13
00:00:35,03 --> 00:00:37,02
One of the things you probably want to do

14
00:00:37,02 --> 00:00:39,06
is think about what are the numbers

15
00:00:39,06 --> 00:00:42,01
that you're going to multiply together

16
00:00:42,01 --> 00:00:44,02
to take the place of all of these numbers

17
00:00:44,02 --> 00:00:47,00
that are repeated over and over again here?

18
00:00:47,00 --> 00:00:50,07
So, think about that for a second.

19
00:00:50,07 --> 00:00:53,09
So, you're probably going to do something like this.

20
00:00:53,09 --> 00:00:56,00
You'll have your root declaration here

21
00:00:56,00 --> 00:00:58,02
and inside of that we'll say something like

22
00:00:58,02 --> 00:01:04,02
our base size will be 1m, correct?

23
00:01:04,02 --> 00:01:07,09
And maybe we'll have a type scale with that.

24
00:01:07,09 --> 00:01:11,06
And I'm going to give that a value of 1.2.

25
00:01:11,06 --> 00:01:15,06
So then, instead of all of this math that's going on here,

26
00:01:15,06 --> 00:01:18,05
what we can do down here for our font size

27
00:01:18,05 --> 00:01:20,03
instead of just saying 1m,

28
00:01:20,03 --> 00:01:27,09
we would say instead our var would be the base-size, yeah?

29
00:01:27,09 --> 00:01:29,08
Much nicer to look at.

30
00:01:29,08 --> 00:01:32,07
And then, if we had some kind of math that we were doing,

31
00:01:32,07 --> 00:01:35,05
let's say this small down here,

32
00:01:35,05 --> 00:01:39,02
instead of doing our calc of 1m divided by 1.2,

33
00:01:39,02 --> 00:01:40,06
we would say what?

34
00:01:40,06 --> 00:01:52,07
Our var is a base-size divided by our var of type scale.

35
00:01:52,07 --> 00:01:54,01
Just like that.

36
00:01:54,01 --> 00:01:59,00
And so, now that math will happen using the variables

37
00:01:59,00 --> 00:02:02,04
rather than all those different numbers.

38
00:02:02,04 --> 00:02:04,08
So, in this challenge, what I'd like for you to do

39
00:02:04,08 --> 00:02:07,04
is to take a look at h1 to h5

40
00:02:07,04 --> 00:02:12,01
and see if you can rewrite those leveraging the importance

41
00:02:12,01 --> 00:02:15,04
of the base-size and the type scale.

42
00:02:15,04 --> 00:02:19,00
Protip, this may not be all of the variables

43
00:02:19,00 --> 00:02:20,06
that you want to declare here.

44
00:02:20,06 --> 00:02:22,01
Maybe there's some other variables

45
00:02:22,01 --> 00:02:24,01
you'd like to declare as well.

46
00:02:24,01 --> 00:02:27,00
I'll show you my answer in the next video.

