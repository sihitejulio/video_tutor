1
00:00:00,05 --> 00:00:02,04
- [Instructor] Recently available for our use,

2
00:00:02,04 --> 00:00:06,03
custom properties are an exciting new development in CSS.

3
00:00:06,03 --> 00:00:09,06
A custom property is similar to a variable,

4
00:00:09,06 --> 00:00:10,08
which you might've encountered

5
00:00:10,08 --> 00:00:15,00
in other programming languages or in Sass.

6
00:00:15,00 --> 00:00:17,06
And if you haven't programmed in other languages,

7
00:00:17,06 --> 00:00:20,09
well, you've likely encountered our old friend x

8
00:00:20,09 --> 00:00:23,03
when you did algebra back in school.

9
00:00:23,03 --> 00:00:26,01
X could mean anything you wanted it to mean.

10
00:00:26,01 --> 00:00:28,04
Substitute x in a math problem

11
00:00:28,04 --> 00:00:30,06
and we know how to use the value of x

12
00:00:30,06 --> 00:00:34,00
to get that math problem computed.

13
00:00:34,00 --> 00:00:37,02
Custom properties work just like these kinds of variables,

14
00:00:37,02 --> 00:00:40,01
but there's a few differences to be aware of.

15
00:00:40,01 --> 00:00:45,03
First of all, define a variable within a CSS declaration.

16
00:00:45,03 --> 00:00:48,07
In other words, you'll have to declare a selector

17
00:00:48,07 --> 00:00:52,01
with values in it in order to declare the variable

18
00:00:52,01 --> 00:00:55,05
rather than just saying x equals two.

19
00:00:55,05 --> 00:00:59,03
Next, values of variables inherit.

20
00:00:59,03 --> 00:01:03,06
If you place a variable value at the top level of a webpage,

21
00:01:03,06 --> 00:01:07,05
then all of the webpage can access that value.

22
00:01:07,05 --> 00:01:10,02
If you place it at the paragraph level,

23
00:01:10,02 --> 00:01:13,00
then only the paragraph and its children

24
00:01:13,00 --> 00:01:14,09
can access that value.

25
00:01:14,09 --> 00:01:17,03
This is the concept of scoping,

26
00:01:17,03 --> 00:01:20,08
where a value is available in selected portions

27
00:01:20,08 --> 00:01:23,08
of your webpage, and in this case, it's based on the DOM,

28
00:01:23,08 --> 00:01:26,01
the document object model.

29
00:01:26,01 --> 00:01:28,05
Just like everything else in CSS,

30
00:01:28,05 --> 00:01:32,00
the value of a given variable may be overridden

31
00:01:32,00 --> 00:01:35,01
by leveraging the cascade and inheritance.

32
00:01:35,01 --> 00:01:38,05
All of the usual CSS rules you've come to know and love

33
00:01:38,05 --> 00:01:41,03
are still in effect here.

34
00:01:41,03 --> 00:01:44,03
And finally, CSS custom properties

35
00:01:44,03 --> 00:01:48,02
may be accessed and manipulated through JavaScript.

36
00:01:48,02 --> 00:01:50,02
This is beyond the scope of this course,

37
00:01:50,02 --> 00:01:52,05
but you'll find plenty of online resources

38
00:01:52,05 --> 00:01:55,05
showing examples of this behavior.

39
00:01:55,05 --> 00:01:57,08
There are tons of big advantages

40
00:01:57,08 --> 00:02:00,00
to working with custom properties.

41
00:02:00,00 --> 00:02:03,01
First of all, don't repeat yourself.

42
00:02:03,01 --> 00:02:05,00
Instead of spelling out values

43
00:02:05,00 --> 00:02:07,09
over and over and over again in your style sheet,

44
00:02:07,09 --> 00:02:12,09
declare them once and refer to their values over and over.

45
00:02:12,09 --> 00:02:15,03
Second, custom property values

46
00:02:15,03 --> 00:02:17,06
may be changed in media queries.

47
00:02:17,06 --> 00:02:19,03
This is a huge advantage,

48
00:02:19,03 --> 00:02:21,09
one that custom properties have over Sass

49
00:02:21,09 --> 00:02:24,04
and other CSS preprocessors.

50
00:02:24,04 --> 00:02:27,03
We'll be talking about how to leverage this big advantage

51
00:02:27,03 --> 00:02:30,02
in many of the coming videos.

52
00:02:30,02 --> 00:02:33,02
Since custom properties are native to CSS,

53
00:02:33,02 --> 00:02:35,00
there's no compiling required

54
00:02:35,00 --> 00:02:39,04
as there is when you use a CSS processor like Sass.

55
00:02:39,04 --> 00:02:42,01
And finally, there is not a major hit

56
00:02:42,01 --> 00:02:43,08
to the performance of your website

57
00:02:43,08 --> 00:02:46,05
by using calc or custom properties.

58
00:02:46,05 --> 00:02:49,05
You'll be better served to improve your performance

59
00:02:49,05 --> 00:02:53,04
by reducing image sizes, minimizing your hosted fonts,

60
00:02:53,04 --> 00:02:56,08
and StreamLiving your JavaScript.

61
00:02:56,08 --> 00:02:59,06
So, if custom properties are so amazing,

62
00:02:59,06 --> 00:03:01,08
what's the disadvantage?

63
00:03:01,08 --> 00:03:05,00
Ya got it, browser support.

64
00:03:05,00 --> 00:03:09,06
As you can see here on caniuse.com, there's no support

65
00:03:09,06 --> 00:03:12,05
for custom properties in Internet Explorer.

66
00:03:12,05 --> 00:03:16,06
As of this recording, IE11 is almost six years old,

67
00:03:16,06 --> 00:03:19,01
and the only versions are even older.

68
00:03:19,01 --> 00:03:22,03
We'll see IE disappear from the web landscape shortly.

69
00:03:22,03 --> 00:03:24,08
Indeed, many developers have already dropped

70
00:03:24,08 --> 00:03:26,02
support for this browser.

71
00:03:26,02 --> 00:03:29,09
Still, at 92% support,

72
00:03:29,09 --> 00:03:31,04
we aren't far from being able

73
00:03:31,04 --> 00:03:34,03
to use this technology on most websites,

74
00:03:34,03 --> 00:03:36,05
and some developers already are.

75
00:03:36,05 --> 00:03:39,01
As always, be sure to be familiar with the browsers

76
00:03:39,01 --> 00:03:41,01
most important to your target audience

77
00:03:41,01 --> 00:03:44,00
before implementing these new technologies.

