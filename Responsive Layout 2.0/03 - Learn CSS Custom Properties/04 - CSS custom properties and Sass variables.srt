1
00:00:00,05 --> 00:00:03,01
- Now that you've seen how custom properties work,

2
00:00:03,01 --> 00:00:06,01
in terms of their inheritance and values and so forth,

3
00:00:06,01 --> 00:00:08,06
let's try that same experiment again,

4
00:00:08,06 --> 00:00:10,07
this time using Sass variables.

5
00:00:10,07 --> 00:00:13,02
So this is exactly the same HTML

6
00:00:13,02 --> 00:00:15,08
that we had in the previous example

7
00:00:15,08 --> 00:00:18,06
where we have one paragraph outside the article,

8
00:00:18,06 --> 00:00:21,04
one paragraph inside the article.

9
00:00:21,04 --> 00:00:23,03
And of course our syntax is going

10
00:00:23,03 --> 00:00:26,00
to be different here in this CSS panel

11
00:00:26,00 --> 00:00:27,08
because this is actually Sass.

12
00:00:27,08 --> 00:00:30,02
Remember that with Sass, we simply declare

13
00:00:30,02 --> 00:00:31,08
a variable by putting the dollar sign

14
00:00:31,08 --> 00:00:33,08
in front of it and just saying color

15
00:00:33,08 --> 00:00:36,01
in this case that is color the variable name.

16
00:00:36,01 --> 00:00:39,05
And we've assigned it a value of red.

17
00:00:39,05 --> 00:00:41,00
So in order to declare this,

18
00:00:41,00 --> 00:00:42,09
if I do exactly the same thing

19
00:00:42,09 --> 00:00:46,02
as I did before, if I declare the text color

20
00:00:46,02 --> 00:00:48,03
inside of my article to be red,

21
00:00:48,03 --> 00:00:52,00
what would you expect to see here on the screen?

22
00:00:52,00 --> 00:00:56,02
Think about that just for a second.

23
00:00:56,02 --> 00:00:58,05
And so here I'll say the color property

24
00:00:58,05 --> 00:01:03,03
is the color variable and this is exactly

25
00:01:03,03 --> 00:01:05,07
the same response that we had

26
00:01:05,07 --> 00:01:08,06
with the CSS custom properties,

27
00:01:08,06 --> 00:01:11,02
both H2 and the paragraph inherit

28
00:01:11,02 --> 00:01:13,09
the color red from the article declaration

29
00:01:13,09 --> 00:01:15,07
and everything is red there.

30
00:01:15,07 --> 00:01:18,01
Obviously the paragraph outside the article

31
00:01:18,01 --> 00:01:20,09
doesn't have anything to do with that

32
00:01:20,09 --> 00:01:24,00
declaration and so it stays black.

33
00:01:24,00 --> 00:01:27,02
Now, what happens if I move this

34
00:01:27,02 --> 00:01:29,04
color declaration that I put on the article?

35
00:01:29,04 --> 00:01:31,03
What if I move that to the paragraph

36
00:01:31,03 --> 00:01:32,09
just as I did before?

37
00:01:32,09 --> 00:01:34,06
So instead of declaring the color here

38
00:01:34,06 --> 00:01:36,07
in the article, what's going to happen

39
00:01:36,07 --> 00:01:39,01
if I move this to the paragraph declaration?

40
00:01:39,01 --> 00:01:41,07
Think about that for a second.

41
00:01:41,07 --> 00:01:43,08
Pretty straightforward, right?

42
00:01:43,08 --> 00:01:45,02
Basically the same thing that happened

43
00:01:45,02 --> 00:01:47,01
with custom properties as well

44
00:01:47,01 --> 00:01:48,06
and both of our paragraphs

45
00:01:48,06 --> 00:01:52,02
wind up being the color red.

46
00:01:52,02 --> 00:01:54,06
Okay, so now I'm going to move

47
00:01:54,06 --> 00:01:58,05
that color declaration back to my article

48
00:01:58,05 --> 00:02:01,00
and then what I'm going to do here

49
00:02:01,00 --> 00:02:03,07
in between my article declaration

50
00:02:03,07 --> 00:02:05,08
and my paragraph declaration

51
00:02:05,08 --> 00:02:09,02
I'm going to re-declare my Sass variable.

52
00:02:09,02 --> 00:02:14,02
So I'm going to have a color of color

53
00:02:14,02 --> 00:02:17,07
here both on my article and on my paragraph

54
00:02:17,07 --> 00:02:20,02
and in between here I'm going

55
00:02:20,02 --> 00:02:24,00
to redefine the value of color.

56
00:02:24,00 --> 00:02:27,03
So this syntax is a little bit different

57
00:02:27,03 --> 00:02:29,07
than what you saw with the custom properties

58
00:02:29,07 --> 00:02:32,07
but effectively it's the same kind of thing.

59
00:02:32,07 --> 00:02:35,01
I'm going to just re-declare this to be blue.

60
00:02:35,01 --> 00:02:37,09
What do you expect to see now in terms

61
00:02:37,09 --> 00:02:41,07
of the color arrangement here on this page?

62
00:02:41,07 --> 00:02:46,01
So think about that for a second.

63
00:02:46,01 --> 00:02:49,08
And so this is the way the colors wind up working now.

64
00:02:49,08 --> 00:02:52,04
So in other words let's think about this,

65
00:02:52,04 --> 00:02:55,08
here I have my Sass variable declared here,

66
00:02:55,08 --> 00:02:59,05
color of red, and the article comes after that.

67
00:02:59,05 --> 00:03:01,08
It takes on that red color

68
00:03:01,08 --> 00:03:03,06
and you see over in the display

69
00:03:03,06 --> 00:03:08,02
the H2, My article, that is red.

70
00:03:08,02 --> 00:03:11,05
Then I re-declare my color of blue,

71
00:03:11,05 --> 00:03:14,04
a little bit later on, and the paragraph

72
00:03:14,04 --> 00:03:15,07
that comes after that, well,

73
00:03:15,07 --> 00:03:19,01
it takes on the color of the previous declaration,

74
00:03:19,01 --> 00:03:21,03
in other words this declaration on line five

75
00:03:21,03 --> 00:03:24,04
is overriding what you saw on line one.

76
00:03:24,04 --> 00:03:27,04
So now my paragraphs are both blue

77
00:03:27,04 --> 00:03:29,06
instead of being red.

78
00:03:29,06 --> 00:03:31,09
And that's just a little bit of the difference

79
00:03:31,09 --> 00:03:34,03
here between Sass and the custom properties

80
00:03:34,03 --> 00:03:37,00
and how their inheritance works.

