1
00:00:00,01 --> 00:00:06,04
(upbeat music)

2
00:00:06,04 --> 00:00:09,01
- So, how'd you do with that challenge?

3
00:00:09,01 --> 00:00:11,00
Hopefully you got most of these

4
00:00:11,00 --> 00:00:12,09
the way that I've done them here.

5
00:00:12,09 --> 00:00:14,06
Some of you, I suspect,

6
00:00:14,06 --> 00:00:17,01
might have taken your H1 to H5 here

7
00:00:17,01 --> 00:00:19,05
and just substituted the variable names

8
00:00:19,05 --> 00:00:23,02
for all of those 1.2's that we were multiplying by,

9
00:00:23,02 --> 00:00:24,08
so like five of them.

10
00:00:24,08 --> 00:00:28,05
Instead, what I did here was I simply defined

11
00:00:28,05 --> 00:00:32,00
my various heading sizes

12
00:00:32,00 --> 00:00:33,09
and then did the math from here

13
00:00:33,09 --> 00:00:35,08
just to make things a little bit easier to read.

14
00:00:35,08 --> 00:00:39,05
So we started with our H5

15
00:00:39,05 --> 00:00:44,09
which previously had been just, uh, 1M times 1.2

16
00:00:44,09 --> 00:00:47,05
I went ahead and wrote that here as our H5,

17
00:00:47,05 --> 00:00:50,01
So our base size times our type scale

18
00:00:50,01 --> 00:00:52,04
and set that to be the value of H5,

19
00:00:52,04 --> 00:00:54,09
and then declared that value down here

20
00:00:54,09 --> 00:00:57,00
so we just go ahead and do that math

21
00:00:57,00 --> 00:00:59,08
and that becomes the size of our H5.

22
00:00:59,08 --> 00:01:02,06
Then for H4 all I have to do is take the value of H5

23
00:01:02,06 --> 00:01:05,00
and multiply it by the type scale again

24
00:01:05,00 --> 00:01:07,00
and write it here as an H4,

25
00:01:07,00 --> 00:01:10,06
and we just continue to work through that series,

26
00:01:10,06 --> 00:01:13,09
and write out all of the values down here in our headings,

27
00:01:13,09 --> 00:01:16,02
so it's a little bit clearer.

28
00:01:16,02 --> 00:01:20,01
Now I've called these variables H1 through H5.

29
00:01:20,01 --> 00:01:23,03
You could call them whatever, font size base,

30
00:01:23,03 --> 00:01:27,09
font size larger, or font size two, or font size three,

31
00:01:27,09 --> 00:01:29,01
something like that.

32
00:01:29,01 --> 00:01:30,06
You could certainly do that.

33
00:01:30,06 --> 00:01:32,05
The way that this is formatted,

34
00:01:32,05 --> 00:01:36,02
obviously in real html and CSS

35
00:01:36,02 --> 00:01:39,06
your headings have a very specific semantic meaning,

36
00:01:39,06 --> 00:01:43,02
but remember that your variable declarations here do not.

37
00:01:43,02 --> 00:01:47,00
So, for example, if later on I wanted to have an H1

38
00:01:47,00 --> 00:01:49,02
that was a very small size,

39
00:01:49,02 --> 00:01:51,07
I could say that H1,

40
00:01:51,07 --> 00:01:53,00
probably not the selector,

41
00:01:53,00 --> 00:01:54,09
but probably something like, whatever,

42
00:01:54,09 --> 00:01:57,05
a section H1 or something like that

43
00:01:57,05 --> 00:01:59,07
would have a font size of H5.

44
00:01:59,07 --> 00:02:02,07
That would give me a smaller H1 font size.

45
00:02:02,07 --> 00:02:07,07
So it's possible to mix and match our variable names,

46
00:02:07,07 --> 00:02:09,08
but don't confuse that with the semantics

47
00:02:09,08 --> 00:02:13,01
of what's going on here in your html.

48
00:02:13,01 --> 00:02:15,04
Obviously those tags have very specific meaning

49
00:02:15,04 --> 00:02:16,08
and that should be honored,

50
00:02:16,08 --> 00:02:20,00
but your variable names can be called whatever you want.

