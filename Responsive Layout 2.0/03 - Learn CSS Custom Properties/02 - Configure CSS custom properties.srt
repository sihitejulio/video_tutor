1
00:00:00,04 --> 00:00:01,03
- [Female narrator] Now that you've heard

2
00:00:01,03 --> 00:00:03,08
a little background about custom properties,

3
00:00:03,08 --> 00:00:05,06
let's see how to configure them

4
00:00:05,06 --> 00:00:07,00
and use them in your CSS.

5
00:00:07,00 --> 00:00:11,03
Declaring a variable is really easy.

6
00:00:11,03 --> 00:00:12,01
First of all,

7
00:00:12,01 --> 00:00:14,03
configure your CSS declaration

8
00:00:14,03 --> 00:00:17,01
with a selector and a declaration body,

9
00:00:17,01 --> 00:00:18,09
just as you always do.

10
00:00:18,09 --> 00:00:22,01
Notice here I'm using the pseudo-class of root.

11
00:00:22,01 --> 00:00:24,00
This is a more specific selector

12
00:00:24,00 --> 00:00:26,06
than the usual HTML element selector,

13
00:00:26,06 --> 00:00:28,03
which may be an advantage.

14
00:00:28,03 --> 00:00:32,07
The root selector also selects the full root

15
00:00:32,07 --> 00:00:34,08
of the document where the HTML elements

16
00:00:34,08 --> 00:00:36,07
selector may not do that,

17
00:00:36,07 --> 00:00:38,04
and that may matter especially

18
00:00:38,04 --> 00:00:41,07
if you're manipulating these values with JavaScript.

19
00:00:41,07 --> 00:00:43,05
By using the root selector,

20
00:00:43,05 --> 00:00:45,07
you're making any variable declarations

21
00:00:45,07 --> 00:00:48,01
here available to your full document,

22
00:00:48,01 --> 00:00:50,07
wherever you'd like to call on the variables.

23
00:00:50,07 --> 00:00:55,06
So essentially anything that's here is scoped globally.

24
00:00:55,06 --> 00:00:57,07
Inside of the declaration body,

25
00:00:57,07 --> 00:01:00,06
declare your variables starting with a double hyphen

26
00:01:00,06 --> 00:01:02,03
followed by the name of the variable

27
00:01:02,03 --> 00:01:03,09
you'll use plus it's value.

28
00:01:03,09 --> 00:01:07,05
Here, I've called my variable font-size-large

29
00:01:07,05 --> 00:01:09,09
and I've given it a value of 2rem,

30
00:01:09,09 --> 00:01:11,00
which makes sense.

31
00:01:11,00 --> 00:01:13,08
Be aware that there's no error checking

32
00:01:13,08 --> 00:01:14,08
here in this declaration.

33
00:01:14,08 --> 00:01:17,07
So if I set my font-size-large to blue,

34
00:01:17,07 --> 00:01:21,03
CSS would be perfectly happy with that.

35
00:01:21,03 --> 00:01:23,03
Next, you're going to call the variable

36
00:01:23,03 --> 00:01:25,01
elsewhere in your document.

37
00:01:25,01 --> 00:01:27,06
Here, I've set a class of wrapper to have

38
00:01:27,06 --> 00:01:30,04
a font size value of 2rem

39
00:01:30,04 --> 00:01:32,05
using the variable that I declared earlier.

40
00:01:32,05 --> 00:01:35,01
You declare the variable by using var

41
00:01:35,01 --> 00:01:36,06
with a set of parenthesis,

42
00:01:36,06 --> 00:01:39,01
and inside the parenthesis include

43
00:01:39,01 --> 00:01:42,08
the variable name with the double hyphens.

44
00:01:42,08 --> 00:01:44,07
You can declare pretty much anything

45
00:01:44,07 --> 00:01:46,02
as a CSS custom property.

46
00:01:46,02 --> 00:01:47,07
Here are some examples,

47
00:01:47,07 --> 00:01:49,05
including colors, font sizes,

48
00:01:49,05 --> 00:01:51,06
file paths, short-hand values,

49
00:01:51,06 --> 00:01:54,02
or even something like a cubic bezier

50
00:01:54,02 --> 00:01:57,03
used in CSS animation.

51
00:01:57,03 --> 00:02:00,01
Once again, I want to emphasize that CSS

52
00:02:00,01 --> 00:02:03,00
does not have a typed variable of any kind.

53
00:02:03,00 --> 00:02:05,04
Here, I've said the wrapper will have

54
00:02:05,04 --> 00:02:07,06
a font size of an image?

55
00:02:07,06 --> 00:02:10,00
Yep, perfectly legal and it will

56
00:02:10,00 --> 00:02:11,08
happily pass validation.

57
00:02:11,08 --> 00:02:13,08
However, the browser will recognize this

58
00:02:13,08 --> 00:02:16,02
as not valid and will skip over this

59
00:02:16,02 --> 00:02:18,05
font size declaration here in the wrapper.

60
00:02:18,05 --> 00:02:20,08
So be careful when you're using your variables,

61
00:02:20,08 --> 00:02:22,08
it's really easy to make a mistake

62
00:02:22,08 --> 00:02:26,00
that may not be obvious in the debugging process.

