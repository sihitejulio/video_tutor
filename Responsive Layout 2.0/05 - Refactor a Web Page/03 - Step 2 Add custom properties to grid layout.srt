1
00:00:00,04 --> 00:00:04,00
- [Instructor] With our grid layout in place and understood

2
00:00:04,00 --> 00:00:07,09
let's rework what we have here with custom properties.

3
00:00:07,09 --> 00:00:11,07
Remember the goal is to put all of the logic

4
00:00:11,07 --> 00:00:14,00
and all of the declarations in one place,

5
00:00:14,00 --> 00:00:18,07
which is outside the media queries in our mobile layout

6
00:00:18,07 --> 00:00:20,07
and then anything that's going to change

7
00:00:20,07 --> 00:00:24,05
from the mobile layout into our media queries

8
00:00:24,05 --> 00:00:27,08
that will become something with a custom property.

9
00:00:27,08 --> 00:00:31,01
So let's just take a look at our layout here to start with.

10
00:00:31,01 --> 00:00:33,06
I have a little comment up here on the top.

11
00:00:33,06 --> 00:00:34,09
As part of our layout,

12
00:00:34,09 --> 00:00:38,02
we had some margins in place that worked really well

13
00:00:38,02 --> 00:00:40,03
for mobile dimensions.

14
00:00:40,03 --> 00:00:44,03
And if you notice, I changed some of those values here

15
00:00:44,03 --> 00:00:45,07
inside the media query.

16
00:00:45,07 --> 00:00:48,05
So for example, we can start with the content.

17
00:00:48,05 --> 00:00:52,08
Here, I have a margin of two rem, one rem here outside

18
00:00:52,08 --> 00:00:54,04
of the media queries.

19
00:00:54,04 --> 00:00:56,00
When we go into the media queries,

20
00:00:56,00 --> 00:00:59,07
I change that margin to three rem auto.

21
00:00:59,07 --> 00:01:01,08
And that's pretty much where it stays.

22
00:01:01,08 --> 00:01:04,09
We don't really change anything again down here at desktop.

23
00:01:04,09 --> 00:01:07,08
Pretty much those margin changes are just happening

24
00:01:07,08 --> 00:01:10,03
between mobile and tablet.

25
00:01:10,03 --> 00:01:13,05
And then of course, they'll carry into the desktop layout.

26
00:01:13,05 --> 00:01:15,03
So we've got that one there.

27
00:01:15,03 --> 00:01:17,02
That's one change that's happening.

28
00:01:17,02 --> 00:01:19,03
And another change that's happening is here

29
00:01:19,03 --> 00:01:22,07
where we go from a margin bottom of three rem here

30
00:01:22,07 --> 00:01:26,07
on the Aside and Main to at least on the Aside,

31
00:01:26,07 --> 00:01:29,01
we have a margin bottom of zero.

32
00:01:29,01 --> 00:01:33,01
And so that is something else that we want to reflect

33
00:01:33,01 --> 00:01:35,00
as we go forward.

34
00:01:35,00 --> 00:01:37,03
So a way that we could go about writing this

35
00:01:37,03 --> 00:01:39,01
would be as follows.

36
00:01:39,01 --> 00:01:41,08
Let's go ahead and start here on layout.

37
00:01:41,08 --> 00:01:46,01
We can just go on ahead and declare this with root.

38
00:01:46,01 --> 00:01:47,04
And if you want to scope it

39
00:01:47,04 --> 00:01:50,08
by putting it on some other portion

40
00:01:50,08 --> 00:01:55,05
of the document tree and declare your values there

41
00:01:55,05 --> 00:01:57,04
that will work also,

42
00:01:57,04 --> 00:02:01,04
so we'll go ahead and let's call it content-margin

43
00:02:01,04 --> 00:02:03,01
because that's what it is

44
00:02:03,01 --> 00:02:06,00
and we'll give it a value of two rem, one rem

45
00:02:06,00 --> 00:02:09,00
because that's what it is to get us started.

46
00:02:09,00 --> 00:02:10,02
And then here in content instead of saying margin two,

47
00:02:10,02 --> 00:02:21,04
one rem, we'll just say var content-margin.

48
00:02:21,04 --> 00:02:25,03
Okay, so that's how we get that started.

49
00:02:25,03 --> 00:02:28,02
And then when we go to our media query,

50
00:02:28,02 --> 00:02:29,05
what we're going to do here

51
00:02:29,05 --> 00:02:32,07
is we're going to read a clear root again

52
00:02:32,07 --> 00:02:35,03
and with root, then all we have to do is declare

53
00:02:35,03 --> 00:02:41,09
our content-margin to now have a new value

54
00:02:41,09 --> 00:02:47,07
and that new value is going to be three rem auto.

55
00:02:47,07 --> 00:02:50,08
So in other words, we no longer hear with our content.

56
00:02:50,08 --> 00:02:53,08
We no longer need to make this declaration here for margins.

57
00:02:53,08 --> 00:02:57,07
So with that we'll just go away.

58
00:02:57,07 --> 00:02:59,08
Let's go through another example of this

59
00:02:59,08 --> 00:03:02,04
so here for a sided main,

60
00:03:02,04 --> 00:03:05,06
we have a bottom margin of three rem down here on the aside.

61
00:03:05,06 --> 00:03:07,04
We have a margin bottom of zero.

62
00:03:07,04 --> 00:03:10,09
So once again, we could go ahead and set up

63
00:03:10,09 --> 00:03:18,06
our variable here of bottom-margin of three rem,

64
00:03:18,06 --> 00:03:25,02
and then here instead of just saying it is three rem,

65
00:03:25,02 --> 00:03:31,09
we'll go ahead and declare it with a variable.

66
00:03:31,09 --> 00:03:33,08
Just like that.

67
00:03:33,08 --> 00:03:37,00
And then here on the aside,

68
00:03:37,00 --> 00:03:41,01
we're going to change the value here of bottom-margin.

69
00:03:41,01 --> 00:03:44,01
So remember this is just on the aside.

70
00:03:44,01 --> 00:03:46,09
It's not on aside and Main

71
00:03:46,09 --> 00:03:49,08
so while I could just simply redefine

72
00:03:49,08 --> 00:03:53,07
my variable value here in root, that would then apply

73
00:03:53,07 --> 00:03:56,03
to both aside and Main.

74
00:03:56,03 --> 00:03:58,04
What if I want it to just apply to aside?

75
00:03:58,04 --> 00:04:00,00
Well, there's nothing that says

76
00:04:00,00 --> 00:04:02,00
I have to put it here in root.

77
00:04:02,00 --> 00:04:05,01
I could choose just to redefine that value right here

78
00:04:05,01 --> 00:04:10,06
on the aside so I could just say instead bottom-margin

79
00:04:10,06 --> 00:04:12,05
is going to be zero here.

80
00:04:12,05 --> 00:04:15,03
So we just redefined that value here for the purpose

81
00:04:15,03 --> 00:04:17,03
of aside, not for main

82
00:04:17,03 --> 00:04:19,01
and we still get the benefit

83
00:04:19,01 --> 00:04:23,06
of just redefining these values.

84
00:04:23,06 --> 00:04:25,06
So that's pretty much the big changes

85
00:04:25,06 --> 00:04:30,01
that we're making between the mobile layout

86
00:04:30,01 --> 00:04:32,09
and our tablet layout believe it or not.

87
00:04:32,09 --> 00:04:35,03
If you go through all of the code that's up here

88
00:04:35,03 --> 00:04:37,02
on the top all of this margin

89
00:04:37,02 --> 00:04:38,08
and background color and border

90
00:04:38,08 --> 00:04:40,03
and all the rest of it,

91
00:04:40,03 --> 00:04:44,00
there's definitely stuff that repeats itself in here

92
00:04:44,00 --> 00:04:46,00
and you should take a look at this

93
00:04:46,00 --> 00:04:47,07
and think about how you could leverage something

94
00:04:47,07 --> 00:04:51,09
like SaaS to prevent so much repetition in the code here

95
00:04:51,09 --> 00:04:53,07
that makes absolute sense.

96
00:04:53,07 --> 00:04:57,01
You can think about all of the SaaS structures applied

97
00:04:57,01 --> 00:05:00,00
to that code in order to make it dryer.

98
00:05:00,00 --> 00:05:02,04
So you could look at variables.

99
00:05:02,04 --> 00:05:05,00
You could look at mix-ins you could look at it extends

100
00:05:05,00 --> 00:05:07,08
and that would clean up this code here

101
00:05:07,08 --> 00:05:10,02
but you don't necessarily need to spend time working

102
00:05:10,02 --> 00:05:11,03
on custom properties

103
00:05:11,03 --> 00:05:17,05
because we're not changing any of this layout look here

104
00:05:17,05 --> 00:05:19,00
into the media queries.

105
00:05:19,00 --> 00:05:24,02
So we're just focusing on that for these custom properties.

106
00:05:24,02 --> 00:05:26,05
All right, so then the next thing we want to look at

107
00:05:26,05 --> 00:05:29,00
is what changes as we go from tablet

108
00:05:29,00 --> 00:05:33,00
in the desktop now that we have our mobile stuff

109
00:05:33,00 --> 00:05:34,08
that's redefined here.

110
00:05:34,08 --> 00:05:37,09
Let's go ahead and take a look at what's going on

111
00:05:37,09 --> 00:05:41,05
for the tablet into our desktop.

112
00:05:41,05 --> 00:05:43,07
So one of the things that changes here

113
00:05:43,07 --> 00:05:46,05
is that the display grid happens

114
00:05:46,05 --> 00:05:49,00
and we're going to change our number

115
00:05:49,00 --> 00:05:51,05
of grid template columns, see how that changes.

116
00:05:51,05 --> 00:05:53,02
We're going to change our max-width.

117
00:05:53,02 --> 00:05:55,01
That's also going to change.

118
00:05:55,01 --> 00:05:58,01
So let's go ahead and set that up.

119
00:05:58,01 --> 00:06:01,01
So here in root again, we'll just continue

120
00:06:01,01 --> 00:06:02,07
to add onto this.

121
00:06:02,07 --> 00:06:05,00
We'll have calls.

122
00:06:05,00 --> 00:06:11,02
And that would be repeat two comma one fr.

123
00:06:11,02 --> 00:06:20,00
So now I could just make that change here var.

124
00:06:20,00 --> 00:06:23,05
Calls, here we go

125
00:06:23,05 --> 00:06:26,04
and then we also have a max

126
00:06:26,04 --> 00:06:30,04
as a maximum width value.

127
00:06:30,04 --> 00:06:35,07
550 pixels and then here for max-width,

128
00:06:35,07 --> 00:06:44,07
we'll go ahead and call that.

129
00:06:44,07 --> 00:06:47,07
So if we look at this content display here then,

130
00:06:47,07 --> 00:06:49,08
we have a I'm just going

131
00:06:49,08 --> 00:06:52,09
to put in a comment so you can understand what's going on.

132
00:06:52,09 --> 00:06:57,03
This will carry into desktop.

133
00:06:57,03 --> 00:07:00,08
So here for carry into desktop our grid

134
00:07:00,08 --> 00:07:03,04
and our grid-gap are both going to carry

135
00:07:03,04 --> 00:07:05,04
into the desktop display.

136
00:07:05,04 --> 00:07:07,06
And then these two values here,

137
00:07:07,06 --> 00:07:08,09
they are a little bit different.

138
00:07:08,09 --> 00:07:16,05
These values change on desktop.

139
00:07:16,05 --> 00:07:21,03
So these will be a little bit different.

140
00:07:21,03 --> 00:07:24,05
What else is changing as we go from our tablet

141
00:07:24,05 --> 00:07:28,05
into our desktop, our value of grid-column is changing.

142
00:07:28,05 --> 00:07:33,02
So we've got we're going from one, three to two four.

143
00:07:33,02 --> 00:07:35,02
So that's something that we can put up here

144
00:07:35,02 --> 00:07:37,07
as a custom property.

145
00:07:37,07 --> 00:07:41,05
So I'm going to call that main-call

146
00:07:41,05 --> 00:07:51,05
and the initial value will be one three.

147
00:07:51,05 --> 00:07:55,06
And then down here again for main we can get rid

148
00:07:55,06 --> 00:07:58,04
of that declaration all together.

149
00:07:58,04 --> 00:08:02,04
We can get rid of this declaration all together.

150
00:08:02,04 --> 00:08:04,08
We can get rid of this declaration all together.

151
00:08:04,08 --> 00:08:09,01
Right, all these things have been defined for us already

152
00:08:09,01 --> 00:08:10,04
in the tablet.

153
00:08:10,04 --> 00:08:14,03
So now all we have to do is come up with our desktop values

154
00:08:14,03 --> 00:08:18,09
and so here,

155
00:08:18,09 --> 00:08:20,02
we'll go ahead and put those in.

156
00:08:20,02 --> 00:08:24,09
Here the calls will be repeat.

157
00:08:24,09 --> 00:08:32,09
Three comma one fr

158
00:08:32,09 --> 00:08:38,03
and our max here will be 1,000 pixels.

159
00:08:38,03 --> 00:08:44,05
And our main columns will be 2/4.

160
00:08:44,05 --> 00:08:47,04
All right, so sweet, we've really refactored this layout

161
00:08:47,04 --> 00:08:50,05
very, very nicely so we don't have to repeat ourselves

162
00:08:50,05 --> 00:08:53,03
over and over again in those media queries.

163
00:08:53,03 --> 00:08:57,03
The media queries are now displaying their logic

164
00:08:57,03 --> 00:08:59,01
at least just one time.

165
00:08:59,01 --> 00:09:02,07
So even though we're not really using any grid layout here

166
00:09:02,07 --> 00:09:03,09
outside the media query,

167
00:09:03,09 --> 00:09:07,01
we're just assuming that the default of display block

168
00:09:07,01 --> 00:09:08,01
is going to happen.

169
00:09:08,01 --> 00:09:11,02
These boxes will display on top of each other.

170
00:09:11,02 --> 00:09:14,02
You could if you wanted to display grid outside

171
00:09:14,02 --> 00:09:17,01
the media query, you could set all of that up if you wanted.

172
00:09:17,01 --> 00:09:18,03
I didn't do it that way.

173
00:09:18,03 --> 00:09:21,06
I declared the grid inside the media query here.

174
00:09:21,06 --> 00:09:25,04
So once again, we've declared our logic one time

175
00:09:25,04 --> 00:09:28,03
and then we're changing values

176
00:09:28,03 --> 00:09:31,06
of our variables every time after that.

177
00:09:31,06 --> 00:09:34,09
So by the time you get down here to our desktop,

178
00:09:34,09 --> 00:09:37,01
things are very different.

179
00:09:37,01 --> 00:09:41,01
So the only things that are added here so like news blogs

180
00:09:41,01 --> 00:09:44,03
and this grid row under main, this is logic

181
00:09:44,03 --> 00:09:47,08
that didn't exist anywhere else in previous media queries

182
00:09:47,08 --> 00:09:49,04
or in the mobile layout

183
00:09:49,04 --> 00:09:51,03
which is why they're declared here,

184
00:09:51,03 --> 00:09:53,04
but everything that had been declared before,

185
00:09:53,04 --> 00:09:55,05
we've now refactored in some way

186
00:09:55,05 --> 00:09:58,00
to include a custom property.

