1
00:00:00,05 --> 00:00:02,00
- [Narrator] In this next project,

2
00:00:02,00 --> 00:00:03,08
over this next chapter,

3
00:00:03,08 --> 00:00:07,05
we're going to apply these calc and custom property skills

4
00:00:07,05 --> 00:00:09,07
we've been developing this whole course long,

5
00:00:09,07 --> 00:00:11,08
and we're going to apply this to a webpage

6
00:00:11,08 --> 00:00:15,02
that's been laid out using CSS grid.

7
00:00:15,02 --> 00:00:18,05
I am showing you here three screenshots.

8
00:00:18,05 --> 00:00:20,09
These are from your exercise files,

9
00:00:20,09 --> 00:00:24,04
so make sure you've downloaded your exercise files.

10
00:00:24,04 --> 00:00:27,06
We take a look in your exercise files folder

11
00:00:27,06 --> 00:00:32,02
in the folder numbered zero four dash zero one.

12
00:00:32,02 --> 00:00:34,02
You will find these three screenshots

13
00:00:34,02 --> 00:00:36,07
that are called desktop, tablet, and mobile,

14
00:00:36,07 --> 00:00:39,00
and just go on ahead and open those up

15
00:00:39,00 --> 00:00:41,05
so you can follow along.

16
00:00:41,05 --> 00:00:43,09
So this was the specification given to us

17
00:00:43,09 --> 00:00:45,06
by the graphic designer,

18
00:00:45,06 --> 00:00:47,01
and as you can see here,

19
00:00:47,01 --> 00:00:49,01
all the way over on the far right side,

20
00:00:49,01 --> 00:00:50,04
this is mobile.

21
00:00:50,04 --> 00:00:52,02
We've got a series of boxes that are stacked

22
00:00:52,02 --> 00:00:54,02
on top of each other.

23
00:00:54,02 --> 00:00:58,06
So we have this leading, artfully arranged thing on the top

24
00:00:58,06 --> 00:01:01,04
with the boxes for the newsletter and the recent blogs,

25
00:01:01,04 --> 00:01:04,04
and then some articles under all of that,

26
00:01:04,04 --> 00:01:06,00
nice footer.

27
00:01:06,00 --> 00:01:07,04
On the tablet display,

28
00:01:07,04 --> 00:01:10,08
we're going to make that big, beautiful crocus picture,

29
00:01:10,08 --> 00:01:12,07
those are purple crocuses,

30
00:01:12,07 --> 00:01:14,03
we're going to make that a little bit bigger.

31
00:01:14,03 --> 00:01:16,09
We'll have to put the newsletter and the blogs underneath,

32
00:01:16,09 --> 00:01:19,02
and then more articles under that,

33
00:01:19,02 --> 00:01:20,07
and then finally on the desktop,

34
00:01:20,07 --> 00:01:24,04
we're going to arrange it a little bit differently again.

35
00:01:24,04 --> 00:01:27,08
So that is the pictures that you should have in your head

36
00:01:27,08 --> 00:01:31,04
as we're working through this material,

37
00:01:31,04 --> 00:01:34,01
and so the other thing to go through at this point in time

38
00:01:34,01 --> 00:01:38,07
is just to show you what our starting files look like.

39
00:01:38,07 --> 00:01:42,08
So I am going to be working with Visual Studio Code,

40
00:01:42,08 --> 00:01:46,00
and if you have not worked with Visual Studio Code before

41
00:01:46,00 --> 00:01:48,07
this is just yet another editor

42
00:01:48,07 --> 00:01:52,05
that does HTML, CSS, JavaScript, a bunch of stuff,

43
00:01:52,05 --> 00:01:54,06
and this is what it would look like

44
00:01:54,06 --> 00:01:58,01
if you go and download it and install it,

45
00:01:58,01 --> 00:02:01,00
and the way we get this configured

46
00:02:01,00 --> 00:02:02,09
to be really, really simple.

47
00:02:02,09 --> 00:02:05,09
I'm just going to grab my exercise files folder

48
00:02:05,09 --> 00:02:09,00
and then all I need to do is just drag and drop it here

49
00:02:09,00 --> 00:02:11,09
on VS Code and this will give me

50
00:02:11,09 --> 00:02:15,09
a list of my files and folders over here on the side.

51
00:02:15,09 --> 00:02:19,06
Then go ahead and close the welcome window

52
00:02:19,06 --> 00:02:21,08
and then if I go on ahead and open this up,

53
00:02:21,08 --> 00:02:27,01
you'll see that we have the HTML file, that's here,

54
00:02:27,01 --> 00:02:29,01
and then I'm going to make a couple of changes

55
00:02:29,01 --> 00:02:30,01
to the way this looks.

56
00:02:30,01 --> 00:02:32,07
It's got this little mini map over here in the corner.

57
00:02:32,07 --> 00:02:35,02
I am not a fan of that so I'm going to turn that off

58
00:02:35,02 --> 00:02:36,08
under view, show mini map.

59
00:02:36,08 --> 00:02:39,02
I'm going to turn that off,

60
00:02:39,02 --> 00:02:40,09
and the other thing I'd like to see

61
00:02:40,09 --> 00:02:42,07
is my CSS at the same time.

62
00:02:42,07 --> 00:02:44,07
So in my CSS folder,

63
00:02:44,07 --> 00:02:49,05
if you right click on style and say "open to the side",

64
00:02:49,05 --> 00:02:52,09
this will open the HTML and CSS side by side,

65
00:02:52,09 --> 00:02:55,04
next to each other, which is really great,

66
00:02:55,04 --> 00:02:58,02
and if you want your file structure to go away,

67
00:02:58,02 --> 00:03:00,07
just click this top button up here on the top.

68
00:03:00,07 --> 00:03:03,07
That'll give you a little bit more room to work with.

69
00:03:03,07 --> 00:03:05,06
You can select either side of these

70
00:03:05,06 --> 00:03:09,03
and go to view, toggle word wrap.

71
00:03:09,03 --> 00:03:13,04
That'll wrap your HTML and your CSS,

72
00:03:13,04 --> 00:03:14,06
there we go,

73
00:03:14,06 --> 00:03:16,06
so that you can see what's going on here,

74
00:03:16,06 --> 00:03:18,05
and then the last thing is,

75
00:03:18,05 --> 00:03:21,00
one of the things I really loved about Sublime Text

76
00:03:21,00 --> 00:03:23,06
was that if I right click somewhere in here,

77
00:03:23,06 --> 00:03:26,08
there was an option on this menu to open my webpage

78
00:03:26,08 --> 00:03:29,06
in a browser and that's missing here in VS Code,

79
00:03:29,06 --> 00:03:30,09
but we can add it.

80
00:03:30,09 --> 00:03:33,06
If you look at this icon down here at the very bottom,

81
00:03:33,06 --> 00:03:34,08
this is the extensions,

82
00:03:34,08 --> 00:03:39,01
go ahead and click that and then inside of this box

83
00:03:39,01 --> 00:03:44,06
if you type in "open in browser",

84
00:03:44,06 --> 00:03:46,05
the very first search result here

85
00:03:46,05 --> 00:03:50,06
is called open in browser 2.0.0 from TechER.

86
00:03:50,06 --> 00:03:54,02
If you just go on ahead and hit the install button,

87
00:03:54,02 --> 00:03:59,09
it will go ahead and install this extension for you,

88
00:03:59,09 --> 00:04:03,03
and once that is installed,

89
00:04:03,03 --> 00:04:08,00
when we go back to our files and go back to our HTML.

90
00:04:08,00 --> 00:04:10,08
So if you just right click somewhere over here,

91
00:04:10,08 --> 00:04:12,05
a little menu is going to come up,

92
00:04:12,05 --> 00:04:15,02
we'll say "open in default browser."

93
00:04:15,02 --> 00:04:17,06
The default browser on this computer is Chrome

94
00:04:17,06 --> 00:04:20,05
so it's going to go ahead and open this up in Chrome,

95
00:04:20,05 --> 00:04:23,09
and you can take a look at the way this looks now.

96
00:04:23,09 --> 00:04:25,00
You see we have a header,

97
00:04:25,00 --> 00:04:26,04
we have a nav bar,

98
00:04:26,04 --> 00:04:28,09
we have pictures,

99
00:04:28,09 --> 00:04:30,04
and so forth.

100
00:04:30,04 --> 00:04:34,03
So a lot of the styling is already done for you,

101
00:04:34,03 --> 00:04:38,09
and if you revisit that styling and so forth here

102
00:04:38,09 --> 00:04:41,01
in your HTML and your CSS,

103
00:04:41,01 --> 00:04:43,06
I would encourage you to take a look at all of that

104
00:04:43,06 --> 00:04:45,06
before you move on to the next video,

105
00:04:45,06 --> 00:04:49,00
just so you can familiarize yourself with what's going on.

106
00:04:49,00 --> 00:04:52,02
In short, we have a wrapper,

107
00:04:52,02 --> 00:04:54,02
we've got headers, we've got nav bars,

108
00:04:54,02 --> 00:04:56,06
we've got some content with some asides in it.

109
00:04:56,06 --> 00:05:00,02
Those are the various little lists of blog posts,

110
00:05:00,02 --> 00:05:02,03
so the newsletter and so forth,

111
00:05:02,03 --> 00:05:03,02
whole bunch of asides.

112
00:05:03,02 --> 00:05:05,01
We have a footer down here on the bottom,

113
00:05:05,01 --> 00:05:07,06
and then in our CSS styles,

114
00:05:07,06 --> 00:05:09,09
you'll see once again we've got our little preamble

115
00:05:09,09 --> 00:05:12,09
here about working in the border box model,

116
00:05:12,09 --> 00:05:15,01
and as we scroll down through the page here you'll see

117
00:05:15,01 --> 00:05:17,08
we've got various kinds of styling applied here

118
00:05:17,08 --> 00:05:20,02
for images, the header, the navigation,

119
00:05:20,02 --> 00:05:21,08
and the way that's going to work.

120
00:05:21,08 --> 00:05:24,02
Buttons and the button styling,

121
00:05:24,02 --> 00:05:25,07
and so forth.

122
00:05:25,07 --> 00:05:27,01
When we get down to layout,

123
00:05:27,01 --> 00:05:28,07
it's down here at the bottom,

124
00:05:28,07 --> 00:05:30,08
and there's not a whole lot that's here.

125
00:05:30,08 --> 00:05:34,02
We just have a little bit of information about our margins

126
00:05:34,02 --> 00:05:36,01
and we have some empty media queries.

127
00:05:36,01 --> 00:05:38,06
Obviously this where we're going to be doing all of our work.

128
00:05:38,06 --> 00:05:40,00
So in the next video,

129
00:05:40,00 --> 00:05:42,00
we'll go ahead and get started.

