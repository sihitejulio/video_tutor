1
00:00:00,05 --> 00:00:03,04
- [Instructor] Our website is looking absolutely fabulous!

2
00:00:03,04 --> 00:00:04,09
But at this point,

3
00:00:04,09 --> 00:00:07,03
if ya take a look at the style sheet in detail,

4
00:00:07,03 --> 00:00:08,05
one of the things you'll notice

5
00:00:08,05 --> 00:00:10,02
that I did not address at all

6
00:00:10,02 --> 00:00:12,00
are font sizes.

7
00:00:12,00 --> 00:00:14,03
And that was actually by design.

8
00:00:14,03 --> 00:00:17,06
I haven't addressed font size anywhere in this document.

9
00:00:17,06 --> 00:00:19,09
Basically so far we're working

10
00:00:19,09 --> 00:00:22,03
with browser default font sizes

11
00:00:22,03 --> 00:00:25,01
for our headings and our paragraphs and so forth.

12
00:00:25,01 --> 00:00:27,00
But we've written this great code

13
00:00:27,00 --> 00:00:29,06
over the course of several challenges.

14
00:00:29,06 --> 00:00:34,04
And this is using custom properties to change the size

15
00:00:34,04 --> 00:00:35,04
of our type,

16
00:00:35,04 --> 00:00:40,09
as we are going through various media queries and so forth.

17
00:00:40,09 --> 00:00:42,05
What I'd like to do is,

18
00:00:42,05 --> 00:00:44,04
I'd like to grab that old code

19
00:00:44,04 --> 00:00:47,04
and integrate it into the page that we're working on.

20
00:00:47,04 --> 00:00:49,04
We can tweak it a little bit.

21
00:00:49,04 --> 00:00:53,05
And make it look great for this particular web design.

22
00:00:53,05 --> 00:00:55,00
What I'd like for you to do,

23
00:00:55,00 --> 00:00:57,08
is look in your Exercise Files folder.

24
00:00:57,08 --> 00:01:01,05
This is 04_04 in the Begin folder.

25
00:01:01,05 --> 00:01:05,06
You'll find a file called starting css.txt.

26
00:01:05,06 --> 00:01:06,07
And if you just go on ahead

27
00:01:06,07 --> 00:01:10,08
and just copy all of that code that's there.

28
00:01:10,08 --> 00:01:13,08
I'll just go on ahead and copy it.

29
00:01:13,08 --> 00:01:17,03
And then over in our CSS,

30
00:01:17,03 --> 00:01:19,04
we can just go on ahead and add that.

31
00:01:19,04 --> 00:01:21,09
And the place I'm going to add this,

32
00:01:21,09 --> 00:01:26,03
is actually outside of all of our media queries.

33
00:01:26,03 --> 00:01:28,08
And I'm just gon' stick it here before layout,

34
00:01:28,08 --> 00:01:32,04
that's not really important where it exactly it goes.

35
00:01:32,04 --> 00:01:36,00
Let's just drop it here.

36
00:01:36,00 --> 00:01:38,07
I'm puttin' in a comment that this is font sizes.

37
00:01:38,07 --> 00:01:40,08
And then go on ahead and paste it in

38
00:01:40,08 --> 00:01:43,07
what I have from here before.

39
00:01:43,07 --> 00:01:45,09
You can certainly make some modifications

40
00:01:45,09 --> 00:01:48,08
of this at this point because for example,

41
00:01:48,08 --> 00:01:52,03
my design doesn't incorporate h4 and h5.

42
00:01:52,03 --> 00:01:56,08
And yeah it's possible that I will never use h4 and h5.

43
00:01:56,08 --> 00:01:58,01
Just to sort of demonstrate

44
00:01:58,01 --> 00:02:00,03
that we can take things out of this.

45
00:02:00,03 --> 00:02:04,07
Absolutely we can make modifications here.

46
00:02:04,07 --> 00:02:08,02
Let's just say we don't have h4 and h5.

47
00:02:08,02 --> 00:02:10,01
We'll get rid of those.

48
00:02:10,01 --> 00:02:12,08
And then we're going to have to somehow

49
00:02:12,08 --> 00:02:16,01
modify our h4 and our h5 here.

50
00:02:16,01 --> 00:02:18,03
Our h3 now is going to become our

51
00:02:18,03 --> 00:02:25,04
base-size times our type-scale.

52
00:02:25,04 --> 00:02:27,06
That's what our h3 will be doing.

53
00:02:27,06 --> 00:02:30,03
We'll leave on our h2 and our h1.

54
00:02:30,03 --> 00:02:32,04
Notice that I've just knocked down the size

55
00:02:32,04 --> 00:02:35,00
of my h1 and h2 a little bit here,

56
00:02:35,00 --> 00:02:37,03
by just making that simple change here

57
00:02:37,03 --> 00:02:39,06
to this particular code.

58
00:02:39,06 --> 00:02:40,07
The other thing I'm going to do is,

59
00:02:40,07 --> 00:02:47,05
I'm going to change my type-scale from 1.125 to a 1.2.

60
00:02:47,05 --> 00:02:48,07
I just thought that that looked better

61
00:02:48,07 --> 00:02:51,04
for this particular design.

62
00:02:51,04 --> 00:02:52,06
And then down here of course,

63
00:02:52,06 --> 00:02:56,00
we want to get rid of the h4 and h5 declarations.

64
00:02:56,00 --> 00:02:58,04
Those no longer have variables that go with them.

65
00:02:58,04 --> 00:02:59,05
There's our h1,

66
00:02:59,05 --> 00:03:00,07
h2 and h3,

67
00:03:00,07 --> 00:03:03,06
and our paragraph.

68
00:03:03,06 --> 00:03:08,00
And then as I get to my larger sizes

69
00:03:08,00 --> 00:03:10,01
here in my media queries,

70
00:03:10,01 --> 00:03:11,02
for larger screens,

71
00:03:11,02 --> 00:03:14,06
I'm actually going to bump up my font size a little bit.

72
00:03:14,06 --> 00:03:17,02
Here inside of my root query,

73
00:03:17,02 --> 00:03:25,02
I'll go ahead and add a type-scale of 1.33.

74
00:03:25,02 --> 00:03:26,05
There we go.

75
00:03:26,05 --> 00:03:28,03
Now this will have larger text

76
00:03:28,03 --> 00:03:31,03
when I go to larger screen sizes.

77
00:03:31,03 --> 00:03:33,03
I thought this worked just fine just for the tablet.

78
00:03:33,03 --> 00:03:34,01
And the desktop,

79
00:03:34,01 --> 00:03:35,06
you could do a different one for desktop

80
00:03:35,06 --> 00:03:37,08
if you wanted to go through that.

81
00:03:37,08 --> 00:03:40,05
The other question that some of you might have is,

82
00:03:40,05 --> 00:03:44,07
I have a root declaration here around line 110

83
00:03:44,07 --> 00:03:48,06
and I have a root declaration down here around line 130.

84
00:03:48,06 --> 00:03:51,00
And I have one part that's here for font sizes,

85
00:03:51,00 --> 00:03:52,06
one part that's here for layout.

86
00:03:52,06 --> 00:03:54,04
Should you combine those?

87
00:03:54,04 --> 00:03:56,06
Or can you combine those?

88
00:03:56,06 --> 00:03:57,09
Absolutely you can go ahead

89
00:03:57,09 --> 00:03:59,05
and have a single root declaration.

90
00:03:59,05 --> 00:04:01,09
You can put all your variables inside of it.

91
00:04:01,09 --> 00:04:03,08
I would recommend if you go that route

92
00:04:03,08 --> 00:04:05,02
that you definitely comment,

93
00:04:05,02 --> 00:04:07,03
so you know what's font sizes,

94
00:04:07,03 --> 00:04:08,04
what's layout,

95
00:04:08,04 --> 00:04:10,09
what's various other things you might be changing

96
00:04:10,09 --> 00:04:11,09
along the way.

97
00:04:11,09 --> 00:04:14,05
Just so you can find things easily.

98
00:04:14,05 --> 00:04:17,07
It's totally legal to leave these separate also.

99
00:04:17,07 --> 00:04:19,02
The way I have it here,

100
00:04:19,02 --> 00:04:21,00
CSS doesn't particularly care.

101
00:04:21,00 --> 00:04:21,08
It'll just go,

102
00:04:21,08 --> 00:04:22,07
"Oh more root."

103
00:04:22,07 --> 00:04:24,05
And it'll just continue to add things on

104
00:04:24,05 --> 00:04:27,09
just as it would with any other kind of CSS.

105
00:04:27,09 --> 00:04:29,01
Let's go ahead and save that.

106
00:04:29,01 --> 00:04:31,01
We'll go ahead and take a look at the result here

107
00:04:31,01 --> 00:04:32,07
in our browser.

108
00:04:32,07 --> 00:04:34,09
And here in the browser,

109
00:04:34,09 --> 00:04:36,07
you'll see we have

110
00:04:36,07 --> 00:04:40,01
some lovely fonts here at desktop dimensions.

111
00:04:40,01 --> 00:04:41,05
And because I've marked this up

112
00:04:41,05 --> 00:04:43,07
using just plain ole headings,

113
00:04:43,07 --> 00:04:45,02
all those plain ole headings

114
00:04:45,02 --> 00:04:47,08
just immediately take effect here,

115
00:04:47,08 --> 00:04:49,06
with the styling that I've added.

116
00:04:49,06 --> 00:04:50,05
If for some reason,

117
00:04:50,05 --> 00:04:53,02
you happen to use classes on your headings or whatever,

118
00:04:53,02 --> 00:04:56,00
you can certainly modify the code that we copied in,

119
00:04:56,00 --> 00:04:58,07
to leverage those classes instead

120
00:04:58,07 --> 00:05:01,07
of leveraging HTML elements.

121
00:05:01,07 --> 00:05:04,08
And as we narrow down this page here,

122
00:05:04,08 --> 00:05:06,06
you'll see our font size stayed the same

123
00:05:06,06 --> 00:05:09,01
between our desktop and our tablet.

124
00:05:09,01 --> 00:05:11,01
That was exactly what we coded.

125
00:05:11,01 --> 00:05:13,08
And then when we get down to mobile dimensions,

126
00:05:13,08 --> 00:05:19,03
we are definitely going to a smaller sized font there.

127
00:05:19,03 --> 00:05:20,01
So there we go!

128
00:05:20,01 --> 00:05:22,03
That was just so easy to go ahead

129
00:05:22,03 --> 00:05:24,08
and add a type-scale to our design.

130
00:05:24,08 --> 00:05:26,06
It'll be great to manage

131
00:05:26,06 --> 00:05:28,04
and easy to make changes

132
00:05:28,04 --> 00:05:32,00
as people decide they want to tweak the typography.

