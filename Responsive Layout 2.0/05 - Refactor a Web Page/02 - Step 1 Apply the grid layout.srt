1
00:00:00,05 --> 00:00:02,07
- [Instructor] Let's first, to get started here

2
00:00:02,07 --> 00:00:04,03
on this webpage layout,

3
00:00:04,03 --> 00:00:09,01
we are going to apply CSS Grid to the page

4
00:00:09,01 --> 00:00:11,04
as it is without adding calc

5
00:00:11,04 --> 00:00:13,00
or custom properties just yet.

6
00:00:13,00 --> 00:00:17,00
Let's just first get the layout working with CSS Grid.

7
00:00:17,00 --> 00:00:20,02
So our first step is to get it working in a mobile layout

8
00:00:20,02 --> 00:00:22,00
and fortunately, that's complete.

9
00:00:22,00 --> 00:00:23,09
So if you take a look at this,

10
00:00:23,09 --> 00:00:26,02
this is the webpage in a mobile layout.

11
00:00:26,02 --> 00:00:28,05
Everything's stacked on top of each other,

12
00:00:28,05 --> 00:00:30,01
so we're done with that.

13
00:00:30,01 --> 00:00:32,06
But of course, nothing else is written yet,

14
00:00:32,06 --> 00:00:35,01
so as I expand my webpage,

15
00:00:35,01 --> 00:00:36,09
it continues to stay in mobile layout

16
00:00:36,09 --> 00:00:39,01
and starts to look kind of silly.

17
00:00:39,01 --> 00:00:41,06
So it is time to go on ahead

18
00:00:41,06 --> 00:00:45,02
and start working on the tablet layout.

19
00:00:45,02 --> 00:00:48,09
Let's go on ahead and think about that.

20
00:00:48,09 --> 00:00:51,02
Remember that my tablet displays

21
00:00:51,02 --> 00:00:53,07
are going to cascade to the desktop,

22
00:00:53,07 --> 00:00:56,00
so let's go on ahead and start with that

23
00:00:56,00 --> 00:00:58,05
and if we just take a quick peek again

24
00:00:58,05 --> 00:01:02,00
at the tablet display to remind us what this looks like,

25
00:01:02,00 --> 00:01:04,03
we're going to have one big box on the top

26
00:01:04,03 --> 00:01:06,04
and these two underneath.

27
00:01:06,04 --> 00:01:08,02
Remember that eventually, these two are going

28
00:01:08,02 --> 00:01:10,00
to stack over here on the side

29
00:01:10,00 --> 00:01:12,02
next to the purple flowers

30
00:01:12,02 --> 00:01:13,09
and then we've got these other three

31
00:01:13,09 --> 00:01:16,08
that are here underneath.

32
00:01:16,08 --> 00:01:20,02
So what we're going to think about is a three-column layout.

33
00:01:20,02 --> 00:01:22,06
A three-column layout is going to work great here

34
00:01:22,06 --> 00:01:25,02
on the desktop, there's our three columns.

35
00:01:25,02 --> 00:01:26,07
Here's a single column.

36
00:01:26,07 --> 00:01:29,01
We have two here for the flowers,

37
00:01:29,01 --> 00:01:32,08
and then when we go to our tablet layout,

38
00:01:32,08 --> 00:01:36,03
we're going to reduce this two a two-column layout

39
00:01:36,03 --> 00:01:39,02
where we have a single cell spanning two columns

40
00:01:39,02 --> 00:01:42,00
and then everything else sort of folding in underneath.

41
00:01:42,00 --> 00:01:45,01
So that's sort of the strategy of what it is

42
00:01:45,01 --> 00:01:47,07
that we're doing here.

43
00:01:47,07 --> 00:01:51,00
Let's just go ahead and start by coding this, then,

44
00:01:51,00 --> 00:01:52,09
with that in mind

45
00:01:52,09 --> 00:01:55,09
and then we'll just want to connect where our code is

46
00:01:55,09 --> 00:01:58,04
in our HTML so that we have the right handles

47
00:01:58,04 --> 00:02:00,06
in order to write the CSS.

48
00:02:00,06 --> 00:02:04,05
And so if we take a quick peek at our HTML here,

49
00:02:04,05 --> 00:02:06,08
scroll on down here line 24,

50
00:02:06,08 --> 00:02:09,07
we have this div with a class of content

51
00:02:09,07 --> 00:02:13,06
and so content is going to wind up being our grid container.

52
00:02:13,06 --> 00:02:15,00
It's the parent.

53
00:02:15,00 --> 00:02:18,03
And if you take a look at what's inside of that,

54
00:02:18,03 --> 00:02:22,05
we simply have all of these, a main,

55
00:02:22,05 --> 00:02:24,02
and then all of these asides.

56
00:02:24,02 --> 00:02:27,05
These are the immediate children of that container

57
00:02:27,05 --> 00:02:29,00
with the class of content.

58
00:02:29,00 --> 00:02:33,04
So the main and the asides would be the grid items

59
00:02:33,04 --> 00:02:35,03
and the div of the class of content is going

60
00:02:35,03 --> 00:02:38,03
to be the grid container or the parent.

61
00:02:38,03 --> 00:02:41,04
So each row is going to be two columns wide

62
00:02:41,04 --> 00:02:42,08
except for the very first one,

63
00:02:42,08 --> 00:02:46,00
so this layout will wind up being really easy to write.

64
00:02:46,00 --> 00:02:48,01
We'll just go on ahead and scroll on down here

65
00:02:48,01 --> 00:02:49,04
to our media queries.

66
00:02:49,04 --> 00:02:52,07
We're going to start with our 600 pixel media query

67
00:02:52,07 --> 00:02:55,02
and so what we're going to do is the following.

68
00:02:55,02 --> 00:02:59,05
We're going to start with our class of content

69
00:02:59,05 --> 00:03:04,09
and shockingly, we'll want to display this as grid.

70
00:03:04,09 --> 00:03:07,03
Exactly as you would expect,

71
00:03:07,03 --> 00:03:09,00
and then at that point,

72
00:03:09,00 --> 00:03:12,01
we can think about how exactly we want to run this layout.

73
00:03:12,01 --> 00:03:15,04
Chances are, it's something like we want to do our grid

74
00:03:15,04 --> 00:03:19,05
hyphen template hyphen columns,

75
00:03:19,05 --> 00:03:22,09
and in this case, we're going to just say repeat.

76
00:03:22,09 --> 00:03:26,07
We want two columns and each will be one fraction large.

77
00:03:26,07 --> 00:03:32,02
Remember that F R syntax is unique to the grid system

78
00:03:32,02 --> 00:03:34,07
and it just simply means take the page,

79
00:03:34,07 --> 00:03:37,06
in this case, take the page and divide it into two parts.

80
00:03:37,06 --> 00:03:39,08
Each part is one fraction large,

81
00:03:39,08 --> 00:03:41,08
so the browser just does that math for you.

82
00:03:41,08 --> 00:03:43,09
It's a beautiful thing.

83
00:03:43,09 --> 00:03:46,00
Then I'm going to want some space in between those,

84
00:03:46,00 --> 00:03:49,05
so let's put in a grid hyphen gap.

85
00:03:49,05 --> 00:03:51,09
And I'm going to make that three rem

86
00:03:51,09 --> 00:03:55,01
because I want it kind of a sizable gap in between those.

87
00:03:55,01 --> 00:03:57,05
We might also think about a max width.

88
00:03:57,05 --> 00:03:59,03
One of the things that I have going on here,

89
00:03:59,03 --> 00:04:01,04
if you take a look at my code very carefully

90
00:04:01,04 --> 00:04:03,07
and those designs,

91
00:04:03,07 --> 00:04:07,00
my header and my nav bar go the full width

92
00:04:07,00 --> 00:04:08,06
of the browser window,

93
00:04:08,06 --> 00:04:12,05
but my content is restricted in its width

94
00:04:12,05 --> 00:04:14,01
and that's by design.

95
00:04:14,01 --> 00:04:16,01
You don't want your content stretching all the way

96
00:04:16,01 --> 00:04:16,09
across the page.

97
00:04:16,09 --> 00:04:19,05
The lines get so long, it's difficult to read,

98
00:04:19,05 --> 00:04:21,03
but by using this technique

99
00:04:21,03 --> 00:04:25,06
with a maximum width here, of let's say, 550 pixels,

100
00:04:25,06 --> 00:04:28,02
what'll happen is that this content will stay nice

101
00:04:28,02 --> 00:04:30,00
and narrow, easy to read,

102
00:04:30,00 --> 00:04:33,04
and then the header and the nav bar and the footer

103
00:04:33,04 --> 00:04:35,04
will stretch the full width of the browser window,

104
00:04:35,04 --> 00:04:36,08
whatever it happens to be,

105
00:04:36,08 --> 00:04:38,03
and make the design sort of feel

106
00:04:38,03 --> 00:04:41,03
like it's a little bit more full.

107
00:04:41,03 --> 00:04:42,06
Okay, once that's in place,

108
00:04:42,06 --> 00:04:46,05
then we can go ahead and add a margin of three rem

109
00:04:46,05 --> 00:04:49,06
and auto and that'll give us, again,

110
00:04:49,06 --> 00:04:52,04
some space at the end of our content

111
00:04:52,04 --> 00:04:55,00
before our headers, the navigation, and the footer,

112
00:04:55,00 --> 00:04:56,07
so that's really great.

113
00:04:56,07 --> 00:04:59,09
And then the other thing that we'll need to add here then

114
00:04:59,09 --> 00:05:02,00
is for main,

115
00:05:02,00 --> 00:05:04,02
we're going to have to spell out how wide

116
00:05:04,02 --> 00:05:06,03
is this particular layout?

117
00:05:06,03 --> 00:05:10,04
So main, remember, is going to be those purple flowers

118
00:05:10,04 --> 00:05:12,09
and it's going to be two columns wide.

119
00:05:12,09 --> 00:05:18,03
So if I use my grid hyphen column syntax,

120
00:05:18,03 --> 00:05:20,02
I can just simply count the number.

121
00:05:20,02 --> 00:05:23,02
So if we start with over here,

122
00:05:23,02 --> 00:05:25,08
if we think about it this way,

123
00:05:25,08 --> 00:05:27,01
over here to the left of all these boxes,

124
00:05:27,01 --> 00:05:29,01
this is number one.

125
00:05:29,01 --> 00:05:31,04
The middle line here is number two,

126
00:05:31,04 --> 00:05:33,09
and the end line over here is number three,

127
00:05:33,09 --> 00:05:35,06
so with this syntax,

128
00:05:35,06 --> 00:05:37,04
what we have to say here with grid column

129
00:05:37,04 --> 00:05:39,09
is simply say one slash three,

130
00:05:39,09 --> 00:05:45,02
so we're telling it to span across everything that is there.

131
00:05:45,02 --> 00:05:47,01
Because of our grid template columns

132
00:05:47,01 --> 00:05:49,04
of two comma one fraction,

133
00:05:49,04 --> 00:05:52,06
what'll happen with all of the other children

134
00:05:52,06 --> 00:05:54,06
of the div with the class of content,

135
00:05:54,06 --> 00:05:57,05
they'll just fill in one box,

136
00:05:57,05 --> 00:05:58,09
one after the other

137
00:05:58,09 --> 00:06:00,04
all the way down the list.

138
00:06:00,04 --> 00:06:03,00
There's actually no additional code we need to write

139
00:06:03,00 --> 00:06:04,09
to lay that page out.

140
00:06:04,09 --> 00:06:08,02
And then finally, for my aside,

141
00:06:08,02 --> 00:06:11,01
I'm going to override some styling that happened earlier.

142
00:06:11,01 --> 00:06:15,02
We'll say that margin on the bottom is zero.

143
00:06:15,02 --> 00:06:17,07
Otherwise, it's going to get a little bit big

144
00:06:17,07 --> 00:06:20,09
and a little bit horsey looking.

145
00:06:20,09 --> 00:06:26,02
So let's go on ahead and refresh our page.

146
00:06:26,02 --> 00:06:27,02
And of course,

147
00:06:27,02 --> 00:06:29,01
we don't have any desktop styles in yet,

148
00:06:29,01 --> 00:06:32,05
so it does show up here at this very large dimension.

149
00:06:32,05 --> 00:06:35,00
There's our header and our nav bar stretching all the way

150
00:06:35,00 --> 00:06:36,01
across the dimension.

151
00:06:36,01 --> 00:06:38,04
Our footer goes all the way across on the bottom,

152
00:06:38,04 --> 00:06:40,04
but here in the middle, there's main

153
00:06:40,04 --> 00:06:42,08
stretching across the full two columns

154
00:06:42,08 --> 00:06:45,03
and then here's all of the other pieces

155
00:06:45,03 --> 00:06:48,01
that are going to wrap in here underneath,

156
00:06:48,01 --> 00:06:50,04
and each one of those children,

157
00:06:50,04 --> 00:06:52,01
each one of those grid items

158
00:06:52,01 --> 00:06:56,00
takes up exactly one column all the way through.

159
00:06:56,00 --> 00:06:57,08
Now we can go ahead and write the styles here

160
00:06:57,08 --> 00:07:00,08
for our desktop display.

161
00:07:00,08 --> 00:07:03,01
So here in this media query with the minimum width

162
00:07:03,01 --> 00:07:05,02
of 1000 pixels,

163
00:07:05,02 --> 00:07:08,03
once again, we're going to start with content.

164
00:07:08,03 --> 00:07:10,01
Same idea, again,

165
00:07:10,01 --> 00:07:12,07
although we're going to change how many columns we have,

166
00:07:12,07 --> 00:07:17,03
so here, our grid hyphen template hyphen columns

167
00:07:17,03 --> 00:07:20,05
is going to be repeat

168
00:07:20,05 --> 00:07:22,05
three comma one fraction.

169
00:07:22,05 --> 00:07:25,00
So now, we're up to a three-column layout instead

170
00:07:25,00 --> 00:07:28,04
and once again, we may want a max width.

171
00:07:28,04 --> 00:07:31,00
Let's say 1000 pixels,

172
00:07:31,00 --> 00:07:32,09
and again, that's really just art direction.

173
00:07:32,09 --> 00:07:35,07
You can decide what you want to do.

174
00:07:35,07 --> 00:07:39,00
Now in terms of our numbers and in terms of our layout,

175
00:07:39,00 --> 00:07:41,01
let's go on ahead and look at that again.

176
00:07:41,01 --> 00:07:43,05
If we look at our desktop dimension here,

177
00:07:43,05 --> 00:07:46,00
now we have our lines at one over here

178
00:07:46,00 --> 00:07:48,06
to the left of corporate functions.

179
00:07:48,06 --> 00:07:50,00
One, two,

180
00:07:50,00 --> 00:07:53,02
three, and four.

181
00:07:53,02 --> 00:07:55,04
So let's figure out what we've got going on here.

182
00:07:55,04 --> 00:07:56,09
For main, then,

183
00:07:56,09 --> 00:08:00,00
we are starting at the column numbers.

184
00:08:00,00 --> 00:08:02,03
We're starting at column number two

185
00:08:02,03 --> 00:08:03,08
and we're going over three

186
00:08:03,08 --> 00:08:05,09
to number four,

187
00:08:05,09 --> 00:08:07,06
but we're also spanning some rows,

188
00:08:07,06 --> 00:08:10,02
so we're also going to use the grid row property.

189
00:08:10,02 --> 00:08:12,00
What rows are we spanning over?

190
00:08:12,00 --> 00:08:14,07
Well, those start up here on the top with number one,

191
00:08:14,07 --> 00:08:17,02
number two, and number three.

192
00:08:17,02 --> 00:08:19,04
So that should give us exactly what we need

193
00:08:19,04 --> 00:08:23,05
to put in here for main.

194
00:08:23,05 --> 00:08:25,09
So our grid hyphen column

195
00:08:25,09 --> 00:08:29,01
will wind up being two slash four

196
00:08:29,01 --> 00:08:32,01
and our grid hyphen row

197
00:08:32,01 --> 00:08:34,04
will wind up being one slash three,

198
00:08:34,04 --> 00:08:37,07
and that's where those numbers came from.

199
00:08:37,07 --> 00:08:40,03
Now for the news and the blogs,

200
00:08:40,03 --> 00:08:43,03
those are the other things we need to think about.

201
00:08:43,03 --> 00:08:44,08
These two asides here

202
00:08:44,08 --> 00:08:47,07
because their layout is a little bit different also.

203
00:08:47,07 --> 00:08:50,02
If we look again at that desktop dimension,

204
00:08:50,02 --> 00:08:52,04
they're over here,

205
00:08:52,04 --> 00:08:53,08
which is a little bit unusual

206
00:08:53,08 --> 00:08:56,03
because of course, in our source code,

207
00:08:56,03 --> 00:08:57,05
main comes first

208
00:08:57,05 --> 00:09:00,00
so we need to specify where the newsletter

209
00:09:00,00 --> 00:09:03,06
and the recent blogs boxes are, as well.

210
00:09:03,06 --> 00:09:06,08
So we can go ahead and do that.

211
00:09:06,08 --> 00:09:09,06
So for news,

212
00:09:09,06 --> 00:09:12,00
we'll have a grid hyphen column

213
00:09:12,00 --> 00:09:14,07
of one slash two,

214
00:09:14,07 --> 00:09:19,07
and then for blogs,

215
00:09:19,07 --> 00:09:24,08
we'll have a grid hyphen column of one slash two, as well.

216
00:09:24,08 --> 00:09:27,06
And what will happen is that again,

217
00:09:27,06 --> 00:09:30,08
grid is going to assume that you want news before blogs

218
00:09:30,08 --> 00:09:32,09
because that's the order of the source order,

219
00:09:32,09 --> 00:09:35,03
so we don't need to spell out the grid row

220
00:09:35,03 --> 00:09:38,02
for that particular situation.

221
00:09:38,02 --> 00:09:40,02
Go ahead and save your CSS.

222
00:09:40,02 --> 00:09:42,05
Let's take a quick peek

223
00:09:42,05 --> 00:09:45,01
at the way the website is looking.

224
00:09:45,01 --> 00:09:48,00
And so now, you should see newsletter

225
00:09:48,00 --> 00:09:52,04
and recent blogs here to the left of the big main area.

226
00:09:52,04 --> 00:09:55,03
Your three boxes down here on the bottom.

227
00:09:55,03 --> 00:09:59,05
As we start to narrow up our screen,

228
00:09:59,05 --> 00:10:02,03
everything should stack in this lovely manner,

229
00:10:02,03 --> 00:10:04,08
exactly as our screenshot showed,

230
00:10:04,08 --> 00:10:07,06
and then if we scrunch it down one more time,

231
00:10:07,06 --> 00:10:09,04
we'll go to our mobile version

232
00:10:09,04 --> 00:10:11,08
and everything stacks on top of each other.

233
00:10:11,08 --> 00:10:14,04
So this is looking absolutely fabulous,

234
00:10:14,04 --> 00:10:17,00
and we're pretty much done with how the webpage looks

235
00:10:17,00 --> 00:10:20,09
because this is exactly the way we wanted it to look,

236
00:10:20,09 --> 00:10:22,09
but now we're going to recode it

237
00:10:22,09 --> 00:10:26,00
to take advantage of calc and custom properties.

