1
00:00:00,03 --> 00:00:06,04
(exciting music)

2
00:00:06,04 --> 00:00:08,06
- [Instructor] So how'd you do on that challenge?

3
00:00:08,06 --> 00:00:11,01
You might of found that challenge a little bit

4
00:00:11,01 --> 00:00:12,04
let's say challenging,

5
00:00:12,04 --> 00:00:15,02
because one of the things you probably really wanted

6
00:00:15,02 --> 00:00:18,01
in order to solve this problem was an exponent.

7
00:00:18,01 --> 00:00:21,09
In other words you could have said 1.2 to the 5th power.

8
00:00:21,09 --> 00:00:24,07
And that would've been a much nicer way

9
00:00:24,07 --> 00:00:25,08
of writing this stuff.

10
00:00:25,08 --> 00:00:29,07
But unfortunately Calc only supports addition, subtraction,

11
00:00:29,07 --> 00:00:31,03
multiplication, and division.

12
00:00:31,03 --> 00:00:34,02
And since the goal here is to document

13
00:00:34,02 --> 00:00:36,01
where these numbers are coming from,

14
00:00:36,01 --> 00:00:38,05
this is the way I wound up writing these.

15
00:00:38,05 --> 00:00:41,07
So we started with a paragraph with our font size of 1M,

16
00:00:41,07 --> 00:00:44,09
base font size I'm just going to use the default browser size

17
00:00:44,09 --> 00:00:46,09
of 16 pixels.

18
00:00:46,09 --> 00:00:52,03
The H5 then is 1M times 1.2, the H4 is 1M times 1.2

19
00:00:52,03 --> 00:00:55,02
times 1.2, and so on and so forth

20
00:00:55,02 --> 00:00:57,03
as we get to the larger numbers.

21
00:00:57,03 --> 00:01:00,06
When we go down to those smaller sizes, we just divide.

22
00:01:00,06 --> 00:01:04,03
And if we had even smaller sizes we would continue dividing

23
00:01:04,03 --> 00:01:06,09
just the same way we multiplied before.

24
00:01:06,09 --> 00:01:10,03
Now some of you might have written here for these answers

25
00:01:10,03 --> 00:01:14,03
an answer sort of like instead of H4

26
00:01:14,03 --> 00:01:17,04
is 1M times 1.2 times 1.2,

27
00:01:17,04 --> 00:01:22,06
maybe you said something like 1.2 times 1.2

28
00:01:22,06 --> 00:01:26,07
or 1.44 times 1.2, something like that.

29
00:01:26,07 --> 00:01:28,09
But then it's not as necessarily as clear

30
00:01:28,09 --> 00:01:31,04
where those numbers are coming from.

31
00:01:31,04 --> 00:01:35,04
While this is incredibly awkward and really wordy,

32
00:01:35,04 --> 00:01:40,05
it is very clear exactly how each number is derived.

33
00:01:40,05 --> 00:01:44,06
And there is a much better way to write this.

34
00:01:44,06 --> 00:01:47,07
And that will involve using some variables.

35
00:01:47,07 --> 00:01:50,03
And that will make our lives so much easier.

36
00:01:50,03 --> 00:01:52,02
We'll start to talk about those variables

37
00:01:52,02 --> 00:01:54,00
in our next chapter.

