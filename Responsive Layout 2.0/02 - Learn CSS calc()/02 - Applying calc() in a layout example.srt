1
00:00:00,05 --> 00:00:02,08
- [Instructor] Now that you have a sense of how Kelp works,

2
00:00:02,08 --> 00:00:05,02
let's apply it to an example.

3
00:00:05,02 --> 00:00:07,01
This is CodePen

4
00:00:07,01 --> 00:00:09,07
and this CodePen is available to you

5
00:00:09,07 --> 00:00:13,05
at the address that's on the screen here.

6
00:00:13,05 --> 00:00:15,07
You can go ahead and create an account,

7
00:00:15,07 --> 00:00:17,02
you can fork this

8
00:00:17,02 --> 00:00:18,08
and save your edits.

9
00:00:18,08 --> 00:00:20,06
If you don't want to create an account,

10
00:00:20,06 --> 00:00:22,02
and you don't want to fork the code,

11
00:00:22,02 --> 00:00:24,01
that's fine, you can just edit directly.

12
00:00:24,01 --> 00:00:27,01
It just means that your changes won't be saved.

13
00:00:27,01 --> 00:00:29,01
So what I have going on here as you can see,

14
00:00:29,01 --> 00:00:30,01
over here on the right side,

15
00:00:30,01 --> 00:00:32,04
is a four column layout.

16
00:00:32,04 --> 00:00:34,09
And I've built this using Flexbox.

17
00:00:34,09 --> 00:00:38,07
If you just take a quick peek at the CSS over here,

18
00:00:38,07 --> 00:00:42,07
the HTML and the CSS down here on the bottom,

19
00:00:42,07 --> 00:00:44,06
you can see exactly what I did here

20
00:00:44,06 --> 00:00:46,03
and where all the numbers come from.

21
00:00:46,03 --> 00:00:49,04
Totally obvious what I did, right?

22
00:00:49,04 --> 00:00:51,04
You totally get it.

23
00:00:51,04 --> 00:00:53,04
Well, if not

24
00:00:53,04 --> 00:00:55,00
then I'm going to need to explain this

25
00:00:55,00 --> 00:00:56,09
in a little bit more detail.

26
00:00:56,09 --> 00:00:58,06
And the big thing you might be wondering about

27
00:00:58,06 --> 00:01:02,04
here is what is going on with these numbers down here,

28
00:01:02,04 --> 00:01:03,04
with the Flex bases.

29
00:01:03,04 --> 00:01:04,08
They look kind of random.

30
00:01:04,08 --> 00:01:07,08
How did I generate those particular numbers?

31
00:01:07,08 --> 00:01:12,03
Well, let me go into that in a little bit more detail.

32
00:01:12,03 --> 00:01:13,08
So, in my code here,

33
00:01:13,08 --> 00:01:16,05
one of the first things that I talk about

34
00:01:16,05 --> 00:01:19,01
in the parent property

35
00:01:19,01 --> 00:01:20,02
is, I've said,

36
00:01:20,02 --> 00:01:23,08
the justified content is set to space-around.

37
00:01:23,08 --> 00:01:25,06
So space-around, uh,

38
00:01:25,06 --> 00:01:28,06
is defined as saying "Place half of the available space

39
00:01:28,06 --> 00:01:30,06
"on each side of the box."

40
00:01:30,06 --> 00:01:33,07
So if I have that row of four boxes,

41
00:01:33,07 --> 00:01:36,02
you think about that as a hundred divided by four,

42
00:01:36,02 --> 00:01:37,07
that's 25 percent.

43
00:01:37,07 --> 00:01:39,08
But I'd like to have a little bit of space in between

44
00:01:39,08 --> 00:01:42,06
so I'm going to divide that space in half,

45
00:01:42,06 --> 00:01:44,04
let's say it's one percent,

46
00:01:44,04 --> 00:01:46,02
just as a starting place.

47
00:01:46,02 --> 00:01:48,02
That would put half a percent of the space

48
00:01:48,02 --> 00:01:50,03
on either side of the box.

49
00:01:50,03 --> 00:01:53,04
And so, when I go into a layout like this

50
00:01:53,04 --> 00:01:56,01
what we're going to see is in the first row,

51
00:01:56,01 --> 00:01:58,04
each box is 24 percent wide.

52
00:01:58,04 --> 00:02:01,06
I've got a one percent gap in between two boxes

53
00:02:01,06 --> 00:02:04,06
because that's half a percent plus a half of percent

54
00:02:04,06 --> 00:02:07,03
and then a half of percent on the outside.

55
00:02:07,03 --> 00:02:10,02
Because of the way I've done my math here,

56
00:02:10,02 --> 00:02:13,07
I simply took those one percents

57
00:02:13,07 --> 00:02:16,00
and removed them from various places

58
00:02:16,00 --> 00:02:18,09
and the various combinations of layouts here.

59
00:02:18,09 --> 00:02:23,00
And this is my drawing of that screen

60
00:02:23,00 --> 00:02:26,04
and how my space is going to shake out.

61
00:02:26,04 --> 00:02:29,09
And what that means is I can code my little grid system

62
00:02:29,09 --> 00:02:31,03
pretty easily here,

63
00:02:31,03 --> 00:02:34,06
and that's how my numbers were derived.

64
00:02:34,06 --> 00:02:37,01
But that's a really long explanation, isn't it?

65
00:02:37,01 --> 00:02:40,02
Wouldn't it be better if I did this some other way,

66
00:02:40,02 --> 00:02:44,01
maybe showing you the math instead?

67
00:02:44,01 --> 00:02:46,09
All right, so instead of writing it this way,

68
00:02:46,09 --> 00:02:49,07
maybe we can just sort of explain it

69
00:02:49,07 --> 00:02:51,03
and right here in the code.

70
00:02:51,03 --> 00:02:53,02
So, there's my row

71
00:02:53,02 --> 00:02:55,05
and as you would expect display Flex.

72
00:02:55,05 --> 00:02:58,04
Flex flows over a wrap, justify content, space-around,

73
00:02:58,04 --> 00:03:01,05
maybe right about here we can just put in a quick comment.

74
00:03:01,05 --> 00:03:06,07
And we'll say, "The one percent of extra space

75
00:03:06,07 --> 00:03:15,02
"is assigned to the left and right of each box by Flexbox

76
00:03:15,02 --> 00:03:21,04
"via the space-around property."

77
00:03:21,04 --> 00:03:22,02
Okay?

78
00:03:22,02 --> 00:03:24,00
So, that's my little comment.

79
00:03:24,00 --> 00:03:26,05
And then, instead of just spelling out

80
00:03:26,05 --> 00:03:29,01
Flex spaces of 24 percent,

81
00:03:29,01 --> 00:03:32,01
ya know, why is this one 24 percent, that one's 49,

82
00:03:32,01 --> 00:03:34,05
that one's 74, we don't understand.

83
00:03:34,05 --> 00:03:37,04
We could just simply spell this out as a formula instead.

84
00:03:37,04 --> 00:03:39,08
So I'll just say CALC

85
00:03:39,08 --> 00:03:46,02
and I could say 25 percent minus one percent.

86
00:03:46,02 --> 00:03:47,09
So that's a little bit more obvious

87
00:03:47,09 --> 00:03:49,05
about what's going on here

88
00:03:49,05 --> 00:03:52,06
because a box that would span two rows

89
00:03:52,06 --> 00:03:54,06
you would expect it to be 50 percent,

90
00:03:54,06 --> 00:03:56,01
but we're actually going to make it

91
00:03:56,01 --> 00:03:58,00
just a little bit smaller instead.

92
00:03:58,00 --> 00:03:59,08
Likewise, it spanning three

93
00:03:59,08 --> 00:04:01,05
you'd expect it to be 75

94
00:04:01,05 --> 00:04:04,06
but this is a little bit less than that.

95
00:04:04,06 --> 00:04:08,01
And of course, the last one, you expect to be 100 percent,

96
00:04:08,01 --> 00:04:10,08
but it's going to be just short of that

97
00:04:10,08 --> 00:04:12,04
because again we want to account for the gap

98
00:04:12,04 --> 00:04:17,01
on the outside of that box.

99
00:04:17,01 --> 00:04:21,07
So, this is a very quick, very simple way of explaining

100
00:04:21,07 --> 00:04:24,05
the math that I have here for my layout.

101
00:04:24,05 --> 00:04:26,04
I'm using CALC to describe

102
00:04:26,04 --> 00:04:29,00
exactly how that layout is derived.

