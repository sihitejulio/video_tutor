1
00:00:00,05 --> 00:00:01,09
- [Instructor] To get started let's take

2
00:00:01,09 --> 00:00:04,09
a closer look at calc, a new specification

3
00:00:04,09 --> 00:00:09,08
which allows you to do math directly in your CSS.

4
00:00:09,08 --> 00:00:13,08
Math in CSS, how could that be useful?

5
00:00:13,08 --> 00:00:16,03
The calc specification is compatible

6
00:00:16,03 --> 00:00:18,03
with several types of CSS properties

7
00:00:18,03 --> 00:00:22,01
including length, this is likely the first type

8
00:00:22,01 --> 00:00:23,08
of property that comes to mind.

9
00:00:23,08 --> 00:00:26,08
Consider that we can use math with widths and heights

10
00:00:26,08 --> 00:00:30,01
as well as properties like flex spaces.

11
00:00:30,01 --> 00:00:34,00
Frequency, angle, and time are common properties associated

12
00:00:34,00 --> 00:00:37,04
with the CSS animations and transitions.

13
00:00:37,04 --> 00:00:40,07
While we won't be using these properties in this course,

14
00:00:40,07 --> 00:00:43,04
you will find interesting applications for calc

15
00:00:43,04 --> 00:00:47,00
with animations and transitions.

16
00:00:47,00 --> 00:00:50,06
Numbers and integers are present throughout CSS.

17
00:00:50,06 --> 00:00:53,08
Font sizes, margin and padding are properties

18
00:00:53,08 --> 00:00:55,05
that come immediately to mind,

19
00:00:55,05 --> 00:00:56,09
but there are many other properties

20
00:00:56,09 --> 00:01:00,00
that use numbers and integers.

21
00:01:00,00 --> 00:01:02,07
Declaring a calculation is fairly simple.

22
00:01:02,07 --> 00:01:04,06
You have a property value pair,

23
00:01:04,06 --> 00:01:06,09
just like you do throughout CSS.

24
00:01:06,09 --> 00:01:09,03
But here, you'll include the calc function

25
00:01:09,03 --> 00:01:11,05
as part of the value with the numbers

26
00:01:11,05 --> 00:01:13,09
that you want to combine inside.

27
00:01:13,09 --> 00:01:18,00
In this example, we're adding one rem to two pixels.

28
00:01:18,00 --> 00:01:19,08
The browsers will run this math

29
00:01:19,08 --> 00:01:23,09
and display the font at the appropriate size.

30
00:01:23,09 --> 00:01:26,03
As you might expect, you can do the usual types

31
00:01:26,03 --> 00:01:30,05
of math with calc using the usual symbols for calculations

32
00:01:30,05 --> 00:01:32,01
including addition, subtraction,

33
00:01:32,01 --> 00:01:35,05
multiplication, and division.

34
00:01:35,05 --> 00:01:38,07
The usual rule about the order of operations applies

35
00:01:38,07 --> 00:01:40,04
in terms of which calculations

36
00:01:40,04 --> 00:01:42,05
are performed first and second.

37
00:01:42,05 --> 00:01:45,03
Can't remember, reach back to the fourth grade

38
00:01:45,03 --> 00:01:49,05
and remember to please, excuse my dear aunt Sally.

39
00:01:49,05 --> 00:01:53,02
Parentheses, exponents, multiplication and division,

40
00:01:53,02 --> 00:01:55,06
and addition and subtraction.

41
00:01:55,06 --> 00:01:58,02
There are two big advantages to using calc

42
00:01:58,02 --> 00:02:03,05
over doing math inside of SaaS or another CSS pre-processor.

43
00:02:03,05 --> 00:02:07,01
First, with calc you can manipulate two numbers

44
00:02:07,01 --> 00:02:09,06
with different units, for example,

45
00:02:09,06 --> 00:02:15,06
try subtracting 20 pixels from 95%, SaaS would not be happy.

46
00:02:15,06 --> 00:02:17,04
But calc will happily do the math

47
00:02:17,04 --> 00:02:19,06
for you and display the result.

48
00:02:19,06 --> 00:02:22,05
Second, calc is a great way to document

49
00:02:22,05 --> 00:02:24,07
where odd numbers come from.

50
00:02:24,07 --> 00:02:26,06
While you may be able to see this type

51
00:02:26,06 --> 00:02:29,04
of documentation with SaaS, you couldn't see it

52
00:02:29,04 --> 00:02:32,06
in the compiled CSS, something you might be doing

53
00:02:32,06 --> 00:02:34,07
if you're reading someone else's code,

54
00:02:34,07 --> 00:02:37,07
calc is a great way to show your work.

55
00:02:37,07 --> 00:02:42,03
For disadvantages, well the usual suspect is browser support

56
00:02:42,03 --> 00:02:46,01
there's buggy support in Internet Explorer 9, 10, and 11

57
00:02:46,01 --> 00:02:48,09
and there's a few little bugs in Firefox as well.

58
00:02:48,09 --> 00:02:50,08
Nonetheless, as of this recording,

59
00:02:50,08 --> 00:02:55,08
about 96% of users are using a browser that supports calc.

60
00:02:55,08 --> 00:02:59,07
Check caniuse.com for the latest information

61
00:02:59,07 --> 00:03:01,00
about browser support.

