1
00:00:00,00 --> 00:00:06,03
(upbeat music)

2
00:00:06,03 --> 00:00:07,07
- [Instructor] Now that you have

3
00:00:07,07 --> 00:00:10,08
a little bit of background about how calc works,

4
00:00:10,08 --> 00:00:15,02
let's apply it to a real-world problem.

5
00:00:15,02 --> 00:00:17,03
And one of those real-world problems

6
00:00:17,03 --> 00:00:20,02
happens to be this concept of a type scale.

7
00:00:20,02 --> 00:00:24,01
This website is type-scale.com.

8
00:00:24,01 --> 00:00:26,02
And the concept of a type scale

9
00:00:26,02 --> 00:00:28,03
is something that comes from the print world,

10
00:00:28,03 --> 00:00:30,07
is now carried over to the web world.

11
00:00:30,07 --> 00:00:36,01
What we have going on here is a harmonious shift

12
00:00:36,01 --> 00:00:37,09
in the sizes of our type here

13
00:00:37,09 --> 00:00:42,05
from sort of a readable size that we might use

14
00:00:42,05 --> 00:00:45,01
in just standard text reading on a page,

15
00:00:45,01 --> 00:00:47,07
all the way on up here to some larger sizes

16
00:00:47,07 --> 00:00:49,07
that maybe might be used as headings

17
00:00:49,07 --> 00:00:51,07
or something on our webpage.

18
00:00:51,07 --> 00:00:54,03
And what this website will do for us

19
00:00:54,03 --> 00:00:56,06
is we set our base font size.

20
00:00:56,06 --> 00:00:58,08
16 pixels is generally the base,

21
00:00:58,08 --> 00:01:01,00
which is what your browser will use,

22
00:01:01,00 --> 00:01:03,07
and that's considered to be 1m.

23
00:01:03,07 --> 00:01:06,00
Then we're going to scale it by some number.

24
00:01:06,00 --> 00:01:07,04
If you're a music person,

25
00:01:07,04 --> 00:01:09,07
you might recognize the Minor Second,

26
00:01:09,07 --> 00:01:11,04
Major Second, Minor Third.

27
00:01:11,04 --> 00:01:14,02
If not, just consider that these are numbers

28
00:01:14,02 --> 00:01:18,05
that as they get larger, say, to a Perfect Fifth,

29
00:01:18,05 --> 00:01:20,02
or the Golden Ratio,

30
00:01:20,02 --> 00:01:24,01
that is a large difference between these type sizes,

31
00:01:24,01 --> 00:01:26,07
as in some cases ridiculously large,

32
00:01:26,07 --> 00:01:30,00
and as we reduce the size of our type scale,

33
00:01:30,00 --> 00:01:32,09
the differences between these sizes get smaller.

34
00:01:32,09 --> 00:01:34,02
Pretty straightforward.

35
00:01:34,02 --> 00:01:36,04
So, what I would like for you to do

36
00:01:36,04 --> 00:01:40,01
is to set this up with the Minor Third,

37
00:01:40,01 --> 00:01:45,01
a pretty popular type size variation here on the web.

38
00:01:45,01 --> 00:01:53,05
And the font that I'm going to use here is called Roboto,

39
00:01:53,05 --> 00:01:55,01
because that's something we're going to be using

40
00:01:55,01 --> 00:01:56,07
in a later example.

41
00:01:56,07 --> 00:02:00,08
And you'll see here this is doing some calculations.

42
00:02:00,08 --> 00:02:04,02
What it's done is it's taken our base font size,

43
00:02:04,02 --> 00:02:07,08
so let's say here at the 1m size.

44
00:02:07,08 --> 00:02:12,04
The next size is 1m times 1.2,

45
00:02:12,04 --> 00:02:15,01
that'll give us the size here, or second size up.

46
00:02:15,01 --> 00:02:20,08
1.44 is the previous size times our Minor Third again,

47
00:02:20,08 --> 00:02:22,08
that gives us 1.44.

48
00:02:22,08 --> 00:02:26,01
This one becomes the previous size times 1.2.

49
00:02:26,01 --> 00:02:27,03
And so, this will give you

50
00:02:27,03 --> 00:02:29,06
a very harmonious set of type sizes

51
00:02:29,06 --> 00:02:32,05
rather than just randomly picking type sizes,

52
00:02:32,05 --> 00:02:35,04
whatever it is that you want to use on your webpage.

53
00:02:35,04 --> 00:02:39,05
So, this is a great way to manage font sizes on your website

54
00:02:39,05 --> 00:02:41,04
in a way that your graphic designer friends

55
00:02:41,04 --> 00:02:44,04
will enjoy looking at your type.

56
00:02:44,04 --> 00:02:46,02
So, what I'd like for you to do now

57
00:02:46,02 --> 00:02:49,07
is in the CodePen that I've given you,

58
00:02:49,07 --> 00:02:52,01
I've given you the starting HTML

59
00:02:52,01 --> 00:02:56,03
and the CSS that comes right out of the type scale website,

60
00:02:56,03 --> 00:02:59,02
and as you see here, it's gone ahead and exported

61
00:02:59,02 --> 00:03:01,06
a whole bunch of different type sizes here.

62
00:03:01,06 --> 00:03:04,00
I'd like you to go through as part of your challenge

63
00:03:04,00 --> 00:03:06,06
and rework these font sizes.

64
00:03:06,06 --> 00:03:12,04
So, you're going to instead of saying 1.2m's,

65
00:03:12,04 --> 00:03:14,01
I actually want to see the math there.

66
00:03:14,01 --> 00:03:16,04
How did you get that 1.2m?

67
00:03:16,04 --> 00:03:18,04
So, go on ahead and write a calc statement

68
00:03:18,04 --> 00:03:20,05
that would show how that is derived.

69
00:03:20,05 --> 00:03:24,05
And just go on ahead and do that for h1 to h5.

70
00:03:24,05 --> 00:03:27,06
And don't forget about down here at the bottom,

71
00:03:27,06 --> 00:03:30,00
there is a small size also.

72
00:03:30,00 --> 00:03:33,03
How might you get 0.833m's?

73
00:03:33,03 --> 00:03:36,00
And I'll show you my answer in the next video.

