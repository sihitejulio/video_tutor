1
00:00:00,01 --> 00:00:06,03
(upbeat music)

2
00:00:06,03 --> 00:00:09,01
- [Instructor] So, time for another challenge,

3
00:00:09,01 --> 00:00:12,03
and we're going to continue on with the same challenge

4
00:00:12,03 --> 00:00:15,00
that we've been working on over the last few chapters,

5
00:00:15,00 --> 00:00:17,09
in terms of writing a type scale,

6
00:00:17,09 --> 00:00:19,02
in this particular challenge,

7
00:00:19,02 --> 00:00:22,04
scaling that type scale across different devices.

8
00:00:22,04 --> 00:00:25,01
So so far you've wrote out some stuff with Calk,

9
00:00:25,01 --> 00:00:27,06
we've went back and we added some custom properties,

10
00:00:27,06 --> 00:00:30,06
and so now it's time to add some media queries,

11
00:00:30,06 --> 00:00:32,03
so down here at the bottom,

12
00:00:32,03 --> 00:00:35,02
you'll see that I've added two empty media queries

13
00:00:35,02 --> 00:00:37,05
that are just waiting for you to think

14
00:00:37,05 --> 00:00:41,09
about how this type scale might change across devices.

15
00:00:41,09 --> 00:00:44,05
So what I'd like for you to do

16
00:00:44,05 --> 00:00:47,04
is to think about how you might go about

17
00:00:47,04 --> 00:00:50,02
making some changes to the type on these different devices,

18
00:00:50,02 --> 00:00:53,09
whatever makes sense to you, whatever you think looks good,

19
00:00:53,09 --> 00:00:57,03
and let me remind you that we got this type scale

20
00:00:57,03 --> 00:01:03,04
originally from the Type Scale website at type-scale.com.

21
00:01:03,04 --> 00:01:06,02
You can come here and you can take a look

22
00:01:06,02 --> 00:01:09,04
at the different kinds of type scales that we have,

23
00:01:09,04 --> 00:01:12,01
you can certainly think about changing something like that,

24
00:01:12,01 --> 00:01:13,09
you could think about maybe making

25
00:01:13,09 --> 00:01:16,05
a change to the base font size,

26
00:01:16,05 --> 00:01:20,00
or some other type of change that you might want to make.

27
00:01:20,00 --> 00:01:24,00
And in the next video, I'll tell you what my solution is.

