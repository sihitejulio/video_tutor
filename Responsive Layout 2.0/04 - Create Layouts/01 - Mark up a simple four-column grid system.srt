1
00:00:00,05 --> 00:00:01,08
- [Instructor] In this next chapter,

2
00:00:01,08 --> 00:00:04,02
I'll walk you through step by step

3
00:00:04,02 --> 00:00:07,00
creating a flex box based grid system,

4
00:00:07,00 --> 00:00:10,06
using the principles of responsive design 2.0.

5
00:00:10,06 --> 00:00:14,02
We'll be creating a simple four column grid system.

6
00:00:14,02 --> 00:00:16,02
However, you can use the principles

7
00:00:16,02 --> 00:00:18,01
that I'm going to introduce here

8
00:00:18,01 --> 00:00:19,09
to create your own grid system.

9
00:00:19,09 --> 00:00:23,03
No matter how many or how few columns you'd like to use,

10
00:00:23,03 --> 00:00:26,05
because we're going to leverage calc and custom properties

11
00:00:26,05 --> 00:00:28,08
as part of our process.

12
00:00:28,08 --> 00:00:30,00
I showed you one way

13
00:00:30,00 --> 00:00:32,03
to start coding a four column grid system

14
00:00:32,03 --> 00:00:34,02
back in the first chapter,

15
00:00:34,02 --> 00:00:36,08
but I didn't include any media queries.

16
00:00:36,08 --> 00:00:38,09
So, over the course of this chapter,

17
00:00:38,09 --> 00:00:41,01
we're going to go through the whole process

18
00:00:41,01 --> 00:00:43,01
using a slightly different methodology

19
00:00:43,01 --> 00:00:45,07
than what I used in the first chapter.

20
00:00:45,07 --> 00:00:47,08
And, we'll be adding CSS.

21
00:00:47,08 --> 00:00:49,01
We'll be adding media queries.

22
00:00:49,01 --> 00:00:50,00
We'll work with calc.

23
00:00:50,00 --> 00:00:51,04
We'll work with custom properties.

24
00:00:51,04 --> 00:00:52,04
All of that.

25
00:00:52,04 --> 00:00:53,08
But, in order to get started,

26
00:00:53,08 --> 00:00:56,01
the very first thing that we have to do is

27
00:00:56,01 --> 00:00:58,01
we have to start with some quality mark up.

28
00:00:58,01 --> 00:01:00,06
That should always be your first stop

29
00:01:00,06 --> 00:01:02,09
in terms of laying out a webpage.

30
00:01:02,09 --> 00:01:04,09
So let's just take a quick peak

31
00:01:04,09 --> 00:01:07,09
about what we've got going on here in our HTML.

32
00:01:07,09 --> 00:01:10,01
We have a div with a class of wrapper.

33
00:01:10,01 --> 00:01:12,02
That's going to go all the way around our article.

34
00:01:12,02 --> 00:01:15,01
You'll see wrappers all over the web.

35
00:01:15,01 --> 00:01:18,09
And inside of this, we have a series of articles.

36
00:01:18,09 --> 00:01:23,00
These are of course little syndicational bits of content.

37
00:01:23,00 --> 00:01:26,00
We can take whatever this is here with the weddings,

38
00:01:26,00 --> 00:01:28,02
the picture, the little paragraph of introduction

39
00:01:28,02 --> 00:01:30,09
and the read more and we could syndicate

40
00:01:30,09 --> 00:01:32,06
that little bit of content,

41
00:01:32,06 --> 00:01:34,09
so we've marked that up as an article.

42
00:01:34,09 --> 00:01:37,08
And this is going to ultimately be row one.

43
00:01:37,08 --> 00:01:41,01
It'll be four individual boxes.

44
00:01:41,01 --> 00:01:42,05
Then you'll see that I have a comment here

45
00:01:42,05 --> 00:01:43,04
where I have row two.

46
00:01:43,04 --> 00:01:46,03
It's two boxes each spanning two rows.

47
00:01:46,03 --> 00:01:48,02
We have row three, a narrow left column

48
00:01:48,02 --> 00:01:50,03
with a wide right column.

49
00:01:50,03 --> 00:01:53,09
We have row four, which will be one big, fat column.

50
00:01:53,09 --> 00:01:56,03
And then finally in row five, we're going to have

51
00:01:56,03 --> 00:01:58,03
one of those one-two-one arrangements,

52
00:01:58,03 --> 00:02:00,08
so the first article will take up one column,

53
00:02:00,08 --> 00:02:02,05
the second will take up two

54
00:02:02,05 --> 00:02:05,03
and the third column take one column, as well.

55
00:02:05,03 --> 00:02:07,05
And these are desktop descriptions

56
00:02:07,05 --> 00:02:10,02
of what it is that we're looking for.

57
00:02:10,02 --> 00:02:12,05
Obviously on something like a mobile site,

58
00:02:12,05 --> 00:02:15,05
this is just going to stack these things on top of each other.

59
00:02:15,05 --> 00:02:18,04
So, what is the core of Flexbox?

60
00:02:18,04 --> 00:02:20,05
We think about it, always have to think

61
00:02:20,05 --> 00:02:24,06
about parents and children, flex containers and flex items,

62
00:02:24,06 --> 00:02:26,07
so remember that with the flex container,

63
00:02:26,07 --> 00:02:28,02
we have a certain set of properties

64
00:02:28,02 --> 00:02:31,02
that will describe how this page is going to look.

65
00:02:31,02 --> 00:02:34,06
The flex items inherent certain characteristics, as well.

66
00:02:34,06 --> 00:02:37,09
The grandchildren, that third level in the flexbox

67
00:02:37,09 --> 00:02:42,07
in that Document Object Model, that HTML tree of tags,

68
00:02:42,07 --> 00:02:45,08
they are not going to inherit anything at all.

69
00:02:45,08 --> 00:02:49,00
So, let's go on ahead and start putting this together.

70
00:02:49,00 --> 00:02:52,02
Probably what I want to do is group these rows,

71
00:02:52,02 --> 00:02:56,08
maybe with something like a div with a class of row.

72
00:02:56,08 --> 00:02:58,06
And that will ultimately be

73
00:02:58,06 --> 00:03:03,09
our flex parent or the flex container.

74
00:03:03,09 --> 00:03:06,06
And I'm going to end that row down here

75
00:03:06,06 --> 00:03:08,03
just before that next comment.

76
00:03:08,03 --> 00:03:11,08
So, here my div with a class of row is going to start

77
00:03:11,08 --> 00:03:15,07
on line three and it will end on line 28.

78
00:03:15,07 --> 00:03:18,09
Let's go on ahead and put in a few more of those.

79
00:03:18,09 --> 00:03:21,00
I'll put one around the next row here,

80
00:03:21,00 --> 00:03:24,09
a div with a class of row.

81
00:03:24,09 --> 00:03:27,03
That's on line 30 and then we'll go ahead

82
00:03:27,03 --> 00:03:33,06
and end that on line 43.

83
00:03:33,06 --> 00:03:35,07
Then on row three, we'll go ahead

84
00:03:35,07 --> 00:03:41,02
and start a div with a class of row,

85
00:03:41,02 --> 00:03:46,05
here on line 45

86
00:03:46,05 --> 00:03:51,07
and then on line 57.

87
00:03:51,07 --> 00:03:57,08
The next div with class of row on line 60

88
00:03:57,08 --> 00:04:03,03
ending on line 67.

89
00:04:03,03 --> 00:04:12,02
And then finally one more here on line 69.

90
00:04:12,02 --> 00:04:18,01
And we can end that on line 88.

91
00:04:18,01 --> 00:04:21,00
Great, so now we have our rows established.

92
00:04:21,00 --> 00:04:24,07
The next thing to do is to throw on some columns

93
00:04:24,07 --> 00:04:27,07
and we'll put some classes to make that happen.

94
00:04:27,07 --> 00:04:32,00
So, in our first row, which starts here on line three,

95
00:04:32,00 --> 00:04:34,03
we want to have four individual boxes,

96
00:04:34,03 --> 00:04:36,03
each one going across the page.

97
00:04:36,03 --> 00:04:39,07
So I'm just going to call these, right in my article tag,

98
00:04:39,07 --> 00:04:43,05
I'll add a class here of col hyphen one.

99
00:04:43,05 --> 00:04:47,03
In other words, this is the definition of one column wide.

100
00:04:47,03 --> 00:04:50,07
So we'll just go on ahead and copy that class with col-1.

101
00:04:50,07 --> 00:04:54,00
We'll go on ahead and paste that into the article

102
00:04:54,00 --> 00:04:58,05
in line 10, the article in line 16

103
00:04:58,05 --> 00:05:01,09
and the article here on line 22.

104
00:05:01,09 --> 00:05:04,01
So that takes care of our first row.

105
00:05:04,01 --> 00:05:07,00
Our second row, we're going to have two boxes

106
00:05:07,00 --> 00:05:10,09
each box spanning two columns.

107
00:05:10,09 --> 00:05:14,09
And so, here with article on line 31

108
00:05:14,09 --> 00:05:18,08
that should be a class of col-2

109
00:05:18,08 --> 00:05:25,09
and our article on line 37 will also have a class of col-2.

110
00:05:25,09 --> 00:05:28,00
Then down here on row three,

111
00:05:28,00 --> 00:05:30,05
we're going to have a narrow left column

112
00:05:30,05 --> 00:05:32,02
and a wide right column.

113
00:05:32,02 --> 00:05:37,04
So the article on line 46,

114
00:05:37,04 --> 00:05:39,09
we're going to give this a class of col-1

115
00:05:39,09 --> 00:05:42,08
that would be a narrow left column, I think.

116
00:05:42,08 --> 00:05:46,08
And then the other article that is on line 52

117
00:05:46,08 --> 00:05:49,01
we'll give that class of col-3.

118
00:05:49,01 --> 00:05:53,01
In other words, it will be three columns wide.

119
00:05:53,01 --> 00:05:56,01
And then finally down here on row four.

120
00:05:56,01 --> 00:05:58,09
We have our single article tag,

121
00:05:58,09 --> 00:06:03,09
we'll just give that a class of col-4.

122
00:06:03,09 --> 00:06:07,03
And then down here for our one-two-one arrangement,

123
00:06:07,03 --> 00:06:09,06
I'm sure you can guess what comes here.

124
00:06:09,06 --> 00:06:13,07
This will be article with a class of col-1 on line 70,

125
00:06:13,07 --> 00:06:18,00
article with a class of col-2 on line 76

126
00:06:18,00 --> 00:06:24,04
and article with a class of col-1 on line 82.

127
00:06:24,04 --> 00:06:28,01
Okay great, so now we have our markup established here

128
00:06:28,01 --> 00:06:31,03
for our layout, we have rows that are going

129
00:06:31,03 --> 00:06:34,02
to wind up being our flex parents, the flex containers,

130
00:06:34,02 --> 00:06:38,02
and we have a series of classes from col-1 to col-4

131
00:06:38,02 --> 00:06:41,02
that are going to be our individual flex items

132
00:06:41,02 --> 00:06:43,02
and they are going to vary in width.

133
00:06:43,02 --> 00:06:45,04
Now that we have that structure established,

134
00:06:45,04 --> 00:06:48,00
we're ready to take a look at the CSS.

