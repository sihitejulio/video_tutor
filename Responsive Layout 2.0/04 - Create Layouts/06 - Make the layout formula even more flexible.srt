1
00:00:00,05 --> 00:00:01,03
- [Instructor] All right,

2
00:00:01,03 --> 00:00:03,07
so we've laid out our whole webpage

3
00:00:03,07 --> 00:00:07,05
using the cool little formula that we've got going on here.

4
00:00:07,05 --> 00:00:11,08
But I've just added onto this another piece of HTML.

5
00:00:11,08 --> 00:00:12,07
You know how clients are.

6
00:00:12,07 --> 00:00:13,06
They come back to you

7
00:00:13,06 --> 00:00:15,09
and they want to lay yet something else out.

8
00:00:15,09 --> 00:00:18,02
And so what's going on here now

9
00:00:18,02 --> 00:00:21,05
is I've added to this the very top of our HTML,

10
00:00:21,05 --> 00:00:23,03
always look at that first,

11
00:00:23,03 --> 00:00:25,09
at the very top of our HTML,

12
00:00:25,09 --> 00:00:28,09
I have a note from the client

13
00:00:28,09 --> 00:00:31,08
that says we want to have one image across here

14
00:00:31,08 --> 00:00:35,00
at mobile, this is going to be an image gallery,

15
00:00:35,00 --> 00:00:37,06
and then we're going to have it go two images across

16
00:00:37,06 --> 00:00:39,02
at the tablet dimension

17
00:00:39,02 --> 00:00:43,00
and three images across at a desktop dimension.

18
00:00:43,00 --> 00:00:45,01
So we have our row declared here.

19
00:00:45,01 --> 00:00:47,01
We have our div with a class of col four

20
00:00:47,01 --> 00:00:50,04
because this is going to occupy the full width

21
00:00:50,04 --> 00:00:52,05
of the screen, but inside of that,

22
00:00:52,05 --> 00:00:56,01
we have an unordered list with individual list items.

23
00:00:56,01 --> 00:00:59,04
There's six of them, six doesn't go anywhere near the four

24
00:00:59,04 --> 00:01:00,07
that we've been building so far.

25
00:01:00,07 --> 00:01:02,04
What are we going to do?

26
00:01:02,04 --> 00:01:07,01
And then we have some images that are here inside of that.

27
00:01:07,01 --> 00:01:10,01
All right, so that's what's going on.

28
00:01:10,01 --> 00:01:11,04
What could we do at this point?

29
00:01:11,04 --> 00:01:14,01
Well obviously, we could sit down and code something

30
00:01:14,01 --> 00:01:17,06
and make it do exactly what it says it wants to do,

31
00:01:17,06 --> 00:01:22,06
but we can actually leverage the formula we already have.

32
00:01:22,06 --> 00:01:24,06
How cool is that?

33
00:01:24,06 --> 00:01:29,00
And apply it directly to this list

34
00:01:29,00 --> 00:01:32,02
to make it do exactly what we want it to do.

35
00:01:32,02 --> 00:01:35,09
So let's go on ahead and make that magic happen.

36
00:01:35,09 --> 00:01:38,03
As always, the very best place to start here

37
00:01:38,03 --> 00:01:41,00
is identifying parents and children,

38
00:01:41,00 --> 00:01:44,06
and so we have this lovely unordered list here.

39
00:01:44,06 --> 00:01:49,05
Our parent or the flex container will wind up being our UL.

40
00:01:49,05 --> 00:01:54,02
The children or the flex item will wind up being our LIs.

41
00:01:54,02 --> 00:01:58,06
So if we apply a Flexbox layout using the UL as our parent,

42
00:01:58,06 --> 00:02:00,04
the LIs as the children,

43
00:02:00,04 --> 00:02:03,01
we should be able to lay this out exactly

44
00:02:03,01 --> 00:02:05,04
as is described there

45
00:02:05,04 --> 00:02:09,04
and so let's go into our CSS down here.

46
00:02:09,04 --> 00:02:11,06
So right now, we have a row

47
00:02:11,06 --> 00:02:13,05
and when we have an individual class

48
00:02:13,05 --> 00:02:16,02
that's going to have this formula attached to it,

49
00:02:16,02 --> 00:02:18,03
what could we do instead here?

50
00:02:18,03 --> 00:02:21,04
Well, we could say something like instead of row,

51
00:02:21,04 --> 00:02:23,09
we could just say row comma,

52
00:02:23,09 --> 00:02:28,05
so in addition to also for col hyphen four UL,

53
00:02:28,05 --> 00:02:30,08
you could just say UL, it is the only list

54
00:02:30,08 --> 00:02:33,01
that is in the HTML we're working with,

55
00:02:33,01 --> 00:02:35,00
but chances are,

56
00:02:35,00 --> 00:02:36,08
you're going to have other lists that are going to wind up

57
00:02:36,08 --> 00:02:37,07
being in your website,

58
00:02:37,07 --> 00:02:40,03
so let's be a little more specific.

59
00:02:40,03 --> 00:02:44,05
And then here where it says class equals call,

60
00:02:44,05 --> 00:02:48,03
what we're going to say is also

61
00:02:48,03 --> 00:02:51,08
dot col hyphen four LI.

62
00:02:51,08 --> 00:02:55,08
So now we've gone ahead and applied our various logic here

63
00:02:55,08 --> 00:03:01,00
to this and why isn't it wanting to go up on rows?

64
00:03:01,00 --> 00:03:04,08
Because none of the desktop dimensions down here

65
00:03:04,08 --> 00:03:06,09
have a width that's set

66
00:03:06,09 --> 00:03:08,06
that will work at the screen dimension

67
00:03:08,06 --> 00:03:09,08
that I'm working with right now.

68
00:03:09,08 --> 00:03:12,02
So that's the next problem.

69
00:03:12,02 --> 00:03:14,08
How do we go on ahead and manipulate some of these variables

70
00:03:14,08 --> 00:03:18,06
to reflect what it is that we want to do?

71
00:03:18,06 --> 00:03:20,07
So here at mobile dimensions,

72
00:03:20,07 --> 00:03:22,06
what we're going to do

73
00:03:22,06 --> 00:03:25,00
is we're going to add to this a little bit.

74
00:03:25,00 --> 00:03:28,07
So here, where it says image gallery styles,

75
00:03:28,07 --> 00:03:30,08
we have our col four of UL.

76
00:03:30,08 --> 00:03:33,06
This is the stuff that turns off the bullets.

77
00:03:33,06 --> 00:03:35,02
Let's add to this,

78
00:03:35,02 --> 00:03:38,01
instead of sticking with four columns,

79
00:03:38,01 --> 00:03:39,02
let's go to six

80
00:03:39,02 --> 00:03:40,07
because six is going to allow us

81
00:03:40,07 --> 00:03:43,01
to go three across and then two across

82
00:03:43,01 --> 00:03:45,03
and then one across, very, very nicely

83
00:03:45,03 --> 00:03:46,05
with six images,

84
00:03:46,05 --> 00:03:49,07
so we're going to say columns

85
00:03:49,07 --> 00:03:52,03
now has a new value of six.

86
00:03:52,03 --> 00:03:55,08
And we had it assigned up here

87
00:03:55,08 --> 00:03:58,08
in our initial declaration up here

88
00:03:58,08 --> 00:04:01,04
that starts around line 18 of four,

89
00:04:01,04 --> 00:04:03,03
but because this comes later in the document,

90
00:04:03,03 --> 00:04:04,09
this is going to take priority,

91
00:04:04,09 --> 00:04:07,05
so we now have six columns for the UL

92
00:04:07,05 --> 00:04:09,03
and then for the LI,

93
00:04:09,03 --> 00:04:14,02
we can just say dot col hyphen four LI.

94
00:04:14,02 --> 00:04:16,03
What do we want to do here?

95
00:04:16,03 --> 00:04:18,07
Let's say its width to start with

96
00:04:18,07 --> 00:04:21,03
is also six because at the mobile layout,

97
00:04:21,03 --> 00:04:24,09
we want it to be one item per row

98
00:04:24,09 --> 00:04:27,07
and then we could also do something like say,

99
00:04:27,07 --> 00:04:32,05
text align of center

100
00:04:32,05 --> 00:04:34,06
because obviously, these are not taking up the full width

101
00:04:34,06 --> 00:04:36,02
of the screen,

102
00:04:36,02 --> 00:04:37,02
and just to drive that home,

103
00:04:37,02 --> 00:04:39,04
let's add to this a color

104
00:04:39,04 --> 00:04:41,09
so we can see what's going on.

105
00:04:41,09 --> 00:04:43,03
And the color I'm going to add,

106
00:04:43,03 --> 00:04:46,03
I'm going to put it up here with my other colors

107
00:04:46,03 --> 00:04:48,07
where I have all my other background color declarations,

108
00:04:48,07 --> 00:04:53,09
so here I'm going to say dot col four LI.

109
00:04:53,09 --> 00:04:57,05
Let's give this a background

110
00:04:57,05 --> 00:05:01,07
color of FA zero.

111
00:05:01,07 --> 00:05:03,03
So a really hot orange

112
00:05:03,03 --> 00:05:07,06
so we can see what's going on here within our list items.

113
00:05:07,06 --> 00:05:09,09
All right, so that's our mobile dimensions.

114
00:05:09,09 --> 00:05:11,00
Now we can go on.

115
00:05:11,00 --> 00:05:13,05
Let's address what happens in the media queries,

116
00:05:13,05 --> 00:05:19,01
so as we scroll on down here to our media queries,

117
00:05:19,01 --> 00:05:21,04
so at 480 pixels,

118
00:05:21,04 --> 00:05:22,08
we're going to make a change here.

119
00:05:22,08 --> 00:05:25,07
We want this now to have two images across

120
00:05:25,07 --> 00:05:28,07
and so we'll just go on ahead and add to this a style

121
00:05:28,07 --> 00:05:32,05
of dot col four LI,

122
00:05:32,05 --> 00:05:36,05
and here, we'll say that the width is three.

123
00:05:36,05 --> 00:05:40,09
So that will give us two images per row, so there it is.

124
00:05:40,09 --> 00:05:43,03
That's all we needed to do.

125
00:05:43,03 --> 00:05:45,07
See how easy it is to take the same formula

126
00:05:45,07 --> 00:05:48,03
and now we can just apply it here on the screen?

127
00:05:48,03 --> 00:05:50,05
When we hit our break point here,

128
00:05:50,05 --> 00:05:53,09
then these images will stack on top of each other.

129
00:05:53,09 --> 00:05:55,01
Okay, good.

130
00:05:55,01 --> 00:05:58,02
And then of course, we need to do something at the desktop,

131
00:05:58,02 --> 00:05:59,06
so here for the desktop,

132
00:05:59,06 --> 00:06:06,04
we just simply add dot col hyphen four LI

133
00:06:06,04 --> 00:06:10,09
and our width value here will be two.

134
00:06:10,09 --> 00:06:14,02
So now we wind up with our three pictures going across here

135
00:06:14,02 --> 00:06:16,00
at desktop dimensions.

136
00:06:16,00 --> 00:06:19,05
We go to two at tablet dimensions

137
00:06:19,05 --> 00:06:22,08
and then one each stacked on top of each other here

138
00:06:22,08 --> 00:06:26,02
at mobile dimensions.

139
00:06:26,02 --> 00:06:30,03
Now some of you may not be very happy

140
00:06:30,03 --> 00:06:33,09
with what's going on with our gap.

141
00:06:33,09 --> 00:06:36,05
If you look at our gap,

142
00:06:36,05 --> 00:06:39,03
our gap gets a little bit wide here,

143
00:06:39,03 --> 00:06:41,05
and that's because

144
00:06:41,05 --> 00:06:44,00
in our original formula,

145
00:06:44,00 --> 00:06:48,09
we set our gap to be this math times 1%.

146
00:06:48,09 --> 00:06:52,05
So to make this formula even more flexible,

147
00:06:52,05 --> 00:06:54,04
what if we take that 1%

148
00:06:54,04 --> 00:06:59,02
and turn that into a variable as well?

149
00:06:59,02 --> 00:07:01,08
So simply after initial basis,

150
00:07:01,08 --> 00:07:03,08
we could create something called, let's say,

151
00:07:03,08 --> 00:07:07,00
gap hyphen constant,

152
00:07:07,00 --> 00:07:08,05
because that's basically what this is.

153
00:07:08,05 --> 00:07:10,00
It's the constant,

154
00:07:10,00 --> 00:07:13,05
and we'll make that 1% as our initial declaration

155
00:07:13,05 --> 00:07:16,02
and then in our gap formula, we'll change this.

156
00:07:16,02 --> 00:07:18,02
So if we say calc,

157
00:07:18,02 --> 00:07:20,08
we can say calc again

158
00:07:20,08 --> 00:07:22,09
because we're going to do this little bit of math.

159
00:07:22,09 --> 00:07:26,04
We're going to subtract our columns minus our width

160
00:07:26,04 --> 00:07:29,02
and then instead of 1%,

161
00:07:29,02 --> 00:07:32,01
we'll multiply that by our variable,

162
00:07:32,01 --> 00:07:37,03
gap hyphen constant.

163
00:07:37,03 --> 00:07:40,09
Okay, so basically we've changed nothing so far.

164
00:07:40,09 --> 00:07:44,02
All we've done is just take out that hard-coded number

165
00:07:44,02 --> 00:07:47,09
of 1%, substituted it with a gap constant.

166
00:07:47,09 --> 00:07:51,08
Now when I go down to my col four LI,

167
00:07:51,08 --> 00:07:55,02
I can now pass in a different value for the gap constant.

168
00:07:55,02 --> 00:08:00,06
So let's just say that we want our gap hyphen constant

169
00:08:00,06 --> 00:08:03,06
to be 0.3%.

170
00:08:03,06 --> 00:08:05,01
So we could do something like that,

171
00:08:05,01 --> 00:08:09,07
so now we have a narrower space in between our images.

172
00:08:09,07 --> 00:08:10,08
If you don't like that,

173
00:08:10,08 --> 00:08:14,01
we could always make it, you know, 0.1%

174
00:08:14,01 --> 00:08:15,09
or you could make it larger,

175
00:08:15,09 --> 00:08:17,03
half a percent,

176
00:08:17,03 --> 00:08:19,00
so whatever you want it to be,

177
00:08:19,00 --> 00:08:22,03
you can just change the value of the gap constant there.

178
00:08:22,03 --> 00:08:25,04
It'll override what comes earlier in the style sheet

179
00:08:25,04 --> 00:08:29,04
where that gap constant was declared at 1%.

180
00:08:29,04 --> 00:08:34,05
So essentially, we've written an entire grid layout

181
00:08:34,05 --> 00:08:37,05
all in these two declarations, very simply.

182
00:08:37,05 --> 00:08:40,04
We wrote it here with our initial declaration

183
00:08:40,04 --> 00:08:41,03
for the parent.

184
00:08:41,03 --> 00:08:44,00
That is display flex, row wrap, space between,

185
00:08:44,00 --> 00:08:49,00
and then with this lovely bit of individual variables

186
00:08:49,00 --> 00:08:51,00
and the math,

187
00:08:51,00 --> 00:08:52,08
we've actually spelled out exactly what the gap should be

188
00:08:52,08 --> 00:08:54,09
and how many gaps there should be,

189
00:08:54,09 --> 00:08:57,04
and how many boxes we're going to have on each row.

190
00:08:57,04 --> 00:08:59,07
We could now change our columns,

191
00:08:59,07 --> 00:09:03,01
change our widths, change our gap constant,

192
00:09:03,01 --> 00:09:06,04
and apply this formula anywhere else in our document

193
00:09:06,04 --> 00:09:09,04
and come up with completely different layouts

194
00:09:09,04 --> 00:09:12,05
or even different layouts across media queries

195
00:09:12,05 --> 00:09:15,08
without having to write a bazillion styles

196
00:09:15,08 --> 00:09:19,02
like we used to do when we wrote things like bootstrap.

197
00:09:19,02 --> 00:09:21,05
It's very, very exciting

198
00:09:21,05 --> 00:09:26,00
to see where this type of layout is going in CSS.

