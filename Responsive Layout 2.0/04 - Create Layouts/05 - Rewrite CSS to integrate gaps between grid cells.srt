1
00:00:00,01 --> 00:00:02,03
- [Instructor] Now that we have the initial layout

2
00:00:02,03 --> 00:00:04,09
scoped for our prototype and it's working,

3
00:00:04,09 --> 00:00:06,03
everything is good,

4
00:00:06,03 --> 00:00:10,00
now we need to add that gap in between each cell,

5
00:00:10,00 --> 00:00:13,04
so let's just start to think about this particular problem.

6
00:00:13,04 --> 00:00:15,09
We have a number here called columns.

7
00:00:15,09 --> 00:00:17,06
That's how many columns we have in our layout.

8
00:00:17,06 --> 00:00:20,08
We know there's four columns, four columns all the time,

9
00:00:20,08 --> 00:00:24,04
and each one of these cells that we're looking at here,

10
00:00:24,04 --> 00:00:27,06
we give them a width, which is how many columns they occupy,

11
00:00:27,06 --> 00:00:30,06
so each of the widths here is one column.

12
00:00:30,06 --> 00:00:32,08
These are two columns and so forth.

13
00:00:32,08 --> 00:00:34,06
Now, lets think about the gaps.

14
00:00:34,06 --> 00:00:38,04
Well, in a row like this where I have four cells,

15
00:00:38,04 --> 00:00:40,01
how many gaps do I have?

16
00:00:40,01 --> 00:00:43,08
Remember we're using the justify content of space between,

17
00:00:43,08 --> 00:00:46,06
so we shove the first cell all the way to the left,

18
00:00:46,06 --> 00:00:48,05
the last cell all the way to the right,

19
00:00:48,05 --> 00:00:51,00
we distribute the space evenly in between.

20
00:00:51,00 --> 00:00:53,03
How many gaps do we have?

21
00:00:53,03 --> 00:00:57,01
Well, in a row of four like that, we have three gaps.

22
00:00:57,01 --> 00:00:59,09
In a row where we have, say, three cells like this,

23
00:00:59,09 --> 00:01:02,06
how many gaps do we have?

24
00:01:02,06 --> 00:01:04,00
Two gaps.

25
00:01:04,00 --> 00:01:06,07
In a row like this where we have two cells,

26
00:01:06,07 --> 00:01:08,04
how many gaps do we have?

27
00:01:08,04 --> 00:01:10,00
One gap.

28
00:01:10,00 --> 00:01:13,00
In row like this where we have one giant cell,

29
00:01:13,00 --> 00:01:15,01
how many gaps do we have?

30
00:01:15,01 --> 00:01:17,02
No gaps.

31
00:01:17,02 --> 00:01:19,03
Are you detecting a pattern?

32
00:01:19,03 --> 00:01:21,06
Okay, so that can always point you to the math

33
00:01:21,06 --> 00:01:24,00
if you can detect a pattern like that.

34
00:01:24,00 --> 00:01:27,04
Let's go on ahead and just write what we just did,

35
00:01:27,04 --> 00:01:31,00
so I'm going to create a variable called gap,

36
00:01:31,00 --> 00:01:33,06
and that gap, basically, all we have

37
00:01:33,06 --> 00:01:39,02
to do is subtract the number of columns minus the width,

38
00:01:39,02 --> 00:01:43,03
so if I have four columns and the width is one

39
00:01:43,03 --> 00:01:45,04
because I have four cells lined up,

40
00:01:45,04 --> 00:01:47,09
that would be four minus one which would be three,

41
00:01:47,09 --> 00:01:52,01
multiply that by something like 1%,

42
00:01:52,01 --> 00:01:55,00
which would be 3%, that would be the space for all

43
00:01:55,00 --> 00:01:58,07
of the gaps, and then because we're using justify content

44
00:01:58,07 --> 00:02:02,04
of space between, that space would be distributed evenly,

45
00:02:02,04 --> 00:02:04,06
so let's just go ahead and write that.

46
00:02:04,06 --> 00:02:13,02
Calc of first of all the variable of columns,

47
00:02:13,02 --> 00:02:15,06
and you can put in your default values here, once again,

48
00:02:15,06 --> 00:02:17,07
if you wish, and we're going to subtract

49
00:02:17,07 --> 00:02:22,07
from that, the variable of width,

50
00:02:22,07 --> 00:02:25,02
and that's just the default value there,

51
00:02:25,02 --> 00:02:28,04
and then I'm going to multiply that whole thing by 1%.

52
00:02:28,04 --> 00:02:30,06
Now, why 1%?

53
00:02:30,06 --> 00:02:33,07
Remember earlier, when we did this math, I said each one

54
00:02:33,07 --> 00:02:36,01
of those cells on that first row there,

55
00:02:36,01 --> 00:02:40,03
they were 22% wide, which gave us a little bit

56
00:02:40,03 --> 00:02:44,01
of space in between that we could use the justify content

57
00:02:44,01 --> 00:02:47,08
and all the math worked out really, really well for that.

58
00:02:47,08 --> 00:02:49,02
Does it have to be 1%?

59
00:02:49,02 --> 00:02:52,00
No, you could absolutely, if you don't want the gap

60
00:02:52,00 --> 00:02:54,01
that big, you could make that number smaller.

61
00:02:54,01 --> 00:02:57,07
Now, half a percent or .1%, or of course,

62
00:02:57,07 --> 00:03:00,07
you could make that number larger if you want a bigger gap

63
00:03:00,07 --> 00:03:05,01
in between your cells, so that is the math of the gap,

64
00:03:05,01 --> 00:03:07,08
so now what is the flex basis.

65
00:03:07,08 --> 00:03:11,04
Well, all we have to do is what, take our initial basis

66
00:03:11,04 --> 00:03:15,09
that we calculated here, and then subtract the gap.

67
00:03:15,09 --> 00:03:18,09
That becomes the value of our flex basis.

68
00:03:18,09 --> 00:03:23,07
Ah, so let's just go on ahead and write that.

69
00:03:23,07 --> 00:03:29,02
Once again, we're going to calc our variable of initial basis,

70
00:03:29,02 --> 00:03:37,04
and we're going to subtract from that our variable of the gap.

71
00:03:37,04 --> 00:03:39,05
Boom, that's all we have to do,

72
00:03:39,05 --> 00:03:41,09
and suddenly, the gap shows up.

73
00:03:41,09 --> 00:03:43,09
It shows up on all of our cells.

74
00:03:43,09 --> 00:03:46,04
Look at that, isn't that amazing

75
00:03:46,04 --> 00:03:48,01
because now we don't have to go through

76
00:03:48,01 --> 00:03:50,03
and edit 5,000 different things to make sure

77
00:03:50,03 --> 00:03:51,08
the gap's in all the same place,

78
00:03:51,08 --> 00:03:54,04
and then we have to worry about the media queries, right?

79
00:03:54,04 --> 00:03:57,03
Oh, because the media queries, the math's already done

80
00:03:57,03 --> 00:03:59,05
for us, look at that.

81
00:03:59,05 --> 00:04:02,06
This is fabulous, right?

82
00:04:02,06 --> 00:04:05,02
Okay, and the everything stacks up so nice

83
00:04:05,02 --> 00:04:07,05
once we get down here to modal dimensions,

84
00:04:07,05 --> 00:04:10,04
so this is truly a beautiful thing

85
00:04:10,04 --> 00:04:14,05
that we have going on here, this little formula right here.

86
00:04:14,05 --> 00:04:17,07
This is super flexible, you can apply this

87
00:04:17,07 --> 00:04:19,06
to a whole bunch of different places

88
00:04:19,06 --> 00:04:23,00
and, in fact, you can use it in more than one place

89
00:04:23,00 --> 00:04:25,02
in your document if you wanted to do so,

90
00:04:25,02 --> 00:04:28,01
so if you had some other layout problems,

91
00:04:28,01 --> 00:04:30,00
maybe you could actually reuse this little bit

92
00:04:30,00 --> 00:04:33,01
of code and apply it to those other layout problems.

93
00:04:33,01 --> 00:04:35,00
Something to think about.

