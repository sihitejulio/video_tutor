1
00:00:00,02 --> 00:00:06,05
(upbeat music)

2
00:00:06,05 --> 00:00:09,04
- [Instructor] So, how'd you do with that challenge?

3
00:00:09,04 --> 00:00:11,04
This is my particular solution.

4
00:00:11,04 --> 00:00:13,04
As before, we don't really need to touch

5
00:00:13,04 --> 00:00:16,05
the HTML at all, and I actually didn't touch

6
00:00:16,05 --> 00:00:20,00
any of the code that we wrote already either.

7
00:00:20,00 --> 00:00:23,01
So what I've done here is simply gone ahead

8
00:00:23,01 --> 00:00:25,03
and worked with a different type scale

9
00:00:25,03 --> 00:00:27,00
at mobile dimensions.

10
00:00:27,00 --> 00:00:31,04
1.125 is the major second,

11
00:00:31,04 --> 00:00:33,07
and if you take a look here,

12
00:00:33,07 --> 00:00:36,06
all of that math is all still the same.

13
00:00:36,06 --> 00:00:38,06
Then down here in my media queries,

14
00:00:38,06 --> 00:00:42,03
all I did was change the factor for my type scale.

15
00:00:42,03 --> 00:00:44,03
So if you watch my type over here,

16
00:00:44,03 --> 00:00:46,08
you see we start at mobile dimensions

17
00:00:46,08 --> 00:00:50,01
with not a lot of difference between those type sizes,

18
00:00:50,01 --> 00:00:54,00
and as I hit my various breakpoints,

19
00:00:54,00 --> 00:00:56,07
those type sizes will increase.

20
00:00:56,07 --> 00:00:59,01
Now the base is always the same.

21
00:00:59,01 --> 00:01:04,01
The base font size was set to be 16 pixels or one m.

22
00:01:04,01 --> 00:01:07,08
And in fact, in the math that we have here,

23
00:01:07,08 --> 00:01:11,04
that's just currently set to one m. here,

24
00:01:11,04 --> 00:01:15,01
and you could have changed that to a different base size.

25
00:01:15,01 --> 00:01:16,09
For example, you could make it bigger

26
00:01:16,09 --> 00:01:19,00
by making it 1.2 m.

27
00:01:19,00 --> 00:01:20,06
That would scale everything

28
00:01:20,06 --> 00:01:23,06
in your particular layout proportionately.

29
00:01:23,06 --> 00:01:26,02
You could have certainly made that smaller.

30
00:01:26,02 --> 00:01:30,00
So if we start with a smaller type size,

31
00:01:30,00 --> 00:01:34,03
we obviously as we change our dimensions here,

32
00:01:34,03 --> 00:01:37,09
the type is going to change proportionately as well

33
00:01:37,09 --> 00:01:41,00
as it changes through the various scaling factors,

34
00:01:41,00 --> 00:01:43,05
in addition to changing that base font size.

35
00:01:43,05 --> 00:01:46,07
So, those were two things that you could have changed.

36
00:01:46,07 --> 00:01:49,05
Chances are, you shouldn't really have changed our math.

37
00:01:49,05 --> 00:01:52,04
It will keep the type scale here in place.

38
00:01:52,04 --> 00:01:54,00
So probably one of those two things are

39
00:01:54,00 --> 00:01:55,07
what you changed as part of the challenge,

40
00:01:55,07 --> 00:01:59,00
and both of those would be absolutely great choices.

