1
00:00:00,00 --> 00:00:02,00
- [Female Instructor] Now that we have our code working

2
00:00:02,00 --> 00:00:03,06
with regular CSS,

3
00:00:03,06 --> 00:00:07,04
let's refactor it using responsive design 2.0,

4
00:00:07,04 --> 00:00:09,08
with custom properties and with calc.

5
00:00:09,08 --> 00:00:11,07
We're going to start with what's in the

6
00:00:11,07 --> 00:00:13,03
mobile display first.

7
00:00:13,03 --> 00:00:15,05
We're going to put all of our logic right there

8
00:00:15,05 --> 00:00:16,09
in the mobile display

9
00:00:16,09 --> 00:00:20,00
and we'll just leave those declarations

10
00:00:20,00 --> 00:00:22,06
of flex basis further down here in the media queries,

11
00:00:22,06 --> 00:00:24,03
just for the moment.

12
00:00:24,03 --> 00:00:27,06
Once we get that formula that we're going to put in

13
00:00:27,06 --> 00:00:30,02
up there and correct at the beginning,

14
00:00:30,02 --> 00:00:31,09
then we'll go ahead and make changes in

15
00:00:31,09 --> 00:00:33,03
the media queries.

16
00:00:33,03 --> 00:00:35,01
So, first of all,

17
00:00:35,01 --> 00:00:38,00
what we want to do is we want to calculate

18
00:00:38,00 --> 00:00:41,02
the flex basis of these cells

19
00:00:41,02 --> 00:00:43,06
and that way we have to come up with that number

20
00:00:43,06 --> 00:00:44,08
is basically,

21
00:00:44,08 --> 00:00:47,07
how do we know that certain cells

22
00:00:47,07 --> 00:00:50,00
are that particular width?

23
00:00:50,00 --> 00:00:51,07
So let's talk about that first row where

24
00:00:51,07 --> 00:00:53,04
we have four boxes.

25
00:00:53,04 --> 00:00:55,02
We originally started by saying each of those

26
00:00:55,02 --> 00:00:56,08
boxes was 25 percent.

27
00:00:56,08 --> 00:00:59,04
What was the math that we did in order to figure that out?

28
00:00:59,04 --> 00:01:02,00
Well we said we have four columns,

29
00:01:02,00 --> 00:01:06,04
and each of those columns is going to have one box.

30
00:01:06,04 --> 00:01:09,03
So one divided by four is point two five.

31
00:01:09,03 --> 00:01:12,04
And then we multiplied that by a hundred percent

32
00:01:12,04 --> 00:01:15,07
to give us 25 percent.

33
00:01:15,07 --> 00:01:18,01
So let's go ahead and just write some code

34
00:01:18,01 --> 00:01:20,04
that would actually reflect that.

35
00:01:20,04 --> 00:01:23,08
And so here in our CSS,

36
00:01:23,08 --> 00:01:25,03
I'm going to start with the row,

37
00:01:25,03 --> 00:01:26,08
just to show you that you can do this,

38
00:01:26,08 --> 00:01:30,01
now I'm going to make a couple of variable

39
00:01:30,01 --> 00:01:31,02
declarations here.

40
00:01:31,02 --> 00:01:34,00
I could put them in the root declaration,

41
00:01:34,00 --> 00:01:36,01
but just to show you that it's possible to scope

42
00:01:36,01 --> 00:01:38,02
your variables and you can declare these anywhere

43
00:01:38,02 --> 00:01:39,00
in your code,

44
00:01:39,00 --> 00:01:40,04
I'm going to go ahead and put these a little bit

45
00:01:40,04 --> 00:01:42,03
closer to where we're using them.

46
00:01:42,03 --> 00:01:44,06
So here on the row I'm going to declare that

47
00:01:44,06 --> 00:01:47,04
I have a variable called columns

48
00:01:47,04 --> 00:01:50,02
and I'm going to give it a value of four.

49
00:01:50,02 --> 00:01:51,01
In other words,

50
00:01:51,01 --> 00:01:52,09
there are four columns in our design.

51
00:01:52,09 --> 00:01:54,05
So far so good?

52
00:01:54,05 --> 00:01:56,02
And then here,

53
00:01:56,02 --> 00:01:59,00
in my class equals call,

54
00:01:59,00 --> 00:02:01,06
in other words all of thy columns,

55
00:02:01,06 --> 00:02:03,08
I'm going to establish another variable

56
00:02:03,08 --> 00:02:05,04
I'll just call width.

57
00:02:05,04 --> 00:02:09,08
So these are the width of each of the boxes,

58
00:02:09,08 --> 00:02:12,02
how many columns is it going to occupy?

59
00:02:12,02 --> 00:02:13,06
While at mobile dimensions,

60
00:02:13,06 --> 00:02:18,01
each one of those boxes is going to occupy four columns.

61
00:02:18,01 --> 00:02:19,00
Okay.

62
00:02:19,00 --> 00:02:20,07
And so then

63
00:02:20,07 --> 00:02:25,09
we'll simply say a value of a variable called initial basis

64
00:02:25,09 --> 00:02:28,05
because this is the value of the initial flex basis

65
00:02:28,05 --> 00:02:29,09
we're going to calculate.

66
00:02:29,09 --> 00:02:32,03
We'll say that this is calc

67
00:02:32,03 --> 00:02:37,07
and we're going to calculate the variable of width,

68
00:02:37,07 --> 00:02:44,08
and we're going to divide that by the variable of columns,

69
00:02:44,08 --> 00:02:49,00
and then we're going to multiply that by a hundred percent.

70
00:02:49,00 --> 00:02:50,08
So in this first example,

71
00:02:50,08 --> 00:02:52,05
this is pretty straightforward.

72
00:02:52,05 --> 00:02:53,08
The width would be four,

73
00:02:53,08 --> 00:02:54,09
the columns would be four,

74
00:02:54,09 --> 00:02:56,03
four divided by four is one,

75
00:02:56,03 --> 00:02:58,00
times a hundred percent is?

76
00:02:58,00 --> 00:03:00,01
A hundred percent.

77
00:03:00,01 --> 00:03:01,08
So that's going to calculate the value of

78
00:03:01,08 --> 00:03:03,04
the initial basis variable,

79
00:03:03,04 --> 00:03:05,06
but we're not actually declaring it yet.

80
00:03:05,06 --> 00:03:06,06
Where would we declare it?

81
00:03:06,06 --> 00:03:09,06
Right here where it says flex basis of a hundred percent.

82
00:03:09,06 --> 00:03:14,04
All we'd have to do now is say a variable of

83
00:03:14,04 --> 00:03:17,02
initial basis.

84
00:03:17,02 --> 00:03:18,09
Just like that.

85
00:03:18,09 --> 00:03:21,07
Now some of you might be bothered by the fact that

86
00:03:21,07 --> 00:03:23,06
I'm calling for variables here,

87
00:03:23,06 --> 00:03:25,06
that don't have a default value.

88
00:03:25,06 --> 00:03:26,04
In other words,

89
00:03:26,04 --> 00:03:29,03
what would happen if I tried to run that math

90
00:03:29,03 --> 00:03:31,02
and those variables were,

91
00:03:31,02 --> 00:03:32,01
for whatever reason,

92
00:03:32,01 --> 00:03:33,04
they were not defined?

93
00:03:33,04 --> 00:03:35,06
Well it's possible to have that default value

94
00:03:35,06 --> 00:03:37,05
available to us,

95
00:03:37,05 --> 00:03:39,04
so we could say for example,

96
00:03:39,04 --> 00:03:41,06
that the default value for width is whatever

97
00:03:41,06 --> 00:03:42,05
you want it to be,

98
00:03:42,05 --> 00:03:43,09
let's say it's one.

99
00:03:43,09 --> 00:03:45,02
And the value for columns,

100
00:03:45,02 --> 00:03:47,01
the default value will be four.

101
00:03:47,01 --> 00:03:49,06
Maybe you've changed it somewhere along the way.

102
00:03:49,06 --> 00:03:52,00
So just by putting a comma with a number after it,

103
00:03:52,00 --> 00:03:54,02
these are not actually going to be used

104
00:03:54,02 --> 00:03:55,06
in the calculations,

105
00:03:55,06 --> 00:03:58,05
but they're here in case for whatever reason,

106
00:03:58,05 --> 00:03:59,09
those variables weren't defined,

107
00:03:59,09 --> 00:04:03,07
those are the default values that they'll use.

108
00:04:03,07 --> 00:04:07,02
Okay so now let's move on to these media queries.

109
00:04:07,02 --> 00:04:10,02
And obviously we're not looking at this gap just yet,

110
00:04:10,02 --> 00:04:12,05
we're just going to start with the same basic math that

111
00:04:12,05 --> 00:04:13,04
we had before,

112
00:04:13,04 --> 00:04:14,09
where we said it was 25 percent,

113
00:04:14,09 --> 00:04:15,08
50 percent,

114
00:04:15,08 --> 00:04:16,06
75 percent,

115
00:04:16,06 --> 00:04:17,07
a hundred percent.

116
00:04:17,07 --> 00:04:20,07
I'll go back and factor in the gap in another video.

117
00:04:20,07 --> 00:04:25,09
So instead of then declaring here in our media queries

118
00:04:25,09 --> 00:04:29,03
that the flex basis is 48 percent or a hundred percent,

119
00:04:29,03 --> 00:04:31,09
what could we do instead?

120
00:04:31,09 --> 00:04:34,06
Well all we'd have to do here is simply

121
00:04:34,06 --> 00:04:37,05
re-declare the value of width.

122
00:04:37,05 --> 00:04:40,06
So here the value of width is just going to be two.

123
00:04:40,06 --> 00:04:42,04
That's pretty much it.

124
00:04:42,04 --> 00:04:45,02
All of these other values here,

125
00:04:45,02 --> 00:04:49,02
they currently have that flex basis of a hundred percent,

126
00:04:49,02 --> 00:04:51,08
we could certainly go on ahead and re-declare this

127
00:04:51,08 --> 00:04:53,08
with a width of four,

128
00:04:53,08 --> 00:04:55,07
but in fact,

129
00:04:55,07 --> 00:04:58,06
the width is already four going into the media query,

130
00:04:58,06 --> 00:05:01,01
so this style here actually isn't even needed at all.

131
00:05:01,01 --> 00:05:03,02
You could just go ahead and get rid of that

132
00:05:03,02 --> 00:05:05,04
and it will carry on into this media query.

133
00:05:05,04 --> 00:05:07,06
Only these two styles,

134
00:05:07,06 --> 00:05:08,05
the call one two

135
00:05:08,05 --> 00:05:09,06
and the call two,

136
00:05:09,06 --> 00:05:11,05
will actually change,

137
00:05:11,05 --> 00:05:14,08
and the width value needs to change with that.

138
00:05:14,08 --> 00:05:18,08
Then as we move into our desktop media query,

139
00:05:18,08 --> 00:05:20,00
and by the way,

140
00:05:20,00 --> 00:05:21,09
let's just check that tablet here real quick.

141
00:05:21,09 --> 00:05:24,08
So if I go to tablet dimensions,

142
00:05:24,08 --> 00:05:26,04
sure enough,

143
00:05:26,04 --> 00:05:28,08
that looks pretty good.

144
00:05:28,08 --> 00:05:31,06
That's all exactly what we would expect it to be.

145
00:05:31,06 --> 00:05:33,04
Obviously we're missing the gap,

146
00:05:33,04 --> 00:05:35,04
but that's okay.

147
00:05:35,04 --> 00:05:39,04
And there's all of the values stacked on top of each other,

148
00:05:39,04 --> 00:05:41,06
there at mobile dimensions.

149
00:05:41,06 --> 00:05:45,03
So, so far it's looking pretty good.

150
00:05:45,03 --> 00:05:46,05
Now let's go to desktop.

151
00:05:46,05 --> 00:05:49,01
We still have a gap here because in some cases

152
00:05:49,01 --> 00:05:51,08
we're working with these hard values,

153
00:05:51,08 --> 00:05:53,09
that we've declared here for flex basis.

154
00:05:53,09 --> 00:05:58,07
So now let's go through and manipulate these.

155
00:05:58,07 --> 00:06:02,09
So instead of declaring a flex basis of 22 percent here,

156
00:06:02,09 --> 00:06:04,07
for our first declaration,

157
00:06:04,07 --> 00:06:09,02
we'll simply say that we have a width of one.

158
00:06:09,02 --> 00:06:11,08
Here for the column two four,

159
00:06:11,08 --> 00:06:14,07
up here the width of two is going to carry into our

160
00:06:14,07 --> 00:06:16,00
desktop media query,

161
00:06:16,00 --> 00:06:18,08
so here all we have to do is just say that this width

162
00:06:18,08 --> 00:06:21,04
is two.

163
00:06:21,04 --> 00:06:22,08
For column three,

164
00:06:22,08 --> 00:06:25,01
that's actually carrying down from the four,

165
00:06:25,01 --> 00:06:26,06
from the mobile.

166
00:06:26,06 --> 00:06:28,06
Not re-declared here in the tablet,

167
00:06:28,06 --> 00:06:30,01
so we carry it down here,

168
00:06:30,01 --> 00:06:33,04
so we need to change its value.

169
00:06:33,04 --> 00:06:38,00
And the width will be three here.

170
00:06:38,00 --> 00:06:39,04
And these order properties,

171
00:06:39,04 --> 00:06:42,00
we can just leave them alone for the moment.

172
00:06:42,00 --> 00:06:44,05
With order two one and three.

173
00:06:44,05 --> 00:06:45,08
That's just fine.

174
00:06:45,08 --> 00:06:47,02
If you wanted to go through

175
00:06:47,02 --> 00:06:49,05
and you wanted to make a variable for each one

176
00:06:49,05 --> 00:06:50,06
of these order values

177
00:06:50,06 --> 00:06:52,07
and you wanted to put that outside the media query

178
00:06:52,07 --> 00:06:54,03
and change the values here,

179
00:06:54,03 --> 00:06:55,09
that's certainly doable,

180
00:06:55,09 --> 00:06:58,01
but I don't really feel like I'm getting a lot of

181
00:06:58,01 --> 00:06:59,09
advantage by doing that here,

182
00:06:59,09 --> 00:07:02,05
based on the fact that this is just a very simple

183
00:07:02,05 --> 00:07:05,01
declaration with one property value pair.

184
00:07:05,01 --> 00:07:07,03
I don't think I really get a lot of mileage out

185
00:07:07,03 --> 00:07:08,01
of doing that,

186
00:07:08,01 --> 00:07:12,03
but you can do it if you wish.

187
00:07:12,03 --> 00:07:14,05
So now at this point,

188
00:07:14,05 --> 00:07:18,04
we are back to where we were a couple of videos ago.

189
00:07:18,04 --> 00:07:21,07
We have all of these

190
00:07:21,07 --> 00:07:24,04
boxes that are stacked so nicely on top of

191
00:07:24,04 --> 00:07:25,03
each other,

192
00:07:25,03 --> 00:07:29,00
things are looking exactly the way we want them to look,

193
00:07:29,00 --> 00:07:31,08
with a fraction of the code of what we had before.

194
00:07:31,08 --> 00:07:34,07
So rather than doing all of that complicated math,

195
00:07:34,07 --> 00:07:37,06
now I just have a formula up here on the top

196
00:07:37,06 --> 00:07:38,06
that's doing it for me.

197
00:07:38,06 --> 00:07:39,04
I'm taking the width,

198
00:07:39,04 --> 00:07:40,05
dividing it by the columns,

199
00:07:40,05 --> 00:07:41,08
multiplying by a hundred percent,

200
00:07:41,08 --> 00:07:45,03
and I get an initial layout on the page.

201
00:07:45,03 --> 00:07:47,02
So there's just one thing that we're missing now,

202
00:07:47,02 --> 00:07:48,09
which is the gaps between the cells,

203
00:07:48,09 --> 00:07:51,06
and in the next video I'll show you how to put those

204
00:07:51,06 --> 00:07:53,00
in place.

