1
00:00:00,05 --> 00:00:03,02
- [Instructor] Now that we have our HTML all marked up

2
00:00:03,02 --> 00:00:06,01
with our wrapper and rows and our columns,

3
00:00:06,01 --> 00:00:08,08
let's go ahead and start writing some Flexbox

4
00:00:08,08 --> 00:00:11,06
for a desktop-based layout.

5
00:00:11,06 --> 00:00:12,09
So first of all,

6
00:00:12,09 --> 00:00:21,00
I'm going to start by putting in some colors here for col one.

7
00:00:21,00 --> 00:00:25,04
Let's put in a background color of a lovely shade

8
00:00:25,04 --> 00:00:30,00
of pale yellow.

9
00:00:30,00 --> 00:00:33,04
And then for col two,

10
00:00:33,04 --> 00:00:42,02
I'll put in a nice purple.

11
00:00:42,02 --> 00:00:48,01
For col three,

12
00:00:48,01 --> 00:00:51,09
I'll put in this blue here.

13
00:00:51,09 --> 00:00:59,02
And then for col four,

14
00:00:59,02 --> 00:01:00,01
there we go,

15
00:01:00,01 --> 00:01:02,06
so we'll put in this nice purple here for col four.

16
00:01:02,06 --> 00:01:03,04
So there we go.

17
00:01:03,04 --> 00:01:05,06
We've just put in some background colors

18
00:01:05,06 --> 00:01:07,02
so that we can see what we're doing.

19
00:01:07,02 --> 00:01:10,09
Anytime you're working with layout problems like this,

20
00:01:10,09 --> 00:01:12,00
putting in some borders,

21
00:01:12,00 --> 00:01:13,09
putting in some background colors

22
00:01:13,09 --> 00:01:15,03
is always super helpful

23
00:01:15,03 --> 00:01:17,08
and if you pick ugly colors like I did,

24
00:01:17,08 --> 00:01:20,00
well, you won't forget to take them out

25
00:01:20,00 --> 00:01:22,07
when you decide to put this code into production.

26
00:01:22,07 --> 00:01:25,09
So that's the whole purpose of those colors there.

27
00:01:25,09 --> 00:01:28,00
And now with that in place,

28
00:01:28,00 --> 00:01:31,09
let's go on ahead and start in on the Flexbox.

29
00:01:31,09 --> 00:01:34,07
So just before line 16 here,

30
00:01:34,07 --> 00:01:37,05
I've got a border box declaration here.

31
00:01:37,05 --> 00:01:39,07
I'm using my border box sizing

32
00:01:39,07 --> 00:01:42,03
instead of the default content box sizing.

33
00:01:42,03 --> 00:01:44,03
Remember, border box sizing says

34
00:01:44,03 --> 00:01:48,08
that we're going to consider the width of something.

35
00:01:48,08 --> 00:01:52,04
We'll include the padding and the border in that width

36
00:01:52,04 --> 00:01:54,02
as opposed to the content box model,

37
00:01:54,02 --> 00:01:58,03
which says that the width is determined only by the content

38
00:01:58,03 --> 00:02:00,00
and that the padding, border,

39
00:02:00,00 --> 00:02:02,00
and margin are a separate calculation.

40
00:02:02,00 --> 00:02:04,04
So that's what this little bit of code is here.

41
00:02:04,04 --> 00:02:06,06
As you can see, there is an article here by Paul Irish

42
00:02:06,06 --> 00:02:08,06
where you can go read more about that.

43
00:02:08,06 --> 00:02:10,08
Down here, we just have some general styling

44
00:02:10,08 --> 00:02:12,09
for some other things.

45
00:02:12,09 --> 00:02:13,07
You're welcome to take a look at that,

46
00:02:13,07 --> 00:02:15,04
but there, we won't be touching that code.

47
00:02:15,04 --> 00:02:18,04
That's just basic styling for this code pen.

48
00:02:18,04 --> 00:02:22,01
So now I'm going to go ahead and set up my row.

49
00:02:22,01 --> 00:02:24,00
And inside of this, we'll go ahead and say,

50
00:02:24,00 --> 00:02:28,07
first of all, of course, display flex.

51
00:02:28,07 --> 00:02:30,01
So just by doing that,

52
00:02:30,01 --> 00:02:31,07
that'll be enough to go on ahead

53
00:02:31,07 --> 00:02:35,05
and put our content together into the rows that we want.

54
00:02:35,05 --> 00:02:38,07
They're all the wrong widths and the layout is a mess,

55
00:02:38,07 --> 00:02:41,00
but everything is at least on the right row

56
00:02:41,00 --> 00:02:43,06
just by saying display flex.

57
00:02:43,06 --> 00:02:46,04
So then we're going to say our flex flow is row wrap

58
00:02:46,04 --> 00:02:50,07
because of course, we want things to be laid out in a row.

59
00:02:50,07 --> 00:02:53,05
We want things to wrap onto the next line

60
00:02:53,05 --> 00:02:55,09
if there's not room on them in one row.

61
00:02:55,09 --> 00:02:59,07
And that will flip this over into a stackable layout.

62
00:02:59,07 --> 00:03:01,00
Why is that?

63
00:03:01,00 --> 00:03:05,00
Well, each of these items doesn't have a flex basis yet,

64
00:03:05,00 --> 00:03:06,07
so the width of each box

65
00:03:06,07 --> 00:03:08,05
is the width of its content,

66
00:03:08,05 --> 00:03:11,03
and then it just sort inherently means

67
00:03:11,03 --> 00:03:14,01
because of the text and the pictures that we've chosen here

68
00:03:14,01 --> 00:03:16,06
pretty much every box winds up on its own line

69
00:03:16,06 --> 00:03:17,06
for the moment,

70
00:03:17,06 --> 00:03:19,08
so that's what's going on there.

71
00:03:19,08 --> 00:03:24,03
And then I'm going to say my justify content

72
00:03:24,03 --> 00:03:27,02
will be space between.

73
00:03:27,02 --> 00:03:29,06
And so space between,

74
00:03:29,06 --> 00:03:31,02
remember there's a whole bunch of values here

75
00:03:31,02 --> 00:03:32,03
for justify content,

76
00:03:32,03 --> 00:03:35,04
but space between in particular says we're going to take all

77
00:03:35,04 --> 00:03:36,03
of these boxes.

78
00:03:36,03 --> 00:03:39,02
We're going to shove them all the way to the edges

79
00:03:39,02 --> 00:03:42,07
of the screen and we're going to take any extra space

80
00:03:42,07 --> 00:03:44,03
that happens to be available

81
00:03:44,03 --> 00:03:46,04
and we're going to turn that into the space

82
00:03:46,04 --> 00:03:48,07
in between each of these boxes.

83
00:03:48,07 --> 00:03:49,07
Okay, great.

84
00:03:49,07 --> 00:03:52,01
That'll become super relevant later on,

85
00:03:52,01 --> 00:03:54,07
although we don't see it in action here just yet.

86
00:03:54,07 --> 00:03:56,00
So now what we need to do

87
00:03:56,00 --> 00:03:59,07
is simply take col one

88
00:03:59,07 --> 00:04:06,03
and we can start by saying its flex basis will be 25%.

89
00:04:06,03 --> 00:04:09,01
So this is just sort of what we had talked about earlier

90
00:04:09,01 --> 00:04:11,08
in some of our earlier examples.

91
00:04:11,08 --> 00:04:14,05
We're just going to go ahead and set this up,

92
00:04:14,05 --> 00:04:17,01
sort of rough it out a little bit.

93
00:04:17,01 --> 00:04:22,00
Our col two is going to have a flex basis of 50%,

94
00:04:22,00 --> 00:04:24,04
so that'll put those two boxes next to each other.

95
00:04:24,04 --> 00:04:28,00
So far, so good.

96
00:04:28,00 --> 00:04:31,03
Shockingly, our col three will be, what?

97
00:04:31,03 --> 00:04:35,05
75%.

98
00:04:35,05 --> 00:04:37,01
There we go there,

99
00:04:37,01 --> 00:04:43,04
and then our col four

100
00:04:43,04 --> 00:04:46,03
will be 100%.

101
00:04:46,03 --> 00:04:47,02
Okay?

102
00:04:47,02 --> 00:04:50,06
So good, now things are of the same width.

103
00:04:50,06 --> 00:04:51,04
They line up.

104
00:04:51,04 --> 00:04:53,02
We still aren't quite pretty yet,

105
00:04:53,02 --> 00:04:57,04
but it's a pretty reasonable working prototype.

106
00:04:57,04 --> 00:04:59,08
All right, now at the moment,

107
00:04:59,08 --> 00:05:03,00
because my flex basis is occupying the full width

108
00:05:03,00 --> 00:05:05,04
of these rows, everything runs into each other.

109
00:05:05,04 --> 00:05:07,00
We'd like to have a little bit of space

110
00:05:07,00 --> 00:05:09,08
in between each one of these boxes

111
00:05:09,08 --> 00:05:13,00
so we can do some quick math to make that happen.

112
00:05:13,00 --> 00:05:17,02
If we have a col three on a row,

113
00:05:17,02 --> 00:05:19,03
we're only going to have one gap.

114
00:05:19,03 --> 00:05:22,04
So let's call that 1%.

115
00:05:22,04 --> 00:05:24,03
Obviously, if we have a col four here,

116
00:05:24,03 --> 00:05:25,07
we have no gaps

117
00:05:25,07 --> 00:05:27,08
and there's no need to make a change there.

118
00:05:27,08 --> 00:05:29,07
But here, we'd have one gap.

119
00:05:29,07 --> 00:05:31,05
Let's call that 1%.

120
00:05:31,05 --> 00:05:35,06
If we have two boxes next to each other like this,

121
00:05:35,06 --> 00:05:38,04
we have potentially two gaps there,

122
00:05:38,04 --> 00:05:40,03
so let's call that 2%

123
00:05:40,03 --> 00:05:43,05
and here with these three boxes on the top,

124
00:05:43,05 --> 00:05:44,08
let's say we have three gaps.

125
00:05:44,08 --> 00:05:46,04
Let's call it 3%.

126
00:05:46,04 --> 00:05:49,06
So I'm going to modify my values here to reflect that.

127
00:05:49,06 --> 00:05:52,06
We'll make each of these boxes 22%

128
00:05:52,06 --> 00:05:55,05
to give it a little bit of gap in between each of those gaps

129
00:05:55,05 --> 00:05:57,00
should be 1%,

130
00:05:57,00 --> 00:06:00,01
because we've shoved everything to the edges of the row

131
00:06:00,01 --> 00:06:04,07
and we're putting the space in between the boxes.

132
00:06:04,07 --> 00:06:08,05
So here on col two, we'll make that 48%.

133
00:06:08,05 --> 00:06:11,04
For col three, we'll make it 74%,

134
00:06:11,04 --> 00:06:13,07
and we'll leave col four alone at 100%,

135
00:06:13,07 --> 00:06:17,06
so you can see that our gaps are lining up nicely here.

136
00:06:17,06 --> 00:06:23,05
That math is working out really, really well for us.

137
00:06:23,05 --> 00:06:26,01
Now roughing things out this way means

138
00:06:26,01 --> 00:06:27,07
that we're going to have flexible space

139
00:06:27,07 --> 00:06:30,06
that still lines up with the cells above

140
00:06:30,06 --> 00:06:32,02
and below that space.

141
00:06:32,02 --> 00:06:33,08
This is the heart of Flexbox.

142
00:06:33,08 --> 00:06:36,08
So rather than using margin

143
00:06:36,08 --> 00:06:39,04
and spelling out a margin for each of these,

144
00:06:39,04 --> 00:06:42,00
margin would not be necessarily flexible.

145
00:06:42,00 --> 00:06:44,09
Here, we're letting Flexbox manage

146
00:06:44,09 --> 00:06:48,02
the flex basis property rather than the width

147
00:06:48,02 --> 00:06:50,06
and Flexbox is now managing the gap

148
00:06:50,06 --> 00:06:54,04
rather than managing that via margin.

149
00:06:54,04 --> 00:06:56,08
So now that this is working with a desktop layout,

150
00:06:56,08 --> 00:06:58,03
let's add some media queries

151
00:06:58,03 --> 00:07:01,00
and get this working on tablet and mobile.

