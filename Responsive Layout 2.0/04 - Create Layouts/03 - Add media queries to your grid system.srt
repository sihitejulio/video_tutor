1
00:00:00,05 --> 00:00:02,04
- [Instructor] Now that we have our prototype working

2
00:00:02,04 --> 00:00:04,06
in a desktop layout,

3
00:00:04,06 --> 00:00:08,03
let's rework this with mobile and tablet configuration.

4
00:00:08,03 --> 00:00:10,05
So first of all,

5
00:00:10,05 --> 00:00:12,08
we are going to take these styles here

6
00:00:12,08 --> 00:00:15,02
for column one to column four,

7
00:00:15,02 --> 00:00:16,02
and we'll just go on ahead

8
00:00:16,02 --> 00:00:18,01
and put these in a media query

9
00:00:18,01 --> 00:00:22,00
at media min width

10
00:00:22,00 --> 00:00:29,05
of, say, 768 pixels.

11
00:00:29,05 --> 00:00:33,09
And we'll go ahead and open up the set of curly braces.

12
00:00:33,09 --> 00:00:40,00
We'll close them down here after col four.

13
00:00:40,00 --> 00:00:42,09
Indent everybody to make it pretty,

14
00:00:42,09 --> 00:00:45,01
and so that's ultimately where we're going to be.

15
00:00:45,01 --> 00:00:48,09
So if we make our screen size over here narrower than that,

16
00:00:48,09 --> 00:00:50,05
what happens,

17
00:00:50,05 --> 00:00:53,01
we're back to that weird, everything just sort of stacks

18
00:00:53,01 --> 00:00:54,07
on top of each other, right?

19
00:00:54,07 --> 00:00:58,09
We'll have no flex basis of any particular width, why?

20
00:00:58,09 --> 00:01:00,08
Because the only place we're currently

21
00:01:00,08 --> 00:01:02,06
declaring our flex basis,

22
00:01:02,06 --> 00:01:05,00
right here inside of this media query.

23
00:01:05,00 --> 00:01:07,02
So let's set up a second media query for,

24
00:01:07,02 --> 00:01:10,03
let's say, tablet dimensions.

25
00:01:10,03 --> 00:01:12,05
I'm just going to copy what I already have here

26
00:01:12,05 --> 00:01:13,09
and paste it

27
00:01:13,09 --> 00:01:16,05
and I'm going to change my value in the media query

28
00:01:16,05 --> 00:01:23,04
from 768 pixels to let's say, 480 pixels.

29
00:01:23,04 --> 00:01:25,04
Okay, so let's just go on ahead

30
00:01:25,04 --> 00:01:28,07
and see what happens when we get down there.

31
00:01:28,07 --> 00:01:32,01
Well, things are looking kind of skinny.

32
00:01:32,01 --> 00:01:34,07
We'd probably like things to wrap around a little bit.

33
00:01:34,07 --> 00:01:37,02
This is, I mean, it sort of vaguely works,

34
00:01:37,02 --> 00:01:39,00
but probably there's more that we can do here

35
00:01:39,00 --> 00:01:40,04
to make this beautiful,

36
00:01:40,04 --> 00:01:44,06
so let's see about changing some of these numbers.

37
00:01:44,06 --> 00:01:47,08
For example, here with these column ones,

38
00:01:47,08 --> 00:01:50,01
maybe what I'd like is for each one of those

39
00:01:50,01 --> 00:01:52,03
to take up two columns at this dimension.

40
00:01:52,03 --> 00:01:54,05
So in other words, we'd have a row

41
00:01:54,05 --> 00:01:57,08
with two boxes on it and a second row with two boxes on it,

42
00:01:57,08 --> 00:01:59,04
and I could do that pretty easily

43
00:01:59,04 --> 00:02:04,01
just by changing my flex basis value here to say 48%.

44
00:02:04,01 --> 00:02:05,07
Hey, that worked great.

45
00:02:05,07 --> 00:02:08,07
So now I've got two boxes on top of two boxes,

46
00:02:08,07 --> 00:02:10,03
that looks good.

47
00:02:10,03 --> 00:02:12,07
These look fine.

48
00:02:12,07 --> 00:02:14,00
And but oops,

49
00:02:14,00 --> 00:02:16,02
this is not working here, is it?

50
00:02:16,02 --> 00:02:19,07
So here, I've got a flex basis of 48%

51
00:02:19,07 --> 00:02:23,06
and this one has still got a flex basis of 74%,

52
00:02:23,06 --> 00:02:25,08
so now I've got a situation

53
00:02:25,08 --> 00:02:28,08
where I have two different behaviors for my column one.

54
00:02:28,08 --> 00:02:31,05
Got one behavior, it's going to look like this

55
00:02:31,05 --> 00:02:33,07
where it's going to go from spanning one column

56
00:02:33,07 --> 00:02:35,05
to spanning two columns.

57
00:02:35,05 --> 00:02:37,01
Down here, I've got a situation

58
00:02:37,01 --> 00:02:39,03
where I really want this one to span all the way

59
00:02:39,03 --> 00:02:41,06
across 100%,

60
00:02:41,06 --> 00:02:44,01
and so what I'm going to need are some more classes

61
00:02:44,01 --> 00:02:45,08
to make that happen.

62
00:02:45,08 --> 00:02:49,06
So let's go on ahead and drop those in real quick.

63
00:02:49,06 --> 00:02:52,01
Up here in our HTML,

64
00:02:52,01 --> 00:02:54,07
I'm going to scroll down here to line 46

65
00:02:54,07 --> 00:02:57,02
and I'm going to change the name of this one here

66
00:02:57,02 --> 00:03:00,04
to column one hyphen four

67
00:03:00,04 --> 00:03:02,03
because I want it to go from one column wide

68
00:03:02,03 --> 00:03:04,02
to four columns wide,

69
00:03:04,02 --> 00:03:07,00
and the ones that I had up here at the top of my page,

70
00:03:07,00 --> 00:03:10,00
these are going to be column one hyphen two

71
00:03:10,00 --> 00:03:11,08
because it's going to go from one column wide

72
00:03:11,08 --> 00:03:13,08
to two columns wide.

73
00:03:13,08 --> 00:03:16,00
And so now I just need to change all

74
00:03:16,00 --> 00:03:25,09
of those class names here in my row.

75
00:03:25,09 --> 00:03:28,02
And remember, down here at the very bottom

76
00:03:28,02 --> 00:03:30,05
I also had some column ones down here.

77
00:03:30,05 --> 00:03:33,09
These, I'm going to want to go from one column wide

78
00:03:33,09 --> 00:03:35,05
to two columns wide,

79
00:03:35,05 --> 00:03:38,00
and we'll probably have some reordering we're going to need

80
00:03:38,00 --> 00:03:39,03
to do down here at the bottom.

81
00:03:39,03 --> 00:03:42,01
I'm not going to address that just yet,

82
00:03:42,01 --> 00:03:45,06
but just sort of file that away for the moment.

83
00:03:45,06 --> 00:03:47,02
Now by changing my class names here,

84
00:03:47,02 --> 00:03:48,03
I've broken my colors.

85
00:03:48,03 --> 00:03:50,07
I've probably also broken parts of my layout,

86
00:03:50,07 --> 00:03:54,03
so let's go back and fix those issues.

87
00:03:54,03 --> 00:03:55,06
If I go to my colors here,

88
00:03:55,06 --> 00:03:57,09
right now, instead of having just a col one,

89
00:03:57,09 --> 00:04:00,09
I have a col two, one hyphen two,

90
00:04:00,09 --> 00:04:05,01
and a col hyphen one hyphen four.

91
00:04:05,01 --> 00:04:06,07
If you wanted to make those different colors,

92
00:04:06,07 --> 00:04:11,00
you could certainly do that.

93
00:04:11,00 --> 00:04:13,05
Down here in my desktop media query,

94
00:04:13,05 --> 00:04:15,02
these are going to behave the same,

95
00:04:15,02 --> 00:04:18,08
so col hyphen one hyphen two

96
00:04:18,08 --> 00:04:23,05
and col hyphen one hyphen four,

97
00:04:23,05 --> 00:04:27,06
they're going to do the same thing.

98
00:04:27,06 --> 00:04:31,07
But when we get up here to our tablet media query,

99
00:04:31,07 --> 00:04:33,07
we're going to change behaviors here.

100
00:04:33,07 --> 00:04:36,05
Column hyphen one hyphen two

101
00:04:36,05 --> 00:04:40,07
is going to have this 48% width,

102
00:04:40,07 --> 00:04:44,04
which is going to be the same as column hyphen two,

103
00:04:44,04 --> 00:04:51,05
so we could actually just combine those into one style.

104
00:04:51,05 --> 00:04:54,08
And just streamline things a little bit there.

105
00:04:54,08 --> 00:04:56,00
There we go.

106
00:04:56,00 --> 00:04:58,07
So how's that looking now?

107
00:04:58,07 --> 00:05:00,02
Okay, better.

108
00:05:00,02 --> 00:05:04,00
Column three definitely needs some attention still, right?

109
00:05:04,00 --> 00:05:05,03
And as I mentioned here,

110
00:05:05,03 --> 00:05:08,02
we have an ordering issue going on down here in row five.

111
00:05:08,02 --> 00:05:10,02
Let's just leave that alone for the moment.

112
00:05:10,02 --> 00:05:13,08
We'll come back and revisit that.

113
00:05:13,08 --> 00:05:15,05
So here with column three,

114
00:05:15,05 --> 00:05:19,04
what I actually want it to do here is go to 100%.

115
00:05:19,04 --> 00:05:20,06
There we go,

116
00:05:20,06 --> 00:05:24,02
and then in the media query, we want it to go to 74.

117
00:05:24,02 --> 00:05:26,04
So with the exception of row five,

118
00:05:26,04 --> 00:05:29,08
I think we've got things pretty well under control here.

119
00:05:29,08 --> 00:05:30,07
Let's just test.

120
00:05:30,07 --> 00:05:35,01
If we go back to a width that will work for our desktop

121
00:05:35,01 --> 00:05:39,03
media query, this is still looking the same.

122
00:05:39,03 --> 00:05:44,03
Good, and if we narrow this up to our table size,

123
00:05:44,03 --> 00:05:45,03
then things are looking good

124
00:05:45,03 --> 00:05:48,03
except for the ordering issue we have going on down here

125
00:05:48,03 --> 00:05:52,01
in row number five.

126
00:05:52,01 --> 00:05:54,03
And then the last thing that I want to do

127
00:05:54,03 --> 00:05:57,00
is set up a default flex basis value

128
00:05:57,00 --> 00:05:58,02
because as you know,

129
00:05:58,02 --> 00:06:04,07
when I go down here to a mobile dimension,

130
00:06:04,07 --> 00:06:07,02
these are all basically the width of their content

131
00:06:07,02 --> 00:06:10,00
and they look like they're lining up really nicely now,

132
00:06:10,00 --> 00:06:11,03
but let's go ahead

133
00:06:11,03 --> 00:06:14,07
and explicitly spell out exactly what we want here.

134
00:06:14,07 --> 00:06:17,03
It's very simple to do outside the media query.

135
00:06:17,03 --> 00:06:21,05
All we have to do is put in an attribute selector.

136
00:06:21,05 --> 00:06:24,08
So I could say here that the class

137
00:06:24,08 --> 00:06:30,04
is going to start equal quote col hyphen.

138
00:06:30,04 --> 00:06:32,07
So what does that mean?

139
00:06:32,07 --> 00:06:35,00
Remember that attribute selector is going to select

140
00:06:35,00 --> 00:06:36,06
for the attribute of the class

141
00:06:36,06 --> 00:06:39,05
rather than the HTML tag,

142
00:06:39,05 --> 00:06:43,06
so here, any class that has C-O-L hyphen in it,

143
00:06:43,06 --> 00:06:45,05
which is all of them

144
00:06:45,05 --> 00:06:47,00
will be affected by the style,

145
00:06:47,00 --> 00:06:51,05
and we'll just say our flex hyphen basis is 100%.

146
00:06:51,05 --> 00:06:54,08
So that'll take care of that mobile version of this.

147
00:06:54,08 --> 00:06:58,04
Now the last part, let's deal with that final row

148
00:06:58,04 --> 00:07:02,06
and we'll start by just looking at our HTML here.

149
00:07:02,06 --> 00:07:05,02
If this is the order that I want to display

150
00:07:05,02 --> 00:07:08,05
at the desktop arrangement,

151
00:07:08,05 --> 00:07:10,04
what I need to think about is

152
00:07:10,04 --> 00:07:14,00
how is this going to work here

153
00:07:14,00 --> 00:07:15,08
when I get to a tablet arrangement?

154
00:07:15,08 --> 00:07:19,05
Maybe here, what I'd like to have happen is this pink box.

155
00:07:19,05 --> 00:07:21,06
Okay, the middle, the main content,

156
00:07:21,06 --> 00:07:25,05
I want that to span across all four columns

157
00:07:25,05 --> 00:07:27,04
with these two yellow boxes,

158
00:07:27,04 --> 00:07:29,08
the Corporate Functions and Keen on Green,

159
00:07:29,08 --> 00:07:32,02
I'd like them spanning two boxes.

160
00:07:32,02 --> 00:07:35,04
So in order to get that funky arrangement to work here,

161
00:07:35,04 --> 00:07:38,08
what we're going to first do is we're going to reorder.

162
00:07:38,08 --> 00:07:42,02
So I'm going to put Other Events

163
00:07:42,02 --> 00:07:47,09
as the very first cell in my row.

164
00:07:47,09 --> 00:07:53,02
Okay, so there it is, our very first cell in our row.

165
00:07:53,02 --> 00:07:56,00
Okay, and now I can make that span across

166
00:07:56,00 --> 00:07:57,04
all of those columns.

167
00:07:57,04 --> 00:08:00,08
So I'm going to call this one two hyphen four,

168
00:08:00,08 --> 00:08:04,05
so that it's going to go from two columns wide

169
00:08:04,05 --> 00:08:06,07
to four columns wide.

170
00:08:06,07 --> 00:08:09,01
And then in my CSS,

171
00:08:09,01 --> 00:08:12,01
I can just add that in.

172
00:08:12,01 --> 00:08:14,02
So here at desktop dimensions,

173
00:08:14,02 --> 00:08:15,08
we'll have a column two

174
00:08:15,08 --> 00:08:21,00
and a column hyphen two hyphen four.

175
00:08:21,00 --> 00:08:24,00
Up here at our tablet dimensions,

176
00:08:24,00 --> 00:08:27,05
this is going to behave more like column three and four, right?

177
00:08:27,05 --> 00:08:29,07
So we can combine these together,

178
00:08:29,07 --> 00:08:37,00
so col hyphen two hyphen four comma, column three.

179
00:08:37,00 --> 00:08:39,07
And for column four,

180
00:08:39,07 --> 00:08:46,00
these are all going to have a flex basis of 100%, right?

181
00:08:46,00 --> 00:08:49,08
Great, so let's just take a quick peek at that.

182
00:08:49,08 --> 00:08:54,00
Okay, oh, we need to add the color for that row,

183
00:08:54,00 --> 00:08:58,03
so let's go on ahead and add that, as well.

184
00:08:58,03 --> 00:09:00,07
Col hyphen two hyphen four, there we go.

185
00:09:00,07 --> 00:09:03,08
Now that's pink, so that looks great here

186
00:09:03,08 --> 00:09:05,04
on our tablet,

187
00:09:05,04 --> 00:09:07,09
but when I go back here,

188
00:09:07,09 --> 00:09:11,04
now the pink's over here on the left side.

189
00:09:11,04 --> 00:09:13,07
I really need to have these reordered.

190
00:09:13,07 --> 00:09:17,01
So we're going to add to this

191
00:09:17,01 --> 00:09:18,03
in our HTML,

192
00:09:18,03 --> 00:09:21,02
well, all we have to do now is add some classes for this.

193
00:09:21,02 --> 00:09:22,05
So what I'm going to do

194
00:09:22,05 --> 00:09:26,02
is I'm going to call this first one here.

195
00:09:26,02 --> 00:09:27,08
We can just add a second class.

196
00:09:27,08 --> 00:09:32,01
I'm going to add this as order hyphen A

197
00:09:32,01 --> 00:09:35,02
because that's the one I want to have come first.

198
00:09:35,02 --> 00:09:39,09
This next one here is going to be order hyphen B,

199
00:09:39,09 --> 00:09:43,05
and then we're going to have the very last one here,

200
00:09:43,05 --> 00:09:48,00
the last article will be order hyphen C.

201
00:09:48,00 --> 00:09:51,08
And then all I have to do is write some CSS for those.

202
00:09:51,08 --> 00:09:56,09
So here, inside of our desktop media query,

203
00:09:56,09 --> 00:09:59,00
we'll go on ahead and add some classes

204
00:09:59,00 --> 00:10:01,09
for order hyphen A.

205
00:10:01,09 --> 00:10:05,08
My order value will be two, okay.

206
00:10:05,08 --> 00:10:13,00
So in other words, it's going to come second.

207
00:10:13,00 --> 00:10:21,03
B is going to wind up being number one,

208
00:10:21,03 --> 00:10:26,08
and letter C

209
00:10:26,08 --> 00:10:31,05
will wind up being number three.

210
00:10:31,05 --> 00:10:35,08
Great.

211
00:10:35,08 --> 00:10:37,01
Then of course,

212
00:10:37,01 --> 00:10:40,07
let's just take a quick peek and see how we're doing here.

213
00:10:40,07 --> 00:10:43,04
So here we are at desktop dimensions.

214
00:10:43,04 --> 00:10:46,03
Now my row is in the correct order here

215
00:10:46,03 --> 00:10:50,00
and if I narrow things up into tablet dimensions,

216
00:10:50,00 --> 00:10:51,00
that's looking great.

217
00:10:51,00 --> 00:10:53,06
We have this on top, these two underneath,

218
00:10:53,06 --> 00:10:56,06
and if we narrow up to mobile dimensions,

219
00:10:56,06 --> 00:11:00,06
then they're all just going to stack on top of each other.

220
00:11:00,06 --> 00:11:02,03
Fabulous, okay.

221
00:11:02,03 --> 00:11:05,03
So this is our starting prototype, then,

222
00:11:05,03 --> 00:11:07,07
for our Flexbox-based grid system,

223
00:11:07,07 --> 00:11:09,03
and this is pretty much state of the art

224
00:11:09,03 --> 00:11:12,01
for what we're doing right now with CSS.

225
00:11:12,01 --> 00:11:13,05
Everything is all spelled out here.

226
00:11:13,05 --> 00:11:14,06
We have flex basis,

227
00:11:14,06 --> 00:11:17,08
we're managing our gap with space between,

228
00:11:17,08 --> 00:11:20,03
but we are repeating ourselves over and over again.

229
00:11:20,03 --> 00:11:21,02
As you can see here,

230
00:11:21,02 --> 00:11:24,07
we're redefining flex basis over and over again

231
00:11:24,07 --> 00:11:26,05
in these various spots,

232
00:11:26,05 --> 00:11:29,02
redefining these numbers over and over again

233
00:11:29,02 --> 00:11:31,03
and things are getting a little bit long.

234
00:11:31,03 --> 00:11:33,08
So the next thing to do is now let's go on ahead

235
00:11:33,08 --> 00:11:36,02
and apply calc and custom properties

236
00:11:36,02 --> 00:11:39,00
and see how we can streamline this code.

