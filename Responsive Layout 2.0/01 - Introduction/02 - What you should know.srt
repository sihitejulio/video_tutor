1
00:00:00,05 --> 00:00:01,03
- [Instructor] This course

2
00:00:01,03 --> 00:00:05,05
is a much more advanced course in HTML and CSS.

3
00:00:05,05 --> 00:00:07,04
And so, there's a number of prerequisites

4
00:00:07,04 --> 00:00:09,06
that you should have either the knowledge of

5
00:00:09,06 --> 00:00:12,02
or you've looked at our courses on those topics

6
00:00:12,02 --> 00:00:13,07
before you watch this course

7
00:00:13,07 --> 00:00:15,03
in order to get the most out of it.

8
00:00:15,03 --> 00:00:18,00
So, first of all, one of the most important things to know

9
00:00:18,00 --> 00:00:21,09
is HTML, the semantics of HTML, and so forth.

10
00:00:21,09 --> 00:00:23,05
And a great place to get that background

11
00:00:23,05 --> 00:00:26,05
is with our HTML Essential Training course.

12
00:00:26,05 --> 00:00:29,02
Second, you should have a really good,

13
00:00:29,02 --> 00:00:32,03
strong background in CSS,

14
00:00:32,03 --> 00:00:35,00
generally and specifically in a couple of areas.

15
00:00:35,00 --> 00:00:37,03
If you're looking for a general background,

16
00:00:37,03 --> 00:00:41,00
a great place to get that is here, CSS Essential Training.

17
00:00:41,00 --> 00:00:42,03
There are three parts to this.

18
00:00:42,03 --> 00:00:44,09
The first two are going to be most relevant

19
00:00:44,09 --> 00:00:47,06
to the course that we'll be working on.

20
00:00:47,06 --> 00:00:51,00
You can also take a look at my CSS Selectors course.

21
00:00:51,00 --> 00:00:55,02
This one talks about how to form selectors in CSS.

22
00:00:55,02 --> 00:00:56,01
You'll find me using

23
00:00:56,01 --> 00:00:59,01
a number of wildly different selectors

24
00:00:59,01 --> 00:01:01,01
in this course for various reasons.

25
00:01:01,01 --> 00:01:02,01
And so, if you're wondering

26
00:01:02,01 --> 00:01:04,03
how I went about forming those selectors,

27
00:01:04,03 --> 00:01:07,01
this course will walk you through that.

28
00:01:07,01 --> 00:01:09,07
You'll definitely need a background in Flexbox

29
00:01:09,07 --> 00:01:11,02
and in Grid Layouts.

30
00:01:11,02 --> 00:01:13,02
If you don't have any background in that,

31
00:01:13,02 --> 00:01:15,09
because these are relatively new technologies,

32
00:01:15,09 --> 00:01:19,00
this is the course where you should start looking at that.

33
00:01:19,00 --> 00:01:20,09
The Responsive Layout course will walk you through

34
00:01:20,09 --> 00:01:22,09
the basics of Flexbox and Grid,

35
00:01:22,09 --> 00:01:25,06
and we're going to build on those skills in this course.

36
00:01:25,06 --> 00:01:28,01
And then finally, although I'm not going to work

37
00:01:28,01 --> 00:01:30,02
with Sass directly in this course,

38
00:01:30,02 --> 00:01:32,06
I do talk about it a number of times,

39
00:01:32,06 --> 00:01:35,03
I make references to how to integrate Sass

40
00:01:35,03 --> 00:01:38,09
with various types of CSS variables,

41
00:01:38,09 --> 00:01:41,02
and custom properties, and calc.

42
00:01:41,02 --> 00:01:43,07
So, if you want to be really clear

43
00:01:43,07 --> 00:01:45,08
about what Sass is and how it works,

44
00:01:45,08 --> 00:01:46,07
you can always take a look

45
00:01:46,07 --> 00:01:49,06
at our Sass Essential Training course.

46
00:01:49,06 --> 00:01:52,03
So, that's sort of the background that you should have

47
00:01:52,03 --> 00:01:54,02
coming into this course, in other words,

48
00:01:54,02 --> 00:01:57,03
strong working knowledge of HTML and CSS,

49
00:01:57,03 --> 00:02:01,00
and definitely have a background with Flexbox and Grid

50
00:02:01,00 --> 00:02:03,05
before you start to layer on all of the new stuff

51
00:02:03,05 --> 00:02:05,00
that I'll be introducing here.

