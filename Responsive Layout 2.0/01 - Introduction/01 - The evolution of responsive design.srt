1
00:00:00,06 --> 00:00:03,02
- What a ride we've been on these last 12 years,

2
00:00:03,02 --> 00:00:04,06
front-end web designers.

3
00:00:04,06 --> 00:00:06,02
When Ethan Marcotte introduced us

4
00:00:06,02 --> 00:00:08,02
to responsive design in 2010,

5
00:00:08,02 --> 00:00:11,03
we worked with floats for laying out our grid-based systems,

6
00:00:11,03 --> 00:00:15,00
some early technology for loading images of the right size,

7
00:00:15,00 --> 00:00:18,00
and badly supported media queries.

8
00:00:18,00 --> 00:00:19,06
Nine years later, our tools

9
00:00:19,06 --> 00:00:22,07
and techniques have radically evolved.

10
00:00:22,07 --> 00:00:25,05
Hacking flexbox to do webpage layouts

11
00:00:25,05 --> 00:00:28,00
displaced our hacked-up floats.

12
00:00:28,00 --> 00:00:31,06
Fortunately, grid is just about ready for us to use,

13
00:00:31,06 --> 00:00:36,02
our first CSS tool for laying out webpages without hacks.

14
00:00:36,02 --> 00:00:40,07
The ability to do math directly in CSS via the calc property

15
00:00:40,07 --> 00:00:43,08
is available and helpful to our work.

16
00:00:43,08 --> 00:00:47,03
But our biggest shift is with CSS custom properties,

17
00:00:47,03 --> 00:00:50,00
or variables as they're often called.

18
00:00:50,00 --> 00:00:53,05
Unlike SAS variables, CSS custom properties

19
00:00:53,05 --> 00:00:56,04
may have their values changed in media queries.

20
00:00:56,04 --> 00:00:59,03
This is a huge game changer for us,

21
00:00:59,03 --> 00:01:02,05
meaning we can streamline our CSS to new levels

22
00:01:02,05 --> 00:01:04,05
rather than having to repeat our styles

23
00:01:04,05 --> 00:01:07,00
and media queries with new values.

24
00:01:07,00 --> 00:01:10,04
CSS custom properties are impacting type sizing,

25
00:01:10,04 --> 00:01:13,04
page layouts, and much more in our work.

26
00:01:13,04 --> 00:01:15,06
In this course, I'll walk you through the basics

27
00:01:15,06 --> 00:01:17,04
of calc and custom properties,

28
00:01:17,04 --> 00:01:19,06
including their positives and negatives.

29
00:01:19,06 --> 00:01:22,00
And we'll walk through multiple examples

30
00:01:22,00 --> 00:01:23,09
of leveraging these properties,

31
00:01:23,09 --> 00:01:27,09
including a type example, an example with flexbox layout,

32
00:01:27,09 --> 00:01:30,01
and an example with grid layout.

33
00:01:30,01 --> 00:01:31,09
So if you're ready, let's get started

34
00:01:31,09 --> 00:01:34,00
with "Responsive Design 2.0".

