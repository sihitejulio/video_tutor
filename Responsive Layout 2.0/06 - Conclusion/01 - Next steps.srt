1
00:00:00,06 --> 00:00:02,07
- Hey, everybody, it's Jen Cramer.

2
00:00:02,07 --> 00:00:06,04
Thank you so much for watching Responsive Design 2.0.

3
00:00:06,04 --> 00:00:09,03
Isn't it just amazing, the coolest stuff,

4
00:00:09,03 --> 00:00:11,03
that we can do now with custom properties,

5
00:00:11,03 --> 00:00:14,00
and calk, and grid, Flexbox,

6
00:00:14,00 --> 00:00:15,09
and I hope you really enjoyed the course.

7
00:00:15,09 --> 00:00:18,09
I certainly enjoyed putting it together for you.

8
00:00:18,09 --> 00:00:20,05
If you're looking for other types

9
00:00:20,05 --> 00:00:22,02
of materials you can watch with me,

10
00:00:22,02 --> 00:00:24,03
there are many other courses available

11
00:00:24,03 --> 00:00:26,02
in the LinkedIn Learning Library.

12
00:00:26,02 --> 00:00:31,01
You will find courses on foundation, for example,

13
00:00:31,01 --> 00:00:33,04
and of course on CSS selectors

14
00:00:33,04 --> 00:00:34,08
that might be relevant to you,

15
00:00:34,08 --> 00:00:37,09
and if you're looking for information to help your clients,

16
00:00:37,09 --> 00:00:39,02
you might want to try pointing them

17
00:00:39,02 --> 00:00:42,03
to my Accidental Web Designer series.

18
00:00:42,03 --> 00:00:45,06
This releases a new video every week,

19
00:00:45,06 --> 00:00:49,06
and they talk about basically how to organize your website,

20
00:00:49,06 --> 00:00:53,00
and content for your website, and marketing tips,

21
00:00:53,00 --> 00:00:55,08
and a little technology translation stuff,

22
00:00:55,08 --> 00:00:57,08
very much oriented towards people

23
00:00:57,08 --> 00:00:59,09
who don't have a technical background.

24
00:00:59,09 --> 00:01:01,08
And so thanks again so much for watching,

25
00:01:01,08 --> 00:01:05,00
and I hope to see you in another course, here on LinkedIn.

