1
00:00:00,80 --> 00:00:03,80
- [Tutor] Welcome to this video on The Store.

2
00:00:03,80 --> 00:00:05,80
So this is gonna be probably quite a quick video

3
00:00:05,80 --> 00:00:07,60
cause I'm just gonna show you what The Store looks like.

4
00:00:07,60 --> 00:00:09,40
But as I said in the last video,

5
00:00:09,40 --> 00:00:10,90
The Store is really just a JavaScript object

6
00:00:10,90 --> 00:00:12,70
with some stuff in it.

7
00:00:12,70 --> 00:00:15,90
So let's take a look and just have a look around.

8
00:00:15,90 --> 00:00:16,70
- [Announcer] In this video,

9
00:00:16,70 --> 00:00:17,80
we're gonna be looking at The Store,

10
00:00:17,80 --> 00:00:18,60
which is where you store

11
00:00:18,60 --> 00:00:21,70
all your application data that's in Redux.

12
00:00:21,70 --> 00:00:24,00
- [Tutor] Okay, so if we load up the app now,

13
00:00:24,00 --> 00:00:26,60
we can see there's an action being fired.

14
00:00:26,60 --> 00:00:27,80
So let's have a look in The Store.

15
00:00:27,80 --> 00:00:29,70
So right now, The Store's actually empty.

16
00:00:29,70 --> 00:00:31,00
So you can see up here.

17
00:00:31,00 --> 00:00:34,30
And then we can see actions here.

18
00:00:34,30 --> 00:00:37,30
So we've got an action of type HELLO with no payload

19
00:00:37,30 --> 00:00:40,60
and then afterwards, it shows us the new store,

20
00:00:40,60 --> 00:00:42,00
which is also empty.

21
00:00:42,00 --> 00:00:43,80
So I'm gonna go ahead and put something in The Store

22
00:00:43,80 --> 00:00:45,30
so you can see how it's working.

23
00:00:45,30 --> 00:00:48,40
So here in reducers post, which is one of our reducers

24
00:00:48,40 --> 00:00:50,30
which kind of defines what's in The Store,

25
00:00:50,30 --> 00:00:53,20
I'm gonna change the initial state to be post,

26
00:00:53,20 --> 00:00:56,00
an array, and then I'll have, like,

27
00:00:56,00 --> 00:00:58,60
then a post1 in there.

28
00:00:58,60 --> 00:01:00,60
So now, if we go back to the site,

29
00:01:00,60 --> 00:01:01,70
so when we go back to the site,

30
00:01:01,70 --> 00:01:03,70
we can now see that posts

31
00:01:03,70 --> 00:01:07,20
has post key inside there and Array,

32
00:01:07,20 --> 00:01:09,30
so we can see that stuff's in there.

33
00:01:09,30 --> 00:01:11,50
And it's still in there afterwards as well.

34
00:01:11,50 --> 00:01:14,20
So this is kind of The Store.

35
00:01:14,20 --> 00:01:15,20
Everything gets logged out.

36
00:01:15,20 --> 00:01:17,30
I really recommend using Redux logger

37
00:01:17,30 --> 00:01:18,90
and looking at these log messages.

38
00:01:18,90 --> 00:01:20,30
I've used them a lot.

39
00:01:20,30 --> 00:01:21,80
I've been doing Redux for a couple of years.

40
00:01:21,80 --> 00:01:23,40
I still look at them really regularly.

41
00:01:23,40 --> 00:01:25,10
They really help me see what's going on.

42
00:01:25,10 --> 00:01:26,40
Which actions were fired,

43
00:01:26,40 --> 00:01:28,20
what did the state look like before,

44
00:01:28,20 --> 00:01:30,00
what does the state look like afterwards?

45
00:01:30,00 --> 00:01:31,80
Really helps you see what's going on.

46
00:01:31,80 --> 00:01:32,70
So that's The Store.

47
00:01:32,70 --> 00:01:34,20
It's just a big JavaScript object

48
00:01:34,20 --> 00:01:35,50
that can have stuff in it,

49
00:01:35,50 --> 00:01:36,70
and that's all it is.

50
00:01:36,70 --> 00:01:37,50
- [Announcer] In this section,

51
00:01:37,50 --> 00:01:39,30
we just had an introduction to Redux.

52
00:01:39,30 --> 00:01:41,50
What it is, why you might want to use it.

53
00:01:41,50 --> 00:01:44,60
We looked at some diagrams of how Redux works

54
00:01:44,60 --> 00:01:46,60
and the different things that make up Redux,

55
00:01:46,60 --> 00:01:48,20
and then we looked at The Store,

56
00:01:48,20 --> 00:01:51,30
which is the most fundamental concept of Redux.

57
00:01:51,30 --> 00:01:52,40
In the next section,

58
00:01:52,40 --> 00:01:55,00
we're gonna look at using Redux in your applications.

