1
00:00:01,20 --> 00:00:04,90
- Section one: Getting Familiar with Redux.

2
00:00:04,90 --> 00:00:06,40
In this section, we're gonna take a lot at

3
00:00:06,40 --> 00:00:09,90
what Redux is and why we should care about it.

4
00:00:09,90 --> 00:00:11,90
We're going to take a look at a big picture diagram

5
00:00:11,90 --> 00:00:13,40
that will really help you understand

6
00:00:13,40 --> 00:00:16,20
where Redux fits into the JavaScript ecosystem

7
00:00:16,20 --> 00:00:18,50
and why you might wanna use it in your application.

8
00:00:18,50 --> 00:00:19,50
And we're going to learn about

9
00:00:19,50 --> 00:00:21,70
one of the fundamental concepts of Redux,

10
00:00:21,70 --> 00:00:23,90
which is "What is The Store?"

11
00:00:23,90 --> 00:00:25,30
In this video, I'm just going to give you

12
00:00:25,30 --> 00:00:27,80
a high-level overview of what Redux is

13
00:00:27,80 --> 00:00:29,70
and why you might care about it.

14
00:00:29,70 --> 00:00:31,80
So, Redux is...

15
00:00:31,80 --> 00:00:35,70
a way of storing application's state,

16
00:00:35,70 --> 00:00:37,00
and I guess the first thing you really

17
00:00:37,00 --> 00:00:38,80
need to understand with Redux is, like,

18
00:00:38,80 --> 00:00:41,60
What is it? Why might you want to use it?

19
00:00:41,60 --> 00:00:43,90
So, the problem Redux is trying to solve

20
00:00:43,90 --> 00:00:47,00
is when you build larger applications, generally,

21
00:00:47,00 --> 00:00:49,70
so maybe something a little bit bigger

22
00:00:49,70 --> 00:00:51,50
than just like a toy example,

23
00:00:51,50 --> 00:00:54,00
and generally, when people have built that

24
00:00:54,00 --> 00:00:56,00
size of application, it gets a little bit bigger,

25
00:00:56,00 --> 00:00:59,00
they start to notice bugs creeping in quite consistently,

26
00:00:59,00 --> 00:01:00,90
some of those bugs can be quite hard to fix.

27
00:01:00,90 --> 00:01:04,50
And this, a lot of the motivation for Redux,

28
00:01:04,50 --> 00:01:07,10
and really what's called the "Flux design pattern,"

29
00:01:07,10 --> 00:01:10,00
which is what Redux is one implementation of,

30
00:01:10,00 --> 00:01:12,40
was born out of Facebook with a bug that they had

31
00:01:12,40 --> 00:01:16,80
with the chat window in the old Facebook site

32
00:01:16,80 --> 00:01:19,20
before it became its own thing.

33
00:01:19,20 --> 00:01:22,80
And so, the bug they had was the,

34
00:01:22,80 --> 00:01:25,60
a little one "unread" notification would pop up,

35
00:01:25,60 --> 00:01:27,30
the user would read the message,

36
00:01:27,30 --> 00:01:28,90
and then the one would come back again.

37
00:01:28,90 --> 00:01:30,20
And they realized that this bug was, like,

38
00:01:30,20 --> 00:01:31,50
really hard to track down.

39
00:01:31,50 --> 00:01:33,40
They fixed it, and then it kept coming back

40
00:01:33,40 --> 00:01:36,20
and they had really smart engineers wasting loads of time.

41
00:01:36,20 --> 00:01:38,40
And so some clever person somewhere sat down

42
00:01:38,40 --> 00:01:40,10
and they were like, "What is it we're doing wrong?

43
00:01:40,10 --> 00:01:42,20
"Why is this so hard for us to fix?"

44
00:01:42,20 --> 00:01:43,80
And what they came up with is something called

45
00:01:43,80 --> 00:01:45,80
the "Flux design pattern."

46
00:01:45,80 --> 00:01:48,30
So it's a design pattern, it's a way of structuring code,

47
00:01:48,30 --> 00:01:50,90
it's a set of rules for how you should write programs

48
00:01:50,90 --> 00:01:53,80
and just structuring them in a specific way

49
00:01:53,80 --> 00:01:55,80
to, like, make things easier for you.

50
00:01:55,80 --> 00:01:58,20
And, really, the rule is about

51
00:01:58,20 --> 00:02:00,10
you need directional data flow,

52
00:02:00,10 --> 00:02:01,90
which I'll talk about a bit later on.

53
00:02:01,90 --> 00:02:05,60
It's just a way of structuring programs.

54
00:02:05,60 --> 00:02:08,00
Redux is a State Machine, or like

55
00:02:08,00 --> 00:02:09,40
a kind of State Machine, really,

56
00:02:09,40 --> 00:02:11,80
so it can hold as much state as you want.

57
00:02:11,80 --> 00:02:13,50
So by "state," I just mean,

58
00:02:13,50 --> 00:02:15,40
in JavaScript has JavaScript variables,

59
00:02:15,40 --> 00:02:18,60
JavaScript objects, arrays, strings, numbers,

60
00:02:18,60 --> 00:02:20,70
really anything you can represent in JSON

61
00:02:20,70 --> 00:02:24,10
would probably be the best, like, bulletproof description

62
00:02:24,10 --> 00:02:26,50
of, you know, what you can store in Redux.

63
00:02:26,50 --> 00:02:28,30
And then your application becomes

64
00:02:28,30 --> 00:02:31,60
a set of transformations between those states.

65
00:02:31,60 --> 00:02:33,70
So maybe you've got, you know,

66
00:02:33,70 --> 00:02:36,30
a page with three Reddit posts on

67
00:02:36,30 --> 00:02:38,30
and someone enters a new Reddit post,

68
00:02:38,30 --> 00:02:40,40
well then, what should happen is you should,

69
00:02:40,40 --> 00:02:42,90
the Redux state will have the three Reddit posts

70
00:02:42,90 --> 00:02:45,00
and then when you reload the page the fourth one appears

71
00:02:45,00 --> 00:02:48,10
and you'll have the fourth one in Redux state.

72
00:02:48,10 --> 00:02:50,80
And then your application just becomes about

73
00:02:50,80 --> 00:02:54,20
managing the state, transitioning the state

74
00:02:54,20 --> 00:02:56,00
from one thing to another, and then

75
00:02:56,00 --> 00:02:58,40
binding that state to the UI you are building.

76
00:02:58,40 --> 00:03:00,60
Now this all sounds pretty generic when I talk about this,

77
00:03:00,60 --> 00:03:01,40
but what we're gonna show you

78
00:03:01,40 --> 00:03:02,60
through these sequence of videos

79
00:03:02,60 --> 00:03:04,20
some practical kind of examples

80
00:03:04,20 --> 00:03:06,00
of how you can use Redux, what it is.

81
00:03:06,00 --> 00:03:09,70
And Redux is about third popular framework

82
00:03:09,70 --> 00:03:12,50
to implement the Flux design pattern.

83
00:03:12,50 --> 00:03:16,40
Before that, there was "Flux," and then "Reflux,"

84
00:03:16,40 --> 00:03:18,10
and then, I think, "Redux."

85
00:03:18,10 --> 00:03:19,70
There were lots of others along the way.

86
00:03:19,70 --> 00:03:22,10
There's also another similar technology called "MobX"

87
00:03:22,10 --> 00:03:23,40
which has gained some momentum,

88
00:03:23,40 --> 00:03:26,10
but Redux is by far still the most popular.

89
00:03:26,10 --> 00:03:29,20
I don't think it matters which library you use that much

90
00:03:29,20 --> 00:03:31,50
and I think they all have quite similar ideas.

91
00:03:31,50 --> 00:03:33,00
I've been using Redux for a couple of years now

92
00:03:33,00 --> 00:03:34,30
and I really like it.

93
00:03:34,30 --> 00:03:36,40
It's a little bit difficult to understand at first

94
00:03:36,40 --> 00:03:38,20
and it can be a little bit cumbersome.

95
00:03:38,20 --> 00:03:40,20
You have to create quite a lot of files,

96
00:03:40,20 --> 00:03:42,40
and that's definitely a downside of that.

97
00:03:42,40 --> 00:03:44,80
But overall, it makes things super simple

98
00:03:44,80 --> 00:03:46,10
and what I really like about the

99
00:03:46,10 --> 00:03:48,00
Flux design pattern in Redux

100
00:03:48,00 --> 00:03:51,70
is I know exactly where each bits of my code should go

101
00:03:51,70 --> 00:03:52,80
and I know what the rules are.

102
00:03:52,80 --> 00:03:54,90
There's very clear rules in Redux

103
00:03:54,90 --> 00:03:56,40
and there's not a lot of ambiguity

104
00:03:56,40 --> 00:03:58,30
and there's a program where I love that, because it means

105
00:03:58,30 --> 00:03:59,90
when I'm working with my colleagues,

106
00:03:59,90 --> 00:04:01,40
it's easy to make decisions because

107
00:04:01,40 --> 00:04:03,30
there are nice clear rules and you can just say,

108
00:04:03,30 --> 00:04:05,80
"Well, you know, the rule is this is how it works,

109
00:04:05,80 --> 00:04:07,40
"and you haven't done it like that," or

110
00:04:07,40 --> 00:04:08,90
"I haven't done it like that, so I'm wrong,"

111
00:04:08,90 --> 00:04:10,50
and it makes things easier.

112
00:04:10,50 --> 00:04:12,30
There are some ambiguities to Redux

113
00:04:12,30 --> 00:04:14,20
which we'll talk about maybe nearer the end,

114
00:04:14,20 --> 00:04:16,50
but I think, overall, things are pretty clear.

115
00:04:16,50 --> 00:04:17,70
Yeah, it's a really good library,

116
00:04:17,70 --> 00:04:19,50
I really recommend it if you work with,

117
00:04:19,50 --> 00:04:21,80
it works really well with React, but the concept,

118
00:04:21,80 --> 00:04:23,90
and Redux actually works with other libraries,

119
00:04:23,90 --> 00:04:25,10
so you can use it with Angular,

120
00:04:25,10 --> 00:04:26,50
you can use it with Vue.js, or

121
00:04:26,50 --> 00:04:28,10
you can use it with many things.

122
00:04:28,10 --> 00:04:30,30
The concept is the main thing that's important,

123
00:04:30,30 --> 00:04:32,00
rather than the specific library.

124
00:04:32,00 --> 00:04:34,30
I'm using the specific library just to show you

125
00:04:34,30 --> 00:04:37,00
because it's easier to see things when you see them working.

126
00:04:37,00 --> 00:04:37,90
And it works super well with React,

127
00:04:37,90 --> 00:04:40,00
that's what it was kind of designed for,

128
00:04:40,00 --> 00:04:41,50
but it is a separate library,

129
00:04:41,50 --> 00:04:43,30
and you can use it with other things.

130
00:04:43,30 --> 00:04:45,50
There's only one library we're going to be using in here,

131
00:04:45,50 --> 00:04:48,30
which is React-specific, that's React-Redux.

132
00:04:48,30 --> 00:04:50,90
But Redux itself and all the middlewares we're using,

133
00:04:50,90 --> 00:04:52,20
you can certainly use those with like

134
00:04:52,20 --> 00:04:53,70
Angular or something else.

135
00:04:53,70 --> 00:04:55,40
We just learned what Redux is

136
00:04:55,40 --> 00:04:57,20
and why we might care about it.

137
00:04:57,20 --> 00:04:59,10
So, in the next video we're gonna look at

138
00:04:59,10 --> 00:05:01,30
a big picture diagram on Redux

139
00:05:01,30 --> 00:05:05,00
and where it fits into the JavaScript ecosystem.

