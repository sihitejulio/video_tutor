1
00:00:01,30 --> 00:00:02,50
- [Instructor] Big Picture Diagram,

2
00:00:02,50 --> 00:00:06,40
Redux and where it fits into the JavaScript ecosystem.

3
00:00:06,40 --> 00:00:10,10
In this video I'm gonna use a diagram to show you

4
00:00:10,10 --> 00:00:13,00
what Redux is and why you might want to use it,

5
00:00:13,00 --> 00:00:17,40
and how all the different things of Redux fit together.

6
00:00:17,40 --> 00:00:21,90
Basically, how it works in the flow of data.

7
00:00:21,90 --> 00:00:25,20
So in this video, we're gonna take a look at this diagram,

8
00:00:25,20 --> 00:00:28,30
to show you how Redux works.

9
00:00:28,30 --> 00:00:31,20
It's a slightly complicated idea at first,

10
00:00:31,20 --> 00:00:32,80
so I'm just gonna run you through

11
00:00:32,80 --> 00:00:35,80
what the different words and vocabulary

12
00:00:35,80 --> 00:00:38,10
that people use to talk about Redux,

13
00:00:38,10 --> 00:00:39,70
'cause it's pretty important here.

14
00:00:39,70 --> 00:00:41,90
I'm gonna introduce to you the main players

15
00:00:41,90 --> 00:00:45,80
of how Redux might work with something like React.

16
00:00:45,80 --> 00:00:48,70
So, in this diagram we've got three things.

17
00:00:48,70 --> 00:00:53,50
We've got the store, we've got connected React components,

18
00:00:53,50 --> 00:00:55,10
and we've got actions.

19
00:00:55,10 --> 00:00:58,40
First we've got reducers which sort of live in the store.

20
00:00:58,40 --> 00:01:01,80
Now I'm gonna walk you through, piece by piece,

21
00:01:01,80 --> 00:01:02,90
what each of these things are,

22
00:01:02,90 --> 00:01:04,50
and I'm gonna sort of explain

23
00:01:04,50 --> 00:01:06,50
what might happen as your,

24
00:01:06,50 --> 00:01:09,40
maybe your program's running and doing some Redux stuff.

25
00:01:09,40 --> 00:01:10,80
So the simplest thing to explain,

26
00:01:10,80 --> 00:01:12,60
and probably the most important thing in Redux,

27
00:01:12,60 --> 00:01:14,00
is the store.

28
00:01:14,00 --> 00:01:18,90
So the store is a JavaScript object, so it's like a JSON,

29
00:01:18,90 --> 00:01:21,50
it has key value pairs in it, like this.

30
00:01:21,50 --> 00:01:23,60
And the store just has application data in it,

31
00:01:23,60 --> 00:01:25,10
or really any data you like, really,

32
00:01:25,10 --> 00:01:28,90
so it could have the currently logged in user,

33
00:01:28,90 --> 00:01:32,50
it could have, if you're building a Reddit client,

34
00:01:32,50 --> 00:01:34,90
it might have the posts that are on the screen,

35
00:01:34,90 --> 00:01:36,90
it can have whatever you want.

36
00:01:36,90 --> 00:01:40,60
So the types you would normally put in your Redux store,

37
00:01:40,60 --> 00:01:43,60
are, so the whole store is always an object,

38
00:01:43,60 --> 00:01:46,10
and usually with some keys, we'll talk about that in a sec.

39
00:01:46,10 --> 00:01:48,60
And then you can have arrays in there,

40
00:01:48,60 --> 00:01:51,00
you can have other objects, you can have strings,

41
00:01:51,00 --> 00:01:53,80
you can have integers and floats, or numbers,

42
00:01:53,80 --> 00:01:56,40
I think that's everything you can have.

43
00:01:56,40 --> 00:01:59,10
I would advise against putting complex types in.

44
00:01:59,10 --> 00:02:03,50
I think you can, but it's important, I think in general,

45
00:02:03,50 --> 00:02:05,20
just stick to the sort of primitive types.

46
00:02:05,20 --> 00:02:08,90
So, objects, arrays, strings, numbers,

47
00:02:08,90 --> 00:02:10,10
and nothing, that's it.

48
00:02:10,10 --> 00:02:13,90
In general, you wanna serialize something to one of those,

49
00:02:13,90 --> 00:02:15,20
if it's something more complicated,

50
00:02:15,20 --> 00:02:17,00
for example, maybe a date.

51
00:02:17,00 --> 00:02:18,00
So that's the store,

52
00:02:18,00 --> 00:02:19,80
and the store's usually broken up

53
00:02:19,80 --> 00:02:22,60
into different bits, and here I've kind of,

54
00:02:22,60 --> 00:02:25,10
put them as Reducer 1, Reducer 2, Reducer 3.

55
00:02:25,10 --> 00:02:27,00
I'm going to explain what reducers are in a minute,

56
00:02:27,00 --> 00:02:28,80
but all you kinda need to remember is that,

57
00:02:28,80 --> 00:02:31,50
the store might be broken up into sort of maybe say,

58
00:02:31,50 --> 00:02:35,10
users and posts, or whatever it is that your app's about.

59
00:02:35,10 --> 00:02:37,20
So it's broken into logical bits.

60
00:02:37,20 --> 00:02:41,00
So, that's the store, and your application can kind of,

61
00:02:41,00 --> 00:02:43,30
it's global, there's only one of them, usually.

62
00:02:43,30 --> 00:02:45,30
Pretty much always in a common Redux scenario,

63
00:02:45,30 --> 00:02:46,30
there's only one store.

64
00:02:46,30 --> 00:02:48,40
And then your application, so by your application,

65
00:02:48,40 --> 00:02:51,20
I mean your views in React for example,

66
00:02:51,20 --> 00:02:55,80
so your UI components, they can usually grab some,

67
00:02:55,80 --> 00:02:58,40
or piece of that store, or a couple of bits of that store,

68
00:02:58,40 --> 00:02:59,70
and put them on the screen.

69
00:02:59,70 --> 00:03:01,10
So that's what the store is about.

70
00:03:01,10 --> 00:03:02,80
So then we've got this arrow over here saying,

71
00:03:02,80 --> 00:03:04,10
these stores, they will come back to that.

72
00:03:04,10 --> 00:03:07,40
The next thing you've got, is Connected React Components.

73
00:03:07,40 --> 00:03:08,50
So I'm gonna talk about React,

74
00:03:08,50 --> 00:03:10,70
but really, these principles work with Angular,

75
00:03:10,70 --> 00:03:13,80
or Ember, or G.js or whatever.

76
00:03:13,80 --> 00:03:17,20
So, when you connect a React component,

77
00:03:17,20 --> 00:03:19,80
or a component, so a component,

78
00:03:19,80 --> 00:03:21,00
if you are not familiar with React,

79
00:03:21,00 --> 00:03:23,00
is something which displays on the screen,

80
00:03:23,00 --> 00:03:25,40
so you can see it in the browser.

81
00:03:25,40 --> 00:03:28,10
When you connect, you'll connect into a part of the store.

82
00:03:28,10 --> 00:03:30,60
And what that means is, you're making part of the store,

83
00:03:30,60 --> 00:03:32,10
which is this big JSON thing

84
00:03:32,10 --> 00:03:33,50
over here on the right hand side,

85
00:03:33,50 --> 00:03:37,40
you're making that available to your components.

86
00:03:37,40 --> 00:03:39,70
And you do that in React via props,

87
00:03:39,70 --> 00:03:41,80
so your React component is connected

88
00:03:41,80 --> 00:03:46,00
to that part of the store, or get props from the store.

89
00:03:46,00 --> 00:03:50,00
And if the store updates, then the props will change.

90
00:03:50,00 --> 00:03:52,60
So it's kinda like a data binding this way,

91
00:03:52,60 --> 00:03:55,40
so it's one way, so the store changes,

92
00:03:55,40 --> 00:03:58,10
and your components just get those things.

93
00:03:58,10 --> 00:04:01,50
It's not two way, it's just one way like that.

94
00:04:01,50 --> 00:04:03,10
And then your components kind of can know

95
00:04:03,10 --> 00:04:04,50
about stuff that's in the store,

96
00:04:04,50 --> 00:04:06,40
and use them to put things on the screen.

97
00:04:06,40 --> 00:04:09,40
So for example, you might have a user whose name's Fred,

98
00:04:09,40 --> 00:04:11,00
and you're putting their name on the screen.

99
00:04:11,00 --> 00:04:13,20
Where you would get that, it would be stored in the store,

100
00:04:13,20 --> 00:04:15,00
you know like a user part of the store,

101
00:04:15,00 --> 00:04:18,10
and the name's Fred, and you would get that through a prop,

102
00:04:18,10 --> 00:04:19,50
if you connected a React component,

103
00:04:19,50 --> 00:04:21,80
then you could put Fred on the screen.

104
00:04:21,80 --> 00:04:24,00
So that's connected React components.

105
00:04:24,00 --> 00:04:27,20
So your React components, when the user does stuff,

106
00:04:27,20 --> 00:04:29,50
or really when anything happens,

107
00:04:29,50 --> 00:04:31,60
but let's just say the user clicks a button,

108
00:04:31,60 --> 00:04:35,20
like they press the big hello button or whatever,

109
00:04:35,20 --> 00:04:38,30
then what you can do, is you can fire an action.

110
00:04:38,30 --> 00:04:40,20
So that's something that your components can just do

111
00:04:40,20 --> 00:04:42,70
at any point, when the user does something,

112
00:04:42,70 --> 00:04:44,70
it may not be a user-driven action,

113
00:04:44,70 --> 00:04:47,80
that it may be that you received a message

114
00:04:47,80 --> 00:04:49,40
over WebSockets or you received a message

115
00:04:49,40 --> 00:04:51,10
from somewhere else or something happened.

116
00:04:51,10 --> 00:04:52,50
I don't know, whatever it is.

117
00:04:52,50 --> 00:04:54,60
Maybe just be on a timer delay,

118
00:04:54,60 --> 00:04:57,30
10 seconds after the site loads, you fire an action.

119
00:04:57,30 --> 00:04:59,90
They're not always human-triggered but they usually are,

120
00:04:59,90 --> 00:05:02,50
and then you can fire an action, so what's an action?

121
00:05:02,50 --> 00:05:06,00
So an action comes with a type that's mandatory,

122
00:05:06,00 --> 00:05:08,80
and then it often, by convention, comes with a payload.

123
00:05:08,80 --> 00:05:10,80
In theory you can have anything else in here,

124
00:05:10,80 --> 00:05:12,30
so the type is mandatory;

125
00:05:12,30 --> 00:05:14,00
if you don't have a type it will break.

126
00:05:14,00 --> 00:05:17,50
By convention, the type is in uppercase with underscores,

127
00:05:17,50 --> 00:05:19,80
you can then parse anything you want in the payload,

128
00:05:19,80 --> 00:05:21,70
really, that go in with the rules of the store.

129
00:05:21,70 --> 00:05:25,60
Say, arrays, objects, strings, numbers,

130
00:05:25,60 --> 00:05:27,20
you can parse whatever you want in the payload.

131
00:05:27,20 --> 00:05:28,90
The payload's up to you.

132
00:05:28,90 --> 00:05:30,70
And what an action is really saying is like,

133
00:05:30,70 --> 00:05:32,50
I think maybe the store should be updated

134
00:05:32,50 --> 00:05:33,40
because this thing's happened.

135
00:05:33,40 --> 00:05:34,90
Maybe a user's clicked something,

136
00:05:34,90 --> 00:05:37,60
maybe they typed an extra character into a box,

137
00:05:37,60 --> 00:05:39,80
like an input field they were editing,

138
00:05:39,80 --> 00:05:42,00
but each of those things would be an action,

139
00:05:42,00 --> 00:05:43,60
potentially with a payload.

140
00:05:43,60 --> 00:05:45,60
So a click, for example, might not have a payload.

141
00:05:45,60 --> 00:05:47,60
'Cause all you're gonna say is, they clicked it,

142
00:05:47,60 --> 00:05:49,20
so you don't really need a payload.

143
00:05:49,20 --> 00:05:51,30
But for example, if they type something in the box,

144
00:05:51,30 --> 00:05:54,40
the payload will probably be, what was it they just typed.

145
00:05:54,40 --> 00:05:56,40
Or what's in the box currently.

146
00:05:56,40 --> 00:05:57,80
So yeah, those are actions.

147
00:05:57,80 --> 00:05:59,10
And then the last piece of the puzzle,

148
00:05:59,10 --> 00:06:02,30
so actions, you can see them getting fired to the store.

149
00:06:02,30 --> 00:06:03,60
And then you've got this last box here,

150
00:06:03,60 --> 00:06:05,80
which we've not talked about yet, the reducers.

151
00:06:05,80 --> 00:06:07,90
So reducers are a function

152
00:06:07,90 --> 00:06:12,60
that take the existing state of the store, and an action,

153
00:06:12,60 --> 00:06:15,60
and they calculate the new state of the store.

154
00:06:15,60 --> 00:06:18,00
So for example, you might get this action here which says,

155
00:06:18,00 --> 00:06:21,60
UPDATE_COMPANY, and then the payload is packed,

156
00:06:21,60 --> 00:06:25,40
and then you would in your reducer,

157
00:06:25,40 --> 00:06:28,20
all of those reducers will run,

158
00:06:28,20 --> 00:06:29,40
and some of them might say,

159
00:06:29,40 --> 00:06:31,90
oh, the update company action's been fired,

160
00:06:31,90 --> 00:06:33,00
oh I'm gonna do something.

161
00:06:33,00 --> 00:06:35,60
And then it will return a new state,

162
00:06:35,60 --> 00:06:37,00
and the store will get updated

163
00:06:37,00 --> 00:06:38,80
based on these reducer functions.

164
00:06:38,80 --> 00:06:42,40
And so each reducer normally has its own place in the store,

165
00:06:42,40 --> 00:06:43,90
and it's usually responsible

166
00:06:43,90 --> 00:06:46,10
for updating just its part of the store.

167
00:06:46,10 --> 00:06:49,00
So Reducer 2, is only responsible for this bit here,

168
00:06:49,00 --> 00:06:50,80
that says stuff: "WOWZERS!".

169
00:06:50,80 --> 00:06:53,20
And it can see each of these actions coming in,

170
00:06:53,20 --> 00:06:55,80
and when it comes in, it could return something else,

171
00:06:55,80 --> 00:06:58,00
and then this part of the store would change.

172
00:06:58,00 --> 00:07:00,60
When the store changes, automatically,

173
00:07:00,60 --> 00:07:02,40
the new store state is parsed out

174
00:07:02,40 --> 00:07:03,70
to all the connected components.

175
00:07:03,70 --> 00:07:06,10
So they just automatically update.

176
00:07:06,10 --> 00:07:09,00
And I mentioned earlier that Flux or Redux, whatever,

177
00:07:09,00 --> 00:07:11,70
is a uni-directional data pattern, and you can see why.

178
00:07:11,70 --> 00:07:15,10
It's because this just goes in one direction.

179
00:07:15,10 --> 00:07:17,20
There's no arrows going both ways.

180
00:07:17,20 --> 00:07:20,50
Connected components can't change the store directly,

181
00:07:20,50 --> 00:07:23,00
they can only change the store by firing an action,

182
00:07:23,00 --> 00:07:24,00
and then this code runs,

183
00:07:24,00 --> 00:07:28,00
which determines how the store should be updated.

184
00:07:28,00 --> 00:07:29,70
And so the reducers and the actions,

185
00:07:29,70 --> 00:07:30,70
the actions are kind of boring,

186
00:07:30,70 --> 00:07:32,70
but the reducers really become the core state

187
00:07:32,70 --> 00:07:34,50
of your application.

188
00:07:34,50 --> 00:07:36,80
So in a nutshell, that's kind of what Redux is

189
00:07:36,80 --> 00:07:37,90
in a higher level.

190
00:07:37,90 --> 00:07:39,40
Don't worry if you've not completely understood

191
00:07:39,40 --> 00:07:40,60
everything I've said there,

192
00:07:40,60 --> 00:07:42,60
I just want you to bear in mind the key facts,

193
00:07:42,60 --> 00:07:44,00
that there's big store,

194
00:07:44,00 --> 00:07:45,30
which is a big JavaScript talk chat

195
00:07:45,30 --> 00:07:47,30
with JavaScript stuff inside it.

196
00:07:47,30 --> 00:07:50,60
You can connect that store to React components,

197
00:07:50,60 --> 00:07:53,40
and then they'll get the data as props.

198
00:07:53,40 --> 00:07:56,80
These components and other code can fire actions,

199
00:07:56,80 --> 00:07:59,30
and when actions are fired, reducers run,

200
00:07:59,30 --> 00:08:01,80
which determines how the store changes.

201
00:08:01,80 --> 00:08:03,40
I'm going to show you each of these bits in detail

202
00:08:03,40 --> 00:08:05,10
as we go through this course,

203
00:08:05,10 --> 00:08:07,60
and it should be quite simple, step-by-step.

204
00:08:07,60 --> 00:08:10,10
So together it all seems a bout overwhelming right now,

205
00:08:10,10 --> 00:08:12,10
'cause there's no real detail here.

206
00:08:12,10 --> 00:08:13,40
But just remember the key words,

207
00:08:13,40 --> 00:08:16,90
so store, connected React components,

208
00:08:16,90 --> 00:08:18,60
and actions and reducers,

209
00:08:18,60 --> 00:08:20,40
those are the main three or four things

210
00:08:20,40 --> 00:08:21,90
that you need to remember.

211
00:08:21,90 --> 00:08:26,30
In this video, we looked at the different things in Redux,

212
00:08:26,30 --> 00:08:30,70
so, the store, the reducers, actions,

213
00:08:30,70 --> 00:08:32,80
and connected components,

214
00:08:32,80 --> 00:08:36,20
and how Redux uses uni-directional data flow

215
00:08:36,20 --> 00:08:37,90
to connect them all.

216
00:08:37,90 --> 00:08:42,00
In the next video, we're gonna look at the store in detail.

