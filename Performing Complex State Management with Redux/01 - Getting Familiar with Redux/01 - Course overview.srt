1
00:00:02,20 --> 00:00:08,10
(keyboard clacking)

2
00:00:08,10 --> 00:00:09,90
- [Instructor] Performing complex state management

3
00:00:09,90 --> 00:00:12,70
with Redux by Packt Publishing.

4
00:00:12,70 --> 00:00:14,50
Hi there, I'm Richard Gill,

5
00:00:14,50 --> 00:00:17,00
I have a background in writing web applications,

6
00:00:17,00 --> 00:00:19,30
I've got a few years experience with JavaScript

7
00:00:19,30 --> 00:00:22,50
and I was a pretty early adopter of React and Redux.

8
00:00:22,50 --> 00:00:25,10
I use Redux almost everyday for my job

9
00:00:25,10 --> 00:00:28,30
and have done for at least a year and a half, two years.

10
00:00:28,30 --> 00:00:29,70
Welcome to the course overview.

11
00:00:29,70 --> 00:00:31,40
I'm gonna run through section by section

12
00:00:31,40 --> 00:00:33,10
what we're gonna learn in this course.

13
00:00:33,10 --> 00:00:35,60
In section one, Getting Familiar with Redux,

14
00:00:35,60 --> 00:00:40,40
we'll look at what Redux is and why we might care about it.

15
00:00:40,40 --> 00:00:42,10
We'll take a look at some diagrams

16
00:00:42,10 --> 00:00:44,80
to explain the core concepts of Redux,

17
00:00:44,80 --> 00:00:45,90
how it all fits together

18
00:00:45,90 --> 00:00:48,80
and what the key words are that make up Redux,

19
00:00:48,80 --> 00:00:51,60
get some of the vocabulary down.

20
00:00:51,60 --> 00:00:52,90
And then we'll take a look at the store,

21
00:00:52,90 --> 00:00:57,00
which is central to the whole concept of Redux.

22
00:00:57,00 --> 00:00:58,60
In section two, we'll have a look

23
00:00:58,60 --> 00:01:01,50
at using Redux in your applications,

24
00:01:01,50 --> 00:01:03,90
so we'll look at how you can connect components

25
00:01:03,90 --> 00:01:07,80
to the store, how you can fire an action,

26
00:01:07,80 --> 00:01:11,20
we'll do a quick recap of yet six slash seven spreadings

27
00:01:11,20 --> 00:01:13,20
or the spread operator and how that works

28
00:01:13,20 --> 00:01:15,40
because its very central to how you use Redux.

29
00:01:15,40 --> 00:01:19,60
We'll look at applying Redux reducers to the application.

30
00:01:19,60 --> 00:01:22,00
In section three, we'll look at some

31
00:01:22,00 --> 00:01:24,10
advanced Redux concepts.

32
00:01:24,10 --> 00:01:26,50
So we'll look at some advanced reducer patterns

33
00:01:26,50 --> 00:01:28,60
that you might want to use in your applications.

34
00:01:28,60 --> 00:01:30,00
We'll look at how you actually set

35
00:01:30,00 --> 00:01:32,40
Redux up in the first place, so that you can

36
00:01:32,40 --> 00:01:34,90
set Redux up on your next project.

37
00:01:34,90 --> 00:01:36,40
We'll look at some of the rules of thumb

38
00:01:36,40 --> 00:01:40,00
of how to use Redux, so the do's and the don'ts.

39
00:01:40,00 --> 00:01:43,60
We'll look at using thunks to dispatch asynchronous actions,

40
00:01:43,60 --> 00:01:47,40
which is quite common for http requests, for example.

41
00:01:47,40 --> 00:01:50,40
And we'll look at selectors, what selectors are

42
00:01:50,40 --> 00:01:53,00
and why they're a good thing to use.

43
00:01:53,00 --> 00:01:55,20
In section four, we'll be building a real life

44
00:01:55,20 --> 00:01:56,70
application with Redux.

45
00:01:56,70 --> 00:01:59,20
We'll start off by walking through an existing app

46
00:01:59,20 --> 00:02:01,80
that already exists, and then we will convert it

47
00:02:01,80 --> 00:02:04,40
to use Redux and load it to Reddit application

48
00:02:04,40 --> 00:02:06,80
or RedditPost application and we will use Redux

49
00:02:06,80 --> 00:02:09,60
to load those posts into the application.

50
00:02:09,60 --> 00:02:12,40
The target audience for this course are developers

51
00:02:12,40 --> 00:02:15,10
that are really looking to learn a bit more about Redux,

52
00:02:15,10 --> 00:02:17,70
developers that already know React or Angular or a similar

53
00:02:17,70 --> 00:02:20,90
framework that are looking to use Redux with those,

54
00:02:20,90 --> 00:02:22,40
and developers that want to understand

55
00:02:22,40 --> 00:02:25,20
some of the advanced Redux usages.

56
00:02:25,20 --> 00:02:27,80
The prerequisites for this course are having a Mac,

57
00:02:27,80 --> 00:02:30,10
Linux or Windows computer.

58
00:02:30,10 --> 00:02:34,50
You need version 8.1.2 of node and you should use NVM

59
00:02:34,50 --> 00:02:37,00
to install node if that's possible.

