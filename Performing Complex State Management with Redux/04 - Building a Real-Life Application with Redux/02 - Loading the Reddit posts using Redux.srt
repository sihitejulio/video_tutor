1
00:00:00,90 --> 00:00:04,70
- [Tutor] Loading the Reddit Posts Using Redux.

2
00:00:04,70 --> 00:00:05,90
In this video, we're gonna have a look at

3
00:00:05,90 --> 00:00:09,80
how we can use Firebase with the existing code we have

4
00:00:09,80 --> 00:00:13,30
to quickly load the Reddit post into Redux.

5
00:00:13,30 --> 00:00:15,40
So let's start off by seeing where we actually load

6
00:00:15,40 --> 00:00:16,90
the Reddit post in today.

7
00:00:16,90 --> 00:00:19,00
So you can see we're using state here

8
00:00:19,00 --> 00:00:20,80
to say that there are no Reddit posts

9
00:00:20,80 --> 00:00:23,20
and you can see we do a Firebase call

10
00:00:23,20 --> 00:00:26,20
and then when that comes back, we set the state.

11
00:00:26,20 --> 00:00:28,20
So the first thing is with Redux,

12
00:00:28,20 --> 00:00:32,00
probably we're not really gonna use state very much,

13
00:00:32,00 --> 00:00:33,20
so let's get rid of this,

14
00:00:33,20 --> 00:00:35,70
'cause this was just us playing around.

15
00:00:35,70 --> 00:00:37,60
This code is gonna move to an action

16
00:00:37,60 --> 00:00:39,70
and this code here will no longer exist,

17
00:00:39,70 --> 00:00:42,90
so I'm going to,

18
00:00:42,90 --> 00:00:45,90
so instead here, we're going to probably call an action,

19
00:00:45,90 --> 00:00:49,90
so we need to cut this code here and then export const,

20
00:00:49,90 --> 00:00:51,30
We'll call it loadPosts,

21
00:00:51,30 --> 00:00:54,30
so this goes and grabs the posts that we need.

22
00:00:54,30 --> 00:00:55,90
Now it's an asynchronous thing,

23
00:00:55,90 --> 00:00:58,00
because we go to Firebase and we say,

24
00:00:58,00 --> 00:00:59,90
hey, Firebase, could we have the post please?

25
00:00:59,90 --> 00:01:01,50
And then at some point in the future,

26
00:01:01,50 --> 00:01:04,30
it comes back with those posts,

27
00:01:04,30 --> 00:01:07,90
so let's, you can see there's a lambda here,

28
00:01:07,90 --> 00:01:11,20
we kind of don't know when this is gonna get called,

29
00:01:11,20 --> 00:01:12,90
so to do that, we say dispatch

30
00:01:12,90 --> 00:01:17,00
and then we say something like type posts

31
00:01:17,00 --> 00:01:19,30
and LOAD_POSTS_SUCCESS,

32
00:01:19,30 --> 00:01:21,80
I'll show you why I'm doing that in a minute,

33
00:01:21,80 --> 00:01:24,10
payload, and then we just pass that as payload,

34
00:01:24,10 --> 00:01:25,40
'cause this is actually the actual post,

35
00:01:25,40 --> 00:01:28,00
that's stored in Reddit, so that's great.

36
00:01:28,00 --> 00:01:31,60
So we need to import Firebase in here like this,

37
00:01:31,60 --> 00:01:34,00
'cause we're using Firebase, so this is okay,

38
00:01:34,00 --> 00:01:36,80
you can get asynchronous stuff like this, that's fine.

39
00:01:36,80 --> 00:01:39,20
So now we need to import, so we've got newPost,

40
00:01:39,20 --> 00:01:40,60
now we need to loadPosts.

41
00:01:40,60 --> 00:01:42,80
So then in the constructor, we can just call that,

42
00:01:42,80 --> 00:01:45,50
so this.prompts.dispatch loadPosts,

43
00:01:45,50 --> 00:01:47,30
so now we're calling this action,

44
00:01:47,30 --> 00:01:49,50
this may or may not work first time,

45
00:01:49,50 --> 00:01:51,80
so we will, we'll work through it together,

46
00:01:51,80 --> 00:01:52,60
until it's worked.

47
00:01:52,60 --> 00:01:55,10
Okay, Firebase is not found,

48
00:01:55,10 --> 00:01:57,00
is Firebase used somewhere else in here?

49
00:01:57,00 --> 00:02:00,80
Yes, so we're using Firebase here already,

50
00:02:00,80 --> 00:02:05,20
so let's back temporarily, very good, so hang on a sec,

51
00:02:05,20 --> 00:02:08,00
so this is kind of working.

52
00:02:08,00 --> 00:02:11,80
Cannot read property redditPosts of null in here,

53
00:02:11,80 --> 00:02:13,60
so 'cause we still reference this state,

54
00:02:13,60 --> 00:02:14,90
so let's go fix that.

55
00:02:14,90 --> 00:02:16,90
So here we're saying, I tell you what actually,

56
00:02:16,90 --> 00:02:19,30
let's see if the stuff that we did actually did work,

57
00:02:19,30 --> 00:02:22,10
okay, so LOAD_POST_SUCCESS,

58
00:02:22,10 --> 00:02:24,10
so you can see that action got fired,

59
00:02:24,10 --> 00:02:26,40
so Firebase has come back with some posts

60
00:02:26,40 --> 00:02:29,10
and you can see, oh yeah, that the posts are in there,

61
00:02:29,10 --> 00:02:30,70
great, so that's brilliant.

62
00:02:30,70 --> 00:02:35,80
So now all we need to do is, well, first of all actually,

63
00:02:35,80 --> 00:02:38,50
we've written the, I'll show you again,

64
00:02:38,50 --> 00:02:40,30
so you can see the action's fired,

65
00:02:40,30 --> 00:02:42,80
so the previous state posts currently have

66
00:02:42,80 --> 00:02:45,60
our dummy posts in, so that's not good, we need to fix that

67
00:02:45,60 --> 00:02:49,10
and then afterwards it still has the dummy posts in,

68
00:02:49,10 --> 00:02:50,40
so what we need to do now,

69
00:02:50,40 --> 00:02:51,80
if we're firing the correct action

70
00:02:51,80 --> 00:02:52,70
and you can see in the payload,

71
00:02:52,70 --> 00:02:54,30
we've gone to get all that stuff from Firebase,

72
00:02:54,30 --> 00:02:57,10
but we need to update Redux, the reducer

73
00:02:57,10 --> 00:02:59,80
to make sure that it's putting the stuff into the store,

74
00:02:59,80 --> 00:03:02,10
so if we go to the reducer, we wanna add,

75
00:03:02,10 --> 00:03:04,90
I'm just gonna copy this to make it easy,

76
00:03:04,90 --> 00:03:08,50
LOAD_POSTS_SUCCESS, I believe is the thing,

77
00:03:08,50 --> 00:03:10,40
so when LOAD_POSTS_SUCCESS happens,

78
00:03:10,40 --> 00:03:14,00
you probably just saw the payload, so action.payload,

79
00:03:14,00 --> 00:03:16,10
it's actually I think,

80
00:03:16,10 --> 00:03:18,50
we just wanna say this, we wanna say

81
00:03:18,50 --> 00:03:20,00
it sends us back all the posts every time,

82
00:03:20,00 --> 00:03:22,50
so we just say look, the posts are whatever

83
00:03:22,50 --> 00:03:23,70
the Firebase told us they were

84
00:03:23,70 --> 00:03:25,20
and we just put those in that,

85
00:03:25,20 --> 00:03:26,70
I'm gonna get rid of this one,

86
00:03:26,70 --> 00:03:28,60
because that's kind of just for playing around,

87
00:03:28,60 --> 00:03:31,50
post by default is gonna be an empty array,

88
00:03:31,50 --> 00:03:33,30
so we've set post to be an empty array,

89
00:03:33,30 --> 00:03:35,60
in fact I think post is a hash,

90
00:03:35,60 --> 00:03:38,40
so post by default is just gonna be an empty object,

91
00:03:38,40 --> 00:03:42,40
so there's nothing there, so let's see if that works,

92
00:03:42,40 --> 00:03:45,40
it's still broken, but in posts,

93
00:03:45,40 --> 00:03:47,60
you can see the current post index is still zero,

94
00:03:47,60 --> 00:03:49,70
but the posts are now loaded in here, brilliant.

95
00:03:49,70 --> 00:03:52,40
So now the problem is that you can see

96
00:03:52,40 --> 00:03:53,60
in this line of code here,

97
00:03:53,60 --> 00:03:56,10
we're still looking for the redditPosts in the wrong place,

98
00:03:56,10 --> 00:04:00,20
so now of course they will be in this.props.posts,

99
00:04:00,20 --> 00:04:03,60
because we're no longer using state, we're using props,

100
00:04:03,60 --> 00:04:06,30
because we've connected, okay,

101
00:04:06,30 --> 00:04:09,40
we log this.props, let's see what it returns,

102
00:04:09,40 --> 00:04:12,90
let's kill these off, because it was just us playing around.

103
00:04:12,90 --> 00:04:16,90
Ah, there we go, okay, cool, here is something for that,

104
00:04:16,90 --> 00:04:18,20
so again, now you can see,

105
00:04:18,20 --> 00:04:19,30
you can probably see there's a delay,

106
00:04:19,30 --> 00:04:20,90
so I'm gonna show you another common pattern

107
00:04:20,90 --> 00:04:22,80
I really like to use with Redux,

108
00:04:22,80 --> 00:04:26,80
so that is that we have like a isLoading,

109
00:04:26,80 --> 00:04:30,10
which we'll set to false initially

110
00:04:30,10 --> 00:04:34,30
and then we have a LOAD_POSTS_REQUEST here,

111
00:04:34,30 --> 00:04:37,70
where we will set isLoading to true

112
00:04:37,70 --> 00:04:41,80
and then when they come back, we set isLoading to false,

113
00:04:41,80 --> 00:04:43,90
this is quite a common thing I like to do,

114
00:04:43,90 --> 00:04:46,00
so those two are saying when they make the request,

115
00:04:46,00 --> 00:04:47,80
so that's, we also need to fire that action,

116
00:04:47,80 --> 00:04:49,90
so let's just do that real quick,

117
00:04:49,90 --> 00:04:53,30
so basically we say, 'cause we're using func again here,

118
00:04:53,30 --> 00:04:54,80
so we can dispatch more than one action,

119
00:04:54,80 --> 00:04:56,60
so I'm saying when we load the post, first of all,

120
00:04:56,60 --> 00:04:59,60
we're gonna make a posts/LOAD_POSTS_REQUEST,

121
00:04:59,60 --> 00:05:01,70
which just sort of says, look, we're gonna go

122
00:05:01,70 --> 00:05:04,50
and ask for this now and this will happen right away,

123
00:05:04,50 --> 00:05:06,80
because it's just on line one of this function,

124
00:05:06,80 --> 00:05:08,30
then we go to fire those to get the data,

125
00:05:08,30 --> 00:05:09,60
at some point in the future,

126
00:05:09,60 --> 00:05:11,60
then hopefully this gets fired with some stuff.

127
00:05:11,60 --> 00:05:15,10
Now you could also do an error one on catch as well,

128
00:05:15,10 --> 00:05:16,20
that's probably good practice in general,

129
00:05:16,20 --> 00:05:17,80
I'm not gonna do that today.

130
00:05:17,80 --> 00:05:18,90
So let's just see which actions fired

131
00:05:18,90 --> 00:05:21,50
and have a look at the store state,

132
00:05:21,50 --> 00:05:22,90
so you can see right off the bat,

133
00:05:22,90 --> 00:05:25,10
LOAD_POSTS_REQUEST comes in and then if you look

134
00:05:25,10 --> 00:05:27,70
at posts, you can see isLoading is true,

135
00:05:27,70 --> 00:05:30,60
later on, you can see that the posts come back

136
00:05:30,60 --> 00:05:33,40
and then after that, isLoading is false,

137
00:05:33,40 --> 00:05:35,40
so now what we can do is we've got

138
00:05:35,40 --> 00:05:38,00
this easy kind of isLoading thing going on,

139
00:05:38,00 --> 00:05:43,80
so in redditPost, we can say this.props.isLoading,

140
00:05:43,80 --> 00:05:45,80
then show like, I'm just going to do this

141
00:05:45,80 --> 00:05:48,30
really, like in a really basic way,

142
00:05:48,30 --> 00:05:52,10
so if isLoading in Redux store, if that's true,

143
00:05:52,10 --> 00:05:55,10
then show this little Loading like just inside some p tags,

144
00:05:55,10 --> 00:05:56,60
otherwise don't show anything

145
00:05:56,60 --> 00:05:58,60
and so what you see that does now,

146
00:05:58,60 --> 00:05:59,60
when you refresh the page,

147
00:05:59,60 --> 00:06:01,50
you see it says Loading, just for a second

148
00:06:01,50 --> 00:06:03,30
and then it switches to this,

149
00:06:03,30 --> 00:06:05,20
so that's how you can implement Loading

150
00:06:05,20 --> 00:06:07,10
and it's so easy to kind of like reason about,

151
00:06:07,10 --> 00:06:10,50
because you just look at these log things and you say okay,

152
00:06:10,50 --> 00:06:12,10
it's no longer loading, so it should show up

153
00:06:12,10 --> 00:06:14,80
and then your components get really simple.

154
00:06:14,80 --> 00:06:18,60
So we've now used Redux to load in the Firebase data

155
00:06:18,60 --> 00:06:20,90
for the Reddit posts into our app.

156
00:06:20,90 --> 00:06:22,20
In the next video, we're gonna look

157
00:06:22,20 --> 00:06:25,00
at how you can create a new post using Redux.

