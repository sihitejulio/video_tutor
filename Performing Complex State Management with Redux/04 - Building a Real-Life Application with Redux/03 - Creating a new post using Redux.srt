1
00:00:01,10 --> 00:00:03,20
- [Instructor] Creating a New Post Using Redux;

2
00:00:03,20 --> 00:00:05,10
in this video, we're gonna have a look at how we

3
00:00:05,10 --> 00:00:08,90
can use inputs with Redux, to store the currently editing

4
00:00:08,90 --> 00:00:12,50
post in our Redux state, so we've just loaded in the post

5
00:00:12,50 --> 00:00:15,30
from Firebase, into Redux state, and then we put that

6
00:00:15,30 --> 00:00:17,30
on the screen, and it was all pretty straightforward,

7
00:00:17,30 --> 00:00:19,20
so now, what we're gonna have a look at, is perhaps,

8
00:00:19,20 --> 00:00:23,70
how we can AddPost using Redux, so the code for that,

9
00:00:23,70 --> 00:00:26,90
really, is with this AddPost component, here.

10
00:00:26,90 --> 00:00:31,20
So we're doing a couple of things, we're saying

11
00:00:31,20 --> 00:00:36,80
onAdd, and we go and grab some data, and then we also

12
00:00:36,80 --> 00:00:41,30
push it; let's just have a look inside the AddPost

13
00:00:41,30 --> 00:00:47,80
component, itself, so this component has some state,

14
00:00:47,80 --> 00:00:51,50
so we're saving the input information in here,

15
00:00:51,50 --> 00:00:55,30
okay, so that's interesting, so this component's stateful,

16
00:00:55,30 --> 00:00:58,10
because we called this .setState, and this .state,

17
00:00:58,10 --> 00:01:00,70
and really, what happens with Redux as you Redux things,

18
00:01:00,70 --> 00:01:03,70
you're gonna get rid of stateful components in most places;

19
00:01:03,70 --> 00:01:06,60
not in all cases, and that's a very advanced topic,

20
00:01:06,60 --> 00:01:08,30
when do you not use Redux?

21
00:01:08,30 --> 00:01:10,00
When do you still use state?

22
00:01:10,00 --> 00:01:12,10
Maybe that's something we can cover in another video;

23
00:01:12,10 --> 00:01:14,30
let's have a look at how we could make this no longer

24
00:01:14,30 --> 00:01:16,20
use state, so I think the first thing we need to do

25
00:01:16,20 --> 00:01:19,70
is eliminate the setState from inside here,

26
00:01:19,70 --> 00:01:23,50
so to do that, I'm going to call prop, instead,

27
00:01:23,50 --> 00:01:25,80
so let me show you what I mean by that, so I'm gonna say

28
00:01:25,80 --> 00:01:33,90
this.props.onPostTitleChange,

29
00:01:33,90 --> 00:01:36,90
and I'm gonna parse the value

30
00:01:36,90 --> 00:01:39,60
of the text box, so you may be thinking,

31
00:01:39,60 --> 00:01:40,40
"what's going on?"

32
00:01:40,40 --> 00:01:42,80
It'll become clear in a sec, hang on, title,

33
00:01:42,80 --> 00:01:46,50
and then we'll say this.props.dispatch, and then we wanna

34
00:01:46,50 --> 00:01:50,60
dispatch that, so we'll say, maybe, updateTitle

35
00:01:50,60 --> 00:01:53,20
with the title, so updateTitle is gonna be a function

36
00:01:53,20 --> 00:01:55,70
that we create, it's an action, again, so let's have

37
00:01:55,70 --> 00:01:59,40
a look at that, so in here, create something,

38
00:01:59,40 --> 00:02:01,70
export const updatetitle,

39
00:02:01,70 --> 00:02:04,00
this one's really simple, it doesn't need any thunk,

40
00:02:04,00 --> 00:02:09,40
so you just say return type post UPDATE TITLE,

41
00:02:09,40 --> 00:02:12,90
and the payload will be the title,

42
00:02:12,90 --> 00:02:14,20
which you pass as a parameter.

43
00:02:14,20 --> 00:02:17,00
So then we're calling update title here,

44
00:02:17,00 --> 00:02:21,20
and parsing the title, and say every time they type

45
00:02:21,20 --> 00:02:26,30
into the box, the AddPost function, the input box

46
00:02:26,30 --> 00:02:29,20
onChange event fires, and it parses it back,

47
00:02:29,20 --> 00:02:31,90
it parses whatever is currently in the box back up here,

48
00:02:31,90 --> 00:02:34,10
and then let's just run it; I think it'll be way clearer

49
00:02:34,10 --> 00:02:38,10
to see if I just run it, as I'm typing stuff in this box,

50
00:02:38,10 --> 00:02:43,30
the actions get fired, so we've got the update title

51
00:02:43,30 --> 00:02:46,00
action getting fired with different payloads,

52
00:02:46,00 --> 00:02:48,00
so now, what we really need to do, is update

53
00:02:48,00 --> 00:02:55,00
the Redux state, so I'm just gonna grab the posts

54
00:02:55,00 --> 00:02:57,20
UPDATE TITLE, so then in here, we just need to add

55
00:02:57,20 --> 00:03:00,70
a case for that when UPDATE TITLE comes in,

56
00:03:00,70 --> 00:03:02,80
now I think we need to add a new part to our Redux state,

57
00:03:02,80 --> 00:03:07,90
so I would call it current, no, editing post,

58
00:03:07,90 --> 00:03:13,10
which starts off as having an empty title.

59
00:03:13,10 --> 00:03:16,00
What we want to say here, is okay, we're gonna keep

60
00:03:16,00 --> 00:03:18,30
the existing state, we want to update the editing post,

61
00:03:18,30 --> 00:03:21,60
and we now want to set the editing post to be whatever

62
00:03:21,60 --> 00:03:24,90
it was before, spread that in, so you got a kind of double

63
00:03:24,90 --> 00:03:27,20
spread thing, going on here, so we're saying keep all

64
00:03:27,20 --> 00:03:29,80
of the state, copy that in; we want to change editing post,

65
00:03:29,80 --> 00:03:32,40
inside editing post, we want to keep what was already

66
00:03:32,40 --> 00:03:35,20
inside editing post, but we want to change the title

67
00:03:35,20 --> 00:03:40,00
to be action.payload, which is the title that we parsed in.

68
00:03:40,00 --> 00:03:42,80
So now, if we run this, you can see I've had a lot

69
00:03:42,80 --> 00:03:44,80
of practice at doing this, like I do it every day

70
00:03:44,80 --> 00:03:48,30
for my job, so it's kind of easy, but it does take a little

71
00:03:48,30 --> 00:03:50,90
bit of time to get; you'll see, now as we type,

72
00:03:50,90 --> 00:03:53,60
so I hope you'll see as we type, here you go,

73
00:03:53,60 --> 00:03:59,20
update posts title, with abcdd, which is what's in here,

74
00:03:59,20 --> 00:04:03,40
and you'll see, in here, editing post now has a title,

75
00:04:03,40 --> 00:04:06,90
abcdd, so now we've got this thing in Redux state,

76
00:04:06,90 --> 00:04:10,20
which is kind of like the post that we're currently adding,

77
00:04:10,20 --> 00:04:13,40
so let's go back and finish taking out the old code.

78
00:04:13,40 --> 00:04:16,90
So when they're done, and they want to add the post,

79
00:04:16,90 --> 00:04:19,30
I'm just gonna call the onAdd prop, now, I'm not gonna

80
00:04:19,30 --> 00:04:21,70
parse in this this.state, 'cause that's no longer

81
00:04:21,70 --> 00:04:23,20
really a thing, 'cause we're not using it,

82
00:04:23,20 --> 00:04:25,80
and then here, there'll be no more Reddit post, here;

83
00:04:25,80 --> 00:04:29,90
you can see, here, we were adding a comments five submitted

84
00:04:29,90 --> 00:04:33,10
moment format; I'm gonna add that in the initial state,

85
00:04:33,10 --> 00:04:37,10
in the store, so five comments moment format, this has now

86
00:04:37,10 --> 00:04:39,70
become unintelligibly large; try and get this

87
00:04:39,70 --> 00:04:42,00
to actually fit on the screen, okay, great,

88
00:04:42,00 --> 00:04:46,90
so let's hang up date title, so we don't need this anymore,

89
00:04:46,90 --> 00:04:48,20
and really, all we want to do when we add.

90
00:04:48,20 --> 00:04:50,10
'Cause the only thing we ever do, really, from a component,

91
00:04:50,10 --> 00:04:52,40
is fire an action, and you can see we're doing some fire

92
00:04:52,40 --> 00:04:54,50
based stuff in here, but we don't really want to do that,

93
00:04:54,50 --> 00:04:59,10
so the action I'm gonna fire, here, is this.props.dispatch,

94
00:04:59,10 --> 00:05:01,80
and it's kind of like creating a new post in newbie light,

95
00:05:01,80 --> 00:05:07,20
so export const savePost, let's just call it savePost;

96
00:05:07,20 --> 00:05:09,00
I'll do dispatch and getState,

97
00:05:09,00 --> 00:05:10,10
I'll show you why in a minute.

98
00:05:10,10 --> 00:05:12,70
So we've got this savePosting, so let's call it now,

99
00:05:12,70 --> 00:05:14,70
so we just dispatch it, now what's interesting, here,

100
00:05:14,70 --> 00:05:17,70
is I've not parsed in the post like I was doing before,

101
00:05:17,70 --> 00:05:20,40
so I'll just cut that out of there, savePost needs to go

102
00:05:20,40 --> 00:05:24,50
up here, now, so that's great, save, and then just grab

103
00:05:24,50 --> 00:05:28,10
some code; I'll just cut, so actually in the wrong file,

104
00:05:28,10 --> 00:05:30,10
(laughing)

105
00:05:30,10 --> 00:05:33,90
okay, so when they save the post, we want to do firebase

106
00:05:33,90 --> 00:05:37,00
database ref; we want to push the new post in,

107
00:05:37,00 --> 00:05:40,00
now, we can go and get that post.

108
00:05:40,00 --> 00:05:41,00
How do we do that?

109
00:05:41,00 --> 00:05:43,70
We use getState, so we say get the state, posts,

110
00:05:43,70 --> 00:05:48,70
and it was like editingPost,

111
00:05:48,70 --> 00:05:51,40
yeah, editingPost, so now we've got the editingPost

112
00:05:51,40 --> 00:05:54,40
that's currently in the store, so we've got that here,

113
00:05:54,40 --> 00:05:58,30
and then we can just boom, save it, push it,

114
00:05:58,30 --> 00:06:01,10
and what's interesting, here, is actually, because of

115
00:06:01,10 --> 00:06:04,00
the way that Firebase works, and it's really brilliant

116
00:06:04,00 --> 00:06:06,60
how it works, it might just work without actually

117
00:06:06,60 --> 00:06:09,70
dispatching anything, which is kind of funny,

118
00:06:09,70 --> 00:06:12,50
so let's try, let's see; Firebase is pretty awesome

119
00:06:12,50 --> 00:06:16,20
at this stuff, so okay, not if moment is not defined,

120
00:06:16,20 --> 00:06:20,90
that one will help, moment from, one, two, three,

121
00:06:20,90 --> 00:06:23,60
and it's appeared, there, so why did that work?

122
00:06:23,60 --> 00:06:25,70
We didn't actually fire an action when we added it,

123
00:06:25,70 --> 00:06:27,90
so actually, in this case, we sort of don't need to,

124
00:06:27,90 --> 00:06:31,40
because what happens, is you add it, and then LOAD POSTS

125
00:06:31,40 --> 00:06:36,10
SUCCESS happens again, because it fires one every time

126
00:06:36,10 --> 00:06:39,30
something updates, so that's pretty much already working;

127
00:06:39,30 --> 00:06:41,50
what you can see, something that happened, there,

128
00:06:41,50 --> 00:06:44,20
is we did one, two, three, four, five, say Add,

129
00:06:44,20 --> 00:06:46,70
this box stays full; we don't really want that,

130
00:06:46,70 --> 00:06:49,70
so let's fix that, let's fire an action, after all.

131
00:06:49,70 --> 00:06:54,10
So the action I'm gonna fire is dispatch type, I'm just

132
00:06:54,10 --> 00:06:57,80
gonna file that to posts as SAVE POST, so all we want to do

133
00:06:57,80 --> 00:07:00,20
is really clear out whatever was in there,

134
00:07:00,20 --> 00:07:04,10
so this is a really cool example, 'cause if I update,

135
00:07:04,10 --> 00:07:07,70
SAVE POST, we say editingPost, so we want to say

136
00:07:07,70 --> 00:07:12,00
when we SAVE POST, we want to keep the state we've got

137
00:07:12,00 --> 00:07:16,40
already, but we set initialState.editingPost,

138
00:07:16,40 --> 00:07:21,60
so we just revert this back to this empty post at the top,

139
00:07:21,60 --> 00:07:22,70
so if we run that now.

140
00:07:22,70 --> 00:07:25,40
Actually, I suspect this might not work, but I'll show you

141
00:07:25,40 --> 00:07:28,10
why in a minute, but I'll show you how to debug it, as well;

142
00:07:28,10 --> 00:07:32,50
okay, so it's not worked; interesting, so when we did

143
00:07:32,50 --> 00:07:35,70
SAVE POST, so if we look at the store beforehand,

144
00:07:35,70 --> 00:07:37,80
editingPost still has one, two, three, four, five in it,

145
00:07:37,80 --> 00:07:41,20
which is correct; afterwards, maybe it shouldn't have that?

146
00:07:41,20 --> 00:07:44,40
It does not, so the store looks okay, so now, really,

147
00:07:44,40 --> 00:07:46,00
you can say, "well, the store's okay,

148
00:07:46,00 --> 00:07:49,30
"so does the component look okay?"

149
00:07:49,30 --> 00:07:50,40
what's going on?

150
00:07:50,40 --> 00:07:52,70
The reason, I think this is broken, I kind of preempted

151
00:07:52,70 --> 00:07:59,20
that it might be, is because in here, we're not parsing;

152
00:07:59,20 --> 00:08:04,60
we need to parse this through editingPost equals

153
00:08:04,60 --> 00:08:09,00
this.props.editingPost, and then inside AddPost, we'll have

154
00:08:09,00 --> 00:08:12,80
that available, and we can set the value of this input

155
00:08:12,80 --> 00:08:16,50
to be whatever; the value of this input is whatever

156
00:08:16,50 --> 00:08:19,40
is in the Redux store, that wasn't linked up;

157
00:08:19,40 --> 00:08:21,80
it's quite a common error that you run into,

158
00:08:21,80 --> 00:08:26,60
so now, when we do that, now it refreshes, so that's looking

159
00:08:26,60 --> 00:08:28,00
really good; looking great, so you can see that was

160
00:08:28,00 --> 00:08:30,80
the one we just added, and it's clearing out the box,

161
00:08:30,80 --> 00:08:33,70
so that's how you do a sort of input form,

162
00:08:33,70 --> 00:08:36,40
using Redux, and we've used it there, to add new posts.

163
00:08:36,40 --> 00:08:39,90
In this video, we looked at how we can use Redux

164
00:08:39,90 --> 00:08:43,70
when we're editing data, and how to make sure that the data

165
00:08:43,70 --> 00:08:47,40
that we're currently editing is bound to the input box,

166
00:08:47,40 --> 00:08:50,10
was editing the data; in the next vide, we're gonna look

167
00:08:50,10 --> 00:08:53,00
at Cross Referencing our Users with Selectors.

