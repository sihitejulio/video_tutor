1
00:00:01,00 --> 00:00:03,10
- [Instructor] Section four:

2
00:00:03,10 --> 00:00:07,10
Building a Real-Life Application with Redux.

3
00:00:07,10 --> 00:00:09,60
So in this section, we're gonna look at

4
00:00:09,60 --> 00:00:11,70
a walkthrough of the existing app.

5
00:00:11,70 --> 00:00:14,00
Walkthrough of an existing app.

6
00:00:14,00 --> 00:00:17,20
In this video, I'm gonna quickly cover

7
00:00:17,20 --> 00:00:20,50
the existing Reddit app and what code there is,

8
00:00:20,50 --> 00:00:22,20
how we're currently using State,

9
00:00:22,20 --> 00:00:24,40
and then I'll show you how to replace it with Redux

10
00:00:24,40 --> 00:00:26,30
over the coming videos.

11
00:00:26,30 --> 00:00:28,10
So let's have a quick run-through of what we've got.

12
00:00:28,10 --> 00:00:30,60
Now, if you've done some of our other courses,

13
00:00:30,60 --> 00:00:32,50
you might already recognize this code base

14
00:00:32,50 --> 00:00:35,90
as kind of a Reddit claim that we've previously started.

15
00:00:35,90 --> 00:00:39,40
So, we've scaffold this app using Create React app,

16
00:00:39,40 --> 00:00:41,50
just to keep it really simple.

17
00:00:41,50 --> 00:00:43,30
So, you've got App.js,

18
00:00:43,30 --> 00:00:46,30
which just wraps Reddit posts with a provider

19
00:00:46,30 --> 00:00:48,80
and we've got RedditPosts,

20
00:00:48,80 --> 00:00:51,60
which just displays all of the posts on the screen.

21
00:00:51,60 --> 00:00:54,00
You can add a new one.

22
00:00:54,00 --> 00:00:55,70
We've got an individual RedditPost,

23
00:00:55,70 --> 00:00:57,90
which represents just one Reddit post.

24
00:00:57,90 --> 00:01:02,50
And we're using Firebase to store the Reddit post

25
00:01:02,50 --> 00:01:06,10
and most of the Firebase stuff is

26
00:01:06,10 --> 00:01:09,10
done in the line, so, like, here,

27
00:01:09,10 --> 00:01:11,20
in things like constructors or component

28
00:01:11,20 --> 00:01:12,80
or mount or whatever.

29
00:01:12,80 --> 00:01:14,60
And so we're gonna switch this up

30
00:01:14,60 --> 00:01:19,50
and use a different, use Redux to do it all instead.

31
00:01:19,50 --> 00:01:21,40
We just had a quick walkthrough of the app

32
00:01:21,40 --> 00:01:22,90
to look at where we're using State

33
00:01:22,90 --> 00:01:24,50
and how everything's put together.

34
00:01:24,50 --> 00:01:26,10
So now we're ready to go on and

35
00:01:26,10 --> 00:01:28,40
replace it with Redux.

36
00:01:28,40 --> 00:01:30,00
In the next video, we're gonna look at

37
00:01:30,00 --> 00:01:32,00
loading the Reddit post using Redux.

