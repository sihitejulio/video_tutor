1
00:00:01,30 --> 00:00:04,20
- [Instructor] Cross Referencing Users with Selectors.

2
00:00:04,20 --> 00:00:06,00
In this video we're going to take a look at

3
00:00:06,00 --> 00:00:09,30
how we can use selectors to cross-reference users

4
00:00:09,30 --> 00:00:12,20
with Reddit posts to provide us with Reddit posts

5
00:00:12,20 --> 00:00:15,20
which have the users name inside of them.

6
00:00:15,20 --> 00:00:18,60
So, let's get started, and the first thing I'm gonna do

7
00:00:18,60 --> 00:00:21,10
is I'm gonna add a second reducer here

8
00:00:21,10 --> 00:00:22,60
I've created for users.

9
00:00:22,60 --> 00:00:24,30
so let's pretend that our Reddit posts

10
00:00:24,30 --> 00:00:26,20
also have some users.

11
00:00:26,20 --> 00:00:30,00
I'm just gonna copy the post stuff, slid it down a bit.

12
00:00:30,00 --> 00:00:33,00
I'm gonna create some users, so user number one,

13
00:00:33,00 --> 00:00:35,50
name is Richard,

14
00:00:35,50 --> 00:00:36,30
That'll do.

15
00:00:36,30 --> 00:00:37,60
Think that will do.

16
00:00:37,60 --> 00:00:41,40
So I've created a new, don't need moment anymore.

17
00:00:41,40 --> 00:00:44,50
So now I've got this users selector.

18
00:00:44,50 --> 00:00:46,60
Great, how do we get that into our application?

19
00:00:46,60 --> 00:00:48,70
That's something that you may or may not remember

20
00:00:48,70 --> 00:00:49,60
from an earlier video.

21
00:00:49,60 --> 00:00:52,00
So you go to the rootReducer's file

22
00:00:52,00 --> 00:00:55,60
in this code base, you import the users reducer

23
00:00:55,60 --> 00:00:58,20
and then you put it in here like that.

24
00:00:58,20 --> 00:00:59,70
Now it will be there.

25
00:00:59,70 --> 00:01:02,30
Cool, so we've got this users,

26
00:01:02,30 --> 00:01:05,00
it's an object by user ID,

27
00:01:05,00 --> 00:01:06,90
and now what I'm gonna do is,

28
00:01:06,90 --> 00:01:08,30
yeah, let's check that that's worked

29
00:01:08,30 --> 00:01:10,30
so that we can see everything working properly.

30
00:01:10,30 --> 00:01:12,30
So if you look here, got the users,

31
00:01:12,30 --> 00:01:13,80
and, great, user one name, Richard.

32
00:01:13,80 --> 00:01:15,60
Great, so now what I wanna do

33
00:01:15,60 --> 00:01:19,10
is if one of the posts has my userId on it,

34
00:01:19,10 --> 00:01:20,60
then I wanna display my name.

35
00:01:20,60 --> 00:01:21,40
First think I'm gonna do

36
00:01:21,40 --> 00:01:24,30
is make sure that all new posts have a userId.

37
00:01:24,30 --> 00:01:25,40
I'm just gonna hard code it,

38
00:01:25,40 --> 00:01:27,10
but still pretty useful.

39
00:01:27,10 --> 00:01:29,60
So here, userId one.

40
00:01:29,60 --> 00:01:32,00
So all new posts we create

41
00:01:32,00 --> 00:01:33,20
will have the userId of one.

42
00:01:33,20 --> 00:01:36,10
So the next thing I'm gonna do is in the post selector.

43
00:01:36,10 --> 00:01:37,60
So let's have a quick look at the state.

44
00:01:37,60 --> 00:01:38,50
Actually I'll show you.

45
00:01:38,50 --> 00:01:39,60
Let's create a new one.

46
00:01:39,60 --> 00:01:40,40
All right, so that one's in there.

47
00:01:40,40 --> 00:01:42,40
So now if we look at,

48
00:01:42,40 --> 00:01:45,30
this is the selector state,

49
00:01:45,30 --> 00:01:48,70
and you can see really here what the selector produces,

50
00:01:48,70 --> 00:01:51,60
and you can see that somewhere in here

51
00:01:51,60 --> 00:01:54,60
some of these posts now have the userId of one.

52
00:01:54,60 --> 00:01:57,20
So this one does, but what we really want

53
00:01:57,20 --> 00:01:59,60
in here is for it to say username

54
00:01:59,60 --> 00:02:01,20
and say Richard, right?

55
00:02:01,20 --> 00:02:02,70
And so we've got that information

56
00:02:02,70 --> 00:02:06,70
'cause we can look it up in the users part state.

57
00:02:06,70 --> 00:02:08,70
We need to cross reference basically.

58
00:02:08,70 --> 00:02:10,90
So we're gonna do that in the selectors.

59
00:02:10,90 --> 00:02:12,30
The selector just make it really easy for us

60
00:02:12,30 --> 00:02:13,90
and to adds that to the post.

61
00:02:13,90 --> 00:02:15,60
So here's a selector for posts.

62
00:02:15,60 --> 00:02:17,70
Now we're also gonna need the user part state.

63
00:02:17,70 --> 00:02:19,00
So let's bring that in.

64
00:02:19,00 --> 00:02:20,00
So you do that.

65
00:02:20,00 --> 00:02:21,70
As I mentioned in the previous video,

66
00:02:21,70 --> 00:02:24,30
you then say usersState.

67
00:02:24,30 --> 00:02:25,10
So now we've got that.

68
00:02:25,10 --> 00:02:26,00
Great.

69
00:02:26,00 --> 00:02:27,50
So now this has users in it,

70
00:02:27,50 --> 00:02:28,90
and so what we're saying here is,

71
00:02:28,90 --> 00:02:30,80
we actually wanna change posts as well,

72
00:02:30,80 --> 00:02:33,80
and what we're gonna do is map over the posts.

73
00:02:33,80 --> 00:02:37,50
So that postsState.posts,

74
00:02:37,50 --> 00:02:38,50
bridge post.

75
00:02:38,50 --> 00:02:41,20
We want the existing post.

76
00:02:41,20 --> 00:02:44,10
So we can use the spread operator again,

77
00:02:44,10 --> 00:02:46,00
but we will say one like userName,

78
00:02:46,00 --> 00:02:49,80
which is like post.userId.

79
00:02:49,80 --> 00:02:51,10
So let's grab the users up here.

80
00:02:51,10 --> 00:02:54,80
So const the users equals usersState.users.

81
00:02:54,80 --> 00:02:55,80
So this is the object.

82
00:02:55,80 --> 00:02:57,10
So we can look up,

83
00:02:57,10 --> 00:02:59,50
I'm gonna use underscore to do this

84
00:02:59,50 --> 00:03:00,80
'cause otherwise I think it's not gonna be

85
00:03:00,80 --> 00:03:02,00
there for all the users.

86
00:03:02,00 --> 00:03:05,20
So we're using lodash in a couple of places here.

87
00:03:05,20 --> 00:03:07,40
So before we said is,

88
00:03:07,40 --> 00:03:08,90
I'll note that you need to really be familiar

89
00:03:08,90 --> 00:03:10,70
with map and all these other functions,

90
00:03:10,70 --> 00:03:13,30
but we're saying bridge post, grab the post,

91
00:03:13,30 --> 00:03:14,70
copy the existing post in,

92
00:03:14,70 --> 00:03:17,50
and then set the userName to be _.get,

93
00:03:17,50 --> 00:03:20,00
which is basically the same as writing

94
00:03:20,00 --> 00:03:23,60
users post.userId.

95
00:03:23,60 --> 00:03:24,50
Basically it's that

96
00:03:24,50 --> 00:03:27,10
except for if this doesn't exist in here,

97
00:03:27,10 --> 00:03:29,80
it doesn't produce an index that bounds exception

98
00:03:29,80 --> 00:03:31,20
or anything like that.

99
00:03:31,20 --> 00:03:33,50
So let's just see if that worked.

100
00:03:33,50 --> 00:03:34,50
Maybe it won't work.

101
00:03:34,50 --> 00:03:35,50
It's a little bit complicated.

102
00:03:35,50 --> 00:03:37,70
So probably I've got it wrong.

103
00:03:37,70 --> 00:03:38,50
Failed to compile.

104
00:03:38,50 --> 00:03:39,50
Not even close.

105
00:03:39,50 --> 00:03:41,30
So what's up with that?

106
00:03:41,30 --> 00:03:43,10
You need an extra set of brackets,

107
00:03:43,10 --> 00:03:44,00
otherwise it gets confused

108
00:03:44,00 --> 00:03:46,40
because this means the start of a new function.

109
00:03:46,40 --> 00:03:47,50
Okay, great.

110
00:03:47,50 --> 00:03:49,10
Well also, it's not blowing up anymore.

111
00:03:49,10 --> 00:03:51,60
Let's have a look at the post.

112
00:03:51,60 --> 00:03:53,40
We can see we're getting userName undefined

113
00:03:53,40 --> 00:03:54,80
in most of these.

114
00:03:54,80 --> 00:03:55,90
We need to find the one,

115
00:03:55,90 --> 00:03:56,90
here you go,

116
00:03:56,90 --> 00:03:58,80
but that doesn't look like it's worked.

117
00:03:58,80 --> 00:04:00,00
That's close.

118
00:04:00,00 --> 00:04:00,80
It's really close.

119
00:04:00,80 --> 00:04:03,50
So userName here is actually an object

120
00:04:03,50 --> 00:04:05,10
with name Richard.

121
00:04:05,10 --> 00:04:08,40
We wanna get the userId, and then we wanna get user.

122
00:04:08,40 --> 00:04:10,00
So you can do that using lodash like this.

123
00:04:10,00 --> 00:04:12,80
So this is the same as going post.

124
00:04:12,80 --> 00:04:14,70
So this is the same as saying

125
00:04:14,70 --> 00:04:18,30
users post.userId

126
00:04:18,30 --> 00:04:19,70
.name,

127
00:04:19,70 --> 00:04:20,90
but in a null safe way.

128
00:04:20,90 --> 00:04:23,30
So let's call lodash.

129
00:04:23,30 --> 00:04:25,70
Get's a really good feature for null safety.

130
00:04:25,70 --> 00:04:26,80
It's great.

131
00:04:26,80 --> 00:04:29,10
So let's have a look now.

132
00:04:29,10 --> 00:04:31,00
So it's still some undefineds.

133
00:04:31,00 --> 00:04:33,40
Lucky number

134
00:04:33,40 --> 00:04:34,30
10,

135
00:04:34,30 --> 00:04:35,10
nine.

136
00:04:35,10 --> 00:04:35,90
Now, okay.

137
00:04:35,90 --> 00:04:36,90
So you can see on this one now

138
00:04:36,90 --> 00:04:38,30
it's got a userName of Richard.

139
00:04:38,30 --> 00:04:39,90
Let's just add that into the UI

140
00:04:39,90 --> 00:04:41,30
so we can see what's going on.

141
00:04:41,30 --> 00:04:42,50
So we'll just add that as a prop

142
00:04:42,50 --> 00:04:44,80
into our redditPost,

143
00:04:44,80 --> 00:04:47,30
and then here we'll probably have comments

144
00:04:47,30 --> 00:04:50,30
from now, and then maybe just include the userName.

145
00:04:50,30 --> 00:04:53,60
So that's props., do the trick.

146
00:04:53,60 --> 00:04:54,40
Let's have a look.

147
00:04:54,40 --> 00:04:56,80
So most of these are empty.

148
00:04:56,80 --> 00:04:58,90
This one you can see has Richard there.

149
00:04:58,90 --> 00:04:59,80
So if we create another one,

150
00:04:59,80 --> 00:05:01,20
that should also have Richard.

151
00:05:01,20 --> 00:05:03,90
Yeah.

152
00:05:03,90 --> 00:05:05,50
So that's how you can cross reference

153
00:05:05,50 --> 00:05:07,20
two bits of Redux state.

154
00:05:07,20 --> 00:05:10,30
So we're cross referencing posts from users.

155
00:05:10,30 --> 00:05:11,80
So we're finding the post

156
00:05:11,80 --> 00:05:14,60
which have a userId that matches it up in here,

157
00:05:14,60 --> 00:05:17,30
and then we're returning it in the selector

158
00:05:17,30 --> 00:05:19,80
so that the posts look how we want them to look.

159
00:05:19,80 --> 00:05:21,80
So this post for example looks just find,

160
00:05:21,80 --> 00:05:23,70
and it's got comments, and it's got a userName in it,

161
00:05:23,70 --> 00:05:25,60
but the post doesn't actually have a userName.

162
00:05:25,60 --> 00:05:27,20
That's something we're doing in reselect.

163
00:05:27,20 --> 00:05:28,60
Really powerful technique.

164
00:05:28,60 --> 00:05:29,60
I really recommend it.

165
00:05:29,60 --> 00:05:31,20
So it's just a like a more advanced example

166
00:05:31,20 --> 00:05:33,00
of how to use reselect.

167
00:05:33,00 --> 00:05:35,40
We learnt how to build a real-life Reddit application

168
00:05:35,40 --> 00:05:36,80
with Redux.

169
00:05:36,80 --> 00:05:54,00
Thanks very much for watching.

