1
00:00:00,50 --> 00:00:03,80
- [Instructor] Section 2, Using Redux in Your Applications.

2
00:00:03,80 --> 00:00:05,00
In this section, we're going to learn

3
00:00:05,00 --> 00:00:07,80
how to connect your components to the store,

4
00:00:07,80 --> 00:00:10,10
how to fire actions,

5
00:00:10,10 --> 00:00:13,60
we're going to do a recap of what ES6/7 spreading is,

6
00:00:13,60 --> 00:00:16,20
because it's really useful for Redux,

7
00:00:16,20 --> 00:00:17,40
and we're going to look at applying

8
00:00:17,40 --> 00:00:19,70
Redux reducers to the application.

9
00:00:19,70 --> 00:00:22,50
Connecting Your Components to the Store.

10
00:00:22,50 --> 00:00:24,30
In this video, we are going to take a quick look at

11
00:00:24,30 --> 00:00:27,50
how you can connect your components to the store.

12
00:00:27,50 --> 00:00:28,90
Which means, allowing your components

13
00:00:28,90 --> 00:00:31,90
to see the data that's in the store.

14
00:00:31,90 --> 00:00:33,40
I've taken the provider

15
00:00:33,40 --> 00:00:35,30
which was wrapping Lasernator's stuff,

16
00:00:35,30 --> 00:00:36,50
and I moved all that stuff

17
00:00:36,50 --> 00:00:38,40
into its own file, called redditPosts.

18
00:00:38,40 --> 00:00:41,00
You probably wanna do this in your app as well.

19
00:00:41,00 --> 00:00:42,40
It's the same code as before,

20
00:00:42,40 --> 00:00:46,20
it's now just split over app.js and redditPosts.

21
00:00:46,20 --> 00:00:49,60
RedditPosts looks exactly the same as how app.js looked

22
00:00:49,60 --> 00:00:52,00
originally when we started this course.

23
00:00:52,00 --> 00:00:54,20
You've got this, we've just covered the store,

24
00:00:54,20 --> 00:00:57,00
and we can see that there's stuff in the store.

25
00:00:57,00 --> 00:00:59,90
Now the question is, how do we get our components

26
00:00:59,90 --> 00:01:02,30
to be able to see what's in the store?

27
00:01:02,30 --> 00:01:04,80
How do we get access to what's in the store in code

28
00:01:04,80 --> 00:01:07,10
in a component and just programmatically.

29
00:01:07,10 --> 00:01:08,80
So to start off with, I'm gonna show you

30
00:01:08,80 --> 00:01:11,50
how you get access to it programmatically,

31
00:01:11,50 --> 00:01:12,70
not in a component.

32
00:01:12,70 --> 00:01:17,40
So to do that, let's say in app.js we've got

33
00:01:17,40 --> 00:01:19,90
access to the store here, so this is the store object

34
00:01:19,90 --> 00:01:21,40
and we're passing it into provider.

35
00:01:21,40 --> 00:01:22,60
I'm just gonna console.log this,

36
00:01:22,60 --> 00:01:24,90
just so we can have a little look at the store.

37
00:01:24,90 --> 00:01:28,00
And now in the programs running,

38
00:01:28,00 --> 00:01:30,90
So this is the store, you can see what stuff you can do.

39
00:01:30,90 --> 00:01:32,60
Dispatch, which we did in the last video,

40
00:01:32,60 --> 00:01:34,30
so you can dispatch an action.

41
00:01:34,30 --> 00:01:36,50
We'll cover that in more detail in another video.

42
00:01:36,50 --> 00:01:40,30
GetState, replace reducer, subscribe.

43
00:01:40,30 --> 00:01:43,10
So the one we wanna look at is called getState.

44
00:01:43,10 --> 00:01:46,70
So let's quickly log that, store.getState.

45
00:01:46,70 --> 00:01:50,20
Let's have a look at what happens when we do that.

46
00:01:50,20 --> 00:01:52,30
So now we can see the state of the store,

47
00:01:52,30 --> 00:01:53,80
which has posts in it.

48
00:01:53,80 --> 00:01:56,00
And that is consistent with

49
00:01:56,00 --> 00:01:58,10
what we were seeing before in the last video.

50
00:01:58,10 --> 00:02:00,60
So that's really simple, I just wanted to show you that.

51
00:02:00,60 --> 00:02:03,80
This is how you get the current state of the store in code,

52
00:02:03,80 --> 00:02:06,00
if you've got a store object,

53
00:02:06,00 --> 00:02:09,20
which we're importing here form our store which we created.

54
00:02:09,20 --> 00:02:11,50
I'll show you how you create a store in a later video.

55
00:02:11,50 --> 00:02:14,40
But yeah, so that's how you get the whole state.

56
00:02:14,40 --> 00:02:17,10
So the next question is, how do you put stuff

57
00:02:17,10 --> 00:02:20,30
that's in the redux store on the screen?

58
00:02:20,30 --> 00:02:23,80
So to use that, we use a library called, react redux,

59
00:02:23,80 --> 00:02:26,40
which we've already installed in this project.

60
00:02:26,40 --> 00:02:27,50
So let's have a look.

61
00:02:27,50 --> 00:02:30,50
So the first thing is we need to import it.

62
00:02:30,50 --> 00:02:35,40
So let me show you how you do that.

63
00:02:35,40 --> 00:02:37,30
So we just import by saying, import,

64
00:02:37,30 --> 00:02:40,80
we import this connection function from react-redux.

65
00:02:40,80 --> 00:02:43,00
The next thing is to use the connect function.

66
00:02:43,00 --> 00:02:46,40
So before we were export default RedditPosts,

67
00:02:46,40 --> 00:02:50,00
now we export default and then we connect,

68
00:02:50,00 --> 00:02:52,60
we pass some sort of lander or a function in here,

69
00:02:52,60 --> 00:02:55,10
and then we pass in RedditPosts in at the end.

70
00:02:55,10 --> 00:02:58,10
Now, what this function does

71
00:02:58,10 --> 00:03:01,90
is it wraps RedditPosts and applies this function

72
00:03:01,90 --> 00:03:04,60
to the state and gives access to this thing in props.

73
00:03:04,60 --> 00:03:06,10
Gives access to the store in props.

74
00:03:06,10 --> 00:03:07,30
Now, I said all that quite quickly,

75
00:03:07,30 --> 00:03:09,10
so let me just go over that point by point.

76
00:03:09,10 --> 00:03:11,00
So the first thing is you call in the connect function

77
00:03:11,00 --> 00:03:13,80
to connect your store to your component.

78
00:03:13,80 --> 00:03:17,40
So we've got connect the store to the component,

79
00:03:17,40 --> 00:03:18,90
and you pass in the last argument

80
00:03:18,90 --> 00:03:21,40
is the component that you wanna connect the store to.

81
00:03:21,40 --> 00:03:24,40
And then, in the first argument, what's going on here?

82
00:03:24,40 --> 00:03:26,50
So it's a lander with one argument,

83
00:03:26,50 --> 00:03:29,50
and that argument is often called state.

84
00:03:29,50 --> 00:03:31,60
Now, state is the thing when we load getState,

85
00:03:31,60 --> 00:03:33,70
so at the moment, our state only has posts in it.

86
00:03:33,70 --> 00:03:35,50
So it's the state of the whole redux store

87
00:03:35,50 --> 00:03:37,20
that we saw in the last video.

88
00:03:37,20 --> 00:03:39,20
And then you can just grab a piece of it,

89
00:03:39,20 --> 00:03:40,40
or grab some pieces of it.

90
00:03:40,40 --> 00:03:41,80
Or you can do whatever you want in here.

91
00:03:41,80 --> 00:03:45,20
So I've just done state.posts,

92
00:03:45,20 --> 00:03:47,80
say I don't want everything that's in the store,

93
00:03:47,80 --> 00:03:49,50
I just want the posts.

94
00:03:49,50 --> 00:03:51,50
But you could just say state => state

95
00:03:51,50 --> 00:03:53,20
and you get the whole state.

96
00:03:53,20 --> 00:03:55,10
But usually you'll just want a bit.

97
00:03:55,10 --> 00:03:56,90
You can run more complicated code in here,

98
00:03:56,90 --> 00:03:58,70
that maybe does some logic if you want,

99
00:03:58,70 --> 00:04:00,70
we'll come into that in a later video as well.

100
00:04:00,70 --> 00:04:02,30
So this is kind of the pattern.

101
00:04:02,30 --> 00:04:05,60
This whole thing I've selected here returns a component.

102
00:04:05,60 --> 00:04:07,40
So it returns something which will look like

103
00:04:07,40 --> 00:04:11,20
a Reddit post component, but it will have certain props.

104
00:04:11,20 --> 00:04:12,60
So let me show you what that does.

105
00:04:12,60 --> 00:04:14,00
So you can see up here I've added a line,

106
00:04:14,00 --> 00:04:16,50
console.log(this.props).

107
00:04:16,50 --> 00:04:17,90
So we're just gonna see what the props are.

108
00:04:17,90 --> 00:04:19,40
So now when we run the program,

109
00:04:19,40 --> 00:04:23,40
you can see it's logged the props

110
00:04:23,40 --> 00:04:26,70
and the props have dispatch in there.

111
00:04:26,70 --> 00:04:29,20
So that's just a shorthand to be able to dispatch an action,

112
00:04:29,20 --> 00:04:30,40
we'll come on to that later.

113
00:04:30,40 --> 00:04:32,60
And it has posts, because we told it to.

114
00:04:32,60 --> 00:04:35,10
So this is the post part of the store.

115
00:04:35,10 --> 00:04:36,30
And now they're in props.

116
00:04:36,30 --> 00:04:38,80
Which means that you can like,

117
00:04:38,80 --> 00:04:40,50
let's just dump them on the screen real fast,

118
00:04:40,50 --> 00:04:41,80
so I'm just gonna put them in here.

119
00:04:41,80 --> 00:04:45,30
So, this.props.posts, let's see if that works

120
00:04:45,30 --> 00:04:46,80
and then what that will do.

121
00:04:46,80 --> 00:04:47,80
Maybe break.

122
00:04:47,80 --> 00:04:49,70
No, you can see it here, post one.

123
00:04:49,70 --> 00:04:53,80
So it's put this thing on the screen.

124
00:04:53,80 --> 00:04:56,90
So it's really simple, use the connect function

125
00:04:56,90 --> 00:04:59,10
to take some part of the store,

126
00:04:59,10 --> 00:05:00,90
so here I've done state.posts,

127
00:05:00,90 --> 00:05:02,70
so just the posts part of the store,

128
00:05:02,70 --> 00:05:06,70
and then it becomes available in this.props.

129
00:05:06,70 --> 00:05:09,40
And now the magic of redux,

130
00:05:09,40 --> 00:05:12,30
if the state of the store changes,

131
00:05:12,30 --> 00:05:14,40
then these props would just change automatically.

132
00:05:14,40 --> 00:05:16,10
So you don't really have to worry about,

133
00:05:16,10 --> 00:05:17,60
oh, you know, what's going on in the store,

134
00:05:17,60 --> 00:05:19,00
you just say, this.props.

135
00:05:19,00 --> 00:05:20,50
the thing from the store you wanted,

136
00:05:20,50 --> 00:05:22,80
that you've like, connected to here,

137
00:05:22,80 --> 00:05:25,00
and if that thing changes, the react will

138
00:05:25,00 --> 00:05:27,40
automatically re-render this component

139
00:05:27,40 --> 00:05:30,40
and this.props.posts will be a different value.

140
00:05:30,40 --> 00:05:32,70
So it just kinda takes care of it for you.

141
00:05:32,70 --> 00:05:34,20
So you don't really have to worry,

142
00:05:34,20 --> 00:05:36,30
your components just kind of like,

143
00:05:36,30 --> 00:05:37,90
which bits of state do I need access to,

144
00:05:37,90 --> 00:05:40,20
and they're gonna be accessible in props,

145
00:05:40,20 --> 00:05:41,80
and then I put them on the screen.

146
00:05:41,80 --> 00:05:42,90
So it's really simple,

147
00:05:42,90 --> 00:05:45,40
and that's how you grab information from the store

148
00:05:45,40 --> 00:05:47,90
and put it on the screen of your react app.

149
00:05:47,90 --> 00:05:49,80
At this video, we looked at connecting

150
00:05:49,80 --> 00:05:51,30
your components to the store,

151
00:05:51,30 --> 00:05:53,60
the libraries that you need to do that,

152
00:05:53,60 --> 00:05:56,60
and how it works in practice with some code examples.

153
00:05:56,60 --> 00:05:58,00
In the next video,

154
00:05:58,00 --> 00:06:00,00
we're gonna take a look at firing actions.

