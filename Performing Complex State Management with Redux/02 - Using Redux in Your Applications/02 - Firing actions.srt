1
00:00:00,80 --> 00:00:03,00
- [Instructor] Firing Actions.

2
00:00:03,00 --> 00:00:04,20
In this video, we're gonna look at how

3
00:00:04,20 --> 00:00:06,30
you fire an action in Redux and

4
00:00:06,30 --> 00:00:08,40
what that looks like when you do it.

5
00:00:08,40 --> 00:00:09,60
I wanna jump straight into it.

6
00:00:09,60 --> 00:00:11,90
It's really easy to kind of do.

7
00:00:11,90 --> 00:00:15,00
You don't really know yet what's the point of firing action

8
00:00:15,00 --> 00:00:16,40
but I just want to show you how to fire them

9
00:00:16,40 --> 00:00:18,40
so you kind of know and it will help inform you

10
00:00:18,40 --> 00:00:20,50
in some of the videos that are coming up.

11
00:00:20,50 --> 00:00:23,20
So, in our code here,

12
00:00:23,20 --> 00:00:24,70
first of all, I'll show you how to fire an action

13
00:00:24,70 --> 00:00:26,30
if you've got a store object.

14
00:00:26,30 --> 00:00:28,10
Now you're probably not going to have a store object

15
00:00:28,10 --> 00:00:30,30
all that often, but when I learned Redux

16
00:00:30,30 --> 00:00:32,00
no one really told me I could do this and

17
00:00:32,00 --> 00:00:34,30
I think it is actually quite a useful thing to know.

18
00:00:34,30 --> 00:00:36,90
I always use the Connect stuff

19
00:00:36,90 --> 00:00:38,80
like we were just using in the last video

20
00:00:38,80 --> 00:00:40,90
but you can just use it from a store.

21
00:00:40,90 --> 00:00:42,30
So to fire an action, you say

22
00:00:42,30 --> 00:00:44,60
"store.dispatch" type,

23
00:00:44,60 --> 00:00:47,50
and then we say type, which by convention,

24
00:00:47,50 --> 00:00:48,40
will be like,

25
00:00:48,40 --> 00:00:49,70
I like to do like this.

26
00:00:49,70 --> 00:00:52,20
Post, new post.

27
00:00:52,20 --> 00:00:54,20
So you would have uppercase here

28
00:00:54,20 --> 00:00:55,90
and the name of the reducer here.

29
00:00:55,90 --> 00:00:56,80
We'll cover that later,

30
00:00:56,80 --> 00:00:58,50
but you can really call it anything you want.

31
00:00:58,50 --> 00:01:00,10
You could call it like that.

32
00:01:00,10 --> 00:01:02,70
And then you can pass payload as well.

33
00:01:02,70 --> 00:01:06,60
I'm gonna pass a payload in this case.

34
00:01:06,60 --> 00:01:08,20
Just any option will do.

35
00:01:08,20 --> 00:01:10,50
So we're dispatching this object like this

36
00:01:10,50 --> 00:01:11,90
so let's check that out.

37
00:01:11,90 --> 00:01:14,00
So you can see, the action gets fired here.

38
00:01:14,00 --> 00:01:16,60
Now, the Redux logger stuff makes it super easy to see

39
00:01:16,60 --> 00:01:18,50
what's going on, but you can see

40
00:01:18,50 --> 00:01:22,00
the state beforehand, the state of the store after hand.

41
00:01:22,00 --> 00:01:24,80
So these are both stores, the state of your stores,

42
00:01:24,80 --> 00:01:28,30
so before the action, we had one post,

43
00:01:28,30 --> 00:01:30,30
it says post one in an array.

44
00:01:30,30 --> 00:01:32,20
Afterwards, the same thing and then

45
00:01:32,20 --> 00:01:33,60
we can see the action in here

46
00:01:33,60 --> 00:01:36,40
and you can see in the payload, it was what we put in.

47
00:01:36,40 --> 00:01:37,50
We fired an action.

48
00:01:37,50 --> 00:01:39,30
Let me show you how to do that from a component.

49
00:01:39,30 --> 00:01:41,50
So, I'm just going to copy this bit

50
00:01:41,50 --> 00:01:43,30
and then we're going to go to RedditPosts

51
00:01:43,30 --> 00:01:45,60
which is already connected from the last video.

52
00:01:45,60 --> 00:01:46,80
So we've already connected

53
00:01:46,80 --> 00:01:49,20
and as I sort of showed you in the last video,

54
00:01:49,20 --> 00:01:51,30
when you connect like this,

55
00:01:51,30 --> 00:01:54,40
you can then say this.props

56
00:01:54,40 --> 00:01:57,80
and then it connect, adds a thing called dispatch

57
00:01:57,80 --> 00:02:00,00
and then just like before, you can do that.

58
00:02:00,00 --> 00:02:01,70
And this is pretty much the same as saying

59
00:02:01,70 --> 00:02:03,50
what we just had stored in dispatch.

60
00:02:03,50 --> 00:02:05,30
Does exactly the same thing.

61
00:02:05,30 --> 00:02:07,50
So let me make sure I deleted,

62
00:02:07,50 --> 00:02:09,50
I combed that one out, let's have a look.

63
00:02:09,50 --> 00:02:12,90
So now if we refresh, you can see new post

64
00:02:12,90 --> 00:02:16,30
still being dispatched exactly the same thing.

65
00:02:16,30 --> 00:02:18,70
So that's really easy and that's how you dispatch.

66
00:02:18,70 --> 00:02:21,00
So maybe someone clicks a button or

67
00:02:21,00 --> 00:02:24,20
when a component's loaded or whatever it is you want to do.

68
00:02:24,20 --> 00:02:27,00
So any kind of UI things, when a component gets loaded

69
00:02:27,00 --> 00:02:28,90
so you can do that on componentWillMount

70
00:02:28,90 --> 00:02:31,10
or componentDidMount is quite common

71
00:02:31,10 --> 00:02:33,00
or maybe when someone clicks a button.

72
00:02:33,00 --> 00:02:35,10
Maybe have some stuff that where you want to dispatch

73
00:02:35,10 --> 00:02:36,60
and it's not to do with the UI tool,

74
00:02:36,60 --> 00:02:39,30
so maybe you'll subscribe to a web socket connection

75
00:02:39,30 --> 00:02:41,50
or some sort of connection and it sends you a message

76
00:02:41,50 --> 00:02:44,10
and you're like "Oh, I really want to update my Redux store"

77
00:02:44,10 --> 00:02:46,00
you can just do store to dispatch.

78
00:02:46,00 --> 00:02:47,50
That's probably rarer.

79
00:02:47,50 --> 00:02:50,30
You'll probably use connect most of the time,

80
00:02:50,30 --> 00:02:52,90
maybe 80 or 90% but it is useful to know

81
00:02:52,90 --> 00:02:54,50
that you can do with store as well

82
00:02:54,50 --> 00:02:56,50
and obviously underneath that's how it works

83
00:02:56,50 --> 00:02:58,20
so it's always good to know.

84
00:02:58,20 --> 00:03:01,30
In summary, the firing actions video was just showing you

85
00:03:01,30 --> 00:03:05,00
how to fire an action inside of Redux.

86
00:03:05,00 --> 00:03:08,90
In the next video, we're gonna recap ES6/7 Spreading

87
00:03:08,90 --> 00:03:11,00
so what the spread operator is.

