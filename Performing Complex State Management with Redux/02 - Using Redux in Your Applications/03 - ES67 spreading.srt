1
00:00:01,30 --> 00:00:04,30
- [Instructor] ES6 or 7 Spreading.

2
00:00:04,30 --> 00:00:06,80
In this video I'm gonna show you what spreading is

3
00:00:06,80 --> 00:00:10,10
and why you might need to use it with Redux.

4
00:00:10,10 --> 00:00:15,10
So, if you're using ES6/7 I know there's a lot of words

5
00:00:15,10 --> 00:00:16,60
with all this JavaScript version,

6
00:00:16,60 --> 00:00:17,80
but basically if you're using

7
00:00:17,80 --> 00:00:19,80
a modern version of JavaScript,

8
00:00:19,80 --> 00:00:22,00
this is a feature which you can probably turn on.

9
00:00:22,00 --> 00:00:24,60
At the time that I'm recording these videos

10
00:00:24,60 --> 00:00:26,10
there's something called Babel,

11
00:00:26,10 --> 00:00:26,90
which enable most of these things.

12
00:00:26,90 --> 00:00:28,10
If you're watching this in a year of two

13
00:00:28,10 --> 00:00:29,20
from when I record it,

14
00:00:29,20 --> 00:00:31,70
maybe it will just be built into the language by then.

15
00:00:31,70 --> 00:00:34,70
It's kind of a modern feature that was first proposed

16
00:00:34,70 --> 00:00:37,80
maybe in 2000, the first time I saw it I think

17
00:00:37,80 --> 00:00:42,20
was maybe 2015 or 16 and it's slowly making it's way

18
00:00:42,20 --> 00:00:46,10
into the JavaScript that you'll find.

19
00:00:46,10 --> 00:00:47,20
So let's have a look at the feature.

20
00:00:47,20 --> 00:00:49,50
And I'll start by creating an object

21
00:00:49,50 --> 00:00:51,70
with the key hello and a value world.

22
00:00:51,70 --> 00:00:53,70
There's an object.

23
00:00:53,70 --> 00:00:56,10
Got a key here and value there.

24
00:00:56,10 --> 00:00:58,00
Log the object out.

25
00:00:58,00 --> 00:01:00,80
So the spread operator is an operator

26
00:01:00,80 --> 00:01:01,70
which looks like this.

27
00:01:01,70 --> 00:01:03,70
It's a dot, dot, dot, or ellipsis,

28
00:01:03,70 --> 00:01:06,20
I don't know what you call that, but dot, dot, dot.

29
00:01:06,20 --> 00:01:08,90
So let me show you some examples of how you can use this.

30
00:01:08,90 --> 00:01:12,30
The simplest example is I'm going to console.log it.

31
00:01:12,30 --> 00:01:15,90
Dot, dot, dot, say I'll create an object again.

32
00:01:15,90 --> 00:01:17,60
I'll show you in kind of the order that it works.

33
00:01:17,60 --> 00:01:19,80
Say this is an empty object, right.

34
00:01:19,80 --> 00:01:21,70
And what JavaScript goes and does when you do this

35
00:01:21,70 --> 00:01:24,10
is it creates a new empty object.

36
00:01:24,10 --> 00:01:26,80
I think it's the same as like brand new object or something.

37
00:01:26,80 --> 00:01:29,40
You're going to create a new object, so this is step one.

38
00:01:29,40 --> 00:01:32,00
And then you say, hey, can you spread in,

39
00:01:32,00 --> 00:01:34,80
or like can you put in my object.

40
00:01:34,80 --> 00:01:39,20
And what that does is it copies in my object

41
00:01:39,20 --> 00:01:40,70
into this new object.

42
00:01:40,70 --> 00:01:43,10
But the object, let's see it in the console,

43
00:01:43,10 --> 00:01:44,70
just so we can prove that it works.

44
00:01:44,70 --> 00:01:47,80
Right, so you can see it's printed, hello world.

45
00:01:47,80 --> 00:01:49,00
And it's printed hello world again.

46
00:01:49,00 --> 00:01:50,10
So it's working.

47
00:01:50,10 --> 00:01:54,20
Now what's special about this is that it's copying

48
00:01:54,20 --> 00:01:56,00
everything, you see it's copying hello and world

49
00:01:56,00 --> 00:01:57,40
into a new one.

50
00:01:57,40 --> 00:02:00,00
So the new one is actually not related

51
00:02:00,00 --> 00:02:02,10
to the old one at all.

52
00:02:02,10 --> 00:02:04,20
And so for example, I guess I can show you

53
00:02:04,20 --> 00:02:05,20
an example like this.

54
00:02:05,20 --> 00:02:07,40
So if we console log my object, and then we say

55
00:02:07,40 --> 00:02:10,50
my object dot hello equals,

56
00:02:10,50 --> 00:02:11,80
you should never do this, by the way,

57
00:02:11,80 --> 00:02:14,00
pretty much more or less when you're programming.

58
00:02:14,00 --> 00:02:17,10
A good rule is to not, this is called mutation,

59
00:02:17,10 --> 00:02:18,50
because we're taking this object and we're

60
00:02:18,50 --> 00:02:20,00
mutating it or changing it,

61
00:02:20,00 --> 00:02:21,70
like editing it.

62
00:02:21,70 --> 00:02:23,10
And so my object is kind of there.

63
00:02:23,10 --> 00:02:24,30
We log it, and then we're going to say

64
00:02:24,30 --> 00:02:27,10
dot hello is equal to, so we're going to assign hello

65
00:02:27,10 --> 00:02:29,10
to be like mars, right.

66
00:02:29,10 --> 00:02:31,30
Because we've got hello world, hello mars.

67
00:02:31,30 --> 00:02:34,50
And then we can console.log my object.

68
00:02:34,50 --> 00:02:38,90
I am going to put this, I'm going to assign this now.

69
00:02:38,90 --> 00:02:40,60
I'm going to, now let me add some lines in here

70
00:02:40,60 --> 00:02:42,60
so it's clear what's going on.

71
00:02:42,60 --> 00:02:46,10
Great, so we got claimed my object.

72
00:02:46,10 --> 00:02:48,80
So this is just the claim that we had before

73
00:02:48,80 --> 00:02:49,80
so we can log that out.

74
00:02:49,80 --> 00:02:53,00
Okay, so we create this hello world my object.

75
00:02:53,00 --> 00:02:55,50
We log it, straightforward, then we use

76
00:02:55,50 --> 00:02:58,30
the spread operator to claim my object.

77
00:02:58,30 --> 00:03:00,20
Right, so now we've got the claimed one.

78
00:03:00,20 --> 00:03:01,00
And then we log that.

79
00:03:01,00 --> 00:03:03,80
And then we take the original my object from the beginning

80
00:03:03,80 --> 00:03:06,90
and we set, assign hello to be mars.

81
00:03:06,90 --> 00:03:09,00
And we're going to log out my object again,

82
00:03:09,00 --> 00:03:11,00
and then as the last thing we're going to do

83
00:03:11,00 --> 00:03:13,50
is we're going to log out claimed my object.

84
00:03:13,50 --> 00:03:15,00
So we'll log this out again.

85
00:03:15,00 --> 00:03:16,00
Let's see what that does.

86
00:03:16,00 --> 00:03:17,80
It kind of explains how this works.

87
00:03:17,80 --> 00:03:21,40
Right, say, the first two logged hello world still.

88
00:03:21,40 --> 00:03:23,10
Then it logged hello mars.

89
00:03:23,10 --> 00:03:24,30
And then it logged hello world.

90
00:03:24,30 --> 00:03:25,70
So let's go back to the code.

91
00:03:25,70 --> 00:03:27,90
This also logged hello world, we already knew that.

92
00:03:27,90 --> 00:03:29,30
This one logged hello mars.

93
00:03:29,30 --> 00:03:31,10
So my object has not been changed.

94
00:03:31,10 --> 00:03:34,20
So this line and this line look the same,

95
00:03:34,20 --> 00:03:36,10
but this time this logged hello world

96
00:03:36,10 --> 00:03:38,60
and the second time this logged hello mars.

97
00:03:38,60 --> 00:03:41,40
Cloned my object is a different thing

98
00:03:41,40 --> 00:03:44,10
because this spread operator has created,

99
00:03:44,10 --> 00:03:46,50
so we've created a new object using these brackets

100
00:03:46,50 --> 00:03:49,60
and then copied the content, or claimed the content,

101
00:03:49,60 --> 00:03:51,10
of my object into it.

102
00:03:51,10 --> 00:03:53,80
So this and this are completely separate,

103
00:03:53,80 --> 00:03:56,50
which means that even though we changed world to mars

104
00:03:56,50 --> 00:04:00,80
in my object here, it's not affected the claimed one.

105
00:04:00,80 --> 00:04:02,20
That's important to know because of the way

106
00:04:02,20 --> 00:04:05,10
it reduces work which we're going to see in a minute.

107
00:04:05,10 --> 00:04:07,60
And so the other sorts of things you can do with this

108
00:04:07,60 --> 00:04:11,00
are you can change a key.

109
00:04:11,00 --> 00:04:13,80
So all of these operations are mutable,

110
00:04:13,80 --> 00:04:15,80
so they don't change the original.

111
00:04:15,80 --> 00:04:19,20
They kind of copy everything into a new object.

112
00:04:19,20 --> 00:04:20,20
That's really what you should,

113
00:04:20,20 --> 00:04:21,90
this is kind of what this means,

114
00:04:21,90 --> 00:04:25,10
is like create a new object, and then copy

115
00:04:25,10 --> 00:04:26,40
this stuff into it.

116
00:04:26,40 --> 00:04:27,70
That's kind of what it means.

117
00:04:27,70 --> 00:04:31,00
And so it's always a clone and it's always completely

118
00:04:31,00 --> 00:04:33,40
separate from the original thing that you're copying,

119
00:04:33,40 --> 00:04:35,40
and because it's a copy, it's not like the same thing.

120
00:04:35,40 --> 00:04:37,20
You see what I mean.

121
00:04:37,20 --> 00:04:38,90
So another sorts of things you can do

122
00:04:38,90 --> 00:04:41,80
is you can say we can add another key called

123
00:04:41,80 --> 00:04:44,50
hi like pluto, right.

124
00:04:44,50 --> 00:04:47,30
Now what we're saying is this bit means the same

125
00:04:47,30 --> 00:04:50,00
as before, like spread in my object,

126
00:04:50,00 --> 00:04:51,80
and then this bit's like okay I'm going to just

127
00:04:51,80 --> 00:04:53,50
carry on just using the object syntax.

128
00:04:53,50 --> 00:04:55,80
So I'm going to have another key called hi

129
00:04:55,80 --> 00:04:57,20
with a value called pluto.

130
00:04:57,20 --> 00:04:58,70
And that's completely fine as well.

131
00:04:58,70 --> 00:05:01,50
So let's log that out to the console,

132
00:05:01,50 --> 00:05:03,00
just so you can see what's going on.

133
00:05:03,00 --> 00:05:07,80
So if we refresh, you see here you've got hello mars

134
00:05:07,80 --> 00:05:11,60
because this had changed, hi, pluto.

135
00:05:11,60 --> 00:05:13,30
So it's just added that in.

136
00:05:13,30 --> 00:05:14,90
This seems like really contrived,

137
00:05:14,90 --> 00:05:16,00
like why are you showing me this,

138
00:05:16,00 --> 00:05:18,50
but I promise you it will become clear in a minute.

139
00:05:18,50 --> 00:05:21,90
You can also use this to override existing things.

140
00:05:21,90 --> 00:05:23,30
So let me show you another example.

141
00:05:23,30 --> 00:05:28,40
Say hello pluto, say, so this is the same key

142
00:05:28,40 --> 00:05:29,80
that's already in there, right.

143
00:05:29,80 --> 00:05:32,20
Let me show you, it'll become clearer.

144
00:05:32,20 --> 00:05:34,40
So now you can see, instead of saying hello mars,

145
00:05:34,40 --> 00:05:35,30
which is what it would have said,

146
00:05:35,30 --> 00:05:36,80
it said hello pluto.

147
00:05:36,80 --> 00:05:39,20
Now the last thing I want to show you is that

148
00:05:39,20 --> 00:05:41,00
the order of these things matters.

149
00:05:41,00 --> 00:05:43,10
So I'll do one more example.

150
00:05:43,10 --> 00:05:45,30
Say, we'll say hello pluto,

151
00:05:45,30 --> 00:05:47,30
so we've created an object that says hello pluto.

152
00:05:47,30 --> 00:05:50,50
And then as the last step we'll spread in my object again.

153
00:05:50,50 --> 00:05:52,80
It's the same as this, it's just the other way around.

154
00:05:52,80 --> 00:05:56,30
So we're doing the spread after the hello pluto.

155
00:05:56,30 --> 00:05:59,80
And as you might predict, the last thing you do wins.

156
00:05:59,80 --> 00:06:01,10
So it still says hello mars

157
00:06:01,10 --> 00:06:04,30
because the order goes from left to right.

158
00:06:04,30 --> 00:06:05,90
So it originally would have said hello pluto,

159
00:06:05,90 --> 00:06:07,80
and then it would have overwritten that with this,

160
00:06:07,80 --> 00:06:08,80
which says hello mars.

161
00:06:08,80 --> 00:06:10,50
So then you lose this bit.

162
00:06:10,50 --> 00:06:13,40
That's kind of, there are some more complicated examples

163
00:06:13,40 --> 00:06:15,30
with the same sorts of things,

164
00:06:15,30 --> 00:06:16,80
where it's completely with arrays.

165
00:06:16,80 --> 00:06:18,60
So let's just quickly show you that

166
00:06:18,60 --> 00:06:20,30
because that's also useful.

167
00:06:20,30 --> 00:06:23,70
As maybe we'll say like my array equals

168
00:06:23,70 --> 00:06:26,80
like one, two, three, and then console log that.

169
00:06:26,80 --> 00:06:29,60
And then we could do something like spread my array

170
00:06:29,60 --> 00:06:31,40
and then have a four at the end.

171
00:06:31,40 --> 00:06:33,90
So this is kind of saying so the brackets

172
00:06:33,90 --> 00:06:35,20
are saying create a new array.

173
00:06:35,20 --> 00:06:37,50
And this is like exactly the same sort of things,

174
00:06:37,50 --> 00:06:40,70
spread in this existing one, two, three, my array.

175
00:06:40,70 --> 00:06:42,30
And then I'm just going to carry on

176
00:06:42,30 --> 00:06:44,40
using the normal array syntax so I'll end up

177
00:06:44,40 --> 00:06:45,60
with a four.

178
00:06:45,60 --> 00:06:48,20
And obviously the ordering here matters as well.

179
00:06:48,20 --> 00:06:50,70
So maybe if we do a more contrived example

180
00:06:50,70 --> 00:06:52,00
we can get it all in one go.

181
00:06:52,00 --> 00:06:56,40
So if you put zero comma spread this comma four,

182
00:06:56,40 --> 00:06:59,00
you'll end up with zero, one, two, three

183
00:06:59,00 --> 00:07:00,90
when this gets spread in, four.

184
00:07:00,90 --> 00:07:02,50
So let's just checked that that works.

185
00:07:02,50 --> 00:07:04,50
Yeah, so you get zero, one, two, three, four.

186
00:07:04,50 --> 00:07:06,60
If you were to then sort of say,

187
00:07:06,60 --> 00:07:08,00
you can mutate arrays right,

188
00:07:08,00 --> 00:07:09,90
so you can like you know if you were to do

189
00:07:09,90 --> 00:07:12,40
like a push or a pop or whatever it is you can do,

190
00:07:12,40 --> 00:07:16,60
and edit that array, or mutate that array,

191
00:07:16,60 --> 00:07:17,60
it wouldn't affect this because this is a copy, cool.

192
00:07:17,60 --> 00:07:19,20
That's really it for spreading.

193
00:07:19,20 --> 00:07:20,60
What you're really going to need to know

194
00:07:20,60 --> 00:07:24,10
is probably this, this kind of thing,

195
00:07:24,10 --> 00:07:25,30
or this kind of thing.

196
00:07:25,30 --> 00:07:27,90
Taking the existing thing and changing one thing.

197
00:07:27,90 --> 00:07:29,20
It will become clearer in a minute,

198
00:07:29,20 --> 00:07:31,50
and it's mainly going to be with objects.

199
00:07:31,50 --> 00:07:34,40
Just how to look at how you can use spreading

200
00:07:34,40 --> 00:07:38,70
to mutably change data without editing the original

201
00:07:38,70 --> 00:07:42,60
data structures that you're working on.

202
00:07:42,60 --> 00:07:44,20
In the next video we're going to look at

203
00:07:44,20 --> 00:07:47,00
applying redux reducers to the application.

