1
00:00:00,90 --> 00:00:04,40
- [Instructor] Applying Redux reducers to the application.

2
00:00:04,40 --> 00:00:07,40
In this video, we're gonna have a look at how reducers work,

3
00:00:07,40 --> 00:00:11,00
how to set them up, what they're there for.

4
00:00:11,00 --> 00:00:13,40
So here is the post reducer,

5
00:00:13,40 --> 00:00:16,20
which is the only reducer in this app.

6
00:00:16,20 --> 00:00:18,50
So the first thing to note is that the store

7
00:00:18,50 --> 00:00:21,40
is usually, in almost every Redux application

8
00:00:21,40 --> 00:00:24,10
I've ever seen, the store is split by a reducer.

9
00:00:24,10 --> 00:00:27,60
So you'll see a key, which is the name of each reducer.

10
00:00:27,60 --> 00:00:30,20
You don't have to do it like that, but nearly everyone does,

11
00:00:30,20 --> 00:00:33,60
so let's just kind of say that that's just how it works.

12
00:00:33,60 --> 00:00:36,50
At its core, a reducer is a function.

13
00:00:36,50 --> 00:00:39,20
So this is a reducer here,

14
00:00:39,20 --> 00:00:41,00
and let's go through this bit by bit.

15
00:00:41,00 --> 00:00:46,00
So the first thing is state is a parameter,

16
00:00:46,00 --> 00:00:47,90
which is often assigned to an initial state,

17
00:00:47,90 --> 00:00:50,00
we'll come back to that, and an action.

18
00:00:50,00 --> 00:00:52,00
Now what happens is every time

19
00:00:52,00 --> 00:00:55,50
an action is dispatched, this code runs.

20
00:00:55,50 --> 00:00:58,10
And the action is the action that's being dispatched,

21
00:00:58,10 --> 00:00:59,90
funnily enough, and the state

22
00:00:59,90 --> 00:01:03,30
is the existing state of just this reducer.

23
00:01:03,30 --> 00:01:06,40
So, I'm gonna start off just by console logging, I think.

24
00:01:06,40 --> 00:01:08,40
We'll just console log, like, what's going on.

25
00:01:08,40 --> 00:01:13,20
So let's log the state, let's log the action,

26
00:01:13,20 --> 00:01:17,50
and now let's fire an action.

27
00:01:17,50 --> 00:01:19,90
So, oh, we're already firing an action here,

28
00:01:19,90 --> 00:01:23,10
posts/NEW_POST, and we've got payload there.

29
00:01:23,10 --> 00:01:25,80
Great, so let's have a look at what's going on

30
00:01:25,80 --> 00:01:27,10
when we run that.

31
00:01:27,10 --> 00:01:29,40
Okay, so we run it, oh we see a bunch

32
00:01:29,40 --> 00:01:31,20
of different things coming through, that's interesting.

33
00:01:31,20 --> 00:01:33,10
So we see all of these logs here,

34
00:01:33,10 --> 00:01:34,20
so let's look at the last one

35
00:01:34,20 --> 00:01:35,30
'cause that's the one we care about.

36
00:01:35,30 --> 00:01:39,10
So it's the existing state, which we get here,

37
00:01:39,10 --> 00:01:42,70
so that has posts, which is an array, post.

38
00:01:42,70 --> 00:01:44,40
Now if you look at the whole store,

39
00:01:44,40 --> 00:01:45,60
you can see it's actually different.

40
00:01:45,60 --> 00:01:48,40
So here you've got posts, then you've got posts again,

41
00:01:48,40 --> 00:01:50,00
and then that's an array.

42
00:01:50,00 --> 00:01:51,80
Here you've just got one post which is an array,

43
00:01:51,80 --> 00:01:56,00
so it's just this bit, and that's because it's just the part

44
00:01:56,00 --> 00:01:58,30
of the store that this reducer's responsible.

45
00:01:58,30 --> 00:02:01,50
This is the post reducer, so it's only responsible

46
00:02:01,50 --> 00:02:03,10
for like, this bit.

47
00:02:03,10 --> 00:02:04,90
If you had in here like another key

48
00:02:04,90 --> 00:02:07,80
called, like, I don't know, users,

49
00:02:07,80 --> 00:02:10,20
then this would never exceed that.

50
00:02:10,20 --> 00:02:12,10
The second thing we've logged is the action,

51
00:02:12,10 --> 00:02:13,70
which looks exactly as you'd expect.

52
00:02:13,70 --> 00:02:14,80
It's got the payload of what we passed in,

53
00:02:14,80 --> 00:02:16,80
and it's got the type.

54
00:02:16,80 --> 00:02:18,30
So this function gets run and you can see

55
00:02:18,30 --> 00:02:20,80
it also got run a bunch of other times.

56
00:02:20,80 --> 00:02:23,40
There's like this Redux init thing that gets run,

57
00:02:23,40 --> 00:02:26,20
which actually is not logged by this, which is surprising.

58
00:02:26,20 --> 00:02:27,50
I didn't know that.

59
00:02:27,50 --> 00:02:29,70
So that's kinda how it works when it gets called.

60
00:02:29,70 --> 00:02:32,90
And then your job in here is to return,

61
00:02:32,90 --> 00:02:34,70
so this is just the post part of the state,

62
00:02:34,70 --> 00:02:36,20
because this is the post reducer.

63
00:02:36,20 --> 00:02:41,10
Your job is to return the new state given that action.

64
00:02:41,10 --> 00:02:44,20
So you're given the old state and you're given the action,

65
00:02:44,20 --> 00:02:46,00
and your job is to return.

66
00:02:46,00 --> 00:02:47,30
You look at the action and you say,

67
00:02:47,30 --> 00:02:48,60
oh well you know this thing's happened,

68
00:02:48,60 --> 00:02:49,80
they're creating a new post, I don't know,

69
00:02:49,80 --> 00:02:51,90
whatever it is they're doing,

70
00:02:51,90 --> 00:02:53,70
and then you return the new state.

71
00:02:53,70 --> 00:02:56,90
See kind of in your mind, you need to decide ahead of time,

72
00:02:56,90 --> 00:03:00,30
given that this action's coming in, what is it I wanna do?

73
00:03:00,30 --> 00:03:02,50
How do I want the state to look afterwards?

74
00:03:02,50 --> 00:03:04,20
And then you return that.

75
00:03:04,20 --> 00:03:05,70
There are certain rules about this function

76
00:03:05,70 --> 00:03:07,30
which will go in in a minute,

77
00:03:07,30 --> 00:03:09,40
but let's have a look at like, how we might do one.

78
00:03:09,40 --> 00:03:12,30
So, usually you're gonna use a switch statement here.

79
00:03:12,30 --> 00:03:15,70
Now switch statements are actually not that commonly used.

80
00:03:15,70 --> 00:03:17,90
You could do it with an if statement,

81
00:03:17,90 --> 00:03:18,90
I'm just debating in my mind

82
00:03:18,90 --> 00:03:21,60
whether we should do it with an if statement.

83
00:03:21,60 --> 00:03:23,80
It's probably, it's neater with a switch statement,

84
00:03:23,80 --> 00:03:26,60
we'll leave it, but basically what the switch statement says

85
00:03:26,60 --> 00:03:28,50
is okay, switch on action.type,

86
00:03:28,50 --> 00:03:33,10
so in this example that was posts/NEW_POST.

87
00:03:33,10 --> 00:03:34,70
It's 'cause this is the action,

88
00:03:34,70 --> 00:03:40,90
and the action has a type key which was posts/NEW_POST.

89
00:03:40,90 --> 00:03:42,90
And so then you can cater for each case,

90
00:03:42,90 --> 00:03:46,40
or default says is like look, if none of the cases match,

91
00:03:46,40 --> 00:03:47,30
then just do this.

92
00:03:47,30 --> 00:03:48,30
So we just say return state.

93
00:03:48,30 --> 00:03:50,00
We say don't do anything.

94
00:03:50,00 --> 00:03:53,40
The reason that you're always gonna want this fall-through

95
00:03:53,40 --> 00:03:56,20
is because as you could see when we were running the code

96
00:03:56,20 --> 00:03:59,30
in the console, you get given any action,

97
00:03:59,30 --> 00:04:00,90
even if it's not for this reducer.

98
00:04:00,90 --> 00:04:04,00
Every single action will run through this code,

99
00:04:04,00 --> 00:04:05,30
and so there will be a lot of actions

100
00:04:05,30 --> 00:04:06,30
that you just don't care about

101
00:04:06,30 --> 00:04:07,60
'cause they're nothing to do with posts.

102
00:04:07,60 --> 00:04:09,80
Like, you know we have that Redux/init one

103
00:04:09,80 --> 00:04:12,40
just come through, maybe you have some news actions,

104
00:04:12,40 --> 00:04:14,70
or actions about, I don't know,

105
00:04:14,70 --> 00:04:16,80
dogs or whatever it is in your app,

106
00:04:16,80 --> 00:04:19,00
and this reducer just does not care.

107
00:04:19,00 --> 00:04:21,80
So you need the default return state here

108
00:04:21,80 --> 00:04:24,60
just to say look, there were some cases up here,

109
00:04:24,60 --> 00:04:28,00
so let me show you a case, so you say case,

110
00:04:28,00 --> 00:04:31,10
posts/NEW_POST, for example,

111
00:04:31,10 --> 00:04:33,20
and then here you would say like, do some stuff.

112
00:04:33,20 --> 00:04:35,20
So for now let's just say return state.

113
00:04:35,20 --> 00:04:36,50
But then maybe there's another one,

114
00:04:36,50 --> 00:04:38,70
and it didn't match this,

115
00:04:38,70 --> 00:04:40,20
so then it will come here at the end.

116
00:04:40,20 --> 00:04:42,10
After all, you can add more cases as well.

117
00:04:42,10 --> 00:04:45,50
So like, you know, posts, like DELETE_POST or something,

118
00:04:45,50 --> 00:04:47,90
I don't know, different thing.

119
00:04:47,90 --> 00:04:50,40
And you can do something else in here.

120
00:04:50,40 --> 00:04:52,60
So in each of these bits here,

121
00:04:52,60 --> 00:04:53,80
like these return statements,

122
00:04:53,80 --> 00:04:56,80
you decide I've got the previous state,

123
00:04:56,80 --> 00:04:58,10
how do I want to change it

124
00:04:58,10 --> 00:05:01,10
and then you have to return it in the,

125
00:05:01,10 --> 00:05:04,10
you return it and then the state gets updated.

126
00:05:04,10 --> 00:05:06,40
So let's build the new post1.

127
00:05:06,40 --> 00:05:10,30
So we got a, I'm gonna change the dispatch actually.

128
00:05:10,30 --> 00:05:11,50
Because I actually think the way

129
00:05:11,50 --> 00:05:13,20
we're dispatching is not the best.

130
00:05:13,20 --> 00:05:16,60
So new post I'm gonna just dispatch post2.

131
00:05:16,60 --> 00:05:17,90
Just a string, so I'm just gonna

132
00:05:17,90 --> 00:05:19,40
change that around a little bit.

133
00:05:19,40 --> 00:05:21,40
And then here what we're gonna say is okay,

134
00:05:21,40 --> 00:05:24,30
state so let's just quickly have a look

135
00:05:24,30 --> 00:05:26,70
at what everything looks like in the store.

136
00:05:26,70 --> 00:05:28,10
It's kind of hard to remember.

137
00:05:28,10 --> 00:05:29,20
Oh we screwed something up.

138
00:05:29,20 --> 00:05:32,10
See I can't even remember how to write a switch statement.

139
00:05:32,10 --> 00:05:33,60
You're just gonna do it once,

140
00:05:33,60 --> 00:05:35,50
you're gonna copy my code or someone else's code

141
00:05:35,50 --> 00:05:37,20
and you're just gonna copy and paste it around.

142
00:05:37,20 --> 00:05:41,20
So the state of the store at the moment before new post,

143
00:05:41,20 --> 00:05:46,10
is there's a post has a post key.

144
00:05:46,10 --> 00:05:48,40
Which is an array and it has one thing in it.

145
00:05:48,40 --> 00:05:51,30
And afterwards it will look the same.

146
00:05:51,30 --> 00:05:54,60
So what I'm gonna do now is add the post into that.

147
00:05:54,60 --> 00:05:56,20
So what we want to say,

148
00:05:56,20 --> 00:05:58,10
so remember what we learned in the last video,

149
00:05:58,10 --> 00:06:00,40
so state's an object.

150
00:06:00,40 --> 00:06:02,70
Okay let's just keep state, that will be the first thing.

151
00:06:02,70 --> 00:06:05,10
So at the moment we're saying return state.

152
00:06:05,10 --> 00:06:06,60
Well we saw in the last video that actually

153
00:06:06,60 --> 00:06:08,40
you could write that like this.

154
00:06:08,40 --> 00:06:09,80
And then you're coining the whole thing

155
00:06:09,80 --> 00:06:11,90
but you're just returning the same thing.

156
00:06:11,90 --> 00:06:13,60
So that's absolutely fine,

157
00:06:13,60 --> 00:06:16,10
and if we run that nothing will happen

158
00:06:16,10 --> 00:06:19,60
because you run it and we keep all the posts.

159
00:06:19,60 --> 00:06:21,30
Now let me show you if I did that,

160
00:06:21,30 --> 00:06:23,50
if I said state's now an empty array

161
00:06:23,50 --> 00:06:25,70
let's have a look at what happens there.

162
00:06:25,70 --> 00:06:28,50
Right now it's different because post is empty.

163
00:06:28,50 --> 00:06:29,60
So that's bad.

164
00:06:29,60 --> 00:06:31,30
So let's bring that back.

165
00:06:31,30 --> 00:06:32,50
Now we want to change post.

166
00:06:32,50 --> 00:06:34,00
We're gonna keep state,

167
00:06:34,00 --> 00:06:37,30
but we're gonna update this post thing.

168
00:06:37,30 --> 00:06:38,60
How are we gonna update it?

169
00:06:38,60 --> 00:06:41,90
Let's update it to be chicken dinner.

170
00:06:41,90 --> 00:06:43,60
I don't know, say we've updated the post

171
00:06:43,60 --> 00:06:45,20
to just say chicken dinner right.

172
00:06:45,20 --> 00:06:46,10
let's have a look at that.

173
00:06:46,10 --> 00:06:47,90
So we spread in the existing states,

174
00:06:47,90 --> 00:06:49,20
we keep everything that's there

175
00:06:49,20 --> 00:06:50,90
but then we've kind of overridden the post thing

176
00:06:50,90 --> 00:06:53,20
and said actually we're just going to

177
00:06:53,20 --> 00:06:55,50
have that unconditionally be chicken dinner now.

178
00:06:55,50 --> 00:06:57,30
So let's have a look.

179
00:06:57,30 --> 00:06:58,70
Refresh the page.

180
00:06:58,70 --> 00:07:03,30
So we come in and you see post says post1,

181
00:07:03,30 --> 00:07:06,40
we fire the action afterwards.

182
00:07:06,40 --> 00:07:08,20
Post now has chicken dinner in it.

183
00:07:08,20 --> 00:07:10,80
So this is kind of where the reducer,

184
00:07:10,80 --> 00:07:12,10
I really like this logger.

185
00:07:12,10 --> 00:07:14,60
So this is before prep state.

186
00:07:14,60 --> 00:07:16,30
The previous state and you can see the post bit

187
00:07:16,30 --> 00:07:17,80
of that state in there.

188
00:07:17,80 --> 00:07:20,30
And then afterward you can see the reducer runs

189
00:07:20,30 --> 00:07:22,40
and it takes this action

190
00:07:22,40 --> 00:07:24,60
and the relevant part of the reducers.

191
00:07:24,60 --> 00:07:25,80
At this bit.

192
00:07:25,80 --> 00:07:27,20
So this is the state variable

193
00:07:27,20 --> 00:07:29,40
which we saw at the top of the function.

194
00:07:29,40 --> 00:07:31,30
And this is the action it receives.

195
00:07:31,30 --> 00:07:32,80
And then you return a new state

196
00:07:32,80 --> 00:07:36,30
and we returned posts but with chicken dinner

197
00:07:36,30 --> 00:07:37,60
in posts instead.

198
00:07:37,60 --> 00:07:39,90
So we spread in what was in here so you got everything,

199
00:07:39,90 --> 00:07:41,60
then we overrode post and just said

200
00:07:41,60 --> 00:07:42,80
put chicken dinner in there.

201
00:07:42,80 --> 00:07:45,00
And so that's what Redux will give you back.

202
00:07:45,00 --> 00:07:46,50
That's not really what we wanna do.

203
00:07:46,50 --> 00:07:47,80
What we actually wanna do

204
00:07:47,80 --> 00:07:50,30
is we wanna add something to the array.

205
00:07:50,30 --> 00:07:52,00
And we saw in the last video you can do that

206
00:07:52,00 --> 00:07:55,30
by saying dot dot dot, state.posts.

207
00:07:55,30 --> 00:07:57,90
This now says okay I wanna keep all of state.

208
00:07:57,90 --> 00:08:00,80
So I claimed that like this.

209
00:08:00,80 --> 00:08:02,80
But we're gonna change posts.

210
00:08:02,80 --> 00:08:04,30
Right because the posts comes after.

211
00:08:04,30 --> 00:08:05,60
So it copies all the state in,

212
00:08:05,60 --> 00:08:07,00
including the existing post

213
00:08:07,00 --> 00:08:08,90
which probably looked like this post1.

214
00:08:08,90 --> 00:08:11,10
And then we're like actually we're gonna change posts.

215
00:08:11,10 --> 00:08:13,90
And then we've then got this array here.

216
00:08:13,90 --> 00:08:15,60
And what we said is what we're gonna do is

217
00:08:15,60 --> 00:08:17,10
go to state.posts which is

218
00:08:17,10 --> 00:08:18,70
what the posts already looked like.

219
00:08:18,70 --> 00:08:20,40
Now what we can say is you know what

220
00:08:20,40 --> 00:08:22,80
we're gonna add the new one, action.payload.

221
00:08:22,80 --> 00:08:25,30
So action.payload was see right here,

222
00:08:25,30 --> 00:08:27,10
in this case it's post2.

223
00:08:27,10 --> 00:08:29,30
So let's see what happens now.

224
00:08:29,30 --> 00:08:32,70
So beforehand post looks like this, just one thing.

225
00:08:32,70 --> 00:08:33,50
Post1.

226
00:08:33,50 --> 00:08:35,60
Fire the action, the reducer runs.

227
00:08:35,60 --> 00:08:38,10
And we're returning all of the existing posts

228
00:08:38,10 --> 00:08:39,70
and we spread one in at the end.

229
00:08:39,70 --> 00:08:41,60
So now we get post1, post2.

230
00:08:41,60 --> 00:08:43,60
Now you can see up here what's happened.

231
00:08:43,60 --> 00:08:45,40
Post1 and post2 are now on the screen.

232
00:08:45,40 --> 00:08:46,70
So this is really cool.

233
00:08:46,70 --> 00:08:50,00
So what's happened is the state of the store

234
00:08:50,00 --> 00:08:51,40
had one post in it.

235
00:08:51,40 --> 00:08:53,80
We fired an action, our reducer ran.

236
00:08:53,80 --> 00:08:57,60
Was given the existing state for this reducer,

237
00:08:57,60 --> 00:08:58,80
and was given an action.

238
00:08:58,80 --> 00:09:00,20
And then our reducer said you know what,

239
00:09:00,20 --> 00:09:04,30
if I see a new post I'm gonna take the existing state

240
00:09:04,30 --> 00:09:05,70
and I'm gonna spread in.

241
00:09:05,70 --> 00:09:07,40
I'm gonna change the posts,

242
00:09:07,40 --> 00:09:08,20
I'm gonna keep all the existing posts

243
00:09:08,20 --> 00:09:10,20
but add this new one to the end.

244
00:09:10,20 --> 00:09:12,30
And then we return that from this function

245
00:09:12,30 --> 00:09:13,90
and then this becomes the new state

246
00:09:13,90 --> 00:09:15,40
for this part of the reducer.

247
00:09:15,40 --> 00:09:17,70
So for the post part of the reducer.

248
00:09:17,70 --> 00:09:20,40
Then what happens was our connect function

249
00:09:20,40 --> 00:09:22,60
which we looked at a couple of videos ago

250
00:09:22,60 --> 00:09:24,80
so let me just remind you.

251
00:09:24,80 --> 00:09:26,60
Our connect function is super dumb

252
00:09:26,60 --> 00:09:29,60
and it just says hey connect me to state.post.

253
00:09:29,60 --> 00:09:31,10
And whenever that changes,

254
00:09:31,10 --> 00:09:33,70
this dot props gets that new stuff.

255
00:09:33,70 --> 00:09:36,10
So we've dispatched an action.

256
00:09:36,10 --> 00:09:39,40
The store changed because of what happened in the reducer.

257
00:09:39,40 --> 00:09:41,40
Because the reducer is responsible for taking

258
00:09:41,40 --> 00:09:43,50
the old part of the store,

259
00:09:43,50 --> 00:09:45,50
like one part of the store,

260
00:09:45,50 --> 00:09:47,30
and saying oh this action happened

261
00:09:47,30 --> 00:09:48,90
how do I want the store to look now.

262
00:09:48,90 --> 00:09:51,60
You return that then the store gets updated.

263
00:09:51,60 --> 00:09:54,10
This is like pretty much all Reduxes.

264
00:09:54,10 --> 00:09:55,60
Like what I'm just describing.

265
00:09:55,60 --> 00:09:58,70
So you can fire actions, which we fired one here.

266
00:09:58,70 --> 00:10:00,30
And then you say in your reducer,

267
00:10:00,30 --> 00:10:02,30
oh like here's an action

268
00:10:02,30 --> 00:10:06,80
like posts/NEW_POST, an action can maybe have a type

269
00:10:06,80 --> 00:10:09,30
which is normally what you use to decide what to do.

270
00:10:09,30 --> 00:10:12,20
And then a payload which may be somewhere down in here.

271
00:10:12,20 --> 00:10:14,40
We updated the posts part of the payload,

272
00:10:14,40 --> 00:10:16,90
and then that updated and we saw it log on the console

273
00:10:16,90 --> 00:10:18,50
that the store had now updated.

274
00:10:18,50 --> 00:10:20,70
And then all of your components are just really stupid

275
00:10:20,70 --> 00:10:22,70
and they just go well the store's changed

276
00:10:22,70 --> 00:10:24,80
so their props change because they're connected.

277
00:10:24,80 --> 00:10:27,00
Because we connected it down here.

278
00:10:27,00 --> 00:10:28,60
So this dot props.paste will have

279
00:10:28,60 --> 00:10:29,80
had one thing in it, post1.

280
00:10:29,80 --> 00:10:32,90
And then after that action fires and the store updates

281
00:10:32,90 --> 00:10:35,20
it will have two things in it and props will change.

282
00:10:35,20 --> 00:10:36,60
And the way that Reacts works

283
00:10:36,60 --> 00:10:38,30
is whenever your props change,

284
00:10:38,30 --> 00:10:39,80
the render function happens again.

285
00:10:39,80 --> 00:10:41,20
That's just like a React thing.

286
00:10:41,20 --> 00:10:42,70
React you should be familiar with.

287
00:10:42,70 --> 00:10:44,50
So because the props change,

288
00:10:44,50 --> 00:10:46,40
all the relevant things rerender

289
00:10:46,40 --> 00:10:49,00
and we put these on the screen in a really dumb way

290
00:10:49,00 --> 00:10:53,00
but you can see that it's just immediately put those things,

291
00:10:53,00 --> 00:10:55,20
put post1, post2, on the screen.

292
00:10:55,20 --> 00:10:56,70
Because it's in the store.

293
00:10:56,70 --> 00:10:58,40
So when you're writing your components

294
00:10:58,40 --> 00:10:59,50
you just need to worry about like

295
00:10:59,50 --> 00:11:01,10
what does the store currently look like

296
00:11:01,10 --> 00:11:02,90
and your component will just put that on the screen

297
00:11:02,90 --> 00:11:04,90
and then you just need to worry about

298
00:11:04,90 --> 00:11:07,00
when you want to update what's on the screen,

299
00:11:07,00 --> 00:11:08,20
you're gonna fire an action.

300
00:11:08,20 --> 00:11:11,00
And the reducer says give it an action

301
00:11:11,00 --> 00:11:12,80
and the existing state of the store,

302
00:11:12,80 --> 00:11:15,20
what should the store look like afterwards?

303
00:11:15,20 --> 00:11:16,70
And you return that.

304
00:11:16,70 --> 00:11:20,10
Say there's one last thing to explain here.

305
00:11:20,10 --> 00:11:22,60
And that's why did I use spreading to do all this

306
00:11:22,60 --> 00:11:25,10
when maybe I could have said something like

307
00:11:25,10 --> 00:11:29,10
state.posts equals like I don't know like yeah.

308
00:11:29,10 --> 00:11:30,20
You kind of get where I'm going.

309
00:11:30,20 --> 00:11:35,20
State.posts like push action.payload.

310
00:11:35,20 --> 00:11:37,60
Maybe something like this.

311
00:11:37,60 --> 00:11:41,50
Or maybe like, maybe like just this line actually.

312
00:11:41,50 --> 00:11:43,30
If you think about this line here,

313
00:11:43,30 --> 00:11:46,80
state.posts push action.payload.

314
00:11:46,80 --> 00:11:49,20
So state.posts will now have the extra thing in it

315
00:11:49,20 --> 00:11:50,80
and we're kind of just done.

316
00:11:50,80 --> 00:11:52,70
For now, just kind of understand

317
00:11:52,70 --> 00:11:55,70
you need to use spread everywhere in these operations.

318
00:11:55,70 --> 00:11:58,00
You can't use words like push

319
00:11:58,00 --> 00:12:01,10
or assigning things like we did in the previous video.

320
00:12:01,10 --> 00:12:02,90
So you're not allowed to retake stuff

321
00:12:02,90 --> 00:12:04,20
because it breaks Redux.

322
00:12:04,20 --> 00:12:06,70
We'll cover exactly what you can and can't do

323
00:12:06,70 --> 00:12:08,00
in a later video though.

324
00:12:08,00 --> 00:12:10,20
In this video we took a look at reducers.

325
00:12:10,20 --> 00:12:12,60
We wrote some example reducers,

326
00:12:12,60 --> 00:12:14,70
which transforms, take actions

327
00:12:14,70 --> 00:12:17,00
and take the current state of the store

328
00:12:17,00 --> 00:12:19,00
and produce a new state of the store.

329
00:12:19,00 --> 00:12:21,30
For that part of the store that

330
00:12:21,30 --> 00:12:23,30
that reducer is responsible for.

331
00:12:23,30 --> 00:12:24,70
In this section we looked at

332
00:12:24,70 --> 00:12:26,70
how to connect your components to the store,

333
00:12:26,70 --> 00:12:28,50
how to fire actions.

334
00:12:28,50 --> 00:12:31,20
We've recapped ES6 and seven spreading.

335
00:12:31,20 --> 00:12:35,90
And we looked at applying Redux reducers to the application.

336
00:12:35,90 --> 00:12:37,20
That's the end of this section

337
00:12:37,20 --> 00:12:40,10
on using Redux in your application.

338
00:12:40,10 --> 00:12:43,00
The next section is on advanced Redux concepts.

