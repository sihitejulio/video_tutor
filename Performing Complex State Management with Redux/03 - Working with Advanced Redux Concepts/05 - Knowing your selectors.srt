1
00:00:01,20 --> 00:00:02,60
- [Instructor] Knowing your selectors.

2
00:00:02,60 --> 00:00:03,90
In this video, I'm gonna cover

3
00:00:03,90 --> 00:00:06,80
an additional concept of Redux, which is not always covered,

4
00:00:06,80 --> 00:00:09,60
which is selectors and reselect.

5
00:00:09,60 --> 00:00:11,60
So, for anybody that knows me,

6
00:00:11,60 --> 00:00:13,50
and knows me as a programmer, I suppose,

7
00:00:13,50 --> 00:00:14,90
maybe not my friends so much,

8
00:00:14,90 --> 00:00:18,60
they'll know that this is a topic pretty close to my heart.

9
00:00:18,60 --> 00:00:21,70
So I think, with Redux, the number one mistake

10
00:00:21,70 --> 00:00:25,30
that I see programmers in the wild making with this library,

11
00:00:25,30 --> 00:00:28,80
is not using selectors or a similar kind of concept.

12
00:00:28,80 --> 00:00:32,00
I see a lot of programmers who get paid, you know,

13
00:00:32,00 --> 00:00:34,40
quite good money to kind of go around to developing apps,

14
00:00:34,40 --> 00:00:36,50
and they'll be developing Redux app after Redux app,

15
00:00:36,50 --> 00:00:38,70
and they have not embraced this concept.

16
00:00:38,70 --> 00:00:40,80
So selectors' kind of a concept.

17
00:00:40,80 --> 00:00:42,60
But it's also a library.

18
00:00:42,60 --> 00:00:44,30
A library does not do a lot,

19
00:00:44,30 --> 00:00:46,10
but we'll go through what it does do.

20
00:00:46,10 --> 00:00:48,50
It's all made by the same guy that made Redux.

21
00:00:48,50 --> 00:00:51,70
And, really, I think, sometimes it's a second class citizen

22
00:00:51,70 --> 00:00:54,50
in a lot of projects, but, actually, I think it's critical

23
00:00:54,50 --> 00:00:55,60
to doing things well.

24
00:00:55,60 --> 00:00:56,80
And, really, it's a conceptual thing.

25
00:00:56,80 --> 00:00:58,00
So I'm going to explain what they are,

26
00:00:58,00 --> 00:00:59,30
and then I'll show you how to write them.

27
00:00:59,30 --> 00:01:00,70
And maybe talk a little bit about

28
00:01:00,70 --> 00:01:02,70
why you might want to use them.

29
00:01:02,70 --> 00:01:05,90
For me, these days, they've become quite central

30
00:01:05,90 --> 00:01:07,20
to most of the apps I write.

31
00:01:07,20 --> 00:01:09,50
With what is a selector.

32
00:01:09,50 --> 00:01:11,80
So I'm gonna show you, with an example.

33
00:01:11,80 --> 00:01:14,30
So, I'm gonna quickly change some code.

34
00:01:14,30 --> 00:01:15,30
So I'm gonna have,

35
00:01:15,30 --> 00:01:16,90
we talked about this earlier in an example,

36
00:01:16,90 --> 00:01:17,90
but I'm gonna do it again.

37
00:01:17,90 --> 00:01:18,90
I'm gonna add something to state,

38
00:01:18,90 --> 00:01:20,60
which is like the current post.

39
00:01:20,60 --> 00:01:23,20
And I'm gonna put the current post to be zero at all times.

40
00:01:23,20 --> 00:01:26,70
So let's say you have like post1, post4,

41
00:01:26,70 --> 00:01:28,50
one and two get added pretty quick.

42
00:01:28,50 --> 00:01:30,50
And then we've got currentPostIndex.

43
00:01:30,50 --> 00:01:34,70
So that's the post that's currently, like, I don't know,

44
00:01:34,70 --> 00:01:36,40
highlighted or something.

45
00:01:36,40 --> 00:01:38,00
And we wanna display it on the screen

46
00:01:38,00 --> 00:01:39,30
in like a special place, right?

47
00:01:39,30 --> 00:01:40,30
So let's quickly do that.

48
00:01:40,30 --> 00:01:42,70
So I'm gonna show you how you would do that

49
00:01:42,70 --> 00:01:43,90
with the code we've got now.

50
00:01:43,90 --> 00:01:45,70
So I've added currentPostIndex: 0

51
00:01:45,70 --> 00:01:47,80
to the initial state of this reducer.

52
00:01:47,80 --> 00:01:49,50
Which means that it will pretty much be in the store

53
00:01:49,50 --> 00:01:50,80
when things kick off.

54
00:01:50,80 --> 00:01:53,60
So, redditPost, how do we get it in here?

55
00:01:53,60 --> 00:01:57,30
So at the moment we're showing all the posts.

56
00:01:57,30 --> 00:01:59,90
So, underneath that, I'm gonna show the current post.

57
00:01:59,90 --> 00:02:03,60
I'm gonna do that by saying, so this.posts,

58
00:02:03,60 --> 00:02:10,00
indexed by this.props.currentPostIndex.

59
00:02:10,00 --> 00:02:11,30
So that will mean, it would be good

60
00:02:11,30 --> 00:02:13,90
if I could actually show it like, I think I can do this.

61
00:02:13,90 --> 00:02:15,00
We shall find out.

62
00:02:15,00 --> 00:02:16,60
So I've put Current Post.

63
00:02:16,60 --> 00:02:18,80
And then I've put this.props.posts.

64
00:02:18,80 --> 00:02:19,60
So that's just all the posts.

65
00:02:19,60 --> 00:02:22,00
And then I said go index that

66
00:02:22,00 --> 00:02:24,00
by whatever the currentPostIndex is.

67
00:02:24,00 --> 00:02:28,00
Let's give that a run.

68
00:02:28,00 --> 00:02:30,90
Pray to the programming gods that this works.

69
00:02:30,90 --> 00:02:32,30
Okay, that's gone pretty well.

70
00:02:32,30 --> 00:02:33,40
So you can see it says

71
00:02:33,40 --> 00:02:38,20
post1post4post2post3Current Post: post1.

72
00:02:38,20 --> 00:02:42,00
If we look at the store, we've got the full posts in here.

73
00:02:42,00 --> 00:02:43,70
In the latest version of the store.

74
00:02:43,70 --> 00:02:46,30
And we've got currentPostIndex is zero.

75
00:02:46,30 --> 00:02:49,20
So that's why it's done this index by zero,

76
00:02:49,20 --> 00:02:52,50
which gets you post1, so that's why post1 shows up here.

77
00:02:52,50 --> 00:02:54,50
And if we were to change this to be two,

78
00:02:54,50 --> 00:02:56,90
then we would get like post2.

79
00:02:56,90 --> 00:03:00,10
So what's the problem with this, or is there a problem?

80
00:03:00,10 --> 00:03:01,20
A lot of developers,

81
00:03:01,20 --> 00:03:03,60
they go along and they write a lot of code like this.

82
00:03:03,60 --> 00:03:04,80
And what we're really doing here

83
00:03:04,80 --> 00:03:08,30
is we're combining two bits of the store together

84
00:03:08,30 --> 00:03:10,60
to sort of get a third outcome.

85
00:03:10,60 --> 00:03:14,10
So the third outcome is like post1, or a string.

86
00:03:14,10 --> 00:03:15,50
I like thinking in types a lot,

87
00:03:15,50 --> 00:03:17,00
even though JavaScript's not type.

88
00:03:17,00 --> 00:03:19,80
We're getting out a string of which post is the current one.

89
00:03:19,80 --> 00:03:21,10
We're getting the whole post.

90
00:03:21,10 --> 00:03:22,90
And the inputs to that are all the posts

91
00:03:22,90 --> 00:03:25,90
and the index of the currently selected one.

92
00:03:25,90 --> 00:03:27,80
And you can take those two things together

93
00:03:27,80 --> 00:03:30,70
and you can compute what is the full post

94
00:03:30,70 --> 00:03:31,80
of the currently selected post.

95
00:03:31,80 --> 00:03:33,60
So I would call that like maybe current post,

96
00:03:33,60 --> 00:03:35,10
what's the current post.

97
00:03:35,10 --> 00:03:37,10
Well, we've done it in line,

98
00:03:37,10 --> 00:03:39,50
right in the JSX in our function.

99
00:03:39,50 --> 00:03:41,50
You could say, you know what, you could do this.

100
00:03:41,50 --> 00:03:43,60
currentPost, so you could create

101
00:03:43,60 --> 00:03:46,00
like a little function here, a little method.

102
00:03:46,00 --> 00:03:48,80
And you would copy that code in, you can return it,

103
00:03:48,80 --> 00:03:52,00
and then you could say, and you'll say something,

104
00:03:52,00 --> 00:03:53,80
rich or fixed.

105
00:03:53,80 --> 00:03:56,00
Prove to you that that still works, yeah, that works.

106
00:03:56,00 --> 00:03:58,60
That is better, don't get me wrong, it definitely is.

107
00:03:58,60 --> 00:04:01,90
So here's another approach to this same problem.

108
00:04:01,90 --> 00:04:03,00
Which I'm gonna show you now.

109
00:04:03,00 --> 00:04:04,50
So the next thing I would say

110
00:04:04,50 --> 00:04:06,20
that's maybe slightly better, again,

111
00:04:06,20 --> 00:04:08,40
is to have this, I'm gonna change this up a bit,

112
00:04:08,40 --> 00:04:14,20
currentPost = (postState), just thinking how this works.

113
00:04:14,20 --> 00:04:16,30
So we've got this function down here.

114
00:04:16,30 --> 00:04:19,10
And, actually, this function says, I'll give you a state

115
00:04:19,10 --> 00:04:20,70
and you tell me which bit you care about.

116
00:04:20,70 --> 00:04:23,30
But you can run any code you want in here.

117
00:04:23,30 --> 00:04:25,30
So I think, probably, the next thing to do

118
00:04:25,30 --> 00:04:29,00
is to maybe say, let's say it takes the whole state.

119
00:04:29,00 --> 00:04:31,90
And then we'd just go, what do we want to return.

120
00:04:31,90 --> 00:04:34,40
We want to return all the existing state,

121
00:04:34,40 --> 00:04:35,70
so we'll spread that in.

122
00:04:35,70 --> 00:04:37,20
I'm just renaming this on the fly.

123
00:04:37,20 --> 00:04:39,30
So I'll create the function called post,

124
00:04:39,30 --> 00:04:40,50
which takes the whole state.

125
00:04:40,50 --> 00:04:41,30
When it gives you the state,

126
00:04:41,30 --> 00:04:43,70
and you say I'm gonna return this state.

127
00:04:43,70 --> 00:04:50,60
But, also, I'm gonna set posts to be the state.posts.

128
00:04:50,60 --> 00:04:54,10
I'm just figuring out the best way to do this, currentPost.

129
00:04:54,10 --> 00:04:56,10
So what we really care about is currentPost.

130
00:04:56,10 --> 00:04:57,50
So we'll say we'll keep all the state,

131
00:04:57,50 --> 00:05:00,00
but we'll let currentPost be this.

132
00:05:00,00 --> 00:05:02,50
I can actually see this is wrong, this is not what I meant.

133
00:05:02,50 --> 00:05:03,70
So what I meant was this.

134
00:05:03,70 --> 00:05:06,90
So you give it all the states, at the global store,

135
00:05:06,90 --> 00:05:09,00
and we say, like, we're only really interested in posts.

136
00:05:09,00 --> 00:05:10,10
So we'll spread that in,

137
00:05:10,10 --> 00:05:12,40
so now you get all the posts stuff back.

138
00:05:12,40 --> 00:05:14,40
And we're also gonna add to that an extra thing,

139
00:05:14,40 --> 00:05:15,50
currentPost.

140
00:05:15,50 --> 00:05:17,20
And so then you crawl this in here with state.

141
00:05:17,20 --> 00:05:18,90
And think it will be a lot clearer

142
00:05:18,90 --> 00:05:20,30
once I actually return this.

143
00:05:20,30 --> 00:05:22,40
I think what we might need to do is,

144
00:05:22,40 --> 00:05:24,40
so I'm just gonna log it before I return it,

145
00:05:24,40 --> 00:05:25,90
so you can actually see what it is.

146
00:05:25,90 --> 00:05:28,30
So that's the same code as it was before,

147
00:05:28,30 --> 00:05:29,90
just with a log statement in the middle.

148
00:05:29,90 --> 00:05:32,30
Let's have a look and see how that now work.

149
00:05:32,30 --> 00:05:33,90
Think our existing code might blow up now.

150
00:05:33,90 --> 00:05:37,30
And there might be some red stuff lurking around.

151
00:05:37,30 --> 00:05:40,40
Oh, yeah, okay, postState is not defined.

152
00:05:40,40 --> 00:05:43,70
So, alright, cool, fair enough.

153
00:05:43,70 --> 00:05:46,00
Okay, this.currentPost is not a function.

154
00:05:46,00 --> 00:05:47,50
Did it do it beforehand, it did.

155
00:05:47,50 --> 00:05:49,90
Okay, so, don't worry about the red arrows.

156
00:05:49,90 --> 00:05:51,10
So, up here, you can see that it's done it.

157
00:05:51,10 --> 00:05:54,60
So it's returned posts, currentPostIndex.

158
00:05:54,60 --> 00:05:57,40
So no news there, because they were both on state before.

159
00:05:57,40 --> 00:06:01,30
But it's also returned the currentPost at post1.

160
00:06:01,30 --> 00:06:03,30
It's almost as if like this is in our Redux store.

161
00:06:03,30 --> 00:06:05,50
So we haven't actually used this yet.

162
00:06:05,50 --> 00:06:09,40
So because it's now our connect that's returning this,

163
00:06:09,40 --> 00:06:11,10
we can just reference that thing.

164
00:06:11,10 --> 00:06:14,90
So that would be this.props.currentPost.

165
00:06:14,90 --> 00:06:17,70
And we can just get rid of that, let's have a look.

166
00:06:17,70 --> 00:06:20,20
So now you can see the Current Post is back to post1.

167
00:06:20,20 --> 00:06:21,60
So let's have a quick look at that again.

168
00:06:21,60 --> 00:06:25,10
So, maps state to props function, it's called.

169
00:06:25,10 --> 00:06:26,80
Is the function that we just wrote.

170
00:06:26,80 --> 00:06:29,20
It's returning this, so, it would be helpful, actually,

171
00:06:29,20 --> 00:06:31,30
if we could see down here.

172
00:06:31,30 --> 00:06:33,50
Okay, yeah, so here's a pretty good place to see it.

173
00:06:33,50 --> 00:06:36,00
So you've got posts, in Redux state,

174
00:06:36,00 --> 00:06:39,20
which only has currentPostIndex: 0.

175
00:06:39,20 --> 00:06:40,80
And posts, which has three things in it.

176
00:06:40,80 --> 00:06:42,50
And then, here, you can see.

177
00:06:42,50 --> 00:06:43,50
Okay, it's not quite worked out.

178
00:06:43,50 --> 00:06:46,60
Because this now has an array of 4, but you get the point.

179
00:06:46,60 --> 00:06:49,90
This has all the same stuff, but it also has currentPost.

180
00:06:49,90 --> 00:06:51,80
So it's got post1 in here.

181
00:06:51,80 --> 00:06:53,30
So, really, what you're doing is,

182
00:06:53,30 --> 00:06:56,10
you're kind of processing the Redux state.

183
00:06:56,10 --> 00:06:58,00
In this case, I'm only doing one bit.

184
00:06:58,00 --> 00:07:00,00
But you can maybe do more, like if you had posts

185
00:07:00,00 --> 00:07:01,90
and you has users, there's no reason,

186
00:07:01,90 --> 00:07:04,30
because, remember, in this function,

187
00:07:04,30 --> 00:07:06,40
we actually have access to the whole state.

188
00:07:06,40 --> 00:07:07,90
And it's just that I only happen

189
00:07:07,90 --> 00:07:10,80
to use state.posts in a bunch of places.

190
00:07:10,80 --> 00:07:13,40
But I could also be like, oh, you know,

191
00:07:13,40 --> 00:07:16,10
state.users, like, whatever.

192
00:07:16,10 --> 00:07:18,50
So you can combine multiple bits of state.

193
00:07:18,50 --> 00:07:21,30
And then I've like calculated some extra stuff

194
00:07:21,30 --> 00:07:22,50
that would be useful.

195
00:07:22,50 --> 00:07:25,00
And then I've made my component really stupid again.

196
00:07:25,00 --> 00:07:27,00
So this component is now like really stupid.

197
00:07:27,00 --> 00:07:29,00
Because it's just gone back to how it was,

198
00:07:29,00 --> 00:07:31,50
it's just like looking at things that are in this.props.

199
00:07:31,50 --> 00:07:34,00
It's not really doing any calculations.

200
00:07:34,00 --> 00:07:36,30
I think this concept's super powerful.

201
00:07:36,30 --> 00:07:38,90
Let me show you now, so this is a map to props function.

202
00:07:38,90 --> 00:07:41,70
And you can actually get away with just doing it like this.

203
00:07:41,70 --> 00:07:42,50
To be honest.

204
00:07:42,50 --> 00:07:45,30
But there's a library to help you do it as well.

205
00:07:45,30 --> 00:07:46,90
So let's quickly install that.

206
00:07:46,90 --> 00:07:52,00
Npm install reselect.

207
00:07:52,00 --> 00:07:55,40
So reselect is the library for helping you do this.

208
00:07:55,40 --> 00:07:57,60
So whilst that's installing, let's come in here.

209
00:07:57,60 --> 00:08:01,60
So in Redux folder, I'm gonna create a selectors folder.

210
00:08:01,60 --> 00:08:04,50
And then I'm gonna have a post selector.

211
00:08:04,50 --> 00:08:07,60
And then as you visit that website,

212
00:08:07,60 --> 00:08:08,70
you'll see there's some sort of code

213
00:08:08,70 --> 00:08:11,20
that you always need to have.

214
00:08:11,20 --> 00:08:14,40
So I'm just gonna export default that.

215
00:08:14,40 --> 00:08:16,90
So the first argument takes state.

216
00:08:16,90 --> 00:08:20,10
And then you could say things like state.posts.

217
00:08:20,10 --> 00:08:21,40
So it's like which stuff.

218
00:08:21,40 --> 00:08:25,10
So the first N arguments are what stuff

219
00:08:25,10 --> 00:08:26,60
in state do you care about.

220
00:08:26,60 --> 00:08:27,70
You could do something like this.

221
00:08:27,70 --> 00:08:30,30
You can basically subscribe to bits of state

222
00:08:30,30 --> 00:08:31,40
that you care about.

223
00:08:31,40 --> 00:08:33,70
And it will only run your function.

224
00:08:33,70 --> 00:08:35,20
If that part of state doesn't change,

225
00:08:35,20 --> 00:08:36,40
it just won't even bother.

226
00:08:36,40 --> 00:08:38,10
So that's kind of a performance optimization,

227
00:08:38,10 --> 00:08:39,80
which is really all this library's for.

228
00:08:39,80 --> 00:08:43,10
And then you're given a, I better show you

229
00:08:43,10 --> 00:08:45,30
a slightly more advanced example in a minute.

230
00:08:45,30 --> 00:08:48,10
So let's say you had users as well.

231
00:08:48,10 --> 00:08:51,70
Then you get one parameter for each part of this.

232
00:08:51,70 --> 00:08:53,70
So this one corresponds to this one.

233
00:08:53,70 --> 00:08:56,40
So whatever this returns will come in here.

234
00:08:56,40 --> 00:08:58,90
So state, so that would be like the post part state.

235
00:08:58,90 --> 00:09:00,50
Whatever this returns comes in here.

236
00:09:00,50 --> 00:09:02,30
You can have as many of these things as you want.

237
00:09:02,30 --> 00:09:03,90
And then you just keep adding them here.

238
00:09:03,90 --> 00:09:04,80
So let's just do postsState,

239
00:09:04,80 --> 00:09:07,10
cause that's actually what this is about.

240
00:09:07,10 --> 00:09:07,90
And then I'm gonna copy the code from here,

241
00:09:07,90 --> 00:09:11,30
because it's actually exactly the same.

242
00:09:11,30 --> 00:09:13,80
Some of the names have changed to other things.

243
00:09:13,80 --> 00:09:16,60
On your way out, we set that, we actually must mean that.

244
00:09:16,60 --> 00:09:19,00
So this is now exactly the same code.

245
00:09:19,00 --> 00:09:21,70
So we're saying go grab me state.posts.

246
00:09:21,70 --> 00:09:23,90
You get that in here, so that's the postsState.

247
00:09:23,90 --> 00:09:25,90
And then I'll say, okay, I just wanna return the postsState,

248
00:09:25,90 --> 00:09:28,00
I'm gonna add this currentPost thing, I'm gonna log it,

249
00:09:28,00 --> 00:09:28,90
and I'm gonna return it.

250
00:09:28,90 --> 00:09:31,60
So that's just wire that in now.

251
00:09:31,60 --> 00:09:33,80
So we shouldn't need this anymore.

252
00:09:33,80 --> 00:09:37,00
And we're gonna use the postsSelector down here.

253
00:09:37,00 --> 00:09:39,80
Then we just need to import the postsSelector.

254
00:09:39,80 --> 00:09:42,00
I've just imported that selector in here.

255
00:09:42,00 --> 00:09:44,90
This selector actually has the same kind of signature

256
00:09:44,90 --> 00:09:46,30
as the function we wrote before.

257
00:09:46,30 --> 00:09:48,10
So you pass it a state.

258
00:09:48,10 --> 00:09:49,70
And you can actually see it most clear down here.

259
00:09:49,70 --> 00:09:51,40
You pass it the whole state.

260
00:09:51,40 --> 00:09:54,10
And it will do whatever it needs to do, and return to you.

261
00:09:54,10 --> 00:09:56,10
Great, so let's see if this is still working.

262
00:09:56,10 --> 00:09:57,70
So, yeah, that's still working, that's brilliant.

263
00:09:57,70 --> 00:10:00,80
So you might be wondering, why use this library?

264
00:10:00,80 --> 00:10:03,20
So the first thing is, it's basically all to do

265
00:10:03,20 --> 00:10:06,60
with something called memoized functions.

266
00:10:06,60 --> 00:10:07,70
But, basically, what it means,

267
00:10:07,70 --> 00:10:09,60
what the word I can't pronounce means,

268
00:10:09,60 --> 00:10:13,30
is that if your function, in this case the selector,

269
00:10:13,30 --> 00:10:16,50
gets called with the same inputs twice,

270
00:10:16,50 --> 00:10:18,40
it just doesn't bother doing it the second time.

271
00:10:18,40 --> 00:10:20,00
Because it goes, well, I kind of know.

272
00:10:20,00 --> 00:10:21,70
I mean, I would sort of call it like a cached function.

273
00:10:21,70 --> 00:10:25,00
It kind of knows, like, if it gets the same inputs again,

274
00:10:25,00 --> 00:10:26,90
it's kinda like, well, I already know the answer

275
00:10:26,90 --> 00:10:28,20
because I know nothing's changed.

276
00:10:28,20 --> 00:10:30,80
Reselect can do that because everything's

277
00:10:30,80 --> 00:10:32,90
written in terms of pure functions.

278
00:10:32,90 --> 00:10:34,50
Because it knows there's no side effects.

279
00:10:34,50 --> 00:10:36,60
And it knows that if you give the same inputs

280
00:10:36,60 --> 00:10:38,60
to the same function twice,

281
00:10:38,60 --> 00:10:41,00
then the way that Redux and all this stuff's working,

282
00:10:41,00 --> 00:10:44,00
reselect works, it knows it can just, you know,

283
00:10:44,00 --> 00:10:45,00
give you the same answer.

284
00:10:45,00 --> 00:10:46,80
Because nothing could have possibly changed.

285
00:10:46,80 --> 00:10:48,90
This concept, I think, is super powerful.

286
00:10:48,90 --> 00:10:50,50
Especially when you start combining

287
00:10:50,50 --> 00:10:52,00
a couple of bits of state.

288
00:10:52,00 --> 00:10:54,30
Things I use it for, typically,

289
00:10:54,30 --> 00:10:57,20
say you've got a dollar amount, or a pound amount,

290
00:10:57,20 --> 00:10:58,70
or whatever currency you're using,

291
00:10:58,70 --> 00:11:01,20
and it's like a float or a double, or whatever,

292
00:11:01,20 --> 00:11:03,10
so it's like 1.01,

293
00:11:03,10 --> 00:11:04,70
and you wanna put a dollar sign in front of it.

294
00:11:04,70 --> 00:11:05,70
I would do that in here.

295
00:11:05,70 --> 00:11:08,00
So I would have like formatted currency.

296
00:11:08,00 --> 00:11:10,00
If I have dates, I'll maybe store them

297
00:11:10,00 --> 00:11:13,10
as a string, in Redux, like, maybe 2000,

298
00:11:13,10 --> 00:11:14,30
something like this.

299
00:11:14,30 --> 00:11:18,90
Then I would use moment inside reselect

300
00:11:18,90 --> 00:11:21,60
to, like, maybe format them into like a really nice,

301
00:11:21,60 --> 00:11:23,90
I don't know, like, you know, it says,

302
00:11:23,90 --> 00:11:26,50
I don't know, whatever it would say, like, you know,

303
00:11:26,50 --> 00:11:29,30
17 years ago, or something.

304
00:11:29,30 --> 00:11:33,30
So you can do kind of like formatting, very much one to one.

305
00:11:33,30 --> 00:11:34,70
You can combine multiple parts

306
00:11:34,70 --> 00:11:36,10
of the same part of Redux state,

307
00:11:36,10 --> 00:11:37,30
which is what we're doing here.

308
00:11:37,30 --> 00:11:39,90
So we're doing sort of a kind of,

309
00:11:39,90 --> 00:11:42,10
we're calculating something based on two bits of state.

310
00:11:42,10 --> 00:11:43,30
So we're using the posts

311
00:11:43,30 --> 00:11:46,00
and we're using the currentPostIndex.

312
00:11:46,00 --> 00:11:48,30
And we're deriving a third thing from it.

313
00:11:48,30 --> 00:11:49,50
It's pretty powerful there.

314
00:11:49,50 --> 00:11:51,30
And then you can do that same operation,

315
00:11:51,30 --> 00:11:54,10
but across two bits of Redux state.

316
00:11:54,10 --> 00:11:57,20
Megapowerful, you can reuse them across multiple components.

317
00:11:57,20 --> 00:12:00,40
So, like, you're probably, if you create a post selector,

318
00:12:00,40 --> 00:12:01,80
you're probably gonna use that post selector

319
00:12:01,80 --> 00:12:03,30
on every screen involving posts.

320
00:12:03,30 --> 00:12:06,40
And that might help you keep your code.

321
00:12:06,40 --> 00:12:09,00
It's not to say that you couldn't keep it neat and tidy,

322
00:12:09,00 --> 00:12:10,40
with no duplication, if you did

323
00:12:10,40 --> 00:12:11,90
just do this in the components.

324
00:12:11,90 --> 00:12:13,20
But what you'll find if go the other route,

325
00:12:13,20 --> 00:12:15,00
the route I originally showed you,

326
00:12:15,00 --> 00:12:16,30
where you do these calculations.

327
00:12:16,30 --> 00:12:19,40
And you can format a currency in React.

328
00:12:19,40 --> 00:12:22,20
Like in your component, it will work, of course it works.

329
00:12:22,20 --> 00:12:24,40
But it just kind of is messy.

330
00:12:24,40 --> 00:12:26,40
And if you're logging this stuff,

331
00:12:26,40 --> 00:12:27,90
your components just get really stupid.

332
00:12:27,90 --> 00:12:29,70
And, actually, if you have a problem,

333
00:12:29,70 --> 00:12:30,70
it's probably in the selector.

334
00:12:30,70 --> 00:12:32,10
And the selector's pure codes.

335
00:12:32,10 --> 00:12:35,00
And you don't need to think about UI's or the dong or Redux.

336
00:12:35,00 --> 00:12:37,90
You're just like, okay, I've got a function,

337
00:12:37,90 --> 00:12:41,40
it takes the Redux stores state, or some bits of it,

338
00:12:41,40 --> 00:12:44,30
and it outputs some stuff, is it outputting the right stuff?

339
00:12:44,30 --> 00:12:45,20
If the answer's yes,

340
00:12:45,20 --> 00:12:47,40
then probably the right thing's going to go on the screen.

341
00:12:47,40 --> 00:12:48,30
And so you'll find that, actually,

342
00:12:48,30 --> 00:12:50,40
your selector's really become the central point

343
00:12:50,40 --> 00:12:52,90
in your application, where a lot of logic will end up.

344
00:12:52,90 --> 00:12:54,10
You know, they can get complicated,

345
00:12:54,10 --> 00:12:55,30
and that's not necessarily good.

346
00:12:55,30 --> 00:12:57,60
But I think it's better to have it there

347
00:12:57,60 --> 00:12:59,00
than to have your react components

348
00:12:59,00 --> 00:13:00,40
get really, really complicated.

349
00:13:00,40 --> 00:13:02,50
And so I'd really recommend like checking it out,

350
00:13:02,50 --> 00:13:03,70
even if you don't quite understand

351
00:13:03,70 --> 00:13:06,20
what I've been going on about for the last few minutes.

352
00:13:06,20 --> 00:13:09,10
Basically, it's a way of applying extra transformations

353
00:13:09,10 --> 00:13:11,70
to your Redux store, before it gets to your components.

354
00:13:11,70 --> 00:13:13,80
And then you just use it the connect.

355
00:13:13,80 --> 00:13:15,90
And it does it in a way that, apparently,

356
00:13:15,90 --> 00:13:20,40
is efficient and doesn't cause the performance to suck.

357
00:13:20,40 --> 00:13:22,60
So, yeah, I think it's really cool.

358
00:13:22,60 --> 00:13:25,20
There's also another library, which is not very well used,

359
00:13:25,20 --> 00:13:29,00
which is called, react reselect change memoize.

360
00:13:29,00 --> 00:13:31,60
And you can grab it on GitHub, it's not used very much,

361
00:13:31,60 --> 00:13:33,00
but I think it's a really great module.

362
00:13:33,00 --> 00:13:35,30
And what it does is it gives you a function like this.

363
00:13:35,30 --> 00:13:38,50
Which will log the output of your selectors, every time.

364
00:13:38,50 --> 00:13:40,00
Kind of like Redux has been doing.

365
00:13:40,00 --> 00:13:42,90
I think that's really useful for debugging as well.

366
00:13:42,90 --> 00:13:46,00
So, yeah, I think this is a great, like, you're using Redux.

367
00:13:46,00 --> 00:13:47,70
It's a slightly advanced topic,

368
00:13:47,70 --> 00:13:49,20
but I think it's really worthwhile.

369
00:13:49,20 --> 00:13:51,10
You saw how you can use reselect

370
00:13:51,10 --> 00:13:54,60
to embellish your data from the Redux store.

371
00:13:54,60 --> 00:13:57,80
Without actually storing it inside the Redux store.

372
00:13:57,80 --> 00:13:58,70
In this section,

373
00:13:58,70 --> 00:14:02,10
we saw some of the advanced Redux concepts in action.

374
00:14:02,10 --> 00:14:05,30
We looked at advanced reducers,

375
00:14:05,30 --> 00:14:07,30
configuring how to configure your application

376
00:14:07,30 --> 00:14:09,40
with Redux in the first place,

377
00:14:09,40 --> 00:14:11,30
where to put your code when you're using Redux,

378
00:14:11,30 --> 00:14:14,20
and somethings that you must not do when using it,

379
00:14:14,20 --> 00:14:17,60
using Thunk so that we could dispatch asynchronous actions,

380
00:14:17,60 --> 00:14:21,50
and using reselect and select to structure your code better.

381
00:14:21,50 --> 00:14:23,30
In the next section, we're gonna be looking at

382
00:14:23,30 --> 00:14:25,00
building a real-life application with Redux.

