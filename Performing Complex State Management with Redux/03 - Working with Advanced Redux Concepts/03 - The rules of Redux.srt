1
00:00:01,10 --> 00:00:03,00
- [Instructor] The Rules of Redux.

2
00:00:03,00 --> 00:00:05,10
In this video I'm gonna cover

3
00:00:05,10 --> 00:00:06,90
the sorts of things you can and can't do

4
00:00:06,90 --> 00:00:09,60
and where you should put certain code

5
00:00:09,60 --> 00:00:11,00
when you're using Redux.

6
00:00:11,00 --> 00:00:13,90
So let's start with the things that you absolutely can't do.

7
00:00:13,90 --> 00:00:16,40
And most of the rules in Redux that are kind of like

8
00:00:16,40 --> 00:00:19,90
set in stone are really in the reducer.

9
00:00:19,90 --> 00:00:23,10
So I kind of touched on this before in a previous video,

10
00:00:23,10 --> 00:00:25,20
but essentially in the reducer

11
00:00:25,20 --> 00:00:27,60
you are forbidden to do a few things.

12
00:00:27,60 --> 00:00:29,90
Your reducer function, so this function

13
00:00:29,90 --> 00:00:31,50
needs to be a pure function,

14
00:00:31,50 --> 00:00:33,80
which means a couple of things.

15
00:00:33,80 --> 00:00:35,70
So the definition of a pure function

16
00:00:35,70 --> 00:00:38,40
is that it's a function where the return value

17
00:00:38,40 --> 00:00:41,40
is only determined by its input values

18
00:00:41,40 --> 00:00:47,00
without observable side effects.

19
00:00:47,00 --> 00:00:47,80
So what does that mean?

20
00:00:47,80 --> 00:00:48,70
That's like a big sentence.

21
00:00:48,70 --> 00:00:50,40
So it's return value is only determined by

22
00:00:50,40 --> 00:00:53,60
its input values without observable side effects.

23
00:00:53,60 --> 00:00:55,70
So basically what it's saying is

24
00:00:55,70 --> 00:00:57,70
you can use state and you can use action,

25
00:00:57,70 --> 00:01:00,90
'cause they're the inputs and you return something.

26
00:01:00,90 --> 00:01:03,30
So we return this and we return that.

27
00:01:03,30 --> 00:01:05,00
Don't produce any side effects.

28
00:01:05,00 --> 00:01:07,10
So I'll give that to you in layman's terms

29
00:01:07,10 --> 00:01:07,90
of the sorts of things

30
00:01:07,90 --> 00:01:09,80
you might be tempted to do that you shouldn't.

31
00:01:09,80 --> 00:01:12,30
So don't mutate anything, that's the first thing.

32
00:01:12,30 --> 00:01:14,30
So here you might be tempted to say

33
00:01:14,30 --> 00:01:19,30
state.posts.push, action.payload or something.

34
00:01:19,30 --> 00:01:20,10
Thank you, doc.

35
00:01:20,10 --> 00:01:22,00
You're mutating posts, you shouldn't mutate things.

36
00:01:22,00 --> 00:01:24,30
That's why we're using the spreadout operator everywhere,

37
00:01:24,30 --> 00:01:25,50
'cause it doesn't mutate things.

38
00:01:25,50 --> 00:01:28,30
I think this might even break Redux if you do this.

39
00:01:28,30 --> 00:01:30,80
I don't think it works, it gets confused.

40
00:01:30,80 --> 00:01:32,40
It assumes that you're not gonna do that

41
00:01:32,40 --> 00:01:34,00
and it has some performance optimizations

42
00:01:34,00 --> 00:01:37,40
which might then break and your code might stop working.

43
00:01:37,40 --> 00:01:38,30
So don't do that.

44
00:01:38,30 --> 00:01:39,20
The other type of thing,

45
00:01:39,20 --> 00:01:41,20
when it says observable side effects,

46
00:01:41,20 --> 00:01:44,30
is it mean don't do something like fetch,

47
00:01:44,30 --> 00:01:47,70
like make a post request or something,

48
00:01:47,70 --> 00:01:50,40
or make a http request to a server

49
00:01:50,40 --> 00:01:52,80
'cause that's obviously an observable side effect.

50
00:01:52,80 --> 00:01:55,70
And if you do that then this function,

51
00:01:55,70 --> 00:01:58,20
you can't be guaranteed when you run it two times in a row,

52
00:01:58,20 --> 00:01:59,20
it will do the same thing,

53
00:01:59,20 --> 00:02:00,40
which is really what they're saying.

54
00:02:00,40 --> 00:02:03,10
They're saying for the same input,

55
00:02:03,10 --> 00:02:05,50
this function should always have the same output.

56
00:02:05,50 --> 00:02:07,40
That's effectively really what they really mean

57
00:02:07,40 --> 00:02:09,10
when they say a pure function.

58
00:02:09,10 --> 00:02:12,10
And that way, you know, that's quite like a rigorous thing

59
00:02:12,10 --> 00:02:13,90
that you can kind of,

60
00:02:13,90 --> 00:02:15,20
that you can be sure if that holds true

61
00:02:15,20 --> 00:02:17,70
and makes it easy to reason about your program.

62
00:02:17,70 --> 00:02:19,80
And so Redux is really based on

63
00:02:19,80 --> 00:02:21,90
that kind of functional principle.

64
00:02:21,90 --> 00:02:24,00
So those are the rules for reducers.

65
00:02:24,00 --> 00:02:25,30
There was some stuff I touched on before

66
00:02:25,30 --> 00:02:28,00
about not making your reducers too heavy

67
00:02:28,00 --> 00:02:29,10
and having too much stuff.

68
00:02:29,10 --> 00:02:31,70
That's not a hard rule, but I think it's pretty good advice.

69
00:02:31,70 --> 00:02:33,90
So what other rules of Redux are there?

70
00:02:33,90 --> 00:02:37,00
So the other rules are really about what happens where.

71
00:02:37,00 --> 00:02:42,10
So let's start with the components.

72
00:02:42,10 --> 00:02:45,00
In the components, generally in regards to Redux,

73
00:02:45,00 --> 00:02:47,40
what you might do, is you might connect,

74
00:02:47,40 --> 00:02:49,70
well you will connect if you're using Redux,

75
00:02:49,70 --> 00:02:52,50
and you'll grab the bit of the state that you care about.

76
00:02:52,50 --> 00:02:55,30
Always try and grab the minimum amount of states.

77
00:02:55,30 --> 00:02:57,30
You could just always get all the state,

78
00:02:57,30 --> 00:02:59,70
yet if you can just make it a smaller part.

79
00:02:59,70 --> 00:03:01,30
In here, what you're entitled to do

80
00:03:01,30 --> 00:03:04,00
is use props to go get things from state

81
00:03:04,00 --> 00:03:05,30
once you've connected.

82
00:03:05,30 --> 00:03:06,90
You can do that, that's fine.

83
00:03:06,90 --> 00:03:09,30
And you can dispatch actions like this,

84
00:03:09,30 --> 00:03:11,50
usually by saying this.props.dispatch

85
00:03:11,50 --> 00:03:13,70
as long as you're connected and then you do this.

86
00:03:13,70 --> 00:03:16,70
That's all you're really allowed to do in posts,

87
00:03:16,70 --> 00:03:18,00
is dispatch actions.

88
00:03:18,00 --> 00:03:20,10
Now, actions are quite interesting,

89
00:03:20,10 --> 00:03:23,50
so usually what I'll do is I'll create a folder,

90
00:03:23,50 --> 00:03:24,40
I'm just gonna go ahead and create it here.

91
00:03:24,40 --> 00:03:27,60
Call it actions in the Redux folder.

92
00:03:27,60 --> 00:03:30,20
I like to have a Redux folder full of Redux stuff.

93
00:03:30,20 --> 00:03:33,00
Some people like to couple them with components.

94
00:03:33,00 --> 00:03:36,20
You can do either, none of them's necessarily wrong.

95
00:03:36,20 --> 00:03:39,70
The reason I like having Redux stuff in it's own folder,

96
00:03:39,70 --> 00:03:41,40
is because in more complicated apps

97
00:03:41,40 --> 00:03:44,00
you'll find that often you'll have a set of components

98
00:03:44,00 --> 00:03:47,00
in reducer that really is related to that component.

99
00:03:47,00 --> 00:03:48,40
You can really see it in this app.

100
00:03:48,40 --> 00:03:50,10
We've got RedditPosts, RedditPosts,

101
00:03:50,10 --> 00:03:52,40
and we've got postReducer, so you kind of like,

102
00:03:52,40 --> 00:03:54,50
maybe you're gonna have a folder for post stuff

103
00:03:54,50 --> 00:03:56,70
and you'll put the reducer and the actions for post stuff

104
00:03:56,70 --> 00:03:58,40
in there and the components together.

105
00:03:58,40 --> 00:04:00,70
That sort of makes sense, except for,

106
00:04:00,70 --> 00:04:03,90
sometimes you might have things, for example, user,

107
00:04:03,90 --> 00:04:06,20
where there's many components that use user

108
00:04:06,20 --> 00:04:07,30
and then it becomes a bit difficult

109
00:04:07,30 --> 00:04:08,30
like where do I put this,

110
00:04:08,30 --> 00:04:11,40
because you've got kind of like one thing in Redux,

111
00:04:11,40 --> 00:04:12,80
like one part of the store,

112
00:04:12,80 --> 00:04:15,20
which is actually used by lots of different components.

113
00:04:15,20 --> 00:04:17,20
That's why I kind of just like to keep it consistent

114
00:04:17,20 --> 00:04:19,70
and have, 'cause you know this is never gonna break,

115
00:04:19,70 --> 00:04:21,60
you also kinda know where to find everything.

116
00:04:21,60 --> 00:04:23,30
Anyway, so that's how I like to structure it.

117
00:04:23,30 --> 00:04:26,00
I'll often have an actions file, which you've not seen yet,

118
00:04:26,00 --> 00:04:27,50
so let's have a look at that.

119
00:04:27,50 --> 00:04:30,40
I'll just call it posts.js inside of actions.

120
00:04:30,40 --> 00:04:33,60
And then all I'll do is export const and,

121
00:04:33,60 --> 00:04:34,40
what's the one we already have?

122
00:04:34,40 --> 00:04:36,60
Let's use that as an example.

123
00:04:36,60 --> 00:04:37,90
So we've got newPost,

124
00:04:37,90 --> 00:04:40,10
so let's create an action called newPost,

125
00:04:40,10 --> 00:04:43,00
so you just call it newPost

126
00:04:43,00 --> 00:04:46,30
and it's a function and you just return,

127
00:04:46,30 --> 00:04:47,30
do it as a big function,

128
00:04:47,30 --> 00:04:49,60
return and then just gonna grab that,

129
00:04:49,60 --> 00:04:50,50
yep, so that's fine.

130
00:04:50,50 --> 00:04:52,60
And then, in here you would say,

131
00:04:52,60 --> 00:04:54,40
you would call that function like that.

132
00:04:54,40 --> 00:04:56,40
This code's the same as the code we just had

133
00:04:56,40 --> 00:05:00,00
because it would just jump in here and return this.

134
00:05:00,00 --> 00:05:02,70
So it's the same code, it really is.

135
00:05:02,70 --> 00:05:04,30
It's just been rearranged basically,

136
00:05:04,30 --> 00:05:05,80
but I think the rearrange is worth it.

137
00:05:05,80 --> 00:05:10,30
So let me, from redux, actions, post,

138
00:05:10,30 --> 00:05:13,50
bring it to the bottom.

139
00:05:13,50 --> 00:05:16,00
Cool, so that's still firing just as it was before.

140
00:05:16,00 --> 00:05:18,20
So yeah, I mean, that's really it.

141
00:05:18,20 --> 00:05:20,70
In the actions if you've got lots of stuff

142
00:05:20,70 --> 00:05:23,50
you want to do when something happens,

143
00:05:23,50 --> 00:05:25,90
the action is probably the right place to put it.

144
00:05:25,90 --> 00:05:27,70
So if you need to make http calls,

145
00:05:27,70 --> 00:05:30,00
if you need to go away and call out the services,

146
00:05:30,00 --> 00:05:31,20
usually you'll see a lot of people

147
00:05:31,20 --> 00:05:32,30
put that in their actions.

148
00:05:32,30 --> 00:05:34,90
So you're kind of making http calls

149
00:05:34,90 --> 00:05:39,00
and you'll see that with the funk video later on.

150
00:05:39,00 --> 00:05:41,00
That's actions, actions do stuff,

151
00:05:41,00 --> 00:05:42,20
they're kinda like doing things.

152
00:05:42,20 --> 00:05:44,90
Normally you should name them in a kind of verby way,

153
00:05:44,90 --> 00:05:48,40
so use verbs like create newPost or delete this.

154
00:05:48,40 --> 00:05:50,90
It's kind of like a doing thing, 'cause it's an action.

155
00:05:50,90 --> 00:05:53,20
Whereas your reducers and store names

156
00:05:53,20 --> 00:05:54,40
are more likely to be nouns

157
00:05:54,40 --> 00:05:57,20
because normally they're just stuff.

158
00:05:57,20 --> 00:05:59,00
That's the actions and that's what goes there.

159
00:05:59,00 --> 00:06:01,30
And your components are more than welcome to import

160
00:06:01,30 --> 00:06:03,80
these actions like this and dispatch them like that.

161
00:06:03,80 --> 00:06:06,80
That's fine, okay, cool.

162
00:06:06,80 --> 00:06:08,90
Then we get to the reducers.

163
00:06:08,90 --> 00:06:10,00
The reducers really, all they do,

164
00:06:10,00 --> 00:06:11,40
we kinda talked about the rules before,

165
00:06:11,40 --> 00:06:13,80
but they take the state, the previous state and the action

166
00:06:13,80 --> 00:06:16,60
and return the state by not mutating anything

167
00:06:16,60 --> 00:06:18,70
and don't do any side effects.

168
00:06:18,70 --> 00:06:20,30
And then on top of that, there's selectors,

169
00:06:20,30 --> 00:06:23,70
which we'll carry, we'll talk about that in a bit.

170
00:06:23,70 --> 00:06:26,90
What I really like about Redux, which I think is, for me,

171
00:06:26,90 --> 00:06:28,50
well, the Flux design pattern in general,

172
00:06:28,50 --> 00:06:31,60
but particularly Redux, is I think the rules are very clear.

173
00:06:31,60 --> 00:06:32,80
And so I don't get confused.

174
00:06:32,80 --> 00:06:34,80
I used to use other things that are similar to this,

175
00:06:34,80 --> 00:06:36,90
'cause Redux is really a design pattern.

176
00:06:36,90 --> 00:06:39,30
Flux and the design patterns that I used before

177
00:06:39,30 --> 00:06:41,60
the Flux design pattern, I used to get confused a lot,

178
00:06:41,60 --> 00:06:43,30
like where does this code live?

179
00:06:43,30 --> 00:06:45,30
Maybe I just never understood anything as well as this,

180
00:06:45,30 --> 00:06:47,30
but I think the rules are super simple.

181
00:06:47,30 --> 00:06:49,20
You can dispatch actions.

182
00:06:49,20 --> 00:06:52,90
When actions dispatch, one or more reducers does some stuff

183
00:06:52,90 --> 00:06:55,80
and updates the state it's responsible for,

184
00:06:55,80 --> 00:06:57,80
and then that gets changed in the store.

185
00:06:57,80 --> 00:06:59,40
Components can connect to the store

186
00:06:59,40 --> 00:07:01,30
and they render them like they're dumb,

187
00:07:01,30 --> 00:07:03,00
they just go, oh, there's some stuff in the store,

188
00:07:03,00 --> 00:07:04,00
I'm gonna put it on the screen.

189
00:07:04,00 --> 00:07:05,80
That's it, just using props.

190
00:07:05,80 --> 00:07:08,30
And then they can fire actions if they want

191
00:07:08,30 --> 00:07:11,10
when uses quick things or do things.

192
00:07:11,10 --> 00:07:12,40
And you can do all of this as well

193
00:07:12,40 --> 00:07:14,30
just programmatically using the store.

194
00:07:14,30 --> 00:07:16,00
For me, that's Redux in a nutshell.

195
00:07:16,00 --> 00:07:17,10
There's only one other concept

196
00:07:17,10 --> 00:07:18,40
which I think is pretty important,

197
00:07:18,40 --> 00:07:19,90
but you don't need it to use Redux,

198
00:07:19,90 --> 00:07:22,30
which is selectors or reselect.

199
00:07:22,30 --> 00:07:27,40
Which is about changing the Redux data into the store data

200
00:07:27,40 --> 00:07:29,00
before it gets into your components.

201
00:07:29,00 --> 00:07:31,50
So you kind of run some functions to change it,

202
00:07:31,50 --> 00:07:33,90
but you don't need that.

203
00:07:33,90 --> 00:07:35,50
Those are kinda the rules, those are things you can do.

204
00:07:35,50 --> 00:07:40,50
You can't fire actions from reducers,

205
00:07:40,50 --> 00:07:43,30
normally you'll only be firing actions from your app.

206
00:07:43,30 --> 00:07:45,10
Those are the rules of Redux.

207
00:07:45,10 --> 00:07:46,80
We just had a look at the rules of Redux,

208
00:07:46,80 --> 00:07:49,70
so where you can put certain bits of code,

209
00:07:49,70 --> 00:07:54,50
so asynchronous code should probably live in your actions.

210
00:07:54,50 --> 00:07:57,10
You mustn't do any thing other than

211
00:07:57,10 --> 00:07:59,40
returning state inside reducers

212
00:07:59,40 --> 00:08:01,00
and basically just provided a blueprint

213
00:08:01,00 --> 00:08:03,50
of where different bits of code should go.

214
00:08:03,50 --> 00:08:05,10
In the next video we'll be using

215
00:08:05,10 --> 00:08:08,00
Thunks to dispatch asynchronous actions.

