1
00:00:01,40 --> 00:00:04,30
- [Instructor] Configuring Redux with your application.

2
00:00:04,30 --> 00:00:05,50
In this video we're gonna look at

3
00:00:05,50 --> 00:00:07,80
how to set up Redux for the first time.

4
00:00:07,80 --> 00:00:09,10
That's something that I had already done

5
00:00:09,10 --> 00:00:10,30
before we started this course,

6
00:00:10,30 --> 00:00:12,40
but now I'm gonna go through it step by step.

7
00:00:12,40 --> 00:00:15,30
So we've kind of done this course so far

8
00:00:15,30 --> 00:00:16,20
a little bit backwards.

9
00:00:16,20 --> 00:00:17,90
So I've shown you how Redux works,

10
00:00:17,90 --> 00:00:19,70
how you can use Redux,

11
00:00:19,70 --> 00:00:22,00
but I never showed you how to actually set it up.

12
00:00:22,00 --> 00:00:23,60
And that's because setting up Redux

13
00:00:23,60 --> 00:00:25,00
is a little bit complicated,

14
00:00:25,00 --> 00:00:27,50
especially if you don't understand the concepts of Redux.

15
00:00:27,50 --> 00:00:30,70
And so we're gonna go back in a time machine now

16
00:00:30,70 --> 00:00:33,20
and we're gonna go to this code base before

17
00:00:33,20 --> 00:00:35,30
Redux was actually added.

18
00:00:35,30 --> 00:00:39,30
I'm gonna show you how I added it beforehand.

19
00:00:39,30 --> 00:00:42,10
So here's the code base before I've added anything.

20
00:00:42,10 --> 00:00:45,20
There's now Redux stuff going on in here.

21
00:00:45,20 --> 00:00:47,80
So the first thing we need to do is install some packages.

22
00:00:47,80 --> 00:00:50,50
I'm gonna do that first.

23
00:00:50,50 --> 00:00:54,20
So we need to install npm install redux, redux-logger,

24
00:00:54,20 --> 00:00:57,60
redux-thunk, and redux-freeze.

25
00:00:57,60 --> 00:00:59,50
So I'm just installing those in the background.

26
00:00:59,50 --> 00:01:00,40
They've all been added.

27
00:01:00,40 --> 00:01:05,40
Let's just confirm that they've been added.

28
00:01:05,40 --> 00:01:07,10
So you can see all of those dependencies

29
00:01:07,10 --> 00:01:08,60
have been added in here.

30
00:01:08,60 --> 00:01:10,60
And now we're gonna go ahead and set that up.

31
00:01:10,60 --> 00:01:13,90
So I'm gonna create a folder just for the Redux stuff.

32
00:01:13,90 --> 00:01:18,10
So I'm gonna call that Redux

33
00:01:18,10 --> 00:01:22,00
and in there I'm gonna create a file called store.js,

34
00:01:22,00 --> 00:01:24,50
which is just the store,

35
00:01:24,50 --> 00:01:26,70
which is what we discussed earlier on.

36
00:01:26,70 --> 00:01:29,40
So let me just paste a bunch of code in here

37
00:01:29,40 --> 00:01:31,30
and then I'm gonna run you through it.

38
00:01:31,30 --> 00:01:34,40
So we're importing a couple of methods from Redux itself,

39
00:01:34,40 --> 00:01:37,20
so applyMiddleware and createStore.

40
00:01:37,20 --> 00:01:40,70
We're importing some what are called Redux middleware.

41
00:01:40,70 --> 00:01:42,90
So we've got three middlewares here,

42
00:01:42,90 --> 00:01:46,70
redux-logger, redux-thunk, and redux-freeze.

43
00:01:46,70 --> 00:01:49,90
Freeze I think is optional in almost all projects.

44
00:01:49,90 --> 00:01:51,30
Thunk, or something like it,

45
00:01:51,30 --> 00:01:52,40
I think you're almost gonna need

46
00:01:52,40 --> 00:01:54,60
for every practical project you do.

47
00:01:54,60 --> 00:01:56,60
And I personal always recommend having the logger

48
00:01:56,60 --> 00:01:58,90
'cause it makes it way easier to see what's going on,

49
00:01:58,90 --> 00:02:01,10
although you could use the Redux dev tools as well,

50
00:02:01,10 --> 00:02:03,20
which do a similar sort of thing.

51
00:02:03,20 --> 00:02:06,60
So we've imported lodash and then the rootReducer,

52
00:02:06,60 --> 00:02:08,40
which doesn't exist yet.

53
00:02:08,40 --> 00:02:10,90
So, let just run through this code.

54
00:02:10,90 --> 00:02:12,80
Let's imagine the rootReducer does exist

55
00:02:12,80 --> 00:02:14,30
and I'll explain what that does.

56
00:02:14,30 --> 00:02:15,70
So there's a little bit of code here,

57
00:02:15,70 --> 00:02:19,40
which actually perhaps isn't needed.

58
00:02:19,40 --> 00:02:21,20
So if we run through this code line by line,

59
00:02:21,20 --> 00:02:22,50
we're creating the logger.

60
00:02:22,50 --> 00:02:25,30
So what the logger does, it just logs the Redux messages

61
00:02:25,30 --> 00:02:26,70
you've been seeing to the console.

62
00:02:26,70 --> 00:02:27,60
That's it.

63
00:02:27,60 --> 00:02:29,30
Then we grab, once we've got the logger,

64
00:02:29,30 --> 00:02:31,50
we take thunk, freeze, and logger,

65
00:02:31,50 --> 00:02:33,40
which are our three middlewares,

66
00:02:33,40 --> 00:02:34,60
we compact them,

67
00:02:34,60 --> 00:02:36,30
so that just means that if any of them are null,

68
00:02:36,30 --> 00:02:37,20
it gets rid of them.

69
00:02:37,20 --> 00:02:38,10
So this doesn't really do anything.

70
00:02:38,10 --> 00:02:39,70
Then we've got our middlewares.

71
00:02:39,70 --> 00:02:41,10
And then we apply the middlewares

72
00:02:41,10 --> 00:02:44,10
using this spread operator and pass createStore.

73
00:02:44,10 --> 00:02:46,10
All of this is detailed on the Redux website.

74
00:02:46,10 --> 00:02:48,40
You can probably follow it from there.

75
00:02:48,40 --> 00:02:50,90
And then we call createStore with middleware.

76
00:02:50,90 --> 00:02:53,20
We pass in this rootReducer, which I'll show you in a sec

77
00:02:53,20 --> 00:02:54,60
and then we've got the store.

78
00:02:54,60 --> 00:02:56,40
And then export default the store.

79
00:02:56,40 --> 00:02:57,70
The reason I do this,

80
00:02:57,70 --> 00:02:59,60
is because in Redux it's pretty handy

81
00:02:59,60 --> 00:03:01,40
to be able to get ahold of your store

82
00:03:01,40 --> 00:03:05,10
and then you can call store.dispatch to dispatch actions

83
00:03:05,10 --> 00:03:07,40
or you can actually take a look at the state of the store.

84
00:03:07,40 --> 00:03:08,90
And if you've got stuff you wanna do

85
00:03:08,90 --> 00:03:10,60
outside React or whatever,

86
00:03:10,60 --> 00:03:13,00
you know, maybe you're using like React Connects,

87
00:03:13,00 --> 00:03:14,70
but maybe you wanna do some stuff, you know,

88
00:03:14,70 --> 00:03:16,90
that isn't tied to your front end at all,

89
00:03:16,90 --> 00:03:18,50
which is quite possible.

90
00:03:18,50 --> 00:03:20,10
And, you know, for example,

91
00:03:20,10 --> 00:03:23,10
maybe you get a web socket message over the network

92
00:03:23,10 --> 00:03:25,40
and you wanna do store to dispatch something,

93
00:03:25,40 --> 00:03:26,70
then you probably don't really wanna have

94
00:03:26,70 --> 00:03:28,00
a React component for that.

95
00:03:28,00 --> 00:03:30,20
So you can just grab this store and just dispatch things,

96
00:03:30,20 --> 00:03:32,00
which is pretty handy.

97
00:03:32,00 --> 00:03:34,90
Cool, so let's quickly define the rootReducer.

98
00:03:34,90 --> 00:03:39,10
So the rootReducer for this is really simple.

99
00:03:39,10 --> 00:03:43,10
So we grab the combineReducers function from Redux,

100
00:03:43,10 --> 00:03:47,10
the posts function from our post reducer,

101
00:03:47,10 --> 00:03:48,70
which I'll create in a second.

102
00:03:48,70 --> 00:03:49,90
And then what we're doing is we're combining

103
00:03:49,90 --> 00:03:51,70
all the reducers of which there are only one

104
00:03:51,70 --> 00:03:54,10
and we're just adding the post part in there.

105
00:03:54,10 --> 00:03:56,60
And then we export that, and that's it.

106
00:03:56,60 --> 00:03:58,30
And then if you wanna add more reducers,

107
00:03:58,30 --> 00:03:59,60
you just add them in here.

108
00:03:59,60 --> 00:04:02,70
So I'm gonna create a new folder now called reducers.

109
00:04:02,70 --> 00:04:04,90
I like to keep everything in separate folders,

110
00:04:04,90 --> 00:04:07,10
but you can put them in whatever folders you want really,

111
00:04:07,10 --> 00:04:08,20
it doesn't matter.

112
00:04:08,20 --> 00:04:12,40
So in posts.js, I'm gonna create a really simple reducer

113
00:04:12,40 --> 00:04:13,60
that doesn't do a lot,

114
00:04:13,60 --> 00:04:16,00
'cause it's just an empty reducer.

115
00:04:16,00 --> 00:04:29,40
Now let's get and run, see if it works.

116
00:04:29,40 --> 00:04:31,70
So just one more step we need to do

117
00:04:31,70 --> 00:04:35,30
and that's actually instantiate Redux

118
00:04:35,30 --> 00:04:37,20
when we start our application.

119
00:04:37,20 --> 00:04:39,30
So the last step we need to do

120
00:04:39,30 --> 00:04:43,60
is to install one more npm module,

121
00:04:43,60 --> 00:04:47,60
npm install react-redux.

122
00:04:47,60 --> 00:04:53,40
So this is a library that let's us connect React to Redux.

123
00:04:53,40 --> 00:04:54,70
Okay, great.

124
00:04:54,70 --> 00:04:59,10
And then we need to import both the store

125
00:04:59,10 --> 00:05:00,60
and that library.

126
00:05:00,60 --> 00:05:01,50
So I'm gonna do that here.

127
00:05:01,50 --> 00:05:05,70
So I've imported Provider from react-redux

128
00:05:05,70 --> 00:05:11,30
and then I think we need to say ./redux/store.

129
00:05:11,30 --> 00:05:12,40
Yep, to import the store.

130
00:05:12,40 --> 00:05:13,70
So we've got the store.

131
00:05:13,70 --> 00:05:18,60
And then last we just add this provider code

132
00:05:18,60 --> 00:05:21,30
around our root component.

133
00:05:21,30 --> 00:05:23,40
Now you should probably have a component

134
00:05:23,40 --> 00:05:24,90
that doesn't really do much.

135
00:05:24,90 --> 00:05:27,20
This one has some stuff inside it,

136
00:05:27,20 --> 00:05:28,40
which is a bit of mess.

137
00:05:28,40 --> 00:05:30,40
And that's probably not how you would do it,

138
00:05:30,40 --> 00:05:31,60
but it proves the point.

139
00:05:31,60 --> 00:05:34,20
So Provider store equals store,

140
00:05:34,20 --> 00:05:36,00
so you pass that in and then you put the (mumbles)

141
00:05:36,00 --> 00:05:38,60
of that as just being like whatever your app does.

142
00:05:38,60 --> 00:05:44,20
So let's give that a run and see if it works.

143
00:05:44,20 --> 00:05:51,40
Okay, great.

144
00:05:51,40 --> 00:05:53,30
Great, so that seems to have worked.

145
00:05:53,30 --> 00:05:54,90
Let's just quickly fire an action

146
00:05:54,90 --> 00:05:57,10
to check that it is actually working.

147
00:05:57,10 --> 00:05:59,40
So I'm gonna fire the action direct from here.

148
00:05:59,40 --> 00:06:02,80
I'm just gonna say store.dispatch

149
00:06:02,80 --> 00:06:06,00
and then I'll do like type HELLO.

150
00:06:06,00 --> 00:06:07,30
That should do it.

151
00:06:07,30 --> 00:06:08,20
Okay, cool.

152
00:06:08,20 --> 00:06:10,00
So now you can see Redux logging some stuff.

153
00:06:10,00 --> 00:06:11,00
So it's setup.

154
00:06:11,00 --> 00:06:12,50
So in this state we've got posts

155
00:06:12,50 --> 00:06:13,90
and posts currently empty.

156
00:06:13,90 --> 00:06:15,70
You can see we've filed an action here.

157
00:06:15,70 --> 00:06:18,10
And before posts was empty as well.

158
00:06:18,10 --> 00:06:19,80
So now it's all set up and it's ready to go.

159
00:06:19,80 --> 00:06:22,10
And that's all there really is to it.

160
00:06:22,10 --> 00:06:24,40
We'll provide the code samples with this video,

161
00:06:24,40 --> 00:06:26,80
so that if you wanna setup for yourself you can,

162
00:06:26,80 --> 00:06:28,30
but to be honest, if I were you,

163
00:06:28,30 --> 00:06:30,20
I would probably just like kinda Google

164
00:06:30,20 --> 00:06:32,40
how do I start React Redux.

165
00:06:32,40 --> 00:06:34,50
Maybe think about using that logger middleware

166
00:06:34,50 --> 00:06:36,80
and you'll find a bunch of code snippets that are useful.

167
00:06:36,80 --> 00:06:38,90
The only thing I would say is it's really useful to be able

168
00:06:38,90 --> 00:06:42,30
to import your store so that you can get it in any file

169
00:06:42,30 --> 00:06:45,40
and then say store to dispatch like I did in this example.

170
00:06:45,40 --> 00:06:48,10
We just had a look at how you can set up Redux

171
00:06:48,10 --> 00:06:50,10
for your application.

172
00:06:50,10 --> 00:06:54,00
In the next video we're gonna take a look at rules of Redux.

