1
00:00:00,60 --> 00:00:02,90
- [Instructor] Using Thunk to dispatch Asynchronous Actions.

2
00:00:02,90 --> 00:00:05,30
In this video, we're gonna take a look at

3
00:00:05,30 --> 00:00:07,70
the limitations of dispatching actions

4
00:00:07,70 --> 00:00:09,10
with what we've seen so far.

5
00:00:09,10 --> 00:00:11,80
And then how we can use Thunk to help us

6
00:00:11,80 --> 00:00:14,30
cater for some more complex use cases.

7
00:00:14,30 --> 00:00:17,50
So if you take a look at our existing action,

8
00:00:17,50 --> 00:00:20,30
we've got newPost and we've got lambda,

9
00:00:20,30 --> 00:00:25,70
and then we return an action.

10
00:00:25,70 --> 00:00:27,30
So an action is an object

11
00:00:27,30 --> 00:00:29,60
which has a type and maybe a payload.

12
00:00:29,60 --> 00:00:31,50
We can do some stuff here.

13
00:00:31,50 --> 00:00:34,10
Like this, we can do some stuff here.

14
00:00:34,10 --> 00:00:37,40
We can do some more stuff, which is great.

15
00:00:37,40 --> 00:00:39,40
And then we'll return there so we get on with it.

16
00:00:39,40 --> 00:00:42,40
What if, so, I guess, one case,

17
00:00:42,40 --> 00:00:45,20
what if we wanted to do some stuff here?

18
00:00:45,20 --> 00:00:47,00
We can.

19
00:00:47,00 --> 00:00:53,20
And what if we wanna do something like a HTTP request?

20
00:00:53,20 --> 00:00:54,50
I'm gonna use lodash to show this.

21
00:00:54,50 --> 00:00:57,60
But if something asynchronous was happening,

22
00:00:57,60 --> 00:01:00,60
and we delay that by two seconds,

23
00:01:00,60 --> 00:01:02,20
I'll show you the whole picture in a sec.

24
00:01:02,20 --> 00:01:05,30
So these are the cases that we can't really cater for,

25
00:01:05,30 --> 00:01:07,80
using this just for returning an object.

26
00:01:07,80 --> 00:01:10,60
So we can do work up front, that's fine.

27
00:01:10,60 --> 00:01:12,70
We can't do work after we return.

28
00:01:12,70 --> 00:01:13,80
So maybe you wanna do that,

29
00:01:13,80 --> 00:01:14,80
maybe you wanna dispatch something

30
00:01:14,80 --> 00:01:16,50
and then do something afterwards, I don't know why,

31
00:01:16,50 --> 00:01:17,60
but you just can't do that.

32
00:01:17,60 --> 00:01:20,10
Because return, this is the end of this function.

33
00:01:20,10 --> 00:01:22,70
And this line will never get run or it's an arrow.

34
00:01:22,70 --> 00:01:26,10
So can't do that, so I'll delete that for now.

35
00:01:26,10 --> 00:01:28,80
And then what about this says delay.

36
00:01:28,80 --> 00:01:31,30
So does a console.log off a delay of two seconds?

37
00:01:31,30 --> 00:01:33,70
So I'll break this into multiple lines

38
00:01:33,70 --> 00:01:35,20
and you can see it a little easier.

39
00:01:35,20 --> 00:01:37,70
So delay takes a lambda.

40
00:01:37,70 --> 00:01:39,60
And, inside that lambda, we do some stuff.

41
00:01:39,60 --> 00:01:41,40
And this lambda will get called after two seconds.

42
00:01:41,40 --> 00:01:44,70
So let's run this quickly.

43
00:01:44,70 --> 00:01:49,50
So, doing stuff, do more stuff, do stuff after a delay,

44
00:01:49,50 --> 00:01:52,10
which happens all the way down here.

45
00:01:52,10 --> 00:01:53,10
So what if,

46
00:01:53,10 --> 00:01:54,90
why would you want to do something after a delay?

47
00:01:54,90 --> 00:01:56,80
So, probably the most common example,

48
00:01:56,80 --> 00:01:58,40
is that you go and get data from, say,

49
00:01:58,40 --> 00:02:01,70
a HTTP API or some sort of data source.

50
00:02:01,70 --> 00:02:03,70
And most of those, you know, over the network,

51
00:02:03,70 --> 00:02:06,30
and most of the time they will reply asynchronously.

52
00:02:06,30 --> 00:02:08,50
So asynchronous just means kind of,

53
00:02:08,50 --> 00:02:10,40
rather than normally synchronous,

54
00:02:10,40 --> 00:02:12,60
like we do this line, then we do this line,

55
00:02:12,60 --> 00:02:14,80
asynchronous is like, this line happens

56
00:02:14,80 --> 00:02:17,40
at some point in the future.

57
00:02:17,40 --> 00:02:19,20
Okay, in this case, we know it's two seconds.

58
00:02:19,20 --> 00:02:21,10
But you might not always know when.

59
00:02:21,10 --> 00:02:25,80
So I could put like a Math.rand in here or whatever,

60
00:02:25,80 --> 00:02:29,10
and then we wouldn't know how long it will be.

61
00:02:29,10 --> 00:02:31,60
And this is really common, because HTTP API's,

62
00:02:31,60 --> 00:02:34,60
and dispatching Redux stuff, that's really useful.

63
00:02:34,60 --> 00:02:38,40
So, in this project, as you saw in the setup video,

64
00:02:38,40 --> 00:02:40,70
we've got Thunk installed.

65
00:02:40,70 --> 00:02:42,30
It's a middleware for Redux.

66
00:02:42,30 --> 00:02:44,60
A middleware's like something which is kind of like,

67
00:02:44,60 --> 00:02:47,40
gets built into Redux, it's running all the time with it.

68
00:02:47,40 --> 00:02:49,60
So like the logger, for example.

69
00:02:49,60 --> 00:02:52,40
So how does Thunk work and how do you use it?

70
00:02:52,40 --> 00:02:54,10
It's not particularly intuitive.

71
00:02:54,10 --> 00:02:55,70
And the other thing I would say is that Thunk

72
00:02:55,70 --> 00:02:58,70
is only one library that solves these problems.

73
00:02:58,70 --> 00:03:00,10
There are a couple of other libraries

74
00:03:00,10 --> 00:03:01,30
that solve these basic problems.

75
00:03:01,30 --> 00:03:05,70
So, like, how do I do dispatch in action

76
00:03:05,70 --> 00:03:07,90
and then also carry on doing stuff?

77
00:03:07,90 --> 00:03:10,70
Or how do I dispatch an action asynchronously,

78
00:03:10,70 --> 00:03:13,10
like when an HTTP call comes back?

79
00:03:13,10 --> 00:03:14,90
There are a bunch of libraries,

80
00:03:14,90 --> 00:03:19,20
Thunk is one of the sort of lower level, most basic ones.

81
00:03:19,20 --> 00:03:21,00
I quite like it for that.

82
00:03:21,00 --> 00:03:24,90
Usually I'm a high level guy but, in this case,

83
00:03:24,90 --> 00:03:26,00
I find some of the other ones,

84
00:03:26,00 --> 00:03:28,00
they're only good for like one specific use case.

85
00:03:28,00 --> 00:03:29,40
So there are a lot of libraries that I like

86
00:03:29,40 --> 00:03:32,10
that only work for HTTP requests.

87
00:03:32,10 --> 00:03:33,70
And I kind of was just like, well,

88
00:03:33,70 --> 00:03:37,60
I'd rather use my own HTTP code and use Thunk with it.

89
00:03:37,60 --> 00:03:39,40
Because then I can kind of know what's going on.

90
00:03:39,40 --> 00:03:40,90
But maybe not everyone feels that way.

91
00:03:40,90 --> 00:03:44,50
And, also, the biggest competitor is probably Redux-saga,

92
00:03:44,50 --> 00:03:45,50
that you might wanna check out,

93
00:03:45,50 --> 00:03:48,20
but that's significantly more complex, in my opinion.

94
00:03:48,20 --> 00:03:51,70
So that's like a topic for another day and another video.

95
00:03:51,70 --> 00:03:53,80
But I like Thunk, it's pretty simple,

96
00:03:53,80 --> 00:03:55,20
once you get your head around it.

97
00:03:55,20 --> 00:03:58,00
So the way Thunks work.

98
00:03:58,00 --> 00:04:01,70
So, to think about Thunks, we need to think about types.

99
00:04:01,70 --> 00:04:04,20
And, obviously, JavaScript's not a strongly type language.

100
00:04:04,20 --> 00:04:06,40
But let's just think about the types of stuff.

101
00:04:06,40 --> 00:04:08,90
So we have got newPost, which is a const.

102
00:04:08,90 --> 00:04:12,10
And it's a function, or a, yeah, a lambda, or whatever.

103
00:04:12,10 --> 00:04:14,60
Because it's got like this bit and an arrow.

104
00:04:14,60 --> 00:04:18,40
And this function, at some point, returns an action.

105
00:04:18,40 --> 00:04:19,50
So that's kind of the signature.

106
00:04:19,50 --> 00:04:23,00
So I would say newPost, I'll just write in english,

107
00:04:23,00 --> 00:04:29,50
is a function which returns an action.

108
00:04:29,50 --> 00:04:33,80
To use Thunk, we need to write,

109
00:04:33,80 --> 00:04:36,10
I'm gonna show you first and I'll write it down later.

110
00:04:36,10 --> 00:04:40,90
So you write a function which returns a function.

111
00:04:40,90 --> 00:04:43,50
Which is a little bit complicated, I'm not gonna lie.

112
00:04:43,50 --> 00:04:46,00
And then, after that, you don't need to return stuff.

113
00:04:46,00 --> 00:04:47,70
So let me show you dispatch.

114
00:04:47,70 --> 00:04:50,20
So that's how you'd convert that existing example.

115
00:04:50,20 --> 00:04:51,80
So let's break this down, let's break this down,

116
00:04:51,80 --> 00:04:53,20
what's going on, I'm gonna make this a little smaller,

117
00:04:53,20 --> 00:04:54,30
so hopefully you could see.

118
00:04:54,30 --> 00:04:57,80
So it's a function which returns a function.

119
00:04:57,80 --> 00:05:00,80
I'm gonna break this out into more curly brackets,

120
00:05:00,80 --> 00:05:03,20
so that you can see exactly what that means.

121
00:05:03,20 --> 00:05:05,00
So this is equivalent to this.

122
00:05:05,00 --> 00:05:08,20
So it's a function, arrow, open a new curly bracket.

123
00:05:08,20 --> 00:05:11,00
Then we immediately return another function.

124
00:05:11,00 --> 00:05:12,60
So this is another function here.

125
00:05:12,60 --> 00:05:14,50
And that function has two parameters.

126
00:05:14,50 --> 00:05:17,70
A dispatch, which when you call it, dispatches actions.

127
00:05:17,70 --> 00:05:20,80
And getState, which is a function as well.

128
00:05:20,80 --> 00:05:24,70
I'll call that up here, console.log, getState.

129
00:05:24,70 --> 00:05:27,10
And you call that function and it gets you the entire state.

130
00:05:27,10 --> 00:05:28,60
Which is also pretty useful.

131
00:05:28,60 --> 00:05:30,50
It's kind of Thunk could exist with just this,

132
00:05:30,50 --> 00:05:31,60
but this is actually really useful.

133
00:05:31,60 --> 00:05:32,50
Because then you are like, oh,

134
00:05:32,50 --> 00:05:34,10
I can just get the entire state.

135
00:05:34,10 --> 00:05:36,30
So if you want something from state, in this function,

136
00:05:36,30 --> 00:05:38,50
you can go grab it, which is pretty handy.

137
00:05:38,50 --> 00:05:40,60
You can also remove this and just do this.

138
00:05:40,60 --> 00:05:42,80
But, depending on whether you need the state.

139
00:05:42,80 --> 00:05:44,50
So let's check that that runs, first of all,

140
00:05:44,50 --> 00:05:46,40
just to prove that I'm not conning you.

141
00:05:46,40 --> 00:05:48,10
Say, it doesn't work there,

142
00:05:48,10 --> 00:05:50,00
no, it's because I've left this thing in.

143
00:05:50,00 --> 00:05:51,40
Okay, here we go.

144
00:05:51,40 --> 00:05:52,60
Cool, so is that working?

145
00:05:52,60 --> 00:05:54,30
So, New Post, and you can see

146
00:05:54,30 --> 00:05:56,50
it's still getting dispatched here, which is cool.

147
00:05:56,50 --> 00:05:58,80
And you can see it doing more stuff, doing more stuff.

148
00:05:58,80 --> 00:05:59,80
Do stuff after a delay.

149
00:05:59,80 --> 00:06:02,90
So let's see if we could dispatch this action after a delay.

150
00:06:02,90 --> 00:06:04,50
So, to do that, all we need to do,

151
00:06:04,50 --> 00:06:06,00
is cause now, rather than returning,

152
00:06:06,00 --> 00:06:07,30
you're just calling this function.

153
00:06:07,30 --> 00:06:09,00
So you can do it in here.

154
00:06:09,00 --> 00:06:11,00
So let's try that, this will be interesting, actually,

155
00:06:11,00 --> 00:06:13,00
cause you'll see, you refresh the page.

156
00:06:13,00 --> 00:06:16,00
Just says post1, then after a two second delay,

157
00:06:16,00 --> 00:06:17,80
the action fires, and this is the first time

158
00:06:17,80 --> 00:06:20,00
we've really seen something change on the screen.

159
00:06:20,00 --> 00:06:22,10
Which is really cool, so that you fire the action.

160
00:06:22,10 --> 00:06:23,50
The store gets updated,

161
00:06:23,50 --> 00:06:25,00
and then the react components re-render,

162
00:06:25,00 --> 00:06:26,70
because they're like, oh, the store's changed

163
00:06:26,70 --> 00:06:27,70
and I'm connected to that store.

164
00:06:27,70 --> 00:06:30,20
So the props change and everything re-renders.

165
00:06:30,20 --> 00:06:31,90
And, so you can see, we're dispatching that action.

166
00:06:31,90 --> 00:06:34,10
Something else you can do that I forgot to mention,

167
00:06:34,10 --> 00:06:36,10
is you can dispatch things twice.

168
00:06:36,10 --> 00:06:37,80
Which you can't do if you're just returning.

169
00:06:37,80 --> 00:06:39,20
Because once you return, you're done.

170
00:06:39,20 --> 00:06:41,60
So, here, we're dispatching like this and that

171
00:06:41,60 --> 00:06:43,70
and what have you, so let's have a look.

172
00:06:43,70 --> 00:06:46,10
So you start off with one, after the two second delay,

173
00:06:46,10 --> 00:06:49,70
post2, then post3, and you can see, there he goes,

174
00:06:49,70 --> 00:06:53,20
so here's post2 getting fired over here,

175
00:06:53,20 --> 00:06:54,80
and then post3 over here.

176
00:06:54,80 --> 00:06:55,70
That's really cool.

177
00:06:55,70 --> 00:06:58,60
So that's, oh, we also logged getState,

178
00:06:58,60 --> 00:07:00,80
which I forgot to check, let's have a look.

179
00:07:00,80 --> 00:07:02,00
Where is getState?

180
00:07:02,00 --> 00:07:03,80
I think it's maybe here.

181
00:07:03,80 --> 00:07:07,60
Actions post fired, yeah, so it just logs.

182
00:07:07,60 --> 00:07:08,50
You can take my word for it,

183
00:07:08,50 --> 00:07:10,60
it just prints out the whole store.

184
00:07:10,60 --> 00:07:11,70
What else is there to know?

185
00:07:11,70 --> 00:07:13,80
I like to write them like this, by the way.

186
00:07:13,80 --> 00:07:16,60
What I think is, just copy one that works right

187
00:07:16,60 --> 00:07:19,60
and then copy from someone else, like me,

188
00:07:19,60 --> 00:07:21,50
from maybe this source from this course.

189
00:07:21,50 --> 00:07:24,60
And then copy that one, going forward.

190
00:07:24,60 --> 00:07:27,20
That's the way, I think, especially when you're starting out

191
00:07:27,20 --> 00:07:28,70
until you really start to understand.

192
00:07:28,70 --> 00:07:30,80
I, also, was going to write down this type thing.

193
00:07:30,80 --> 00:07:36,20
So newPost is a function which returns a function

194
00:07:36,20 --> 00:07:40,90
which has dispatched and getState parameters

195
00:07:40,90 --> 00:07:44,00
and, that's it, it doesn't return anything.

196
00:07:44,00 --> 00:07:45,00
So that's kind of it.

197
00:07:45,00 --> 00:07:47,60
So it's a function which returns a function

198
00:07:47,60 --> 00:07:50,50
which has dispatched and getState parameters.

199
00:07:50,50 --> 00:07:53,00
So dispatch is a function and getState's a function.

200
00:07:53,00 --> 00:07:54,70
You can call getState to get the state,

201
00:07:54,70 --> 00:07:57,30
and you can call dispatch and pass in an action.

202
00:07:57,30 --> 00:07:58,60
That's kind of it, really.

203
00:07:58,60 --> 00:07:59,70
It is quite complicated.

204
00:07:59,70 --> 00:08:01,20
I would say it's an advanced feature.

205
00:08:01,20 --> 00:08:03,50
But it's a feature that you're almost certainly

206
00:08:03,50 --> 00:08:07,20
going to need to use in most applications.

207
00:08:07,20 --> 00:08:11,10
So it's important just to learn Thunk.

208
00:08:11,10 --> 00:08:13,10
Or maybe look at, you know, Redux-saga

209
00:08:13,10 --> 00:08:15,60
has gotten a lot more popular.

210
00:08:15,60 --> 00:08:17,70
So it might be worth looking at that, maybe.

211
00:08:17,70 --> 00:08:20,30
Although, I think that's quite confusing as well.

212
00:08:20,30 --> 00:08:23,50
This is confusing, but I've kind of got to grips with it.

213
00:08:23,50 --> 00:08:25,80
And it's stopped causing me problems.

214
00:08:25,80 --> 00:08:28,10
Thunk is maybe a good one to start with.

215
00:08:28,10 --> 00:08:29,70
You can go back to the previous video

216
00:08:29,70 --> 00:08:32,60
where I showed you how to add it into your Redux project.

217
00:08:32,60 --> 00:08:35,10
In this video, we just saw how you can use Thunk

218
00:08:35,10 --> 00:08:37,80
to dispatch more than one action at a time.

219
00:08:37,80 --> 00:08:40,80
And to dispatch an action asynchronously.

220
00:08:40,80 --> 00:08:43,60
So after some amount of time has passed.

221
00:08:43,60 --> 00:08:45,00
In the next video, we're gonna make sure

222
00:08:45,00 --> 00:08:47,00
that you know your selectors.

