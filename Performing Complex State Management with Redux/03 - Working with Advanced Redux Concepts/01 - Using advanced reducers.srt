1
00:00:01,10 --> 00:00:03,90
- [Instructor] Working with advanced Redux concepts.

2
00:00:03,90 --> 00:00:05,40
In this section, we are going to look

3
00:00:05,40 --> 00:00:08,00
at using advanced reducers,

4
00:00:08,00 --> 00:00:11,00
configuring Redux within your application,

5
00:00:11,00 --> 00:00:12,90
the rules of Redux,

6
00:00:12,90 --> 00:00:17,00
using Thunk to dispatch asynchronous actions,

7
00:00:17,00 --> 00:00:18,90
and knowing your selectors.

8
00:00:18,90 --> 00:00:20,90
Using advanced reducers.

9
00:00:20,90 --> 00:00:22,00
In this video we're going to look

10
00:00:22,00 --> 00:00:24,20
at some of the more advanced reduce concepts.

11
00:00:24,20 --> 00:00:27,00
Okay, so we looked at a really basic reducer

12
00:00:27,00 --> 00:00:28,50
in the last video,

13
00:00:28,50 --> 00:00:29,80
and what I'm going to show you now

14
00:00:29,80 --> 00:00:34,00
is the sorts of patterns that you might use in reducers.

15
00:00:34,00 --> 00:00:36,60
There are a bunch and a lot of them are quite common,

16
00:00:36,60 --> 00:00:38,90
and you'll find they come up quite regularly.

17
00:00:38,90 --> 00:00:42,40
I am going to do this in pseudo code.

18
00:00:42,40 --> 00:00:44,50
Not pseudo code but in real code.

19
00:00:44,50 --> 00:00:47,60
I'm just going to show you some of the patterns.

20
00:00:47,60 --> 00:00:49,50
I'm not going to run all of the code,

21
00:00:49,50 --> 00:00:50,50
because in this project it parts

22
00:00:50,50 --> 00:00:52,20
doesn't make sense to use them all.

23
00:00:52,20 --> 00:00:53,30
I'm just going to run you through

24
00:00:53,30 --> 00:00:54,90
some of the different things that might happen.

25
00:00:54,90 --> 00:00:56,90
So, probably the first one I can think of

26
00:00:56,90 --> 00:00:59,10
that's pretty common is something like,

27
00:00:59,10 --> 00:01:02,00
say we want to delete a post.

28
00:01:02,00 --> 00:01:04,60
So, if you wanted to delete a post.

29
00:01:04,60 --> 00:01:08,50
Let's say that you've got some sort of post ID has come in.

30
00:01:08,50 --> 00:01:10,10
So you've got action dot payload,

31
00:01:10,10 --> 00:01:12,30
and you know the one you want to get rid of, right.

32
00:01:12,30 --> 00:01:13,60
So you've got the ID.

33
00:01:13,60 --> 00:01:14,60
So you obviously want to,

34
00:01:14,60 --> 00:01:15,70
you always want to keep your state,

35
00:01:15,70 --> 00:01:17,10
there's like a trend here.

36
00:01:17,10 --> 00:01:19,00
Always want to spread your state back in,

37
00:01:19,00 --> 00:01:21,30
pretty much, in almost every single case.

38
00:01:21,30 --> 00:01:22,60
And then it's a question of well,

39
00:01:22,60 --> 00:01:25,40
we want to delete a post and we know the post ID.

40
00:01:25,40 --> 00:01:26,70
What we sort of want to do is

41
00:01:26,70 --> 00:01:29,50
find the post that we want to get rid of,

42
00:01:29,50 --> 00:01:32,20
and return some post but without that one.

43
00:01:32,20 --> 00:01:33,60
Now, one thing that's really cool is,

44
00:01:33,60 --> 00:01:37,70
if you are using the underscore library, or lodash rather,

45
00:01:37,70 --> 00:01:39,90
is what I normally use in most of my projects.

46
00:01:39,90 --> 00:01:42,90
Most lodash functions, unless they say otherwise,

47
00:01:42,90 --> 00:01:44,60
and some of them do say otherwise,

48
00:01:44,60 --> 00:01:47,10
they return a new collection.

49
00:01:47,10 --> 00:01:48,60
So they don't modify data.

50
00:01:48,60 --> 00:01:51,20
So they don't break the rules that we talked about before.

51
00:01:51,20 --> 00:01:52,00
So that's really cool.

52
00:01:52,00 --> 00:01:54,00
So you can probably here say something like

53
00:01:54,00 --> 00:01:56,80
reject state dot post,

54
00:01:56,80 --> 00:01:58,30
and then you say the one you want to reject.

55
00:01:58,30 --> 00:02:01,40
So you might say the post where, I don't know.

56
00:02:01,40 --> 00:02:04,10
Post dot ID is equal to post dot ID,

57
00:02:04,10 --> 00:02:05,20
or something like that.

58
00:02:05,20 --> 00:02:08,40
And then this will clone, so reject internally,

59
00:02:08,40 --> 00:02:11,30
clones posts, it doesn't actually modify posts.

60
00:02:11,30 --> 00:02:12,20
There'll be another function

61
00:02:12,20 --> 00:02:13,90
which does actually modify posts,

62
00:02:13,90 --> 00:02:15,80
but we obviously don't want to use that one.

63
00:02:15,80 --> 00:02:18,70
And then this will return all of these but minus this one.

64
00:02:18,70 --> 00:02:20,70
That's one kind of pattern.

65
00:02:20,70 --> 00:02:25,20
Probably the most interesting pattern is to delete,

66
00:02:25,20 --> 00:02:28,60
well no, probably to update something in position,

67
00:02:28,60 --> 00:02:30,00
that's quite hard.

68
00:02:30,00 --> 00:02:32,40
So maybe you say update post,

69
00:02:32,40 --> 00:02:34,60
and as part of that you would get the,

70
00:02:34,60 --> 00:02:36,30
I don't know, the post ID,

71
00:02:36,30 --> 00:02:40,50
the one you're updating and the new post,

72
00:02:40,50 --> 00:02:42,30
looking at what you're changing it to.

73
00:02:42,30 --> 00:02:44,50
So you've got both these things in the payload.

74
00:02:44,50 --> 00:02:46,40
So how do you do this one?

75
00:02:46,40 --> 00:02:48,90
So again, you always want to keep the existing state

76
00:02:48,90 --> 00:02:50,00
and then probably modify it.

77
00:02:50,00 --> 00:02:51,80
And now we'll say we want the post.

78
00:02:51,80 --> 00:02:52,90
So now what you want to do is,

79
00:02:52,90 --> 00:02:55,60
let's say you want to keep them the same order, as well.

80
00:02:55,60 --> 00:02:57,30
So this is actually pretty tricky.

81
00:02:57,30 --> 00:02:59,10
So what I'd normally say is

82
00:02:59,10 --> 00:03:01,50
use something like lodash dot,

83
00:03:01,50 --> 00:03:03,90
I think there's a function called find index,

84
00:03:03,90 --> 00:03:05,90
state dot post, something like this.

85
00:03:05,90 --> 00:03:08,00
And then you would say post,

86
00:03:08,00 --> 00:03:13,80
post dot ID is equal to post ID.

87
00:03:13,80 --> 00:03:16,20
So this will find you the index of that post.

88
00:03:16,20 --> 00:03:17,80
Then what you can say is

89
00:03:17,80 --> 00:03:26,00
state dot post dot slice zero to post index,

90
00:03:26,00 --> 00:03:29,00
and then here we're going to update.

91
00:03:29,00 --> 00:03:32,10
So we're going to put in the new post.

92
00:03:32,10 --> 00:03:37,10
And then here we're going to say state dot post dot slice,

93
00:03:37,10 --> 00:03:40,00
post index plus one.

94
00:03:40,00 --> 00:03:42,40
And that is too big to go on the screen so let me just.

95
00:03:42,40 --> 00:03:45,40
Cool, so let's go back one.

96
00:03:45,40 --> 00:03:47,40
So we're saying post is going to be

97
00:03:47,40 --> 00:03:49,60
the post up until the index,

98
00:03:49,60 --> 00:03:51,40
then we're going to put in the new post,

99
00:03:51,40 --> 00:03:53,10
then we're going to keep the rest.

100
00:03:53,10 --> 00:03:55,60
Something like this, roughly.

101
00:03:55,60 --> 00:03:57,50
Yeah, I'm just checking that, yeah that's correct.

102
00:03:57,50 --> 00:04:00,50
So this is everything up until but not including the one

103
00:04:00,50 --> 00:04:01,60
that we're replacing.

104
00:04:01,60 --> 00:04:03,40
This is everything afterwards.

105
00:04:03,40 --> 00:04:04,70
Oh, I've messed it up, look here we go.

106
00:04:04,70 --> 00:04:06,20
You need to spread these in, obviously.

107
00:04:06,20 --> 00:04:08,60
So this is saying, okay copy in everything up unto

108
00:04:08,60 --> 00:04:11,90
but not including the one that we cared about.

109
00:04:11,90 --> 00:04:14,30
This is saying copy in everything

110
00:04:14,30 --> 00:04:15,90
after the one we cared about

111
00:04:15,90 --> 00:04:17,20
and so that leaves a gap in the middle,

112
00:04:17,20 --> 00:04:19,00
where the one that we cared about was.

113
00:04:19,00 --> 00:04:19,90
And then we'll say, okay

114
00:04:19,90 --> 00:04:21,70
we'll just replace that with a new dash end.

115
00:04:21,70 --> 00:04:24,40
So this replaces, you know if you've got a list of

116
00:04:24,40 --> 00:04:29,50
I don't know, post one, post two, post three

117
00:04:29,50 --> 00:04:31,90
then this would maybe find the middle one,

118
00:04:31,90 --> 00:04:33,70
still put this in and still put that in.

119
00:04:33,70 --> 00:04:36,50
Okay to use slice because it doesn't retain anything.

120
00:04:36,50 --> 00:04:39,20
Those are probably two quite common patterns,

121
00:04:39,20 --> 00:04:42,10
other than that it's pretty straight forward.

122
00:04:42,10 --> 00:04:43,10
I mean, here we've got an array,

123
00:04:43,10 --> 00:04:45,70
which is making things a little bit more tricky.

124
00:04:45,70 --> 00:04:47,80
Often, you'll just be taking the data

125
00:04:47,80 --> 00:04:50,50
and just directly replacing it.

126
00:04:50,50 --> 00:04:52,40
Whilst we're here, let's talk about another thing.

127
00:04:52,40 --> 00:04:54,10
So we've got this initial state at the top,

128
00:04:54,10 --> 00:04:55,60
which I didn't really cover in the last video.

129
00:04:55,60 --> 00:04:56,60
So what's that?

130
00:04:56,60 --> 00:04:59,00
So it's kind of a convention, like you don't have to do it.

131
00:04:59,00 --> 00:05:00,20
But what a lot of people do is,

132
00:05:00,20 --> 00:05:03,20
they say okay, what's the state that's in this reducer

133
00:05:03,20 --> 00:05:05,10
when I just load up the app for the first time?

134
00:05:05,10 --> 00:05:07,20
So someone hits refresh on your webpage,

135
00:05:07,20 --> 00:05:08,90
what should be there?

136
00:05:08,90 --> 00:05:10,00
And that's the initial state,

137
00:05:10,00 --> 00:05:14,00
and then you pass it in here as a default thing.

138
00:05:14,00 --> 00:05:16,40
And the first time, as a default parameter.

139
00:05:16,40 --> 00:05:18,80
So you're saying, okay so the state will be initial state,

140
00:05:18,80 --> 00:05:19,90
if it's not set,

141
00:05:19,90 --> 00:05:23,20
and then when Redux calls this function for the first time,

142
00:05:23,20 --> 00:05:25,20
it calls it with undefined,

143
00:05:25,20 --> 00:05:27,40
which means that this takes over.

144
00:05:27,40 --> 00:05:29,30
So basically, the first time it calls it,

145
00:05:29,30 --> 00:05:31,60
it will plop in your initial state for you.

146
00:05:31,60 --> 00:05:32,50
It's also pretty handy,

147
00:05:32,50 --> 00:05:33,80
because if you want to reset some things.

148
00:05:33,80 --> 00:05:36,90
Say you have some sort of reset case,

149
00:05:36,90 --> 00:05:40,60
I don't know, post resets all right back to the beginning.

150
00:05:40,60 --> 00:05:43,70
You could just say, return initial state.

151
00:05:43,70 --> 00:05:46,40
Or maybe, it's unlikely you'd want to do it like this,

152
00:05:46,40 --> 00:05:48,00
but you might more likely,

153
00:05:48,00 --> 00:05:49,00
you might say something like

154
00:05:49,00 --> 00:05:51,60
state, I'll keep the state,

155
00:05:51,60 --> 00:05:54,80
but actually my posts are going to be

156
00:05:54,80 --> 00:05:56,20
initial state dot post

157
00:05:56,20 --> 00:05:57,80
because I just want to reset that bit.

158
00:05:57,80 --> 00:05:58,70
Or something like that.

159
00:05:58,70 --> 00:05:59,80
That's initial state.

160
00:05:59,80 --> 00:06:01,90
And this pattern, the way that these lines look

161
00:06:01,90 --> 00:06:03,60
is pretty much the same everywhere.

162
00:06:03,60 --> 00:06:05,30
You're probably not going to have a lot of imports in here.

163
00:06:05,30 --> 00:06:07,90
You should probably try not to do too much logic in here.

164
00:06:07,90 --> 00:06:09,00
So something I've learned

165
00:06:09,00 --> 00:06:12,80
from doing Redux for a couple years now is

166
00:06:12,80 --> 00:06:15,10
the way I like to do it and this is not necessarily

167
00:06:15,10 --> 00:06:16,40
like there's a right or wrong way,

168
00:06:16,40 --> 00:06:18,00
but the way I like to structure my code

169
00:06:18,00 --> 00:06:22,70
is I will just put things in Redux, in my reducer.

170
00:06:22,70 --> 00:06:25,00
I don't try and do too much work here.

171
00:06:25,00 --> 00:06:28,70
I'll normally just say, here's the thing,

172
00:06:28,70 --> 00:06:31,40
actions get filed, I'll normally try and store

173
00:06:31,40 --> 00:06:34,00
the representation of state for the app

174
00:06:34,00 --> 00:06:36,40
in the smallest possible store,

175
00:06:36,40 --> 00:06:40,10
like the smallest amount of data I can in the store,

176
00:06:40,10 --> 00:06:42,50
and I try not to duplicate any of that data.

177
00:06:42,50 --> 00:06:44,30
So an example of that might be,

178
00:06:44,30 --> 00:06:48,00
you've got five posts and a currently selected post, right.

179
00:06:48,00 --> 00:06:50,30
So you could represent that in a couple of ways.

180
00:06:50,30 --> 00:06:52,60
So for example, if I just write some code down here.

181
00:06:52,60 --> 00:06:54,50
You have might have posts

182
00:06:54,50 --> 00:06:58,30
and you've got I don't know, post one, post two,

183
00:06:58,30 --> 00:06:59,70
these are probably going to be objects

184
00:06:59,70 --> 00:07:00,50
in real life, but whatever.

185
00:07:00,50 --> 00:07:01,60
You've got a few posts,

186
00:07:01,60 --> 00:07:05,10
and then you've got the currently selected post.

187
00:07:05,10 --> 00:07:07,40
Let's call it current post, it's shorter.

188
00:07:07,40 --> 00:07:09,30
You could do this, say like, oh okay,

189
00:07:09,30 --> 00:07:12,80
it's post one and put all of this data back in here.

190
00:07:12,80 --> 00:07:15,80
And then when, if this changes,

191
00:07:15,80 --> 00:07:17,70
you would then change it here.

192
00:07:17,70 --> 00:07:18,90
But what I think is best to do

193
00:07:18,90 --> 00:07:20,50
is not duplicate it in the data,

194
00:07:20,50 --> 00:07:23,20
so I would probably have current post index.

195
00:07:23,20 --> 00:07:26,30
And just say zero, which means that it's this one.

196
00:07:26,30 --> 00:07:27,40
So this is less convenient

197
00:07:27,40 --> 00:07:29,40
if you access your store directly.

198
00:07:29,40 --> 00:07:31,80
But basically, you're not repeating yourself.

199
00:07:31,80 --> 00:07:34,00
So there's no duplication of data,

200
00:07:34,00 --> 00:07:35,90
you've kept it kind of,

201
00:07:35,90 --> 00:07:37,80
I would think of it as almost like a normal form

202
00:07:37,80 --> 00:07:41,00
or in keeping it dry, like DRY, Don't Repeat Yourself,

203
00:07:41,00 --> 00:07:43,00
because you're not repeating any data.

204
00:07:43,00 --> 00:07:44,50
And then I use selectors

205
00:07:44,50 --> 00:07:46,50
to often calculate some of these things.

206
00:07:46,50 --> 00:07:48,90
So I would use a selector to calculate the current post,

207
00:07:48,90 --> 00:07:50,80
which would actually provide post three.

208
00:07:50,80 --> 00:07:53,40
So I like to keep the reducer state minimal,

209
00:07:53,40 --> 00:07:55,60
but maybe not everyone's doing it that way.

210
00:07:55,60 --> 00:07:56,50
And it kind of depends.

211
00:07:56,50 --> 00:07:58,10
There might be some performance considerations

212
00:07:58,10 --> 00:07:59,80
that mean that you don't want to do it that way.

213
00:07:59,80 --> 00:08:01,20
But I think in general,

214
00:08:01,20 --> 00:08:03,80
try and keep your Redux state as small as possible,

215
00:08:03,80 --> 00:08:06,60
and try not to do too much crazy stuff.

216
00:08:06,60 --> 00:08:08,80
I often push a lot of my logic to my selectors,

217
00:08:08,80 --> 00:08:11,40
but we're going to talk about that in the video in a minute.

218
00:08:11,40 --> 00:08:13,30
So that was a quick tour of using advanced reducers.

219
00:08:13,30 --> 00:08:15,90
It's one of the more tricky things that might come up

220
00:08:15,90 --> 00:08:18,10
and some of the best practices for where to put code

221
00:08:18,10 --> 00:08:19,70
and how to write them.

222
00:08:19,70 --> 00:08:21,10
In the next video, we're going to look

223
00:08:21,10 --> 00:08:24,00
at configuring Redux within your application.

