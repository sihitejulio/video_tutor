1
00:00:00,50 --> 00:00:02,30
- [Narrator] Webpack is a build tool that allows us

2
00:00:02,30 --> 00:00:04,20
to take all of our assets and turn them

3
00:00:04,20 --> 00:00:06,50
into a production ready bundle.

4
00:00:06,50 --> 00:00:08,90
All of our files are considered modules and we tell

5
00:00:08,90 --> 00:00:13,10
Webpack to load those modules when we configure our project.

6
00:00:13,10 --> 00:00:15,90
So why would we use something like Webpack?

7
00:00:15,90 --> 00:00:18,20
Well the answer to that starts by looking back

8
00:00:18,20 --> 00:00:21,90
at how loading assets used to work in web projects.

9
00:00:21,90 --> 00:00:24,30
Consider this HTML file.

10
00:00:24,30 --> 00:00:27,60
We're adding tons of script tags here in the head,

11
00:00:27,60 --> 00:00:31,20
three of them, and when we used to add script tags

12
00:00:31,20 --> 00:00:33,70
like this, it could get pretty messy.

13
00:00:33,70 --> 00:00:36,80
Errors could occur from the hierarchy of these scripts.

14
00:00:36,80 --> 00:00:38,80
Global variables might be over written,

15
00:00:38,80 --> 00:00:40,80
and there might be unintended consequences

16
00:00:40,80 --> 00:00:43,30
of functions being called before others.

17
00:00:43,30 --> 00:00:45,30
Order was extremely important

18
00:00:45,30 --> 00:00:47,40
and really difficult to manage.

19
00:00:47,40 --> 00:00:49,60
And all of these HTTP requests

20
00:00:49,60 --> 00:00:51,70
are really taxing on browsers.

21
00:00:51,70 --> 00:00:54,30
So some of the benefits of working with something

22
00:00:54,30 --> 00:00:56,80
like Webpack, is that it provides an alternative

23
00:00:56,80 --> 00:00:59,50
to some of these complications, with a concept

24
00:00:59,50 --> 00:01:01,40
of a dependency graph.

25
00:01:01,40 --> 00:01:05,10
We're going to require assets like images, CSS files,

26
00:01:05,10 --> 00:01:08,20
and Java script files and their loaded when they're needed

27
00:01:08,20 --> 00:01:09,60
for the page.

28
00:01:09,60 --> 00:01:13,00
We can also split our app into different files

29
00:01:13,00 --> 00:01:16,20
and just load the code that the page requires.

30
00:01:16,20 --> 00:01:18,20
Then when the user goes to a new page,

31
00:01:18,20 --> 00:01:21,40
they don't download the already downloaded code again.

32
00:01:21,40 --> 00:01:25,10
It also minimizes the initial loading time for the app.

33
00:01:25,10 --> 00:01:27,00
This process is called code splitting.

34
00:01:27,00 --> 00:01:31,30
Webpack also helps us deal with transformations.

35
00:01:31,30 --> 00:01:34,70
If we're using Sass or Less we can build that code

36
00:01:34,70 --> 00:01:36,50
we can build that code to CSS prior to production.

37
00:01:36,50 --> 00:01:39,90
If we're using React of ES6 we can transform that

38
00:01:39,90 --> 00:01:42,10
into vanilla Java script.

39
00:01:42,10 --> 00:01:44,60
All of this can be configured and then automated

40
00:01:44,60 --> 00:01:47,90
so that these transforms don't have to occur manually.

41
00:01:47,90 --> 00:01:51,20
So Webpack may seem a little bit complex at first glance,

42
00:01:51,20 --> 00:01:53,30
but throughout these videos, we'll cover some of the most

43
00:01:53,30 --> 00:01:56,40
useful features that will optimize and simplify

44
00:01:56,40 --> 00:01:58,00
your web projects.

