1
00:00:00,40 --> 00:00:02,20
- [Instructor] Now that we know how to install webpack,

2
00:00:02,20 --> 00:00:04,20
let's go through the process of putting together

3
00:00:04,20 --> 00:00:06,00
our first webpack build.

4
00:00:06,00 --> 00:00:07,80
First of all, you'll notice in your start folder

5
00:00:07,80 --> 00:00:10,70
that we have a package JSON and a package-lock file,

6
00:00:10,70 --> 00:00:13,40
but we don't have node_modules folder.

7
00:00:13,40 --> 00:00:15,40
This is because having a node_modules folder

8
00:00:15,40 --> 00:00:17,20
in ever exercise files folder

9
00:00:17,20 --> 00:00:20,50
starts to make our Exercise Files folder pretty huge.

10
00:00:20,50 --> 00:00:23,00
So, we do have the package JSON, though,

11
00:00:23,00 --> 00:00:25,50
so that's going to act as an installer.

12
00:00:25,50 --> 00:00:26,70
So, over in my terminal,

13
00:00:26,70 --> 00:00:32,80
I'm in 0104 start and I can run npm i or npm install,

14
00:00:32,80 --> 00:00:34,30
and this is going to install

15
00:00:34,30 --> 00:00:38,60
all of the current dependencies, webpack and webpack CLI.

16
00:00:38,60 --> 00:00:42,10
And you can always do this in any project folder.

17
00:00:42,10 --> 00:00:45,10
Alternatively, you can drag the node_modules folder

18
00:00:45,10 --> 00:00:48,80
into the proper Exercise Files folder.

19
00:00:48,80 --> 00:00:52,10
Let's also install jQuery

20
00:00:52,10 --> 00:00:54,80
and we'll send it the save flag

21
00:00:54,80 --> 00:00:56,20
and now that that's included,

22
00:00:56,20 --> 00:00:58,90
I'm going to create a couple of folders

23
00:00:58,90 --> 00:01:00,10
inside of the start folder.

24
00:01:00,10 --> 00:01:02,30
The first is going to be called source,

25
00:01:02,30 --> 00:01:04,30
and the second is going to be called dist.

26
00:01:04,30 --> 00:01:06,80
So, source is all of our source files,

27
00:01:06,80 --> 00:01:08,40
dist is everything that's intended

28
00:01:08,40 --> 00:01:10,70
for distribution, for production.

29
00:01:10,70 --> 00:01:13,30
So, let's go to our source folder

30
00:01:13,30 --> 00:01:15,10
and we're going to create a new file.

31
00:01:15,10 --> 00:01:17,80
This is going to be called index.js,

32
00:01:17,80 --> 00:01:20,20
and just for the sake of brevity,

33
00:01:20,20 --> 00:01:23,60
we're using jQuery and we're gong to create

34
00:01:23,60 --> 00:01:28,30
a variable to import or require jQuery

35
00:01:28,30 --> 00:01:32,90
and then we will do a simple selector here.

36
00:01:32,90 --> 00:01:36,50
We're going to select the target ID

37
00:01:36,50 --> 00:01:42,10
and we're going to set the HTML to hello world!

38
00:01:42,10 --> 00:01:44,00
Alright, so super simple.

39
00:01:44,00 --> 00:01:46,40
We're importing a library,

40
00:01:46,40 --> 00:01:50,00
we are changing some dom elements,

41
00:01:50,00 --> 00:01:52,80
we're injecting a string into some HTML there.

42
00:01:52,80 --> 00:01:54,80
The reason for that, is we're going to go

43
00:01:54,80 --> 00:01:56,30
over here to our dist folder,

44
00:01:56,30 --> 00:02:00,00
and we're going to create a new file called index.html.

45
00:02:00,00 --> 00:02:02,80
Alright, so let's just create a simple HTML file.

46
00:02:02,80 --> 00:02:05,70
So, pivot to DOCTYPE html.

47
00:02:05,70 --> 00:02:07,50
We'll use the HTML tag.

48
00:02:07,50 --> 00:02:09,60
We'll give it a head element.

49
00:02:09,60 --> 00:02:14,50
We'll give it a title, getting started with webpack,

50
00:02:14,50 --> 00:02:17,00
lowercase w for good measure.

51
00:02:17,00 --> 00:02:23,40
Let's go ahead and add a body tag

52
00:02:23,40 --> 00:02:26,20
and we'll add that h1.

53
00:02:26,20 --> 00:02:29,60
So, we want to find the target element.

54
00:02:29,60 --> 00:02:31,20
So, we're going to go ahead

55
00:02:31,20 --> 00:02:34,70
and place this here.

56
00:02:34,70 --> 00:02:36,00
Now, it would be pretty simple

57
00:02:36,00 --> 00:02:40,40
to just simply link to the source index.js file

58
00:02:40,40 --> 00:02:41,70
if we wanted to,

59
00:02:41,70 --> 00:02:44,20
but we're trying to ready this for production

60
00:02:44,20 --> 00:02:46,80
and we're trying to use webpack as a bundler.

61
00:02:46,80 --> 00:02:47,70
So here's what I want to do.

62
00:02:47,70 --> 00:02:49,90
I'm going to add a script tag here.

63
00:02:49,90 --> 00:02:52,60
So the script is going to use the source main.js.

64
00:02:52,60 --> 00:02:54,30
Now, main.js doesn't exist yet,

65
00:02:54,30 --> 00:02:57,10
but we're going to generate it using webpack.

66
00:02:57,10 --> 00:02:58,70
Alright, so, the next step we want

67
00:02:58,70 --> 00:03:00,70
to take is back to our terminal.

68
00:03:00,70 --> 00:03:03,60
We're going to run the webpack command.

69
00:03:03,60 --> 00:03:05,10
Now, this is a common thing

70
00:03:05,10 --> 00:03:07,30
that you see sometimes when you're working with webpack.

71
00:03:07,30 --> 00:03:09,20
You see "command not found,"

72
00:03:09,20 --> 00:03:10,90
and that's just because we may not

73
00:03:10,90 --> 00:03:12,90
have installed webpack globally.

74
00:03:12,90 --> 00:03:15,20
And sometimes installing things globally

75
00:03:15,20 --> 00:03:17,90
upsets people or [chuckles] people don't like

76
00:03:17,90 --> 00:03:20,30
working with global packages,

77
00:03:20,30 --> 00:03:23,70
so let me show you how you run this from your folder.

78
00:03:23,70 --> 00:03:32,20
I'm going to say ./node_modules/.bin/webpack.

79
00:03:32,20 --> 00:03:35,50
Now, if I run this, it's going to run the webpack command,

80
00:03:35,50 --> 00:03:38,50
and it's going to look in the source folder

81
00:03:38,50 --> 00:03:39,80
for this index file

82
00:03:39,80 --> 00:03:42,60
and it's going to generate this main.js file.

83
00:03:42,60 --> 00:03:44,50
So, not only does this have

84
00:03:44,50 --> 00:03:46,20
all of our code that we wrote,

85
00:03:46,20 --> 00:03:48,80
our simple selector here,

86
00:03:48,80 --> 00:03:51,40
but it's also including the jQuery library

87
00:03:51,40 --> 00:03:54,00
and anything that we need from it.

88
00:03:54,00 --> 00:03:55,90
Now, one other way that we can do this,

89
00:03:55,90 --> 00:03:57,80
and I'm going to delete the main.js

90
00:03:57,80 --> 00:04:00,70
just to prove that it's working,

91
00:04:00,70 --> 00:04:03,20
is we can use a command called npx.

92
00:04:03,20 --> 00:04:06,20
Now, npx is a package runner

93
00:04:06,20 --> 00:04:10,00
that ships with npm version 5.2 and higher.

94
00:04:10,00 --> 00:04:15,10
So, I can check to make sure I'm using higher than 5.2.

95
00:04:15,10 --> 00:04:16,60
If you aren't, no worries.

96
00:04:16,60 --> 00:04:18,80
If you are, you can give it a shot.

97
00:04:18,80 --> 00:04:20,60
npx webpack

98
00:04:20,60 --> 00:04:22,90
and that's going to run webpack for you

99
00:04:22,90 --> 00:04:24,10
and the results are the same.

100
00:04:24,10 --> 00:04:25,90
We have our main.js file,

101
00:04:25,90 --> 00:04:29,10
which is the bundle of all of our jQuery

102
00:04:29,10 --> 00:04:32,10
and all of the code that we wrote.

103
00:04:32,10 --> 00:04:33,50
And if we pull this up in our browser,

104
00:04:33,50 --> 00:04:37,00
we should see hello world! being added to this h1.

