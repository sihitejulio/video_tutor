1
00:00:00,40 --> 00:00:01,90
- At the root of our project we want to

2
00:00:01,90 --> 00:00:05,30
create a file called webpack.config.js.

3
00:00:05,30 --> 00:00:09,20
So we're going to add this to our start file,

4
00:00:09,20 --> 00:00:11,80
webpack.config.js.

5
00:00:11,80 --> 00:00:13,20
Now this is going to set up all

6
00:00:13,20 --> 00:00:15,60
of our configuration options for webpack

7
00:00:15,60 --> 00:00:18,50
so that we don't have to manually type commands.

8
00:00:18,50 --> 00:00:19,80
The first thing that we're going to add

9
00:00:19,80 --> 00:00:22,30
is the path module from note.js,

10
00:00:22,30 --> 00:00:24,00
and this is going to help us route our

11
00:00:24,00 --> 00:00:27,60
created files into the proper directory.

12
00:00:27,60 --> 00:00:30,60
We then are going to use module.exports,

13
00:00:30,60 --> 00:00:32,10
we'll set up an entry point,

14
00:00:32,10 --> 00:00:34,70
the entry point is just our main JavaScript file

15
00:00:34,70 --> 00:00:38,80
so at this point is index.js inside of our source.

16
00:00:38,80 --> 00:00:42,30
So we'll say ./src/index.js,

17
00:00:42,30 --> 00:00:45,40
we also will specify an output

18
00:00:45,40 --> 00:00:48,80
and this is going to be the file name

19
00:00:48,80 --> 00:00:50,60
main.js,

20
00:00:50,60 --> 00:00:53,40
and the path,

21
00:00:53,40 --> 00:00:56,20
path.resolve,

22
00:00:56,20 --> 00:01:01,50
we want to look for the dirname dist for our output.

23
00:01:01,50 --> 00:01:03,20
Now if you still have your main .js file

24
00:01:03,20 --> 00:01:05,20
in your disk folder you want to delete it

25
00:01:05,20 --> 00:01:07,90
just to make sure this is working as expected.

26
00:01:07,90 --> 00:01:12,90
Alright, so to run webpack let's run npx webpack.

27
00:01:12,90 --> 00:01:15,50
This is going to generate that main .js file

28
00:01:15,50 --> 00:01:18,90
which we should see here in our dist folder.

29
00:01:18,90 --> 00:01:21,60
Cool, now by default webpack is going to

30
00:01:21,60 --> 00:01:24,70
look for this webpack.config.js file

31
00:01:24,70 --> 00:01:26,90
but if you want to call it something different

32
00:01:26,90 --> 00:01:29,10
you can always pass a config flag,

33
00:01:29,10 --> 00:01:34,50
so you could do something like npx webpack flag config,

34
00:01:34,50 --> 00:01:36,10
and then you could say

35
00:01:36,10 --> 00:01:42,10
my.custom.webpack.config.js

36
00:01:42,10 --> 00:01:45,60
and this will look for this file instead.

37
00:01:45,60 --> 00:01:48,40
This is the tip of the iceberg for config options

38
00:01:48,40 --> 00:01:51,00
but we're going to add on to this in the next videos.

