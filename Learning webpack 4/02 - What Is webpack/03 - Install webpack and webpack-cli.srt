1
00:00:00,50 --> 00:00:02,00
- [Instructor] To get started working with webpack,

2
00:00:02,00 --> 00:00:04,00
we'll set up our webpack project.

3
00:00:04,00 --> 00:00:07,50
We'll be using NPM for this, the Node Package Manager.

4
00:00:07,50 --> 00:00:09,50
NPM allows us to install packages

5
00:00:09,50 --> 00:00:11,20
that have been created by companies

6
00:00:11,20 --> 00:00:13,90
or individuals to use in our own projects.

7
00:00:13,90 --> 00:00:17,20
Webpack is an example of one of these packages.

8
00:00:17,20 --> 00:00:19,00
First, I'm going to make sure that I'm navigated

9
00:00:19,00 --> 00:00:23,30
to the 01_03 directory inside of my terminal,

10
00:00:23,30 --> 00:00:26,00
or if I'm on a PC, my command prompt.

11
00:00:26,00 --> 00:00:29,40
Then, I'm going to type node dash v.

12
00:00:29,40 --> 00:00:32,30
This will let me know what version of node I'm using.

13
00:00:32,30 --> 00:00:34,70
As long as you see version six or higher,

14
00:00:34,70 --> 00:00:37,70
you'll be able to use webpack four.

15
00:00:37,70 --> 00:00:39,70
I can also check the version of NPM

16
00:00:39,70 --> 00:00:41,70
that I'm using with npm dash v.

17
00:00:41,70 --> 00:00:43,60
And again, as long as you have

18
00:00:43,60 --> 00:00:47,70
something version four or higher here, you should be good.

19
00:00:47,70 --> 00:00:49,60
The next thing I'm going to do is I'm going to navigate

20
00:00:49,60 --> 00:00:54,60
to the start folder, I then am going to run NPM init

21
00:00:54,60 --> 00:00:57,60
and this utility is going to take me through the process

22
00:00:57,60 --> 00:01:01,10
of creating a package JSON file for my project.

23
00:01:01,10 --> 00:01:04,00
So I'm going to call this webpack-basic.

24
00:01:04,00 --> 00:01:06,90
I then am going to call this version one.

25
00:01:06,90 --> 00:01:12,00
I'm going to say learning all about webpack four.

26
00:01:12,00 --> 00:01:14,50
I'm going to hit enter here

27
00:01:14,50 --> 00:01:17,60
because index JS is a great starting point.

28
00:01:17,60 --> 00:01:19,50
I'll hit enter for test command.

29
00:01:19,50 --> 00:01:21,90
We won't set up a GitHub repo yet.

30
00:01:21,90 --> 00:01:26,50
You can add some keywords here if you'd like.

31
00:01:26,50 --> 00:01:29,00
You can give yourself credit as the author.

32
00:01:29,00 --> 00:01:31,40
Give it a license, I'll use MIT.

33
00:01:31,40 --> 00:01:33,60
And then I'll say yeah, this is okay.

34
00:01:33,60 --> 00:01:34,60
Perfect.

35
00:01:34,60 --> 00:01:37,70
Now the next thing I want to do is install webpack.

36
00:01:37,70 --> 00:01:40,30
So I can hit clear.

37
00:01:40,30 --> 00:01:43,80
Now I need to install webpack and webpack CLI.

38
00:01:43,80 --> 00:01:45,30
So I'm going to make sure to install these

39
00:01:45,30 --> 00:01:46,40
at a specific version.

40
00:01:46,40 --> 00:01:51,10
So first I'll say webpack@4.28.4,

41
00:01:51,10 --> 00:01:54,50
and I'll send it the save dev flag.

42
00:01:54,50 --> 00:01:58,20
This will save webpack to our dev dependencies.

43
00:01:58,20 --> 00:02:00,90
Next up, I'm going to install the webpack CLI.

44
00:02:00,90 --> 00:02:04,30
So I'm going to install webpack hyphen CLI.

45
00:02:04,30 --> 00:02:06,50
This will allow us to interact with webpack

46
00:02:06,50 --> 00:02:08,60
from the command line, and I'll make sure

47
00:02:08,60 --> 00:02:10,70
to install this at its most recent version,

48
00:02:10,70 --> 00:02:16,50
which is 3.2.1, and I'll also give it the save dev flag

49
00:02:16,50 --> 00:02:19,00
so that this is saved in our package JSON

50
00:02:19,00 --> 00:02:21,30
with our dev dependencies.

51
00:02:21,30 --> 00:02:23,20
Perfect, so now that we've done this,

52
00:02:23,20 --> 00:02:24,80
we can clear this out.

53
00:02:24,80 --> 00:02:28,80
And I could say vi package.json,

54
00:02:28,80 --> 00:02:31,00
and we should see that in vim,

55
00:02:31,00 --> 00:02:35,90
we have the webpack and webpack-cli

56
00:02:35,90 --> 00:02:38,00
saved with our package JSON.

57
00:02:38,00 --> 00:02:40,30
So I'm going to hit colon Q.

58
00:02:40,30 --> 00:02:41,80
So now that we've set up our project

59
00:02:41,80 --> 00:02:44,00
and have installed our dependencies,

60
00:02:44,00 --> 00:02:47,00
we're going to run our first webpack build.

