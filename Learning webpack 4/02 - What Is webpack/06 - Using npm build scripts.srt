1
00:00:00,40 --> 00:00:01,80
- [Instructor] Let's add an npm script

2
00:00:01,80 --> 00:00:03,50
to our package JSON file.

3
00:00:03,50 --> 00:00:06,30
Here in the package JSON, under chapter one,

4
00:00:06,30 --> 00:00:08,70
chapter one six, under start,

5
00:00:08,70 --> 00:00:11,50
I'm going to change the test command

6
00:00:11,50 --> 00:00:14,60
to a new one called build.

7
00:00:14,60 --> 00:00:17,90
Now build is going to be set to webpack

8
00:00:17,90 --> 00:00:20,50
so that I can run npm run build

9
00:00:20,50 --> 00:00:22,60
anywhere inside of this project folder

10
00:00:22,60 --> 00:00:24,50
and it will run webpack for me.

11
00:00:24,50 --> 00:00:26,60
So nice little shortcut there.

12
00:00:26,60 --> 00:00:28,00
Let's give this a shot.

13
00:00:28,00 --> 00:00:31,10
I'm going to say npm run build

14
00:00:31,10 --> 00:00:33,60
and this should execute webpack.

15
00:00:33,60 --> 00:00:34,50
Perfect.

16
00:00:34,50 --> 00:00:37,20
Now if you get an error here that says webpack not found

17
00:00:37,20 --> 00:00:38,80
it's a pretty common error.

18
00:00:38,80 --> 00:00:40,40
Something you can do to fix this

19
00:00:40,40 --> 00:00:45,00
is you can find it directly in the node modules binary.

20
00:00:45,00 --> 00:00:47,10
So, let me show you what I mean by this.

21
00:00:47,10 --> 00:00:49,40
If we look at node modules,

22
00:00:49,40 --> 00:00:52,00
and then I look at this dot bin folder,

23
00:00:52,00 --> 00:00:55,90
there are all the executables for webpack in this folder.

24
00:00:55,90 --> 00:01:00,70
So you could dig it out of there by saying dot slash node

25
00:01:00,70 --> 00:01:05,90
underscore modules slash dot bin slash webpack.

26
00:01:05,90 --> 00:01:08,40
And again if I run npm run build,

27
00:01:08,40 --> 00:01:10,30
that's going to work as well.

28
00:01:10,30 --> 00:01:13,30
So, either one of those is totally fine,

29
00:01:13,30 --> 00:01:16,00
but you can choose from either.

30
00:01:16,00 --> 00:01:19,70
I can also use a dash W command.

31
00:01:19,70 --> 00:01:23,40
This is going to be a flag for running this as a watch.

32
00:01:23,40 --> 00:01:25,10
So, let me show you what I mean by this.

33
00:01:25,10 --> 00:01:27,40
I'm going to run npm run build,

34
00:01:27,40 --> 00:01:31,00
and notice that webpack is watching the files.

35
00:01:31,00 --> 00:01:35,10
If I go to my index JS file,

36
00:01:35,10 --> 00:01:39,20
and I can find that in my source.

37
00:01:39,20 --> 00:01:44,10
If I say hello everyone, and I hit save,

38
00:01:44,10 --> 00:01:47,20
we should see that this will rerun the files.

39
00:01:47,20 --> 00:01:49,30
So that seems like a quick thing in the terminal,

40
00:01:49,30 --> 00:01:52,30
but I can also see this if I look at my browser.

41
00:01:52,30 --> 00:01:54,90
So, right now it says hello everyone.

42
00:01:54,90 --> 00:01:59,50
If I change this to hello everybody,

43
00:01:59,50 --> 00:02:04,30
and I add a colossal amount of exclamation points,

44
00:02:04,30 --> 00:02:07,00
we should see that this watch command runs,

45
00:02:07,00 --> 00:02:09,20
and I should see hello everybody.

46
00:02:09,20 --> 00:02:12,00
Now we do have to hit a refresh here but that's okay.

47
00:02:12,00 --> 00:02:15,80
We do have that build occurring once more.

48
00:02:15,80 --> 00:02:17,60
So that's how we set up a build script

49
00:02:17,60 --> 00:02:19,90
to easily execute this command,

50
00:02:19,90 --> 00:02:21,70
and we can also add the watch flag

51
00:02:21,70 --> 00:02:25,20
in order to always watch files for changes

52
00:02:25,20 --> 00:02:28,00
and to rerun webpack when there are.

