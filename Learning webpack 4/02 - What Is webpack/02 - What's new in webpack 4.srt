1
00:00:00,50 --> 00:00:02,00
- [Instructor] If you're using an earlier version

2
00:00:02,00 --> 00:00:04,00
of webpack and are considering an upgrade,

3
00:00:04,00 --> 00:00:05,80
here are a few of the latest features

4
00:00:05,80 --> 00:00:08,60
that can help you decide whether to make the jump.

5
00:00:08,60 --> 00:00:12,40
First of all, webpack doesn't support Node.js four.

6
00:00:12,40 --> 00:00:15,00
But webpack is much faster than earlier versions,

7
00:00:15,00 --> 00:00:17,20
meaning that each time you run a build

8
00:00:17,20 --> 00:00:19,00
you can expect it to happen more quickly.

9
00:00:19,00 --> 00:00:21,90
In webpack four we also have development

10
00:00:21,90 --> 00:00:23,30
and production mode.

11
00:00:23,30 --> 00:00:26,50
With production mode, a build size is prioritized.

12
00:00:26,50 --> 00:00:29,80
For dev mode, the speed of the build is optimized.

13
00:00:29,80 --> 00:00:32,00
Another nice feature of webpack four

14
00:00:32,00 --> 00:00:35,60
is that you can import and export any web assembly module

15
00:00:35,60 --> 00:00:38,90
meaning you can load Russ, C ++, and C

16
00:00:38,90 --> 00:00:40,70
into your projects.

17
00:00:40,70 --> 00:00:42,60
While the main ideas are mostly the same

18
00:00:42,60 --> 00:00:44,20
you can definitely take advantage

19
00:00:44,20 --> 00:00:45,90
of all of these new features

20
00:00:45,90 --> 00:00:48,00
by upgrading to webpack four.

