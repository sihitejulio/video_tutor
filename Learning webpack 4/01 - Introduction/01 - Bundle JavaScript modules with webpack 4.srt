1
00:00:00,50 --> 00:00:03,20
- [Eve] JavaScript today is more powerful than ever,

2
00:00:03,20 --> 00:00:05,80
but on any JavaScript project there are a multitude

3
00:00:05,80 --> 00:00:08,00
of decisions that have to be made.

4
00:00:08,00 --> 00:00:10,20
How do I ensure that my code is compatible

5
00:00:10,20 --> 00:00:11,80
with different browsers?

6
00:00:11,80 --> 00:00:13,70
How do I make sure that my bundle sizes

7
00:00:13,70 --> 00:00:17,40
are small enough to load quickly on all sorts of devices?

8
00:00:17,40 --> 00:00:20,70
Luckily webpack is here to make all of this possible.

9
00:00:20,70 --> 00:00:22,20
In this course, we'll get into

10
00:00:22,20 --> 00:00:24,90
the most commonly used features in webpack,

11
00:00:24,90 --> 00:00:27,60
and we'll look at how webpack is used in the real world

12
00:00:27,60 --> 00:00:29,50
with hands-on activities.

13
00:00:29,50 --> 00:00:30,80
My name is Eve Porcello.

14
00:00:30,80 --> 00:00:33,20
I'm a software engineer and author at Moon Highway,

15
00:00:33,20 --> 00:00:35,60
and I've been configuring webpack projects

16
00:00:35,60 --> 00:00:37,10
since webpack was released.

17
00:00:37,10 --> 00:00:39,80
I found that there's really no better build tool

18
00:00:39,80 --> 00:00:42,10
in the JavaScript ecosystem today.

19
00:00:42,10 --> 00:00:44,00
So now is the time to learn about it,

20
00:00:44,00 --> 00:00:47,00
and really put it to work in your projects.

