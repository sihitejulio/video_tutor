1
00:00:00,50 --> 00:00:02,10
- [Instructor] Learning webpack will introduce us

2
00:00:02,10 --> 00:00:04,20
to new ways of packaging and building

3
00:00:04,20 --> 00:00:06,30
our JavaScript code.

4
00:00:06,30 --> 00:00:07,80
For that reason, I'll assume that you have

5
00:00:07,80 --> 00:00:11,20
a bit of JavaScript knowledge prior to taking this course.

6
00:00:11,20 --> 00:00:13,10
If you need a refresher, check out

7
00:00:13,10 --> 00:00:16,80
JavaScript Essential Training with Morton Rand-Hendrikson.

8
00:00:16,80 --> 00:00:20,20
We will also be installing webpack via npm,

9
00:00:20,20 --> 00:00:23,10
so you'll need to be sure to have node.js and npm

10
00:00:23,10 --> 00:00:25,20
installed to work with wepback.

11
00:00:25,20 --> 00:00:27,20
In a later video I'll show you how to make sure

12
00:00:27,20 --> 00:00:29,40
that these tools are installed.

13
00:00:29,40 --> 00:00:31,70
If you've never worked with them before though,

14
00:00:31,70 --> 00:00:34,30
you may want to watch Node.js Essential Training

15
00:00:34,30 --> 00:00:36,00
with Alex Banks.

16
00:00:36,00 --> 00:00:37,30
That's about all you would need

17
00:00:37,30 --> 00:00:39,00
to get started with this course,

18
00:00:39,00 --> 00:00:40,00
so let's jump right in.

