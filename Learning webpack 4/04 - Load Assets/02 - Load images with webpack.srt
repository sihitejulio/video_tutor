1
00:00:00,00 --> 00:00:01,90
- [Instructor] The process of loading images

2
00:00:01,90 --> 00:00:05,10
with webpack is very similar to loading CSS.

3
00:00:05,10 --> 00:00:09,20
With webpack, we load images by using URL Loader.

4
00:00:09,20 --> 00:00:12,00
Webpack inlines a URL to the image bundle,

5
00:00:12,00 --> 00:00:14,70
and then returns it from require.

6
00:00:14,70 --> 00:00:16,90
We want to do this because inlining images will

7
00:00:16,90 --> 00:00:19,30
reduce the number of http requests,

8
00:00:19,30 --> 00:00:22,10
which will then speed up our applications.

9
00:00:22,10 --> 00:00:25,80
We'll start by installing URL Loader.

10
00:00:25,80 --> 00:00:29,30
So, we'll type npm install url hyphen loader,

11
00:00:29,30 --> 00:00:32,90
and we'll save this again to our DEVit dependencies.

12
00:00:32,90 --> 00:00:34,00
Perfect.

13
00:00:34,00 --> 00:00:34,80
Now, what we'll do is we'll

14
00:00:34,80 --> 00:00:37,50
refactor our greeting component in a little bit.

15
00:00:37,50 --> 00:00:42,40
So, we'll say instead of returning just the h one,

16
00:00:42,40 --> 00:00:47,40
we'll return a div, and the div should have an h one.

17
00:00:47,40 --> 00:00:49,70
I guess I could just cut and paste this.

18
00:00:49,70 --> 00:00:51,60
That'll be a little easier.

19
00:00:51,60 --> 00:00:55,40
And then, we also want to add another nested div here.

20
00:00:55,40 --> 00:00:59,60
This div is going to have an id of image.

21
00:00:59,60 --> 00:01:00,50
Perfect.

22
00:01:00,50 --> 00:01:03,80
So, now, if we go over to our CSS file,

23
00:01:03,80 --> 00:01:06,20
we're going to add a style here,

24
00:01:06,20 --> 00:01:09,20
a little selector for our image.

25
00:01:09,20 --> 00:01:12,50
So, we'll say pound image, and then,

26
00:01:12,50 --> 00:01:16,50
we'll say background url,

27
00:01:16,50 --> 00:01:18,70
and the background URL is actually

28
00:01:18,70 --> 00:01:21,20
going to be this image that I've placed in this folder.

29
00:01:21,20 --> 00:01:24,10
So, you'll see this file called ski-day dot jpg.

30
00:01:24,10 --> 00:01:27,80
Again, feel free to use whatever image you'd like,

31
00:01:27,80 --> 00:01:32,60
but I'm just going to use this one, ski-day dot jpg.

32
00:01:32,60 --> 00:01:37,40
Going to give it a height of 500 pixels,

33
00:01:37,40 --> 00:01:42,30
and I'll give it a width of 500 pixels, perfect.

34
00:01:42,30 --> 00:01:43,40
The next step we'll take is we'll

35
00:01:43,40 --> 00:01:45,70
open up the webpack config.

36
00:01:45,70 --> 00:01:47,90
Now, the webpack config., just as we've

37
00:01:47,90 --> 00:01:51,80
added our babel loader and our style and CSS loaders,

38
00:01:51,80 --> 00:01:56,30
we need to add another neighboring loader for our URLs.

39
00:01:56,30 --> 00:01:58,20
For our test command, we're going to look

40
00:01:58,20 --> 00:02:02,40
for anything that's a jpg or a png, so,

41
00:02:02,40 --> 00:02:05,10
we'll say test, and we'll look

42
00:02:05,10 --> 00:02:11,90
for anything that's a png or a jpg,

43
00:02:11,90 --> 00:02:18,30
and then, we'll use a URL loader.

44
00:02:18,30 --> 00:02:19,30
Perfect.

45
00:02:19,30 --> 00:02:20,30
So, now that I've done this, I

46
00:02:20,30 --> 00:02:22,90
can come back over to the terminal,

47
00:02:22,90 --> 00:02:24,30
run my build command, then we should

48
00:02:24,30 --> 00:02:27,50
see that our ski-day jpg has been added.

49
00:02:27,50 --> 00:02:32,00
If we refresh, we see that we have our ski day picture here.

50
00:02:32,00 --> 00:02:33,80
If I right click over this,

51
00:02:33,80 --> 00:02:37,80
and I click inspect, I will open up my developer tools,

52
00:02:37,80 --> 00:02:41,00
and you'll see that this URL has been inlined here.

53
00:02:41,00 --> 00:02:43,10
So, this is all webpack doing this.

54
00:02:43,10 --> 00:02:45,80
It's been added inline to our bundle,

55
00:02:45,80 --> 00:02:49,00
to make our request a little bit smaller.

56
00:02:49,00 --> 00:02:50,10
So, this is pretty cool.

57
00:02:50,10 --> 00:02:52,50
Webpack can load images to help you better

58
00:02:52,50 --> 00:02:55,00
manage these binary dependencies.

