1
00:00:00,50 --> 00:00:02,10
- [Instructor] In addition to loading JavaScript

2
00:00:02,10 --> 00:00:04,90
with Webpack, we can load CSS, Sass, and Less

3
00:00:04,90 --> 00:00:06,60
to style our pages.

4
00:00:06,60 --> 00:00:09,60
The benefit of loading CSS as a module like this

5
00:00:09,60 --> 00:00:12,20
is that Webpack will only bundle the styles

6
00:00:12,20 --> 00:00:14,00
that our app uses.

7
00:00:14,00 --> 00:00:16,40
We can also require or import styles

8
00:00:16,40 --> 00:00:20,60
for use in certain files and it will perform transformations

9
00:00:20,60 --> 00:00:24,00
on Sass and Less to turn it into CSS.

10
00:00:24,00 --> 00:00:26,50
So let's first create a simple style sheet

11
00:00:26,50 --> 00:00:28,20
with some simple styles.

12
00:00:28,20 --> 00:00:30,70
I'm going to go to the source folder

13
00:00:30,70 --> 00:00:35,40
and create a new file called style.css.

14
00:00:35,40 --> 00:00:37,80
Now, in this file, I'm going to create a simple selector

15
00:00:37,80 --> 00:00:44,40
for an h1 and this h1 should have a font family of Arial.

16
00:00:44,40 --> 00:00:47,60
It also should have color salmon,

17
00:00:47,60 --> 00:00:50,40
a nice pink HTML color there.

18
00:00:50,40 --> 00:00:52,70
All right, so over in our index JS,

19
00:00:52,70 --> 00:00:54,10
we want to import this.

20
00:00:54,10 --> 00:01:00,40
So we'll just say import style.css, perfect.

21
00:01:00,40 --> 00:01:04,40
Now in order to import this and load it we need a loader.

22
00:01:04,40 --> 00:01:08,50
So we're going to say npm install style-loader

23
00:01:08,50 --> 00:01:10,70
and css-loader and we're going to save these

24
00:01:10,70 --> 00:01:12,90
to our dev dependencies.

25
00:01:12,90 --> 00:01:18,10
Now finally, we want to go back to our webpack config file,

26
00:01:18,10 --> 00:01:20,90
and in the webpack config, we need to define another rule.

27
00:01:20,90 --> 00:01:24,40
So we have our babble loader going on here, that's all good.

28
00:01:24,40 --> 00:01:27,90
But we need to add another neighboring object,

29
00:01:27,90 --> 00:01:31,90
which is going to look for any css files and transform them.

30
00:01:31,90 --> 00:01:34,90
So let's add a test key,

31
00:01:34,90 --> 00:01:40,50
we then will look for any css files, .css.

32
00:01:40,50 --> 00:01:44,50
We also want to add a use.

33
00:01:44,50 --> 00:01:46,50
So use is going to be in red,

34
00:01:46,50 --> 00:01:50,30
and we can add any loaders that we want to use here.

35
00:01:50,30 --> 00:01:54,40
So loader, style-loader

36
00:01:54,40 --> 00:02:00,90
and loader, css-loader, perfect.

37
00:02:00,90 --> 00:02:06,60
So now if I take this over and I run npm run build,

38
00:02:06,60 --> 00:02:08,20
we should see that this bundles this up.

39
00:02:08,20 --> 00:02:11,50
Notice that our style.css has been bundled.

40
00:02:11,50 --> 00:02:12,70
And if we hit refresh,

41
00:02:12,70 --> 00:02:15,90
we should see all of our styles being applied.

42
00:02:15,90 --> 00:02:19,40
So this is a quick way to add a little bit of styling

43
00:02:19,40 --> 00:02:22,90
to our react component, to our Java script file,

44
00:02:22,90 --> 00:02:24,60
simply by using the style-loader

45
00:02:24,60 --> 00:02:27,00
and the css-loaders from webpack.

