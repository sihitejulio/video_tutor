1
00:00:00,50 --> 00:00:01,90
- [Narrator] Another nice feature of webpack

2
00:00:01,90 --> 00:00:04,30
is that it allows you to set up a development server

3
00:00:04,30 --> 00:00:06,50
that will automatically reload your project

4
00:00:06,50 --> 00:00:08,40
when things change.

5
00:00:08,40 --> 00:00:12,10
Webpack dev server uses a node JS express server.

6
00:00:12,10 --> 00:00:14,10
This uses the webpack dev middleware

7
00:00:14,10 --> 00:00:15,90
to serve a webpack bundle.

8
00:00:15,90 --> 00:00:18,10
It opens up a socket using Socket IO

9
00:00:18,10 --> 00:00:20,20
that listens for any changes.

10
00:00:20,20 --> 00:00:22,30
The webpack dev server is an important tool

11
00:00:22,30 --> 00:00:25,00
to have at your disposal when working with webpack.

12
00:00:25,00 --> 00:00:26,70
When you include it in your project,

13
00:00:26,70 --> 00:00:28,40
it'll allow for the live reloading

14
00:00:28,40 --> 00:00:30,40
of all of your assets.

15
00:00:30,40 --> 00:00:32,00
Let's first start by installing it.

16
00:00:32,00 --> 00:00:37,90
We're going to install webpack dev server.

17
00:00:37,90 --> 00:00:39,10
Now once this is installed,

18
00:00:39,10 --> 00:00:41,70
we can go back over to our webpack config

19
00:00:41,70 --> 00:00:46,20
and we're going to add another node here called devServer.

20
00:00:46,20 --> 00:00:49,20
I usually like to add it here above all these loaders,

21
00:00:49,20 --> 00:00:51,20
so I can remember that it's here.

22
00:00:51,20 --> 00:00:56,00
We'll also add a contentBase path dot join,

23
00:00:56,00 --> 00:01:00,40
so underscore, underscore, dirname, and dist,

24
00:01:00,40 --> 00:01:02,90
and then we want to describe which port that should run on,

25
00:01:02,90 --> 00:01:05,80
so typically the port is 9000.

26
00:01:05,80 --> 00:01:08,70
Alright, so at this point what I'll do,

27
00:01:08,70 --> 00:01:10,90
is I'll open up our package json file,

28
00:01:10,90 --> 00:01:12,50
and I'm going to add another build script.

29
00:01:12,50 --> 00:01:17,00
So we have the build, which is just typical webpack.

30
00:01:17,00 --> 00:01:20,60
Let's add another one here called start dev

31
00:01:20,60 --> 00:01:23,90
and this is going to use

32
00:01:23,90 --> 00:01:30,70
node modules dot bin slash webpack dev server.

33
00:01:30,70 --> 00:01:31,50
Awesome.

34
00:01:31,50 --> 00:01:34,10
So you may not always need to include

35
00:01:34,10 --> 00:01:36,50
the binary for this

36
00:01:36,50 --> 00:01:38,70
but I always like to add it for good measure

37
00:01:38,70 --> 00:01:42,10
cause it's usually more compatible with all computers.

38
00:01:42,10 --> 00:01:44,90
Okay, so let's go back over to the start

39
00:01:44,90 --> 00:01:46,70
and again I called this start dev,

40
00:01:46,70 --> 00:01:51,40
so I can say npm run start dev

41
00:01:51,40 --> 00:01:52,90
and this will tell me that the project

42
00:01:52,90 --> 00:01:55,20
is running at local host 9000

43
00:01:55,20 --> 00:01:59,80
and I can pull that up here.

44
00:01:59,80 --> 00:02:01,30
And it seems to be true.

45
00:02:01,30 --> 00:02:03,50
It's running from that spot.

46
00:02:03,50 --> 00:02:05,60
Now let's go back over to our code

47
00:02:05,60 --> 00:02:08,20
to ensure that this is working as expected.

48
00:02:08,20 --> 00:02:10,60
We're going to open the source

49
00:02:10,60 --> 00:02:12,50
and let's open our style.

50
00:02:12,50 --> 00:02:14,70
First I want to change the color here,

51
00:02:14,70 --> 00:02:16,50
that's a nice little drop down.

52
00:02:16,50 --> 00:02:19,50
Blue, let's say blue violet,

53
00:02:19,50 --> 00:02:20,50
how about that?

54
00:02:20,50 --> 00:02:22,00
So if I save that

55
00:02:22,00 --> 00:02:26,30
and I go back over to our browser,

56
00:02:26,30 --> 00:02:29,80
once the build ends up bundling

57
00:02:29,80 --> 00:02:33,20
it's going to automatically reload this for us.

58
00:02:33,20 --> 00:02:37,10
So, a webpack dev server set up is fairly simple,

59
00:02:37,10 --> 00:02:40,00
but very useful in working on projects

60
00:02:40,00 --> 00:02:42,20
in development cause it's going to always

61
00:02:42,20 --> 00:02:43,00
reload your code for you.

