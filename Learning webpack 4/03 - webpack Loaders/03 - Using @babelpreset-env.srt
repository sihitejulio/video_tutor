1
00:00:00,50 --> 00:00:02,00
- [Instructor] To actually transform the code,

2
00:00:02,00 --> 00:00:04,00
we need to use a preset.

3
00:00:04,00 --> 00:00:06,00
In the most recent versions of Babel,

4
00:00:06,00 --> 00:00:07,70
we use babel/preset-env,

5
00:00:07,70 --> 00:00:11,80
which compiles our code to compatibility with most browsers.

6
00:00:11,80 --> 00:00:13,80
To learn more about the methodology behind this,

7
00:00:13,80 --> 00:00:15,70
you can check out the documentation

8
00:00:15,70 --> 00:00:19,70
at this link here under babel/preset-env.

9
00:00:19,70 --> 00:00:22,20
Next let's install this package using npm.

10
00:00:22,20 --> 00:00:23,10
We'll say,

11
00:00:23,10 --> 00:00:28,90
npm install @babel/preset-env.

12
00:00:28,90 --> 00:00:33,80
And then let's save this to our dev dependencies.

13
00:00:33,80 --> 00:00:37,10
At this point, we can go back to our webpack.config file

14
00:00:37,10 --> 00:00:40,40
and we're going to add on to our loader.

15
00:00:40,40 --> 00:00:44,30
So we'll say options.

16
00:00:44,30 --> 00:00:47,00
And then we're going to add this presets array.

17
00:00:47,00 --> 00:00:50,30
The array at this point will have

18
00:00:50,30 --> 00:00:56,90
just @babel/preset-env.

19
00:00:56,90 --> 00:01:00,80
So at this point, we can run npm run build.

20
00:01:00,80 --> 00:01:02,60
This is going to run this with the watch,

21
00:01:02,60 --> 00:01:05,30
and it's going to compile everything

22
00:01:05,30 --> 00:01:07,90
to the standards of preset-env,

23
00:01:07,90 --> 00:01:10,90
which basically means most every browser is going

24
00:01:10,90 --> 00:01:13,10
to be supported here.

25
00:01:13,10 --> 00:01:15,20
If you want to customize this further,

26
00:01:15,20 --> 00:01:17,90
you can use the Browserlist integration

27
00:01:17,90 --> 00:01:20,40
and you can be very granular and specific

28
00:01:20,40 --> 00:01:22,50
about which browsers you want to target.

29
00:01:22,50 --> 00:01:25,60
However, preset-env will capture most everything

30
00:01:25,60 --> 00:01:28,00
that we need to optimize for.

