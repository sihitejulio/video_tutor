1
00:00:00,50 --> 00:00:01,70
- [Instructor] webpack loaders can help

2
00:00:01,70 --> 00:00:04,10
us perform transformations on files.

3
00:00:04,10 --> 00:00:07,10
They can help with the loading of files and images,

4
00:00:07,10 --> 00:00:09,30
and webpack loaders are commonly used

5
00:00:09,30 --> 00:00:12,40
when using different dialects of JavaScript.

6
00:00:12,40 --> 00:00:16,00
For example, many React applications use JSX,

7
00:00:16,00 --> 00:00:20,30
or JavaScript as XML, to create user interface components.

8
00:00:20,30 --> 00:00:23,00
JSX doesn't run natively in the browser.

9
00:00:23,00 --> 00:00:27,30
It must be first transformed into plain JavaScript.

10
00:00:27,30 --> 00:00:29,90
You also may be using ECMAScript6,

11
00:00:29,90 --> 00:00:34,90
or ECMAScripts 2016, 2017 or even 2018.

12
00:00:34,90 --> 00:00:37,70
So not all browsers support all of the newest features

13
00:00:37,70 --> 00:00:39,20
without a transform.

14
00:00:39,20 --> 00:00:42,30
So like JSX, ECMAScript is often compiled

15
00:00:42,30 --> 00:00:46,60
into plan JavaScript prior to being loaded into the browser.

16
00:00:46,60 --> 00:00:49,10
The tool most commonly used for transformation

17
00:00:49,10 --> 00:00:53,30
of JSX and ES6 is Babel.

18
00:00:53,30 --> 00:00:56,20
When we use the Babel loader in webpack projects,

19
00:00:56,20 --> 00:00:58,80
we tell webpack to use Babel to perform

20
00:00:58,80 --> 00:01:02,00
these transformations on certain types of files.

21
00:01:02,00 --> 00:01:05,60
JSX and ES6 go in, get transpiled,

22
00:01:05,60 --> 00:01:07,90
and plain JavaScript comes out.

23
00:01:07,90 --> 00:01:09,70
For a full list of webpack loaders,

24
00:01:09,70 --> 00:01:12,50
you can check out the official webpack documentation,

25
00:01:12,50 --> 00:01:14,40
which provides a multitude of options

26
00:01:14,40 --> 00:01:17,00
for loading what you need with webpack.

