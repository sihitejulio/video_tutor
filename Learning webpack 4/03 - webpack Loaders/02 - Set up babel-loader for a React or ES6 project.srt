1
00:00:01,00 --> 00:00:02,40
- [Instructor] To set up the babel-loader,

2
00:00:02,40 --> 00:00:04,30
we'll need to take a few steps.

3
00:00:04,30 --> 00:00:06,70
Let's go to our terminal and we're going to,

4
00:00:06,70 --> 00:00:12,70
install babel-loader and also @babel/core

5
00:00:12,70 --> 00:00:15,50
and we're going to save these to our dev dependencies.

6
00:00:15,50 --> 00:00:17,20
Babel-loader is the loader,

7
00:00:17,20 --> 00:00:19,50
babel/core is the actual babel package

8
00:00:19,50 --> 00:00:21,50
that handles the transformations.

9
00:00:21,50 --> 00:00:24,90
And both are going to be added to our package json.

10
00:00:24,90 --> 00:00:27,90
Next we're going to need to adjust the webpack.config.

11
00:00:27,90 --> 00:00:31,60
We're going to add another key here called module

12
00:00:31,60 --> 00:00:35,70
and we're going to add another called rules.

13
00:00:35,70 --> 00:00:38,70
The next thing we want to do here is add an object

14
00:00:38,70 --> 00:00:41,80
and we're going to add a test key.

15
00:00:41,80 --> 00:00:45,40
Now this test key is going to say that whenever

16
00:00:45,40 --> 00:00:46,90
a JavaScript file is found,

17
00:00:46,90 --> 00:00:49,10
babel should transpile it.

18
00:00:49,10 --> 00:00:51,20
We can also define an optional key

19
00:00:51,20 --> 00:00:54,20
for exclude and this is just going to say

20
00:00:54,20 --> 00:00:57,70
don't run babel on the node modules folder

21
00:00:57,70 --> 00:00:59,90
'cause that will take a long time.

22
00:00:59,90 --> 00:01:01,00
And finally we're going

23
00:01:01,00 --> 00:01:05,40
to add another object here called use

24
00:01:05,40 --> 00:01:06,40
and we're going to say that this

25
00:01:06,40 --> 00:01:09,60
should use the babel-loader.

26
00:01:09,60 --> 00:01:11,00
Perfect.

27
00:01:11,00 --> 00:01:13,90
So let's go ahead and run our build command

28
00:01:13,90 --> 00:01:15,90
which will run webpack

29
00:01:15,90 --> 00:01:20,00
and even though it looks like some transpiling has occurred,

30
00:01:20,00 --> 00:01:22,40
nothing is really happening yet.

31
00:01:22,40 --> 00:01:24,90
This is actually what we should expect at this point

32
00:01:24,90 --> 00:01:28,30
because we need to add one other step to this.

33
00:01:28,30 --> 00:01:29,80
We need to define some rules

34
00:01:29,80 --> 00:01:32,20
around what should be transformed here.

35
00:01:32,20 --> 00:01:34,80
So, we're going to make some additional changes

36
00:01:34,80 --> 00:01:37,00
to handle these transformations.

