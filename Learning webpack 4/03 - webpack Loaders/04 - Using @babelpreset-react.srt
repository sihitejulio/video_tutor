1
00:00:00,50 --> 00:00:01,60
- [Instructor] Let's make some adjustments

2
00:00:01,60 --> 00:00:05,30
to our project to use React instead of jQuery.

3
00:00:05,30 --> 00:00:07,10
The first step I'm going to take,

4
00:00:07,10 --> 00:00:10,60
is to install React and React dom

5
00:00:10,60 --> 00:00:14,60
as dependencies using the save flag,

6
00:00:14,60 --> 00:00:17,50
then I'm going to go back to my Index JS file,

7
00:00:17,50 --> 00:00:20,10
and I'm going to replace this code with the following,

8
00:00:20,10 --> 00:00:23,10
so I'm import React from react,

9
00:00:23,10 --> 00:00:30,00
I also will render from react dom,

10
00:00:30,00 --> 00:00:32,50
and I'm just going to create a simple component

11
00:00:32,50 --> 00:00:37,50
here called Greeting, which returns an H one,

12
00:00:37,50 --> 00:00:42,00
and this H one should say "Hello from React."

13
00:00:42,00 --> 00:00:45,10
Finally, we want to render this, and remember

14
00:00:45,10 --> 00:00:47,80
we have that element called Target

15
00:00:47,80 --> 00:00:49,80
that we can mount this to, so I'll say

16
00:00:49,80 --> 00:00:52,80
that the greeting should be our component

17
00:00:52,80 --> 00:00:58,60
that is added to the dom at Target.

18
00:00:58,60 --> 00:01:04,00
Perfect. So now to use React web pack and Babel together.

19
00:01:04,00 --> 00:01:06,60
I need to install that one last preset,

20
00:01:06,60 --> 00:01:09,50
so I'll say N-P-M install

21
00:01:09,50 --> 00:01:14,60
at Babel slash preset hyphen React,

22
00:01:14,60 --> 00:01:18,00
and I'll save that as a dev dependency.

23
00:01:18,00 --> 00:01:19,70
Excellent. At this point, I'm going to go back

24
00:01:19,70 --> 00:01:22,80
to the code and open the webpack in config file.

25
00:01:22,80 --> 00:01:26,40
I know need to add my additional preset here,

26
00:01:26,40 --> 00:01:32,60
so at Babel slash preset React,

27
00:01:32,60 --> 00:01:33,90
and another requirement here,

28
00:01:33,90 --> 00:01:36,90
is I need to add a file to the root,

29
00:01:36,90 --> 00:01:39,20
so I'm going to add this to our Start folder

30
00:01:39,20 --> 00:01:43,30
and it's going to be called Dot Babel RC.

31
00:01:43,30 --> 00:01:45,30
SO this is another configuration file

32
00:01:45,30 --> 00:01:49,50
that lists which of the Babel presets that we're using.

33
00:01:49,50 --> 00:01:53,20
We're going to add a key here call Presets

34
00:01:53,20 --> 00:01:59,50
and it should say at Babel slash preset React

35
00:01:59,50 --> 00:02:05,60
and at Babel preset env.

36
00:02:05,60 --> 00:02:09,40
Excellent. So at this point, I can run this

37
00:02:09,40 --> 00:02:15,00
and remember our component here reads "Hello from React,"

38
00:02:15,00 --> 00:02:16,60
so if everything works okay,

39
00:02:16,60 --> 00:02:18,70
we should see that in our browser.

40
00:02:18,70 --> 00:02:23,50
Let's go ahead and run our build.

41
00:02:23,50 --> 00:02:26,40
That seems like a pretty successful message.

42
00:02:26,40 --> 00:02:30,10
If I hit refresh, we should see Hello from React.

43
00:02:30,10 --> 00:02:33,40
Excellent. So just to recap, we have added

44
00:02:33,40 --> 00:02:36,00
the Babel preset React command,

45
00:02:36,00 --> 00:02:38,50
we've added it to the webpack config,

46
00:02:38,50 --> 00:02:40,60
and we've added a Babel RC,

47
00:02:40,60 --> 00:02:45,00
and now we can use JSX with abandon in all our projects.

