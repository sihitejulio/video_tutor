1
00:00:00,40 --> 00:00:01,30
- [Instructor] Throughout these videos,

2
00:00:01,30 --> 00:00:03,10
we've discussed how Webpack aims

3
00:00:03,10 --> 00:00:05,40
to optimize our code for performance

4
00:00:05,40 --> 00:00:07,60
and for developer ergonomics.

5
00:00:07,60 --> 00:00:09,70
By inlining CSS in the images,

6
00:00:09,70 --> 00:00:11,90
we can make our code faster.

7
00:00:11,90 --> 00:00:13,10
With the Webpack config,

8
00:00:13,10 --> 00:00:16,50
we can automate processes like transpiling.

9
00:00:16,50 --> 00:00:18,10
Now that we understand the basics,

10
00:00:18,10 --> 00:00:19,30
there may be situations

11
00:00:19,30 --> 00:00:21,90
where we want to use code splitting.

12
00:00:21,90 --> 00:00:23,00
Code splitting allows you

13
00:00:23,00 --> 00:00:25,70
to break up your code into different bundles.

14
00:00:25,70 --> 00:00:28,70
This can be done using multiple entry points.

15
00:00:28,70 --> 00:00:30,50
For example, if I have an application

16
00:00:30,50 --> 00:00:33,70
that has an About us and a Contact us page,

17
00:00:33,70 --> 00:00:35,70
we can tell Webpack to create bundles

18
00:00:35,70 --> 00:00:37,00
for each of these,

19
00:00:37,00 --> 00:00:39,20
and then only the resources necessary

20
00:00:39,20 --> 00:00:41,60
for the page will be loaded.

21
00:00:41,60 --> 00:00:43,80
This reduces the size of the request

22
00:00:43,80 --> 00:00:45,40
and if somebody visits the site

23
00:00:45,40 --> 00:00:47,50
and only goes to the About page,

24
00:00:47,50 --> 00:00:50,30
they won't load the Contact us information.

25
00:00:50,30 --> 00:00:53,00
Additionally, we can use Webpack to create bundles

26
00:00:53,00 --> 00:00:54,20
for shared code

27
00:00:54,20 --> 00:00:55,00
and for vendor code.

