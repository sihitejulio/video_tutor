1
00:00:00,40 --> 00:00:02,40
- [Instructor] In our first exploration of code splitting,

2
00:00:02,40 --> 00:00:03,60
we'll put together a project

3
00:00:03,60 --> 00:00:06,00
that uses multiple entry points.

4
00:00:06,00 --> 00:00:07,20
With multiple entry points

5
00:00:07,20 --> 00:00:09,90
we create separate bundles for different pages,

6
00:00:09,90 --> 00:00:13,60
that way we'll only load the code that we need.

7
00:00:13,60 --> 00:00:16,80
So let's start by going to the dist folder,

8
00:00:16,80 --> 00:00:18,70
and I'm going to create two new files here.

9
00:00:18,70 --> 00:00:21,80
The first is going to be called about.html,

10
00:00:21,80 --> 00:00:25,50
the second called contact.

11
00:00:25,50 --> 00:00:29,10
Now, since we have the shell of an HTML page here,

12
00:00:29,10 --> 00:00:30,30
I'm going to go ahead and copy

13
00:00:30,30 --> 00:00:35,40
and paste this into about and into contact

14
00:00:35,40 --> 00:00:37,30
and then we'll make some customizations here.

15
00:00:37,30 --> 00:00:41,80
So, I'll say about webpack,

16
00:00:41,80 --> 00:00:47,20
and how about, contact webpack.

17
00:00:47,20 --> 00:00:49,90
Cool. At this point I need to go over

18
00:00:49,90 --> 00:00:55,10
to the source folder and create two new JavaScript files.

19
00:00:55,10 --> 00:00:56,50
Not surprisingly, these are going to

20
00:00:56,50 --> 00:01:00,80
be called about and contact.

21
00:01:00,80 --> 00:01:05,00
And, I already have my component for greeting here

22
00:01:05,00 --> 00:01:06,70
so I'm going to use this as a model

23
00:01:06,70 --> 00:01:09,70
for the about and the contact.

24
00:01:09,70 --> 00:01:11,20
So, let's go ahead and paste that in.

25
00:01:11,20 --> 00:01:12,80
I'm going to remove the style,

26
00:01:12,80 --> 00:01:15,50
I'm going to call this component about,

27
00:01:15,50 --> 00:01:20,40
I'm going to say that I want to render about,

28
00:01:20,40 --> 00:01:26,40
perfect, and I'll say, about webpack,

29
00:01:26,40 --> 00:01:29,50
and then I'll get rid of this image.

30
00:01:29,50 --> 00:01:32,10
Perfect, now I can take this

31
00:01:32,10 --> 00:01:34,60
and I can paste it into the contact file

32
00:01:34,60 --> 00:01:37,20
and say contact webpack.

33
00:01:37,20 --> 00:01:40,40
We will create the component contact

34
00:01:40,40 --> 00:01:42,60
and we'll render the contact.

35
00:01:42,60 --> 00:01:44,50
Perfect.

36
00:01:44,50 --> 00:01:47,50
Now we want to link the HTML to these files.

37
00:01:47,50 --> 00:01:50,00
So, it's going to look a little different

38
00:01:50,00 --> 00:01:51,10
than you might expect.

39
00:01:51,10 --> 00:01:52,50
So I need to adjust this so

40
00:01:52,50 --> 00:01:58,50
that we look for the about.bundle.js as our file.

41
00:01:58,50 --> 00:02:03,80
Similarly, we'll look for contact.bundle.js.

42
00:02:03,80 --> 00:02:07,10
Now, in order to set this bundling behavior up

43
00:02:07,10 --> 00:02:12,80
we want to move to the webpack config

44
00:02:12,80 --> 00:02:14,60
and within the webpack config

45
00:02:14,60 --> 00:02:17,90
we can make another adjustment here.

46
00:02:17,90 --> 00:02:19,90
And the adjustment is going to be for

47
00:02:19,90 --> 00:02:21,80
the entry point and output point.

48
00:02:21,80 --> 00:02:24,80
So, we're actually going to have two different entry points.

49
00:02:24,80 --> 00:02:27,50
The first is, again, about.

50
00:02:27,50 --> 00:02:29,70
And we need to put these in an object,

51
00:02:29,70 --> 00:02:32,40
so let's go ahead and do that.

52
00:02:32,40 --> 00:02:36,30
So the first is about, we'll paste that in.

53
00:02:36,30 --> 00:02:40,20
The second is contact

54
00:02:40,20 --> 00:02:45,50
and we'll look for a source contact.js.

55
00:02:45,50 --> 00:02:48,00
So, we can keep the path the same for the output,

56
00:02:48,00 --> 00:02:50,10
we want to put everything in the dist folder

57
00:02:50,10 --> 00:02:53,70
but we want to make sure that this is bundled like so.

58
00:02:53,70 --> 00:02:58,50
We're going to say name.bundle.js.

59
00:02:58,50 --> 00:03:01,20
So, it'll say about and contact.

60
00:03:01,20 --> 00:03:03,20
Alright, let's see how we do.

61
00:03:03,20 --> 00:03:06,10
We're going to run npm run build

62
00:03:06,10 --> 00:03:07,70
and this has worked as expected.

63
00:03:07,70 --> 00:03:09,80
We have one bundle for about

64
00:03:09,80 --> 00:03:12,30
and we have one bundle for contact.

65
00:03:12,30 --> 00:03:15,50
So, each of these bundles, it has been created

66
00:03:15,50 --> 00:03:17,80
for each of these different entry points.

67
00:03:17,80 --> 00:03:20,50
So, when you go to the about page

68
00:03:20,50 --> 00:03:22,20
you'll just get the about bundle.

69
00:03:22,20 --> 00:03:23,80
When you go to the contact page,

70
00:03:23,80 --> 00:03:25,90
you'll just get the contact bundle.

71
00:03:25,90 --> 00:03:28,40
So, this is the most basic form of code splitting

72
00:03:28,40 --> 00:03:30,20
but it's very, very effective

73
00:03:30,20 --> 00:03:32,00
when you're working on these webpack projects.

