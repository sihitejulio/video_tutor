1
00:00:00,50 --> 00:00:02,50
- [Instructor] Another nice feature to use in your project

2
00:00:02,50 --> 00:00:05,30
is the HtmlWebpackPlugin.

3
00:00:05,30 --> 00:00:07,60
This is going to generate an HTML file

4
00:00:07,60 --> 00:00:09,40
that links to the bundle.

5
00:00:09,40 --> 00:00:15,10
So I'm going to go ahead and npm install html-webpack-plugin

6
00:00:15,10 --> 00:00:18,00
and I'll save it to our dev dependencies.

7
00:00:18,00 --> 00:00:22,00
Now over in the code, I need to make a few adjustments.

8
00:00:22,00 --> 00:00:25,80
So let's make sure that we're in 04_04, start,

9
00:00:25,80 --> 00:00:27,90
and we'll grab the webpack.config.

10
00:00:27,90 --> 00:00:31,20
So we have these multiple entry points.

11
00:00:31,20 --> 00:00:33,60
I want to first start by getting rid of these.

12
00:00:33,60 --> 00:00:38,80
So I'll say ./src/index.js.

13
00:00:38,80 --> 00:00:43,90
And then I'm just going to export everything to the main file.

14
00:00:43,90 --> 00:00:47,60
Now the next thing I want to do is to add another key here,

15
00:00:47,60 --> 00:00:52,30
called plugins, and you can add this anywhere in the object.

16
00:00:52,30 --> 00:00:55,80
Plugins, should take in this array.

17
00:00:55,80 --> 00:01:03,90
And I'm going to use new HtmlWebpackPlugin.

18
00:01:03,90 --> 00:01:06,30
And I also need to import this,

19
00:01:06,30 --> 00:01:11,10
const HtmlWebpackPlugin

20
00:01:11,10 --> 00:01:14,10
and we'll require it.

21
00:01:14,10 --> 00:01:15,60
Okay.

22
00:01:15,60 --> 00:01:19,90
So now I want to do something in the dist folder.

23
00:01:19,90 --> 00:01:22,70
And that is to just go ahead

24
00:01:22,70 --> 00:01:25,20
and delete everything that's there.

25
00:01:25,20 --> 00:01:28,10
So this is scary to do, but it's okay.

26
00:01:28,10 --> 00:01:29,80
We're going to delete everything that's there

27
00:01:29,80 --> 00:01:32,70
so that we can auto-generate a file

28
00:01:32,70 --> 00:01:34,80
that is going to link to the bundle.

29
00:01:34,80 --> 00:01:36,60
So if everything goes as planned,

30
00:01:36,60 --> 00:01:40,70
our dist folder should have an HTML file

31
00:01:40,70 --> 00:01:42,90
as well as our bundle.

32
00:01:42,90 --> 00:01:45,50
Finally, just to make sure that this working as expected,

33
00:01:45,50 --> 00:01:47,50
I'm going to get rid of the optimization,

34
00:01:47,50 --> 00:01:49,20
the code splitting bit.

35
00:01:49,20 --> 00:01:52,80
Just so that we can see that the webpack plugin is working.

36
00:01:52,80 --> 00:01:54,80
So let's give it a save.

37
00:01:54,80 --> 00:01:56,50
We're going to go back to the terminal.

38
00:01:56,50 --> 00:01:59,00
And I'm going to say npm run build.

39
00:01:59,00 --> 00:02:03,20
This is going to generate two different files for me.

40
00:02:03,20 --> 00:02:06,40
The first is our index.html.

41
00:02:06,40 --> 00:02:08,10
The second is the main.

42
00:02:08,10 --> 00:02:10,40
So let's take a look at the folder.

43
00:02:10,40 --> 00:02:11,90
We should see the index.

44
00:02:11,90 --> 00:02:13,70
So this is what it generates for you.

45
00:02:13,70 --> 00:02:16,30
So it's nice that it links to the bundle,

46
00:02:16,30 --> 00:02:18,20
but it's also nice that you don't have to

47
00:02:18,20 --> 00:02:19,80
manually type this out.

48
00:02:19,80 --> 00:02:23,20
So if there's any case where you ever need to do this,

49
00:02:23,20 --> 00:02:25,70
you can use the HtmlWebpackPlugin

50
00:02:25,70 --> 00:02:27,00
to generate this file for you.

