1
00:00:00,50 --> 00:00:01,70
- [Instructor] Once our code is ready to go

2
00:00:01,70 --> 00:00:03,40
to production, we want to make sure

3
00:00:03,40 --> 00:00:05,70
that it is as small as possible.

4
00:00:05,70 --> 00:00:09,00
So one way that we can do this is by minifying it

5
00:00:09,00 --> 00:00:11,50
and making it production ready.

6
00:00:11,50 --> 00:00:15,10
So a really nice tool for this is a webpack plugin

7
00:00:15,10 --> 00:00:17,70
called the Uglify JS webpack plugin.

8
00:00:17,70 --> 00:00:20,30
And we can use this to make these files

9
00:00:20,30 --> 00:00:23,50
as small as possible by minifying them.

10
00:00:23,50 --> 00:00:26,60
The first thing I want to do is NPM install

11
00:00:26,60 --> 00:00:32,20
the Uglify JS webpack plugin.

12
00:00:32,20 --> 00:00:34,50
Save dev.

13
00:00:34,50 --> 00:00:37,80
Once this is installed we can add it to our config.

14
00:00:37,80 --> 00:00:40,90
So let's go ahead

15
00:00:40,90 --> 00:00:46,60
and add Uglify JS plugin.

16
00:00:46,60 --> 00:00:48,80
And then we'll require it.

17
00:00:48,80 --> 00:00:51,00
Perfect.

18
00:00:51,00 --> 00:00:53,40
Within our config object we're going to add this here.

19
00:00:53,40 --> 00:00:56,40
So we'll say optimization.

20
00:00:56,40 --> 00:01:00,80
We will add a key called minimizer.

21
00:01:00,80 --> 00:01:06,30
So we'll just add new Uglify JS plugin.

22
00:01:06,30 --> 00:01:08,10
And we will create this.

23
00:01:08,10 --> 00:01:12,10
Now I can go ahead and run this.

24
00:01:12,10 --> 00:01:17,40
NPM run build.

25
00:01:17,40 --> 00:01:19,50
And we should see a couple things happen.

26
00:01:19,50 --> 00:01:23,30
So first of all, we should see that our

27
00:01:23,30 --> 00:01:25,60
index HTML has been generated

28
00:01:25,60 --> 00:01:29,00
using the HTML webpack plugin.

29
00:01:29,00 --> 00:01:33,00
Also should see that the main JS file has been minified.

30
00:01:33,00 --> 00:01:36,80
So this is uglifying it, taking all the variables

31
00:01:36,80 --> 00:01:38,90
and making them as small as possible.

32
00:01:38,90 --> 00:01:41,30
So this is a nice step to take before

33
00:01:41,30 --> 00:01:43,40
pushing out to production so that

34
00:01:43,40 --> 00:01:46,00
these files load as fast as is possible.

