1
00:00:00,40 --> 00:00:02,50
- [Instructor] To take the optimizations one step further

2
00:00:02,50 --> 00:00:05,60
we can use the split chunks optimization.

3
00:00:05,60 --> 00:00:07,10
So first I'm going to add a key here

4
00:00:07,10 --> 00:00:10,90
to our webpack config called optimization.

5
00:00:10,90 --> 00:00:15,90
We're going to add a key called split chunks.

6
00:00:15,90 --> 00:00:20,00
And another key called chunks and we'll say all.

7
00:00:20,00 --> 00:00:21,90
Now what this is going to do is it's going to look

8
00:00:21,90 --> 00:00:24,90
for any repeated code between my two bundles

9
00:00:24,90 --> 00:00:28,10
and it's going to create a vendor bundle for that.

10
00:00:28,10 --> 00:00:31,50
So both of these, let's see

11
00:00:31,50 --> 00:00:34,50
both of these about and contact

12
00:00:34,50 --> 00:00:36,30
have react and react on.

13
00:00:36,30 --> 00:00:38,40
So webpack is going to create a bundle

14
00:00:38,40 --> 00:00:41,10
just for that vendor code.

15
00:00:41,10 --> 00:00:42,80
Let's run it.

16
00:00:42,80 --> 00:00:46,30
Npm run build.

17
00:00:46,30 --> 00:00:47,80
And we should see that we've created

18
00:00:47,80 --> 00:00:50,90
a bundle for about, for contact.

19
00:00:50,90 --> 00:00:52,70
And then we also have this vendors bundle

20
00:00:52,70 --> 00:00:54,90
that is much, much bigger.

21
00:00:54,90 --> 00:00:59,10
And that has all of the code that we've imported

22
00:00:59,10 --> 00:01:01,40
from react and react on.

23
00:01:01,40 --> 00:01:03,50
So this is a really useful thing if you're using

24
00:01:03,50 --> 00:01:04,50
a lot of libraries.

25
00:01:04,50 --> 00:01:07,30
You can use webpack to branch that out

26
00:01:07,30 --> 00:01:11,10
into its own thing, to minimize repeating of code

27
00:01:11,10 --> 00:01:13,00
and to just make things more efficient.

