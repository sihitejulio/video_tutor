1
00:00:00,50 --> 00:00:03,00
- [Eve] Thank you for joining me for this course.

2
00:00:03,00 --> 00:00:05,60
Now you'll be able to use webpack in your projects

3
00:00:05,60 --> 00:00:08,60
and you'll be able to join teams already using webpack,

4
00:00:08,60 --> 00:00:10,80
and really hit the ground running.

5
00:00:10,80 --> 00:00:13,10
Now that we have an understanding of how webpack works,

6
00:00:13,10 --> 00:00:15,70
there are many different loaders that you can use

7
00:00:15,70 --> 00:00:18,10
with all sorts of different types of data.

8
00:00:18,10 --> 00:00:19,70
So if you're looking for specifics

9
00:00:19,70 --> 00:00:21,50
about how to take the next steps,

10
00:00:21,50 --> 00:00:25,20
I'd checkout webpack.js.org/guides

11
00:00:25,20 --> 00:00:27,80
for all sorts of use cases.

12
00:00:27,80 --> 00:00:29,60
Also, since you're taking this tutorial

13
00:00:29,60 --> 00:00:32,90
you may already use, or be interested in using, React.

14
00:00:32,90 --> 00:00:35,40
I encourage you to checkout the official documentation

15
00:00:35,40 --> 00:00:37,20
for React, 'cause they're rolling out

16
00:00:37,20 --> 00:00:39,70
some really cool new features right now.

17
00:00:39,70 --> 00:00:41,40
So again, I thank you for watching

18
00:00:41,40 --> 00:00:43,40
and I hope that webpack helps you build

19
00:00:43,40 --> 00:00:46,00
many powerful apps more efficiently.

