1
00:00:00,40 --> 00:00:01,50
- [Instructor] The most common way

2
00:00:01,50 --> 00:00:04,10
to implement a Content Security Policy

3
00:00:04,10 --> 00:00:07,20
is by configuring a header on your web server.

4
00:00:07,20 --> 00:00:10,00
This header is sent with the http response

5
00:00:10,00 --> 00:00:12,90
when an html document is requested.

6
00:00:12,90 --> 00:00:15,70
Browsers use the policy detailed in this header

7
00:00:15,70 --> 00:00:18,00
to decide which external resources will be

8
00:00:18,00 --> 00:00:21,30
allowed to load for the current page.

9
00:00:21,30 --> 00:00:24,00
The exercise files for this video use the

10
00:00:24,00 --> 00:00:26,70
fictitious site hansel and petal.

11
00:00:26,70 --> 00:00:30,20
And so digging in I'm going to inspect this page

12
00:00:30,20 --> 00:00:32,60
and just taking a look for a sec

13
00:00:32,60 --> 00:00:35,50
at the overall content we're loading here.

14
00:00:35,50 --> 00:00:38,60
You can see down here we've got some scripts and

15
00:00:38,60 --> 00:00:41,50
one of them's loading from ajax.googleapis.com

16
00:00:41,50 --> 00:00:44,00
so that's a Google CDN and then I've got a few

17
00:00:44,00 --> 00:00:47,50
also loading from hpassets.herokuapp.com

18
00:00:47,50 --> 00:00:50,00
which is a CDN I've set up at a

19
00:00:50,00 --> 00:00:53,50
Heroku subdomain just for this course.

20
00:00:53,50 --> 00:00:56,60
And then my main page, my html is loading from a

21
00:00:56,60 --> 00:00:58,80
different Heroku subdomain which is

22
00:00:58,80 --> 00:01:01,60
hanselandpetal.herokuapp.com.

23
00:01:01,60 --> 00:01:03,80
And I'm going to full screen this.

24
00:01:03,80 --> 00:01:05,30
So the point here is that this site

25
00:01:05,30 --> 00:01:07,90
is loading content from different subdomains.

26
00:01:07,90 --> 00:01:11,50
If we right click one of the images and go to inspect,

27
00:01:11,50 --> 00:01:20,20
I've also got that loading from my CDN for this project.

28
00:01:20,20 --> 00:01:22,60
And so moving over to my editor,

29
00:01:22,60 --> 00:01:26,40
I'm in the index.js file for the site server

30
00:01:26,40 --> 00:01:30,80
which is where I'm serving the public html files.

31
00:01:30,80 --> 00:01:34,60
And so this is my express router configuration

32
00:01:34,60 --> 00:01:40,60
and I want to add a custom header to my app.get route here

33
00:01:40,60 --> 00:01:42,30
and I'm going to put that just before

34
00:01:42,30 --> 00:01:45,90
the response.render statement.

35
00:01:45,90 --> 00:01:51,10
And so within response.set I'm going to add

36
00:01:51,10 --> 00:01:57,00
a Content-Security-Policy header

37
00:01:57,00 --> 00:02:00,90
with a value of script-src and then

38
00:02:00,90 --> 00:02:04,60
I need the word self with single quotes around it.

39
00:02:04,60 --> 00:02:06,90
And so this specifies that I want to restrict

40
00:02:06,90 --> 00:02:09,30
the source values of script elements

41
00:02:09,30 --> 00:02:11,70
to the same origin that the html

42
00:02:11,70 --> 00:02:14,10
documents themselves load from.

43
00:02:14,10 --> 00:02:15,90
And so this is what's called a directive,

44
00:02:15,90 --> 00:02:17,70
script source is the directive

45
00:02:17,70 --> 00:02:20,50
and self in quotes is the value.

46
00:02:20,50 --> 00:02:22,30
Now obviously this is too restrictive

47
00:02:22,30 --> 00:02:25,10
for our page because I'm not even loading any scripts

48
00:02:25,10 --> 00:02:28,70
from that same origin that the html is coming from

49
00:02:28,70 --> 00:02:34,50
but I want to run this here and just see the effect.

50
00:02:34,50 --> 00:02:41,20
So I'll save this, and so over at my terminal,

51
00:02:41,20 --> 00:02:53,00
I need to push my changes up and build them.

52
00:02:53,00 --> 00:02:58,60
And so Heroku is going to build that out for me.

53
00:02:58,60 --> 00:03:01,40
And then once this is done building,

54
00:03:01,40 --> 00:03:05,20
I can go reload that page and see the effect.

55
00:03:05,20 --> 00:03:07,50
Now again we've built a header here

56
00:03:07,50 --> 00:03:09,70
that we expect to exclude some resources

57
00:03:09,70 --> 00:03:12,60
that we need from other origins.

58
00:03:12,60 --> 00:03:14,90
And so I'm going to do a hard reload here.

59
00:03:14,90 --> 00:03:21,60
So that's shift reload, so Shift Command + R on a Mac.

60
00:03:21,60 --> 00:03:27,20
And in the console and we've got five different

61
00:03:27,20 --> 00:03:30,90
refusals to load scripts because they violate

62
00:03:30,90 --> 00:03:33,00
the Content Security Policy directive.

63
00:03:33,00 --> 00:03:34,90
And so notice that we don't have any animation

64
00:03:34,90 --> 00:03:38,10
going on anymore here and basically anything

65
00:03:38,10 --> 00:03:41,80
that's requiring those scripts here is not going to work.

66
00:03:41,80 --> 00:03:44,00
And that's what we expect to happen.

67
00:03:44,00 --> 00:03:46,90
So the good news is the policy worked that we specified,

68
00:03:46,90 --> 00:03:49,70
the browser implemented it and we didn't get script files

69
00:03:49,70 --> 00:03:51,40
because none of them were being served

70
00:03:51,40 --> 00:03:54,70
from that web server that's serving the html document.

71
00:03:54,70 --> 00:03:56,20
So the bad news is we need these scripts

72
00:03:56,20 --> 00:03:58,80
to load but that's easily remedied.

73
00:03:58,80 --> 00:04:05,50
So switching back to our index.js file for the site server.

74
00:04:05,50 --> 00:04:08,40
I'm simply going to add additional origins to

75
00:04:08,40 --> 00:04:11,60
the value for the script source directive.

76
00:04:11,60 --> 00:04:15,20
And so inside those double quotes

77
00:04:15,20 --> 00:04:21,90
I want to add https://hpassets.herokuapp.com.

78
00:04:21,90 --> 00:04:25,20
So that's my asset server for this project.

79
00:04:25,20 --> 00:04:26,60
You're going to have your own if

80
00:04:26,60 --> 00:04:29,20
you're actually working through this,

81
00:04:29,20 --> 00:04:36,20
and then I need https://ajax.googleapis.com.

82
00:04:36,20 --> 00:04:39,30
That's where we're grabbing the jQuery files from.

83
00:04:39,30 --> 00:04:41,30
It's a jQuery CDN.

84
00:04:41,30 --> 00:04:58,50
And so saving those changes and I need to deploy this again.

85
00:04:58,50 --> 00:05:02,80
And so this time once Heroku finishes building this out,

86
00:05:02,80 --> 00:05:05,20
I'm going to expect that because I have

87
00:05:05,20 --> 00:05:08,20
specified the correct origins for

88
00:05:08,20 --> 00:05:10,40
all of the scripts in this page,

89
00:05:10,40 --> 00:05:12,60
that my Content Security Policy should

90
00:05:12,60 --> 00:05:15,60
allow all of my scripts to load.

91
00:05:15,60 --> 00:05:18,70
And so back in the browser again I'm going to do a hard reload

92
00:05:18,70 --> 00:05:21,60
so Shift Command + R on a Mac,

93
00:05:21,60 --> 00:05:24,50
and I've got no errors in the console

94
00:05:24,50 --> 00:05:27,20
and I notice that my animation is working,

95
00:05:27,20 --> 00:05:30,10
so the scripts loaded and then

96
00:05:30,10 --> 00:05:31,80
if I switch over to the network tab

97
00:05:31,80 --> 00:05:33,90
and I'm going to do a reload one more time,

98
00:05:33,90 --> 00:05:35,30
just so I can log everything,

99
00:05:35,30 --> 00:05:38,00
and at the very top we have that request for the html

100
00:05:38,00 --> 00:05:41,50
document and in the response centers we can see

101
00:05:41,50 --> 00:05:45,20
that Content Security Policy header with its value,

102
00:05:45,20 --> 00:05:48,10
the scripts source directive being set for self,

103
00:05:48,10 --> 00:05:54,00
for our custom CDN, and for the Google API CDN.

