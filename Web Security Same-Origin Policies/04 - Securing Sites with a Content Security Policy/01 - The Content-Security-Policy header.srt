1
00:00:00,50 --> 00:00:02,50
- [Instructor] Solving script injection attacks

2
00:00:02,50 --> 00:00:05,90
against websites is a complex problem.

3
00:00:05,90 --> 00:00:09,10
Browsers implement a same origin policy for scripts.

4
00:00:09,10 --> 00:00:11,10
But this policy focuses on limiting

5
00:00:11,10 --> 00:00:12,60
how scripts can be loaded

6
00:00:12,60 --> 00:00:15,60
rather than where scripts can be loaded from.

7
00:00:15,60 --> 00:00:16,90
On the modern web,

8
00:00:16,90 --> 00:00:20,10
websites commonly depend on scripts from many sources,

9
00:00:20,10 --> 00:00:23,60
including analytics, CDNs and widgets.

10
00:00:23,60 --> 00:00:26,30
So simply tightening down the same origin policy

11
00:00:26,30 --> 00:00:30,60
to load scripts only from a page's origin wouldn't work.

12
00:00:30,60 --> 00:00:33,60
However a customizable version of this idea

13
00:00:33,60 --> 00:00:35,80
has been standardized and implemented

14
00:00:35,80 --> 00:00:38,80
through the Content-Security-Policy header.

15
00:00:38,80 --> 00:00:41,80
This header is set in the HTTP response

16
00:00:41,80 --> 00:00:45,50
when an HTML document is requested by a user.

17
00:00:45,50 --> 00:00:47,80
Content-Security-Policy enables a site

18
00:00:47,80 --> 00:00:51,20
to list exactly which domains the HTML document

19
00:00:51,20 --> 00:00:53,30
can load scripts from.

20
00:00:53,30 --> 00:00:55,20
Browsers then deny requests

21
00:00:55,20 --> 00:00:58,80
for scripts from any other servers.

22
00:00:58,80 --> 00:01:00,60
The architecture of this policy

23
00:01:00,60 --> 00:01:03,20
allows sites to whitelist only servers

24
00:01:03,20 --> 00:01:05,10
containing resources they need

25
00:01:05,10 --> 00:01:07,70
such as CDNs and widgets.

26
00:01:07,70 --> 00:01:10,40
As a result, if an attacker is successful

27
00:01:10,40 --> 00:01:12,50
in getting the page to request a script

28
00:01:12,50 --> 00:01:14,50
through a cross site scripting attack

29
00:01:14,50 --> 00:01:16,70
the browser will refuse to load the script

30
00:01:16,70 --> 00:01:20,10
because the origin isn't whitelisted.

31
00:01:20,10 --> 00:01:23,80
In addition to disallowing non-whitelisted sites,

32
00:01:23,80 --> 00:01:26,80
a site that implements the Content-Security-Policy header

33
00:01:26,80 --> 00:01:29,60
no longer supports inline JavaScript.

34
00:01:29,60 --> 00:01:32,70
This means that sites must remove code within script tags

35
00:01:32,70 --> 00:01:36,20
in an HTML file, JavaScript URLs

36
00:01:36,20 --> 00:01:38,10
and inline event handlers

37
00:01:38,10 --> 00:01:42,30
and handle those tasks using script files instead.

38
00:01:42,30 --> 00:01:44,60
Attackers have come up with all sorts of ways

39
00:01:44,60 --> 00:01:47,20
to breach browser security through scripts

40
00:01:47,20 --> 00:01:50,20
and a single addition to the browser security model

41
00:01:50,20 --> 00:01:52,30
won't mitigate all of them.

42
00:01:52,30 --> 00:01:54,50
But the Content-Security-Policy header

43
00:01:54,50 --> 00:01:56,00
provides an important tool

44
00:01:56,00 --> 00:01:58,00
for securing sites on the web today.

