1
00:00:00,50 --> 00:00:02,90
- [Narrator] The contents security policy specification

2
00:00:02,90 --> 00:00:04,60
includes a number of directives

3
00:00:04,60 --> 00:00:07,60
for different items besides script source.

4
00:00:07,60 --> 00:00:08,90
These directives can enable you

5
00:00:08,90 --> 00:00:11,70
to create policies for specific resources

6
00:00:11,70 --> 00:00:14,80
such as styles, images, and other media.

7
00:00:14,80 --> 00:00:19,20
As well as set policies for nested elements in the Dom.

8
00:00:19,20 --> 00:00:21,80
The frame source directive applies to items

9
00:00:21,80 --> 00:00:25,30
in embedded contexts, such as Iframes.

10
00:00:25,30 --> 00:00:28,20
This policy is especially useful for embedded content

11
00:00:28,20 --> 00:00:30,20
such as videos hosted on YouTube

12
00:00:30,20 --> 00:00:32,30
or for social media widgets such as

13
00:00:32,30 --> 00:00:34,50
those from Facebook or Twitter.

14
00:00:34,50 --> 00:00:37,40
You specify origins for frame source in the same way

15
00:00:37,40 --> 00:00:40,40
you do for script source and other similar directives,

16
00:00:40,40 --> 00:00:43,10
as a space separated list.

17
00:00:43,10 --> 00:00:45,40
I'm joining to add a LinkedIn widget to the website

18
00:00:45,40 --> 00:00:46,80
for this course.

19
00:00:46,80 --> 00:00:49,00
This is going to create an Iframe but it will

20
00:00:49,00 --> 00:00:51,40
load a script before doing so.

21
00:00:51,40 --> 00:00:53,60
So to start, I need to add this origin

22
00:00:53,60 --> 00:00:56,80
to my script source directive.

23
00:00:56,80 --> 00:01:00,60
So, in index.js or my site directory,

24
00:01:00,60 --> 00:01:03,50
I'm going to add one more origin

25
00:01:03,50 --> 00:01:05,00
to the script source directive.

26
00:01:05,00 --> 00:01:11,80
And that's going to be https://platform.linkedin.com

27
00:01:11,80 --> 00:01:16,00
which is the URL that's used by the LinkedIn widget.

28
00:01:16,00 --> 00:01:19,20
And then at the end of the script source value

29
00:01:19,20 --> 00:01:21,70
I'm going to add in a semicolon

30
00:01:21,70 --> 00:01:25,80
and then another directive, frame-src.

31
00:01:25,80 --> 00:01:31,60
And to start I'm going to specify none, in single quotes

32
00:01:31,60 --> 00:01:33,30
as the value for this directive

33
00:01:33,30 --> 00:01:37,60
just to see what effect this has on the page.

34
00:01:37,60 --> 00:01:41,20
Then I'm going to save this

35
00:01:41,20 --> 00:01:44,00
and switching over to the HTML file

36
00:01:44,00 --> 00:01:46,10
and down here on line 166,

37
00:01:46,10 --> 00:01:50,00
I've included in the HTML code a list item element

38
00:01:50,00 --> 00:01:52,80
that includes the preset code

39
00:01:52,80 --> 00:01:57,60
for the Hansel and Petal company on LinkedIn.

40
00:01:57,60 --> 00:02:00,50
So, I'm just going to uncomment that and save

41
00:02:00,50 --> 00:02:02,30
that HTML file.

42
00:02:02,30 --> 00:02:04,80
So, when the page loads in the browser,

43
00:02:04,80 --> 00:02:10,00
this URL at platform.linkedin.com will be requested

44
00:02:10,00 --> 00:02:11,90
and it's going to try and create an Iframe

45
00:02:11,90 --> 00:02:14,50
and we'll see what happens.

46
00:02:14,50 --> 00:02:18,30
And so, in the terminal

47
00:02:18,30 --> 00:02:20,40
I'm going to deploy

48
00:02:20,40 --> 00:02:29,00
(typing)

49
00:02:29,00 --> 00:02:32,90
And so we've specified that LinkedIn origin

50
00:02:32,90 --> 00:02:35,40
as a legal script source for the page

51
00:02:35,40 --> 00:02:38,80
but we've also specified a frame source

52
00:02:38,80 --> 00:02:43,40
for those nested contexts including an Iframe

53
00:02:43,40 --> 00:02:45,70
with a more restrictive policy.

54
00:02:45,70 --> 00:02:49,20
And so, going back to our browser

55
00:02:49,20 --> 00:02:55,20
and I'm going to reload.

56
00:02:55,20 --> 00:02:57,60
So, we have an error in the console,

57
00:02:57,60 --> 00:02:59,60
specifically for trying to load something

58
00:02:59,60 --> 00:03:05,30
from LinkedIn.com because it violates script source, Self.

59
00:03:05,30 --> 00:03:07,30
So we can see from this error that even though

60
00:03:07,30 --> 00:03:11,70
our original script source is platform.linkedin.com

61
00:03:11,70 --> 00:03:15,40
we also have a request going to www.linkedin.com

62
00:03:15,40 --> 00:03:17,40
and so we need to add that

63
00:03:17,40 --> 00:03:21,70
to our content security policy directive

64
00:03:21,70 --> 00:03:24,00
for script source.

65
00:03:24,00 --> 00:03:28,40
And so going back to index.js before the semicolon

66
00:03:28,40 --> 00:03:30,50
after platform.linkedin.com we're going to do

67
00:03:30,50 --> 00:03:37,20
https://www.linkedin.com

68
00:03:37,20 --> 00:03:39,10
going to save that

69
00:03:39,10 --> 00:03:43,60
and I'm going to deploy it.

70
00:03:43,60 --> 00:03:45,90
So this is a process that's pretty common.

71
00:03:45,90 --> 00:03:51,40
When we are including a widget we add an origin,

72
00:03:51,40 --> 00:03:53,50
we run it and we see what kind of violations we get,

73
00:03:53,50 --> 00:03:55,40
we add that origin too,

74
00:03:55,40 --> 00:03:57,30
because often when a script is loaded

75
00:03:57,30 --> 00:03:59,90
for the widget other scripts are then loaded

76
00:03:59,90 --> 00:04:02,30
to continue building it.

77
00:04:02,30 --> 00:04:07,10
So, now going to the browser and reloading.

78
00:04:07,10 --> 00:04:10,30
Now we've got two issues,

79
00:04:10,30 --> 00:04:11,10
but we notice here

80
00:04:11,10 --> 00:04:13,10
that now we're down to the frame.

81
00:04:13,10 --> 00:04:18,50
Now we're having a violation in the frame source directive.

82
00:04:18,50 --> 00:04:21,30
We're trying to load something from www.linkedin.com

83
00:04:21,30 --> 00:04:24,60
in the frame and we have prohibited everything

84
00:04:24,60 --> 00:04:29,80
except for the origin of the HTML file as an origin

85
00:04:29,80 --> 00:04:30,80
for that frame.

86
00:04:30,80 --> 00:04:32,30
So, now we know we got to put that in

87
00:04:32,30 --> 00:04:34,40
as a frame source as well.

88
00:04:34,40 --> 00:04:38,70
So in the frame source I'm going to take out none in quotes

89
00:04:38,70 --> 00:04:44,20
and instead I'm going to add in the www.linkedin.com.

90
00:04:44,20 --> 00:04:49,70
I'm going to save that and redeploy.

91
00:04:49,70 --> 00:04:51,50
So now we're starting to allow,

92
00:04:51,50 --> 00:04:54,30
we're allowing at least the first level of script

93
00:04:54,30 --> 00:04:57,80
as a source within the Iframe that's being created

94
00:04:57,80 --> 00:05:00,50
on this page, and so once this deploys

95
00:05:00,50 --> 00:05:04,90
we can reload and see what the console looks like.

96
00:05:04,90 --> 00:05:06,00
And there we are.

97
00:05:06,00 --> 00:05:10,60
We have our widget and we have no errors.

98
00:05:10,60 --> 00:05:13,80
So, by specifying a value for the frame source directive

99
00:05:13,80 --> 00:05:16,60
we've identified specifically where data can come

100
00:05:16,60 --> 00:05:19,60
into a nested context and kept that limited

101
00:05:19,60 --> 00:05:22,90
to only the remote sources that we need

102
00:05:22,90 --> 00:05:26,00
for the scripts that we're loading in our document.

