1
00:00:00,00 --> 00:00:02,20
- [Instructor] Content Security Policy covers a wide

2
00:00:02,20 --> 00:00:06,80
variety of content, including fonts, images, other media,

3
00:00:06,80 --> 00:00:08,60
scripts and styles.

4
00:00:08,60 --> 00:00:11,70
You may create a finely tuned content security policy

5
00:00:11,70 --> 00:00:16,40
focused on one area of concern say, say scripts, but if

6
00:00:16,40 --> 00:00:19,70
your policy doesn't include source directives for other data

7
00:00:19,70 --> 00:00:23,70
types, then those policies are wide open by default.

8
00:00:23,70 --> 00:00:27,10
You can use the default source directive to specify

9
00:00:27,10 --> 00:00:30,20
allowable origins for all the types of content

10
00:00:30,20 --> 00:00:31,80
covered by the policy.

11
00:00:31,80 --> 00:00:36,70
All directives that end with -src take none as a value.

12
00:00:36,70 --> 00:00:39,20
So by starting your content security policy with the

13
00:00:39,20 --> 00:00:42,20
default source directive set to none, you block

14
00:00:42,20 --> 00:00:43,50
all potential requests

15
00:00:43,50 --> 00:00:46,70
except for those you specifically allow.

16
00:00:46,70 --> 00:00:50,60
Note that first site with a large number of associated files

17
00:00:50,60 --> 00:00:53,70
and assets coming from different origins, starting

18
00:00:53,70 --> 00:00:55,90
with such a restricted directive can then require

19
00:00:55,90 --> 00:00:59,40
significant work to specify all of those origins

20
00:00:59,40 --> 00:01:02,20
that should be allowed, but when security is paramount

21
00:01:02,20 --> 00:01:05,30
such as on a site dealing with financial services,

22
00:01:05,30 --> 00:01:08,50
this can be a crucial step in ensuring that you're leaving

23
00:01:08,50 --> 00:01:11,80
nothing allowed that you don't intend to allow.

24
00:01:11,80 --> 00:01:16,70
So let's try this out, for our project site, I'm going to back

25
00:01:16,70 --> 00:01:19,40
to go back to index.js for the front end,

26
00:01:19,40 --> 00:01:23,50
for the site folder, and I'm going to add a default source

27
00:01:23,50 --> 00:01:25,80
directive, I can put that anywhere but I'm going to put it

28
00:01:25,80 --> 00:01:28,90
first, so we'll say right inside those opening double

29
00:01:28,90 --> 00:01:37,50
quotes, default-src 'none, then a semi colon to separate

30
00:01:37,50 --> 00:01:42,10
that from the next directive, and so saving that file

31
00:01:42,10 --> 00:01:50,00
and I will go back and deploy ...

32
00:01:50,00 --> 00:01:54,80
and so we are disallowing all other assets

33
00:01:54,80 --> 00:01:57,50
that are not specifically allowed by our existing

34
00:01:57,50 --> 00:02:01,60
directives, so we have directive that applies to scripts

35
00:02:01,60 --> 00:02:05,50
and we have a directive that applies to nested contexts

36
00:02:05,50 --> 00:02:11,40
like i-frames and so if I reload, suddenly my site looks

37
00:02:11,40 --> 00:02:13,70
nothing like it used to, nothing like I want it to

38
00:02:13,70 --> 00:02:16,40
and I've got a whole bunch of errors left in the console

39
00:02:16,40 --> 00:02:20,60
so we've got style sheet issues, we've got in-line style

40
00:02:20,60 --> 00:02:25,40
issues, we've got images and so all of these things are

41
00:02:25,40 --> 00:02:29,20
loading from other origins and we haven't specifically

42
00:02:29,20 --> 00:02:31,00
mentioned them as allowed origins,

43
00:02:31,00 --> 00:02:34,10
so they're suddenly disallowed, so then I can go back

44
00:02:34,10 --> 00:02:39,50
into index.js and I can add these allowable origins

45
00:02:39,50 --> 00:02:42,80
for these other resource types.

46
00:02:42,80 --> 00:02:46,60
So in my response.set right before those closing double

47
00:02:46,60 --> 00:02:50,00
quotes I'm going to add a semicolon and we'll start with image

48
00:02:50,00 --> 00:02:54,10
source which is img just like the HTML element -src

49
00:02:54,10 --> 00:03:00,40
and that's going to be my Rokuapp.Com URL, so I'm just going to

50
00:03:00,40 --> 00:03:05,10
copy and paste that to avoid typing errors

51
00:03:05,10 --> 00:03:09,60
and the semicolon and then I need style-src for my style

52
00:03:09,60 --> 00:03:14,10
sheets and that's the same origin for me, semicolon and then

53
00:03:14,10 --> 00:03:17,70
finally font-src 'cause we've got font issues as well

54
00:03:17,70 --> 00:03:23,50
and that's also coming from my assets resources URL

55
00:03:23,50 --> 00:03:28,30
and so now i've specified allowable origins for images,

56
00:03:28,30 --> 00:03:36,80
styles, and fonts, so I will save that go back and redeploy

57
00:03:36,80 --> 00:03:41,20
and so now with allowable origins specified for all of these

58
00:03:41,20 --> 00:03:44,90
things that map to the URL, the specific sub-domain on

59
00:03:44,90 --> 00:03:47,80
Heroku that I'm using to serve all of those resource files,

60
00:03:47,80 --> 00:03:51,10
I should be getting rid of most all the errors

61
00:03:51,10 --> 00:03:55,40
and so back in the browser reloading, now I've got

62
00:03:55,40 --> 00:03:57,10
the images, now the page looks like it should,

63
00:03:57,10 --> 00:04:00,30
I've still got a few errors, we notice that these are about

64
00:04:00,30 --> 00:04:04,20
inline styles and so when were using a content security

65
00:04:04,20 --> 00:04:07,10
policy, inline styles are not considered safe,

66
00:04:07,10 --> 00:04:10,10
you actually have to specify that you want unsafe inline

67
00:04:10,10 --> 00:04:15,60
styles to use those and so to fix that final issue

68
00:04:15,60 --> 00:04:20,30
with the inline styles, we eventually probably want to

69
00:04:20,30 --> 00:04:23,60
weed those out but for now, just to get things working,

70
00:04:23,60 --> 00:04:27,30
we can go back to our content security policy and make one

71
00:04:27,30 --> 00:04:30,20
more change in the style source section,

72
00:04:30,20 --> 00:04:32,40
I'm going to add a space and then quotes, I'm going to type

73
00:04:32,40 --> 00:04:40,60
unsafe-inline and so this is a directive value that you

74
00:04:40,60 --> 00:04:43,20
are forced to type the word unsafe just so it's flagged

75
00:04:43,20 --> 00:04:44,80
for you, you know what's going on,

76
00:04:44,80 --> 00:04:46,50
so I'm going to save that,

77
00:04:46,50 --> 00:04:53,30
I'm going to deploy it one more time

78
00:04:53,30 --> 00:04:58,10
and so we have these inline styles in place, we probably

79
00:04:58,10 --> 00:05:00,50
want to take those out at some point but for now,

80
00:05:00,50 --> 00:05:04,10
we can specify that we recognize there are inline styles

81
00:05:04,10 --> 00:05:06,80
and we're okay with taking the risks

82
00:05:06,80 --> 00:05:11,10
and so then if I go and reload one more time,

83
00:05:11,10 --> 00:05:13,50
get everything loaded, I don't have any console errors

84
00:05:13,50 --> 00:05:17,00
and so we've got a content security policy that works

85
00:05:17,00 --> 00:05:20,00
for all the content that we need to use for this site.

