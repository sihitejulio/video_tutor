1
00:00:00,50 --> 00:00:02,40
- [Instructor] Although Content Security Policies

2
00:00:02,40 --> 00:00:05,10
are commonly defined in HTTP headers,

3
00:00:05,10 --> 00:00:09,00
it's also possible to create a Content Security Policy

4
00:00:09,00 --> 00:00:11,30
using an HTML <meta> tag.

5
00:00:11,30 --> 00:00:12,90
This approach can be useful

6
00:00:12,90 --> 00:00:14,70
when a one size fits all approach

7
00:00:14,70 --> 00:00:17,00
doesn't work for the pages on your site.

8
00:00:17,00 --> 00:00:21,20
To specify a Content Security Policy using a <meta> tag,

9
00:00:21,20 --> 00:00:22,90
you use two attributes.

10
00:00:22,90 --> 00:00:25,90
You include http-equiv with a value

11
00:00:25,90 --> 00:00:27,70
of Content Security Policy.

12
00:00:27,70 --> 00:00:31,00
You also use the content attribute with a value

13
00:00:31,00 --> 00:00:34,90
equal to your Content Security Policy.

14
00:00:34,90 --> 00:00:36,10
There are a couple differences

15
00:00:36,10 --> 00:00:38,20
between a <meta> Content Security Policy

16
00:00:38,20 --> 00:00:40,70
and a server-side Content Security Policy

17
00:00:40,70 --> 00:00:42,70
that are minor but important.

18
00:00:42,70 --> 00:00:44,70
First, there's a few directives

19
00:00:44,70 --> 00:00:46,20
that can't be used in a <meta> tag

20
00:00:46,20 --> 00:00:49,20
including frame-ancestors and sandbox.

21
00:00:49,20 --> 00:00:51,50
So if you want to use those, they have to be declared

22
00:00:51,50 --> 00:00:53,70
in an HTTP header.

23
00:00:53,70 --> 00:00:55,60
In addition, when you're using headers

24
00:00:55,60 --> 00:00:57,30
and <meta> elements together,

25
00:00:57,30 --> 00:00:58,70
the <meta> element can strengthen

26
00:00:58,70 --> 00:01:02,70
but not loosen the overall Content Security Policy.

27
00:01:02,70 --> 00:01:04,90
So you could load a Content Security Policy

28
00:01:04,90 --> 00:01:08,30
using a header, and then remove some allowed origins

29
00:01:08,30 --> 00:01:09,80
in a <meta> tag.

30
00:01:09,80 --> 00:01:13,10
But a <meta> tag can never add additional allowed origins.

31
00:01:13,10 --> 00:01:15,70
In practice, this means that when you're using

32
00:01:15,70 --> 00:01:17,60
the two methods in conjunction,

33
00:01:17,60 --> 00:01:21,10
the server needs to specify the least restrictive policy

34
00:01:21,10 --> 00:01:23,90
and then the <meta> elements should tighten this up

35
00:01:23,90 --> 00:01:25,40
where appropriate.

36
00:01:25,40 --> 00:01:27,40
Obviously, having a single policy

37
00:01:27,40 --> 00:01:30,20
coming from a single source is easier to manage.

38
00:01:30,20 --> 00:01:33,00
But some scenarios make use of both.

39
00:01:33,00 --> 00:01:34,50
So I'm going to first try

40
00:01:34,50 --> 00:01:37,20
totally switching out my Content Security Policy

41
00:01:37,20 --> 00:01:39,80
and moving it over to a <meta> element.

42
00:01:39,80 --> 00:01:44,10
So in my index.js file for my site,

43
00:01:44,10 --> 00:01:45,90
where I'm serving the HTML,

44
00:01:45,90 --> 00:01:50,20
I'm going to copy the value of my Content Security Policy.

45
00:01:50,20 --> 00:01:53,80
So I'm going to copy the string after the colon

46
00:01:53,80 --> 00:01:55,90
then I'm going to comment out

47
00:01:55,90 --> 00:01:59,20
actually just this entire response.set

48
00:01:59,20 --> 00:02:02,40
and I'm not going to add that HTTP header

49
00:02:02,40 --> 00:02:03,80
to the response at all.

50
00:02:03,80 --> 00:02:05,60
So I'll save those changes.

51
00:02:05,60 --> 00:02:08,80
And then switching over to the index.html file

52
00:02:08,80 --> 00:02:10,20
for this site.

53
00:02:10,20 --> 00:02:12,80
And at the top of the file there's a <meta charset

54
00:02:12,80 --> 00:02:14,70
and then I'm just going to add a new <meta> tag after that.

55
00:02:14,70 --> 00:02:26,60
So <meta http-equiv="Content-Security-Policy"

56
00:02:26,60 --> 00:02:29,30
and then content=

57
00:02:29,30 --> 00:02:32,40
and then I can just paste in that Content Security Policy

58
00:02:32,40 --> 00:02:37,40
that I had in originally in my Java script file.

59
00:02:37,40 --> 00:02:41,30
And then I just need to close that <meta> tag.

60
00:02:41,30 --> 00:02:44,60
And so now I've specified exactly what I had originally

61
00:02:44,60 --> 00:02:47,50
specified in my router, in my JS file.

62
00:02:47,50 --> 00:02:50,50
But I'm instead putting in the index.html file.

63
00:02:50,50 --> 00:02:53,00
So I'm going to save that.

64
00:02:53,00 --> 00:03:03,50
And got my terminal and I'm going to deploy.

65
00:03:03,50 --> 00:03:06,60
And so when the deploy is done

66
00:03:06,60 --> 00:03:09,80
I expect to see no changes in the browser.

67
00:03:09,80 --> 00:03:12,00
When I look in the console I shouldn't have any errors.

68
00:03:12,00 --> 00:03:14,40
Everything should still be loading.

69
00:03:14,40 --> 00:03:17,20
And so we'll start by just making sure that things work

70
00:03:17,20 --> 00:03:20,20
just the way we expect them to and just the way they did

71
00:03:20,20 --> 00:03:24,70
when we were using an HTTP header.

72
00:03:24,70 --> 00:03:29,10
I'm going to open my network tab and I'm going to reload my page.

73
00:03:29,10 --> 00:03:32,50
And noticing the console there are no errors.

74
00:03:32,50 --> 00:03:36,90
And if I look at the top of my network list,

75
00:03:36,90 --> 00:03:41,60
my request headers, I don't have a Content Security Policy.

76
00:03:41,60 --> 00:03:44,40
And so that just means that I don't have that HTTP header

77
00:03:44,40 --> 00:03:46,80
but I still have that <meta> element that's being parced.

78
00:03:46,80 --> 00:03:49,40
And so let's test that out a little bit.

79
00:03:49,40 --> 00:03:52,90
So moving back into the cope,

80
00:03:52,90 --> 00:03:56,30
I'm going to go into my index.js file,

81
00:03:56,30 --> 00:03:59,90
I'm going to uncomment that response.set statement.

82
00:03:59,90 --> 00:04:05,30
So I'm going to have the server set an HTTP header

83
00:04:05,30 --> 00:04:07,00
for Content Security Policy.

84
00:04:07,00 --> 00:04:12,30
That gives me permission for both my hp assets CDN

85
00:04:12,30 --> 00:04:15,90
and the Google APIs for my jQuery file.

86
00:04:15,90 --> 00:04:18,90
And then in my HTML file,

87
00:04:18,90 --> 00:04:21,30
I'm going to keep this Content Security Policy in place

88
00:04:21,30 --> 00:04:26,20
but I'm going to take out that Google APIs URL.

89
00:04:26,20 --> 00:04:29,50
And so here I'm making a more restrictive

90
00:04:29,50 --> 00:04:33,30
Content Security Policy in the <meta> tag

91
00:04:33,30 --> 00:04:35,80
than the one that's coming from HTTP.

92
00:04:35,80 --> 00:04:37,80
And so what we expect to see

93
00:04:37,80 --> 00:04:39,40
is that this one is enforced

94
00:04:39,40 --> 00:04:41,70
and that we actually lose the ability

95
00:04:41,70 --> 00:04:44,10
to load that jQuery file.

96
00:04:44,10 --> 00:04:47,40
So I've got my file saved.

97
00:04:47,40 --> 00:04:54,10
I'm going to deploy.

98
00:04:54,10 --> 00:04:56,90
And so this time when we reload

99
00:04:56,90 --> 00:05:00,50
we should expect to see an HTTP header in the network tab.

100
00:05:00,50 --> 00:05:02,30
If this is working as we expect,

101
00:05:02,30 --> 00:05:05,40
we're going to see some errors in console as well

102
00:05:05,40 --> 00:05:08,50
as the jQuery file won't be able to load

103
00:05:08,50 --> 00:05:13,00
from the Google CDN.

104
00:05:13,00 --> 00:05:17,10
So back in the browser I'm going to do a hard reload.

105
00:05:17,10 --> 00:05:19,10
And I'm going to go to the console

106
00:05:19,10 --> 00:05:22,70
and I can see that the Content Security Policy directive

107
00:05:22,70 --> 00:05:28,40
was violated by this reference to ajax.googleapis.com.

108
00:05:28,40 --> 00:05:29,70
And if I look in the network,

109
00:05:29,70 --> 00:05:33,90
I look at that first request for the HTML file,

110
00:05:33,90 --> 00:05:37,10
and we do have this very permissive Content Security Policy

111
00:05:37,10 --> 00:05:40,30
that includes ajax.googleapis.com.

112
00:05:40,30 --> 00:05:43,80
And so we got this error because our <meta> element

113
00:05:43,80 --> 00:05:47,80
restricts the Content Security Policy

114
00:05:47,80 --> 00:05:49,50
that we get from the server.

115
00:05:49,50 --> 00:05:52,00
And so by putting a more restrictive policy in place,

116
00:05:52,00 --> 00:05:56,50
we disallowed this Google APIs origin

117
00:05:56,50 --> 00:05:59,90
even though the server allowed it.

118
00:05:59,90 --> 00:06:01,80
Now we want that to work

119
00:06:01,80 --> 00:06:04,40
and we are okay with our server policy

120
00:06:04,40 --> 00:06:07,20
so I'm going to go ahead and just comment out

121
00:06:07,20 --> 00:06:09,60
this <meta> tag here so we still have it here

122
00:06:09,60 --> 00:06:12,80
but it's not interfering with our work going forward.

123
00:06:12,80 --> 00:06:19,00
Then I'll do one more deploy

124
00:06:19,00 --> 00:06:21,40
just to make sure that

125
00:06:21,40 --> 00:06:24,70
all of the assets are loading as we expect.

126
00:06:24,70 --> 00:06:27,50
And so we can keep that HTML code in the file

127
00:06:27,50 --> 00:06:30,00
just in case you want to go back and revisit it later on.

128
00:06:30,00 --> 00:06:32,60
But for now we'll just be relying going forward

129
00:06:32,60 --> 00:06:37,00
on the server HTTP heading.

130
00:06:37,00 --> 00:06:40,50
And so reloading one more time in the browser.

131
00:06:40,50 --> 00:06:43,00
Now we don't have those errors in the console

132
00:06:43,00 --> 00:06:45,00
so all of our assets are loading correctly.

