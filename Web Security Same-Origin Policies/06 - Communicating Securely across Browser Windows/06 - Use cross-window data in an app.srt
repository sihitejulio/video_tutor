1
00:00:00,04 --> 00:00:01,03
- [Instructor] When we implement

2
00:00:01,03 --> 00:00:03,07
cross-window messaging in an app

3
00:00:03,07 --> 00:00:05,01
there are a number of details

4
00:00:05,01 --> 00:00:06,09
that often need ironing out.

5
00:00:06,09 --> 00:00:10,06
So let's look at implementing this for a realistic use case.

6
00:00:10,06 --> 00:00:12,08
The product page that my app is opening

7
00:00:12,08 --> 00:00:17,01
has both an add to cart and a mark as favorite button.

8
00:00:17,01 --> 00:00:18,08
When a user clicks the favorite button,

9
00:00:18,08 --> 00:00:23,01
I want the product summary over in index.html

10
00:00:23,01 --> 00:00:25,03
to get a class of fave,

11
00:00:25,03 --> 00:00:28,03
which will add a yellow highlight to it.

12
00:00:28,03 --> 00:00:31,00
To do this I'm going to create an event listener

13
00:00:31,00 --> 00:00:37,00
for the favorite button that posts a message to the opener.

14
00:00:37,00 --> 00:00:41,04
So I'm going to select fave with jQuery,

15
00:00:41,04 --> 00:00:45,08
and then set up a click event listener.

16
00:00:45,08 --> 00:00:48,01
I'm going to capture the event object,

17
00:00:48,01 --> 00:00:53,02
and I'm going to prevent default.

18
00:00:53,02 --> 00:00:56,00
Now for my message content I want to send an object.

19
00:00:56,00 --> 00:01:03,09
So I'm going to send this to window.opener using postMessage,

20
00:01:03,09 --> 00:01:09,05
and its going to be an object so curly braces,

21
00:01:09,05 --> 00:01:12,01
and then I'm going to use data as my first key

22
00:01:12,01 --> 00:01:18,05
with a value of dollar this dot data prod,

23
00:01:18,05 --> 00:01:24,00
and so this is a data attribute in my HTML data-prod,

24
00:01:24,00 --> 00:01:25,07
and I'm getting the value of that

25
00:01:25,07 --> 00:01:28,03
and passing it back to the other page.

26
00:01:28,03 --> 00:01:29,03
And so this'll be flexible

27
00:01:29,03 --> 00:01:32,00
as we build this out for more product pages.

28
00:01:32,00 --> 00:01:34,08
We won't have to write code specific to each page.

29
00:01:34,08 --> 00:01:38,07
And then my other key is going to be request,

30
00:01:38,07 --> 00:01:42,06
and it's just going to be a string which is favorite,

31
00:01:42,06 --> 00:01:44,03
so I can use this in my listener

32
00:01:44,03 --> 00:01:46,07
to indicate that the user has favorited this item.

33
00:01:46,07 --> 00:01:51,00
And then after my object, comma,

34
00:01:51,00 --> 00:01:54,03
and then I need to specify the origin.

35
00:01:54,03 --> 00:01:55,09
So full protocol and domain,

36
00:01:55,09 --> 00:02:03,06
https://hanselandpetal.herokuapp.com,

37
00:02:03,06 --> 00:02:10,01
and that needs to be a string.

38
00:02:10,01 --> 00:02:12,05
And then in my message event listener,

39
00:02:12,05 --> 00:02:15,08
I need to add code to work with the message value.

40
00:02:15,08 --> 00:02:18,01
So first I'm going to add a return statement

41
00:02:18,01 --> 00:02:21,09
at the end of e.data equals ready.

42
00:02:21,09 --> 00:02:25,08
Because if that indeed is the value,

43
00:02:25,08 --> 00:02:27,02
it's not something I want to work with,

44
00:02:27,02 --> 00:02:30,02
and I want my code to just end right there.

45
00:02:30,02 --> 00:02:32,08
And I want my code to just return right there.

46
00:02:32,08 --> 00:02:35,04
And then after that I'm going to add another if,

47
00:02:35,04 --> 00:02:39,08
and I'm going to check if e.data, which is the message,

48
00:02:39,08 --> 00:02:43,00
.request, which is one of the keys

49
00:02:43,00 --> 00:02:45,08
that I created in the object I'm sending in my message,

50
00:02:45,08 --> 00:02:49,08
is equal to favorite.

51
00:02:49,08 --> 00:02:56,00
And if that's the case then I want to use the jQuery selector

52
00:02:56,00 --> 00:03:02,03
for the ID that corresponds to e.data.data,

53
00:03:02,03 --> 00:03:04,03
which is the data attribute

54
00:03:04,03 --> 00:03:07,05
that we sent as the first key in our object.

55
00:03:07,05 --> 00:03:10,09
And then to that element I'm going to add a class,

56
00:03:10,09 --> 00:03:14,02
and the class name is fave.

57
00:03:14,02 --> 00:03:18,00
And so I'm indicating that the element should be favorited,

58
00:03:18,00 --> 00:03:20,08
and then I'm using that information to add this class

59
00:03:20,08 --> 00:03:23,05
which will put the highlighting on the element

60
00:03:23,05 --> 00:03:25,02
that we favorited.

61
00:03:25,02 --> 00:03:28,02
So saving my work here.

62
00:03:28,02 --> 00:03:30,01
And so I'm going to,

63
00:03:30,01 --> 00:03:33,04
in the browser I'm going to do a Shift + Reload,

64
00:03:33,04 --> 00:03:37,07
I'm going to click those roses, open up the console.

65
00:03:37,07 --> 00:03:40,02
And so in this case we also need to do a hard reload,

66
00:03:40,02 --> 00:03:43,03
a Shift + Reload on the opened page as well.

67
00:03:43,03 --> 00:03:45,02
So I'm going to do a Shift + Reload here,

68
00:03:45,02 --> 00:03:46,08
to make sure that this page

69
00:03:46,08 --> 00:03:50,01
has loaded the newest version of the JavaScript.

70
00:03:50,01 --> 00:03:52,06
So then I'm going to click the MARK AS FAVORITE button.

71
00:03:52,06 --> 00:03:54,05
Let me go back to my original page,

72
00:03:54,05 --> 00:03:56,04
and there we go, we got that yellow background,

73
00:03:56,04 --> 00:03:58,01
we got the data logged

74
00:03:58,01 --> 00:03:59,09
showing that object that we sent over,

75
00:03:59,09 --> 00:04:02,06
and that got implemented by our view code

76
00:04:02,06 --> 00:04:04,00
to put a highlight around it.

