1
00:00:00,50 --> 00:00:01,80
- [Instructor] In addition to letting you work

2
00:00:01,80 --> 00:00:04,50
with data for any website you access,

3
00:00:04,50 --> 00:00:06,70
a browser is responsible for keeping the data

4
00:00:06,70 --> 00:00:10,30
in each window, tab, and frame isolated

5
00:00:10,30 --> 00:00:12,80
from other windows, tabs, and frames,

6
00:00:12,80 --> 00:00:16,10
collectively known as browsing contexts.

7
00:00:16,10 --> 00:00:18,90
However, in some cases, as a developer

8
00:00:18,90 --> 00:00:22,60
you need to exchange data between browsing contexts.

9
00:00:22,60 --> 00:00:25,30
Such as between an iframe and its parent,

10
00:00:25,30 --> 00:00:29,20
or between one tab and another tab that it spawned.

11
00:00:29,20 --> 00:00:32,50
To directly reference another open browsing context,

12
00:00:32,50 --> 00:00:36,40
you can use the methods of the Window API.

13
00:00:36,40 --> 00:00:39,30
For instance, if a site opens a selector

14
00:00:39,30 --> 00:00:42,30
in a separate window and implements the user's choice

15
00:00:42,30 --> 00:00:44,40
in the original window, the developer

16
00:00:44,40 --> 00:00:46,70
can simply send the selection information

17
00:00:46,70 --> 00:00:49,20
from the second window to the original one,

18
00:00:49,20 --> 00:00:52,50
sparing a round trip to the back end.

19
00:00:52,50 --> 00:00:56,10
The Window API allows you to send messages between windows

20
00:00:56,10 --> 00:00:59,30
and other browsing contexts, as long as you have a reference

21
00:00:59,30 --> 00:01:01,80
to the window you're sending a message to.

22
00:01:01,80 --> 00:01:03,60
You can easily get that reference

23
00:01:03,60 --> 00:01:06,80
if the current window spawned the other window,

24
00:01:06,80 --> 00:01:10,40
or if the current window was spawned by the other window.

25
00:01:10,40 --> 00:01:12,00
You can set up an event listener

26
00:01:12,00 --> 00:01:14,00
for messages from the other window,

27
00:01:14,00 --> 00:01:15,70
which then handles the message

28
00:01:15,70 --> 00:01:18,00
and works with whatever data was sent.

