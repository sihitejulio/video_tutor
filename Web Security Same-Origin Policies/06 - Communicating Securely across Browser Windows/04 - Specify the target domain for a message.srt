1
00:00:00,04 --> 00:00:01,07
- [Instructor] Whenever you send a message

2
00:00:01,07 --> 00:00:04,09
to another window using the postMessage method,

3
00:00:04,09 --> 00:00:06,08
you have to specify an origin.

4
00:00:06,08 --> 00:00:10,00
For testing, it can be useful to first get things working

5
00:00:10,00 --> 00:00:12,04
using the string asterisk

6
00:00:12,04 --> 00:00:15,00
which allows the target window to receive the message

7
00:00:15,00 --> 00:00:16,09
no matter what its origin.

8
00:00:16,09 --> 00:00:18,07
However, what if the target window

9
00:00:18,07 --> 00:00:21,08
has since been redirected to a malicious site?

10
00:00:21,08 --> 00:00:24,07
Just having a reference to this window does not guarantee

11
00:00:24,07 --> 00:00:27,07
where the content of your message will end up.

12
00:00:27,07 --> 00:00:29,08
To ensure that a message is not received

13
00:00:29,08 --> 00:00:31,04
by a redirected window,

14
00:00:31,04 --> 00:00:33,06
you should be as specific as possible

15
00:00:33,06 --> 00:00:35,08
in the second argument to postMessage,

16
00:00:35,08 --> 00:00:40,05
indicating the exact origin that should receive the message.

17
00:00:40,05 --> 00:00:43,05
So in my DOMContentLoaded event listener,

18
00:00:43,05 --> 00:00:47,02
which is for the pages that are opened by the main page,

19
00:00:47,02 --> 00:00:51,07
I currently use asterisk as the origin.

20
00:00:51,07 --> 00:00:53,03
Now I'm serving my main page

21
00:00:53,03 --> 00:00:56,09
from hanselandpetal.herokuapp.com,

22
00:00:56,09 --> 00:01:00,05
and my opened page I'm serving from a different origin,

23
00:01:00,05 --> 00:01:03,02
hpassets.herokuapp.com.

24
00:01:03,02 --> 00:01:05,09
So I'm going to start by specifying the wrong origin

25
00:01:05,09 --> 00:01:07,07
just to see what happens,

26
00:01:07,07 --> 00:01:10,04
and so here, I'm going to replace the asterisk

27
00:01:10,04 --> 00:01:18,01
with https://hpassets.herokuapp.com.

28
00:01:18,01 --> 00:01:21,01
So this is the origin of the window

29
00:01:21,01 --> 00:01:23,07
that will be sending the message,

30
00:01:23,07 --> 00:01:25,05
but it is not the origin of the window

31
00:01:25,05 --> 00:01:28,09
that'll be receiving it, and that's what matters here.

32
00:01:28,09 --> 00:01:31,06
And so I expect this to fail.

33
00:01:31,06 --> 00:01:46,04
So I'm going to save this, and I'm going to deploy.

34
00:01:46,04 --> 00:01:49,07
And so the code we just wrote,

35
00:01:49,07 --> 00:01:54,00
it's going to send a message from the new window

36
00:01:54,00 --> 00:01:56,00
to the original window.

37
00:01:56,00 --> 00:01:58,03
The browser is going to look at the origin

38
00:01:58,03 --> 00:01:59,08
specified in the message,

39
00:01:59,08 --> 00:02:02,02
and notice that it does not correspond

40
00:02:02,02 --> 00:02:05,09
to the origin of the window that it's being sent to,

41
00:02:05,09 --> 00:02:08,08
and it's going to refuse to deliver the message.

42
00:02:08,08 --> 00:02:12,09
And so again, doing a shift reload, a hard reload here,

43
00:02:12,09 --> 00:02:16,00
and then clicking on the roses,

44
00:02:16,00 --> 00:02:22,09
and so notice that no message was dispatched,

45
00:02:22,09 --> 00:02:26,07
and indeed, we have an error specifying

46
00:02:26,07 --> 00:02:30,02
that a message was sent with a target origin

47
00:02:30,02 --> 00:02:33,04
that does not match the recipient's origin.

48
00:02:33,04 --> 00:02:37,07
So to fix this, we need to go back to our code,

49
00:02:37,07 --> 00:02:40,01
and instead of hpassets.heroku.com,

50
00:02:40,01 --> 00:02:46,04
my target origin is actually hanselandpetal.herokuapp.com,

51
00:02:46,04 --> 00:02:48,00
and so again, this is the origin

52
00:02:48,00 --> 00:02:50,06
for the window that I'm expecting in the window

53
00:02:50,06 --> 00:02:52,08
that I'm sending the message to.

54
00:02:52,08 --> 00:02:56,09
And then we also want to replace the asterisk here

55
00:02:56,09 --> 00:02:59,00
in our message event listener,

56
00:02:59,00 --> 00:03:03,09
and so here, we are responding to a message of ready

57
00:03:03,09 --> 00:03:06,07
which is coming from the opened window

58
00:03:06,07 --> 00:03:08,01
back to the original window,

59
00:03:08,01 --> 00:03:10,00
and so that opened window should have

60
00:03:10,00 --> 00:03:20,00
a domain of https://hpassets.heroku,

61
00:03:20,00 --> 00:03:24,02
and so for my installation and for my test app,

62
00:03:24,02 --> 00:03:26,02
these are the domains that will work.

63
00:03:26,02 --> 00:03:29,00
So I'm going to save that.

64
00:03:29,00 --> 00:03:33,04
Going to one more time.

65
00:03:33,04 --> 00:03:36,03
Deploy my code.

66
00:03:36,03 --> 00:03:42,02
And so now if I go back and I do a hard reload,

67
00:03:42,02 --> 00:03:44,00
and then closing the window that spawned before,

68
00:03:44,00 --> 00:03:47,09
I'm going to click roses again to open that up.

69
00:03:47,09 --> 00:03:49,07
Now we have our response message,

70
00:03:49,07 --> 00:03:51,07
and we have our ready message,

71
00:03:51,07 --> 00:03:53,08
and this is the one that was originally blocked

72
00:03:53,08 --> 00:03:57,08
because we had the origin specified incorrectly.

73
00:03:57,08 --> 00:04:00,00
So now the way our code is written,

74
00:04:00,00 --> 00:04:02,03
the new pages can send messages

75
00:04:02,03 --> 00:04:05,06
only to windows with an origin

76
00:04:05,06 --> 00:04:08,02
of hanselandpetal.herokuapp.com,

77
00:04:08,02 --> 00:04:10,04
and again, that ensures that if this page

78
00:04:10,04 --> 00:04:12,00
has somehow been hijacked

79
00:04:12,00 --> 00:04:16,05
in between the time that it opened this second page

80
00:04:16,05 --> 00:04:18,02
and when the message is sent,

81
00:04:18,02 --> 00:04:20,04
that message, that data that we're sending over,

82
00:04:20,04 --> 00:04:28,00
won't actually get read into the original page.

