1
00:00:00,40 --> 00:00:01,90
- [Instructor] Just as you want to ensure

2
00:00:01,90 --> 00:00:04,30
that the Window you send a message to has not been hijacked

3
00:00:04,30 --> 00:00:05,60
has not been hijacked by content from another origin,

4
00:00:05,60 --> 00:00:07,50
by content from another origin,

5
00:00:07,50 --> 00:00:09,90
it's also a good practice to check the origin

6
00:00:09,90 --> 00:00:13,50
of messages you receive before working with them.

7
00:00:13,50 --> 00:00:16,30
You can do so by checking the origin property

8
00:00:16,30 --> 00:00:17,90
of the event object

9
00:00:17,90 --> 00:00:20,20
and returning if the origin isn't one your code needs

10
00:00:20,20 --> 00:00:21,60
isn't one your code needs to handle messages from.

11
00:00:21,60 --> 00:00:24,70
to handle messages from.

12
00:00:24,70 --> 00:00:25,00
Right now my code works with any data

13
00:00:25,00 --> 00:00:27,10
Right now, my code works with any data

14
00:00:27,10 --> 00:00:27,50
that comes in as the result of the message event.

15
00:00:27,50 --> 00:00:30,70
that comes in as the result of the message event.

16
00:00:30,70 --> 00:00:31,00
But instead, I want to check the origin

17
00:00:31,00 --> 00:00:33,20
But instead, I want to check the origin

18
00:00:33,20 --> 00:00:33,50
and work only with the origins hanselandpetal.herokuapp.com

19
00:00:33,50 --> 00:00:35,60
and work only with the origins

20
00:00:35,60 --> 00:00:37,80
hanselandpetal.herokuapp.com or hpassets.herokuapp.com.

21
00:00:37,80 --> 00:00:40,80
or hpassets.herokuapp.com.

22
00:00:40,80 --> 00:00:41,00
And a really common way to do this is to check event.origin

23
00:00:41,00 --> 00:00:43,50
And a really common way to do this is to check

24
00:00:43,50 --> 00:00:44,20
event.origin at the start of the event handler

25
00:00:44,20 --> 00:00:46,00
at the start of the event handler,

26
00:00:46,00 --> 00:00:46,30
and if it doesn't match, simply return.

27
00:00:46,30 --> 00:00:49,30
and if doesn't match, simply return.

28
00:00:49,30 --> 00:00:49,80
So even before my console.log statement,

29
00:00:49,80 --> 00:00:52,00
Even before my console.log statement,

30
00:00:52,00 --> 00:00:52,30
I'm going to set up an if.

31
00:00:52,30 --> 00:00:54,20
I'm going to set up an if

32
00:00:54,20 --> 00:00:54,50
And I'm going to check

33
00:00:54,50 --> 00:00:55,80
and I'm going to check for event.origin

34
00:00:55,80 --> 00:00:59,50
for event.origin not matching

35
00:00:59,50 --> 00:01:01,70
not matching

36
00:01:01,70 --> 00:01:02,00
https://hpassets.herokuapp.com

37
00:01:02,00 --> 00:01:08,30
https://hpassets.herokuapp.com

38
00:01:08,30 --> 00:01:08,60
and event.origin not matching

39
00:01:08,60 --> 00:01:12,50
and

40
00:01:12,50 --> 00:01:14,30
event.origin not matching

41
00:01:14,30 --> 00:01:15,80
https://hanselpetal.herokuapp.com.

42
00:01:15,80 --> 00:01:23,20
https://hanselandpetal.herokuapp.com.

43
00:01:23,20 --> 00:01:24,70
And if that's the case, I simply want to return.

44
00:01:24,70 --> 00:01:26,50
And if that's the case, I simply want to return.

45
00:01:26,50 --> 00:01:28,00
Looks like I put in an extra paren there.

46
00:01:28,00 --> 00:01:29,10
It looks like I put in an extra paren there

47
00:01:29,10 --> 00:01:30,50
Not good.

48
00:01:30,50 --> 00:01:32,90
And so if the origin on the message

49
00:01:32,90 --> 00:01:34,70
If the origin on the message that comes in

50
00:01:34,70 --> 00:01:37,70
that comes in does not match one of these two,

51
00:01:37,70 --> 00:01:38,00
does not match one of these two, I'm going to return it,

52
00:01:38,00 --> 00:01:39,50
I'm going to return it,

53
00:01:39,50 --> 00:01:40,90
and none of the rest of this actually gets executed.

54
00:01:40,90 --> 00:01:42,50
and none of the rest of this actually gets executed.

55
00:01:42,50 --> 00:01:44,00
But if it does match one of those two origins,

56
00:01:44,00 --> 00:01:45,50
But, if it does match one of those two origins,

57
00:01:45,50 --> 00:01:47,00
then we're going to console.log it.

58
00:01:47,00 --> 00:01:47,10
then we're going to console.log it

59
00:01:47,10 --> 00:01:48,50
And if it's the secondary window,

60
00:01:48,50 --> 00:01:48,60
go ahead and send back a reply.

61
00:01:48,60 --> 00:01:50,00
and if it's the secondary window,

62
00:01:50,00 --> 00:01:50,70
go ahead and send back a reply.

63
00:01:50,70 --> 00:01:52,20
So I'm going to save that,

64
00:01:52,20 --> 00:01:53,90
I'm going to save that

65
00:01:53,90 --> 00:01:55,20
and we're going to deploy.

66
00:01:55,20 --> 00:02:03,40
and we're going to deploy.

67
00:02:03,40 --> 00:02:05,00
And so what I'll expect to see now is

68
00:02:05,00 --> 00:02:06,40
What I'll expect to see now

69
00:02:06,40 --> 00:02:07,30
that my messaging should still work because I'm working.

70
00:02:07,30 --> 00:02:09,70
is that my messaging should still work

71
00:02:09,70 --> 00:02:09,90
All of the messages I'm sending are

72
00:02:09,90 --> 00:02:11,00
because all the messages I'm sending

73
00:02:11,00 --> 00:02:12,20
from one of those two domains.

74
00:02:12,20 --> 00:02:12,90
are from one of those two domains,

75
00:02:12,90 --> 00:02:14,00
But notice that we've had

76
00:02:14,00 --> 00:02:14,30
that extra sort of junk logging going on

77
00:02:14,30 --> 00:02:17,10
but notice that we've that extra sort of junk logging

78
00:02:17,10 --> 00:02:17,80
from the LinkedIn widget, and this should get rid of that.

79
00:02:17,80 --> 00:02:19,80
going on from the LinkedIn widget,

80
00:02:19,80 --> 00:02:19,90
and this should get rid of that.

81
00:02:19,90 --> 00:02:21,40
So let's take a look.

82
00:02:21,40 --> 00:02:21,80
Let's take a look.

83
00:02:21,80 --> 00:02:24,10
So there are those LinkedIn objects

84
00:02:24,10 --> 00:02:25,60
There are those LinkedIn objects

85
00:02:25,60 --> 00:02:27,00
that have been getting logged.

86
00:02:27,00 --> 00:02:27,20
that have been getting logged

87
00:02:27,20 --> 00:02:28,50
And LinkedIn is not in our approved senders list,

88
00:02:28,50 --> 00:02:31,60
and LinkedIn is not in our approved senders list

89
00:02:31,60 --> 00:02:33,00
and so those aren't showing up now.

90
00:02:33,00 --> 00:02:33,30
and so those aren't showing up now,

91
00:02:33,30 --> 00:02:34,30
That's a good sign.

92
00:02:34,30 --> 00:02:34,50
which is a good sign.

93
00:02:34,50 --> 00:02:35,90
And now clicking on the roses,

94
00:02:35,90 --> 00:02:36,40
And now clicking on the roses,

95
00:02:36,40 --> 00:02:37,70
I'm going to open up the console.

96
00:02:37,70 --> 00:02:38,40
I'm going to open up the console,

97
00:02:38,40 --> 00:02:39,70
And we've got the response from the main page.

98
00:02:39,70 --> 00:02:41,00
and we've got the response from the main page,

99
00:02:41,00 --> 00:02:42,40
We have the message from the opened page.

100
00:02:42,40 --> 00:02:43,60
we have the message from the opened page,

101
00:02:43,60 --> 00:02:45,00
And so our code is working with messages

102
00:02:45,00 --> 00:02:47,90
and so our code is working with messages

103
00:02:47,90 --> 00:02:49,20
from expected origins

104
00:02:49,20 --> 00:02:50,40
from expected origins

105
00:02:50,40 --> 00:02:51,70
and ignoring messages from everywhere else.

106
00:02:51,70 --> 00:02:56,00
and ignoring messages from everywhere else.

