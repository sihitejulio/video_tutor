1
00:00:00,40 --> 00:00:01,40
- [Instructor] To send a message

2
00:00:01,40 --> 00:00:04,20
to another browser window or tab,

3
00:00:04,20 --> 00:00:08,60
you use the postMessage method of the target window object.

4
00:00:08,60 --> 00:00:10,90
PostMessage takes two arguments,

5
00:00:10,90 --> 00:00:15,00
the message content and the target origin.

6
00:00:15,00 --> 00:00:17,80
To handle a message from another browsing context,

7
00:00:17,80 --> 00:00:20,20
you set up a listener for the message event

8
00:00:20,20 --> 00:00:23,00
on the window object, which fires whenever

9
00:00:23,00 --> 00:00:23,50
a message is successfully received

10
00:00:23,50 --> 00:00:24,90
is successfully received from another window, tab, or frame.

11
00:00:24,90 --> 00:00:28,60
from another window, tab, or frame.

12
00:00:28,60 --> 00:00:28,70
For the Hansel and Petal site,

13
00:00:28,70 --> 00:00:30,20
For the Hansel and Petal site,

14
00:00:30,20 --> 00:00:30,30
we're going to prototype a new feature

15
00:00:30,30 --> 00:00:31,90
we're going to prototype a new feature

16
00:00:31,90 --> 00:00:32,00
that launches product detail pages in a new window or tab,

17
00:00:32,00 --> 00:00:35,80
that launches product detail pages in a new window or tab.

18
00:00:35,80 --> 00:00:35,90
then on the product detail page,

19
00:00:35,90 --> 00:00:37,30
Then, on the product detail page,

20
00:00:37,30 --> 00:00:37,40
if a user clicks the Add to Favorites button,

21
00:00:37,40 --> 00:00:39,30
if a user clicks the Add to Favorites button,

22
00:00:39,30 --> 00:00:39,40
a message is sent back to the original page,

23
00:00:39,40 --> 00:00:42,00
a message is sent back to the original page,

24
00:00:42,00 --> 00:00:42,10
which then changes the display of the favorited item.

25
00:00:42,10 --> 00:00:44,80
which then changes the display of the favorited item

26
00:00:44,80 --> 00:00:44,90
I'm going to do this first with the heirloom roses item.

27
00:00:44,90 --> 00:00:47,60
and I'm going to do this first with the Heirloom Roses item.

28
00:00:47,60 --> 00:00:48,00
So, I'm going to start by just

29
00:00:48,00 --> 00:00:48,90
I'm going to start by just setting up

30
00:00:48,90 --> 00:00:49,30
setting up a test message between pages.

31
00:00:49,30 --> 00:00:51,40
a test message between pages.

32
00:00:51,40 --> 00:00:51,60
So, I'm in all.js for the resources server

33
00:00:51,60 --> 00:00:55,50
I'm in all.js for the resources server

34
00:00:55,50 --> 00:00:56,40
and at the end, just before the end of the iffy,

35
00:00:56,40 --> 00:00:58,40
and at the end, just before the end of the iffy,

36
00:00:58,40 --> 00:00:59,20
I'm going to add some code.

37
00:00:59,20 --> 00:00:59,70
I'm going to add some code.

38
00:00:59,70 --> 00:01:00,90
So, I'm going to start by adding a click event listener

39
00:01:00,90 --> 00:01:02,20
I'm going to start by adding a click event listener

40
00:01:02,20 --> 00:01:03,10
for the element on the main page,

41
00:01:03,10 --> 00:01:04,50
for the element on the main page,

42
00:01:04,50 --> 00:01:05,40
which has the ID value roses.

43
00:01:05,40 --> 00:01:08,90
which has the id value roses.

44
00:01:08,90 --> 00:01:11,70
I'm going to use jQuery here, but you can easily do this

45
00:01:11,70 --> 00:01:12,20
in Vanilla JavaScript as well.

46
00:01:12,20 --> 00:01:13,30
I'm going to use jQuery here,

47
00:01:13,30 --> 00:01:14,20
but you could easily do this in Vanilla JavaScript as well.

48
00:01:14,20 --> 00:01:16,40
So, I'm capturing the event

49
00:01:16,40 --> 00:01:20,60
and I'm going to start by preventing default

50
00:01:20,60 --> 00:01:20,80
and then, I want to use the window.open method

51
00:01:20,80 --> 00:01:23,10
I'm capturing the event and I'm going to start

52
00:01:23,10 --> 00:01:24,70
by preventing default.

53
00:01:24,70 --> 00:01:26,70
to open a URL that I specify in a new tab or window

54
00:01:26,70 --> 00:01:28,80
And then I want to use the window.open method

55
00:01:28,80 --> 00:01:30,80
and you can pull that URL out of the html file

56
00:01:30,80 --> 00:01:32,70
to open a URL that I specify in a new tab or window.

57
00:01:32,70 --> 00:01:37,10
out of index.html if you'd rather do that than type it.

58
00:01:37,10 --> 00:01:41,70
And so then, on the new page that opens,

59
00:01:41,70 --> 00:01:44,10
I want to send a message back to the original page

60
00:01:44,10 --> 00:01:46,00
after the DOM is loaded.

61
00:01:46,00 --> 00:01:48,10
So, I can create an event listener for that

62
00:01:48,10 --> 00:01:48,60
And you can pull that URL out of the html file

63
00:01:48,60 --> 00:01:51,50
and then, I'll add code to send a message.

64
00:01:51,50 --> 00:01:52,00
So, we'll do window.addEventListener

65
00:01:52,00 --> 00:01:55,90
out of index.html if you'd rather do that than type it.

66
00:01:55,90 --> 00:01:57,10
and we're going to listen for DOMContentLoaded.

67
00:01:57,10 --> 00:02:01,00
Then, on the new page that opens,

68
00:02:01,00 --> 00:02:03,30
Now, to send a message, I use the postMessage method

69
00:02:03,30 --> 00:02:03,40
of the target window to specify where I want to send it.

70
00:02:03,40 --> 00:02:05,70
after the DOM is loaded.

71
00:02:05,70 --> 00:02:06,40
I can create an event listener for that

72
00:02:06,40 --> 00:02:07,90
So, I'll start by checking if

73
00:02:07,90 --> 00:02:10,50
the current window has an opener.

74
00:02:10,50 --> 00:02:11,20
If it wasn't opened by another browsing context,

75
00:02:11,20 --> 00:02:13,10
We'll do window.addEventListener

76
00:02:13,10 --> 00:02:15,20
then this will be null.

77
00:02:15,20 --> 00:02:15,90
So, I'm going to check if window.opener and then,

78
00:02:15,90 --> 00:02:20,00
and we're going to listen for DOMContentLoaded.

79
00:02:20,00 --> 00:02:21,80
if I do have a window.opener,

80
00:02:21,80 --> 00:02:22,00
that actually gives me a reference

81
00:02:22,00 --> 00:02:24,50
Now, to send a message, I use the postMessage method

82
00:02:24,50 --> 00:02:24,80
of the target window to specify where I want to send it.

83
00:02:24,80 --> 00:02:27,00
to the window that opened the current window.

84
00:02:27,00 --> 00:02:27,90
So, I can use window.opener.postMessage

85
00:02:27,90 --> 00:02:31,70
I'll start by checking if the current window has an opener.

86
00:02:31,70 --> 00:02:32,60
If it wasn't opened by another browsing context,

87
00:02:32,60 --> 00:02:34,30
and then, the postMessage method takes two arguments.

88
00:02:34,30 --> 00:02:36,20
then this will be null.

89
00:02:36,20 --> 00:02:36,30
The first is the method itself,

90
00:02:36,30 --> 00:02:38,10
So I'm going to check if window.opener

91
00:02:38,10 --> 00:02:40,40
which is pretty straightforward.

92
00:02:40,40 --> 00:02:40,70
PostMessage accept a wide range of data,

93
00:02:40,70 --> 00:02:43,00
and then if I do have a window.opener,

94
00:02:43,00 --> 00:02:46,00
but I'm going to stick with a simple string for now, ready.

95
00:02:46,00 --> 00:02:46,50
to the window that opened the current window.

96
00:02:46,50 --> 00:02:47,80
And then, I also need to specify

97
00:02:47,80 --> 00:02:48,70
a second argument, which is the target origin

98
00:02:48,70 --> 00:02:50,00
I can use window.opener.postMessage

99
00:02:50,00 --> 00:02:51,90
and for now, because I'm just testing

100
00:02:51,90 --> 00:02:53,70
and I just want to get this working,

101
00:02:53,70 --> 00:02:53,80
I'm just going to use an asterisk as a string

102
00:02:53,80 --> 00:02:57,30
and then the postMessage method takes two arguments.

103
00:02:57,30 --> 00:02:58,10
The first is the method itself,

104
00:02:58,10 --> 00:02:59,30
and this means that I'm not asking the browser

105
00:02:59,30 --> 00:02:59,90
which is pretty straightforward.

106
00:02:59,90 --> 00:03:01,10
to check the origin of the target

107
00:03:01,10 --> 00:03:01,30
PostMessage accepts a wide range of data,

108
00:03:01,30 --> 00:03:02,50
before giving it my message.

109
00:03:02,50 --> 00:03:04,20
Any origin's fine here.

110
00:03:04,20 --> 00:03:04,70
but I'm going to stick to a simple string for now, ready.

111
00:03:04,70 --> 00:03:06,10
So finally, I need an event listener

112
00:03:06,10 --> 00:03:08,90
so that both windows are listening for message events.

113
00:03:08,90 --> 00:03:09,50
And then I also need to specify a second argument,

114
00:03:09,50 --> 00:03:11,00
So, that's going to be window.addEventListener

115
00:03:11,00 --> 00:03:12,70
which is that target origin.

116
00:03:12,70 --> 00:03:13,90
For now, 'cause I'm just testing

117
00:03:13,90 --> 00:03:14,20
and we're going to listen on the message event

118
00:03:14,20 --> 00:03:16,10
and I just want to get this working,

119
00:03:16,10 --> 00:03:18,10
I'm just going to use an asterisk as a string

120
00:03:18,10 --> 00:03:20,50
and then, for my function, I'm going to capture the event

121
00:03:20,50 --> 00:03:22,10
and this means I'm not asking the browser

122
00:03:22,10 --> 00:03:22,20
and then I'm just going to consol.log event.data,

123
00:03:22,20 --> 00:03:23,70
to check the origin of the target

124
00:03:23,70 --> 00:03:24,90
before giving it my message.

125
00:03:24,90 --> 00:03:25,90
Any origin's fine here.

126
00:03:25,90 --> 00:03:27,20
which is the value of the message.

127
00:03:27,20 --> 00:03:28,50
Finally, I need an event listener

128
00:03:28,50 --> 00:03:29,80
so that both windows are listening for message events.

129
00:03:29,80 --> 00:03:32,30
So, I'm going to save those changes and I'm going to deploy

130
00:03:32,30 --> 00:03:36,10
That's going to be window.addEventListener.

131
00:03:36,10 --> 00:03:40,50
We're going to listen on the message event

132
00:03:40,50 --> 00:03:43,00
and then for my function I'm going to capture the event

133
00:03:43,00 --> 00:03:44,50
and so, once this is running,

134
00:03:44,50 --> 00:03:45,00
and then I'm just going to console.log event.data,

135
00:03:45,00 --> 00:03:48,30
what we expect is that in the index.html page,

136
00:03:48,30 --> 00:03:49,20
which is the value of the message.

137
00:03:49,20 --> 00:03:52,60
when I click the rose item, it will spawn a new window.

138
00:03:52,60 --> 00:03:53,80
I'm going to save those changes and I'm going to deploy.

139
00:03:53,80 --> 00:03:55,80
When that new window finishes loading,

140
00:03:55,80 --> 00:03:58,10
it's going to send a message back to the original window

141
00:03:58,10 --> 00:04:04,00
and we should see that show up in the console.

142
00:04:04,00 --> 00:04:05,40
And so, I'm going to reload my page

143
00:04:05,40 --> 00:04:05,60
Once this is running, what we expect is that

144
00:04:05,60 --> 00:04:07,00
and since we're working with JavaScript,

145
00:04:07,00 --> 00:04:08,90
you may need to do a hard refresh here.

146
00:04:08,90 --> 00:04:09,50
in the index.html page, when I click the rose item,

147
00:04:09,50 --> 00:04:12,30
So, that is holding down the Shift button

148
00:04:12,30 --> 00:04:13,50
while you press the Reload button

149
00:04:13,50 --> 00:04:14,00
it will spawn a new window.

150
00:04:14,00 --> 00:04:15,60
or on a Mac, for instance, I can do Command Shift R.

151
00:04:15,60 --> 00:04:17,60
When that new window finishes loading,

152
00:04:17,60 --> 00:04:19,90
it's going to send a message back to the original window

153
00:04:19,90 --> 00:04:20,40
and we should see that show up in the console.

154
00:04:20,40 --> 00:04:22,30
And so, we have a couple things logged here

155
00:04:22,30 --> 00:04:26,40
because our LinkedIn widget is actually using messaging

156
00:04:26,40 --> 00:04:27,40
to communicate with a LinkedIn server,

157
00:04:27,40 --> 00:04:27,70
and since we're working with JavaScript,

158
00:04:27,70 --> 00:04:28,80
but we can ignore those.

159
00:04:28,80 --> 00:04:29,10
you may need to a hard refresh here.

160
00:04:29,10 --> 00:04:30,90
They're not part of what we're trying to do here.

161
00:04:30,90 --> 00:04:32,40
And so, here's the roses down here.

162
00:04:32,40 --> 00:04:34,10
I'm going to click that, opens a new window.

163
00:04:34,10 --> 00:04:35,20
while you press the reload button

164
00:04:35,20 --> 00:04:35,90
So, this is a browser setting.

165
00:04:35,90 --> 00:04:36,70
or on a Mac, for instance, I can do cmd+shift+r.

166
00:04:36,70 --> 00:04:39,80
You can't control whether your users get a window or a tab,

167
00:04:39,80 --> 00:04:42,20
but modern browsers default to a tab.

168
00:04:42,20 --> 00:04:42,70
So then, going back to the original page

169
00:04:42,70 --> 00:04:44,10
We have a couple things logged here

170
00:04:44,10 --> 00:04:44,80
because our LinkedIn widget is actually

171
00:04:44,80 --> 00:04:47,20
and in the console, there's a message from the new page.

172
00:04:47,20 --> 00:04:47,40
Ready.

173
00:04:47,40 --> 00:04:48,70
using messaging to communicate with LinkedIn server,

174
00:04:48,70 --> 00:04:49,50
So, we successfully sent a message

175
00:04:49,50 --> 00:04:50,60
but we can ignore those.

176
00:04:50,60 --> 00:04:50,90
from one browsing context, the new tab,

177
00:04:50,90 --> 00:04:52,90
They're not part of what we're trying to do here.

178
00:04:52,90 --> 00:04:53,50
Here's the roses down here, going to click that,

179
00:04:53,50 --> 00:04:57,70
to another, the original tab.

180
00:04:57,70 --> 00:04:58,60
This is a browser setting,

181
00:04:58,60 --> 00:05:01,70
you can't control whether your users get a window or tab,

182
00:05:01,70 --> 00:05:04,10
but modern browsers default to a tab.

183
00:05:04,10 --> 00:05:07,40
So then going back to the original page and in the console

184
00:05:07,40 --> 00:05:10,50
there's a message from the new page, ready.

185
00:05:10,50 --> 00:05:12,40
So we successfully sent a message

186
00:05:12,40 --> 00:05:15,30
from one browsing context, the new tab,

187
00:05:15,30 --> 00:05:18,00
to another, the original tab.

