1
00:00:00,50 --> 00:00:00,70
- [Instructor] So far in our app,

2
00:00:00,70 --> 00:00:01,80
- [Instructor] So far in our app,

3
00:00:01,80 --> 00:00:02,00
we have communication from the opened window

4
00:00:02,00 --> 00:00:04,00
we have communication from the opened window

5
00:00:04,00 --> 00:00:04,30
to the original window,

6
00:00:04,30 --> 00:00:05,60
to the original window,

7
00:00:05,60 --> 00:00:05,80
but sometimes we'll want to do it the other way as well.

8
00:00:05,80 --> 00:00:08,20
but sometimes we'll want to do it the other way as well.

9
00:00:08,20 --> 00:00:08,40
If something changes in the main page,

10
00:00:08,40 --> 00:00:10,40
If something changes in the main page,

11
00:00:10,40 --> 00:00:10,60
the page that was opened might need to integrate that.

12
00:00:10,60 --> 00:00:13,40
the page that was opened might need to integrate that.

13
00:00:13,40 --> 00:00:13,60
Now, window.open is not an asynchronous event,

14
00:00:13,60 --> 00:00:17,30
Now window.open is not an asynchronous event.

15
00:00:17,30 --> 00:00:17,50
so if we just put code after that,

16
00:00:17,50 --> 00:00:19,20
So if we just put code after that,

17
00:00:19,20 --> 00:00:19,40
it won't help because the message will be dispatched

18
00:00:19,40 --> 00:00:21,60
it won't help because the message will be dispatched

19
00:00:21,60 --> 00:00:21,90
before the page is finished loading,

20
00:00:21,90 --> 00:00:23,70
before the page has finished loading

21
00:00:23,70 --> 00:00:24,00
so the message won't be caught.

22
00:00:24,00 --> 00:00:25,80
so the message won't be caught.

23
00:00:25,80 --> 00:00:26,10
But we have an event handler set up already

24
00:00:26,10 --> 00:00:28,70
But we have an event handler set up already

25
00:00:28,70 --> 00:00:28,90
that has the opened window message the original window

26
00:00:28,90 --> 00:00:32,50
that has the opened window message the original window

27
00:00:32,50 --> 00:00:32,80
when it's done loading,

28
00:00:32,80 --> 00:00:34,40
when it's done loading.

29
00:00:34,40 --> 00:00:35,40
So we can use that as an indication

30
00:00:35,40 --> 00:00:36,80
so we can use that as an indication that it's safe

31
00:00:36,80 --> 00:00:38,50
that it's safe to message the opened window.

32
00:00:38,50 --> 00:00:39,40
to message the opened window.

33
00:00:39,40 --> 00:00:40,20
So thinking about this

34
00:00:40,20 --> 00:00:40,60
from the perspective of the original window,

35
00:00:40,60 --> 00:00:42,10
Thinking about this from the perspective

36
00:00:42,10 --> 00:00:42,70
of the original window,

37
00:00:42,70 --> 00:00:43,80
the receive message function will be called

38
00:00:43,80 --> 00:00:45,70
the receive message function will be called

39
00:00:45,70 --> 00:00:46,80
when the message comes in.

40
00:00:46,80 --> 00:00:47,50
when the message comes in

41
00:00:47,50 --> 00:00:48,50
And if the content of the message is ready,

42
00:00:48,50 --> 00:00:49,80
and if the content of the message is ready,

43
00:00:49,80 --> 00:00:50,90
we can go ahead and have the original page message back.

44
00:00:50,90 --> 00:00:53,30
we can go ahead and have the original page message back.

45
00:00:53,30 --> 00:00:55,70
So when we received a message,

46
00:00:55,70 --> 00:00:56,00
When we've received a message,

47
00:00:56,00 --> 00:00:56,80
I'm going to go ahead

48
00:00:56,80 --> 00:00:57,30
and leave that console.log statement in there,

49
00:00:57,30 --> 00:00:59,10
going to go ahead and leave that console.log

50
00:00:59,10 --> 00:00:59,40
statement in there,

51
00:00:59,40 --> 00:01:00,50
then we'll check if event.data is equal to ready

52
00:01:00,50 --> 00:01:03,90
then we'll check if event.data is equal to ready,

53
00:01:03,90 --> 00:01:05,00
which is the message that we've sent

54
00:01:05,00 --> 00:01:07,70
which is the message that we've sent

55
00:01:07,70 --> 00:01:08,80
from the new window when its DOM is loaded.

56
00:01:08,80 --> 00:01:11,60
from the new window when it's DOM is loaded.

57
00:01:11,60 --> 00:01:12,60
And then using event.source, which is the new page,

58
00:01:12,60 --> 00:01:16,80
And then, using event.source, which is the new page,

59
00:01:16,80 --> 00:01:17,80
we're going to postMessage using that as a destination

60
00:01:17,80 --> 00:01:20,40
we're going to postMessage using that as a destination

61
00:01:20,40 --> 00:01:21,50
and we'll do a string like

62
00:01:21,50 --> 00:01:22,60
and we'll do a string like

63
00:01:22,60 --> 00:01:23,70
Two-way communication established.

64
00:01:23,70 --> 00:01:27,10
Two-way communication established.

65
00:01:27,10 --> 00:01:28,20
And again, we need to specify an origin.

66
00:01:28,20 --> 00:01:29,90
And again, we need to specify an origin.

67
00:01:29,90 --> 00:01:30,90
For now, we're just going to use an asterisk

68
00:01:30,90 --> 00:01:32,00
For now, we're just going to use an asterisk

69
00:01:32,00 --> 00:01:33,10
to allow all origins.

70
00:01:33,10 --> 00:01:34,80
to allow all origins.

71
00:01:34,80 --> 00:01:35,90
And so saving this,

72
00:01:35,90 --> 00:01:37,60
And so, saving this, going to redeploy.

73
00:01:37,60 --> 00:01:48,00
I'm going to redeploy,

74
00:01:48,00 --> 00:01:50,50
and so what I expect now

75
00:01:50,50 --> 00:01:50,90
What I expect now is that

76
00:01:50,90 --> 00:01:52,50
is that when I click the rose image from the first page,

77
00:01:52,50 --> 00:01:53,70
when I click the rose image from the first page,

78
00:01:53,70 --> 00:01:54,50
it's going to open the second page.

79
00:01:54,50 --> 00:01:56,50
it's going to open the second page.

80
00:01:56,50 --> 00:01:57,30
And then if I go back to the first page,

81
00:01:57,30 --> 00:01:59,50
And then if I go back to the first page,

82
00:01:59,50 --> 00:02:00,40
after a second when that second page has finished loading,

83
00:02:00,40 --> 00:02:02,70
after a second, when that second page is finished loading,

84
00:02:02,70 --> 00:02:03,60
I should see a message on the first page

85
00:02:03,60 --> 00:02:05,00
I should see a message on the first page

86
00:02:05,00 --> 00:02:05,80
that's come from the second page.

87
00:02:05,80 --> 00:02:07,60
that's come from the second page.

88
00:02:07,60 --> 00:02:08,60
So let's check it out.

89
00:02:08,60 --> 00:02:09,70
Let's check it out.

90
00:02:09,70 --> 00:02:11,00
So I'm doing hard reload again using Shift.

91
00:02:11,00 --> 00:02:14,20
I'm doing a hard reload again using shift.

92
00:02:14,20 --> 00:02:15,00
I've got these messages I don't need to worry about

93
00:02:15,00 --> 00:02:16,30
I've got these messages I don't need to worry about

94
00:02:16,30 --> 00:02:17,10
because they're from our LinkedIn widget.

95
00:02:17,10 --> 00:02:18,30
'cause they're from our LinkedIn widget.

96
00:02:18,30 --> 00:02:19,30
So I'm going to click roses.

97
00:02:19,30 --> 00:02:20,00
I'm going to click roses, this opens up another page,

98
00:02:20,00 --> 00:02:22,00
This opens up another page.

99
00:02:22,00 --> 00:02:22,80
And if I open up the console here,

100
00:02:22,80 --> 00:02:25,40
and if I open up the console here,

101
00:02:25,40 --> 00:02:26,30
voila, we have a message from the first page

102
00:02:26,30 --> 00:02:28,40
voila, we have a message from the first page

103
00:02:28,40 --> 00:02:29,20
to the second page.

104
00:02:29,20 --> 00:02:30,40
to the second page.

105
00:02:30,40 --> 00:02:31,30
And going back to the first page,

106
00:02:31,30 --> 00:02:33,00
And going back to the first page,

107
00:02:33,00 --> 00:02:33,90
we have that ready message that we had before.

108
00:02:33,90 --> 00:02:35,60
we have that ready message that we had before.

109
00:02:35,60 --> 00:02:37,20
So the second page opened,

110
00:02:37,20 --> 00:02:37,90
The second page opened, sent a message

111
00:02:37,90 --> 00:02:39,40
sent a message to the first page saying ready,

112
00:02:39,40 --> 00:02:40,40
to the first page saying ready.

113
00:02:40,40 --> 00:02:41,20
when the first page received that ready message,

114
00:02:41,20 --> 00:02:43,30
When the first page received that ready message,

115
00:02:43,30 --> 00:02:44,10
it sent its own message back to the second page

116
00:02:44,10 --> 00:02:46,30
it sent its own message back to the second page

117
00:02:46,30 --> 00:02:47,10
saying two-way communication established.

118
00:02:47,10 --> 00:02:48,70
saying Two-way communication established.

119
00:02:48,70 --> 00:02:50,50
And so this is a way that we can reference

120
00:02:50,50 --> 00:02:51,90
This is a way that we can reference

121
00:02:51,90 --> 00:02:52,80
each of these pages in a relationship

122
00:02:52,80 --> 00:02:53,60
each of these pages in a relationship

123
00:02:53,60 --> 00:02:54,40
where one is opening the other

124
00:02:54,40 --> 00:02:55,50
where one is opening the other

125
00:02:55,50 --> 00:02:56,30
and exchange information between them.

126
00:02:56,30 --> 00:02:59,00
and exchange information between them.

