1
00:00:00,50 --> 00:00:03,20
- [Sasha] Thanks so much for joining me in this course.

2
00:00:03,20 --> 00:00:05,20
You now have a fuller understanding

3
00:00:05,20 --> 00:00:07,60
of what same-origin policies are

4
00:00:07,60 --> 00:00:09,90
and how they effect different resource.

5
00:00:09,90 --> 00:00:12,40
And you have experience working with and customizing

6
00:00:12,40 --> 00:00:14,50
aspects of these policies.

7
00:00:14,50 --> 00:00:18,20
To dig deeper into some more advanced JavaScript concepts

8
00:00:18,20 --> 00:00:20,90
check out courses on asynchronous JavaScript,

9
00:00:20,90 --> 00:00:23,20
prototypes, or closures.

10
00:00:23,20 --> 00:00:25,00
If you want to learn more about working

11
00:00:25,00 --> 00:00:29,50
with a backend in Node, explore courses on Express.js.

12
00:00:29,50 --> 00:00:32,00
Finally, if you want to dig into the tools available

13
00:00:32,00 --> 00:00:34,30
for inspecting code on the front end,

14
00:00:34,30 --> 00:00:37,50
check out courses on browser developer tools.

15
00:00:37,50 --> 00:00:39,70
Feel free to follow me online.

16
00:00:39,70 --> 00:00:42,50
Now take your new skills and build something amazing.

17
00:00:42,50 --> 00:00:43,00
Good luck.

