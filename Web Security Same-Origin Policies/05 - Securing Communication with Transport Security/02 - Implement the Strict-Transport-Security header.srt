1
00:00:00,50 --> 00:00:02,00
- [Instructor] To configure a web service

2
00:00:02,00 --> 00:00:04,50
to request Strict-Transport-Security,

3
00:00:04,50 --> 00:00:07,10
you specify the Strict-Transport-Security header

4
00:00:07,10 --> 00:00:09,40
for HTTP responses.

5
00:00:09,40 --> 00:00:12,00
At a minimum, the value for this header

6
00:00:12,00 --> 00:00:15,20
needs to specify a maximum age for the setting

7
00:00:15,20 --> 00:00:18,00
using the max-age directive.

8
00:00:18,00 --> 00:00:20,40
This value is specified in seconds,

9
00:00:20,40 --> 00:00:23,00
and it's recommended to set it to a longer time frame,

10
00:00:23,00 --> 00:00:25,30
such as a year, which we can approximate

11
00:00:25,30 --> 00:00:31,10
as 31,536,000 seconds.

12
00:00:31,10 --> 00:00:33,50
So back in the hansel & petal site,

13
00:00:33,50 --> 00:00:38,10
and I'm working with the index.js file for the router

14
00:00:38,10 --> 00:00:43,40
for the site section, the site server.

15
00:00:43,40 --> 00:00:48,00
And so, I've got response.set for my Content-Security-Policy

16
00:00:48,00 --> 00:00:56,60
and I want to add another response.set method.

17
00:00:56,60 --> 00:01:04,60
And I'm going to set Strict-Transport-Security.

18
00:01:04,60 --> 00:01:15,30
And the value's going to be max-age=31536000.

19
00:01:15,30 --> 00:01:17,90
And so, this is setting

20
00:01:17,90 --> 00:01:20,60
that Strict-Transport-Security header.

21
00:01:20,60 --> 00:01:22,60
I'm using the max-age directive,

22
00:01:22,60 --> 00:01:26,40
and I'm setting it to 31 million and some change seconds,

23
00:01:26,40 --> 00:01:28,70
which is about a year.

24
00:01:28,70 --> 00:01:32,10
So I'm going to save that change.

25
00:01:32,10 --> 00:01:36,20
And I'm going to go over and deploy.

26
00:01:36,20 --> 00:01:44,80
(keyboard clicking)

27
00:01:44,80 --> 00:01:49,40
And so, what we should expect to see when this is deployed

28
00:01:49,40 --> 00:01:52,60
is that in the browser, if we open the site with HTTP,

29
00:01:52,60 --> 00:01:55,80
we should see the header, but we should still be getting

30
00:01:55,80 --> 00:01:57,70
an insecure response.

31
00:01:57,70 --> 00:02:00,20
We should still be loading the page over HTTP.

32
00:02:00,20 --> 00:02:04,50
But if we load it with HTTPS, that should start the cycle

33
00:02:04,50 --> 00:02:09,10
where all data from that site comes in securely.

34
00:02:09,10 --> 00:02:12,60
So going back to the browser, I'm going to specifically

35
00:02:12,60 --> 00:02:16,50
enter the URL here, and I'm going to use HTTP

36
00:02:16,50 --> 00:02:21,10
and then my URL, so this is the URL for my front end.

37
00:02:21,10 --> 00:02:26,40
Oh, and I also want to open up the console.

38
00:02:26,40 --> 00:02:28,70
And I want to open up the Network tab

39
00:02:28,70 --> 00:02:33,90
so that we can capture that data as it's coming in.

40
00:02:33,90 --> 00:02:35,60
And so the browser's already telling me

41
00:02:35,60 --> 00:02:37,00
that I have not loaded this securely,

42
00:02:37,00 --> 00:02:38,70
and that's what I expect.

43
00:02:38,70 --> 00:02:42,10
And if I go and look at that first request,

44
00:02:42,10 --> 00:02:45,70
my response headers include Content-Security-Policy

45
00:02:45,70 --> 00:02:48,10
and Strict-Transport-Security, so I am getting

46
00:02:48,10 --> 00:02:50,90
that header back, but notice, the browser is not

47
00:02:50,90 --> 00:02:54,40
loading this securely, and that's because my initial request

48
00:02:54,40 --> 00:02:57,70
was specifically HTTP, and the browser's not going to trust

49
00:02:57,70 --> 00:03:00,20
a response from that request.

50
00:03:00,20 --> 00:03:02,40
So also notice here, we've got some errors,

51
00:03:02,40 --> 00:03:06,90
and those are just because we have HTTPS content

52
00:03:06,90 --> 00:03:12,70
specified in the HTML and in the CSS,

53
00:03:12,70 --> 00:03:16,60
and we are entering this area called mixed content

54
00:03:16,60 --> 00:03:19,50
because we're loading HTTP content

55
00:03:19,50 --> 00:03:21,60
and then trying to load other HTTPS,

56
00:03:21,60 --> 00:03:24,50
the same origin policies aren't working

57
00:03:24,50 --> 00:03:27,30
quite as we would want them to.

58
00:03:27,30 --> 00:03:30,00
But all that is fixed by loading everything securely,

59
00:03:30,00 --> 00:03:31,50
which is what we want anyway.

60
00:03:31,50 --> 00:03:33,70
So I'm going to go back to the URL

61
00:03:33,70 --> 00:03:38,40
and I'm going to put an HTTPS at the start,

62
00:03:38,40 --> 00:03:42,10
and we will see what's different.

63
00:03:42,10 --> 00:03:44,70
So we have a little lock in Chrome.

64
00:03:44,70 --> 00:03:47,20
We scroll up and look at our first request.

65
00:03:47,20 --> 00:03:48,70
We have all those same response headers,

66
00:03:48,70 --> 00:03:50,70
including Strict-Transport-Security.

67
00:03:50,70 --> 00:03:52,80
We don't have these errors anymore.

68
00:03:52,80 --> 00:03:54,80
So everything loaded correctly

69
00:03:54,80 --> 00:03:58,40
and we got a request over HTTPS.

70
00:03:58,40 --> 00:04:01,00
So now the browser knows that when it's loading

71
00:04:01,00 --> 00:04:04,20
from this domain, the server actually wants the browser

72
00:04:04,20 --> 00:04:06,70
to use HTTPS no matter what.

73
00:04:06,70 --> 00:04:09,50
So let's go back and what if we click the link

74
00:04:09,50 --> 00:04:15,10
or just entered an old URL using HTTP?

75
00:04:15,10 --> 00:04:22,60
And notice, the browser upgraded to HTTPS.

76
00:04:22,60 --> 00:04:24,30
There's an internal redirect,

77
00:04:24,30 --> 00:04:25,80
and it just went ahead and said,

78
00:04:25,80 --> 00:04:27,20
nope, I've got this on a list.

79
00:04:27,20 --> 00:04:29,00
I know this is supposed to be secure,

80
00:04:29,00 --> 00:04:31,80
and so the browser went ahead and made that initial request

81
00:04:31,80 --> 00:04:35,60
securely, because it knew that the site

82
00:04:35,60 --> 00:04:39,00
had set that header on a previous load.

