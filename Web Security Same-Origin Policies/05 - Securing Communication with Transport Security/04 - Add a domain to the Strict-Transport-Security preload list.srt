1
00:00:00,50 --> 00:00:02,10
- [Instructor] While the Strict Transport Security

2
00:00:02,10 --> 00:00:04,50
response header can be useful in ensuring

3
00:00:04,50 --> 00:00:07,30
that HTTPS is used for future requests

4
00:00:07,30 --> 00:00:10,50
to a resource, getting a browser to recognize

5
00:00:10,50 --> 00:00:13,50
that HTTPS is preferred for a given site

6
00:00:13,50 --> 00:00:15,70
can be a challenge to set up.

7
00:00:15,70 --> 00:00:17,00
To ease this challenge,

8
00:00:17,00 --> 00:00:18,50
Google has spearheaded an effort

9
00:00:18,50 --> 00:00:21,00
to maintain and distribute a list of sites

10
00:00:21,00 --> 00:00:24,50
that have affirmatively opted in to HSTS.

11
00:00:24,50 --> 00:00:26,60
Documentation and requirements,

12
00:00:26,60 --> 00:00:28,90
as well as a site's submission process,

13
00:00:28,90 --> 00:00:30,50
are available online.

14
00:00:30,50 --> 00:00:33,40
This list is hard-coded into the Chrome browser,

15
00:00:33,40 --> 00:00:35,40
and other browser makers have adopted it

16
00:00:35,40 --> 00:00:37,80
into their software as well.

17
00:00:37,80 --> 00:00:40,70
This means that when an initial request is made

18
00:00:40,70 --> 00:00:42,90
for a resource, the browser can check

19
00:00:42,90 --> 00:00:45,40
if the site is on the built in list.

20
00:00:45,40 --> 00:00:51,50
And if so, enforce HSTS on all requests to that web service.

21
00:00:51,50 --> 00:00:56,20
In addition to submitting a domain via hstspreload.org,

22
00:00:56,20 --> 00:00:59,10
servers also need to include the preload directive

23
00:00:59,10 --> 00:01:01,20
in the Strict Transport Security header

24
00:01:01,20 --> 00:01:02,90
to affirmatively indicate

25
00:01:02,90 --> 00:01:05,60
that the current domain is on the preload list

26
00:01:05,60 --> 00:01:08,80
and should continue to be part of that list.

27
00:01:08,80 --> 00:01:12,00
Using this directive doesn't automatically add a domain

28
00:01:12,00 --> 00:01:15,10
to the list, but if a domain is added to the list

29
00:01:15,10 --> 00:01:18,30
and doesn't include this directive in responses,

30
00:01:18,30 --> 00:01:20,20
the domain may eventually be removed

31
00:01:20,20 --> 00:01:23,10
from the list for noncompliance.

32
00:01:23,10 --> 00:01:25,30
Because the preload list is incorporated

33
00:01:25,30 --> 00:01:29,20
into browser software, it can take several weeks or months

34
00:01:29,20 --> 00:01:31,00
between being added to the list

35
00:01:31,00 --> 00:01:34,30
and being on the preload list in most browsers.

36
00:01:34,30 --> 00:01:37,30
For instance, if a new browser version is released,

37
00:01:37,30 --> 00:01:40,40
and then you add your domain to the list right afterwards,

38
00:01:40,40 --> 00:01:42,70
it could be several weeks or more

39
00:01:42,70 --> 00:01:45,20
until the next browser version comes out

40
00:01:45,20 --> 00:01:50,40
and incorporates the list containing your domain.

41
00:01:50,40 --> 00:01:52,20
In addition, adding a domain

42
00:01:52,20 --> 00:01:55,30
to the preload list is not easily undone,

43
00:01:55,30 --> 00:01:57,00
especially since older browsers

44
00:01:57,00 --> 00:01:58,70
containing non-current versions

45
00:01:58,70 --> 00:02:00,90
of the preload list can remain in use

46
00:02:00,90 --> 00:02:03,30
for a small number of users.

47
00:02:03,30 --> 00:02:05,80
For these reasons, it's important to be sure

48
00:02:05,80 --> 00:02:08,20
that your site's backend is configured

49
00:02:08,20 --> 00:02:11,40
to support HSTS for all requests.

50
00:02:11,40 --> 00:02:13,00
Until this is the case,

51
00:02:13,00 --> 00:02:15,40
you should avoid using the preload directive

52
00:02:15,40 --> 00:02:18,90
because once you turn it on it's not easy to go back.

53
00:02:18,90 --> 00:02:21,80
An incorrect or incomplete configuration

54
00:02:21,80 --> 00:02:24,80
can result in users being unable to access your site

55
00:02:24,80 --> 00:02:26,00
and its resources.

