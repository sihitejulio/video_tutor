1
00:00:00,50 --> 00:00:01,30
- [Instructor] By default,

2
00:00:01,30 --> 00:00:03,20
a strict transport security header

3
00:00:03,20 --> 00:00:06,70
applies only to the origin from which it is applied.

4
00:00:06,70 --> 00:00:09,50
However, because some data such as cookies

5
00:00:09,50 --> 00:00:12,70
can be accessed and manipulated across subdomains,

6
00:00:12,70 --> 00:00:17,70
it's important to apply HSTS to subdomains as well.

7
00:00:17,70 --> 00:00:21,30
To do so, you add the include subdomains directive

8
00:00:21,30 --> 00:00:23,90
in the value for the header.

9
00:00:23,90 --> 00:00:29,30
So in my index.js file for the front end,

10
00:00:29,30 --> 00:00:34,40
I want to add the include subdomains directive.

11
00:00:34,40 --> 00:00:37,40
So for my closing quote

12
00:00:37,40 --> 00:00:39,20
after the max age directive, in it's value,

13
00:00:39,20 --> 00:00:42,20
I'm just going to put a semicolon,

14
00:00:42,20 --> 00:00:45,50
and include subdomains.

15
00:00:45,50 --> 00:00:47,60
This is Camel Case.

16
00:00:47,60 --> 00:00:51,30
And I'm going to save that.

17
00:00:51,30 --> 00:01:03,50
I'm going to deploy.

18
00:01:03,50 --> 00:01:08,50
Now I'm working with a couple subdomains on Heroku,

19
00:01:08,50 --> 00:01:09,80
herokuapp.com,

20
00:01:09,80 --> 00:01:12,90
and so I'm not actually going to see any difference

21
00:01:12,90 --> 00:01:14,50
in the performance on my site here,

22
00:01:14,50 --> 00:01:16,40
because I'm not working with a whole domain,

23
00:01:16,40 --> 00:01:18,90
and checking how subdomains respond to that.

24
00:01:18,90 --> 00:01:21,40
But what we can do in the browser as we load,

25
00:01:21,40 --> 00:01:24,50
and verify that we are getting this new updated header.

26
00:01:24,50 --> 00:01:31,20
So I am going to reload my site with the network tab open,

27
00:01:31,20 --> 00:01:34,50
and then checking out that response,

28
00:01:34,50 --> 00:01:37,30
and we can see that for the strip transport security header,

29
00:01:37,30 --> 00:01:41,10
now we have that include subdomains directive as well.

30
00:01:41,10 --> 00:01:43,80
And so if I was working with subdomains,

31
00:01:43,80 --> 00:01:49,20
then any requests for resources from those subdomains

32
00:01:49,20 --> 00:01:54,50
would also be made via HTTPS once this HSTS

33
00:01:54,50 --> 00:01:57,00
has been established for this site in the browser.

