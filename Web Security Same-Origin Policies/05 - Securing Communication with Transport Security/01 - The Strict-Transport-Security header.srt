1
00:00:00,50 --> 00:00:04,00
- [Instructor] Part of a website's origin is the protocol.

2
00:00:04,00 --> 00:00:05,70
In the early days of the web,

3
00:00:05,70 --> 00:00:10,60
HTTP was the standard protocol used for all communications.

4
00:00:10,60 --> 00:00:14,00
Using HTTP request and response data

5
00:00:14,00 --> 00:00:16,50
is theoretically viewable by anyone,

6
00:00:16,50 --> 00:00:18,80
between the client and the server.

7
00:00:18,80 --> 00:00:21,10
Like the information on a postcard would be

8
00:00:21,10 --> 00:00:24,80
while en route from sender to recipient.

9
00:00:24,80 --> 00:00:26,20
When the need arose for more

10
00:00:26,20 --> 00:00:28,50
secure communication on the web,

11
00:00:28,50 --> 00:00:30,50
for requests and responses involving

12
00:00:30,50 --> 00:00:33,50
sensitive information like credit card numbers.

13
00:00:33,50 --> 00:00:38,40
A version of HTTP using encryption was implemented.

14
00:00:38,40 --> 00:00:42,10
Using the protocol HTTPS.

15
00:00:42,10 --> 00:00:45,20
With HTTPS requests and response

16
00:00:45,20 --> 00:00:48,30
date is encrypted before transmission,

17
00:00:48,30 --> 00:00:50,90
and decrypted after receipt.

18
00:00:50,90 --> 00:00:54,80
With ensures it can't be read or altered in transit.

19
00:00:54,80 --> 00:00:57,30
This is more like sending a letter through the mail,

20
00:00:57,30 --> 00:00:59,70
sealed in an envelope.

21
00:00:59,70 --> 00:01:04,60
HTTPS sets up a secure channel between client and server,

22
00:01:04,60 --> 00:01:06,80
allowing requests and responses

23
00:01:06,80 --> 00:01:09,00
to be encrypted before transmission.

24
00:01:09,00 --> 00:01:12,00
This prevents what's known as a man in the middle attack,

25
00:01:12,00 --> 00:01:15,00
in which a third party intercepts communications.

26
00:01:15,00 --> 00:01:17,70
And potentially changes them en route.

27
00:01:17,70 --> 00:01:21,80
Today this protocol is used for much of web traffic.

28
00:01:21,80 --> 00:01:23,90
Web services are increasingly

29
00:01:23,90 --> 00:01:27,00
switching to use HTTPS exclusively,

30
00:01:27,00 --> 00:01:29,30
to ensure that users web browsing

31
00:01:29,30 --> 00:01:31,60
can not be casually monitored.

32
00:01:31,60 --> 00:01:33,60
As part of the scheme known as

33
00:01:33,60 --> 00:01:38,10
HTTP Strict-Transport Security, or HSTS.

34
00:01:38,10 --> 00:01:41,30
A web service can request that browsers enforce

35
00:01:41,30 --> 00:01:46,30
the use of HTTPS when communicating

36
00:01:46,30 --> 00:01:50,10
To do this they send a strict-transport security header

37
00:01:50,10 --> 00:01:54,00
with a response to a client.

38
00:01:54,00 --> 00:01:55,80
When a serve using this header

39
00:01:55,80 --> 00:01:58,20
receives a request for a resource,

40
00:01:58,20 --> 00:02:00,00
it replies with this header.

41
00:02:00,00 --> 00:02:02,70
However, what the browser does with this information

42
00:02:02,70 --> 00:02:05,20
depends on a couple factors.

43
00:02:05,20 --> 00:02:07,00
If the users original request,

44
00:02:07,00 --> 00:02:09,80
the URL entered or the linked clicked

45
00:02:09,80 --> 00:02:12,90
specified HTTPS as the protocol.

46
00:02:12,90 --> 00:02:15,60
Then the browser logs the fact that this web service

47
00:02:15,60 --> 00:02:18,90
should use only HTTPS connections.

48
00:02:18,90 --> 00:02:21,00
And any future requests to this service

49
00:02:21,00 --> 00:02:24,70
will be sent as HTTPS requests by that browser.

50
00:02:24,70 --> 00:02:30,20
Even if the URL entered or the link clicked is only HTTP.

51
00:02:30,20 --> 00:02:33,90
However, if a users initial visit to the service

52
00:02:33,90 --> 00:02:36,60
involves entering a URL or clicking a link

53
00:02:36,60 --> 00:02:39,20
that uses the HTTP protocol,

54
00:02:39,20 --> 00:02:41,40
then the browser ignores the strict

55
00:02:41,40 --> 00:02:44,70
transport security directed in the response.

56
00:02:44,70 --> 00:02:46,80
The reason for this is that because

57
00:02:46,80 --> 00:02:49,40
the original request was not encrypted,

58
00:02:49,40 --> 00:02:51,20
the browser can't be sure that the

59
00:02:51,20 --> 00:02:55,00
information in the response was not tampered with en route.

60
00:02:55,00 --> 00:02:56,80
The reason for this is that because

61
00:02:56,80 --> 00:02:59,40
the original request was not encrypted,

62
00:02:59,40 --> 00:03:03,50
the browser can't rely on the information in the response.

63
00:03:03,50 --> 00:03:06,30
It's possible that a tampered request could redirect

64
00:03:06,30 --> 00:03:08,30
the browser to an imposter site,

65
00:03:08,30 --> 00:03:11,70
and the tampered result should not be set

66
00:03:11,70 --> 00:03:14,70
as the browser default for future requests.

67
00:03:14,70 --> 00:03:17,70
Instead the strict-transport security directive

68
00:03:17,70 --> 00:03:19,80
is honored only when a users initial

69
00:03:19,80 --> 00:03:24,30
request to the web service happens via HTTPS.

70
00:03:24,30 --> 00:03:26,10
At that point, the directive is

71
00:03:26,10 --> 00:03:29,00
applied to all future visits.

