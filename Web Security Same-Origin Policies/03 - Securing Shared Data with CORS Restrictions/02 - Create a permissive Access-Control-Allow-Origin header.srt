1
00:00:00,50 --> 00:00:02,40
- [Instructor] In some situations it makes sense

2
00:00:02,40 --> 00:00:05,20
to set an Access-Control-Allow-Origin header,

3
00:00:05,20 --> 00:00:08,90
to allow use of a resource in a document from any origin.

4
00:00:08,90 --> 00:00:11,60
For instance, a CDN provides access

5
00:00:11,60 --> 00:00:13,20
to a widely used resource,

6
00:00:13,20 --> 00:00:15,70
such as a library or open source font,

7
00:00:15,70 --> 00:00:19,00
has no need to restrict the origin.

8
00:00:19,00 --> 00:00:22,20
To ensure a resource is available to any origin,

9
00:00:22,20 --> 00:00:26,00
you specify a value of star or asterisk,

10
00:00:26,00 --> 00:00:29,00
for the Access-Control-Allow-Origin header.

11
00:00:29,00 --> 00:00:31,30
Browsers recognize this as allowing use

12
00:00:31,30 --> 00:00:34,90
of the resource within any domain.

13
00:00:34,90 --> 00:00:38,50
So I've opened my site in my browser,

14
00:00:38,50 --> 00:00:40,10
and opening up the console,

15
00:00:40,10 --> 00:00:41,50
in the developer tools,

16
00:00:41,50 --> 00:00:44,40
I noticed that I have a number of errors,

17
00:00:44,40 --> 00:00:46,20
the scary red ones.

18
00:00:46,20 --> 00:00:49,90
So all of these are course issues.

19
00:00:49,90 --> 00:00:52,70
And these are specific to fonts.

20
00:00:52,70 --> 00:00:56,50
Even if there's no access limitations specified,

21
00:00:56,50 --> 00:00:58,60
other types of files are generally fine,

22
00:00:58,60 --> 00:01:01,80
but browsers have a specific relationship with fonts

23
00:01:01,80 --> 00:01:04,60
where a header is specifically required

24
00:01:04,60 --> 00:01:07,80
to make them available on different origins.

25
00:01:07,80 --> 00:01:11,40
So we need to go into our file server for these assets

26
00:01:11,40 --> 00:01:15,50
and add a header so that these fonts can be used

27
00:01:15,50 --> 00:01:17,00
on our main site,

28
00:01:17,00 --> 00:01:18,50
which is on a different sub-domain

29
00:01:18,50 --> 00:01:20,20
and thus a different origin.

30
00:01:20,20 --> 00:01:24,10
And so I'm in the files for my resources server,

31
00:01:24,10 --> 00:01:26,80
just different from my site server

32
00:01:26,80 --> 00:01:29,10
and I'm in my index dot Js file.

33
00:01:29,10 --> 00:01:32,40
So in express, I can just do another app dot use statement

34
00:01:32,40 --> 00:01:36,00
to specify any HTTP headers that I want to include

35
00:01:36,00 --> 00:01:37,70
in my responses.

36
00:01:37,70 --> 00:01:42,10
So I'm going to do an App dot use, pass it a function,

37
00:01:42,10 --> 00:01:47,60
specify rec press the next.

38
00:01:47,60 --> 00:01:53,60
And so res for response dot set header and in quotes,

39
00:01:53,60 --> 00:02:00,30
that's access-control-allow-origin comma

40
00:02:00,30 --> 00:02:05,80
it mean I'm passing in as a value in quotes an asterisk.

41
00:02:05,80 --> 00:02:12,20
And then I need a return next.

42
00:02:12,20 --> 00:02:13,60
Let me finish up that semi colon.

43
00:02:13,60 --> 00:02:15,00
And so this is just going to specify

44
00:02:15,00 --> 00:02:18,30
that every response should include

45
00:02:18,30 --> 00:02:20,30
an Access-Control-Allow-Origin header

46
00:02:20,30 --> 00:02:22,10
with a value of asterisk.

47
00:02:22,10 --> 00:02:24,50
And that's going to basically say all the files

48
00:02:24,50 --> 00:02:26,70
that I'm serving from here, any requests,

49
00:02:26,70 --> 00:02:30,70
it's fine to use it on any origin.

50
00:02:30,70 --> 00:02:35,80
And so then I need to actually push

51
00:02:35,80 --> 00:02:38,30
my changes up to my server.

52
00:02:38,30 --> 00:02:48,60
(typing)

53
00:02:48,60 --> 00:02:50,70
And so I'm going to push that change,

54
00:02:50,70 --> 00:02:54,80
let Heroku rebuild my app,

55
00:02:54,80 --> 00:02:57,30
and then returning to my browser.

56
00:02:57,30 --> 00:03:03,10
I am going to reload and now the page is loaded

57
00:03:03,10 --> 00:03:06,60
and I don't have any errors over here in my console.

58
00:03:06,60 --> 00:03:10,80
So checking out the network and specifically

59
00:03:10,80 --> 00:03:13,40
we'll find a font in here, so here's one.

60
00:03:13,40 --> 00:03:17,90
Got a W-O-F-F and so I can see here in the response headers,

61
00:03:17,90 --> 00:03:19,70
I specifically have now,

62
00:03:19,70 --> 00:03:23,30
Access-Control-Allow-Origin with a value of asterisk.

63
00:03:23,30 --> 00:03:25,10
So that's coming in,

64
00:03:25,10 --> 00:03:28,30
in the response to my browser's HTTP request.

65
00:03:28,30 --> 00:03:32,80
And so that font is allowed by my browser.

66
00:03:32,80 --> 00:03:34,50
It's incorporated into the page

67
00:03:34,50 --> 00:03:37,00
and everything works without an error.

