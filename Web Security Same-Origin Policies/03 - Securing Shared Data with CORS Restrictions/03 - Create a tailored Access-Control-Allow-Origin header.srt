1
00:00:00,40 --> 00:00:02,00
- [Instructor] When you want to limit the origins

2
00:00:02,00 --> 00:00:04,80
that can request and use a resource,

3
00:00:04,80 --> 00:00:06,70
you can use more specific value

4
00:00:06,70 --> 00:00:09,70
for the access control allow origin header.

5
00:00:09,70 --> 00:00:11,70
This is useful when you're serving resources

6
00:00:11,70 --> 00:00:14,20
that you want to maintain tight control over

7
00:00:14,20 --> 00:00:16,70
such as API access or assets

8
00:00:16,70 --> 00:00:18,90
that have licensing restrictions.

9
00:00:18,90 --> 00:00:21,90
To create an access control allow origin policy

10
00:00:21,90 --> 00:00:25,10
that limits access, you can specify an origin

11
00:00:25,10 --> 00:00:27,60
that should have access to the resource.

12
00:00:27,60 --> 00:00:29,50
Browsers will reject the resource

13
00:00:29,50 --> 00:00:32,10
for any origin that requests access

14
00:00:32,10 --> 00:00:34,60
but does not match the value you set.

15
00:00:34,60 --> 00:00:37,30
Note that the access control allow origin header

16
00:00:37,30 --> 00:00:40,20
can take only one origin as its value.

17
00:00:40,20 --> 00:00:42,40
If your server needs to make content available

18
00:00:42,40 --> 00:00:44,90
to multiple origins, the common practice

19
00:00:44,90 --> 00:00:48,60
is to store a list of those domains such as in an array

20
00:00:48,60 --> 00:00:50,80
and then check the origin of the request

21
00:00:50,80 --> 00:00:54,20
against the array and if the origin indeed in the list,

22
00:00:54,20 --> 00:00:57,00
you go ahead and send an access control allow origin header

23
00:00:57,00 --> 00:01:01,90
in the response with the requesting domain as the value.

24
00:01:01,90 --> 00:01:04,50
It's also important to include the vary header

25
00:01:04,50 --> 00:01:06,90
with a value of origin when you use

26
00:01:06,90 --> 00:01:09,20
the access control allow origin header

27
00:01:09,20 --> 00:01:12,10
with any value other than an asterisk.

28
00:01:12,10 --> 00:01:14,40
This lets browsers know that the response

29
00:01:14,40 --> 00:01:17,00
is based on a value set in the coarse header

30
00:01:17,00 --> 00:01:19,60
ending with origin and thus shouldn't be

31
00:01:19,60 --> 00:01:21,50
automatically served from a cache

32
00:01:21,50 --> 00:01:25,80
without considering that value.

33
00:01:25,80 --> 00:01:28,70
The images, scripts, fonts, and style sheets

34
00:01:28,70 --> 00:01:30,40
for the Hansel and Petal website

35
00:01:30,40 --> 00:01:33,20
are specific to that site and I don't want to serve them

36
00:01:33,20 --> 00:01:35,80
anywhere else, however note that I'm serving

37
00:01:35,80 --> 00:01:40,40
those assets from the hpassets.herokuapp.com origin

38
00:01:40,40 --> 00:01:41,70
while the page requesting them

39
00:01:41,70 --> 00:01:44,90
has an origin at hanselandpetal.herokuapp.com

40
00:01:44,90 --> 00:01:47,40
and this is a pretty common architecture

41
00:01:47,40 --> 00:01:50,50
where your assets are coming from a different origin.

42
00:01:50,50 --> 00:01:53,70
So in order to ensure that I can use those assets,

43
00:01:53,70 --> 00:01:56,30
I need to specify the domain where I'm using them

44
00:01:56,30 --> 00:02:00,70
as the value for the access control allow origin header.

45
00:02:00,70 --> 00:02:04,40
So back in index.js for my Resources server,

46
00:02:04,40 --> 00:02:07,50
I want to change the value for access control allow origin,

47
00:02:07,50 --> 00:02:08,70
I don't want to use an asterisk,

48
00:02:08,70 --> 00:02:10,50
I want to be very specific here,

49
00:02:10,50 --> 00:02:12,40
and so the origin that I'm serving from

50
00:02:12,40 --> 00:02:20,20
is https://hanselandpetal.herokuapp.com

51
00:02:20,20 --> 00:02:21,80
Obviously yours is going to be different

52
00:02:21,80 --> 00:02:23,90
if you're actually using your own servers,

53
00:02:23,90 --> 00:02:25,80
you want to use the server name here

54
00:02:25,80 --> 00:02:29,00
where you are hosting and serving

55
00:02:29,00 --> 00:02:32,80
the actual front-end, the HTML.

56
00:02:32,80 --> 00:02:34,80
And then in addition to this header,

57
00:02:34,80 --> 00:02:36,30
I need to set another header.

58
00:02:36,30 --> 00:02:41,80
So I'm going to do another res.setHeader

59
00:02:41,80 --> 00:02:47,40
and this is going to use the header vary

60
00:02:47,40 --> 00:02:52,40
with a value of origin.

61
00:02:52,40 --> 00:02:54,90
And so again, it's important to include that vary header

62
00:02:54,90 --> 00:02:57,40
when you're using access control allow origin

63
00:02:57,40 --> 00:03:00,30
with a value other than asterisk

64
00:03:00,30 --> 00:03:01,80
so we're letting browsers know

65
00:03:01,80 --> 00:03:05,30
that access to this resource is based on

66
00:03:05,30 --> 00:03:07,40
the access control allow origin header

67
00:03:07,40 --> 00:03:09,60
in case they have it cached.

68
00:03:09,60 --> 00:03:15,60
And so saving those changes, going to go back to my terminal

69
00:03:15,60 --> 00:03:31,00
and I'm going to push everything up

70
00:03:31,00 --> 00:03:35,80
And then once my app is finished building on the server

71
00:03:35,80 --> 00:03:39,40
I can go and test it out.

72
00:03:39,40 --> 00:03:41,90
And so in my browser I'm going to

73
00:03:41,90 --> 00:03:43,70
open up the Developer tools

74
00:03:43,70 --> 00:03:45,50
and I'm going to switch over to the Network tab

75
00:03:45,50 --> 00:03:49,20
and then I'm going to reload the page.

76
00:03:49,20 --> 00:03:50,80
And then I'm just going to look at an asset

77
00:03:50,80 --> 00:03:55,80
so here we've got an image, I'm going to click on that request

78
00:03:55,80 --> 00:03:59,00
and so looking at my headers here,

79
00:03:59,00 --> 00:04:02,50
the response headers include access control allow origin

80
00:04:02,50 --> 00:04:06,10
and now it's specifying the origin

81
00:04:06,10 --> 00:04:09,60
that I added to that router,

82
00:04:09,60 --> 00:04:14,10
and we've also go the vary origin header down here

83
00:04:14,10 --> 00:04:17,80
so now these files are going to be served to this domain

84
00:04:17,80 --> 00:04:20,30
but any other domain that requests them

85
00:04:20,30 --> 00:04:23,00
will get a CORS error which is what we want.

