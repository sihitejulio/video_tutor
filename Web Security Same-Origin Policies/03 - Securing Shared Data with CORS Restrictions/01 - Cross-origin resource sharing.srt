1
00:00:00,40 --> 00:00:01,80
- [Narrator] When you're building websites

2
00:00:01,80 --> 00:00:04,40
that incorporate content from other domains,

3
00:00:04,40 --> 00:00:05,90
it's common to encounter errors

4
00:00:05,90 --> 00:00:08,70
in the console that refer to CORS.

5
00:00:08,70 --> 00:00:11,90
CORS stands for Cross-Origin Resource Sharing,

6
00:00:11,90 --> 00:00:14,40
which is the standardize approach on the web

7
00:00:14,40 --> 00:00:17,60
for deciding whether a site should have access to resources

8
00:00:17,60 --> 00:00:20,70
loaded from a different origin.

9
00:00:20,70 --> 00:00:23,50
CORS enables content hosts to specify

10
00:00:23,50 --> 00:00:27,40
which origins should have access to which content,

11
00:00:27,40 --> 00:00:30,70
as well as the methods that requests from those origins

12
00:00:30,70 --> 00:00:34,30
need to use in order to have access.

13
00:00:34,30 --> 00:00:37,40
CORS policies are commonly used with APIs

14
00:00:37,40 --> 00:00:39,70
as they allow these web services to restrict

15
00:00:39,70 --> 00:00:43,20
external origins, only to get requests for instance,

16
00:00:43,20 --> 00:00:47,10
and keep other types of HTTP requests like post and delete

17
00:00:47,10 --> 00:00:50,30
to the services own interface.

18
00:00:50,30 --> 00:00:53,90
CORS is also useful for hosts of resource files

19
00:00:53,90 --> 00:00:56,00
such as images and fonts.

20
00:00:56,00 --> 00:01:00,10
For instance, if your site hosts licensed fonts and art

21
00:01:00,10 --> 00:01:02,60
on your own content delivery network,

22
00:01:02,60 --> 00:01:05,40
it would be important to ensure that these resources

23
00:01:05,40 --> 00:01:08,50
could be used only by your own origins.

24
00:01:08,50 --> 00:01:11,40
Otherwise, anyone on the web could create a request

25
00:01:11,40 --> 00:01:14,80
for the files and incorporate them into their own sites,

26
00:01:14,80 --> 00:01:16,70
which would violate your licensing terms

27
00:01:16,70 --> 00:01:19,60
with the original provider.

28
00:01:19,60 --> 00:01:22,60
CORS encompasses several HTTP headers

29
00:01:22,60 --> 00:01:26,40
which allow you to specify aspects of request and usage

30
00:01:26,40 --> 00:01:30,40
including headers, methods, and credentials.

31
00:01:30,40 --> 00:01:32,50
Perhaps the most widely used header

32
00:01:32,50 --> 00:01:35,20
and maybe the most misunderstood as well,

33
00:01:35,20 --> 00:01:38,00
is access-control-allow-origin,

34
00:01:38,00 --> 00:01:41,10
which enables a server to specify which origins

35
00:01:41,10 --> 00:01:45,20
or resource may be incorporated into.

36
00:01:45,20 --> 00:01:48,70
Although each HTTP request includes information

37
00:01:48,70 --> 00:01:52,40
on it's origin, the access-control-allow-origin header

38
00:01:52,40 --> 00:01:55,40
is enforced by the browser after a resource has

39
00:01:55,40 --> 00:01:57,70
been retrieved from a third party server.

40
00:01:57,70 --> 00:02:00,50
If the header exists and does not provide access

41
00:02:00,50 --> 00:02:02,80
to the origin of the current document,

42
00:02:02,80 --> 00:02:05,30
then the browser refuses to render the resource

43
00:02:05,30 --> 00:02:08,40
and logs an error to the console.

44
00:02:08,40 --> 00:02:10,90
Browser extensions are available that inject

45
00:02:10,90 --> 00:02:13,20
an access-control-allow-origin header

46
00:02:13,20 --> 00:02:16,40
into all incoming HTTP responses.

47
00:02:16,40 --> 00:02:18,20
These extensions are sometimes used

48
00:02:18,20 --> 00:02:20,70
during development by front end developers.

49
00:02:20,70 --> 00:02:24,40
However, such extensions open a significant security hole,

50
00:02:24,40 --> 00:02:27,40
and you should avoid them, instead relying on

51
00:02:27,40 --> 00:02:32,20
appropriate server configuration during development.

52
00:02:32,20 --> 00:02:34,70
In addition to protecting hosted resources

53
00:02:34,70 --> 00:02:38,00
and account access, CORS also serves an important

54
00:02:38,00 --> 00:02:41,80
security role by preventing resources from a sensitive site,

55
00:02:41,80 --> 00:02:46,60
such as a bank, from being loaded by another website.

56
00:02:46,60 --> 00:02:49,20
This blocks attempts at cross-site scripting

57
00:02:49,20 --> 00:02:51,70
and cross-site requests forgery attacks

58
00:02:51,70 --> 00:02:54,40
that attempt to simulate the appearance and interface

59
00:02:54,40 --> 00:02:57,20
of the target site by loading its assets from a different

60
00:02:57,20 --> 00:03:00,10
origin and then tricking unsuspecting users

61
00:03:00,10 --> 00:03:03,10
into providing their login credentials to the third party,

62
00:03:03,10 --> 00:03:05,00
which can them use them to perform

63
00:03:05,00 --> 00:03:08,50
unauthorized HTTP requests that modify the account,

64
00:03:08,50 --> 00:03:11,40
such as transferring money from a user's bank account.

65
00:03:11,40 --> 00:03:15,40
Installing an access-control-allow-origin browser extension

66
00:03:15,40 --> 00:03:17,40
removes an important piece of protection

67
00:03:17,40 --> 00:03:19,00
against attacks like this.

