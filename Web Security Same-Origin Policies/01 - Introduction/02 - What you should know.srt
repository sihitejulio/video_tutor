1
00:00:00,50 --> 00:00:02,90
- [Narrator] This course assumes you have some experience

2
00:00:02,90 --> 00:00:06,30
coding in front-end JavaScript and in Express,

3
00:00:06,30 --> 00:00:07,90
as well as basic familiarity

4
00:00:07,90 --> 00:00:11,00
with HTTP requests and responses.

5
00:00:11,00 --> 00:00:13,20
If you don't have experience with JavaScript,

6
00:00:13,20 --> 00:00:17,10
a basic JavaScript course would be a great place to start.

7
00:00:17,10 --> 00:00:20,20
To learn how to build server side apps with Express,

8
00:00:20,20 --> 00:00:23,30
check out a course on Express.js.

9
00:00:23,30 --> 00:00:25,50
For the basics on how data is exchanged

10
00:00:25,50 --> 00:00:27,50
between clients and servers,

11
00:00:27,50 --> 00:00:30,00
explore a course on HTTP.

