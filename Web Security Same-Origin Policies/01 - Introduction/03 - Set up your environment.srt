1
00:00:00,40 --> 00:00:02,30
- [Instructor] To work along with me on the files

2
00:00:02,30 --> 00:00:05,10
for this course, you need two applications.

3
00:00:05,10 --> 00:00:08,80
A web browser and a code editor.

4
00:00:08,80 --> 00:00:11,40
You undoubtedly already have a web browser installed

5
00:00:11,40 --> 00:00:15,20
on your machine and any major modern browser, Chrome,

6
00:00:15,20 --> 00:00:19,40
Firefox or Microsoft Edge is fine for this course.

7
00:00:19,40 --> 00:00:21,30
I'll be using Chrome in these videos,

8
00:00:21,30 --> 00:00:23,70
which is a popular choice among web developers

9
00:00:23,70 --> 00:00:26,40
because of the extensive and powerful developer tools

10
00:00:26,40 --> 00:00:28,30
it has built in.

11
00:00:28,30 --> 00:00:31,80
Note that if you're on a Mac and you want to use Safari,

12
00:00:31,80 --> 00:00:35,60
you need to go to preferences

13
00:00:35,60 --> 00:00:38,00
and select the advanced tab

14
00:00:38,00 --> 00:00:42,30
and then click this box, show develop menu in menu bar

15
00:00:42,30 --> 00:00:45,10
in order to be able to access developer tools

16
00:00:45,10 --> 00:00:48,70
using the menu.

17
00:00:48,70 --> 00:00:51,30
A number of great code editors are available,

18
00:00:51,30 --> 00:00:53,50
both free and paid apps.

19
00:00:53,50 --> 00:00:56,60
Any editor that lets you edit and save plain text

20
00:00:56,60 --> 00:00:58,10
is fine for this course.

21
00:00:58,10 --> 00:01:00,20
So if you have a code editor you like,

22
00:01:00,20 --> 00:01:03,90
such as Sublime Text or Atom, it's fine to use it.

23
00:01:03,90 --> 00:01:06,60
I use Visual Studio Code in these videos,

24
00:01:06,60 --> 00:01:09,10
which is a version of Microsoft Visual Studio

25
00:01:09,10 --> 00:01:11,60
created specifically for web development.

26
00:01:11,60 --> 00:01:13,30
Visual Studio Code is free.

27
00:01:13,30 --> 00:01:16,60
It has Windows, Mac and Linux releases.

28
00:01:16,60 --> 00:01:19,10
The code is available on GitHub and users

29
00:01:19,10 --> 00:01:21,30
can submit issues there as well.

30
00:01:21,30 --> 00:01:23,00
Now let's get started.

