1
00:00:00,80 --> 00:00:02,30
- [Instructor] Same-origin policies

2
00:00:02,30 --> 00:00:04,40
touch almost every part of data flow

3
00:00:04,40 --> 00:00:06,70
in modern web applications.

4
00:00:06,70 --> 00:00:09,00
But you're not alone if you're not totally clear

5
00:00:09,00 --> 00:00:11,40
on the reasons these policies are in place,

6
00:00:11,40 --> 00:00:14,20
or how to customize them without compromising

7
00:00:14,20 --> 00:00:16,50
your user security.

8
00:00:16,50 --> 00:00:19,20
Setting headers on a server and seeing the result

9
00:00:19,20 --> 00:00:22,30
in HTTP requests and responses in the browser

10
00:00:22,30 --> 00:00:24,00
can help you get a clearer idea

11
00:00:24,00 --> 00:00:25,70
of what each same-origin policy

12
00:00:25,70 --> 00:00:28,20
is meant to do and how it works.

13
00:00:28,20 --> 00:00:30,80
And working with client side data in code

14
00:00:30,80 --> 00:00:32,80
and in browser developer tools,

15
00:00:32,80 --> 00:00:34,30
can help you build your skills

16
00:00:34,30 --> 00:00:38,00
at making same origin policies work for you and your apps,

17
00:00:38,00 --> 00:00:40,00
while keeping your site secure.

18
00:00:40,00 --> 00:00:41,90
In my LinkedIn learning course,

19
00:00:41,90 --> 00:00:44,70
I customize settings for same origin policies,

20
00:00:44,70 --> 00:00:48,90
to optimize secure communication among origins.

21
00:00:48,90 --> 00:00:51,80
I'm Sasha Vodnik, and I've been writing JavaScript

22
00:00:51,80 --> 00:00:54,30
since the browser wars of the 90's.

23
00:00:54,30 --> 00:00:57,30
If you feel fuzzy on how cross origin communication

24
00:00:57,30 --> 00:00:59,20
is kept secure in the browser,

25
00:00:59,20 --> 00:01:01,00
I invite you to join me on this course

26
00:01:01,00 --> 00:01:02,90
on same origin policies.

27
00:01:02,90 --> 00:01:04,00
Let's get started.

