1
00:00:00,50 --> 00:00:02,90
- [Lecturer] The same-origin policy comes into play

2
00:00:02,90 --> 00:00:05,90
when resources are loaded from different origins.

3
00:00:05,90 --> 00:00:08,70
So to work along with the videos in this course

4
00:00:08,70 --> 00:00:10,80
you'll need to deploy the exercise files

5
00:00:10,80 --> 00:00:13,90
to two different web servers with node environments.

6
00:00:13,90 --> 00:00:16,40
Ideally at two different domains,

7
00:00:16,40 --> 00:00:18,90
though you can complete almost everything

8
00:00:18,90 --> 00:00:22,00
using two subdomains of the same domain.

9
00:00:22,00 --> 00:00:25,30
Because some same-origin policies treat local host

10
00:00:25,30 --> 00:00:28,50
as a special case, you won't always get the same results

11
00:00:28,50 --> 00:00:31,60
if you simply run two web servers locally.

12
00:00:31,60 --> 00:00:34,80
However, if you don't have access to remote servers,

13
00:00:34,80 --> 00:00:36,20
you'll still be able to accomplish

14
00:00:36,20 --> 00:00:38,00
some of the tasks in this course

15
00:00:38,00 --> 00:00:41,30
using a local HTTP server.

16
00:00:41,30 --> 00:00:44,90
The exercise files contain folders for each chapter

17
00:00:44,90 --> 00:00:47,50
as well as a servers folder.

18
00:00:47,50 --> 00:00:50,70
The servers folder contains a resources folder

19
00:00:50,70 --> 00:00:53,00
and a site folder.

20
00:00:53,00 --> 00:00:57,50
Site serves the front-end HTML files

21
00:00:57,50 --> 00:01:00,30
while resources is a sort of CDN,

22
00:01:00,30 --> 00:01:03,80
hosting assets like images and fonts.

23
00:01:03,80 --> 00:01:05,50
Because many of the files in the servers

24
00:01:05,50 --> 00:01:07,10
contain links to each other,

25
00:01:07,10 --> 00:01:10,50
you'll need to customize all domain references in files

26
00:01:10,50 --> 00:01:14,60
on both servers to use the domain names of your servers.

27
00:01:14,60 --> 00:01:18,80
In the exercise files all the URLs include placeholders

28
00:01:18,80 --> 00:01:21,40
for the domain names to make it easy for you

29
00:01:21,40 --> 00:01:23,40
to globally search and replace,

30
00:01:23,40 --> 00:01:26,30
which you can do pretty easily in any code editor.

31
00:01:26,30 --> 00:01:27,80
So here we can see this reference

32
00:01:27,80 --> 00:01:30,00
which is just the word RESOURCES all caps,

33
00:01:30,00 --> 00:01:33,30
surrounded by two sets of square brackets

34
00:01:33,30 --> 00:01:35,10
and that you'll replace with the domain name

35
00:01:35,10 --> 00:01:40,40
of your resources server and likewise for the site server.

36
00:01:40,40 --> 00:01:43,30
So for instance, if I'm using the domain name example.com

37
00:01:43,30 --> 00:01:44,90
for the site server

38
00:01:44,90 --> 00:01:45,70
and

39
00:01:45,70 --> 00:01:50,70
794147-1.youcanlearnit.net for the resources server

40
00:01:50,70 --> 00:01:54,10
in Visual Studio Code I'll open the search box

41
00:01:54,10 --> 00:01:56,00
and I'll search on square bracket,

42
00:01:56,00 --> 00:01:58,50
square bracket, all caps SITE

43
00:01:58,50 --> 00:02:01,00
close those square brackets.

44
00:02:01,00 --> 00:02:05,50
And then I'll replace all instances with example.com,

45
00:02:05,50 --> 00:02:06,70
press Enter

46
00:02:06,70 --> 00:02:09,70
and we find 23 results.

47
00:02:09,70 --> 00:02:12,20
And so Visual Studio Code has this Replace All button

48
00:02:12,20 --> 00:02:14,20
and just click that.

49
00:02:14,20 --> 00:02:16,20
Confirm.

50
00:02:16,20 --> 00:02:18,10
And everything's replaced.

51
00:02:18,10 --> 00:02:20,20
And likewise,

52
00:02:20,20 --> 00:02:22,00
I can search for

53
00:02:22,00 --> 00:02:24,20
RESOURCES

54
00:02:24,20 --> 00:02:27,70
all caps in two sets of square brackets.

55
00:02:27,70 --> 00:02:33,80
And for this one I've got a more complicated domain name.

56
00:02:33,80 --> 00:02:36,40
794147 dash 1

57
00:02:36,40 --> 00:02:38,70
dot youcanlearnit

58
00:02:38,70 --> 00:02:39,50
dot net,

59
00:02:39,50 --> 00:02:42,40
there's an N in there.

60
00:02:42,40 --> 00:02:46,80
And then if I press Enter it finds a whole bunch of results

61
00:02:46,80 --> 00:02:50,30
and then I click the Replace All button,

62
00:02:50,30 --> 00:02:52,20
confirm that I want to replace all

63
00:02:52,20 --> 00:02:57,10
and now we can see that that domain is replaced throughout.

64
00:02:57,10 --> 00:02:58,70
So I've still got the correct path,

65
00:02:58,70 --> 00:03:01,90
based on the way those server folders are structured,

66
00:03:01,90 --> 00:03:04,60
but now it's going to the actual domain name

67
00:03:04,60 --> 00:03:06,60
that I'm going to be use.

68
00:03:06,60 --> 00:03:10,80
And so now my files are ready to use with my domains.

69
00:03:10,80 --> 00:03:14,30
You want to follow your web host's instructions for deployment

70
00:03:14,30 --> 00:03:16,30
In many of the videos in this course

71
00:03:16,30 --> 00:03:19,60
my sites are deployed to different Heroku instances.

72
00:03:19,60 --> 00:03:21,80
Heroku maintains thorough documentation

73
00:03:21,80 --> 00:03:23,30
on deploying with Git,

74
00:03:23,30 --> 00:03:26,70
which is the method I use throughout the course.

75
00:03:26,70 --> 00:03:29,00
Remember that you want to deploy the site

76
00:03:29,00 --> 00:03:32,50
and resources folders to two separate domains

77
00:03:32,50 --> 00:03:35,40
or subdomains if you're using Heroku.

78
00:03:35,40 --> 00:03:38,00
Hosting both folders as a single app

79
00:03:38,00 --> 00:03:40,50
with different subfolders won't work

80
00:03:40,50 --> 00:03:43,80
for most of the videos in this course.

81
00:03:43,80 --> 00:03:45,70
In addition to the servers folder

82
00:03:45,70 --> 00:03:49,70
the exercise files also contains a folder for each chapter

83
00:03:49,70 --> 00:03:54,40
with a subfolder for each video that makes changes to files.

84
00:03:54,40 --> 00:03:58,40
Within a chapter folder you'll find begin and end versions

85
00:03:58,40 --> 00:04:01,60
of the changed files only.

86
00:04:01,60 --> 00:04:04,50
If you want to start a video without working through those

87
00:04:04,50 --> 00:04:07,60
that came just before it, you can use the files

88
00:04:07,60 --> 00:04:09,60
in the begin folder.

89
00:04:09,60 --> 00:04:12,50
The folder structure within the begin folder

90
00:04:12,50 --> 00:04:15,20
replicates the path where the file needs to be placed

91
00:04:15,20 --> 00:04:18,50
in one of the servers.

92
00:04:18,50 --> 00:04:22,30
The easiest approach is to simply copy the folder

93
00:04:22,30 --> 00:04:24,70
for the server name to the clipboard

94
00:04:24,70 --> 00:04:27,00
and then paste it into the servers folder

95
00:04:27,00 --> 00:04:29,40
and select the option to merge files.

96
00:04:29,40 --> 00:04:32,90
So for instance here I have files from the resources folder

97
00:04:32,90 --> 00:04:36,00
that are changing, so I can right-click on a Mac

98
00:04:36,00 --> 00:04:39,60
and click Copy resources

99
00:04:39,60 --> 00:04:41,70
and then over in the servers folder

100
00:04:41,70 --> 00:04:45,50
I can just right-click servers and click

101
00:04:45,50 --> 00:04:46,90
Paste Item

102
00:04:46,90 --> 00:04:49,70
and then I get a prompt saying

103
00:04:49,70 --> 00:04:51,40
that this folder already exists

104
00:04:51,40 --> 00:04:53,10
and asking me if I want to merge,

105
00:04:53,10 --> 00:04:56,10
and that's exactly what I want to do.

106
00:04:56,10 --> 00:04:58,80
The start files folder contains those files

107
00:04:58,80 --> 00:05:00,70
in the correct subdirectory,

108
00:05:00,70 --> 00:05:02,40
so I can just click Merge

109
00:05:02,40 --> 00:05:04,50
and the start files will replace the files

110
00:05:04,50 --> 00:05:08,10
with the same name that I had in that particular server

111
00:05:08,10 --> 00:05:13,00
setting me up to start this particular video.

