1
00:00:00,40 --> 00:00:02,10
- [Instructor] When we write and deploy code

2
00:00:02,10 --> 00:00:03,80
that runs in web browsers,

3
00:00:03,80 --> 00:00:07,40
we're building applications that rely on the browser to run.

4
00:00:07,40 --> 00:00:09,90
Even though multiple applications can be executed

5
00:00:09,90 --> 00:00:13,70
in a browser simultaneously in different windows or tabs,

6
00:00:13,70 --> 00:00:17,90
we expect each app to operate independently of the others.

7
00:00:17,90 --> 00:00:20,30
This is the same thing we expect from applications

8
00:00:20,30 --> 00:00:22,80
we install and run on any computer,

9
00:00:22,80 --> 00:00:25,20
which should be unaffected by other applications

10
00:00:25,20 --> 00:00:27,00
on that same computer.

11
00:00:27,00 --> 00:00:29,90
Modern web browsers have multiple mechanisms in place

12
00:00:29,90 --> 00:00:32,80
to keep code in one window or tab isolated

13
00:00:32,80 --> 00:00:35,60
from code in other windows or tabs.

14
00:00:35,60 --> 00:00:38,20
The foundation of many of these mechanisms

15
00:00:38,20 --> 00:00:40,50
is a same-origin policy.

16
00:00:40,50 --> 00:00:43,00
A same-origin policy sets out to ensure

17
00:00:43,00 --> 00:00:45,50
that the code for one site is not affected

18
00:00:45,50 --> 00:00:47,40
by code from other sides.

19
00:00:47,40 --> 00:00:50,10
This policy affects a number of sensitive parts

20
00:00:50,10 --> 00:00:51,90
of the web API.

21
00:00:51,90 --> 00:00:55,00
Because each part is affected in its own unique way,

22
00:00:55,00 --> 00:00:57,60
it can be helpful to think of the policy instead

23
00:00:57,60 --> 00:01:00,50
as a collection of same-origin policies

24
00:01:00,50 --> 00:01:02,10
for different components,

25
00:01:02,10 --> 00:01:05,10
each with its own restrictions and rules.

26
00:01:05,10 --> 00:01:08,80
For instance, the same-origin policy for XHR

27
00:01:08,80 --> 00:01:11,50
serves to prevent access to web services

28
00:01:11,50 --> 00:01:13,90
from unauthorized domains.

29
00:01:13,90 --> 00:01:15,80
The same-origin policy for cookies

30
00:01:15,80 --> 00:01:18,00
protects information stored in a cookie

31
00:01:18,00 --> 00:01:20,00
from being read by any site

32
00:01:20,00 --> 00:01:22,20
but the one that originally set it.

33
00:01:22,20 --> 00:01:24,60
And the same-origin policy for DOM access

34
00:01:24,60 --> 00:01:27,10
ensures that access to a webpage's DOM

35
00:01:27,10 --> 00:01:31,30
is restricted only to other pages from the same website.

36
00:01:31,30 --> 00:01:33,30
Now, all of these policies have exceptions

37
00:01:33,30 --> 00:01:35,00
that developers can work with,

38
00:01:35,00 --> 00:01:38,40
otherwise the web as we know it would not exist.

39
00:01:38,40 --> 00:01:40,80
So understanding same-origin policies

40
00:01:40,80 --> 00:01:43,30
also involves learning how to work with them

41
00:01:43,30 --> 00:01:46,30
to achieve the outcomes your app or site needs

42
00:01:46,30 --> 00:01:48,00
without compromising security.

