1
00:00:00,50 --> 00:00:02,50
- [Instructor] Same origin policies are built

2
00:00:02,50 --> 00:00:04,40
on the concept of an origin,

3
00:00:04,40 --> 00:00:06,30
which defines the default sources

4
00:00:06,30 --> 00:00:07,60
where code can come from

5
00:00:07,60 --> 00:00:10,80
and still be incorporated into a website.

6
00:00:10,80 --> 00:00:14,70
An origin is defined by four components of a URL.

7
00:00:14,70 --> 00:00:21,20
The domain, the subdomain, the protocol, and the port.

8
00:00:21,20 --> 00:00:24,80
The domain is the core part of an origin.

9
00:00:24,80 --> 00:00:28,70
Same origin policies generally permit information sharing

10
00:00:28,70 --> 00:00:31,30
between documents from the same domain.

11
00:00:31,30 --> 00:00:34,40
So a script from description.com is blocked

12
00:00:34,40 --> 00:00:36,90
by default from accessing information

13
00:00:36,90 --> 00:00:40,30
on a page loaded from example.com.

14
00:00:40,30 --> 00:00:42,80
Even if the domain names are the same,

15
00:00:42,80 --> 00:00:46,90
if one or more other details of the URL are different,

16
00:00:46,90 --> 00:00:50,90
then same origin polices do not permit sharing.

17
00:00:50,90 --> 00:00:53,70
One of these details is the subdomain,

18
00:00:53,70 --> 00:00:56,90
which is the portion of the URL after the protocol

19
00:00:56,90 --> 00:00:58,90
and before the domain name.

20
00:00:58,90 --> 00:01:03,50
If a browser loaded a page from users.example.com,

21
00:01:03,50 --> 00:01:06,00
this page would not have access to information

22
00:01:06,00 --> 00:01:10,20
associated with www.example.com,

23
00:01:10,20 --> 00:01:12,30
because the different subdomains mean

24
00:01:12,30 --> 00:01:16,30
that these URL's constitute different origins.

25
00:01:16,30 --> 00:01:19,10
For two sources to share the same origin,

26
00:01:19,10 --> 00:01:22,00
they must also use the same protocol.

27
00:01:22,00 --> 00:01:25,10
This means that, for instance, a site being served

28
00:01:25,10 --> 00:01:28,50
using the HTTPS protocol would be considered

29
00:01:28,50 --> 00:01:34,00
a different origin than the same URL served over HTTP.

30
00:01:34,00 --> 00:01:38,10
So even with all other parts of a URL being the same,

31
00:01:38,10 --> 00:01:43,70
a page loaded from HTTP www.example.com

32
00:01:43,70 --> 00:01:46,10
would not have access to information associated

33
00:01:46,10 --> 00:01:50,40
with HTTPS www.example.com,

34
00:01:50,40 --> 00:01:54,00
because of the different protocols in the URL's.

35
00:01:54,00 --> 00:01:57,00
Finally, the port number is also considered part

36
00:01:57,00 --> 00:01:58,60
of the origin.

37
00:01:58,60 --> 00:02:00,90
You don't often see an explicit port number

38
00:02:00,90 --> 00:02:03,20
as part of of HTTP requests

39
00:02:03,20 --> 00:02:05,90
and under the hood each protocol is associated

40
00:02:05,90 --> 00:02:07,70
with a different port number.

41
00:02:07,70 --> 00:02:09,90
So we would expect differences in ports

42
00:02:09,90 --> 00:02:12,60
to accompany differences in protocols.

43
00:02:12,60 --> 00:02:15,90
But in situations where a port number is set explicitly,

44
00:02:15,90 --> 00:02:18,70
it must match across URL's in order

45
00:02:18,70 --> 00:02:20,80
for them to share an origin.

46
00:02:20,80 --> 00:02:28,00
So for instance, HTTPS www.example.com:443

47
00:02:28,00 --> 00:02:30,40
would be considered a different origin

48
00:02:30,40 --> 00:02:36,80
from HTTPS www.example.com:4430,

49
00:02:36,80 --> 00:02:39,00
because of the difference in port numbers.

