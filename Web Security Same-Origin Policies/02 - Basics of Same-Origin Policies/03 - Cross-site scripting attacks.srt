1
00:00:00,50 --> 00:00:02,50
- Hackers are continually identifying

2
00:00:02,50 --> 00:00:05,80
and exploiting imperfections in the security model

3
00:00:05,80 --> 00:00:08,00
and browser makers are constantly testing

4
00:00:08,00 --> 00:00:09,30
and hardening their code

5
00:00:09,30 --> 00:00:11,40
to seal up these gaps.

6
00:00:11,40 --> 00:00:14,60
One of the primary threats is a cross-site scripting

7
00:00:14,60 --> 00:00:16,80
or XSS attack.

8
00:00:16,80 --> 00:00:17,70
By default,

9
00:00:17,70 --> 00:00:19,80
the same origin policy for scripts

10
00:00:19,80 --> 00:00:22,90
dictates their browser trusts every script request

11
00:00:22,90 --> 00:00:24,70
that comes from a webpage.

12
00:00:24,70 --> 00:00:26,60
This enables me to write code

13
00:00:26,60 --> 00:00:29,10
that requests scripts from my own domain,

14
00:00:29,10 --> 00:00:32,00
as well as requesting scripts for common libraries,

15
00:00:32,00 --> 00:00:33,90
frameworks, and other assets

16
00:00:33,90 --> 00:00:38,10
from content delivery networks at different domains.

17
00:00:38,10 --> 00:00:40,40
This includes scripts other than those coded

18
00:00:40,40 --> 00:00:42,50
into the html document.

19
00:00:42,50 --> 00:00:44,10
With default settings,

20
00:00:44,10 --> 00:00:45,70
any dell manipulation method

21
00:00:45,70 --> 00:00:48,20
can add a script element requesting a resource

22
00:00:48,20 --> 00:00:50,00
from any domain.

23
00:00:50,00 --> 00:00:51,10
As a developer,

24
00:00:51,10 --> 00:00:53,60
it's more common to simply add script elements

25
00:00:53,60 --> 00:00:56,40
in the html document from the start.

26
00:00:56,40 --> 00:00:58,00
Although, in some situations,

27
00:00:58,00 --> 00:01:01,10
it makes sense to add script elements later on

28
00:01:01,10 --> 00:01:03,10
for performance reasons.

29
00:01:03,10 --> 00:01:04,90
But any script from another origin

30
00:01:04,90 --> 00:01:08,70
that hasn't been requested is prevented from loading.

31
00:01:08,70 --> 00:01:11,10
It a cross-site scripting attack,

32
00:01:11,10 --> 00:01:13,10
an attacker is able to trick a browser

33
00:01:13,10 --> 00:01:15,60
with a script request that's not actually part

34
00:01:15,60 --> 00:01:17,90
of the code for the webpage.

35
00:01:17,90 --> 00:01:20,60
A cross-site scripting attack is commonly used

36
00:01:20,60 --> 00:01:22,60
to access a user's private data

37
00:01:22,60 --> 00:01:26,10
and send that data to the attacker.

38
00:01:26,10 --> 00:01:28,50
Attackers use two main routes to trick a browser

39
00:01:28,50 --> 00:01:30,30
into parsing a new script element

40
00:01:30,30 --> 00:01:32,20
as part of the current webpage.

41
00:01:32,20 --> 00:01:35,30
Malformed form-field input in the webpage itself

42
00:01:35,30 --> 00:01:37,40
and booby-trapped links in email

43
00:01:37,40 --> 00:01:39,60
or other messaging platforms

44
00:01:39,60 --> 00:01:41,80
that open a page in the browser.

45
00:01:41,80 --> 00:01:43,70
For instance, this is a basic script

46
00:01:43,70 --> 00:01:48,40
to work with input that a user enters in a form-field.

47
00:01:48,40 --> 00:01:52,40
The eval method parses any JavaScript it has passed,

48
00:01:52,40 --> 00:01:54,00
which could be any combination

49
00:01:54,00 --> 00:01:57,50
of literal code and references to values.

50
00:01:57,50 --> 00:02:00,00
In this case, we're sending the entire value

51
00:02:00,00 --> 00:02:03,00
of the sign-up field to the eval method.

52
00:02:03,00 --> 00:02:04,90
When I open a page,

53
00:02:04,90 --> 00:02:07,20
and scroll down to the sign-up box,

54
00:02:07,20 --> 00:02:10,80
and I'm going to use the alert method,

55
00:02:10,80 --> 00:02:14,30
and just alert the value one.

56
00:02:14,30 --> 00:02:19,90
And so sending this JavaScript method to eval means

57
00:02:19,90 --> 00:02:22,00
that it actually gets processed,

58
00:02:22,00 --> 00:02:27,30
and so that value of one is alerted as box.

59
00:02:27,30 --> 00:02:29,00
By the same token,

60
00:02:29,00 --> 00:02:35,30
I could do something like alert document.cookie,

61
00:02:35,30 --> 00:02:37,10
and if I click the box,

62
00:02:37,10 --> 00:02:38,90
now I've got the users cookies,

63
00:02:38,90 --> 00:02:40,40
including email address,

64
00:02:40,40 --> 00:02:41,40
the last item they've viewed,

65
00:02:41,40 --> 00:02:43,70
and the total in their cart.

66
00:02:43,70 --> 00:02:46,00
So anything that's in the document.cookie,

67
00:02:46,00 --> 00:02:48,50
I could suddenly have access to.

68
00:02:48,50 --> 00:02:52,70
But how about instead of simply opening an alert box,

69
00:02:52,70 --> 00:02:54,90
I encode a simple fetch request,

70
00:02:54,90 --> 00:02:57,40
using the URL of a different server

71
00:02:57,40 --> 00:03:00,10
and including the value of document.cookie

72
00:03:00,10 --> 00:03:02,20
as a query string.

73
00:03:02,20 --> 00:03:04,60
And so I've written up some basic code to do this.

74
00:03:04,60 --> 00:03:07,30
It's a fetch request to another server

75
00:03:07,30 --> 00:03:10,30
and using data as my key

76
00:03:10,30 --> 00:03:11,80
and as my value,

77
00:03:11,80 --> 00:03:14,00
I'm using the encodeURIcomponent

78
00:03:14,00 --> 00:03:16,10
to grab document.cookie

79
00:03:16,10 --> 00:03:18,90
and make that a single string

80
00:03:18,90 --> 00:03:22,70
that I can pass as the value in URI.

81
00:03:22,70 --> 00:03:26,80
And so I'm going to copy this whole fetch request

82
00:03:26,80 --> 00:03:32,60
and back in my form, paste that in.

83
00:03:32,60 --> 00:03:33,90
And before I submit that,

84
00:03:33,90 --> 00:03:36,10
I'm going to open up the console

85
00:03:36,10 --> 00:03:39,40
and I'm going to switch to the network tab

86
00:03:39,40 --> 00:03:42,70
so that network activity is recorded,

87
00:03:42,70 --> 00:03:46,20
and now I'm going to hit sign-up.

88
00:03:46,20 --> 00:03:48,70
Notice I have an HTTP request here

89
00:03:48,70 --> 00:03:51,20
and if I click that to view it

90
00:03:51,20 --> 00:03:52,70
and I look at the headers,

91
00:03:52,70 --> 00:03:55,70
the request URL includes those cookies

92
00:03:55,70 --> 00:03:59,90
with the spaces and semicolons URL encoded,

93
00:03:59,90 --> 00:04:02,10
but you can see I've got the email address,

94
00:04:02,10 --> 00:04:04,90
the last item viewed, and the cart total.

95
00:04:04,90 --> 00:04:07,00
And this has a 200 code,

96
00:04:07,00 --> 00:04:10,70
which means it was a successfully sent get request.

97
00:04:10,70 --> 00:04:12,90
Even though I didn't get data back,

98
00:04:12,90 --> 00:04:16,00
we know that the request was actually sent,

99
00:04:16,00 --> 00:04:18,40
which means this data would have been leaked

100
00:04:18,40 --> 00:04:20,50
using this statement.

101
00:04:20,50 --> 00:04:22,50
Now none of this is of any value

102
00:04:22,50 --> 00:04:26,40
for an attacker to do on their own browser session.

103
00:04:26,40 --> 00:04:30,60
In addition, modern JavaScript coding practices avoid use

104
00:04:30,60 --> 00:04:32,10
of the eval statement,

105
00:04:32,10 --> 00:04:37,10
in part to plug security holes just like this one.

106
00:04:37,10 --> 00:04:39,50
Modern cross-site scripting threats focus

107
00:04:39,50 --> 00:04:41,30
on using the browser to gain access

108
00:04:41,30 --> 00:04:43,60
to the data of other users.

109
00:04:43,60 --> 00:04:46,30
Instead of vulnerable front-end code,

110
00:04:46,30 --> 00:04:48,20
these attacks focus on identifying

111
00:04:48,20 --> 00:04:49,90
and exploiting vulnerable code

112
00:04:49,90 --> 00:04:51,50
on the back-end.

113
00:04:51,50 --> 00:04:54,40
And instead of the attacker running the code locally,

114
00:04:54,40 --> 00:04:55,90
attackers use a number of techniques

115
00:04:55,90 --> 00:04:58,80
to get other users to execute code

116
00:04:58,80 --> 00:05:00,70
that can result in leaks

117
00:05:00,70 --> 00:05:03,30
of those user's personal information

118
00:05:03,30 --> 00:05:05,30
to the attackers.

119
00:05:05,30 --> 00:05:06,20
One attack,

120
00:05:06,20 --> 00:05:09,20
know as a reflected cross-site scripting attack,

121
00:05:09,20 --> 00:05:12,10
involves identifying a vulnerable server

122
00:05:12,10 --> 00:05:14,40
and then tricking users into clicking a link

123
00:05:14,40 --> 00:05:16,80
to that server that's specially constructed

124
00:05:16,80 --> 00:05:18,70
to leak information.

125
00:05:18,70 --> 00:05:19,70
For instance,

126
00:05:19,70 --> 00:05:22,10
for a server that accepts query parameters

127
00:05:22,10 --> 00:05:23,60
and tries to parse them,

128
00:05:23,60 --> 00:05:25,00
attackers check what happens

129
00:05:25,00 --> 00:05:28,50
if they send invalid parameters to the server.

130
00:05:28,50 --> 00:05:31,30
An insecure server will send back an error

131
00:05:31,30 --> 00:05:35,60
that includes the value of each invalid parameter.

132
00:05:35,60 --> 00:05:38,40
If the attacker sends an html script tag,

133
00:05:38,40 --> 00:05:40,40
along with embedded JavaScript

134
00:05:40,40 --> 00:05:42,40
as a query parameter,

135
00:05:42,40 --> 00:05:44,40
then the resulting error page loads

136
00:05:44,40 --> 00:05:47,00
and executes that script.

137
00:05:47,00 --> 00:05:50,30
If the script includes an ATTP request

138
00:05:50,30 --> 00:05:51,70
that sends the user's cookies

139
00:05:51,70 --> 00:05:54,30
to a server controlled by the attacker,

140
00:05:54,30 --> 00:05:55,90
this can leak the user's information

141
00:05:55,90 --> 00:05:58,80
and allow the attacker to use it.

142
00:05:58,80 --> 00:06:00,10
The attacker might share the link

143
00:06:00,10 --> 00:06:01,80
in a spam email blast

144
00:06:01,80 --> 00:06:04,10
or in a social media post.

145
00:06:04,10 --> 00:06:05,50
If only a small fraction

146
00:06:05,50 --> 00:06:07,50
of users who receive the link click it,

147
00:06:07,50 --> 00:06:09,00
the attacker still gains access

148
00:06:09,00 --> 00:06:13,00
to the information of a handful of users.

149
00:06:13,00 --> 00:06:14,40
A related attack is known

150
00:06:14,40 --> 00:06:17,10
as a stored cross-site scripting attack.

151
00:06:17,10 --> 00:06:19,80
This is similar to a reflected attack,

152
00:06:19,80 --> 00:06:21,00
except in this case,

153
00:06:21,00 --> 00:06:22,70
the attacker somehow gains access

154
00:06:22,70 --> 00:06:25,00
to the vulnerable web server itself

155
00:06:25,00 --> 00:06:26,70
and is able to plant malicious code

156
00:06:26,70 --> 00:06:29,30
that serve the users directly.

157
00:06:29,30 --> 00:06:31,20
As long as it goes undetected,

158
00:06:31,20 --> 00:06:33,50
user requests to the back-end will

159
00:06:33,50 --> 00:06:38,00
also involve data being sent to the attacker's server.

