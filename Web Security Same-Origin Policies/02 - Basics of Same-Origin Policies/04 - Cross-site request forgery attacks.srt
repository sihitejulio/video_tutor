1
00:00:00,50 --> 00:00:01,80
- [Narrator] Modern web browsers

2
00:00:01,80 --> 00:00:06,60
have multiple mechanisms in place

3
00:00:06,60 --> 00:00:09,30
isolated from code in other windows or tabs.

4
00:00:09,30 --> 00:00:12,60
However, hackers are continually identifying

5
00:00:12,60 --> 00:00:15,70
and exploiting imperfections in the security model

6
00:00:15,70 --> 00:00:18,30
and browser makers are constantly testing

7
00:00:18,30 --> 00:00:21,80
and hardening their code to seal up these gaps.

8
00:00:21,80 --> 00:00:24,30
Another significant threat to browser security

9
00:00:24,30 --> 00:00:29,30
is known as a Cross-Site Request Forgery or CSRF attack.

10
00:00:29,30 --> 00:00:32,10
A CSRF attack focuses on making changes

11
00:00:32,10 --> 00:00:33,80
to back end resources.

12
00:00:33,80 --> 00:00:37,00
These attacks take advantage of an active session

13
00:00:37,00 --> 00:00:40,10
to make authenticated requests to the back end

14
00:00:40,10 --> 00:00:44,60
without the intention or awareness of the user.

15
00:00:44,60 --> 00:00:47,20
A CSRF attack starts with the attackers

16
00:00:47,20 --> 00:00:50,10
studying the API for the targeted site

17
00:00:50,10 --> 00:00:52,40
and constructing request URL

18
00:00:52,40 --> 00:00:55,70
that will make a specific change to a user's data.

19
00:00:55,70 --> 00:00:58,70
This might include changing the contact info on the account

20
00:00:58,70 --> 00:01:00,70
in a way that gives the attacker control

21
00:01:00,70 --> 00:01:03,30
or it could involve a financial transaction

22
00:01:03,30 --> 00:01:07,00
on the site for a bank or other institution.

23
00:01:07,00 --> 00:01:09,80
Once the attacker has built the exploit URL,

24
00:01:09,80 --> 00:01:11,60
the final step is to trick a user

25
00:01:11,60 --> 00:01:13,50
into initiating the request

26
00:01:13,50 --> 00:01:16,10
usually by clicking a specially constructed link

27
00:01:16,10 --> 00:01:18,30
in an email or on a web page

28
00:01:18,30 --> 00:01:20,50
that the targeted user visits.

29
00:01:20,50 --> 00:01:23,80
For instance, if a server accepts GET requests,

30
00:01:23,80 --> 00:01:27,00
the exploit URL can simply be specified as a link

31
00:01:27,00 --> 00:01:29,40
or as the source of an image element.

32
00:01:29,40 --> 00:01:32,60
If a user clicks the link or allows the image to load,

33
00:01:32,60 --> 00:01:35,60
the HTTP request is executed

34
00:01:35,60 --> 00:01:38,30
and assuming the attacker constructed it correctly,

35
00:01:38,30 --> 00:01:40,80
the backend resource is modified

36
00:01:40,80 --> 00:01:43,30
to the attackers specification.

37
00:01:43,30 --> 00:01:46,50
Note that the attacker doesn't get direct feedback here.

38
00:01:46,50 --> 00:01:50,10
No information is being directly forwarded.

39
00:01:50,10 --> 00:01:53,30
Instead, if for instance the attacker had constructed

40
00:01:53,30 --> 00:01:56,10
a request to change the users contact info

41
00:01:56,10 --> 00:01:58,10
to the attacker's email address,

42
00:01:58,10 --> 00:02:00,70
the attacker might receive an email from the web service

43
00:02:00,70 --> 00:02:02,60
confirming the change.

44
00:02:02,60 --> 00:02:04,80
Likewise, if the attacker had initiated

45
00:02:04,80 --> 00:02:07,60
a financial transaction to an account they control,

46
00:02:07,60 --> 00:02:09,20
they would be able to see the result

47
00:02:09,20 --> 00:02:12,10
of the fraudulent transaction in their account

48
00:02:12,10 --> 00:02:15,20
as soon as the transaction was processed.

49
00:02:15,20 --> 00:02:17,80
Websites have a number of measures in place

50
00:02:17,80 --> 00:02:21,00
to protect you against CSRF attacks.

51
00:02:21,00 --> 00:02:23,80
For instance, when you're logged into a sensitive site

52
00:02:23,80 --> 00:02:25,60
such as a bank and switch

53
00:02:25,60 --> 00:02:27,70
to a different browser tab for a while,

54
00:02:27,70 --> 00:02:30,20
you may switch back to find a countdown timer

55
00:02:30,20 --> 00:02:33,90
indicating that you'll be logged out soon due to inactivity.

56
00:02:33,90 --> 00:02:36,50
This is meant to minimize the amount of time

57
00:02:36,50 --> 00:02:38,60
that a user has an active session

58
00:02:38,60 --> 00:02:40,80
because after a user is logged out,

59
00:02:40,80 --> 00:02:44,00
a CSRF attack cannot succeed.

60
00:02:44,00 --> 00:02:47,90
Likewise, email is commonly displayed without images loading

61
00:02:47,90 --> 00:02:50,30
for users who aren't in your address book

62
00:02:50,30 --> 00:02:53,50
or for messages that are suspected of being spam

63
00:02:53,50 --> 00:02:56,50
This tactic has a number of reasons behind it,

64
00:02:56,50 --> 00:02:59,50
but one of them is to avoid loading a malformed image

65
00:02:59,50 --> 00:03:03,00
in an email from an attacker that is aimed at executing

66
00:03:03,00 --> 00:03:06,00
a CSRF attack on an open session.

