1
00:00:00,50 --> 00:00:02,40
- [Instructor] Cookies are set using directives

2
00:00:02,40 --> 00:00:04,60
which allow you to do a bit of customization

3
00:00:04,60 --> 00:00:06,80
to limit a cookie's scope.

4
00:00:06,80 --> 00:00:08,90
To specify the domain of a cookie,

5
00:00:08,90 --> 00:00:11,30
you use the domain directive.

6
00:00:11,30 --> 00:00:14,00
If you set a cookie without a domain directive,

7
00:00:14,00 --> 00:00:17,10
it applies to the current domain of the current document,

8
00:00:17,10 --> 00:00:19,90
including the subdomain.

9
00:00:19,90 --> 00:00:23,00
However, if you do use the domain directive,

10
00:00:23,00 --> 00:00:26,30
then allowable domain or subdomain you specify

11
00:00:26,30 --> 00:00:29,30
in that directive is associated with the cookie.

12
00:00:29,30 --> 00:00:31,00
That may be the current subdomain

13
00:00:31,00 --> 00:00:34,80
or the parent domain, for example.

14
00:00:34,80 --> 00:00:37,70
I've been using a couple sites at herokuapp.com

15
00:00:37,70 --> 00:00:39,90
to test my code for this project,

16
00:00:39,90 --> 00:00:43,10
but herokuapp.com is considered a public suffix

17
00:00:43,10 --> 00:00:45,80
because so many different apps are assigned subdomains

18
00:00:45,80 --> 00:00:48,80
at this domain, so it's part of this list.

19
00:00:48,80 --> 00:00:51,00
And users are prevented from setting cookies

20
00:00:51,00 --> 00:00:53,50
on the parent domain, which keeps all the apps

21
00:00:53,50 --> 00:00:58,10
on the subdomains segmented from each other and more secure.

22
00:00:58,10 --> 00:01:00,60
So to explore across domain cookies,

23
00:01:00,60 --> 00:01:03,70
I've instead uploaded my code to a couple subdomains

24
00:01:03,70 --> 00:01:07,20
on a domain that's not considered a public suffix.

25
00:01:07,20 --> 00:01:10,10
I have index.html open in one tab

26
00:01:10,10 --> 00:01:15,00
from 794147-1.youcanlearnit.net

27
00:01:15,00 --> 00:01:18,00
and then product-detail-arrangements.html

28
00:01:18,00 --> 00:01:19,50
in a different tab loading

29
00:01:19,50 --> 00:01:24,10
from 794147-2.youcanlearnit.net.

30
00:01:24,10 --> 00:01:26,90
I named the subdomains with -1 and -2 at the end

31
00:01:26,90 --> 00:01:29,80
to make them more easily distinguishable.

32
00:01:29,80 --> 00:01:32,70
I can create and view cookies using JavaScript commands,

33
00:01:32,70 --> 00:01:35,20
but browser developer tools also offer utilities

34
00:01:35,20 --> 00:01:38,60
that let me do that, which is ideal for our purposes.

35
00:01:38,60 --> 00:01:42,60
So on the tab for index.html,

36
00:01:42,60 --> 00:01:46,60
I'm going to open up my browser developer tools.

37
00:01:46,60 --> 00:01:49,20
And we've just changed the origin on this,

38
00:01:49,20 --> 00:01:52,40
so the fonts aren't loading, but that's not a big deal.

39
00:01:52,40 --> 00:01:56,20
And I'm guess use these arrows to find the Application tab

40
00:01:56,20 --> 00:01:58,30
and open that up.

41
00:01:58,30 --> 00:02:00,30
And then I'm going to click Cookies.

42
00:02:00,30 --> 00:02:02,40
And there's actually a little arrow next to Cookies.

43
00:02:02,40 --> 00:02:04,20
And I'm going to click the current domain.

44
00:02:04,20 --> 00:02:06,90
Clicking either domain seems to show all the cookies,

45
00:02:06,90 --> 00:02:09,50
and so some of these are loading from LinkedIn.com.

46
00:02:09,50 --> 00:02:12,20
Now, there's a lot of different columns,

47
00:02:12,20 --> 00:02:13,50
a lot of information for the cookies,

48
00:02:13,50 --> 00:02:14,30
and we want to see it all,

49
00:02:14,30 --> 00:02:16,30
so I'm going to actually switch my view

50
00:02:16,30 --> 00:02:20,50
using these three dots here to the bottom of the screen

51
00:02:20,50 --> 00:02:22,10
and Dock to bottom button.

52
00:02:22,10 --> 00:02:25,60
And so now all of these columns widened

53
00:02:25,60 --> 00:02:28,80
so I can see all or most of the information.

54
00:02:28,80 --> 00:02:32,60
Now, I don't really care about the LinkedIn.com cookies

55
00:02:32,60 --> 00:02:33,50
in this case.

56
00:02:33,50 --> 00:02:35,70
I just want to work with the ones from my own domain.

57
00:02:35,70 --> 00:02:37,80
So I'm going to use the Filter box here

58
00:02:37,80 --> 00:02:41,70
and just type youcan, which is part of my domain,

59
00:02:41,70 --> 00:02:43,90
and it automatically filters out those LinkedIn cookies

60
00:02:43,90 --> 00:02:46,30
which aren't on the same domain.

61
00:02:46,30 --> 00:02:48,10
So now I can create a new cookie

62
00:02:48,10 --> 00:02:49,90
just by double-clicking and typing.

63
00:02:49,90 --> 00:02:53,20
And so in the Name column, I'm going to double-click,

64
00:02:53,20 --> 00:02:56,90
and I'm going to type total.

65
00:02:56,90 --> 00:02:59,80
I'm going to tab over to the Value column.

66
00:02:59,80 --> 00:03:03,00
Then we'll say 56 24.

67
00:03:03,00 --> 00:03:04,70
And then tabbing over,

68
00:03:04,70 --> 00:03:07,30
we can see that in the domain field,

69
00:03:07,30 --> 00:03:10,20
it's already been populated with the current domain

70
00:03:10,20 --> 00:03:12,90
and subdomain for the current resource.

71
00:03:12,90 --> 00:03:14,70
I didn't have to do anything to configure that.

72
00:03:14,70 --> 00:03:16,90
By default, that's a cookie's domain value

73
00:03:16,90 --> 00:03:19,50
unless you specify it otherwise.

74
00:03:19,50 --> 00:03:22,10
And then I'm going to tab a couple more times over

75
00:03:22,10 --> 00:03:23,90
to the Expires field.

76
00:03:23,90 --> 00:03:25,90
Now, the dev tools won't let you create a cookie

77
00:03:25,90 --> 00:03:30,70
without an expiration, so I'll just enter the year 2100 here

78
00:03:30,70 --> 00:03:33,70
and press Tab, and the tools translate that

79
00:03:33,70 --> 00:03:36,60
into an appropriate date format.

80
00:03:36,60 --> 00:03:38,40
Now, if I go to my other tab,

81
00:03:38,40 --> 00:03:41,90
which is at the same domain, but a different subdomain,

82
00:03:41,90 --> 00:03:44,80
and I'm going to open up my dev tools.

83
00:03:44,80 --> 00:03:47,10
Switch to the Application tab.

84
00:03:47,10 --> 00:03:50,10
Open up Cookies and select the domain.

85
00:03:50,10 --> 00:03:51,70
And then there's a refresh button here

86
00:03:51,70 --> 00:03:54,50
right next to the filter box, so I'll hit that.

87
00:03:54,50 --> 00:03:56,30
And there are indeed no cookies

88
00:03:56,30 --> 00:03:58,60
for this particular resource.

89
00:03:58,60 --> 00:03:59,90
And that makes sense.

90
00:03:59,90 --> 00:04:01,10
It's because the cookie that I set

91
00:04:01,10 --> 00:04:04,10
in the other tab was specific to the other domain,

92
00:04:04,10 --> 00:04:06,70
so it's not accessible by this domain.

93
00:04:06,70 --> 00:04:08,60
So it's super straightforward to create cookies

94
00:04:08,60 --> 00:04:10,30
that are specific to a subdomain

95
00:04:10,30 --> 00:04:12,70
and aren't accessible by the parent domain

96
00:04:12,70 --> 00:04:15,00
or any other subdomains of that parent.

