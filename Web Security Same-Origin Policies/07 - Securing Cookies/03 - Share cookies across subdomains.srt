1
00:00:00,50 --> 00:00:02,20
- [Instructor] Sometimes you want to share cookies

2
00:00:02,20 --> 00:00:05,00
across subdomains, for instance,

3
00:00:05,00 --> 00:00:08,10
in an online store that sells furnishings,

4
00:00:08,10 --> 00:00:11,50
and has a separate subdomain for each section of the home,

5
00:00:11,50 --> 00:00:14,50
you'd expect that dining.example.com

6
00:00:14,50 --> 00:00:17,30
and kitchen.example.com should both be able

7
00:00:17,30 --> 00:00:19,20
to set and read the same cookies

8
00:00:19,20 --> 00:00:21,00
for the user's shopping cart.

9
00:00:21,00 --> 00:00:24,40
However, by default, any cookies you set

10
00:00:24,40 --> 00:00:27,80
on pages at each of these subdomains defaults

11
00:00:27,80 --> 00:00:31,20
to being a cookie just for that subdomain.

12
00:00:31,20 --> 00:00:33,60
To share cookies across subdomains,

13
00:00:33,60 --> 00:00:36,80
you can simply create cookies with the domain directive set

14
00:00:36,80 --> 00:00:40,40
to the parent domain, in this case, example.com,

15
00:00:40,40 --> 00:00:45,90
rather than either of the specific subdomains.

16
00:00:45,90 --> 00:00:47,60
In my browser, I have the index

17
00:00:47,60 --> 00:00:50,20
and product detail pages open.

18
00:00:50,20 --> 00:00:53,20
And on the product detail page,

19
00:00:53,20 --> 00:00:55,90
going back to the cookies in the Application tab

20
00:00:55,90 --> 00:01:00,20
of the developer tools, I'm going to create a new cookie.

21
00:01:00,20 --> 00:01:03,40
So double-clicking in the Name column,

22
00:01:03,40 --> 00:01:05,70
I'm just going to call the cookie shared

23
00:01:05,70 --> 00:01:09,10
and set its value to true.

24
00:01:09,10 --> 00:01:10,50
And then, in the Domain column,

25
00:01:10,50 --> 00:01:12,40
I'm going to edit the default value,

26
00:01:12,40 --> 00:01:15,80
which is the subdomain that I've opened this from.

27
00:01:15,80 --> 00:01:19,90
So I'm going to click and remove just the subdomain.

28
00:01:19,90 --> 00:01:22,10
Now, an older spec calls for including

29
00:01:22,10 --> 00:01:25,40
this leading dot here to set a domain-level cookie,

30
00:01:25,40 --> 00:01:27,80
while newer specs don't require that.

31
00:01:27,80 --> 00:01:29,50
But the developer tools seem to prefer it,

32
00:01:29,50 --> 00:01:31,10
so I'm going to leave it in.

33
00:01:31,10 --> 00:01:33,40
And then tabbing over to the Expires field,

34
00:01:33,40 --> 00:01:35,80
I'll enter the year 2100 and tab away,

35
00:01:35,80 --> 00:01:38,20
and now that cookie's set.

36
00:01:38,20 --> 00:01:42,00
And now, going back to index.html,

37
00:01:42,00 --> 00:01:43,60
and I'm going to use the Refresh button

38
00:01:43,60 --> 00:01:46,70
next to the filter box to refresh the cookies,

39
00:01:46,70 --> 00:01:49,20
and so, now I have an earlier cookie I created

40
00:01:49,20 --> 00:01:51,40
on this specific subdomain.

41
00:01:51,40 --> 00:01:53,70
And then I have the shared cookie I just created

42
00:01:53,70 --> 00:01:56,00
on the other subdomain, with the domain set

43
00:01:56,00 --> 00:01:57,80
to the parent domain.

44
00:01:57,80 --> 00:02:00,50
So the cookie value is available to the code

45
00:02:00,50 --> 00:02:04,00
in both tabs on both subdomains, as easy as that.

