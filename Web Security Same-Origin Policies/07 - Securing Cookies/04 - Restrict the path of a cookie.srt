1
00:00:00,50 --> 00:00:02,50
- Cookies support a path directive,

2
00:00:02,50 --> 00:00:04,90
which enables you to limit the scope of a cookie

3
00:00:04,90 --> 00:00:08,90
to documents whose origin includes a specific path.

4
00:00:08,90 --> 00:00:11,10
This can be useful if, for example,

5
00:00:11,10 --> 00:00:12,90
pages in a certain part of your site

6
00:00:12,90 --> 00:00:15,80
need to share cookie values, but the rest of the domain

7
00:00:15,80 --> 00:00:18,90
or sub domain doesn't need those extra values.

8
00:00:18,90 --> 00:00:23,20
Because cookies are part of every http request and response,

9
00:00:23,20 --> 00:00:25,00
limiting cookie values to only

10
00:00:25,00 --> 00:00:26,70
those pages where they're needed

11
00:00:26,70 --> 00:00:29,50
can shave time off your http round trips,

12
00:00:29,50 --> 00:00:32,20
increasing your site's performance.

13
00:00:32,20 --> 00:00:35,90
Using the path directive, you can limit cookie values

14
00:00:35,90 --> 00:00:38,30
to documents at a specific path,

15
00:00:38,30 --> 00:00:41,50
making the cookie value available only to those documents

16
00:00:41,50 --> 00:00:46,80
as long as all other directives, such as domain, are met.

17
00:00:46,80 --> 00:00:49,60
I've opened the product detail arrangements page

18
00:00:49,60 --> 00:00:54,30
from the arrangements sub folder on the asset server.

19
00:00:54,30 --> 00:00:58,30
And then, in another tab, I have the category arrangements

20
00:00:58,30 --> 00:01:01,70
page open from that sub folder as well.

21
00:01:01,70 --> 00:01:06,90
Finally, I have the index.html page open from that server,

22
00:01:06,90 --> 00:01:12,40
which is not in the same sub folder as the other two files.

23
00:01:12,40 --> 00:01:15,10
So imagine I want to share what a user has browsed

24
00:01:15,10 --> 00:01:18,00
between pages within the arrangements sub folder,

25
00:01:18,00 --> 00:01:21,10
but that information has no relevance to pages

26
00:01:21,10 --> 00:01:24,40
in other folders or outside of sub folders.

27
00:01:24,40 --> 00:01:29,60
Opening up the application tab in the dev tools,

28
00:01:29,60 --> 00:01:34,60
and digging into the cookies for this particular domain.

29
00:01:34,60 --> 00:01:36,20
I'm going to create a new cookie

30
00:01:36,20 --> 00:01:38,40
by double-clicking in the name column.

31
00:01:38,40 --> 00:01:42,70
I'll name it lastviewed

32
00:01:42,70 --> 00:01:46,10
and I'm going to set its value to roses.

33
00:01:46,10 --> 00:01:49,10
And then in the path column I want to capture

34
00:01:49,10 --> 00:01:53,00
the specific path that this document is at,

35
00:01:53,00 --> 00:01:56,40
and that is /pages/arrangements,

36
00:01:56,40 --> 00:02:01,20
so I'm going to copy that from my URL in the address box

37
00:02:01,20 --> 00:02:05,00
and then paste that into the path box.

38
00:02:05,00 --> 00:02:07,30
I'm going to tab over to the expires value

39
00:02:07,30 --> 00:02:11,10
and enter the year 2100 to put that in the future.

40
00:02:11,10 --> 00:02:13,60
And so I have a cookie that specifically

41
00:02:13,60 --> 00:02:16,60
applies to the current path.

42
00:02:16,60 --> 00:02:18,40
And so then I'm going to switch over

43
00:02:18,40 --> 00:02:21,40
to the category arrangements page.

44
00:02:21,40 --> 00:02:23,10
I'm going to open up my dev tools

45
00:02:23,10 --> 00:02:25,50
and look at the application tab.

46
00:02:25,50 --> 00:02:29,30
And here in the cookies for the current page,

47
00:02:29,30 --> 00:02:30,10
so we can see that we have

48
00:02:30,10 --> 00:02:32,80
that lastviewed cookie that we created

49
00:02:32,80 --> 00:02:36,00
over on the product detail arrangements page,

50
00:02:36,00 --> 00:02:37,40
and that's because this page

51
00:02:37,40 --> 00:02:40,90
is also opened from the same path.

52
00:02:40,90 --> 00:02:44,60
Now, if I switch over to the index.html page,

53
00:02:44,60 --> 00:02:47,90
open my dev tools and go to application,

54
00:02:47,90 --> 00:02:52,20
and then I'm going to filter by haroku

55
00:02:52,20 --> 00:02:54,70
to filter out the linked in cookies.

56
00:02:54,70 --> 00:02:56,80
Then I'm going to refresh just to make sure.

57
00:02:56,80 --> 00:03:00,30
And so we can see that the cookie we've created for the path

58
00:03:00,30 --> 00:03:03,80
that the other two pages are located at doesn't show up here

59
00:03:03,80 --> 00:03:07,10
because this file isn't at /pages/arrangements,

60
00:03:07,10 --> 00:03:08,50
it's just at /pages.

61
00:03:08,50 --> 00:03:11,60
So even though the domain is the same, the path isn't

62
00:03:11,60 --> 00:03:15,80
and so that cookie we created doesn't apply to this page.

63
00:03:15,80 --> 00:03:17,40
So because that last viewed data

64
00:03:17,40 --> 00:03:19,40
isn't relevant for the index page,

65
00:03:19,40 --> 00:03:22,70
we prevent this page from needing to work with that data,

66
00:03:22,70 --> 00:03:24,30
and we keep the cookies for this page

67
00:03:24,30 --> 00:03:26,30
focused on only what it needs.

68
00:03:26,30 --> 00:03:28,80
This also allows you to scope cookies

69
00:03:28,80 --> 00:03:30,80
so that resources in different directories

70
00:03:30,80 --> 00:03:32,80
can use a common set of cookie names

71
00:03:32,80 --> 00:03:35,00
while maintaining unique values.

