1
00:00:00,50 --> 00:00:02,30
- [Instructor] Cookies associated with a page

2
00:00:02,30 --> 00:00:06,30
are sent with every http request from that page.

3
00:00:06,30 --> 00:00:08,70
However, sometimes you want to create cookies

4
00:00:08,70 --> 00:00:10,20
that are used by the server,

5
00:00:10,20 --> 00:00:14,00
but are not accessible on the client side of your app.

6
00:00:14,00 --> 00:00:17,30
You can do this using the httpOnly directive,

7
00:00:17,30 --> 00:00:22,10
which takes a Boolean as its value, either true or false.

8
00:00:22,10 --> 00:00:24,70
Restricting client-side access to cookies

9
00:00:24,70 --> 00:00:28,60
provides another defense against cross-site scripting.

10
00:00:28,60 --> 00:00:30,60
When cookies containing access tokens

11
00:00:30,60 --> 00:00:32,30
and other sensitive data

12
00:00:32,30 --> 00:00:35,60
are accessible via client-side JavaScript,

13
00:00:35,60 --> 00:00:38,90
they're included in the payload sent to an attacker's server

14
00:00:38,90 --> 00:00:41,20
in a cross-site scripting attack.

15
00:00:41,20 --> 00:00:44,80
Using the httpOnly directive for sensitive cookies

16
00:00:44,80 --> 00:00:47,80
adds a layer of protection from this type of attack

17
00:00:47,80 --> 00:00:50,50
as the cookies can't be read and forwarded

18
00:00:50,50 --> 00:00:52,70
even when an attacker is able to send

19
00:00:52,70 --> 00:00:57,00
an http request on the user's behalf.

20
00:00:57,00 --> 00:01:01,40
To check this out, I'm going to edit the index.js file

21
00:01:01,40 --> 00:01:05,40
from my site server

22
00:01:05,40 --> 00:01:08,40
and I'm going to generate a couple cookies on the server

23
00:01:08,40 --> 00:01:12,70
when the http response is built out.

24
00:01:12,70 --> 00:01:16,50
I'm going to scroll down just after

25
00:01:16,50 --> 00:01:21,90
my second response.set method,

26
00:01:21,90 --> 00:01:24,60
right before response.render.

27
00:01:24,60 --> 00:01:27,70
First, I'm going to create a standard cookie

28
00:01:27,70 --> 00:01:30,30
with no special directives,

29
00:01:30,30 --> 00:01:34,00
so that's response.cookie

30
00:01:34,00 --> 00:01:35,60
and the first string I'm going to set

31
00:01:35,60 --> 00:01:40,20
is lang for language and the second one will be en-US,

32
00:01:40,20 --> 00:01:44,20
which is a standard abbreviation.

33
00:01:44,20 --> 00:01:46,40
And then I want to try out something like a site token

34
00:01:46,40 --> 00:01:49,10
for a logged-in user.

35
00:01:49,10 --> 00:01:52,00
We'll do another response.cookie

36
00:01:52,00 --> 00:01:57,90
and this time I'll set a key of siteToken

37
00:01:57,90 --> 00:02:03,90
and a value of something random, 8d92,

38
00:02:03,90 --> 00:02:07,50
and then I need to add in my directives here,

39
00:02:07,50 --> 00:02:11,90
so I'm going to put curly braces to make an object,

40
00:02:11,90 --> 00:02:18,20
going to do httpOnly, camelCase, colon, true.

41
00:02:18,20 --> 00:02:21,60
And I'll save those changes

42
00:02:21,60 --> 00:02:29,30
and then I want to deploy.

43
00:02:29,30 --> 00:02:37,40
HttpOnly cookie.

44
00:02:37,40 --> 00:02:40,50
When this is deployed and I test it,

45
00:02:40,50 --> 00:02:41,90
what I expect to see is that

46
00:02:41,90 --> 00:02:44,40
when I load the main page from my site server,

47
00:02:44,40 --> 00:02:47,00
I'll see both cookies in the application tab

48
00:02:47,00 --> 00:02:48,20
of the dev tools.

49
00:02:48,20 --> 00:02:50,00
But when I check the console,

50
00:02:50,00 --> 00:02:53,00
the httpOnly cookie shouldn't be returned

51
00:02:53,00 --> 00:02:57,30
when I access the value of document.cookie.

52
00:02:57,30 --> 00:03:00,90
Back in the browser, I'm going to reload,

53
00:03:00,90 --> 00:03:05,10
and let me go ahead and filter on Hansel

54
00:03:05,10 --> 00:03:08,10
to get rid of those LinkedIn cookies,

55
00:03:08,10 --> 00:03:10,90
and so I see both the lang cookie,

56
00:03:10,90 --> 00:03:13,40
which had no special directives

57
00:03:13,40 --> 00:03:15,50
and I see the site token cookie

58
00:03:15,50 --> 00:03:17,30
and over here we see that there's a check mark

59
00:03:17,30 --> 00:03:23,30
in the http column, so this is an httpOnly cookie.

60
00:03:23,30 --> 00:03:25,40
Now I'm going to go over to the console

61
00:03:25,40 --> 00:03:31,80
and I'm going to try out document.cookie.

62
00:03:31,80 --> 00:03:34,20
What I get back is the lang cookie,

63
00:03:34,20 --> 00:03:36,50
which I set without any restrictions,

64
00:03:36,50 --> 00:03:39,00
but the value for document.cookie

65
00:03:39,00 --> 00:03:42,30
in JavaScript in the browser does not return

66
00:03:42,30 --> 00:03:45,10
that httpOnly cookie, which is also referred to

67
00:03:45,10 --> 00:03:48,10
sometimes as a server cookie or a server-only cookie.

68
00:03:48,10 --> 00:03:50,70
And so we've successfully created a cookie

69
00:03:50,70 --> 00:03:53,10
on the server end, sent it to the client

70
00:03:53,10 --> 00:03:55,80
that's not accessible by code in the client.

71
00:03:55,80 --> 00:03:58,10
And that's great because it protects users

72
00:03:58,10 --> 00:04:01,40
from malicious code accessing those sensitive cookies,

73
00:04:01,40 --> 00:04:04,60
but it still allows the browser to send them to the server

74
00:04:04,60 --> 00:04:08,00
at the same origin for legitimate purposes.

