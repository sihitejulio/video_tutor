1
00:00:00,50 --> 00:00:03,10
- [Instructor] Cookies are governed by a same-origin policy

2
00:00:03,10 --> 00:00:06,60
that has some important differences from other resources.

3
00:00:06,60 --> 00:00:09,80
Although origins are generally defined to include protocol,

4
00:00:09,80 --> 00:00:12,70
domain, subdomain, and path,

5
00:00:12,70 --> 00:00:16,00
a document can set a cookie for its subdomain,

6
00:00:16,00 --> 00:00:18,90
as well as for its parent domain.

7
00:00:18,90 --> 00:00:22,60
It can also access cookies set on both its subdomain

8
00:00:22,60 --> 00:00:25,60
and its parent's domain.

9
00:00:25,60 --> 00:00:27,80
There's one exception to this behavior,

10
00:00:27,80 --> 00:00:30,80
which is that subdomains can't set or read cookies

11
00:00:30,80 --> 00:00:34,60
on a root domain that's part of the public suffix list.

12
00:00:34,60 --> 00:00:36,70
This is a publicly available list

13
00:00:36,70 --> 00:00:38,40
that enables the owners of sites

14
00:00:38,40 --> 00:00:41,40
where subdomains are shared between different users,

15
00:00:41,40 --> 00:00:45,00
to exclude their root domains from cookie sharing.

16
00:00:45,00 --> 00:00:47,50
All major browsers incorporate this list

17
00:00:47,50 --> 00:00:49,00
into their cookie handling,

18
00:00:49,00 --> 00:00:51,80
and it makes the web a more secure place.

19
00:00:51,80 --> 00:00:53,80
For instance, if I deploy an app

20
00:00:53,80 --> 00:00:56,60
to a subdomain on herokuapp.com,

21
00:00:56,60 --> 00:01:00,30
which is the public address for all apps deployed on Heroku,

22
00:01:00,30 --> 00:01:02,80
I won't share cookies with apps at any of the

23
00:01:02,80 --> 00:01:06,60
thousands of other subdomains on herokuapp.com.

24
00:01:06,60 --> 00:01:09,50
This ensures that my app's data is kept private,

25
00:01:09,50 --> 00:01:14,20
and that cookies I create aren't overwritten by other apps.

26
00:01:14,20 --> 00:01:16,40
The protocol and port are irrelevant

27
00:01:16,40 --> 00:01:19,10
in same-origin access for a cookie.

28
00:01:19,10 --> 00:01:20,20
Whether a cookie is set

29
00:01:20,20 --> 00:01:24,70
from a document loaded over HTTP or HTTPS,

30
00:01:24,70 --> 00:01:28,00
it may be read by a document loaded from either protocol

31
00:01:28,00 --> 00:01:29,80
and over any port.

32
00:01:29,80 --> 00:01:33,00
Effectively, the domain is the only limiting component

33
00:01:33,00 --> 00:01:35,00
of the origin for cookies.

