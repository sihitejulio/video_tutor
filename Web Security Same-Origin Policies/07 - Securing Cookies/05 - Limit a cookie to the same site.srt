1
00:00:00,50 --> 00:00:02,10
- [Instructor] You can make your cookies more secure

2
00:00:02,10 --> 00:00:04,10
by specifying that they should not be sent

3
00:00:04,10 --> 00:00:06,50
as part of a request to another site.

4
00:00:06,50 --> 00:00:10,30
You implement this with the SameSite directive.

5
00:00:10,30 --> 00:00:13,80
The SameSite directive has only two allowable values,

6
00:00:13,80 --> 00:00:16,80
strict and lax, the strict setting

7
00:00:16,80 --> 00:00:19,40
prevents a cookie from being sent with any request

8
00:00:19,40 --> 00:00:22,30
that didn't originate at the current document's origin.

9
00:00:22,30 --> 00:00:24,50
The lax allows cookies to be sent

10
00:00:24,50 --> 00:00:26,70
with a request from another origin

11
00:00:26,70 --> 00:00:29,40
such as when a user clicks a link on a different site.

12
00:00:29,40 --> 00:00:31,60
However, the cookie is not included

13
00:00:31,60 --> 00:00:34,30
for any requests within the document that loads

14
00:00:34,30 --> 00:00:37,90
such as for scripts, images, or fonts.

15
00:00:37,90 --> 00:00:40,50
The SameSite directive provides one piece

16
00:00:40,50 --> 00:00:42,80
of a broad defense against cross-site

17
00:00:42,80 --> 00:00:44,80
request forgery attacks.

18
00:00:44,80 --> 00:00:47,90
Without a SameSite directive, a malformed link

19
00:00:47,90 --> 00:00:50,10
such as an image tag in an email,

20
00:00:50,10 --> 00:00:53,00
can generate a request for a sensitive operation

21
00:00:53,00 --> 00:00:56,10
to a site where user is already logged in.

22
00:00:56,10 --> 00:00:58,80
Although user credentials are not stored in cookies

23
00:00:58,80 --> 00:01:02,90
on the modern web, site store access tokens in cookies

24
00:01:02,90 --> 00:01:05,70
which verify with each HTTP request

25
00:01:05,70 --> 00:01:07,70
that the user is logged in.

26
00:01:07,70 --> 00:01:10,70
If the cookie containing user's access token

27
00:01:10,70 --> 00:01:14,50
has its SameSite directive set to strict for instance,

28
00:01:14,50 --> 00:01:18,60
the attacker's HTTP request is sent without this cookie

29
00:01:18,60 --> 00:01:20,20
meaning the user's access token

30
00:01:20,20 --> 00:01:22,90
is not included in the request.

31
00:01:22,90 --> 00:01:25,50
When the remote server receives the request,

32
00:01:25,50 --> 00:01:29,70
the request is rejected as unauthorized.

33
00:01:29,70 --> 00:01:33,60
Let's take a look at how the SameSite directive works.

34
00:01:33,60 --> 00:01:35,90
I've opened the Category Arrangements page

35
00:01:35,90 --> 00:01:40,30
from the Resources server, and scrolling down,

36
00:01:40,30 --> 00:01:43,70
the roses image is linked to the Product

37
00:01:43,70 --> 00:01:46,80
Detail Arrangements page which is also loading

38
00:01:46,80 --> 00:01:49,70
from the Resources server.

39
00:01:49,70 --> 00:01:53,20
Now in the Application tab of the Dev tools,

40
00:01:53,20 --> 00:01:55,40
I already have one cookie here

41
00:01:55,40 --> 00:01:59,50
and I can just flush that using the Clear All button

42
00:01:59,50 --> 00:02:01,30
so I'm going to use that just to get rid of

43
00:02:01,30 --> 00:02:03,50
my existing cookies and start fresh.

44
00:02:03,50 --> 00:02:05,70
Now I could create a new cookie here,

45
00:02:05,70 --> 00:02:08,10
but the tools don't actually allow direct editing

46
00:02:08,10 --> 00:02:11,70
of a few directives including SameSite.

47
00:02:11,70 --> 00:02:14,50
So instead, I'm going to switch over to the console

48
00:02:14,50 --> 00:02:19,70
to create the cookie and so I can use document.cookie

49
00:02:19,70 --> 00:02:26,20
equals token equals B six B

50
00:02:26,20 --> 00:02:34,50
and then a semicolon and my SameSite directive equals strict

51
00:02:34,50 --> 00:02:37,50
close my quote and press Enter

52
00:02:37,50 --> 00:02:40,10
Now obviously a real token would be much longer

53
00:02:40,10 --> 00:02:42,30
but this will give us a taste.

54
00:02:42,30 --> 00:02:44,50
And now switching back to the Application tab

55
00:02:44,50 --> 00:02:47,90
on the same page, I can see my cookie

56
00:02:47,90 --> 00:02:52,80
and it's even got the SameSite box filled in

57
00:02:52,80 --> 00:02:55,70
showing that we've set this value to strict.

58
00:02:55,70 --> 00:02:59,50
And I'm going to create one other cookie here just to test.

59
00:02:59,50 --> 00:03:04,10
So I'll set prev page for previous page

60
00:03:04,10 --> 00:03:07,60
and set the value to arrangements

61
00:03:07,60 --> 00:03:10,20
and I'm not going to set that as a SameSite cookie.

62
00:03:10,20 --> 00:03:15,40
And now I do want to set my expires value here to 2,100

63
00:03:15,40 --> 00:03:18,00
and I'll go ahead and do that for my SameSite cookie

64
00:03:18,00 --> 00:03:20,20
just for completeness.

65
00:03:20,20 --> 00:03:22,60
Now I want to navigate over to the Product Detail

66
00:03:22,60 --> 00:03:24,20
Arrangements page and I want to see

67
00:03:24,20 --> 00:03:26,50
what cookies are sent in that request,

68
00:03:26,50 --> 00:03:29,40
so first I need to click the Network tab

69
00:03:29,40 --> 00:03:32,40
to start recording network traffic.

70
00:03:32,40 --> 00:03:36,10
And then I'll click the roses image

71
00:03:36,10 --> 00:03:40,40
and down here in the list of all the HTTP requests

72
00:03:40,40 --> 00:03:44,30
I'll click the first one which is for the HTML file

73
00:03:44,30 --> 00:03:49,40
and I'll scroll down and I can see

74
00:03:49,40 --> 00:03:53,60
that in my request headers we have the cookie header

75
00:03:53,60 --> 00:03:56,80
and its value includes both the token

76
00:03:56,80 --> 00:03:59,70
and prev page cookie values.

77
00:03:59,70 --> 00:04:02,60
And those are both set because this request

78
00:04:02,60 --> 00:04:06,10
meets both the domain and path criteria

79
00:04:06,10 --> 00:04:09,40
and the HTTP request was from this site

80
00:04:09,40 --> 00:04:13,50
so since I clicked the link on hpassets.herokuapp.com

81
00:04:13,50 --> 00:04:16,10
it's that same site cookie, the token cookie

82
00:04:16,10 --> 00:04:18,40
was included in the request.

83
00:04:18,40 --> 00:04:19,60
And now over here in this tab,

84
00:04:19,60 --> 00:04:21,40
I have the main page that I've opened

85
00:04:21,40 --> 00:04:25,60
from my Site server, not my Resources server,

86
00:04:25,60 --> 00:04:30,00
so this is opening from another domain.

87
00:04:30,00 --> 00:04:32,40
Now this page has a link to the Product Detail

88
00:04:32,40 --> 00:04:35,00
Arrangements page on my Resources server

89
00:04:35,00 --> 00:04:39,00
and that's down here on the tulips image.

90
00:04:39,00 --> 00:04:43,30
And so once again I want to switch over to that Network tab

91
00:04:43,30 --> 00:04:46,70
and then I'm going to click that tulips image

92
00:04:46,70 --> 00:04:50,70
and that page loads, and then in the Network tab

93
00:04:50,70 --> 00:04:53,10
I'm going to scroll up to that first request and click it

94
00:04:53,10 --> 00:04:56,70
to see the details and I'm going to scroll down

95
00:04:56,70 --> 00:04:59,00
and look at the request headers,

96
00:04:59,00 --> 00:05:02,60
and here's the cookie header, and it only contains

97
00:05:02,60 --> 00:05:04,80
the prev page cookie which was the one

98
00:05:04,80 --> 00:05:07,90
that I did not specify as a SameSite cookie.

99
00:05:07,90 --> 00:05:12,00
And so here we notice that the restricted cookie

100
00:05:12,00 --> 00:05:15,10
is not sent because this request

101
00:05:15,10 --> 00:05:20,20
came from a different origin, it came from, in my case,

102
00:05:20,20 --> 00:05:22,60
hanselandpetal.heroku.com

103
00:05:22,60 --> 00:05:26,40
rather than from hpassets.herokuapp.com

104
00:05:26,40 --> 00:05:28,90
which is where this file is hosted.

105
00:05:28,90 --> 00:05:31,30
And again, this protects users

106
00:05:31,30 --> 00:05:37,00
from CSRF attacks and other security threats.

