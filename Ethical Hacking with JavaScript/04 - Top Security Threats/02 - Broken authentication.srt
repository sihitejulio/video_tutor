1
00:00:00,50 --> 00:00:02,10
- [Instructor] Broken authentication is where

2
00:00:02,10 --> 00:00:05,60
a hacker is able to see or exploit user information

3
00:00:05,60 --> 00:00:08,60
such as username and password.

4
00:00:08,60 --> 00:00:10,40
It is extremely dangerous

5
00:00:10,40 --> 00:00:11,90
and if you have an application with

6
00:00:11,90 --> 00:00:14,00
poor authentication practices,

7
00:00:14,00 --> 00:00:17,00
you're putting your application and your users at risk

8
00:00:17,00 --> 00:00:19,50
and potential legal liabilities.

9
00:00:19,50 --> 00:00:22,50
If any of your code provides clear text representation

10
00:00:22,50 --> 00:00:24,60
of your user's password,

11
00:00:24,60 --> 00:00:27,70
either publicly or when the user's logged in,

12
00:00:27,70 --> 00:00:31,30
you're a victim of your own bad authentication practices.

13
00:00:31,30 --> 00:00:33,90
Here's a few examples of broken authentication

14
00:00:33,90 --> 00:00:35,10
and as you can imagine,

15
00:00:35,10 --> 00:00:37,40
what hackers could exploit from these.

16
00:00:37,40 --> 00:00:38,90
Text passwords,

17
00:00:38,90 --> 00:00:41,00
if your code doesn't hash the passwords

18
00:00:41,00 --> 00:00:44,40
through the use of bcrypt or similar libraries,

19
00:00:44,40 --> 00:00:46,20
you're exposing your user's password

20
00:00:46,20 --> 00:00:48,20
for anyone to exploit them.

21
00:00:48,20 --> 00:00:50,40
Session IDs in the browser,

22
00:00:50,40 --> 00:00:53,00
a session ID provides information about the user

23
00:00:53,00 --> 00:00:55,10
logged into the application.

24
00:00:55,10 --> 00:00:58,00
A hacker could use the session ID to access

25
00:00:58,00 --> 00:01:00,50
user or application data.

26
00:01:00,50 --> 00:01:03,80
Use proper session libraries for that purpose.

27
00:01:03,80 --> 00:01:05,70
Unsecure HTTP,

28
00:01:05,70 --> 00:01:09,10
if you're still not using HTTPS protocols,

29
00:01:09,10 --> 00:01:11,60
and your user's connected to your site

30
00:01:11,60 --> 00:01:13,40
through an unsecure network,

31
00:01:13,40 --> 00:01:14,90
like, a public Wi-Fi,

32
00:01:14,90 --> 00:01:17,50
then a hacker could grab the session IDs

33
00:01:17,50 --> 00:01:20,10
and get any information from them.

34
00:01:20,10 --> 00:01:21,80
In a similar scenario,

35
00:01:21,80 --> 00:01:24,50
you could be providing access to users to areas

36
00:01:24,50 --> 00:01:26,10
they're not supposed to.

37
00:01:26,10 --> 00:01:27,80
Having a strong plan of action

38
00:01:27,80 --> 00:01:32,10
as to who should have access to what areas is crucial.

39
00:01:32,10 --> 00:01:34,10
Creating roles within your application

40
00:01:34,10 --> 00:01:37,50
and providing the proper access rights to these roles

41
00:01:37,50 --> 00:01:38,60
should be in your planning

42
00:01:38,60 --> 00:01:43,60
when creating your application and authentication practices.

43
00:01:43,60 --> 00:01:46,80
So in any case, always use the proper libraries

44
00:01:46,80 --> 00:01:49,40
to connect your users securely to your site

45
00:01:49,40 --> 00:01:52,00
or leverage Auth0 to help you secure your

46
00:01:52,00 --> 00:01:54,00
authentication workflow.

