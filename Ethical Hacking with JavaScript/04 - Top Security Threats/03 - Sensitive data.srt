1
00:00:00,50 --> 00:00:02,30
- [Instructor] Like broken authentication,

2
00:00:02,30 --> 00:00:05,10
having a plan to properly hide sensitive user

3
00:00:05,10 --> 00:00:08,00
or application data is crucial.

4
00:00:08,00 --> 00:00:10,90
Take a look at all the data available in your application.

5
00:00:10,90 --> 00:00:12,80
What are the areas that would be safe

6
00:00:12,80 --> 00:00:14,20
for public consumption

7
00:00:14,20 --> 00:00:17,60
and the ones that are private or could be exploited?

8
00:00:17,60 --> 00:00:19,70
For example, in general,

9
00:00:19,70 --> 00:00:22,60
all user data should be considered sensitive

10
00:00:22,60 --> 00:00:24,70
and hidden from the public eye.

11
00:00:24,70 --> 00:00:27,50
Then take a look at the data users generate.

12
00:00:27,50 --> 00:00:30,60
Can it be public or not?

13
00:00:30,60 --> 00:00:32,90
Once you have a plan, you need to implement some

14
00:00:32,90 --> 00:00:35,80
of the strategies mentioned in the previous video.

15
00:00:35,80 --> 00:00:39,60
Proper crypto hashing, HTTPS protocols,

16
00:00:39,60 --> 00:00:43,30
and the use of advanced tools like Jscrambler.

17
00:00:43,30 --> 00:00:47,00
Jscrambler goes a step further than just a simple crypto.

18
00:00:47,00 --> 00:00:49,70
It works at a deeper level with your code logic

19
00:00:49,70 --> 00:00:52,00
and scrambles your code and data

20
00:00:52,00 --> 00:00:55,70
so it isn't easy to reverse engineer your minified code

21
00:00:55,70 --> 00:00:58,20
and figure out what function does what.

22
00:00:58,20 --> 00:01:00,80
At the end of the day, you need to have a plan

23
00:01:00,80 --> 00:01:05,00
to fully secure any sensitive data in your application.

