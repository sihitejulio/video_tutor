1
00:00:00,50 --> 00:00:02,30
- [Instructor] When you're building out your application,

2
00:00:02,30 --> 00:00:04,40
more often than not you'll be using packages

3
00:00:04,40 --> 00:00:06,20
that offer you extensibility

4
00:00:06,20 --> 00:00:08,80
and leverage other components from these libraries.

5
00:00:08,80 --> 00:00:11,10
This extra help is great,

6
00:00:11,10 --> 00:00:13,80
but can sometimes lead you into an insecure path

7
00:00:13,80 --> 00:00:15,10
with your application.

8
00:00:15,10 --> 00:00:17,40
A great example is using a navigation

9
00:00:17,40 --> 00:00:20,60
or routing library with your react application,

10
00:00:20,60 --> 00:00:22,10
where you'd be leveraging their components

11
00:00:22,10 --> 00:00:24,80
to build out your routing or navigation areas

12
00:00:24,80 --> 00:00:26,20
with their components.

13
00:00:26,20 --> 00:00:28,20
This is a simple example, but could occur

14
00:00:28,20 --> 00:00:29,90
with any kind of packages.

15
00:00:29,90 --> 00:00:32,00
And I'm not saying that this particular package

16
00:00:32,00 --> 00:00:33,90
has problems, but what I'm saying

17
00:00:33,90 --> 00:00:36,60
is whenever you're leveraging other packages,

18
00:00:36,60 --> 00:00:40,10
you could have issues in those components.

19
00:00:40,10 --> 00:00:42,60
And the best way to identify these issues

20
00:00:42,60 --> 00:00:45,70
is when you test with Snyk or Retire js

21
00:00:45,70 --> 00:00:48,70
where you get a good idea of which packages are insecure.

22
00:00:48,70 --> 00:00:51,30
Unless you only build your own components,

23
00:00:51,30 --> 00:00:54,50
you need to be vigilant of the packages you use

24
00:00:54,50 --> 00:00:56,80
and must at all times validate you are using

25
00:00:56,80 --> 00:00:59,40
secure components in your application.

26
00:00:59,40 --> 00:01:02,20
And when you find some that are troublesome,

27
00:01:02,20 --> 00:01:05,00
figure out if it's not just a simple update

28
00:01:05,00 --> 00:01:07,80
of the package and the syntax used.

29
00:01:07,80 --> 00:01:10,40
And if not, then plan to replace these packages

30
00:01:10,40 --> 00:01:12,00
or write your own.

