1
00:00:00,50 --> 00:00:02,40
- [Instructor] I can't stress this enough.

2
00:00:02,40 --> 00:00:05,80
If your application isn't being monitored by any tools

3
00:00:05,80 --> 00:00:09,00
or you get insufficient logging mechanisms in place,

4
00:00:09,00 --> 00:00:11,10
with alerts when something suspicious

5
00:00:11,10 --> 00:00:13,10
in your resource is being executed,

6
00:00:13,10 --> 00:00:16,00
how will you prevent hackers from completely

7
00:00:16,00 --> 00:00:20,40
getting control of your application or pulling data from it?

8
00:00:20,40 --> 00:00:23,80
Take this scenario, on average your website or application

9
00:00:23,80 --> 00:00:26,10
gets about 200 users per day.

10
00:00:26,10 --> 00:00:29,80
And you allocated resource for that workload.

11
00:00:29,80 --> 00:00:32,20
But, all of a sudden, your application jumps

12
00:00:32,20 --> 00:00:36,10
to 500 users, then 1,000 or more.

13
00:00:36,10 --> 00:00:39,30
How will you know about the sudden surge in users?

14
00:00:39,30 --> 00:00:41,50
How will you be able to react to it

15
00:00:41,50 --> 00:00:44,00
and even worse, how will you determine

16
00:00:44,00 --> 00:00:46,50
if this is an official user growth

17
00:00:46,50 --> 00:00:51,40
or a hacker sending his minions for a typical DUS attack?

18
00:00:51,40 --> 00:00:55,50
Monitoring and logging is key to supervisor resources,

19
00:00:55,50 --> 00:00:58,40
user growth, and yes, hackers on the prowl

20
00:00:58,40 --> 00:01:00,50
towards your application.

21
00:01:00,50 --> 00:01:02,90
Most host providers have a decent amount of tools

22
00:01:02,90 --> 00:01:04,60
you can use for this purpose.

23
00:01:04,60 --> 00:01:07,00
And please, do use them.

24
00:01:07,00 --> 00:01:11,80
Set alerts so you get notifications for unusual behaviors.

25
00:01:11,80 --> 00:01:15,50
And make sure you set some type of user-based monitoring

26
00:01:15,50 --> 00:01:18,30
and you can either use pre-built resources

27
00:01:18,30 --> 00:01:21,70
such as Google Analytics, Elastic Search,

28
00:01:21,70 --> 00:01:23,70
or build your own within your application.

29
00:01:23,70 --> 00:01:26,60
But do get something in place.

30
00:01:26,60 --> 00:01:29,20
It is often better to have more information,

31
00:01:29,20 --> 00:01:32,80
set many alerts, than to get caught dealing with

32
00:01:32,80 --> 00:01:34,80
lost resources because a hacker

33
00:01:34,80 --> 00:01:37,90
exploited them one way or another.

34
00:01:37,90 --> 00:01:41,00
This is always a good tool to have to detect

35
00:01:41,00 --> 00:01:45,00
the delivery and control phase of the cyber kill chain

36
00:01:45,00 --> 00:01:47,00
of the plan we discussed earlier.

