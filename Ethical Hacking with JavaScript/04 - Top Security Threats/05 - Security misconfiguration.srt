1
00:00:00,50 --> 00:00:02,80
- [Instructor] Security misconfiguration usually occurs

2
00:00:02,80 --> 00:00:06,30
from a lack of proper settings in your application.

3
00:00:06,30 --> 00:00:08,70
Or, exposed information from the server side.

4
00:00:08,70 --> 00:00:11,20
And, they happen typically when a developer

5
00:00:11,20 --> 00:00:15,10
one, publishes the developer version of the server.

6
00:00:15,10 --> 00:00:16,90
This could open all kinds of settings

7
00:00:16,90 --> 00:00:18,60
for hackers to exploit,

8
00:00:18,60 --> 00:00:21,90
and also hinder the performance of your application.

9
00:00:21,90 --> 00:00:24,50
Number two, leave debugging on,

10
00:00:24,50 --> 00:00:28,80
or even worse, console log sensitive data in the client.

11
00:00:28,80 --> 00:00:31,60
From time to time, I test applications and sites

12
00:00:31,60 --> 00:00:34,50
I use often, and sometimes it's very easy

13
00:00:34,50 --> 00:00:38,30
to manipulate the data with a few scripts in the console.

14
00:00:38,30 --> 00:00:40,10
Be wary of this issue.

15
00:00:40,10 --> 00:00:43,90
Number three, use default logins and password.

16
00:00:43,90 --> 00:00:48,00
Example, admin login with an admin password.

17
00:00:48,00 --> 00:00:50,00
This happens a lot in the world of bloggers

18
00:00:50,00 --> 00:00:52,40
or many sites from templates.

19
00:00:52,40 --> 00:00:54,70
Always use complex passwords,

20
00:00:54,70 --> 00:00:57,70
or use a tool for managing your password.

21
00:00:57,70 --> 00:01:01,10
Number four, wide open folder, or code access

22
00:01:01,10 --> 00:01:04,70
due to improper restrictions and access controls.

23
00:01:04,70 --> 00:01:06,50
Every section of your application

24
00:01:06,50 --> 00:01:11,00
should have roles, and access controls to match those roles.

25
00:01:11,00 --> 00:01:14,80
So, the right data is shown to the right people.

26
00:01:14,80 --> 00:01:18,10
In general, when setting up your server variables,

27
00:01:18,10 --> 00:01:21,00
which usually have some credentials for your host,

28
00:01:21,00 --> 00:01:23,80
always use files that are protected,

29
00:01:23,80 --> 00:01:25,80
and not published to the host.

30
00:01:25,80 --> 00:01:29,00
And, never commit those files to your repos.

