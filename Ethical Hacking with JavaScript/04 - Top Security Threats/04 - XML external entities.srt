1
00:00:00,60 --> 00:00:02,20
- [Instructor] XML External Entities

2
00:00:02,20 --> 00:00:05,30
have been promoted to a top 10 issue on OWASP

3
00:00:05,30 --> 00:00:07,00
and for a good reason.

4
00:00:07,00 --> 00:00:09,10
This attack occurs when an XML document

5
00:00:09,10 --> 00:00:13,20
is malformed and could be exploited for DOS attacks.

6
00:00:13,20 --> 00:00:16,50
This is what a malformed XML document looks like.

7
00:00:16,50 --> 00:00:19,20
And to detect malformed XML documents you have,

8
00:00:19,20 --> 00:00:21,70
most editors will be able to detect them.

9
00:00:21,70 --> 00:00:23,80
But if it doesn't, or one was added

10
00:00:23,80 --> 00:00:25,60
to your application or website,

11
00:00:25,60 --> 00:00:27,80
then you have a problem.

12
00:00:27,80 --> 00:00:29,80
In this scenario, the attacker exploited

13
00:00:29,80 --> 00:00:33,30
the XML malformed document that takes a bit more time

14
00:00:33,30 --> 00:00:36,10
to process due to its structure,

15
00:00:36,10 --> 00:00:38,10
and then leveraged this bad XML document

16
00:00:38,10 --> 00:00:41,60
to render the resource processing unit useless,

17
00:00:41,60 --> 00:00:44,90
therefore denying its users of the resource,

18
00:00:44,90 --> 00:00:49,30
which is a denial of service or DOS attack.

19
00:00:49,30 --> 00:00:53,30
If in your application you are using XML documents,

20
00:00:53,30 --> 00:00:55,90
make sure you're using proper syntax

21
00:00:55,90 --> 00:00:57,90
as you could be subjecting your users

22
00:00:57,90 --> 00:01:01,40
to not only slower performance, but also opening up

23
00:01:01,40 --> 00:01:04,10
an opportunity for a hacker to attack your systems

24
00:01:04,10 --> 00:01:06,00
with a DOS attack.

