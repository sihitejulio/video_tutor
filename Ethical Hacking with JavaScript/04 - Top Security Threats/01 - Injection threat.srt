1
00:00:00,50 --> 00:00:02,50
- [Instructor] There are many injection attacks.

2
00:00:02,50 --> 00:00:06,30
Such as SQL injection and JavaScript injections.

3
00:00:06,30 --> 00:00:09,60
And one of the most common security issue in this category

4
00:00:09,60 --> 00:00:13,10
is referred to as cross-site scripting attacks.

5
00:00:13,10 --> 00:00:15,60
This attack is basically when malicious code

6
00:00:15,60 --> 00:00:19,00
is executed inside of a user input.

7
00:00:19,00 --> 00:00:21,30
The danger in this threat is automated code

8
00:00:21,30 --> 00:00:23,60
that goes through your public pages

9
00:00:23,60 --> 00:00:27,60
and sends code that executes once the form is submitted.

10
00:00:27,60 --> 00:00:30,00
Many frameworks such as React and Angular

11
00:00:30,00 --> 00:00:32,10
have means to escape the bad code

12
00:00:32,10 --> 00:00:34,30
and it submits the input as a string.

13
00:00:34,30 --> 00:00:37,40
But not all frameworks are made equal.

14
00:00:37,40 --> 00:00:38,70
And if you go to this website,

15
00:00:38,70 --> 00:00:41,40
and you scroll down until you see Show Demo,

16
00:00:41,40 --> 00:00:44,90
this is a perfect example of a cross-site scripting attack

17
00:00:44,90 --> 00:00:47,90
and I often use that example to show you how it works.

18
00:00:47,90 --> 00:00:50,80
So if you click on Show Demo,

19
00:00:50,80 --> 00:00:52,20
you can do a typical search here.

20
00:00:52,20 --> 00:00:55,30
So let's go and do test.

21
00:00:55,30 --> 00:00:57,70
And this is the typical result.

22
00:00:57,70 --> 00:00:59,50
But let's see what happens.

23
00:00:59,50 --> 00:01:03,70
This is an input, so what if did something like this?

24
00:01:03,70 --> 00:01:09,30
Script and then I do an alert method,

25
00:01:09,30 --> 00:01:16,20
and pass let's say, Hello hackers.

26
00:01:16,20 --> 00:01:19,40
And then I close out the script.

27
00:01:19,40 --> 00:01:23,80
If I click search, most modern application

28
00:01:23,80 --> 00:01:26,90
or secured application would literally pass

29
00:01:26,90 --> 00:01:32,00
this as a string and escape the potential threat.

30
00:01:32,00 --> 00:01:35,80
But if your application is not protected

31
00:01:35,80 --> 00:01:39,00
against that, it would do something like this.

32
00:01:39,00 --> 00:01:41,40
And run an alert inside of my browser.

33
00:01:41,40 --> 00:01:43,20
Which is a big issue.

34
00:01:43,20 --> 00:01:48,30
And this says an embedded page at xss-doc.appspot.com

35
00:01:48,30 --> 00:01:50,80
says Hello hackers.

36
00:01:50,80 --> 00:01:52,70
So I can run any type of script I want

37
00:01:52,70 --> 00:01:55,80
in here since this is possible.

38
00:01:55,80 --> 00:01:59,00
So if you think about it any hackers can literally

39
00:01:59,00 --> 00:02:01,70
run whatever the code they want in here

40
00:02:01,70 --> 00:02:05,50
and in fact or have an impact on your application.

41
00:02:05,50 --> 00:02:07,10
So when you see a sign of this issue

42
00:02:07,10 --> 00:02:10,90
in your reconnaissance, fix this as soon as possible.

43
00:02:10,90 --> 00:02:14,00
As it puts your application at the mercy of hackers

44
00:02:14,00 --> 00:02:15,60
and they can do some damage.

45
00:02:15,60 --> 00:02:19,80
In other words, check you inputs for any malicious code

46
00:02:19,80 --> 00:02:23,00
and follow the recommended fix by the tools you use.

