1
00:00:00,50 --> 00:00:01,40
- [Instructor] Before we talk

2
00:00:01,40 --> 00:00:03,70
about insecure deserialization,

3
00:00:03,70 --> 00:00:06,20
let's talk about what deserialization is,

4
00:00:06,20 --> 00:00:08,40
if you're not familiar with the term.

5
00:00:08,40 --> 00:00:11,90
In short, deserialization is the transformation of data

6
00:00:11,90 --> 00:00:14,30
coming from a file or the network,

7
00:00:14,30 --> 00:00:17,10
typically from a JSON or XML format,

8
00:00:17,10 --> 00:00:19,40
into an object that your application can read.

9
00:00:19,40 --> 00:00:21,70
So, when you hear about serialization,

10
00:00:21,70 --> 00:00:24,70
well it's the opposite, where the object is serialized

11
00:00:24,70 --> 00:00:26,90
into readable JSON format,

12
00:00:26,90 --> 00:00:29,30
when transferred through the network.

13
00:00:29,30 --> 00:00:30,80
Most frameworks have methods

14
00:00:30,80 --> 00:00:33,60
to do this automatically for us.

15
00:00:33,60 --> 00:00:37,00
So, what is insecure deserialization?

16
00:00:37,00 --> 00:00:39,30
It's when hackers exploit un-trusted data

17
00:00:39,30 --> 00:00:42,30
to render the applications resources useless,

18
00:00:42,30 --> 00:00:45,50
like a DOS attack, or even execute code

19
00:00:45,50 --> 00:00:47,50
inside your application.

20
00:00:47,50 --> 00:00:49,80
To remedy this, there are several ways

21
00:00:49,80 --> 00:00:51,90
and packages that exist.

22
00:00:51,90 --> 00:00:53,60
Feel free to use which one you prefer,

23
00:00:53,60 --> 00:00:57,10
when serializing and deserializing your data.

24
00:00:57,10 --> 00:01:00,70
As long as the don't use the JS method, eval,

25
00:01:00,70 --> 00:01:02,90
you should be fine in most cases,

26
00:01:02,90 --> 00:01:04,90
and if you're looking for suggestions,

27
00:01:04,90 --> 00:01:07,20
do a search for serialize on npm,

28
00:01:07,20 --> 00:01:10,00
and look for the most updated and active packages.

