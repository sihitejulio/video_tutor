1
00:00:00,50 --> 00:00:02,30
- [Instructor] Many of my security base courses

2
00:00:02,30 --> 00:00:05,00
only scratch the surface of the topics

3
00:00:05,00 --> 00:00:06,60
when it comes to security.

4
00:00:06,60 --> 00:00:09,60
You should always be on the lookout for new threats,

5
00:00:09,60 --> 00:00:12,50
analyze how your code is doing against those threats,

6
00:00:12,50 --> 00:00:14,90
and find the best possible fix

7
00:00:14,90 --> 00:00:17,60
based on the latest recommendations.

8
00:00:17,60 --> 00:00:19,50
Keep that in mind whenever you're learning more

9
00:00:19,50 --> 00:00:23,10
about security, it's an ongoing process.

10
00:00:23,10 --> 00:00:26,70
So, needless to say, it's a great idea to arm yourself

11
00:00:26,70 --> 00:00:30,50
with information by exploring more of the security courses.

12
00:00:30,50 --> 00:00:33,50
Simply do a search under security, hacking,

13
00:00:33,50 --> 00:00:35,70
or even language essentials.

14
00:00:35,70 --> 00:00:38,70
Watching these courses along with constant vigilance

15
00:00:38,70 --> 00:00:41,10
of your code will go a long way to reducing

16
00:00:41,10 --> 00:00:43,80
the vulnerability of your code.

17
00:00:43,80 --> 00:00:46,10
I would also subscribe to the language

18
00:00:46,10 --> 00:00:48,30
and security base feeds for,

19
00:00:48,30 --> 00:00:51,50
in the case of this course, in JavaScript.

20
00:00:51,50 --> 00:00:53,40
This will allow you to be well informed

21
00:00:53,40 --> 00:00:55,60
of the latest updates around your language

22
00:00:55,60 --> 00:00:58,70
and what dangers lurk in your code.

23
00:00:58,70 --> 00:01:01,50
An informed developer makes for better applications

24
00:01:01,50 --> 00:01:05,10
for their users, keep that in mind.

25
00:01:05,10 --> 00:01:07,00
And finally, share the knowledge

26
00:01:07,00 --> 00:01:09,10
with other developers you work with.

27
00:01:09,10 --> 00:01:11,10
I can't stress this enough.

28
00:01:11,10 --> 00:01:13,30
Well informed teams, well,

29
00:01:13,30 --> 00:01:16,30
makes better applications for users.

30
00:01:16,30 --> 00:01:17,70
Thanks for taking my course

31
00:01:17,70 --> 00:01:19,00
and I'll see you in a bit!

