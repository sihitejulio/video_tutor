1
00:00:00,60 --> 00:00:02,70
- [Instructor] Retire.js is another scanning tool

2
00:00:02,70 --> 00:00:04,40
you can use to scan your application

3
00:00:04,40 --> 00:00:07,30
for known issues and get some ideas as to what is

4
00:00:07,30 --> 00:00:10,60
the security issue in the code, so let me demonstrate.

5
00:00:10,60 --> 00:00:13,70
So you can go to retirejs.github.io

6
00:00:13,70 --> 00:00:16,40
and get more information about Retire.js,

7
00:00:16,40 --> 00:00:18,90
but the way it works is basically from the command line,

8
00:00:18,90 --> 00:00:21,50
so, let's go back to our application,

9
00:00:21,50 --> 00:00:26,40
and what I'm going to do is clear that up here.

10
00:00:26,40 --> 00:00:30,40
And then, what you need to do is install globally Retire.js,

11
00:00:30,40 --> 00:00:32,60
so what we're going to do is do a sudo,

12
00:00:32,60 --> 00:00:36,20
so for those that don't have admin rights to their system,

13
00:00:36,20 --> 00:00:39,30
you want to do a sudo first.

14
00:00:39,30 --> 00:00:42,10
npm installed

15
00:00:42,10 --> 00:00:44,60
dash g, for global,

16
00:00:44,60 --> 00:00:48,90
and then do retire.

17
00:00:48,90 --> 00:00:51,70
And then, let's enter the password,

18
00:00:51,70 --> 00:00:56,20
and now I have Retire.js install globally in my system.

19
00:00:56,20 --> 00:01:00,00
So now what I want to do is use Retire to scan my application

20
00:01:00,00 --> 00:01:04,10
and it's very simple, you just write retire

21
00:01:04,10 --> 00:01:06,80
and then enter.

22
00:01:06,80 --> 00:01:09,90
So now it's going to go and download the information it needs

23
00:01:09,90 --> 00:01:12,50
to scan your application, and then

24
00:01:12,50 --> 00:01:16,50
it's going to scan your application top to bottom

25
00:01:16,50 --> 00:01:18,70
and then, you'll get some results.

26
00:01:18,70 --> 00:01:20,80
So if you scroll quickly through the results,

27
00:01:20,80 --> 00:01:24,60
these are all the issues in this application that we have.

28
00:01:24,60 --> 00:01:27,90
So, for example if we take a look at url parse

29
00:01:27,90 --> 00:01:31,30
it has a known issue with severity high,

30
00:01:31,30 --> 00:01:34,40
an open redirect, and then you can get

31
00:01:34,40 --> 00:01:36,80
more information on this particular item here

32
00:01:36,80 --> 00:01:39,80
so if you do command click,

33
00:01:39,80 --> 00:01:42,50
you're going to go directly to where you can get

34
00:01:42,50 --> 00:01:45,70
more information on the issue and how to fix it.

35
00:01:45,70 --> 00:01:49,10
So you can scroll down and get the fix,

36
00:01:49,10 --> 00:01:51,00
and so on and so forth.

37
00:01:51,00 --> 00:01:53,10
And then you have the same information available

38
00:01:53,10 --> 00:01:56,20
for every known issues in your application,

39
00:01:56,20 --> 00:02:01,70
so if we scroll down we can take a look at this guy here.

40
00:02:01,70 --> 00:02:05,70
So has a severity of low, prototype pollution attack

41
00:02:05,70 --> 00:02:09,00
and then you can take a look again at the detail by doing

42
00:02:09,00 --> 00:02:13,90
command click, and on Windows I believe is control click.

43
00:02:13,90 --> 00:02:16,50
And then scroll down and get the exact same information

44
00:02:16,50 --> 00:02:23,50
so what is the issue, how to fix it, and so on so forth.

45
00:02:23,50 --> 00:02:26,50
So Retire.js offers similar information

46
00:02:26,50 --> 00:02:28,60
as you would get from Snyk,

47
00:02:28,60 --> 00:02:31,10
so the major difference in between the two is that sometimes

48
00:02:31,10 --> 00:02:33,90
you're going to need both to find all the issues

49
00:02:33,90 --> 00:02:36,40
that are known inside of your application,

50
00:02:36,40 --> 00:02:38,40
so I would use a combination of both,

51
00:02:38,40 --> 00:02:40,90
and get the information from both and then

52
00:02:40,90 --> 00:02:45,00
make decisions based on all the information that you have.

