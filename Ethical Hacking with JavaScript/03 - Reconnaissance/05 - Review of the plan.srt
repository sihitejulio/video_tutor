1
00:00:00,50 --> 00:00:02,20
- [Instructor] Now we have done some reconnaissance

2
00:00:02,20 --> 00:00:05,10
with tools to scan our code, and/or you have

3
00:00:05,10 --> 00:00:09,30
a tool like AppSensor monitoring your activity.

4
00:00:09,30 --> 00:00:10,50
So if we take a look at the plan

5
00:00:10,50 --> 00:00:12,70
at this stage, we have two options.

6
00:00:12,70 --> 00:00:14,70
Depending on what your client is asking,

7
00:00:14,70 --> 00:00:18,70
or your internal teams, either fix the security issues,

8
00:00:18,70 --> 00:00:22,00
which is most likely the option that you're going to take,

9
00:00:22,00 --> 00:00:26,60
or exploit them to show potential dangers and the damage.

10
00:00:26,60 --> 00:00:28,70
And at this stage it will all depend on

11
00:00:28,70 --> 00:00:32,00
the client's objectives, or your internal teams'.

12
00:00:32,00 --> 00:00:34,80
But in most cases, they ask to fix the issues,

13
00:00:34,80 --> 00:00:37,70
and test again, and then stress test

14
00:00:37,70 --> 00:00:39,90
the application further by finding

15
00:00:39,90 --> 00:00:43,30
and seeing if there are ways to exploit the system.

16
00:00:43,30 --> 00:00:46,00
For the purpose of teaching you about the issues,

17
00:00:46,00 --> 00:00:48,50
we'll move on to explore the issues you may find

18
00:00:48,50 --> 00:00:52,00
as you scan the application in the reconnaissance stage.

