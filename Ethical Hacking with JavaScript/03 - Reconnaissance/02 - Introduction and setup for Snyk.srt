1
00:00:00,70 --> 00:00:02,50
- [Narrator] Snyk, is a great tool to help

2
00:00:02,50 --> 00:00:05,30
find potential issues in your dependencies.

3
00:00:05,30 --> 00:00:07,70
It does it automatically as you make updates

4
00:00:07,70 --> 00:00:09,10
to your repository.

5
00:00:09,10 --> 00:00:10,60
And it provides a lot more details

6
00:00:10,60 --> 00:00:12,40
on vulnerable dependencies,

7
00:00:12,40 --> 00:00:14,00
than you get with npm audit

8
00:00:14,00 --> 00:00:16,30
and the specific exploit that occur.

9
00:00:16,30 --> 00:00:18,60
So let me demonstrate how it works.

10
00:00:18,60 --> 00:00:19,40
So the first thing you need to do

11
00:00:19,40 --> 00:00:21,60
is to make sure that whatever is

12
00:00:21,60 --> 00:00:23,30
the application you want to test

13
00:00:23,30 --> 00:00:27,10
is in the GitHub repo or another repository,

14
00:00:27,10 --> 00:00:31,00
or services like BitBucket or GitLab.

15
00:00:31,00 --> 00:00:32,40
So the first thing you want to make sure

16
00:00:32,40 --> 00:00:36,10
is the application that you want to test is on a GitHub repo

17
00:00:36,10 --> 00:00:39,50
or any services that are similar to GitHub.

18
00:00:39,50 --> 00:00:41,80
Once you have the application in the repo

19
00:00:41,80 --> 00:00:44,70
you can log in directly on Snyk

20
00:00:44,70 --> 00:00:46,50
and you can log in on with GitHub.

21
00:00:46,50 --> 00:00:49,10
You can also create an account with your email,

22
00:00:49,10 --> 00:00:52,20
but in this case I want to log in with my GitHub account

23
00:00:52,20 --> 00:00:57,40
so I can get access to GitHub immediately.

24
00:00:57,40 --> 00:00:58,70
Once I'm logged in,

25
00:00:58,70 --> 00:01:01,10
and you may have to go through several other screens

26
00:01:01,10 --> 00:01:04,60
to make sure that you authorize GitHub to connect to Snyk.

27
00:01:04,60 --> 00:01:06,30
I already done that,

28
00:01:06,30 --> 00:01:08,70
and I already have a package here

29
00:01:08,70 --> 00:01:12,60
for a project that is validated by Snyk.

30
00:01:12,60 --> 00:01:15,50
When you first log in you won't see this,

31
00:01:15,50 --> 00:01:18,70
so you may have to add more projects or add

32
00:01:18,70 --> 00:01:20,60
a brand new project and your screen may look

33
00:01:20,60 --> 00:01:22,10
something like this.

34
00:01:22,10 --> 00:01:24,10
So you can select basically any project

35
00:01:24,10 --> 00:01:26,70
that you want Snyk to validate,

36
00:01:26,70 --> 00:01:30,30
and then what you do you just click on the project

37
00:01:30,30 --> 00:01:33,70
and then add selected repository to Snyk.

38
00:01:33,70 --> 00:01:35,20
And then what happens is Snyk is going to

39
00:01:35,20 --> 00:01:37,60
go to that particular repositiory,

40
00:01:37,60 --> 00:01:39,20
check the dependencies,

41
00:01:39,20 --> 00:01:41,60
and make sure to find any issues

42
00:01:41,60 --> 00:01:42,60
which are high,

43
00:01:42,60 --> 00:01:44,20
medium or low.

44
00:01:44,20 --> 00:01:45,90
Once you have the report what you can do

45
00:01:45,90 --> 00:01:50,00
is click on the project itself,

46
00:01:50,00 --> 00:01:52,40
and then scroll down to find which dependencies

47
00:01:52,40 --> 00:01:55,60
need to be looked at or actually updated,

48
00:01:55,60 --> 00:02:00,70
or fixed with any bugs or any issues with security.

49
00:02:00,70 --> 00:02:01,50
So for example,

50
00:02:01,50 --> 00:02:03,20
we scroll down,

51
00:02:03,20 --> 00:02:05,20
we have a denial service here,

52
00:02:05,20 --> 00:02:07,40
that is basically the module,

53
00:02:07,40 --> 00:02:08,40
mem.

54
00:02:08,40 --> 00:02:11,10
And mem inside of react-scripts

55
00:02:11,10 --> 00:02:13,30
has an issue with denial of service.

56
00:02:13,30 --> 00:02:15,30
You can read more about it if you

57
00:02:15,30 --> 00:02:18,00
click on the link here.

58
00:02:18,00 --> 00:02:19,80
And what it is exactly the issue

59
00:02:19,80 --> 00:02:22,50
how to fix it and so on and so forth.

60
00:02:22,50 --> 00:02:23,30
Sometimes,

61
00:02:23,30 --> 00:02:25,00
you're going to have recommendations that you can actually

62
00:02:25,00 --> 00:02:27,80
create a PR right away and fix it right away.

63
00:02:27,80 --> 00:02:28,70
So for example,

64
00:02:28,70 --> 00:02:31,40
here you have a PR that you can fix

65
00:02:31,40 --> 00:02:33,20
this vulnerability right away

66
00:02:33,20 --> 00:02:36,20
by clicking on this link.

67
00:02:36,20 --> 00:02:38,40
And then you can create a PR on your repo

68
00:02:38,40 --> 00:02:39,50
to fix this link,

69
00:02:39,50 --> 00:02:43,40
and it will fix that package automatically for you.

70
00:02:43,40 --> 00:02:46,30
And then you can just merge the PR into your master

71
00:02:46,30 --> 00:02:48,70
if you want to.

72
00:02:48,70 --> 00:02:50,10
So this is how Snyk works,

73
00:02:50,10 --> 00:02:52,10
it basically scans your repos,

74
00:02:52,10 --> 00:02:54,90
and then figure out what are the issues with security,

75
00:02:54,90 --> 00:02:57,00
which packages are the issues,

76
00:02:57,00 --> 00:02:59,00
and then offers some fixes,

77
00:02:59,00 --> 00:03:02,10
it even suggests a PR if you want to go ahead

78
00:03:02,10 --> 00:03:04,00
and to a fix right away.

