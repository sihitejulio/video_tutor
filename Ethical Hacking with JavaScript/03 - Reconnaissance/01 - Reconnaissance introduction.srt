1
00:00:00,50 --> 00:00:02,10
- [Narrator] The goal of doing reconnaissance

2
00:00:02,10 --> 00:00:03,80
is to be able to figure out with a set of

3
00:00:03,80 --> 00:00:06,80
common tools if there are any issues or open

4
00:00:06,80 --> 00:00:09,10
doors for our actors to exploit.

5
00:00:09,10 --> 00:00:11,60
In this stage, you evaluate your code.

6
00:00:11,60 --> 00:00:14,40
So let's go ahead and open up the project that

7
00:00:14,40 --> 00:00:16,70
we just set inside of VS Code,

8
00:00:16,70 --> 00:00:19,10
so you can drag and drop inside of VS Code

9
00:00:19,10 --> 00:00:20,90
and on Windows you can right-click

10
00:00:20,90 --> 00:00:24,90
on the folder and open it in VS Code.

11
00:00:24,90 --> 00:00:26,70
And once we have this open here,

12
00:00:26,70 --> 00:00:28,90
what we can do is bring up the Terminal,

13
00:00:28,90 --> 00:00:36,30
so click on View, Terminal and then do an npm install.

14
00:00:36,30 --> 00:00:38,20
The very first step is to go through any

15
00:00:38,20 --> 00:00:41,70
messages the console.log could be telling you.

16
00:00:41,70 --> 00:00:43,70
When they are messages related to dependencies

17
00:00:43,70 --> 00:00:49,20
being deprecated, act on them immediately.

18
00:00:49,20 --> 00:00:51,80
So when we first do an npm install with our project

19
00:00:51,80 --> 00:00:54,60
you see already some issues with npm audit.

20
00:00:54,60 --> 00:00:59,20
So you can do an npm audit, to get a bit more

21
00:00:59,20 --> 00:01:06,00
information on these issues.

22
00:01:06,00 --> 00:01:07,70
And if you take a look at the list,

23
00:01:07,70 --> 00:01:11,00
any of these issues could be exploited.

24
00:01:11,00 --> 00:01:12,30
Then you also want to use

25
00:01:12,30 --> 00:01:14,20
the latest versions of the dependencies.

26
00:01:14,20 --> 00:01:16,40
If they break your code, figure out why

27
00:01:16,40 --> 00:01:18,40
as in most cases the owner of this package

28
00:01:18,40 --> 00:01:21,90
fixed some inherent issues in the code

29
00:01:21,90 --> 00:01:23,60
and you may want to update your syntax

30
00:01:23,60 --> 00:01:27,20
to the latest if anything breaks your own application.

31
00:01:27,20 --> 00:01:29,40
So if we take a look at the current messages,

32
00:01:29,40 --> 00:01:32,10
most likely if we update the React dependencies

33
00:01:32,10 --> 00:01:34,30
we may avoid these messages.

34
00:01:34,30 --> 00:01:37,20
The advantage that you have over a hacker

35
00:01:37,20 --> 00:01:39,00
is the ability to see your code

36
00:01:39,00 --> 00:01:40,60
and therefore improve it if any

37
00:01:40,60 --> 00:01:43,50
danger lurks in your application.

38
00:01:43,50 --> 00:01:45,10
Linters are good to help improve

39
00:01:45,10 --> 00:01:47,80
your writing style and cleanliness of your code

40
00:01:47,80 --> 00:01:50,50
but in most cases they won't catch security issues.

41
00:01:50,50 --> 00:01:52,70
You need better tools for that.

42
00:01:52,70 --> 00:01:56,20
And this is where tools like Snyk, Retire.js,

43
00:01:56,20 --> 00:01:58,30
and AppSensor comes into play and

44
00:01:58,30 --> 00:02:00,40
we will explore these tools shortly.

45
00:02:00,40 --> 00:02:02,30
The exercises done in this chapter are

46
00:02:02,30 --> 00:02:04,60
exactly for this purpose to do some

47
00:02:04,60 --> 00:02:08,00
reconnaissance and figure out any potential dangers.

