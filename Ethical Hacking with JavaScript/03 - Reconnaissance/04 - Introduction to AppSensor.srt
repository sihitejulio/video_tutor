1
00:00:00,30 --> 00:00:02,50
- AppSensor is a tool that is very useful,

2
00:00:02,50 --> 00:00:05,20
especially when you need to monitor an application

3
00:00:05,20 --> 00:00:06,90
for live intrusions.

4
00:00:06,90 --> 00:00:09,10
This tool goes beyond the mere detection of vulnerable

5
00:00:09,10 --> 00:00:10,50
spot in your code,

6
00:00:10,50 --> 00:00:12,70
but goes into the activities that occur around

7
00:00:12,70 --> 00:00:14,30
your application.

8
00:00:14,30 --> 00:00:16,60
So if we look at the plan we've defined initially

9
00:00:16,60 --> 00:00:18,70
or the cyber kill chain workflow,

10
00:00:18,70 --> 00:00:21,90
at the initial stages we spend a considerable amount

11
00:00:21,90 --> 00:00:24,70
of time scanning our code for trouble dependencies

12
00:00:24,70 --> 00:00:26,40
and improve this code.

13
00:00:26,40 --> 00:00:29,50
But that doesn't make our application 100% secure

14
00:00:29,50 --> 00:00:32,70
and we need other tools to help us monitor further

15
00:00:32,70 --> 00:00:35,60
threats in the delivery or control stages.

16
00:00:35,60 --> 00:00:38,40
This is where AppSensor comes into play.

17
00:00:38,40 --> 00:00:41,50
You need strategies to constantly monitor your application

18
00:00:41,50 --> 00:00:45,10
for unknown access or unusual activities,

19
00:00:45,10 --> 00:00:48,00
etc.

20
00:00:48,00 --> 00:00:50,40
So there are many tools on the market for this purpose

21
00:00:50,40 --> 00:00:53,80
and AppSensor is an example of one you could use.

22
00:00:53,80 --> 00:00:56,70
We won't go through how to install and use this tool

23
00:00:56,70 --> 00:00:58,60
as it is a course on it's own.

24
00:00:58,60 --> 00:01:02,20
But if you want to get started go to this page here called

25
00:01:02,20 --> 00:01:05,10
Getting Started.

26
00:01:05,10 --> 00:01:08,50
And then download the actual AppSensor application

27
00:01:08,50 --> 00:01:11,30
and follow the instructions.

28
00:01:11,30 --> 00:01:15,90
You can also choose the deployment model.

29
00:01:15,90 --> 00:01:18,40
So if you're using SOAPWeb Services

30
00:01:18,40 --> 00:01:20,10
or if you're using Thrift

31
00:01:20,10 --> 00:01:21,10
and so on and so forth.

32
00:01:21,10 --> 00:01:22,90
If you're using RESTWeb Services,

33
00:01:22,90 --> 00:01:26,00
which is usually what I do in my own course

34
00:01:26,00 --> 00:01:29,00
then use that one.

35
00:01:29,00 --> 00:01:31,10
And then you can use the pluggable components here,

36
00:01:31,10 --> 00:01:34,20
so if you want to have specific things looked at

37
00:01:34,20 --> 00:01:35,40
such as storage,

38
00:01:35,40 --> 00:01:39,00
or if you want to have access controllers to take a look

39
00:01:39,00 --> 00:01:40,70
at the access points of your application

40
00:01:40,70 --> 00:01:43,90
you can choose the components here

41
00:01:43,90 --> 00:01:47,20
and then configure the reporting that you want

42
00:01:47,20 --> 00:01:48,80
from the application.

43
00:01:48,80 --> 00:01:53,00
So this is very useful if you want to have a monitoring

44
00:01:53,00 --> 00:01:55,50
done on your application live.

45
00:01:55,50 --> 00:01:59,50
So if you want to basically inspect for intrusions

46
00:01:59,50 --> 00:02:01,70
while the application is running

47
00:02:01,70 --> 00:02:04,70
or if there is a specific user that has logged in

48
00:02:04,70 --> 00:02:07,80
and logged out many times in a very short amount

49
00:02:07,80 --> 00:02:10,10
of time then these are the kinds of things that you

50
00:02:10,10 --> 00:02:13,60
want to look at and start researching further just

51
00:02:13,60 --> 00:02:16,10
to make sure that you're not under attack.

52
00:02:16,10 --> 00:02:19,00
And AppSensor is a good application for that.

