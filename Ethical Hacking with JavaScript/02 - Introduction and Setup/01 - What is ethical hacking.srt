1
00:00:00,50 --> 00:00:02,80
- [Instructor] Whenever people think about hacking,

2
00:00:02,80 --> 00:00:05,40
they think about bad guys sitting behind our computers

3
00:00:05,40 --> 00:00:07,70
with a hoodie, ready to do no good

4
00:00:07,70 --> 00:00:09,60
and destroy computer resources,

5
00:00:09,60 --> 00:00:12,60
and do all kinds of activities for their own interest.

6
00:00:12,60 --> 00:00:15,30
In several cases, that would be correct.

7
00:00:15,30 --> 00:00:19,70
And these type of hackers are referred to black hat hacking.

8
00:00:19,70 --> 00:00:21,20
These hackers will use all kinds

9
00:00:21,20 --> 00:00:23,70
of techniques to access privileged data

10
00:00:23,70 --> 00:00:27,00
or destroy the same data, all for their own benefit;

11
00:00:27,00 --> 00:00:30,00
in other words, unethical hacking.

12
00:00:30,00 --> 00:00:32,10
But what is ethical hacking?

13
00:00:32,10 --> 00:00:35,00
It's the complete opposite of what we just looked at.

14
00:00:35,00 --> 00:00:38,40
Hackers following ethical hacking use the same techniques,

15
00:00:38,40 --> 00:00:40,60
but only to determine if the resources

16
00:00:40,60 --> 00:00:43,00
are vulnerable to threats or not.

17
00:00:43,00 --> 00:00:46,20
These hackers are called white hat hackers.

18
00:00:46,20 --> 00:00:49,10
Let me repeat, ethical hacking is the practice

19
00:00:49,10 --> 00:00:51,90
of using the same methods and tools to define

20
00:00:51,90 --> 00:00:56,10
if there might be vulnerabilities into the code or network.

21
00:00:56,10 --> 00:00:59,20
And companies pay for these services, so they understand

22
00:00:59,20 --> 00:01:02,80
if they could be in danger of being attacked.

23
00:01:02,80 --> 00:01:06,00
Typically, an ethical hacker will get hired by a company,

24
00:01:06,00 --> 00:01:08,60
then the hacker will do some reconnaissance to determine

25
00:01:08,60 --> 00:01:11,50
if there are any open doors for them to exploit,

26
00:01:11,50 --> 00:01:13,40
and then exploit these doors to figure out

27
00:01:13,40 --> 00:01:16,70
what data or resources they could damage or pull.

28
00:01:16,70 --> 00:01:18,80
Then they report their findings to the company,

29
00:01:18,80 --> 00:01:20,30
along with recommendations how

30
00:01:20,30 --> 00:01:22,60
to eliminate the system weakness.

31
00:01:22,60 --> 00:01:25,00
And that is ethical hacking.

