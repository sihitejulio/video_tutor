1
00:00:00,50 --> 00:00:01,60
- [Instructor] As a proper follow up

2
00:00:01,60 --> 00:00:03,10
to the cyber kill chain,

3
00:00:03,10 --> 00:00:05,40
an ethical hacker always needs a plan

4
00:00:05,40 --> 00:00:08,00
which he typically presents to his clients.

5
00:00:08,00 --> 00:00:10,70
The plan of attack consists and properly determine

6
00:00:10,70 --> 00:00:12,70
what are going to be the next steps

7
00:00:12,70 --> 00:00:16,30
to evaluate the resources are vulnerable or not.

8
00:00:16,30 --> 00:00:19,20
When a white hat hacker is hired to evaluate

9
00:00:19,20 --> 00:00:20,70
specific resources,

10
00:00:20,70 --> 00:00:24,10
the plan needs to be customized to fit this approach.

11
00:00:24,10 --> 00:00:25,90
So let's say we were hired to evaluate

12
00:00:25,90 --> 00:00:27,90
an application's vulnerabilities.

13
00:00:27,90 --> 00:00:31,30
Let's take a look at what this plan would look like.

14
00:00:31,30 --> 00:00:33,20
First, we would do reconnaissance,

15
00:00:33,20 --> 00:00:36,00
evaluate vulnerabilities of the application's

16
00:00:36,00 --> 00:00:39,80
code, database, access to users.

17
00:00:39,80 --> 00:00:41,60
Then we would create the script tool

18
00:00:41,60 --> 00:00:43,90
for exploiting this resource.

19
00:00:43,90 --> 00:00:45,90
Next, we would send the exploit

20
00:00:45,90 --> 00:00:48,40
or upload to the application database.

21
00:00:48,40 --> 00:00:51,40
Then, we would install the tool or code

22
00:00:51,40 --> 00:00:53,20
in the application

23
00:00:53,20 --> 00:00:54,40
and typically at this point,

24
00:00:54,40 --> 00:00:56,70
the tool would probably make some minor changes

25
00:00:56,70 --> 00:01:00,10
into the code without hurting the application at large

26
00:01:00,10 --> 00:01:02,00
so the hacker could demonstrate success

27
00:01:02,00 --> 00:01:05,10
of the exploit and how dangerous it could be.

28
00:01:05,10 --> 00:01:07,40
The next step would be to report to the client

29
00:01:07,40 --> 00:01:10,60
what recommendations, or code, or system changes

30
00:01:10,60 --> 00:01:13,00
needs to be done in order to improve

31
00:01:13,00 --> 00:01:14,80
the application's security.

32
00:01:14,80 --> 00:01:16,80
So as you determine how you could evaluate

33
00:01:16,80 --> 00:01:20,20
your application's vulnerabilities or your client's

34
00:01:20,20 --> 00:01:22,10
have a plan of attack in place

35
00:01:22,10 --> 00:01:25,00
and communicate it to all stakeholders.

