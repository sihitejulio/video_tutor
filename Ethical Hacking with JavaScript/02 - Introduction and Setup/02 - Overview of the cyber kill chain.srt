1
00:00:00,50 --> 00:00:01,70
- [Instructor] The cyber kill chain

2
00:00:01,70 --> 00:00:05,20
is a framework that was developed by Lockheed Martin

3
00:00:05,20 --> 00:00:07,80
and a great example of how to plan an attack

4
00:00:07,80 --> 00:00:09,80
as an ethical hacker.

5
00:00:09,80 --> 00:00:11,90
There are many ways to approach this framework,

6
00:00:11,90 --> 00:00:13,40
and once you're done with this video,

7
00:00:13,40 --> 00:00:16,20
I strongly recommend you visit their website.

8
00:00:16,20 --> 00:00:17,90
They have great documentation

9
00:00:17,90 --> 00:00:20,80
and many examples of how it can be used.

10
00:00:20,80 --> 00:00:22,00
But as an introduction,

11
00:00:22,00 --> 00:00:25,10
here is an overview of the framework.

12
00:00:25,10 --> 00:00:27,90
Any attack starts with reconnaissance,

13
00:00:27,90 --> 00:00:31,40
figuring out who or where we could plan an attack.

14
00:00:31,40 --> 00:00:34,90
For example, you could be harvesting lists of emails

15
00:00:34,90 --> 00:00:37,30
or information from social sites.

16
00:00:37,30 --> 00:00:39,60
In this step, the attacker also looks

17
00:00:39,60 --> 00:00:42,90
for potential areas where he could exploit,

18
00:00:42,90 --> 00:00:47,10
vulnerabilities in an application, system, or network.

19
00:00:47,10 --> 00:00:50,00
Then the attacker moves to weaponization,

20
00:00:50,00 --> 00:00:51,60
building the tool or a code

21
00:00:51,60 --> 00:00:53,60
that could exploit the vulnerability

22
00:00:53,60 --> 00:00:56,40
and to be delivered to the resources we've gathered

23
00:00:56,40 --> 00:00:58,40
in the reconnaissance step.

24
00:00:58,40 --> 00:01:00,60
Next is the delivery step,

25
00:01:00,60 --> 00:01:03,80
where an attacker will send the Trojan, virus,

26
00:01:03,80 --> 00:01:06,10
or whatever weapon he's programmed

27
00:01:06,10 --> 00:01:11,20
though emails, social communication channels, text, etc.

28
00:01:11,20 --> 00:01:14,60
In the next step, the attacker exploits the vulnerability

29
00:01:14,60 --> 00:01:16,90
he found in the reconnaissance stage

30
00:01:16,90 --> 00:01:19,40
through the communication channels.

31
00:01:19,40 --> 00:01:23,50
In step five, the tool is installing malware or code

32
00:01:23,50 --> 00:01:25,70
into the system application or network

33
00:01:25,70 --> 00:01:28,20
to be able to exploit these resources.

34
00:01:28,20 --> 00:01:31,30
This is where a user may have clicked on a link,

35
00:01:31,30 --> 00:01:33,00
and it'll install itself

36
00:01:33,00 --> 00:01:35,60
in the background without his knowledge.

37
00:01:35,60 --> 00:01:38,60
Then the tool install will eventually take over

38
00:01:38,60 --> 00:01:41,10
and command the resource in question.

39
00:01:41,10 --> 00:01:44,30
A good example is how a hacker was able

40
00:01:44,30 --> 00:01:45,90
to control the temperature levels

41
00:01:45,90 --> 00:01:49,60
in a server environment and overheated the entire room,

42
00:01:49,60 --> 00:01:51,20
which eventually overheated

43
00:01:51,20 --> 00:01:53,60
and shut down the server hardware.

44
00:01:53,60 --> 00:01:57,30
And finally, once the resource is controlled by the hacker,

45
00:01:57,30 --> 00:02:00,70
he takes over and pulls whatever data he was after

46
00:02:00,70 --> 00:02:03,00
or holds ransom over the resources

47
00:02:03,00 --> 00:02:07,10
in one manner or another for his own benefit.

48
00:02:07,10 --> 00:02:09,40
The goal of this framework is to determine

49
00:02:09,40 --> 00:02:11,70
what proper steps needs to be taken

50
00:02:11,70 --> 00:02:13,70
when a resource is under threat

51
00:02:13,70 --> 00:02:15,80
at any of these stages,

52
00:02:15,80 --> 00:02:17,60
but also how to prevent the attacker

53
00:02:17,60 --> 00:02:21,00
from going any further in this process.

