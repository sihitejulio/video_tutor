1
00:00:00,60 --> 00:00:02,00
- [Instructor] I've included in this course

2
00:00:02,00 --> 00:00:05,40
a base project, just a template to look at the code

3
00:00:05,40 --> 00:00:09,00
and evaluate certain areas as we go through the course.

4
00:00:09,00 --> 00:00:11,90
This won't be a course where we deeply go into the code,

5
00:00:11,90 --> 00:00:13,50
but will simply use certain parts

6
00:00:13,50 --> 00:00:15,50
for demonstration purposes.

7
00:00:15,50 --> 00:00:17,80
Feel free, and I'd strongly recommend it,

8
00:00:17,80 --> 00:00:21,10
to use your own application as we go through the examples.

9
00:00:21,10 --> 00:00:23,90
So if you want to use the code that we have,

10
00:00:23,90 --> 00:00:26,20
look into the Exercise files,

11
00:00:26,20 --> 00:00:29,20
click on Resources, and you can use that code here

12
00:00:29,20 --> 00:00:30,00
as an example.

13
00:00:30,00 --> 00:00:32,90
So what I'm going to do is create a new folder here

14
00:00:32,90 --> 00:00:34,80
so I can separate the resources

15
00:00:34,80 --> 00:00:37,40
from the actual project we're looking at.

16
00:00:37,40 --> 00:00:39,30
And I'm going to do a copy of this one

17
00:00:39,30 --> 00:00:41,60
and then just create a new folder

18
00:00:41,60 --> 00:00:47,50
and call this Example and then simply paste the project

19
00:00:47,50 --> 00:00:49,50
into that example.

20
00:00:49,50 --> 00:00:51,20
You don't need to install the dependencies

21
00:00:51,20 --> 00:00:53,70
if you don't want to, but this is basically a project

22
00:00:53,70 --> 00:00:55,50
that we've done in another course

23
00:00:55,50 --> 00:00:57,70
which is a react info portal,

24
00:00:57,70 --> 00:01:00,40
so we're doing APIs with react.

25
00:01:00,40 --> 00:01:02,20
So if you want to take a look at the application,

26
00:01:02,20 --> 00:01:04,20
feel free to install the dependencies.

27
00:01:04,20 --> 00:01:07,30
In this case, I'm not going to install the dependencies

28
00:01:07,30 --> 00:01:09,90
because I'm simply going to look at the code

29
00:01:09,90 --> 00:01:11,80
and show you examples.

30
00:01:11,80 --> 00:01:13,00
So let's move on.

