1
00:00:00,50 --> 00:00:01,60
- [Manny] Have you ever wondered

2
00:00:01,60 --> 00:00:05,10
how vulnerable your web site or application is?

3
00:00:05,10 --> 00:00:08,00
Are you a JavaScript developer with little to no experience

4
00:00:08,00 --> 00:00:10,10
in hacking or security measures?

5
00:00:10,10 --> 00:00:12,90
If you've answered yes to either of these questions,

6
00:00:12,90 --> 00:00:14,80
you've come to the right place.

7
00:00:14,80 --> 00:00:17,20
In this course, my objective is to provide you

8
00:00:17,20 --> 00:00:20,00
with reconnaissance and security tools.

9
00:00:20,00 --> 00:00:21,50
This will give you the skills

10
00:00:21,50 --> 00:00:24,10
to assess common security threats.

11
00:00:24,10 --> 00:00:26,60
You'll also learn how to anticipate threats

12
00:00:26,60 --> 00:00:29,30
and improve your security awareness in order

13
00:00:29,30 --> 00:00:33,00
to reduce how vulnerable your JavaScript code might be.

14
00:00:33,00 --> 00:00:36,60
Hi, I'm Manny Henri and security is always top of mind

15
00:00:36,60 --> 00:00:40,00
when I build new applications, and it would be my pleasure

16
00:00:40,00 --> 00:00:43,50
to show you how to hack into your own code.

17
00:00:43,50 --> 00:00:46,40
First, we'll get started with a few basic terms

18
00:00:46,40 --> 00:00:51,20
and the difference between white hat and black hat hacking.

19
00:00:51,20 --> 00:00:53,20
Then we'll move on to do some reconnaissance

20
00:00:53,20 --> 00:00:55,40
of your code with a few tools.

21
00:00:55,40 --> 00:00:58,60
And finally, we'll cover the most common threats,

22
00:00:58,60 --> 00:01:01,60
what they look like and what to look for.

23
00:01:01,60 --> 00:01:04,10
So, if you're ready to increase the security

24
00:01:04,10 --> 00:01:05,90
of your JavaScript code,

25
00:01:05,90 --> 00:01:09,00
rev up your engines and let's get started.

