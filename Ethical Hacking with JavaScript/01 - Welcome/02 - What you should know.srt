1
00:00:00,50 --> 00:00:01,30
- [Instructor] In this course,

2
00:00:01,30 --> 00:00:03,90
you don't need to have prior knowledge in security.

3
00:00:03,90 --> 00:00:05,70
It can be taken as a first step

4
00:00:05,70 --> 00:00:08,20
into getting more security skills.

5
00:00:08,20 --> 00:00:10,10
However, I strongly recommend

6
00:00:10,10 --> 00:00:13,20
to have a solid foundation in JavaScript programming

7
00:00:13,20 --> 00:00:15,20
as you will get lost if you don't understand

8
00:00:15,20 --> 00:00:18,70
the code I use to demonstrate the security issues.

9
00:00:18,70 --> 00:00:21,30
In most cases, if you understand JavaScript

10
00:00:21,30 --> 00:00:22,50
and are able to build code

11
00:00:22,50 --> 00:00:25,40
without constantly to go back to references,

12
00:00:25,40 --> 00:00:27,70
you'll be fine to take this course.

13
00:00:27,70 --> 00:00:30,70
Otherwise, I'd suggest you take some basic courses

14
00:00:30,70 --> 00:00:34,00
in JavaScript essentials and mid-level courses.

