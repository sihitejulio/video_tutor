1
00:00:00,80 --> 00:00:03,30
- [Sasha] Thanks so much for joining me in this course.

2
00:00:03,30 --> 00:00:06,10
You now have a fuller understanding of what asynchronous

3
00:00:06,10 --> 00:00:09,70
code does, what's going on under the hood in common

4
00:00:09,70 --> 00:00:13,50
asynchronous methods, and experience writing asynchronous

5
00:00:13,50 --> 00:00:17,80
code using a variety of modern JavaScript features.

6
00:00:17,80 --> 00:00:21,20
To dig deeper into some more advanced JavaScript concepts,

7
00:00:21,20 --> 00:00:25,10
check out courses on Closures or Prototypes.

8
00:00:25,10 --> 00:00:27,80
If you want to learn more about using data from APIs

9
00:00:27,80 --> 00:00:33,20
in your apps, explore courses on Ajax or HTTP.

10
00:00:33,20 --> 00:00:35,70
Feel free to follow me online.

11
00:00:35,70 --> 00:00:38,60
Now take your skills and build something amazing.

12
00:00:38,60 --> 00:00:40,00
Good luck!

