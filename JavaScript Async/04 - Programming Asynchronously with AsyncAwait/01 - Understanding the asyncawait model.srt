1
00:00:00,80 --> 00:00:03,10
- [Instructor] Promises are a clear step forward

2
00:00:03,10 --> 00:00:06,30
from callbacks in terms of code organization.

3
00:00:06,30 --> 00:00:09,20
However, especially for a developer who doesn't use

4
00:00:09,20 --> 00:00:11,80
them all the time, it can still be a challenge

5
00:00:11,80 --> 00:00:14,60
to follow what's happening in a promise chain.

6
00:00:14,60 --> 00:00:17,20
In particular, because it requires identifying

7
00:00:17,20 --> 00:00:20,50
where the code is asynchronous and where it's not.

8
00:00:20,50 --> 00:00:23,50
An even newer way to work with asynchronous code

9
00:00:23,50 --> 00:00:26,40
became part of JavaScript in 2017.

10
00:00:26,40 --> 00:00:29,80
This syntax uses the async and await keywords

11
00:00:29,80 --> 00:00:32,00
to enable writing asynchronous code

12
00:00:32,00 --> 00:00:35,50
that closely resembles synchronous code patterns.

13
00:00:35,50 --> 00:00:38,80
For this reason, code using async and await

14
00:00:38,80 --> 00:00:42,00
is widely recognized as being easier for humans

15
00:00:42,00 --> 00:00:45,40
to parse than code using promises.

16
00:00:45,40 --> 00:00:48,00
You use the async keyword as part of

17
00:00:48,00 --> 00:00:50,40
a function declaration or expression

18
00:00:50,40 --> 00:00:52,10
to specify that the function

19
00:00:52,10 --> 00:00:54,40
should be executed asynchronously.

20
00:00:54,40 --> 00:00:59,60
That is, it should not block the parser's main process.

21
00:00:59,60 --> 00:01:01,80
Within an asynchronous function,

22
00:01:01,80 --> 00:01:04,90
you use the await keyword before a statement

23
00:01:04,90 --> 00:01:07,60
that returns a promise to indicate

24
00:01:07,60 --> 00:01:09,90
that the function should stop and wait for

25
00:01:09,90 --> 00:01:13,50
the result of the promise before proceeding.

26
00:01:13,50 --> 00:01:16,00
It's important to understand that aside from

27
00:01:16,00 --> 00:01:19,80
a couple details, async and await are essentially

28
00:01:19,80 --> 00:01:22,60
just syntactic sugar, meaning that

29
00:01:22,60 --> 00:01:25,20
they simply provide shorthand syntax

30
00:01:25,20 --> 00:01:29,00
for writing code using an existing coding practice.

31
00:01:29,00 --> 00:01:32,60
In this case, async and await code creates

32
00:01:32,60 --> 00:01:35,80
and works with promises under the hood.

33
00:01:35,80 --> 00:01:38,90
So for many situations, you can write simpler code

34
00:01:38,90 --> 00:01:43,50
using async and await instead of expressly writing promises

35
00:01:43,50 --> 00:01:46,00
and still get the same results.

