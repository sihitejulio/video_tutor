1
00:00:01,00 --> 00:00:02,80
- [Instructor] On our Explore California site,

2
00:00:02,80 --> 00:00:05,70
we have code that uses promises to make requests

3
00:00:05,70 --> 00:00:08,30
for weather information at four locations

4
00:00:08,30 --> 00:00:10,70
and then, when all requests are completed,

5
00:00:10,70 --> 00:00:13,30
to use the return data to render the information

6
00:00:13,30 --> 00:00:15,20
as part of the DOM.

7
00:00:15,20 --> 00:00:18,10
We can async and await in place of

8
00:00:18,10 --> 00:00:19,80
the chain promise pattern here

9
00:00:19,80 --> 00:00:22,70
to make the code look more like the synchronous functions

10
00:00:22,70 --> 00:00:25,30
that are common in JavaScript.

11
00:00:25,30 --> 00:00:29,70
So, I'm going to start by commenting out my promise

12
00:00:29,70 --> 00:00:34,10
and I want to make sure not to comment out that last line

13
00:00:34,10 --> 00:00:36,20
because that last line closes up

14
00:00:36,20 --> 00:00:39,40
my event listener for the page load.

15
00:00:39,40 --> 00:00:42,30
And so, below my promise, I want to rewrite

16
00:00:42,30 --> 00:00:46,30
this same functionality using an asynchronous function

17
00:00:46,30 --> 00:00:47,50
with some await statements.

18
00:00:47,50 --> 00:00:50,60
So, I need to create an asynchronous function

19
00:00:50,60 --> 00:00:52,50
but I don't need to reference it later,

20
00:00:52,50 --> 00:00:53,90
so I'm going to go ahead and create an iffy.

21
00:00:53,90 --> 00:00:56,30
I'm going to instantly invoke that function.

22
00:00:56,30 --> 00:01:03,10
So, I'm going to start by declaring an async function

23
00:01:03,10 --> 00:01:05,80
and I'm making this an anonymous function,

24
00:01:05,80 --> 00:01:07,20
it doesn't need a name.

25
00:01:07,20 --> 00:01:09,70
And so, to make this instantly invoked,

26
00:01:09,70 --> 00:01:11,80
put that parenth at the beginning,

27
00:01:11,80 --> 00:01:15,20
put another one at the end, followed by a pair.

28
00:01:15,20 --> 00:01:17,90
And so now, everything in here will be an async function.

29
00:01:17,90 --> 00:01:23,30
It will also be immediately invoked when it's declared.

30
00:01:23,30 --> 00:01:25,10
And so, the first thing I want to do here

31
00:01:25,10 --> 00:01:27,90
is create an array called responses

32
00:01:27,90 --> 00:01:29,90
and that's going to be an empty array

33
00:01:29,90 --> 00:01:33,90
that I'm going to use to be able to compile

34
00:01:33,90 --> 00:01:39,10
the results from my get requests and then work with them.

35
00:01:39,10 --> 00:01:41,40
So, I could work with promise.all in here,

36
00:01:41,40 --> 00:01:44,30
but I'd rather totally take out the promise syntax

37
00:01:44,30 --> 00:01:46,90
and just deal with some very basic async

38
00:01:46,90 --> 00:01:48,80
and await statements so we can get a flavor for that.

39
00:01:48,80 --> 00:01:52,50
So, we're going to go responses.push

40
00:01:52,50 --> 00:01:55,10
and then I want to make a call to the get function,

41
00:01:55,10 --> 00:02:01,90
passing at the first URL in my urls array.

42
00:02:01,90 --> 00:02:09,10
And so, to do that, I'm just going to type get urls[0]

43
00:02:09,10 --> 00:02:14,20
and now, what I want is for my async function

44
00:02:14,20 --> 00:02:16,80
to wait for the result of that get request

45
00:02:16,80 --> 00:02:19,30
before it moves on to other code.

46
00:02:19,30 --> 00:02:26,00
And do to that, I just preface the get call with await.

47
00:02:26,00 --> 00:02:29,90
And so, this is going to grab the first URL in my array,

48
00:02:29,90 --> 00:02:32,10
which will be for Los Angeles.

49
00:02:32,10 --> 00:02:33,70
It's going to send it to the get function

50
00:02:33,70 --> 00:02:36,20
that we've been using, that we built.

51
00:02:36,20 --> 00:02:38,50
And then, when it gets data back,

52
00:02:38,50 --> 00:02:41,00
it's going to push that data to the responses array

53
00:02:41,00 --> 00:02:44,30
and then it will move on to the next line.

54
00:02:44,30 --> 00:02:46,30
So, I just want to repeat that three times

55
00:02:46,30 --> 00:02:48,10
for all the items in that array.

56
00:02:48,10 --> 00:02:51,40
So, I'm just going to copy and paste that statement three times

57
00:02:51,40 --> 00:02:57,60
and change my index number and clearly,

58
00:02:57,60 --> 00:03:00,50
I could do this with some sort of a loop,

59
00:03:00,50 --> 00:03:02,50
but in order to really see how

60
00:03:02,50 --> 00:03:04,00
async and await are working here,

61
00:03:04,00 --> 00:03:06,60
I think it's useful to make these individual statements

62
00:03:06,60 --> 00:03:08,80
and see how they're executed.

63
00:03:08,80 --> 00:03:13,70
So next, we're going to create another variable called literals

64
00:03:13,70 --> 00:03:15,20
and the value here is going to use

65
00:03:15,20 --> 00:03:18,40
the same responses.map code that we used

66
00:03:18,40 --> 00:03:21,90
above in our first then statement.

67
00:03:21,90 --> 00:03:25,00
So, I'm just going to copy and paste that here.

68
00:03:25,00 --> 00:03:30,20
I need to uncomment these lines

69
00:03:30,20 --> 00:03:37,80
and get the indents looking better.

70
00:03:37,80 --> 00:03:41,60
And so, this is going to create a new array

71
00:03:41,60 --> 00:03:45,40
based on our existing array of responses.

72
00:03:45,40 --> 00:03:47,20
It's going to run those through the successHandler

73
00:03:47,20 --> 00:03:50,30
and then that literals array is going to contain

74
00:03:50,30 --> 00:03:53,90
all of our template literals.

75
00:03:53,90 --> 00:03:58,00
And then, we need to do those little clean up tasks.

76
00:03:58,00 --> 00:03:59,40
So, I'm going to grab the statement

77
00:03:59,40 --> 00:04:04,70
from the next then function, paste that in here.

78
00:04:04,70 --> 00:04:07,20
Get the indents all ironed out.

79
00:04:07,20 --> 00:04:11,20
So here, we are adding that Weather heading,

80
00:04:11,20 --> 00:04:15,40
followed by joining all of the literals we just generated.

81
00:04:15,40 --> 00:04:18,00
And then the last thing we need to do

82
00:04:18,00 --> 00:04:29,20
is actually display this in the DOM.

83
00:04:29,20 --> 00:04:35,00
And so, saving that, going to switch over to browser,

84
00:04:35,00 --> 00:04:36,90
and there we are.

85
00:04:36,90 --> 00:04:41,40
We got the same results here that we had before.

86
00:04:41,40 --> 00:04:46,80
But this time, we are using an async function instead.

87
00:04:46,80 --> 00:04:49,00
And so, what's happening here in our function

88
00:04:49,00 --> 00:04:52,90
is a combination of synchronous and asynchronous statements.

89
00:04:52,90 --> 00:04:54,90
So, we're starting out with the synchronous statement

90
00:04:54,90 --> 00:04:56,40
just creating an array.

91
00:04:56,40 --> 00:04:59,10
Now, these next four statements all involve

92
00:04:59,10 --> 00:05:03,50
an asynchronous request through this get function.

93
00:05:03,50 --> 00:05:08,00
And so, all four of these await statements

94
00:05:08,00 --> 00:05:09,80
will start at the same time.

95
00:05:09,80 --> 00:05:12,40
So, they're all going to run in parallel.

96
00:05:12,40 --> 00:05:15,40
But then we get to our next synchronous statement

97
00:05:15,40 --> 00:05:17,10
and that is what's going to wait

98
00:05:17,10 --> 00:05:20,30
for the results of all of these other statements.

99
00:05:20,30 --> 00:05:22,30
And so, once we get responses

100
00:05:22,30 --> 00:05:25,70
from all four of those requests,

101
00:05:25,70 --> 00:05:27,90
then this synchronous code here is going to run

102
00:05:27,90 --> 00:05:31,00
here and here and then we're all done.

