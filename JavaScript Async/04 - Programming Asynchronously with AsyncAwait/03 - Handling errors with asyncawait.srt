1
00:00:01,00 --> 00:00:02,30
- [Narrator] Async and await statements

2
00:00:02,30 --> 00:00:04,40
don't come with their own error handling methods

3
00:00:04,40 --> 00:00:05,80
like promises do.

4
00:00:05,80 --> 00:00:07,80
But the async and await structure

5
00:00:07,80 --> 00:00:09,00
lets us write code

6
00:00:09,00 --> 00:00:11,40
that looks a lot like synchronous JavaScript.

7
00:00:11,40 --> 00:00:14,10
And it turns out that it also works just fine

8
00:00:14,10 --> 00:00:18,40
with standard JavaScript error handling statements.

9
00:00:18,40 --> 00:00:21,20
You enclose your statements in a try block

10
00:00:21,20 --> 00:00:24,50
and then add a catch block afterward

11
00:00:24,50 --> 00:00:27,90
that specifies a parameter name for the error object

12
00:00:27,90 --> 00:00:30,40
and includes code to handle the error.

13
00:00:30,40 --> 00:00:33,30
So for our Explore California site,

14
00:00:33,30 --> 00:00:38,60
I can update the async function to handle errors.

15
00:00:38,60 --> 00:00:43,50
And so, first off, I'm going to add a try block.

16
00:00:43,50 --> 00:00:48,10
And I want to move all of my code so far

17
00:00:48,10 --> 00:00:50,90
inside the function

18
00:00:50,90 --> 00:00:54,40
I want to move that, actually, inside the try block.

19
00:00:54,40 --> 00:00:57,20
So leaving those last 2 lines down here

20
00:00:57,20 --> 00:00:59,60
that close the async function

21
00:00:59,60 --> 00:01:02,50
and that close our event listener.

22
00:01:02,50 --> 00:01:04,60
And so now, all of that code,

23
00:01:04,60 --> 00:01:06,30
include the await statements,

24
00:01:06,30 --> 00:01:09,70
is in the try block.

25
00:01:09,70 --> 00:01:12,30
And so after the try block,

26
00:01:12,30 --> 00:01:16,00
I'm going to add a catch block.

27
00:01:16,00 --> 00:01:19,10
It's going to catch the status

28
00:01:19,10 --> 00:01:22,30
and just like we did above,

29
00:01:22,30 --> 00:01:27,90
we're going to call the fail handler,

30
00:01:27,90 --> 00:01:30,10
passing it the status.

31
00:01:30,10 --> 00:01:32,80
So in promises, the catch and finally methods

32
00:01:32,80 --> 00:01:34,30
are based on

33
00:01:34,30 --> 00:01:38,00
the more generic JavaScript methods for error handling.

34
00:01:38,00 --> 00:01:40,30
And so,

35
00:01:40,30 --> 00:01:45,40
we can also add a finally clause

36
00:01:45,40 --> 00:01:48,10
to our async function.

37
00:01:48,10 --> 00:01:52,60
And so I can just grab this code in line 98

38
00:01:52,60 --> 00:01:55,90
that displays that weather div

39
00:01:55,90 --> 00:01:58,80
and move it down into the finally block.

40
00:01:58,80 --> 00:02:01,80
So what happens is both after the try block

41
00:02:01,80 --> 00:02:03,40
and after the catch block

42
00:02:03,40 --> 00:02:08,20
no matter whether or not there's an error involved.

43
00:02:08,20 --> 00:02:14,30
And so, saving that, going to switch over to my browser.

44
00:02:14,30 --> 00:02:16,00
I have all my weather coming in.

45
00:02:16,00 --> 00:02:19,80
So the changes we made haven't affected our functionality.

46
00:02:19,80 --> 00:02:21,60
And then going back to the code,

47
00:02:21,60 --> 00:02:24,10
I'm going to swap in

48
00:02:24,10 --> 00:02:27,50
my empty API key

49
00:02:27,50 --> 00:02:31,40
to ensure we have errors.

50
00:02:31,40 --> 00:02:35,20
And now we have the fallback image.

51
00:02:35,20 --> 00:02:37,20
And checking out the console,

52
00:02:37,20 --> 00:02:40,00
we have an error from the parser,

53
00:02:40,00 --> 00:02:44,60
and we have the error that our code generate on line 48.

54
00:02:44,60 --> 00:02:48,50
And so notice here we only have 1 error message.

55
00:02:48,50 --> 00:02:52,60
So since we're not doing a group of get calls,

56
00:02:52,60 --> 00:02:55,10
the first one of these that fails

57
00:02:55,10 --> 00:02:58,20
kicks us down into the catch clause.

58
00:02:58,20 --> 00:03:01,30
And so we've used an async function

59
00:03:01,30 --> 00:03:04,70
to create some asynchronous requests,

60
00:03:04,70 --> 00:03:07,40
we've used a catch statement to deal with errors,

61
00:03:07,40 --> 00:03:11,10
and a finally clause to clean things up

62
00:03:11,10 --> 00:03:16,00
no matter which one of those previous blocks executed.

