1
00:00:00,80 --> 00:00:03,00
- [Instructor] The async and await construction

2
00:00:03,00 --> 00:00:05,80
became part of Javascript in 2017

3
00:00:05,80 --> 00:00:09,20
and it's supported by all modern browsers.

4
00:00:09,20 --> 00:00:11,40
Like Promises, this code won't work

5
00:00:11,40 --> 00:00:14,30
on older browsers or on Opera Mini.

6
00:00:14,30 --> 00:00:17,30
But the process of making your async and await code

7
00:00:17,30 --> 00:00:20,20
backward compatible is a bit more complicated

8
00:00:20,20 --> 00:00:22,40
than for Promises.

9
00:00:22,40 --> 00:00:26,10
Because async and await is based on Promises under the hood,

10
00:00:26,10 --> 00:00:28,70
it requires a polyfill for Promises

11
00:00:28,70 --> 00:00:31,60
to replicate Promise syntax on browsers

12
00:00:31,60 --> 00:00:33,20
that don't support it.

13
00:00:33,20 --> 00:00:35,60
Our code for the Explore California site

14
00:00:35,60 --> 00:00:38,40
already includes a reference to this polyfill

15
00:00:38,40 --> 00:00:40,40
to make the earlier version of the code

16
00:00:40,40 --> 00:00:43,70
using the Promise constructor backwards compatible.

17
00:00:43,70 --> 00:00:45,90
But this Promise polyfill by itself

18
00:00:45,90 --> 00:00:50,50
won't make async and await code work on older browsers.

19
00:00:50,50 --> 00:00:53,20
That's because the async and await construction

20
00:00:53,20 --> 00:00:57,30
first has to be transpiled into a Promises construction

21
00:00:57,30 --> 00:00:59,60
using a tool like Babel.

22
00:00:59,60 --> 00:01:02,40
Now transpiling is generally part of a tool chain

23
00:01:02,40 --> 00:01:05,30
that's configured on a developer's machine,

24
00:01:05,30 --> 00:01:06,50
but we can get a flavor

25
00:01:06,50 --> 00:01:09,90
for what transpiled async and await code looks like

26
00:01:09,90 --> 00:01:13,90
by using a demo version of Babel REPL online.

27
00:01:13,90 --> 00:01:19,40
So in my code I'm just going to copy my async function.

28
00:01:19,40 --> 00:01:24,00
So in the start file here, that's just lines 87 through 103.

29
00:01:24,00 --> 00:01:27,60
And then on the live version of Babel REPL,

30
00:01:27,60 --> 00:01:32,30
I'm going to click in the left pane and paste.

31
00:01:32,30 --> 00:01:35,20
I'm going to accept all of those defaults on the left side

32
00:01:35,20 --> 00:01:37,40
and on the right side I get

33
00:01:37,40 --> 00:01:41,40
a backward compatible transpiled version of the code.

34
00:01:41,40 --> 00:01:43,70
Now there's a lot going on here.

35
00:01:43,70 --> 00:01:46,70
This has been architected to replicate everything

36
00:01:46,70 --> 00:01:49,50
that we're expecting out of our async and await code,

37
00:01:49,50 --> 00:01:52,30
but notice that this is based on Promises,

38
00:01:52,30 --> 00:01:55,70
and so our async and await code

39
00:01:55,70 --> 00:02:00,00
has been rewritten as Promises.

40
00:02:00,00 --> 00:02:02,20
And then when this code is executed

41
00:02:02,20 --> 00:02:05,10
in conjunction with the Promise polyfill,

42
00:02:05,10 --> 00:02:08,00
it does the same thing that our original async function

43
00:02:08,00 --> 00:02:11,00
can do in a modern browser.

