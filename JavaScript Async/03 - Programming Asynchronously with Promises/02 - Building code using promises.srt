1
00:00:00,80 --> 00:00:03,90
Modern JavaScript includes a method called Fetch,

2
00:00:03,90 --> 00:00:07,20
that creates an asynchronous HTTP request,

3
00:00:07,20 --> 00:00:10,70
using a syntax that's based on promises.

4
00:00:10,70 --> 00:00:12,80
However, to get practice building

5
00:00:12,80 --> 00:00:14,50
promises from the ground up,

6
00:00:14,50 --> 00:00:16,50
we're going to create our own method

7
00:00:16,50 --> 00:00:19,90
using the Promises constructor to create and send

8
00:00:19,90 --> 00:00:22,50
an XHR request for the weather information

9
00:00:22,50 --> 00:00:24,60
on the Explore California site.

10
00:00:24,60 --> 00:00:28,00
So, were going to start by basically rewriting

11
00:00:28,00 --> 00:00:30,60
the get method that we've already created.

12
00:00:30,60 --> 00:00:34,20
So, our get method is just creating an XHR request,

13
00:00:34,20 --> 00:00:37,70
sending that, and then working with the response.

14
00:00:37,70 --> 00:00:40,10
And we just want to turn this into code

15
00:00:40,10 --> 00:00:43,80
that is actually creating, and then working with

16
00:00:43,80 --> 00:00:45,00
a promise.

17
00:00:45,00 --> 00:00:48,40
So, the first thing I'm going to do is take out

18
00:00:48,40 --> 00:00:51,60
the success and fail callback parameters,

19
00:00:51,60 --> 00:00:54,20
because promises don't actually work with those.

20
00:00:54,20 --> 00:00:56,00
We don't use callbacks with promises,

21
00:00:56,00 --> 00:00:58,00
we do have more under the hood.

22
00:00:58,00 --> 00:01:00,60
And then the first line of my function,

23
00:01:00,60 --> 00:01:03,40
I want to return a new promise.

24
00:01:03,40 --> 00:01:07,30
And this is a capital P using the Promise constructor.

25
00:01:07,30 --> 00:01:09,10
And then within the parens,

26
00:01:09,10 --> 00:01:11,90
I'm going to pass in an anonymous function,

27
00:01:11,90 --> 00:01:14,30
and that's going to have two parameters,

28
00:01:14,30 --> 00:01:15,90
resolve and reject,

29
00:01:15,90 --> 00:01:21,50
which correspond to methods inherited from the constructor.

30
00:01:21,50 --> 00:01:25,50
So promises are essentially callbacks under the hood,

31
00:01:25,50 --> 00:01:27,70
but they let us write the syntax, of course,

32
00:01:27,70 --> 00:01:30,70
in a way that's easier for us to work with.

33
00:01:30,70 --> 00:01:33,30
And so then I basically need to stick all of

34
00:01:33,30 --> 00:01:37,40
this other content, that used to be part of my get function,

35
00:01:37,40 --> 00:01:43,00
and just nest that inside of my new Promise.

36
00:01:43,00 --> 00:01:46,80
And then, I need to deal with what happens

37
00:01:46,80 --> 00:01:50,00
in response to the load event.

38
00:01:50,00 --> 00:01:51,20
So,

39
00:01:51,20 --> 00:01:53,50
when the status is 200,

40
00:01:53,50 --> 00:01:55,90
instead of calling that success callback,

41
00:01:55,90 --> 00:01:57,40
like I used to do,

42
00:01:57,40 --> 00:01:58,60
I instead

43
00:01:58,60 --> 00:02:02,30
want to call the resolve callback

44
00:02:02,30 --> 00:02:05,90
that I specified for my promise.

45
00:02:05,90 --> 00:02:08,90
And then for the fail case,

46
00:02:08,90 --> 00:02:10,60
instead of calling the fail callback,

47
00:02:10,60 --> 00:02:14,20
now I have a reject callback

48
00:02:14,20 --> 00:02:18,80
and here I can actually create an Error object,

49
00:02:18,80 --> 00:02:23,20
which is just adding an extra layer to my Javascript,

50
00:02:23,20 --> 00:02:25,50
so that I can actually generate an error

51
00:02:25,50 --> 00:02:26,60
in a browser console.

52
00:02:26,60 --> 00:02:28,90
That's not a promise specific thing.

53
00:02:28,90 --> 00:02:31,60
But it is something else we can do to make our errors

54
00:02:31,60 --> 00:02:36,70
a little bit more useful to us as developers.

55
00:02:36,70 --> 00:02:40,00
And then, scrolling down,

56
00:02:40,00 --> 00:02:41,20
to the very bottom,

57
00:02:41,20 --> 00:02:45,80
where I'm actually calling my get method,

58
00:02:45,80 --> 00:02:49,30
I'm not going to be calling it

59
00:02:49,30 --> 00:02:52,70
and passing in any methods right now.

60
00:02:52,70 --> 00:02:55,20
I'm going to comment out that previous function call,

61
00:02:55,20 --> 00:02:57,30
and because we don't have

62
00:02:57,30 --> 00:03:00,60
any response handling set up yet,

63
00:03:00,60 --> 00:03:03,90
I'm going to start by just console.log-ing the response.

64
00:03:03,90 --> 00:03:09,00
So we can see what happens when a promise

65
00:03:09,00 --> 00:03:11,30
resolves or rejects.

66
00:03:11,30 --> 00:03:14,20
So I'm just going to call

67
00:03:14,20 --> 00:03:16,30
that get method,

68
00:03:16,30 --> 00:03:17,90
pass in our URL,

69
00:03:17,90 --> 00:03:21,70
and consol.log the results.

70
00:03:21,70 --> 00:03:23,70
And so over in my browser,

71
00:03:23,70 --> 00:03:26,10
I have my page

72
00:03:26,10 --> 00:03:28,90
loading with live reload,

73
00:03:28,90 --> 00:03:31,40
and so here is the result

74
00:03:31,40 --> 00:03:33,10
of my console.log.

75
00:03:33,10 --> 00:03:36,30
So my get function that returns a promise,

76
00:03:36,30 --> 00:03:38,00
and we can actually look inside that promise

77
00:03:38,00 --> 00:03:40,40
and see it has a status and a value,

78
00:03:40,40 --> 00:03:41,80
the status is resolved,

79
00:03:41,80 --> 00:03:44,10
meaning that it got a successful response,

80
00:03:44,10 --> 00:03:45,40
and got some data back.

81
00:03:45,40 --> 00:03:48,40
And then the promise value is actually that data,

82
00:03:48,40 --> 00:03:50,30
and when I hover over that I can actually see

83
00:03:50,30 --> 00:03:51,90
I have this big blob of JSON

84
00:03:51,90 --> 00:03:54,10
containing weather data, just like I expect.

85
00:03:54,10 --> 00:03:59,00
So this is the first stage of actually creating a promise.

