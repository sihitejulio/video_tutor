1
00:00:00,80 --> 00:00:05,50
- [Instructor] And so now, moving down to the code

2
00:00:05,50 --> 00:00:07,80
that runs when the page is loaded,

3
00:00:07,80 --> 00:00:09,90
right now I have my Get, Then, Catch,

4
00:00:09,90 --> 00:00:12,20
and Finally statements.

5
00:00:12,20 --> 00:00:14,60
And down here in my Finally statement,

6
00:00:14,60 --> 00:00:17,70
I'm creating this element called weatherDiv

7
00:00:17,70 --> 00:00:21,90
that just creates a reference to a down element,

8
00:00:21,90 --> 00:00:24,90
but I'm going to need to use that in other parts of my code.

9
00:00:24,90 --> 00:00:28,10
So, I'm going to cut that and I'm going to move it up here

10
00:00:28,10 --> 00:00:29,60
right after my apiKey,

11
00:00:29,60 --> 00:00:31,30
so that that's available

12
00:00:31,30 --> 00:00:34,30
all throughout my EventListener here.

13
00:00:34,30 --> 00:00:35,80
And then the next thing I'm going to do

14
00:00:35,80 --> 00:00:38,90
is create a variable containing the coded names

15
00:00:38,90 --> 00:00:42,30
of the locations that I want to get weather for.

16
00:00:42,30 --> 00:00:44,30
So, I'm going to make that a constant.

17
00:00:44,30 --> 00:00:46,00
I'm going to call it locations.

18
00:00:46,00 --> 00:00:47,40
It's going to be an array.

19
00:00:47,40 --> 00:00:50,40
I'm going to break that onto multiple lines for legibility.

20
00:00:50,40 --> 00:00:55,10
So I'm going to do los+angeles,us.

21
00:00:55,10 --> 00:00:59,30
So this is the encoding that openweathermap expects.

22
00:00:59,30 --> 00:01:04,40
And then we'll do san+francisco,us.

23
00:01:04,40 --> 00:01:07,40
We'll do lone+pine,us

24
00:01:07,40 --> 00:01:11,20
which is the nearest town to Mount Whitney.

25
00:01:11,20 --> 00:01:15,00
And then we'll do mariposa,us

26
00:01:15,00 --> 00:01:19,60
which is a town reasonably near Yosemite National Park.

27
00:01:19,60 --> 00:01:23,10
Put a semicolon at the end of my statement.

28
00:01:23,10 --> 00:01:26,80
And so next, instead of a single URL,

29
00:01:26,80 --> 00:01:30,60
I need to create an array of URL's for my call.

30
00:01:30,60 --> 00:01:35,00
And so, I'm just going to update this a little bit.

31
00:01:35,00 --> 00:01:37,60
And for clarity, I'm going to comment out the one

32
00:01:37,60 --> 00:01:41,00
that was already there,

33
00:01:41,00 --> 00:01:43,30
and I'm going to rewrite this.

34
00:01:43,30 --> 00:01:48,60
So, I'm going to do const urls plural

35
00:01:48,60 --> 00:01:51,80
and I'm going to use my locations array

36
00:01:51,80 --> 00:01:56,80
and I'm going to use the map method to create a new array,

37
00:01:56,80 --> 00:01:59,70
passing in an anonymous function

38
00:01:59,70 --> 00:02:02,70
and using the parameter name location

39
00:02:02,70 --> 00:02:05,00
for each time through the loop.

40
00:02:05,00 --> 00:02:08,30
And for my map function, I need to return,

41
00:02:08,30 --> 00:02:15,50
and so I'm actually going to copy my string

42
00:02:15,50 --> 00:02:19,90
up through the sd equal sign

43
00:02:19,90 --> 00:02:22,40
to the beginning of my old url

44
00:02:22,40 --> 00:02:24,80
and I want to return a templete literal here

45
00:02:24,80 --> 00:02:26,90
so I'm going to use a back tick,

46
00:02:26,90 --> 00:02:30,60
paste that in so far to the equal sign.

47
00:02:30,60 --> 00:02:35,90
Then I'm going to do dollar, curly brace, location

48
00:02:35,90 --> 00:02:38,20
ampersand APPID in all caps

49
00:02:38,20 --> 00:02:43,10
equals, dollar sign, curly brace apiKey,

50
00:02:43,10 --> 00:02:46,80
curly brace.

51
00:02:46,80 --> 00:02:52,70
And so, this is going to build me an array of URLs

52
00:02:52,70 --> 00:02:55,40
ready to go to make requests to open Weather Map

53
00:02:55,40 --> 00:02:59,80
for those four locations using my API key.

54
00:02:59,80 --> 00:03:03,40
Now, once I have that array of URLs,

55
00:03:03,40 --> 00:03:07,40
I can replace my get(URL) statement

56
00:03:07,40 --> 00:03:12,50
with Promise, capital P .all.

57
00:03:12,50 --> 00:03:16,60
So, I need to pass an array that contains a Promise

58
00:03:16,60 --> 00:03:18,70
for each one of these URLs.

59
00:03:18,70 --> 00:03:22,40
And so, I'm going to create an array and then,

60
00:03:22,40 --> 00:03:29,40
I'm just going to use get(urls[0])

61
00:03:29,40 --> 00:03:36,00
get(urls)[1],

62
00:03:36,00 --> 00:03:39,00
and the same thing for the other two.

63
00:03:39,00 --> 00:03:42,50
So, I'm passing in an array of promises

64
00:03:42,50 --> 00:03:44,70
and then for my Then,

65
00:03:44,70 --> 00:03:46,00
instead of response,

66
00:03:46,00 --> 00:03:47,70
I'm going to change it to responses

67
00:03:47,70 --> 00:03:48,70
just so I am being clear

68
00:03:48,70 --> 00:03:50,20
and a little bit of self-documenting

69
00:03:50,20 --> 00:03:54,10
that I'm expecting back an array.

70
00:03:54,10 --> 00:03:57,80
And, within that anonymous function,

71
00:03:57,80 --> 00:04:01,80
I want to again use the map method.

72
00:04:01,80 --> 00:04:05,00
So, I'm going to return

73
00:04:05,00 --> 00:04:07,90
responses.map.

74
00:04:07,90 --> 00:04:09,80
I'm going to pass in a function

75
00:04:09,80 --> 00:04:13,50
using the parameter name response

76
00:04:13,50 --> 00:04:17,30
and then I'm going to grab the upline we had in here before.

77
00:04:17,30 --> 00:04:20,10
Within my map method, I need to return,

78
00:04:20,10 --> 00:04:25,00
and I'm simply going to return the result of running

79
00:04:25,00 --> 00:04:29,30
that response through the successHandler function.

80
00:04:29,30 --> 00:04:34,30
And so, this is going to give me a template literal

81
00:04:34,30 --> 00:04:38,70
that builds out the data I got back into a chunk of the dom

82
00:04:38,70 --> 00:04:43,00
and then I'm going to return the array containing

83
00:04:43,00 --> 00:04:45,20
all four of those template literals

84
00:04:45,20 --> 00:04:49,60
and that return is going to pass it on down the chain.

85
00:04:49,60 --> 00:04:54,10
And so here, I actually want another Then statement.

86
00:04:54,10 --> 00:04:57,40
So, I'm going to do another .then

87
00:04:57,40 --> 00:05:01,00
and in my anonymous function, I'm going to use the word

88
00:05:01,00 --> 00:05:06,30
literals as my parameter name.

89
00:05:06,30 --> 00:05:09,40
And so, I'm getting an array of template literals here.

90
00:05:09,40 --> 00:05:16,40
And so, I'm going to use that

91
00:05:16,40 --> 00:05:18,50
weatherDiv reference

92
00:05:18,50 --> 00:05:21,90
and I want to set it's innerHTML.

93
00:05:21,90 --> 00:05:23,80
And the first thing I want of that innerHTML

94
00:05:23,80 --> 00:05:25,80
is that H1 element we had earlier.

95
00:05:25,80 --> 00:05:27,10
I just want that once.

96
00:05:27,10 --> 00:05:29,80
So a template literal, which is an h1 containing

97
00:05:29,80 --> 00:05:33,80
the word weather.

98
00:05:33,80 --> 00:05:37,70
And then,

99
00:05:37,70 --> 00:05:41,70
a reference to literals.join

100
00:05:41,70 --> 00:05:43,90
with no space.

101
00:05:43,90 --> 00:05:47,30
And so, that's going to pull together all four of those

102
00:05:47,30 --> 00:05:49,50
array values, those template literals,

103
00:05:49,50 --> 00:05:51,60
join them up with what I've specified here

104
00:05:51,60 --> 00:05:54,90
and specify all of that as the innerHTML for the weatherDiv.

105
00:05:54,90 --> 00:05:59,00
So that gets all of our data for all four of those requests

106
00:05:59,00 --> 00:06:00,30
into the dom.

107
00:06:00,30 --> 00:06:03,10
I still have the same failHandler

108
00:06:03,10 --> 00:06:06,90
and then my Finally function is going to make that

109
00:06:06,90 --> 00:06:08,50
weatherDiv visible.

110
00:06:08,50 --> 00:06:12,80
So saving those changes and now switching back

111
00:06:12,80 --> 00:06:17,50
to my page and I have weather for four locations,

112
00:06:17,50 --> 00:06:21,00
all within that same weather area.

113
00:06:21,00 --> 00:06:23,90
So, we can see weather for Los Angeles, San Francisco,

114
00:06:23,90 --> 00:06:28,20
Lone Pine near Mount Whitney and Mariposa near Yosemite.

115
00:06:28,20 --> 00:06:32,70
And so, that promises.all method allows us to make

116
00:06:32,70 --> 00:06:36,40
all those requests and wait for that processing

117
00:06:36,40 --> 00:06:39,00
until all of those requests have returned.

