1
00:00:01,00 --> 00:00:04,50
- [Instructor] Promises became part of JavaScript in 2015

2
00:00:04,50 --> 00:00:07,80
and are supported by all modern browsers.

3
00:00:07,80 --> 00:00:10,20
However, code that uses promises

4
00:00:10,20 --> 00:00:14,30
won't work on older browsers or on Opera Mini.

5
00:00:14,30 --> 00:00:17,80
And unlike a lot of other modern JavaScript syntax,

6
00:00:17,80 --> 00:00:23,70
promises can't be simply transpiled by a tool like Babel.

7
00:00:23,70 --> 00:00:26,90
Instead, a number of polyfills are available

8
00:00:26,90 --> 00:00:29,20
to replicate promise syntax

9
00:00:29,20 --> 00:00:31,90
on browsers that don't support it natively.

10
00:00:31,90 --> 00:00:34,50
This polyfill written by Taylor Hakes

11
00:00:34,50 --> 00:00:40,20
makes a minified version available via CDN.

12
00:00:40,20 --> 00:00:44,30
So I'm going to copy that, and then in my editor,

13
00:00:44,30 --> 00:00:49,70
I'm actually going to work with the HTML file.

14
00:00:49,70 --> 00:00:52,30
And in this file, I have my JavaScript files

15
00:00:52,30 --> 00:00:54,50
in the Head section, and I need to make sure

16
00:00:54,50 --> 00:00:57,30
to insert the code for the promises polyfill

17
00:00:57,30 --> 00:01:01,00
before the code for my ajax which uses it.

18
00:01:01,00 --> 00:01:04,80
So I'm going to insert that new script element

19
00:01:04,80 --> 00:01:07,20
on the line before the script element

20
00:01:07,20 --> 00:01:10,40
that references ajax.js.

21
00:01:10,40 --> 00:01:15,70
So I'm going to save that, and going back to my browser,

22
00:01:15,70 --> 00:01:18,00
there's no change.

23
00:01:18,00 --> 00:01:20,10
There's nothing in the console.

24
00:01:20,10 --> 00:01:23,60
So the polyfill doesn't change anything in a modern browser,

25
00:01:23,60 --> 00:01:25,80
but it will make the code we've written work

26
00:01:25,80 --> 00:01:29,00
for users of older browsers as well.

