1
00:00:00,70 --> 00:00:02,30
- [Instructor] Callbacks are really handy

2
00:00:02,30 --> 00:00:04,90
for basic cases where we want to execute

3
00:00:04,90 --> 00:00:08,90
additional code after an asynchronous function completes.

4
00:00:08,90 --> 00:00:12,10
And as you might imagine, it's even possible

5
00:00:12,10 --> 00:00:13,90
to string callbacks together,

6
00:00:13,90 --> 00:00:17,30
pretty much as far as we need to.

7
00:00:17,30 --> 00:00:20,10
However, when we start working with code

8
00:00:20,10 --> 00:00:23,20
that uses multiple chained callbacks,

9
00:00:23,20 --> 00:00:25,00
it can be hard to write code that's both

10
00:00:25,00 --> 00:00:29,70
well organized and easy for humans to read.

11
00:00:29,70 --> 00:00:33,90
To solve this problem, modern JavaScript supports promises,

12
00:00:33,90 --> 00:00:37,00
which allow us to string together multiple callbacks

13
00:00:37,00 --> 00:00:38,90
while maintaining well organized

14
00:00:38,90 --> 00:00:41,80
and human readable code.

15
00:00:41,80 --> 00:00:44,30
A promise is an object that represents

16
00:00:44,30 --> 00:00:48,20
the eventual result of an asynchronous operation.

17
00:00:48,20 --> 00:00:51,30
A promise contains information about the operation

18
00:00:51,30 --> 00:00:53,60
and tracks its status.

19
00:00:53,60 --> 00:00:56,40
It has a state property, which can be pending,

20
00:00:56,40 --> 00:01:00,60
fulfilled, or rejected, and it has a result property

21
00:01:00,60 --> 00:01:03,60
which starts as undefined, and can be given

22
00:01:03,60 --> 00:01:07,20
a value based on the operation.

23
00:01:07,20 --> 00:01:09,10
When the operation is complete,

24
00:01:09,10 --> 00:01:12,50
the promise executes one of two methods,

25
00:01:12,50 --> 00:01:16,10
resolve, meaning that the operation was successful,

26
00:01:16,10 --> 00:01:19,10
or reject, meaning that an error occurred.

27
00:01:19,10 --> 00:01:22,50
These methods change the state and result properties

28
00:01:22,50 --> 00:01:26,50
to reflect the outcome of the operation.

29
00:01:26,50 --> 00:01:30,30
You create a promise using the promise constructor.

30
00:01:30,30 --> 00:01:32,00
You can put code within a promise

31
00:01:32,00 --> 00:01:35,80
to do most anything you want, but a promise is most useful

32
00:01:35,80 --> 00:01:39,00
with an asynchronous task, using the resolve

33
00:01:39,00 --> 00:01:41,30
or reject callbacks.

34
00:01:41,30 --> 00:01:45,10
When a promise resolves, the result is returned.

35
00:01:45,10 --> 00:01:48,90
When a promise rejects, the error is returned.

36
00:01:48,90 --> 00:01:51,00
What makes promises easier for humans

37
00:01:51,00 --> 00:01:54,00
to read and understand than nested callbacks

38
00:01:54,00 --> 00:01:59,30
is the syntax used to work with the result of a promise.

39
00:01:59,30 --> 00:02:02,80
The promise specification allows additional methods

40
00:02:02,80 --> 00:02:07,00
to be chained to the code that calls the original promise,

41
00:02:07,00 --> 00:02:10,20
in order to work with the result of the promise.

42
00:02:10,20 --> 00:02:12,60
To work with the result of a resolved promise,

43
00:02:12,60 --> 00:02:16,10
you can add the then method.

44
00:02:16,10 --> 00:02:18,20
Note that there are a few common ways

45
00:02:18,20 --> 00:02:21,80
you might see the promise then code organized,

46
00:02:21,80 --> 00:02:24,10
because JavaScript ignores white space,

47
00:02:24,10 --> 00:02:26,50
it's fine to use any combination of line breaks

48
00:02:26,50 --> 00:02:30,10
and indents that works for you.

49
00:02:30,10 --> 00:02:33,10
Whatever format you choose, you have the possibility

50
00:02:33,10 --> 00:02:36,10
of stringing together a number of then methods,

51
00:02:36,10 --> 00:02:37,90
all indented the same amount,

52
00:02:37,90 --> 00:02:41,20
and with a clear flow from one to the next,

53
00:02:41,20 --> 00:02:43,80
making the progression of code visually obvious

54
00:02:43,80 --> 00:02:47,00
to you and other developers.

