1
00:00:01,10 --> 00:00:03,40
- (Voice-over) So far our code returns an object

2
00:00:03,40 --> 00:00:06,30
from a promise and that means we're on the right track.

3
00:00:06,30 --> 00:00:09,30
But we want to do something with that object.

4
00:00:09,30 --> 00:00:11,50
The object returned by a resolve method

5
00:00:11,50 --> 00:00:14,10
isn't meant for direct examination.

6
00:00:14,10 --> 00:00:15,70
It, itself, is a promise

7
00:00:15,70 --> 00:00:19,40
and requires specific syntax to work with.

8
00:00:19,40 --> 00:00:22,50
To handle the object returned from a successful promise,

9
00:00:22,50 --> 00:00:26,30
we simple append the then method to the promise call.

10
00:00:26,30 --> 00:00:29,30
Then expects a promise as an argument

11
00:00:29,30 --> 00:00:30,70
and enables you to work with

12
00:00:30,70 --> 00:00:34,90
the result data embedded in that object.

13
00:00:34,90 --> 00:00:38,30
And you can chain as many then methods as you want

14
00:00:38,30 --> 00:00:39,90
one after another,

15
00:00:39,90 --> 00:00:41,00
allowing you to work with

16
00:00:41,00 --> 00:00:43,80
data synchronously or asynchronously

17
00:00:43,80 --> 00:00:47,90
and hand off the result to another function at each step.

18
00:00:47,90 --> 00:00:50,30
And this makes code easier to read than

19
00:00:50,30 --> 00:00:53,00
nested callbacks sometimes too.

20
00:00:53,00 --> 00:00:56,10
In our get function for the Explore California site,

21
00:00:56,10 --> 00:01:00,20
we want to dig into the data return from out HTTP request.

22
00:01:00,20 --> 00:01:06,00
So to do that, instead of logging that object to the console

23
00:01:06,00 --> 00:01:09,40
I'm going to make an actual straight-up call

24
00:01:09,40 --> 00:01:11,60
to my method get.

25
00:01:11,60 --> 00:01:15,90
And then I'm going to add dot then

26
00:01:15,90 --> 00:01:18,50
to the end of that function call.

27
00:01:18,50 --> 00:01:22,80
I'm going to pass an anonymous function to the then method

28
00:01:22,80 --> 00:01:27,40
and I'm going to use a parameter name of result

29
00:01:27,40 --> 00:01:31,30
to make it clear that that's what I'm starting with here.

30
00:01:31,30 --> 00:01:32,90
Now the result that I get from the promise

31
00:01:32,90 --> 00:01:34,90
should be a JSON string

32
00:01:34,90 --> 00:01:36,90
and so I can simply hand that off

33
00:01:36,90 --> 00:01:38,90
to my success handler function.

34
00:01:38,90 --> 00:01:40,70
And so my success handler function

35
00:01:40,70 --> 00:01:43,60
is still totally ready to pick that up,

36
00:01:43,60 --> 00:01:47,10
and so I can simply call success handler

37
00:01:47,10 --> 00:01:52,10
and pass it my response parameter.

38
00:01:52,10 --> 00:01:55,50
And so my success handler should actually go ahead

39
00:01:55,50 --> 00:01:57,60
and grab that data and as long as we have a good request

40
00:01:57,60 --> 00:01:59,80
we should see that in the dot.

41
00:01:59,80 --> 00:02:01,50
So I'm going to save my changes

42
00:02:01,50 --> 00:02:02,90
and I've got an error there because

43
00:02:02,90 --> 00:02:04,60
I'm not using the parameter name

44
00:02:04,60 --> 00:02:08,60
Let me just change that so these are both response.

45
00:02:08,60 --> 00:02:11,20
So my parameter is response and then I'm using that

46
00:02:11,20 --> 00:02:14,30
when I'm calling success handler.

47
00:02:14,30 --> 00:02:17,00
And there we go. No errors in the console

48
00:02:17,00 --> 00:02:19,70
and I have my weather being displayed once again.

49
00:02:19,70 --> 00:02:21,80
And so now we actually have weather

50
00:02:21,80 --> 00:02:24,70
being displayed on this page as a result

51
00:02:24,70 --> 00:02:28,80
of a resolved promise handing off the data

52
00:02:28,80 --> 00:02:32,70
to our success handler function.

53
00:02:32,70 --> 00:02:35,10
And so we're getting that same functionality

54
00:02:35,10 --> 00:02:39,00
but by using promises our code is better organized.

