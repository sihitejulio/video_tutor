1
00:00:00,80 --> 00:00:03,00
- [Instructor] In some cases, you need more than one

2
00:00:03,00 --> 00:00:05,80
promise to finish before moving on to code

3
00:00:05,80 --> 00:00:07,80
that works with the results.

4
00:00:07,80 --> 00:00:11,40
While you could write somewhat involved code to do this,

5
00:00:11,40 --> 00:00:13,70
promises include an all method

6
00:00:13,70 --> 00:00:16,60
that allows you to specify an array containing

7
00:00:16,60 --> 00:00:20,30
multiple promises, and move into any chained methods,

8
00:00:20,30 --> 00:00:25,30
like then and catch only when all promises have finished.

9
00:00:25,30 --> 00:00:28,60
The promise.all method returns an array of results

10
00:00:28,60 --> 00:00:31,50
from the promises it was passed.

11
00:00:31,50 --> 00:00:34,00
For our Explore California page,

12
00:00:34,00 --> 00:00:38,00
we're currently including weather for only Los Angeles,

13
00:00:38,00 --> 00:00:40,50
but California is a big state.

14
00:00:40,50 --> 00:00:42,40
At the very least we could be including

15
00:00:42,40 --> 00:00:44,40
weather for San Francisco as well,

16
00:00:44,40 --> 00:00:46,50
and maybe even some tourist destinations

17
00:00:46,50 --> 00:00:47,80
known for their extreme weather,

18
00:00:47,80 --> 00:00:53,00
like Yosemite National Park in Mount Whitney.

19
00:00:53,00 --> 00:00:54,80
So switching over to our code,

20
00:00:54,80 --> 00:00:56,40
we need to rewrite things a little bit

21
00:00:56,40 --> 00:00:59,70
to work with arrays, and then we need to actually

22
00:00:59,70 --> 00:01:04,90
create our code using promise.all.

23
00:01:04,90 --> 00:01:08,50
So our get request is going to be fine,

24
00:01:08,50 --> 00:01:10,50
but our success handler is going to have

25
00:01:10,50 --> 00:01:14,40
to work with individual promises.

26
00:01:14,40 --> 00:01:16,70
And so one thing we need to do is make sure

27
00:01:16,70 --> 00:01:18,60
we're not putting the weather h1

28
00:01:18,60 --> 00:01:20,70
at the beginning of each of our responses,

29
00:01:20,70 --> 00:01:22,60
'cause we only need it once.

30
00:01:22,60 --> 00:01:25,40
So I'm just going to cut that out here,

31
00:01:25,40 --> 00:01:31,90
and we'll recreate that later on in our promise.all code.

32
00:01:31,90 --> 00:01:35,60
I'm also going to change my constant name to div,

33
00:01:35,60 --> 00:01:37,70
just to make it clear that I've rewritten this

34
00:01:37,70 --> 00:01:41,60
and it's different than the one I had before.

35
00:01:41,60 --> 00:01:43,80
Now all of my references are still going to work

36
00:01:43,80 --> 00:01:46,70
for the data that I'm going to be getting in here,

37
00:01:46,70 --> 00:01:49,40
and then at the end here, I'm going to need

38
00:01:49,40 --> 00:01:54,90
to just return that div variable that I just created.

39
00:01:54,90 --> 00:01:59,50
And then I won't actually need to do any dal

40
00:01:59,50 --> 00:02:02,30
manipulation right here, that's going to have to come later,

41
00:02:02,30 --> 00:02:05,60
so I'm just going to comment out that final line.

42
00:02:05,60 --> 00:02:08,30
So now we have our success handle function

43
00:02:08,30 --> 00:02:09,70
ready to go to work with promises.all.

44
00:02:09,70 --> 00:02:12,00
And so to get our app working,

45
00:02:12,00 --> 00:02:16,00
we'll just need to rebuild the function call.

