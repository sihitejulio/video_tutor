1
00:00:00,90 --> 00:00:04,20
- [Narrator] In addition to the then and catch methods,

2
00:00:04,20 --> 00:00:08,40
promises also support chaining a finally method.

3
00:00:08,40 --> 00:00:10,70
You use finally to specify statements

4
00:00:10,70 --> 00:00:12,20
that should be executed

5
00:00:12,20 --> 00:00:16,10
no matter whether promise resolved, or rejected

6
00:00:16,10 --> 00:00:19,70
and so examining our code,

7
00:00:19,70 --> 00:00:21,70
we have things in pretty good shape right now.

8
00:00:21,70 --> 00:00:24,70
We have a successful request that's doing some

9
00:00:24,70 --> 00:00:25,80
dominant manipulation.

10
00:00:25,80 --> 00:00:29,20
We have error handling for unsuccessful requests

11
00:00:29,20 --> 00:00:31,10
or network errors,

12
00:00:31,10 --> 00:00:33,20
and so if would look through our code,

13
00:00:33,20 --> 00:00:35,10
one thing we can notice

14
00:00:35,10 --> 00:00:40,00
is that between the success handler and fail handler,

15
00:00:40,00 --> 00:00:43,10
we have a little bit of repetition.

16
00:00:43,10 --> 00:00:47,70
We in both cases, notice on line 45 and 51,

17
00:00:47,70 --> 00:00:50,80
we are removing the hidden class

18
00:00:50,80 --> 00:00:53,90
from our class list to either display the weather

19
00:00:53,90 --> 00:00:57,00
or for the fallback to display that image,

20
00:00:57,00 --> 00:00:59,40
and so, we should be able to just combine these

21
00:00:59,40 --> 00:01:02,40
into a single statement that happens,

22
00:01:02,40 --> 00:01:05,50
no matter whether we have a success or failure,

23
00:01:05,50 --> 00:01:07,70
and so, the statement as written,

24
00:01:07,70 --> 00:01:10,00
using the weatherDiv variable,

25
00:01:10,00 --> 00:01:12,50
relies on this statement defining that variable.

26
00:01:12,50 --> 00:01:14,80
That's used elsewhere in our success handler

27
00:01:14,80 --> 00:01:18,00
and so we can't take that out of both places,

28
00:01:18,00 --> 00:01:21,70
but we do need it for our always code.

29
00:01:21,70 --> 00:01:27,00
I'm going to copy lines 50 and 51 from the failHandler.

30
00:01:27,00 --> 00:01:30,70
I'm going to scroll down to my get call

31
00:01:30,70 --> 00:01:34,50
and then after the then and the catch.

32
00:01:34,50 --> 00:01:35,60
I'm going to start a new line

33
00:01:35,60 --> 00:01:38,60
and I'm going to do a dot finally,

34
00:01:38,60 --> 00:01:41,00
pass that anonymous function,

35
00:01:41,00 --> 00:01:43,80
it does not need a perimeter

36
00:01:43,80 --> 00:01:47,00
and I'm going to paste in those two lines that I just copied.

37
00:01:47,00 --> 00:01:51,10
Here we're first selecting an element from the dom,

38
00:01:51,10 --> 00:01:55,00
and then we are removing a class

39
00:01:55,00 --> 00:01:57,60
from that element,

40
00:01:57,60 --> 00:02:01,20
and so now with that code being executed

41
00:02:01,20 --> 00:02:04,30
in our finally method,

42
00:02:04,30 --> 00:02:10,20
I can scroll up and take that out of the failHandler.

43
00:02:10,20 --> 00:02:14,10
I can also take this final line,

44
00:02:14,10 --> 00:02:16,20
out of the success handler

45
00:02:16,20 --> 00:02:19,70
but I do need to keep line 44 in here.

46
00:02:19,70 --> 00:02:22,10
We still have to change the inner HTML

47
00:02:22,10 --> 00:02:24,80
only in the success handler.

48
00:02:24,80 --> 00:02:27,90
I've taken out the final line of the success handler.

49
00:02:27,90 --> 00:02:31,40
The final two lines of the failHandler,

50
00:02:31,40 --> 00:02:34,00
and so saving my changes,

51
00:02:34,00 --> 00:02:35,80
switching back to the browser,

52
00:02:35,80 --> 00:02:38,30
things are still working there just fine,

53
00:02:38,30 --> 00:02:41,40
so the success handler isn't making

54
00:02:41,40 --> 00:02:43,30
that weather section visible,

55
00:02:43,30 --> 00:02:48,00
but the finally method is doing that instead,

56
00:02:48,00 --> 00:02:54,00
and if we go back and test with a bad api key.

57
00:02:54,00 --> 00:02:56,30
I'm just replacing my existing api key

58
00:02:56,30 --> 00:03:00,80
with an empty string so I'm sure ill get a bad response,

59
00:03:00,80 --> 00:03:02,90
and so we did indeed get an error

60
00:03:02,90 --> 00:03:07,00
and we've got that fall back image being displayed,

61
00:03:07,00 --> 00:03:09,70
so again that's the finally method

62
00:03:09,70 --> 00:03:14,20
that is making that section of the page visible,

63
00:03:14,20 --> 00:03:16,10
and so here finally has allowed us

64
00:03:16,10 --> 00:03:17,80
to clean up our code a little bit,

65
00:03:17,80 --> 00:03:20,80
to reduce a little bit of duplication

66
00:03:20,80 --> 00:03:23,60
and pull together this statement

67
00:03:23,60 --> 00:03:25,50
that needs to happen no matter what,

68
00:03:25,50 --> 00:03:28,90
to just run after everything else has happened.

69
00:03:28,90 --> 00:03:32,00
Either through a resolve or a reject.

