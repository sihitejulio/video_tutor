1
00:00:01,10 --> 00:00:03,00
- [Instructor] Our Explore California site

2
00:00:03,00 --> 00:00:06,10
uses an Ajax request to get weather,

3
00:00:06,10 --> 00:00:09,40
and a callback to work with the data received.

4
00:00:09,40 --> 00:00:11,10
But what happens when the response

5
00:00:11,10 --> 00:00:14,30
contains something other than the data we expect?

6
00:00:14,30 --> 00:00:17,00
We can receive an authentication error,

7
00:00:17,00 --> 00:00:19,60
a notification that the service is down,

8
00:00:19,60 --> 00:00:22,60
or any one of a number of other responses

9
00:00:22,60 --> 00:00:25,80
that don't include the data we're looking for.

10
00:00:25,80 --> 00:00:26,90
To deal with this,

11
00:00:26,90 --> 00:00:29,00
it's common to use error checking

12
00:00:29,00 --> 00:00:32,10
in conjunction with multiple callbacks.

13
00:00:32,10 --> 00:00:34,00
So when the response comes in,

14
00:00:34,00 --> 00:00:36,20
instead of simply assuming we have good data

15
00:00:36,20 --> 00:00:38,40
and calling the success callback,

16
00:00:38,40 --> 00:00:41,60
we can check what kind of response we got.

17
00:00:41,60 --> 00:00:45,20
If our Ajax request results in a 200 response,

18
00:00:45,20 --> 00:00:47,10
then we know the request succeeded

19
00:00:47,10 --> 00:00:50,70
and we have data so we can call the success callback.

20
00:00:50,70 --> 00:00:52,80
But for any other response code,

21
00:00:52,80 --> 00:00:55,70
we may or may not be getting the data we expect,

22
00:00:55,70 --> 00:00:57,40
and we're probably not.

23
00:00:57,40 --> 00:01:00,20
So we want to instead call a different callback function

24
00:01:00,20 --> 00:01:01,60
to examine the response

25
00:01:01,60 --> 00:01:04,80
and figure out how best to deal with it.

26
00:01:04,80 --> 00:01:06,80
So in my code,

27
00:01:06,80 --> 00:01:08,00
the first thing I want to do

28
00:01:08,00 --> 00:01:10,10
is update that get function

29
00:01:10,10 --> 00:01:12,90
because in addition to a success callback,

30
00:01:12,90 --> 00:01:15,70
I want to specify a fail callback.

31
00:01:15,70 --> 00:01:17,80
So I want to make it possible

32
00:01:17,80 --> 00:01:22,10
to actually specify what callback should be called

33
00:01:22,10 --> 00:01:24,80
in case I don't get good data back.

34
00:01:24,80 --> 00:01:26,60
And so the next thing I'm going to do

35
00:01:26,60 --> 00:01:30,10
is change my onload function,

36
00:01:30,10 --> 00:01:33,30
so instead of just assuming success,

37
00:01:33,30 --> 00:01:36,40
I'm going to use an if-else construction.

38
00:01:36,40 --> 00:01:41,50
So I'm going to check if httpRequest.status,

39
00:01:41,50 --> 00:01:45,10
which gives us the numerical status code of the response,

40
00:01:45,10 --> 00:01:48,10
I want to see if that status code is 200.

41
00:01:48,10 --> 00:01:48,90
If it is,

42
00:01:48,90 --> 00:01:50,30
we know everything's good.

43
00:01:50,30 --> 00:01:52,70
So in that case,

44
00:01:52,70 --> 00:01:54,30
we are just going to use

45
00:01:54,30 --> 00:02:00,90
our existing success function call,

46
00:02:00,90 --> 00:02:03,30
but otherwise,

47
00:02:03,30 --> 00:02:05,50
we may need to dig in a little bit deeper.

48
00:02:05,50 --> 00:02:07,20
And in this case,

49
00:02:07,20 --> 00:02:08,70
just to get started,

50
00:02:08,70 --> 00:02:11,40
I'm going to use our fail callback

51
00:02:11,40 --> 00:02:14,50
and I'm just going to send it

52
00:02:14,50 --> 00:02:18,80
the status property of our XHR object,

53
00:02:18,80 --> 00:02:23,00
and that will give us that actual status code.

54
00:02:23,00 --> 00:02:26,50
And so I have my successHandler

55
00:02:26,50 --> 00:02:29,10
and now I want to write another function

56
00:02:29,10 --> 00:02:31,80
that I'm going to call failHandler,

57
00:02:31,80 --> 00:02:33,50
and I want that to take a parameter,

58
00:02:33,50 --> 00:02:36,50
which is going to be called status,

59
00:02:36,50 --> 00:02:38,50
and that will be our status code.

60
00:02:38,50 --> 00:02:40,30
And so the first thing I want to do

61
00:02:40,30 --> 00:02:42,90
is console.log

62
00:02:42,90 --> 00:02:44,50
that status code,

63
00:02:44,50 --> 00:02:45,80
and so that may be useful

64
00:02:45,80 --> 00:02:48,40
if we have a user who's actually reporting an issue,

65
00:02:48,40 --> 00:02:50,40
who can give us that status code.

66
00:02:50,40 --> 00:02:52,70
And then I actually have a fallback image

67
00:02:52,70 --> 00:02:55,20
already built into my app

68
00:02:55,20 --> 00:02:57,80
and so I'm just going to add a couple lines

69
00:02:57,80 --> 00:02:59,60
of DOM manipulation

70
00:02:59,60 --> 00:03:02,80
to forget about the weather report

71
00:03:02,80 --> 00:03:05,10
and just show an image.

72
00:03:05,10 --> 00:03:08,50
And I can actually copy those lines from up above.

73
00:03:08,50 --> 00:03:11,20
So up in my successHandler,

74
00:03:11,20 --> 00:03:13,30
line 16 on my screen,

75
00:03:13,30 --> 00:03:15,90
defines a constant called weatherDiv,

76
00:03:15,90 --> 00:03:18,20
so I'm going to copy that

77
00:03:18,20 --> 00:03:21,50
and stick that in my failHandler.

78
00:03:21,50 --> 00:03:25,50
And then up here on line 32,

79
00:03:25,50 --> 00:03:29,80
we've got weatherDiv.classList.remove,

80
00:03:29,80 --> 00:03:31,30
and I'm going to copy that

81
00:03:31,30 --> 00:03:33,20
and stick that

82
00:03:33,20 --> 00:03:35,90
as the last line of my failHandler.

83
00:03:35,90 --> 00:03:37,40
And so this is basically

84
00:03:37,40 --> 00:03:39,90
just going to

85
00:03:39,90 --> 00:03:43,10
display this existing section of the page

86
00:03:43,10 --> 00:03:45,90
which includes an image by default.

87
00:03:45,90 --> 00:03:48,60
So there's one final thing I need to do.

88
00:03:48,60 --> 00:03:51,00
At the very bottom of my code,

89
00:03:51,00 --> 00:03:53,80
I'm calling my get method,

90
00:03:53,80 --> 00:03:55,00
I'm sending it a URL,

91
00:03:55,00 --> 00:03:59,10
specifying successHandler as my success callback,

92
00:03:59,10 --> 00:04:01,50
I also need to specify

93
00:04:01,50 --> 00:04:04,40
my new function failHandler

94
00:04:04,40 --> 00:04:07,80
as my fail fallback.

95
00:04:07,80 --> 00:04:10,10
And so up here in my get function,

96
00:04:10,10 --> 00:04:13,60
it's now expecting three parameters,

97
00:04:13,60 --> 00:04:15,70
and so now I have three arguments

98
00:04:15,70 --> 00:04:17,40
in my function call.

99
00:04:17,40 --> 00:04:21,20
So I've saved my code,

100
00:04:21,20 --> 00:04:25,40
switching back to my app in the browser,

101
00:04:25,40 --> 00:04:26,80
nothing looks different.

102
00:04:26,80 --> 00:04:29,90
So the success is still working it looks like,

103
00:04:29,90 --> 00:04:32,20
I'm still getting successful information back

104
00:04:32,20 --> 00:04:34,00
having that displayed,

105
00:04:34,00 --> 00:04:35,20
but let's go back to the code

106
00:04:35,20 --> 00:04:37,10
and simulate a problem.

107
00:04:37,10 --> 00:04:38,20
So

108
00:04:38,20 --> 00:04:39,60
I'm going to

109
00:04:39,60 --> 00:04:41,00
duplicate the line

110
00:04:41,00 --> 00:04:42,90
containing my API key,

111
00:04:42,90 --> 00:04:46,10
I'm going to comment out the first one,

112
00:04:46,10 --> 00:04:47,10
and then the second one

113
00:04:47,10 --> 00:04:51,20
I'm going to turn it into an empty string.

114
00:04:51,20 --> 00:04:54,10
And so now when I build out that URL,

115
00:04:54,10 --> 00:04:56,30
it's going to have an invalid API key

116
00:04:56,30 --> 00:04:58,60
that should give me an error

117
00:04:58,60 --> 00:05:00,70
from OpenWeatherMap

118
00:05:00,70 --> 00:05:03,50
and should result in me not actually getting data.

119
00:05:03,50 --> 00:05:05,50
And so with those changes saved,

120
00:05:05,50 --> 00:05:07,00
going back to the browser,

121
00:05:07,00 --> 00:05:08,10
and there we are.

122
00:05:08,10 --> 00:05:09,60
We've got the default image showing

123
00:05:09,60 --> 00:05:11,10
instead of the weather report,

124
00:05:11,10 --> 00:05:14,00
and if I open up the console,

125
00:05:14,00 --> 00:05:16,00
I have

126
00:05:16,00 --> 00:05:17,90
well I have a failure message

127
00:05:17,90 --> 00:05:20,20
from the browser itself,

128
00:05:20,20 --> 00:05:24,30
I also have that failure status code of 401

129
00:05:24,30 --> 00:05:27,30
logged from my code in case that's useful.

130
00:05:27,30 --> 00:05:28,30
And clearly,

131
00:05:28,30 --> 00:05:29,70
it'd be helpful,

132
00:05:29,70 --> 00:05:32,90
there are a lot of different HTTP status codes

133
00:05:32,90 --> 00:05:34,30
that we could get back,

134
00:05:34,30 --> 00:05:37,10
and they tell us lots of information

135
00:05:37,10 --> 00:05:38,90
about what has happened.

136
00:05:38,90 --> 00:05:40,80
So there may be indeed be some codes

137
00:05:40,80 --> 00:05:42,90
we can figure out a way to reformulate

138
00:05:42,90 --> 00:05:44,70
our query and resubmit it,

139
00:05:44,70 --> 00:05:46,10
some of them are dead ends,

140
00:05:46,10 --> 00:05:48,00
but this gives us a good start

141
00:05:48,00 --> 00:05:51,40
toward dealing with a known success

142
00:05:51,40 --> 00:05:53,50
and dealing with anything else

143
00:05:53,50 --> 00:05:54,70
and we can certainly

144
00:05:54,70 --> 00:05:59,00
make this more fine-grained down the road.

