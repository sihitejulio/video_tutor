1
00:00:00,80 --> 00:00:03,60
- [Narrator] We can build program flow in Javascript

2
00:00:03,60 --> 00:00:08,40
in two different ways: synchronously and asynchrounously.

3
00:00:08,40 --> 00:00:11,90
Using synchronous program flow, code is executed

4
00:00:11,90 --> 00:00:15,10
in the order it appears in your Javascript files.

5
00:00:15,10 --> 00:00:18,60
So here, lines one through five of the code

6
00:00:18,60 --> 00:00:20,40
are executed first.

7
00:00:20,40 --> 00:00:24,40
Followed by lines six through ten and then lines

8
00:00:24,40 --> 00:00:27,00
eleven through fifteen are executed next,

9
00:00:27,00 --> 00:00:31,30
matching the order of statements in the code as written.

10
00:00:31,30 --> 00:00:33,90
In synchronous program flow, the browser

11
00:00:33,90 --> 00:00:38,10
parses and executes only one statement at a time.

12
00:00:38,10 --> 00:00:41,10
This results in our statements being executed

13
00:00:41,10 --> 00:00:43,80
in order.

14
00:00:43,80 --> 00:00:46,50
Synchronous program flow exhibits a behavior

15
00:00:46,50 --> 00:00:48,10
known as blocking.

16
00:00:48,10 --> 00:00:50,40
Meaning that later statements are prevented

17
00:00:50,40 --> 00:00:53,80
from executing until earlier statements are finished.

18
00:00:53,80 --> 00:00:56,70
In many cases, this is what we want.

19
00:00:56,70 --> 00:00:59,40
For instance, a value needs to be calculated

20
00:00:59,40 --> 00:01:02,20
first before it can be used.

21
00:01:02,20 --> 00:01:04,60
But if an earlier statement may take awhile to

22
00:01:04,60 --> 00:01:07,80
execute and statements that follow don't rely

23
00:01:07,80 --> 00:01:10,40
on that earlier statement having finished

24
00:01:10,40 --> 00:01:13,40
then synchronous program flow can needlessly

25
00:01:13,40 --> 00:01:15,40
slow down our apps.

26
00:01:15,40 --> 00:01:20,90
In this case we want to use asynchronous program flow.

27
00:01:20,90 --> 00:01:22,70
When we write this type of code, also known

28
00:01:22,70 --> 00:01:26,30
as asynchronous code or simply, async.

29
00:01:26,30 --> 00:01:28,90
The parser can start executing some code

30
00:01:28,90 --> 00:01:31,80
and continue that execution while other code

31
00:01:31,80 --> 00:01:34,40
is executed, as well.

32
00:01:34,40 --> 00:01:37,00
So here, lines one through five of the code

33
00:01:37,00 --> 00:01:40,00
start execution but these include some activity

34
00:01:40,00 --> 00:01:41,60
that's going to take awhile.

35
00:01:41,60 --> 00:01:45,40
Now technically, Javascript has only a single thread

36
00:01:45,40 --> 00:01:48,40
but it does have a mechanism that allows some processes

37
00:01:48,40 --> 00:01:51,10
to execute in parallel while other things

38
00:01:51,10 --> 00:01:52,50
are going on.

39
00:01:52,50 --> 00:01:55,10
So here this first set of statements is moved

40
00:01:55,10 --> 00:01:57,90
into that parallel process which continues

41
00:01:57,90 --> 00:02:00,90
executing even as the main program flow

42
00:02:00,90 --> 00:02:03,60
moves to the next set of statements.

43
00:02:03,60 --> 00:02:06,60
Now that main program flow continues synchronous

44
00:02:06,60 --> 00:02:09,20
execution so it keeps moving through the remaining

45
00:02:09,20 --> 00:02:11,70
code while that first block of code

46
00:02:11,70 --> 00:02:13,80
is executing in parallel.

47
00:02:13,80 --> 00:02:16,40
Now remember that synchronous code is executed

48
00:02:16,40 --> 00:02:18,50
only in the main program flow.

49
00:02:18,50 --> 00:02:21,00
While we've seen that asynchronous code is instead

50
00:02:21,00 --> 00:02:24,60
moved over to execute in parallel.

51
00:02:24,60 --> 00:02:29,00
This means that the statements are executed simultaneously.

52
00:02:29,00 --> 00:02:32,00
The upshot of this architecture is that asynchronous

53
00:02:32,00 --> 00:02:36,20
program flow lets us write code that's non-blocking.

54
00:02:36,20 --> 00:02:39,40
We write asynchronous code when we want to give

55
00:02:39,40 --> 00:02:41,70
part of our code time to complete, while

56
00:02:41,70 --> 00:02:44,50
still allowing the parsers to execute the code

57
00:02:44,50 --> 00:02:46,50
that follows right away.

58
00:02:46,50 --> 00:02:49,00
One of the most common situations where we use

59
00:02:49,00 --> 00:02:53,20
asynchronous program flow is when we make ajax requests.

60
00:02:53,20 --> 00:02:56,40
We write code using XHR or fetch or maybe

61
00:02:56,40 --> 00:02:59,40
a library specific method that sends a request

62
00:02:59,40 --> 00:03:01,90
for data to a remote server.

63
00:03:01,90 --> 00:03:04,50
This code defaults to an asynchronous mode,

64
00:03:04,50 --> 00:03:07,10
meaning that the request gets sent off and while

65
00:03:07,10 --> 00:03:09,50
the parser is waiting for a response the main

66
00:03:09,50 --> 00:03:13,00
program flow moves on to execute other code,

67
00:03:13,00 --> 00:03:16,60
like responding to user interactions.

68
00:03:16,60 --> 00:03:19,40
Another common situation where asynchronous flow

69
00:03:19,40 --> 00:03:21,60
is useful is setting a timer

70
00:03:21,60 --> 00:03:24,00
using something like set time out.

71
00:03:24,00 --> 00:03:26,70
The timer starts and while it's counting down,

72
00:03:26,70 --> 00:03:31,00
the main program flow can go on to execute other code.

