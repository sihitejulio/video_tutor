1
00:00:00,80 --> 00:00:01,70
- [Instructor] Let's take a look at

2
00:00:01,70 --> 00:00:04,90
what a callback function looks like in practice.

3
00:00:04,90 --> 00:00:05,90
We're going to work with the

4
00:00:05,90 --> 00:00:08,40
fictitious Explore California site,

5
00:00:08,40 --> 00:00:11,60
and we're going to implement a feature using a callback.

6
00:00:11,60 --> 00:00:13,90
We want to add a feature to the Tours page

7
00:00:13,90 --> 00:00:16,40
that shows the current weather in Los Angeles.

8
00:00:16,40 --> 00:00:20,20
Here's what that looks like in the end file for this video.

9
00:00:20,20 --> 00:00:22,20
We're going to create an AJAX request

10
00:00:22,20 --> 00:00:25,90
to open weather map to get current weather data for LA.

11
00:00:25,90 --> 00:00:28,90
And then we want to incorporate the data that's returned

12
00:00:28,90 --> 00:00:33,10
into the DOM so it's displayed in the browser window.

13
00:00:33,10 --> 00:00:39,70
And so to do that, I want to create an XMLHttpRequest object.

14
00:00:39,70 --> 00:00:43,00
So I'm going to create that here.

15
00:00:43,00 --> 00:00:46,70
I'm going to call it httpRequest,

16
00:00:46,70 --> 00:00:51,80
and set that to new XMLHttpRequest.

17
00:00:51,80 --> 00:00:55,30
And then my plan is to create a function called get

18
00:00:55,30 --> 00:00:59,80
where I can pass in a URL to actually make that httpRequest.

19
00:00:59,80 --> 00:01:03,40
So I'll create a function called get,

20
00:01:03,40 --> 00:01:06,60
primary name will be URL.

21
00:01:06,60 --> 00:01:11,40
And we'll start by referencing our variable httpRequest,

22
00:01:11,40 --> 00:01:13,70
and we'll use the open method,

23
00:01:13,70 --> 00:01:21,60
passing in the get http verb and that URL parameter.

24
00:01:21,60 --> 00:01:24,30
So that's going to create our request.

25
00:01:24,30 --> 00:01:26,20
And then we also have to send that request

26
00:01:26,20 --> 00:01:29,10
so we're going to do httpRequest.send

27
00:01:29,10 --> 00:01:34,20
and that'll actually send it off to the web service.

28
00:01:34,20 --> 00:01:37,80
Now I've created a function here called tempToF

29
00:01:37,80 --> 00:01:40,70
because the weather that we actually get back

30
00:01:40,70 --> 00:01:43,00
from Open Weather Map is in Kelvin,

31
00:01:43,00 --> 00:01:45,40
it's not used in actually understanding

32
00:01:45,40 --> 00:01:46,80
what temperature it is outside

33
00:01:46,80 --> 00:01:49,60
and so this function will simply take a Kelvin temperature

34
00:01:49,60 --> 00:01:52,00
and give you back a Fahrenheit temperature.

35
00:01:52,00 --> 00:01:54,80
And then we've got a function called success handler.

36
00:01:54,80 --> 00:01:59,00
And this is actually going to take the data that we get back

37
00:01:59,00 --> 00:02:01,40
and it's going to render it into the dom.

38
00:02:01,40 --> 00:02:04,50
This uses a template literal,

39
00:02:04,50 --> 00:02:06,20
and then some Vanilla JavaScript

40
00:02:06,20 --> 00:02:09,20
to actually add the structure to the dom.

41
00:02:09,20 --> 00:02:10,60
And so then down here I've got another

42
00:02:10,60 --> 00:02:13,20
EventListener for when the dom is finished loading.

43
00:02:13,20 --> 00:02:15,60
I have an API key for Open Weather Map,

44
00:02:15,60 --> 00:02:18,50
now this is an API key that won't be active

45
00:02:18,50 --> 00:02:19,60
when you're watching this video

46
00:02:19,60 --> 00:02:22,80
so you need to go to Open Weather Map and get your own.

47
00:02:22,80 --> 00:02:25,20
And I also have a variable called URL

48
00:02:25,20 --> 00:02:28,80
which is storing the constructed URL

49
00:02:28,80 --> 00:02:31,50
to get the weather from Los Angeles.

50
00:02:31,50 --> 00:02:34,80
I got this URL just by looking at the

51
00:02:34,80 --> 00:02:36,80
API documentation for Open Weather Map

52
00:02:36,80 --> 00:02:38,60
so like most other APIs, there's

53
00:02:38,60 --> 00:02:40,40
really good documentation here.

54
00:02:40,40 --> 00:02:41,70
And I'm looking for the current

55
00:02:41,70 --> 00:02:43,10
weather data for one location,

56
00:02:43,10 --> 00:02:45,90
so they're showing me some sample strings,

57
00:02:45,90 --> 00:02:48,20
some sample queries, that I can construct

58
00:02:48,20 --> 00:02:51,90
and so that's what I used to create this URL.

59
00:02:51,90 --> 00:02:53,80
So all I should have to do then

60
00:02:53,80 --> 00:02:57,20
is call that new function that I just created

61
00:02:57,20 --> 00:03:01,10
called get, I'm going to pass it in that URL,

62
00:03:01,10 --> 00:03:03,70
and then once I've made that call,

63
00:03:03,70 --> 00:03:06,40
I want to add this data to the dom.

64
00:03:06,40 --> 00:03:11,00
And so I'm going to call my successHandler function

65
00:03:11,00 --> 00:03:16,60
and I'm going to pass it httpRequest.responseText

66
00:03:16,60 --> 00:03:19,30
so we are getting JSON back from this API

67
00:03:19,30 --> 00:03:21,70
that comes back in the response text

68
00:03:21,70 --> 00:03:24,50
property of that XHR object.

69
00:03:24,50 --> 00:03:34,20
And so let's see what happens, going to save that code.

70
00:03:34,20 --> 00:03:39,70
Going to switch over to the HDML file and go live with that.

71
00:03:39,70 --> 00:03:41,40
And I don't have any data here.

72
00:03:41,40 --> 00:03:42,70
Nothing's showing up in my dom

73
00:03:42,70 --> 00:03:43,90
like it did in solutions file,

74
00:03:43,90 --> 00:03:46,70
so opening up the console, I've got

75
00:03:46,70 --> 00:03:48,50
an unexpected end of JSON input.

76
00:03:48,50 --> 00:03:50,40
My experience that usually means

77
00:03:50,40 --> 00:03:52,10
you're trying to parse something as JSON

78
00:03:52,10 --> 00:03:54,80
that's not actually JSON or that doesn't exist.

79
00:03:54,80 --> 00:03:57,60
So let's look back at our code.

80
00:03:57,60 --> 00:04:01,00
Now we created an XHR request here

81
00:04:01,00 --> 00:04:03,70
and we sent that off to get data.

82
00:04:03,70 --> 00:04:05,80
So when we called that get function,

83
00:04:05,80 --> 00:04:08,30
we're sending off a request and then immediately,

84
00:04:08,30 --> 00:04:11,50
synchronously we are calling this successHandler function

85
00:04:11,50 --> 00:04:16,00
and saying hey take that data and do something with it.

86
00:04:16,00 --> 00:04:18,90
But since we know that an XHR request

87
00:04:18,90 --> 00:04:21,30
is generally going to take at least

88
00:04:21,30 --> 00:04:23,60
a couple seconds to make that round trip,

89
00:04:23,60 --> 00:04:26,00
to make the request, get the data processed,

90
00:04:26,00 --> 00:04:29,70
and then get it back, we are calling our

91
00:04:29,70 --> 00:04:31,70
successHandler function before we

92
00:04:31,70 --> 00:04:33,70
actually have any data for it,

93
00:04:33,70 --> 00:04:36,00
because we're using a synchronous model here.

94
00:04:36,00 --> 00:04:38,90
We're just saying do this thing then do this thing.

95
00:04:38,90 --> 00:04:41,60
And our code is not waiting around

96
00:04:41,60 --> 00:04:44,80
for that getRequest to return any data.

97
00:04:44,80 --> 00:04:49,10
And so one thing we can do instead is to use a callback.

98
00:04:49,10 --> 00:04:53,40
And so I'm going to tweak my get function here

99
00:04:53,40 --> 00:04:56,60
to take a second parameter called success

100
00:04:56,60 --> 00:04:59,10
and this is going to be a callback.

101
00:04:59,10 --> 00:05:01,40
And then I can take advantage of one

102
00:05:01,40 --> 00:05:05,10
of the properties of the XHR object.

103
00:05:05,10 --> 00:05:06,90
I can actually specify callback functions

104
00:05:06,90 --> 00:05:09,10
to respond to specific events.

105
00:05:09,10 --> 00:05:12,20
And so referencing that object,

106
00:05:12,20 --> 00:05:16,10
I can use the onload property to respond

107
00:05:16,10 --> 00:05:18,60
when we actually get a response.

108
00:05:18,60 --> 00:05:21,50
And I'm going to use an anonymous function

109
00:05:21,50 --> 00:05:24,60
to call that success callback

110
00:05:24,60 --> 00:05:30,90
and pass it httpRequest.resonseText.

111
00:05:30,90 --> 00:05:32,50
So once I get that data back,

112
00:05:32,50 --> 00:05:34,40
the next thing I want to happen is for

113
00:05:34,40 --> 00:05:39,30
that data to be passed along to this callback function.

114
00:05:39,30 --> 00:05:41,50
And then I just need to comment out

115
00:05:41,50 --> 00:05:43,60
my call to successHandler down here

116
00:05:43,60 --> 00:05:48,20
because I don't need that anymore.

117
00:05:48,20 --> 00:05:50,10
Oh, so I still have to pass in

118
00:05:50,10 --> 00:05:52,50
that callback function in my get call.

119
00:05:52,50 --> 00:05:55,00
So here I'm calling URL and I need

120
00:05:55,00 --> 00:05:56,60
to pass in the name of my callback

121
00:05:56,60 --> 00:06:00,20
function which is successHandler.

122
00:06:00,20 --> 00:06:03,70
Save that, switch back to my browser

123
00:06:03,70 --> 00:06:07,80
and there's my weather and so we used

124
00:06:07,80 --> 00:06:09,90
a callback function to wait for

125
00:06:09,90 --> 00:06:11,80
the response from the remote server

126
00:06:11,80 --> 00:06:15,50
and then used that callback function as the next step

127
00:06:15,50 --> 00:06:19,00
to work with that data in our asynchronous model.

