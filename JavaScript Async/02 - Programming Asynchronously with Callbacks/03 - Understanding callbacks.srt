1
00:00:00,80 --> 00:00:02,50
- [Instructor] JavaScript includes a handful

2
00:00:02,50 --> 00:00:03,50
of built-in functions

3
00:00:03,50 --> 00:00:07,20
that are executed asynchronously by default.

4
00:00:07,20 --> 00:00:08,60
These include ajax() methods,

5
00:00:08,60 --> 00:00:13,40
such as those in an XMLHttpRequest Object

6
00:00:13,40 --> 00:00:16,70
as well as timing functions like setTimeout()

7
00:00:16,70 --> 00:00:20,40
and animation methods like requestAnimationFrame().

8
00:00:20,40 --> 00:00:22,70
These asynchronous methods and functions

9
00:00:22,70 --> 00:00:24,40
have long provided the foundation

10
00:00:24,40 --> 00:00:28,50
for building asynchronous code in Javascript.

11
00:00:28,50 --> 00:00:30,10
Sometimes we want to specify

12
00:00:30,10 --> 00:00:33,00
additional functions or methods to execute

13
00:00:33,00 --> 00:00:36,30
after an asynchronous function has finished.

14
00:00:36,30 --> 00:00:38,10
Traditionally, this has been accomplished

15
00:00:38,10 --> 00:00:39,90
using the callback pattern.

16
00:00:39,90 --> 00:00:44,00
In this pattern, an asynchronous function is called

17
00:00:44,00 --> 00:00:46,50
and it's execution happens in parallel

18
00:00:46,50 --> 00:00:49,50
while the main program flow continues.

19
00:00:49,50 --> 00:00:51,50
This asynchronous function takes

20
00:00:51,50 --> 00:00:54,00
another function as an argument.

21
00:00:54,00 --> 00:00:55,00
After the statements

22
00:00:55,00 --> 00:00:57,40
of the original function are executed,

23
00:00:57,40 --> 00:01:01,20
the function passed as an argument is called.

24
00:01:01,20 --> 00:01:04,60
This function in known as a callback function.

25
00:01:04,60 --> 00:01:08,30
A callback enables you to specify what should happen next

26
00:01:08,30 --> 00:01:13,10
after a function is executed asynchronously.

27
00:01:13,10 --> 00:01:16,50
For instance, the asynchronous setTimeout() function

28
00:01:16,50 --> 00:01:20,00
takes two arguments, a callback function,

29
00:01:20,00 --> 00:01:23,30
and delay time in milliseconds.

30
00:01:23,30 --> 00:01:27,60
In this code, the first argument is the callback function,

31
00:01:27,60 --> 00:01:29,50
and in this case I have specified

32
00:01:29,50 --> 00:01:32,50
an anonymous function right in the function column.

33
00:01:32,50 --> 00:01:35,70
This a pretty common way to use setTimeout().

34
00:01:35,70 --> 00:01:38,50
And the callback function simply logs "Hello world"

35
00:01:38,50 --> 00:01:40,40
to the console.

36
00:01:40,40 --> 00:01:42,00
After the callback function

37
00:01:42,00 --> 00:01:44,20
is the second and final argument

38
00:01:44,20 --> 00:01:47,80
which is the length of the delay in milliseconds.

39
00:01:47,80 --> 00:01:50,70
Here I've specified 5,000 milliseconds

40
00:01:50,70 --> 00:01:52,90
which is the same as five seconds.

41
00:01:52,90 --> 00:01:55,70
So when this code runs, the setTimeout() function

42
00:01:55,70 --> 00:01:58,70
first delays for 5,000 milliseconds,

43
00:01:58,70 --> 00:02:01,00
and then calls the callback function

44
00:02:01,00 --> 00:02:04,80
which logs "Hello world" to the console.

45
00:02:04,80 --> 00:02:06,60
When setTimeout() is called,

46
00:02:06,60 --> 00:02:09,20
it's moved out of the main program flow

47
00:02:09,20 --> 00:02:12,70
and then pauses for the specified number of milliseconds

48
00:02:12,70 --> 00:02:15,00
which in this case is 5,000.

49
00:02:15,00 --> 00:02:16,40
After the time is elapsed,

50
00:02:16,40 --> 00:02:19,20
The callback function is called

51
00:02:19,20 --> 00:02:23,20
and "Hello world" is logged to the console.

52
00:02:23,20 --> 00:02:26,90
So let's take a look at this in practice.

53
00:02:26,90 --> 00:02:31,50
So I've written up some sample code that does three things.

54
00:02:31,50 --> 00:02:33,00
We're basically just logging three things

55
00:02:33,00 --> 00:02:34,80
to the console here.

56
00:02:34,80 --> 00:02:38,30
So first, we log the word "Hi" to the console,

57
00:02:38,30 --> 00:02:40,00
and that's going to happen immediately

58
00:02:40,00 --> 00:02:41,60
when the script loads.

59
00:02:41,60 --> 00:02:44,20
And next, we call setTimeout()

60
00:02:44,20 --> 00:02:45,40
passing it a function

61
00:02:45,40 --> 00:02:49,20
that logs the text "Asynchronous result" to the console,

62
00:02:49,20 --> 00:02:52,90
but only after 5,000 milliseconds have passed.

63
00:02:52,90 --> 00:02:54,60
And then finally, we have text

64
00:02:54,60 --> 00:02:58,30
that logs "Synchronous result" to the console.

65
00:02:58,30 --> 00:02:59,80
Now remember that setTimeout()

66
00:02:59,80 --> 00:03:02,40
is an asynchronous function.

67
00:03:02,40 --> 00:03:05,40
So, according to what we've just learned,

68
00:03:05,40 --> 00:03:07,80
"Hi" should log to the console first,

69
00:03:07,80 --> 00:03:09,80
setTimeout() should start running

70
00:03:09,80 --> 00:03:11,70
but should be set aside,

71
00:03:11,70 --> 00:03:15,60
run in parallel and wait 5,000 milliseconds.

72
00:03:15,60 --> 00:03:18,20
But the main program flow will move on

73
00:03:18,20 --> 00:03:20,20
to that final console.log statement

74
00:03:20,20 --> 00:03:22,00
and log "Synchronous result"

75
00:03:22,00 --> 00:03:25,30
before that setTimeout() is finished.

76
00:03:25,30 --> 00:03:29,70
And so I have my index.html file running

77
00:03:29,70 --> 00:03:32,50
using Live Server so I need

78
00:03:32,50 --> 00:03:34,50
to kill my previous Live Server,

79
00:03:34,50 --> 00:03:37,70
and then with the index.html file open,

80
00:03:37,70 --> 00:03:39,90
going to hit GoLive,

81
00:03:39,90 --> 00:03:43,80
and in my browser I'm going to open up the console.

82
00:03:43,80 --> 00:03:46,20
And just to get rid of that live reload text,

83
00:03:46,20 --> 00:03:47,80
I'm going to reload my page one more time

84
00:03:47,80 --> 00:03:48,90
and just see the flow here.

85
00:03:48,90 --> 00:03:51,40
So immediately the word "Hi" is logged,

86
00:03:51,40 --> 00:03:54,70
and "Synchronous result" is logged almost immediately.

87
00:03:54,70 --> 00:03:56,90
Then notice that it took about five seconds

88
00:03:56,90 --> 00:03:58,90
for that text "Asynchronous result"

89
00:03:58,90 --> 00:04:00,80
to be logged to the console.

90
00:04:00,80 --> 00:04:05,00
So in this case, our code ran that setTimeout() function,

91
00:04:05,00 --> 00:04:09,20
and when setTimeout() was finished running asynchronously,

92
00:04:09,20 --> 00:04:12,30
it went ahead and called this callback function

93
00:04:12,30 --> 00:04:16,40
and this other anonymous function ran after setTimeout().

94
00:04:16,40 --> 00:04:20,40
And so, callback functions are a tool that allow us

95
00:04:20,40 --> 00:04:22,10
to specify what happens next

96
00:04:22,10 --> 00:04:26,20
after a function is executed asynchronously.

97
00:04:26,20 --> 00:04:29,50
Now callbacks can also be used synchronously

98
00:04:29,50 --> 00:04:31,40
to simply specify what happens

99
00:04:31,40 --> 00:04:34,50
after code is executed in the main program flow.

100
00:04:34,50 --> 00:04:37,20
And developers do this all the time,

101
00:04:37,20 --> 00:04:39,40
but as we've seen here callbacks are

102
00:04:39,40 --> 00:04:41,40
an especially important tool

103
00:04:41,40 --> 00:04:44,00
when building asynchronous code.

