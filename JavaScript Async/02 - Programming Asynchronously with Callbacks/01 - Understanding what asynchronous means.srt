1
00:00:00,70 --> 00:00:02,80
- [Instructor] The term asynchronous has

2
00:00:02,80 --> 00:00:05,80
technical applications in the field of programming.

3
00:00:05,80 --> 00:00:08,40
However, it also fundamentally describes

4
00:00:08,40 --> 00:00:12,00
experiences that we all go through every day.

5
00:00:12,00 --> 00:00:16,30
Now, asynchronous is the opposite of synchronous.

6
00:00:16,30 --> 00:00:19,50
When a set of things has to happen one after another,

7
00:00:19,50 --> 00:00:21,80
that's a synchronous process.

8
00:00:21,80 --> 00:00:25,30
For instance, imagine you go to the movies,

9
00:00:25,30 --> 00:00:27,70
and you get in line to buy a ticket.

10
00:00:27,70 --> 00:00:30,80
If there are three people in line in front of you,

11
00:00:30,80 --> 00:00:33,40
and just one cashier, you have to wait

12
00:00:33,40 --> 00:00:35,20
until each of the people in front of you

13
00:00:35,20 --> 00:00:38,60
buy their tickets before you can buy yours.

14
00:00:38,60 --> 00:00:40,60
That's a synchronous process,

15
00:00:40,60 --> 00:00:42,50
because you can't make your purchase

16
00:00:42,50 --> 00:00:45,70
until all the other purchasers who are queued up

17
00:00:45,70 --> 00:00:48,00
in front of you have been served.

18
00:00:48,00 --> 00:00:52,00
Synchronous processes are sometimes described as blocking,

19
00:00:52,00 --> 00:00:54,80
because the next step in the process is blocked

20
00:00:54,80 --> 00:00:58,70
from happening until the current step is finished.

21
00:00:58,70 --> 00:01:00,90
Now, instead of a movie ticket line,

22
00:01:00,90 --> 00:01:03,40
imagine you go to a sit-down restaurant.

23
00:01:03,40 --> 00:01:07,30
Now, if this worked synchronously, like our movie theater,

24
00:01:07,30 --> 00:01:09,90
you might wait in line behind everybody else

25
00:01:09,90 --> 00:01:12,10
who wanted to eat at the restaurant.

26
00:01:12,10 --> 00:01:16,10
There would be one table at the restaurant and one employee.

27
00:01:16,10 --> 00:01:18,90
They'd take the customer's order, go back to the kitchen

28
00:01:18,90 --> 00:01:21,20
and cook it, serve it to the customer,

29
00:01:21,20 --> 00:01:23,40
clear the table when the customer was finished,

30
00:01:23,40 --> 00:01:26,40
present the bill, take payment, and then prepare

31
00:01:26,40 --> 00:01:28,20
the table for another customer.

32
00:01:28,20 --> 00:01:30,10
This would never work for a restaurant,

33
00:01:30,10 --> 00:01:32,70
because preparing and eating a meal takes way too long

34
00:01:32,70 --> 00:01:35,80
for anyone to want to wait for their turn.

35
00:01:35,80 --> 00:01:39,90
Fortunately, restaurants work on an asynchronous model,

36
00:01:39,90 --> 00:01:43,60
in which multiple things can happen at the same time.

37
00:01:43,60 --> 00:01:46,60
So, if my friends and I sit down at a restaurant,

38
00:01:46,60 --> 00:01:50,00
a server might bring us a menu and take our drink order.

39
00:01:50,00 --> 00:01:52,20
Meanwhile, another server might be serving food

40
00:01:52,20 --> 00:01:54,30
to people at a different table.

41
00:01:54,30 --> 00:01:57,50
Just having multiple people working at the same time

42
00:01:57,50 --> 00:02:00,30
means that multiple things can happen at once.

43
00:02:00,30 --> 00:02:02,90
Multiple servers are taking and serving orders,

44
00:02:02,90 --> 00:02:05,20
the cooks are in the kitchen preparing meals,

45
00:02:05,20 --> 00:02:08,30
other people are busing tables and setting them.

46
00:02:08,30 --> 00:02:11,40
As a result, many people can be at different stages

47
00:02:11,40 --> 00:02:15,00
of ordering, eating, and paying simultaneously,

48
00:02:15,00 --> 00:02:18,00
with the blocking kept to a minimum.

