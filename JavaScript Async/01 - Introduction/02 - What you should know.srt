1
00:00:00,90 --> 00:00:03,30
- [Instructor] This course assumes you have some experience

2
00:00:03,30 --> 00:00:08,70
coding in JavaScript and making API requests using HTTP.

3
00:00:08,70 --> 00:00:11,90
If you don't have experience with JavaScript, a basic

4
00:00:11,90 --> 00:00:15,20
JavaScript course would be a great place to start.

5
00:00:15,20 --> 00:00:18,00
To get an overview of the modern JavaScript tools we'll be

6
00:00:18,00 --> 00:00:22,80
using, check out a course on ES6 or ECMAScript 6.

7
00:00:22,80 --> 00:00:26,30
For the basics of working with APIs, explore a course

8
00:00:26,30 --> 00:00:28,00
on Ajax.

