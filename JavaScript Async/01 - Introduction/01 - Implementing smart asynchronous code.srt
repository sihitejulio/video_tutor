1
00:00:01,00 --> 00:00:03,20
- [Sasha] Asynchronous code is a foundation

2
00:00:03,20 --> 00:00:04,90
of the modern web.

3
00:00:04,90 --> 00:00:07,50
But understanding what's going on, line by line,

4
00:00:07,50 --> 00:00:11,00
in a code snippet can be a challenge, and choosing between

5
00:00:11,00 --> 00:00:14,30
the many asynchronous options in modern JavaScript

6
00:00:14,30 --> 00:00:16,20
can be overwhelming.

7
00:00:16,20 --> 00:00:19,20
Revisiting basic structures like the XHR object

8
00:00:19,20 --> 00:00:21,60
and set timeout can help you nail down

9
00:00:21,60 --> 00:00:25,60
the basic concepts and flow of asynchronous code.

10
00:00:25,60 --> 00:00:28,30
And getting your hands dirty with newer syntaxes

11
00:00:28,30 --> 00:00:30,70
like promises and async and await

12
00:00:30,70 --> 00:00:33,30
prepares you for most any asynchronous challenge

13
00:00:33,30 --> 00:00:35,00
that might be waiting for you.

14
00:00:35,00 --> 00:00:36,80
In my LinkedIn Learning course,

15
00:00:36,80 --> 00:00:39,10
I use asynchronous code structures

16
00:00:39,10 --> 00:00:42,30
to solve concrete problems on a website.

17
00:00:42,30 --> 00:00:45,10
I'm Sasha Vodnik, and I've been writing JavaScript

18
00:00:45,10 --> 00:00:47,70
since the browser wars of the '90s.

19
00:00:47,70 --> 00:00:51,20
If you've waited long enough to master async,

20
00:00:51,20 --> 00:00:53,50
I invite you to join me on this course

21
00:00:53,50 --> 00:00:55,60
on asynchronous JavaScript.

22
00:00:55,60 --> 00:00:57,00
Let's get started.

