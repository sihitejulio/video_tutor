1
00:00:00,80 --> 00:00:02,80
- [Narrator] Finally, the videos in this course

2
00:00:02,80 --> 00:00:06,60
make use of data from openweathermap.org.

3
00:00:06,60 --> 00:00:10,10
Data requests from this service require an API key,

4
00:00:10,10 --> 00:00:12,40
which is a unique identifier that associates

5
00:00:12,40 --> 00:00:14,90
you with a request coming in.

6
00:00:14,90 --> 00:00:17,50
The service provides a limited amount of data for free

7
00:00:17,50 --> 00:00:20,60
and by having your own key rather than sharing one

8
00:00:20,60 --> 00:00:22,60
you can ensure that you stay below the limit

9
00:00:22,60 --> 00:00:24,30
of free requests.

10
00:00:24,30 --> 00:00:26,60
To get data from this service you need to sign up

11
00:00:26,60 --> 00:00:29,00
for an account with openweathermap.org

12
00:00:29,00 --> 00:00:33,00
and obtain an API key, which you'll use when creating

13
00:00:33,00 --> 00:00:35,00
your Ajax request for data.

14
00:00:35,00 --> 00:00:37,40
So let's set that up now.

15
00:00:37,40 --> 00:00:40,00
Openweathermap.org makes weather information available

16
00:00:40,00 --> 00:00:42,60
under a creative commons license.

17
00:00:42,60 --> 00:00:45,70
Data is free for up to 60 requests per minute

18
00:00:45,70 --> 00:00:48,00
which will work fine for this course.

19
00:00:48,00 --> 00:00:50,10
It's only more intensive uses that require

20
00:00:50,10 --> 00:00:51,70
a paid subscription.

21
00:00:51,70 --> 00:00:54,40
To get an API key, go to openweathermap.org

22
00:00:54,40 --> 00:00:59,00
in your browser and then up here at the top click API.

23
00:00:59,00 --> 00:01:03,10
This documents the terms as well as endpoints

24
00:01:03,10 --> 00:01:05,80
that you can hit with your Ajax requests.

25
00:01:05,80 --> 00:01:07,50
So to sign up for an account we can click

26
00:01:07,50 --> 00:01:10,10
the sign up link at the top,

27
00:01:10,10 --> 00:01:12,60
set up a user name and password.

28
00:01:12,60 --> 00:01:15,10
For this course I'm going to use the username

29
00:01:15,10 --> 00:01:22,60
asynccourse and create a password in both boxes.

30
00:01:22,60 --> 00:01:26,80
Make sure you're clear on what you're agreeing to here.

31
00:01:26,80 --> 00:01:29,70
Make sure they understand you're not a robot

32
00:01:29,70 --> 00:01:32,20
and create your account.

33
00:01:32,20 --> 00:01:34,30
They ask you some questions which I am just

34
00:01:34,30 --> 00:01:36,40
going to cancel out of for now.

35
00:01:36,40 --> 00:01:39,00
And so now I'm logged in.

36
00:01:39,00 --> 00:01:41,60
And so we now have another nav bar

37
00:01:41,60 --> 00:01:45,20
and I'm going to click API keys here

38
00:01:45,20 --> 00:01:48,60
and that's going to show me the API key for my account.

39
00:01:48,60 --> 00:01:50,70
So I'm just double-click there.

40
00:01:50,70 --> 00:01:53,50
I'm going to copy that to the clipboard

41
00:01:53,50 --> 00:01:55,20
and then I want to put it somewhere where I can

42
00:01:55,20 --> 00:01:56,90
access it easily.

43
00:01:56,90 --> 00:01:59,20
Now I'm on a Mac so I'm going to open up the notes app.

44
00:01:59,20 --> 00:02:03,10
You can certainly use text edit on a Windows machine

45
00:02:03,10 --> 00:02:06,30
and in a new note I'm just going to paste my API key

46
00:02:06,30 --> 00:02:07,20
in there.

47
00:02:07,20 --> 00:02:13,50
Maybe give myself a little note.

48
00:02:13,50 --> 00:02:16,20
So that's my openweathermap API key.

49
00:02:16,20 --> 00:02:18,00
Now the key you'll see me using in the videos

50
00:02:18,00 --> 00:02:20,20
for this course has been deactivated so it's important

51
00:02:20,20 --> 00:02:24,00
that you use your own key because this one won't work.

52
00:02:24,00 --> 00:02:26,30
So whenever you need to access your API key

53
00:02:26,30 --> 00:02:29,00
during this course you can just go back to your note,

54
00:02:29,00 --> 00:02:32,00
copy the text of the key and continue coding.

55
00:02:32,00 --> 00:02:34,00
Now let's get started.

