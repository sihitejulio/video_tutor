1
00:00:00,70 --> 00:00:01,80
- [Instructor] To work along with me

2
00:00:01,80 --> 00:00:03,30
on the files for this course,

3
00:00:03,30 --> 00:00:05,30
you need three applications,

4
00:00:05,30 --> 00:00:10,30
a web browser, a code editor, and an HTTP server.

5
00:00:10,30 --> 00:00:14,90
You also need an API key from OpenWeatherMap.org.

6
00:00:14,90 --> 00:00:17,30
You undoubtedly already have a web browser

7
00:00:17,30 --> 00:00:18,60
installed on your machine,

8
00:00:18,60 --> 00:00:20,80
and any major modern browser,

9
00:00:20,80 --> 00:00:23,50
Chrome, Firefox, or Microsoft Edge

10
00:00:23,50 --> 00:00:25,50
is fine for this course.

11
00:00:25,50 --> 00:00:27,60
I'll be using Chrome in these videos,

12
00:00:27,60 --> 00:00:30,00
which is a popular choice among web developers

13
00:00:30,00 --> 00:00:31,90
because of the extensive and powerful

14
00:00:31,90 --> 00:00:34,30
developer tools it has built in.

15
00:00:34,30 --> 00:00:36,70
Because we'll only be using the console though,

16
00:00:36,70 --> 00:00:40,40
that won't really a difference for this course.

17
00:00:40,40 --> 00:00:43,10
A number of great code editors are available,

18
00:00:43,10 --> 00:00:45,70
both free and paid apps.

19
00:00:45,70 --> 00:00:49,10
Any editor that lets you edit and save plain text

20
00:00:49,10 --> 00:00:50,70
is fine for this course.

21
00:00:50,70 --> 00:00:52,80
So if you have a code editor you like,

22
00:00:52,80 --> 00:00:56,70
such as Sublime Text or Atom, it's fine to use that.

23
00:00:56,70 --> 00:00:59,40
I use Visual Studio Code in these videos,

24
00:00:59,40 --> 00:01:02,40
which is a version of Microsoft Visual Studio

25
00:01:02,40 --> 00:01:05,10
created specifically for web development.

26
00:01:05,10 --> 00:01:07,00
Visual Studio Code is free

27
00:01:07,00 --> 00:01:10,70
and has Windows, Mac, and Linux releases.

28
00:01:10,70 --> 00:01:12,40
The code is available on GitHub,

29
00:01:12,40 --> 00:01:15,70
and users can submit issues there as well.

30
00:01:15,70 --> 00:01:18,20
I've customized my editor with a few extensions

31
00:01:18,20 --> 00:01:22,10
that are particularly useful for this course.

32
00:01:22,10 --> 00:01:25,90
Bracket Pair Colorizer by CoenraadS

33
00:01:25,90 --> 00:01:28,50
assigns different colors to the parens,

34
00:01:28,50 --> 00:01:31,90
braces, and brackets throughout your code.

35
00:01:31,90 --> 00:01:33,90
It uses the same color for the start

36
00:01:33,90 --> 00:01:36,00
and end bracket in each pair

37
00:01:36,00 --> 00:01:39,40
and it uses contrasting colors for nested pairs.

38
00:01:39,40 --> 00:01:41,30
In code with a lot of nesting,

39
00:01:41,30 --> 00:01:42,70
this extension makes it easier

40
00:01:42,70 --> 00:01:45,00
to identify which opening character

41
00:01:45,00 --> 00:01:48,50
a closing bracket corresponds to.

42
00:01:48,50 --> 00:01:51,80
Identicator by SirTori adds a vertical line

43
00:01:51,80 --> 00:01:55,70
to the editor window for each indent level.

44
00:01:55,70 --> 00:01:58,50
And it highlights the line for the parent element

45
00:01:58,50 --> 00:02:01,40
of the active line of code.

46
00:02:01,40 --> 00:02:03,50
This creates another visual cue

47
00:02:03,50 --> 00:02:05,30
to help you identify where a statement

48
00:02:05,30 --> 00:02:10,00
or block fits into the nesting hierarchy.

49
00:02:10,00 --> 00:02:13,50
And finally, I use the Live Server extension

50
00:02:13,50 --> 00:02:17,10
by Ritwick Dey as my HTTP server.

51
00:02:17,10 --> 00:02:20,70
So this extension launces an HTTP server

52
00:02:20,70 --> 00:02:23,30
and opens the current document in your browser

53
00:02:23,30 --> 00:02:27,60
using the HTTP protocol rather than using the file system.

54
00:02:27,60 --> 00:02:31,10
This is invaluable for testing API requests and responses,

55
00:02:31,10 --> 00:02:33,40
which are often blocked for security reasons

56
00:02:33,40 --> 00:02:40,20
when initiated from a document using the file protocol.

57
00:02:40,20 --> 00:02:42,20
Live Server adds a Go Live icon

58
00:02:42,20 --> 00:02:45,00
at the bottom of the Visual Studio Code window,

59
00:02:45,00 --> 00:02:48,30
which you click to serve and open the current document.

60
00:02:48,30 --> 00:02:49,60
Just note that when testing front

61
00:02:49,60 --> 00:02:51,50
end web code using Live Server,

62
00:02:51,50 --> 00:02:55,10
you need to launch the server on an HTML document.

63
00:02:55,10 --> 00:02:57,50
So right now I have a JavaScript file open,

64
00:02:57,50 --> 00:03:00,00
and if I click Go Live,

65
00:03:00,00 --> 00:03:02,60
it's going to open a live view of that file,

66
00:03:02,60 --> 00:03:08,30
which is just the directory of the currently open project.

67
00:03:08,30 --> 00:03:11,30
So to close that Live Server connection,

68
00:03:11,30 --> 00:03:14,00
I just click this icon,

69
00:03:14,00 --> 00:03:17,10
disposes of that connection.

70
00:03:17,10 --> 00:03:19,40
And if I want to see the effect

71
00:03:19,40 --> 00:03:21,00
of my current JavaScript code,

72
00:03:21,00 --> 00:03:24,00
I need the HTML file where that code is showing up,

73
00:03:24,00 --> 00:03:26,70
which in this case is tours.htm.

74
00:03:26,70 --> 00:03:29,50
And I can either open that and use the Go Live icon,

75
00:03:29,50 --> 00:03:31,50
or I can right click it,

76
00:03:31,50 --> 00:03:34,80
and click Open with Live Server.

77
00:03:34,80 --> 00:03:38,10
And that is going to create that connection

78
00:03:38,10 --> 00:03:41,00
with the browser and going to show me the file.

79
00:03:41,00 --> 00:03:43,00
And when I make any changes in the HTML,

80
00:03:43,00 --> 00:03:45,70
they are immediately going to be reflected in the browser.

81
00:03:45,70 --> 00:03:47,70
So that saves me a lot of saving,

82
00:03:47,70 --> 00:03:52,30
reloading, saving, and reloading.

83
00:03:52,30 --> 00:03:54,20
If you're going to use the start files

84
00:03:54,20 --> 00:03:55,80
for each video in this course,

85
00:03:55,80 --> 00:03:57,10
rather than working with one set

86
00:03:57,10 --> 00:03:59,10
of files all the way through,

87
00:03:59,10 --> 00:04:01,30
you need to make sure that you end the server session

88
00:04:01,30 --> 00:04:06,00
for one page before you can start it up for a different one.

