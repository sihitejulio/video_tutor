1
00:00:00,05 --> 00:00:02,00
- [Instructor] To use a web worker,

2
00:00:02,00 --> 00:00:05,04
you send it a message using the PostMessage method.

3
00:00:05,04 --> 00:00:07,02
The method simply passes data

4
00:00:07,02 --> 00:00:09,09
which can range from a simple string or number

5
00:00:09,09 --> 00:00:14,03
to a collection like an array or object.

6
00:00:14,03 --> 00:00:16,04
Back in my filters.js file,

7
00:00:16,04 --> 00:00:19,04
I'm going to start by testing things out.

8
00:00:19,04 --> 00:00:24,01
So, near the top, immediately after my worker variable,

9
00:00:24,01 --> 00:00:26,07
I'm going to send a message.

10
00:00:26,07 --> 00:00:29,06
And to do that, I reference that worker,

11
00:00:29,06 --> 00:00:31,04
that variable name that I created,

12
00:00:31,04 --> 00:00:33,08
and I use the PostMessage method,

13
00:00:33,08 --> 00:00:39,05
and I'm just going to pass text: "Hello worker!"

14
00:00:39,05 --> 00:00:42,04
And then, after that, I'll add a console.log statement

15
00:00:42,04 --> 00:00:46,08
just to say "message sent to worker",

16
00:00:46,08 --> 00:00:49,03
just to verify that I've reached that point in my code.

17
00:00:49,03 --> 00:00:50,08
Now, after I send that message,

18
00:00:50,08 --> 00:00:53,02
I want to make sure that it actually got received.

19
00:00:53,02 --> 00:00:55,04
And to do that, I need to go to the code

20
00:00:55,04 --> 00:01:00,06
for the worker itself, and that's in filter-worker.js.

21
00:01:00,06 --> 00:01:02,07
Within the code for this worker,

22
00:01:02,07 --> 00:01:07,02
I can use this to refer to the worker itself.

23
00:01:07,02 --> 00:01:10,06
And I can set up an event listener for the Message event.

24
00:01:10,06 --> 00:01:12,02
So I'm at the bottom of the file

25
00:01:12,02 --> 00:01:20,02
and I'm using this.AddEventListener, listening for Message.

26
00:01:20,02 --> 00:01:22,01
And, for my function, I'm going to be sure

27
00:01:22,01 --> 00:01:24,03
to grab that Event object.

28
00:01:24,03 --> 00:01:32,03
I'm going to console.log "message received from main script".

29
00:01:32,03 --> 00:01:34,03
That does not need a this on it

30
00:01:34,03 --> 00:01:40,07
and then I'm going to console.log...

31
00:01:40,07 --> 00:01:45,06
Event, dot, data.

32
00:01:45,06 --> 00:01:48,09
I can access the Message data using the data property

33
00:01:48,09 --> 00:01:51,01
of the Event object for the Message event,

34
00:01:51,01 --> 00:01:53,06
and I'm capturing that with that parameter named E.

35
00:01:53,06 --> 00:01:56,00
So console.log, e.data here

36
00:01:56,00 --> 00:01:58,03
actually lets me view the message itself.

37
00:01:58,03 --> 00:02:01,07
So saving those changes, I've got Live Server

38
00:02:01,07 --> 00:02:04,04
fired up in my browser.

39
00:02:04,04 --> 00:02:06,00
And so, when the page loaded,

40
00:02:06,00 --> 00:02:08,07
I got that log that says "message sent to worker"

41
00:02:08,07 --> 00:02:10,07
from filters.js.

42
00:02:10,07 --> 00:02:13,03
And then, over in the Filter Worker,

43
00:02:13,03 --> 00:02:15,03
I've logged "Message received from main script"

44
00:02:15,03 --> 00:02:18,01
and there's the text of the message: "Hello worker!"

45
00:02:18,01 --> 00:02:20,03
So I have successfully sent data

46
00:02:20,03 --> 00:02:24,08
from the main thread to the web worker, which is awesome.

47
00:02:24,08 --> 00:02:27,06
Now, obviously, sending a throwaway string like this

48
00:02:27,06 --> 00:02:31,00
isn't especially useful and I'm going to comment that out,

49
00:02:31,00 --> 00:02:33,04
but I can send all kinds of data.

50
00:02:33,04 --> 00:02:37,04
In my filter.js file, the event listeners,

51
00:02:37,04 --> 00:02:40,06
further down for the Grayscale button,

52
00:02:40,06 --> 00:02:43,00
the Brightness button, and the Threshold button,

53
00:02:43,00 --> 00:02:45,02
all call the ApplyFilter function,

54
00:02:45,02 --> 00:02:50,08
and they pass in the text of the filter that's been chosen

55
00:02:50,08 --> 00:02:54,08
as well as the Level value and an image object.

56
00:02:54,08 --> 00:02:58,00
Now I've moved the ApplyFilter function to my web worker

57
00:02:58,00 --> 00:03:00,01
so, in order to use it, I have to start

58
00:03:00,01 --> 00:03:02,00
by changing the event listeners

59
00:03:02,00 --> 00:03:05,00
to send a message to the web worker with an object

60
00:03:05,00 --> 00:03:08,03
containing those three pieces of data as the content.

61
00:03:08,03 --> 00:03:11,05
And so, starting up in the event listener

62
00:03:11,05 --> 00:03:13,04
for the Grayscale button,

63
00:03:13,04 --> 00:03:18,01
I'm going to comment out this Results variable

64
00:03:18,01 --> 00:03:20,00
which calls the ApplyFilter function

65
00:03:20,00 --> 00:03:22,08
as if it's in the current file.

66
00:03:22,08 --> 00:03:27,07
And instead, I just want to say worker.PostMessage

67
00:03:27,07 --> 00:03:30,04
and I'm going to pass in an object

68
00:03:30,04 --> 00:03:33,08
and I'll have a key called Filter

69
00:03:33,08 --> 00:03:38,00
whose value is going to be the SelectedFilter variable,

70
00:03:38,00 --> 00:03:39,06
a key called Level, whose value

71
00:03:39,06 --> 00:03:41,09
is going to be the Level variable,

72
00:03:41,09 --> 00:03:45,03
and a key called Image, whose value

73
00:03:45,03 --> 00:03:54,05
is going to be ImageDataObj.

74
00:03:54,05 --> 00:03:57,07
And I'll go ahead and copy that same code

75
00:03:57,07 --> 00:04:00,00
to my other two event listeners,

76
00:04:00,00 --> 00:04:03,05
so again, in the Brightness, commenting out results

77
00:04:03,05 --> 00:04:07,09
and adding in this new PostMessage statement

78
00:04:07,09 --> 00:04:11,09
and, likewise, for the Threshold button,

79
00:04:11,09 --> 00:04:14,09
I'm going to comment out Results

80
00:04:14,09 --> 00:04:18,01
and paste in that PostMessage statement.

81
00:04:18,01 --> 00:04:23,02
And so, with that all saved, switching back to my browser,

82
00:04:23,02 --> 00:04:26,09
if I click the Grayscale button, I have an issue.

83
00:04:26,09 --> 00:04:28,04
So let's go back and take a look at that.

84
00:04:28,04 --> 00:04:32,03
When I click Grayscale...

85
00:04:32,03 --> 00:04:37,00
Now my error message says "results not defined"

86
00:04:37,00 --> 00:04:38,04
and that's simply because I've got this

87
00:04:38,04 --> 00:04:41,08
DisplayFilteredImage statement that I need

88
00:04:41,08 --> 00:04:44,02
to comment out as well.

89
00:04:44,02 --> 00:04:51,02
So let's do that here as well as for the Brightness button

90
00:04:51,02 --> 00:04:52,09
and for the Threshold button

91
00:04:52,09 --> 00:04:57,02
and we'll save that and we'll rerun it.

92
00:04:57,02 --> 00:04:59,07
And now, when I click the Grayscale button,

93
00:04:59,07 --> 00:05:03,03
that worker logs the fact that I have sent

94
00:05:03,03 --> 00:05:07,02
over an object containing these three pieces of data.

95
00:05:07,02 --> 00:05:09,07
So the next thing I want to do, then, in my worker,

96
00:05:09,07 --> 00:05:13,06
is to change what that Message event listener does.

97
00:05:13,06 --> 00:05:19,05
And so I can comment out what I've done so far

98
00:05:19,05 --> 00:05:21,02
within that event listener.

99
00:05:21,02 --> 00:05:28,08
Instead, I'm going to create a variable called WorkerResult.

100
00:05:28,08 --> 00:05:31,09
And I want to take those three pieces of data

101
00:05:31,09 --> 00:05:33,07
that I've received and pass them

102
00:05:33,07 --> 00:05:35,02
to the ApplyFilter function.

103
00:05:35,02 --> 00:05:38,03
So I'm going to call ApplyFilter

104
00:05:38,03 --> 00:05:42,01
and I'm going to pass event, dot, data.filter,

105
00:05:42,01 --> 00:05:44,04
which is the first key in my object,

106
00:05:44,04 --> 00:05:47,05
and then the second parameter, event, dot, data.level,

107
00:05:47,05 --> 00:05:48,09
which is the second key,

108
00:05:48,09 --> 00:05:52,08
and then event, dot, data.image.

109
00:05:52,08 --> 00:05:56,08
And so what this means is that, when I receive that data,

110
00:05:56,08 --> 00:05:59,04
I'm going to be running it through ApplyFilter

111
00:05:59,04 --> 00:06:07,06
and then I'll go ahead and console.log WorkerResult.

112
00:06:07,06 --> 00:06:10,01
And we'll check out in the browser what happens.

113
00:06:10,01 --> 00:06:14,02
So, if I click Grayscale, I get some image data.

114
00:06:14,02 --> 00:06:18,00
If I click Brightness, same thing.

115
00:06:18,00 --> 00:06:20,00
And Threshold, same thing.

116
00:06:20,00 --> 00:06:21,07
So notice that the filtering is going on,

117
00:06:21,07 --> 00:06:24,09
so I can see the filters are actually being applied

118
00:06:24,09 --> 00:06:27,06
and the result of calling

119
00:06:27,06 --> 00:06:30,09
that ApplyFilter function is ImageData.

120
00:06:30,09 --> 00:06:34,01
So I'm sending a request to the web worker

121
00:06:34,01 --> 00:06:36,07
in response to a user event on the main thread,

122
00:06:36,07 --> 00:06:39,01
and the web worker is executing that task

123
00:06:39,01 --> 00:06:42,00
in a separate browser thread, and that's amazing.

