1
00:00:00,06 --> 00:00:02,04
- [Instructor] Sending a message from the app

2
00:00:02,04 --> 00:00:05,06
to a web worker is only half the process.

3
00:00:05,06 --> 00:00:08,00
After the web worker does its own processing,

4
00:00:08,00 --> 00:00:11,00
it needs to communicate the result back to the app.

5
00:00:11,00 --> 00:00:14,05
To do this, the web worker uses the post message method,

6
00:00:14,05 --> 00:00:18,08
passing along data as the message.

7
00:00:18,08 --> 00:00:22,02
The web worker code in my filterworker.js file

8
00:00:22,02 --> 00:00:24,02
is receiving requests from the app

9
00:00:24,02 --> 00:00:26,03
and calling the apply filter function,

10
00:00:26,03 --> 00:00:28,03
passing along the data it received.

11
00:00:28,03 --> 00:00:31,03
I'm saving the result in the worker result variable.

12
00:00:31,03 --> 00:00:32,08
To send this back to the app,

13
00:00:32,08 --> 00:00:37,09
I can simply call post message and pass the variable.

14
00:00:37,09 --> 00:00:41,06
So right after my console.log statement,

15
00:00:41,06 --> 00:00:47,00
I'm going to say post message worker result.

16
00:00:47,00 --> 00:00:48,02
And I'll go ahead and comment out that

17
00:00:48,02 --> 00:00:51,08
console.log statement, shouldn't need it anymore.

18
00:00:51,08 --> 00:00:55,06
Then, back in my filters.js file

19
00:00:55,06 --> 00:00:57,00
I still am posting a message

20
00:00:57,00 --> 00:01:00,00
at the very beginning just saying hello worker,

21
00:01:00,00 --> 00:01:01,01
and I don't need that anymore,

22
00:01:01,01 --> 00:01:02,09
so I'm going to comment that out as well.

23
00:01:02,09 --> 00:01:05,09
And then the place that I'm actually

24
00:01:05,09 --> 00:01:08,09
dispatching the messages to the web worker

25
00:01:08,09 --> 00:01:13,06
are in my three event listeners for my buttons,

26
00:01:13,06 --> 00:01:17,06
greyscale, brightness, and threshold buttons.

27
00:01:17,06 --> 00:01:20,04
But the web worker's responses don't necessarily

28
00:01:20,04 --> 00:01:23,02
go back to the location where they were called.

29
00:01:23,02 --> 00:01:25,04
Instead, I need to create an event listener

30
00:01:25,04 --> 00:01:28,09
in the app that listens for that same message event

31
00:01:28,09 --> 00:01:32,06
and works with the data I receive.

32
00:01:32,06 --> 00:01:36,03
So, at the bottom, right before that closing punctuation,

33
00:01:36,03 --> 00:01:39,00
I'm going to add a new event listener.

34
00:01:39,00 --> 00:01:43,01
So that'll be worker.addeventlistener.

35
00:01:43,01 --> 00:01:46,08
I'm going to listen for a message.

36
00:01:46,08 --> 00:01:48,04
And that function is, again,

37
00:01:48,04 --> 00:01:50,07
going to grab that event object.

38
00:01:50,07 --> 00:01:53,09
And first, I will console.log

39
00:01:53,09 --> 00:01:59,02
received data back from worker,

40
00:01:59,02 --> 00:02:03,03
and then I'm going to console.log e.data.

41
00:02:03,03 --> 00:02:05,09
And checking back in my browser,

42
00:02:05,09 --> 00:02:08,04
I'm going to click one of the filter buttons.

43
00:02:08,04 --> 00:02:10,00
And after a second,

44
00:02:10,00 --> 00:02:13,03
and I see the message that I actually

45
00:02:13,03 --> 00:02:14,09
got that data back from the worker,

46
00:02:14,09 --> 00:02:17,03
and that is logged from the filters.js file,

47
00:02:17,03 --> 00:02:18,04
so that's my app.

48
00:02:18,04 --> 00:02:21,02
And then, I also see that app data logged,

49
00:02:21,02 --> 00:02:22,08
again from filters.js.

50
00:02:22,08 --> 00:02:24,05
So that data has been communicated

51
00:02:24,05 --> 00:02:29,00
from filter worker to filters.js.

52
00:02:29,00 --> 00:02:31,05
So next, I simply want to pass the results

53
00:02:31,05 --> 00:02:33,08
to the display filtered image function,

54
00:02:33,08 --> 00:02:36,07
along with the rendering context.

55
00:02:36,07 --> 00:02:39,03
So, this is what I was doing earlier,

56
00:02:39,03 --> 00:02:40,08
the code for my three buttons,

57
00:02:40,08 --> 00:02:45,04
before I replaced those with these calls to post message.

58
00:02:45,04 --> 00:02:48,00
So I'm just going to copy one of those

59
00:02:48,00 --> 00:02:51,05
display filtered image statements.

60
00:02:51,05 --> 00:02:54,05
Scrolling to the bottom in my message event listener,

61
00:02:54,05 --> 00:02:56,04
I'm going to add that in there.

62
00:02:56,04 --> 00:03:00,04
I'm going to take that comment mark out of there.

63
00:03:00,04 --> 00:03:03,09
So, saving that, and then I'm going to test that in my browser.

64
00:03:03,09 --> 00:03:07,00
So, if I hit greyscale, I've got an issue,

65
00:03:07,00 --> 00:03:10,06
and that's because I am trying to use

66
00:03:10,06 --> 00:03:13,04
the word results, but that doesn't actually work.

67
00:03:13,04 --> 00:03:16,01
I have to reference the results that came back,

68
00:03:16,01 --> 00:03:18,08
and that's going to be e.data.

69
00:03:18,08 --> 00:03:21,04
So I'm setting a context like I did before,

70
00:03:21,04 --> 00:03:24,06
but instead of having the name e.data for my data,

71
00:03:24,06 --> 00:03:25,09
I can just reference it directly

72
00:03:25,09 --> 00:03:27,03
in that event object.

73
00:03:27,03 --> 00:03:29,02
So, saving that change,

74
00:03:29,02 --> 00:03:31,08
back in my browser I'm going to hit greyscale,

75
00:03:31,08 --> 00:03:33,08
and I've got a greyscaled image.

76
00:03:33,08 --> 00:03:36,07
Hit brightness, same thing.

77
00:03:36,07 --> 00:03:39,03
I hit threshold, same thing.

78
00:03:39,03 --> 00:03:41,03
And so, I can see that the filtering is happening

79
00:03:41,03 --> 00:03:44,03
in filter worker, and that that data is coming back

80
00:03:44,03 --> 00:03:47,04
into filters.js and getting rendered.

81
00:03:47,04 --> 00:03:49,01
So now let's challenge things a bit.

82
00:03:49,01 --> 00:03:50,06
I'm going to click these filter buttons

83
00:03:50,06 --> 00:03:53,07
a few times quickly to get that few second delay,

84
00:03:53,07 --> 00:03:55,05
and notice that each time I click a button,

85
00:03:55,05 --> 00:03:58,04
I get that blue outline saying you just clicked this.

86
00:03:58,04 --> 00:04:01,08
I can also change images, and they happen right away.

87
00:04:01,08 --> 00:04:04,05
So the image processing is no longer blocking

88
00:04:04,05 --> 00:04:07,02
the main thread because that image processing

89
00:04:07,02 --> 00:04:10,06
has its own separate cue in the separate thread.

90
00:04:10,06 --> 00:04:12,06
My code could use an additional update

91
00:04:12,06 --> 00:04:15,01
to account for this, because right now that cued

92
00:04:15,01 --> 00:04:16,08
image manipulation might overwrite

93
00:04:16,08 --> 00:04:18,04
the more recent image selection.

94
00:04:18,04 --> 00:04:20,04
But the web workers offload that heavy

95
00:04:20,04 --> 00:04:23,04
image processing task to a separate thread

96
00:04:23,04 --> 00:04:25,06
and they make my code more responsive.

97
00:04:25,06 --> 00:04:27,00
And that is a definite win.

