1
00:00:00,05 --> 00:00:02,06
- [Instructor] The ability to create a shared worker

2
00:00:02,06 --> 00:00:05,00
was originally developed as a way to avoid

3
00:00:05,00 --> 00:00:07,06
duplicating resources that need to be shared

4
00:00:07,06 --> 00:00:11,07
by just sharing one extra thread.

5
00:00:11,07 --> 00:00:15,04
However, if I check support on something like can I use,

6
00:00:15,04 --> 00:00:18,09
I can see that shared workers are not widely supported

7
00:00:18,09 --> 00:00:22,05
by modern browsers, and in fact, support has been removed

8
00:00:22,05 --> 00:00:26,03
in some cases because shared workers were not widely used

9
00:00:26,03 --> 00:00:28,05
and they presented some architectural issues

10
00:00:28,05 --> 00:00:31,08
for browser makers.

11
00:00:31,08 --> 00:00:34,02
A separate, but related technology,

12
00:00:34,02 --> 00:00:36,08
known as a service worker, can perform some

13
00:00:36,08 --> 00:00:40,00
of what a shared worker was originally designed to do.

14
00:00:40,00 --> 00:00:42,04
Like a web worker, a service worker runs

15
00:00:42,04 --> 00:00:45,05
in a separate thread, but instead of simply running code

16
00:00:45,05 --> 00:00:47,07
and sending a response back to the app,

17
00:00:47,07 --> 00:00:51,02
a service worker is optimized to make network requests,

18
00:00:51,02 --> 00:00:54,07
monitor responses, and work with those responses

19
00:00:54,07 --> 00:00:57,01
all in the background.

20
00:00:57,01 --> 00:01:00,07
Another related feature is a WebSocket.

21
00:01:00,07 --> 00:01:03,01
A WebSocket maintains a persistent connection

22
00:01:03,01 --> 00:01:05,06
with a server and listens for updates.

23
00:01:05,06 --> 00:01:07,09
Unlike web workers and service workers,

24
00:01:07,09 --> 00:01:10,07
WebSockets do not run in their own threads.

25
00:01:10,07 --> 00:01:14,00
As a result, they have direct access to the dom.

26
00:01:14,00 --> 00:01:16,01
You can use a WebSocket to replace code

27
00:01:16,01 --> 00:01:19,07
that performs polling, that is setting up an ajax request

28
00:01:19,07 --> 00:01:21,04
to run at a regular interval

29
00:01:21,04 --> 00:01:23,08
to check for changes on the server.

30
00:01:23,08 --> 00:01:26,07
With an open socket, any changes are pushed

31
00:01:26,07 --> 00:01:30,06
and your code responds to them as events.

32
00:01:30,06 --> 00:01:33,01
Like web workers, you can use service workers

33
00:01:33,01 --> 00:01:37,00
and WebSockets to build robust, asynchronous code

34
00:01:37,00 --> 00:01:41,00
that optimized your apps speed and responsiveness.

