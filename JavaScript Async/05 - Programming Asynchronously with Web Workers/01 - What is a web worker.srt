1
00:00:00,05 --> 00:00:02,06
- [Instructor] JavaScript includes a number of features

2
00:00:02,06 --> 00:00:05,03
that enable you to program asynchronously,

3
00:00:05,03 --> 00:00:09,00
including call backs, promises, and asynch await.

4
00:00:09,00 --> 00:00:11,08
But all of these asynch processes still rely

5
00:00:11,08 --> 00:00:14,08
on the main browser thread to execute their code

6
00:00:14,08 --> 00:00:17,00
after any delay has taken place,

7
00:00:17,00 --> 00:00:21,05
such as a set time-out or a response to an ajax request.

8
00:00:21,05 --> 00:00:24,03
The traditional asynchronous model for JavaScript

9
00:00:24,03 --> 00:00:27,08
simply rearranges the order in which code is executed

10
00:00:27,08 --> 00:00:30,04
on a single thread to avoid blocking,

11
00:00:30,04 --> 00:00:33,03
but features that use this model remain limited

12
00:00:33,03 --> 00:00:35,07
by the fact that with a single thread

13
00:00:35,07 --> 00:00:41,02
the browser can do only one thing at a time.

14
00:00:41,02 --> 00:00:43,07
Modern browsers support some newer additions

15
00:00:43,07 --> 00:00:46,07
to JavaScript that make use of additional threads.

16
00:00:46,07 --> 00:00:49,05
One of these newer additions is Web Workers.

17
00:00:49,05 --> 00:00:52,02
A Web Worker enables you to specify code

18
00:00:52,02 --> 00:00:54,09
that will be executed in its own thread

19
00:00:54,09 --> 00:00:57,09
on the processor separate from the browser thread.

20
00:00:57,09 --> 00:01:00,04
This enables you to make your code asynchronous

21
00:01:00,04 --> 00:01:04,00
and also allows your app to do multiple things at once

22
00:01:04,00 --> 00:01:09,08
by leveraging one or more additional processor threads.

23
00:01:09,08 --> 00:01:12,03
With a Web Worker, your app sends a message

24
00:01:12,03 --> 00:01:14,08
to the code running in the additional thread.

25
00:01:14,08 --> 00:01:17,07
This code does the requested work on its thread

26
00:01:17,07 --> 00:01:19,09
then responds with its own message,

27
00:01:19,09 --> 00:01:23,02
which often contains data that's then used in your app.

28
00:01:23,02 --> 00:01:25,06
Web Workers are most useful in an app

29
00:01:25,06 --> 00:01:27,07
that needs to do complex processing,

30
00:01:27,07 --> 00:01:30,06
such as involved mathematical operations.

31
00:01:30,06 --> 00:01:33,00
Sending a task like this to a Web Worker

32
00:01:33,00 --> 00:01:34,07
frees up the browser thread

33
00:01:34,07 --> 00:01:37,00
so your user interface remains responsive.

