1
00:00:00,05 --> 00:00:01,09
- [Instructor] To create a web worker,

2
00:00:01,09 --> 00:00:03,08
you use the worker constructor.

3
00:00:03,08 --> 00:00:05,09
You assign the worker to a variable

4
00:00:05,09 --> 00:00:08,06
so you can reference it in other parts of your code.

5
00:00:08,06 --> 00:00:11,04
The worker constructor requires a single argument,

6
00:00:11,04 --> 00:00:14,01
which is aa reference to a script file containing the code

7
00:00:14,01 --> 00:00:16,02
that should be executed by the worker.

8
00:00:16,02 --> 00:00:17,08
When the statement is executed,

9
00:00:17,08 --> 00:00:20,02
a new thread is created for the code

10
00:00:20,02 --> 00:00:23,00
in the specified file.

11
00:00:23,00 --> 00:00:24,09
I started building out a new feature

12
00:00:24,09 --> 00:00:26,09
for the Explore California site.

13
00:00:26,09 --> 00:00:29,04
Users can pick a photo from a image gallery

14
00:00:29,04 --> 00:00:34,00
and then manipulate it using a few phot filters.

15
00:00:34,00 --> 00:00:36,08
Eventually, I image allowing them to add text

16
00:00:36,08 --> 00:00:39,07
and share the image they've created like a postcard.

17
00:00:39,07 --> 00:00:43,01
But these basic features represent my first milestone.

18
00:00:43,01 --> 00:00:45,03
Each of these filter buttons calls a function

19
00:00:45,03 --> 00:00:48,02
that takes the pixels of the current selected image

20
00:00:48,02 --> 00:00:51,02
and modifies them based on the relevant algorithm.

21
00:00:51,02 --> 00:00:52,08
Image manipulation can require

22
00:00:52,08 --> 00:00:54,07
a significant processing power,

23
00:00:54,07 --> 00:00:57,08
and especially on older computers or mobile devices,

24
00:00:57,08 --> 00:01:00,07
this work might be enough to slow the browser down.

25
00:01:00,07 --> 00:01:03,07
One alternative would be to send the data to the server

26
00:01:03,07 --> 00:01:06,00
and have the manipulation perform there,

27
00:01:06,00 --> 00:01:08,07
but AJAX requests generally take a lot of time

28
00:01:08,07 --> 00:01:10,02
and can be unpredictable,

29
00:01:10,02 --> 00:01:12,06
so the browser is going to make more sense here.

30
00:01:12,06 --> 00:01:15,04
Right now, the app seems pretty responsive.

31
00:01:15,04 --> 00:01:20,00
And if I open up my console and go to the Console,

32
00:01:20,00 --> 00:01:22,06
and I have logging turned on so I can see exactly

33
00:01:22,06 --> 00:01:26,04
how long each filtering step takes,

34
00:01:26,04 --> 00:01:27,09
and it's not very long.

35
00:01:27,09 --> 00:01:29,09
I'm on a pretty recent computer,

36
00:01:29,09 --> 00:01:31,05
and both computer hardware

37
00:01:31,05 --> 00:01:36,03
and modern browsers are optimized for this sort of work.

38
00:01:36,03 --> 00:01:39,08
Now back in my code, I've include a function called delay,

39
00:01:39,08 --> 00:01:43,09
which just incorporates a delay using a while statement

40
00:01:43,09 --> 00:01:46,09
that does nothing to allow me to specify a certain number

41
00:01:46,09 --> 00:01:48,08
of milliseconds I'd like to wait

42
00:01:48,08 --> 00:01:50,05
before the next thing happens.

43
00:01:50,05 --> 00:01:52,04
So, this is essentially allowing me

44
00:01:52,04 --> 00:01:54,03
to create a synchronous delay

45
00:01:54,03 --> 00:01:56,05
and forcing everything to wait.

46
00:01:56,05 --> 00:01:58,05
In essence, I'm creating a block.

47
00:01:58,05 --> 00:02:01,08
So, I want to go ahead and call this delay function

48
00:02:01,08 --> 00:02:05,02
within each of my three algorithm functions.

49
00:02:05,02 --> 00:02:08,09
So, in grayscaleImage, right before that return statement,

50
00:02:08,09 --> 00:02:13,01
I'm going to call delay and send it 1,000,

51
00:02:13,01 --> 00:02:17,02
so that's 1,000 milliseconds or a one second delay.

52
00:02:17,02 --> 00:02:22,07
And likewise for the brightnessImage functions, do a 1,000.

53
00:02:22,07 --> 00:02:26,03
And for thresholdImage, right before that return statement,

54
00:02:26,03 --> 00:02:29,01
do a 1,000.

55
00:02:29,01 --> 00:02:32,04
Now I already have this running using a live server,

56
00:02:32,04 --> 00:02:35,06
so I can switch back to my browser,

57
00:02:35,06 --> 00:02:38,07
and these changes are reflected now.

58
00:02:38,07 --> 00:02:41,07
So if I click Grayscale, it takes a second,

59
00:02:41,07 --> 00:02:43,02
then I see that filter applied,

60
00:02:43,02 --> 00:02:45,04
and I can see that instead of eight milliseconds,

61
00:02:45,04 --> 00:02:48,07
it's 1,008 milliseconds because it includes that delay.

62
00:02:48,07 --> 00:02:52,04
And likewise, I can hit Brightness, Threshold.

63
00:02:52,04 --> 00:02:56,06
Looks like the Brightness is not incorporating the filter.

64
00:02:56,06 --> 00:03:00,04
And that's because I put that delay in

65
00:03:00,04 --> 00:03:01,09
after the return statement.

66
00:03:01,09 --> 00:03:04,04
So, let me save that.

67
00:03:04,04 --> 00:03:06,06
Double-check that Brightness now has delay.

68
00:03:06,06 --> 00:03:07,06
It does.

69
00:03:07,06 --> 00:03:10,03
Same for Threshold and for Grayscale.

70
00:03:10,03 --> 00:03:13,04
So now that I'm seeing this lag in my user interface,

71
00:03:13,04 --> 00:03:16,00
I have delay after I click things.

72
00:03:16,00 --> 00:03:18,04
And so I can actually queue things up a little bit

73
00:03:18,04 --> 00:03:21,01
by clicking a few of these filter buttons in a row,

74
00:03:21,01 --> 00:03:23,08
and notice that the UI doesn't respond.

75
00:03:23,08 --> 00:03:26,01
Also, if I click some images after that,

76
00:03:26,01 --> 00:03:27,09
the filtering all has to happen

77
00:03:27,09 --> 00:03:30,05
before the image changes take place.

78
00:03:30,05 --> 00:03:33,07
So, I want to rewrite this app using web worker.

79
00:03:33,07 --> 00:03:37,02
With this delay, I can still expect the image processing

80
00:03:37,02 --> 00:03:39,08
to be slow, and that simulates and older device

81
00:03:39,08 --> 00:03:41,00
or a mobile device.

82
00:03:41,00 --> 00:03:44,04
But moving that image processing work to a web worker

83
00:03:44,04 --> 00:03:47,05
will free that main thread to respond to the user

84
00:03:47,05 --> 00:03:51,01
for non-processor intensive tasks, like switching images,

85
00:03:51,01 --> 00:03:52,06
or simply updating the UI

86
00:03:52,06 --> 00:03:54,09
to show me which button I just clicked.

87
00:03:54,09 --> 00:03:59,00
I've already created a file called filter-worker.js,

88
00:03:59,00 --> 00:04:03,05
and this contains the functions for each of my filters

89
00:04:03,05 --> 00:04:05,08
as well as the applyFilter function,

90
00:04:05,08 --> 00:04:09,02
so the guts of my photo manipulation work.

91
00:04:09,02 --> 00:04:12,08
And so I'm going to start back in filters.js file

92
00:04:12,08 --> 00:04:15,09
by commenting out those four functions.

93
00:04:15,09 --> 00:04:18,09
And I also can comment out the delay function.

94
00:04:18,09 --> 00:04:22,06
So, I'm going to click above the delay function,

95
00:04:22,06 --> 00:04:24,00
go down passed delay, grayscaleImage,

96
00:04:24,00 --> 00:04:27,07
brightnessImage, thresholdImage, and applyFilter,

97
00:04:27,07 --> 00:04:31,00
I'm going to hold Shift + Click to select all of that code,

98
00:04:31,00 --> 00:04:34,07
and in Visual Studio Code, I can press Command + Slash

99
00:04:34,07 --> 00:04:38,02
to just make a line comment out of each line of code

100
00:04:38,02 --> 00:04:41,05
in that section, and I'll save those changes.

101
00:04:41,05 --> 00:04:43,08
And then I want to create a worker.

102
00:04:43,08 --> 00:04:49,09
So I'm going to scroll the top of my DOMContentLoaded section,

103
00:04:49,09 --> 00:04:52,09
and I'm going to add a new variable.

104
00:04:52,09 --> 00:04:54,09
So below selectedFilter,

105
00:04:54,09 --> 00:04:58,02
I'm going to say const worker

106
00:04:58,02 --> 00:05:03,00
= new, capital W, Worker.

107
00:05:03,00 --> 00:05:06,01
And then the file that I want to reference

108
00:05:06,01 --> 00:05:10,05
is that filter-worker.js file within the _scripts folder.

109
00:05:10,05 --> 00:05:11,03
So that's

110
00:05:11,03 --> 00:05:18,08
_scripts/filter-worker.js.

111
00:05:18,08 --> 00:05:21,07
So by assigning that worker to a variable,

112
00:05:21,07 --> 00:05:23,06
I'll be able to message to it,

113
00:05:23,06 --> 00:05:26,04
and then I'll also be able event listeners

114
00:05:26,04 --> 00:05:29,01
so that my code can work with the response messages

115
00:05:29,01 --> 00:05:31,00
that come from that web worker.

