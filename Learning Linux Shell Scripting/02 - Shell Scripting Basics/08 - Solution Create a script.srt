1
00:00:04,00 --> 00:00:05,50
- [Instructor] So, how'd it go?

2
00:00:05,50 --> 00:00:07,40
Did you complete the challenge?

3
00:00:07,40 --> 00:00:09,00
Here's my solution.

4
00:00:09,00 --> 00:00:11,20
Don't be surprised if it doesn't look like yours.

5
00:00:11,20 --> 00:00:14,20
Two solutions to the same problem rarely do.

6
00:00:14,20 --> 00:00:16,00
So, we're gonna start off in the terminal

7
00:00:16,00 --> 00:00:20,50
and we'll type in touch sport.sh.

8
00:00:20,50 --> 00:00:22,30
That creates the file.

9
00:00:22,30 --> 00:00:26,80
Change mode 755 sport.sh.

10
00:00:26,80 --> 00:00:27,60
That's gonna make it

11
00:00:27,60 --> 00:00:29,30
so we can execute it

12
00:00:29,30 --> 00:00:30,80
and finally I'm gonna edit it

13
00:00:30,80 --> 00:00:35,30
with atom sport.sh.

14
00:00:35,30 --> 00:00:36,80
Now, the first thing we're gonna do

15
00:00:36,80 --> 00:00:39,30
is we're gonna type in our shebang,

16
00:00:39,30 --> 00:00:51,40
so that's a pound sign, exclamation /usr/bin/env bash

17
00:00:51,40 --> 00:00:52,80
and I'm gonna add a comment in here

18
00:00:52,80 --> 00:01:02,00
just so I can remember what was going on.

19
00:01:02,00 --> 00:01:06,40
Then I'm gonna say that my first thing is a name

20
00:01:06,40 --> 00:01:09,70
and it is going to be equal to my parameter one.

21
00:01:09,70 --> 00:01:11,20
Remember, parameter zero

22
00:01:11,20 --> 00:01:12,60
is the name of the script,

23
00:01:12,60 --> 00:01:14,50
whereas parameter one is the first thing

24
00:01:14,50 --> 00:01:17,60
that the user typed after the name.

25
00:01:17,60 --> 00:01:20,00
Then ask what their favorite sport is

26
00:01:20,00 --> 00:01:24,70
and so, we'll just say sport equals parameter two

27
00:01:24,70 --> 00:01:27,00
and then we're gonna print something out,

28
00:01:27,00 --> 00:01:32,80
print the name and sport.

29
00:01:32,80 --> 00:01:36,30
Usually your comments wouldn't be so obvious

30
00:01:36,30 --> 00:01:41,10
and we'll say echo $NAME

31
00:01:41,10 --> 00:01:44,50
likes to watch

32
00:01:44,50 --> 00:01:48,50
and we'll say $SPORT.

33
00:01:48,50 --> 00:01:51,90
Then we'll do a control A to save this.

34
00:01:51,90 --> 00:01:53,00
Go to the terminal

35
00:01:53,00 --> 00:01:58,60
and I will say ./sport.sh

36
00:01:58,60 --> 00:02:01,80
and I'm gonna say my name Troy

37
00:02:01,80 --> 00:02:04,80
and put football.

38
00:02:04,80 --> 00:02:08,40
And we get the sentence Troy likes to watch football.

39
00:02:08,40 --> 00:02:10,00
Hopefully you solved this challenge.

40
00:02:10,00 --> 00:02:11,00
If you had any issues,

41
00:02:11,00 --> 00:02:12,50
please take a look at my solution

42
00:02:12,50 --> 00:02:14,20
and make sure you understand it

43
00:02:14,20 --> 00:02:16,00
before moving on.

