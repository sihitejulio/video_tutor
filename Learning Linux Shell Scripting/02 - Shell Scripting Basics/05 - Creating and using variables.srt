1
00:00:01,10 --> 00:00:02,60
- Simply echoing text directly to the

2
00:00:02,60 --> 00:00:04,60
screen is bound to get boring.

3
00:00:04,60 --> 00:00:06,50
In order to start making dynamic scripts,

4
00:00:06,50 --> 00:00:08,30
we need to understand how to create

5
00:00:08,30 --> 00:00:09,80
and use variables.

6
00:00:09,80 --> 00:00:11,30
Let's return to the terminal,

7
00:00:11,30 --> 00:00:16,30
we're gonna type touch, greeting, dot sh,

8
00:00:16,30 --> 00:00:19,20
we're gonna do change mode,

9
00:00:19,20 --> 00:00:21,60
seven, five, five.

10
00:00:21,60 --> 00:00:23,60
On greeting,

11
00:00:23,60 --> 00:00:24,50
dot, sh.

12
00:00:24,50 --> 00:00:29,50
And finally we're going to open up the atom editor.

13
00:00:29,50 --> 00:00:35,20
Alright we're gonna add our shebang.

14
00:00:35,20 --> 00:00:37,30
And then we'll add a couple of variables.

15
00:00:37,30 --> 00:00:40,50
So we're gonna go first name

16
00:00:40,50 --> 00:00:42,80
equals Bob

17
00:00:42,80 --> 00:00:46,80
and favorite color

18
00:00:46,80 --> 00:00:49,10
equals blue.

19
00:00:49,10 --> 00:00:52,20
And we'll type echo, HI.

20
00:00:52,20 --> 00:00:53,90
And if you want to display the contents

21
00:00:53,90 --> 00:00:56,30
of a variable you need to proceed it with a dollar sign.

22
00:00:56,30 --> 00:00:58,40
So dollar sign.

23
00:00:58,40 --> 00:01:00,70
First name,

24
00:01:00,70 --> 00:01:02,10
comma,

25
00:01:02,10 --> 00:01:04,70
your favorite

26
00:01:04,70 --> 00:01:07,40
color is

27
00:01:07,40 --> 00:01:11,30
and then dollar sign, favorite color.

28
00:01:11,30 --> 00:01:13,80
And then we're gonna save this with a control S,

29
00:01:13,80 --> 00:01:17,30
return to the terminal and we're gonna just

30
00:01:17,30 --> 00:01:22,20
say dots, forward slash, greeting

31
00:01:22,20 --> 00:01:24,20
dot, sh.

32
00:01:24,20 --> 00:01:27,80
We get, Hi Bob, your favorite color is blue.

33
00:01:27,80 --> 00:01:31,80
A variable is created by putting a equal sign

34
00:01:31,80 --> 00:01:34,40
between a name on the left side

35
00:01:34,40 --> 00:01:37,90
and a value on the right side.

36
00:01:37,90 --> 00:01:41,10
Variable consists of letter and numbers and underscores.

37
00:01:41,10 --> 00:01:44,30
It must begin with a letter or an underscore.

38
00:01:44,30 --> 00:01:45,70
Not a number.

39
00:01:45,70 --> 00:01:47,60
It is case sensitive.

40
00:01:47,60 --> 00:01:51,20
It is usually all uppercase, but that's not mandatory,

41
00:01:51,20 --> 00:01:53,50
that's just by tradition.

42
00:01:53,50 --> 00:01:55,90
The value is anything that you can type,

43
00:01:55,90 --> 00:01:59,20
but if it has spaces it should be wrapped in quotes.

44
00:01:59,20 --> 00:02:04,00
Spaces are not allowed on either side of the equal sign.

45
00:02:04,00 --> 00:02:06,60
So if we accidentally put a space,

46
00:02:06,60 --> 00:02:09,00
like between the equal sign and Bob,

47
00:02:09,00 --> 00:02:13,50
save that off, return to the terminal

48
00:02:13,50 --> 00:02:17,10
and try executing our script again.

49
00:02:17,10 --> 00:02:19,20
We get a Bob command not found

50
00:02:19,20 --> 00:02:21,60
and that's because it's interpreting the space

51
00:02:21,60 --> 00:02:25,30
as though it was a value, leaving out the word Bob,

52
00:02:25,30 --> 00:02:28,50
and then interpreting Bob as a command.

53
00:02:28,50 --> 00:02:32,90
First off, let's go back to atom and fix our script.

54
00:02:32,90 --> 00:02:35,40
So we'll remove that space we added right there,

55
00:02:35,40 --> 00:02:38,00
do a control S, to save it.

56
00:02:38,00 --> 00:02:41,90
What if we wanted to pass the first and last name?

57
00:02:41,90 --> 00:02:44,20
We'll tweak our script a bit and see,

58
00:02:44,20 --> 00:02:45,90
so now we're gonna just gonna make it,

59
00:02:45,90 --> 00:02:47,30
so that instead of being the first name,

60
00:02:47,30 --> 00:02:49,50
we're gonna make it so that it's just a name

61
00:02:49,50 --> 00:02:54,30
and we're gonna add Bob, space, Roberts

62
00:02:54,30 --> 00:02:57,60
and change down here from first name,

63
00:02:57,60 --> 00:03:01,90
to just name and we'll save that off.

64
00:03:01,90 --> 00:03:04,50
And we'll go to the terminal

65
00:03:04,50 --> 00:03:07,40
and try executing it again.

66
00:03:07,40 --> 00:03:08,40
And you notice we still end up with,

67
00:03:08,40 --> 00:03:12,30
Hey, Hi, Bob, your favorite color is blue.

68
00:03:12,30 --> 00:03:14,20
So what went on, wrong there ?

69
00:03:14,20 --> 00:03:17,30
well variables can't have white space in them.

70
00:03:17,30 --> 00:03:20,40
If we need them to have a space between the words

71
00:03:20,40 --> 00:03:22,10
we must wrap them in either single

72
00:03:22,10 --> 00:03:23,50
or double quotes.

73
00:03:23,50 --> 00:03:27,70
We could also create two separate variables.

74
00:03:27,70 --> 00:03:33,20
So if we go back to atom one more time.

75
00:03:33,20 --> 00:03:37,90
Here we could just put double quotes, double quotes

76
00:03:37,90 --> 00:03:42,10
let's save it off, return to terminal.

77
00:03:42,10 --> 00:03:44,50
And try running that one more time.

78
00:03:44,50 --> 00:03:46,60
And not we get the, hi Bob Roberts,

79
00:03:46,60 --> 00:03:48,80
your favorite color is blue.

80
00:03:48,80 --> 00:03:50,80
So now we know how to create variables

81
00:03:50,80 --> 00:03:52,00
in our script.

