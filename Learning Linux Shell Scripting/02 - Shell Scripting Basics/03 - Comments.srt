1
00:00:01,00 --> 00:00:02,70
- [Instructor] All scripts start small,

2
00:00:02,70 --> 00:00:05,60
but even small ones can be difficult to understand.

3
00:00:05,60 --> 00:00:07,70
It is a best practice to include comments

4
00:00:07,70 --> 00:00:10,30
in your scripts which explain them.

5
00:00:10,30 --> 00:00:13,00
A comment begins with the pound sign,

6
00:00:13,00 --> 00:00:15,70
and ends at the end of the line.

7
00:00:15,70 --> 00:00:20,10
Let's go back into our script,

8
00:00:20,10 --> 00:00:21,80
Let's add one to our script

9
00:00:21,80 --> 00:00:23,80
just before the first echo line.

10
00:00:23,80 --> 00:00:26,90
So we'll go to the top of the page,

11
00:00:26,90 --> 00:00:29,10
give ourselves a little room,

12
00:00:29,10 --> 00:00:33,20
add a comment,

13
00:00:33,20 --> 00:00:35,20
and even put a little space in there,

14
00:00:35,20 --> 00:00:37,60
and then do a Ctrl + S to save it.

15
00:00:37,60 --> 00:00:39,60
And let's run our script again.

16
00:00:39,60 --> 00:00:41,70
We'll go back to Terminal,

17
00:00:41,70 --> 00:00:44,60
and we will say dot slash

18
00:00:44,60 --> 00:00:47,40
hello.sh.

19
00:00:47,40 --> 00:00:49,80
Our script still functions as it did before.

20
00:00:49,80 --> 00:00:51,40
Comments are a programmer thing

21
00:00:51,40 --> 00:00:53,20
not something for the user.

22
00:00:53,20 --> 00:00:56,00
They aren't displayed like echo.

