1
00:00:00,90 --> 00:00:01,80
- [Instructor] Let's take a look

2
00:00:01,80 --> 00:00:04,60
at our hello.sh script again.

3
00:00:04,60 --> 00:00:06,70
Open it using Atom.

4
00:00:06,70 --> 00:00:12,30
Atom hello.sh.

5
00:00:12,30 --> 00:00:15,20
For fun, let's add another message to it.

6
00:00:15,20 --> 00:00:18,60
Let's add a line below echo Hello, World,

7
00:00:18,60 --> 00:00:20,10
add whatever you'd like.

8
00:00:20,10 --> 00:00:26,70
I'm gonna add echo Mars is red.

9
00:00:26,70 --> 00:00:28,90
Control S to save.

10
00:00:28,90 --> 00:00:32,60
And now I'm gonna return to the terminal

11
00:00:32,60 --> 00:00:38,10
and to run it I'm gonna type bash hello.sh.

12
00:00:38,10 --> 00:00:41,50
The response is predictably unsurprising.

13
00:00:41,50 --> 00:00:43,50
Okay, I know what you're thinking.

14
00:00:43,50 --> 00:00:45,50
Why do I have to type bash

15
00:00:45,50 --> 00:00:47,40
in order to execute my script?

16
00:00:47,40 --> 00:00:48,60
Good question.

17
00:00:48,60 --> 00:00:50,40
The answer is because files

18
00:00:50,40 --> 00:00:53,70
by default don't have the execute permission.

19
00:00:53,70 --> 00:00:54,70
Don't believe me?

20
00:00:54,70 --> 00:00:57,30
Let's try it together.

21
00:00:57,30 --> 00:01:01,60
./hello.sh.

22
00:01:01,60 --> 00:01:03,60
We get an error message.

23
00:01:03,60 --> 00:01:05,10
Permission denied.

24
00:01:05,10 --> 00:01:07,90
If we'd like to make a file executable,

25
00:01:07,90 --> 00:01:10,40
we must use the change mode command,

26
00:01:10,40 --> 00:01:12,50
let's do that now.

27
00:01:12,50 --> 00:01:22,00
We type chmod 755 hello.sh.

28
00:01:22,00 --> 00:01:24,70
This gives everyone the permission to read

29
00:01:24,70 --> 00:01:26,20
and execute the script

30
00:01:26,20 --> 00:01:29,30
but only the owner permission to write it.

31
00:01:29,30 --> 00:01:31,70
Okay, let's try it again.

32
00:01:31,70 --> 00:01:36,40
./hello.sh.

33
00:01:36,40 --> 00:01:38,30
Okay, that worked

34
00:01:38,30 --> 00:01:40,30
but why do we have to type ./

35
00:01:40,30 --> 00:01:41,90
before the name of the script?

36
00:01:41,90 --> 00:01:44,30
Can't we just type the name of the script?

37
00:01:44,30 --> 00:01:46,10
Let's see what happens.

38
00:01:46,10 --> 00:01:49,60
Hello.sh.

39
00:01:49,60 --> 00:01:51,70
Command not found.

40
00:01:51,70 --> 00:01:53,50
Linux systems look for commands

41
00:01:53,50 --> 00:01:56,20
on the path, not in the current directory.

42
00:01:56,20 --> 00:01:58,00
And if the command is not in the path,

43
00:01:58,00 --> 00:02:00,60
they report that it is not found.

44
00:02:00,60 --> 00:02:02,10
When we put the ./

45
00:02:02,10 --> 00:02:04,60
we tell the system don't bother with the path,

46
00:02:04,60 --> 00:02:06,70
here's the location of the command.

47
00:02:06,70 --> 00:02:10,10
Excellent, now we know how to allow our scripts

48
00:02:10,10 --> 00:02:12,00
to run on their own.

