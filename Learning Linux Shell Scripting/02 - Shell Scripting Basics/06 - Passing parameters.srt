1
00:00:01,00 --> 00:00:02,50
- [Instructor] We can get input from users

2
00:00:02,50 --> 00:00:04,50
of our scripts via parameters.

3
00:00:04,50 --> 00:00:06,90
Your script always receives parameters,

4
00:00:06,90 --> 00:00:08,50
even if it doesn't use them.

5
00:00:08,50 --> 00:00:12,60
Bash passes them in via some special symbols.

6
00:00:12,60 --> 00:00:18,00
The first parameter, $0 is special in that it is the path

7
00:00:18,00 --> 00:00:20,40
and name of the executing script.

8
00:00:20,40 --> 00:00:25,10
Generally, we don't use parameters past $9s since the use

9
00:00:25,10 --> 00:00:26,90
of curly braces with parameters

10
00:00:26,90 --> 00:00:30,20
is only supported in newer versions of the Bash shell.

11
00:00:30,20 --> 00:00:33,80
Parameters make it possible to be interactive with the user.

12
00:00:33,80 --> 00:00:35,10
If for example we want it

13
00:00:35,10 --> 00:00:38,80
to echo the user's name, we could do this.

14
00:00:38,80 --> 00:00:40,10
I'm gonna create another script.

15
00:00:40,10 --> 00:00:44,50
So we're gonna say, touch params.sh.

16
00:00:44,50 --> 00:00:53,00
We're gonna do a change mode 755 params.sh.

17
00:00:53,00 --> 00:01:00,50
And finally, atom, params.sh.

18
00:01:00,50 --> 00:01:04,80
And what we'll do is first put our shebang,

19
00:01:04,80 --> 00:01:16,10
and it's going to be usr/bin/env bash.

20
00:01:16,10 --> 00:01:22,90
And then we'll just do an echo Hello $1,

21
00:01:22,90 --> 00:01:25,40
which is our first parameter.

22
00:01:25,40 --> 00:01:28,20
And we'll save that with a Ctrl + S,

23
00:01:28,20 --> 00:01:37,50
go back to the terminal, and do a ./params.sh.

24
00:01:37,50 --> 00:01:39,90
And we get nothing, because I didn't type a name,

25
00:01:39,90 --> 00:01:42,80
but if we do it again and I say,

26
00:01:42,80 --> 00:01:46,70
Joe, this time we get a Hello Joe.

27
00:01:46,70 --> 00:01:50,40
I can do it again, and say Sally.

28
00:01:50,40 --> 00:01:51,90
We get a Hello Sally.

29
00:01:51,90 --> 00:01:54,90
So that way, we can actually get some data from the user

30
00:01:54,90 --> 00:01:57,40
and then echo it back out to the screen.

31
00:01:57,40 --> 00:02:00,80
Now, let's go back into Atom.

32
00:02:00,80 --> 00:02:02,70
It is generally considered a bad practice

33
00:02:02,70 --> 00:02:04,80
to work directly with the parameter,

34
00:02:04,80 --> 00:02:06,90
since their names have no meaning.

35
00:02:06,90 --> 00:02:09,90
It is better to assign the parameter to a variable.

36
00:02:09,90 --> 00:02:13,70
That way, the variable's name can give some meaning to it.

37
00:02:13,70 --> 00:02:16,20
So in this case, what we'll do is we'll say

38
00:02:16,20 --> 00:02:23,40
that USER_NAME equals $1,

39
00:02:23,40 --> 00:02:28,30
and then here we'll print Hello $USER_NAME,

40
00:02:28,30 --> 00:02:30,70
and do a Ctrl + S.

41
00:02:30,70 --> 00:02:33,00
And the result should still be the same.

42
00:02:33,00 --> 00:02:34,80
So if we say param Sally,

43
00:02:34,80 --> 00:02:36,70
we'll still get a Hello Sally,

44
00:02:36,70 --> 00:02:38,50
but now when we look at our code,

45
00:02:38,50 --> 00:02:41,10
it'll make more sense to us.

46
00:02:41,10 --> 00:02:43,90
It is sometimes useful to know when something happened,

47
00:02:43,90 --> 00:02:46,00
not just that it happened.

48
00:02:46,00 --> 00:02:48,00
That is where the date command comes in handy.

49
00:02:48,00 --> 00:02:51,70
So let's go back into Atom.

50
00:02:51,70 --> 00:02:53,20
And after we print the name,

51
00:02:53,20 --> 00:02:54,50
let's actually print the date,

52
00:02:54,50 --> 00:02:59,10
and so we'll do an echo $, anytime you

53
00:02:59,10 --> 00:03:01,30
want to do an actual command,

54
00:03:01,30 --> 00:03:03,50
you can wrap it in the dollar sign the date

55
00:03:03,50 --> 00:03:06,60
so that it does the thing that you want it to do,

56
00:03:06,60 --> 00:03:09,00
not just display the word date.

57
00:03:09,00 --> 00:03:10,10
So in this case, we are

58
00:03:10,10 --> 00:03:12,40
going to get it to display the date for us,

59
00:03:12,40 --> 00:03:17,60
we'll do a Ctrl + S, go back into terminal.

60
00:03:17,60 --> 00:03:24,10
We'll do params, and let's say Judy.

61
00:03:24,10 --> 00:03:25,70
And now we get a Hello Judy

62
00:03:25,70 --> 00:03:28,70
and the date at the time of this recording.

63
00:03:28,70 --> 00:03:30,70
We can also know where

64
00:03:30,70 --> 00:03:33,60
we are in the file system when something happens.

65
00:03:33,60 --> 00:03:35,70
With the pwd command,

66
00:03:35,70 --> 00:03:38,60
which stands for print working directory.

67
00:03:38,60 --> 00:03:42,00
So here we can go back in,

68
00:03:42,00 --> 00:03:44,20
and I'm just gonna echo this out,

69
00:03:44,20 --> 00:03:49,80
and at the bottom here, say echo.

70
00:03:49,80 --> 00:03:51,90
Save it with Ctrl + S,

71
00:03:51,90 --> 00:04:00,50
back to the terminal, and params.sh and we'll say Susan.

72
00:04:00,50 --> 00:04:03,20
And now we get both a Hello Susan,

73
00:04:03,20 --> 00:04:05,20
the date, and the directory

74
00:04:05,20 --> 00:04:08,50
that this script is currently executing in.

75
00:04:08,50 --> 00:04:11,90
And the final thing is if a script executes without error,

76
00:04:11,90 --> 00:04:14,50
it should return a zero to the system.

77
00:04:14,50 --> 00:04:16,40
We currently aren't returning anything

78
00:04:16,40 --> 00:04:18,20
in particular to the system.

79
00:04:18,20 --> 00:04:20,60
We can see what got returned

80
00:04:20,60 --> 00:04:21,90
from the system by just doing

81
00:04:21,90 --> 00:04:26,30
an echo dollar sign, question mark.

82
00:04:26,30 --> 00:04:29,00
And that will show us the last exit code

83
00:04:29,00 --> 00:04:30,80
that the system received.

84
00:04:30,80 --> 00:04:33,60
So in this case, we are actually returning a zero,

85
00:04:33,60 --> 00:04:35,90
but we should make it more purposeful.

86
00:04:35,90 --> 00:04:39,80
And so if we go back into Atom,

87
00:04:39,80 --> 00:04:43,10
as our final line of our script,

88
00:04:43,10 --> 00:04:48,20
we'll say exit 0, that will set the exit code

89
00:04:48,20 --> 00:04:50,80
and zero means success.

90
00:04:50,80 --> 00:04:52,50
Anything other than zero means

91
00:04:52,50 --> 00:04:54,40
that there was some sort of an issue.

92
00:04:54,40 --> 00:04:59,30
The exit codes go from zero to 255.

93
00:04:59,30 --> 00:05:02,00
And let's save that with a Ctrl + S,

94
00:05:02,00 --> 00:05:04,20
go back to the terminal,

95
00:05:04,20 --> 00:05:07,70
and if we run again, and check the exit code,

96
00:05:07,70 --> 00:05:09,60
we should still see a zero.

97
00:05:09,60 --> 00:05:14,10
So I'm doing an echo $?, dollar sign question mark

98
00:05:14,10 --> 00:05:16,20
says what's the last exit code?

99
00:05:16,20 --> 00:05:19,70
If we go into Atom and we change that value,

100
00:05:19,70 --> 00:05:21,80
we make it something different.

101
00:05:21,80 --> 00:05:26,30
So let's set it to exit code 100.

102
00:05:26,30 --> 00:05:27,60
Save it.

103
00:05:27,60 --> 00:05:29,60
Back to the terminal.

104
00:05:29,60 --> 00:05:31,90
Let's give ourselves a little bit of clarity,

105
00:05:31,90 --> 00:05:34,10
and do a clear screen right here,

106
00:05:34,10 --> 00:05:37,50
and try typing that one more time.

107
00:05:37,50 --> 00:05:43,60
So now, we'll see Susan print out, if we say echo $?,

108
00:05:43,60 --> 00:05:45,00
we get 100.

109
00:05:45,00 --> 00:05:48,30
Now, the important thing about the dollar sign question mark

110
00:05:48,30 --> 00:05:52,40
is it returns the last thing returned to the system.

111
00:05:52,40 --> 00:05:55,70
So right now, it's saying 100, because our script just ran.

112
00:05:55,70 --> 00:05:58,30
If we run it again, it will tell

113
00:05:58,30 --> 00:06:01,30
you what the echo command returned.

114
00:06:01,30 --> 00:06:03,30
So now it's a zero.

115
00:06:03,30 --> 00:06:04,70
And it'll stay zero until

116
00:06:04,70 --> 00:06:06,40
something else happens in the system.

117
00:06:06,40 --> 00:06:08,30
So if you ever need that value,

118
00:06:08,30 --> 00:06:11,50
you should put it in a variable in your script.

119
00:06:11,50 --> 00:06:14,40
And just remember that a value of zero means success,

120
00:06:14,40 --> 00:06:18,00
and anything else usually indicates an error.

