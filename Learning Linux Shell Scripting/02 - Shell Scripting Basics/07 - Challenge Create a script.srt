1
00:00:00,00 --> 00:00:04,00
(whooshing)

2
00:00:04,00 --> 00:00:05,10
- [Instructor] Using a text editor,

3
00:00:05,10 --> 00:00:08,30
create a script named sport.sh.

4
00:00:08,30 --> 00:00:10,70
Make it so it is executable

5
00:00:10,70 --> 00:00:15,30
and accepts two parameters, a name and a favorite sport.

6
00:00:15,30 --> 00:00:17,80
Then, display any sentence you like

7
00:00:17,80 --> 00:00:21,20
to the console using those two parameters.

8
00:00:21,20 --> 00:00:23,20
Give yourselves five minutes or so

9
00:00:23,20 --> 00:00:25,00
to complete this challenge.

