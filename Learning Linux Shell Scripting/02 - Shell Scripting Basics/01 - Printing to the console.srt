1
00:00:01,00 --> 00:00:02,00
- [Instructor] The first tool that every

2
00:00:02,00 --> 00:00:04,70
shell script programmer needs is a text editor.

3
00:00:04,70 --> 00:00:07,20
This is not the same thing as a word processor.

4
00:00:07,20 --> 00:00:08,90
In fact, you should never use a

5
00:00:08,90 --> 00:00:11,00
word processor to edit a script.

6
00:00:11,00 --> 00:00:13,20
Vim is a free, and open source editor

7
00:00:13,20 --> 00:00:16,00
that has been around since the early 1990s.

8
00:00:16,00 --> 00:00:19,30
It is by and far the most popular editor for scripting.

9
00:00:19,30 --> 00:00:23,20
However, there is no requirement that you use it too.

10
00:00:23,20 --> 00:00:25,60
I am going to use Atom for this course.

11
00:00:25,60 --> 00:00:28,80
It is an open source editor from the folks at GitHub.

12
00:00:28,80 --> 00:00:30,60
It works well from a GUI,

13
00:00:30,60 --> 00:00:33,00
and is easily invoked from the command line.

14
00:00:33,00 --> 00:00:34,70
If you'd like to use it too

15
00:00:34,70 --> 00:00:38,20
it is available for free at atom.io.

16
00:00:38,20 --> 00:00:41,80
You are, of course, welcome to use any editor you'd like.

17
00:00:41,80 --> 00:00:43,80
Traditionally the first program created

18
00:00:43,80 --> 00:00:46,30
in a new language is Hello World.

19
00:00:46,30 --> 00:00:47,50
So let's honor that tradition,

20
00:00:47,50 --> 00:00:49,20
and make one ourselves.

21
00:00:49,20 --> 00:00:50,30
The first thing you need to know

22
00:00:50,30 --> 00:00:54,10
is the command which prints to the display, echo.

23
00:00:54,10 --> 00:00:56,30
I've already installed Atom in my machine,

24
00:00:56,30 --> 00:00:57,80
and I'm in the Terminal.

25
00:00:57,80 --> 00:01:01,40
Let's name our script hello.sh.

26
00:01:01,40 --> 00:01:03,80
So we'll type touch

27
00:01:03,80 --> 00:01:07,20
hello.sh.

28
00:01:07,20 --> 00:01:08,60
What did we just do?

29
00:01:08,60 --> 00:01:12,40
The touch command updates a file's timestamp if it exists,

30
00:01:12,40 --> 00:01:15,10
and if the file doesn't exist creates it.

31
00:01:15,10 --> 00:01:18,70
So we created an empty file named sh.

32
00:01:18,70 --> 00:01:21,90
There is no requirement that scripts have an extension,

33
00:01:21,90 --> 00:01:25,20
but I like to make the purpose of things painfully obvious.

34
00:01:25,20 --> 00:01:28,20
So I use the extension sh.

35
00:01:28,20 --> 00:01:30,00
The next command will launch Atom,

36
00:01:30,00 --> 00:01:33,50
and open it with our hello script ready to be edited.

37
00:01:33,50 --> 00:01:39,20
So we type atom hello.sh.

38
00:01:39,20 --> 00:01:41,60
The echo command prints to the display.

39
00:01:41,60 --> 00:01:44,90
Let's add our hello message.

40
00:01:44,90 --> 00:01:46,20
Echo

41
00:01:46,20 --> 00:01:47,40
hello

42
00:01:47,40 --> 00:01:48,30
comma

43
00:01:48,30 --> 00:01:50,20
world.

44
00:01:50,20 --> 00:01:53,80
You can save the file either with File,

45
00:01:53,80 --> 00:01:56,80
and then Save.

46
00:01:56,80 --> 00:02:01,00
Or, quickly, by using the keyboard shortcut Ctrl + S.

47
00:02:01,00 --> 00:02:03,90
Let's return to the Terminal.

48
00:02:03,90 --> 00:02:10,90
To execute our script we're gonna type bash hello.sh.

49
00:02:10,90 --> 00:02:13,10
Bash is a shell, and command language.

50
00:02:13,10 --> 00:02:16,30
It opens our script, and invokes the commands in it.

51
00:02:16,30 --> 00:02:19,50
We see the familiar greeting hello, world.

52
00:02:19,50 --> 00:02:20,50
Nice.

53
00:02:20,50 --> 00:02:22,20
We've created our first script,

54
00:02:22,20 --> 00:02:24,00
and got it to print to the screen.

