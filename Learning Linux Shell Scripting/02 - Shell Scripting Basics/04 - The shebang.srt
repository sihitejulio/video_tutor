1
00:00:01,00 --> 00:00:03,40
- [Instructor] Let's open hello.sh again.

2
00:00:03,40 --> 00:00:05,50
It has a problem which we should take care of

3
00:00:05,50 --> 00:00:07,00
before we forget.

4
00:00:07,00 --> 00:00:11,00
Our script code is written specifically for the Bash shell.

5
00:00:11,00 --> 00:00:13,00
While Bash is incredibly popular,

6
00:00:13,00 --> 00:00:14,70
it is not the only shell around.

7
00:00:14,70 --> 00:00:18,20
If someone tried to run it on one of those other shells,

8
00:00:18,20 --> 00:00:19,80
the results are unpredictable

9
00:00:19,80 --> 00:00:21,10
and that's not good.

10
00:00:21,10 --> 00:00:22,70
We want our scripts to run

11
00:00:22,70 --> 00:00:24,30
as we designed them.

12
00:00:24,30 --> 00:00:26,30
We need to make sure that our script

13
00:00:26,30 --> 00:00:27,80
only runs on Bash.

14
00:00:27,80 --> 00:00:31,40
Luckily there's an easy way to do that.

15
00:00:31,40 --> 00:00:33,00
So, we go to the top of the page,

16
00:00:33,00 --> 00:00:35,00
we insert a new line.

17
00:00:35,00 --> 00:00:38,60
Every shell script should be begin with the interpreter line

18
00:00:38,60 --> 00:00:41,40
colorfully known as the shebang.

19
00:00:41,40 --> 00:00:42,70
This line tells the system

20
00:00:42,70 --> 00:00:46,10
which command processor should handle this script.

21
00:00:46,10 --> 00:00:56,00
For Bash, it is #!/usr/bin

22
00:00:56,00 --> 00:01:01,90
and then /env space bash.

23
00:01:01,90 --> 00:01:06,10
The shebang must be the first line of the file

24
00:01:06,10 --> 00:01:09,30
or it will simply be ignored like a comment.

25
00:01:09,30 --> 00:01:11,00
You may also see

26
00:01:11,00 --> 00:01:14,90
the Basher bang written as

27
00:01:14,90 --> 00:01:22,20
#!/bin/bash.

28
00:01:22,20 --> 00:01:23,40
While both will work,

29
00:01:23,40 --> 00:01:25,30
the form is a bit more portable

30
00:01:25,30 --> 00:01:28,80
since it will search for Bash on the path.

31
00:01:28,80 --> 00:01:32,30
Let's go ahead and delete this one.

32
00:01:32,30 --> 00:01:35,40
Now imagine that we had written our script in Python.

33
00:01:35,40 --> 00:01:38,40
Here's the shebang for that.

34
00:01:38,40 --> 00:01:40,30
It would just be Python.

35
00:01:40,30 --> 00:01:43,10
If we had written it in JavaScript,

36
00:01:43,10 --> 00:01:45,60
it would just be node.

37
00:01:45,60 --> 00:01:46,90
In order to run JavaScript,

38
00:01:46,90 --> 00:01:48,30
you must have node installed

39
00:01:48,30 --> 00:01:50,90
and likewise you must have Python installed

40
00:01:50,90 --> 00:01:53,30
to run scripts written in it.

41
00:01:53,30 --> 00:01:55,60
What happens when someone tries to run our script

42
00:01:55,60 --> 00:01:57,10
on the wrong system?

43
00:01:57,10 --> 00:01:59,90
Let's pretend we wrote our script in Bailey.

44
00:01:59,90 --> 00:02:01,10
Now, to the best of my knowledge,

45
00:02:01,10 --> 00:02:02,40
there's no such thing

46
00:02:02,40 --> 00:02:06,30
but here's what would happen.

47
00:02:06,30 --> 00:02:08,50
So, we go ahead and add Bailey,

48
00:02:08,50 --> 00:02:10,50
save this with a control S,

49
00:02:10,50 --> 00:02:13,50
go back down to the terminal

50
00:02:13,50 --> 00:02:19,80
and type in ./hello.sh

51
00:02:19,80 --> 00:02:23,10
and we get the error no such file or directory.

52
00:02:23,10 --> 00:02:25,20
So, let's go ahead and return

53
00:02:25,20 --> 00:02:30,60
our script back to Bash.

54
00:02:30,60 --> 00:02:32,40
And save it.

55
00:02:32,40 --> 00:02:33,90
The tricky thing about the shebang

56
00:02:33,90 --> 00:02:36,00
is that you may never see the ill effects

57
00:02:36,00 --> 00:02:38,70
from not using it since the default shell

58
00:02:38,70 --> 00:02:41,10
in most Linux systems is Bash.

59
00:02:41,10 --> 00:02:44,30
Of course, if you write scripts in JavaScript or Python

60
00:02:44,30 --> 00:02:46,00
the shebang is mandatory.

