1
00:00:01,00 --> 00:00:02,10
- [Troy] Thanks for watching this course

2
00:00:02,10 --> 00:00:03,60
on Linux shell scripting.

3
00:00:03,60 --> 00:00:05,40
I hope you have enjoyed the information

4
00:00:05,40 --> 00:00:07,70
and will find some creative and innovative ways

5
00:00:07,70 --> 00:00:09,40
to put it to good use.

6
00:00:09,40 --> 00:00:12,00
There are a wide variety of courses covering Linux

7
00:00:12,00 --> 00:00:13,80
found here in our library,

8
00:00:13,80 --> 00:00:16,70
including my favorite and always educational,

9
00:00:16,70 --> 00:00:18,40
Linux Tips Weekly.

10
00:00:18,40 --> 00:00:21,00
Take care and keep on learning.

