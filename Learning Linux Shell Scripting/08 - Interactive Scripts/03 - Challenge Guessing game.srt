1
00:00:04,00 --> 00:00:05,40
- [Narrator] Let's create a simple version

2
00:00:05,40 --> 00:00:06,70
of a guessing game.

3
00:00:06,70 --> 00:00:09,10
Create a script, guess.sh.

4
00:00:09,10 --> 00:00:11,30
In it, set a global variable named COMPUTER

5
00:00:11,30 --> 00:00:13,40
to any number one to 50.

6
00:00:13,40 --> 00:00:16,60
Use the READ command to prompt the user for input.

7
00:00:16,60 --> 00:00:18,90
If their value matches the COMPUTER,

8
00:00:18,90 --> 00:00:21,10
let them know that they won and exit.

9
00:00:21,10 --> 00:00:24,00
Else, let them know if they're too high or too low

10
00:00:24,00 --> 00:00:26,00
compared to the COMPUTER's number.

