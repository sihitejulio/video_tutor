1
00:00:01,00 --> 00:00:02,20
- [Instructor] Your program should be prepared

2
00:00:02,20 --> 00:00:03,60
to handle bad data.

3
00:00:03,60 --> 00:00:06,60
Let's see a few ways to validate user input.

4
00:00:06,60 --> 00:00:09,30
Let's assume that we need two parameters from the user,

5
00:00:09,30 --> 00:00:13,40
their name, a string, and their age, an integer.

6
00:00:13,40 --> 00:00:17,70
So, we'll start off by saying touch user.sh,

7
00:00:17,70 --> 00:00:19,00
chmod

8
00:00:19,00 --> 00:00:20,40
755

9
00:00:20,40 --> 00:00:22,50
user.sh,

10
00:00:22,50 --> 00:00:24,00
and atom

11
00:00:24,00 --> 00:00:28,30
user.sh.

12
00:00:28,30 --> 00:00:31,60
Put the shebang in,

13
00:00:31,60 --> 00:00:35,80
usr/bin/env

14
00:00:35,80 --> 00:00:37,20
bash.

15
00:00:37,20 --> 00:00:39,60
And we'll begin with by having a global variable

16
00:00:39,60 --> 00:00:42,80
that says whether or not we consider the input valid,

17
00:00:42,80 --> 00:00:45,20
so we're gonna say VALID=0,

18
00:00:45,20 --> 00:00:47,70
meaning that it is not validated yet.

19
00:00:47,70 --> 00:00:53,70
Then we have a while, square brackets, space, $VALID,

20
00:00:53,70 --> 00:00:57,30
- eq, for equals, 0.

21
00:00:57,30 --> 00:01:00,20
So, while the VALID equals 0,

22
00:01:00,20 --> 00:01:02,30
we're gonna continue in this loop.

23
00:01:02,30 --> 00:01:06,20
We're gonna do a do, and a done,

24
00:01:06,20 --> 00:01:11,40
and then in the middle, we'll say read -p,

25
00:01:11,40 --> 00:01:13,00
and then we'll give the user a prompt.

26
00:01:13,00 --> 00:01:17,80
Remember, the -p says, put the prompt on the same line

27
00:01:17,80 --> 00:01:20,50
as the user input, put them both on the same line.

28
00:01:20,50 --> 00:01:21,30
So, we'll say,

29
00:01:21,30 --> 00:01:24,00
please enter

30
00:01:24,00 --> 00:01:25,60
your name

31
00:01:25,60 --> 00:01:27,10
and age,

32
00:01:27,10 --> 00:01:29,10
colon, space.

33
00:01:29,10 --> 00:01:31,60
Then we're gonna get our two input variables,

34
00:01:31,60 --> 00:01:35,80
the first one is NAME, the second one is AGE.

35
00:01:35,80 --> 00:01:38,30
Then we're gonna have our first if.

36
00:01:38,30 --> 00:01:41,60
So, in this first if, what we're gonna do is just make sure

37
00:01:41,60 --> 00:01:45,40
that we got some data in both of the parameters

38
00:01:45,40 --> 00:01:47,20
that we're expecting to have data in,

39
00:01:47,20 --> 00:01:50,20
so both NAME and AGE should have some values in there.

40
00:01:50,20 --> 00:01:55,90
So, double square brackets, space, parentheses, space,

41
00:01:55,90 --> 00:01:56,90
- z.

42
00:01:56,90 --> 00:02:02,10
Remember, -z is checking to see if the variable is empty.

43
00:02:02,10 --> 00:02:05,90
So, NAME, then outside of the parenthese,

44
00:02:05,90 --> 00:02:09,70
to give us a space, the pipe symbol, which is,

45
00:02:09,70 --> 00:02:12,10
two pipe symbols together is an or,

46
00:02:12,10 --> 00:02:14,20
so if either one of these are true,

47
00:02:14,20 --> 00:02:15,80
we didn't get enough parameters,

48
00:02:15,80 --> 00:02:18,20
and so right inside of the second parentheses,

49
00:02:18,20 --> 00:02:20,50
we're gonna do a -z,

50
00:02:20,50 --> 00:02:22,60
$AGE,

51
00:02:22,60 --> 00:02:25,30
and put a space right there.

52
00:02:25,30 --> 00:02:29,80
Then we put a then, and then we're gonna echo,

53
00:02:29,80 --> 00:02:31,30
give the user some kind of message

54
00:02:31,30 --> 00:02:32,60
to let them know what's wrong,

55
00:02:32,60 --> 00:02:34,70
Not enough

56
00:02:34,70 --> 00:02:37,20
parameters

57
00:02:37,20 --> 00:02:39,00
passed.

58
00:02:39,00 --> 00:02:41,70
And then we'll use our old friend, the continue,

59
00:02:41,70 --> 00:02:44,10
to go back to the top of the loop.

60
00:02:44,10 --> 00:02:47,20
Otherwise, we're gonna do an elif,

61
00:02:47,20 --> 00:02:50,90
and this time we're gonna have double square brackets,

62
00:02:50,90 --> 00:02:54,20
the not symbol, so we're gonna reverse whatever the logic is

63
00:02:54,20 --> 00:02:55,90
of this test.

64
00:02:55,90 --> 00:02:57,90
$NAME,

65
00:02:57,90 --> 00:03:00,20
space, equal sign,

66
00:03:00,20 --> 00:03:03,60
then next to the equal sign, touching it, is the tilde,

67
00:03:03,60 --> 00:03:07,20
and that is the character above the backtick

68
00:03:07,20 --> 00:03:08,90
in most keyboards.

69
00:03:08,90 --> 00:03:10,70
And we're gonna put a space,

70
00:03:10,70 --> 00:03:15,90
then we're gonna have a carat, square brackets,

71
00:03:15,90 --> 00:03:17,50
on the outside of the square brackets

72
00:03:17,50 --> 00:03:22,30
a plus and a dollar sign, and then a space.

73
00:03:22,30 --> 00:03:23,60
Now, what we're doing here,

74
00:03:23,60 --> 00:03:25,70
is we're gonna do a regular expression,

75
00:03:25,70 --> 00:03:28,30
and the regular expression we're gonna check for,

76
00:03:28,30 --> 00:03:30,80
is we want whatever characters are in the name,

77
00:03:30,80 --> 00:03:32,70
they must be alphabetic,

78
00:03:32,70 --> 00:03:36,50
so it's gotta be A through Z, uppercase or lowercase.

79
00:03:36,50 --> 00:03:37,60
So, A

80
00:03:37,60 --> 00:03:39,50
dash Z,

81
00:03:39,50 --> 00:03:41,50
that handles our uppercase,

82
00:03:41,50 --> 00:03:46,30
and then a dash z, and there's our lowercase.

83
00:03:46,30 --> 00:03:48,00
And we're gonna say, then.

84
00:03:48,00 --> 00:03:52,60
So, if this test fails, we're gonna say, echo,

85
00:03:52,60 --> 00:03:56,90
non alpha characters

86
00:03:56,90 --> 00:03:58,60
detected.

87
00:03:58,60 --> 00:04:01,20
And we'll give the user some sort of hint of what's wrong,

88
00:04:01,20 --> 00:04:05,30
by putting the name in the string as well.

89
00:04:05,30 --> 00:04:09,20
And again, if it fails, we're gonna do a continue,

90
00:04:09,20 --> 00:04:11,80
and go back to the top of the loop.

91
00:04:11,80 --> 00:04:13,80
Then we're gonna do one more else if,

92
00:04:13,80 --> 00:04:15,70
so here's our elif,

93
00:04:15,70 --> 00:04:18,90
again with the double sets of square brackets,

94
00:04:18,90 --> 00:04:21,60
and where gonna do a not symbol, $AGE.

95
00:04:21,60 --> 00:04:23,20
This time we're gonna check,

96
00:04:23,20 --> 00:04:24,80
using a regular expression again,

97
00:04:24,80 --> 00:04:28,80
to make sure the name is all number characters.

98
00:04:28,80 --> 00:04:31,90
So, we're gonna say, equals tilde,

99
00:04:31,90 --> 00:04:33,50
space,

100
00:04:33,50 --> 00:04:34,50
carat.

101
00:04:34,50 --> 00:04:37,60
Now, the carat says, start from the beginning of the string,

102
00:04:37,60 --> 00:04:40,40
then we're gonna have the open and close square brackets,

103
00:04:40,40 --> 00:04:43,60
the plus sign, and the dollar sign, and a space.

104
00:04:43,60 --> 00:04:44,70
The plus sign means that,

105
00:04:44,70 --> 00:04:46,80
whatever's inside of these square brackets,

106
00:04:46,80 --> 00:04:49,70
there must be at least one of them, and there can be more,

107
00:04:49,70 --> 00:04:51,60
and the dollar sign means that

108
00:04:51,60 --> 00:04:54,00
this string must end here as well.

109
00:04:54,00 --> 00:04:57,80
So, here we're gonna just say 0 dash 9,

110
00:04:57,80 --> 00:05:00,80
and that says that all the characters in this string

111
00:05:00,80 --> 00:05:02,80
must be digits.

112
00:05:02,80 --> 00:05:04,80
And then we're gonna do a then,

113
00:05:04,80 --> 00:05:07,50
and if this fails, we're gonna echo

114
00:05:07,50 --> 00:05:11,30
non digit characters

115
00:05:11,30 --> 00:05:13,00
detected,

116
00:05:13,00 --> 00:05:17,30
and echo back the character to the user, $AGE.

117
00:05:17,30 --> 00:05:21,00
And again, we'll continue, if the test passes here,

118
00:05:21,00 --> 00:05:23,40
meaning that the validation has failed.

119
00:05:23,40 --> 00:05:25,50
And then we're gonna do a fi.

120
00:05:25,50 --> 00:05:26,60
Before we exit the loop,

121
00:05:26,60 --> 00:05:28,60
since we've passed all the validation steps,

122
00:05:28,60 --> 00:05:31,50
we're gonna say that VALID now equals 1.

123
00:05:31,50 --> 00:05:35,20
And then we'll echo back the input that the user gave us,

124
00:05:35,20 --> 00:05:37,00
so Name

125
00:05:37,00 --> 00:05:39,20
equals $NAME,

126
00:05:39,20 --> 00:05:40,20
and

127
00:05:40,20 --> 00:05:41,50
Age

128
00:05:41,50 --> 00:05:42,70
equals

129
00:05:42,70 --> 00:05:45,20
$AGE",

130
00:05:45,20 --> 00:05:48,00
and do an exit 0.

131
00:05:48,00 --> 00:05:50,20
And then we do a Control + S to save it,

132
00:05:50,20 --> 00:05:53,00
and we return to the terminal,

133
00:05:53,00 --> 00:05:56,20
./user.sh.

134
00:05:56,20 --> 00:05:58,00
It'll ask us for our name and age,

135
00:05:58,00 --> 00:06:02,10
so let's just enter one, so I'll say Troy.

136
00:06:02,10 --> 00:06:04,80
It lets us know that not enough parameters were passed.

137
00:06:04,80 --> 00:06:06,50
So, if I say,

138
00:06:06,50 --> 00:06:07,70
Troy

139
00:06:07,70 --> 00:06:11,30
and Troy, so this time I'm giving it two strings,

140
00:06:11,30 --> 00:06:12,80
not a string and an integer,

141
00:06:12,80 --> 00:06:17,60
it says, hey, non digit characters detected, and Troy.

142
00:06:17,60 --> 00:06:21,70
And so, if we say, Troy and, I don't know, what am I?

143
00:06:21,70 --> 00:06:24,30
Right now I feel about 99.

144
00:06:24,30 --> 00:06:27,50
And it tells us, hey, we passed all the validations,

145
00:06:27,50 --> 00:06:29,90
so, name is Troy, age is 99.

146
00:06:29,90 --> 00:06:33,20
And that's some of the ways that you can validate user input

147
00:06:33,20 --> 00:06:35,00
in the Bash shell.

