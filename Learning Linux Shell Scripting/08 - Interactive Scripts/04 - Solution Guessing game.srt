1
00:00:04,00 --> 00:00:06,30
- [Instructor] So, how did you do at this challenge?

2
00:00:06,30 --> 00:00:08,00
Let me show you my solution.

3
00:00:08,00 --> 00:00:14,60
From the terminal I'm gonna do touch guess.sh,

4
00:00:14,60 --> 00:00:21,20
change mode 755 guess.sh

5
00:00:21,20 --> 00:00:26,70
and atom guess.sh.

6
00:00:26,70 --> 00:00:29,50
Then we do our shebang.

7
00:00:29,50 --> 00:00:34,90
Usr/bin/env bash.

8
00:00:34,90 --> 00:00:37,50
Now, we're gonna create a global variable computer

9
00:00:37,50 --> 00:00:39,50
which is gonna hold the computer's guess.

10
00:00:39,50 --> 00:00:41,20
Normally we would put a random in here.

11
00:00:41,20 --> 00:00:42,60
There is a random

12
00:00:42,60 --> 00:00:45,00
in the Bash shell

13
00:00:45,00 --> 00:00:47,10
but that's a problem for another day

14
00:00:47,10 --> 00:00:50,50
and we'll just that it's equal to 50

15
00:00:50,50 --> 00:00:53,30
and we're gonna create one more global variable

16
00:00:53,30 --> 00:00:55,40
and we're going to call it playing

17
00:00:55,40 --> 00:00:57,60
and set it to zero.

18
00:00:57,60 --> 00:00:59,90
Then we're gonna do a while loop

19
00:00:59,90 --> 00:01:01,40
and in the while loop we're gonna do

20
00:01:01,40 --> 00:01:07,00
square brackets braces $PLAYING

21
00:01:07,00 --> 00:01:12,00
and we're gonna say -eq for equals, zero,

22
00:01:12,00 --> 00:01:15,10
so while playing is equal to zero,

23
00:01:15,10 --> 00:01:17,90
we're gonna continue in this loop.

24
00:01:17,90 --> 00:01:21,50
Then we're gonna have a do and a done

25
00:01:21,50 --> 00:01:23,10
and in the middle.

26
00:01:23,10 --> 00:01:24,00
First thing we're gonna do

27
00:01:24,00 --> 00:01:26,50
is we're gonna read some input from the user.

28
00:01:26,50 --> 00:01:30,50
So, we're gonna say read -p

29
00:01:30,50 --> 00:01:33,10
and then prompt the user

30
00:01:33,10 --> 00:01:39,80
what's your guess colon with a space

31
00:01:39,80 --> 00:01:42,30
and then a space here and then say input.

32
00:01:42,30 --> 00:01:45,40
That's gonna be the name of our input variable.

33
00:01:45,40 --> 00:01:51,20
Then we're gonna say if square brackets $INPUT

34
00:01:51,20 --> 00:01:55,80
- eq $COMPUTER,

35
00:01:55,80 --> 00:02:02,50
so if the user's input equals the computer's guess,

36
00:02:02,50 --> 00:02:09,30
then we're gonna echo you've won,

37
00:02:09,30 --> 00:02:15,00
the number was $COMPUTER.

38
00:02:15,00 --> 00:02:19,10
And then we exit with a zero.

39
00:02:19,10 --> 00:02:21,10
If they didn't make the right guess,

40
00:02:21,10 --> 00:02:22,10
the first thing we're gonna do

41
00:02:22,10 --> 00:02:24,70
is we're gonna say elif

42
00:02:24,70 --> 00:02:27,20
and then square brackets again,

43
00:02:27,20 --> 00:02:35,00
$INPUT -lt for less than $COMPUTER

44
00:02:35,00 --> 00:02:37,10
and have another then.

45
00:02:37,10 --> 00:02:42,70
This is gonna be echo you're too low

46
00:02:42,70 --> 00:02:47,10
because their number is less than the computer's number.

47
00:02:47,10 --> 00:02:49,80
And finally, we're gonna have an else,

48
00:02:49,80 --> 00:02:57,00
and we're gonna echo you're too high.

49
00:02:57,00 --> 00:03:00,30
Then a feed and this if statement.

50
00:03:00,30 --> 00:03:02,00
Then we're gonna have a done.

51
00:03:02,00 --> 00:03:03,30
And then finally after the done

52
00:03:03,30 --> 00:03:06,00
we'll have an exit 0.

53
00:03:06,00 --> 00:03:09,60
So, let's check out our simple version

54
00:03:09,60 --> 00:03:11,10
of a guessing game.

55
00:03:11,10 --> 00:03:13,20
Do a control S to save this.

56
00:03:13,20 --> 00:03:15,90
Go to the terminal.

57
00:03:15,90 --> 00:03:19,60
./guess.sh.

58
00:03:19,60 --> 00:03:21,60
And it'll ask us what our guess is,

59
00:03:21,60 --> 00:03:24,10
so we'll start off with 20.

60
00:03:24,10 --> 00:03:25,80
Tells us we're too low.

61
00:03:25,80 --> 00:03:29,00
Start off, say 70.

62
00:03:29,00 --> 00:03:30,70
Now we're too high.

63
00:03:30,70 --> 00:03:33,00
So, we'll say 40.

64
00:03:33,00 --> 00:03:34,40
We're still too low.

65
00:03:34,40 --> 00:03:37,20
55, too high

66
00:03:37,20 --> 00:03:39,30
and then we'll say 50.

67
00:03:39,30 --> 00:03:42,10
And you've won, the number was 50

68
00:03:42,10 --> 00:03:43,90
and that is how we use what we know

69
00:03:43,90 --> 00:03:48,00
to create a simple guessing game using Bash.

