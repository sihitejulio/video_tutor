1
00:00:01,00 --> 00:00:02,70
- [Instructor] In order to make our script interactive,

2
00:00:02,70 --> 00:00:05,80
we need to be able to get input from the user.

3
00:00:05,80 --> 00:00:09,30
From the terminal we're gonna do a touch,

4
00:00:09,30 --> 00:00:12,80
prompt.sh,

5
00:00:12,80 --> 00:00:13,80
change mode

6
00:00:13,80 --> 00:00:15,70
755

7
00:00:15,70 --> 00:00:18,10
prompt.sh,

8
00:00:18,10 --> 00:00:19,90
and atom

9
00:00:19,90 --> 00:00:22,40
prompt.sh.

10
00:00:22,40 --> 00:00:24,90
Put in our shebang.

11
00:00:24,90 --> 00:00:26,70
Bin forward slash

12
00:00:26,70 --> 00:00:27,90
env space

13
00:00:27,90 --> 00:00:30,10
bash,

14
00:00:30,10 --> 00:00:33,30
and then we're gonna begin with the read command.

15
00:00:33,30 --> 00:00:37,00
And we're gonna say read dash p,

16
00:00:37,00 --> 00:00:41,10
and this next part is the prompt message the user will see.

17
00:00:41,10 --> 00:00:42,20
What

18
00:00:42,20 --> 00:00:43,30
is your

19
00:00:43,30 --> 00:00:44,90
first

20
00:00:44,90 --> 00:00:46,00
name

21
00:00:46,00 --> 00:00:47,70
colon space,

22
00:00:47,70 --> 00:00:50,90
and then all caps name.

23
00:00:50,90 --> 00:00:53,80
The dash p prompts the output string

24
00:00:53,80 --> 00:00:56,00
without adding a new line character.

25
00:00:56,00 --> 00:01:00,20
This keeps the user's input on the same line as the prompt.

26
00:01:00,20 --> 00:01:02,50
Name is gonna be the variable that

27
00:01:02,50 --> 00:01:06,10
the string that the user types' gonna go into

28
00:01:06,10 --> 00:01:07,80
so then we're gonna say

29
00:01:07,80 --> 00:01:09,70
echo

30
00:01:09,70 --> 00:01:10,60
your

31
00:01:10,60 --> 00:01:12,70
name is,

32
00:01:12,70 --> 00:01:15,10
and let put colon right there,

33
00:01:15,10 --> 00:01:18,20
dollar sign name,

34
00:01:18,20 --> 00:01:20,30
and just an exit zero.

35
00:01:20,30 --> 00:01:24,60
Save that off, go to the Terminal.

36
00:01:24,60 --> 00:01:29,10
Dot forward slash prompt.sh,

37
00:01:29,10 --> 00:01:30,90
and it'll ask us what is our name?

38
00:01:30,90 --> 00:01:33,10
I'll say Troy,

39
00:01:33,10 --> 00:01:36,00
and it'll respond your name is Troy.

40
00:01:36,00 --> 00:01:39,00
We used the read command to get input from the user.

