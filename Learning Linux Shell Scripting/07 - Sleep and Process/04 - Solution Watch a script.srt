1
00:00:04,00 --> 00:00:06,10
- [Instructor] How did you do with the challenge?

2
00:00:06,10 --> 00:00:08,70
The solution is to start the delay script

3
00:00:08,70 --> 00:00:11,70
using the backgrounder operator, the ampersand.

4
00:00:11,70 --> 00:00:15,90
It will reveal the delay script's PID before going to sleep.

5
00:00:15,90 --> 00:00:20,60
So we're gonna do a ./delay.sh,

6
00:00:20,60 --> 00:00:23,80
say 20 seconds and put the ampersand in there

7
00:00:23,80 --> 00:00:25,80
to send it to the background.

8
00:00:25,80 --> 00:00:27,80
There it goes, it gave us its ID,

9
00:00:27,80 --> 00:00:34,10
so we're gonna do ./prop.sh 25249

10
00:00:34,10 --> 00:00:37,10
and it tell us it's watching that PID,

11
00:00:37,10 --> 00:00:39,30
any second now

12
00:00:39,30 --> 00:00:40,60
and there we go.

13
00:00:40,60 --> 00:00:43,10
We see that our delay script has woken back up

14
00:00:43,10 --> 00:00:45,70
with the we are awake now message

15
00:00:45,70 --> 00:00:48,70
and then our proc has noticed

16
00:00:48,70 --> 00:00:53,10
that it's terminated and say process 25249 has terminated

17
00:00:53,10 --> 00:00:58,00
and so, we are able to watch a script with another script.

