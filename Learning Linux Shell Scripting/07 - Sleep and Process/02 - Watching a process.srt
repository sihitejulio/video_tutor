1
00:00:01,00 --> 00:00:02,30
- [Instructor] You can have your scripts watch

2
00:00:02,30 --> 00:00:05,10
other processes and message when they terminate.

3
00:00:05,10 --> 00:00:07,10
Let's learn how to do that.

4
00:00:07,10 --> 00:00:14,50
From the terminal, touch proc.sh and let's do a change mode

5
00:00:14,50 --> 00:00:23,80
755 proc.sh and atom proc.sh.

6
00:00:23,80 --> 00:00:32,00
Got user forward slash bin forward slash env space bash

7
00:00:32,00 --> 00:00:34,50
and we're gonna create a variable called status

8
00:00:34,50 --> 00:00:37,00
and we're just gonna give it an initial value of zero.

9
00:00:37,00 --> 00:00:41,50
Status equals zero.

10
00:00:41,50 --> 00:00:43,90
Again we're gonna check to make sure that we got

11
00:00:43,90 --> 00:00:48,30
a parameter passed to us, so we'll say if square brackets

12
00:00:48,30 --> 00:00:53,50
space minus Z, and we'll say dollar sign one,

13
00:00:53,50 --> 00:00:56,80
and that's our input parameter if it's empty.

14
00:00:56,80 --> 00:01:07,40
Then we're gonna say then echo please supply a process

15
00:01:07,40 --> 00:01:13,20
which is PID, a process ID.

16
00:01:13,20 --> 00:01:15,70
And then exit with a one.

17
00:01:15,70 --> 00:01:19,90
And we got the fi, now this time we're gonna do a while loop

18
00:01:19,90 --> 00:01:21,80
but it's gonna be a little different than before

19
00:01:21,80 --> 00:01:23,90
'cause we're gonna wait for this value to change.

20
00:01:23,90 --> 00:01:26,90
So we're gonna say while square brackets space

21
00:01:26,90 --> 00:01:32,80
dollar sign status is equal to zero.

22
00:01:32,80 --> 00:01:36,50
And we're gonna do and done.

23
00:01:36,50 --> 00:01:40,70
And the PS command lists out all of the running processes,

24
00:01:40,70 --> 00:01:45,50
so we're gonna say PS dollar sign one,

25
00:01:45,50 --> 00:01:48,10
and what we're gonna do is a little bit unusual,

26
00:01:48,10 --> 00:01:49,60
so pay attention.

27
00:01:49,60 --> 00:01:53,40
We're gonna say use redirection to forward the output

28
00:01:53,40 --> 00:01:57,10
to forward slash DEV, forward slash null.

29
00:01:57,10 --> 00:01:59,50
This is going to send it to the null device,

30
00:01:59,50 --> 00:02:03,40
basically sending it to nowhere.

31
00:02:03,40 --> 00:02:05,60
But the thing that's important here is we're gonna

32
00:02:05,60 --> 00:02:11,60
do status equals dollar sign question mark.

33
00:02:11,60 --> 00:02:15,80
This picks up the status of the last command ran,

34
00:02:15,80 --> 00:02:20,10
so it's going to let us know if the PS command

35
00:02:20,10 --> 00:02:23,30
was successful, if it's successful it'll be zero,

36
00:02:23,30 --> 00:02:25,30
if it was not successful it will be

37
00:02:25,30 --> 00:02:28,60
some other value than zero.

38
00:02:28,60 --> 00:02:35,10
Then we're gonna say echo and say process

39
00:02:35,10 --> 00:02:43,90
dollar sign one has terminated, and then exit zero

40
00:02:43,90 --> 00:02:46,20
And let's add one more echo in here

41
00:02:46,20 --> 00:02:48,70
and we're gonna put it right before the while,

42
00:02:48,70 --> 00:02:55,00
so this is gonna be echo watching PID

43
00:02:55,00 --> 00:02:58,80
equals dollar sign one.

44
00:02:58,80 --> 00:03:01,30
So this is just gonna print out which process

45
00:03:01,30 --> 00:03:03,20
we're watching over.

46
00:03:03,20 --> 00:03:08,90
Then we do a control S to save it, and we go to the terminal

47
00:03:08,90 --> 00:03:11,90
and what we're gonna do in the terminal is we're gonna

48
00:03:11,90 --> 00:03:15,40
open up another, a second terminal window,

49
00:03:15,40 --> 00:03:19,50
and in that window we're just gonna type nano.

50
00:03:19,50 --> 00:03:23,20
Nano is a text editor that's supplied with most versions

51
00:03:23,20 --> 00:03:26,40
of Linux, and we're just gonna have it open up,

52
00:03:26,40 --> 00:03:30,70
and then from the original window, we're going to do

53
00:03:30,70 --> 00:03:33,50
a PS dash A.

54
00:03:33,50 --> 00:03:36,10
This will show us all of the running process

55
00:03:36,10 --> 00:03:38,20
and give us their familiar names.

56
00:03:38,20 --> 00:03:40,60
The name that we would call it.

57
00:03:40,60 --> 00:03:44,10
So when we list this out, we can see that nano

58
00:03:44,10 --> 00:03:50,50
is process ID 21681, so it is the second from the bottom

59
00:03:50,50 --> 00:03:53,30
and there's nano right there.

60
00:03:53,30 --> 00:04:00,00
So if we call our proc script, so proc.sh,

61
00:04:00,00 --> 00:04:05,20
we're gonna pass it 21681.

62
00:04:05,20 --> 00:04:10,50
And we see that it says hey, I'm watching PID 21681

63
00:04:10,50 --> 00:04:16,70
and if we go to that first window, do control X to exit nano

64
00:04:16,70 --> 00:04:20,10
we see that it also now tells us in the first window

65
00:04:20,10 --> 00:04:24,80
process 21681 has terminated, so it's let us know

66
00:04:24,80 --> 00:04:27,70
that process that we were watching terminated.

67
00:04:27,70 --> 00:04:31,00
And that's how you watch a process with a script.

