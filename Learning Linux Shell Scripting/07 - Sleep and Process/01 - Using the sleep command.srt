1
00:00:01,10 --> 00:00:02,00
- [instructor] Some processes

2
00:00:02,00 --> 00:00:04,00
only need to run intermittently.

3
00:00:04,00 --> 00:00:06,10
You can use the sleep command to have them

4
00:00:06,10 --> 00:00:07,90
go to sleep until they are needed.

5
00:00:07,90 --> 00:00:13,70
From the terminal, we're gonna say touch delay.sh,

6
00:00:13,70 --> 00:00:19,00
chmod 755 delay.sh,

7
00:00:19,00 --> 00:00:24,30
and atom delay.sh.

8
00:00:24,30 --> 00:00:34,40
Begin with our ever-present #!/usr/bin/env bash,

9
00:00:34,40 --> 00:00:37,10
then we're gonna create a variable to hold the time

10
00:00:37,10 --> 00:00:40,70
for our delay, how long we want our script

11
00:00:40,70 --> 00:00:47,30
to go to sleep, so we'll say DELAY=$1,

12
00:00:47,30 --> 00:00:49,10
then we're gonna do an if.

13
00:00:49,10 --> 00:00:50,20
Now, what we're gonna do here

14
00:00:50,20 --> 00:00:51,60
is we're gonna check to make sure

15
00:00:51,60 --> 00:00:53,60
that we got some input, 'cause without input

16
00:00:53,60 --> 00:00:55,20
this script won't work.

17
00:00:55,20 --> 00:01:00,40
We're gonna say, -z $DELAY.

18
00:01:00,40 --> 00:01:02,50
This is just gonna make sure

19
00:01:02,50 --> 00:01:05,40
that DELAY has a value, because if it doesn't,

20
00:01:05,40 --> 00:01:08,10
and that's what the -z is for,

21
00:01:08,10 --> 00:01:09,60
then what we're gonna do is say,

22
00:01:09,60 --> 00:01:19,80
then echo "You must supply a delay" and then exit.

23
00:01:19,80 --> 00:01:21,30
And, this time, we're gonna use an exit 1

24
00:01:21,30 --> 00:01:24,00
to indicate there was some sort of an error.

25
00:01:24,00 --> 00:01:27,90
Then, we'll have a fi to close off our if statement.

26
00:01:27,90 --> 00:01:30,20
Then, we're gonna let the user know

27
00:01:30,20 --> 00:01:31,80
what's gonna happen next, and so we'll say

28
00:01:31,80 --> 00:01:42,50
echo "Going to sleep for $DELAY seconds",

29
00:01:42,50 --> 00:01:47,20
then we say sleep $DELAY.

30
00:01:47,20 --> 00:01:49,40
On the sleep, the script is gonna pause

31
00:01:49,40 --> 00:01:50,50
and it's not gonna do anything.

32
00:01:50,50 --> 00:01:53,70
It's gonna return all of its cycles to the system,

33
00:01:53,70 --> 00:02:01,50
and once it's over, we can say "We are awake now"

34
00:02:01,50 --> 00:02:04,60
and do an exit 0.

35
00:02:04,60 --> 00:02:06,40
Now, the caution that comes with this

36
00:02:06,40 --> 00:02:07,90
is just be careful about your delay.

37
00:02:07,90 --> 00:02:08,90
This is in seconds.

38
00:02:08,90 --> 00:02:12,00
Yes, you can figure out a way to terminate the process,

39
00:02:12,00 --> 00:02:14,40
but it's kind of a pain so you don't wanna do

40
00:02:14,40 --> 00:02:16,10
something like say, hey, you know,

41
00:02:16,10 --> 00:02:17,90
sleep for nine hours or whatever,

42
00:02:17,90 --> 00:02:21,00
'cause that's gonna make it go to sleep for nine hours.

43
00:02:21,00 --> 00:02:23,60
So, we do a Control + S to save this,

44
00:02:23,60 --> 00:02:30,20
we go to the terminal, we type ./delay.sh,

45
00:02:30,20 --> 00:02:33,00
and let's just try something reasonable like 5,

46
00:02:33,00 --> 00:02:34,90
which is five seconds.

47
00:02:34,90 --> 00:02:39,10
And, we see it say it's going to sleep for 5 seconds,

48
00:02:39,10 --> 00:02:41,80
and it's woke back up.

49
00:02:41,80 --> 00:02:44,40
We can also put a sleeping script in the background

50
00:02:44,40 --> 00:02:47,70
so we can continue to use the shell while it is away.

51
00:02:47,70 --> 00:02:52,50
To do that, simply add the ampersand to the command line.

52
00:02:52,50 --> 00:02:54,30
So, let's try that again.

53
00:02:54,30 --> 00:02:57,20
We'll say, ./delay.sh 5

54
00:02:57,20 --> 00:03:00,20
and we'll put the ampersand on the line.

55
00:03:00,20 --> 00:03:03,30
Now, this time it showed us a couple of things.

56
00:03:03,30 --> 00:03:06,30
First off, that number where it says 4,

57
00:03:06,30 --> 00:03:09,90
that is the process id of the script

58
00:03:09,90 --> 00:03:11,30
running in the background.

59
00:03:11,30 --> 00:03:12,40
Then, it shows the message.

60
00:03:12,40 --> 00:03:15,00
Then, it shows the woke message.

61
00:03:15,00 --> 00:03:16,80
And, then we're gonna have to hit return

62
00:03:16,80 --> 00:03:19,60
in order to get it to fully release.

63
00:03:19,60 --> 00:03:21,10
Either way, when it's away,

64
00:03:21,10 --> 00:03:24,30
we are able to type in other commands.

65
00:03:24,30 --> 00:03:27,00
And, that's how you put a script to sleep.

