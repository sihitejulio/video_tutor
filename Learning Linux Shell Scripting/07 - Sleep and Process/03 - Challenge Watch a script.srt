1
00:00:00,40 --> 00:00:04,00
(whooshes)

2
00:00:04,00 --> 00:00:06,00
- [Instructor] So let's show what we know once more,

3
00:00:06,00 --> 00:00:07,50
with another challenge.

4
00:00:07,50 --> 00:00:09,60
Use this chapter's scripts,

5
00:00:09,60 --> 00:00:12,40
delay.sh and proc.sh.

6
00:00:12,40 --> 00:00:16,40
Have proc.sh watch the delay.sh script.

7
00:00:16,40 --> 00:00:18,30
Use what you already know how to do,

8
00:00:18,30 --> 00:00:22,00
you shouldn't have to write any code for this challenge.

