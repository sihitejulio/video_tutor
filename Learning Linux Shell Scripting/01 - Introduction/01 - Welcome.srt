1
00:00:00,50 --> 00:00:01,90
- [Troy] Do you sometimes find yourself

2
00:00:01,90 --> 00:00:04,40
doing boring and repetitive tasks from the terminal

3
00:00:04,40 --> 00:00:07,60
and wish you could create a way to automate the process?

4
00:00:07,60 --> 00:00:09,80
Well, you are in the right place.

5
00:00:09,80 --> 00:00:12,10
Welcome to Linux Shell Scripting.

6
00:00:12,10 --> 00:00:14,90
I'm Troy Miles and I'm a software engineer.

7
00:00:14,90 --> 00:00:16,80
In this LinkedIn learning course,

8
00:00:16,80 --> 00:00:19,40
we'll start with the basics of shell scripting

9
00:00:19,40 --> 00:00:21,40
then we'll take a look at how to write scripts

10
00:00:21,40 --> 00:00:23,70
that take parameters, read files,

11
00:00:23,70 --> 00:00:25,80
and monitor other processes.

12
00:00:25,80 --> 00:00:28,60
We'll finish up by creating interactive scripts

13
00:00:28,60 --> 00:00:31,30
and by learning how to handle bad data.

14
00:00:31,30 --> 00:00:34,20
Shell scripts can make your everyday tasks easier,

15
00:00:34,20 --> 00:00:36,90
predictable, and perhaps even fun

16
00:00:36,90 --> 00:00:41,00
so fire up your terminal, buckle up, and let's get started.

