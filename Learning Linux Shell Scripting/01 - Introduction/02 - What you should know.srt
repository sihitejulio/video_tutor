1
00:00:01,00 --> 00:00:02,10
- [Instructor] There are a few things you should know

2
00:00:02,10 --> 00:00:04,00
before starting this course.

3
00:00:04,00 --> 00:00:06,90
The first is you'll need to have a Linux computer

4
00:00:06,90 --> 00:00:08,60
with the Bash shell.

5
00:00:08,60 --> 00:00:11,60
If you want to virtual machine running Linux,

6
00:00:11,60 --> 00:00:13,40
that will also work.

7
00:00:13,40 --> 00:00:16,70
The second item you will need is a text editor.

8
00:00:16,70 --> 00:00:21,10
And I will expand on this a little later on in the course.

9
00:00:21,10 --> 00:00:24,00
Finally, in order to get the most out of this course,

10
00:00:24,00 --> 00:00:25,90
you should have some prior experience

11
00:00:25,90 --> 00:00:29,30
using terminal commands and the command line.

12
00:00:29,30 --> 00:00:31,60
A great place to start is the course

13
00:00:31,60 --> 00:00:35,00
Learning Linux Command Line found here in our library.

