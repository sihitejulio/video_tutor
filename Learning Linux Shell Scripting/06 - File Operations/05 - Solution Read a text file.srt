1
00:00:00,00 --> 00:00:04,00
(inspiring music)

2
00:00:04,00 --> 00:00:05,60
- [Instructor] How did it go, did you come up

3
00:00:05,60 --> 00:00:06,90
with a solution?

4
00:00:06,90 --> 00:00:08,90
Here is mine.

5
00:00:08,90 --> 00:00:18,60
We're gonna say touch read3.sh, change mode 755 read3.sh,

6
00:00:18,60 --> 00:00:24,60
and finally atom read3.sh.

7
00:00:24,60 --> 00:00:27,00
And then from within side of the file,

8
00:00:27,00 --> 00:00:35,40
we're gonna do our chabang, usr/bin/env bash.

9
00:00:35,40 --> 00:00:39,20
We're gonna create a count variable, count equals one.

10
00:00:39,20 --> 00:00:45,00
And we're gonna do while IFS equals and again,

11
00:00:45,00 --> 00:00:47,50
these are single quotes with nothing in between them,

12
00:00:47,50 --> 00:00:50,80
so they're an empty string, read,

13
00:00:50,80 --> 00:00:52,60
so the read command's actually gonna do

14
00:00:52,60 --> 00:00:57,90
the bulk of the work for us, then we do a -r, and LINE

15
00:00:57,90 --> 00:01:01,50
is going to hold each line of text as we read it in.

16
00:01:01,50 --> 00:01:06,40
And we're gonna have do and done and then in between them

17
00:01:06,40 --> 00:01:08,60
is where we do some work, so we're gonna say

18
00:01:08,60 --> 00:01:17,50
echo LINE $COUNT: $LINE.

19
00:01:17,50 --> 00:01:21,10
Then we're gonna use the if statement to check to see

20
00:01:21,10 --> 00:01:25,50
if we've already shown three, so we'll say if,

21
00:01:25,50 --> 00:01:28,90
square brackets, space, dollar sign, count,

22
00:01:28,90 --> 00:01:33,90
is greater than or equal to three,

23
00:01:33,90 --> 00:01:35,80
make sure we have a space before

24
00:01:35,80 --> 00:01:37,80
the end of the square brackets.

25
00:01:37,80 --> 00:01:46,90
Then we have a then and a fi and in between them, a break.

26
00:01:46,90 --> 00:01:51,60
We still need to do a ((COUNT++))

27
00:01:51,60 --> 00:01:53,70
and on the line with the done, we're gonna use

28
00:01:53,70 --> 00:01:57,20
some redirection with the less than operator

29
00:01:57,20 --> 00:02:03,60
so we can get the input, "$1" to get that first parameter.

30
00:02:03,60 --> 00:02:06,20
And then we're gonna do an exit zero

31
00:02:06,20 --> 00:02:09,40
and then we do a Ctrl + S to save it,

32
00:02:09,40 --> 00:02:16,00
go to the terminal, and we will do a ./read3.sh

33
00:02:16,00 --> 00:02:20,20
and we'll use names again, names.txt.

34
00:02:20,20 --> 00:02:23,20
And we get line one, two, and three.

35
00:02:23,20 --> 00:02:26,60
That's my solution, if yours doesn't look like mine,

36
00:02:26,60 --> 00:02:27,70
don't worry about that.

37
00:02:27,70 --> 00:02:29,40
If you didn't come up with a solution,

38
00:02:29,40 --> 00:02:32,00
be sure to study mine before you move forward.

