1
00:00:00,10 --> 00:00:04,00
(clanging)

2
00:00:04,00 --> 00:00:06,60
- [Instructor] Let's show what we know with a challenge.

3
00:00:06,60 --> 00:00:09,60
Create a script named read3.sh.

4
00:00:09,60 --> 00:00:12,60
Have it read a file name passed as a parameter.

5
00:00:12,60 --> 00:00:15,80
It should only display the first three lines of the file

6
00:00:15,80 --> 00:00:17,40
with line counts.

7
00:00:17,40 --> 00:00:19,20
Use what you already know how to do.

8
00:00:19,20 --> 00:00:24,00
Give yourself five or 10 minutes to complete this challenge.

