1
00:00:01,00 --> 00:00:02,10
- [Instructor] A checksum is a value

2
00:00:02,10 --> 00:00:04,80
that is used to validate the integrity of a file.

3
00:00:04,80 --> 00:00:08,80
Let's run a checksum on the file names.text.

4
00:00:08,80 --> 00:00:15,50
From the terminal cksum names.txt.

5
00:00:15,50 --> 00:00:17,00
The output has three values.

6
00:00:17,00 --> 00:00:20,40
The checksum, the size of the file in bytes

7
00:00:20,40 --> 00:00:22,10
and the name of the file.

8
00:00:22,10 --> 00:00:25,50
Let's see if we can detect us tampering with the file.

9
00:00:25,50 --> 00:00:30,30
Adam names.txt.

10
00:00:30,30 --> 00:00:34,20
Let's add an extra E to the end of the name Rene.

11
00:00:34,20 --> 00:00:36,10
Do a control S to save,

12
00:00:36,10 --> 00:00:38,20
return to the terminal

13
00:00:38,20 --> 00:00:41,30
and run the checksum again.

14
00:00:41,30 --> 00:00:43,40
This time the checksum is very different

15
00:00:43,40 --> 00:00:44,80
than the first time we ran it

16
00:00:44,80 --> 00:00:49,00
and the size of the file has increased by one byte.

17
00:00:49,00 --> 00:00:51,40
Let's return names.txt to the way we found it

18
00:00:51,40 --> 00:00:53,90
and run checksum one more time.

19
00:00:53,90 --> 00:00:57,80
So, we say atom names.txt

20
00:00:57,80 --> 00:01:01,00
and we gonna delete that extra E we put on here.

21
00:01:01,00 --> 00:01:02,70
Do a control S.

22
00:01:02,70 --> 00:01:07,00
Return to the terminal and we run checksum one more time.

23
00:01:07,00 --> 00:01:09,50
And we see the original checksum is restored.

24
00:01:09,50 --> 00:01:11,80
Now we know how to use the checksum command

25
00:01:11,80 --> 00:01:15,00
and we can validate the integrity of our files.

