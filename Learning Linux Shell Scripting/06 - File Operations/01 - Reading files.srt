1
00:00:01,10 --> 00:00:03,50
- [Instructor] We can read a file into a script using

2
00:00:03,50 --> 00:00:05,30
the read command and redirection.

3
00:00:05,30 --> 00:00:10,00
First, let's create a text file so we can read it later.

4
00:00:10,00 --> 00:00:18,70
Touch names.txt and atom names.txt

5
00:00:18,70 --> 00:00:21,30
Enter the following name or your own if you like.

6
00:00:21,30 --> 00:00:23,50
Place each name on it's own line.

7
00:00:23,50 --> 00:00:25,90
There is no need for a shebang since,

8
00:00:25,90 --> 00:00:28,10
this is a text file and not a script.

9
00:00:28,10 --> 00:00:31,00
So Abel, Maria,

10
00:00:31,00 --> 00:00:37,10
Richard, Tom, and Rene.

11
00:00:37,10 --> 00:00:39,90
Then we do a control S to save it

12
00:00:39,90 --> 00:00:43,10
and let's return to the terminal.

13
00:00:43,10 --> 00:00:45,50
Now let's create our script.

14
00:00:45,50 --> 00:00:51,50
We do touch reader.sh,

15
00:00:51,50 --> 00:00:57,70
change mode 755, reader.sh,

16
00:00:57,70 --> 00:01:03,50
and finally atom reader.sh

17
00:01:03,50 --> 00:01:07,40
Enter our shebang

18
00:01:07,40 --> 00:01:14,10
/bin/env space bash

19
00:01:14,10 --> 00:01:19,60
and we create a counter, so COUNT=1

20
00:01:19,60 --> 00:01:25,30
and then we type in while IFS=, and these are single quotes

21
00:01:25,30 --> 00:01:31,20
with nothing in between them space read -r

22
00:01:31,20 --> 00:01:34,20
and LINE in all caps.

23
00:01:34,20 --> 00:01:36,40
IFS is the internal field separator.

24
00:01:36,40 --> 00:01:38,70
It tells Linux how to parse the field

25
00:01:38,70 --> 00:01:41,20
of the line we are reading.

26
00:01:41,20 --> 00:01:43,10
We aren't doing anything special so we don't

27
00:01:43,10 --> 00:01:46,10
really need this so we set it to an empty string.

28
00:01:46,10 --> 00:01:50,00
The -r on the read command says do not allow backslashes

29
00:01:50,00 --> 00:01:52,10
to escape any characters.

30
00:01:52,10 --> 00:01:55,80
And finally LINE is the name of the variable used to hold

31
00:01:55,80 --> 00:01:57,60
the current line of text.

32
00:01:57,60 --> 00:02:01,40
And we're going to have a do and a done

33
00:02:01,40 --> 00:02:11,50
and in between them we say echo LINE $COUNT: $LINE

34
00:02:11,50 --> 00:02:14,20
and this is going to display the current line of text

35
00:02:14,20 --> 00:02:20,10
and the line number and we have two sets of parenthesis

36
00:02:20,10 --> 00:02:24,40
and in between them COUNT++ to increment our counter.

37
00:02:24,40 --> 00:02:28,10
Then on the line that says done we're going to do a

38
00:02:28,10 --> 00:02:31,40
less than sign, double quotes,

39
00:02:31,40 --> 00:02:34,70
and in between the double quotes $1.

40
00:02:34,70 --> 00:02:37,80
All of this should look familiar except for the last line's

41
00:02:37,80 --> 00:02:41,10
less than $1.

42
00:02:41,10 --> 00:02:44,70
The $1 is the fist parameter passed into our script.

43
00:02:44,70 --> 00:02:49,00
The less than sign says use redirection so that

44
00:02:49,00 --> 00:02:52,30
we can use the past file as input.

45
00:02:52,30 --> 00:02:58,30
Then we do an exit 0 and do a control S to save.

46
00:02:58,30 --> 00:03:05,00
Let's return to the terminal and we do a ./reader.sh

47
00:03:05,00 --> 00:03:10,30
and then we pass in names.txt and there we see

48
00:03:10,30 --> 00:03:14,00
all the names from our file and their line numbers.

49
00:03:14,00 --> 00:03:17,00
Now we know how to read a file in Linux.

