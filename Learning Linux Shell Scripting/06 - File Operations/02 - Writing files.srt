1
00:00:01,00 --> 00:00:01,80
- [Instructor] The easiest way

2
00:00:01,80 --> 00:00:04,00
to write a file is to use redirection.

3
00:00:04,00 --> 00:00:07,60
Let's send the output of our reader script to a file.

4
00:00:07,60 --> 00:00:09,70
There are two similar ways to do this.

5
00:00:09,70 --> 00:00:15,60
From the command line type ./reader.sh,

6
00:00:15,60 --> 00:00:18,20
I'm gonna say names.txt

7
00:00:18,20 --> 00:00:20,50
and then we do a greater than sign

8
00:00:20,50 --> 00:00:22,30
and then the name of the file,

9
00:00:22,30 --> 00:00:25,00
output.txt

10
00:00:25,00 --> 00:00:26,60
and when we do that we don't see anything

11
00:00:26,60 --> 00:00:28,00
on the command line

12
00:00:28,00 --> 00:00:30,80
but if we show what's inside of output.txt,

13
00:00:30,80 --> 00:00:35,00
so we do cat output.txt,

14
00:00:35,00 --> 00:00:36,50
we can see the results

15
00:00:36,50 --> 00:00:38,70
of our saving the file.

16
00:00:38,70 --> 00:00:41,00
This method creates a file name output.txt.

17
00:00:41,00 --> 00:00:42,40
If it already exists,

18
00:00:42,40 --> 00:00:43,90
its contents are erased

19
00:00:43,90 --> 00:00:46,00
and our output is written to it.

20
00:00:46,00 --> 00:00:47,90
We could also do this

21
00:00:47,90 --> 00:00:56,70
which is ./reader/sh names.txt

22
00:00:56,70 --> 00:00:59,70
and this time we do two greater than symbols

23
00:00:59,70 --> 00:01:02,80
and output.txt.

24
00:01:02,80 --> 00:01:05,60
Note that there are two greater than signs.

25
00:01:05,60 --> 00:01:07,50
This is similar to the first command

26
00:01:07,50 --> 00:01:10,50
except it doesn't erase the contents of the output file.

27
00:01:10,50 --> 00:01:12,20
Instead, it adds the new text

28
00:01:12,20 --> 00:01:13,90
to the bottom of the file.

29
00:01:13,90 --> 00:01:16,70
If we show what's inside of this file,

30
00:01:16,70 --> 00:01:21,20
so we do cat output.txt,

31
00:01:21,20 --> 00:01:24,60
we can see that it added the output from both times

32
00:01:24,60 --> 00:01:26,30
that we ran it together

33
00:01:26,30 --> 00:01:28,50
and just added files to the bottom of this.

34
00:01:28,50 --> 00:01:31,20
Now we can write the output from our scripts

35
00:01:31,20 --> 00:01:34,00
to files using redirection.

