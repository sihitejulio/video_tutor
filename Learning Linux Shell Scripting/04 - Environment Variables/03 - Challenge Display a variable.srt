1
00:00:04,00 --> 00:00:05,60
- [Narrator] Let's put what we've learned to use

2
00:00:05,60 --> 00:00:06,90
with a challenge.

3
00:00:06,90 --> 00:00:10,00
Create a script named env.sh.

4
00:00:10,00 --> 00:00:12,10
Have it display a sentence which includes

5
00:00:12,10 --> 00:00:14,50
the computer's name, the user's name,

6
00:00:14,50 --> 00:00:16,70
and the user's home directory.

7
00:00:16,70 --> 00:00:18,30
Give yourself about five minutes

8
00:00:18,30 --> 00:00:20,00
to complete this challenge.

