1
00:00:00,60 --> 00:00:03,30
- [Narrator] Here are a few environment variables.

2
00:00:03,30 --> 00:00:05,60
HOME is your user's home directory.

3
00:00:05,60 --> 00:00:08,10
HOSTNAME is the name of the computer.

4
00:00:08,10 --> 00:00:11,30
EDITOR is the name of the computer's default editor.

5
00:00:11,30 --> 00:00:14,40
Take the time to explore some of these environment variables

6
00:00:14,40 --> 00:00:16,40
and keep in mind that this is not even close

7
00:00:16,40 --> 00:00:18,20
to a comprehensive list.

8
00:00:18,20 --> 00:00:20,10
There are a lot more environment variables

9
00:00:20,10 --> 00:00:22,00
than can fit on a slide.

