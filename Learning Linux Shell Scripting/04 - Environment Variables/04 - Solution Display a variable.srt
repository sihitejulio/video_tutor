1
00:00:04,00 --> 00:00:06,20
- [Instructor] Hopefully this wasn't too difficult.

2
00:00:06,20 --> 00:00:09,50
Here is my solution to the challenge.

3
00:00:09,50 --> 00:00:12,50
We do a touch env.sh,

4
00:00:12,50 --> 00:00:18,90
change mode 755 env.sh

5
00:00:18,90 --> 00:00:23,70
and finally, atom env.sh.

6
00:00:23,70 --> 00:00:26,80
Enter the shebang,

7
00:00:26,80 --> 00:00:31,90
usr/bin/env bash

8
00:00:31,90 --> 00:00:34,60
and print out our one long sentence,

9
00:00:34,60 --> 00:00:44,60
echo the computer's name is $HOSTNAME,

10
00:00:44,60 --> 00:00:51,00
the user's name is $USER

11
00:00:51,00 --> 00:00:57,00
and the home directory

12
00:00:57,00 --> 00:01:00,30
is $HOME

13
00:01:00,30 --> 00:01:03,40
and then we do an exit 0

14
00:01:03,40 --> 00:01:05,70
and do a control S to save.

15
00:01:05,70 --> 00:01:08,50
Go back to the terminal,

16
00:01:08,50 --> 00:01:11,40
/env.sh

17
00:01:11,40 --> 00:01:13,80
and we get the computer's name,

18
00:01:13,80 --> 00:01:15,10
the user's name

19
00:01:15,10 --> 00:01:16,80
and the home directory

20
00:01:16,80 --> 00:01:19,00
and that's it for that challenge.

