1
00:00:01,00 --> 00:00:02,20
- [Man] Environment variables are

2
00:00:02,20 --> 00:00:03,10
like our own,

3
00:00:03,10 --> 00:00:05,30
except they are defined for us,

4
00:00:05,30 --> 00:00:06,60
by the system.

5
00:00:06,60 --> 00:00:08,10
They allow us to know things

6
00:00:08,10 --> 00:00:09,70
about our script's environment.

7
00:00:09,70 --> 00:00:12,50
Let's check them out with a bit of code.

8
00:00:12,50 --> 00:00:13,40
So we'll say,

9
00:00:13,40 --> 00:00:16,30
touch vars dot sh.

10
00:00:16,30 --> 00:00:18,40
To a change mode.

11
00:00:18,40 --> 00:00:23,20
Seven five five vars dot sh.

12
00:00:23,20 --> 00:00:29,20
And atom vars dot sh.

13
00:00:29,20 --> 00:00:33,10
Add our old friend, the shabang.

14
00:00:33,10 --> 00:00:35,60
Let's check out three environment variables.

15
00:00:35,60 --> 00:00:37,70
So we're gonna check out the PATH,

16
00:00:37,70 --> 00:00:39,00
the term and the editor.

17
00:00:39,00 --> 00:00:40,10
We'll just echo them out

18
00:00:40,10 --> 00:00:41,10
to the screen.

19
00:00:41,10 --> 00:00:41,90
So we'll say,

20
00:00:41,90 --> 00:00:47,20
echo the PATH is

21
00:00:47,20 --> 00:00:48,50
dollar sign PATH,

22
00:00:48,50 --> 00:00:50,80
so notice that they're just like

23
00:00:50,80 --> 00:00:52,90
our own internal variables.

24
00:00:52,90 --> 00:00:55,10
Except, these are defined elsewhere,

25
00:00:55,10 --> 00:00:56,80
they're actually defined in other scripts

26
00:00:56,80 --> 00:00:59,90
that run before our stuff executes.

27
00:00:59,90 --> 00:01:05,90
And we're gonna say echo the terminal is,

28
00:01:05,90 --> 00:01:10,30
and this is dollar sign term.

29
00:01:10,30 --> 00:01:12,80
And the final one that we'll check our right now,

30
00:01:12,80 --> 00:01:15,90
is gonna be echo

31
00:01:15,90 --> 00:01:19,90
the editor is,

32
00:01:19,90 --> 00:01:23,50
and this is dollar sign editor.

33
00:01:23,50 --> 00:01:26,20
So let's do a control s,

34
00:01:26,20 --> 00:01:27,60
go to the terminal,

35
00:01:27,60 --> 00:01:30,40
and execute this script.

36
00:01:30,40 --> 00:01:33,10
And we do var sh.

37
00:01:33,10 --> 00:01:37,60
Or if we spell it correctly, vars dot sh.

38
00:01:37,60 --> 00:01:38,60
We get the PATH

39
00:01:38,60 --> 00:01:40,00
and we get the very long string.

40
00:01:40,00 --> 00:01:43,40
We get the terminal is xterm dash 256 color.

41
00:01:43,40 --> 00:01:45,60
And we notice that the editor isn't set.

42
00:01:45,60 --> 00:01:46,60
This is usually something

43
00:01:46,60 --> 00:01:49,50
that developers will set in their

44
00:01:49,50 --> 00:01:52,40
bash underscore profile settings.

45
00:01:52,40 --> 00:01:55,20
So let's go back into atom.

46
00:01:55,20 --> 00:01:57,10
Now not all values are used

47
00:01:57,10 --> 00:01:58,70
by all systems.

48
00:01:58,70 --> 00:02:00,70
If not said, it will hold an empty string.

49
00:02:00,70 --> 00:02:02,50
And we can detect this though,

50
00:02:02,50 --> 00:02:03,60
using an IF statement,

51
00:02:03,60 --> 00:02:04,60
so let's go ahead and check

52
00:02:04,60 --> 00:02:06,20
that editor value and we'll say,

53
00:02:06,20 --> 00:02:10,10
if space minus z,

54
00:02:10,10 --> 00:02:12,70
this checks for an empty string.

55
00:02:12,70 --> 00:02:15,10
And we'll say editor.

56
00:02:15,10 --> 00:02:17,50
Again, note the spacing.

57
00:02:17,50 --> 00:02:21,30
And we'll say then echo

58
00:02:21,30 --> 00:02:24,40
the editor

59
00:02:24,40 --> 00:02:27,30
variable

60
00:02:27,30 --> 00:02:30,00
is not set.

61
00:02:30,00 --> 00:02:32,90
And give our fi,

62
00:02:32,90 --> 00:02:35,40
and do a control s again.

63
00:02:35,40 --> 00:02:39,10
And when we run this modification,

64
00:02:39,10 --> 00:02:41,50
now we see that we get the message down here,

65
00:02:41,50 --> 00:02:44,30
that the editor is not set.

66
00:02:44,30 --> 00:02:46,60
We got one last trick.

67
00:02:46,60 --> 00:02:48,40
And we can change the value

68
00:02:48,40 --> 00:02:50,10
of any of the environment variables

69
00:02:50,10 --> 00:02:51,10
in our script,

70
00:02:51,10 --> 00:02:53,20
but its value will revert

71
00:02:53,20 --> 00:02:54,80
when the script exits.

72
00:02:54,80 --> 00:02:57,20
So let's go ahead and change the PATH.

73
00:02:57,20 --> 00:02:58,70
And so what we'll do is we'll say that,

74
00:02:58,70 --> 00:03:04,10
PATH equals forward slash bob.

75
00:03:04,10 --> 00:03:07,10
And then when we say echo again,

76
00:03:07,10 --> 00:03:07,90
and again we'll say,

77
00:03:07,90 --> 00:03:09,90
the PATH

78
00:03:09,90 --> 00:03:14,10
is dollar sign PATH.

79
00:03:14,10 --> 00:03:16,80
And do a control s.

80
00:03:16,80 --> 00:03:18,70
Go back to the terminal,

81
00:03:18,70 --> 00:03:21,50
execute the script once more.

82
00:03:21,50 --> 00:03:23,70
And you notice that the very last line is,

83
00:03:23,70 --> 00:03:24,80
the PATH is,

84
00:03:24,80 --> 00:03:26,00
and it says bob.

85
00:03:26,00 --> 00:03:28,60
But if from here, and the terminal

86
00:03:28,60 --> 00:03:32,80
I do an echo dollar sign PATH,

87
00:03:32,80 --> 00:03:34,90
the PATH has been restored.

88
00:03:34,90 --> 00:03:37,50
So you can change these and they'll

89
00:03:37,50 --> 00:03:39,10
affect your script,

90
00:03:39,10 --> 00:03:41,40
but environment variables always

91
00:03:41,40 --> 00:03:43,60
revert to the way that they were set

92
00:03:43,60 --> 00:03:46,00
before your script ran.

