1
00:00:00,00 --> 00:00:04,00
(whooshing)

2
00:00:04,00 --> 00:00:06,40
- [Instructor] Let's have some fun with pipes and functions.

3
00:00:06,40 --> 00:00:08,80
Create a script named pfunc.sh,

4
00:00:08,80 --> 00:00:10,80
short for pipes and functions.

5
00:00:10,80 --> 00:00:12,90
Create two functions in it.

6
00:00:12,90 --> 00:00:15,00
Name the first GetFiles.

7
00:00:15,00 --> 00:00:17,80
It gets the first 10 regularly sorted files

8
00:00:17,80 --> 00:00:19,30
in the current directory.

9
00:00:19,30 --> 00:00:21,50
Keep in mind that there's no easy way

10
00:00:21,50 --> 00:00:23,60
to return values from a function.

11
00:00:23,60 --> 00:00:26,10
Go with what you know how to do.

12
00:00:26,10 --> 00:00:28,50
Name the second function ShowFiles.

13
00:00:28,50 --> 00:00:30,30
It will take an array of files

14
00:00:30,30 --> 00:00:33,80
as a parameter, then render each out with a counter.

15
00:00:33,80 --> 00:00:38,00
Give yourself about 15 minutes to complete this challenge.

