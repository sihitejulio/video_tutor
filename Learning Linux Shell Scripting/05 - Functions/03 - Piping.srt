1
00:00:01,20 --> 00:00:02,50
- [Instructor] If Linux was a superhero,

2
00:00:02,50 --> 00:00:04,40
its superpower would be pipes.

3
00:00:04,40 --> 00:00:07,10
Pipes let us take the output of one program,

4
00:00:07,10 --> 00:00:09,30
and feed it to the input of another.

5
00:00:09,30 --> 00:00:11,60
We can create exceptionally sophisticated programs

6
00:00:11,60 --> 00:00:13,90
simply by piping commands together.

7
00:00:13,90 --> 00:00:15,60
And, like almost everything else

8
00:00:15,60 --> 00:00:18,60
that works from the terminal, pipes also work in a script.

9
00:00:18,60 --> 00:00:22,20
Let's code up an example script using pipes.

10
00:00:22,20 --> 00:00:26,50
If I do pipe.sh, we'll touch it.

11
00:00:26,50 --> 00:00:28,80
Change mode 755,

12
00:00:28,80 --> 00:00:32,00
pipe.sh.

13
00:00:32,00 --> 00:00:37,20
Then we do atom pipe.sh.

14
00:00:37,20 --> 00:00:38,60
What we want is to display

15
00:00:38,60 --> 00:00:40,90
the first three files in our current directory

16
00:00:40,90 --> 00:00:42,90
in descending alphabetical order.

17
00:00:42,90 --> 00:00:45,70
Each file should also have a count.

18
00:00:45,70 --> 00:00:47,50
This kinda thing could easily be

19
00:00:47,50 --> 00:00:49,20
several hours of programming,

20
00:00:49,20 --> 00:00:52,00
or just a few lines of script code.

21
00:00:52,00 --> 00:00:59,30
Let's go ahead and put in our shebang.

22
00:00:59,30 --> 00:01:03,80
Then we got FILES=, and this is the backtick,

23
00:01:03,80 --> 00:01:07,00
not the single quote, but the backtick,

24
00:01:07,00 --> 00:01:11,90
and then we're gonna do an ls -1,

25
00:01:11,90 --> 00:01:13,60
then we use the pipe,

26
00:01:13,60 --> 00:01:18,20
which is the character over the backslash,

27
00:01:18,20 --> 00:01:22,20
then we do a sort -r,

28
00:01:22,20 --> 00:01:23,60
the pipe again,

29
00:01:23,60 --> 00:01:29,80
then we do a head -3, and then we have the closing backtick.

30
00:01:29,80 --> 00:01:32,80
This is the line that contains all of the magic.

31
00:01:32,80 --> 00:01:37,00
The first part, ls -1, runs the ls command,

32
00:01:37,00 --> 00:01:39,60
and limits the columns to one.

33
00:01:39,60 --> 00:01:41,70
Next comes the sort method.

34
00:01:41,70 --> 00:01:43,80
There is no need to write our own sort,

35
00:01:43,80 --> 00:01:45,90
since Linux has one built in.

36
00:01:45,90 --> 00:01:49,50
The -r says to reverse the sort order.

37
00:01:49,50 --> 00:01:55,00
And finally, head -3 says, take the first three results.

38
00:01:55,00 --> 00:01:58,70
The remainder of this script is all stuff we've seen before.

39
00:01:58,70 --> 00:02:02,50
So, we're gonna say, COUNT=1,

40
00:02:02,50 --> 00:02:05,50
this is gonna give us our file count.

41
00:02:05,50 --> 00:02:07,60
Then we're going to use a for loop,

42
00:02:07,60 --> 00:02:10,30
and we say for FILE in

43
00:02:10,30 --> 00:02:13,50
$FILES,

44
00:02:13,50 --> 00:02:15,30
do,

45
00:02:15,30 --> 00:02:20,60
and then we do echo "File #$COUNT

46
00:02:20,60 --> 00:02:24,00
= $FILE",

47
00:02:24,00 --> 00:02:27,20
and then we finally gotta increment the counter,

48
00:02:27,20 --> 00:02:33,40
so we add two sets of parentheses, and COUNT++,

49
00:02:33,40 --> 00:02:37,80
and we close the loop with a done,

50
00:02:37,80 --> 00:02:40,40
and then we give an exit of 0.

51
00:02:40,40 --> 00:02:45,40
We do a Control + S to save, then we return to the terminal.

52
00:02:45,40 --> 00:02:48,40
What you get as output depends on what's in your directory.

53
00:02:48,40 --> 00:02:50,90
Let's run it and see what we get.

54
00:02:50,90 --> 00:02:55,20
So, we do the pipe.sh,

55
00:02:55,20 --> 00:03:00,80
and we end up with while.sh, vars.sh, and sport.sh.

56
00:03:00,80 --> 00:03:04,20
We can verify the output with the regular ls command,

57
00:03:04,20 --> 00:03:07,90
so let's do an ls, and this time we'll just do a -1,

58
00:03:07,90 --> 00:03:10,00
to put them in one column,

59
00:03:10,00 --> 00:03:12,90
and we can see that the last three files,

60
00:03:12,90 --> 00:03:16,10
while.sh, vars.sh, and sport.sh,

61
00:03:16,10 --> 00:03:18,40
are the three files that we got listed

62
00:03:18,40 --> 00:03:20,90
in reverse alphabetical order.

63
00:03:20,90 --> 00:03:24,00
And that's an example of using pipes in a script.

