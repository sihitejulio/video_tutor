1
00:00:00,00 --> 00:00:04,00
(whoosh, chime)

2
00:00:04,00 --> 00:00:05,70
- [Instructor] How did you do at this challenge?

3
00:00:05,70 --> 00:00:07,90
Hopefully you solved it on your own.

4
00:00:07,90 --> 00:00:12,10
Either way, here is my version of a solution.

5
00:00:12,10 --> 00:00:16,40
So we're gonna say touch pfunc.sh,

6
00:00:16,40 --> 00:00:22,30
do a change mode 755 pfunc.sh,

7
00:00:22,30 --> 00:00:28,80
and finally atom pfunc.sh.

8
00:00:28,80 --> 00:00:33,40
Go ahead and add our shebang.

9
00:00:33,40 --> 00:00:35,40
And then we'll add our first function,

10
00:00:35,40 --> 00:00:40,70
so this is gonna be function get files.

11
00:00:40,70 --> 00:00:43,70
There is no easy way to return a value from a function,

12
00:00:43,70 --> 00:00:46,70
so we will use a global variable, files,

13
00:00:46,70 --> 00:00:53,00
to hold the file names, so files and it's gonna be equal to,

14
00:00:53,00 --> 00:00:58,60
and note these are back ticks, ls dash one,

15
00:00:58,60 --> 00:01:01,50
and we have a pipe, this time we're gonna use

16
00:01:01,50 --> 00:01:04,60
a sort with no options, so it's just gonna do

17
00:01:04,60 --> 00:01:09,30
an alphabetical sort, then we have another pipe,

18
00:01:09,30 --> 00:01:12,70
then we're gonna do a head dash 10,

19
00:01:12,70 --> 00:01:17,20
so that will take the first 10 files.

20
00:01:17,20 --> 00:01:23,60
Then below that, we're gonna do a function show files.

21
00:01:23,60 --> 00:01:27,90
Open and close parentheses, open and close curly braces,

22
00:01:27,90 --> 00:01:32,40
in here we'll use a local variable, local count,

23
00:01:32,40 --> 00:01:35,20
which is gonna be initialized to one,

24
00:01:35,20 --> 00:01:43,50
then we're gonna do a for loop, for file in $@.

25
00:01:43,50 --> 00:01:46,80
And we have a do and a done

26
00:01:46,80 --> 00:01:51,20
and in between them we're gonna echo

27
00:01:51,20 --> 00:02:00,40
file #$ count equals $ file.

28
00:02:00,40 --> 00:02:02,60
So this is gonna list out each of the files

29
00:02:02,60 --> 00:02:04,70
and give it a count,

30
00:02:04,70 --> 00:02:07,00
and then we need to increment our counter,

31
00:02:07,00 --> 00:02:12,40
so we've got two sets of parentheses, count ++.

32
00:02:12,40 --> 00:02:17,60
Then outside of any function, below the show file function,

33
00:02:17,60 --> 00:02:20,80
we're gonna say get files,

34
00:02:20,80 --> 00:02:24,40
and that's gonna call the first function,

35
00:02:24,40 --> 00:02:27,90
then after that we're gonna say show files,

36
00:02:27,90 --> 00:02:32,40
and we're gonna pass to it $ files,

37
00:02:32,40 --> 00:02:36,20
so that's gonna pass all the files that we received.

38
00:02:36,20 --> 00:02:39,70
And then finally, exit zero,

39
00:02:39,70 --> 00:02:41,90
then we do a control S to save this,

40
00:02:41,90 --> 00:02:44,30
go back to the terminal,

41
00:02:44,30 --> 00:02:50,80
and from the terminal we say .pfunc.sh

42
00:02:50,80 --> 00:02:54,80
and bam, we've got our 10 files showing

43
00:02:54,80 --> 00:02:56,40
and just to validate that,

44
00:02:56,40 --> 00:03:01,70
if we say list with a dash one,

45
00:03:01,70 --> 00:03:04,30
you notice that we get the same files in the same order,

46
00:03:04,30 --> 00:03:07,20
'cause it's doing an alphabetical sort as well,

47
00:03:07,20 --> 00:03:09,40
and that is my solution to this challenge.

48
00:03:09,40 --> 00:03:10,50
If you had any trouble,

49
00:03:10,50 --> 00:03:14,00
be sure to look over my solution carefully before moving on.

