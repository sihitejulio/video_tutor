1
00:00:01,00 --> 00:00:02,10
- [Instructor] Functions let us avoid

2
00:00:02,10 --> 00:00:04,10
duplicating code in our script.

3
00:00:04,10 --> 00:00:07,10
We can create a function to hold a single copy of code

4
00:00:07,10 --> 00:00:09,40
which we can call from multiple places.

5
00:00:09,40 --> 00:00:11,70
Let's take a look at functions.

6
00:00:11,70 --> 00:00:13,20
So touch.

7
00:00:13,20 --> 00:00:14,40
Func.

8
00:00:14,40 --> 00:00:16,20
Dot SH.

9
00:00:16,20 --> 00:00:19,10
Do a change mode.

10
00:00:19,10 --> 00:00:22,70
755.

11
00:00:22,70 --> 00:00:27,40
And atom.

12
00:00:27,40 --> 00:00:31,30
Add our sphang.

13
00:00:31,30 --> 00:00:33,30
There are two ways to create a function.

14
00:00:33,30 --> 00:00:35,90
The first begins with the word function.

15
00:00:35,90 --> 00:00:39,30
So we say function.

16
00:00:39,30 --> 00:00:41,10
Hello.

17
00:00:41,10 --> 00:00:43,70
And we include parenthesis.

18
00:00:43,70 --> 00:00:47,20
And then an opening curly brace.

19
00:00:47,20 --> 00:00:49,40
And then we put the function inside of it.

20
00:00:49,40 --> 00:00:52,30
And this time we'll just say echo.

21
00:00:52,30 --> 00:00:54,20
Hello.

22
00:00:54,20 --> 00:00:56,40
The second way to create a function is identical.

23
00:00:56,40 --> 00:01:00,00
Except it emits the word function.

24
00:01:00,00 --> 00:01:03,20
So now we'll just say goodbye.

25
00:01:03,20 --> 00:01:05,00
With the parenthesis.

26
00:01:05,00 --> 00:01:07,80
Opening curly brace and then closing curly brace.

27
00:01:07,80 --> 00:01:10,80
And in the middle we'll say echo.

28
00:01:10,80 --> 00:01:12,30
Goodbye.

29
00:01:12,30 --> 00:01:14,20
Now in order to call these,

30
00:01:14,20 --> 00:01:15,90
let's just first set up a message

31
00:01:15,90 --> 00:01:19,40
where we'll say calling

32
00:01:19,40 --> 00:01:25,00
the Hello function.

33
00:01:25,00 --> 00:01:27,80
And we say hello.

34
00:01:27,80 --> 00:01:30,20
And we'll do another message.

35
00:01:30,20 --> 00:01:33,90
Calling the goodbye function.

36
00:01:33,90 --> 00:01:37,10
And then we say goodbye.

37
00:01:37,10 --> 00:01:39,20
Please note that we don't use parenthesis

38
00:01:39,20 --> 00:01:40,90
when we call the function.

39
00:01:40,90 --> 00:01:42,90
Like other Linux commands,

40
00:01:42,90 --> 00:01:45,40
they are invoked by their name.

41
00:01:45,40 --> 00:01:48,00
Add a exit zero.

42
00:01:48,00 --> 00:01:50,60
And then control S to save this off.

43
00:01:50,60 --> 00:01:53,10
And go to the terminal to test it.

44
00:01:53,10 --> 00:01:55,50
And swirly func

45
00:01:55,50 --> 00:01:57,20
dot SH.

46
00:01:57,20 --> 00:01:59,80
And we get calling the hello function.

47
00:01:59,80 --> 00:02:02,30
Hello calling the goodbye function.

48
00:02:02,30 --> 00:02:04,50
Goodbye.

49
00:02:04,50 --> 00:02:07,10
Also keep in mind that functions must be defined

50
00:02:07,10 --> 00:02:08,80
before they can be called.

51
00:02:08,80 --> 00:02:10,40
If we move the goodbye function

52
00:02:10,40 --> 00:02:11,60
to the bottom of the file.

53
00:02:11,60 --> 00:02:14,80
So we're just gonna take this function,

54
00:02:14,80 --> 00:02:16,30
do a control X,

55
00:02:16,30 --> 00:02:18,80
and then paste it down to the bottom.

56
00:02:18,80 --> 00:02:21,20
And do a control S to save it off.

57
00:02:21,20 --> 00:02:22,90
And go back to the terminal

58
00:02:22,90 --> 00:02:25,30
and invoke the function again.

59
00:02:25,30 --> 00:02:27,70
Notice how we get the

60
00:02:27,70 --> 00:02:29,50
goodbye command not found.

61
00:02:29,50 --> 00:02:31,80
So it can't find that command because

62
00:02:31,80 --> 00:02:35,60
it doesn't exist at the time that it is called.

63
00:02:35,60 --> 00:02:37,80
So let's go back into atom and

64
00:02:37,80 --> 00:02:40,40
fix our script.

65
00:02:40,40 --> 00:02:43,60
Restore the goodbye back to the way it was before.

66
00:02:43,60 --> 00:02:45,00
Do a control S.

67
00:02:45,00 --> 00:02:48,00
And now we know how to use functions in our scripts.

