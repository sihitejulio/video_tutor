1
00:00:00,90 --> 00:00:02,80
- [Instructor] We can pass parameters to our functions

2
00:00:02,80 --> 00:00:05,10
similar to the way we pass them to our scripts

3
00:00:05,10 --> 00:00:07,10
by using the parameter symbols.

4
00:00:07,10 --> 00:00:09,10
Let's modify our func script

5
00:00:09,10 --> 00:00:12,60
to pass a name to the hello and goodbye functions.

6
00:00:12,60 --> 00:00:16,30
We pass parameters by following the function name

7
00:00:16,30 --> 00:00:18,80
with the parameter's value.

8
00:00:18,80 --> 00:00:21,40
So, in this case we're gonna say hello

9
00:00:21,40 --> 00:00:22,70
and we're gonna give it a name

10
00:00:22,70 --> 00:00:24,30
and we'll say Bob

11
00:00:24,30 --> 00:00:27,70
and for goodbye we'll give it a name also

12
00:00:27,70 --> 00:00:30,00
and call it Robert.

13
00:00:30,00 --> 00:00:32,30
Inside the function we can access the parameters

14
00:00:32,30 --> 00:00:36,00
by their symbols, $1 is the first parameter,

15
00:00:36,00 --> 00:00:38,20
$2 is the second and so forth.

16
00:00:38,20 --> 00:00:40,70
We could assign the parameters to global variables

17
00:00:40,70 --> 00:00:43,90
if we like or we could use a local variable.

18
00:00:43,90 --> 00:00:47,00
Let's use a local variable.

19
00:00:47,00 --> 00:00:54,10
So, we'll say local LNAME equals $1.

20
00:00:54,10 --> 00:00:55,30
And then here in the string

21
00:00:55,30 --> 00:00:59,50
we'll say hello $LNAME.

22
00:00:59,50 --> 00:01:01,80
So, LNAME is our local name.

23
00:01:01,80 --> 00:01:04,20
We don't necessarily have to use a local name

24
00:01:04,20 --> 00:01:05,90
but that just makes it so

25
00:01:05,90 --> 00:01:10,40
that LNAME is not defined outside of the hello function.

26
00:01:10,40 --> 00:01:13,50
And we'll take it easy inside of the goodbye function

27
00:01:13,50 --> 00:01:16,40
and we'll say $1.

28
00:01:16,40 --> 00:01:19,10
And do a control S to save this off

29
00:01:19,10 --> 00:01:21,60
and go back to the terminal

30
00:01:21,60 --> 00:01:24,80
and run our script again

31
00:01:24,80 --> 00:01:28,60
and we get hello Bob and goodbye Robert.

32
00:01:28,60 --> 00:01:30,50
Now we've seen that passing parameters

33
00:01:30,50 --> 00:01:33,10
to a function is just as easy as it is

34
00:01:33,10 --> 00:01:35,00
to pass them to a script.

