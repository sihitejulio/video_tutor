1
00:00:01,00 --> 00:00:02,70
- [Instructor] Sometimes you'd like to do one thing

2
00:00:02,70 --> 00:00:04,10
if the expression is true

3
00:00:04,10 --> 00:00:06,40
and something else if it is false.

4
00:00:06,40 --> 00:00:09,20
That is where the else clause comes in.

5
00:00:09,20 --> 00:00:11,10
If the expression is false,

6
00:00:11,10 --> 00:00:12,90
then the commands following the else

7
00:00:12,90 --> 00:00:15,00
up to the fi are executed.

8
00:00:15,00 --> 00:00:16,10
And if it is true,

9
00:00:16,10 --> 00:00:18,60
it executes the commands in between the then

10
00:00:18,60 --> 00:00:19,80
and the else.

11
00:00:19,80 --> 00:00:23,30
So, let's go ahead and add the else to our script

12
00:00:23,30 --> 00:00:25,40
and we're gonna come in here

13
00:00:25,40 --> 00:00:28,60
and right after the echo,

14
00:00:28,60 --> 00:00:31,10
we'll say else

15
00:00:31,10 --> 00:00:34,60
and we're gonna echo out

16
00:00:34,60 --> 00:00:41,10
the color is NOT blue

17
00:00:41,10 --> 00:00:44,90
and here with our number comparison,

18
00:00:44,90 --> 00:00:46,90
we'll say else

19
00:00:46,90 --> 00:00:49,50
and we'll echo

20
00:00:49,50 --> 00:00:51,90
and there's actually two possibilities here.

21
00:00:51,90 --> 00:00:57,90
You're equal or too high.

22
00:00:57,90 --> 00:01:00,30
And we'll do a control S to save that.

23
00:01:00,30 --> 00:01:02,50
Go back down to the terminal.

24
00:01:02,50 --> 00:01:05,00
Let's go ahead and just clear the screen.

25
00:01:05,00 --> 00:01:08,40
And we will say

26
00:01:08,40 --> 00:01:10,40
if.sh

27
00:01:10,40 --> 00:01:12,90
and this time we'll type the color blue

28
00:01:12,90 --> 00:01:16,00
and we'll put in the number 45.

29
00:01:16,00 --> 00:01:18,50
So, we get the color is blue,

30
00:01:18,50 --> 00:01:20,40
you're too low,

31
00:01:20,40 --> 00:01:22,80
and this time we'll try blue again

32
00:01:22,80 --> 00:01:24,80
but we'll change the number to 55

33
00:01:24,80 --> 00:01:26,90
which we know is too high

34
00:01:26,90 --> 00:01:29,70
and we get the color is blue

35
00:01:29,70 --> 00:01:31,20
and you're equal or too high

36
00:01:31,20 --> 00:01:33,30
and let's just do one more

37
00:01:33,30 --> 00:01:35,20
'cause I should have changed the color

38
00:01:35,20 --> 00:01:36,70
and this time we'll say orange

39
00:01:36,70 --> 00:01:40,10
and we'll make it 75.

40
00:01:40,10 --> 00:01:41,80
Then we get the color is not blue

41
00:01:41,80 --> 00:01:43,70
and you're equal or too high.

42
00:01:43,70 --> 00:01:47,00
So, that's how the else clause works.

