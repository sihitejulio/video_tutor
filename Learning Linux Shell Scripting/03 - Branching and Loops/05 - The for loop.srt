1
00:00:01,00 --> 00:00:02,70
- [Instructor] Let's take a look at the for loop.

2
00:00:02,70 --> 00:00:07,20
From the terminal type touch for.sh,

3
00:00:07,20 --> 00:00:12,00
change mode 755 for.sh,

4
00:00:12,00 --> 00:00:17,00
and atom for.sh.

5
00:00:17,00 --> 00:00:19,70
And we'll do our shebang.

6
00:00:19,70 --> 00:00:21,40
This time we'll take some parameters

7
00:00:21,40 --> 00:00:22,40
from the user.

8
00:00:22,40 --> 00:00:24,60
We could take the parameters individually

9
00:00:24,60 --> 00:00:28,10
using $1, $2 and so on

10
00:00:28,10 --> 00:00:30,30
but there is another special symbol,

11
00:00:30,30 --> 00:00:32,80
dollar sign at sign.

12
00:00:32,80 --> 00:00:35,10
So, here we're gonna say NAMES

13
00:00:35,10 --> 00:00:40,00
and we're gonna say equals $@.

14
00:00:40,00 --> 00:00:43,70
$@ holds all of the parameters the user typed in one array.

15
00:00:43,70 --> 00:00:45,90
We can then use this

16
00:00:45,90 --> 00:00:47,70
with the loop to iterate

17
00:00:47,70 --> 00:00:50,90
over each parameter one at a time.

18
00:00:50,90 --> 00:00:58,00
So, we say for NAME in $NAMES

19
00:00:58,00 --> 00:00:59,80
and then we say do

20
00:00:59,80 --> 00:01:01,90
and we're gonna have a done

21
00:01:01,90 --> 00:01:04,00
and then in between here

22
00:01:04,00 --> 00:01:09,30
we'll say echo hello $NAME,

23
00:01:09,30 --> 00:01:11,20
so this will print hello for each name

24
00:01:11,20 --> 00:01:13,30
that we typed in.

25
00:01:13,30 --> 00:01:20,50
Then we'll say echo for loop terminated.

26
00:01:20,50 --> 00:01:22,90
And exit 0.

27
00:01:22,90 --> 00:01:24,80
Do a control S to save,

28
00:01:24,80 --> 00:01:26,80
go back to the terminal

29
00:01:26,80 --> 00:01:29,00
and we'll type in some names,

30
00:01:29,00 --> 00:01:33,40
so we're gonna say for.sh

31
00:01:33,40 --> 00:01:34,70
and we'll type in a few names

32
00:01:34,70 --> 00:01:41,50
and I'll say Stacy, Tracy and Lacy.

33
00:01:41,50 --> 00:01:45,90
And we get hello Stacy, hello Tracy and hello Lacy.

34
00:01:45,90 --> 00:01:47,80
One thing that's nice about the for loop

35
00:01:47,80 --> 00:01:49,90
is if the array has not members

36
00:01:49,90 --> 00:01:52,80
as would be the case if the user didn't enter any names,

37
00:01:52,80 --> 00:01:54,90
the loop is never executed.

38
00:01:54,90 --> 00:01:59,20
So, if we type in just .for.sh

39
00:01:59,20 --> 00:02:00,70
with no names,

40
00:02:00,70 --> 00:02:03,80
you notice we just get the loop terminated message

41
00:02:03,80 --> 00:02:06,00
and now we know how the for loop works.

