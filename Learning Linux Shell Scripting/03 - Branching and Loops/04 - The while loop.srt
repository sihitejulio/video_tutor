1
00:00:01,00 --> 00:00:02,30
- [Instructor] Loops give us the ability

2
00:00:02,30 --> 00:00:04,30
to execute our code repetitively.

3
00:00:04,30 --> 00:00:08,00
We have two loop statements, while and for.

4
00:00:08,00 --> 00:00:10,40
Let's begin with the while loop.

5
00:00:10,40 --> 00:00:16,10
In the terminal type touch while

6
00:00:16,10 --> 00:00:19,70
and then we're gonna do a change mode

7
00:00:19,70 --> 00:00:27,30
while.sh and finally, atom while.sh.

8
00:00:27,30 --> 00:00:32,60
We'll add the shebang.

9
00:00:32,60 --> 00:00:34,20
And we'll give ourselves a counter

10
00:00:34,20 --> 00:00:38,30
which will hold the count of the passes through the loop.

11
00:00:38,30 --> 00:00:40,20
Count equals zero.

12
00:00:40,20 --> 00:00:42,10
Now, the while loops is structured similar

13
00:00:42,10 --> 00:00:44,50
to the if statement.

14
00:00:44,50 --> 00:00:47,00
So, we're gonna say while count

15
00:00:47,00 --> 00:00:49,10
and we need the dollar sign count

16
00:00:49,10 --> 00:00:53,00
is less than and we'll make it 10.

17
00:00:53,00 --> 00:00:54,20
It begins with a test

18
00:00:54,20 --> 00:00:56,80
which is evaluated at the top of each loop.

19
00:00:56,80 --> 00:00:58,80
If true, the commands between the do

20
00:00:58,80 --> 00:01:01,40
and the done statements are executed.

21
00:01:01,40 --> 00:01:03,40
So, we've got a do,

22
00:01:03,40 --> 00:01:04,70
and a done

23
00:01:04,70 --> 00:01:09,90
and then in between we're gonna say echo

24
00:01:09,90 --> 00:01:12,60
count equals #COUNT

25
00:01:12,60 --> 00:01:16,90
and count++.

26
00:01:16,90 --> 00:01:19,90
Here we display the value of the count variable

27
00:01:19,90 --> 00:01:23,20
and increment it by one each pass through the loop.

28
00:01:23,20 --> 00:01:25,00
Then it returns to the top of the loop

29
00:01:25,00 --> 00:01:27,00
and evaluates again.

30
00:01:27,00 --> 00:01:28,20
If the test fails,

31
00:01:28,20 --> 00:01:30,60
it exits the loop and resumes execution

32
00:01:30,60 --> 00:01:32,60
with the statements after the done.

33
00:01:32,60 --> 00:01:39,20
So, here let's put in while loop finished

34
00:01:39,20 --> 00:01:41,70
and exit 0.

35
00:01:41,70 --> 00:01:44,00
Do a control S to save it.

36
00:01:44,00 --> 00:01:47,10
And let's test our code in the terminal.

37
00:01:47,10 --> 00:01:51,80
And while.sh

38
00:01:51,80 --> 00:01:52,80
and there we have it,

39
00:01:52,80 --> 00:01:56,80
it counts from zero to nine and stops.

40
00:01:56,80 --> 00:01:59,00
Use care when constructing the test.

41
00:01:59,00 --> 00:02:00,30
It is easy to create a loop

42
00:02:00,30 --> 00:02:01,40
which never ends.

43
00:02:01,40 --> 00:02:03,80
For instance, if we delete the comparison

44
00:02:03,80 --> 00:02:05,30
and leave only the variable,

45
00:02:05,30 --> 00:02:10,30
so we're gonna delete the -lt 10, save it.

46
00:02:10,30 --> 00:02:12,60
This will always evaluate to true.

47
00:02:12,60 --> 00:02:15,50
So, let's return to the terminal

48
00:02:15,50 --> 00:02:18,80
and run the script again

49
00:02:18,80 --> 00:02:22,00
and you notice now the numbers just endlessly increment.

50
00:02:22,00 --> 00:02:26,30
Luckily we can do a control C to stop the script.

51
00:02:26,30 --> 00:02:27,50
And just to be sure,

52
00:02:27,50 --> 00:02:30,40
let's go back into atom

53
00:02:30,40 --> 00:02:34,00
and we store the code that we deleted

54
00:02:34,00 --> 00:02:35,30
and save that.

55
00:02:35,30 --> 00:02:36,80
Now we know the basics

56
00:02:36,80 --> 00:02:38,00
of the while loop.

