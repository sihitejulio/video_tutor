1
00:00:01,00 --> 00:00:03,40
- [Instructor] Both loops import two special instructions,

2
00:00:03,40 --> 00:00:05,00
break and continue.

3
00:00:05,00 --> 00:00:07,30
Break causes the current loop to terminate

4
00:00:07,30 --> 00:00:09,30
and it begins executing the instructions

5
00:00:09,30 --> 00:00:10,80
after the done statement.

6
00:00:10,80 --> 00:00:11,90
Let's try it out.

7
00:00:11,90 --> 00:00:14,10
We'll add an if statement in the loop

8
00:00:14,10 --> 00:00:17,10
before printing the current name.

9
00:00:17,10 --> 00:00:20,30
So, here we'll put our if

10
00:00:20,30 --> 00:00:21,90
and we'll say $NAME

11
00:00:21,90 --> 00:00:24,50
equals Tracy

12
00:00:24,50 --> 00:00:28,60
and make sure we got a space before the square bracket.

13
00:00:28,60 --> 00:00:32,80
Then, and we'll put a break.

14
00:00:32,80 --> 00:00:35,80
And then we'll put the fi.

15
00:00:35,80 --> 00:00:38,50
We'll save this off with a control S.

16
00:00:38,50 --> 00:00:40,80
Go back to the terminal.

17
00:00:40,80 --> 00:00:45,50
Once again, we'll type in Stacy, Tracy and Lacy

18
00:00:45,50 --> 00:00:47,10
and you notice what we get.

19
00:00:47,10 --> 00:00:49,20
Now we get the hello Stacy

20
00:00:49,20 --> 00:00:51,40
and as soon as it detects the Tracy,

21
00:00:51,40 --> 00:00:54,80
the break statement causes it to stop execution

22
00:00:54,80 --> 00:00:56,70
of the loop and the loop gets terminated

23
00:00:56,70 --> 00:00:58,70
and we exit our program.

24
00:00:58,70 --> 00:00:59,90
The continue statement

25
00:00:59,90 --> 00:01:02,60
goes immediately to the top of the loop.

26
00:01:02,60 --> 00:01:05,30
Let's replace the break with continue

27
00:01:05,30 --> 00:01:07,10
and see what happens.

28
00:01:07,10 --> 00:01:11,30
So, here we'll change break to continue.

29
00:01:11,30 --> 00:01:14,30
We'll save this off with a control S.

30
00:01:14,30 --> 00:01:15,40
Go to the terminal.

31
00:01:15,40 --> 00:01:19,10
Once again we'll try Stacy, Tracy and Lacy

32
00:01:19,10 --> 00:01:22,40
and now this time we get a hello Stacy, hello Lacy

33
00:01:22,40 --> 00:01:24,50
and then the loop terminates.

34
00:01:24,50 --> 00:01:26,50
So, when it gets to Tracy,

35
00:01:26,50 --> 00:01:29,90
it just goes from here to the top of the loop

36
00:01:29,90 --> 00:01:32,40
and then continues with the execution.

37
00:01:32,40 --> 00:01:33,90
Break and continue are both ways

38
00:01:33,90 --> 00:01:36,20
of modifying a loop's behavior.

39
00:01:36,20 --> 00:01:37,80
Break terminates a current loop

40
00:01:37,80 --> 00:01:40,00
while continue goes to the top of it.

