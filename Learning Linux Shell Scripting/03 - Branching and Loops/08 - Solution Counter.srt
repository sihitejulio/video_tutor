1
00:00:01,00 --> 00:00:04,00
(soft bell chiming)

2
00:00:04,00 --> 00:00:06,40
- [Instructor] So, how did you do with the challenge?

3
00:00:06,40 --> 00:00:08,50
Let me show you my solution.

4
00:00:08,50 --> 00:00:13,60
So, we're gonna say touch, counter.sh.

5
00:00:13,60 --> 00:00:20,30
Change mode, 755 counter.sh.

6
00:00:20,30 --> 00:00:25,00
Atom, counter.sh.

7
00:00:25,00 --> 00:00:28,30
We'll put in the sha-bang.

8
00:00:28,30 --> 00:00:30,20
First, we'll need a counter.

9
00:00:30,20 --> 00:00:33,20
So, we'll say count equals one.

10
00:00:33,20 --> 00:00:35,90
Then we need something that tells us when to stop,

11
00:00:35,90 --> 00:00:38,20
and this value is coming from the user.

12
00:00:38,20 --> 00:00:41,40
So, we'll call this end, that's gonna equal to

13
00:00:41,40 --> 00:00:43,30
dollar sign one.

14
00:00:43,30 --> 00:00:45,30
Then we'll create our while loop.

15
00:00:45,30 --> 00:00:50,30
We'll say while, dollar sign count

16
00:00:50,30 --> 00:00:58,00
is less than or equal to dollar sign, end, do.

17
00:00:58,00 --> 00:01:06,40
And we're gonna have echo count equals dollar sign count.

18
00:01:06,40 --> 00:01:08,20
Then we need to increment the counter.

19
00:01:08,20 --> 00:01:11,80
Remember, we have two sets of parentheses here.

20
00:01:11,80 --> 00:01:15,90
That is count, plus plus.

21
00:01:15,90 --> 00:01:19,00
And then we end the loop with a done.

22
00:01:19,00 --> 00:01:24,40
And we're gonna echo, loop, finished.

23
00:01:24,40 --> 00:01:26,20
And exit, zero.

24
00:01:26,20 --> 00:01:28,90
Save this with a control S.

25
00:01:28,90 --> 00:01:32,00
Go to the terminal, do the counter sh

26
00:01:32,00 --> 00:01:33,40
and then give it a number value.

27
00:01:33,40 --> 00:01:35,20
So, let's give it 10.

28
00:01:35,20 --> 00:01:37,70
We can save counts one to 10.

29
00:01:37,70 --> 00:01:41,30
And just for giggles let's try, 20.

30
00:01:41,30 --> 00:01:43,70
And now we see from one to 20.

31
00:01:43,70 --> 00:01:45,30
We used a parameter and a while loop

32
00:01:45,30 --> 00:01:47,00
to solve this challenge.

