1
00:00:01,00 --> 00:00:02,40
- [Narrator] Writing code in a straight line,

2
00:00:02,40 --> 00:00:04,90
where one line always executes after the next

3
00:00:04,90 --> 00:00:06,50
can't take us very far.

4
00:00:06,50 --> 00:00:08,90
In order to write useful scripts, we need the ability

5
00:00:08,90 --> 00:00:10,20
to make decisions.

6
00:00:10,20 --> 00:00:12,90
The if statement allows us to do that.

7
00:00:12,90 --> 00:00:16,80
The basic form of the if statement is if-then-fi.

8
00:00:16,80 --> 00:00:19,50
If is a test to see if an expression is true.

9
00:00:19,50 --> 00:00:21,50
If it is true, then the commands

10
00:00:21,50 --> 00:00:24,30
between the then and the fi are executed.

11
00:00:24,30 --> 00:00:26,90
Fi sounds funny but has no deep meaning.

12
00:00:26,90 --> 00:00:29,30
It is simply if backwards and denotes

13
00:00:29,30 --> 00:00:32,50
the end of the if statement.

14
00:00:32,50 --> 00:00:37,40
From the terminal let's create a new script named if.sh.

15
00:00:37,40 --> 00:00:41,60
So we're gonna do a touch if.sh.

16
00:00:41,60 --> 00:00:47,60
And then we'll say change mode 755 if.sh.

17
00:00:47,60 --> 00:00:52,50
And finally atom if.sh.

18
00:00:52,50 --> 00:00:56,70
First thing we'll do is add the shebang

19
00:00:56,70 --> 00:01:00,50
and we'll set a variable

20
00:01:00,50 --> 00:01:04,10
and set it to the input from the user.

21
00:01:04,10 --> 00:01:07,10
And we'll compare, so we'll say if

22
00:01:07,10 --> 00:01:12,60
dollar string color equals blue.

23
00:01:12,60 --> 00:01:14,70
Because we are comparing two strings,

24
00:01:14,70 --> 00:01:17,40
the strings are either equal or they're not.

25
00:01:17,40 --> 00:01:20,50
We use the equal sign to check if they're the same.

26
00:01:20,50 --> 00:01:23,50
And the bang equal to check if they're not.

27
00:01:23,50 --> 00:01:26,70
If the expression is true, then the commands in between

28
00:01:26,70 --> 00:01:29,20
the then and the fi are executed.

29
00:01:29,20 --> 00:01:36,20
So, we're gonna say then and echo

30
00:01:36,20 --> 00:01:38,90
the color is blue.

31
00:01:38,90 --> 00:01:41,30
And then fi.

32
00:01:41,30 --> 00:01:44,50
Now let's compare two variables that hold numbers.

33
00:01:44,50 --> 00:01:47,00
So we'll create another variable

34
00:01:47,00 --> 00:01:50,90
and we'll call it user guess.

35
00:01:50,90 --> 00:01:55,00
And we'll set this equal to the input parameter two

36
00:01:55,00 --> 00:02:00,90
and set the computer's value equal to 50.

37
00:02:00,90 --> 00:02:07,00
And we're gonna have an if, dollar sign user,

38
00:02:07,00 --> 00:02:09,30
underscore guess.

39
00:02:09,30 --> 00:02:13,30
And in this case we're gonna say dash lt which stands

40
00:02:13,30 --> 00:02:19,30
for less than, computer.

41
00:02:19,30 --> 00:02:22,50
We have a lot more options when we're comparing two numbers.

42
00:02:22,50 --> 00:02:25,40
The first variable can be equal to the second.

43
00:02:25,40 --> 00:02:27,10
They can be not equal.

44
00:02:27,10 --> 00:02:29,20
The first can be less than the second or it can be

45
00:02:29,20 --> 00:02:31,60
less than or equal to the second.

46
00:02:31,60 --> 00:02:34,80
Or it can be greater than or greater than and equal to.

47
00:02:34,80 --> 00:02:40,30
So we have a lot more compares when we use Boolean values.

48
00:02:40,30 --> 00:02:44,60
Alright so then we're gonna do a then and if it's

49
00:02:44,60 --> 00:02:52,20
less than we're going to echo out you're too low.

50
00:02:52,20 --> 00:02:57,60
And then put the fi and save this with the control s.

51
00:02:57,60 --> 00:03:02,80
Go back to the terminal and then execute the script.

52
00:03:02,80 --> 00:03:05,00
And we're gonna put in a color first

53
00:03:05,00 --> 00:03:07,30
and so we'll just say orange.

54
00:03:07,30 --> 00:03:11,70
And then we'll put in a value and we'll say 40.

55
00:03:11,70 --> 00:03:15,00
And in this case we get a you're too low.

56
00:03:15,00 --> 00:03:19,10
And if we try it one more time only this time we say 60

57
00:03:19,10 --> 00:03:22,20
which we know is too high, we notice we get no output

58
00:03:22,20 --> 00:03:25,50
at all because we're only printing when the color

59
00:03:25,50 --> 00:03:27,90
matches and the value is too low.

60
00:03:27,90 --> 00:03:31,00
So that's the basics of the if statement.

