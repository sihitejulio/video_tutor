1
00:00:04,00 --> 00:00:06,10
- [Instructor] Let's put what we've learned to use.

2
00:00:06,10 --> 00:00:08,80
Write a script named counter.sh.

3
00:00:08,80 --> 00:00:12,20
It will count from one to whatever number the user enters.

4
00:00:12,20 --> 00:00:15,80
Assume that the user will always enter a positive integer.

5
00:00:15,80 --> 00:00:17,20
Each pass through the loop,

6
00:00:17,20 --> 00:00:19,50
display the value of the counter,

7
00:00:19,50 --> 00:00:23,00
and when the loop terminates, display Loop Finished.

8
00:00:23,00 --> 00:00:26,00
It should take about 10 minutes to solve this challenge.

