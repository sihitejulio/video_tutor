1
00:00:01,00 --> 00:00:02,60
- [Instructor] There is still one more variation

2
00:00:02,60 --> 00:00:04,50
of the if we need to discuss.

3
00:00:04,50 --> 00:00:08,30
The elif which is an abbreviation of else if.

4
00:00:08,30 --> 00:00:10,80
It allows us to check a different expression

5
00:00:10,80 --> 00:00:12,90
than the one used in the if.

6
00:00:12,90 --> 00:00:15,10
Elif must come before the else

7
00:00:15,10 --> 00:00:16,90
which must be the last clause

8
00:00:16,90 --> 00:00:18,50
in the if statement.

9
00:00:18,50 --> 00:00:21,20
So, let's modify our script to use the elif.

10
00:00:21,20 --> 00:00:25,30
So, right here when we do the first echo,

11
00:00:25,30 --> 00:00:27,90
we'll do an elif

12
00:00:27,90 --> 00:00:33,50
and now we'll compare again the two numbers,

13
00:00:33,50 --> 00:00:34,50
USER_GUESS

14
00:00:34,50 --> 00:00:38,40
and this time we'll use a greater than

15
00:00:38,40 --> 00:00:41,80
and computer,

16
00:00:41,80 --> 00:00:45,70
then we need to put the then clause in

17
00:00:45,70 --> 00:00:49,00
and we'll echo

18
00:00:49,00 --> 00:00:53,60
you're too high.

19
00:00:53,60 --> 00:00:55,60
Then we need to change our else

20
00:00:55,60 --> 00:00:57,20
since it's a little bit different.

21
00:00:57,20 --> 00:00:59,00
This time we know

22
00:00:59,00 --> 00:01:01,20
that you're equal,

23
00:01:01,20 --> 00:01:04,70
so we'll just say

24
00:01:04,70 --> 00:01:08,10
you've guessed it.

25
00:01:08,10 --> 00:01:10,60
And we'll do a control S to save this,

26
00:01:10,60 --> 00:01:12,10
go to the terminal

27
00:01:12,10 --> 00:01:14,30
and run the script

28
00:01:14,30 --> 00:01:20,70
and we'll just say red and 60

29
00:01:20,70 --> 00:01:22,90
and this gives us we're too high.

30
00:01:22,90 --> 00:01:25,50
If we say red and 40,

31
00:01:25,50 --> 00:01:27,70
we're gonna be too low

32
00:01:27,70 --> 00:01:31,10
and if we put it right on 50,

33
00:01:31,10 --> 00:01:32,10
you've guessed it

34
00:01:32,10 --> 00:01:35,30
and so, that's how we use the elif.

35
00:01:35,30 --> 00:01:37,10
Note the elif gets the event clause

36
00:01:37,10 --> 00:01:38,60
just like the if.

37
00:01:38,60 --> 00:01:41,20
It is a syntax error not to have it.

38
00:01:41,20 --> 00:01:43,40
You can have as many elifs as you need

39
00:01:43,40 --> 00:01:45,00
to make your script work.

