1
00:00:00,50 --> 00:00:01,40
- [Instructor] Okay, so the first thing

2
00:00:01,40 --> 00:00:03,00
we're going to need to do to update

3
00:00:03,00 --> 00:00:05,30
our contents from a nested component,

4
00:00:05,30 --> 00:00:08,70
so we'll basically pass a function through our provider,

5
00:00:08,70 --> 00:00:11,80
and then leverage it and consume it in this component.

6
00:00:11,80 --> 00:00:13,70
We're going to need first, to add a few things

7
00:00:13,70 --> 00:00:17,00
and refactor our code a little bit in our App.js.

8
00:00:17,00 --> 00:00:18,70
So let's go ahead and do that.

9
00:00:18,70 --> 00:00:21,30
So the first thing I'm going to do is create a function

10
00:00:21,30 --> 00:00:23,30
inside of my constructors.

11
00:00:23,30 --> 00:00:26,90
Let's go ahead and create a function that

12
00:00:26,90 --> 00:00:33,90
we're going to call toggleName.

13
00:00:33,90 --> 00:00:36,80
And this function will basically

14
00:00:36,80 --> 00:00:41,10
take the setState function,

15
00:00:41,10 --> 00:00:46,20
and take the state.

16
00:00:46,20 --> 00:00:48,50
So basically we're going to check on the name property.

17
00:00:48,50 --> 00:00:50,60
We're going to change this in our state in a second,

18
00:00:50,60 --> 00:00:53,80
so we're going to leverage what we've done here with our user

19
00:00:53,80 --> 00:00:57,80
and basically remove this and then create our name property

20
00:00:57,80 --> 00:00:58,80
inside of our state.

21
00:00:58,80 --> 00:01:04,60
We're going to leverage the name and then

22
00:01:04,60 --> 00:01:05,90
do an if statement,

23
00:01:05,90 --> 00:01:08,30
so this is basically a shorter version,

24
00:01:08,30 --> 00:01:12,10
so the ES6 version of an if statement.

25
00:01:12,10 --> 00:01:16,70
So if our name property so let me just add the name property

26
00:01:16,70 --> 00:01:19,80
right away so it's not confusing what we're doing right now.

27
00:01:19,80 --> 00:01:21,10
So what we're going to do now,

28
00:01:21,10 --> 00:01:23,70
as opposed to doing the user inside of an object,

29
00:01:23,70 --> 00:01:25,30
we're going to do it inside of our state.

30
00:01:25,30 --> 00:01:30,30
So what I'm going to do is literally remove all this,

31
00:01:30,30 --> 00:01:32,00
then inside of our state we're going to create

32
00:01:32,00 --> 00:01:36,20
a new property called name, which we're going to do

33
00:01:36,20 --> 00:01:39,30
with Manny Henri for the time being.

34
00:01:39,30 --> 00:01:41,70
So basically what we're doing here is checking

35
00:01:41,70 --> 00:01:44,40
if the name is Manny Henri,

36
00:01:44,40 --> 00:01:50,80
then change it to Somebody else,

37
00:01:50,80 --> 00:01:54,10
otherwise change it back to Manny Henri.

38
00:01:54,10 --> 00:01:57,70
So basically we're going to do setState,

39
00:01:57,70 --> 00:02:00,90
and then check interstate the name property,

40
00:02:00,90 --> 00:02:02,50
if it's Manny Henri,

41
00:02:02,50 --> 00:02:06,10
then change it to Somebody else, if it's not Manny Henri

42
00:02:06,10 --> 00:02:09,00
then change it back to Manny Henri.

43
00:02:09,00 --> 00:02:10,90
So basically we're going to toggle the name,

44
00:02:10,90 --> 00:02:15,80
and check this, or test this, with a simple function here.

45
00:02:15,80 --> 00:02:18,20
Alright, so now that we have the function created,

46
00:02:18,20 --> 00:02:19,80
we need to pass it down to the state,

47
00:02:19,80 --> 00:02:23,50
because now in our value as opposed to pass the user,

48
00:02:23,50 --> 00:02:25,50
we're going to pass the state.

49
00:02:25,50 --> 00:02:29,50
So let's go ahead and make sure that the function

50
00:02:29,50 --> 00:02:32,50
that we created in our constructor is part of our state.

51
00:02:32,50 --> 00:02:36,20
So we're going to do toggleName

52
00:02:36,20 --> 00:02:41,50
and pass this.toggleName

53
00:02:41,50 --> 00:02:44,20
Alright, so now that we have all of our stuff set up,

54
00:02:44,20 --> 00:02:46,80
so we have our function ready,

55
00:02:46,80 --> 00:02:48,20
we've passed it to the state,

56
00:02:48,20 --> 00:02:51,40
and we've passed the initial property state

57
00:02:51,40 --> 00:02:52,80
inside of our state,

58
00:02:52,80 --> 00:02:55,30
that we'll be able to pass down to our provider.

59
00:02:55,30 --> 00:02:57,60
So now as opposed to pass in the value,

60
00:02:57,60 --> 00:03:00,00
the user that we had created up there

61
00:03:00,00 --> 00:03:01,00
is no longer available,

62
00:03:01,00 --> 00:03:03,30
so now we need to pass something else.

63
00:03:03,30 --> 00:03:06,40
What we're going to do is pass simply our state

64
00:03:06,40 --> 00:03:08,20
inside of our provider.

65
00:03:08,20 --> 00:03:10,40
So now inside of our provider,

66
00:03:10,40 --> 00:03:12,80
we have the state available.

67
00:03:12,80 --> 00:03:15,50
So all the items here that we have in our state,

68
00:03:15,50 --> 00:03:17,90
we have them available for our consumers.

69
00:03:17,90 --> 00:03:20,00
We could do news, we could do name,

70
00:03:20,00 --> 00:03:24,30
we could do toggleName inside of where we consume it.

71
00:03:24,30 --> 00:03:26,20
So let's save this,

72
00:03:26,20 --> 00:03:28,30
and we'll continue working on this in the next video

73
00:03:28,30 --> 00:03:32,00
where we update our new single with the consumer.

