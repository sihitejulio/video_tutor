1
00:00:00,60 --> 00:00:04,50
- Alright, so now that we have created a new provider here

2
00:00:04,50 --> 00:00:07,00
with this.state and we created a function

3
00:00:07,00 --> 00:00:09,90
that will be able to leverage in NewSingle.

4
00:00:09,90 --> 00:00:12,40
Let's go ahead and update our NewSingle.

5
00:00:12,40 --> 00:00:14,10
So right now if we leave it as is,

6
00:00:14,10 --> 00:00:15,80
it's not going to work because we're passing

7
00:00:15,80 --> 00:00:16,80
first name and last name

8
00:00:16,80 --> 00:00:19,30
which is not available from our provider.

9
00:00:19,30 --> 00:00:23,80
So if we switch back to our application here

10
00:00:23,80 --> 00:00:27,00
and refresh, we should have some issues here.

11
00:00:27,00 --> 00:00:30,00
So we're not having anything on the name here.

12
00:00:30,00 --> 00:00:32,90
We don't have access to first name and last name now.

13
00:00:32,90 --> 00:00:35,50
So if we bring the console as well

14
00:00:35,50 --> 00:00:37,60
we have some issues here.

15
00:00:37,60 --> 00:00:40,80
So, we also, if we go back to react.

16
00:00:40,80 --> 00:00:42,30
So if you don't have this plugin

17
00:00:42,30 --> 00:00:44,50
I would strongly recommend that you install

18
00:00:44,50 --> 00:00:46,80
the react developer tool because right now

19
00:00:46,80 --> 00:00:49,00
we can take a look at what our state looks like

20
00:00:49,00 --> 00:00:51,50
and as we click on toggle name

21
00:00:51,50 --> 00:00:55,00
we're going to see how our state is being impacted by this.

22
00:00:55,00 --> 00:00:56,50
Alright, so now let's go ahead and change

23
00:00:56,50 --> 00:00:58,90
a few things in our consumer.

24
00:00:58,90 --> 00:01:00,80
So the first thing I would like to do

25
00:01:00,80 --> 00:01:03,00
is pass the right stuff inside of the consumers.

26
00:01:03,00 --> 00:01:05,80
Right now, first name and last name is not available.

27
00:01:05,80 --> 00:01:08,20
So, let's go and change that

28
00:01:08,20 --> 00:01:12,60
so the first thing we have access to if we check our state,

29
00:01:12,60 --> 00:01:15,40
so now we're passing our state

30
00:01:15,40 --> 00:01:18,30
we have access to name, news, and toggle name.

31
00:01:18,30 --> 00:01:21,40
So let's go and pass at least name

32
00:01:21,40 --> 00:01:24,40
and the function toggle name.

33
00:01:24,40 --> 00:01:26,50
So we have access to these two here

34
00:01:26,50 --> 00:01:28,00
and we can make the changes here.

35
00:01:28,00 --> 00:01:29,30
So the first thing we are going

36
00:01:29,30 --> 00:01:34,00
to do is change this to name

37
00:01:34,00 --> 00:01:35,10
because we have access to it.

38
00:01:35,10 --> 00:01:38,70
So if we save that and we go back here

39
00:01:38,70 --> 00:01:41,00
now our name is back.

40
00:01:41,00 --> 00:01:42,40
Perfect.

41
00:01:42,40 --> 00:01:45,20
Alright, so now if we switch back to VS Code

42
00:01:45,20 --> 00:01:47,80
we need to use the toggle name function that we created.

43
00:01:47,80 --> 00:01:53,30
So whenever we click on something and we pass this function

44
00:01:53,30 --> 00:01:54,70
we'll be able to change the name

45
00:01:54,70 --> 00:01:56,70
to Somebody else or Manny Henri.

46
00:01:56,70 --> 00:01:58,50
So let's go ahead and update that

47
00:01:58,50 --> 00:02:00,90
and let's pass it down here.

48
00:02:00,90 --> 00:02:03,90
So I'm going to do it at the top level of this component.

49
00:02:03,90 --> 00:02:06,90
So I'm going to do a non-click

50
00:02:06,90 --> 00:02:10,40
and basically do toggle name.

51
00:02:10,40 --> 00:02:13,60
So whenever we click on any of the components

52
00:02:13,60 --> 00:02:15,90
or any of the instance of the components

53
00:02:15,90 --> 00:02:17,30
inside of our application

54
00:02:17,30 --> 00:02:21,50
the name should change to Somebody else or Manny Henri.

55
00:02:21,50 --> 00:02:24,80
So let's save this, let's go back to our application.

56
00:02:24,80 --> 00:02:27,60
Let's keep an eye on our state here

57
00:02:27,60 --> 00:02:29,30
so while we click on these things

58
00:02:29,30 --> 00:02:32,20
let's see how our state is being affected by this.

59
00:02:32,20 --> 00:02:34,80
Whenever we click, now it's changed

60
00:02:34,80 --> 00:02:36,30
to Somebody else in our state

61
00:02:36,30 --> 00:02:39,70
it changed also on our component

62
00:02:39,70 --> 00:02:41,90
and now because I've clicked it again

63
00:02:41,90 --> 00:02:44,60
now it's switched back to Manny Henri

64
00:02:44,60 --> 00:02:45,40
and so on and so forth.

65
00:02:45,40 --> 00:02:47,00
Whenever we click on this guy

66
00:02:47,00 --> 00:02:48,70
our state is going to change

67
00:02:48,70 --> 00:02:50,70
because of our contacts.

68
00:02:50,70 --> 00:02:52,60
So again, we then have to pass down

69
00:02:52,60 --> 00:02:56,90
that function from App to News to Newsingle

70
00:02:56,90 --> 00:02:59,70
we only needed to pass it down to our provider

71
00:02:59,70 --> 00:03:02,60
leveraging our state and then consume it

72
00:03:02,60 --> 00:03:06,20
with our contacts here and we're able to use it.

73
00:03:06,20 --> 00:03:09,70
So this is pretty much how you would use contacts

74
00:03:09,70 --> 00:03:12,50
to pass down function and be able to change

75
00:03:12,50 --> 00:03:16,80
our contacts while we're leveraging the function here.

76
00:03:16,80 --> 00:03:18,00
So let's move on.

