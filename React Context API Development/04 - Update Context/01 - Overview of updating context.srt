1
00:00:00,50 --> 00:00:02,90
- [Narrator] So now we've got our contacts set up

2
00:00:02,90 --> 00:00:05,20
but we may want to update our contacts.

3
00:00:05,20 --> 00:00:08,70
So for example, if we had a first name or last name

4
00:00:08,70 --> 00:00:12,00
or a full name and we want to pass that full name

5
00:00:12,00 --> 00:00:14,50
with a contact so I'll pass it down again

6
00:00:14,50 --> 00:00:16,30
to a new single.

7
00:00:16,30 --> 00:00:18,10
But whenever we click on this,

8
00:00:18,10 --> 00:00:19,00
let's go a card here,

9
00:00:19,00 --> 00:00:22,60
so if we go back to our application here.

10
00:00:22,60 --> 00:00:24,40
Whenever we click on one of the card,

11
00:00:24,40 --> 00:00:26,00
the name would change.

12
00:00:26,00 --> 00:00:28,80
So basically, that would represent any kind of function

13
00:00:28,80 --> 00:00:29,60
that we could do.

14
00:00:29,60 --> 00:00:31,60
So this is a quick example

15
00:00:31,60 --> 00:00:35,00
so you can understand the concepts how to do this.

16
00:00:35,00 --> 00:00:37,50
But it could be done with any kind of function.

17
00:00:37,50 --> 00:00:40,50
Any kind of function or events that you want to create

18
00:00:40,50 --> 00:00:41,60
inside of your application.

19
00:00:41,60 --> 00:00:44,40
You can also pass it down with the contacts

20
00:00:44,40 --> 00:00:47,40
and use it in one of the component

21
00:00:47,40 --> 00:00:48,50
where you has the contacts.

22
00:00:48,50 --> 00:00:50,50
So if you have the provider,

23
00:00:50,50 --> 00:00:52,70
and then the consumer.

24
00:00:52,70 --> 00:00:55,20
Then you can pass whatever function you'll pass

25
00:00:55,20 --> 00:00:57,70
inside of the provider.

26
00:00:57,70 --> 00:00:59,60
So we'll get started on actually updating

27
00:00:59,60 --> 00:01:01,30
our application here.

28
00:01:01,30 --> 00:01:03,80
So we can pass through the contacts

29
00:01:03,80 --> 00:01:06,60
a function that will leverage a new single.

30
00:01:06,60 --> 00:01:09,00
So we'll get started in the next video.

