1
00:00:00,50 --> 00:00:02,10
- [Instructor] One area to avoid when you're

2
00:00:02,10 --> 00:00:05,50
leveraging context and provider and consumers

3
00:00:05,50 --> 00:00:08,20
is to pass a value here,

4
00:00:08,20 --> 00:00:10,80
because if you're passing a straight value,

5
00:00:10,80 --> 00:00:13,80
so if we're doing something like this for example.

6
00:00:13,80 --> 00:00:16,20
Whenever the provider rerenders,

7
00:00:16,20 --> 00:00:20,40
so whenever something changes and, basically, executes

8
00:00:20,40 --> 00:00:22,50
a new rerender of the provider,

9
00:00:22,50 --> 00:00:25,30
if you want to avoid, all of the consumers,

10
00:00:25,30 --> 00:00:28,20
so in this case, this one is a consumer,

11
00:00:28,20 --> 00:00:29,90
but there could be a lot more.

12
00:00:29,90 --> 00:00:34,30
So if I want to pass this context to a lot more components,

13
00:00:34,30 --> 00:00:35,50
then I'm having an issue.

14
00:00:35,50 --> 00:00:38,60
So all these components will rerender whenever

15
00:00:38,60 --> 00:00:41,20
the provider actually renders.

16
00:00:41,20 --> 00:00:43,60
So if you want to avoid something like this,

17
00:00:43,60 --> 00:00:45,10
the best way to prevent this,

18
00:00:45,10 --> 00:00:49,70
so if the provider rerenders, you want to have only

19
00:00:49,70 --> 00:00:51,30
the components that something

20
00:00:51,30 --> 00:00:53,40
has changed into it to rerender.

21
00:00:53,40 --> 00:00:55,40
Then for performance, what you want to do

22
00:00:55,40 --> 00:00:58,50
is something like this, this.state.

23
00:00:58,50 --> 00:01:01,30
So this is the best way to prevent any renders

24
00:01:01,30 --> 00:01:03,50
when they're not needed.

25
00:01:03,50 --> 00:01:08,10
So if you want to rerender the top level here,

26
00:01:08,10 --> 00:01:09,50
but nothing is changed here,

27
00:01:09,50 --> 00:01:11,40
you don't want to rerender this one,

28
00:01:11,40 --> 00:01:13,70
you need to pass it down the state as opposed

29
00:01:13,70 --> 00:01:16,70
to the previous syntax.

30
00:01:16,70 --> 00:01:17,90
So something to keep in mind

31
00:01:17,90 --> 00:01:20,00
when you're actually working with the context.

