1
00:00:00,50 --> 00:00:01,70
- [Instructor] Some experience with React

2
00:00:01,70 --> 00:00:02,50
is definitely needed

3
00:00:02,50 --> 00:00:04,80
to be able to follow along on this course.

4
00:00:04,80 --> 00:00:08,10
If you're not familiar with components, state, props,

5
00:00:08,10 --> 00:00:10,90
you should get a basic course on React first.

6
00:00:10,90 --> 00:00:13,90
This course can be followed on any operating systems

7
00:00:13,90 --> 00:00:16,60
and, although I'm using VSCode in this course,

8
00:00:16,60 --> 00:00:19,00
feel free to use any editor you prefer.

