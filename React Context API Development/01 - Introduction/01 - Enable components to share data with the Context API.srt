1
00:00:00,50 --> 00:00:01,60
- When writing applications

2
00:00:01,60 --> 00:00:02,50
with React

3
00:00:02,50 --> 00:00:05,10
you may sometimes find yourself looking at your code

4
00:00:05,10 --> 00:00:07,90
and hoping you'd have a better way to handle the state

5
00:00:07,90 --> 00:00:10,50
and stop this madness of passing down props

6
00:00:10,50 --> 00:00:11,90
from top to bottom

7
00:00:11,90 --> 00:00:13,90
into your components.

8
00:00:13,90 --> 00:00:16,00
There is a solution and it is called

9
00:00:16,00 --> 00:00:17,60
a context API.

10
00:00:17,60 --> 00:00:19,30
Which is part of the React library

11
00:00:19,30 --> 00:00:22,80
and it is exactly what we'll explore in this course.

12
00:00:22,80 --> 00:00:24,50
Hi, I'm Mani Henri

13
00:00:24,50 --> 00:00:25,80
and having worked with React

14
00:00:25,80 --> 00:00:27,40
since the beginning of the library

15
00:00:27,40 --> 00:00:29,30
it'll be my pleasure to explore with you

16
00:00:29,30 --> 00:00:31,60
how best to leverage the context API

17
00:00:31,60 --> 00:00:33,90
into your React applications.

18
00:00:33,90 --> 00:00:36,80
We'll first take a look at what is the context API

19
00:00:36,80 --> 00:00:39,50
and what is the difference between a top down approach

20
00:00:39,50 --> 00:00:41,50
versus using context.

21
00:00:41,50 --> 00:00:43,80
Then we'll dive deep into the syntax

22
00:00:43,80 --> 00:00:45,90
of using the context API

23
00:00:45,90 --> 00:00:48,60
and how to set it up into your projects.

24
00:00:48,60 --> 00:00:51,60
Next, we'll explore how to update the context

25
00:00:51,60 --> 00:00:54,10
and what to avoid using it.

26
00:00:54,10 --> 00:00:57,50
And finally, we'll explore how to consume multiple context

27
00:00:57,50 --> 00:00:59,50
into your application.

28
00:00:59,50 --> 00:01:02,80
So if you're ready to get started with React's context API,

29
00:01:02,80 --> 00:01:04,30
open up your favorite editor

30
00:01:04,30 --> 00:01:06,00
and let's get started.

