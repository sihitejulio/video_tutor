1
00:00:00,50 --> 00:00:01,90
- [Instructor] Okay, so let's get started by

2
00:00:01,90 --> 00:00:03,80
creating a context object.

3
00:00:03,80 --> 00:00:05,60
So that's the very first thing you need to do

4
00:00:05,60 --> 00:00:09,30
when you want to leverage or use context.

5
00:00:09,30 --> 00:00:12,30
Let's go in to our App.js, first.

6
00:00:12,30 --> 00:00:15,70
What we're going to do is create an object that will leverage

7
00:00:15,70 --> 00:00:18,50
and has into our context object.

8
00:00:18,50 --> 00:00:22,80
Let's go and create a user.

9
00:00:22,80 --> 00:00:24,80
I will create some data inside of the user,

10
00:00:24,80 --> 00:00:28,00
so let's do first name,

11
00:00:28,00 --> 00:00:31,10
do "Manny".

12
00:00:31,10 --> 00:00:36,60
And then last name, let's do "the last name".

13
00:00:36,60 --> 00:00:40,10
And then let's put some data in there as well.

14
00:00:40,10 --> 00:00:44,80
"This is some data".

15
00:00:44,80 --> 00:00:48,50
Okay, now that we've created an object,

16
00:00:48,50 --> 00:00:52,60
the way to use the context is to first create

17
00:00:52,60 --> 00:00:59,20
a context object, and we'll call this one "User Data".

18
00:00:59,20 --> 00:01:01,80
And the syntax to create a context object is

19
00:01:01,80 --> 00:01:04,00
React, dot,

20
00:01:04,00 --> 00:01:07,30
create context.

21
00:01:07,30 --> 00:01:10,10
And you've seen in some cases people passing some

22
00:01:10,10 --> 00:01:13,30
default value here, so if you want to to do something like

23
00:01:13,30 --> 00:01:17,90
"light" or "cyan" or if you want to use this for

24
00:01:17,90 --> 00:01:21,30
any type of data that you want to pass as the default data

25
00:01:21,30 --> 00:01:22,70
you can pass it here.

26
00:01:22,70 --> 00:01:24,90
But since we're going to pass the user when we create

27
00:01:24,90 --> 00:01:27,80
our context inside of the rendered method

28
00:01:27,80 --> 00:01:30,00
then we're not going to use any default values here.

29
00:01:30,00 --> 00:01:31,70
But that's how you would do it.

30
00:01:31,70 --> 00:01:34,50
Okay, so then what were going to do

31
00:01:34,50 --> 00:01:38,90
is create inside of our rendered method

32
00:01:38,90 --> 00:01:43,30
a levage the user data context object that we created.

33
00:01:43,30 --> 00:01:47,40
So, what we're going to do is go ahead and create

34
00:01:47,40 --> 00:01:52,60
"User Data", which is going to be our main object.

35
00:01:52,60 --> 00:01:56,40
So, you want to to wrap this around all of your data.

36
00:01:56,40 --> 00:01:57,60
So, if you want to have access to

37
00:01:57,60 --> 00:02:01,10
the context data inside of your html here

38
00:02:01,10 --> 00:02:04,70
or inside of your components that you pass in here,

39
00:02:04,70 --> 00:02:07,40
you want to wrap this

40
00:02:07,40 --> 00:02:11,80
around everything else, like so.

41
00:02:11,80 --> 00:02:15,30
Let's just

42
00:02:15,30 --> 00:02:16,90
correct this.

43
00:02:16,90 --> 00:02:18,40
Okay, perfect.

44
00:02:18,40 --> 00:02:23,10
So, now we've created a context object right here.

45
00:02:23,10 --> 00:02:26,80
And what we're going to do next is pass that object

46
00:02:26,80 --> 00:02:29,30
inside of our "User Data" here.

47
00:02:29,30 --> 00:02:32,30
So, we're going to create a provider next.

48
00:02:32,30 --> 00:02:35,80
But right now, this is how simple it is

49
00:02:35,80 --> 00:02:37,70
to create a context object.

50
00:02:37,70 --> 00:02:42,20
You create a variable here and you pass the context object.

51
00:02:42,20 --> 00:02:46,90
And then you wrap the context object that you'd just created

52
00:02:46,90 --> 00:02:50,50
around all of your rendered components here.

53
00:02:50,50 --> 00:02:52,90
All right, so let's move on and we'll continue working

54
00:02:52,90 --> 00:02:55,00
on this in the next video.

