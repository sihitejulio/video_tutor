1
00:00:00,50 --> 00:00:02,80
- The next stage in working with the context

2
00:00:02,80 --> 00:00:05,00
is to create the Consumer,

3
00:00:05,00 --> 00:00:06,70
so let's go ahead and do that.

4
00:00:06,70 --> 00:00:09,40
So first let's go to App.js and let's

5
00:00:09,40 --> 00:00:13,30
add a little line right after the UserData,

6
00:00:13,30 --> 00:00:15,10
where we create our contacts.

7
00:00:15,10 --> 00:00:18,90
And what we'll do is quickly export an object

8
00:00:18,90 --> 00:00:22,60
that is called UserConsumer

9
00:00:22,60 --> 00:00:27,30
which includes the UserData.Consumer.

10
00:00:27,30 --> 00:00:29,80
So basically, what we're doing here is

11
00:00:29,80 --> 00:00:32,70
using the contacts that we created here

12
00:00:32,70 --> 00:00:36,30
and then passing the consumer to this value

13
00:00:36,30 --> 00:00:37,70
that we're exporting,

14
00:00:37,70 --> 00:00:39,30
which is going to be available

15
00:00:39,30 --> 00:00:42,50
on any of our other components.

16
00:00:42,50 --> 00:00:46,30
Now, let's open NewSingle.js

17
00:00:46,30 --> 00:00:49,30
and let's start working with our Consumer.

18
00:00:49,30 --> 00:00:53,70
So what we're going to do first is import the UserConsumer.

19
00:00:53,70 --> 00:00:58,50
And let's just do from before, like so,

20
00:00:58,50 --> 00:01:01,80
so we can quickly have co-completion here.

21
00:01:01,80 --> 00:01:04,90
So let's do UserConsumer, like so.

22
00:01:04,90 --> 00:01:06,90
So now we can wrap the Consumer

23
00:01:06,90 --> 00:01:09,60
inside of our NewSingle component.

24
00:01:09,60 --> 00:01:10,70
Let's go ahead and do that.

25
00:01:10,70 --> 00:01:14,40
So we can do UserConsumer

26
00:01:14,40 --> 00:01:16,30
and let's wrap it.

27
00:01:16,30 --> 00:01:17,70
So let's cut that

28
00:01:17,70 --> 00:01:20,90
and pass it down here.

29
00:01:20,90 --> 00:01:23,70
What I'm going to do is tap that

30
00:01:23,70 --> 00:01:28,10
and inside of that component, we need to do a function.

31
00:01:28,10 --> 00:01:30,30
So what we're going to do is,

32
00:01:30,30 --> 00:01:35,70
first, do a function, which will pass a value.

33
00:01:35,70 --> 00:01:39,10
So we'll pass firstName, so we have firstName available.

34
00:01:39,10 --> 00:01:40,20
So if we go back here,

35
00:01:40,20 --> 00:01:43,20
we have this value firstName in our User.

36
00:01:43,20 --> 00:01:47,00
So let's go ahead and pass that.

37
00:01:47,00 --> 00:01:51,50
And then we'll return in here our components.

38
00:01:51,50 --> 00:01:53,10
So let's, basically,

39
00:01:53,10 --> 00:01:56,90
grab the curly brace and parenthesis

40
00:01:56,90 --> 00:02:01,10
and cut that and just paste it after our div on line 21,

41
00:02:01,10 --> 00:02:02,80
like so.

42
00:02:02,80 --> 00:02:05,00
So now that we've passed the firstName value

43
00:02:05,00 --> 00:02:09,10
from our Consumer, which we have here,

44
00:02:09,10 --> 00:02:10,40
we're able to use it somewhere.

45
00:02:10,40 --> 00:02:13,30
So let's replace the item.source.name

46
00:02:13,30 --> 00:02:18,10
by the firstName that we passed.

47
00:02:18,10 --> 00:02:19,90
So now we're going to have access to this here.

48
00:02:19,90 --> 00:02:23,60
So the firstName is, Manny.

49
00:02:23,60 --> 00:02:25,70
So we should see this in our application.

50
00:02:25,70 --> 00:02:26,90
So let's go and run it.

51
00:02:26,90 --> 00:02:29,30
So let's go into our Terminal

52
00:02:29,30 --> 00:02:30,40
and it's already running.

53
00:02:30,40 --> 00:02:32,80
So let's go to our localhost:3000/

54
00:02:32,80 --> 00:02:37,00
So I'm going to open a brand new window.

55
00:02:37,00 --> 00:02:38,20
And as you can see,

56
00:02:38,20 --> 00:02:42,60
my first name is passed on every single item

57
00:02:42,60 --> 00:02:44,70
because I've got it from the contacts

58
00:02:44,70 --> 00:02:48,40
and I've passed it to the NewSingle.

59
00:02:48,40 --> 00:02:49,80
So if we make a change to that--

60
00:02:49,80 --> 00:02:52,80
So if we pass also the lastName,

61
00:02:52,80 --> 00:02:56,70
so let's go ahead and pass lastName,

62
00:02:56,70 --> 00:03:00,00
and use it as well, here.

63
00:03:00,00 --> 00:03:00,90
And then save.

64
00:03:00,90 --> 00:03:04,60
And again, we have that data available so let's go back.

65
00:03:04,60 --> 00:03:08,10
We have all of our contacts item passed down

66
00:03:08,10 --> 00:03:09,90
to our component.

67
00:03:09,90 --> 00:03:10,90
So as you can see,

68
00:03:10,90 --> 00:03:14,80
we can pass item from the top level, which is here.

69
00:03:14,80 --> 00:03:16,60
We've created a contacts.

70
00:03:16,60 --> 00:03:19,90
We've passed it with a Provider, here.

71
00:03:19,90 --> 00:03:23,90
And we've passed the user value inside of our Provider.

72
00:03:23,90 --> 00:03:27,70
And then we went through a single item or a component,

73
00:03:27,70 --> 00:03:29,50
which is not really at the top level.

74
00:03:29,50 --> 00:03:31,00
So a single is not available here.

75
00:03:31,00 --> 00:03:34,40
So, in theory, if I would do the top-down approach,

76
00:03:34,40 --> 00:03:36,30
I wouldn't have access to this data.

77
00:03:36,30 --> 00:03:37,60
So I'm passing on, basically,

78
00:03:37,60 --> 00:03:40,60
going from the App to News,

79
00:03:40,60 --> 00:03:43,30
like we've shown in the slides earlier.

80
00:03:43,30 --> 00:03:46,70
And then we're going to News and then in the News,

81
00:03:46,70 --> 00:03:49,00
we're actually using the NewSingle,

82
00:03:49,00 --> 00:03:50,60
which we're not passing anything here.

83
00:03:50,60 --> 00:03:52,90
We're not passing the firstName and lastName

84
00:03:52,90 --> 00:03:55,00
from the top-down approach.

85
00:03:55,00 --> 00:03:56,50
We're going to NewSingle

86
00:03:56,50 --> 00:04:01,40
and now, we're importing the Consumer from our contacts

87
00:04:01,40 --> 00:04:04,80
and leveraging it by running this function, here,

88
00:04:04,80 --> 00:04:08,10
and using it inside of our component.

89
00:04:08,10 --> 00:04:11,90
So this is how you would use contacts in a very simple way.

90
00:04:11,90 --> 00:04:13,70
There's other ways to use the contacts

91
00:04:13,70 --> 00:04:16,60
and we'll explore this in the next few chapters.

92
00:04:16,60 --> 00:04:18,00
So let's move on.

