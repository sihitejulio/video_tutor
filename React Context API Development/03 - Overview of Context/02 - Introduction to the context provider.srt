1
00:00:00,50 --> 00:00:02,80
- [Instructor] The next stage in working with a context

2
00:00:02,80 --> 00:00:04,90
is to create the provider.

3
00:00:04,90 --> 00:00:07,80
So, the provider will provide the context

4
00:00:07,80 --> 00:00:10,20
to its children components.

5
00:00:10,20 --> 00:00:15,90
So, the way to do this is do UserData.Provider

6
00:00:15,90 --> 00:00:20,30
and the provider needs to have a value

7
00:00:20,30 --> 00:00:23,40
and as you probably guessed it, the value will be

8
00:00:23,40 --> 00:00:26,70
the user data that we've created in this object here.

9
00:00:26,70 --> 00:00:28,80
It could also be something from the state.

10
00:00:28,80 --> 00:00:31,90
So, you could do, for example, you could do the user data

11
00:00:31,90 --> 00:00:36,20
inside of the state and then pass this.state.user

12
00:00:36,20 --> 00:00:37,50
and we're going to do this later on

13
00:00:37,50 --> 00:00:40,50
when we start working with updating our context.

14
00:00:40,50 --> 00:00:43,60
So, for now, let's just do, user.

15
00:00:43,60 --> 00:00:47,00
So, basically the job of the provider is providing access

16
00:00:47,00 --> 00:00:50,00
to the data to its children components

17
00:00:50,00 --> 00:00:51,80
and if there's any changes to it,

18
00:00:51,80 --> 00:00:54,40
it's going to rely on the value here.

19
00:00:54,40 --> 00:00:57,60
So, for example, if we had the user in the state

20
00:00:57,60 --> 00:01:00,80
and, for example, we make a change to the state,

21
00:01:00,80 --> 00:01:03,80
then this provider would update the value

22
00:01:03,80 --> 00:01:06,80
inside of the context and then provide this value

23
00:01:06,80 --> 00:01:09,30
across all of our components.

24
00:01:09,30 --> 00:01:12,90
So, it's really important to have, first, a provider

25
00:01:12,90 --> 00:01:15,80
before you start using the state.

26
00:01:15,80 --> 00:01:23,10
Now let's make sure we do this properly

27
00:01:23,10 --> 00:01:26,80
and then close the UserData provider.

28
00:01:26,80 --> 00:01:30,00
So, you need to make sure you close the same way

29
00:01:30,00 --> 00:01:32,60
as you open

30
00:01:32,60 --> 00:01:37,20
your HTML tags so we don't have any issues like this.

31
00:01:37,20 --> 00:01:40,00
Alright, so now we've created a provider

32
00:01:40,00 --> 00:01:43,80
and we're going to use that provider to start consume

33
00:01:43,80 --> 00:01:47,20
the context inside of one of our components.

34
00:01:47,20 --> 00:01:48,90
So, let's save this first

35
00:01:48,90 --> 00:01:51,00
and let's move on to the next video.

