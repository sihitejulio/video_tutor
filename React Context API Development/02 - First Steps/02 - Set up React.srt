1
00:00:00,80 --> 00:00:02,20
- [Instructor] The first thing we need to do is

2
00:00:02,20 --> 00:00:03,40
set up React properly.

3
00:00:03,40 --> 00:00:06,00
We want to make sure we are on the latest version of

4
00:00:06,00 --> 00:00:09,40
React before we get started with contacts.

5
00:00:09,40 --> 00:00:12,20
I typically do this in VS Code, but feel free to

6
00:00:12,20 --> 00:00:14,60
do this on any terminal windows.

7
00:00:14,60 --> 00:00:17,40
On a Mac, you can do that on the terminal window,

8
00:00:17,40 --> 00:00:19,50
and on Windows, you can do this on

9
00:00:19,50 --> 00:00:22,40
Git BASH or the command prompt.

10
00:00:22,40 --> 00:00:24,60
I'm going to do this inside of VS Code.

11
00:00:24,60 --> 00:00:27,90
If you're following along, open up VS Code

12
00:00:27,90 --> 00:00:33,30
and then click on View, Terminal.

13
00:00:33,30 --> 00:00:34,40
The first thing we're going to do is

14
00:00:34,40 --> 00:00:37,00
make sure we have the latest version of React.

15
00:00:37,00 --> 00:00:38,50
Let's go ahead and do this.

16
00:00:38,50 --> 00:00:42,70
We're going to do npm install, or I.

17
00:00:42,70 --> 00:00:46,10
I typically do I, but I'm going to type install.

18
00:00:46,10 --> 00:00:55,10
G, so it's global, and then create-react-app.

19
00:00:55,10 --> 00:00:56,90
Once you have the latest version of

20
00:00:56,90 --> 00:00:59,40
create-react-app open and installed,

21
00:00:59,40 --> 00:01:01,00
you're good to go.

22
00:01:01,00 --> 00:01:03,30
There's two ways we can continue working on this.

23
00:01:03,30 --> 00:01:05,50
You can follow along with the base project that

24
00:01:05,50 --> 00:01:07,50
we're going to have in this course,

25
00:01:07,50 --> 00:01:10,10
or you can create a brand new React project.

26
00:01:10,10 --> 00:01:11,90
It doesn't matter as long as you're

27
00:01:11,90 --> 00:01:14,50
following along with the examples we're going to do.

28
00:01:14,50 --> 00:01:16,30
If you want to create a brand new project,

29
00:01:16,30 --> 00:01:20,70
you do create-react-app.

30
00:01:20,70 --> 00:01:28,90
You can call this one portal, and then hit Enter.

31
00:01:28,90 --> 00:01:30,50
If you want to use the application that you

32
00:01:30,50 --> 00:01:32,70
just installed, go ahead and do it.

33
00:01:32,70 --> 00:01:34,50
But in this case, I'm going to use

34
00:01:34,50 --> 00:01:36,90
the portal that I created.

35
00:01:36,90 --> 00:01:42,70
Let's go ahead and go into the exercise files

36
00:01:42,70 --> 00:01:45,90
and then go into the resources.

37
00:01:45,90 --> 00:01:50,20
Let's create a brand new folder on our desktop.

38
00:01:50,20 --> 00:01:54,40
I'm going to call this one portal.

39
00:01:54,40 --> 00:01:59,30
Basically, copy everything in here

40
00:01:59,30 --> 00:02:04,20
and paste it in here like so.

41
00:02:04,20 --> 00:02:07,00
Basically, if you want to have the very same

42
00:02:07,00 --> 00:02:11,40
versions that I'm using in my package.json file,

43
00:02:11,40 --> 00:02:13,20
copy the package-lock.

44
00:02:13,20 --> 00:02:15,30
If not, and you want to use the latest

45
00:02:15,30 --> 00:02:18,20
versions of what we're using in this project,

46
00:02:18,20 --> 00:02:20,80
go ahead and simply copy the package one

47
00:02:20,80 --> 00:02:23,20
and npm install that.

48
00:02:23,20 --> 00:02:24,10
It's going to install

49
00:02:24,10 --> 00:02:27,00
the latest version of your packages.

50
00:02:27,00 --> 00:02:28,20
Now that we have this,

51
00:02:28,20 --> 00:02:30,60
now we can open this inside of VS Code.

52
00:02:30,60 --> 00:02:34,80
Let's go back to VS Code, Open Folder.

53
00:02:34,80 --> 00:02:37,20
By the way, this Open Folder is in the Explorer,

54
00:02:37,20 --> 00:02:38,70
so if you don't have this open,

55
00:02:38,70 --> 00:02:41,80
click on Explorer, Open Folder,

56
00:02:41,80 --> 00:02:48,40
and let's go to the desktop, and then portal.

57
00:02:48,40 --> 00:02:51,60
Then let's go and install all of our dependencies.

58
00:02:51,60 --> 00:02:58,20
I'm going to go to Terminal and do an npm i.

59
00:02:58,20 --> 00:02:59,80
It's going to install everything that I

60
00:02:59,80 --> 00:03:00,90
have in this project.

61
00:03:00,90 --> 00:03:04,50
If we take a look here, we have axios,

62
00:03:04,50 --> 00:03:06,70
React version 16.8 ...

63
00:03:06,70 --> 00:03:09,30
You may actually be using a different version

64
00:03:09,30 --> 00:03:11,60
if you have created your own project,

65
00:03:11,60 --> 00:03:14,70
but as long as you're using version 16.8,

66
00:03:14,70 --> 00:03:19,70
you should be good to follow along in this course.

67
00:03:19,70 --> 00:03:23,00
Let's go back and take a look at our terminal.

68
00:03:23,00 --> 00:03:25,50
All our dependencies have been installed.

69
00:03:25,50 --> 00:03:29,40
If we do an npm start,

70
00:03:29,40 --> 00:03:31,80
we can see our project here which is not really

71
00:03:31,80 --> 00:03:33,70
good-looking, but that's not the ...

72
00:03:33,70 --> 00:03:35,00
The purpose of this course is to

73
00:03:35,00 --> 00:03:36,80
teach context API.

74
00:03:36,80 --> 00:03:38,20
But if you want, you can go ahead and do

75
00:03:38,20 --> 00:03:42,50
a little CSS job on this one to make it look better.

76
00:03:42,50 --> 00:03:43,80
Now that we have our project,

77
00:03:43,80 --> 00:03:46,30
we'll explore what is inside that project

78
00:03:46,30 --> 00:03:48,00
in the next video.

