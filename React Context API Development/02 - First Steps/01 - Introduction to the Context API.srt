1
00:00:00,50 --> 00:00:03,30
- If you're completely new to Reacts Context API,

2
00:00:03,30 --> 00:00:04,60
and why you should use it,

3
00:00:04,60 --> 00:00:07,10
or even when is a good idea to use it,

4
00:00:07,10 --> 00:00:09,80
this is a perfect introduction for you.

5
00:00:09,80 --> 00:00:11,60
If you want to get to code right away,

6
00:00:11,60 --> 00:00:13,20
feel free to skip this video.

7
00:00:13,20 --> 00:00:15,40
When we create our state at the top level of

8
00:00:15,40 --> 00:00:16,90
a React Component Tree,

9
00:00:16,90 --> 00:00:19,80
typically we need to pass down our props from one component

10
00:00:19,80 --> 00:00:20,90
to another.

11
00:00:20,90 --> 00:00:23,40
This can be quite repetitive and redundant as

12
00:00:23,40 --> 00:00:25,20
our application grows.

13
00:00:25,20 --> 00:00:28,00
Facebook being the biggest consumer of React Code,

14
00:00:28,00 --> 00:00:30,10
faced this issue like all of us.

15
00:00:30,10 --> 00:00:33,10
And the React Team was determined to find a solution.

16
00:00:33,10 --> 00:00:36,00
This is why the Context API was created.

17
00:00:36,00 --> 00:00:38,10
The Context API is basically a way,

18
00:00:38,10 --> 00:00:41,20
with special syntax to pass props directly

19
00:00:41,20 --> 00:00:43,30
into the component that needs it.

20
00:00:43,30 --> 00:00:47,10
Without the redundancy of passing props from one component

21
00:00:47,10 --> 00:00:48,50
to another.

22
00:00:48,50 --> 00:00:51,50
If we look at the picture here, this is the typical method

23
00:00:51,50 --> 00:00:53,90
of passing props from top to bottom.

24
00:00:53,90 --> 00:00:56,60
In this case, which is the Apple use,

25
00:00:56,60 --> 00:01:01,90
where we pass values from App.js to News.js to NewSingle.js.

26
00:01:01,90 --> 00:01:05,90
But does the Context API always is the perfect approach

27
00:01:05,90 --> 00:01:07,40
to all cases?

28
00:01:07,40 --> 00:01:08,20
No.

29
00:01:08,20 --> 00:01:10,80
In fact, it's an option we have in our tool shed

30
00:01:10,80 --> 00:01:12,40
to pass values.

31
00:01:12,40 --> 00:01:15,60
Top-down approach might still be the right way to go.

32
00:01:15,60 --> 00:01:17,80
So when do you use Contexts?

33
00:01:17,80 --> 00:01:20,00
When you want to share global data.

34
00:01:20,00 --> 00:01:22,90
Items that seem redundant to pass.

35
00:01:22,90 --> 00:01:24,30
If you go to examples,

36
00:01:24,30 --> 00:01:26,30
coming right out of Reacts Documentation,

37
00:01:26,30 --> 00:01:29,10
are Theme Props, CSS Props or

38
00:01:29,10 --> 00:01:31,30
even Language Definition Props.

39
00:01:31,30 --> 00:01:34,40
Any kind of items that could be used across.

40
00:01:34,40 --> 00:01:37,00
And if you're trying to implement the Context CPI,

41
00:01:37,00 --> 00:01:39,30
and start feeling like you can't keep track of

42
00:01:39,30 --> 00:01:41,90
your Component Tree and it's data,

43
00:01:41,90 --> 00:01:44,90
most likely Context isn't the right choice.

44
00:01:44,90 --> 00:01:47,00
Always consider both options.

