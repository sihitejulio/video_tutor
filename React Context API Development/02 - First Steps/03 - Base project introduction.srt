1
00:00:00,10 --> 00:00:02,70
- Okay, so let's quickly explore the content

2
00:00:02,70 --> 00:00:04,10
of this particular project.

3
00:00:04,10 --> 00:00:06,40
What is inside that project?

4
00:00:06,40 --> 00:00:09,60
The first thing I want you to do is go to the public folder

5
00:00:09,60 --> 00:00:11,80
and then click on index.

6
00:00:11,80 --> 00:00:16,30
So, the first thing we have in this project is materialize.

7
00:00:16,30 --> 00:00:19,90
We have materialize loaded as the styles of this project.

8
00:00:19,90 --> 00:00:22,40
Sorry, if you don't see any styles in this project

9
00:00:22,40 --> 00:00:24,50
or just the one file,

10
00:00:24,50 --> 00:00:27,50
so the App.css here,

11
00:00:27,50 --> 00:00:30,20
that's the only styles that we're applying

12
00:00:30,20 --> 00:00:33,40
besides materialize. If you want to switch to something else

13
00:00:33,40 --> 00:00:36,10
or you want to make changes to the css,

14
00:00:36,10 --> 00:00:38,20
feel free to remove this.

15
00:00:38,20 --> 00:00:40,70
Now, let's go into the source

16
00:00:40,70 --> 00:00:44,70
and then first, take a look at atl js.

17
00:00:44,70 --> 00:00:47,30
So, as you can see what we are doing is creating a state

18
00:00:47,30 --> 00:00:49,60
where we are creating some items here.

19
00:00:49,60 --> 00:00:51,00
I'll type everything

20
00:00:51,00 --> 00:00:55,40
and query property that we're passing down

21
00:00:55,40 --> 00:00:59,30
to our news component here.

22
00:00:59,30 --> 00:01:02,00
Then we go into the news folder

23
00:01:02,00 --> 00:01:04,70
and go to the news,

24
00:01:04,70 --> 00:01:06,10
and now we are creating

25
00:01:06,10 --> 00:01:09,90
the url with the props that we've passed.

26
00:01:09,90 --> 00:01:14,20
So, we can actually return a query to the news api.

27
00:01:14,20 --> 00:01:17,00
This is an api that we can grab the most recent news

28
00:01:17,00 --> 00:01:20,80
and I've passed some items here inside of the url

29
00:01:20,80 --> 00:01:23,90
to do a query and then return it inside

30
00:01:23,90 --> 00:01:27,60
of that particular new single item here.

31
00:01:27,60 --> 00:01:32,70
We have the news that is returned and then has many times

32
00:01:32,70 --> 00:01:34,30
we have news items here

33
00:01:34,30 --> 00:01:36,60
and I believe if I am correct,

34
00:01:36,60 --> 00:01:40,80
we are pulling about ten items out of that news

35
00:01:40,80 --> 00:01:43,30
or it's returning ten items.

36
00:01:43,30 --> 00:01:45,80
Then, we are returning that with a new single and then

37
00:01:45,80 --> 00:01:50,10
passing again some stuff here into the new single

38
00:01:50,10 --> 00:01:52,60
and then rendering this functional component

39
00:01:52,60 --> 00:01:55,20
with all the items that we're passing down.

40
00:01:55,20 --> 00:01:57,20
This is what makes the application that you've

41
00:01:57,20 --> 00:01:59,10
seen in the last video.

42
00:01:59,10 --> 00:02:02,80
So, basically, the one area that we're not going to change

43
00:02:02,80 --> 00:02:07,10
and this is where the context api would not be a good idea

44
00:02:07,10 --> 00:02:10,40
is the data that we're passing down like that.

45
00:02:10,40 --> 00:02:14,30
So, we're passing down this thing here and then

46
00:02:14,30 --> 00:02:18,70
creating a link in there with the props that we're passing

47
00:02:18,70 --> 00:02:21,50
and then returning a new item here.

48
00:02:21,50 --> 00:02:24,20
This is not something that would be global

49
00:02:24,20 --> 00:02:26,80
and we don't need to quite change the way

50
00:02:26,80 --> 00:02:28,60
the components are structured right now.

51
00:02:28,60 --> 00:02:31,30
What we are going to do is pass different items

52
00:02:31,30 --> 00:02:33,80
inside of this application.

53
00:02:33,80 --> 00:02:36,50
This is an overview of this application.

54
00:02:36,50 --> 00:02:38,90
If you want to take a look at the css afterwards,

55
00:02:38,90 --> 00:02:40,90
these are some changes that we've made,

56
00:02:40,90 --> 00:02:45,00
but again, this is not a styles or css course.

57
00:02:45,00 --> 00:02:47,90
This is about learning about context api's,

58
00:02:47,90 --> 00:02:52,20
We are not going to do any style changes in this course.

59
00:02:52,20 --> 00:02:54,70
This is a brief introduction to the application

60
00:02:54,70 --> 00:02:56,90
and we are going to start working on the context api

61
00:02:56,90 --> 00:02:58,00
starting next.

