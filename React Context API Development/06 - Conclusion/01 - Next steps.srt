1
00:00:00,50 --> 00:00:01,90
- [Emmanuel] My hope is that you've gained

2
00:00:01,90 --> 00:00:04,20
a thorough understanding of what benefits you can get

3
00:00:04,20 --> 00:00:06,50
by implementing React's Context API

4
00:00:06,50 --> 00:00:08,80
into your application development workflow.

5
00:00:08,80 --> 00:00:11,00
If you got lost understanding the subjects

6
00:00:11,00 --> 00:00:12,60
we've explored in this course,

7
00:00:12,60 --> 00:00:14,00
we have several basic courses

8
00:00:14,00 --> 00:00:16,50
to help you gain more experience with React.

9
00:00:16,50 --> 00:00:19,70
Simply do a search under the term React.

10
00:00:19,70 --> 00:00:22,20
Equally, if the subject of state management

11
00:00:22,20 --> 00:00:24,20
is something you would like to dig deeper,

12
00:00:24,20 --> 00:00:27,60
find courses on Redux, MobX in our library,

13
00:00:27,60 --> 00:00:29,10
and this will give you a great overview

14
00:00:29,10 --> 00:00:31,60
of the major options available for React.

15
00:00:31,60 --> 00:00:33,80
In any case, I would highly recommend

16
00:00:33,80 --> 00:00:36,40
to continue practice and learn about React

17
00:00:36,40 --> 00:00:39,00
by exploring the documentation as well.

18
00:00:39,00 --> 00:00:40,20
Thanks for taking my course,

19
00:00:40,20 --> 00:00:42,00
and I'll see you in a bit.

