1
00:00:00,50 --> 00:00:01,90
- [Narrator] Okay, so now let's explore

2
00:00:01,90 --> 00:00:03,90
how you would add multiple contexts

3
00:00:03,90 --> 00:00:06,00
inside of one application.

4
00:00:06,00 --> 00:00:07,50
And, you're going to find it very simple

5
00:00:07,50 --> 00:00:09,20
because it's very much the same way

6
00:00:09,20 --> 00:00:10,50
that we've done so far.

7
00:00:10,50 --> 00:00:12,80
It's basically creating more contexts and then

8
00:00:12,80 --> 00:00:15,90
passing the right value to those contexts.

9
00:00:15,90 --> 00:00:17,80
So, let's create another context

10
00:00:17,80 --> 00:00:19,50
that we're going to call Theme.

11
00:00:19,50 --> 00:00:22,30
So, let's copy that here.

12
00:00:22,30 --> 00:00:26,00
And then, let's call this guy Theme.

13
00:00:26,00 --> 00:00:28,70
And now, we create a new context so we have two

14
00:00:28,70 --> 00:00:31,50
contexts inside of the same application.

15
00:00:31,50 --> 00:00:33,70
And then let's export another consumer.

16
00:00:33,70 --> 00:00:37,10
So let's go and copy line seven

17
00:00:37,10 --> 00:00:39,20
and paste it.

18
00:00:39,20 --> 00:00:44,30
And then, let's call this guy Theme Consumer.

19
00:00:44,30 --> 00:00:48,00
And leverage theme here.

20
00:00:48,00 --> 00:00:50,60
So now we've created a second context.

21
00:00:50,60 --> 00:00:52,60
So what we're going to go do now is create some data

22
00:00:52,60 --> 00:00:56,20
we can pass to that second context.

23
00:00:56,20 --> 00:00:58,30
So what I'm going to do here, is create a simple

24
00:00:58,30 --> 00:01:01,20
styles object inside of my state.

25
00:01:01,20 --> 00:01:04,30
Let's go ahead and create that.

26
00:01:04,30 --> 00:01:07,90
And, basically we're going to do display,

27
00:01:07,90 --> 00:01:11,50
and do flex.

28
00:01:11,50 --> 00:01:14,50
And now what we need to do is do like we've done

29
00:01:14,50 --> 00:01:16,60
here, so user data provider.

30
00:01:16,60 --> 00:01:19,40
We can basically create a second one

31
00:01:19,40 --> 00:01:21,90
inside of this component here.

32
00:01:21,90 --> 00:01:24,60
So what we're going to do here is,

33
00:01:24,60 --> 00:01:29,90
go ahead and do theme provider.

34
00:01:29,90 --> 00:01:32,60
So, dot provider.

35
00:01:32,60 --> 00:01:34,90
And then, let's open and close.

36
00:01:34,90 --> 00:01:39,30
Woops, it's provider, not provide.

37
00:01:39,30 --> 00:01:42,10
So then let's make sure that we are

38
00:01:42,10 --> 00:01:45,80
inserting this guy here, inside of our

39
00:01:45,80 --> 00:01:50,20
theme provider, like so.

40
00:01:50,20 --> 00:01:52,10
Alright, so now we're passing the news

41
00:01:52,10 --> 00:01:54,50
to this guy, so we're not going to change that.

42
00:01:54,50 --> 00:01:56,70
I still want to have the top-down approach

43
00:01:56,70 --> 00:01:59,00
for this particular component here.

44
00:01:59,00 --> 00:02:02,00
So, basically we're passing the state dot news.

45
00:02:02,00 --> 00:02:05,10
We're leveraging it to make a fetch here

46
00:02:05,10 --> 00:02:06,80
with this information.

47
00:02:06,80 --> 00:02:08,40
And then returning the new single

48
00:02:08,40 --> 00:02:10,20
with a map method.

49
00:02:10,20 --> 00:02:12,90
So now what we need to do is pass the values

50
00:02:12,90 --> 00:02:15,10
that we want to our provider.

51
00:02:15,10 --> 00:02:19,10
So what we're going to do is value,

52
00:02:19,10 --> 00:02:23,80
then we're going to pass this dot state.

53
00:02:23,80 --> 00:02:25,60
Perfect.

54
00:02:25,60 --> 00:02:28,00
So, what we're going to do is continue working

55
00:02:28,00 --> 00:02:31,10
on this in the next video and actually

56
00:02:31,10 --> 00:02:33,00
challenge you to complete it.

57
00:02:33,00 --> 00:02:40,00
So let's save this for now, and lets move on.

