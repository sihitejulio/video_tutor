1
00:00:00,10 --> 00:00:05,00
(upbeat techno music)

2
00:00:05,00 --> 00:00:06,90
- [Male Voiceover] Okay so now that we have created

3
00:00:06,90 --> 00:00:10,20
our second provider here, let's challenge you

4
00:00:10,20 --> 00:00:13,80
to continue and complete this by yourself.

5
00:00:13,80 --> 00:00:16,10
So if you want to get a reference go back to

6
00:00:16,10 --> 00:00:18,60
new single and make sure that you take a look at

7
00:00:18,60 --> 00:00:22,00
what code we added when we actually did the consumer.

8
00:00:22,00 --> 00:00:25,80
So I want you to do the same thing here for the new single.

9
00:00:25,80 --> 00:00:28,80
The only thing that I'm going to do beforehand

10
00:00:28,80 --> 00:00:31,80
to kind of give you a hand, because that's the one area

11
00:00:31,80 --> 00:00:33,90
that really doesn't matter for this course

12
00:00:33,90 --> 00:00:36,20
but I still want to add it here.

13
00:00:36,20 --> 00:00:41,70
Let's remove the class name here and then let's add style

14
00:00:41,70 --> 00:00:46,70
equals and then pass flex

15
00:00:46,70 --> 00:00:50,50
one by itself, so basically what I've done here is

16
00:00:50,50 --> 00:00:53,50
prepped this internal component here

17
00:00:53,50 --> 00:00:58,40
for the styles that we've passed or we've created here.

18
00:00:58,40 --> 00:01:01,40
But I want you to complete the consumer

19
00:01:01,40 --> 00:01:03,60
in the news by yourself,

20
00:01:03,60 --> 00:01:05,90
so just to test yourself to make sure that you

21
00:01:05,90 --> 00:01:08,20
understood what we needed to do and

22
00:01:08,20 --> 00:01:10,10
what is the syntax that you need to add

23
00:01:10,10 --> 00:01:11,80
to complete the consumer.

24
00:01:11,80 --> 00:01:13,50
So I'm going to leave you guys to this

25
00:01:13,50 --> 00:01:17,00
and we'll explore the solution in the next video.

