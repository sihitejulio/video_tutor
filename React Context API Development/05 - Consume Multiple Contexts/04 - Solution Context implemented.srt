1
00:00:00,00 --> 00:00:05,10
(upbeat music)

2
00:00:05,10 --> 00:00:08,00
- Alright so now let's take a look at the solution.

3
00:00:08,00 --> 00:00:09,60
So, if you've done this properly,

4
00:00:09,60 --> 00:00:11,30
this is what it should look like.

5
00:00:11,30 --> 00:00:13,10
At least, not the same articles,

6
00:00:13,10 --> 00:00:15,00
but at least the way it looks.

7
00:00:15,00 --> 00:00:16,80
So one after the other.

8
00:00:16,80 --> 00:00:19,30
Again, this is not a course on CSS style

9
00:00:19,30 --> 00:00:22,40
so it doesn't really matter if this is too big,

10
00:00:22,40 --> 00:00:23,30
or so on so forth,

11
00:00:23,30 --> 00:00:25,80
but I just wanted you to pass a value

12
00:00:25,80 --> 00:00:27,90
to the consumer and leverage the consumer

13
00:00:27,90 --> 00:00:31,40
to make the styles at least look better than it was before.

14
00:00:31,40 --> 00:00:34,40
So if you want to really take a look how it works

15
00:00:34,40 --> 00:00:37,00
Flexbox actually shrinks our cards here

16
00:00:37,00 --> 00:00:39,40
as we move our browser.

17
00:00:39,40 --> 00:00:41,90
Alright, so let's close this,

18
00:00:41,90 --> 00:00:43,40
or minimize this,

19
00:00:43,40 --> 00:00:44,80
and let's take a look at the solution

20
00:00:44,80 --> 00:00:49,00
and what you had to add inside of that particular component.

21
00:00:49,00 --> 00:00:51,50
So again, let's just go back to App.js.

22
00:00:51,50 --> 00:00:53,90
We had some styles here that we created.

23
00:00:53,90 --> 00:00:56,70
We created a Provider at the top here,

24
00:00:56,70 --> 00:00:58,20
second Provider,

25
00:00:58,20 --> 00:01:01,50
so Theme, and the ThemeConsumer,

26
00:01:01,50 --> 00:01:06,10
and then we wrapped our news here, or news component

27
00:01:06,10 --> 00:01:10,30
with the Theme Provider and pass this.state as a value.

28
00:01:10,30 --> 00:01:14,20
Alright now that we're going back to here in news,

29
00:01:14,20 --> 00:01:16,70
we need to import a ThemeConsumer,

30
00:01:16,70 --> 00:01:20,00
to be able to leverage it inside of that component.

31
00:01:20,00 --> 00:01:22,50
When we actually have it imported,

32
00:01:22,50 --> 00:01:25,80
then we go back here and we do exactly the same

33
00:01:25,80 --> 00:01:28,20
as we've done on the NewSingle,

34
00:01:28,20 --> 00:01:30,70
so we wrap first the Consumer,

35
00:01:30,70 --> 00:01:33,10
and then we do the expression here,

36
00:01:33,10 --> 00:01:37,30
so we do the function before we wrap all the rest

37
00:01:37,30 --> 00:01:41,10
of the code that the component inside of the function.

38
00:01:41,10 --> 00:01:43,30
So you need to do the exact same thing here.

39
00:01:43,30 --> 00:01:45,50
So we grab our ThemeConsumer,

40
00:01:45,50 --> 00:01:49,00
and then we do the the expression where we pass the styles

41
00:01:49,00 --> 00:01:51,20
and then I just create an extra div

42
00:01:51,20 --> 00:01:53,60
to make sure that we pass the style to it

43
00:01:53,60 --> 00:01:55,00
and then we have a NewSingle

44
00:01:55,00 --> 00:01:57,50
that will be repeated a couple of times.

45
00:01:57,50 --> 00:01:59,90
Now the safer way to do this would be normally

46
00:01:59,90 --> 00:02:02,90
to apply the syle of Flexbox outside

47
00:02:02,90 --> 00:02:04,90
of this return statement,

48
00:02:04,90 --> 00:02:08,40
because we are returning these styles multiple times

49
00:02:08,40 --> 00:02:12,00
but this is just an exercise to pass the Consumers

50
00:02:12,00 --> 00:02:16,00
so again that's not the purpose of this exercise

51
00:02:16,00 --> 00:02:17,90
really what we trying to do is understand how

52
00:02:17,90 --> 00:02:19,20
to pass the Consumer.

53
00:02:19,20 --> 00:02:21,90
So once you have done this, you have access

54
00:02:21,90 --> 00:02:24,90
to the styles and then you can apply them here.

55
00:02:24,90 --> 00:02:27,40
So this is pretty much how you would do it

56
00:02:27,40 --> 00:02:31,40
and pass multiple context inside of you application.

57
00:02:31,40 --> 00:02:33,60
Now if we go back here,

58
00:02:33,60 --> 00:02:34,50
to the application,

59
00:02:34,50 --> 00:02:38,00
and we open our Developer tools

60
00:02:38,00 --> 00:02:39,80
and go to...

61
00:02:39,80 --> 00:02:43,00
React,

62
00:02:43,00 --> 00:02:44,80
we have multiple content provider.

63
00:02:44,80 --> 00:02:46,00
We have one here

64
00:02:46,00 --> 00:02:48,30
where we have these values pass

65
00:02:48,30 --> 00:02:50,00
and then we have another one here,

66
00:02:50,00 --> 00:02:53,10
where we have these values pass.

67
00:02:53,10 --> 00:02:56,10
So now we can leverage this content provider here

68
00:02:56,10 --> 00:02:59,50
and then leverage this content provider here.

69
00:02:59,50 --> 00:03:02,50
And this is pretty much how you would the context.

70
00:03:02,50 --> 00:03:05,20
So hopefully this course help you understand really how

71
00:03:05,20 --> 00:03:06,70
to work with context,

72
00:03:06,70 --> 00:03:08,50
how to update the context,

73
00:03:08,50 --> 00:03:12,00
how to work with multiple context and what to avoid.

