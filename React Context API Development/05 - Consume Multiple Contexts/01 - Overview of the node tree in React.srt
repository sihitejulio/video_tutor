1
00:00:00,50 --> 00:00:02,70
- [Instructor] Now let's quickly explore how you would

2
00:00:02,70 --> 00:00:05,60
explore the node tree and understand what's being passed

3
00:00:05,60 --> 00:00:09,40
from the context to what component and so on, so forth.

4
00:00:09,40 --> 00:00:11,40
This is really important when you start working

5
00:00:11,40 --> 00:00:13,50
with React because if you don't understand how

6
00:00:13,50 --> 00:00:16,80
your node tree is being passed, or how to inspect it

7
00:00:16,80 --> 00:00:19,80
you're going to find yourself doing a lot of code

8
00:00:19,80 --> 00:00:21,70
and not understanding what's happening.

9
00:00:21,70 --> 00:00:23,90
and why you're having some errors,

10
00:00:23,90 --> 00:00:25,90
and how to debug your application.

11
00:00:25,90 --> 00:00:27,60
This is really important,

12
00:00:27,60 --> 00:00:30,20
so the first thing you need to do in order to do this,

13
00:00:30,20 --> 00:00:32,70
is to have the React Devtools,

14
00:00:32,70 --> 00:00:34,90
as you saw me inspect the application

15
00:00:34,90 --> 00:00:37,90
with the React Devtools, this is really important

16
00:00:37,90 --> 00:00:40,70
when you explore your node tree and try to understand

17
00:00:40,70 --> 00:00:42,90
what's being passed from one to the other.

18
00:00:42,90 --> 00:00:49,00
So let's go ahead to GitHub.com/Facebook/React-Devtools

19
00:00:49,00 --> 00:00:51,70
and then scroll all the way down

20
00:00:51,70 --> 00:00:56,80
to the pre-packaged installation, so if you are on Chrome

21
00:00:56,80 --> 00:00:58,30
install the Chrome extension,

22
00:00:58,30 --> 00:01:02,10
if you're on Firefox, install the Firefox extension

23
00:01:02,10 --> 00:01:04,60
and if you want the standalone app which is available

24
00:01:04,60 --> 00:01:07,30
for React Native and others you can install

25
00:01:07,30 --> 00:01:08,70
this one as well.

26
00:01:08,70 --> 00:01:10,70
So once you have that installed, you should be able

27
00:01:10,70 --> 00:01:13,90
to see a React tab, so I'm on Chrome right now

28
00:01:13,90 --> 00:01:16,60
and you should be able to see this here,

29
00:01:16,60 --> 00:01:19,30
and if you're not seeing it, it's because your application

30
00:01:19,30 --> 00:01:23,10
isn't even started or there's a bug in your application.

31
00:01:23,10 --> 00:01:26,00
So once you fix the bug, or once your application is started

32
00:01:26,00 --> 00:01:27,60
you're going to see this.

33
00:01:27,60 --> 00:01:30,90
At every node and component you should see the state

34
00:01:30,90 --> 00:01:34,30
or the props that are being passed to that specific area.

35
00:01:34,30 --> 00:01:36,30
So here we're going to see the children,

36
00:01:36,30 --> 00:01:39,30
and you see the props the functions that are available

37
00:01:39,30 --> 00:01:41,90
and the values that we've passed here,

38
00:01:41,90 --> 00:01:45,80
and then you can continue scrolling down to the container

39
00:01:45,80 --> 00:01:49,20
and every time you switch another component

40
00:01:49,20 --> 00:01:51,50
you're going to see what's available there.

41
00:01:51,50 --> 00:01:53,90
So if you're passing properties from one component

42
00:01:53,90 --> 00:01:56,60
to the other, so if you're using the typical

43
00:01:56,60 --> 00:02:00,30
top-down approach and you're not seeing something appear

44
00:02:00,30 --> 00:02:01,80
or you're not seeing your application

45
00:02:01,80 --> 00:02:03,30
react the way it should.

46
00:02:03,30 --> 00:02:07,20
Then what you should do is inspect at the component level.

47
00:02:07,20 --> 00:02:10,80
Okay so let's go down here so I'm seeing Manny Henri

48
00:02:10,80 --> 00:02:12,60
I'm going to click on it I see someone else.

49
00:02:12,60 --> 00:02:17,40
Alright so let's click here in this particular item here

50
00:02:17,40 --> 00:02:19,10
and let's inspect what's in here.

51
00:02:19,10 --> 00:02:23,40
So I have these items here, and this is all the stuff

52
00:02:23,40 --> 00:02:25,70
that I have available, if you wouldn't see this

53
00:02:25,70 --> 00:02:27,80
in this particular item here.

54
00:02:27,80 --> 00:02:30,30
That means that something isn't done properly

55
00:02:30,30 --> 00:02:33,80
from your top to bottom, the same thing goes if you want to

56
00:02:33,80 --> 00:02:36,30
check the props that are being passed at the top here

57
00:02:36,30 --> 00:02:40,00
so here we have for example the class name role

58
00:02:40,00 --> 00:02:42,50
that's being passed because of materialize.

59
00:02:42,50 --> 00:02:46,80
If we click on news here, we have this prop available.

60
00:02:46,80 --> 00:02:51,10
So we have this query and the type which is being passed

61
00:02:51,10 --> 00:02:53,90
from the application so if we go back to the application

62
00:02:53,90 --> 00:02:58,60
quickly we're passing here in the news,

63
00:02:58,60 --> 00:03:02,10
from this .state news we're passing these items here.

64
00:03:02,10 --> 00:03:05,30
So if we go back here, we have our query

65
00:03:05,30 --> 00:03:07,20
and we have our type available here,

66
00:03:07,20 --> 00:03:12,00
and then we're doing a call to fetching all these news,

67
00:03:12,00 --> 00:03:13,60
and so on so forth.

68
00:03:13,60 --> 00:03:16,30
So this is how you would explore a node tree

69
00:03:16,30 --> 00:03:18,80
and try to figure out what's happening in your application

70
00:03:18,80 --> 00:03:21,00
while you work with it, so if something is not

71
00:03:21,00 --> 00:03:23,90
passed properly, or if there's a bug in your application

72
00:03:23,90 --> 00:03:25,80
you can understand where it stops.

73
00:03:25,80 --> 00:03:30,20
So if you're passing props from the app here,

74
00:03:30,20 --> 00:03:31,70
you have access to this state,

75
00:03:31,70 --> 00:03:34,50
and you're passing togglename here

76
00:03:34,50 --> 00:03:36,80
and as you scroll down to togglename.

77
00:03:36,80 --> 00:03:39,40
So let's go back to that particular component,

78
00:03:39,40 --> 00:03:43,10
where we have togglename in the NewSingles

79
00:03:43,10 --> 00:03:52,20
somewhere here, and you click on the children.

80
00:03:52,20 --> 00:03:54,10
Let's go down until we find that guy,

81
00:03:54,10 --> 00:03:57,00
there you go on click and you're not seeing

82
00:03:57,00 --> 00:04:01,00
the togglename available here from the context

83
00:04:01,00 --> 00:04:02,60
then you have a problem.

84
00:04:02,60 --> 00:04:04,80
So if you would be clicking here

85
00:04:04,80 --> 00:04:07,50
and nothing would change then there's a little

86
00:04:07,50 --> 00:04:10,10
problem somewhere, so you need to figure out why

87
00:04:10,10 --> 00:04:12,90
there's a problem and where it is,

88
00:04:12,90 --> 00:04:16,00
and the best way to do this is to use the React Devtools

89
00:04:16,00 --> 00:04:18,30
and go through the node tree here

90
00:04:18,30 --> 00:04:20,10
and figure out where the issues are.

91
00:04:20,10 --> 00:04:21,80
There's another way to go through your components

92
00:04:21,80 --> 00:04:24,60
very quickly at the bottom here so you can click

93
00:04:24,60 --> 00:04:27,60
on NewSingle like that so this goes a little bit faster

94
00:04:27,60 --> 00:04:31,20
if you want to go through all of the stuff that you have

95
00:04:31,20 --> 00:04:33,60
in here so as you can see here we have the values that

96
00:04:33,60 --> 00:04:35,50
are being passed to togglename here.

97
00:04:35,50 --> 00:04:39,30
So we have it available down here as well

98
00:04:39,30 --> 00:04:40,50
and so on, so forth.

99
00:04:40,50 --> 00:04:44,30
So this is how I would go and inspect my components

100
00:04:44,30 --> 00:04:48,80
if I had any issues, and this is how we're going to figure out

101
00:04:48,80 --> 00:04:51,90
well now we're going to be passing multiple contexts

102
00:04:51,90 --> 00:04:54,50
through our application, if something is not

103
00:04:54,50 --> 00:04:57,70
being passed here you can go back to the React Devtools

104
00:04:57,70 --> 00:05:00,20
and figure out and check the context provider here.

105
00:05:00,20 --> 00:05:03,00
Okay so this one is passing these items here,

106
00:05:03,00 --> 00:05:06,40
I should have these available somewhere in here

107
00:05:06,40 --> 00:05:08,00
and so on so forth.

108
00:05:08,00 --> 00:05:10,60
So this is how I would inspect my application

109
00:05:10,60 --> 00:05:12,30
as I work through it.

110
00:05:12,30 --> 00:05:14,70
Alright so now that we have an understanding of how

111
00:05:14,70 --> 00:05:17,60
we can inspect our application, let's go head

112
00:05:17,60 --> 00:05:19,90
and add multiple contexts to our application

113
00:05:19,90 --> 00:05:23,40
and we're going to add a style or a theme context

114
00:05:23,40 --> 00:05:25,70
so we can change the styles on this application,

115
00:05:25,70 --> 00:05:28,10
'cause I really don't like the way it looks right now.

116
00:05:28,10 --> 00:05:30,40
So we're going to change the styles by passing

117
00:05:30,40 --> 00:05:33,50
a second context with some styles in it

118
00:05:33,50 --> 00:05:36,40
and then leverage that with a consumer somewhere

119
00:05:36,40 --> 00:05:39,00
in the application, so let's move on.

