1
00:00:00,30 --> 00:00:01,80
- When users visit websites,

2
00:00:01,80 --> 00:00:05,10
each receives a slightly different experience

3
00:00:05,10 --> 00:00:08,50
depending on the data stored by those websites.

4
00:00:08,50 --> 00:00:11,60
Web developers, especially backend specialists,

5
00:00:11,60 --> 00:00:14,90
need to be able to work with databases.

6
00:00:14,90 --> 00:00:17,70
To do this they'll need to know data languages

7
00:00:17,70 --> 00:00:19,90
like Structured Query Language

8
00:00:19,90 --> 00:00:22,10
which is also called SQL.

9
00:00:22,10 --> 00:00:24,00
And this is a popular way to store

10
00:00:24,00 --> 00:00:25,60
and retrieve information.

11
00:00:25,60 --> 00:00:30,40
Now you may hear SQL pronounced as sequel.

12
00:00:30,40 --> 00:00:33,40
There are many variants but they're pretty similar.

13
00:00:33,40 --> 00:00:35,60
There's another important category

14
00:00:35,60 --> 00:00:38,80
of databases that you should be familiar with.

15
00:00:38,80 --> 00:00:41,80
They're called NoSQL, and you can tell by the name

16
00:00:41,80 --> 00:00:44,30
that they're very different than SQL.

17
00:00:44,30 --> 00:00:47,40
Now sequel databases have clearly defined structures

18
00:00:47,40 --> 00:00:51,20
which is sometimes referred to as the schema.

19
00:00:51,20 --> 00:00:56,00
In contrast, NoSQL databases are the hippies of databases.

20
00:00:56,00 --> 00:00:57,70
They have like no pre-defined structure

21
00:00:57,70 --> 00:01:01,30
and store information in dynamic schemas.

22
00:01:01,30 --> 00:01:03,70
I know if you're an organized person like me,

23
00:01:03,70 --> 00:01:05,90
you're probably thinking how can all

24
00:01:05,90 --> 00:01:08,60
this disorder be any good?

25
00:01:08,60 --> 00:01:11,60
NoSQL databases can actually be faster

26
00:01:11,60 --> 00:01:14,40
and more efficient because some of that

27
00:01:14,40 --> 00:01:17,60
structure in sequel ends up as overhead

28
00:01:17,60 --> 00:01:19,30
and makes them run slower.

29
00:01:19,30 --> 00:01:21,20
Now on the other side of databases,

30
00:01:21,20 --> 00:01:24,00
you need a language that can take the data

31
00:01:24,00 --> 00:01:28,60
and merge it with templates to display customized info.

32
00:01:28,60 --> 00:01:31,30
They are languages like PHP, Node.js,

33
00:01:31,30 --> 00:01:34,60
.NET and Ruby on Rails that combine templates

34
00:01:34,60 --> 00:01:39,20
with data and build custom pages for each user.

35
00:01:39,20 --> 00:01:41,10
The set of tools that each company uses

36
00:01:41,10 --> 00:01:43,70
is often called the Stack

37
00:01:43,70 --> 00:01:47,40
and it varies sometimes even between different projects.

38
00:01:47,40 --> 00:01:51,00
Some stacks can become their own specialized platforms.

39
00:01:51,00 --> 00:01:53,90
The most famous of these is WordPress.

40
00:01:53,90 --> 00:01:56,60
Which started out as a blogging platform

41
00:01:56,60 --> 00:02:01,10
but has grown to let you build full website infrastructures.

42
00:02:01,10 --> 00:02:03,10
New web developers need to study

43
00:02:03,10 --> 00:02:06,40
at least one structured database language.

44
00:02:06,40 --> 00:02:09,70
One NoSQL language and at least one language

45
00:02:09,70 --> 00:02:12,30
for merging that with templates.

46
00:02:12,30 --> 00:02:14,50
Now this is one place where your choices

47
00:02:14,50 --> 00:02:16,70
can affect where they work.

48
00:02:16,70 --> 00:02:18,90
I suggest starting with PHP

49
00:02:18,90 --> 00:02:21,50
and then explore some of the other options

50
00:02:21,50 --> 00:02:25,70
like Node.js, .NET, or Ruby on Rails.

51
00:02:25,70 --> 00:02:27,80
Also WordPress is so popular that

52
00:02:27,80 --> 00:02:30,80
it powers 30% of the web.

53
00:02:30,80 --> 00:02:33,10
So you should at least know how to set it up.

54
00:02:33,10 --> 00:02:35,00
Now entire businesses are built

55
00:02:35,00 --> 00:02:38,00
around the WordPress stack.

56
00:02:38,00 --> 00:02:39,40
It pays to be flexible when you're

57
00:02:39,40 --> 00:02:42,10
starting out but once you make some decisions

58
00:02:42,10 --> 00:02:44,70
it will pay even more to develop

59
00:02:44,70 --> 00:02:48,00
deep knowledge in one environment.

