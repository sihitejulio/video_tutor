1
00:00:00,70 --> 00:00:04,20
- Frameworks and libraries help you build projects quicker

2
00:00:04,20 --> 00:00:06,40
by letting you write code in a language

3
00:00:06,40 --> 00:00:10,30
that adds superpowers to the way you build projects.

4
00:00:10,30 --> 00:00:13,40
Now there are libraries that extend the capabilities

5
00:00:13,40 --> 00:00:16,00
of existing languages, like Lodash,

6
00:00:16,00 --> 00:00:19,70
which helps you manage objects and arrays more easily.

7
00:00:19,70 --> 00:00:22,70
Now because they've been battle tested over time,

8
00:00:22,70 --> 00:00:26,40
these tools use some of the industry's best practices,

9
00:00:26,40 --> 00:00:28,00
so they can also help you learn

10
00:00:28,00 --> 00:00:30,90
how professional developers do things.

11
00:00:30,90 --> 00:00:34,50
There are popular tools that make working with data simple.

12
00:00:34,50 --> 00:00:37,80
The D3 library can help you create interactive charts

13
00:00:37,80 --> 00:00:41,50
by combining data with a structured language.

14
00:00:41,50 --> 00:00:44,80
There are also some older libraries that are very common,

15
00:00:44,80 --> 00:00:46,00
like jQuery.

16
00:00:46,00 --> 00:00:48,80
It's one of the most popular libraries in use today,

17
00:00:48,80 --> 00:00:51,20
but it's starting to show its age.

18
00:00:51,20 --> 00:00:53,70
jQuery gives you easy access to the DOM

19
00:00:53,70 --> 00:00:55,90
and it's still something front end developers

20
00:00:55,90 --> 00:00:58,60
should be comfortable using.

21
00:00:58,60 --> 00:01:01,40
Some frameworks make it easier to build websites,

22
00:01:01,40 --> 00:01:03,70
like the famous Boostrap framework.

23
00:01:03,70 --> 00:01:07,90
It's just a way to write HTML that adds popular components,

24
00:01:07,90 --> 00:01:12,00
like carousels, modals, and cards.

25
00:01:12,00 --> 00:01:15,00
By adding just a few classes in your HTML,

26
00:01:15,00 --> 00:01:17,90
you get components with pre-written JavaScript

27
00:01:17,90 --> 00:01:20,40
to handle common interactivity.

28
00:01:20,40 --> 00:01:22,70
Now there are plenty of similar frameworks,

29
00:01:22,70 --> 00:01:25,60
like Material Design and Foundation,

30
00:01:25,60 --> 00:01:30,00
but Bootstrap is both the oldest and most popular.

31
00:01:30,00 --> 00:01:31,80
There are also frameworks that help you write

32
00:01:31,80 --> 00:01:35,00
in a language that translates to JavaScript.

33
00:01:35,00 --> 00:01:39,70
These are frameworks like Angular, React, and Vue.js.

34
00:01:39,70 --> 00:01:42,50
They're so popular that a lot of companies

35
00:01:42,50 --> 00:01:44,70
will hire developers who specialize

36
00:01:44,70 --> 00:01:46,90
in one of these frameworks.

37
00:01:46,90 --> 00:01:49,60
Angular is the oldest and it's so large

38
00:01:49,60 --> 00:01:52,70
that it's considered a platform.

39
00:01:52,70 --> 00:01:54,40
Next was React,

40
00:01:54,40 --> 00:01:57,90
which is currently the most popular framework.

41
00:01:57,90 --> 00:02:00,50
React was designed to create interfaces,

42
00:02:00,50 --> 00:02:03,20
so it's less comprehensive than Angular,

43
00:02:03,20 --> 00:02:05,30
but it brought important concepts

44
00:02:05,30 --> 00:02:10,00
like a component based architecture and the virtual DOM,

45
00:02:10,00 --> 00:02:13,00
which manages changes in the pages for you.

46
00:02:13,00 --> 00:02:15,80
Now Vue is the newest kid on the block,

47
00:02:15,80 --> 00:02:20,20
but it's by far the easiest to learn and implement.

48
00:02:20,20 --> 00:02:23,10
It's similar to React, but one of it's main benefits

49
00:02:23,10 --> 00:02:25,90
is that it can be installed in a project

50
00:02:25,90 --> 00:02:28,70
using a simple script hack.

51
00:02:28,70 --> 00:02:30,90
Now to be a marketable developer,

52
00:02:30,90 --> 00:02:32,80
you should be comfortable with at least

53
00:02:32,80 --> 00:02:34,40
one of these frameworks.

54
00:02:34,40 --> 00:02:35,40
If you know one,

55
00:02:35,40 --> 00:02:39,00
you can easily move your skills to the others.

