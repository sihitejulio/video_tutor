1
00:00:00,90 --> 00:00:04,30
- A developer doesn't always build an entire project,

2
00:00:04,30 --> 00:00:09,70
but often uses or creates a number of services and APIs.

3
00:00:09,70 --> 00:00:13,00
A service is a tool offered by a server

4
00:00:13,00 --> 00:00:16,00
that provides access to some information.

5
00:00:16,00 --> 00:00:18,10
Developers often create these

6
00:00:18,10 --> 00:00:20,80
to simplify the sharing of data.

7
00:00:20,80 --> 00:00:24,00
Now this is how some sites allow you to log in

8
00:00:24,00 --> 00:00:27,80
through your Facebook, Google, or Twitter accounts.

9
00:00:27,80 --> 00:00:31,80
And this is made possible by something called an API,

10
00:00:31,80 --> 00:00:34,40
or application programming interface.

11
00:00:34,40 --> 00:00:36,00
These are small applications

12
00:00:36,00 --> 00:00:40,90
used to formalize the way that the information is requested.

13
00:00:40,90 --> 00:00:44,00
Most APIs are considered restful,

14
00:00:44,00 --> 00:00:49,10
which is a service based on the hypertext transfer protocol.

15
00:00:49,10 --> 00:00:53,60
Restful services used HTTP verbs like GET and POST

16
00:00:53,60 --> 00:00:58,10
to retrieve and send information to manage that data.

17
00:00:58,10 --> 00:01:02,50
The requests are usually returned in a format called JSON,

18
00:01:02,50 --> 00:01:05,10
or JavaScript Object Notation.

19
00:01:05,10 --> 00:01:06,70
That's a text-based format

20
00:01:06,70 --> 00:01:10,00
that easily converts into JavaScript objects.

21
00:01:10,00 --> 00:01:11,40
Now as a developer,

22
00:01:11,40 --> 00:01:13,60
you'll need to be comfortable with using

23
00:01:13,60 --> 00:01:17,90
and managing data from a service like REST.

24
00:01:17,90 --> 00:01:19,10
More advanced users

25
00:01:19,10 --> 00:01:23,10
will create their own web services and APIs.

26
00:01:23,10 --> 00:01:26,80
Making sure that a request comes from an authorized source

27
00:01:26,80 --> 00:01:29,60
is handled by standards like OAuth.

28
00:01:29,60 --> 00:01:32,40
Now OAuth can act like an intermediary

29
00:01:32,40 --> 00:01:35,50
so that requests can be securely managed

30
00:01:35,50 --> 00:01:38,20
without having to constantly share passwords

31
00:01:38,20 --> 00:01:40,90
between two systems.

32
00:01:40,90 --> 00:01:43,80
That's why you can tell an application like LinkedIn

33
00:01:43,80 --> 00:01:45,80
to post to your Twitter feed.

34
00:01:45,80 --> 00:01:47,90
Once you've authorized your LinkedIn account

35
00:01:47,90 --> 00:01:49,10
to post to Twitter,

36
00:01:49,10 --> 00:01:52,10
the two services share a secret token,

37
00:01:52,10 --> 00:01:54,60
and that makes communication easier.

38
00:01:54,60 --> 00:01:57,10
Now learning to work with APIs and services

39
00:01:57,10 --> 00:01:59,80
is a critical skill for developers.

40
00:01:59,80 --> 00:02:02,90
How much you need to learn depends on the type of developer

41
00:02:02,90 --> 00:02:05,00
you want to be.

