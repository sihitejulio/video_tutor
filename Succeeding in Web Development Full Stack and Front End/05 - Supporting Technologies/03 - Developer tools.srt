1
00:00:00,70 --> 00:00:04,20
- Developers use a number of tools to get their work done,

2
00:00:04,20 --> 00:00:08,40
and the lightsaver of development tools is your code editor.

3
00:00:08,40 --> 00:00:10,40
In recent years, Visual Studio code

4
00:00:10,40 --> 00:00:14,50
has become the most popular tool for coding web projects.

5
00:00:14,50 --> 00:00:16,80
But there are tons of other options.

6
00:00:16,80 --> 00:00:19,90
Now some popular options include Adam from GitHub,

7
00:00:19,90 --> 00:00:22,90
as well as Sublime Text.

8
00:00:22,90 --> 00:00:26,10
Code editors are sometimes called IDEs

9
00:00:26,10 --> 00:00:28,60
or Integrated Development Environments.

10
00:00:28,60 --> 00:00:32,40
Now, often these terms can be used interchangeably

11
00:00:32,40 --> 00:00:36,00
but the primary difference between a standard code editor

12
00:00:36,00 --> 00:00:40,80
and an IDEs is that IDEs tend to be more structured,

13
00:00:40,80 --> 00:00:43,50
so that things are done in a specific way

14
00:00:43,50 --> 00:00:46,90
with lots of shortcuts that help you program faster,

15
00:00:46,90 --> 00:00:51,40
as well as hints for developing and structuring your code.

16
00:00:51,40 --> 00:00:56,20
Also, an IDE is usually meant for more than web languages.

17
00:00:56,20 --> 00:01:00,30
You can really see this in the difference between an IDE

18
00:01:00,30 --> 00:01:03,30
like Visual Studio and a basic code editor

19
00:01:03,30 --> 00:01:05,20
like Visual Studio code.

20
00:01:05,20 --> 00:01:07,70
So let's take a look at some of the features

21
00:01:07,70 --> 00:01:09,80
that you should be looking for.

22
00:01:09,80 --> 00:01:12,70
One of the main advantages of certain editors

23
00:01:12,70 --> 00:01:16,70
is in their ability to guess the language you're working on,

24
00:01:16,70 --> 00:01:19,90
and provide assistance in writing the code.

25
00:01:19,90 --> 00:01:21,70
They can have built in references

26
00:01:21,70 --> 00:01:24,60
that let you look up documentation, for example.

27
00:01:24,60 --> 00:01:27,90
Now, editors can also check and format your code

28
00:01:27,90 --> 00:01:29,80
for potential errors.

29
00:01:29,80 --> 00:01:32,20
It's not good at detecting logical errors

30
00:01:32,20 --> 00:01:35,00
but it can catch formatting issues,

31
00:01:35,00 --> 00:01:38,30
suggest best practices and help your team

32
00:01:38,30 --> 00:01:40,40
use similar styles.

33
00:01:40,40 --> 00:01:42,10
When working with web projects,

34
00:01:42,10 --> 00:01:45,00
you often code in a version of a language

35
00:01:45,00 --> 00:01:47,20
that needs to be transpile.

36
00:01:47,20 --> 00:01:50,20
To do this developer use Build Processes

37
00:01:50,20 --> 00:01:52,80
to automate that conversion.

38
00:01:52,80 --> 00:01:56,50
A Build Process is a set of automation commands

39
00:01:56,50 --> 00:01:59,60
that take care of common tasks.

40
00:01:59,60 --> 00:02:01,40
They include tools like Webpack,

41
00:02:01,40 --> 00:02:04,90
which can bundle your code and removed unused pieces.

42
00:02:04,90 --> 00:02:08,30
Babel, which transpose your code for older browsers

43
00:02:08,30 --> 00:02:11,30
and local servers so you can test the application

44
00:02:11,30 --> 00:02:13,30
on your own machine.

45
00:02:13,30 --> 00:02:15,70
Build Processes make development easier

46
00:02:15,70 --> 00:02:17,70
and can also be a powerful tool

47
00:02:17,70 --> 00:02:20,60
for automating and testing your code.

48
00:02:20,60 --> 00:02:23,20
Learning to take advantage of these Developer Tools

49
00:02:23,20 --> 00:02:24,80
is an important piece

50
00:02:24,80 --> 00:02:28,00
and helping you become a more productive developer.

