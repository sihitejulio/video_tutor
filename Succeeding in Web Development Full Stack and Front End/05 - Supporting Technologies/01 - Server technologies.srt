1
00:00:00,70 --> 00:00:04,60
- One of the most important devices on the web are servers.

2
00:00:04,60 --> 00:00:06,90
Now a server is just a computer

3
00:00:06,90 --> 00:00:10,20
running specialized software that receives requests

4
00:00:10,20 --> 00:00:13,00
and serves them to another device.

5
00:00:13,00 --> 00:00:14,50
And most of the servers don't even

6
00:00:14,50 --> 00:00:16,50
run on physical machines anymore.

7
00:00:16,50 --> 00:00:19,20
They run the software on virtual machines

8
00:00:19,20 --> 00:00:21,60
which live in the cloud.

9
00:00:21,60 --> 00:00:25,20
Now the cloud is a worldwide network of computers

10
00:00:25,20 --> 00:00:27,90
that hosts applications by creating

11
00:00:27,90 --> 00:00:31,80
virtual machines configured, managed, and run

12
00:00:31,80 --> 00:00:35,90
by companies like Microsoft, Amazon, or Google.

13
00:00:35,90 --> 00:00:38,40
And different server applications can live

14
00:00:38,40 --> 00:00:42,00
in the same machine or in different servers.

15
00:00:42,00 --> 00:00:44,20
Applications can specialize in serving

16
00:00:44,20 --> 00:00:47,60
websites, data, delivering streaming services,

17
00:00:47,60 --> 00:00:50,90
and lots of other specialized functionality.

18
00:00:50,90 --> 00:00:52,70
One of the first decisions you'll make

19
00:00:52,70 --> 00:00:54,50
when you need to host a site,

20
00:00:54,50 --> 00:00:56,50
is the address of your site.

21
00:00:56,50 --> 00:00:59,40
So, a site like LinkedIn has to use

22
00:00:59,40 --> 00:01:01,50
a domain name that will be easily

23
00:01:01,50 --> 00:01:04,00
remembered by folks visiting the site.

24
00:01:04,00 --> 00:01:06,40
What you do is purchase a domain name

25
00:01:06,40 --> 00:01:10,80
and then include a TLD or Top-Level Domain

26
00:01:10,80 --> 00:01:15,00
like .com, .org, or .edu.

27
00:01:15,00 --> 00:01:18,10
Now you can also ask for country specific domains

28
00:01:18,10 --> 00:01:22,70
like .ly, .co, and .ca.

29
00:01:22,70 --> 00:01:24,80
In the old days there were only a few

30
00:01:24,80 --> 00:01:27,20
TLDs available for purchase.

31
00:01:27,20 --> 00:01:29,00
But they have recently grown to include

32
00:01:29,00 --> 00:01:30,70
more than a thousand.

33
00:01:30,70 --> 00:01:32,10
So you can include things like

34
00:01:32,10 --> 00:01:36,90
.tech, .basketball, and even .viking.

35
00:01:36,90 --> 00:01:39,40
Cloud services specialize in providing

36
00:01:39,40 --> 00:01:42,40
virtual machines and applications that are

37
00:01:42,40 --> 00:01:44,20
elastic in nature.

38
00:01:44,20 --> 00:01:46,70
So they grow based on your needs.

39
00:01:46,70 --> 00:01:49,50
So you only purchase the services you need.

40
00:01:49,50 --> 00:01:52,30
And as your site grows and you gain more users

41
00:01:52,30 --> 00:01:56,30
your speed, memory and throughput can adjust.

42
00:01:56,30 --> 00:01:59,50
Now there are three major providers for the services

43
00:01:59,50 --> 00:02:03,10
like Google Cloud, Amazon Web Services,

44
00:02:03,10 --> 00:02:05,60
and Azure for Microsoft.

45
00:02:05,60 --> 00:02:08,20
If your site needs to deliver maximum speed,

46
00:02:08,20 --> 00:02:12,90
it really matters how far the customer is from the server.

47
00:02:12,90 --> 00:02:15,70
You can place data closer to your users

48
00:02:15,70 --> 00:02:19,90
by using a Content Delivery Network or CDN.

49
00:02:19,90 --> 00:02:22,30
Now this is going to create copies of your site's

50
00:02:22,30 --> 00:02:26,10
information in multiple places throughout the world

51
00:02:26,10 --> 00:02:28,80
so that users can get to it quicker.

52
00:02:28,80 --> 00:02:31,40
Servers are what makes the web possible.

53
00:02:31,40 --> 00:02:32,60
As a back-end developer,

54
00:02:32,60 --> 00:02:34,80
you will often work on configuring

55
00:02:34,80 --> 00:02:36,70
and getting the most out of them.

56
00:02:36,70 --> 00:02:39,30
But even front-end developers and designers

57
00:02:39,30 --> 00:02:41,40
need to know how to interact

58
00:02:41,40 --> 00:02:45,00
and send files to and from servers.

