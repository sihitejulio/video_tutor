1
00:00:00,70 --> 00:00:02,80
- Let's take a look at the hiring process.

2
00:00:02,80 --> 00:00:06,40
It's good to think about this from a company's perspective.

3
00:00:06,40 --> 00:00:10,00
The first step is to find the right person for the job.

4
00:00:10,00 --> 00:00:13,20
In a bigger company, that means posting the job online

5
00:00:13,20 --> 00:00:16,00
and waiting for applications to arrive.

6
00:00:16,00 --> 00:00:19,30
Now, this step is normally managed by a recruiter

7
00:00:19,30 --> 00:00:21,70
within Human Resources.

8
00:00:21,70 --> 00:00:24,60
A recruiter doesn't necessarily understand everything

9
00:00:24,60 --> 00:00:26,00
it takes to do the job.

10
00:00:26,00 --> 00:00:31,00
Their job is to process applications and narrow them down.

11
00:00:31,00 --> 00:00:33,90
Now, it's important that your profile meets the requirements

12
00:00:33,90 --> 00:00:35,70
of the job but don't worry

13
00:00:35,70 --> 00:00:38,90
if you don't perfectly match all the skills.

14
00:00:38,90 --> 00:00:41,20
For example, if you show that you have some experience

15
00:00:41,20 --> 00:00:44,60
with React, the employer might be willing to train you

16
00:00:44,60 --> 00:00:48,10
on a related technology, like Vue.js.

17
00:00:48,10 --> 00:00:50,00
Once your application gets chosen,

18
00:00:50,00 --> 00:00:54,00
you might get a phone or in-person second interview.

19
00:00:54,00 --> 00:00:57,00
This is to verify that you meet the requirements.

20
00:00:57,00 --> 00:00:59,80
Now, be ready to talk about some of the things you've worked

21
00:00:59,80 --> 00:01:04,10
on and experience using the tools that they're using.

22
00:01:04,10 --> 00:01:06,90
You shouldn't talk about salary at this point

23
00:01:06,90 --> 00:01:08,60
but if you get questions about it,

24
00:01:08,60 --> 00:01:10,10
just say something like,

25
00:01:10,10 --> 00:01:12,60
I'm not ready to negotiate salary right now,

26
00:01:12,60 --> 00:01:15,70
I want to make sure that this is the right fit.

27
00:01:15,70 --> 00:01:18,70
The next step is a more detailed interview.

28
00:01:18,70 --> 00:01:21,50
Sometimes, this is a technical interview

29
00:01:21,50 --> 00:01:24,00
and can mean giving you some assignments

30
00:01:24,00 --> 00:01:28,00
or technical questions to make sure you can code.

31
00:01:28,00 --> 00:01:29,90
Now, this can be really scary,

32
00:01:29,90 --> 00:01:33,60
so think of your entire job search as a process,

33
00:01:33,60 --> 00:01:35,20
not a single goal.

34
00:01:35,20 --> 00:01:37,40
It might be a good idea to assume

35
00:01:37,40 --> 00:01:40,30
that you'll be going through several technical interviews

36
00:01:40,30 --> 00:01:42,70
before you find the right job.

37
00:01:42,70 --> 00:01:45,20
Next will be a final interview.

38
00:01:45,20 --> 00:01:47,60
At this point, they've narrowed down the candidates

39
00:01:47,60 --> 00:01:49,10
to just a few.

40
00:01:49,10 --> 00:01:50,50
They know you can do the job

41
00:01:50,50 --> 00:01:54,10
and are just trying to pick the best person for the job.

42
00:01:54,10 --> 00:01:56,50
So make sure you've researched the company

43
00:01:56,50 --> 00:01:59,30
and the person you'll be meeting.

44
00:01:59,30 --> 00:02:01,70
One thing people miss during this point of the interview

45
00:02:01,70 --> 00:02:05,70
is that managers hire people they want to work with,

46
00:02:05,70 --> 00:02:08,60
so it's real important to be nice and polite.

47
00:02:08,60 --> 00:02:12,40
Now, this is also a great time to ask good questions

48
00:02:12,40 --> 00:02:14,00
about the company.

49
00:02:14,00 --> 00:02:15,60
Asking questions will help you

50
00:02:15,60 --> 00:02:19,00
appear more prepared and professional.

