1
00:00:00,70 --> 00:00:04,20
- Let's talk about how developers approach the job search

2
00:00:04,20 --> 00:00:07,00
and how it's different from traditional jobs.

3
00:00:07,00 --> 00:00:09,80
You need to spend time with other programmers

4
00:00:09,80 --> 00:00:12,90
to make contacts, find out who's hiring,

5
00:00:12,90 --> 00:00:16,00
plus what the popular frameworks, platforms,

6
00:00:16,00 --> 00:00:17,90
and languages are.

7
00:00:17,90 --> 00:00:19,40
Every area is different,

8
00:00:19,40 --> 00:00:22,40
and the tools and languages that companies use

9
00:00:22,40 --> 00:00:25,20
are related to where you live.

10
00:00:25,20 --> 00:00:28,60
Almost 30% of all developers got their jobs

11
00:00:28,60 --> 00:00:32,20
because of a friend, while less than 20% found work

12
00:00:32,20 --> 00:00:34,40
by applying through a website.

13
00:00:34,40 --> 00:00:36,40
Companies value employee referrals

14
00:00:36,40 --> 00:00:39,50
because it's the best way to find experienced talent.

15
00:00:39,50 --> 00:00:41,10
Like in many businesses,

16
00:00:41,10 --> 00:00:44,90
it's not what you know but who knows you.

17
00:00:44,90 --> 00:00:47,70
A great way to participate in the local community

18
00:00:47,70 --> 00:00:50,00
is to attend local meetings.

19
00:00:50,00 --> 00:00:51,90
Now these are often called meetups.

20
00:00:51,90 --> 00:00:54,00
Meetup.com is a great site

21
00:00:54,00 --> 00:00:56,90
to find developer events in your area.

22
00:00:56,90 --> 00:00:58,20
Now most of them are free

23
00:00:58,20 --> 00:01:00,80
and a great way to start building connections.

24
00:01:00,80 --> 00:01:03,60
Attendance is only part of the solution.

25
00:01:03,60 --> 00:01:06,90
You will get the best results if you contribute in some way.

26
00:01:06,90 --> 00:01:09,90
So volunteer to take photos, sign up people,

27
00:01:09,90 --> 00:01:13,50
or even give a talk on a topic you're passionate about.

28
00:01:13,50 --> 00:01:15,40
Don't worry that you don't know everything.

29
00:01:15,40 --> 00:01:16,60
Meetups are great

30
00:01:16,60 --> 00:01:19,90
because everyone can learn something from each other.

31
00:01:19,90 --> 00:01:23,20
You can easily share about the tools that you're using

32
00:01:23,20 --> 00:01:25,50
or about the projects you're working on.

33
00:01:25,50 --> 00:01:28,80
Sometimes teaching is the best way to learn something.

34
00:01:28,80 --> 00:01:31,90
Now something else you can do is attend conferences,

35
00:01:31,90 --> 00:01:33,70
but these are pricier.

36
00:01:33,70 --> 00:01:34,80
They give you access

37
00:01:34,80 --> 00:01:37,80
to a different level of industry professionals.

38
00:01:37,80 --> 00:01:41,00
Some offer discount tickets if you volunteer

39
00:01:41,00 --> 00:01:43,20
or may have student pricing.

40
00:01:43,20 --> 00:01:46,00
There is almost always a job board at conferences

41
00:01:46,00 --> 00:01:47,60
where you can find local companies

42
00:01:47,60 --> 00:01:50,30
who are hungry for talented developers.

43
00:01:50,30 --> 00:01:53,60
Now there's a special type of meetup called a hackathon.

44
00:01:53,60 --> 00:01:55,00
It's more of a competition

45
00:01:55,00 --> 00:01:56,90
where you can sign up to participate

46
00:01:56,90 --> 00:02:00,50
with teams of developers to build real projects.

47
00:02:00,50 --> 00:02:01,90
It's usually over a weekend,

48
00:02:01,90 --> 00:02:05,40
and they are often sponsored by schools or companies.

49
00:02:05,40 --> 00:02:08,70
Now potential employers love to see real work,

50
00:02:08,70 --> 00:02:11,70
so this is a great way to get some experience.

51
00:02:11,70 --> 00:02:14,90
The key to meetups is to be friendly and helpful.

52
00:02:14,90 --> 00:02:19,00
Find events that truly interest you and try to get involved.

