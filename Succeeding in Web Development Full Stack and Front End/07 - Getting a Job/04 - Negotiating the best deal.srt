1
00:00:00,70 --> 00:00:02,90
- Once you've gone through the interview process,

2
00:00:02,90 --> 00:00:04,90
if you're chosen for the position,

3
00:00:04,90 --> 00:00:07,20
it's time to negotiate your offer.

4
00:00:07,20 --> 00:00:09,50
If at any point in time before this

5
00:00:09,50 --> 00:00:12,10
someone wants to know what you want to make,

6
00:00:12,10 --> 00:00:14,40
tell them, "I'm not ready to negotiate salary,

7
00:00:14,40 --> 00:00:16,60
until you're ready to make me an offer."

8
00:00:16,60 --> 00:00:19,20
Now if they say that they're ready to make you an offer,

9
00:00:19,20 --> 00:00:21,90
simply ask them what the offer is.

10
00:00:21,90 --> 00:00:24,50
You're going to need to have some numbers at this point.

11
00:00:24,50 --> 00:00:27,70
Number one, you need to know what you're currently worth.

12
00:00:27,70 --> 00:00:30,00
Number two, how much the job pays,

13
00:00:30,00 --> 00:00:33,60
and what the minimum amount you're willing to take.

14
00:00:33,60 --> 00:00:35,40
If their offer comes below that,

15
00:00:35,40 --> 00:00:36,70
then you'll need to let them know

16
00:00:36,70 --> 00:00:39,00
what your numbers should be.

17
00:00:39,00 --> 00:00:41,00
What you're currently worth depends on

18
00:00:41,00 --> 00:00:42,50
what you're making now.

19
00:00:42,50 --> 00:00:44,70
How much the job pays is sometimes

20
00:00:44,70 --> 00:00:46,90
listed in their application.

21
00:00:46,90 --> 00:00:48,30
Or you can look on pages

22
00:00:48,30 --> 00:00:50,70
like the LinkedIn salary calculator.

23
00:00:50,70 --> 00:00:53,40
If their offer is below what you want to make,

24
00:00:53,40 --> 00:00:55,40
simply say something like this,

25
00:00:55,40 --> 00:00:58,10
"I think I'm worth X amount of money."

26
00:00:58,10 --> 00:01:00,90
Now this is a good time to practice your poker face,

27
00:01:00,90 --> 00:01:03,00
so say it like you mean it.

28
00:01:03,00 --> 00:01:05,20
As part of your reason for additional money,

29
00:01:05,20 --> 00:01:08,00
you can go over the differences and benefits

30
00:01:08,00 --> 00:01:11,20
between what you currently have, and what they're offering,

31
00:01:11,20 --> 00:01:13,50
so you can justify an adjustment.

32
00:01:13,50 --> 00:01:15,10
Once you've nailed down the offer,

33
00:01:15,10 --> 00:01:19,00
if it feels like a good one, ask for an offer letter.

34
00:01:19,00 --> 00:01:23,00
Everything they said should be written down and in paper.

35
00:01:23,00 --> 00:01:26,00
You should always ask for at least a couple of days

36
00:01:26,00 --> 00:01:27,80
to think the offer through.

37
00:01:27,80 --> 00:01:30,70
Don't make a final decision at this point, but say,

38
00:01:30,70 --> 00:01:32,50
"Offer looks good, but I need

39
00:01:32,50 --> 00:01:34,80
a couple of days to think it over."

40
00:01:34,80 --> 00:01:36,80
Listen, if you're really excited,

41
00:01:36,80 --> 00:01:38,80
and you know the offer is phenomenal,

42
00:01:38,80 --> 00:01:40,50
then feel free to accept it.

43
00:01:40,50 --> 00:01:42,20
But it's sometimes better to extract

44
00:01:42,20 --> 00:01:43,60
yourself from the excitement,

45
00:01:43,60 --> 00:01:47,00
and take the time to go through their numbers carefully.

46
00:01:47,00 --> 00:01:49,40
With an offer letter, the tables turn,

47
00:01:49,40 --> 00:01:50,90
and you should examine everything

48
00:01:50,90 --> 00:01:52,70
you've learned about the company.

49
00:01:52,70 --> 00:01:56,30
And ask yourself, is this the best place for me?

50
00:01:56,30 --> 00:01:58,10
Now you may be having second thoughts

51
00:01:58,10 --> 00:02:00,30
about leaving your current job.

52
00:02:00,30 --> 00:02:01,80
You can choose to negotiate

53
00:02:01,80 --> 00:02:03,70
with your current company.

54
00:02:03,70 --> 00:02:06,60
If so, make an appointment with your manager.

55
00:02:06,60 --> 00:02:09,00
They may be willing to give you a counter offer,

56
00:02:09,00 --> 00:02:10,90
or, give you a promotion.

57
00:02:10,90 --> 00:02:12,90
Staying where you are is fine,

58
00:02:12,90 --> 00:02:15,00
and sometimes the best option.

59
00:02:15,00 --> 00:02:17,00
If you're working in a great place

60
00:02:17,00 --> 00:02:18,80
that cares about you and gives you

61
00:02:18,80 --> 00:02:20,60
opportunities for growth,

62
00:02:20,60 --> 00:02:24,90
that's sometimes worth as much as monetary compensation.

63
00:02:24,90 --> 00:02:27,00
It's okay to turn down the offer

64
00:02:27,00 --> 00:02:28,70
if it's not the right one.

65
00:02:28,70 --> 00:02:30,00
It's really important that you're

66
00:02:30,00 --> 00:02:31,90
willing to walk away.

67
00:02:31,90 --> 00:02:34,50
Be polite, but just say something like this,

68
00:02:34,50 --> 00:02:36,80
"I'm not sure this is the right offer for me.

69
00:02:36,80 --> 00:02:38,10
Thank you for your time,

70
00:02:38,10 --> 00:02:39,40
I really hope you can find

71
00:02:39,40 --> 00:02:41,40
the right person to fill this position."

72
00:02:41,40 --> 00:02:44,00
Sometimes they'll make you their best offer

73
00:02:44,00 --> 00:02:47,30
after this. Sometimes they won't be able to.

74
00:02:47,30 --> 00:02:49,80
Now making decisions can be tough,

75
00:02:49,80 --> 00:02:53,40
but you need to be happy about the decision you're making.

76
00:02:53,40 --> 00:02:57,00
If you're not, it's a sign that it isn't the right decision.

