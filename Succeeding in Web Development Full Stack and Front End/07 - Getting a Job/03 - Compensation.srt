1
00:00:00,70 --> 00:00:03,30
- Developers are compensated in different ways

2
00:00:03,30 --> 00:00:05,20
beyond the traditional salary.

3
00:00:05,20 --> 00:00:08,70
So it's important to understand how compensation works,

4
00:00:08,70 --> 00:00:11,40
and how it is calculated for developers.

5
00:00:11,40 --> 00:00:13,90
When you're hired by a company you typically start

6
00:00:13,90 --> 00:00:15,90
with a certain base salary.

7
00:00:15,90 --> 00:00:19,00
That salary will grow an average of two to five percent

8
00:00:19,00 --> 00:00:20,40
or more per year.

9
00:00:20,40 --> 00:00:22,50
Now that raise might seem great,

10
00:00:22,50 --> 00:00:25,10
but you should also consider that the average rate

11
00:00:25,10 --> 00:00:28,10
of inflation is currently at 2%,

12
00:00:28,10 --> 00:00:30,60
which is historically low.

13
00:00:30,60 --> 00:00:34,60
It has been as high as 18%, so sometimes raises,

14
00:00:34,60 --> 00:00:36,60
if you get them at all, don't keep up

15
00:00:36,60 --> 00:00:38,20
with the cost of goods.

16
00:00:38,20 --> 00:00:39,90
When you look for a job you should look

17
00:00:39,90 --> 00:00:44,00
to make about 15% or more than what you're making

18
00:00:44,00 --> 00:00:46,00
at your current job.

19
00:00:46,00 --> 00:00:48,30
Now don't just ask about salary,

20
00:00:48,30 --> 00:00:52,50
but ask how much the compensation package is worth.

21
00:00:52,50 --> 00:00:53,90
Now that means taking into account

22
00:00:53,90 --> 00:00:56,20
how much the company pays for insurance,

23
00:00:56,20 --> 00:00:59,50
if there are stock options, bonuses, et cetera.

24
00:00:59,50 --> 00:01:02,20
Now developer jobs are so competitive

25
00:01:02,20 --> 00:01:06,20
that companies often offer significant incentives.

26
00:01:06,20 --> 00:01:09,50
They can be hiring bonuses, stock in the company,

27
00:01:09,50 --> 00:01:11,40
and annual bonuses.

28
00:01:11,40 --> 00:01:13,00
Don't forget to ask about those

29
00:01:13,00 --> 00:01:16,60
during your final interview, before they make you an offer.

30
00:01:16,60 --> 00:01:20,10
You won't get what you don't ask for, so be bold.

31
00:01:20,10 --> 00:01:22,80
You'll be surprised what a difference asking

32
00:01:22,80 --> 00:01:24,70
about these things can make.

33
00:01:24,70 --> 00:01:27,50
Make sure you ask about quality of life benefits

34
00:01:27,50 --> 00:01:30,40
like vacation, and time off.

35
00:01:30,40 --> 00:01:32,90
If you're already getting a more generous package

36
00:01:32,90 --> 00:01:35,30
in your current company, you can turn that

37
00:01:35,30 --> 00:01:39,40
into a better negotiation with the new company.

38
00:01:39,40 --> 00:01:41,60
One of the things that separates development jobs

39
00:01:41,60 --> 00:01:44,80
from others is that your knowledge of certain topics

40
00:01:44,80 --> 00:01:48,30
has a big part in determining what you're worth.

41
00:01:48,30 --> 00:01:50,70
A developer with more than five years of experience

42
00:01:50,70 --> 00:01:54,00
will often make 20 to 30% more,

43
00:01:54,00 --> 00:01:57,00
and that difference grows over time.

44
00:01:57,00 --> 00:02:00,40
Asking about these things makes you appear more professional

45
00:02:00,40 --> 00:02:02,50
and shows a level of preparation

46
00:02:02,50 --> 00:02:05,30
that will impress your potential employer.

47
00:02:05,30 --> 00:02:08,80
It's also a good way of gauging how well the company deals

48
00:02:08,80 --> 00:02:10,50
with real questions.

49
00:02:10,50 --> 00:02:13,00
It's your turn to interview them.

