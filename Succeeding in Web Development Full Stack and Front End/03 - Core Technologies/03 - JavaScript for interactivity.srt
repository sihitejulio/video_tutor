1
00:00:00,70 --> 00:00:03,10
- Everyone who works in the world of web development

2
00:00:03,10 --> 00:00:07,10
has to be comfortable with a certain amount of programming.

3
00:00:07,10 --> 00:00:11,10
For web development that means learning about JavaScript,

4
00:00:11,10 --> 00:00:14,00
the language responsible for managing interaction

5
00:00:14,00 --> 00:00:15,60
within a browser.

6
00:00:15,60 --> 00:00:17,00
It's not just front end developers,

7
00:00:17,00 --> 00:00:21,00
50% of back end developers say they use some form

8
00:00:21,00 --> 00:00:22,90
of JavaScript daily.

9
00:00:22,90 --> 00:00:26,90
JavaScript interacts with the DOM handling events,

10
00:00:26,90 --> 00:00:30,80
loading and unloading media and creating, modifying

11
00:00:30,80 --> 00:00:33,30
and deleting elements in a page.

12
00:00:33,30 --> 00:00:35,60
Now this is a job for someone who appreciates

13
00:00:35,60 --> 00:00:39,20
creating and designing interactions with content.

14
00:00:39,20 --> 00:00:41,90
There have been several revisions to the language

15
00:00:41,90 --> 00:00:45,20
with a new version always on the horizon.

16
00:00:45,20 --> 00:00:48,80
Developers also have to worry about browser support

17
00:00:48,80 --> 00:00:52,30
issues because the language has matured rapidly

18
00:00:52,30 --> 00:00:54,30
and browsers don't always keep up

19
00:00:54,30 --> 00:00:56,70
with the latest improvements.

20
00:00:56,70 --> 00:00:59,40
To take care of this, most developers use something

21
00:00:59,40 --> 00:01:02,80
called a transpiler, which will convert code written

22
00:01:02,80 --> 00:01:05,50
in newer versions of JavaScript so that they work

23
00:01:05,50 --> 00:01:07,50
in older versions.

24
00:01:07,50 --> 00:01:09,70
Some developers write code in languages

25
00:01:09,70 --> 00:01:12,30
other than JavaScript that let them take advantage

26
00:01:12,30 --> 00:01:15,60
of new techniques and improvements that will eventually

27
00:01:15,60 --> 00:01:18,00
get folded back into JavaScript.

28
00:01:18,00 --> 00:01:21,60
JavaScript has also been implemented on the server side

29
00:01:21,60 --> 00:01:23,90
through something called Node.js,

30
00:01:23,90 --> 00:01:27,30
which is an implementation of the engine that runs

31
00:01:27,30 --> 00:01:30,90
in most browsers called Chrome V8.

32
00:01:30,90 --> 00:01:34,60
Now this allows you to use JavaScript in unexpected ways.

33
00:01:34,60 --> 00:01:37,90
For example, it gives you access to the file system.

34
00:01:37,90 --> 00:01:41,30
JavaScript has become a way of building sites,

35
00:01:41,30 --> 00:01:43,70
but also running applications that are going

36
00:01:43,70 --> 00:01:46,00
to make your life easier.

