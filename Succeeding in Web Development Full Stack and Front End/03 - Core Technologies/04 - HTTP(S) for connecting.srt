1
00:00:00,70 --> 00:00:02,90
- Remember C3PO in Star Wars?

2
00:00:02,90 --> 00:00:05,40
If you ask him how many languages he spoke,

3
00:00:05,40 --> 00:00:06,80
he'd tell you he was fluent

4
00:00:06,80 --> 00:00:09,70
in over six million forms of communication.

5
00:00:09,70 --> 00:00:11,70
But, if you asked him what he was,

6
00:00:11,70 --> 00:00:14,50
he'd tell you he was a protocol droid.

7
00:00:14,50 --> 00:00:17,40
Protocols are pretty important for web developers.

8
00:00:17,40 --> 00:00:18,70
They're the different languages

9
00:00:18,70 --> 00:00:21,70
that devices use to share information.

10
00:00:21,70 --> 00:00:25,90
There are different dialects like POP, SMTP and IMAP,

11
00:00:25,90 --> 00:00:28,10
that programs use to talk about mail,

12
00:00:28,10 --> 00:00:31,90
and sometimes FTP, the File Transfer Protocol

13
00:00:31,90 --> 00:00:33,60
that's used to share files.

14
00:00:33,60 --> 00:00:36,40
Now, for web developers the most important language

15
00:00:36,40 --> 00:00:42,10
is HTTP, and its more secure cousin HTTPS.

16
00:00:42,10 --> 00:00:45,60
HTTP stands for Hypertext Transfer Protocol.

17
00:00:45,60 --> 00:00:47,70
It's designed to transfer information

18
00:00:47,70 --> 00:00:50,30
from languages like HTML.

19
00:00:50,30 --> 00:00:53,80
It works by allowing someone using a browser,

20
00:00:53,80 --> 00:00:56,60
which is often referred to as a client,

21
00:00:56,60 --> 00:00:59,60
to send requests to software that handles them.

22
00:00:59,60 --> 00:01:01,70
Now that's called a server.

23
00:01:01,70 --> 00:01:04,50
HTTP uses a series of verbs.

24
00:01:04,50 --> 00:01:07,30
There are nine of these, but the most common ones

25
00:01:07,30 --> 00:01:10,40
are GET, POST and DELETE.

26
00:01:10,40 --> 00:01:14,50
Now, GET asks for a resource or a piece of information,

27
00:01:14,50 --> 00:01:16,60
POST sends some information,

28
00:01:16,60 --> 00:01:21,10
and DELETE, of course, sends a request to delete something.

29
00:01:21,10 --> 00:01:24,50
So if I was using HTTP requests while ordering coffee,

30
00:01:24,50 --> 00:01:28,20
I could send a series of requests like this.

31
00:01:28,20 --> 00:01:30,70
There's a few variants of HTTP.

32
00:01:30,70 --> 00:01:36,20
First, HTTP/2 was a major revision of the HTTP language,

33
00:01:36,20 --> 00:01:39,30
which had been around since 1997.

34
00:01:39,30 --> 00:01:42,40
It quickly became the standard for web traffic,

35
00:01:42,40 --> 00:01:46,30
and it's backwards compatible with HTTP/1.

36
00:01:46,30 --> 00:01:50,40
One of the main features of HTTP/2 is multiplexing,

37
00:01:50,40 --> 00:01:53,60
which lets you send multiple requests at once.

38
00:01:53,60 --> 00:01:55,90
Now that would be convenient if I'm ordering coffee

39
00:01:55,90 --> 00:01:58,00
for my team at the office.

40
00:01:58,00 --> 00:02:01,30
Another revision is HTTPS.

41
00:02:01,30 --> 00:02:06,00
Now, this is an extension of HTTP that makes it more secure.

42
00:02:06,00 --> 00:02:08,50
It takes care of this by encrypting the requests

43
00:02:08,50 --> 00:02:10,20
to and from the server.

44
00:02:10,20 --> 00:02:12,80
If you're not using something like HTTPS,

45
00:02:12,80 --> 00:02:15,20
the requests to and from the server

46
00:02:15,20 --> 00:02:17,70
are pretty easy for bad actors to copy.

47
00:02:17,70 --> 00:02:21,10
So, it's sort of like asking for your coffees in Pig Latin,

48
00:02:21,10 --> 00:02:23,70
although in a language that's more secure.

49
00:02:23,70 --> 00:02:25,90
HTTP, like any other web language,

50
00:02:25,90 --> 00:02:28,60
continues to grow, and there are already proposals

51
00:02:28,60 --> 00:02:31,50
for future versions, like HTTP/3,

52
00:02:31,50 --> 00:02:35,00
that improve and expand the language.

