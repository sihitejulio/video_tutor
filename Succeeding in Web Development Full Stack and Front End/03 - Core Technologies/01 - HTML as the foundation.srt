1
00:00:00,70 --> 00:00:03,60
- Whether you're a front end, back end, full stack,

2
00:00:03,60 --> 00:00:05,60
or any type of web developer,

3
00:00:05,60 --> 00:00:08,30
there is one language that everyone needs to master,

4
00:00:08,30 --> 00:00:10,00
and that's HTML.

5
00:00:10,00 --> 00:00:13,20
HTML stands for Hypertext Markup Language.

6
00:00:13,20 --> 00:00:16,40
It describes the content and defines the structure

7
00:00:16,40 --> 00:00:18,00
within a webpage.

8
00:00:18,00 --> 00:00:20,70
Hypertext means that documents on the web

9
00:00:20,70 --> 00:00:23,90
are not meant to be consumed in a linear fashion.

10
00:00:23,90 --> 00:00:27,30
They're designed to be linked to other documents.

11
00:00:27,30 --> 00:00:29,90
A friend and developer has to understand the relationship

12
00:00:29,90 --> 00:00:31,90
of the entire site,

13
00:00:31,90 --> 00:00:34,80
and how documents relate to one another.

14
00:00:34,80 --> 00:00:37,70
Now this is called information architecture,

15
00:00:37,70 --> 00:00:40,60
and it involves how best to structure a website.

16
00:00:40,60 --> 00:00:42,50
How a user is going to navigate,

17
00:00:42,50 --> 00:00:46,40
and what type of access users need to other content.

18
00:00:46,40 --> 00:00:50,10
The second part of HTML is the markup language.

19
00:00:50,10 --> 00:00:51,90
That's the set of tags that are added

20
00:00:51,90 --> 00:00:55,00
to the text of a webpage to define its structure.

21
00:00:55,00 --> 00:00:57,10
It has some really critical roles.

22
00:00:57,10 --> 00:01:01,30
First, it defines the role each piece of content will have.

23
00:01:01,30 --> 00:01:04,60
Some content will identify different parts of the page

24
00:01:04,60 --> 00:01:07,60
like headers, navigation, and footers.

25
00:01:07,60 --> 00:01:10,30
Some tags describe non-textual elements

26
00:01:10,30 --> 00:01:12,60
like images or video.

27
00:01:12,60 --> 00:01:16,30
Now all web developers are masters of the markup language.

28
00:01:16,30 --> 00:01:18,10
They have to be detail-oriented,

29
00:01:18,10 --> 00:01:21,80
and passionate about structure and semantics.

30
00:01:21,80 --> 00:01:24,60
Markup does something else that is really important.

31
00:01:24,60 --> 00:01:28,50
It generates the DOM, or Document Object Model.

32
00:01:28,50 --> 00:01:30,90
The DOM describes the structure of the page,

33
00:01:30,90 --> 00:01:32,50
and it's sort of like an outline

34
00:01:32,50 --> 00:01:35,50
or a map of the content within a website.

35
00:01:35,50 --> 00:01:37,70
When you add Markup tags to a document,

36
00:01:37,70 --> 00:01:38,90
you're building the map

37
00:01:38,90 --> 00:01:41,60
that defines the role of your content.

38
00:01:41,60 --> 00:01:46,00
This map is really important to how CSS and JavaScript work.

39
00:01:46,00 --> 00:01:49,70
The DOM is the way you access elements on the page

40
00:01:49,70 --> 00:01:51,60
with the other languages.

41
00:01:51,60 --> 00:01:53,80
The DOM is so important to sites and apps

42
00:01:53,80 --> 00:01:55,70
that if it isn't coded correctly,

43
00:01:55,70 --> 00:01:58,00
it can break the entire website.

44
00:01:58,00 --> 00:02:00,80
Although there are many skills to learn in web development,

45
00:02:00,80 --> 00:02:04,20
HTML is foundational to every type of job,

46
00:02:04,20 --> 00:02:07,30
and although technically it's the easiest of the languages,

47
00:02:07,30 --> 00:02:11,00
it's also the one everyone must master.

