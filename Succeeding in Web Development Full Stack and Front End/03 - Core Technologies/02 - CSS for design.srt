1
00:00:00,70 --> 00:00:04,20
- Design plays an important part and is a key component

2
00:00:04,20 --> 00:00:06,40
of every successful website.

3
00:00:06,40 --> 00:00:08,50
Here you'll notice a specialization

4
00:00:08,50 --> 00:00:11,20
within the different types of developers.

5
00:00:11,20 --> 00:00:13,00
There are folks who are more interested

6
00:00:13,00 --> 00:00:14,70
in the look and feel of a site

7
00:00:14,70 --> 00:00:18,40
and enjoy working on the user experience or UX,

8
00:00:18,40 --> 00:00:20,20
and there are those who prefer to work

9
00:00:20,20 --> 00:00:22,10
on the functionality of a site.

10
00:00:22,10 --> 00:00:25,20
That's called user interface or UI.

11
00:00:25,20 --> 00:00:27,40
Design for a website will require learning

12
00:00:27,40 --> 00:00:30,50
how to work with the layout language of the web,

13
00:00:30,50 --> 00:00:32,80
which is called CSS.

14
00:00:32,80 --> 00:00:36,00
CSS stands for Cascading Style Sheets.

15
00:00:36,00 --> 00:00:39,20
Like HTML, it's made up of two parts.

16
00:00:39,20 --> 00:00:43,00
First, CSS is about designing a set of rules

17
00:00:43,00 --> 00:00:46,20
or style sheets for how the DOM is translated

18
00:00:46,20 --> 00:00:48,10
into visual form.

19
00:00:48,10 --> 00:00:51,10
The second part is the cascading style rules.

20
00:00:51,10 --> 00:00:54,60
Those are a set of rules that describe the priority

21
00:00:54,60 --> 00:00:57,70
of how the styles are rendered on a page.

22
00:00:57,70 --> 00:01:00,60
CSS defines a set of rules that identify

23
00:01:00,60 --> 00:01:03,10
the part of the page that you want to modify,

24
00:01:03,10 --> 00:01:04,90
and then describes the styles

25
00:01:04,90 --> 00:01:08,70
that make the page look and behave in that manner.

26
00:01:08,70 --> 00:01:12,70
A good example of this is the CSS Zen Garden.

27
00:01:12,70 --> 00:01:15,30
This website demonstrates how the same content

28
00:01:15,30 --> 00:01:18,50
can be designed to look completely different.

29
00:01:18,50 --> 00:01:20,60
The site showcases different styles

30
00:01:20,60 --> 00:01:23,30
for the same HTML page.

31
00:01:23,30 --> 00:01:26,50
When I chose another set of rules for this page,

32
00:01:26,50 --> 00:01:30,70
the layout of the pages change, sometimes dramatically.

33
00:01:30,70 --> 00:01:31,80
Now just to emphasize,

34
00:01:31,80 --> 00:01:34,60
this is exactly the same HTML code,

35
00:01:34,60 --> 00:01:38,20
just presented in dramatically different ways.

36
00:01:38,20 --> 00:01:41,10
CSS will also let you add some motion

37
00:01:41,10 --> 00:01:44,40
and even a level of interactivity to your designs.

38
00:01:44,40 --> 00:01:47,20
So, learning CSS means also learning to work

39
00:01:47,20 --> 00:01:49,30
with the elements of motion

40
00:01:49,30 --> 00:01:51,40
and the language of interaction

41
00:01:51,40 --> 00:01:53,90
as it relates to graphic design.

42
00:01:53,90 --> 00:01:58,40
As CSS has matured, things like variables, calculations,

43
00:01:58,40 --> 00:02:01,80
and more complex features have made it into the language.

44
00:02:01,80 --> 00:02:04,80
So, CSS now has some features that are similar

45
00:02:04,80 --> 00:02:08,20
to what's available in programming languages.

46
00:02:08,20 --> 00:02:11,10
One of the big differences in designing for the web

47
00:02:11,10 --> 00:02:14,30
is that content has to be responsive,

48
00:02:14,30 --> 00:02:16,80
in other words, adjust to the proportions

49
00:02:16,80 --> 00:02:18,30
of different viewports,

50
00:02:18,30 --> 00:02:22,00
like desktops, laptops, and mobile devices.

51
00:02:22,00 --> 00:02:25,20
Also, the CSS language is always changing.

52
00:02:25,20 --> 00:02:27,20
There have been three major revisions,

53
00:02:27,20 --> 00:02:31,90
and a third revision has been split into independent modules

54
00:02:31,90 --> 00:02:33,70
that will expand in the future,

55
00:02:33,70 --> 00:02:36,00
so the language will always be evolving.

