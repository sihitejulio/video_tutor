1
00:00:00,70 --> 00:00:04,70
- Higher education is an important part of many careers,

2
00:00:04,70 --> 00:00:06,20
but not everyone thrives

3
00:00:06,20 --> 00:00:09,20
in a standard educational environment.

4
00:00:09,20 --> 00:00:10,90
Do you really need to get a college degree

5
00:00:10,90 --> 00:00:12,80
to get a developer job?

6
00:00:12,80 --> 00:00:15,10
You may have heard that people like Steve Jobs

7
00:00:15,10 --> 00:00:18,70
and Mark Zuckerberg never finished their college education.

8
00:00:18,70 --> 00:00:21,20
I think a better question is what value

9
00:00:21,20 --> 00:00:23,00
does having a college degree

10
00:00:23,00 --> 00:00:25,90
bring to my ability to get hired?

11
00:00:25,90 --> 00:00:27,90
Now first let's talk about the cost

12
00:00:27,90 --> 00:00:29,60
of a college education.

13
00:00:29,60 --> 00:00:31,80
It has skyrocketed.

14
00:00:31,80 --> 00:00:34,80
The average cost of attending a private university

15
00:00:34,80 --> 00:00:38,20
is now nearly $50,000 annually,

16
00:00:38,20 --> 00:00:41,10
three times the price since 1990.

17
00:00:41,10 --> 00:00:44,60
Tuition has climbed at a faster rate than inflation,

18
00:00:44,60 --> 00:00:46,60
and median income levels.

19
00:00:46,60 --> 00:00:48,90
However, having a college education

20
00:00:48,90 --> 00:00:51,20
does make a big difference.

21
00:00:51,20 --> 00:00:54,40
College graduates earn about $1 million more

22
00:00:54,40 --> 00:00:57,90
over their lifetime than people without a degree.

23
00:00:57,90 --> 00:01:00,70
Now some companies require a college degree

24
00:01:00,70 --> 00:01:02,50
in order to be hired.

25
00:01:02,50 --> 00:01:08,00
About 65% of all jobs require a post-secondary education.

26
00:01:08,00 --> 00:01:09,50
However, in recent years,

27
00:01:09,50 --> 00:01:12,50
companies like Apple, Google, and IBM

28
00:01:12,50 --> 00:01:14,80
stopped requiring a college degree.

29
00:01:14,80 --> 00:01:17,80
A study of professionals working for more than five years

30
00:01:17,80 --> 00:01:19,30
in software development

31
00:01:19,30 --> 00:01:22,40
found two major benefits to a college education.

32
00:01:22,40 --> 00:01:25,20
College students learned to solve problems

33
00:01:25,20 --> 00:01:28,90
and explore research into higher level concepts.

34
00:01:28,90 --> 00:01:30,80
Being able to study concepts deeper

35
00:01:30,80 --> 00:01:35,60
and over a longer period of time is a huge advantage.

36
00:01:35,60 --> 00:01:39,60
General education gives students a firm and wide foundation.

37
00:01:39,60 --> 00:01:41,10
Math and physics courses

38
00:01:41,10 --> 00:01:44,00
can be very useful to computer science,

39
00:01:44,00 --> 00:01:45,70
but are not really incorporated

40
00:01:45,70 --> 00:01:48,20
into bootcamp style programs.

41
00:01:48,20 --> 00:01:51,00
Students can also be exposed to business courses

42
00:01:51,00 --> 00:01:53,60
that will teach them how to write proposals

43
00:01:53,60 --> 00:01:56,60
and documents for decision makers.

44
00:01:56,60 --> 00:01:58,90
If you're comparing a traditional college degree

45
00:01:58,90 --> 00:02:01,40
to a bootcamp or self-study program,

46
00:02:01,40 --> 00:02:05,20
it definitely has advantages in terms of earning potential,

47
00:02:05,20 --> 00:02:06,60
learning methodologies,

48
00:02:06,60 --> 00:02:10,00
and increasing your ability to get hired.

