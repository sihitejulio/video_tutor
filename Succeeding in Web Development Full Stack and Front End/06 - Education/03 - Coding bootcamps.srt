1
00:00:00,80 --> 00:00:03,50
- Bootcamps can be a fast way to get the skills

2
00:00:03,50 --> 00:00:06,90
you need to get into the developer field quickly.

3
00:00:06,90 --> 00:00:10,10
They are designed to teach the practical skills necessary

4
00:00:10,10 --> 00:00:14,80
to get a job in the industry in a condensed timeframe.

5
00:00:14,80 --> 00:00:16,90
According to a 2018 survey,

6
00:00:16,90 --> 00:00:20,80
these programs have an average tuition of $12,000,

7
00:00:20,80 --> 00:00:24,90
and courses are an average of 14 weeks long.

8
00:00:24,90 --> 00:00:28,20
They graduated 20,000 students in 2018,

9
00:00:28,20 --> 00:00:32,20
and that's up 20% from the previous year.

10
00:00:32,20 --> 00:00:35,40
The firms surveyed have an average placement success rate

11
00:00:35,40 --> 00:00:40,00
of 88% within 120 days.

12
00:00:40,00 --> 00:00:44,60
Now this is a full-time, in-person, immersive experience.

13
00:00:44,60 --> 00:00:48,50
Many camps run 40 hours or more per week.

14
00:00:48,50 --> 00:00:51,00
But that hard work often pays off.

15
00:00:51,00 --> 00:00:52,20
Why?

16
00:00:52,20 --> 00:00:55,40
Well, they teach skills not often taught in schools,

17
00:00:55,40 --> 00:00:58,30
like peer programming, test-driven development,

18
00:00:58,30 --> 00:01:00,30
and cutting-edge tools.

19
00:01:00,30 --> 00:01:03,80
However, many bootcamps have pretty low acceptance rates

20
00:01:03,80 --> 00:01:05,90
compared to traditional schools.

21
00:01:05,90 --> 00:01:07,40
They pick and choose the students

22
00:01:07,40 --> 00:01:10,10
with the highest chance of success,

23
00:01:10,10 --> 00:01:13,90
and not everyone is prepared for 10 to 12 weeks

24
00:01:13,90 --> 00:01:16,10
of intensive training,

25
00:01:16,10 --> 00:01:18,60
especially if you have a current job

26
00:01:18,60 --> 00:01:20,80
and have no other means of support.

27
00:01:20,80 --> 00:01:23,30
So, this can be a tough proposition.

28
00:01:23,30 --> 00:01:26,30
Now, too much practical knowledge without an understanding

29
00:01:26,30 --> 00:01:30,50
of why you should do something, can also be a disadvantage.

30
00:01:30,50 --> 00:01:33,70
Not everyone knows exactly what they want to do

31
00:01:33,70 --> 00:01:35,60
when they first go to college.

32
00:01:35,60 --> 00:01:38,70
Only about 50% of college graduates have jobs

33
00:01:38,70 --> 00:01:40,70
that relate to their degree.

34
00:01:40,70 --> 00:01:44,80
Over 30% of college educated workers never find jobs

35
00:01:44,80 --> 00:01:46,50
related to their major.

36
00:01:46,50 --> 00:01:49,90
Case in point, my B.A. was in Journalism.

37
00:01:49,90 --> 00:01:52,20
I learned other concepts, like design,

38
00:01:52,20 --> 00:01:54,60
by taking different college classes,

39
00:01:54,60 --> 00:01:57,30
which were helpful to a development job.

40
00:01:57,30 --> 00:02:01,00
So, the opportunity to explore and learn a broad range

41
00:02:01,00 --> 00:02:04,60
of skills is a big plus for college.

42
00:02:04,60 --> 00:02:08,20
Majority of bootcamp attendees are college graduates,

43
00:02:08,20 --> 00:02:11,20
so most students are already prepared for the type of

44
00:02:11,20 --> 00:02:14,60
intensive training required in a bootcamp.

45
00:02:14,60 --> 00:02:16,80
Bootcamps can be great if you're sure that

46
00:02:16,80 --> 00:02:19,60
being a web developer is what you want to do.

47
00:02:19,60 --> 00:02:22,30
Committing to an immersive program,

48
00:02:22,30 --> 00:02:26,00
and the financial challenges, are not for everyone.

