1
00:00:00,70 --> 00:00:02,90
- If you're a self starter your education

2
00:00:02,90 --> 00:00:05,20
can happen completely online.

3
00:00:05,20 --> 00:00:07,50
Online learning has several advantages,

4
00:00:07,50 --> 00:00:09,60
including the fact that you can learn where

5
00:00:09,60 --> 00:00:11,60
and when you like.

6
00:00:11,60 --> 00:00:12,60
It's low cost,

7
00:00:12,60 --> 00:00:16,50
and you have access to an enormous amount of content.

8
00:00:16,50 --> 00:00:19,90
Now obviously Google and YouTube are great ways to find

9
00:00:19,90 --> 00:00:22,50
quick answers, but these can be full of good

10
00:00:22,50 --> 00:00:27,90
and bad as well as old and sometimes distracting answers.

11
00:00:27,90 --> 00:00:31,10
An excellent tool is Stack Overflow.

12
00:00:31,10 --> 00:00:33,60
It's a website where you can post a question

13
00:00:33,60 --> 00:00:36,40
that other professionals will answer.

14
00:00:36,40 --> 00:00:39,70
Shameless plug, one of the advantages of an online learning

15
00:00:39,70 --> 00:00:42,50
site like ours, is curation.

16
00:00:42,50 --> 00:00:46,30
The quality, quantity and velocity of new content

17
00:00:46,30 --> 00:00:49,10
we produce is a great way to keep up

18
00:00:49,10 --> 00:00:51,00
with cutting edge technologies.

19
00:00:51,00 --> 00:00:53,50
Sometimes there is no substitute for working

20
00:00:53,50 --> 00:00:56,30
on a real project.

21
00:00:56,30 --> 00:00:59,40
Full code for many major projects is freely

22
00:00:59,40 --> 00:01:02,80
availible at sites like GitHub, which allow you to create

23
00:01:02,80 --> 00:01:05,60
your own version of an existing project.

24
00:01:05,60 --> 00:01:07,50
Now that's called forking.

25
00:01:07,50 --> 00:01:11,10
And you can learn a lot just from reading how someone else

26
00:01:11,10 --> 00:01:14,60
solved the problem in a real project.

27
00:01:14,60 --> 00:01:18,40
Sites like CodePen and JS Bin let you look at sample code

28
00:01:18,40 --> 00:01:20,30
that can show you how to do things.

29
00:01:20,30 --> 00:01:23,30
Now there is an abundance of learning availible

30
00:01:23,30 --> 00:01:27,70
to web developers, and if there's any job where continuing

31
00:01:27,70 --> 00:01:30,00
education is required, it's this job.

32
00:01:30,00 --> 00:01:31,60
And I'll let you in on a secret.

33
00:01:31,60 --> 00:01:34,10
Everyone, even expert developers,

34
00:01:34,10 --> 00:01:39,00
constantly look through the web for help and new ideas.

