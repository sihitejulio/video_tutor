1
00:00:00,70 --> 00:00:04,30
- Education can make a big difference in your career.

2
00:00:04,30 --> 00:00:06,50
There's four paths most people take

3
00:00:06,50 --> 00:00:08,50
to become web developers:

4
00:00:08,50 --> 00:00:12,70
instructor-led training, self-study, working on projects,

5
00:00:12,70 --> 00:00:15,90
and learning that only happens as a result

6
00:00:15,90 --> 00:00:17,90
or working with real clients.

7
00:00:17,90 --> 00:00:20,40
Instructor-led means learning from an instructor

8
00:00:20,40 --> 00:00:22,30
who follows a curriculum

9
00:00:22,30 --> 00:00:26,40
with exercises, lesson plans, and other tools.

10
00:00:26,40 --> 00:00:28,90
Now this type of learning typically happens

11
00:00:28,90 --> 00:00:33,40
at a school, in a bootcamp, or in a workshop or conference.

12
00:00:33,40 --> 00:00:36,80
This approach works great for someone with less experience

13
00:00:36,80 --> 00:00:41,30
who wants to be able to ask questions and work with others.

14
00:00:41,30 --> 00:00:44,20
Now almost 70% of all developers

15
00:00:44,20 --> 00:00:47,10
describe themselves as self-taught.

16
00:00:47,10 --> 00:00:50,40
So in a way no amount of instructor-led training

17
00:00:50,40 --> 00:00:54,20
is going to teach you everything you need to be successful.

18
00:00:54,20 --> 00:00:57,50
In this industry software changes so rapidly

19
00:00:57,50 --> 00:01:00,60
that it's not as important to learn specific skills,

20
00:01:00,60 --> 00:01:02,50
but rather learn the basics

21
00:01:02,50 --> 00:01:06,00
and then adjust quickly to new technologies.

22
00:01:06,00 --> 00:01:08,70
Some of the keys to becoming a great developer

23
00:01:08,70 --> 00:01:11,60
are learning to read online documentation,

24
00:01:11,60 --> 00:01:14,10
examining other developers' code,

25
00:01:14,10 --> 00:01:18,50
and exploring new ideas that can make you more efficient.

26
00:01:18,50 --> 00:01:22,00
No amount of reading books or watching videos

27
00:01:22,00 --> 00:01:24,10
will make you into a great programmer.

28
00:01:24,10 --> 00:01:26,50
You'll have to build real projects.

29
00:01:26,50 --> 00:01:28,80
So some learning is only going to happen

30
00:01:28,80 --> 00:01:32,90
as a result of making mistakes with real things.

31
00:01:32,90 --> 00:01:36,40
Making mistakes actually makes you a stronger developer

32
00:01:36,40 --> 00:01:38,10
by improving your ability

33
00:01:38,10 --> 00:01:42,00
to find, test, and anticipate errors.

34
00:01:42,00 --> 00:01:46,50
Other learning will only happen when you're on a real job.

35
00:01:46,50 --> 00:01:50,20
Working in a corporate or even a start-up environment

36
00:01:50,20 --> 00:01:54,70
is vastly different from the classroom or personal projects.

37
00:01:54,70 --> 00:01:57,00
It's important to learn how to work with others

38
00:01:57,00 --> 00:01:59,10
in cross-functional teams

39
00:01:59,10 --> 00:02:02,30
that are made up of more than just developers.

40
00:02:02,30 --> 00:02:03,70
Your education starts

41
00:02:03,70 --> 00:02:06,30
when you decide to become a web developer,

42
00:02:06,30 --> 00:02:08,00
but it never really ends.

