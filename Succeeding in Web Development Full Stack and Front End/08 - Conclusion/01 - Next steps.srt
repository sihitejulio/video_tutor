1
00:00:00,70 --> 00:00:02,40
- Looks like all my tests are passing,

2
00:00:02,40 --> 00:00:04,80
and this is almost done.

3
00:00:04,80 --> 00:00:06,20
That was a lot of stuff to learn,

4
00:00:06,20 --> 00:00:08,40
and I know that it can sound overwhelming,

5
00:00:08,40 --> 00:00:10,80
so take it one step at a time.

6
00:00:10,80 --> 00:00:13,80
All developers start in the same place,

7
00:00:13,80 --> 00:00:17,80
HTML, then move on to CSS, and JavaScript.

8
00:00:17,80 --> 00:00:21,90
Along the way, start building projects as soon as possible.

9
00:00:21,90 --> 00:00:24,10
On LinkedIn Learning, we've got some learning paths

10
00:00:24,10 --> 00:00:26,90
in our library that can help guide your learning,

11
00:00:26,90 --> 00:00:30,10
just search for Become a Web Developer.

12
00:00:30,10 --> 00:00:33,00
Now, if you get a feeling that you're not there yet,

13
00:00:33,00 --> 00:00:35,30
don't forget, we're on the same boat,

14
00:00:35,30 --> 00:00:39,10
because this is one career without a finish line.

15
00:00:39,10 --> 00:00:41,40
Also, one more thing, you are going to need

16
00:00:41,40 --> 00:00:43,10
to get some more hoodies.

17
00:00:43,10 --> 00:00:45,40
All right, it looks like my code is ready to go,

18
00:00:45,40 --> 00:00:47,70
and I'm ready for that big Sprint meeting.

19
00:00:47,70 --> 00:00:50,20
I'll see you around in the library.

20
00:00:50,20 --> 00:00:54,10
(keyboard clicking)

21
00:00:54,10 --> 00:00:55,60
What, you're still here?

22
00:00:55,60 --> 00:00:56,90
There's no post-credit scene.

23
00:00:56,90 --> 00:00:58,50
This isn't a superhero movie now.

24
00:00:58,50 --> 00:01:00,20
So get out there and start learning,

25
00:01:00,20 --> 00:01:04,00
HTML, CSS, and JavaScript, get crackin'.

