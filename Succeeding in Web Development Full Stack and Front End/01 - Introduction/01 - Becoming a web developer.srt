1
00:00:00,40 --> 00:00:10,50
(upbeat music)

2
00:00:10,50 --> 00:00:11,80
- All right, looks like that code is ready,

3
00:00:11,80 --> 00:00:14,40
and some of these tests are ready to go.

4
00:00:14,40 --> 00:00:17,00
Hey there, you're probably here because you're interested

5
00:00:17,00 --> 00:00:19,40
in becoming a web developer.

6
00:00:19,40 --> 00:00:20,20
That's great.

7
00:00:20,20 --> 00:00:23,00
I often get questions from friends, colleagues,

8
00:00:23,00 --> 00:00:25,30
and total strangers who are interested

9
00:00:25,30 --> 00:00:27,20
in becoming web developers.

10
00:00:27,20 --> 00:00:29,00
What's it like to be a developer?

11
00:00:29,00 --> 00:00:30,70
How much money can I make?

12
00:00:30,70 --> 00:00:33,40
What do I need to know and how do I learn everything?

13
00:00:33,40 --> 00:00:35,70
Plus, do I need to buy more hoodies?

14
00:00:35,70 --> 00:00:38,60
Hey, I'm Ray Villalobos and for years I've taught

15
00:00:38,60 --> 00:00:41,80
a bunch of courses on tools and skills

16
00:00:41,80 --> 00:00:44,20
that help developers in their career.

17
00:00:44,20 --> 00:00:47,70
But now, I want to focus on the career of a developer,

18
00:00:47,70 --> 00:00:51,20
and help you figure out if this is right for you.

19
00:00:51,20 --> 00:00:53,90
Now, before we get started, let's discuss what this course

20
00:00:53,90 --> 00:00:56,80
is about and who should watch it.

21
00:00:56,80 --> 00:01:00,20
We'll start by going over the different types of developers

22
00:01:00,20 --> 00:01:02,30
and what a developer career is like

23
00:01:02,30 --> 00:01:06,20
so that you can find out whether or not this appeals to you.

24
00:01:06,20 --> 00:01:09,80
Then, we'll dig into what every developer needs to know,

25
00:01:09,80 --> 00:01:13,50
and dive deeper into the types of technical skills you need

26
00:01:13,50 --> 00:01:15,10
to be successful.

27
00:01:15,10 --> 00:01:17,50
Then we'll look at the options you have for learning

28
00:01:17,50 --> 00:01:19,00
about web development

29
00:01:19,00 --> 00:01:21,40
and discuss the always popular question,

30
00:01:21,40 --> 00:01:24,00
do I need to get a college degree?

31
00:01:24,00 --> 00:01:27,50
Finally, if you decide to pursue the career of a developer,

32
00:01:27,50 --> 00:01:29,00
I'll help you get a job.

33
00:01:29,00 --> 00:01:31,30
Well, I'll show you how to look for a job,

34
00:01:31,30 --> 00:01:34,30
the hiring process for developers and how to negotiate

35
00:01:34,30 --> 00:01:35,80
a great salary.

36
00:01:35,80 --> 00:01:38,50
Now being a web developer is a great career,

37
00:01:38,50 --> 00:01:40,30
that is highly in demand,

38
00:01:40,30 --> 00:01:42,80
so if you're interested in learning what it takes,

39
00:01:42,80 --> 00:01:44,00
let's get started.

