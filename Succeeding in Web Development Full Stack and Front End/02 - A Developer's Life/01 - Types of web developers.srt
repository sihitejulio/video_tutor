1
00:00:00,50 --> 00:00:01,80
- Jobs in web development

2
00:00:01,80 --> 00:00:04,70
are divided in two different categories.

3
00:00:04,70 --> 00:00:08,00
Most developers identify as being a combination

4
00:00:08,00 --> 00:00:11,30
of front-end or back-end developers.

5
00:00:11,30 --> 00:00:14,80
A front-end developer focuses on what's in front of a user,

6
00:00:14,80 --> 00:00:17,30
and that's usually some type of web browser

7
00:00:17,30 --> 00:00:20,40
like Google's Chrome, or Microsoft's Edge.

8
00:00:20,40 --> 00:00:22,40
Now about a third of web developers

9
00:00:22,40 --> 00:00:24,20
are front-end developers

10
00:00:24,20 --> 00:00:26,80
that deal with code that manages the interaction

11
00:00:26,80 --> 00:00:29,90
between the user and the browser.

12
00:00:29,90 --> 00:00:33,20
Most people think that is the design of the page,

13
00:00:33,20 --> 00:00:35,70
but it also means the interactions that happen

14
00:00:35,70 --> 00:00:38,30
between users and websites.

15
00:00:38,30 --> 00:00:41,20
Now as the market has changed over the years,

16
00:00:41,20 --> 00:00:43,90
front-end developers are expected to know more

17
00:00:43,90 --> 00:00:48,00
about interfacing with servers, as well as programming.

18
00:00:48,00 --> 00:00:49,70
About half of all developers

19
00:00:49,70 --> 00:00:52,50
identify as back-end developers.

20
00:00:52,50 --> 00:00:55,10
A back-end developer focuses on what happens

21
00:00:55,10 --> 00:00:58,90
behind the scenes on something called a server.

22
00:00:58,90 --> 00:01:00,80
The code for websites and applications

23
00:01:00,80 --> 00:01:03,80
often relies on groups of machines

24
00:01:03,80 --> 00:01:05,70
that live in the cloud.

25
00:01:05,70 --> 00:01:08,40
These machines run programs or apps

26
00:01:08,40 --> 00:01:10,50
that take care of certain tasks

27
00:01:10,50 --> 00:01:13,50
like serving up websites, or video.

28
00:01:13,50 --> 00:01:17,40
So back-end developers deal with setting up these machines,

29
00:01:17,40 --> 00:01:19,90
designing the structure of the information,

30
00:01:19,90 --> 00:01:22,50
and managing the apps and services

31
00:01:22,50 --> 00:01:24,90
that users communicate with.

32
00:01:24,90 --> 00:01:28,30
Now many developers identify as a third type

33
00:01:28,30 --> 00:01:30,60
called a full-stack developer.

34
00:01:30,60 --> 00:01:33,30
This is just a combination of a front-end developer

35
00:01:33,30 --> 00:01:35,30
and a back-end developer.

36
00:01:35,30 --> 00:01:37,70
In other words, this is someone that can handle

37
00:01:37,70 --> 00:01:41,80
both the front and the back end of web development.

38
00:01:41,80 --> 00:01:44,40
In reality, there's no perfect front-end

39
00:01:44,40 --> 00:01:45,60
or back-end developer,

40
00:01:45,60 --> 00:01:49,20
so all developers are somewhere in between.

41
00:01:49,20 --> 00:01:51,40
The notion of a full-stack developer,

42
00:01:51,40 --> 00:01:53,60
is somewhat of a unicorn.

43
00:01:53,60 --> 00:01:56,20
Now these are some of the typical job titles,

44
00:01:56,20 --> 00:01:58,80
and how prevalent they are in the industry.

45
00:01:58,80 --> 00:02:00,10
The important thing to remember

46
00:02:00,10 --> 00:02:04,10
is that you need to have a t-shaped strategy for learning.

47
00:02:04,10 --> 00:02:07,20
Developers tend to learn a wide variety of skills,

48
00:02:07,20 --> 00:02:10,80
but specialize in a few that they enjoy.

49
00:02:10,80 --> 00:02:12,80
Now it's also smart to specialize

50
00:02:12,80 --> 00:02:15,50
in what makes you more marketable.

51
00:02:15,50 --> 00:02:17,50
And that's why it's important to keep learning,

52
00:02:17,50 --> 00:02:21,00
because what's marketable changes constantly.

