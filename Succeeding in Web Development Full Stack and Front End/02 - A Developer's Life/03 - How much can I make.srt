1
00:00:00,90 --> 00:00:03,90
- A career as a developer can be very lucrative,

2
00:00:03,90 --> 00:00:05,70
and although that's not necessarily

3
00:00:05,70 --> 00:00:07,50
what you should focus on,

4
00:00:07,50 --> 00:00:11,60
it's good to know that it's in demand and pays well.

5
00:00:11,60 --> 00:00:12,80
According to LinkedIn,

6
00:00:12,80 --> 00:00:15,20
the median salary for a front-end developer

7
00:00:15,20 --> 00:00:19,00
in the U.S. is $74,900,

8
00:00:19,00 --> 00:00:23,40
while a back-end developer is higher at $85,000 a year.

9
00:00:23,40 --> 00:00:25,80
Now back-end development is often considered

10
00:00:25,80 --> 00:00:28,40
the more challenging of the practices,

11
00:00:28,40 --> 00:00:30,80
and thus reflected in salary.

12
00:00:30,80 --> 00:00:32,50
Much of your earning potential

13
00:00:32,50 --> 00:00:35,60
will vary depending on many factors,

14
00:00:35,60 --> 00:00:38,00
and that includes the size of the company,

15
00:00:38,00 --> 00:00:41,70
the popularity of languages or frameworks that you know,

16
00:00:41,70 --> 00:00:43,60
and years of experience.

17
00:00:43,60 --> 00:00:45,70
For example, employees at companies

18
00:00:45,70 --> 00:00:48,00
with more than 10,000 employees

19
00:00:48,00 --> 00:00:52,20
make 12% more than those at smaller companies.

20
00:00:52,20 --> 00:00:54,20
Developers with more experience

21
00:00:54,20 --> 00:00:57,60
also tend to make more money than newer developers.

22
00:00:57,60 --> 00:01:00,10
So it pays to stay in the industry.

23
00:01:00,10 --> 00:01:02,80
Now according to a Stack Overflow survey,

24
00:01:02,80 --> 00:01:05,10
the median salary for front-end developers

25
00:01:05,10 --> 00:01:09,60
with more than five years of experience was $97,000.

26
00:01:09,60 --> 00:01:11,50
I wouldn't recommend becoming a developer

27
00:01:11,50 --> 00:01:13,00
because of the money.

28
00:01:13,00 --> 00:01:15,60
A lot of development is problem-solving,

29
00:01:15,60 --> 00:01:17,50
so if you're not the type of person

30
00:01:17,50 --> 00:01:19,40
that enjoys solving problems,

31
00:01:19,40 --> 00:01:21,60
then this career will be tough

32
00:01:21,60 --> 00:01:23,90
in spite of the financial benefits.

33
00:01:23,90 --> 00:01:27,40
However, if you enjoy programming and problem-solving,

34
00:01:27,40 --> 00:01:31,00
this can be a meaningful and profitable career.

