1
00:00:00,70 --> 00:00:02,40
- Besides traditional job titles,

2
00:00:02,40 --> 00:00:05,00
developers share certain traits.

3
00:00:05,00 --> 00:00:07,70
Perhaps you're wondering if you'd make a good developer,

4
00:00:07,70 --> 00:00:09,20
so let's take a look at some

5
00:00:09,20 --> 00:00:11,80
common developer characteristics.

6
00:00:11,80 --> 00:00:15,70
Now overall, developers are pretty satisfied with their job,

7
00:00:15,70 --> 00:00:20,20
almost 75% are satisfied with their current position.

8
00:00:20,20 --> 00:00:21,90
Most of them also report

9
00:00:21,90 --> 00:00:24,20
that they are confident in their managers,

10
00:00:24,20 --> 00:00:26,80
and don't feel the need to become a manager

11
00:00:26,80 --> 00:00:30,10
in order to be fulfilled or to earn more money.

12
00:00:30,10 --> 00:00:33,90
Developers work inside and outside of teams,

13
00:00:33,90 --> 00:00:35,10
and in larger companies

14
00:00:35,10 --> 00:00:37,40
they tend to be part of larger groups

15
00:00:37,40 --> 00:00:41,10
which work on small parts of bigger projects.

16
00:00:41,10 --> 00:00:44,30
Most developers participate in code reviews,

17
00:00:44,30 --> 00:00:47,50
which means having other people look at your code

18
00:00:47,50 --> 00:00:50,30
in order to improve each other's work.

19
00:00:50,30 --> 00:00:52,70
The majority of professional developers

20
00:00:52,70 --> 00:00:55,70
also contribute to opensource projects.

21
00:00:55,70 --> 00:00:57,60
This means that they publish their work

22
00:00:57,60 --> 00:01:00,70
for everyone to see and contribute to.

23
00:01:00,70 --> 00:01:04,10
Although not all developers contribute to opensource,

24
00:01:04,10 --> 00:01:07,20
all web developers use opensource projects

25
00:01:07,20 --> 00:01:10,90
so they have to learn how to work with opensource code.

26
00:01:10,90 --> 00:01:13,20
It's important to get along with others,

27
00:01:13,20 --> 00:01:15,20
learn to appreciate critiques,

28
00:01:15,20 --> 00:01:19,10
and also be willing to help improve the work of others.

29
00:01:19,10 --> 00:01:21,20
Working in web development might be

30
00:01:21,20 --> 00:01:24,40
the biggest team sport you'll ever participate in.

31
00:01:24,40 --> 00:01:27,70
One of the most important characteristics of a developer

32
00:01:27,70 --> 00:01:30,10
is that they're lifelong learners.

33
00:01:30,10 --> 00:01:32,80
Almost 90% of all developers

34
00:01:32,80 --> 00:01:35,60
say they have taught themselves a new language,

35
00:01:35,60 --> 00:01:39,90
framework, or tool, outside of their formal education.

36
00:01:39,90 --> 00:01:41,90
Often, developers have to learn to use

37
00:01:41,90 --> 00:01:46,00
different types of tools that shift between projects.

38
00:01:46,00 --> 00:01:48,00
This is not a great job for someone

39
00:01:48,00 --> 00:01:50,10
who likes things to stay the same.

40
00:01:50,10 --> 00:01:53,50
But if you enjoy an atmosphere of continuous learning,

41
00:01:53,50 --> 00:01:55,80
then you'll really enjoy the challenge

42
00:01:55,80 --> 00:01:58,00
of being a developer.

