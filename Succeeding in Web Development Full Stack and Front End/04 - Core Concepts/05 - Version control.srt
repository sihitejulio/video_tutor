1
00:00:00,70 --> 00:00:04,10
- A lot of people have a misconception about developers.

2
00:00:04,10 --> 00:00:07,40
That we're all a bunch of loners, working in our rooms,

3
00:00:07,40 --> 00:00:10,20
on the next big Facebook or Google.

4
00:00:10,20 --> 00:00:14,20
In reality, most developers work as part of a team.

5
00:00:14,20 --> 00:00:16,30
To create high performing teams,

6
00:00:16,30 --> 00:00:19,90
developers make use of formalized processes

7
00:00:19,90 --> 00:00:21,40
and version control.

8
00:00:21,40 --> 00:00:25,70
One of these processes, Agile, focuses on producing

9
00:00:25,70 --> 00:00:28,50
working software and regrouping regularly

10
00:00:28,50 --> 00:00:30,70
to make sure that what you're doing

11
00:00:30,70 --> 00:00:32,70
aligns to customer needs.

12
00:00:32,70 --> 00:00:36,60
Now it works by scheduling a series of sprints

13
00:00:36,60 --> 00:00:40,20
where you build, test, and replan your work

14
00:00:40,20 --> 00:00:41,80
every few weeks.

15
00:00:41,80 --> 00:00:44,90
That's a big improvement from a waterfall approach,

16
00:00:44,90 --> 00:00:47,90
where all the planning is done at the beginning

17
00:00:47,90 --> 00:00:50,40
and takes most of the time.

18
00:00:50,40 --> 00:00:53,90
You may also hear about something called DevOps.

19
00:00:53,90 --> 00:00:57,20
It combines software development, the dev part,

20
00:00:57,20 --> 00:00:59,90
with operations, or ops.

21
00:00:59,90 --> 00:01:02,30
The goal is to shorten the time between

22
00:01:02,30 --> 00:01:05,00
writing the code and deployment.

23
00:01:05,00 --> 00:01:08,50
Whereas Agile focuses more on the communication

24
00:01:08,50 --> 00:01:10,80
between consumers and the product,

25
00:01:10,80 --> 00:01:14,20
DevOps focuses on how that software is deployed

26
00:01:14,20 --> 00:01:16,60
in the current environment.

27
00:01:16,60 --> 00:01:19,50
Today more software is being developed within

28
00:01:19,50 --> 00:01:22,40
a processes called Continuous Delivery.

29
00:01:22,40 --> 00:01:25,20
Many development teams are constantly shipping

30
00:01:25,20 --> 00:01:28,20
new features to existing software.

31
00:01:28,20 --> 00:01:31,00
The software goes through continuous testing,

32
00:01:31,00 --> 00:01:34,60
monitoring, and analyzing the success of the code.

33
00:01:34,60 --> 00:01:37,60
Good software should also be well documented

34
00:01:37,60 --> 00:01:40,80
so that other developers can understand your code.

35
00:01:40,80 --> 00:01:42,20
And honestly, sometimes,

36
00:01:42,20 --> 00:01:46,30
so that a future version of yourself can understand it too.

37
00:01:46,30 --> 00:01:48,20
If many people are working

38
00:01:48,20 --> 00:01:51,60
on the same piece of software at the same time,

39
00:01:51,60 --> 00:01:53,20
how do you manage and combine

40
00:01:53,20 --> 00:01:57,00
those changes to make a new version of the software?

41
00:01:57,00 --> 00:01:58,30
Now one of the most popular ways

42
00:01:58,30 --> 00:02:01,20
to manage software on the web is Git,

43
00:02:01,20 --> 00:02:03,70
which allows people to work on

44
00:02:03,70 --> 00:02:05,80
their own versions of projects

45
00:02:05,80 --> 00:02:07,70
and then merge changes together,

46
00:02:07,70 --> 00:02:10,00
identifying potential conflicts

47
00:02:10,00 --> 00:02:12,60
and providing a way to resolve them.

48
00:02:12,60 --> 00:02:16,50
This is typically referred to as Version Control.

49
00:02:16,50 --> 00:02:18,70
Now a good developer needs to be comfortable

50
00:02:18,70 --> 00:02:20,90
with not just how to code,

51
00:02:20,90 --> 00:02:25,00
but also the process of how software is made.

