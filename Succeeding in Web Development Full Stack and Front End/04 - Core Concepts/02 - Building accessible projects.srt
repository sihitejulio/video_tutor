1
00:00:02,30 --> 00:00:03,80
- Have you ever used the peeler with handles

2
00:00:03,80 --> 00:00:05,20
that were hard to use?

3
00:00:05,20 --> 00:00:06,80
Like this one right here.

4
00:00:06,80 --> 00:00:10,00
In 1990, OXO decided to redesign products

5
00:00:10,00 --> 00:00:11,80
so that they were easier for people

6
00:00:11,80 --> 00:00:14,00
with limited mobility use.

7
00:00:14,00 --> 00:00:17,10
They look more like this.

8
00:00:17,10 --> 00:00:19,50
Now these designs became best sellers

9
00:00:19,50 --> 00:00:21,80
because making something easier to use

10
00:00:21,80 --> 00:00:24,20
for these users had the side effect

11
00:00:24,20 --> 00:00:27,30
of making it easier to use for every user.

12
00:00:27,30 --> 00:00:29,50
One of the key skills web designers have

13
00:00:29,50 --> 00:00:32,40
to learn is to make things accessible

14
00:00:32,40 --> 00:00:34,80
not just because it's the right thing to do

15
00:00:34,80 --> 00:00:37,90
but also because it can effect the bottom line.

16
00:00:37,90 --> 00:00:39,40
Now people with disabilities

17
00:00:39,40 --> 00:00:42,40
use assisted technologies that help them navigate

18
00:00:42,40 --> 00:00:43,50
with their devices,

19
00:00:43,50 --> 00:00:45,40
and although these technologies

20
00:00:45,40 --> 00:00:47,90
can do a fair job by themselves,

21
00:00:47,90 --> 00:00:49,90
developers can effect how well these

22
00:00:49,90 --> 00:00:51,80
technologies actually work.

23
00:00:51,80 --> 00:00:55,00
In either a positive or negative way.

24
00:00:55,00 --> 00:00:56,90
In addition to the DOM the devices

25
00:00:56,90 --> 00:00:59,30
also create an accessibility trait

26
00:00:59,30 --> 00:01:01,50
Which provides additional information

27
00:01:01,50 --> 00:01:02,90
to these assistive tools.

28
00:01:02,90 --> 00:01:05,90
Including the state of the current application

29
00:01:05,90 --> 00:01:08,10
as well as the roles of different

30
00:01:08,10 --> 00:01:09,80
elements on your page.

31
00:01:09,80 --> 00:01:12,00
Now you can help these technologies first

32
00:01:12,00 --> 00:01:14,90
by learning to write HTML properly,

33
00:01:14,90 --> 00:01:16,80
and then by using ARIA

34
00:01:16,80 --> 00:01:19,70
or Accessible Rich Internet Application tags

35
00:01:19,70 --> 00:01:22,80
to further describe the roles of your elements.

36
00:01:22,80 --> 00:01:25,90
The Americans with Disabilities Act

37
00:01:25,90 --> 00:01:28,40
require that states and local governments

38
00:01:28,40 --> 00:01:31,30
provide equal access to all those

39
00:01:31,30 --> 00:01:32,60
who visit their sites.

40
00:01:32,60 --> 00:01:34,40
And failing to understand how best

41
00:01:34,40 --> 00:01:37,30
to serve everyone can have not just monetary

42
00:01:37,30 --> 00:01:40,50
but legal consequences in some instances.

43
00:01:40,50 --> 00:01:43,00
Making it easier to navigate a website

44
00:01:43,00 --> 00:01:44,70
through your keyboard makes it easier

45
00:01:44,70 --> 00:01:47,30
for everyone to navigate your site quicker.

46
00:01:47,30 --> 00:01:49,00
Giving focus on the right elements

47
00:01:49,00 --> 00:01:52,20
on the page for example, can help someone

48
00:01:52,20 --> 00:01:55,00
quickly navigate to search with their keyboard.

49
00:01:55,00 --> 00:01:57,20
And making sure fonts are easier to read

50
00:01:57,20 --> 00:02:00,90
by using a reasonable size plus having enough

51
00:02:00,90 --> 00:02:02,10
contrast and color

52
00:02:02,10 --> 00:02:04,40
makes it easier for people with less

53
00:02:04,40 --> 00:02:06,60
then perfect vision to get the right

54
00:02:06,60 --> 00:02:08,50
amount information.

55
00:02:08,50 --> 00:02:09,80
Have you ever been on a site where

56
00:02:09,80 --> 00:02:11,20
it's just hard to fill out a form

57
00:02:11,20 --> 00:02:13,40
because of the placement of form elements

58
00:02:13,40 --> 00:02:15,10
or the size of the links?

59
00:02:15,10 --> 00:02:18,70
Now people skip forms that aren't easy to work with

60
00:02:18,70 --> 00:02:21,00
and that can hurt your bottom line.

61
00:02:21,00 --> 00:02:23,70
Another thing to consider is how well your site works

62
00:02:23,70 --> 00:02:26,80
when using a mobile device, a phone verses

63
00:02:26,80 --> 00:02:28,80
a tablet, and a different orientations.

64
00:02:28,80 --> 00:02:31,20
There's lots of tools that you can use

65
00:02:31,20 --> 00:02:33,70
to improve accessibility on your sites.

66
00:02:33,70 --> 00:02:35,60
And one of my favorites its googles

67
00:02:35,60 --> 00:02:36,70
light house tool.

68
00:02:36,70 --> 00:02:38,90
Now this helps you identify potential

69
00:02:38,90 --> 00:02:41,20
accessibility as well as performance

70
00:02:41,20 --> 00:02:43,60
and gives you tips for best practices.

71
00:02:43,60 --> 00:02:46,80
Great companies and people who hire developers

72
00:02:46,80 --> 00:02:48,80
appreciate people that put time

73
00:02:48,80 --> 00:02:51,00
into creating accessible projects.

74
00:02:51,00 --> 00:02:52,00
Could make a difference

75
00:02:52,00 --> 00:02:56,00
between you and someone else getting the job you want.

