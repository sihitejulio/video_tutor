1
00:00:00,80 --> 00:00:02,40
- Developers approach how they program

2
00:00:02,40 --> 00:00:04,60
in a variety of ways, and some of these

3
00:00:04,60 --> 00:00:07,50
are related to the specific language they use.

4
00:00:07,50 --> 00:00:12,90
Now, although all browsers use HTML, CSS and JavaScript,

5
00:00:12,90 --> 00:00:16,20
how that combination of languages is arrived at

6
00:00:16,20 --> 00:00:17,90
isn't always straight forward.

7
00:00:17,90 --> 00:00:20,10
There's alternatives to these three,

8
00:00:20,10 --> 00:00:21,80
but you might be asking yourself,

9
00:00:21,80 --> 00:00:23,90
why would I want to write in a language

10
00:00:23,90 --> 00:00:25,50
other than these three?

11
00:00:25,50 --> 00:00:27,60
Well, the reason is, other languages

12
00:00:27,60 --> 00:00:30,10
offer more efficient solutions

13
00:00:30,10 --> 00:00:32,50
than these core three languages.

14
00:00:32,50 --> 00:00:35,80
Now, case in point, if you look at this SAS code,

15
00:00:35,80 --> 00:00:40,10
it generates an infinite amount of CSS code for buttons

16
00:00:40,10 --> 00:00:42,60
based on a list of colors.

17
00:00:42,60 --> 00:00:44,50
Now, you could spend more time writing

18
00:00:44,50 --> 00:00:47,70
and updating your HTML, CSS, and JavaScript,

19
00:00:47,70 --> 00:00:50,40
or you could use a language like this

20
00:00:50,40 --> 00:00:53,10
that gets compiled into something else,

21
00:00:53,10 --> 00:00:55,40
in this case, CSS.

22
00:00:55,40 --> 00:00:57,10
Now, a lot of languages, like SAS,

23
00:00:57,10 --> 00:01:00,60
are ways of simplifying one of the core languages,

24
00:01:00,60 --> 00:01:05,30
and will then translate into HTML, CSS, or JavaScript.

25
00:01:05,30 --> 00:01:08,50
Sometimes, we call these transpiled languages.

26
00:01:08,50 --> 00:01:11,00
Now, the other problem these languages can solve

27
00:01:11,00 --> 00:01:15,20
is how to be backwards compatible with older browsers.

28
00:01:15,20 --> 00:01:17,10
By using transpiled languages,

29
00:01:17,10 --> 00:01:20,70
you can use the latest versions and ideas available

30
00:01:20,70 --> 00:01:23,60
in newer languages, and use tools

31
00:01:23,60 --> 00:01:27,60
that convert that code, so that it works in older browsers.

32
00:01:27,60 --> 00:01:30,20
There's also different ideas as to how your code

33
00:01:30,20 --> 00:01:32,40
should be structured.

34
00:01:32,40 --> 00:01:35,80
The main programming language for the web, JavaScript,

35
00:01:35,80 --> 00:01:39,60
was originally designed to have a functional style.

36
00:01:39,60 --> 00:01:40,80
A functional style means

37
00:01:40,80 --> 00:01:43,30
the language is designed to be flexible

38
00:01:43,30 --> 00:01:47,80
and less structured, but not in anyway, less powerful.

39
00:01:47,80 --> 00:01:50,40
Over the years, JavaScript has incorporated

40
00:01:50,40 --> 00:01:53,60
object-oriented concepts and methodologies,

41
00:01:53,60 --> 00:01:56,00
and it's really a language,

42
00:01:56,00 --> 00:01:59,30
that is a hybrid of functional and object oriented concepts.

43
00:01:59,30 --> 00:02:02,30
Using JavaScript means that you need to learn the strengths

44
00:02:02,30 --> 00:02:06,40
and weaknesses of different styles and methodologies,

45
00:02:06,40 --> 00:02:09,00
and be flexible in how you write code.

46
00:02:09,00 --> 00:02:12,20
Now, MVC is a popular design concept

47
00:02:12,20 --> 00:02:14,10
you'll likely come across.

48
00:02:14,10 --> 00:02:17,30
This involves treating the data or the model

49
00:02:17,30 --> 00:02:19,10
as a separate entity,

50
00:02:19,10 --> 00:02:22,50
and then creating a template, which is called the view,

51
00:02:22,50 --> 00:02:24,20
that uses that data.

52
00:02:24,20 --> 00:02:27,40
And finally, writing code, called the controller

53
00:02:27,40 --> 00:02:31,00
that combines the data and the view together.

54
00:02:31,00 --> 00:02:32,50
Another way of organizing code

55
00:02:32,50 --> 00:02:35,00
is a component-based architecture.

56
00:02:35,00 --> 00:02:37,50
That means separating functionality

57
00:02:37,50 --> 00:02:39,80
into self-contained pieces,

58
00:02:39,80 --> 00:02:43,10
so you can build flexible, and reusable parts,

59
00:02:43,10 --> 00:02:46,30
that can be shared between applications.

60
00:02:46,30 --> 00:02:49,00
Another process aimed at reducing errors,

61
00:02:49,00 --> 00:02:52,00
is building tests for your applications.

62
00:02:52,00 --> 00:02:53,60
This can be really useful,

63
00:02:53,60 --> 00:02:55,80
because errors are hard to debug,

64
00:02:55,80 --> 00:02:59,40
when applications start getting very large.

65
00:02:59,40 --> 00:03:02,10
By defining tests, you make sure that your process

66
00:03:02,10 --> 00:03:04,30
constantly checks for problems

67
00:03:04,30 --> 00:03:07,20
in all parts of your development cycle.

68
00:03:07,20 --> 00:03:09,10
Now, you need to learn about these different styles

69
00:03:09,10 --> 00:03:11,40
of coding, but most importantly,

70
00:03:11,40 --> 00:03:13,50
you need to be flexible.

71
00:03:13,50 --> 00:03:15,10
You'll be working with different teams

72
00:03:15,10 --> 00:03:18,50
and different approaches, sometimes even

73
00:03:18,50 --> 00:03:20,10
within the same company.

74
00:03:20,10 --> 00:03:23,20
Your ability to be flexible and embrace different approaches

75
00:03:23,20 --> 00:03:27,00
will effect your career success and satisfaction.

