1
00:00:00,70 --> 00:00:02,70
- The technology that made the web possible

2
00:00:02,70 --> 00:00:08,10
is a combination of two protocols called TCP and IP.

3
00:00:08,10 --> 00:00:11,30
Before this technology took off, most communications

4
00:00:11,30 --> 00:00:14,10
between computers happened by computers

5
00:00:14,10 --> 00:00:16,70
that were directly connected to one another.

6
00:00:16,70 --> 00:00:18,80
It's sort of what happens when you get one of those

7
00:00:18,80 --> 00:00:20,70
older strands of Christmas lights.

8
00:00:20,70 --> 00:00:23,50
If just one of them in the sequence was broken,

9
00:00:23,50 --> 00:00:25,30
then none of them would work.

10
00:00:25,30 --> 00:00:28,30
Now, TCP IP allows computers to form

11
00:00:28,30 --> 00:00:30,20
different types of connections.

12
00:00:30,20 --> 00:00:31,80
This creates connections that look

13
00:00:31,80 --> 00:00:34,50
more like spiderwebs than straight lines.

14
00:00:34,50 --> 00:00:36,80
There's two ingenious things happening here.

15
00:00:36,80 --> 00:00:41,60
First, the messages aren't all sent as a single, huge file.

16
00:00:41,60 --> 00:00:44,90
They're broken down into little pieces called packets.

17
00:00:44,90 --> 00:00:47,10
Now, that's the TCP part.

18
00:00:47,10 --> 00:00:49,60
The advantage of the packets is that data can travel

19
00:00:49,60 --> 00:00:53,00
through the best available paths at any moment.

20
00:00:53,00 --> 00:00:55,80
This is like when you place a big order from Amazon,

21
00:00:55,80 --> 00:00:58,40
and the packages that make up the order

22
00:00:58,40 --> 00:01:00,50
might arrive at different times

23
00:01:00,50 --> 00:01:02,90
and be sent from different places.

24
00:01:02,90 --> 00:01:05,10
But how do the packages know where to go?

25
00:01:05,10 --> 00:01:07,10
That's what the IP part does.

26
00:01:07,10 --> 00:01:09,40
It's a series of numbers that describes

27
00:01:09,40 --> 00:01:11,90
the location of the packages.

28
00:01:11,90 --> 00:01:14,70
Now, think of them as the address of your house.

29
00:01:14,70 --> 00:01:17,10
Your ZIP code tells the post office

30
00:01:17,10 --> 00:01:20,10
the general location of where you are in the country,

31
00:01:20,10 --> 00:01:23,40
and then the street address gets more specific.

32
00:01:23,40 --> 00:01:25,80
Most of the time, when we're working on the web,

33
00:01:25,80 --> 00:01:27,50
we don't see these numbers.

34
00:01:27,50 --> 00:01:31,80
But instead use names like Google.com or LinkedIn.com.

35
00:01:31,80 --> 00:01:34,30
But those names get translated into numbers

36
00:01:34,30 --> 00:01:36,20
by a sort of address book system

37
00:01:36,20 --> 00:01:39,80
called the Domain Name Servers, or DNS.

38
00:01:39,80 --> 00:01:42,90
Now, these are simply machines that manage translating

39
00:01:42,90 --> 00:01:47,80
the names that you type into a browser, into IP addresses.

40
00:01:47,80 --> 00:01:50,50
Now those can be located by computers

41
00:01:50,50 --> 00:01:53,80
and hackers can sometimes target the DNS servers

42
00:01:53,80 --> 00:01:56,10
because it's a way to overwhelm access

43
00:01:56,10 --> 00:01:58,40
to more than one website.

44
00:01:58,40 --> 00:02:00,70
The Internet uses a notation called

45
00:02:00,70 --> 00:02:04,50
URIs or Uniform Resource Identifiers.

46
00:02:04,50 --> 00:02:09,30
And they're also known as URLs or Uniform Resource Locators.

47
00:02:09,30 --> 00:02:13,30
When you type in an address, like https://linkedin.com,

48
00:02:13,30 --> 00:02:17,60
you're identifying that you want to use the ACTPS protocol

49
00:02:17,60 --> 00:02:20,30
and that you want to locate the LinkedIn domain

50
00:02:20,30 --> 00:02:23,20
using the dot com extension.

51
00:02:23,20 --> 00:02:26,30
A DNS server will translate that into a number

52
00:02:26,30 --> 00:02:28,70
and send the request to a server

53
00:02:28,70 --> 00:02:31,30
that returns back a website.

54
00:02:31,30 --> 00:02:34,50
URIs can request information from different ports.

55
00:02:34,50 --> 00:02:37,10
Now, these are like T.V. or radio channels,

56
00:02:37,10 --> 00:02:40,40
so although you're enjoying a show on the same television,

57
00:02:40,40 --> 00:02:43,90
the information can be coming from different channels.

58
00:02:43,90 --> 00:02:45,90
Hackers can scan these ports to see

59
00:02:45,90 --> 00:02:48,50
what services a company is offering

60
00:02:48,50 --> 00:02:50,40
and attempt to upload files

61
00:02:50,40 --> 00:02:53,20
or exploit other vulnerabilities.

62
00:02:53,20 --> 00:02:56,50
Like the DNS server, there is a bunch of information

63
00:02:56,50 --> 00:02:58,70
that passes between client and servers.

64
00:02:58,70 --> 00:03:02,90
One of these pieces is something called HTTP Headers.

65
00:03:02,90 --> 00:03:05,20
Now, think of them as the tags that you get

66
00:03:05,20 --> 00:03:06,70
when you purchase clothing.

67
00:03:06,70 --> 00:03:08,50
It's something that you probably ignore,

68
00:03:08,50 --> 00:03:10,50
but it's there and can provide

69
00:03:10,50 --> 00:03:13,20
a lot of information to hackers.

70
00:03:13,20 --> 00:03:16,00
These headers can also have useful information

71
00:03:16,00 --> 00:03:18,60
when working with servers on the back end.

72
00:03:18,60 --> 00:03:22,10
Developers can often read and manage this information

73
00:03:22,10 --> 00:03:24,00
to make their sites more secure.

