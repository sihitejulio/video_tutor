1
00:00:00,70 --> 00:00:01,70
- One of the huge changes

2
00:00:01,70 --> 00:00:04,80
on how we approach web design came as a result

3
00:00:04,80 --> 00:00:08,00
of the explosion of cell phones and tablets.

4
00:00:08,00 --> 00:00:10,30
Designers had to radically change the way

5
00:00:10,30 --> 00:00:12,80
that we thought about web design.

6
00:00:12,80 --> 00:00:15,50
Now, when I design for newspapers, magazines,

7
00:00:15,50 --> 00:00:18,80
or really any other media, I always design

8
00:00:18,80 --> 00:00:20,40
for a fixed size.

9
00:00:20,40 --> 00:00:21,90
So, one of my first questions

10
00:00:21,90 --> 00:00:24,40
was always the size of the project.

11
00:00:24,40 --> 00:00:28,30
On the web, we don't design for any specific sizes.

12
00:00:28,30 --> 00:00:32,00
Almost every device has its own size and proportions.

13
00:00:32,00 --> 00:00:36,00
So, on the web, we design for flexible proportions.

14
00:00:36,00 --> 00:00:39,80
At certain sizes, the layouts have to completely change

15
00:00:39,80 --> 00:00:42,90
to accommodate both the size and the proportions

16
00:00:42,90 --> 00:00:44,30
of the device.

17
00:00:44,30 --> 00:00:46,00
Also, websites don't always have

18
00:00:46,00 --> 00:00:49,40
to show the same information in all devices.

19
00:00:49,40 --> 00:00:51,90
A methodology called Mobile First

20
00:00:51,90 --> 00:00:54,90
teaches that you should always seek to simplify

21
00:00:54,90 --> 00:00:59,10
what is being displayed, especially on smaller devices.

22
00:00:59,10 --> 00:01:01,00
One of the basic ways that you can control

23
00:01:01,00 --> 00:01:03,40
how items appear on the page

24
00:01:03,40 --> 00:01:06,60
is by modifying its display model.

25
00:01:06,60 --> 00:01:09,10
Every element on the page is displayed

26
00:01:09,10 --> 00:01:12,20
using a property called the display property,

27
00:01:12,20 --> 00:01:15,20
and that controls its size, proportions,

28
00:01:15,20 --> 00:01:18,30
and how they behave next to other objects.

29
00:01:18,30 --> 00:01:20,30
Now, there's a separate Positioning Model

30
00:01:20,30 --> 00:01:24,30
that controls how elements flow in relation to one another.

31
00:01:24,30 --> 00:01:26,60
The default mode is that objects appear

32
00:01:26,60 --> 00:01:29,30
in the order that the code was written,

33
00:01:29,30 --> 00:01:32,30
but they can also appear relative to other objects

34
00:01:32,30 --> 00:01:36,10
or have a position that is relative to the device.

35
00:01:36,10 --> 00:01:38,70
Next, Flexbox presented a huge leap

36
00:01:38,70 --> 00:01:41,40
on how designers approach web design.

37
00:01:41,40 --> 00:01:43,90
For the first time, containers were given the ability

38
00:01:43,90 --> 00:01:47,50
to alter dimensions to fit the available space.

39
00:01:47,50 --> 00:01:51,00
The last huge upgrade to how we approach design on the web

40
00:01:51,00 --> 00:01:53,30
is the Grid Layout system.

41
00:01:53,30 --> 00:01:56,50
It's a two-dimensional system that, unlike Flexbox,

42
00:01:56,50 --> 00:02:00,80
allows for careful control of both rows and columns.

43
00:02:00,80 --> 00:02:03,30
Now, as your CSS grows, there's another concern

44
00:02:03,30 --> 00:02:04,80
that you'll have to deal with,

45
00:02:04,80 --> 00:02:08,30
that's how to properly organize all your CSS.

46
00:02:08,30 --> 00:02:11,40
Designers on the web deal with this in a variety of ways,

47
00:02:11,40 --> 00:02:16,30
and you may hear names like OOCSS, SMACSS, and BEM.

48
00:02:16,30 --> 00:02:18,10
And although these names are different,

49
00:02:18,10 --> 00:02:22,80
they are a simple way of naming and organizing your CSS.

50
00:02:22,80 --> 00:02:23,90
Now, you can see how this works

51
00:02:23,90 --> 00:02:26,30
in the popular Bootstrap framework

52
00:02:26,30 --> 00:02:28,70
and how it names its card component.

53
00:02:28,70 --> 00:02:30,90
The main card is just called card,

54
00:02:30,90 --> 00:02:32,30
but there are other elements,

55
00:02:32,30 --> 00:02:36,20
like card-body, card-title, and card-text.

56
00:02:36,20 --> 00:02:37,80
They're inside the main card

57
00:02:37,80 --> 00:02:40,60
and help define its internal elements.

58
00:02:40,60 --> 00:02:42,60
Now, designing for the web is one of those things

59
00:02:42,60 --> 00:02:45,40
that has radically changed in just a few years,

60
00:02:45,40 --> 00:02:49,00
and the future is going to bring additional changes.

