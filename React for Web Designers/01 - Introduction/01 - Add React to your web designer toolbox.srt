1
00:00:00,40 --> 00:00:02,60
- [Instructor] Hi I'm Joe Chellman and welcome

2
00:00:02,60 --> 00:00:04,80
to React for Web Designers.

3
00:00:04,80 --> 00:00:07,40
React is super popular and if you're interested in web

4
00:00:07,40 --> 00:00:09,30
design or have been working in the business, you've

5
00:00:09,30 --> 00:00:11,60
undoubtedly heard a little bit about it.

6
00:00:11,60 --> 00:00:13,80
React is a user interface library that can be used

7
00:00:13,80 --> 00:00:17,00
in the creation of websites, mobile apps, and more.

8
00:00:17,00 --> 00:00:19,30
What you might not know is that you don't have to dive

9
00:00:19,30 --> 00:00:21,70
into React head-first building only new projects

10
00:00:21,70 --> 00:00:23,10
out of (mumbles).

11
00:00:23,10 --> 00:00:25,30
You can use React as part of existing sites without any

12
00:00:25,30 --> 00:00:27,60
complicated build processes.

13
00:00:27,60 --> 00:00:30,00
That's exactly what we'll be doing in this course.

14
00:00:30,00 --> 00:00:33,00
First, I'll look at some of the conceptual aspects

15
00:00:33,00 --> 00:00:35,20
of React to help us get our bearings.

16
00:00:35,20 --> 00:00:37,70
Then we'll work through a series of three small websites

17
00:00:37,70 --> 00:00:40,50
incorporating React components into each one.

18
00:00:40,50 --> 00:00:42,90
We'll create a simple product customizer to get acquainted

19
00:00:42,90 --> 00:00:45,30
to managing basic state and props and how React

20
00:00:45,30 --> 00:00:46,30
handles events.

21
00:00:46,30 --> 00:00:49,80
Next we'll work on a searchable filterable directory

22
00:00:49,80 --> 00:00:52,00
of people for a small company, which will include

23
00:00:52,00 --> 00:00:54,60
controlled forms and simple animation.

24
00:00:54,60 --> 00:00:57,00
Our final project will be a small messaging app

25
00:00:57,00 --> 00:00:59,80
that communicates with a simple API using asynchronous calls

26
00:00:59,80 --> 00:01:02,80
over the network and React's lifecycle methods.

27
00:01:02,80 --> 00:01:05,70
All in all you'll see much of what makes React interesting

28
00:01:05,70 --> 00:01:07,90
and be able to add it to your web design toolbox

29
00:01:07,90 --> 00:01:11,00
without a lot of headaches, let's check it out.

