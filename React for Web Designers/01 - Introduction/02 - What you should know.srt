1
00:00:00,40 --> 00:00:01,70
- [Instructor] Let's talk about what you should know

2
00:00:01,70 --> 00:00:04,00
coming into this course.

3
00:00:04,00 --> 00:00:06,80
First of all, you should understand HTML and CSS

4
00:00:06,80 --> 00:00:09,80
at at least an intermediate level or better.

5
00:00:09,80 --> 00:00:11,30
And the same thing with JavaScript.

6
00:00:11,30 --> 00:00:13,70
You should have at least intermediate familiarity

7
00:00:13,70 --> 00:00:16,60
with ES5, that is, the version of JavaScript

8
00:00:16,60 --> 00:00:19,00
that's been around for many, many years at this point,

9
00:00:19,00 --> 00:00:20,30
or better.

10
00:00:20,30 --> 00:00:22,20
You don't have to know ES6,

11
00:00:22,20 --> 00:00:24,70
but if you know that there is such a thing called ES6,

12
00:00:24,70 --> 00:00:25,70
know that you will get a little bit

13
00:00:25,70 --> 00:00:28,30
of exposure to it in this course.

14
00:00:28,30 --> 00:00:29,80
Really, you should be pretty proficient

15
00:00:29,80 --> 00:00:33,20
at all these technologies to get started working with React.

16
00:00:33,20 --> 00:00:35,40
If you're not comfortable with these technologies,

17
00:00:35,40 --> 00:00:37,80
there are an absolute plethora of courses available

18
00:00:37,80 --> 00:00:38,70
in the library,

19
00:00:38,70 --> 00:00:40,60
from relatively brief getting started videos

20
00:00:40,60 --> 00:00:43,00
all the way to comprehensive essential trainings.

