1
00:00:00,50 --> 00:00:02,30
- [Instructor] The exercise files for this course

2
00:00:02,30 --> 00:00:04,80
include everything you'll need to follow along.

3
00:00:04,80 --> 00:00:06,80
If you're just watching and following along,

4
00:00:06,80 --> 00:00:10,20
there are folders for each chapter, and for each video,

5
00:00:10,20 --> 00:00:11,90
including all the files as they stand

6
00:00:11,90 --> 00:00:14,20
at the start and end of each video.

7
00:00:14,20 --> 00:00:17,30
You can open those and read through them at your own pace.

8
00:00:17,30 --> 00:00:19,20
If you're planning to type along with me, and I recommend

9
00:00:19,20 --> 00:00:21,90
that you do, there's also a work in progress folder,

10
00:00:21,90 --> 00:00:24,50
W-I-P, included in each chapter folder

11
00:00:24,50 --> 00:00:27,50
with files as they should begin in each chapter.

12
00:00:27,50 --> 00:00:29,60
You can write all the same code I am,

13
00:00:29,60 --> 00:00:32,30
and just go through each video one by one.

14
00:00:32,30 --> 00:00:34,10
There will be a few instances where I've included

15
00:00:34,10 --> 00:00:36,30
a snippet of code, or an HTML to be pasted in,

16
00:00:36,30 --> 00:00:38,10
which you'll find in the start folder

17
00:00:38,10 --> 00:00:39,40
for the relevant video.

18
00:00:39,40 --> 00:00:42,10
I'll call them out as we get to them.

19
00:00:42,10 --> 00:00:44,10
The note modules folder included in this course

20
00:00:44,10 --> 00:00:47,10
has copies of React and other libraries we'll be using,

21
00:00:47,10 --> 00:00:50,20
so you can work through the projects while you're offline.

22
00:00:50,20 --> 00:00:52,40
Also note that if you use the package managers

23
00:00:52,40 --> 00:00:55,90
N-P-M or yarn, those command line tools,

24
00:00:55,90 --> 00:00:57,80
there is a package dot J file included,

25
00:00:57,80 --> 00:01:00,60
so you can use NPM or yarn to update

26
00:01:00,60 --> 00:01:02,00
the libraries we're using.

