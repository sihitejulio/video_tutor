1
00:00:00,50 --> 00:00:01,30
- [Instructor] The next step we'll

2
00:00:01,30 --> 00:00:03,80
be taking is updating our little app's state based on

3
00:00:03,80 --> 00:00:05,90
what happens in our filter form here.

4
00:00:05,90 --> 00:00:07,80
But before we do that let's take a brief pause

5
00:00:07,80 --> 00:00:09,20
to talk about the two main ways

6
00:00:09,20 --> 00:00:12,40
that you can work with forms in React.

7
00:00:12,40 --> 00:00:13,70
Specifically, we're going to be talking

8
00:00:13,70 --> 00:00:18,40
about controlled versus uncontrolled form elements.

9
00:00:18,40 --> 00:00:20,70
What we're asking here is who is the source

10
00:00:20,70 --> 00:00:23,50
of truth for the form data?

11
00:00:23,50 --> 00:00:25,90
It can either be React state

12
00:00:25,90 --> 00:00:30,30
or it can be the DOM elements that comprise the form.

13
00:00:30,30 --> 00:00:33,20
First, we'll talk about controlled components.

14
00:00:33,20 --> 00:00:36,50
A controlled form component is one where React is the source

15
00:00:36,50 --> 00:00:38,20
of truth or more specifically,

16
00:00:38,20 --> 00:00:42,20
React's state is the source of truth.

17
00:00:42,20 --> 00:00:43,50
That means that your code controls

18
00:00:43,50 --> 00:00:45,90
whether the form value changes.

19
00:00:45,90 --> 00:00:49,80
So, for example, you can do validation in your React code

20
00:00:49,80 --> 00:00:52,00
and then not update the state of the element

21
00:00:52,00 --> 00:00:55,10
that you're validating and the value won't change

22
00:00:55,10 --> 00:00:57,60
no matter what the user has typed.

23
00:00:57,60 --> 00:01:00,30
When you're working with controlled components every piece

24
00:01:00,30 --> 00:01:02,60
of state works exactly the same way

25
00:01:02,60 --> 00:01:05,30
whether it's forms or other pieces of state.

26
00:01:05,30 --> 00:01:08,30
So, that consistency can be nice.

27
00:01:08,30 --> 00:01:09,90
This is the approach that's recommended

28
00:01:09,90 --> 00:01:12,60
in the React documentation, so that helps

29
00:01:12,60 --> 00:01:14,90
when you're looking at example code.

30
00:01:14,90 --> 00:01:16,20
The one possible drawback is

31
00:01:16,20 --> 00:01:18,90
that you write a little bit more code.

32
00:01:18,90 --> 00:01:21,60
And, when I say more code, it's not a ton more code,

33
00:01:21,60 --> 00:01:23,80
but as the documentation says, you end up needing

34
00:01:23,80 --> 00:01:27,20
to write a handler for every way that you're data can change

35
00:01:27,20 --> 00:01:29,60
and everything needs to go through React components.

36
00:01:29,60 --> 00:01:30,90
That can be a little inconvenient

37
00:01:30,90 --> 00:01:34,50
if you're integrating React with an existing code base.

38
00:01:34,50 --> 00:01:37,80
The other way to go about it is uncontrolled components.

39
00:01:37,80 --> 00:01:39,80
This is what we've seen previously.

40
00:01:39,80 --> 00:01:41,10
This is a more familiar approach

41
00:01:41,10 --> 00:01:43,70
where the DOM is the source of truth.

42
00:01:43,70 --> 00:01:46,70
So, if you want to find out what's in a form element,

43
00:01:46,70 --> 00:01:48,50
you find that form element on the page

44
00:01:48,50 --> 00:01:51,40
and you ask it what's in it right now.

45
00:01:51,40 --> 00:01:52,60
It's a little bit less code

46
00:01:52,60 --> 00:01:55,80
to write, not a lot, but a little bit.

47
00:01:55,80 --> 00:01:58,50
You end up using a feature of React called refs

48
00:01:58,50 --> 00:02:00,20
and you might set a default value

49
00:02:00,20 --> 00:02:02,90
as we saw in the previous chapter.

50
00:02:02,90 --> 00:02:05,20
This approach can be easier when porting existing sites

51
00:02:05,20 --> 00:02:07,80
to React, but it does mean

52
00:02:07,80 --> 00:02:11,60
that you can't enforce field values as easily.

53
00:02:11,60 --> 00:02:13,30
When you're controlling your form elements

54
00:02:13,30 --> 00:02:17,40
using React state, you have a little bit tighter control,

55
00:02:17,40 --> 00:02:20,60
hence the name controlled components.

56
00:02:20,60 --> 00:02:22,90
This approach can make it a little confusing about

57
00:02:22,90 --> 00:02:24,70
who owns what.

58
00:02:24,70 --> 00:02:27,50
So, that's one reason why the React community

59
00:02:27,50 --> 00:02:30,50
generally recommends that you go with controlled components

60
00:02:30,50 --> 00:02:32,10
and it's the approach we're going to take

61
00:02:32,10 --> 00:02:34,80
as we build out our filter form.

62
00:02:34,80 --> 00:02:36,60
But depending on the project that you're working on,

63
00:02:36,60 --> 00:02:38,20
you may find that one approach

64
00:02:38,20 --> 00:02:41,70
or the other could work a little better for you.

65
00:02:41,70 --> 00:02:43,60
There are sections in the React documentation

66
00:02:43,60 --> 00:02:46,60
for each of these and you can read up on both topics

67
00:02:46,60 --> 00:02:48,90
to understand them in more detail.

68
00:02:48,90 --> 00:02:50,70
But now we're going to proceed building up

69
00:02:50,70 --> 00:02:53,00
controlled components for our filter form.

