1
00:00:00,50 --> 00:00:02,20
- [Instructor] Now that we have our directory component,

2
00:00:02,20 --> 00:00:05,20
which has our state and the whole wrapping structure

3
00:00:05,20 --> 00:00:07,90
that we're going to place our other components in,

4
00:00:07,90 --> 00:00:10,80
let's build up those other components.

5
00:00:10,80 --> 00:00:14,10
Specifically we'll build up filters, people,

6
00:00:14,10 --> 00:00:18,00
and the person component that isn't even mentioned so far.

7
00:00:18,00 --> 00:00:20,70
So we'll create these as function components.

8
00:00:20,70 --> 00:00:27,90
We'll start with the filters component.

9
00:00:27,90 --> 00:00:31,10
Function filters with props as the argument,

10
00:00:31,10 --> 00:00:33,10
and we're going to do something super simple here.

11
00:00:33,10 --> 00:00:34,30
We're just going to return a little block

12
00:00:34,30 --> 00:00:35,90
of static java script for now.

13
00:00:35,90 --> 00:00:39,40
We're going to deal with this component a little more later.

14
00:00:39,40 --> 00:00:43,00
Let's go into our file, and grab some markup.

15
00:00:43,00 --> 00:00:47,30
So I'll just grab this form tag.

16
00:00:47,30 --> 00:00:50,60
That'll be our wrapper.

17
00:00:50,60 --> 00:00:54,10
And then when we define the actual elements in here,

18
00:00:54,10 --> 00:00:55,70
they'll go in this place.

19
00:00:55,70 --> 00:00:57,60
For now we'll just say filters go here.

20
00:00:57,60 --> 00:00:59,20
That's enough.

21
00:00:59,20 --> 00:01:04,30
Next we'll add our people component.

22
00:01:04,30 --> 00:01:09,10
This is going to return mostly another block of JSX,

23
00:01:09,10 --> 00:01:12,30
but because we're passing in our people prop,

24
00:01:12,30 --> 00:01:14,40
we're going to make use of that.

25
00:01:14,40 --> 00:01:17,50
So first we'll add a wrapper for these,

26
00:01:17,50 --> 00:01:20,70
call it results.

27
00:01:20,70 --> 00:01:25,40
If we refer back to the mock-up,

28
00:01:25,40 --> 00:01:28,60
we can see that there is no wrapping element on these.

29
00:01:28,60 --> 00:01:33,00
It wasn't really needed initially,

30
00:01:33,00 --> 00:01:35,60
but there's no harm in wrapping it in another div.

31
00:01:35,60 --> 00:01:37,40
We just want to make sure that we use class name

32
00:01:37,40 --> 00:01:39,50
instead of simply class.

33
00:01:39,50 --> 00:01:42,50
Now before, we've rendered a series of elements

34
00:01:42,50 --> 00:01:43,80
using a helper function.

35
00:01:43,80 --> 00:01:46,30
We can also just do it directly,

36
00:01:46,30 --> 00:01:49,50
as an expression inside this initial element.

37
00:01:49,50 --> 00:01:53,00
So, we'll refer to props.people,

38
00:01:53,00 --> 00:01:56,40
and now we'll use map again.

39
00:01:56,40 --> 00:01:57,60
If we refer back to the data,

40
00:01:57,60 --> 00:02:00,30
we can see that people is an array,

41
00:02:00,30 --> 00:02:05,70
and every piece of person data has these various fields.

42
00:02:05,70 --> 00:02:08,00
Now in the people component, it's main job

43
00:02:08,00 --> 00:02:11,90
is just to wrap up each of these person elements.

44
00:02:11,90 --> 00:02:13,10
So we're just going to create those

45
00:02:13,10 --> 00:02:18,30
inside the callback we're defining inside our map function.

46
00:02:18,30 --> 00:02:20,70
So the argument is going to be person,

47
00:02:20,70 --> 00:02:22,70
which is going to refer to an individual object

48
00:02:22,70 --> 00:02:28,10
in this people array.

49
00:02:28,10 --> 00:02:32,90
And so we're going to return a person,

50
00:02:32,90 --> 00:02:35,10
and recall that we want to set a key

51
00:02:35,10 --> 00:02:37,70
on each of these items.

52
00:02:37,70 --> 00:02:41,70
So we'll use the I.D. field from person.

53
00:02:41,70 --> 00:02:43,00
If we look back at the data,

54
00:02:43,00 --> 00:02:46,50
we can see that there's a unique I.D. on each of these,

55
00:02:46,50 --> 00:02:50,50
so we might as well use it.

56
00:02:50,50 --> 00:02:53,70
And then we'll pass in a person prop,

57
00:02:53,70 --> 00:02:56,30
which has this data.

58
00:02:56,30 --> 00:02:59,70
Just the whole blob, sent right in there.

59
00:02:59,70 --> 00:03:03,10
That should do us just fine for people.

60
00:03:03,10 --> 00:03:05,40
Now finally, let's create the person component,

61
00:03:05,40 --> 00:03:08,10
once again with props.

62
00:03:08,10 --> 00:03:11,90
This is going to be another big blob of JSX.

63
00:03:11,90 --> 00:03:15,70
And we're going to steal it right out of the mockup.

64
00:03:15,70 --> 00:03:19,80
So here is a person,

65
00:03:19,80 --> 00:03:25,70
an H3 tag followed by a P tag, with an image and their bio.

66
00:03:25,70 --> 00:03:27,70
So we can just paste that right in here.

67
00:03:27,70 --> 00:03:32,80
And we're going to adapt it into JSX.

68
00:03:32,80 --> 00:03:35,40
So instead of keeping this static name,

69
00:03:35,40 --> 00:03:37,80
we're going to use the props that are coming in.

70
00:03:37,80 --> 00:03:39,50
We're going to substitute all these expressions

71
00:03:39,50 --> 00:03:41,10
inside curly braces.

72
00:03:41,10 --> 00:03:47,30
So we'll say props.person.name for the name,

73
00:03:47,30 --> 00:03:51,20
copy and paste this over the title.

74
00:03:51,20 --> 00:03:52,90
And once again, referring back to the data,

75
00:03:52,90 --> 00:03:56,20
we know we're going to need the name, the title,

76
00:03:56,20 --> 00:04:02,40
the bio, and the image.

77
00:04:02,40 --> 00:04:07,10
So we'll use the title here,

78
00:04:07,10 --> 00:04:09,70
get rid of all this bio stuff,

79
00:04:09,70 --> 00:04:12,80
and replace that with person.bio.

80
00:04:12,80 --> 00:04:15,30
And now we need to do some work on this image tag.

81
00:04:15,30 --> 00:04:21,00
So first of all we'll change class to class name.

82
00:04:21,00 --> 00:04:22,00
We'll get rid of this class

83
00:04:22,00 --> 00:04:24,80
that WordPress inserts per image,

84
00:04:24,80 --> 00:04:28,00
and just leave the ones that are generic.

85
00:04:28,00 --> 00:04:29,80
For the source, we're going to get rid of

86
00:04:29,80 --> 00:04:32,80
all this static stuff, and replace it with an expression,

87
00:04:32,80 --> 00:04:38,80
which is props.person.image.

88
00:04:38,80 --> 00:04:46,20
For the alt, we can use the name again.

89
00:04:46,20 --> 00:04:48,70
We can leave the width and the height just as they are,

90
00:04:48,70 --> 00:04:54,50
and leave this sizes definition just as it is, as well.

91
00:04:54,50 --> 00:04:56,30
Now we can save this,

92
00:04:56,30 --> 00:04:59,40
and I can see that we have an error here.

93
00:04:59,40 --> 00:05:01,30
Visual studio code is handy enough

94
00:05:01,30 --> 00:05:04,10
to tell me that I need to include my parent element

95
00:05:04,10 --> 00:05:08,80
for all of my JSX expressions.

96
00:05:08,80 --> 00:05:10,90
So once again, that means that we can't just have

97
00:05:10,90 --> 00:05:15,40
two side by side JSX expressions as we have here.

98
00:05:15,40 --> 00:05:17,60
We could return them as an array,

99
00:05:17,60 --> 00:05:20,20
we could use fragments, which are a relatively recent

100
00:05:20,20 --> 00:05:24,60
feature of React, or we can do what we've always done

101
00:05:24,60 --> 00:05:30,40
in the web design world, and just wrap them in another div.

102
00:05:30,40 --> 00:05:32,00
That's certainly a technique that we're very used to,

103
00:05:32,00 --> 00:05:35,60
so we'll just stick to that for now.

104
00:05:35,60 --> 00:05:37,90
There are no errors, and so previewer takes

105
00:05:37,90 --> 00:05:39,70
a formatting pass over the whole file,

106
00:05:39,70 --> 00:05:41,30
as you can see.

107
00:05:41,30 --> 00:05:43,40
And now we can switch back to the browser,

108
00:05:43,40 --> 00:05:47,10
reload it, and see what we have.

109
00:05:47,10 --> 00:05:50,10
Okay, looks like it's all working.

110
00:05:50,10 --> 00:05:52,30
We have our components, our filters,

111
00:05:52,30 --> 00:05:54,60
which are not fleshed out yet,

112
00:05:54,60 --> 00:05:57,80
and our people, containing these person records.

113
00:05:57,80 --> 00:05:59,60
Let's open up the developer tools

114
00:05:59,60 --> 00:06:02,20
and just confirm what we have.

115
00:06:02,20 --> 00:06:04,00
So after hitting command option I,

116
00:06:04,00 --> 00:06:06,10
I switch to the React tab,

117
00:06:06,10 --> 00:06:07,70
I can see here's my Directory component

118
00:06:07,70 --> 00:06:09,90
containing everything,

119
00:06:09,90 --> 00:06:14,50
here are my filters, my people,

120
00:06:14,50 --> 00:06:19,80
and each of my person results.

121
00:06:19,80 --> 00:06:22,10
I can see here's the state,

122
00:06:22,10 --> 00:06:25,80
all of my people,

123
00:06:25,80 --> 00:06:27,70
and then here inside people,

124
00:06:27,70 --> 00:06:31,90
is my read only prop.

125
00:06:31,90 --> 00:06:33,90
Okay, everything looks good at this point,

126
00:06:33,90 --> 00:06:35,60
so now we can move on to the next step,

127
00:06:35,60 --> 00:06:37,00
dealing with our filters.

