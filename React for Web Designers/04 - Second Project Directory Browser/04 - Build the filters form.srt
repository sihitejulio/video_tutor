1
00:00:00,50 --> 00:00:01,70
- [Narrator] In this video, we're going to build up

2
00:00:01,70 --> 00:00:04,80
the form elements for our filters component.

3
00:00:04,80 --> 00:00:06,80
We're going to group these together in a single component

4
00:00:06,80 --> 00:00:08,60
because there aren't a lot of elements here,

5
00:00:08,60 --> 00:00:11,20
just these three, and they're pretty simple.

6
00:00:11,20 --> 00:00:13,30
As your forms get larger, you can always consider

7
00:00:13,30 --> 00:00:15,80
breaking them up into smaller child components.

8
00:00:15,80 --> 00:00:19,80
But for our purposes, I think just one is going to be fine.

9
00:00:19,80 --> 00:00:21,30
Just to recap, in our current state,

10
00:00:21,30 --> 00:00:24,20
we just have a little message that says filters go here,

11
00:00:24,20 --> 00:00:25,20
we're going to replace that

12
00:00:25,20 --> 00:00:28,00
with the markup from our mock up.

13
00:00:28,00 --> 00:00:30,70
So I'll switch back over to editor,

14
00:00:30,70 --> 00:00:34,80
going to open the mockup.html file.

15
00:00:34,80 --> 00:00:36,50
I know that we already have the form tag in there,

16
00:00:36,50 --> 00:00:42,20
so I'm going to copy everything else that appears inside it.

17
00:00:42,20 --> 00:00:45,10
Bring that over to directory.js.

18
00:00:45,10 --> 00:00:49,10
I'm going to use command + shift + o, which is go to symbol,

19
00:00:49,10 --> 00:00:51,20
I can see all of my little symbols,

20
00:00:51,20 --> 00:00:54,00
in this case my functions and classes in this file,

21
00:00:54,00 --> 00:00:57,90
I'm going to jump down to filters by clicking it.

22
00:00:57,90 --> 00:01:01,30
So I can get rid of my message, and paste it

23
00:01:01,30 --> 00:01:05,40
this markup for my form elements.

24
00:01:05,40 --> 00:01:07,50
And we need to make some adaptations to this

25
00:01:07,50 --> 00:01:09,80
so that it will be valid jsx.

26
00:01:09,80 --> 00:01:15,00
Start by renaming all these classes to your class name,

27
00:01:15,00 --> 00:01:19,00
and then each of these single input elements,

28
00:01:19,00 --> 00:01:21,80
specified as html and they need to use

29
00:01:21,80 --> 00:01:24,80
xml syntax for jsx,

30
00:01:24,80 --> 00:01:28,60
so let's fix those on these two inputs,

31
00:01:28,60 --> 00:01:30,90
you can see the little squiggly underlines

32
00:01:30,90 --> 00:01:32,50
that are indicating that there are problems

33
00:01:32,50 --> 00:01:35,30
starting to go away here, that's good.

34
00:01:35,30 --> 00:01:38,50
Now I need to fix my labels so they use htmlFor

35
00:01:38,50 --> 00:01:42,40
instead of simply for.

36
00:01:42,40 --> 00:01:43,60
The checkbox doesn't need that

37
00:01:43,60 --> 00:01:49,00
because the label tag is wrapping the entire input area.

38
00:01:49,00 --> 00:01:51,20
Now, just as a matter of taste,

39
00:01:51,20 --> 00:01:54,00
I'm going to change these labels so they refer

40
00:01:54,00 --> 00:01:55,90
more specifically to the components

41
00:01:55,90 --> 00:01:57,90
that they're going to be filtering.

42
00:01:57,90 --> 00:02:00,50
So I'm going to change these generic labels

43
00:02:00,50 --> 00:02:05,10
so they say person, I'll use person_name here,

44
00:02:05,10 --> 00:02:07,00
get rid of this value for now,

45
00:02:07,00 --> 00:02:11,00
'cause we don't have anything set.

46
00:02:11,00 --> 00:02:15,50
Likewise this will be person_title.

47
00:02:15,50 --> 00:02:19,10
I like to have different values for my name and id,

48
00:02:19,10 --> 00:02:22,40
they can be used somewhat interchangeably in the DOM API,

49
00:02:22,40 --> 00:02:24,30
so I like to know which one I'm trying to use,

50
00:02:24,30 --> 00:02:27,00
if it comes to that.

51
00:02:27,00 --> 00:02:31,10
Likewise, I will set name here on this checkbox,

52
00:02:31,10 --> 00:02:34,90
call this person_intern.

53
00:02:34,90 --> 00:02:37,70
Okay, I think that gets to a place

54
00:02:37,70 --> 00:02:41,50
where all of our jsx is working.

55
00:02:41,50 --> 00:02:44,00
Let's save this, and switch back to the browser

56
00:02:44,00 --> 00:02:47,30
and see what we have so far.

57
00:02:47,30 --> 00:02:50,00
And refresh, okay, everything looks good so far.

58
00:02:50,00 --> 00:02:52,00
I'm going to open up my console,

59
00:02:52,00 --> 00:02:55,00
make sure I don't have any errors,

60
00:02:55,00 --> 00:02:57,60
all right, so far so good.

61
00:02:57,60 --> 00:02:59,20
Now we'll switch back to the editor

62
00:02:59,20 --> 00:03:01,30
and the next thing we need to do is

63
00:03:01,30 --> 00:03:04,80
change these options so they are no longer hard coded.

64
00:03:04,80 --> 00:03:07,70
We have a data set for these,

65
00:03:07,70 --> 00:03:09,70
here in the data file.

66
00:03:09,70 --> 00:03:13,20
Even though it looks like it's just a lot of people,

67
00:03:13,20 --> 00:03:17,20
if I collapse this, there is a set of titles as well.

68
00:03:17,20 --> 00:03:19,80
If your API, or wherever you're getting your data from,

69
00:03:19,80 --> 00:03:22,50
didn't provide this for you, you could probably compute it

70
00:03:22,50 --> 00:03:24,70
from the contents of this people array,

71
00:03:24,70 --> 00:03:27,50
by doing some filtering and so fourth.

72
00:03:27,50 --> 00:03:30,40
But, our little fake API has returned titles for us,

73
00:03:30,40 --> 00:03:32,80
which is very convenient.

74
00:03:32,80 --> 00:03:34,90
So we're going to use that.

75
00:03:34,90 --> 00:03:38,30
This list of titles is never going to be updated,

76
00:03:38,30 --> 00:03:40,60
so we're going to just make this

77
00:03:40,60 --> 00:03:46,70
a property of this function, it doesn't need to be state.

78
00:03:46,70 --> 00:03:49,10
So, we'll pull in these titles,

79
00:03:49,10 --> 00:03:55,00
they're going to be coming from window.LMDirectory.titles.

80
00:03:55,00 --> 00:03:59,20
Just confirm that here, in my data file.

81
00:03:59,20 --> 00:04:01,70
All right, now we're going to do the same thing

82
00:04:01,70 --> 00:04:04,60
we've been doing with arrays in the past,

83
00:04:04,60 --> 00:04:09,30
going to write titles.map,

84
00:04:09,30 --> 00:04:12,70
we'll have callback function here,

85
00:04:12,70 --> 00:04:13,90
and each of these is going to be

86
00:04:13,90 --> 00:04:17,30
one of these title objects from our title data.

87
00:04:17,30 --> 00:04:21,00
We have two keys, display and key.

88
00:04:21,00 --> 00:04:22,40
So display is what we're going to want

89
00:04:22,40 --> 00:04:25,80
to appear in the select box, and key will be the value,

90
00:04:25,80 --> 00:04:32,30
and we can always use it for our react iterator key.

91
00:04:32,30 --> 00:04:35,30
So we're going to be returning from this function

92
00:04:35,30 --> 00:04:41,50
a series of options.

93
00:04:41,50 --> 00:04:47,70
So we'll have a value here, which will be title.key,

94
00:04:47,70 --> 00:04:53,00
going to use our react key as well.

95
00:04:53,00 --> 00:04:56,80
Close this tag, and then the value that will be displayed,

96
00:04:56,80 --> 00:04:59,70
we title .display.

97
00:04:59,70 --> 00:05:04,30
Okay, we'll save this, switch back to the browser, refresh.

98
00:05:04,30 --> 00:05:06,60
Okay, looks good.

99
00:05:06,60 --> 00:05:08,10
Let's open up our developer tools

100
00:05:08,10 --> 00:05:13,00
with command + option + I.

101
00:05:13,00 --> 00:05:15,50
Just check this, make sure everything looks

102
00:05:15,50 --> 00:05:19,10
as we expect it to.

103
00:05:19,10 --> 00:05:20,90
Looks good.

104
00:05:20,90 --> 00:05:23,80
So now we've built up our filter component,

105
00:05:23,80 --> 00:05:26,90
and at this point we have all of the major pieces in place

106
00:05:26,90 --> 00:05:29,60
in terms of what we need to see on the page.

107
00:05:29,60 --> 00:05:32,60
We have our wrapping component, our filters component

108
00:05:32,60 --> 00:05:36,20
and our people and person components.

109
00:05:36,20 --> 00:05:38,10
The next step will be adding the interactivity

110
00:05:38,10 --> 00:05:41,00
that starts to make these things really work.

