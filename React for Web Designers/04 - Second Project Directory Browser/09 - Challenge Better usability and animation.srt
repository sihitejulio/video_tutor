1
00:00:00,50 --> 00:00:01,50
- [Instructor] In this challenge,

2
00:00:01,50 --> 00:00:05,40
we're going to enhance our company directory mini app here

3
00:00:05,40 --> 00:00:07,60
with a couple of new pieces.

4
00:00:07,60 --> 00:00:10,00
First of all, we're going to add a reset button.

5
00:00:10,00 --> 00:00:14,00
So right now, as I start to fill in different things,

6
00:00:14,00 --> 00:00:16,60
to reset it I have to go back through manually

7
00:00:16,60 --> 00:00:20,70
and change each one so it's empty.

8
00:00:20,70 --> 00:00:23,90
So we'll add a reset button to take care of that for us.

9
00:00:23,90 --> 00:00:28,20
And then, we're going to enhance the animation.

10
00:00:28,20 --> 00:00:30,30
We're going to integrate an external library

11
00:00:30,30 --> 00:00:34,30
of animations to make this a little more fun to play with.

12
00:00:34,30 --> 00:00:37,50
Specifically, we're going to use Animate.css.

13
00:00:37,50 --> 00:00:39,60
I'll reload this page so you can see little bit

14
00:00:39,60 --> 00:00:41,80
of the animation here.

15
00:00:41,80 --> 00:00:44,50
This is a CSS file, nothing more.

16
00:00:44,50 --> 00:00:47,40
With a bunch of pre-existing animations defined

17
00:00:47,40 --> 00:00:51,60
by various classes whose names appear in this popup menu.

18
00:00:51,60 --> 00:00:56,00
And as you can see, there's a whole bunch of 'em.

19
00:00:56,00 --> 00:00:57,40
You can experiment with these

20
00:00:57,40 --> 00:01:02,80
and watch what happens on the logo.

21
00:01:02,80 --> 00:01:04,80
Lot of options.

22
00:01:04,80 --> 00:01:06,90
So we're going to integrate Animate.css

23
00:01:06,90 --> 00:01:10,40
into our project, and then use it with our CSS traditions

24
00:01:10,40 --> 00:01:12,10
and react.

25
00:01:12,10 --> 00:01:13,60
So I want to do a little set up with you

26
00:01:13,60 --> 00:01:14,80
to get this ready before you

27
00:01:14,80 --> 00:01:17,20
start writing your own solution.

28
00:01:17,20 --> 00:01:19,40
Switching back to my editor.

29
00:01:19,40 --> 00:01:21,30
We're going to need to install the CSS file

30
00:01:21,30 --> 00:01:23,50
in our index.html file.

31
00:01:23,50 --> 00:01:26,20
There's an animate CSS snipet file in the exercise files

32
00:01:26,20 --> 00:01:27,70
for this challenge.

33
00:01:27,70 --> 00:01:30,30
So I'll select this little line.

34
00:01:30,30 --> 00:01:31,50
Copy it.

35
00:01:31,50 --> 00:01:34,30
And we'll bring it over into index.html,

36
00:01:34,30 --> 00:01:36,40
and I'll paste it right here

37
00:01:36,40 --> 00:01:40,10
above my directory.css file.

38
00:01:40,10 --> 00:01:40,90
Save that.

39
00:01:40,90 --> 00:01:46,10
And now Animate.css is installed and ready to use.

40
00:01:46,10 --> 00:01:48,70
I want to mention, there is documentation available

41
00:01:48,70 --> 00:01:51,00
for this, but I'll tell you most of what you need to know

42
00:01:51,00 --> 00:01:53,00
to get started.

43
00:01:53,00 --> 00:01:54,40
So here we go, a challenge

44
00:01:54,40 --> 00:01:56,60
focused on usability and animation.

45
00:01:56,60 --> 00:01:59,70
First we're going to add a working reset button.

46
00:01:59,70 --> 00:02:02,30
Keep in mind that a regular reset button, by itself,

47
00:02:02,30 --> 00:02:05,40
won't work because we have a controlled form setup.

48
00:02:05,40 --> 00:02:06,90
So we have to hook up an event listener

49
00:02:06,90 --> 00:02:10,60
to that reset button, however you choose to define it,

50
00:02:10,60 --> 00:02:13,90
that will actually take care of resetting the state for us.

51
00:02:13,90 --> 00:02:17,90
Next we're gong to add more animation with Animate.css.

52
00:02:17,90 --> 00:02:19,50
The way this is going to work

53
00:02:19,50 --> 00:02:21,40
is we're going to use the object version

54
00:02:21,40 --> 00:02:25,70
of the classNames prop on our CSS transition component.

55
00:02:25,70 --> 00:02:27,70
You'll find an animation you like for each transition

56
00:02:27,70 --> 00:02:30,50
so that they match the animation that you've chosen

57
00:02:30,50 --> 00:02:32,70
for each kind of transition.

58
00:02:32,70 --> 00:02:34,90
I'll just tell you that the triggering class

59
00:02:34,90 --> 00:02:39,50
that marks an element as being animatable with Animate.css

60
00:02:39,50 --> 00:02:41,70
is animated.

61
00:02:41,70 --> 00:02:44,20
After that, you'll use the classNames that you can find

62
00:02:44,20 --> 00:02:48,00
on the Animate.css homepage.

63
00:02:48,00 --> 00:02:51,80
Specifically all of these using the case that's defined

64
00:02:51,80 --> 00:02:54,00
on each one.

65
00:02:54,00 --> 00:02:56,60
So you can pick out the animations that you like best

66
00:02:56,60 --> 00:03:00,50
and then bring them in and use them in the CSS transitions.

67
00:03:00,50 --> 00:03:02,80
I expect this will take 10 minutes or less.

68
00:03:02,80 --> 00:03:04,00
Good luck.

