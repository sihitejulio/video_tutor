1
00:00:01,60 --> 00:00:04,80
up the filter form fields as controlled elements,

2
00:00:04,80 --> 00:00:06,30
that update some new state variables

3
00:00:06,30 --> 00:00:08,60
tracking their contents.

4
00:00:08,60 --> 00:00:13,00
So as of now, I can type in these and nothing happens.

5
00:00:13,00 --> 00:00:18,60
We're going to change these so they are controlled.

6
00:00:18,60 --> 00:00:22,80
So first, we know we're going to need to track some new state.

7
00:00:22,80 --> 00:00:26,60
We're going to need to track the currently typed in name,

8
00:00:26,60 --> 00:00:36,00
the currently selected title and whether

9
00:00:36,00 --> 00:00:38,50
We'll add a key called current name

10
00:00:38,50 --> 00:00:41,70
which will start out empty.

11
00:00:41,70 --> 00:00:46,80
Likewise the current title, also empty.

12
00:00:46,80 --> 00:00:49,90
And we'll use isIntern to track whether

13
00:00:49,90 --> 00:00:54,40
that checkbox is checked, and that will start as false.

14
00:00:54,40 --> 00:00:56,50
So these are the pieces of state we have

15
00:00:56,50 --> 00:00:58,90
at the Directory level, and we'll pass these

16
00:00:58,90 --> 00:01:01,90
into the filters as props.

17
00:01:01,90 --> 00:01:04,20
We're keeping the state here at the Directory level

18
00:01:04,20 --> 00:01:07,40
because these are all going to influence each other.

19
00:01:07,40 --> 00:01:10,10
Seems like the right place for now.

20
00:01:10,10 --> 00:01:13,90
So we'll pass these all in currentName equals

21
00:01:13,90 --> 00:01:22,90
this.state.currentName, currentTitle is

22
00:01:22,90 --> 00:01:27,90
this.state.currentTitle.

23
00:01:27,90 --> 00:01:35,40
We'll also pass in isIntern this.state.isIntern.

24
00:01:35,40 --> 00:01:37,10
Okay, that's good so far.

25
00:01:37,10 --> 00:01:38,80
So we're passing all these in as props

26
00:01:38,80 --> 00:01:40,90
to our filters component.

27
00:01:40,90 --> 00:01:42,50
So we'll go up to the filters component

28
00:01:42,50 --> 00:01:46,10
and we'll make sure of that now.

29
00:01:46,10 --> 00:01:48,10
So here's my name input.

30
00:01:48,10 --> 00:01:50,70
So let's add after the id.

31
00:01:50,70 --> 00:01:52,30
I'm going to set the value.

32
00:01:52,30 --> 00:01:53,40
And this is where we start to get

33
00:01:53,40 --> 00:01:57,90
into controlling our components.

34
00:01:57,90 --> 00:02:01,30
So that will be props.currentName.

35
00:02:01,30 --> 00:02:04,20
We'll set the value on our select box as well.

36
00:02:04,20 --> 00:02:06,90
We have our name and id so far.

37
00:02:06,90 --> 00:02:09,20
We'll add a value for this.

38
00:02:09,20 --> 00:02:11,90
So of course in normal html we wouldn't do it this way.

39
00:02:11,90 --> 00:02:15,60
We'd be setting the selected property of a particular option

40
00:02:15,60 --> 00:02:18,20
but in react you can use value.

41
00:02:18,20 --> 00:02:22,60
Use props.currentTitle.

42
00:02:22,60 --> 00:02:26,10
And then down here on our checkbox,

43
00:02:26,10 --> 00:02:29,10
instead of using value which is just set to one here

44
00:02:29,10 --> 00:02:33,70
'cause it's a checkbox, we will use checked.

45
00:02:33,70 --> 00:02:39,50
And we'll set checked to props.isIntern.

46
00:02:39,50 --> 00:02:40,70
Now I can save this.

47
00:02:40,70 --> 00:02:42,30
So let's go back, reload the page

48
00:02:42,30 --> 00:02:46,10
and see what if anything has changed.

49
00:02:46,10 --> 00:02:50,30
Going to type in a name here, and I can type furiously

50
00:02:50,30 --> 00:02:52,60
but whatever I type nothing happens

51
00:02:52,60 --> 00:02:57,70
because now react is controlling what appears in this form.

52
00:02:57,70 --> 00:03:00,90
Likewise I can select anything in here,

53
00:03:00,90 --> 00:03:03,70
all I want, and nothing will change.

54
00:03:03,70 --> 00:03:06,00
Check the intern checkbox, and nothing happens.

55
00:03:06,00 --> 00:03:09,30
So these components are now controlled.

56
00:03:09,30 --> 00:03:11,80
So now it's incumbent on me to add the event handlers

57
00:03:11,80 --> 00:03:14,70
that will allow these elements to be changed.

58
00:03:14,70 --> 00:03:18,70
Let's add one for each one of these.

59
00:03:18,70 --> 00:03:24,40
First on my text field I'll add in onChange,

60
00:03:24,40 --> 00:03:26,00
and we'll add an event handler,

61
00:03:26,00 --> 00:03:33,30
we'll just call it updateName.

62
00:03:33,30 --> 00:03:36,10
So we'll add a function called updateName.

63
00:03:36,10 --> 00:03:38,90
We're not going to do anything with it just yet.

64
00:03:38,90 --> 00:03:40,30
But I know we're going to have one of these

65
00:03:40,30 --> 00:03:41,70
for each one of our elements.

66
00:03:41,70 --> 00:03:44,20
So I'll have one for updateTitle,

67
00:03:44,20 --> 00:03:47,70
and I'll have one for updateIntern.

68
00:03:47,70 --> 00:03:55,70
And we'll add one to each of these elements.

69
00:03:55,70 --> 00:03:59,80
So here for a title we'll have updateTitle,

70
00:03:59,80 --> 00:04:10,10
and for the checkbox another onChange will be updateIntern.

71
00:04:10,10 --> 00:04:12,00
Now I have a separate handler for each of these

72
00:04:12,00 --> 00:04:15,00
but of course they don't do anything yet.

73
00:04:15,00 --> 00:04:16,90
We're going to need them to update state

74
00:04:16,90 --> 00:04:19,40
and the state is owned by our parent component.

75
00:04:19,40 --> 00:04:22,20
Just as we've seen before, we need a handler function

76
00:04:22,20 --> 00:04:25,00
that will appear in the component that owns the state,

77
00:04:25,00 --> 00:04:26,30
and then we'll pass that down

78
00:04:26,30 --> 00:04:30,00
into our child component as a prop.

79
00:04:30,00 --> 00:04:32,60
So let's go back to the Directory.

80
00:04:32,60 --> 00:04:36,50
And we're going to add a new handler function.

81
00:04:36,50 --> 00:04:39,00
Now as you saw we have separate update functions

82
00:04:39,00 --> 00:04:41,90
for each of those form elements.

83
00:04:41,90 --> 00:04:44,60
But what they're going to be doing is very similar

84
00:04:44,60 --> 00:04:46,80
so I'm going to use one handler function here.

85
00:04:46,80 --> 00:04:50,90
We'll called it updateFormState,

86
00:04:50,90 --> 00:04:53,00
and we'll pass in two parameters,

87
00:04:53,00 --> 00:04:55,70
the name of the piece of state that we want to update,

88
00:04:55,70 --> 00:04:59,00
and then the value to which we want to update it.

89
00:04:59,00 --> 00:05:01,30
We're going to use a feature of ES6 called

90
00:05:01,30 --> 00:05:03,80
computed property keys to let us use

91
00:05:03,80 --> 00:05:07,00
this one handler function to set any of the keys

92
00:05:07,00 --> 00:05:09,10
in our form state.

93
00:05:09,10 --> 00:05:14,50
So we're going to call this.setState,

94
00:05:14,50 --> 00:05:16,90
and in our object, we're going

95
00:05:16,90 --> 00:05:20,60
to use name in square braces.

96
00:05:20,60 --> 00:05:57,90
This will take the parameter that was passed in

97
00:05:57,90 --> 00:06:02,20
and use this very short version for now.

98
00:06:02,20 --> 00:06:04,10
So this generic handler function can update

99
00:06:04,10 --> 00:06:06,20
any piece of state that we need it to,

100
00:06:06,20 --> 00:06:08,40
and we need to pass that into our filter function

101
00:06:08,40 --> 00:06:11,70
so that it can be used by our child components.

102
00:06:11,70 --> 00:06:15,10
So we'll pass in updateFormState,

103
00:06:15,10 --> 00:06:20,20
and that will be this.updateFormState.

104
00:06:20,20 --> 00:06:21,70
I'll save that.

105
00:06:21,70 --> 00:06:26,40
Now we can scroll back up to filters component

106
00:06:26,40 --> 00:06:28,60
and update each of our handler functions here

107
00:06:28,60 --> 00:06:32,90
to use that new handler function from the parent.

108
00:06:32,90 --> 00:06:37,90
So in each of these we're going to call props.updateFormState.

109
00:06:37,90 --> 00:06:39,40
And the first argument will be the piece

110
00:06:39,40 --> 00:06:41,90
of state we're updating.

111
00:06:41,90 --> 00:06:44,70
So in this case it would be currentName,

112
00:06:44,70 --> 00:06:49,20
and we'll use the event, and we'll get the value

113
00:06:49,20 --> 00:06:53,90
that was being typed out of that form component.

114
00:06:53,90 --> 00:06:58,00
We can copy and paste this into each of these,

115
00:06:58,00 --> 00:07:02,70
and update which piece of state we intend to set.

116
00:07:02,70 --> 00:07:05,40
So that would be currentTitle for updateTitle

117
00:07:05,40 --> 00:07:09,30
and isIntern for updateIntern.

118
00:07:09,30 --> 00:07:11,90
And we can use the value for each of these two

119
00:07:11,90 --> 00:07:14,10
but for updateIntern we need to use the value

120
00:07:14,10 --> 00:07:17,70
of the checked property which is going to be true or false.

121
00:07:17,70 --> 00:07:20,40
One small note on the way we've implemented this.

122
00:07:20,40 --> 00:07:22,60
We are using the names of the state variables

123
00:07:22,60 --> 00:07:25,20
that we're updating here in a child component,

124
00:07:25,20 --> 00:07:27,50
which is not quite as much as a separation

125
00:07:27,50 --> 00:07:29,50
of concerns as you might like.

126
00:07:29,50 --> 00:07:31,90
And depending on the structure of your components,

127
00:07:31,90 --> 00:07:34,10
how big your application becomes,

128
00:07:34,10 --> 00:07:37,00
having this sort of pollution of information

129
00:07:37,00 --> 00:07:39,50
from one component in another component can be

130
00:07:39,50 --> 00:07:41,20
something you want to avoid.

131
00:07:41,20 --> 00:07:43,60
For something that's very small like this,

132
00:07:43,60 --> 00:07:46,10
we're sort of trading off having a little bit less code

133
00:07:46,10 --> 00:07:48,50
to write in terms of the number of lines

134
00:07:48,50 --> 00:07:50,90
for a little bit of extra maintenance

135
00:07:50,90 --> 00:07:53,60
that we might have to do across components.

136
00:07:53,60 --> 00:07:55,60
Where you draw this line is going to be up to you

137
00:07:55,60 --> 00:07:58,90
or up to the team that you're working with to sort out.

138
00:07:58,90 --> 00:08:00,70
We're sticking with this for now

139
00:08:00,70 --> 00:08:02,00
but it's something that could deserve

140
00:08:02,00 --> 00:08:05,70
some attention later as the app gets bigger.

141
00:08:05,70 --> 00:08:09,40
So switching back to the browser and reloading.

142
00:08:09,40 --> 00:08:10,80
Let's try typing.

143
00:08:10,80 --> 00:08:12,90
As I type nothing is changing.

144
00:08:12,90 --> 00:08:15,40
So let's open up our developer tools,

145
00:08:15,40 --> 00:08:17,40
command + I and I'll look as the console.

146
00:08:17,40 --> 00:08:19,50
We can see we have a bunch of errors here

147
00:08:19,50 --> 00:08:21,70
so I'll scroll up to the top and I can see

148
00:08:21,70 --> 00:08:23,80
that this.state is not a function.

149
00:08:23,80 --> 00:08:27,40
Hmm, well I've certainly used that quite a bit previously,

150
00:08:27,40 --> 00:08:30,10
so why isn't this working?

151
00:08:30,10 --> 00:08:34,80
We'll scroll down to where we're calling this.setState.

152
00:08:34,80 --> 00:08:36,60
So what's going on here is we're missing a bit

153
00:08:36,60 --> 00:08:38,60
of important boilerplate code

154
00:08:38,60 --> 00:08:41,80
when we're creating components using ES6 classes

155
00:08:41,80 --> 00:08:46,00
instead of create-react-class.

156
00:08:46,00 --> 00:08:48,60
You'll see this a lot in the react documentation.

157
00:08:48,60 --> 00:08:52,10
What we need to do is bind this.

158
00:08:52,10 --> 00:08:53,10
I'll show you what that looks like

159
00:08:53,10 --> 00:08:56,20
and then we'll talk about what it means.

160
00:08:56,20 --> 00:08:59,70
So what needs to happen is we need to update

161
00:08:59,70 --> 00:09:05,30
this.updateFormState to itself,

162
00:09:05,30 --> 00:09:09,70
and then call bind this.

163
00:09:09,70 --> 00:09:12,10
If you've used java script for a pretty long time

164
00:09:12,10 --> 00:09:13,80
this might be a familiar pattern.

165
00:09:13,80 --> 00:09:16,30
For everyone else here's what this means.

166
00:09:16,30 --> 00:09:17,90
Here in this handler function which is going

167
00:09:17,90 --> 00:09:20,20
to be passed down to the child component,

168
00:09:20,20 --> 00:09:25,40
the value of this is not always going to be what we expect.

169
00:09:25,40 --> 00:09:27,00
There are many occasions in java script

170
00:09:27,00 --> 00:09:31,40
where dealing with this keyword can get confusing.

171
00:09:31,40 --> 00:09:34,30
And so here we're making sure that this

172
00:09:34,30 --> 00:09:36,20
here in our handler function is always going

173
00:09:36,20 --> 00:09:40,00
to refer to this instance of my Directory component.

174
00:09:40,00 --> 00:09:42,10
And so here in the constructor we pass in

175
00:09:42,10 --> 00:09:47,10
this to bind which makes sure that when we call it here,

176
00:09:47,10 --> 00:09:49,70
it's going to be the right value.

177
00:09:49,70 --> 00:09:50,90
If that doesn't make any sense

178
00:09:50,90 --> 00:09:54,70
just remember that if you're doing ES6 classes,

179
00:09:54,70 --> 00:09:56,80
this is a piece of boilerplate that you're always going to have

180
00:09:56,80 --> 00:09:58,60
to write for any of your handler functions

181
00:09:58,60 --> 00:10:01,10
that are passed into child components.

182
00:10:01,10 --> 00:10:03,30
You'll see this in the react documentation.

183
00:10:03,30 --> 00:10:06,30
If you read it, or expose yourself to it enough times,

184
00:10:06,30 --> 00:10:09,50
eventually it will sink in and start to make sense.

185
00:10:09,50 --> 00:10:14,30
So let's save the file and switch back to the browser.

186
00:10:14,30 --> 00:10:16,10
Now I can start to type,

187
00:10:16,10 --> 00:10:19,20
and I'm getting the result that I expect.

188
00:10:19,20 --> 00:10:23,30
All of these form elements are letting me set values.

189
00:10:23,30 --> 00:10:27,60
So let's open up the developer tools,

190
00:10:27,60 --> 00:10:29,70
close up the console, and we'll confirm

191
00:10:29,70 --> 00:10:33,20
that state is updating as we expect down here as well.

192
00:10:33,20 --> 00:10:39,30
And indeed it is.

193
00:10:39,30 --> 00:10:41,50
Excellent.

194
00:10:41,50 --> 00:10:43,60
So now we're updating state,

195
00:10:43,60 --> 00:10:48,20
but the results area is still showing everyone.

196
00:10:48,20 --> 00:10:50,00
So that will be our next step.

197
00:10:50,00 --> 00:10:52,60
But at this point we've hooked up our filter component

198
00:10:52,60 --> 00:10:55,00
so that we can use each of these elements

199
00:10:55,00 --> 00:10:57,00
and they update state correctly.

