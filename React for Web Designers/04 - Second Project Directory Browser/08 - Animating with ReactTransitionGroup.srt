1
00:00:00,30 --> 00:00:01,50
- [Instructor] In this video we're going to make

2
00:00:01,50 --> 00:00:04,00
updating the list of our people more pleasant

3
00:00:04,00 --> 00:00:05,30
with some animation by adding

4
00:00:05,30 --> 00:00:06,40
some off-the-shelf React component

5
00:00:06,40 --> 00:00:08,70
made just for this purpose.

6
00:00:08,70 --> 00:00:10,20
There's a very vibrant ecosystem

7
00:00:10,20 --> 00:00:12,30
of components out there for React.

8
00:00:12,30 --> 00:00:14,10
Because React is a user interface library

9
00:00:14,10 --> 00:00:16,50
you might think it's all UI widgets like buttons,

10
00:00:16,50 --> 00:00:18,40
slideshows, and so on.

11
00:00:18,40 --> 00:00:20,50
Some components are actually a little more abstract,

12
00:00:20,50 --> 00:00:22,40
like the one we're going to work with now,

13
00:00:22,40 --> 00:00:24,60
react-transition-group.

14
00:00:24,60 --> 00:00:28,90
You can read more about it here on reactcommunity.org.

15
00:00:28,90 --> 00:00:31,20
React-transition-group is a suite of a few components

16
00:00:31,20 --> 00:00:32,50
that let you describe what happens

17
00:00:32,50 --> 00:00:35,80
to a component over time using transitions.

18
00:00:35,80 --> 00:00:37,70
Usually this takes the form of animation,

19
00:00:37,70 --> 00:00:39,90
which is what we're going to be using for it here.

20
00:00:39,90 --> 00:00:41,40
But just know that these components can support

21
00:00:41,40 --> 00:00:44,30
other types of changes as well should you need them.

22
00:00:44,30 --> 00:00:47,00
To start let's get react-transition-group installed

23
00:00:47,00 --> 00:00:50,60
and include some basic elements in our code.

24
00:00:50,60 --> 00:00:52,30
So back here in my editor,

25
00:00:52,30 --> 00:00:55,90
we're going to open this rtg-snippet file,

26
00:00:55,90 --> 00:00:59,40
which is included in the exercise files for this video.

27
00:00:59,40 --> 00:01:02,00
And let's copy everything that's in here,

28
00:01:02,00 --> 00:01:05,90
and we're going to install it in our index.html file.

29
00:01:05,90 --> 00:01:08,20
I'll jump down to the bottom here

30
00:01:08,20 --> 00:01:10,90
where all my other React script tags appear.

31
00:01:10,90 --> 00:01:13,70
I'll paste it in right below them.

32
00:01:13,70 --> 00:01:15,40
As before we have an online version

33
00:01:15,40 --> 00:01:18,30
and then an offline fallback.

34
00:01:18,30 --> 00:01:20,30
We can save this.

35
00:01:20,30 --> 00:01:23,80
And now switching over to the directory.js file

36
00:01:23,80 --> 00:01:25,90
we're going to include the components that are provided

37
00:01:25,90 --> 00:01:29,00
by react-transition-group.

38
00:01:29,00 --> 00:01:30,90
We need to scroll down to the piece

39
00:01:30,90 --> 00:01:33,10
that's actually going to control the animation,

40
00:01:33,10 --> 00:01:35,20
which is the people list.

41
00:01:35,20 --> 00:01:37,50
So what we're going to do first is add a group,

42
00:01:37,50 --> 00:01:40,20
which defines which group of elements are the ones

43
00:01:40,20 --> 00:01:43,30
that are supposed to be changing and transitioning.

44
00:01:43,30 --> 00:01:46,80
So we're going to surround our results

45
00:01:46,80 --> 00:01:48,50
with one of those.

46
00:01:48,50 --> 00:01:57,60
This is ReactTransitionGroup.TransitionGroup.

47
00:01:57,60 --> 00:02:01,30
We'll add a close tag for this.

48
00:02:01,30 --> 00:02:03,40
And now inside this transition group

49
00:02:03,40 --> 00:02:06,40
we need to add a transition of some sort.

50
00:02:06,40 --> 00:02:08,90
The one we're going to use is the CSS transition

51
00:02:08,90 --> 00:02:12,40
for controlling CSS animations.

52
00:02:12,40 --> 00:02:14,50
Those are going to surround each of the elements

53
00:02:14,50 --> 00:02:18,00
that we want to have a transition.

54
00:02:18,00 --> 00:02:20,70
In this case our person components.

55
00:02:20,70 --> 00:02:22,00
So I'll add parentheses around this

56
00:02:22,00 --> 00:02:25,90
because it's going to become a multi-line return.

57
00:02:25,90 --> 00:02:27,50
I'll copy this tag,

58
00:02:27,50 --> 00:02:29,90
'cause it's still in the ReactTransitionGroup.

59
00:02:29,90 --> 00:02:35,40
And this will be a CSSTransition.

60
00:02:35,40 --> 00:02:39,80
I'll add a close tag for that as well.

61
00:02:39,80 --> 00:02:43,40
And now because I've changed the structure of my components

62
00:02:43,40 --> 00:02:44,70
that are being iterated over,

63
00:02:44,70 --> 00:02:46,60
I need to move my key from

64
00:02:46,60 --> 00:02:54,30
the inner element to the outermost element.

65
00:02:54,30 --> 00:02:56,40
Now I can save this.

66
00:02:56,40 --> 00:02:58,20
CSSTransitions are the easiest ones

67
00:02:58,20 --> 00:03:00,00
to work with for animation.

68
00:03:00,00 --> 00:03:01,30
Here's what happens.

69
00:03:01,30 --> 00:03:03,30
When components are added or removed,

70
00:03:03,30 --> 00:03:06,60
which is called mounting and unmounting in React parlance,

71
00:03:06,60 --> 00:03:08,90
ReactTransitionGroup will add classes that we defined

72
00:03:08,90 --> 00:03:11,30
to each component that's being added or removed,

73
00:03:11,30 --> 00:03:13,50
indicating what state it's in.

74
00:03:13,50 --> 00:03:17,20
For our purposes that's going to be entering or exiting.

75
00:03:17,20 --> 00:03:19,80
Then we need some CSS that's associated with those classes

76
00:03:19,80 --> 00:03:23,30
to make the animation work how we want.

77
00:03:23,30 --> 00:03:25,80
So let's take a look at our directory.css file

78
00:03:25,80 --> 00:03:28,70
that's included in the assets folder for the entire course,

79
00:03:28,70 --> 00:03:31,50
not just this chapter.

80
00:03:31,50 --> 00:03:34,20
So if I scroll down you can see here's a little comment

81
00:03:34,20 --> 00:03:36,60
that says CSSTransition styles.

82
00:03:36,60 --> 00:03:38,80
And these are the styles that we're going to need.

83
00:03:38,80 --> 00:03:43,00
So we have a fade namespace for all of these.

84
00:03:43,00 --> 00:03:45,00
And they're defined with -enter,

85
00:03:45,00 --> 00:03:49,10
- enter-active, exit, and exit-active.

86
00:03:49,10 --> 00:03:50,70
So just know that these styles are here,

87
00:03:50,70 --> 00:03:53,80
and as you can see they're going to fade in

88
00:03:53,80 --> 00:03:59,10
and fade out elements as they enter or exit.

89
00:03:59,10 --> 00:04:01,00
So we have the CSS that's going to go with them,

90
00:04:01,00 --> 00:04:02,80
but we need to tell ReactTransitionGroup

91
00:04:02,80 --> 00:04:06,00
how to actually use those classes.

92
00:04:06,00 --> 00:04:09,40
So what we do is on the CSSTransition

93
00:04:09,40 --> 00:04:12,40
we add a prop called classNames,

94
00:04:12,40 --> 00:04:15,60
and this can be configured two ways.

95
00:04:15,60 --> 00:04:17,20
We'll do the explicit way first,

96
00:04:17,20 --> 00:04:19,90
which is using an object.

97
00:04:19,90 --> 00:04:23,50
And that object will have a series of keys

98
00:04:23,50 --> 00:04:27,00
for what we want each of these classes to look like.

99
00:04:27,00 --> 00:04:31,60
So we need one for entering,

100
00:04:31,60 --> 00:04:34,70
which we will match up with our

101
00:04:34,70 --> 00:04:36,30
styles over here in the CSS file.

102
00:04:36,30 --> 00:04:39,10
You can see it's fade-enter, fade-enter-active

103
00:04:39,10 --> 00:04:42,60
and so forth.

104
00:04:42,60 --> 00:04:46,50
So we have the keys for enter,

105
00:04:46,50 --> 00:04:52,10
enterActive,

106
00:04:52,10 --> 00:05:02,30
and then the same thing for exit.

107
00:05:02,30 --> 00:05:05,00
So ReactTransitionGroup is going to manage

108
00:05:05,00 --> 00:05:07,40
based on when these components are added or removed

109
00:05:07,40 --> 00:05:09,20
based on state changes.

110
00:05:09,20 --> 00:05:11,50
These classes for us.

111
00:05:11,50 --> 00:05:12,90
Now you can see this is a little verbose,

112
00:05:12,90 --> 00:05:15,60
and it's basically matching the keys exactly,

113
00:05:15,60 --> 00:05:18,30
so there's a shorter way that we can do this.

114
00:05:18,30 --> 00:05:20,00
Instead of using an object,

115
00:05:20,00 --> 00:05:22,00
and by the way notice these double curly braces,

116
00:05:22,00 --> 00:05:24,70
because the first outer one tells us

117
00:05:24,70 --> 00:05:26,10
that this is an expression

118
00:05:26,10 --> 00:05:31,20
and then the inner one is an actual object literal.

119
00:05:31,20 --> 00:05:33,10
We can get rid of those.

120
00:05:33,10 --> 00:05:34,60
And the very short way to do that,

121
00:05:34,60 --> 00:05:37,00
if you're just following that specific pattern,

122
00:05:37,00 --> 00:05:38,40
is just to use that word that you're

123
00:05:38,40 --> 00:05:40,20
prepending all of those classes with.

124
00:05:40,20 --> 00:05:41,80
Sort of like a namespace.

125
00:05:41,80 --> 00:05:44,00
In this case it's fade.

126
00:05:44,00 --> 00:05:46,00
So defining it this way is exactly the same

127
00:05:46,00 --> 00:05:49,70
as specifying all that stuff in that longer object.

128
00:05:49,70 --> 00:05:51,30
And along with this we need one more

129
00:05:51,30 --> 00:05:53,60
main piece of configuration,

130
00:05:53,60 --> 00:05:56,00
which is the timeout prop.

131
00:05:56,00 --> 00:05:59,00
And this will set the timing of how long those classes

132
00:05:59,00 --> 00:06:03,00
are left on or removed from those elements.

133
00:06:03,00 --> 00:06:05,10
And it's specified in milliseconds.

134
00:06:05,10 --> 00:06:07,00
So to keep things easy to see,

135
00:06:07,00 --> 00:06:11,30
I'm going to set this to two seconds or 2,000 milliseconds.

136
00:06:11,30 --> 00:06:12,90
Now let's take a look at the results

137
00:06:12,90 --> 00:06:14,40
of having done all this stuff

138
00:06:14,40 --> 00:06:17,40
and see what it looks like in our little directory app.

139
00:06:17,40 --> 00:06:20,30
Let's refresh the page.

140
00:06:20,30 --> 00:06:24,80
I'm going to scroll down and start typing a name.

141
00:06:24,80 --> 00:06:26,90
You can see all the other elements fade out,

142
00:06:26,90 --> 00:06:29,90
and just this one is left.

143
00:06:29,90 --> 00:06:36,90
Let's try it with the intern checkbox.

144
00:06:36,90 --> 00:06:38,50
Let's take a look at the web inspector

145
00:06:38,50 --> 00:06:41,10
and watch these classes change.

146
00:06:41,10 --> 00:06:42,40
I'm just going to open up the inspector

147
00:06:42,40 --> 00:06:45,30
with command option i.

148
00:06:45,30 --> 00:06:50,40
Let's scroll down and inspect one of these elements here.

149
00:06:50,40 --> 00:06:52,80
So I'm going to close up all of these,

150
00:06:52,80 --> 00:06:54,70
and we're just going to watch this results area

151
00:06:54,70 --> 00:06:57,80
as I start to type.

152
00:06:57,80 --> 00:07:01,90
You can see the fade-exit classes appear and then disappear

153
00:07:01,90 --> 00:07:06,20
right before all those elements are removed.

154
00:07:06,20 --> 00:07:07,50
If I clear this out again,

155
00:07:07,50 --> 00:07:09,60
we have the enter classes and the enter active classes

156
00:07:09,60 --> 00:07:15,10
that then disappear as the elements are left behind.

157
00:07:15,10 --> 00:07:18,00
You can see enters and exits appearing there

158
00:07:18,00 --> 00:07:23,00
as things are moved around.

159
00:07:23,00 --> 00:07:24,70
I've left the timing of these animations

160
00:07:24,70 --> 00:07:26,90
pretty long at two seconds just

161
00:07:26,90 --> 00:07:29,70
so you can see what's happening more easily.

162
00:07:29,70 --> 00:07:32,30
But as you're configuring these in real use,

163
00:07:32,30 --> 00:07:34,20
you'll probably have shorter animations,

164
00:07:34,20 --> 00:07:36,30
and then you can tweak that timeout prop

165
00:07:36,30 --> 00:07:38,60
on your CSSTransitions in combination

166
00:07:38,60 --> 00:07:41,90
with the timing of the CSSTransitions themselves

167
00:07:41,90 --> 00:07:43,40
in your CSS,

168
00:07:43,40 --> 00:07:47,00
to get everything working just exactly how you want it to.

169
00:07:47,00 --> 00:07:48,50
And I want to make one final note

170
00:07:48,50 --> 00:07:51,60
about the documentation for these components.

171
00:07:51,60 --> 00:07:53,80
Many examples of them that you'll see online

172
00:07:53,80 --> 00:07:57,70
will refer to the 1.x version of ReactTransitionGroup,

173
00:07:57,70 --> 00:07:59,40
which was built with a little different API

174
00:07:59,40 --> 00:08:02,10
than the 2.x version that we're using here.

175
00:08:02,10 --> 00:08:03,80
So as you're looking for examples,

176
00:08:03,80 --> 00:08:07,20
just watch out for that as a possible gotcha.

177
00:08:07,20 --> 00:08:09,30
The 2.x version has been out for quite a while now,

178
00:08:09,30 --> 00:08:11,20
so it's starting to get out and people

179
00:08:11,20 --> 00:08:12,20
are starting to use it more,

180
00:08:12,20 --> 00:08:15,00
but it's just something to keep in mind

181
00:08:15,00 --> 00:08:17,90
and so now we have a very nice looking animation

182
00:08:17,90 --> 00:08:21,00
that makes our filtering experience much nicer.

