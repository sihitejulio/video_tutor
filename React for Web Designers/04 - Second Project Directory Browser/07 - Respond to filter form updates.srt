1
00:00:00,50 --> 00:00:01,70
- [Instructor] In this video, we're going to add

2
00:00:01,70 --> 00:00:04,00
the last major feature of our directory,

3
00:00:04,00 --> 00:00:05,60
updating the list of people

4
00:00:05,60 --> 00:00:07,70
according to the state of the filters.

5
00:00:07,70 --> 00:00:10,60
In larger apps or sites when updating filters,

6
00:00:10,60 --> 00:00:12,60
we'd probably batch the changes up a little bit

7
00:00:12,60 --> 00:00:14,00
and make a call to an API

8
00:00:14,00 --> 00:00:16,50
to retrieve the current list of people.

9
00:00:16,50 --> 00:00:18,20
In our case, we're just using a blob of data

10
00:00:18,20 --> 00:00:19,50
that's always available,

11
00:00:19,50 --> 00:00:20,60
so we have quicker access

12
00:00:20,60 --> 00:00:22,70
and can do things a little differently.

13
00:00:22,70 --> 00:00:25,20
Switching back to the code,

14
00:00:25,20 --> 00:00:28,70
I have a snippet in the exercise files of this video,

15
00:00:28,70 --> 00:00:32,80
updatePeopleList.js.

16
00:00:32,80 --> 00:00:34,90
I've included this inside a class,

17
00:00:34,90 --> 00:00:38,10
just to keep certain editors from throwing errors at you

18
00:00:38,10 --> 00:00:39,60
for the syntax,

19
00:00:39,60 --> 00:00:42,40
but what we really need is just what's inside

20
00:00:42,40 --> 00:00:44,50
this class snippet.

21
00:00:44,50 --> 00:00:45,50
Let's copy all this stuff

22
00:00:45,50 --> 00:00:50,20
and bring it over to the directory.js file.

23
00:00:50,20 --> 00:00:53,40
We'll scroll down to our directory component,

24
00:00:53,40 --> 00:01:00,70
and I'll paste it in just below update form state.

25
00:01:00,70 --> 00:01:04,70
Save it so that Prettier can work its magic,

26
00:01:04,70 --> 00:01:08,50
and now let's take a look at what this actually does.

27
00:01:08,50 --> 00:01:11,20
So what I'm doing is making a filtered list of people,

28
00:01:11,20 --> 00:01:15,10
and I'm starting with the original list of people

29
00:01:15,10 --> 00:01:17,10
from the data file.

30
00:01:17,10 --> 00:01:19,60
This is where you might normally make an API call,

31
00:01:19,60 --> 00:01:20,70
but we always want to make sure

32
00:01:20,70 --> 00:01:22,30
that when we're starting from the filtered list,

33
00:01:22,30 --> 00:01:23,70
we're starting with everyone,

34
00:01:23,70 --> 00:01:25,80
not just whatever the currently filtered state is

35
00:01:25,80 --> 00:01:27,80
because we might miss someone.

36
00:01:27,80 --> 00:01:29,50
And now, as we've done before

37
00:01:29,50 --> 00:01:32,00
with arrays that we're working with,

38
00:01:32,00 --> 00:01:34,50
previously we've called map to work with every item

39
00:01:34,50 --> 00:01:35,50
in the array,

40
00:01:35,50 --> 00:01:38,10
but this time we're calling filter.

41
00:01:38,10 --> 00:01:41,30
Filter takes a callback function, which we've defined here,

42
00:01:41,30 --> 00:01:44,90
and if your callback function returns true,

43
00:01:44,90 --> 00:01:48,80
that particular item, which is passed in as a parameter here

44
00:01:48,80 --> 00:01:52,30
will be included in the resulting filtered array.

45
00:01:52,30 --> 00:01:54,10
So what we need to do here is figure out

46
00:01:54,10 --> 00:01:56,70
if the person of interest matches

47
00:01:56,70 --> 00:01:58,40
the current state of the filters.

48
00:01:58,40 --> 00:02:01,70
And we're doing that with a series of three pieces of logic.

49
00:02:01,70 --> 00:02:04,10
So we're checking if the current state

50
00:02:04,10 --> 00:02:06,00
of the intern checkbox is true,

51
00:02:06,00 --> 00:02:07,80
and that person is an intern,

52
00:02:07,80 --> 00:02:09,20
that would be one case.

53
00:02:09,20 --> 00:02:11,40
Then we need to check the name field.

54
00:02:11,40 --> 00:02:14,30
So we're converting whatever the person's name is

55
00:02:14,30 --> 00:02:15,40
to lowercase,

56
00:02:15,40 --> 00:02:18,20
and then we're checking if whatever the current state

57
00:02:18,20 --> 00:02:21,00
of the name is, converted to lowercase again,

58
00:02:21,00 --> 00:02:23,80
is contained in that name.

59
00:02:23,80 --> 00:02:25,80
So this will do partial matching,

60
00:02:25,80 --> 00:02:29,10
so if the person's name is Tony

61
00:02:29,10 --> 00:02:32,80
and we start to type T, O, or T, O lowercase,

62
00:02:32,80 --> 00:02:36,20
these will match.

63
00:02:36,20 --> 00:02:40,60
And then finally we're checking if the current title,

64
00:02:40,60 --> 00:02:43,30
and we're using these keys, not the displayed values,

65
00:02:43,30 --> 00:02:44,80
so those are always going to match,

66
00:02:44,80 --> 00:02:47,10
we don't have to do anything too special with those

67
00:02:47,10 --> 00:02:49,90
if a title has been entered at all,

68
00:02:49,90 --> 00:02:51,90
and if the titles match.

69
00:02:51,90 --> 00:02:54,60
And then finally you can see we're doing another bind this

70
00:02:54,60 --> 00:02:56,90
to make sure that this callback function has access

71
00:02:56,90 --> 00:03:02,20
to the correct value of this when looking for state.

72
00:03:02,20 --> 00:03:04,00
After we've done that, we have a filtered list

73
00:03:04,00 --> 00:03:06,30
of the original array of people,

74
00:03:06,30 --> 00:03:10,30
and we're updating the people state with that filtered list.

75
00:03:10,30 --> 00:03:11,90
So we've defined this function,

76
00:03:11,90 --> 00:03:14,10
now we just have to call it somewhere,

77
00:03:14,10 --> 00:03:16,90
'cause right now it won't do anything.

78
00:03:16,90 --> 00:03:18,90
Here's how we want this to work.

79
00:03:18,90 --> 00:03:22,20
We want people to be able to type in these input fields

80
00:03:22,20 --> 00:03:26,40
and have the filter function execute pretty much immediately

81
00:03:26,40 --> 00:03:29,40
so that this list is always as up-to-date as possible.

82
00:03:29,40 --> 00:03:34,00
Those updates are taking place after the state is updated,

83
00:03:34,00 --> 00:03:36,00
but as we've mentioned previously,

84
00:03:36,00 --> 00:03:39,30
set state is an asynchronous function,

85
00:03:39,30 --> 00:03:44,30
so if we were to call update people list right here,

86
00:03:44,30 --> 00:03:46,90
we aren't necessarily guaranteed that this.state

87
00:03:46,90 --> 00:03:48,70
inside our helper function

88
00:03:48,70 --> 00:03:50,90
is always going include the results of set state

89
00:03:50,90 --> 00:03:53,00
as we've defined it here.

90
00:03:53,00 --> 00:03:54,10
In the React documentation,

91
00:03:54,10 --> 00:03:57,40
you can see that there are various ways to call set state,

92
00:03:57,40 --> 00:04:00,50
and one of the features of the set state API

93
00:04:00,50 --> 00:04:03,80
is that you can pass a callback function as well

94
00:04:03,80 --> 00:04:11,60
to be called after these updates have taken place.

95
00:04:11,60 --> 00:04:14,30
So we can just pass in this.updatePeopleList

96
00:04:14,30 --> 00:04:17,70
here as the second parameter to set state,

97
00:04:17,70 --> 00:04:20,10
and then it will be called after these updates

98
00:04:20,10 --> 00:04:22,40
have taken place.

99
00:04:22,40 --> 00:04:23,80
So I can save this,

100
00:04:23,80 --> 00:04:27,70
and now we can go back to our browser and try it all out.

101
00:04:27,70 --> 00:04:29,60
Let's reload.

102
00:04:29,60 --> 00:04:34,20
Scroll down so we can see a little bit more of this.

103
00:04:34,20 --> 00:04:38,60
Here we go.

104
00:04:38,60 --> 00:04:48,90
You can see that these are working.

105
00:04:48,90 --> 00:04:49,90
Pretty cool.

106
00:04:49,90 --> 00:04:51,50
Now I want to reiterate that much of the time

107
00:04:51,50 --> 00:04:52,40
when pulling live data,

108
00:04:52,40 --> 00:04:54,60
you'll be making AJAX or WebSockets calls

109
00:04:54,60 --> 00:04:57,90
to some API using REST or GraphQL or whatever,

110
00:04:57,90 --> 00:05:00,00
and exactly where and how you'll make those calls

111
00:05:00,00 --> 00:05:01,50
is going to be different than what we're doing here

112
00:05:01,50 --> 00:05:03,60
because we're using static data.

113
00:05:03,60 --> 00:05:06,10
We're able to do things immediately because that data

114
00:05:06,10 --> 00:05:08,30
are always available to us.

115
00:05:08,30 --> 00:05:09,80
To save lots of calls over the network,

116
00:05:09,80 --> 00:05:12,00
we'd probably need to consider debouncing the updates,

117
00:05:12,00 --> 00:05:13,50
in other words, having a short waiting period

118
00:05:13,50 --> 00:05:15,80
while we basically ask the user, are you done typing yet,

119
00:05:15,80 --> 00:05:18,80
how about now, before making a query.

120
00:05:18,80 --> 00:05:20,30
Those are things you'll be looking at later

121
00:05:20,30 --> 00:05:23,80
on larger projects as you start to use React more.

122
00:05:23,80 --> 00:05:25,50
But for now, victory is ours.

123
00:05:25,50 --> 00:05:26,70
We have a filterable list of people

124
00:05:26,70 --> 00:05:30,30
that responds to user input, and it's very fast.

125
00:05:30,30 --> 00:05:33,00
We're going to enhance it a little bit in the next video.

