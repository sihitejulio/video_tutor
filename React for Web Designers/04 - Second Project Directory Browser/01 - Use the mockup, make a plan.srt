1
00:00:00,50 --> 00:00:01,40
- [Man] In this video we're going

2
00:00:01,40 --> 00:00:03,80
to start our next React project.

3
00:00:03,80 --> 00:00:05,40
We're going to get familiar with the mock-up

4
00:00:05,40 --> 00:00:06,70
of what we're looking for,

5
00:00:06,70 --> 00:00:08,60
and we're going to do a little bit of setup

6
00:00:08,60 --> 00:00:10,80
to get the groundwork in place.

7
00:00:10,80 --> 00:00:12,90
So, first of all we'll introduce the website.

8
00:00:12,90 --> 00:00:14,80
This is Leaf and Mortar.

9
00:00:14,80 --> 00:00:16,50
This is a sample, existing website

10
00:00:16,50 --> 00:00:20,10
into which we're going to to bring some React components.

11
00:00:20,10 --> 00:00:22,40
We're going to be working with this company directory,

12
00:00:22,40 --> 00:00:24,70
which is a list of all the employees

13
00:00:24,70 --> 00:00:28,50
at this architecture firm, say.

14
00:00:28,50 --> 00:00:30,00
And we have these filters,

15
00:00:30,00 --> 00:00:32,30
which currently don't work, it's just a mock-up,

16
00:00:32,30 --> 00:00:35,30
but the idea will be that we can type somebody's name in,

17
00:00:35,30 --> 00:00:37,10
and it will search the available names,

18
00:00:37,10 --> 00:00:40,10
and only show people whose name matches.

19
00:00:40,10 --> 00:00:43,60
Likewise, we can limit by certain job titles,

20
00:00:43,60 --> 00:00:45,40
and we have a check box to show

21
00:00:45,40 --> 00:00:50,80
only people who are interns or those who are not.

22
00:00:50,80 --> 00:00:54,00
So, if we're going to build this into a series of components,

23
00:00:54,00 --> 00:00:57,30
we need to think about how we're going to do that.

24
00:00:57,30 --> 00:00:58,50
What I'm going to recommend is that

25
00:00:58,50 --> 00:01:01,00
we have one, big component that contains everything.

26
00:01:01,00 --> 00:01:03,60
We'll call that the Directory,

27
00:01:03,60 --> 00:01:07,50
and inside that component we'll have a Filters component

28
00:01:07,50 --> 00:01:10,10
that will have these three form elements.

29
00:01:10,10 --> 00:01:13,20
Then we'll have a People component below that,

30
00:01:13,20 --> 00:01:16,50
which contains a series of person components,

31
00:01:16,50 --> 00:01:18,40
which will be these little pieces;

32
00:01:18,40 --> 00:01:21,50
the name, the bio, the photo.

33
00:01:21,50 --> 00:01:23,10
So, those will be our components.

34
00:01:23,10 --> 00:01:25,20
Now, what data are we tracking?

35
00:01:25,20 --> 00:01:27,40
We're going to need to track the selected values

36
00:01:27,40 --> 00:01:28,60
in each of these filters.

37
00:01:28,60 --> 00:01:31,70
So, the name, the job title, the flag

38
00:01:31,70 --> 00:01:33,80
of whether someone is an intern,

39
00:01:33,80 --> 00:01:36,80
and we need to track the results of who,

40
00:01:36,80 --> 00:01:38,50
of the available people that we know of

41
00:01:38,50 --> 00:01:42,20
in the directory, are currently shown.

42
00:01:42,20 --> 00:01:43,60
Okay, we have all that.

43
00:01:43,60 --> 00:01:47,50
Let's take a quick look at the code that we have so far.

44
00:01:47,50 --> 00:01:51,20
Here in my index.html file, I'm going to skip down

45
00:01:51,20 --> 00:01:53,90
to the middle of the file using Control G,

46
00:01:53,90 --> 00:01:57,30
and I'm going to skip down to line 115.

47
00:01:57,30 --> 00:01:59,40
This is where I've set up the directory route

48
00:01:59,40 --> 00:02:02,40
where my React components are going to be placed.

49
00:02:02,40 --> 00:02:06,20
And then, if we skip back down to the bottom of the file,

50
00:02:06,20 --> 00:02:08,00
you can see that we have the same boilerplate

51
00:02:08,00 --> 00:02:11,10
that we've seen before; with the polyfills,

52
00:02:11,10 --> 00:02:12,90
for our older JavaScript environments,

53
00:02:12,90 --> 00:02:17,20
and then the React Dev build, ReactDOM.

54
00:02:17,20 --> 00:02:20,00
You'll notice that we do not have Create React Class here,

55
00:02:20,00 --> 00:02:21,40
because we're going to take a different approach

56
00:02:21,40 --> 00:02:25,40
where we use ES6 classes to build up our components.

57
00:02:25,40 --> 00:02:28,90
We also have this data file, which is very similar

58
00:02:28,90 --> 00:02:31,30
to what we saw in the last chapter,

59
00:02:31,30 --> 00:02:34,20
where we have a static set of data,

60
00:02:34,20 --> 00:02:36,50
which is formatted as JSON,

61
00:02:36,50 --> 00:02:39,20
which mimics what you might get back from an API.

62
00:02:39,20 --> 00:02:43,40
So, each of these people has as ID, their name, title,

63
00:02:43,40 --> 00:02:46,30
whether they're an intern, a longer, spelled out version

64
00:02:46,30 --> 00:02:50,00
of the title, their bio, and their image.

65
00:02:50,00 --> 00:02:51,30
And finally, a key that says

66
00:02:51,30 --> 00:02:53,70
what order they should appear in.

67
00:02:53,70 --> 00:02:55,30
We're not going to use all this stuff,

68
00:02:55,30 --> 00:02:56,80
but it's just here as an example

69
00:02:56,80 --> 00:02:58,30
of what an API might give you,

70
00:02:58,30 --> 00:03:01,60
so that you can start playing with actual data.

71
00:03:01,60 --> 00:03:03,60
And then, we have our mock-up file here,

72
00:03:03,60 --> 00:03:05,50
where we'll be able to steal Markup

73
00:03:05,50 --> 00:03:08,80
for the components as we're building them up.

74
00:03:08,80 --> 00:03:10,80
The last thing I want to mention is that

75
00:03:10,80 --> 00:03:13,60
because we're going to try and use ES6 classes,

76
00:03:13,60 --> 00:03:15,90
we're going to skip using Babel in the browser,

77
00:03:15,90 --> 00:03:19,20
and we're going to switch to using a real preprocessor

78
00:03:19,20 --> 00:03:21,10
that will generate script tags

79
00:03:21,10 --> 00:03:23,20
that we can actually use right in the browser

80
00:03:23,20 --> 00:03:25,50
and publish on the internet.

81
00:03:25,50 --> 00:03:27,00
Now, this is the sort of thing that

82
00:03:27,00 --> 00:03:28,40
if you're a React professional,

83
00:03:28,40 --> 00:03:30,50
you would probably use WebPack.

84
00:03:30,50 --> 00:03:33,70
But, to get us on board a lot more easily,

85
00:03:33,70 --> 00:03:35,40
you have some other options.

86
00:03:35,40 --> 00:03:39,70
If you're a MAC user, you could use CodeKit.

87
00:03:39,70 --> 00:03:41,90
In this course we're going to use Prepros,

88
00:03:41,90 --> 00:03:43,80
which is available across platform,

89
00:03:43,80 --> 00:03:45,70
and has a free, unlimited trial,

90
00:03:45,70 --> 00:03:47,40
as you can see here.

91
00:03:47,40 --> 00:03:50,70
So, I've downloaded that already.

92
00:03:50,70 --> 00:03:52,80
The way you work with Prepros

93
00:03:52,80 --> 00:03:54,70
is you drag and drop your project

94
00:03:54,70 --> 00:03:56,60
right onto the window, as I've done here

95
00:03:56,60 --> 00:03:59,50
with my Work in Progress, start folder.

96
00:03:59,50 --> 00:04:01,30
Then, you need to tell Prepros what to do

97
00:04:01,30 --> 00:04:03,50
with these files.

98
00:04:03,50 --> 00:04:05,40
So, the only one that I'm interested in,

99
00:04:05,40 --> 00:04:08,80
is this directory.js file.

100
00:04:08,80 --> 00:04:10,00
You have some other settings here

101
00:04:10,00 --> 00:04:11,70
that tell it how to process the file.

102
00:04:11,70 --> 00:04:13,30
I'm going to uncheck all these boxes,

103
00:04:13,30 --> 00:04:17,00
because what I really want is to run it through Babel.

104
00:04:17,00 --> 00:04:18,70
I'm also going to have it create source maps,

105
00:04:18,70 --> 00:04:20,60
and then auto compile,

106
00:04:20,60 --> 00:04:23,40
which means every time I save directory.js

107
00:04:23,40 --> 00:04:29,30
it will re compile, and create this directory-dist file.

108
00:04:29,30 --> 00:04:31,50
You can also look, if you're interested,

109
00:04:31,50 --> 00:04:35,10
here in the settings, and under other settings

110
00:04:35,10 --> 00:04:37,40
you can see those that are specific to Babel.

111
00:04:37,40 --> 00:04:40,40
Out of the box, it should have all of these boxes checked,

112
00:04:40,40 --> 00:04:42,50
which will handle all the React stuff for you,

113
00:04:42,50 --> 00:04:45,10
and various other features of ES6

114
00:04:45,10 --> 00:04:48,80
that you might want to use.

115
00:04:48,80 --> 00:04:50,60
So, with this in place we should be good to go.

116
00:04:50,60 --> 00:04:53,40
I'm going to click Process File,

117
00:04:53,40 --> 00:04:54,30
just to get Prepros

118
00:04:54,30 --> 00:04:56,20
to create this directory-dist file for me.

119
00:04:56,20 --> 00:04:57,80
Now, we're going to switch back to our editor,

120
00:04:57,80 --> 00:05:00,60
and make sure that we actually use it.

121
00:05:00,60 --> 00:05:03,20
Okay, so here in my index.html file,

122
00:05:03,20 --> 00:05:05,90
I'm referring to my directory file.

123
00:05:05,90 --> 00:05:09,20
I'm going to just change this to directory-dist,

124
00:05:09,20 --> 00:05:12,20
and so now every time that I make a change

125
00:05:12,20 --> 00:05:14,60
to my original, directory file,

126
00:05:14,60 --> 00:05:17,30
it's going to recreate this directory-dist,

127
00:05:17,30 --> 00:05:20,00
and it'll be available in the browser for me.

128
00:05:20,00 --> 00:05:22,10
Prepros and CodeKit have many other features

129
00:05:22,10 --> 00:05:24,50
that can make your life, as a web developer, much easier.

130
00:05:24,50 --> 00:05:25,50
If you've never seen them before

131
00:05:25,50 --> 00:05:26,70
I definitely recommend you check

132
00:05:26,70 --> 00:05:29,40
either or both of them out.

133
00:05:29,40 --> 00:05:31,20
But, this is something that's going to be especially useful

134
00:05:31,20 --> 00:05:32,10
for us in this course,

135
00:05:32,10 --> 00:05:34,60
because now we no longer have to worry about Babel

136
00:05:34,60 --> 00:05:35,90
in the browser.

137
00:05:35,90 --> 00:05:39,30
And, what that means, is we can continue working

138
00:05:39,30 --> 00:05:42,00
with our local host, our local web server,

139
00:05:42,00 --> 00:05:44,30
or we can just drag and drop the files

140
00:05:44,30 --> 00:05:45,50
right onto the browser window,

141
00:05:45,50 --> 00:05:47,40
and they will work again.

142
00:05:47,40 --> 00:05:48,40
So, from this point forward

143
00:05:48,40 --> 00:05:50,50
you can drop these files onto your browser,

144
00:05:50,50 --> 00:05:52,00
and just work with them locally,

145
00:05:52,00 --> 00:05:54,10
or you can address them through your local, web server,

146
00:05:54,10 --> 00:05:56,90
whichever you find easier.

147
00:05:56,90 --> 00:05:59,80
So, at this point, we've seen what the project is,

148
00:05:59,80 --> 00:06:01,40
we've gotten a little groundwork in place,

149
00:06:01,40 --> 00:06:03,30
so we can start building up our components,

150
00:06:03,30 --> 00:06:09,00
and doing some real work.

