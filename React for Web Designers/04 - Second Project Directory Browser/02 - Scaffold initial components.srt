1
00:00:00,50 --> 00:00:01,60
- [Instructor] In this video, we're going to start

2
00:00:01,60 --> 00:00:03,20
scaffolding out the components we need

3
00:00:03,20 --> 00:00:06,00
to build up our Company Directory app.

4
00:00:06,00 --> 00:00:09,40
To recap, we need one big directory component

5
00:00:09,40 --> 00:00:11,40
to wrap everything.

6
00:00:11,40 --> 00:00:15,20
We'll have a filters component, a people component,

7
00:00:15,20 --> 00:00:19,80
which is going to contain person components.

8
00:00:19,80 --> 00:00:21,80
So we're going to need to work with four components.

9
00:00:21,80 --> 00:00:24,80
Let's start with the big one first, the company directory.

10
00:00:24,80 --> 00:00:26,70
Currently, we have pretty much nothing

11
00:00:26,70 --> 00:00:29,80
going on in our index.html file.

12
00:00:29,80 --> 00:00:33,10
So let's start working on the script to change that.

13
00:00:33,10 --> 00:00:36,50
I have my directory.js file open here.

14
00:00:36,50 --> 00:00:38,20
So the first thing I want to do

15
00:00:38,20 --> 00:00:39,80
is set up a place where these components

16
00:00:39,80 --> 00:00:42,70
can be rendered when they're finished.

17
00:00:42,70 --> 00:00:48,40
So just as we've seen before, we'll use ReactDOM.render.

18
00:00:48,40 --> 00:00:54,20
And we need our component here, which will be a Directory.

19
00:00:54,20 --> 00:00:57,10
This is just what we'll choose to call it this time.

20
00:00:57,10 --> 00:00:58,80
And then we need a place to put it.

21
00:00:58,80 --> 00:01:02,30
So we'll use document.getElementById.

22
00:01:02,30 --> 00:01:04,70
And we need to retrieve the right ID

23
00:01:04,70 --> 00:01:07,50
from our index.html file.

24
00:01:07,50 --> 00:01:10,30
Down here on line 115, you can see

25
00:01:10,30 --> 00:01:12,40
that it's called directory-root.

26
00:01:12,40 --> 00:01:15,90
So let's copy that, paste it in here

27
00:01:15,90 --> 00:01:18,40
so we make sure we get it right.

28
00:01:18,40 --> 00:01:21,00
And now we need to create this directory component.

29
00:01:21,00 --> 00:01:22,70
This time, we're going to use a class.

30
00:01:22,70 --> 00:01:23,90
So you can see how they work

31
00:01:23,90 --> 00:01:26,70
in contrast to function components.

32
00:01:26,70 --> 00:01:28,70
We're going to use the ES6 version of this

33
00:01:28,70 --> 00:01:30,40
this time around, because that's what most

34
00:01:30,40 --> 00:01:32,40
of the React documentation supports

35
00:01:32,40 --> 00:01:34,50
and it'll make it easier as you go forward

36
00:01:34,50 --> 00:01:37,50
to follow other people's examples.

37
00:01:37,50 --> 00:01:40,40
So if you're never used classes in ES6 before,

38
00:01:40,40 --> 00:01:42,90
it's very similar to how other languages

39
00:01:42,90 --> 00:01:45,20
do classical object-oriented programming,

40
00:01:45,20 --> 00:01:47,70
if that means anything to you.

41
00:01:47,70 --> 00:01:50,50
If it doesn't, this is the syntax.

42
00:01:50,50 --> 00:01:53,60
We use the class key word followed by the name of our class

43
00:01:53,60 --> 00:01:55,30
and in this case, we're extending

44
00:01:55,30 --> 00:02:00,20
the built in React.Component class.

45
00:02:00,20 --> 00:02:01,10
And that's it.

46
00:02:01,10 --> 00:02:03,20
Everything else is going to be a method

47
00:02:03,20 --> 00:02:07,40
that is a function inside this definition.

48
00:02:07,40 --> 00:02:09,30
So we're going to need two parts:

49
00:02:09,30 --> 00:02:13,50
a constructor and a render function.

50
00:02:13,50 --> 00:02:15,30
There are lots of other functions that you can define

51
00:02:15,30 --> 00:02:17,30
inside your classes, but the ones that we need

52
00:02:17,30 --> 00:02:19,20
for our basic purposes right now

53
00:02:19,20 --> 00:02:21,70
are the render function and the constructor.

54
00:02:21,70 --> 00:02:24,30
When we were using Create React Class previously,

55
00:02:24,30 --> 00:02:27,30
there was a function called Get Initial State,

56
00:02:27,30 --> 00:02:28,60
which sort of stands in for what

57
00:02:28,60 --> 00:02:33,30
the constructor does in an ES6 class.

58
00:02:33,30 --> 00:02:38,00
Inside the constructor, we always call super.

59
00:02:38,00 --> 00:02:41,70
And incidentally, we want to pass in an argument of props

60
00:02:41,70 --> 00:02:46,40
here in our React classes.

61
00:02:46,40 --> 00:02:49,00
So super here is calling the constructor

62
00:02:49,00 --> 00:02:51,90
of the class, which our class extends.

63
00:02:51,90 --> 00:02:53,00
This is just a bit of boiler plate

64
00:02:53,00 --> 00:02:54,50
that you're always going to want to write

65
00:02:54,50 --> 00:02:57,90
when you create React classes.

66
00:02:57,90 --> 00:03:00,20
And we'll do some more work afterwards.

67
00:03:00,20 --> 00:03:02,20
But now let's do our render function.

68
00:03:02,20 --> 00:03:03,50
We've seen this sort of thing a lot

69
00:03:03,50 --> 00:03:05,70
in our components so far.

70
00:03:05,70 --> 00:03:08,90
We're just going to return a big JSX expression.

71
00:03:08,90 --> 00:03:10,50
And we can steal the markup for that

72
00:03:10,50 --> 00:03:13,50
right out of our mockup.html file.

73
00:03:13,50 --> 00:03:16,50
So let's use the go to line command, Control+G,

74
00:03:16,50 --> 00:03:19,40
to skip down to right about where

75
00:03:19,40 --> 00:03:24,90
everything starts on line 116, in this case.

76
00:03:24,90 --> 00:03:28,80
And first, we'll copy this static markup,

77
00:03:28,80 --> 00:03:35,00
bring it over to our render function, and paste it in.

78
00:03:35,00 --> 00:03:38,80
We have a few too many header tags here

79
00:03:38,80 --> 00:03:40,80
'cause we pasted this too many times.

80
00:03:40,80 --> 00:03:42,30
There we go.

81
00:03:42,30 --> 00:03:47,40
Now we're going to wrap this entire thing

82
00:03:47,40 --> 00:03:50,10
in its own wrapping div.

83
00:03:50,10 --> 00:03:52,80
All JSX elements need to be wrapped

84
00:03:52,80 --> 00:03:56,10
in a single wrapping element.

85
00:03:56,10 --> 00:03:57,40
There are different ways that you can work

86
00:03:57,40 --> 00:03:59,10
around this need in React.

87
00:03:59,10 --> 00:04:02,10
But because we're coming from a web design perspective,

88
00:04:02,10 --> 00:04:04,10
having to wrap things in divs is pretty natural,

89
00:04:04,10 --> 00:04:06,90
so we'll just stick with what we know.

90
00:04:06,90 --> 00:04:12,10
So we'll give this a class name of company-directory.

91
00:04:12,10 --> 00:04:19,20
All right, we know we're going to need our filters.

92
00:04:19,20 --> 00:04:21,20
So we'll just set up an empty component for that.

93
00:04:21,20 --> 00:04:25,30
And we also know that we're going to need people.

94
00:04:25,30 --> 00:04:27,20
So our render function is ready to go

95
00:04:27,20 --> 00:04:30,10
enough to move back to our constructor.

96
00:04:30,10 --> 00:04:34,10
And now we need to set up our initial state.

97
00:04:34,10 --> 00:04:36,30
The only thing that we really need at this point

98
00:04:36,30 --> 00:04:38,30
is the people data.

99
00:04:38,30 --> 00:04:40,10
When we were using Create React Class,

100
00:04:40,10 --> 00:04:43,60
we used Get Initial State to set our initial state.

101
00:04:43,60 --> 00:04:45,40
Because we're using ES6 classes,

102
00:04:45,40 --> 00:04:47,30
we have a different way of doing this.

103
00:04:47,30 --> 00:04:50,00
We just directly set the state property

104
00:04:50,00 --> 00:04:53,80
of our class using the this keyword.

105
00:04:53,80 --> 00:04:57,50
And we just literally set it up as an object.

106
00:04:57,50 --> 00:05:00,60
So in this case, we're going to set people

107
00:05:00,60 --> 00:05:05,00
to the data that's contained inside our data file.

108
00:05:05,00 --> 00:05:07,10
Now, if you were working against an API,

109
00:05:07,10 --> 00:05:08,40
there would be different ways to do this.

110
00:05:08,40 --> 00:05:10,30
But because this is just a set of scalar data

111
00:05:10,30 --> 00:05:11,90
that's just available to us,

112
00:05:11,90 --> 00:05:13,10
we can do it this way without having

113
00:05:13,10 --> 00:05:14,50
to do anything special.

114
00:05:14,50 --> 00:05:16,20
We can just reference it directly

115
00:05:16,20 --> 00:05:18,40
by copying this definition.

116
00:05:18,40 --> 00:05:20,30
So that would be window.LMDirectory.

117
00:05:20,30 --> 00:05:24,50
That's Leaf & Mortar Directory, .people.

118
00:05:24,50 --> 00:05:27,60
And now that we have this state,

119
00:05:27,60 --> 00:05:29,20
we're going to pass it into our people component

120
00:05:29,20 --> 00:05:34,40
where it's actually going to be used.

121
00:05:34,40 --> 00:05:37,30
So we'll say this.state.people

122
00:05:37,30 --> 00:05:42,70
is passed in as the people prop on that component.

123
00:05:42,70 --> 00:05:45,30
So now, we've set up our big wrapping component.

124
00:05:45,30 --> 00:05:48,20
We haven't defined everything that's going to be in here.

125
00:05:48,20 --> 00:05:50,30
But we've set up our initial structure

126
00:05:50,30 --> 00:05:53,20
for how this thing is going to be put together.

127
00:05:53,20 --> 00:05:55,40
And in the next step, we'll build up the child components

128
00:05:55,40 --> 00:05:58,00
that will be featured inside this directory component.

