1
00:00:00,50 --> 00:00:02,50
- Let's take a look at one possible solution

2
00:00:02,50 --> 00:00:06,10
to this usability and animation challenge.

3
00:00:06,10 --> 00:00:08,40
So we can see there's a 'Reset' button here.

4
00:00:08,40 --> 00:00:13,40
If I try setting a few of these filters,

5
00:00:13,40 --> 00:00:15,70
I have some different animations happening,

6
00:00:15,70 --> 00:00:18,40
and if I click 'Reset',

7
00:00:18,40 --> 00:00:21,50
I have all the people once again.

8
00:00:21,50 --> 00:00:23,40
All right, let's look at the code.

9
00:00:23,40 --> 00:00:24,40
So first we'll take a look

10
00:00:24,40 --> 00:00:26,80
at the 'Reset' button implementation.

11
00:00:26,80 --> 00:00:28,80
That's going to be down here in 'Filters'.

12
00:00:28,80 --> 00:00:31,90
So I'll scroll down first to the elements

13
00:00:31,90 --> 00:00:33,70
that create the 'Reset' button.

14
00:00:33,70 --> 00:00:35,60
I just used another one of these divs

15
00:00:35,60 --> 00:00:37,20
with the class "group",

16
00:00:37,20 --> 00:00:38,60
and put a 'Reset' button in there.

17
00:00:38,60 --> 00:00:41,20
And it has an event handler 'onClick',

18
00:00:41,20 --> 00:00:45,50
which is calling a function called 'resetFilters'.

19
00:00:45,50 --> 00:00:48,40
A function which is defined up here.

20
00:00:48,40 --> 00:00:51,20
And this is just calling 'props.updateFormState'

21
00:00:51,20 --> 00:00:52,60
for each of the elements of state

22
00:00:52,60 --> 00:00:54,40
that need to be updated.

23
00:00:54,40 --> 00:00:55,30
Now any time

24
00:00:55,30 --> 00:00:57,60
that you start writing the same thing more than once,

25
00:00:57,60 --> 00:00:59,80
that's an opportunity to make some changes,

26
00:00:59,80 --> 00:01:01,60
which is something that you could do

27
00:01:01,60 --> 00:01:03,40
by changing how this function works.

28
00:01:03,40 --> 00:01:05,50
And maybe we'll look at that later.

29
00:01:05,50 --> 00:01:08,30
But that's the 'Reset' button, pretty straightforward.

30
00:01:08,30 --> 00:01:11,10
Not let's take a look at the animation part.

31
00:01:11,10 --> 00:01:12,60
And for that we need to look

32
00:01:12,60 --> 00:01:14,40
where our transitions are defined,

33
00:01:14,40 --> 00:01:17,50
up here in our 'People' component.

34
00:01:17,50 --> 00:01:18,90
So I've updated class names

35
00:01:18,90 --> 00:01:21,10
to use the object syntax,

36
00:01:21,10 --> 00:01:23,10
and for the 'enter' and 'exit',

37
00:01:23,10 --> 00:01:26,70
I'm using the "animated" class from 'animate.css',

38
00:01:26,70 --> 00:01:28,20
which tells us these are going

39
00:01:28,20 --> 00:01:30,00
to be animated elements.

40
00:01:30,00 --> 00:01:31,90
And then the 'Active' keys

41
00:01:31,90 --> 00:01:35,20
are used to specify which animations I want to use.

42
00:01:35,20 --> 00:01:37,80
And I've taken the "zoomIn" and "zoomOut" animations

43
00:01:37,80 --> 00:01:40,20
from 'animate.css'.

44
00:01:40,20 --> 00:01:42,20
I've adjusted the timeout, so it's a little bit shorter,

45
00:01:42,20 --> 00:01:43,90
only one second this time,

46
00:01:43,90 --> 00:01:46,40
which is the same as what 'animate.css' uses

47
00:01:46,40 --> 00:01:48,40
for a lot of its transitions.

48
00:01:48,40 --> 00:01:49,80
And at that point, you're done.

49
00:01:49,80 --> 00:01:52,80
All the heavy lifting has been done by 'animate.css'.

50
00:01:52,80 --> 00:01:58,60
You just needed to pick out the right classes.

51
00:01:58,60 --> 00:02:01,90
And you get these nice animations.

52
00:02:01,90 --> 00:02:04,50
And of course there are lots of other options here,

53
00:02:04,50 --> 00:02:05,70
which you could play with.

54
00:02:05,70 --> 00:02:07,50
There's even fade-ins and fade-outs,

55
00:02:07,50 --> 00:02:08,90
just like we had before.

56
00:02:08,90 --> 00:02:11,60
So you could change this to "fadeIn",

57
00:02:11,60 --> 00:02:13,90
and this to "fadeOut".

58
00:02:13,90 --> 00:02:17,30
Now we can save and switch back to the browser.

59
00:02:17,30 --> 00:02:19,20
Reload.

60
00:02:19,20 --> 00:02:23,10
And there we go, basic fades.

61
00:02:23,10 --> 00:02:27,00
I'm going to change this back to "zoomIn" and "zoomOut".

62
00:02:27,00 --> 00:02:29,20
So before we leave this, let's take a look

63
00:02:29,20 --> 00:02:31,90
at that repetition that we created up here

64
00:02:31,90 --> 00:02:34,30
by using 'updateFormState' the same way,

65
00:02:34,30 --> 00:02:36,00
over and over again.

66
00:02:36,00 --> 00:02:37,60
We change the way this works, so instead

67
00:02:37,60 --> 00:02:41,70
of taking two parameters, it takes an object.

68
00:02:41,70 --> 00:02:47,30
So it might be something like 'props.updateFormState'

69
00:02:47,30 --> 00:02:48,80
and then we pass in an object representing

70
00:02:48,80 --> 00:02:51,40
which pieces of state we want to update.

71
00:02:51,40 --> 00:02:52,20
This would be very similar

72
00:02:52,20 --> 00:02:56,10
to the way that 'setState' is implemented by React itself.

73
00:02:56,10 --> 00:03:02,10
So we could say, "currentName" is empty,

74
00:03:02,10 --> 00:03:06,30
"currentTitle" is empty,

75
00:03:06,30 --> 00:03:14,90
and "isIntern" is false.

76
00:03:14,90 --> 00:03:16,40
So if we did this,

77
00:03:16,40 --> 00:03:23,50
we might update all of these function calls as well,

78
00:03:23,50 --> 00:03:29,10
to use an object as the parameter.

79
00:03:29,10 --> 00:03:40,20
(keyboard clicks)

80
00:03:40,20 --> 00:03:42,10
So now we need to change the actual implementation

81
00:03:42,10 --> 00:03:45,60
of this function, to accept that object.

82
00:03:45,60 --> 00:03:48,40
Currently it's expecting a name and a value.

83
00:03:48,40 --> 00:03:50,80
So let's change that to 'spec',

84
00:03:50,80 --> 00:03:57,00
to refer to some sort of object specification.

85
00:03:57,00 --> 00:04:01,90
And now, we can just pass that in as the first parameter,

86
00:04:01,90 --> 00:04:05,10
keeping our 'updatePeopleList' as the second parameter,

87
00:04:05,10 --> 00:04:09,40
to happen after the asynchronous update of state.

88
00:04:09,40 --> 00:04:12,40
So now if I save this, and go back,

89
00:04:12,40 --> 00:04:21,30
I can reload, and check my work.

90
00:04:21,30 --> 00:04:24,60
Everything is still working just as we expect it to.

91
00:04:24,60 --> 00:04:27,80
So, there's different ways that you could implement this.

92
00:04:27,80 --> 00:04:29,60
I've chosen to eliminate a little bit

93
00:04:29,60 --> 00:04:31,30
of repetition up here,

94
00:04:31,30 --> 00:04:33,80
in terms of calling the same function over and over again.

95
00:04:33,80 --> 00:04:35,40
Now it doesn't necessarily reduce the number

96
00:04:35,40 --> 00:04:38,60
of lines of code, because formatted nicely,

97
00:04:38,60 --> 00:04:40,70
these are broken across different lines.

98
00:04:40,70 --> 00:04:43,10
But it looks a little cleaner to me.

99
00:04:43,10 --> 00:04:44,60
So at this point,

100
00:04:44,60 --> 00:04:47,40
we have a nicely working company directory.

101
00:04:47,40 --> 00:04:50,10
It's quite usable, has some nice animation.

102
00:04:50,10 --> 00:04:52,00
I'd say we've done a good job.

