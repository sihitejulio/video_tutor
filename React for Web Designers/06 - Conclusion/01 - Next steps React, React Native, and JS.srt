1
00:00:00,60 --> 00:00:02,70
- [Joe] Now that you've gone through the basics with React,

2
00:00:02,70 --> 00:00:04,70
let's talk about some possible next steps.

3
00:00:04,70 --> 00:00:05,70
First step.

4
00:00:05,70 --> 00:00:08,40
Courses to help deepen your understanding of React.

5
00:00:08,40 --> 00:00:10,70
React.js, building the user interface

6
00:00:10,70 --> 00:00:12,10
is another project course geared

7
00:00:12,10 --> 00:00:14,20
toward front-end developers and web-designers,

8
00:00:14,20 --> 00:00:17,00
and it's a great companion to this one.

9
00:00:17,00 --> 00:00:20,00
Building and deploying a full-stack React application

10
00:00:20,00 --> 00:00:21,10
looks at how to build a web app

11
00:00:21,10 --> 00:00:23,90
that's based entirely on the React stack

12
00:00:23,90 --> 00:00:25,70
rather than just a piece of an existing site

13
00:00:25,70 --> 00:00:27,20
as we went through here.

14
00:00:27,20 --> 00:00:31,80
React ES6, ES2016 and Beyond covers the new features

15
00:00:31,80 --> 00:00:35,50
of modern JavaScript, specifically, through a React lens,

16
00:00:35,50 --> 00:00:37,60
and then there's learning Redux, which,

17
00:00:37,60 --> 00:00:39,10
of course covers Redux,

18
00:00:39,10 --> 00:00:41,30
which is a state store that trades off

19
00:00:41,30 --> 00:00:43,50
some complexity in favor of not having to pass

20
00:00:43,50 --> 00:00:46,50
as much data around through components props.

21
00:00:46,50 --> 00:00:49,10
It's a very common piece of larger React projects

22
00:00:49,10 --> 00:00:50,60
and it's very popular.

23
00:00:50,60 --> 00:00:52,80
Next up we'll talk about courses to help you deepen

24
00:00:52,80 --> 00:00:55,40
your understanding of JavaScript in general.

25
00:00:55,40 --> 00:00:58,10
Building declarative apps using functional JavaScript

26
00:00:58,10 --> 00:01:00,30
delves deeply into functional JavaScript

27
00:01:00,30 --> 00:01:03,00
in the declarative way that React is so steeped in.

28
00:01:03,00 --> 00:01:05,00
If you want to really understand the technical reasons

29
00:01:05,00 --> 00:01:07,90
why React is the way it is, this could be a good one.

30
00:01:07,90 --> 00:01:10,50
JavaScript Async covers the myriad of ways

31
00:01:10,50 --> 00:01:12,60
that JavaScript handles asynchronous tasks

32
00:01:12,60 --> 00:01:15,90
like data fetching, lazy building, all that sort of thing.

33
00:01:15,90 --> 00:01:18,10
TypeScript is the language that's more feature-rich

34
00:01:18,10 --> 00:01:21,20
than JavaScript, but compiles down into JavaScript.

35
00:01:21,20 --> 00:01:23,90
Many large JavaScript projects ending up adopting TypeScript

36
00:01:23,90 --> 00:01:25,30
for some of the features that it adds,

37
00:01:25,30 --> 00:01:26,80
and this essential training,

38
00:01:26,80 --> 00:01:29,70
TypeScript Essential Training might interest you.

39
00:01:29,70 --> 00:01:31,90
Webpack can handle library dependencies

40
00:01:31,90 --> 00:01:34,20
and with code that isn't being used via code splitting

41
00:01:34,20 --> 00:01:37,30
and tree shaking and lots of other useful tasks.

42
00:01:37,30 --> 00:01:40,20
It's useful for any JavaScript project, not just React,

43
00:01:40,20 --> 00:01:41,70
but now that you have some React traps,

44
00:01:41,70 --> 00:01:43,40
this is a fine introduction to it,

45
00:01:43,40 --> 00:01:44,40
and then finally,

46
00:01:44,40 --> 00:01:48,00
we'll talk about some courses dealing with React Native.

47
00:01:48,00 --> 00:01:49,30
From React to React Native,

48
00:01:49,30 --> 00:01:51,50
we'll help bridge the conceptual gap between React

49
00:01:51,50 --> 00:01:55,20
for the web and React as used in mobile applications.

50
00:01:55,20 --> 00:01:57,40
From there, you can try learning React Native

51
00:01:57,40 --> 00:01:59,40
to build something a little larger,

52
00:01:59,40 --> 00:02:02,50
and then we have create a CRM mobile application

53
00:02:02,50 --> 00:02:03,80
with React Native.

54
00:02:03,80 --> 00:02:06,70
This is actually the second part of a two-course sequence

55
00:02:06,70 --> 00:02:09,00
on prototyping an app and then building it.

56
00:02:09,00 --> 00:02:09,90
So if you want to get a taste

57
00:02:09,90 --> 00:02:11,30
of what it might be like to plan

58
00:02:11,30 --> 00:02:14,30
and build a full mobile application using React Native,

59
00:02:14,30 --> 00:02:16,40
these two courses could be just the ticket.

60
00:02:16,40 --> 00:02:18,40
I hope you've enjoyed this introduction to React,

61
00:02:18,40 --> 00:02:20,50
and that you feel ready to use it in your own projects

62
00:02:20,50 --> 00:02:21,70
should you choose to do so.

63
00:02:21,70 --> 00:02:23,00
Thanks for watching.

