1
00:00:00,50 --> 00:00:02,80
- [Instructor] Now, let's go over the React Toolbox,

2
00:00:02,80 --> 00:00:04,90
the specific pieces of technology you'll be using

3
00:00:04,90 --> 00:00:06,70
when you're working with React,

4
00:00:06,70 --> 00:00:09,80
especially the ones that might be a little bit new to you.

5
00:00:09,80 --> 00:00:14,40
First up is ECMAScript2015, also known as ES6,

6
00:00:14,40 --> 00:00:17,20
which is basically Modern JavaScript.

7
00:00:17,20 --> 00:00:19,40
There are some syntactical features

8
00:00:19,40 --> 00:00:23,00
and other niceties built into this new version of JavaScript

9
00:00:23,00 --> 00:00:25,10
that make it pretty pleasant to work with.

10
00:00:25,10 --> 00:00:27,60
And almost all of the React documentation is written

11
00:00:27,60 --> 00:00:30,10
targeting this version.

12
00:00:30,10 --> 00:00:34,20
You'll also see JSX, which is an extension of JavaScript.

13
00:00:34,20 --> 00:00:38,30
Basically resulting in HTML in your JavaScript.

14
00:00:38,30 --> 00:00:41,40
You'll see why we end up using JSX all the time

15
00:00:41,40 --> 00:00:42,60
when we're working with React

16
00:00:42,60 --> 00:00:44,80
as we start building things up.

17
00:00:44,80 --> 00:00:46,40
It makes things a lot more convenient

18
00:00:46,40 --> 00:00:49,10
than they would be otherwise.

19
00:00:49,10 --> 00:00:52,70
Next up is Babel, which is a JavaScript transpiler.

20
00:00:52,70 --> 00:00:54,80
That means it converts these newer and different

21
00:00:54,80 --> 00:00:56,60
JavaScript structures that we're writing

22
00:00:56,60 --> 00:00:59,30
into code that's more compatible across more browsers.

23
00:00:59,30 --> 00:01:02,90
So for example, it can convert JSX into JavaScript,

24
00:01:02,90 --> 00:01:05,70
and it can convert ES6 into ES5,

25
00:01:05,70 --> 00:01:09,10
a version of JavaScript that basically everything can run.

26
00:01:09,10 --> 00:01:11,60
You'll also see Yarn or npm,

27
00:01:11,60 --> 00:01:14,20
which are dependency managers for JavaScript.

28
00:01:14,20 --> 00:01:17,50
This is the way that you'll install JavaScript libraries,

29
00:01:17,50 --> 00:01:19,50
which you're going to incorporate into your project,

30
00:01:19,50 --> 00:01:20,90
which includes React itself,

31
00:01:20,90 --> 00:01:23,10
and then any supporting components

32
00:01:23,10 --> 00:01:24,60
or other pieces of JavaScript

33
00:01:24,60 --> 00:01:27,00
you might want to bring into your project.

34
00:01:27,00 --> 00:01:29,40
Some of these libraries depend on other libraries.

35
00:01:29,40 --> 00:01:31,20
So if you had to track all that stuff by hand,

36
00:01:31,20 --> 00:01:33,00
it could get to be very problematic,

37
00:01:33,00 --> 00:01:34,60
especially if you're working in teams

38
00:01:34,60 --> 00:01:35,40
where you need everybody

39
00:01:35,40 --> 00:01:38,30
to be working on the same versions of everything.

40
00:01:38,30 --> 00:01:40,90
So these tools help you work through those problems

41
00:01:40,90 --> 00:01:44,30
and make them not problems so much.

42
00:01:44,30 --> 00:01:45,70
And finally webpack.

43
00:01:45,70 --> 00:01:47,40
Webpack is a JavaScript build tool

44
00:01:47,40 --> 00:01:48,80
that's preferred in the React community

45
00:01:48,80 --> 00:01:50,70
and used elsewhere as well.

46
00:01:50,70 --> 00:01:52,20
It's a little bit tricky to learn,

47
00:01:52,20 --> 00:01:53,60
but it solves a lot of problems

48
00:01:53,60 --> 00:01:55,00
that you may run into someday.

49
00:01:55,00 --> 00:01:56,80
For now, you should just know that it's there,

50
00:01:56,80 --> 00:01:58,50
but we're not going to be using it.

51
00:01:58,50 --> 00:01:59,40
For more on webpack,

52
00:01:59,40 --> 00:02:02,80
you can check out other courses in the library.

53
00:02:02,80 --> 00:02:04,20
The main takeaway for this section,

54
00:02:04,20 --> 00:02:05,90
other than introducing these tools to you

55
00:02:05,90 --> 00:02:08,10
if you're not already aware of them is this

56
00:02:08,10 --> 00:02:10,70
the community assumes that most people are using them,

57
00:02:10,70 --> 00:02:12,80
so most of the documentation that you'll be reading

58
00:02:12,80 --> 00:02:15,50
assumes that you're using ES6 and JSX,

59
00:02:15,50 --> 00:02:19,10
and bundling everything using webpack or something like it.

60
00:02:19,10 --> 00:02:21,20
Once you've gone through the basics with React,

61
00:02:21,20 --> 00:02:23,00
if you want to try building a website with React

62
00:02:23,00 --> 00:02:24,20
and all the tools and trimmings

63
00:02:24,20 --> 00:02:26,00
sensibly preconfigured for you,

64
00:02:26,00 --> 00:02:28,70
take a look a Create React App.

65
00:02:28,70 --> 00:02:30,40
You get everything we discussed here

66
00:02:30,40 --> 00:02:32,20
plus unit testing and linting tools

67
00:02:32,20 --> 00:02:36,20
to help with code quality and lots of other stuff.

68
00:02:36,20 --> 00:02:38,90
As you'll see, you don't have to use all these tools

69
00:02:38,90 --> 00:02:40,70
to get started with React.

70
00:02:40,70 --> 00:02:41,90
I specifically wanted to make sure

71
00:02:41,90 --> 00:02:43,50
that you didn't have to learn a ton of new stuff

72
00:02:43,50 --> 00:02:44,30
to get going.

73
00:02:44,30 --> 00:02:46,30
That said, each tool we discussed here

74
00:02:46,30 --> 00:02:48,00
does serve useful purposes,

75
00:02:48,00 --> 00:02:49,40
so I'll introduce some of them as we go

76
00:02:49,40 --> 00:02:50,70
in a way that should let you see

77
00:02:50,70 --> 00:02:52,00
how they can be helpful to you.

