1
00:00:00,40 --> 00:00:01,50
- [Presenter] Over the next several videos,

2
00:00:01,50 --> 00:00:03,70
we're going to talk about some of the conceptual aspects

3
00:00:03,70 --> 00:00:06,30
of working with React as well as the practical.

4
00:00:06,30 --> 00:00:09,00
So let's start with some basic questions and answers.

5
00:00:09,00 --> 00:00:11,70
First, what is React?

6
00:00:11,70 --> 00:00:16,00
React is a JavaScript library for creating user interfaces.

7
00:00:16,00 --> 00:00:17,90
It's used by a lot of companies

8
00:00:17,90 --> 00:00:20,90
and people, hugely popular right now.

9
00:00:20,90 --> 00:00:22,80
This includes of course, Facebook and Instagram.

10
00:00:22,80 --> 00:00:25,90
Facebook leads the effort to make React.

11
00:00:25,90 --> 00:00:29,40
You'll also see React in use on almost every Wordpress site

12
00:00:29,40 --> 00:00:31,30
which is probably going to be even more popular

13
00:00:31,30 --> 00:00:34,80
going forward with some of the efforts in that community.

14
00:00:34,80 --> 00:00:36,90
You'll also see it in many other companies

15
00:00:36,90 --> 00:00:41,70
like Airbnb, even Twitter and the New York Times.

16
00:00:41,70 --> 00:00:44,10
So, where does React work?

17
00:00:44,10 --> 00:00:46,40
This course is for Web Designers so we'll be concentrating

18
00:00:46,40 --> 00:00:48,90
mainly on the browser, which is powered

19
00:00:48,90 --> 00:00:52,90
by a specific renderer in React called ReactDOM.

20
00:00:52,90 --> 00:00:55,30
But you can also render React components

21
00:00:55,30 --> 00:00:57,20
on the server which are then served

22
00:00:57,20 --> 00:01:01,00
to the browser, using ReactDOMServer.

23
00:01:01,00 --> 00:01:02,70
And you may have heard that you can use React

24
00:01:02,70 --> 00:01:06,80
to create native apps using React Native.

25
00:01:06,80 --> 00:01:08,90
These all involve different kinds of components

26
00:01:08,90 --> 00:01:10,80
and different kinds of renderers.

27
00:01:10,80 --> 00:01:12,20
There are lots of different ones

28
00:01:12,20 --> 00:01:15,10
supporting all different kinds of environments.

29
00:01:15,10 --> 00:01:16,80
You could see a list of some of the many,

30
00:01:16,80 --> 00:01:19,20
many places that React can be rendered

31
00:01:19,20 --> 00:01:23,70
at this link on GitHub, it's pretty fascinating.

32
00:01:23,70 --> 00:01:25,40
So the next question might be,

33
00:01:25,40 --> 00:01:27,70
how would I work with React?

34
00:01:27,70 --> 00:01:30,90
Well you can build an entire site or app using React.

35
00:01:30,90 --> 00:01:33,10
Soup to nuts, you can do the whole thing.

36
00:01:33,10 --> 00:01:35,10
But you don't have to, you can also build

37
00:01:35,10 --> 00:01:37,80
just one piece of an existing site

38
00:01:37,80 --> 00:01:40,50
or integrate React into an existing code base.

39
00:01:40,50 --> 00:01:43,50
That's what we're going to be doing in this course.

40
00:01:43,50 --> 00:01:44,90
What is React good for?

41
00:01:44,90 --> 00:01:47,40
What kinds of things can we do with it?

42
00:01:47,40 --> 00:01:49,30
It's really good for making repeatable,

43
00:01:49,30 --> 00:01:51,70
reusable user interface elements.

44
00:01:51,70 --> 00:01:54,60
You'll see it all the time for interactive form components

45
00:01:54,60 --> 00:01:57,20
and different things that will change on a page.

46
00:01:57,20 --> 00:01:59,70
It's really good for any kind of data display

47
00:01:59,70 --> 00:02:02,20
that changes over time based on user input,

48
00:02:02,20 --> 00:02:04,70
changes based on messages coming from a server

49
00:02:04,70 --> 00:02:06,80
from different kinds of APIs,

50
00:02:06,80 --> 00:02:08,90
anything that changes over time.

51
00:02:08,90 --> 00:02:10,90
It's also really good for making apps and websites

52
00:02:10,90 --> 00:02:13,10
built entirely with JavaScript.

53
00:02:13,10 --> 00:02:14,60
If you're really into JavaScript,

54
00:02:14,60 --> 00:02:17,40
then using React will get you into almost any kind

55
00:02:17,40 --> 00:02:20,20
of environment where you might want to deploy something.

56
00:02:20,20 --> 00:02:23,50
A site could be offered entirely with JavaScript and React,

57
00:02:23,50 --> 00:02:25,30
rendered on the server and served as HTML

58
00:02:25,30 --> 00:02:27,20
with JavaScript taking over afterwards,

59
00:02:27,20 --> 00:02:29,60
that's a pretty popular pattern.

60
00:02:29,60 --> 00:02:31,30
You can also use static site generators,

61
00:02:31,30 --> 00:02:32,90
like Gatsby, which is what powers

62
00:02:32,90 --> 00:02:35,20
the documentation for React itself.

63
00:02:35,20 --> 00:02:39,70
It's built in React, but then it's served as static HTML.

64
00:02:39,70 --> 00:02:41,20
One thing that's worth mentioning about React

65
00:02:41,20 --> 00:02:44,20
is that web components are a different technology.

66
00:02:44,20 --> 00:02:46,70
React is component based, we're building up a series

67
00:02:46,70 --> 00:02:49,30
of components and putting them together on our pages.

68
00:02:49,30 --> 00:02:50,80
The web components is a different set

69
00:02:50,80 --> 00:02:52,90
of technologies which are standardized.

70
00:02:52,90 --> 00:02:54,70
You could check out Learning Web Components

71
00:02:54,70 --> 00:02:58,80
and other courses in the library to learn more about this.

72
00:02:58,80 --> 00:02:59,90
So that's a quick overview

73
00:02:59,90 --> 00:03:03,00
of what React is and what it's good for.

