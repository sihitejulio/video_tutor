1
00:00:00,50 --> 00:00:02,40
- [Instructor] We've looked at the way most react sites

2
00:00:02,40 --> 00:00:05,20
are assumed to be built, but we're going to do things

3
00:00:05,20 --> 00:00:06,90
a little differently in this course to help you dip

4
00:00:06,90 --> 00:00:09,70
your toes into the react waters.

5
00:00:09,70 --> 00:00:11,70
So specifically at first we're going to have no build

6
00:00:11,70 --> 00:00:14,80
process at all, we're going to install react with a couple

7
00:00:14,80 --> 00:00:17,40
of simple script tags, the same way you've installed

8
00:00:17,40 --> 00:00:20,80
any other JavaScript library at any time in the past.

9
00:00:20,80 --> 00:00:23,10
Use the right ones in the right order and you're good

10
00:00:23,10 --> 00:00:24,90
to go, same as it ever was.

11
00:00:24,90 --> 00:00:27,80
There are some syntactical differences that you'll

12
00:00:27,80 --> 00:00:30,20
experience in your code versus what you'll see

13
00:00:30,20 --> 00:00:32,90
in the documentation, mostly having to specify a little bit

14
00:00:32,90 --> 00:00:34,90
more of where your components are coming from,

15
00:00:34,90 --> 00:00:37,30
but basically you're not going to have to deal with any

16
00:00:37,30 --> 00:00:39,80
build tools at all.

17
00:00:39,80 --> 00:00:42,20
We're going to add on some build tools later as it becomes

18
00:00:42,20 --> 00:00:44,80
appropriate to help us work with some different things,

19
00:00:44,80 --> 00:00:47,00
but to get started, you don't need a thing.

