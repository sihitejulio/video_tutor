1
00:00:00,50 --> 00:00:02,10
- [Teacher] React has a browser extension

2
00:00:02,10 --> 00:00:04,30
for Chrome and Firefox that makes debugging

3
00:00:04,30 --> 00:00:06,80
and learning about React a little bit easier.

4
00:00:06,80 --> 00:00:08,80
In this video, we're going to install those extensions

5
00:00:08,80 --> 00:00:10,30
and see how they work.

6
00:00:10,30 --> 00:00:13,40
So here is the mobile Twitter site,

7
00:00:13,40 --> 00:00:15,40
specifically my timeline,

8
00:00:15,40 --> 00:00:18,40
and we're going to install the Chrome extension

9
00:00:18,40 --> 00:00:23,80
and take a look at what React is doing on this page.

10
00:00:23,80 --> 00:00:24,90
So I'm going to open up the menu

11
00:00:24,90 --> 00:00:29,00
and go to extensions under more tools.

12
00:00:29,00 --> 00:00:31,70
Here are the extensions that are installed right now.

13
00:00:31,70 --> 00:00:33,90
Click get more extensions.

14
00:00:33,90 --> 00:00:37,80
Let's search for React Developer Tools.

15
00:00:37,80 --> 00:00:40,60
Add this to Chrome.

16
00:00:40,60 --> 00:00:42,10
Add extension, and there we go.

17
00:00:42,10 --> 00:00:46,00
We have this little icon up here.

18
00:00:46,00 --> 00:00:48,20
Currently grayed out.

19
00:00:48,20 --> 00:00:53,00
Close all this down, and refresh this page.

20
00:00:53,00 --> 00:00:55,00
You can see it lights up, which shows me

21
00:00:55,00 --> 00:00:57,20
that React is being used on this page.

22
00:00:57,20 --> 00:00:59,00
If I click it, it tells me

23
00:00:59,00 --> 00:01:02,10
that I'm using the production build.

24
00:01:02,10 --> 00:01:03,40
If I were using a developer build,

25
00:01:03,40 --> 00:01:05,90
it would light up in a different color.

26
00:01:05,90 --> 00:01:11,60
So let's open up the developer tools.

27
00:01:11,60 --> 00:01:14,40
You can use command + option + I on a Mac,

28
00:01:14,40 --> 00:01:16,30
or just pop it open from that menu,

29
00:01:16,30 --> 00:01:19,20
and now we have a new pane here called React.

30
00:01:19,20 --> 00:01:23,20
So it looks for wherever React is instantiated on the page

31
00:01:23,20 --> 00:01:25,10
and starts to show you the components.

32
00:01:25,10 --> 00:01:26,90
Now because we're using a production build here,

33
00:01:26,90 --> 00:01:29,50
because this is live twitter.com,

34
00:01:29,50 --> 00:01:31,10
these components are not going to be named anything

35
00:01:31,10 --> 00:01:34,00
that's particularly helpful in terms of really inspecting

36
00:01:34,00 --> 00:01:35,90
how they've put this thing together,

37
00:01:35,90 --> 00:01:40,70
but these tools will show you what components are available.

38
00:01:40,70 --> 00:01:45,10
You can see there are a lot of them on this page.

39
00:01:45,10 --> 00:01:48,80
And you can see different aspects of props and state,

40
00:01:48,80 --> 00:01:50,90
and other objects depending on what other pieces

41
00:01:50,90 --> 00:01:53,30
of the React ecosystem are involved

42
00:01:53,30 --> 00:01:56,50
in the construction of a particular page.

43
00:01:56,50 --> 00:01:59,60
Let's switch over to Firefox and install it here as well.

44
00:01:59,60 --> 00:02:03,30
Go into the menu and look at the add-ons.

45
00:02:03,30 --> 00:02:06,70
Extensions.

46
00:02:06,70 --> 00:02:09,90
Search for React Developer Tools here.

47
00:02:09,90 --> 00:02:15,40
And install this one.

48
00:02:15,40 --> 00:02:19,90
Now I can close the add-on manager and refresh Twitter.

49
00:02:19,90 --> 00:02:23,50
And the experience is very similar.

50
00:02:23,50 --> 00:02:29,30
I can also open the developer tools here.

51
00:02:29,30 --> 00:02:32,30
And here's React again.

52
00:02:32,30 --> 00:02:34,20
The experience of using this particular thing

53
00:02:34,20 --> 00:02:38,00
in Chrome or in Firefox is pretty much exactly the same.

54
00:02:38,00 --> 00:02:40,30
So whichever of these two browsers you happen to like,

55
00:02:40,30 --> 00:02:43,70
there's an extension available for you to use.

56
00:02:43,70 --> 00:02:45,70
We'll be looking at the React Developer Tools

57
00:02:45,70 --> 00:02:48,00
many times throughout this course,

58
00:02:48,00 --> 00:02:50,00
so if you haven't installed this extension already,

59
00:02:50,00 --> 00:02:53,00
I recommend you go and get it in your browser.

