1
00:00:00,50 --> 00:00:03,60
- [Instructor] We know that React manages and displays data.

2
00:00:03,60 --> 00:00:06,00
Let's talk a little bit about what that means conceptually.

3
00:00:06,00 --> 00:00:08,00
We'll go over some of the concepts and jargon

4
00:00:08,00 --> 00:00:10,30
that you'll experience when working with React

5
00:00:10,30 --> 00:00:12,80
and reading through the documentation.

6
00:00:12,80 --> 00:00:15,90
First of all, React helps to visualize data.

7
00:00:15,90 --> 00:00:18,40
When we're creating web pages or working with CMS

8
00:00:18,40 --> 00:00:20,20
as the generate pages for us,

9
00:00:20,20 --> 00:00:22,10
in a sense we're just displaying data.

10
00:00:22,10 --> 00:00:24,40
Our data might be products in a store, news articles,

11
00:00:24,40 --> 00:00:26,10
photographs and other artwork but in a sense,

12
00:00:26,10 --> 00:00:28,50
it's all data and of course at a lower level

13
00:00:28,50 --> 00:00:30,00
we all work with web forms which have

14
00:00:30,00 --> 00:00:33,80
their own data as well, the value in those fields.

15
00:00:33,80 --> 00:00:35,90
React is a user interface library that makes it easier

16
00:00:35,90 --> 00:00:38,40
to manage the data that's displayed in a user interface,

17
00:00:38,40 --> 00:00:40,40
in other words, visualizing the data.

18
00:00:40,40 --> 00:00:42,50
One way that React tries to make this easier

19
00:00:42,50 --> 00:00:45,00
is by its declarative nature.

20
00:00:45,00 --> 00:00:46,90
Often when we're building up interactive pages

21
00:00:46,90 --> 00:00:49,50
with unassisted JavaScript, J Query or what have you,

22
00:00:49,50 --> 00:00:51,10
we tie functions to events that say

23
00:00:51,10 --> 00:00:52,10
a button just got clicked

24
00:00:52,10 --> 00:00:55,10
or something happened on this field.

25
00:00:55,10 --> 00:00:56,80
These functions then collect all the bits of the page

26
00:00:56,80 --> 00:00:58,60
they need to fulfill whatever the interaction is

27
00:00:58,60 --> 00:00:59,60
that we've specified.

28
00:00:59,60 --> 00:01:03,30
They'll go and grab form data, DOM elements and so on.

29
00:01:03,30 --> 00:01:04,80
Then we specify what we want to happen

30
00:01:04,80 --> 00:01:07,40
with those collective bits using code that's very tied

31
00:01:07,40 --> 00:01:10,90
to the state of the DOM at any point in time.

32
00:01:10,90 --> 00:01:12,80
This kind of programming is called Imperative

33
00:01:12,80 --> 00:01:14,80
because it's basically a series of commands.

34
00:01:14,80 --> 00:01:17,00
Pretty low levels of implementation

35
00:01:17,00 --> 00:01:19,30
involving specific DOM elements and so forth.

36
00:01:19,30 --> 00:01:22,20
You can feel a little bit micromanaging.

37
00:01:22,20 --> 00:01:24,40
React works in a way that's called declarative

38
00:01:24,40 --> 00:01:27,10
where you declare or define your components and data

39
00:01:27,10 --> 00:01:29,70
and how they interact in response to events

40
00:01:29,70 --> 00:01:32,30
at a higher level and in advance.

41
00:01:32,30 --> 00:01:34,90
You still assign event handlers and so forth

42
00:01:34,90 --> 00:01:37,30
but their relationships are between high level components,

43
00:01:37,30 --> 00:01:39,20
not between specific DOM elements

44
00:01:39,20 --> 00:01:41,40
and it's managed much more by React

45
00:01:41,40 --> 00:01:43,20
than by the particulars of your code.

46
00:01:43,20 --> 00:01:44,90
I don't want to make this sound too much like magic.

47
00:01:44,90 --> 00:01:46,00
You're definitely still writing

48
00:01:46,00 --> 00:01:47,50
your fair share of JavaScript

49
00:01:47,50 --> 00:01:50,70
but React takes away some of the tedium.

50
00:01:50,70 --> 00:01:52,40
Before we leave this conceptual area,

51
00:01:52,40 --> 00:01:54,70
let's get a little more jargon out of the way.

52
00:01:54,70 --> 00:01:56,30
First, state and props.

53
00:01:56,30 --> 00:01:58,00
These are the main ways that you define

54
00:01:58,00 --> 00:02:00,00
and pass around data in React.

55
00:02:00,00 --> 00:02:01,70
We'll get into more detail on this but basically

56
00:02:01,70 --> 00:02:04,90
components can own their own state and they can pass state

57
00:02:04,90 --> 00:02:07,90
to other components via props.

58
00:02:07,90 --> 00:02:09,50
One-way data flow refers to the way

59
00:02:09,50 --> 00:02:11,40
that data are passed around in React.

60
00:02:11,40 --> 00:02:14,40
It's all downhill from the top most pairing component

61
00:02:14,40 --> 00:02:16,40
down through its children.

62
00:02:16,40 --> 00:02:18,70
Data don't flow from the children upward.

63
00:02:18,70 --> 00:02:21,70
A consequence of this is something called lifting up state

64
00:02:21,70 --> 00:02:23,60
which means taking state data that's defined

65
00:02:23,60 --> 00:02:26,90
in one component and moving it up to some parent component.

66
00:02:26,90 --> 00:02:28,40
That's a common part of the React workflow

67
00:02:28,40 --> 00:02:30,60
when you're figuring out where things should go.

68
00:02:30,60 --> 00:02:34,00
Something you'll often do when you're refactoring.

69
00:02:34,00 --> 00:02:36,80
React maintains its own representation of the DOM

70
00:02:36,80 --> 00:02:39,00
for the purpose of figuring out what has changed,

71
00:02:39,00 --> 00:02:40,40
what needs to be updated on your page

72
00:02:40,40 --> 00:02:42,00
in response to those changes.

73
00:02:42,00 --> 00:02:44,00
This virtual DOM is one of the main ingredients

74
00:02:44,00 --> 00:02:46,40
in React's secret sauce.

75
00:02:46,40 --> 00:02:48,70
And then finally synthetic events.

76
00:02:48,70 --> 00:02:50,50
Instead of using standard DOM events,

77
00:02:50,50 --> 00:02:52,40
React has a thin wrapper on top of them

78
00:02:52,40 --> 00:02:54,30
to minimize compatibility issues

79
00:02:54,30 --> 00:02:56,20
and to standardize the way certain events work

80
00:02:56,20 --> 00:02:58,30
a bit more than they do in real browsers.

81
00:02:58,30 --> 00:03:00,10
They work largely the same, it's just worth knowing

82
00:03:00,10 --> 00:03:02,70
that they're a little different.

83
00:03:02,70 --> 00:03:04,70
So that's a little bit of the concepts and jargon

84
00:03:04,70 --> 00:03:06,50
that you'll see working with React

85
00:03:06,50 --> 00:03:09,00
and especially reading through the documentation.

