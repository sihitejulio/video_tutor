1
00:00:00,50 --> 00:00:02,60
- [Instructor] Before I started working with React,

2
00:00:02,60 --> 00:00:03,80
one of the things I had heard about

3
00:00:03,80 --> 00:00:07,60
was the weird, to me, way that people worked with CSS.

4
00:00:07,60 --> 00:00:09,40
So let's talk about the special relationship

5
00:00:09,40 --> 00:00:12,60
between React and CSS.

6
00:00:12,60 --> 00:00:14,90
Right off the top, know that you don't have to change

7
00:00:14,90 --> 00:00:16,80
the way you work with CSS,

8
00:00:16,80 --> 00:00:18,40
especially if you're integrating React

9
00:00:18,40 --> 00:00:19,60
into an existing code base,

10
00:00:19,60 --> 00:00:21,30
the way we're doing in this course.

11
00:00:21,30 --> 00:00:23,20
You can still create classes on your components

12
00:00:23,20 --> 00:00:25,60
and style them in CSS files.

13
00:00:25,60 --> 00:00:27,00
That still works just fine,

14
00:00:27,00 --> 00:00:28,60
and indeed, that's how we'll be working,

15
00:00:28,60 --> 00:00:31,90
because we're starting with static-styled mock ups.

16
00:00:31,90 --> 00:00:33,10
On relatively small projects,

17
00:00:33,10 --> 00:00:36,80
you're unlikely to run into any problems with this approach.

18
00:00:36,80 --> 00:00:39,30
One of the principles that drives how React is built

19
00:00:39,30 --> 00:00:41,50
is separation of concerns.

20
00:00:41,50 --> 00:00:42,40
You might think, of course,

21
00:00:42,40 --> 00:00:44,60
that's why my HTML, CSS, and JavaScript

22
00:00:44,60 --> 00:00:47,40
are all separate, and I put them in separate files.

23
00:00:47,40 --> 00:00:48,40
But that's not the same thing.

24
00:00:48,40 --> 00:00:52,00
That's more like separation of technologies.

25
00:00:52,00 --> 00:00:54,20
On large projects like, say, Facebook

26
00:00:54,20 --> 00:00:56,80
with lots of people working on lots of moving parts,

27
00:00:56,80 --> 00:00:59,00
the amount of CSS can become burdensome.

28
00:00:59,00 --> 00:01:01,00
If I need a new class, what do I call it?

29
00:01:01,00 --> 00:01:03,30
Will it mess up some other part of the app?

30
00:01:03,30 --> 00:01:04,80
Separation of concerns means

31
00:01:04,80 --> 00:01:06,70
components are more independent,

32
00:01:06,70 --> 00:01:08,50
making them reusable and needing to know

33
00:01:08,50 --> 00:01:10,40
as little as possible about the specifics

34
00:01:10,40 --> 00:01:13,30
about how other components work and are styled

35
00:01:13,30 --> 00:01:15,70
in order to play nicely together.

36
00:01:15,70 --> 00:01:17,30
This impacts our use of CSS,

37
00:01:17,30 --> 00:01:19,70
because using bundling tools like webpack,

38
00:01:19,70 --> 00:01:21,90
not only can we include the mark-up we need

39
00:01:21,90 --> 00:01:23,40
to make a component render correctly,

40
00:01:23,40 --> 00:01:25,70
we can include CSS as well,

41
00:01:25,70 --> 00:01:27,10
and do it in a way that's safe to use

42
00:01:27,10 --> 00:01:28,70
in large code bases.

43
00:01:28,70 --> 00:01:30,90
This is CSS in JavaScript.

44
00:01:30,90 --> 00:01:34,50
Our build tools can safely manage styles by inlining them,

45
00:01:34,50 --> 00:01:37,00
generating safe class names or other approaches

46
00:01:37,00 --> 00:01:38,50
to allow components to maintain

47
00:01:38,50 --> 00:01:40,80
that separation of concerns.

48
00:01:40,80 --> 00:01:43,60
You can find out more about CSS in JavaScript approaches,

49
00:01:43,60 --> 00:01:46,10
especially techniques like CSS modules,

50
00:01:46,10 --> 00:01:49,30
in the styling and CSS section of the React docs.

51
00:01:49,30 --> 00:01:50,70
Again, I want to emphasize

52
00:01:50,70 --> 00:01:52,90
that you don't have to change your approach.

53
00:01:52,90 --> 00:01:54,60
It's just that CSS in JavaScript

54
00:01:54,60 --> 00:01:56,70
is a pretty big deal in the React community,

55
00:01:56,70 --> 00:01:58,00
and you should be aware of it.

