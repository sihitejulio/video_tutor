1
00:00:00,50 --> 00:00:03,30
- Now we have all of our components in place.

2
00:00:03,30 --> 00:00:05,50
But if we make a selection, nothing changes.

3
00:00:05,50 --> 00:00:07,00
The list of five colors is the same,

4
00:00:07,00 --> 00:00:09,00
no matter what size I select.

5
00:00:09,00 --> 00:00:10,00
And if I change the color,

6
00:00:10,00 --> 00:00:11,50
the shoe doesn't change,

7
00:00:11,50 --> 00:00:12,50
the sizes don't change,

8
00:00:12,50 --> 00:00:13,90
nothing changes.

9
00:00:13,90 --> 00:00:16,10
So we need to make some changes to our code,

10
00:00:16,10 --> 00:00:17,90
so that these events that fire in the browser,

11
00:00:17,90 --> 00:00:20,50
actually make some updates on the page.

12
00:00:20,50 --> 00:00:22,70
We're going to do that for the first time in this video,

13
00:00:22,70 --> 00:00:25,20
wiring up this size selector to update the list

14
00:00:25,20 --> 00:00:28,10
of colors that are available in that size.

15
00:00:28,10 --> 00:00:29,60
So over here in my code,

16
00:00:29,60 --> 00:00:32,50
we need to wire something up to this select box.

17
00:00:32,50 --> 00:00:35,30
Specifically, we need to listen for the change event.

18
00:00:35,30 --> 00:00:37,10
And to do that on React,

19
00:00:37,10 --> 00:00:39,50
we set a prop called onChange.

20
00:00:39,50 --> 00:00:41,30
These are just like the inline event handlers

21
00:00:41,30 --> 00:00:44,50
that you see in regular HTML and JavaScript.

22
00:00:44,50 --> 00:00:47,00
In React, they just need to be in camel case.

23
00:00:47,00 --> 00:00:49,50
React has pretty much all the same event handlers

24
00:00:49,50 --> 00:00:52,90
that the DOM API offers with very, very rare exceptions.

25
00:00:52,90 --> 00:00:55,10
So if you can remember what it was called in HTML,

26
00:00:55,10 --> 00:00:57,60
odds are the same thing is going to work in React.

27
00:00:57,60 --> 00:00:59,10
We're going to need a handler that fires

28
00:00:59,10 --> 00:01:00,70
when this event happens.

29
00:01:00,70 --> 00:01:03,70
So we will call this onSizeChange.

30
00:01:03,70 --> 00:01:04,80
And that's a function that we need

31
00:01:04,80 --> 00:01:06,90
to define in this component.

32
00:01:06,90 --> 00:01:08,40
So I'll copy this name,

33
00:01:08,40 --> 00:01:10,40
come up here and we'll add this function.

34
00:01:10,40 --> 00:01:12,30
And as with almost any event handler,

35
00:01:12,30 --> 00:01:14,40
we can pass in an event object.

36
00:01:14,40 --> 00:01:16,60
I'll just abbreviate it as evt.

37
00:01:16,60 --> 00:01:19,40
And now let's log it to the console.

38
00:01:19,40 --> 00:01:20,20
So I'll save this.

39
00:01:20,20 --> 00:01:22,60
Prettier reformats a little bit for me,

40
00:01:22,60 --> 00:01:25,40
and I'll switch back to the browser and refresh.

41
00:01:25,40 --> 00:01:28,30
I'll open up my Developer Tools with Command + Option + I,

42
00:01:28,30 --> 00:01:29,90
and switch to the console,

43
00:01:29,90 --> 00:01:32,00
get rid of everything that's here.

44
00:01:32,00 --> 00:01:34,50
Now let's see if this event is actually firing.

45
00:01:34,50 --> 00:01:35,30
It is.

46
00:01:35,30 --> 00:01:37,20
This is called a synthetic event.

47
00:01:37,20 --> 00:01:39,20
React has its own event handling system

48
00:01:39,20 --> 00:01:41,60
that deals with what's called, a synthetic event.

49
00:01:41,60 --> 00:01:43,50
It's very much like a browser event

50
00:01:43,50 --> 00:01:45,00
in terms of the API that it offers

51
00:01:45,00 --> 00:01:46,60
and the way that you can interface with it.

52
00:01:46,60 --> 00:01:48,20
But technically, it is a little different

53
00:01:48,20 --> 00:01:50,90
so it's worth pointing that out and being aware of it.

54
00:01:50,90 --> 00:01:52,50
With that said,

55
00:01:52,50 --> 00:01:54,20
the API is very similar.

56
00:01:54,20 --> 00:01:57,60
So if I want to get the value of this select box,

57
00:01:57,60 --> 00:01:59,30
I can use the same interface that I would use

58
00:01:59,30 --> 00:02:00,90
with a regular browser event,

59
00:02:00,90 --> 00:02:04,70
by looking at the target and its value.

60
00:02:04,70 --> 00:02:07,00
Let's save this again and go back and refresh.

61
00:02:07,00 --> 00:02:09,80
Just confirm that this works.

62
00:02:09,80 --> 00:02:12,60
Get rid of these extra messages.

63
00:02:12,60 --> 00:02:14,10
There we go, 10.

64
00:02:14,10 --> 00:02:16,40
12, and so forth.

65
00:02:16,40 --> 00:02:18,60
Okay, so now we have that value,

66
00:02:18,60 --> 00:02:21,30
but we need to use that value to update state,

67
00:02:21,30 --> 00:02:23,40
but the state does not exist in this component.

68
00:02:23,40 --> 00:02:25,60
It exists in the parent.

69
00:02:25,60 --> 00:02:27,00
React's one way data flow means

70
00:02:27,00 --> 00:02:29,30
that we can't manipulate the state directly

71
00:02:29,30 --> 00:02:31,60
from inside this child component.

72
00:02:31,60 --> 00:02:34,20
This is a tricky thing about working with React at first.

73
00:02:34,20 --> 00:02:36,10
So the question is, how do we deal with this?

74
00:02:36,10 --> 00:02:38,30
We need a function that can see the state data,

75
00:02:38,30 --> 00:02:39,90
which means it needs to come from the parent

76
00:02:39,90 --> 00:02:40,80
where the state is.

77
00:02:40,80 --> 00:02:41,70
Bear with me, I understand

78
00:02:41,70 --> 00:02:43,10
this might be getting a little weird,

79
00:02:43,10 --> 00:02:45,10
but here's how it works.

80
00:02:45,10 --> 00:02:46,50
I need to create a function

81
00:02:46,50 --> 00:02:48,90
that can update that state down here

82
00:02:48,90 --> 00:02:50,70
in the ProductCustomizer component

83
00:02:50,70 --> 00:02:52,60
where the state data lives.

84
00:02:52,60 --> 00:02:53,90
And then I pass that function down

85
00:02:53,90 --> 00:02:56,40
into the component of interest as a prop.

86
00:02:56,40 --> 00:02:58,80
So specifically, what I'm going to do here

87
00:02:58,80 --> 00:03:03,40
is pass in something that I'll call handleSizeChange.

88
00:03:03,40 --> 00:03:04,60
It's a pretty common pattern to use

89
00:03:04,60 --> 00:03:08,00
on a handle for these different pieces in React.

90
00:03:08,00 --> 00:03:11,60
I'm going to call the function handleSizeChange as well,

91
00:03:11,60 --> 00:03:13,60
but it needs to be defined here.

92
00:03:13,60 --> 00:03:15,20
So here's my function.

93
00:03:15,20 --> 00:03:18,60
So I need to figure out how I want this function to work.

94
00:03:18,60 --> 00:03:20,90
I want a little bit of separation of concerns.

95
00:03:20,90 --> 00:03:23,90
So I don't necessarily want to pass in the event to this.

96
00:03:23,90 --> 00:03:26,30
I just want to find out what the size is,

97
00:03:26,30 --> 00:03:29,00
and then update state with that new value.

98
00:03:29,00 --> 00:03:32,30
So let's say I'm going to pass in the selectedSize,

99
00:03:32,30 --> 00:03:33,20
and then I'll act on it,

100
00:03:33,20 --> 00:03:35,10
in the body of this function.

101
00:03:35,10 --> 00:03:36,70
So we'll do that in a second.

102
00:03:36,70 --> 00:03:39,70
But now let's use this handleSizeChange

103
00:03:39,70 --> 00:03:41,90
up here in our SizeSelector component.

104
00:03:41,90 --> 00:03:44,10
So again, onSize change,

105
00:03:44,10 --> 00:03:46,40
I now have a prop available.

106
00:03:46,40 --> 00:03:50,20
I'll get from my props the handleSizeChange function

107
00:03:50,20 --> 00:03:53,00
and I'll pass to that function this value.

108
00:03:53,00 --> 00:03:55,30
And because of the way that my functions are scoped,

109
00:03:55,30 --> 00:03:58,10
this function, even though I'm passing it in as a prop,

110
00:03:58,10 --> 00:04:01,60
has access to all of the state data right here.

111
00:04:01,60 --> 00:04:03,10
Once I've gotten to this point,

112
00:04:03,10 --> 00:04:05,20
I now can actually take some action.

113
00:04:05,20 --> 00:04:06,30
So the first thing that I need to do,

114
00:04:06,30 --> 00:04:08,10
is figure out what colors are available

115
00:04:08,10 --> 00:04:09,50
for that selected size.

116
00:04:09,50 --> 00:04:11,10
And just to remind you,

117
00:04:11,10 --> 00:04:13,50
I'm going to use Command + P to open up my inventory file.

118
00:04:13,50 --> 00:04:17,30
You can start to type it in here, Ch02-inventory.

119
00:04:17,30 --> 00:04:18,70
And what I'm doing,

120
00:04:18,70 --> 00:04:22,10
is I'm asking my little fake API,

121
00:04:22,10 --> 00:04:25,30
what colors are available by size.

122
00:04:25,30 --> 00:04:28,20
I'm going to be looking at window.Inventory.bySize

123
00:04:28,20 --> 00:04:32,70
and then the index should be the currently selected size.

124
00:04:32,70 --> 00:04:37,00
So what I want is the availableColors.

125
00:04:37,00 --> 00:04:41,60
That'll be window.Inventory.bySize

126
00:04:41,60 --> 00:04:43,60
sub selectedSize.

127
00:04:43,60 --> 00:04:44,60
And that's going to give me back

128
00:04:44,60 --> 00:04:47,40
the array of available colors.

129
00:04:47,40 --> 00:04:49,40
And now I will update the state,

130
00:04:49,40 --> 00:04:51,30
using this setColors function

131
00:04:51,30 --> 00:04:54,10
that comes back from my useState Hook.

132
00:04:54,10 --> 00:04:55,10
I will set the colors

133
00:04:55,10 --> 00:04:57,60
to the available colors that I just got.

134
00:04:57,60 --> 00:04:58,40
Okay.

135
00:04:58,40 --> 00:05:00,90
Okay, now I can save this and try it out.

136
00:05:00,90 --> 00:05:02,30
So switching back to my browser,

137
00:05:02,30 --> 00:05:04,90
I will refresh.

138
00:05:04,90 --> 00:05:07,60
And now if I choose 7 as my selected size,

139
00:05:07,60 --> 00:05:09,40
I can see that something changed here,

140
00:05:09,40 --> 00:05:11,20
and now I just have red and blue.

141
00:05:11,20 --> 00:05:12,60
It worked.

142
00:05:12,60 --> 00:05:14,60
I change the size to 12,

143
00:05:14,60 --> 00:05:16,10
I have three colors.

144
00:05:16,10 --> 00:05:16,90
Great.

145
00:05:16,90 --> 00:05:20,20
If I open up my Developer Tools and switch to the React tab,

146
00:05:20,20 --> 00:05:23,70
I'll observe my ColorSelector component here,

147
00:05:23,70 --> 00:05:25,90
and I can watch these things change

148
00:05:25,90 --> 00:05:28,00
as I make my different selections.

149
00:05:28,00 --> 00:05:29,70
So to recap,

150
00:05:29,70 --> 00:05:31,30
here in my SizeSelector,

151
00:05:31,30 --> 00:05:35,60
I added an event handler using a camel case inline function.

152
00:05:35,60 --> 00:05:37,10
I added a handler,

153
00:05:37,10 --> 00:05:39,30
which by convention this is not required,

154
00:05:39,30 --> 00:05:42,80
but just by convention I started with on,

155
00:05:42,80 --> 00:05:44,80
and then that calls a prop that's passed down

156
00:05:44,80 --> 00:05:46,10
from the parent component,

157
00:05:46,10 --> 00:05:48,70
who actually has access to that state,

158
00:05:48,70 --> 00:05:51,10
which is down here in the ProductCustomizer.

159
00:05:51,10 --> 00:05:52,50
And that handler function,

160
00:05:52,50 --> 00:05:54,00
does whatever I need to do,

161
00:05:54,00 --> 00:05:55,50
finally updating state,

162
00:05:55,50 --> 00:05:58,40
using the function that came back from my useState Hook.

163
00:05:58,40 --> 00:06:00,20
We definitely had to get a little into the weeds

164
00:06:00,20 --> 00:06:01,20
for a little bit there,

165
00:06:01,20 --> 00:06:02,80
but now we have React doing our bidding

166
00:06:02,80 --> 00:06:04,60
in a way that's genuinely interesting.

167
00:06:04,60 --> 00:06:06,10
We update some data in our script,

168
00:06:06,10 --> 00:06:08,30
and React figures out wherever on the page needs

169
00:06:08,30 --> 00:06:10,30
to be updated as a result of that change,

170
00:06:10,30 --> 00:06:12,70
and handles it how we've declared that it should be.

171
00:06:12,70 --> 00:06:14,00
Nice.

