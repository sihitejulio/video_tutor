1
00:00:00,60 --> 00:00:01,50
- [Instructor] In this video we're going

2
00:00:01,50 --> 00:00:04,00
to implement State for the first time

3
00:00:04,00 --> 00:00:06,70
with the help of React's useState hook.

4
00:00:06,70 --> 00:00:09,70
Hooks are a relatively recent and nifty addition to React.

5
00:00:09,70 --> 00:00:11,70
Their function is to let your function components

6
00:00:11,70 --> 00:00:13,70
access features that used to be accessible

7
00:00:13,70 --> 00:00:15,50
only by class components.

8
00:00:15,50 --> 00:00:17,90
State is one of those features, and as we know,

9
00:00:17,90 --> 00:00:20,20
state lets the component track the condition of data

10
00:00:20,20 --> 00:00:23,00
and update itself when those data change.

11
00:00:23,00 --> 00:00:25,10
So all that said, let's give this a whirl.

12
00:00:25,10 --> 00:00:26,60
Looking at how things are right now,

13
00:00:26,60 --> 00:00:28,00
there are a few pieces of data

14
00:00:28,00 --> 00:00:29,50
that we're going to need to track.

15
00:00:29,50 --> 00:00:31,80
Let's go with the current color of the shoe,

16
00:00:31,80 --> 00:00:33,70
the currently selected size,

17
00:00:33,70 --> 00:00:36,00
and all of the sizes that are available.

18
00:00:36,00 --> 00:00:37,60
We're using a hard coded data source right now

19
00:00:37,60 --> 00:00:40,20
for all of these sizes, but theoretically they might change

20
00:00:40,20 --> 00:00:42,80
if we were accessing an API, so we'll treat them

21
00:00:42,80 --> 00:00:44,90
as something that might be changeable at some point.

22
00:00:44,90 --> 00:00:48,00
So current color, current size, all sizes.

23
00:00:48,00 --> 00:00:50,40
Over here in my code, we're going to scroll down

24
00:00:50,40 --> 00:00:52,70
to the product customizer component.

25
00:00:52,70 --> 00:00:54,40
This is where all the state will be stored

26
00:00:54,40 --> 00:00:56,90
so they're available to all our child components.

27
00:00:56,90 --> 00:00:59,60
You can see we have color and size hard coded right here,

28
00:00:59,60 --> 00:01:02,20
and we're going to add in all sizes as well.

29
00:01:02,20 --> 00:01:05,70
So we'll start by dealing with the currently selected size.

30
00:01:05,70 --> 00:01:08,30
The way you implement a hook in React

31
00:01:08,30 --> 00:01:10,70
is by using a function.

32
00:01:10,70 --> 00:01:13,90
In this case, it's one called use state.

33
00:01:13,90 --> 00:01:15,50
And the way this functions works,

34
00:01:15,50 --> 00:01:17,80
is you pass in the current value,

35
00:01:17,80 --> 00:01:20,80
the initial value that you want that piece of state to hold.

36
00:01:20,80 --> 00:01:23,20
And we will use eight just like we have here.

37
00:01:23,20 --> 00:01:26,70
And this function returns an array with two elements.

38
00:01:26,70 --> 00:01:28,80
The first element is a variable that will track

39
00:01:28,80 --> 00:01:32,00
the current value of that piece of state,

40
00:01:32,00 --> 00:01:35,00
and then the second element is a function

41
00:01:35,00 --> 00:01:37,80
that allows you to change the value of that state.

42
00:01:37,80 --> 00:01:40,10
We're going to use some ES6 syntax

43
00:01:40,10 --> 00:01:42,60
to make this a little bit easier to read.

44
00:01:42,60 --> 00:01:44,80
This is called array destructuring.

45
00:01:44,80 --> 00:01:46,30
So we're going to take that returned array

46
00:01:46,30 --> 00:01:49,90
from this function and we're going to collect

47
00:01:49,90 --> 00:01:55,20
those two items as size and set size.

48
00:01:55,20 --> 00:01:58,30
If you've never seen array destructuring in ES6,

49
00:01:58,30 --> 00:02:00,50
this is basically the same as if I said

50
00:02:00,50 --> 00:02:05,10
let's call this size state equals React use state eight,

51
00:02:05,10 --> 00:02:11,00
and then I set size to size state sub zero,

52
00:02:11,00 --> 00:02:15,10
and set size to size state sub one.

53
00:02:15,10 --> 00:02:19,10
All of this is exactly the same as this one line.

54
00:02:19,10 --> 00:02:20,20
Because we're using Babel,

55
00:02:20,20 --> 00:02:21,90
which will transpile all this stuff for us,

56
00:02:21,90 --> 00:02:24,50
we're totally happy to use some ES6 syntax here

57
00:02:24,50 --> 00:02:27,20
when it buys us something very nice like this,

58
00:02:27,20 --> 00:02:29,00
much more readable code.

59
00:02:29,00 --> 00:02:31,00
One quick thing to note, is that even though

60
00:02:31,00 --> 00:02:34,10
we're setting this current size and maintaining it as state,

61
00:02:34,10 --> 00:02:37,50
it's not currently used as the value of the select box.

62
00:02:37,50 --> 00:02:38,80
We'll deal with that later.

63
00:02:38,80 --> 00:02:41,40
So this is how we would get the size state

64
00:02:41,40 --> 00:02:42,80
into this function.

65
00:02:42,80 --> 00:02:44,80
So let's do this a couple more times,

66
00:02:44,80 --> 00:02:47,70
and we're going to collect all of our sizes,

67
00:02:47,70 --> 00:02:50,30
and we'll have state for the color,

68
00:02:50,30 --> 00:02:52,40
we'll call that function set color,

69
00:02:52,40 --> 00:02:55,90
and now we need to pass in some initial values for these.

70
00:02:55,90 --> 00:03:01,80
So this would be window dot inventory dot all sizes,

71
00:03:01,80 --> 00:03:04,90
and we'll make our initial color red,

72
00:03:04,90 --> 00:03:06,40
just like we have it here.

73
00:03:06,40 --> 00:03:08,20
Now this can seem a little verbose.

74
00:03:08,20 --> 00:03:10,40
If you end up with lots and lots of lines like this,

75
00:03:10,40 --> 00:03:11,90
you might consider trying something else

76
00:03:11,90 --> 00:03:14,20
like breaking the component up into smaller ones,

77
00:03:14,20 --> 00:03:17,90
or returning related bits of state into objects.

78
00:03:17,90 --> 00:03:19,10
State can be objects.

79
00:03:19,10 --> 00:03:20,90
It can be any value you want.

80
00:03:20,90 --> 00:03:23,10
But React doesn't do any clever merging or diffing

81
00:03:23,10 --> 00:03:25,60
of object data in the use state hook

82
00:03:25,60 --> 00:03:28,80
to figure out exactly what changed and to act accordingly.

83
00:03:28,80 --> 00:03:30,40
For that kind of cleverness,

84
00:03:30,40 --> 00:03:33,50
there's a more advanced hook called use reducer.

85
00:03:33,50 --> 00:03:34,90
Use state is easier to start with

86
00:03:34,90 --> 00:03:37,10
so that's what we're going with here.

87
00:03:37,10 --> 00:03:38,40
We're taking a little bit of verbosity

88
00:03:38,40 --> 00:03:42,30
in exchange for approachability I guess you could say.

89
00:03:42,30 --> 00:03:43,30
We have these variables,

90
00:03:43,30 --> 00:03:46,60
let's use them and pass them in as props.

91
00:03:46,60 --> 00:03:49,00
So the color is going to be color,

92
00:03:49,00 --> 00:03:52,60
the size is going to be size,

93
00:03:52,60 --> 00:03:55,80
and we also want to pass in all the sizes here.

94
00:03:55,80 --> 00:03:57,70
Now we can save this.

95
00:03:57,70 --> 00:03:59,90
And there's just one thing that remains to be done.

96
00:03:59,90 --> 00:04:02,20
Up here in my size selector component,

97
00:04:02,20 --> 00:04:04,50
I'm referring to that global object up here.

98
00:04:04,50 --> 00:04:06,40
I don't need to do that anymore.

99
00:04:06,40 --> 00:04:07,80
Instead of referencing this directly,

100
00:04:07,80 --> 00:04:10,20
I'm going to call it props dot sizes.

101
00:04:10,20 --> 00:04:12,60
And now I can save and switch back to the browser

102
00:04:12,60 --> 00:04:14,70
and make sure everything worked.

103
00:04:14,70 --> 00:04:16,90
So we will reload.

104
00:04:16,90 --> 00:04:19,00
Everything looks okay so far.

105
00:04:19,00 --> 00:04:20,60
I'm going to use command option I

106
00:04:20,60 --> 00:04:24,70
to open my developer tools and switch to the React tab.

107
00:04:24,70 --> 00:04:25,70
And let's take a look here

108
00:04:25,70 --> 00:04:28,00
at our product customizer component.

109
00:04:28,00 --> 00:04:30,30
I can see that I have all my hooks here

110
00:04:30,30 --> 00:04:31,70
with my state objects.

111
00:04:31,70 --> 00:04:35,20
Red, eight, and then this just says array 12,

112
00:04:35,20 --> 00:04:37,50
but it does represent all those sizes.

113
00:04:37,50 --> 00:04:41,10
And if I open this up and go to my sizes selector,

114
00:04:41,10 --> 00:04:43,50
I can expand all those values here.

115
00:04:43,50 --> 00:04:46,40
As of this recording, the React developer tools

116
00:04:46,40 --> 00:04:49,90
can't expand all values that come through as state hooks.

117
00:04:49,90 --> 00:04:52,30
You get a lot of it, just not every single thing.

118
00:04:52,30 --> 00:04:55,00
This will surely change in the future.

119
00:04:55,00 --> 00:04:56,40
Now you've seen how to implement state

120
00:04:56,40 --> 00:04:59,50
in your function components with React's useState hook.

121
00:04:59,50 --> 00:05:01,70
As we'll see later, you can also convert function components

122
00:05:01,70 --> 00:05:04,30
into classes if you like, but the React community appears

123
00:05:04,30 --> 00:05:06,10
to be making some effort to close the gaps

124
00:05:06,10 --> 00:05:07,60
where we might have had to do that.

125
00:05:07,60 --> 00:05:11,00
So starting with hook is, to me, the way to go.

