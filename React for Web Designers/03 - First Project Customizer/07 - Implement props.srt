1
00:00:00,50 --> 00:00:02,10
- [Instructor] In this video, we're going to implement props

2
00:00:02,10 --> 00:00:03,10
for the first time

3
00:00:03,10 --> 00:00:07,00
and see how that one way data flow happens at a basic level.

4
00:00:07,00 --> 00:00:08,10
So we've said that we need a

5
00:00:08,10 --> 00:00:11,10
top level customizer component to own the data,

6
00:00:11,10 --> 00:00:13,40
because the image needs to know the color of the sneaker,

7
00:00:13,40 --> 00:00:14,60
and of course, the color selector,

8
00:00:14,60 --> 00:00:15,90
when we eventually have one,

9
00:00:15,90 --> 00:00:18,50
is going to need to know that as well.

10
00:00:18,50 --> 00:00:19,50
So the state we care about will

11
00:00:19,50 --> 00:00:21,40
eventually come from the product customizer

12
00:00:21,40 --> 00:00:25,60
and be passed down to these child components as props.

13
00:00:25,60 --> 00:00:27,50
Anything a child component needs,

14
00:00:27,50 --> 00:00:32,20
state, callback functions, whatever, comes in as props.

15
00:00:32,20 --> 00:00:34,70
So switching back to the code,

16
00:00:34,70 --> 00:00:36,50
here's what we're going to do.

17
00:00:36,50 --> 00:00:38,80
We're going to pass in

18
00:00:38,80 --> 00:00:41,30
things that we need to these child components,

19
00:00:41,30 --> 00:00:43,00
from the product customizer.

20
00:00:43,00 --> 00:00:45,20
To start out, we're going to keep this super simple.

21
00:00:45,20 --> 00:00:48,80
We're just going to pass in some hard coded props.

22
00:00:48,80 --> 00:00:50,40
So for the product image,

23
00:00:50,40 --> 00:00:53,30
what we're going to need to know is what color to display.

24
00:00:53,30 --> 00:00:58,80
So let's set up a color prop, we'll just pass in red.

25
00:00:58,80 --> 00:01:00,30
And for the size selector,

26
00:01:00,30 --> 00:01:03,40
we're going to need to know what the current size is,

27
00:01:03,40 --> 00:01:06,30
so let's say eight for that.

28
00:01:06,30 --> 00:01:09,50
Now you'll notice I'm using quotes here for red

29
00:01:09,50 --> 00:01:11,50
and curly braces down here.

30
00:01:11,50 --> 00:01:13,90
Curly braces are for any JavaScript expression

31
00:01:13,90 --> 00:01:16,70
that needs to be passed as a prop.

32
00:01:16,70 --> 00:01:18,60
If you just need to pass a string,

33
00:01:18,60 --> 00:01:19,70
you can work with it just like

34
00:01:19,70 --> 00:01:22,20
a regular looking HTML attribute.

35
00:01:22,20 --> 00:01:23,80
But really, it's a prop,

36
00:01:23,80 --> 00:01:26,70
just it happens to be passing a string.

37
00:01:26,70 --> 00:01:29,40
So eventually, these are not going to be hard coded.

38
00:01:29,40 --> 00:01:31,20
And some other cleverness will be here

39
00:01:31,20 --> 00:01:34,30
in our product customizer to determine what these should be.

40
00:01:34,30 --> 00:01:35,70
But for now, we're just going to hard code them

41
00:01:35,70 --> 00:01:39,50
and then we're going to use them in our child components.

42
00:01:39,50 --> 00:01:43,90
So, for example, here in the size selector.

43
00:01:43,90 --> 00:01:46,00
We've already seen that we have this prop's argument

44
00:01:46,00 --> 00:01:47,90
that's passed in.

45
00:01:47,90 --> 00:01:50,00
This gives me access to any of the props

46
00:01:50,00 --> 00:01:53,20
that come in from my parents.

47
00:01:53,20 --> 00:01:55,30
And what we're going to use that size prop

48
00:01:55,30 --> 00:01:57,80
that's passed into the size selector is to determine

49
00:01:57,80 --> 00:02:02,00
what the default value for my select box should be.

50
00:02:02,00 --> 00:02:05,80
And what we want for that is default value.

51
00:02:05,80 --> 00:02:08,50
And the prop that we want to use to implement

52
00:02:08,50 --> 00:02:11,90
that default value is called default value.

53
00:02:11,90 --> 00:02:13,80
So what's that going to be?

54
00:02:13,80 --> 00:02:15,90
That's actually going to be an expression.

55
00:02:15,90 --> 00:02:17,50
And we can address what's coming in

56
00:02:17,50 --> 00:02:20,50
on the props using object syntax.

57
00:02:20,50 --> 00:02:23,30
So this will be props.size.

58
00:02:23,30 --> 00:02:25,40
I'll save the file and switch back to the browser to confirm

59
00:02:25,40 --> 00:02:29,00
that everything's working so far.

60
00:02:29,00 --> 00:02:31,00
Let's refresh this.

61
00:02:31,00 --> 00:02:33,50
And where before I had size seven selected,

62
00:02:33,50 --> 00:02:36,10
because that was the first item in the pop up menu,

63
00:02:36,10 --> 00:02:37,90
now eight is selected by default.

64
00:02:37,90 --> 00:02:40,20
Great, so far, so good.

65
00:02:40,20 --> 00:02:44,10
Now let's implement the prop on the sneaker.

66
00:02:44,10 --> 00:02:46,30
That would be the product image component here.

67
00:02:46,30 --> 00:02:48,10
We also have props and what we need to do

68
00:02:48,10 --> 00:02:51,30
is substitute this color prop for red.

69
00:02:51,30 --> 00:02:56,20
Now it's going to be the same thing here.

70
00:02:56,20 --> 00:02:58,20
We can pass this in

71
00:02:58,20 --> 00:03:02,90
and just update the size here to be props.color.

72
00:03:02,90 --> 00:03:04,40
And you can see this doesn't quite work

73
00:03:04,40 --> 00:03:06,40
because now we're converting the string literal

74
00:03:06,40 --> 00:03:08,50
into an expression.

75
00:03:08,50 --> 00:03:12,40
So we need to turn this whole thing

76
00:03:12,40 --> 00:03:16,20
into an expression by surrounding it with braces.

77
00:03:16,20 --> 00:03:18,80
So I'll switch back to the browser and refresh.

78
00:03:18,80 --> 00:03:20,90
We can see that the sneaker is still red.

79
00:03:20,90 --> 00:03:25,90
Let's change our prop to green this time, refresh again.

80
00:03:25,90 --> 00:03:29,90
Great, so that is working as well.

81
00:03:29,90 --> 00:03:31,10
One nice thing that we can do here,

82
00:03:31,10 --> 00:03:34,40
because we are using Pebble to transform our JavaScript

83
00:03:34,40 --> 00:03:39,00
is that instead of using string concatenation, like this,

84
00:03:39,00 --> 00:03:43,50
we can convert this into a template literal.

85
00:03:43,50 --> 00:03:46,70
This is a feature of ES6

86
00:03:46,70 --> 00:03:50,30
that lets us do our strings in line without having

87
00:03:50,30 --> 00:03:53,00
to do pluses and keeping track of opening

88
00:03:53,00 --> 00:03:55,90
and closing all those quote marks.

89
00:03:55,90 --> 00:03:58,50
What that looks like, is another set of curly braces

90
00:03:58,50 --> 00:04:01,00
with a dollar sign in front of it.

91
00:04:01,00 --> 00:04:04,10
We'll just put props.color inside there.

92
00:04:04,10 --> 00:04:06,00
So I can save this again,

93
00:04:06,00 --> 00:04:11,80
switch back, refresh, and there we go.

94
00:04:11,80 --> 00:04:13,80
This is just one of those syntactical niceties

95
00:04:13,80 --> 00:04:16,50
that ECMAScript 6 brings for us.

96
00:04:16,50 --> 00:04:19,40
And so we can stop there for now.

97
00:04:19,40 --> 00:04:22,60
The basics of props are just to pass them in as attributes,

98
00:04:22,60 --> 00:04:25,10
and then using them by calling them from the props object

99
00:04:25,10 --> 00:04:28,00
that you pass in to your function components.

