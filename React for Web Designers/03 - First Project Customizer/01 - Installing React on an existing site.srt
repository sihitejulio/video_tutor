1
00:00:02,00 --> 00:00:05,40
into the React water, and get React installed

2
00:00:05,40 --> 00:00:08,40
on our little project here.

3
00:00:08,40 --> 00:00:12,00
This is an existing website for H+ Sport.

4
00:00:12,00 --> 00:00:13,50
And this is the shop page.

5
00:00:13,50 --> 00:00:15,90
We're specifically going to be looking at a product page

6
00:00:15,90 --> 00:00:17,40
for these sneakers.

7
00:00:17,40 --> 00:00:19,40
As you can see there's nothing here so far.

8
00:00:19,40 --> 00:00:22,60
This is something we're going to be building up as we go.

9
00:00:22,60 --> 00:00:25,20
I'm going to switch over to the HTML files so you can see

10
00:00:25,20 --> 00:00:27,50
what our script tags look like.

11
00:00:27,50 --> 00:00:30,40
So here I am in Visual Studio Code.

12
00:00:30,40 --> 00:00:33,40
And as I said, this is an existing website.

13
00:00:33,40 --> 00:00:36,40
This is a static copy of a WordPress website, in fact.

14
00:00:36,40 --> 00:00:40,20
So there's a whole ton of HTML in this file.

15
00:00:40,20 --> 00:00:42,10
We're going to skip to the parts that are really important

16
00:00:42,10 --> 00:00:45,70
for us, as we're integrating React into this page.

17
00:00:45,70 --> 00:00:47,50
So first I'm going to use the go to line command,

18
00:00:47,50 --> 00:00:51,20
which is Control + G in Visual Studio Code by default.

19
00:00:51,20 --> 00:00:53,00
I'm going to go to a line 114.

20
00:00:53,00 --> 00:00:55,60
About halfway down the page.

21
00:00:55,60 --> 00:00:57,40
Something we're going to need when we're inserting

22
00:00:57,40 --> 00:00:58,60
our components into the page

23
00:00:58,60 --> 00:01:01,40
is a place for React to put them.

24
00:01:01,40 --> 00:01:04,40
So we have an ID here called react-root.

25
00:01:04,40 --> 00:01:06,80
All you have to know is that this exists.

26
00:01:06,80 --> 00:01:07,90
I've already stuck it in there for you

27
00:01:07,90 --> 00:01:09,00
so you don't have to create it.

28
00:01:09,00 --> 00:01:11,60
And this is where all our stuff is going to go.

29
00:01:11,60 --> 00:01:13,30
Now I'm going to jump down to the bottom of this file

30
00:01:13,30 --> 00:01:15,70
using Command + down arrow.

31
00:01:15,70 --> 00:01:19,20
We'll scroll up a little bit.

32
00:01:19,20 --> 00:01:20,50
First we have some script tags

33
00:01:20,50 --> 00:01:21,80
from the original WordPress site

34
00:01:21,80 --> 00:01:25,70
that this exercise file is based on.

35
00:01:25,70 --> 00:01:29,90
And now we get to everything that deals with React.

36
00:01:29,90 --> 00:01:31,80
So first I have a few polyfills.

37
00:01:31,80 --> 00:01:34,50
These are script tags that would fill in features

38
00:01:34,50 --> 00:01:36,70
that your browser might be missing.

39
00:01:36,70 --> 00:01:39,20
Most modern browsers are not going to require these.

40
00:01:39,20 --> 00:01:41,50
So I've kept them in HTML comments.

41
00:01:41,50 --> 00:01:42,90
But they're here if you need them.

42
00:01:42,90 --> 00:01:44,30
You can read more about these

43
00:01:44,30 --> 00:01:46,90
at the React documentation site,

44
00:01:46,90 --> 00:01:47,70
where they talk about the

45
00:01:47,70 --> 00:01:51,50
JavaScript environment requirements.

46
00:01:51,50 --> 00:01:53,70
Next I have the React tags.

47
00:01:53,70 --> 00:01:56,60
First there's React itself.

48
00:01:56,60 --> 00:01:58,20
And this is version 16,

49
00:01:58,20 --> 00:02:01,60
the latest version of version 16 for React.

50
00:02:01,60 --> 00:02:03,30
This is a development build, which means you get

51
00:02:03,30 --> 00:02:06,90
better error reporting and debugger support.

52
00:02:06,90 --> 00:02:08,90
Specifically it's coming from the unpackaged

53
00:02:08,90 --> 00:02:10,20
content distribution network.

54
00:02:10,20 --> 00:02:12,70
And this little tag here means that you're getting

55
00:02:12,70 --> 00:02:15,20
whatever the latest version is.

56
00:02:15,20 --> 00:02:18,20
Following that, I have some offline support.

57
00:02:18,20 --> 00:02:21,30
We're checking if the window.React object exists.

58
00:02:21,30 --> 00:02:23,90
And if it does not, we're pulling in a version

59
00:02:23,90 --> 00:02:25,70
of this script tag which is current

60
00:02:25,70 --> 00:02:28,40
as of when I'm recording this video.

61
00:02:28,40 --> 00:02:30,20
From here in the node modules folder,

62
00:02:30,20 --> 00:02:32,40
inside the exercise files.

63
00:02:32,40 --> 00:02:34,20
So this means that if you're online,

64
00:02:34,20 --> 00:02:36,60
you're always going to get the latest version of React 16.

65
00:02:36,60 --> 00:02:37,80
Whatever that is.

66
00:02:37,80 --> 00:02:39,80
And if you're offline, you're going to get whatever

67
00:02:39,80 --> 00:02:41,90
was current as of when I was recording this video,

68
00:02:41,90 --> 00:02:44,90
but you will still be able to work.

69
00:02:44,90 --> 00:02:47,00
Following that is the React DOM library,

70
00:02:47,00 --> 00:02:49,70
which is the renderer for putting React components

71
00:02:49,70 --> 00:02:52,50
that are made for the web, into your web pages.

72
00:02:52,50 --> 00:02:54,50
And it's the same thing with the latest version

73
00:02:54,50 --> 00:02:58,70
if you're online, and an offline version if you are not.

74
00:02:58,70 --> 00:03:01,80
And following all this is the app.js file,

75
00:03:01,80 --> 00:03:03,90
which is where all of our custom JavaScript

76
00:03:03,90 --> 00:03:05,60
is going to appear.

77
00:03:05,60 --> 00:03:07,50
So this is how you can get React installed

78
00:03:07,50 --> 00:03:09,00
without any fancy tools.

79
00:03:09,00 --> 00:03:10,40
A bunch of script tags.

80
00:03:10,40 --> 00:03:12,60
If you're not concerned about being able to work offline,

81
00:03:12,60 --> 00:03:15,20
you could just get rid of all these offline fallbacks.

82
00:03:15,20 --> 00:03:17,30
And follow that with your custom script tag,

83
00:03:17,30 --> 00:03:20,60
and off you go.

84
00:03:20,60 --> 00:03:23,70
So let's look at our actual code, here.

85
00:03:23,70 --> 00:03:25,20
We don't have anything in here except

86
00:03:25,20 --> 00:03:28,00
this little functional expression called an IFFE,

87
00:03:28,00 --> 00:03:31,70
or an immediately invoked function expression.

88
00:03:31,70 --> 00:03:34,30
Which just is a way of siloing off your code

89
00:03:34,30 --> 00:03:36,50
from the rest of the global name space.

90
00:03:36,50 --> 00:03:38,30
Just keeps you safe.

91
00:03:38,30 --> 00:03:39,80
And we have a use strict line up here

92
00:03:39,80 --> 00:03:41,20
to keep us in strict mode,

93
00:03:41,20 --> 00:03:44,10
so that we avoid errors.

94
00:03:44,10 --> 00:03:46,60
So what we're going to do now is write our first little element

95
00:03:46,60 --> 00:03:48,90
and get it rendered on the page.

96
00:03:48,90 --> 00:03:50,40
So the first thing that we're going to do

97
00:03:50,40 --> 00:03:55,40
is add a ReactDOM render line.

98
00:03:55,40 --> 00:03:57,60
ReactDOM is what will actually render our components

99
00:03:57,60 --> 00:03:59,20
into the page.

100
00:03:59,20 --> 00:04:01,50
And it takes two arguments that we really

101
00:04:01,50 --> 00:04:03,20
are interested in right now.

102
00:04:03,20 --> 00:04:05,50
The first is an element to be rendered.

103
00:04:05,50 --> 00:04:06,80
We haven't created that yet,

104
00:04:06,80 --> 00:04:09,40
so we'll just ignore that for a moment.

105
00:04:09,40 --> 00:04:11,20
And the second argument is going to be

106
00:04:11,20 --> 00:04:14,10
document.getElementById

107
00:04:14,10 --> 00:04:15,80
react-root.

108
00:04:15,80 --> 00:04:19,10
That was that place in the index.html file.

109
00:04:19,10 --> 00:04:22,80
I'm going to go back up to line 114 here.

110
00:04:22,80 --> 00:04:26,10
This is where we want our stuff to go.

111
00:04:26,10 --> 00:04:28,50
Usually this is done with an ID but you can use

112
00:04:28,50 --> 00:04:32,20
querie selector or whatever you like.

113
00:04:32,20 --> 00:04:33,90
We're going to use getElementById

114
00:04:33,90 --> 00:04:36,70
because we do have a known ID For this.

115
00:04:36,70 --> 00:04:39,60
So that's your second argument.

116
00:04:39,60 --> 00:04:42,80
For our first argument, we need an element to render.

117
00:04:42,80 --> 00:04:47,00
So I'm going to call this ProductCustomizer.

118
00:04:47,00 --> 00:04:48,70
And we haven't defined this variable yet.

119
00:04:48,70 --> 00:04:52,20
So let's do that.

120
00:04:52,20 --> 00:04:53,70
Now the way we define elements

121
00:04:53,70 --> 00:04:56,50
when we're just using React via script tags,

122
00:04:56,50 --> 00:04:59,90
is with React.createElement.

123
00:04:59,90 --> 00:05:04,20
This is a function that takes three arguments.

124
00:05:04,20 --> 00:05:05,90
The first is going to be

125
00:05:05,90 --> 00:05:08,00
what type of element we're creating.

126
00:05:08,00 --> 00:05:11,40
In this case we're just creating an HTML element.

127
00:05:11,40 --> 00:05:12,90
We just want to get something on the page

128
00:05:12,90 --> 00:05:15,70
that's basically like a hello world.

129
00:05:15,70 --> 00:05:18,60
So we're going to create a div tag.

130
00:05:18,60 --> 00:05:19,70
And then the second element

131
00:05:19,70 --> 00:05:22,50
is the props that we're going to pass into that element.

132
00:05:22,50 --> 00:05:23,60
Or the properties.

133
00:05:23,60 --> 00:05:26,30
In this case because we're creating just an HTML element,

134
00:05:26,30 --> 00:05:28,00
it's basically going to be the attributes

135
00:05:28,00 --> 00:05:31,10
that we want to set on that element.

136
00:05:31,10 --> 00:05:33,60
So we're going to set a class on this.

137
00:05:33,60 --> 00:05:36,30
All attributes on React elements,

138
00:05:36,30 --> 00:05:38,90
even if you're creating just HTML tags,

139
00:05:38,90 --> 00:05:41,70
are going to use the names of those attributes,

140
00:05:41,70 --> 00:05:43,60
as specified in the DOM API.

141
00:05:43,60 --> 00:05:46,20
So you can't just write, for example, class.

142
00:05:46,20 --> 00:05:47,80
You need to write className as you would do

143
00:05:47,80 --> 00:05:52,50
in anything else in JavaScript that interacts with a DOM.

144
00:05:52,50 --> 00:05:56,80
So let's call this customizer.

145
00:05:56,80 --> 00:05:59,00
And that's all we need for now.

146
00:05:59,00 --> 00:06:00,40
And finally,

147
00:06:00,40 --> 00:06:02,10
the third argument is going to be

148
00:06:02,10 --> 00:06:03,90
whatever it should be inside that element.

149
00:06:03,90 --> 00:06:06,70
And that can just be text for our purposes here.

150
00:06:06,70 --> 00:06:11,10
We'll say product customizer will go here.

151
00:06:11,10 --> 00:06:12,50
Now when I save this,

152
00:06:12,50 --> 00:06:14,70
my JavaScript formatter prettier

153
00:06:14,70 --> 00:06:18,30
just reformatted everything looking very nice.

154
00:06:18,30 --> 00:06:20,70
Okay so now we've created an element

155
00:06:20,70 --> 00:06:22,40
and it should render into the page.

156
00:06:22,40 --> 00:06:23,70
So let's switch back to the browser

157
00:06:23,70 --> 00:06:26,10
and take a look at it.

158
00:06:26,10 --> 00:06:28,30
If I reload this page.

159
00:06:28,30 --> 00:06:29,20
Here we go.

160
00:06:29,20 --> 00:06:31,10
Product customizer will go here.

161
00:06:31,10 --> 00:06:32,50
It worked.

162
00:06:32,50 --> 00:06:34,90
So congratulations, you've just created your first

163
00:06:34,90 --> 00:06:37,50
little tiny piece of React code.

164
00:06:37,50 --> 00:06:40,10
Before we move on, I just want to mention one other thing

165
00:06:40,10 --> 00:06:42,70
about working with React in the browser.

166
00:06:42,70 --> 00:06:44,30
I've installed the React developer tools

167
00:06:44,30 --> 00:06:46,70
here in Google Chrome.

168
00:06:46,70 --> 00:06:48,20
You can see there's a little icon here,

169
00:06:48,20 --> 00:06:51,60
that if I hover over it says React developer tools.

170
00:06:51,60 --> 00:06:53,20
But it's grayed out right now.

171
00:06:53,20 --> 00:06:55,20
So we're going to

172
00:06:55,20 --> 00:06:58,10
look at our extensions here under more tools

173
00:06:58,10 --> 00:06:59,60
and extensions,

174
00:06:59,60 --> 00:07:02,80
to get a little more information about these tools.

175
00:07:02,80 --> 00:07:04,40
I'm going to look at the details

176
00:07:04,40 --> 00:07:05,80
and there's a piece of configuration

177
00:07:05,80 --> 00:07:08,80
that I need to change here.

178
00:07:08,80 --> 00:07:10,50
Specifically I need to tell this extension

179
00:07:10,50 --> 00:07:12,90
to allow access to file URLs.

180
00:07:12,90 --> 00:07:15,00
Because right now I've just dropped the file

181
00:07:15,00 --> 00:07:19,40
into my browser with no local web server.

182
00:07:19,40 --> 00:07:24,00
So I need to toggle this on.

183
00:07:24,00 --> 00:07:27,10
And now when I come back here and reload,

184
00:07:27,10 --> 00:07:29,50
this lights up.

185
00:07:29,50 --> 00:07:30,80
Doesn't actually tell me anything here,

186
00:07:30,80 --> 00:07:31,70
but it lights up,

187
00:07:31,70 --> 00:07:34,60
indicating that React has been found on the page.

188
00:07:34,60 --> 00:07:40,80
And if I open my developer tools,

189
00:07:40,80 --> 00:07:44,60
you can see that I have React over here on the right.

190
00:07:44,60 --> 00:07:47,40
And I can see a little bit more information on this.

191
00:07:47,40 --> 00:07:49,20
Now because we only have a simple element here,

192
00:07:49,20 --> 00:07:51,40
there's not much to learn at this point.

193
00:07:51,40 --> 00:07:53,60
But this is a very useful tool that we'll be looking at

194
00:07:53,60 --> 00:07:56,80
over and over again as we go through this course.

195
00:07:56,80 --> 00:07:57,70
So there you have it.

196
00:07:57,70 --> 00:08:00,50
We've seen how to install React onto a page,

197
00:08:00,50 --> 00:08:02,50
and we've created our first element.

198
00:08:02,50 --> 00:08:04,50
Now let's move on and see how we're going to proceed

199
00:08:04,50 --> 00:08:07,00
into the real meat of our project.

