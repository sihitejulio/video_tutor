1
00:00:00,50 --> 00:00:01,70
- [Instructor] In this video we're going to create

2
00:00:01,70 --> 00:00:04,50
the ProductImage component for the photo of the sneaker

3
00:00:04,50 --> 00:00:07,00
and we're going to change our ProductCustomizer

4
00:00:07,00 --> 00:00:09,00
into a component as well,

5
00:00:09,00 --> 00:00:12,00
so it can contain all the other components.

6
00:00:12,00 --> 00:00:15,10
So here's our index.html file in its current state,

7
00:00:15,10 --> 00:00:18,50
just our little "Hello world" element here.

8
00:00:18,50 --> 00:00:21,20
And here's the mock-up.

9
00:00:21,20 --> 00:00:22,50
So we're going to create the component

10
00:00:22,50 --> 00:00:26,30
for this product image and the wrapper.

11
00:00:26,30 --> 00:00:27,80
Switching back to My Editor,

12
00:00:27,80 --> 00:00:29,80
we're going to get started here.

13
00:00:29,80 --> 00:00:34,20
Currently, ProductCustomizer is just an element.

14
00:00:34,20 --> 00:00:35,40
We're going to change this.

15
00:00:35,40 --> 00:00:37,90
We're going to make this into a component.

16
00:00:37,90 --> 00:00:39,50
Now, there are two kinds of components

17
00:00:39,50 --> 00:00:41,60
that you can create in React.

18
00:00:41,60 --> 00:00:44,30
You can create functions and you can create classes.

19
00:00:44,30 --> 00:00:45,90
Usually, we start out with functions

20
00:00:45,90 --> 00:00:47,80
because they're simpler to do

21
00:00:47,80 --> 00:00:49,10
and then if we need more power,

22
00:00:49,10 --> 00:00:52,00
we can always change them into a class later.

23
00:00:52,00 --> 00:00:58,60
So I'm going to redefine this as a function.

24
00:00:58,60 --> 00:00:59,50
So doing it this way,

25
00:00:59,50 --> 00:01:02,70
we're creating what's called a functional component.

26
00:01:02,70 --> 00:01:04,20
When you write one of these,

27
00:01:04,20 --> 00:01:07,50
it always takes the argument of props.

28
00:01:07,50 --> 00:01:09,20
We're not going to use this yet,

29
00:01:09,20 --> 00:01:10,70
but just know that when you create these,

30
00:01:10,70 --> 00:01:14,50
this is basically the signature you're going to be using.

31
00:01:14,50 --> 00:01:16,80
And so, I'm going to copy this element

32
00:01:16,80 --> 00:01:19,00
that I created up here.

33
00:01:19,00 --> 00:01:20,60
Get rid of this definition

34
00:01:20,60 --> 00:01:24,80
and I'm just going to paste this in, right here.

35
00:01:24,80 --> 00:01:27,10
And the primary job of our functional component

36
00:01:27,10 --> 00:01:30,80
is to return some sort of React element.

37
00:01:30,80 --> 00:01:33,30
So, I'm just going to return it like this.

38
00:01:33,30 --> 00:01:35,20
And then I need to change what I'm passing

39
00:01:35,20 --> 00:01:38,00
in to ReactDOM.render.

40
00:01:38,00 --> 00:01:40,50
Right now, I'm just passing in the component directly,

41
00:01:40,50 --> 00:01:43,20
but what ReactDOM wants is an element,

42
00:01:43,20 --> 00:01:47,20
so we'll just say React.createElement down here

43
00:01:47,20 --> 00:01:51,00
and pass in our ProductCustomizer component.

44
00:01:51,00 --> 00:01:52,50
So this is going to create an element

45
00:01:52,50 --> 00:01:54,60
which is represented by this component here,

46
00:01:54,60 --> 00:01:56,30
and render it into the browser.

47
00:01:56,30 --> 00:01:58,30
No big deal.

48
00:01:58,30 --> 00:01:59,30
We can save this.

49
00:01:59,30 --> 00:02:01,10
Prettier reformats it for me.

50
00:02:01,10 --> 00:02:02,50
And we can jump back over to the browser

51
00:02:02,50 --> 00:02:03,50
to make sure that I haven't made

52
00:02:03,50 --> 00:02:06,60
a terrible mistake so far.

53
00:02:06,60 --> 00:02:08,60
So here we are in the index.html file.

54
00:02:08,60 --> 00:02:10,30
I'll reload.

55
00:02:10,30 --> 00:02:12,00
Great. It still works.

56
00:02:12,00 --> 00:02:14,20
We're good so far.

57
00:02:14,20 --> 00:02:15,40
So now that we've done this,

58
00:02:15,40 --> 00:02:17,50
we need to create our Product Image component.

59
00:02:17,50 --> 00:02:24,60
So let's call this ProductImage

60
00:02:24,60 --> 00:02:27,00
and we'll pass in props as the argument.

61
00:02:27,00 --> 00:02:28,70
So now that I've created this,

62
00:02:28,70 --> 00:02:31,20
I'm going to update my ProductCustomizer

63
00:02:31,20 --> 00:02:34,10
so that it includes my new ProductImage

64
00:02:34,10 --> 00:02:34,90
even though I'm not actually doing anything

65
00:02:34,90 --> 00:02:37,30
in the function yet.

66
00:02:37,30 --> 00:02:39,40
This third argument to React.createElement

67
00:02:39,40 --> 00:02:43,00
is what's going to appear inside that element.

68
00:02:43,00 --> 00:02:45,60
So I'm going to call React.createElement again

69
00:02:45,60 --> 00:02:49,20
and reference my new ProductImage component.

70
00:02:49,20 --> 00:02:51,80
So, I'm going to follow the convention

71
00:02:51,80 --> 00:02:54,40
that I followed here in my mock-up file.

72
00:02:54,40 --> 00:02:56,90
I have this wrapping div called ProductImage.

73
00:02:56,90 --> 00:02:59,40
So let's copy that class.

74
00:02:59,40 --> 00:03:03,70
Come back over to app.js

75
00:03:03,70 --> 00:03:05,40
And I'm creating a div tag.

76
00:03:05,40 --> 00:03:12,60
I'm going to pass in a class name of product-image.

77
00:03:12,60 --> 00:03:14,30
And then I'm going to create my element,

78
00:03:14,30 --> 00:03:17,30
which instantiates the ProductImage component.

79
00:03:17,30 --> 00:03:20,40
using React.createElement again.

80
00:03:20,40 --> 00:03:22,10
If you're getting tired of writing this,

81
00:03:22,10 --> 00:03:22,90
just you wait.

82
00:03:22,90 --> 00:03:25,40
We'll soon have relief.

83
00:03:25,40 --> 00:03:29,30
So, this is going to be my product image.

84
00:03:29,30 --> 00:03:33,30
I'm creating an element for this ProductImage component.

85
00:03:33,30 --> 00:03:34,40
All right.

86
00:03:34,40 --> 00:03:35,20
Now we need to define

87
00:03:35,20 --> 00:03:38,10
what the ProductImage component actually does.

88
00:03:38,10 --> 00:03:39,80
Looking back here in my mock-up,

89
00:03:39,80 --> 00:03:41,80
all it is an image tag.

90
00:03:41,80 --> 00:03:43,50
It's an image tag that's going to be updating

91
00:03:43,50 --> 00:03:45,60
with the active color.

92
00:03:45,60 --> 00:03:48,70
But, all I need right now is this tag.

93
00:03:48,70 --> 00:03:52,10
So let's copy it over here into my app.js file

94
00:03:52,10 --> 00:03:54,70
just so we know what we're doing.

95
00:03:54,70 --> 00:03:59,00
We're going to return yet another React.createElement.

96
00:03:59,00 --> 00:04:00,50
First argument is the type,

97
00:04:00,50 --> 00:04:03,80
which is going to be an image tag.

98
00:04:03,80 --> 00:04:06,30
And then the props that we're going to pass in,

99
00:04:06,30 --> 00:04:09,90
will be the source,

100
00:04:09,90 --> 00:04:14,90
and I can just copy this straight out of here for now,

101
00:04:14,90 --> 00:04:17,60
and we'll use the alt as well.

102
00:04:17,60 --> 00:04:18,80
It was blank originally,

103
00:04:18,80 --> 00:04:20,50
which is not great for accessibility.

104
00:04:20,50 --> 00:04:25,00
So let's just call this Product image for now.

105
00:04:25,00 --> 00:04:26,60
We can delete this.

106
00:04:26,60 --> 00:04:27,90
Save the whole thing.

107
00:04:27,90 --> 00:04:29,80
It gets reformatted a little bit.

108
00:04:29,80 --> 00:04:33,50
And now we can switch back to the browser and take a look.

109
00:04:33,50 --> 00:04:35,60
I'll reload.

110
00:04:35,60 --> 00:04:36,50
And there we go.

111
00:04:36,50 --> 00:04:39,10
We have a red shoe showing up on the page.

112
00:04:39,10 --> 00:04:41,70
No "Hello World" stuff.

113
00:04:41,70 --> 00:04:44,00
So, I'll open up the developer tools

114
00:04:44,00 --> 00:04:46,90
using command + option + I here on my Mac.

115
00:04:46,90 --> 00:04:50,20
And I'll switch to the React Dev tools.

116
00:04:50,20 --> 00:04:56,10
You can see, here is my product customizer component

117
00:04:56,10 --> 00:04:59,30
with the div for the customizer and the product image

118
00:04:59,30 --> 00:05:01,30
and here's my ProductImage component.

119
00:05:01,30 --> 00:05:04,70
Now you'll note, all these components are capitalized.

120
00:05:04,70 --> 00:05:06,10
When you're creating components in React,

121
00:05:06,10 --> 00:05:08,20
you want to make sure that they are capitalized,

122
00:05:08,20 --> 00:05:10,20
which is the convention that we follow in React

123
00:05:10,20 --> 00:05:11,60
so that React knows the difference

124
00:05:11,60 --> 00:05:13,80
between elements that we're expecting to be

125
00:05:13,80 --> 00:05:16,90
just plain html elements like these divs

126
00:05:16,90 --> 00:05:18,60
and the components that we're creating.

127
00:05:18,60 --> 00:05:21,10
So always capitalize your components.

128
00:05:21,10 --> 00:05:24,40
So there we are: ProductImage, ProductCustomizer.

129
00:05:24,40 --> 00:05:26,90
Everything looks good.

130
00:05:26,90 --> 00:05:29,20
So now you've just created two components

131
00:05:29,20 --> 00:05:31,00
and we're ready to move on to the next step.

