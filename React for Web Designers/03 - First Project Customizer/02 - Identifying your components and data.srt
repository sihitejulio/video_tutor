1
00:00:00,50 --> 00:00:01,80
- In this video, we're going to take a look

2
00:00:01,80 --> 00:00:05,40
at a static mock-up of what we want our result to look like

3
00:00:05,40 --> 00:00:07,50
when our React project is done

4
00:00:07,50 --> 00:00:08,80
and start making plans about

5
00:00:08,80 --> 00:00:09,90
where our components should be

6
00:00:09,90 --> 00:00:12,30
and what data we're planning for.

7
00:00:12,30 --> 00:00:14,30
So, I've opened up the mockup.html file

8
00:00:14,30 --> 00:00:16,20
here in my browser.

9
00:00:16,20 --> 00:00:19,30
I have a mock-up of what we actually want in the end.

10
00:00:19,30 --> 00:00:21,50
Pretty simple little thing.

11
00:00:21,50 --> 00:00:24,60
We're going to be able to select the size of the sneaker,

12
00:00:24,60 --> 00:00:26,80
and we're going to be able to choose a color

13
00:00:26,80 --> 00:00:30,20
and when we choose the color,

14
00:00:30,20 --> 00:00:32,60
the color of the shoe changes.

15
00:00:32,60 --> 00:00:35,20
So, let's take a look at our html file

16
00:00:35,20 --> 00:00:38,70
just to see how this mock-up works from that perspective.

17
00:00:38,70 --> 00:00:40,00
Over here in Visual Studio Code,

18
00:00:40,00 --> 00:00:44,50
I'm going to use control-G to skip down to line 114 again.

19
00:00:44,50 --> 00:00:47,80
Here's where my React components will go.

20
00:00:47,80 --> 00:00:50,90
And here's my static mock-up.

21
00:00:50,90 --> 00:00:54,20
So, I just want you to note that we have some wrapping divs

22
00:00:54,20 --> 00:00:56,10
for all these different elements.

23
00:00:56,10 --> 00:00:59,30
The selectors are contained in their own div.

24
00:00:59,30 --> 00:01:01,60
I have my size selector.

25
00:01:01,60 --> 00:01:03,10
I have my color selector.

26
00:01:03,10 --> 00:01:06,10
This is all very standard html stuff

27
00:01:06,10 --> 00:01:08,40
but when we re-create these things in React,

28
00:01:08,40 --> 00:01:10,00
we're going to want to re-create a lot of this

29
00:01:10,00 --> 00:01:11,70
so that we can inherit the same styles

30
00:01:11,70 --> 00:01:15,30
that we're already using here in the static mock-up.

31
00:01:15,30 --> 00:01:16,70
Switching back to the browser,

32
00:01:16,70 --> 00:01:20,00
we need to think about what data we're going to be tracking,

33
00:01:20,00 --> 00:01:22,70
what we're going to have React updating for us

34
00:01:22,70 --> 00:01:24,90
and how we're going to break up these pieces

35
00:01:24,90 --> 00:01:27,10
into components.

36
00:01:27,10 --> 00:01:28,40
So, here's what we're going to do.

37
00:01:28,40 --> 00:01:31,50
We're going to have one component for the sneaker image

38
00:01:31,50 --> 00:01:33,20
and we know that's going to need to be aware

39
00:01:33,20 --> 00:01:36,10
of what color is currently selected.

40
00:01:36,10 --> 00:01:39,70
And then over here, we could put both of these selectors

41
00:01:39,70 --> 00:01:42,20
into a single component if we wanted to.

42
00:01:42,20 --> 00:01:44,60
I'm going to choose to make each of these

43
00:01:44,60 --> 00:01:45,60
its own component,

44
00:01:45,60 --> 00:01:46,90
so I'll have a size selector,

45
00:01:46,90 --> 00:01:49,50
and a color selector.

46
00:01:49,50 --> 00:01:51,20
The size selector is going to need to know

47
00:01:51,20 --> 00:01:54,60
what sizes are available to be selected

48
00:01:54,60 --> 00:01:57,60
and which one is the currently selected size.

49
00:01:57,60 --> 00:01:59,90
Likewise, the colors is going to need to know

50
00:01:59,90 --> 00:02:01,20
the available colors,

51
00:02:01,20 --> 00:02:03,70
and the currently selected color.

52
00:02:03,70 --> 00:02:06,60
We know that React does one-way data flow

53
00:02:06,60 --> 00:02:09,10
so if you have components that are going to need to

54
00:02:09,10 --> 00:02:10,40
share a piece of information,

55
00:02:10,40 --> 00:02:13,30
that is, share a piece of state,

56
00:02:13,30 --> 00:02:14,20
you're going to need to make sure

57
00:02:14,20 --> 00:02:16,00
that that state is available to both of them

58
00:02:16,00 --> 00:02:20,20
by having a component at a level above those two.

59
00:02:20,20 --> 00:02:23,10
So, we know for sure that the product image

60
00:02:23,10 --> 00:02:25,10
and the color selector are both going to need to

61
00:02:25,10 --> 00:02:28,30
be aware of what the currently selected color is.

62
00:02:28,30 --> 00:02:30,70
So, along with each of these smaller components,

63
00:02:30,70 --> 00:02:32,60
we're going to need to have one larger component

64
00:02:32,60 --> 00:02:34,90
that can wrap all of them together

65
00:02:34,90 --> 00:02:37,30
and keep track of that state that needs to be known

66
00:02:37,30 --> 00:02:40,30
in each of these children components.

67
00:02:40,30 --> 00:02:42,20
So, to recap, we're going to have

68
00:02:42,20 --> 00:02:44,00
one big component for the whole thing,

69
00:02:44,00 --> 00:02:46,60
we'll call that the product customizer.

70
00:02:46,60 --> 00:02:49,30
We'll have a product image component,

71
00:02:49,30 --> 00:02:51,90
we'll have a size selector,

72
00:02:51,90 --> 00:02:53,50
and a color selector,

73
00:02:53,50 --> 00:02:54,70
and that should cover us for

74
00:02:54,70 --> 00:02:57,40
this very simple project's needs.

75
00:02:57,40 --> 00:03:00,60
So, at this point, we've looked at the mock-up,

76
00:03:00,60 --> 00:03:04,00
we know the kind of interactivity that we're going to use,

77
00:03:04,00 --> 00:03:05,90
and we've made a plan for how we're going to

78
00:03:05,90 --> 00:03:07,20
break these components up.

79
00:03:07,20 --> 00:03:08,90
Now, of course, as you start writing your code,

80
00:03:08,90 --> 00:03:10,00
you may change your mind about

81
00:03:10,00 --> 00:03:11,50
how you want to proceed with this,

82
00:03:11,50 --> 00:03:13,60
but going into it, we have at least some idea

83
00:03:13,60 --> 00:03:15,80
of what we're going to do.

84
00:03:15,80 --> 00:03:17,00
So now, we're going to proceed

85
00:03:17,00 --> 00:03:19,00
and start writing a little more code.

