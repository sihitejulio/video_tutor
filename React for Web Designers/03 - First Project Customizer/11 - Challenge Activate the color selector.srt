1
00:00:00,00 --> 00:00:07,40
(bright music)

2
00:00:07,40 --> 00:00:08,30
- [Instructor] At this point,

3
00:00:08,30 --> 00:00:09,70
we have a series of components

4
00:00:09,70 --> 00:00:12,60
that work pretty nicely together.

5
00:00:12,60 --> 00:00:14,50
But now it's time for our first challenge

6
00:00:14,50 --> 00:00:15,60
where you're going to be working on

7
00:00:15,60 --> 00:00:18,50
one of these components yourself.

8
00:00:18,50 --> 00:00:19,80
Right now, we have a size selector

9
00:00:19,80 --> 00:00:22,30
that can update the selected colors.

10
00:00:22,30 --> 00:00:25,70
We're going to do the opposite for this challenge.

11
00:00:25,70 --> 00:00:27,40
That is when we select color,

12
00:00:27,40 --> 00:00:31,60
we want to update the state of everything else accordingly.

13
00:00:31,60 --> 00:00:34,80
So here we are in our color selector challenge.

14
00:00:34,80 --> 00:00:36,20
What we're going to do is activate

15
00:00:36,20 --> 00:00:38,80
the color selector's behaviors, specifically,

16
00:00:38,80 --> 00:00:41,60
we're going to update the currently selected color,

17
00:00:41,60 --> 00:00:44,00
and then we're going to check the inventory,

18
00:00:44,00 --> 00:00:47,50
and update the size selector with the available sizes

19
00:00:47,50 --> 00:00:49,90
for the currently selected color.

20
00:00:49,90 --> 00:00:51,30
I have a little bit of extra credit

21
00:00:51,30 --> 00:00:53,00
in this challenge as well,

22
00:00:53,00 --> 00:00:57,70
which is to update the selectors by current availability.

23
00:00:57,70 --> 00:00:59,50
So what I mean by that is we'll check

24
00:00:59,50 --> 00:01:02,90
if the currently selected size is available

25
00:01:02,90 --> 00:01:05,30
for the currently selected color.

26
00:01:05,30 --> 00:01:10,10
So, if for example, I currently have size seven selected,

27
00:01:10,10 --> 00:01:12,60
and I change the color to blue,

28
00:01:12,60 --> 00:01:14,80
if size seven is not available in blue,

29
00:01:14,80 --> 00:01:17,80
I need to make some kind of change to the state.

30
00:01:17,80 --> 00:01:19,40
So I want to update the state,

31
00:01:19,40 --> 00:01:22,10
and just make sure that if that size

32
00:01:22,10 --> 00:01:24,80
is not available in the selected color,

33
00:01:24,80 --> 00:01:28,00
I change the state so that the selected size is one

34
00:01:28,00 --> 00:01:31,40
that actually is available in that color.

35
00:01:31,40 --> 00:01:33,50
As you're going through this extra credit portion,

36
00:01:33,50 --> 00:01:35,60
you may see some unexpected behaviors

37
00:01:35,60 --> 00:01:37,50
in the way the select boxes react

38
00:01:37,50 --> 00:01:39,90
to the changes and state that you're making.

39
00:01:39,90 --> 00:01:42,40
We're going to go into the details of what's going on

40
00:01:42,40 --> 00:01:44,70
and why it's happening in a future chapter,

41
00:01:44,70 --> 00:01:46,20
but I just want you to notice that you may

42
00:01:46,20 --> 00:01:48,00
see something a little unexpected.

43
00:01:48,00 --> 00:01:50,30
We'll talk about it in the solution and we'll deal with it

44
00:01:50,30 --> 00:01:53,70
in more detail in upcoming chapters.

45
00:01:53,70 --> 00:01:55,90
But for now, go to work on the color selector.

46
00:01:55,90 --> 00:01:58,00
This should take 10 minutes or less.

