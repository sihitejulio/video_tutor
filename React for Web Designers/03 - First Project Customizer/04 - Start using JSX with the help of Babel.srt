1
00:00:00,50 --> 00:00:01,70
- [Instructor] At this point, we have not been

2
00:00:01,70 --> 00:00:05,00
in the business of writing React code for very long.

3
00:00:05,00 --> 00:00:06,50
But you may already be getting tired

4
00:00:06,50 --> 00:00:08,80
of having to write React.createElement

5
00:00:08,80 --> 00:00:11,10
over and over and over again.

6
00:00:11,10 --> 00:00:12,90
Now, there are different ways to work around this.

7
00:00:12,90 --> 00:00:15,30
You could create a variable

8
00:00:15,30 --> 00:00:20,80
for React.createElement, say, rce.

9
00:00:20,80 --> 00:00:23,60
And then, you could write your functions like this

10
00:00:23,60 --> 00:00:27,70
instead of having to type that code out all the time.

11
00:00:27,70 --> 00:00:29,30
But there's another way to do that

12
00:00:29,30 --> 00:00:33,90
that the React community uses all the time, which is JSX.

13
00:00:33,90 --> 00:00:36,50
In this video, we're going to start using JSX

14
00:00:36,50 --> 00:00:38,80
and using the Babel transpiler in the browser

15
00:00:38,80 --> 00:00:40,40
so that we don't have to install any build tools

16
00:00:40,40 --> 00:00:45,20
to get the benefits of JSX and ES6 in our files.

17
00:00:45,20 --> 00:00:46,10
To do that, we're going to need

18
00:00:46,10 --> 00:00:48,00
a little bit of installation first.

19
00:00:48,00 --> 00:00:48,90
When I said that we weren't going to

20
00:00:48,90 --> 00:00:50,70
use any extra tools at all,

21
00:00:50,70 --> 00:00:51,80
that was sort of a half-truth

22
00:00:51,80 --> 00:00:53,80
because to get Babel running properly,

23
00:00:53,80 --> 00:00:55,30
we need to use a local web server

24
00:00:55,30 --> 00:00:59,80
instead of addressing our files over a local file URL.

25
00:00:59,80 --> 00:01:02,80
Here on this Mac, I'm using MAMP,

26
00:01:02,80 --> 00:01:05,20
which is a very popular local web server,

27
00:01:05,20 --> 00:01:07,20
very simple, there's a free version of it,

28
00:01:07,20 --> 00:01:09,40
which you can download here.

29
00:01:09,40 --> 00:01:11,40
If you've never used a tool like this before,

30
00:01:11,40 --> 00:01:13,60
the documentation on the MAMP site is pretty thorough,

31
00:01:13,60 --> 00:01:15,50
and there are also courses in the library

32
00:01:15,50 --> 00:01:18,50
that can help you with local web servers.

33
00:01:18,50 --> 00:01:20,40
I already have MAMP installed here,

34
00:01:20,40 --> 00:01:21,50
and I just want to take you

35
00:01:21,50 --> 00:01:25,30
through a couple of settings that I've changed.

36
00:01:25,30 --> 00:01:27,60
So we're going to open the MAMP Preferences,

37
00:01:27,60 --> 00:01:29,80
and there are two things I want to show you.

38
00:01:29,80 --> 00:01:32,80
One is that here, under the Web-Server preferences,

39
00:01:32,80 --> 00:01:34,70
I've changed the Document Root.

40
00:01:34,70 --> 00:01:36,60
This is the folder that MAMP is going to look at

41
00:01:36,60 --> 00:01:40,20
when it's looking for which files to serve to your browser.

42
00:01:40,20 --> 00:01:42,70
Out of the box, it uses MAMP's own folder

43
00:01:42,70 --> 00:01:44,90
inside your Applications folder.

44
00:01:44,90 --> 00:01:46,00
I don't particularly like that,

45
00:01:46,00 --> 00:01:48,10
I'd rather have it in some other folder.

46
00:01:48,10 --> 00:01:49,40
In this case, I've decide to create

47
00:01:49,40 --> 00:01:52,60
a Sites folder inside my Documents folder.

48
00:01:52,60 --> 00:01:53,80
The other thing that I've changed

49
00:01:53,80 --> 00:01:56,40
is the ports from which MAMP serves

50
00:01:56,40 --> 00:01:59,10
your web server and your database.

51
00:01:59,10 --> 00:02:01,50
By default, it uses Port 8888 for the web

52
00:02:01,50 --> 00:02:05,70
and 8889 for your MySQL database.

53
00:02:05,70 --> 00:02:08,70
The standard web ports are 80 for web

54
00:02:08,70 --> 00:02:11,30
and 3306 for MySQL.

55
00:02:11,30 --> 00:02:13,80
I use those because that allows me, in the browser,

56
00:02:13,80 --> 00:02:17,10
not to have to type colon 8888 after localhost

57
00:02:17,10 --> 00:02:19,50
when I'm addressing my local web server.

58
00:02:19,50 --> 00:02:22,80
Just a matter of convenience.

59
00:02:22,80 --> 00:02:24,50
So if you chose to change those settings,

60
00:02:24,50 --> 00:02:26,40
you would do that, MAMP will restart,

61
00:02:26,40 --> 00:02:28,40
and you're good to go.

62
00:02:28,40 --> 00:02:29,70
The next thing that we need to do

63
00:02:29,70 --> 00:02:31,30
is make sure that the Exercise Files

64
00:02:31,30 --> 00:02:33,60
are addressable by this web server.

65
00:02:33,60 --> 00:02:35,60
So I've copied the Exercise Files folder

66
00:02:35,60 --> 00:02:40,20
into my document's Sites folder right here.

67
00:02:40,20 --> 00:02:41,80
I'm going to rename this

68
00:02:41,80 --> 00:02:45,60
from Exercise Files to reactjs

69
00:02:45,60 --> 00:02:47,50
'cause that way, there's no spaces in the file name,

70
00:02:47,50 --> 00:02:49,00
and it's just easier to type

71
00:02:49,00 --> 00:02:51,90
which I'm typing out the URL.

72
00:02:51,90 --> 00:02:53,20
So now, let's load this in the browser

73
00:02:53,20 --> 00:02:55,30
and confirm that it works.

74
00:02:55,30 --> 00:02:58,20
Here's my old file URL.

75
00:02:58,20 --> 00:03:01,50
Going to strip off all the stuff at the beginning

76
00:03:01,50 --> 00:03:05,40
and go to http localhost,

77
00:03:05,40 --> 00:03:08,30
reactjs, which the name of my folder,

78
00:03:08,30 --> 00:03:11,80
and there we go, everything loads and works great.

79
00:03:11,80 --> 00:03:12,90
So now, I'm going to open the files

80
00:03:12,90 --> 00:03:14,00
that I need in my Editor,

81
00:03:14,00 --> 00:03:18,20
and we'll get started writing some actual code.

82
00:03:18,20 --> 00:03:20,30
Okay, the first thing we need to do in the Editor

83
00:03:20,30 --> 00:03:22,20
is install Babel.

84
00:03:22,20 --> 00:03:24,00
I've included a Babel snippet

85
00:03:24,00 --> 00:03:26,20
in the Exercise Files for this video.

86
00:03:26,20 --> 00:03:29,30
We're loading babel-standalone from unpackage,

87
00:03:29,30 --> 00:03:31,20
just like we're doing with React,

88
00:03:31,20 --> 00:03:33,20
and then we have a local fallback

89
00:03:33,20 --> 00:03:35,30
for when we're offline.

90
00:03:35,30 --> 00:03:38,70
So I'm going to select all this and copy it,

91
00:03:38,70 --> 00:03:41,30
and then go over to my index.html file

92
00:03:41,30 --> 00:03:46,40
and scroll all the way down with Command + Down Arrow.

93
00:03:46,40 --> 00:03:47,60
Now, I'm just going to install Babel

94
00:03:47,60 --> 00:03:52,80
right after my React script tags right here.

95
00:03:52,80 --> 00:03:54,40
So if I save it, at this point,

96
00:03:54,40 --> 00:03:56,00
the library should be accessible,

97
00:03:56,00 --> 00:03:57,10
but it's going to interpret

98
00:03:57,10 --> 00:03:59,40
just all my script tags on the page.

99
00:03:59,40 --> 00:04:02,20
If I want Babel to interpret these and transpile them,

100
00:04:02,20 --> 00:04:09,70
I need to include type equals text slash babel.

101
00:04:09,70 --> 00:04:11,90
The reason we have to go with a local web server for this

102
00:04:11,90 --> 00:04:14,60
is that Babel is going to make an Ajax request

103
00:04:14,60 --> 00:04:18,20
for any script file that has text slash babel,

104
00:04:18,20 --> 00:04:20,60
do all its transpiling, and then dump that file

105
00:04:20,60 --> 00:04:22,40
back into the browser, basically.

106
00:04:22,40 --> 00:04:23,90
So again, it's only going to do that

107
00:04:23,90 --> 00:04:25,50
for scripts that we've marked

108
00:04:25,50 --> 00:04:29,00
with this type text slash babel.

109
00:04:29,00 --> 00:04:32,30
So having done that, I can now switch back to my app.js file

110
00:04:32,30 --> 00:04:38,00
and convert these React.createElements into JSX.

111
00:04:38,00 --> 00:04:41,90
This is going to be a fairly quick operation.

112
00:04:41,90 --> 00:04:43,80
Instead of React.createElement,

113
00:04:43,80 --> 00:04:45,90
now, I can write these JSX elements,

114
00:04:45,90 --> 00:04:49,30
which basically look like inline HTML

115
00:04:49,30 --> 00:04:51,40
right here in your scripts,

116
00:04:51,40 --> 00:04:54,70
and they're going to look just like HTML for the most part.

117
00:04:54,70 --> 00:05:01,20
So in this case, this image tag

118
00:05:01,20 --> 00:05:07,30
becomes an image tag.

119
00:05:07,30 --> 00:05:10,50
This is a JSX JavaScript expression

120
00:05:10,50 --> 00:05:13,80
that is then transpiled into a React.createElement

121
00:05:13,80 --> 00:05:15,40
that is specified the same way,

122
00:05:15,40 --> 00:05:18,50
but as you can see, this is a lot nicer to write,

123
00:05:18,50 --> 00:05:20,00
and it sets especially nice

124
00:05:20,00 --> 00:05:23,40
when you have nested elements like these.

125
00:05:23,40 --> 00:05:27,70
So let's refactor this one as well.

126
00:05:27,70 --> 00:05:30,30
Going to create a second return statement down here.

127
00:05:30,30 --> 00:05:33,80
This is going to end up being some nested HTML tags.

128
00:05:33,80 --> 00:05:35,30
And when we do that, it's a good idea

129
00:05:35,30 --> 00:05:36,90
to wrap everything in parentheses

130
00:05:36,90 --> 00:05:39,20
so that JavaScript knows that this is an entire expression

131
00:05:39,20 --> 00:05:42,20
that's supposed to be interpreted together.

132
00:05:42,20 --> 00:05:45,80
So here we have a div with the className customizer,

133
00:05:45,80 --> 00:05:53,60
so we're just going to write a div tag for that.

134
00:05:53,60 --> 00:05:55,20
Now, this looks like HTML,

135
00:05:55,20 --> 00:05:57,20
but we're still not going to be able to write class

136
00:05:57,20 --> 00:05:59,40
as we would with normal HTML.

137
00:05:59,40 --> 00:06:01,70
We need to refer to it using the same names.

138
00:06:01,70 --> 00:06:04,60
So className,

139
00:06:04,60 --> 00:06:08,00
a copy and paste customzier in here.

140
00:06:08,00 --> 00:06:10,90
And now, inside that, I'm going to have another div,

141
00:06:10,90 --> 00:06:17,20
just like I do here with the className of product-image,

142
00:06:17,20 --> 00:06:21,50
like so, and I'll close that.

143
00:06:21,50 --> 00:06:23,60
And then, finally, inside that,

144
00:06:23,60 --> 00:06:26,60
where I have React.createElement product-image here,

145
00:06:26,60 --> 00:06:29,30
I can just create a product-image tag, just like this.

146
00:06:29,30 --> 00:06:31,50
It's just an empty tag,

147
00:06:31,50 --> 00:06:33,20
but it refers to my custom element.

148
00:06:33,20 --> 00:06:38,10
And again, these need to be capitalized, like this.

149
00:06:38,10 --> 00:06:40,90
So now, I can get rid of all that stuff.

150
00:06:40,90 --> 00:06:43,50
Now, my final instance of React.createElement

151
00:06:43,50 --> 00:06:48,00
here in this file is my ProductCustomizer tag down here,

152
00:06:48,00 --> 00:06:51,00
which I can just convert into an empty tag.

153
00:06:51,00 --> 00:06:52,80
So now, I can save this.

154
00:06:52,80 --> 00:06:55,20
Now we can test it in the browser.

155
00:06:55,20 --> 00:06:57,50
So here we are, once again, in my localhost.

156
00:06:57,50 --> 00:06:59,40
I'll reload this, and we should see

157
00:06:59,40 --> 00:07:00,70
nothing different at all.

158
00:07:00,70 --> 00:07:03,20
Great!

159
00:07:03,20 --> 00:07:04,50
Let's open up the Developer Tools

160
00:07:04,50 --> 00:07:06,50
and just make sure everything looks right,

161
00:07:06,50 --> 00:07:08,80
Command + Option + I.

162
00:07:08,80 --> 00:07:10,70
So as you can see, we're getting a warning here

163
00:07:10,70 --> 00:07:13,90
about using the Babel transformer in the browser.

164
00:07:13,90 --> 00:07:16,30
If we were doing this on a real project,

165
00:07:16,30 --> 00:07:19,40
we'd want to be publishing our transfile scripts

166
00:07:19,40 --> 00:07:22,80
as regular JavaScript files on the Internet,

167
00:07:22,80 --> 00:07:24,80
and that's what something like Webpack

168
00:07:24,80 --> 00:07:26,40
or the other tools that we'll be looking at

169
00:07:26,40 --> 00:07:28,70
later in this course are good for.

170
00:07:28,70 --> 00:07:29,90
But for our purposes right now,

171
00:07:29,90 --> 00:07:31,80
where we don't want to introduce a lot of tools

172
00:07:31,80 --> 00:07:33,60
that we might not be familiar with already,

173
00:07:33,60 --> 00:07:36,60
this is a great way to get working with it.

174
00:07:36,60 --> 00:07:38,70
And now, I can see here in the React DevTools,

175
00:07:38,70 --> 00:07:43,80
everything looks just the way it should.

176
00:07:43,80 --> 00:07:45,30
And now, we can move on to the next step,

177
00:07:45,30 --> 00:07:57,00
and we never have to type React.createElement ever again.

