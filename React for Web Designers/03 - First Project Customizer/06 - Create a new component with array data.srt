1
00:00:00,60 --> 00:00:02,40
- In this video, we're going to use the data source

2
00:00:02,40 --> 00:00:05,40
that we installed to build the size selector component

3
00:00:05,40 --> 00:00:07,40
without having to hard code any data.

4
00:00:07,40 --> 00:00:09,60
We're going to try and minimize effort by stealing HTML

5
00:00:09,60 --> 00:00:11,70
from this mock-up wherever we can.

6
00:00:11,70 --> 00:00:13,40
Just to be clear, this is what we're going to be building,

7
00:00:13,40 --> 00:00:15,40
the size selector right here.

8
00:00:15,40 --> 00:00:16,50
Switching over to my editor,

9
00:00:16,50 --> 00:00:20,80
I'm going to jump down to line 114 with control g.

10
00:00:20,80 --> 00:00:22,20
Here's my react root

11
00:00:22,20 --> 00:00:25,80
and down here is where all of my elements appear.

12
00:00:25,80 --> 00:00:28,60
Here in the selectors div.

13
00:00:28,60 --> 00:00:31,70
I'm currently interested in this size selector,

14
00:00:31,70 --> 00:00:33,70
but to make sure I get everything exactly as it is,

15
00:00:33,70 --> 00:00:38,00
I'm going to collapse that and just select the whole thing.

16
00:00:38,00 --> 00:00:42,70
Copy it and I'm going to bring it over into my app.js file.

17
00:00:42,70 --> 00:00:46,60
It appears right underneath the product image

18
00:00:46,60 --> 00:00:48,90
and I'm not interested right now in the color selector,

19
00:00:48,90 --> 00:00:51,10
so I'm just going to take that out.

20
00:00:51,10 --> 00:00:53,00
Make this a little nicer.

21
00:00:53,00 --> 00:00:54,50
Okay.

22
00:00:54,50 --> 00:00:56,20
So now I have the markup,

23
00:00:56,20 --> 00:00:58,70
but I want this to be rendered as a component.

24
00:00:58,70 --> 00:01:00,80
So I'm going to extract what's going to be used

25
00:01:00,80 --> 00:01:02,30
in the size selector.

26
00:01:02,30 --> 00:01:06,70
I'm going cut this and replace it with

27
00:01:06,70 --> 00:01:09,40
what'll be called the size selector.

28
00:01:09,40 --> 00:01:10,90
Of course this isn't defined yet,

29
00:01:10,90 --> 00:01:13,10
so we need to define it above.

30
00:01:13,10 --> 00:01:16,60
Going to create a new function component called size selector.

31
00:01:16,60 --> 00:01:20,30
We'll take props as it's first argument.

32
00:01:20,30 --> 00:01:26,00
Now I'm just going to return the markup that I copied in.

33
00:01:26,00 --> 00:01:28,30
So this is a start,

34
00:01:28,30 --> 00:01:31,20
but we need to convert this first to valid JSX.

35
00:01:31,20 --> 00:01:33,70
And that means I need to convert some of these attributes

36
00:01:33,70 --> 00:01:36,20
into some different names.

37
00:01:36,20 --> 00:01:40,80
React uses the DOM API to name it's attributes like this.

38
00:01:40,80 --> 00:01:44,50
The JavaScript equivalent basically of what appears in HTML.

39
00:01:44,50 --> 00:01:46,90
So class has a different meaning in JavaScript.

40
00:01:46,90 --> 00:01:50,20
So the DOM API uses class name.

41
00:01:50,20 --> 00:01:52,40
And for of course has a different name as well

42
00:01:52,40 --> 00:01:53,70
for setting up for loops.

43
00:01:53,70 --> 00:01:57,60
So we have HTML for as the name of that attribute.

44
00:01:57,60 --> 00:01:59,10
Fortunately in most cases,

45
00:01:59,10 --> 00:02:02,20
you won't have these kind of issues.

46
00:02:02,20 --> 00:02:04,40
But every once in a while, you will need to change the names

47
00:02:04,40 --> 00:02:06,70
of your attributes when you bring them in

48
00:02:06,70 --> 00:02:08,90
and convert them into JSX props.

49
00:02:08,90 --> 00:02:10,60
Class name and HTML for are the ones

50
00:02:10,60 --> 00:02:12,50
I've run into most often.

51
00:02:12,50 --> 00:02:13,30
And before I move on,

52
00:02:13,30 --> 00:02:16,80
while I'm converting things to valid JSX,

53
00:02:16,80 --> 00:02:18,50
I need to jump down here to the bottom of the file

54
00:02:18,50 --> 00:02:22,20
and change this class to class name as well.

55
00:02:22,20 --> 00:02:25,40
So now we have valid JSX, but we're still hard coding all

56
00:02:25,40 --> 00:02:27,60
of these option elements.

57
00:02:27,60 --> 00:02:30,30
So we need to get rid of that as well.

58
00:02:30,30 --> 00:02:33,80
Going to just cut one of these for future use.

59
00:02:33,80 --> 00:02:36,30
And we'll be replacing this with a helper function

60
00:02:36,30 --> 00:02:37,60
that will render them all.

61
00:02:37,60 --> 00:02:39,90
I'll call it size options.

62
00:02:39,90 --> 00:02:41,90
We need to define the function that will actually return

63
00:02:41,90 --> 00:02:43,80
all this stuff.

64
00:02:43,80 --> 00:02:45,30
So we'll do that here inside our

65
00:02:45,30 --> 00:02:47,90
size selector function component.

66
00:02:47,90 --> 00:02:50,30
This will be size options

67
00:02:50,30 --> 00:02:52,60
and this is going to return some stuff.

68
00:02:52,60 --> 00:02:55,60
And we need to determine what that stuff is going to be.

69
00:02:55,60 --> 00:02:57,70
So what we need to do is collect all the sizes

70
00:02:57,70 --> 00:02:59,70
from our inventory and return an option element

71
00:02:59,70 --> 00:03:01,30
that goes with each one.

72
00:03:01,30 --> 00:03:03,70
I will first collect all those sizes.

73
00:03:03,70 --> 00:03:06,50
I'll give them a slightly handier name.

74
00:03:06,50 --> 00:03:11,10
Window.Inventory.allsizes is what it was called

75
00:03:11,10 --> 00:03:13,80
inside that script.

76
00:03:13,80 --> 00:03:16,40
And now what I need to do is return sizes.

77
00:03:16,40 --> 00:03:19,60
I'm going to use the map method on JavaScript array object.

78
00:03:19,60 --> 00:03:22,40
This pattern of using array map

79
00:03:22,40 --> 00:03:24,60
is something you will see all over the place in react

80
00:03:24,60 --> 00:03:26,00
when you're creating lists.

81
00:03:26,00 --> 00:03:29,00
It just allows you to take every single item in the array

82
00:03:29,00 --> 00:03:31,10
and return something that is usually generated

83
00:03:31,10 --> 00:03:35,00
using that element and what array map takes is

84
00:03:35,00 --> 00:03:38,50
a callback function whose parameter is the element

85
00:03:38,50 --> 00:03:40,40
that's in the array.

86
00:03:40,40 --> 00:03:42,20
In this case, we are dealing with sizes.

87
00:03:42,20 --> 00:03:43,60
So those are basically numbers.

88
00:03:43,60 --> 00:03:45,10
I'm going to call it num.

89
00:03:45,10 --> 00:03:47,80
We could call it size, but then I'd be seeing sizes and size

90
00:03:47,80 --> 00:03:48,80
all over the place in here

91
00:03:48,80 --> 00:03:50,70
and I don't want to confuse myself.

92
00:03:50,70 --> 00:03:53,10
So I'm going to call it num for a number.

93
00:03:53,10 --> 00:03:54,40
And then for each of those sizes,

94
00:03:54,40 --> 00:03:58,00
I'm going to return an option element like this.

95
00:03:58,00 --> 00:03:59,80
Instead of always returning seven,

96
00:03:59,80 --> 00:04:02,80
I'm going to return whatever the current size value is,

97
00:04:02,80 --> 00:04:04,70
which is num.

98
00:04:04,70 --> 00:04:07,00
This is enough to get us exactly what we had before,

99
00:04:07,00 --> 00:04:09,40
but I'm going to flush it out just a tiny bit,

100
00:04:09,40 --> 00:04:10,60
by adding the value attribute,

101
00:04:10,60 --> 00:04:13,20
which I'll also set to num.

102
00:04:13,20 --> 00:04:14,90
This would give me the freedom to

103
00:04:14,90 --> 00:04:16,80
change this however I like.

104
00:04:16,80 --> 00:04:19,70
If I needed to re label what's inside the option element.

105
00:04:19,70 --> 00:04:21,50
Like if I wanted to include size colon

106
00:04:21,50 --> 00:04:23,60
or just something else in here.

107
00:04:23,60 --> 00:04:25,90
I don't need to do that, so I'll take it out,

108
00:04:25,90 --> 00:04:28,00
but this does give me a little flexibility.

109
00:04:28,00 --> 00:04:29,60
Now I need to add a special prop

110
00:04:29,60 --> 00:04:32,20
that react needs called a key.

111
00:04:32,20 --> 00:04:34,10
Any time you're creating a list of items

112
00:04:34,10 --> 00:04:36,30
that's generated by a helper function,

113
00:04:36,30 --> 00:04:38,00
you're going to need a key.

114
00:04:38,00 --> 00:04:39,90
The value of that key that react wants

115
00:04:39,90 --> 00:04:43,20
should be something unique to this component.

116
00:04:43,20 --> 00:04:44,80
It doesn't have to be globally unique

117
00:04:44,80 --> 00:04:47,80
the way, say HTML ID does on a page,

118
00:04:47,80 --> 00:04:50,60
but it does need to be unique to this component.

119
00:04:50,60 --> 00:04:52,90
In our case, we can just use the size.

120
00:04:52,90 --> 00:04:56,40
If you had an object where one of the keys inside that

121
00:04:56,40 --> 00:04:58,60
object is an ID, you could use that.

122
00:04:58,60 --> 00:05:02,80
Something like object.id or what have you.

123
00:05:02,80 --> 00:05:04,00
And if you get really desperate

124
00:05:04,00 --> 00:05:06,80
and the array doesn't have anything like that,

125
00:05:06,80 --> 00:05:08,20
where you can be sure that it's going to be

126
00:05:08,20 --> 00:05:10,50
unique every time that function runs,

127
00:05:10,50 --> 00:05:13,90
you could, in your map function, use the second

128
00:05:13,90 --> 00:05:16,20
optional parameter, called the index.

129
00:05:16,20 --> 00:05:17,50
And use that here.

130
00:05:17,50 --> 00:05:19,70
React specifically recommends that you don't do that

131
00:05:19,70 --> 00:05:22,20
because you can't exactly guarantee the mapping

132
00:05:22,20 --> 00:05:25,10
of an index to a particular value,

133
00:05:25,10 --> 00:05:26,80
depending on the structure of the array,

134
00:05:26,80 --> 00:05:28,20
but it is an option that's available to you

135
00:05:28,20 --> 00:05:29,20
if you really need it.

136
00:05:29,20 --> 00:05:30,60
I'm going to leave this as num.

137
00:05:30,60 --> 00:05:32,30
And now I can save this.

138
00:05:32,30 --> 00:05:34,50
Prettier re-formats the page for me a little bit.

139
00:05:34,50 --> 00:05:38,30
And now I can go back to my browser and check if it works.

140
00:05:38,30 --> 00:05:40,20
Here's my index.html.

141
00:05:40,20 --> 00:05:41,80
I'll reload the page

142
00:05:41,80 --> 00:05:42,90
and there we have it.

143
00:05:42,90 --> 00:05:44,90
I've created my size selector component,

144
00:05:44,90 --> 00:05:47,10
which is driven by an array of data.

145
00:05:47,10 --> 00:05:48,60
Those data come from another script tag,

146
00:05:48,60 --> 00:05:51,40
it's not from an API or anything fancy like that,

147
00:05:51,40 --> 00:05:53,10
but I've been able to create this component

148
00:05:53,10 --> 00:05:56,00
without having to hard code all of those values.

