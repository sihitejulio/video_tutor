1
00:00:00,60 --> 00:00:02,80
- [Instructor] Comparing my index.html file

2
00:00:02,80 --> 00:00:05,60
to my mock up, there's just one thing that's missing,

3
00:00:05,60 --> 00:00:07,30
this color selector.

4
00:00:07,30 --> 00:00:08,60
So, in this video, we're going to create

5
00:00:08,60 --> 00:00:10,20
that ColorSelector component.

6
00:00:10,20 --> 00:00:11,50
It's going to be very similar

7
00:00:11,50 --> 00:00:13,80
to our SizeSelector.

8
00:00:13,80 --> 00:00:15,90
Switching back over to my editor,

9
00:00:15,90 --> 00:00:17,40
I'm going to jump down

10
00:00:17,40 --> 00:00:20,10
to my ProductCustomizer

11
00:00:20,10 --> 00:00:24,60
and I'm going to add my ColorSelector here.

12
00:00:24,60 --> 00:00:27,10
Now of course with the SizeSelector I'm passing in the size

13
00:00:27,10 --> 00:00:29,10
and the sizes but with colors

14
00:00:29,10 --> 00:00:32,90
I want to pass in the color and the colors.

15
00:00:32,90 --> 00:00:34,70
So, I'm going to select this

16
00:00:34,70 --> 00:00:36,70
and I'm going to use Command + D

17
00:00:36,70 --> 00:00:40,00
to select every instance following this one

18
00:00:40,00 --> 00:00:41,30
of that word.

19
00:00:41,30 --> 00:00:44,10
Many editors have this kind of multiple cursor editing

20
00:00:44,10 --> 00:00:47,30
that will allow you to do something like this.

21
00:00:47,30 --> 00:00:48,80
I'll just type color here

22
00:00:48,80 --> 00:00:50,00
and that replaces all of them

23
00:00:50,00 --> 00:00:52,00
and I can see it in action.

24
00:00:52,00 --> 00:00:54,80
It's also undoable with Command + Z

25
00:00:54,80 --> 00:00:57,90
and redoable with Shift + Command + Z.

26
00:00:57,90 --> 00:01:01,60
Of course I need the value for colors here.

27
00:01:01,60 --> 00:01:04,40
Just as we're doing with the sizes,

28
00:01:04,40 --> 00:01:06,00
going to collect those colors

29
00:01:06,00 --> 00:01:08,50
from the inventory as well.

30
00:01:08,50 --> 00:01:11,90
Now this time if I wanted to change this,

31
00:01:11,90 --> 00:01:14,20
I can use that Command + D trick again

32
00:01:14,20 --> 00:01:15,60
but when I type this,

33
00:01:15,60 --> 00:01:18,50
the case will just be exactly what I typed.

34
00:01:18,50 --> 00:01:20,00
Unfortunately Visual Studio Code

35
00:01:20,00 --> 00:01:21,80
doesn't have a way to do searching and replacing

36
00:01:21,80 --> 00:01:23,30
in a way that preserves the case

37
00:01:23,30 --> 00:01:24,80
as it was originally typed.

38
00:01:24,80 --> 00:01:26,00
Many editors will have that

39
00:01:26,00 --> 00:01:28,30
and that's something you can look for in yours.

40
00:01:28,30 --> 00:01:29,20
What I'm going to do here

41
00:01:29,20 --> 00:01:32,20
is type the uppercase version

42
00:01:32,20 --> 00:01:33,40
which will work for all of these

43
00:01:33,40 --> 00:01:35,10
except this very first one

44
00:01:35,10 --> 00:01:36,70
which I'll just change manually.

45
00:01:36,70 --> 00:01:38,40
Okay, so I have all my state

46
00:01:38,40 --> 00:01:39,30
and I have my element

47
00:01:39,30 --> 00:01:41,60
where this component is going to be rendered

48
00:01:41,60 --> 00:01:43,60
but now I need to create the function

49
00:01:43,60 --> 00:01:45,20
that will make this component,

50
00:01:45,20 --> 00:01:46,90
so I'm going to select

51
00:01:46,90 --> 00:01:50,20
and copy the whole SizeSelector component

52
00:01:50,20 --> 00:01:52,20
and we'll do the same thing here

53
00:01:52,20 --> 00:01:56,20
to replace all these instances of size with color.

54
00:01:56,20 --> 00:01:57,50
So, I'll hit Command + D a bunch of times

55
00:01:57,50 --> 00:01:59,40
as I watch these highlight

56
00:01:59,40 --> 00:02:01,20
and I'll use the lowercase version here.

57
00:02:01,20 --> 00:02:03,30
That seems a little more common.

58
00:02:03,30 --> 00:02:04,40
So, this'll be the color selector

59
00:02:04,40 --> 00:02:06,90
with its color options and so forth

60
00:02:06,90 --> 00:02:09,00
and I think this might be the only uppercase one

61
00:02:09,00 --> 00:02:10,60
that I need to change.

62
00:02:10,60 --> 00:02:12,40
And the only other thing that I might change here

63
00:02:12,40 --> 00:02:15,00
is here on my map function.

64
00:02:15,00 --> 00:02:15,90
This is going to be a color,

65
00:02:15,90 --> 00:02:18,70
so calling the argument num doesn't really make sense.

66
00:02:18,70 --> 00:02:22,80
So, I will update this, again using Command + D.

67
00:02:22,80 --> 00:02:24,90
We'll call this the color name.

68
00:02:24,90 --> 00:02:26,60
So, again we're not using the word color

69
00:02:26,60 --> 00:02:27,70
all over the place.

70
00:02:27,70 --> 00:02:28,70
This is the name of the color,

71
00:02:28,70 --> 00:02:30,60
so I think that works.

72
00:02:30,60 --> 00:02:33,00
All right and let me not forget

73
00:02:33,00 --> 00:02:35,60
that all components need to have an uppercase letter

74
00:02:35,60 --> 00:02:37,60
at the beginning of their implementation.

75
00:02:37,60 --> 00:02:40,90
So, I will change this so it's uppercase.

76
00:02:40,90 --> 00:02:43,20
Going to follow those React conventions

77
00:02:43,20 --> 00:02:47,50
and now if I switch back to the browser and refresh,

78
00:02:47,50 --> 00:02:51,20
okay, we have our component rendering on the page.

79
00:02:51,20 --> 00:02:55,50
Let's open up the developer tools with Command + Option + I.

80
00:02:55,50 --> 00:02:57,60
Nothing untoward in the console,

81
00:02:57,60 --> 00:02:59,80
let's switch over to the React tab.

82
00:02:59,80 --> 00:03:01,80
And get rid of this extra console.

83
00:03:01,80 --> 00:03:03,80
And I can see here on my product customizer

84
00:03:03,80 --> 00:03:08,40
that I have some new state here, a five-element-long array.

85
00:03:08,40 --> 00:03:09,40
I can't see that directly

86
00:03:09,40 --> 00:03:11,30
because it's a hook but if I open this up

87
00:03:11,30 --> 00:03:12,50
and check the ColorSelector,

88
00:03:12,50 --> 00:03:14,90
I can see by inspecting its props

89
00:03:14,90 --> 00:03:17,10
that all the right values are coming through.

90
00:03:17,10 --> 00:03:18,30
And of course I can know that

91
00:03:18,30 --> 00:03:21,10
because I know how I saw it up here as well.

92
00:03:21,10 --> 00:03:22,80
So, now we have a ColorSelector component

93
00:03:22,80 --> 00:03:24,30
that's rendered on the page

94
00:03:24,30 --> 00:03:27,00
very much like our SizeSelector and we can move on.

