1
00:00:00,50 --> 00:00:02,20
- [Instructor] In this video, we're going to attach

2
00:00:02,20 --> 00:00:05,90
a source of inventory data for our little store page.

3
00:00:05,90 --> 00:00:09,50
The first component we'll be building is this size selector,

4
00:00:09,50 --> 00:00:12,60
which has hard coded values right here.

5
00:00:12,60 --> 00:00:14,60
But really, we'll be assuming that these sizes would be

6
00:00:14,60 --> 00:00:17,20
retrieved from sort of inventory system.

7
00:00:17,20 --> 00:00:20,30
Maybe it's an external API or who knows what.

8
00:00:20,30 --> 00:00:22,00
We're going to cheat a little bit just to make sure that

9
00:00:22,00 --> 00:00:24,40
this doesn't get too hellaciously complicated.

10
00:00:24,40 --> 00:00:26,40
We're going to have an external data source that

11
00:00:26,40 --> 00:00:28,30
our component is going to draw from.

12
00:00:28,30 --> 00:00:31,40
So let's start by installing that little data source.

13
00:00:31,40 --> 00:00:33,50
Switching over to my editor,

14
00:00:33,50 --> 00:00:36,30
the index dot html file is where I'll be installing that

15
00:00:36,30 --> 00:00:38,70
source of data, but we're going to get it from this

16
00:00:38,70 --> 00:00:41,40
inventory snippet that comes with the exercise files.

17
00:00:41,40 --> 00:00:44,10
It's a JavaScript file and I'm going to just select all of

18
00:00:44,10 --> 00:00:47,60
this and bring it over into my index dot html.

19
00:00:47,60 --> 00:00:49,80
We'll scroll all the way down.

20
00:00:49,80 --> 00:00:51,20
Command down arrow.

21
00:00:51,20 --> 00:00:55,10
And I'm going to install this right before my app dot js.

22
00:00:55,10 --> 00:00:56,60
That way, everything that appears in

23
00:00:56,60 --> 00:00:58,80
this script is going to be available here.

24
00:00:58,80 --> 00:01:00,70
So, I'll save this and let's take a look at

25
00:01:00,70 --> 00:01:03,20
this inventory script to see what it looks like.

26
00:01:03,20 --> 00:01:05,30
I'm going to use command click here in

27
00:01:05,30 --> 00:01:09,10
Visual Studio to jump over to that file.

28
00:01:09,10 --> 00:01:10,80
So what I have here is a global object,

29
00:01:10,80 --> 00:01:12,90
called Inventory, with a capital I.

30
00:01:12,90 --> 00:01:14,80
Tell it apart from other things that

31
00:01:14,80 --> 00:01:16,70
I might see in the developer console.

32
00:01:16,70 --> 00:01:18,10
And it's got a few properties that

33
00:01:18,10 --> 00:01:20,80
I'll be using as my sources of data.

34
00:01:20,80 --> 00:01:22,90
The first is all sizes.

35
00:01:22,90 --> 00:01:24,70
I could've written this out as a hard coded object,

36
00:01:24,70 --> 00:01:26,70
but I got a little overly clever I suppose

37
00:01:26,70 --> 00:01:28,80
by writing a little helper function that will

38
00:01:28,80 --> 00:01:32,10
take the smallest size and the largest size

39
00:01:32,10 --> 00:01:34,80
and return an array of sizes from the smallest one to

40
00:01:34,80 --> 00:01:37,90
the largest, including all the half sizes.

41
00:01:37,90 --> 00:01:39,80
That way you can change these, if you feel like it,

42
00:01:39,80 --> 00:01:43,20
to have sizes five up to 18 or whatever you like.

43
00:01:43,20 --> 00:01:44,80
We're going to stick with seven to 12.

44
00:01:44,80 --> 00:01:47,00
But, you can change that however you like.

45
00:01:47,00 --> 00:01:48,70
Next up is the colors, which are

46
00:01:48,70 --> 00:01:50,80
available when there's all of them.

47
00:01:50,80 --> 00:01:55,10
And this is just a scaler array, no need to get fancy here.

48
00:01:55,10 --> 00:01:56,20
Then I have two keys.

49
00:01:56,20 --> 00:01:57,60
I'm going to collapse these down so you can just

50
00:01:57,60 --> 00:02:00,80
see their names, by size and by color.

51
00:02:00,80 --> 00:02:03,10
If you were working with a real API,

52
00:02:03,10 --> 00:02:04,50
you would probably be able to query the

53
00:02:04,50 --> 00:02:07,10
sizes that are available to get whichever

54
00:02:07,10 --> 00:02:10,70
colors are available for those sizes and vice versa.

55
00:02:10,70 --> 00:02:12,50
So I've hard coded this into

56
00:02:12,50 --> 00:02:15,20
a couple of keys in this object.

57
00:02:15,20 --> 00:02:18,00
So I can ask, within the size seven,

58
00:02:18,00 --> 00:02:19,20
what colors are available?

59
00:02:19,20 --> 00:02:22,10
Or for a size nine, what colors are available?

60
00:02:22,10 --> 00:02:26,40
Likewise, if I have the color red, what sizes are available?

61
00:02:26,40 --> 00:02:27,60
You could probably do something clever where

62
00:02:27,60 --> 00:02:29,10
you could just transform one version of

63
00:02:29,10 --> 00:02:31,20
this object into another with a function.

64
00:02:31,20 --> 00:02:32,70
I just didn't feel like doing that,

65
00:02:32,70 --> 00:02:35,50
so I have each of these hard coded.

66
00:02:35,50 --> 00:02:38,70
So that's what our little source of data looks like.

67
00:02:38,70 --> 00:02:41,00
This will simulate everything that we need in order to

68
00:02:41,00 --> 00:02:43,80
be able to fulfill the requirements of our little page.

69
00:02:43,80 --> 00:02:46,20
Now we can use that in our components.

70
00:02:46,20 --> 00:02:47,50
Just to confirm that this all works,

71
00:02:47,50 --> 00:02:49,60
I'm going to switch back over to the browser.

72
00:02:49,60 --> 00:02:51,30
Here's my index dot html file.

73
00:02:51,30 --> 00:02:54,10
I will refresh this and open up the developer console.

74
00:02:54,10 --> 00:02:55,40
Clear these messages.

75
00:02:55,40 --> 00:02:57,80
Let's just confirm that window dot

76
00:02:57,80 --> 00:03:00,50
inventory exists, and it does.

77
00:03:00,50 --> 00:03:02,10
You can see, I'm getting a little preview of the things

78
00:03:02,10 --> 00:03:07,80
that are available here, all colors, all sizes, great.

79
00:03:07,80 --> 00:03:10,40
Okay, so with that snippet installed, we now have access

80
00:03:10,40 --> 00:03:12,90
in our app dot js file to all that inventory data.

81
00:03:12,90 --> 00:03:15,60
So, by doing this, we're not having to do any API queries.

82
00:03:15,60 --> 00:03:17,90
But, we are at least treating our data as something that

83
00:03:17,90 --> 00:03:20,00
we're not hard coding directly into the script.

