1
00:00:00,30 --> 00:00:07,80
(upbeat music)

2
00:00:07,80 --> 00:00:08,80
- [Instructor] Here's my solution

3
00:00:08,80 --> 00:00:10,60
to the color selector challenge.

4
00:00:10,60 --> 00:00:12,30
The goal is to activate the color selector

5
00:00:12,30 --> 00:00:13,60
so that it causes changes

6
00:00:13,60 --> 00:00:16,40
in the available list of sizes and optionally,

7
00:00:16,40 --> 00:00:19,30
to update the selected size piece of state

8
00:00:19,30 --> 00:00:21,70
based on that availability check.

9
00:00:21,70 --> 00:00:24,50
So if I change the color from red to green,

10
00:00:24,50 --> 00:00:26,90
I can see that the shoe changes color.

11
00:00:26,90 --> 00:00:30,70
And my list of sizes is shrinking.

12
00:00:30,70 --> 00:00:33,70
Now if I start changing things over here,

13
00:00:33,70 --> 00:00:35,50
I'm starting to paint myself into a corner a little bit

14
00:00:35,50 --> 00:00:37,10
where some of the colors are disappearing,

15
00:00:37,10 --> 00:00:39,20
and some of the sizes are disappearing as well.

16
00:00:39,20 --> 00:00:42,60
We could add a react powered reset button to fix all this.

17
00:00:42,60 --> 00:00:45,20
We don't have it right now, so I'll just reload.

18
00:00:45,20 --> 00:00:46,90
I also said to watch out for weird behavior,

19
00:00:46,90 --> 00:00:49,20
and of course, paint ourselves into a corner like that

20
00:00:49,20 --> 00:00:52,40
was one piece of weird behavior, but here's another one.

21
00:00:52,40 --> 00:00:56,20
If I keep the color red and go to size 12, say,

22
00:00:56,20 --> 00:00:58,80
the picture shows a brown shoe

23
00:00:58,80 --> 00:01:01,60
but the currently selected color is green.

24
00:01:01,60 --> 00:01:03,70
We're going to look at why this is, in future lessons,

25
00:01:03,70 --> 00:01:05,40
but for now, just know that the problem is

26
00:01:05,40 --> 00:01:08,50
we don't have a single source of truth here.

27
00:01:08,50 --> 00:01:10,70
The DOM has the notion of the current color,

28
00:01:10,70 --> 00:01:15,10
which is separate from the state, represented in React.

29
00:01:15,10 --> 00:01:17,10
We'll look at how to bring these together later.

30
00:01:17,10 --> 00:01:18,50
For now, let's switch over to the editor,

31
00:01:18,50 --> 00:01:20,40
and look at the code.

32
00:01:20,40 --> 00:01:22,80
So first I'm going to jump down to my ColorSelector component.

33
00:01:22,80 --> 00:01:27,90
I'll use cmd + shift + o to jump down to my ColorSelector.

34
00:01:27,90 --> 00:01:30,50
And just like I had in the SizeSelector before,

35
00:01:30,50 --> 00:01:32,40
I have an onChange event with a handler

36
00:01:32,40 --> 00:01:36,30
that I'm calling onColorChange, which is defined right here.

37
00:01:36,30 --> 00:01:40,40
And that's calling a prop which is called handleColorChange

38
00:01:40,40 --> 00:01:43,60
and passing in the currently selected value

39
00:01:43,60 --> 00:01:45,60
that we're retrieving from the DOM.

40
00:01:45,60 --> 00:01:47,80
And down here in the ProductCustomizer,

41
00:01:47,80 --> 00:01:50,40
I have my handleColorChange function here,

42
00:01:50,40 --> 00:01:52,50
which is passed in as a prop down here

43
00:01:52,50 --> 00:01:55,10
in the ColorSelector element.

44
00:01:55,10 --> 00:01:56,80
I'm doing a similar inventory check here,

45
00:01:56,80 --> 00:01:59,10
just looking at the Inventory.byColor object,

46
00:01:59,10 --> 00:02:02,60
passing in the selected color to get the available sizes.

47
00:02:02,60 --> 00:02:05,80
And then setting the sizes state using availableSizes

48
00:02:05,80 --> 00:02:08,20
and setting the color to the selectedColor

49
00:02:08,20 --> 00:02:10,00
that was passed in which is what updates

50
00:02:10,00 --> 00:02:12,30
the picture of the shoe.

51
00:02:12,30 --> 00:02:15,30
Then for the bonus material with the inventory check,

52
00:02:15,30 --> 00:02:16,50
I have a little addition to both

53
00:02:16,50 --> 00:02:19,30
handleSizeChange and handleColorChange.

54
00:02:19,30 --> 00:02:22,50
What I'm doing here is using the array index of function

55
00:02:22,50 --> 00:02:25,30
to check if the color state from up here

56
00:02:25,30 --> 00:02:28,10
is in that array of availableColors.

57
00:02:28,10 --> 00:02:30,60
If it's not, which I know because indexOf

58
00:02:30,60 --> 00:02:33,70
would return negative one, I'm setting that color state

59
00:02:33,70 --> 00:02:35,60
to be whatever the first color is

60
00:02:35,60 --> 00:02:37,90
in that availableColors array.

61
00:02:37,90 --> 00:02:40,40
And I'm doing the same thing down here with the sizes.

62
00:02:40,40 --> 00:02:42,40
If whatever the current size that's selected

63
00:02:42,40 --> 00:02:45,40
is not available in the currently selected color,

64
00:02:45,40 --> 00:02:48,10
I'm changing the state so that size is the

65
00:02:48,10 --> 00:02:50,50
first available size.

66
00:02:50,50 --> 00:02:53,90
So that completes our little color selector challenge.

67
00:02:53,90 --> 00:02:55,60
We've got all those pieces of state updating.

68
00:02:55,60 --> 00:02:56,90
We've got a little bit of weird behavior

69
00:02:56,90 --> 00:02:58,70
that would need to be dealt with later

70
00:02:58,70 --> 00:03:01,20
as we are flushing this material out some more.

71
00:03:01,20 --> 00:03:04,00
But we've learned a lot and we're ready to move on.

