1
00:00:00,20 --> 00:00:02,00
- [Instructor] Now we have a status updater

2
00:00:02,00 --> 00:00:04,30
that can read messages from our little API

3
00:00:04,30 --> 00:00:05,80
and post new ones.

4
00:00:05,80 --> 00:00:08,90
But to see the new ones, we have to reload the page.

5
00:00:08,90 --> 00:00:11,10
In this video, we'll start fixing that problem

6
00:00:11,10 --> 00:00:14,90
using a very common task in React, lifting up state.

7
00:00:14,90 --> 00:00:16,20
We want to let this post form

8
00:00:16,20 --> 00:00:17,90
add to the list of status messages

9
00:00:17,90 --> 00:00:20,00
when it sends a new one to the API.

10
00:00:20,00 --> 00:00:23,00
React's data flow is one way, downhill only,

11
00:00:23,00 --> 00:00:25,20
from parents to children via props.

12
00:00:25,20 --> 00:00:27,10
These two components are siblings

13
00:00:27,10 --> 00:00:29,20
and they can't talk to each other directly.

14
00:00:29,20 --> 00:00:31,10
In order for both of them to be able to access

15
00:00:31,10 --> 00:00:32,70
the messages state, which is currently

16
00:00:32,70 --> 00:00:34,60
in the message list component,

17
00:00:34,60 --> 00:00:37,70
we need to lift it out of there, into their shared parent.

18
00:00:37,70 --> 00:00:39,60
During the planning phase, we'll often catch

19
00:00:39,60 --> 00:00:42,10
possible issues like this before we even start,

20
00:00:42,10 --> 00:00:44,90
but sometimes we don't and in the course of building a site,

21
00:00:44,90 --> 00:00:47,30
needs can change, so this is a very common exercise

22
00:00:47,30 --> 00:00:49,00
that's worth going through now.

23
00:00:49,00 --> 00:00:51,60
And it's not very strenuous exercise.

24
00:00:51,60 --> 00:00:53,50
So over here in hotel.js,

25
00:00:53,50 --> 00:00:55,30
we're going to use Command + Shift + O

26
00:00:55,30 --> 00:00:58,20
to jump down to the message list component

27
00:00:58,20 --> 00:00:59,70
and here is our state.

28
00:00:59,70 --> 00:01:01,80
So we definitely want to move the statuses,

29
00:01:01,80 --> 00:01:03,50
but we'll also move loaded

30
00:01:03,50 --> 00:01:06,20
because they both deal with the same action.

31
00:01:06,20 --> 00:01:08,00
In fact, we can just move everything that deals

32
00:01:08,00 --> 00:01:09,60
with the state here.

33
00:01:09,60 --> 00:01:13,30
This helper function calls both of our state setters.

34
00:01:13,30 --> 00:01:16,30
So we'll just grab everything here and cut it.

35
00:01:16,30 --> 00:01:18,00
We'll bring it down to the shared parent

36
00:01:18,00 --> 00:01:20,60
which is the status message manager component.

37
00:01:20,60 --> 00:01:23,20
I'll paste this in right under the message types.

38
00:01:23,20 --> 00:01:25,70
We don't need to change anything here so far.

39
00:01:25,70 --> 00:01:28,00
We're just moving the state up

40
00:01:28,00 --> 00:01:29,60
and the actions are still the same.

41
00:01:29,60 --> 00:01:31,90
What we do need to do is pass that state

42
00:01:31,90 --> 00:01:33,80
back into the components that need it.

43
00:01:33,80 --> 00:01:35,60
Post form doesn't need anything yet,

44
00:01:35,60 --> 00:01:38,60
but the message list component needs these statuses.

45
00:01:38,60 --> 00:01:42,60
So let's pass them down as a prop called statuses

46
00:01:42,60 --> 00:01:45,20
'cause that is what this was called.

47
00:01:45,20 --> 00:01:47,80
We will also pass down loaded.

48
00:01:47,80 --> 00:01:49,70
No need to change the names.

49
00:01:49,70 --> 00:01:52,90
Let's jump back up to our status message list component.

50
00:01:52,90 --> 00:01:57,00
We need to change this so it uses those props.

51
00:01:57,00 --> 00:01:59,90
This statuses is now going to be props.statuses.

52
00:01:59,90 --> 00:02:02,50
Everything inside this helper function should work fine.

53
00:02:02,50 --> 00:02:06,60
And then loaded is now a prop also, props.loaded.

54
00:02:06,60 --> 00:02:09,00
Now we can save this and switch back to the browser

55
00:02:09,00 --> 00:02:11,10
and confirm that everything still works.

56
00:02:11,10 --> 00:02:12,60
Great, nothing is broken.

57
00:02:12,60 --> 00:02:15,50
Because function components have access to state via hooks,

58
00:02:15,50 --> 00:02:17,30
lifting up state between function components

59
00:02:17,30 --> 00:02:18,50
is relatively easy.

60
00:02:18,50 --> 00:02:20,10
We don't have to make any changes to the code,

61
00:02:20,10 --> 00:02:21,10
most of the time.

62
00:02:21,10 --> 00:02:23,20
Before hooks, if we were moving state

63
00:02:23,20 --> 00:02:24,50
between different components,

64
00:02:24,50 --> 00:02:27,00
we might have to convert a function into a class

65
00:02:27,00 --> 00:02:28,50
when the state is being lifted up,

66
00:02:28,50 --> 00:02:30,80
requiring more than just the simple copy and paste.

67
00:02:30,80 --> 00:02:31,90
This is pretty cool.

68
00:02:31,90 --> 00:02:33,60
Lifting state up like this is something

69
00:02:33,60 --> 00:02:35,50
that's quite common in React.

70
00:02:35,50 --> 00:02:37,20
Even the best planned component tree

71
00:02:37,20 --> 00:02:39,00
can end up needing this work at various times

72
00:02:39,00 --> 00:02:41,40
due to changing requirements or other issues.

73
00:02:41,40 --> 00:02:43,20
As we've seen, it's not terribly difficult,

74
00:02:43,20 --> 00:02:44,50
especially when we don't have to think

75
00:02:44,50 --> 00:02:48,00
about changing a component room function to a class.

