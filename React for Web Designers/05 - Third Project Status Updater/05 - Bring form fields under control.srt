1
00:00:00,60 --> 00:00:02,70
- [Instructor] Now we need to start wiring up this post form

2
00:00:02,70 --> 00:00:05,60
so it can send updates to the API.

3
00:00:05,60 --> 00:00:07,20
We'll take this in two pieces.

4
00:00:07,20 --> 00:00:09,10
First, the form is currently uncontrolled,

5
00:00:09,10 --> 00:00:11,00
so we can type in it all we want,

6
00:00:11,00 --> 00:00:13,60
and React does not notice.

7
00:00:13,60 --> 00:00:15,30
We'll handle that in this video.

8
00:00:15,30 --> 00:00:16,70
The second part, we'll be taking action

9
00:00:16,70 --> 00:00:18,30
when the form is submitted.

10
00:00:18,30 --> 00:00:22,80
Over here in hotel.js, here is my post form component,

11
00:00:22,80 --> 00:00:24,70
and here are my form fields.

12
00:00:24,70 --> 00:00:26,80
So the first thing that I'm going to do is add a little bit

13
00:00:26,80 --> 00:00:30,90
of state to hold the values of those fields.

14
00:00:30,90 --> 00:00:33,80
So we'll have one called message text,

15
00:00:33,80 --> 00:00:37,70
which will have set message text to go with it,

16
00:00:37,70 --> 00:00:41,00
and these will come from React.usestate,

17
00:00:41,00 --> 00:00:42,60
and the initial value for message text

18
00:00:42,60 --> 00:00:44,90
will be an empty string.

19
00:00:44,90 --> 00:00:47,50
We'll call React.usestate again.

20
00:00:47,50 --> 00:00:49,90
This time it will be for the message type,

21
00:00:49,90 --> 00:00:51,60
that select box.

22
00:00:51,60 --> 00:00:54,50
Set message type and the initial value

23
00:00:54,50 --> 00:00:57,60
will be this default type that we saved earlier.

24
00:00:57,60 --> 00:00:59,80
Now we need to update our form fields.

25
00:00:59,80 --> 00:01:01,50
We'll start with the text area.

26
00:01:01,50 --> 00:01:04,50
This will need an on change handler,

27
00:01:04,50 --> 00:01:08,50
which we can call on text change.

28
00:01:08,50 --> 00:01:12,80
We'll have one for the type as well called on type change.

29
00:01:12,80 --> 00:01:16,60
We'll define these functions up here underneath the state.

30
00:01:16,60 --> 00:01:20,50
This will be a function called on text change,

31
00:01:20,50 --> 00:01:22,50
and we'll pass in the event,

32
00:01:22,50 --> 00:01:26,40
and now we just need to call the state updater

33
00:01:26,40 --> 00:01:27,50
that goes with it,

34
00:01:27,50 --> 00:01:32,90
and we'll pass in the target.value from the event,

35
00:01:32,90 --> 00:01:35,60
and we're going to do pretty much exactly the same thing

36
00:01:35,60 --> 00:01:37,30
with the type field,

37
00:01:37,30 --> 00:01:39,20
so on type change,

38
00:01:39,20 --> 00:01:41,20
we'll set message type

39
00:01:41,20 --> 00:01:43,90
using separate handlers that have the same body right now,

40
00:01:43,90 --> 00:01:44,80
which is kind of a bummer

41
00:01:44,80 --> 00:01:47,10
if we want to avoid repeating ourselves,

42
00:01:47,10 --> 00:01:48,00
but we'll allow it here

43
00:01:48,00 --> 00:01:50,40
because if we were going to run this out in the wild,

44
00:01:50,40 --> 00:01:52,70
we want to sanitize and validate the values that are

45
00:01:52,70 --> 00:01:56,10
coming in from the user before saving them to state,

46
00:01:56,10 --> 00:01:58,60
and we might want to handle those two pieces differently.

47
00:01:58,60 --> 00:02:00,50
We're not doing that here to keep things relatively brief,

48
00:02:00,50 --> 00:02:03,80
but because we could, we'll leave these handlers separate.

49
00:02:03,80 --> 00:02:05,00
You can, of course, flush these out

50
00:02:05,00 --> 00:02:07,10
however you want once you finished,

51
00:02:07,10 --> 00:02:08,60
and the last thing we need to do to make sure

52
00:02:08,60 --> 00:02:10,50
that this form is under React's control

53
00:02:10,50 --> 00:02:15,60
is to add value prop to each of these fields.

54
00:02:15,60 --> 00:02:18,20
This one will be message text.

55
00:02:18,20 --> 00:02:22,10
We can copy this and paste it down here.

56
00:02:22,10 --> 00:02:24,40
This one will be message type

57
00:02:24,40 --> 00:02:26,20
and now we can save.

58
00:02:26,20 --> 00:02:29,80
Prettier takes a quick pass over the file to reformat it,

59
00:02:29,80 --> 00:02:32,10
now we can reload and give it a try.

60
00:02:32,10 --> 00:02:36,70
I'll open up the React dev tools with command option I,

61
00:02:36,70 --> 00:02:37,90
and switch to the React pane

62
00:02:37,90 --> 00:02:41,70
and open up my post form component here

63
00:02:41,70 --> 00:02:43,00
and I can see I have some state.

64
00:02:43,00 --> 00:02:44,60
Let's try typing.

65
00:02:44,60 --> 00:02:46,60
Here is our message.

66
00:02:46,60 --> 00:02:49,40
My typing is fabulous, but we can see that a state

67
00:02:49,40 --> 00:02:52,90
is updating over here for both fields.

68
00:02:52,90 --> 00:02:54,00
Great.

69
00:02:54,00 --> 00:02:56,20
So now we have controlled form fields

70
00:02:56,20 --> 00:02:57,50
for the post form component

71
00:02:57,50 --> 00:02:59,60
and we can move on to wiring up the form

72
00:02:59,60 --> 00:03:03,00
so it actually posts the updates next.

