1
00:00:00,10 --> 00:00:01,10
- [Instructor] In this video,

2
00:00:01,10 --> 00:00:03,40
we're going to retrieve data from our API

3
00:00:03,40 --> 00:00:06,40
using another React hook called useEffect

4
00:00:06,40 --> 00:00:09,50
and render the status messages on the page.

5
00:00:09,50 --> 00:00:11,70
Over here in our code, we're going to go down to

6
00:00:11,70 --> 00:00:14,20
the status message list component.

7
00:00:14,20 --> 00:00:15,80
So I'm going to use

8
00:00:15,80 --> 00:00:17,70
Shift command + O,

9
00:00:17,70 --> 00:00:20,60
I can arrow down to status message list,

10
00:00:20,60 --> 00:00:23,40
and I have my stub statuses collapsed down here

11
00:00:23,40 --> 00:00:26,20
to make it a little easier to read.

12
00:00:26,20 --> 00:00:27,70
So what we're going to do

13
00:00:27,70 --> 00:00:31,30
is load the status messages using an http get

14
00:00:31,30 --> 00:00:33,60
from our little API.

15
00:00:33,60 --> 00:00:37,10
The URL for that as we currently have it configured

16
00:00:37,10 --> 00:00:38,00
is going to be something like

17
00:00:38,00 --> 00:00:44,30
localhost/reactjs/status_api/get.php.

18
00:00:44,30 --> 00:00:46,60
I can select that, copy it,

19
00:00:46,60 --> 00:00:48,80
I'll just check this over in my browser.

20
00:00:48,80 --> 00:00:49,60
There we go.

21
00:00:49,60 --> 00:00:51,70
That doesn't load stuff.

22
00:00:51,70 --> 00:00:53,80
So I'm going to write a helper function to

23
00:00:53,80 --> 00:00:55,50
do the message retrieval.

24
00:00:55,50 --> 00:01:00,20
We'll call it retrieve status messages.

25
00:01:00,20 --> 00:01:02,70
And for this, we'll use that Axios library

26
00:01:02,70 --> 00:01:04,60
that we brought in,

27
00:01:04,60 --> 00:01:06,30
which has a get method.

28
00:01:06,30 --> 00:01:10,40
Now all we need to pass to that in the URL of interest.

29
00:01:10,40 --> 00:01:12,60
And this will return a promise.

30
00:01:12,60 --> 00:01:15,40
So we'll tack on a then method

31
00:01:15,40 --> 00:01:18,50
and pass in a call back function

32
00:01:18,50 --> 00:01:21,40
that will take the response as parameter,

33
00:01:21,40 --> 00:01:22,90
and then we can act on that response

34
00:01:22,90 --> 00:01:24,00
here in the helper function.

35
00:01:24,00 --> 00:01:27,80
And what we do is set our status's state

36
00:01:27,80 --> 00:01:30,60
to the messages that we get back from that API.

37
00:01:30,60 --> 00:01:32,20
So I will copy and paste

38
00:01:32,20 --> 00:01:35,30
the set status's function down here.

39
00:01:35,30 --> 00:01:38,40
And with Axios, this response will contain a data field

40
00:01:38,40 --> 00:01:40,70
which is where we can get those messages.

41
00:01:40,70 --> 00:01:43,10
So response.data.

42
00:01:43,10 --> 00:01:44,60
All right, I will save that,

43
00:01:44,60 --> 00:01:46,80
Prettier does a little bit of reformatting for me,

44
00:01:46,80 --> 00:01:48,20
and now we have a helper function

45
00:01:48,20 --> 00:01:50,20
that can retrieve those status messages.

46
00:01:50,20 --> 00:01:53,00
But we're not actually calling it anywhere yet.

47
00:01:53,00 --> 00:01:54,70
So how are we going to do that?

48
00:01:54,70 --> 00:01:58,10
With classes, we use life cycle methods,

49
00:01:58,10 --> 00:01:59,90
which are functions a class component calls

50
00:01:59,90 --> 00:02:02,10
at different times in a component's life span.

51
00:02:02,10 --> 00:02:04,00
Like when the component is rendered to the dom,

52
00:02:04,00 --> 00:02:07,80
called mounting, and when it's removed, called unmounting.

53
00:02:07,80 --> 00:02:10,10
This is all covered in the React documentation,

54
00:02:10,10 --> 00:02:13,50
in the state and life cycle section.

55
00:02:13,50 --> 00:02:17,20
We are using hooks and function components,

56
00:02:17,20 --> 00:02:18,80
but this is not a class component.

57
00:02:18,80 --> 00:02:21,50
This is a function component, and for function components

58
00:02:21,50 --> 00:02:24,70
we have a different tool using hooks.

59
00:02:24,70 --> 00:02:26,40
Specifically, we're going to call one

60
00:02:26,40 --> 00:02:29,60
called React useEffect.

61
00:02:29,60 --> 00:02:31,70
useEffect lets us take action on a component

62
00:02:31,70 --> 00:02:33,30
after its initial render.

63
00:02:33,30 --> 00:02:36,10
These things are often called side effects or just effects,

64
00:02:36,10 --> 00:02:38,40
which is why it's called useEffect.

65
00:02:38,40 --> 00:02:40,00
So we insert this method

66
00:02:40,00 --> 00:02:42,70
and we pass to it a call back function,

67
00:02:42,70 --> 00:02:46,20
which will then do what we need to do as our side effect.

68
00:02:46,20 --> 00:02:49,20
In this case, retrieve the status messages.

69
00:02:49,20 --> 00:02:50,30
With something as simple as this,

70
00:02:50,30 --> 00:02:52,00
we could just take the body of this function

71
00:02:52,00 --> 00:02:53,30
and put it right in here,

72
00:02:53,30 --> 00:02:56,10
but it is kind of nice to write helper functions

73
00:02:56,10 --> 00:02:58,90
to make very clear what it is you're doing.

74
00:02:58,90 --> 00:03:00,80
Retrieve status messages needs to be defined

75
00:03:00,80 --> 00:03:02,80
inside status message list,

76
00:03:02,80 --> 00:03:05,60
because it needs to have access to this set status's

77
00:03:05,60 --> 00:03:07,30
state updater function.

78
00:03:07,30 --> 00:03:09,10
If it didn't need action to that function,

79
00:03:09,10 --> 00:03:10,70
we could define this outside the scope

80
00:03:10,70 --> 00:03:14,10
of all of our components and just call it inside here.

81
00:03:14,10 --> 00:03:15,60
And if this retrieve statuses function

82
00:03:15,60 --> 00:03:17,70
needed access to the component's props,

83
00:03:17,70 --> 00:03:21,20
it would need to be defined inside this call back function.

84
00:03:21,20 --> 00:03:23,00
React will warn you if you don't do that.

85
00:03:23,00 --> 00:03:24,20
So you can just watch out for that

86
00:03:24,20 --> 00:03:26,90
as you're writing these little side effect helpers.

87
00:03:26,90 --> 00:03:29,40
This side effective will be run when the component mounts,

88
00:03:29,40 --> 00:03:32,00
when it updates, and when it unmounts.

89
00:03:32,00 --> 00:03:33,50
But we don't want to fetch messages

90
00:03:33,50 --> 00:03:35,20
anytime anything changes.

91
00:03:35,20 --> 00:03:36,20
For now, we just want that to happen

92
00:03:36,20 --> 00:03:38,70
when the component has first rendered.

93
00:03:38,70 --> 00:03:39,90
To make sure that's how it works,

94
00:03:39,90 --> 00:03:43,40
we'll use a second parameter that's passed into useEffect,

95
00:03:43,40 --> 00:03:45,30
which is its list of dependencies

96
00:03:45,30 --> 00:03:47,60
which needs to be an array.

97
00:03:47,60 --> 00:03:49,00
These are supposed to be data that should

98
00:03:49,00 --> 00:03:52,00
trigger the side effect to rerun when they change,

99
00:03:52,00 --> 00:03:53,80
like state or props.

100
00:03:53,80 --> 00:03:56,60
In this case, we have no dependencies like that,

101
00:03:56,60 --> 00:03:58,30
so we just pass an empty array.

102
00:03:58,30 --> 00:04:00,60
This tells React to let the effect run one time

103
00:04:00,60 --> 00:04:03,60
when the component mounts, and only that one time.

104
00:04:03,60 --> 00:04:06,00
So let's save this, and we'll switch back over

105
00:04:06,00 --> 00:04:08,10
and give it a try.

106
00:04:08,10 --> 00:04:10,60
Great, and because each of these status messages

107
00:04:10,60 --> 00:04:13,10
updated with a little #db,

108
00:04:13,10 --> 00:04:16,20
I'm know that I'm pulling the messages from the database.

109
00:04:16,20 --> 00:04:17,70
So our side effect is working.

110
00:04:17,70 --> 00:04:18,80
Great.

111
00:04:18,80 --> 00:04:20,10
If you have different side effects

112
00:04:20,10 --> 00:04:22,00
that should take place on a single component,

113
00:04:22,00 --> 00:04:25,20
you can put them all in one call to useEffect.

114
00:04:25,20 --> 00:04:26,50
Or if they have different dependencies,

115
00:04:26,50 --> 00:04:29,00
you can call useEffect as many times as you need to,

116
00:04:29,00 --> 00:04:30,40
specifying different dependencies

117
00:04:30,40 --> 00:04:33,40
for each invocation in this array.

118
00:04:33,40 --> 00:04:35,20
The useEffect hook is great for doing things

119
00:04:35,20 --> 00:04:37,00
after a component renders;

120
00:04:37,00 --> 00:04:39,30
fetching data, setting timers and so forth.

121
00:04:39,30 --> 00:04:43,10
There is also a useLayout effect hook

122
00:04:43,10 --> 00:04:45,30
for side effects that need to measure elements of the dom

123
00:04:45,30 --> 00:04:47,40
or other things that are more visual.

124
00:04:47,40 --> 00:04:50,30
The recommendation is to always try useEffect first,

125
00:04:50,30 --> 00:04:51,50
then use Layout Effect

126
00:04:51,50 --> 00:04:53,60
only if you're seeing something weird.

127
00:04:53,60 --> 00:04:56,50
And looking toward the future, React's Suspense API

128
00:04:56,50 --> 00:04:58,10
will almost certainly take up the mantle

129
00:04:58,10 --> 00:05:00,50
for best practice for fetching data at some point,

130
00:05:00,50 --> 00:05:02,40
but for now this is the way to go.

131
00:05:02,40 --> 00:05:04,40
So now our status message list component

132
00:05:04,40 --> 00:05:06,20
is interfacing with our API

133
00:05:06,20 --> 00:05:10,00
and fetching messages using React's useEffect hook.

