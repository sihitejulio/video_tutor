1
00:00:00,50 --> 00:00:01,40
- [Instructor] In this video we are going

2
00:00:01,40 --> 00:00:02,80
to finish this project.

3
00:00:02,80 --> 00:00:04,50
Updating the post form component so

4
00:00:04,50 --> 00:00:06,20
it can trigger a direct update

5
00:00:06,20 --> 00:00:08,30
to the list of messages.

6
00:00:08,30 --> 00:00:09,70
Basically we need to create a function

7
00:00:09,70 --> 00:00:11,30
that can add a message to the list

8
00:00:11,30 --> 00:00:13,30
over here and use that function in

9
00:00:13,30 --> 00:00:14,80
the post form component.

10
00:00:14,80 --> 00:00:16,70
Over here in the code, we'll start where

11
00:00:16,70 --> 00:00:18,80
the state data now live, in the

12
00:00:18,80 --> 00:00:20,70
status message manager.

13
00:00:20,70 --> 00:00:23,60
Scroll down to the component.

14
00:00:23,60 --> 00:00:25,40
Here we are, and now below our

15
00:00:25,40 --> 00:00:27,20
retrieve status messages function

16
00:00:27,20 --> 00:00:28,80
we'll define a new function called

17
00:00:28,80 --> 00:00:30,90
add status message.

18
00:00:30,90 --> 00:00:32,70
And the expectation is that we will have

19
00:00:32,70 --> 00:00:34,80
a status message to add.

20
00:00:34,80 --> 00:00:36,30
So what we need to do here is

21
00:00:36,30 --> 00:00:39,10
eventually call set statuses with

22
00:00:39,10 --> 00:00:43,30
some updated data, some updated statuses we'll say.

23
00:00:43,30 --> 00:00:44,70
Now we don't want to change the

24
00:00:44,70 --> 00:00:46,80
statuses state directly.

25
00:00:46,80 --> 00:00:48,80
What we'll do is follow the pattern that

26
00:00:48,80 --> 00:00:50,30
we follow in react of creating a new

27
00:00:50,30 --> 00:00:53,10
variable, which we'll call updated statuses.

28
00:00:53,10 --> 00:00:56,70
Which will start out as statuses as they are.

29
00:00:56,70 --> 00:00:59,00
It'll make a copy of those statuses

30
00:00:59,00 --> 00:01:01,50
using slice with a zero index.

31
00:01:01,50 --> 00:01:03,00
It will just copy the whole thing.

32
00:01:03,00 --> 00:01:04,60
And we'll take this new status message

33
00:01:04,60 --> 00:01:06,80
that will be coming in to this function

34
00:01:06,80 --> 00:01:08,30
and we'll push it on to the end.

35
00:01:08,30 --> 00:01:12,00
Dated statuses dot push, status.

36
00:01:12,00 --> 00:01:13,70
So that should do what we want.

37
00:01:13,70 --> 00:01:15,80
Now we're going to pass this helper

38
00:01:15,80 --> 00:01:19,40
function down into the post form as a prop.

39
00:01:19,40 --> 00:01:21,20
We'll call it add status message,

40
00:01:21,20 --> 00:01:23,80
equals, add status message.

41
00:01:23,80 --> 00:01:24,90
Ok, that looks good.

42
00:01:24,90 --> 00:01:26,50
Now lets command click post form to

43
00:01:26,50 --> 00:01:28,40
jump up to it's definition.

44
00:01:28,40 --> 00:01:31,90
We can scroll down to our post status update function.

45
00:01:31,90 --> 00:01:33,50
This is where we're doing all the action and

46
00:01:33,50 --> 00:01:36,60
where we will need to update the list once

47
00:01:36,60 --> 00:01:38,20
we have a new message.

48
00:01:38,20 --> 00:01:40,60
We'll still get rid of this console log,

49
00:01:40,60 --> 00:01:43,20
and if the response has told us we are successful,

50
00:01:43,20 --> 00:01:47,00
we're going to update the list of messages.

51
00:01:47,00 --> 00:01:48,40
And we want to make sure that we capture the

52
00:01:48,40 --> 00:01:51,50
ID that came through, with that new message.

53
00:01:51,50 --> 00:01:53,40
So we're going to take our new status,

54
00:01:53,40 --> 00:01:56,10
copy and paste that here and add on

55
00:01:56,10 --> 00:01:57,90
an ID property, which will be

56
00:01:57,90 --> 00:02:02,90
response dot data, dot ID that came through.

57
00:02:02,90 --> 00:02:05,40
And then we can call that new function.

58
00:02:05,40 --> 00:02:07,50
Prop, set, add status message and

59
00:02:07,50 --> 00:02:09,40
we'll pass in the new status.

60
00:02:09,40 --> 00:02:10,80
And that's going to update the state in

61
00:02:10,80 --> 00:02:12,50
the parent component, which will trigger

62
00:02:12,50 --> 00:02:14,30
an update to the messages list.

63
00:02:14,30 --> 00:02:15,40
So we can save that.

64
00:02:15,40 --> 00:02:16,60
There is one more thing that

65
00:02:16,60 --> 00:02:18,00
we should probably do.

66
00:02:18,00 --> 00:02:21,10
Which is clear out the form values.

67
00:02:21,10 --> 00:02:22,30
So we can add a little comment here

68
00:02:22,30 --> 00:02:23,70
to remind us what we are doing.

69
00:02:23,70 --> 00:02:27,50
Reset the form values by calling set message text.

70
00:02:27,50 --> 00:02:29,30
We'll set that to an empty string.

71
00:02:29,30 --> 00:02:31,20
And we'll also set the message type

72
00:02:31,20 --> 00:02:33,20
back to the default type.

73
00:02:33,20 --> 00:02:36,20
Ok, so to recap, when we've seen

74
00:02:36,20 --> 00:02:38,60
that we are successful, we capture that ID,

75
00:02:38,60 --> 00:02:40,80
add it on to the data that we added on to

76
00:02:40,80 --> 00:02:43,00
our message data that we have stored locally.

77
00:02:43,00 --> 00:02:45,00
And then we'll call that add status message

78
00:02:45,00 --> 00:02:46,70
function that came through as a prop

79
00:02:46,70 --> 00:02:48,20
and we reset the form.

80
00:02:48,20 --> 00:02:50,70
We've saved, now we can switch to the browser.

81
00:02:50,70 --> 00:02:52,80
Refresh and we'll give this a try.

82
00:02:52,80 --> 00:02:55,10
Once the loading happens, we'll say

83
00:02:55,10 --> 00:03:00,50
here is another message from, this time, pool people.

84
00:03:00,50 --> 00:03:03,20
Post that update and instantly we can see it.

85
00:03:03,20 --> 00:03:04,90
And just to confirm that it really worked,

86
00:03:04,90 --> 00:03:07,30
and it saved to the API as well as putting it

87
00:03:07,30 --> 00:03:09,50
in here, we can refresh the page,

88
00:03:09,50 --> 00:03:11,60
wait a few seconds and there it is.

89
00:03:11,60 --> 00:03:14,10
That concludes our status updater project.

90
00:03:14,10 --> 00:03:15,60
We've looked at the use effect hook,

91
00:03:15,60 --> 00:03:17,80
controlled forms again, lifting state and

92
00:03:17,80 --> 00:03:18,90
working with an API.

93
00:03:18,90 --> 00:03:20,00
Not bad.

