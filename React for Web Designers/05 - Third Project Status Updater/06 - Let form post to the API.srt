1
00:00:00,50 --> 00:00:01,50
- [Narrator] In this video, we're going to

2
00:00:01,50 --> 00:00:03,80
connect the post form to the API,

3
00:00:03,80 --> 00:00:05,00
adding the submit handler that

4
00:00:05,00 --> 00:00:07,20
will post new status messages.

5
00:00:07,20 --> 00:00:09,60
Over here in my post form component,

6
00:00:09,60 --> 00:00:12,80
I'm going to scroll down to the form field.

7
00:00:12,80 --> 00:00:16,30
And we're going to add an on submit handler.

8
00:00:16,30 --> 00:00:18,40
That way, however we submit the form,

9
00:00:18,40 --> 00:00:19,80
our handler will be triggered.

10
00:00:19,80 --> 00:00:22,90
We'll call this post status update.

11
00:00:22,90 --> 00:00:25,10
It's a function we're going to need to define.

12
00:00:25,10 --> 00:00:28,30
And to save having to watch me do a lot of typing here,

13
00:00:28,30 --> 00:00:30,30
I've included a snippet in the exercise files

14
00:00:30,30 --> 00:00:33,80
for this video with all the code that we can copy and paste,

15
00:00:33,80 --> 00:00:36,20
and then we'll talk through what it's doing.

16
00:00:36,20 --> 00:00:37,40
So, we'll paste that in.

17
00:00:37,40 --> 00:00:38,80
So this is an event handler that

18
00:00:38,80 --> 00:00:40,30
takes an event as its argument.

19
00:00:40,30 --> 00:00:41,80
First we prevent the default actions

20
00:00:41,80 --> 00:00:44,40
so the page doesn't reload when we submit the form.

21
00:00:44,40 --> 00:00:47,30
And we create a new status object.

22
00:00:47,30 --> 00:00:51,50
This takes the message text and type from our form state

23
00:00:51,50 --> 00:00:53,60
and saves them to fields called message and type,

24
00:00:53,60 --> 00:00:55,20
which are what the API uses.

25
00:00:55,20 --> 00:00:57,20
And then we are capturing the time

26
00:00:57,20 --> 00:00:58,90
that this message was posted.

27
00:00:58,90 --> 00:01:02,40
And we're using the date and time libraries format method,

28
00:01:02,40 --> 00:01:05,50
passing in the current time by creating a new date object

29
00:01:05,50 --> 00:01:08,80
and then using the format that the API uses here.

30
00:01:08,80 --> 00:01:12,20
The year, month, date and the hours and minutes.

31
00:01:12,20 --> 00:01:13,70
Then we're using the axios library

32
00:01:13,70 --> 00:01:16,00
to post this message to the API,

33
00:01:16,00 --> 00:01:19,10
using our config API URL, but we're going to

34
00:01:19,10 --> 00:01:21,60
post.php this time, instead of get,

35
00:01:21,60 --> 00:01:23,60
passing in our new status object

36
00:01:23,60 --> 00:01:26,50
and we get back a promise so we attach then,

37
00:01:26,50 --> 00:01:28,10
and then we act on the response.

38
00:01:28,10 --> 00:01:30,70
In this case, we're just going to log it to the console,

39
00:01:30,70 --> 00:01:33,00
so we can see what the response looks like.

40
00:01:33,00 --> 00:01:35,20
And then we'll be taking some more action later,

41
00:01:35,20 --> 00:01:36,90
if this actually worked.

42
00:01:36,90 --> 00:01:39,50
For now, we can save this and switch over

43
00:01:39,50 --> 00:01:41,60
to the browser and take a look.

44
00:01:41,60 --> 00:01:43,30
Refresh this.

45
00:01:43,30 --> 00:01:47,00
We'll open up the developer tool, with command + option + I.

46
00:01:47,00 --> 00:01:50,30
I'm going to switch to the network tab.

47
00:01:50,30 --> 00:01:52,30
Different browsers will have different network tools.

48
00:01:52,30 --> 00:01:54,20
So if you're using Chrome, you'll have your

49
00:01:54,20 --> 00:01:56,30
network tab and then you can filter

50
00:01:56,30 --> 00:01:58,80
by the various kinds of network activity.

51
00:01:58,80 --> 00:02:00,60
I'm going to filter this by XHR,

52
00:02:00,60 --> 00:02:03,50
which means XML HTTP Requests,

53
00:02:03,50 --> 00:02:07,00
which is basically Ajax, Fetch, all that sort of thing.

54
00:02:07,00 --> 00:02:09,10
That's what's going to happen when we use axios

55
00:02:09,10 --> 00:02:11,40
to post this message to our API.

56
00:02:11,40 --> 00:02:15,90
So I'll say, here is a message from...

57
00:02:15,90 --> 00:02:19,80
We'll send it from the plumbing department.

58
00:02:19,80 --> 00:02:21,30
And if you click post update,

59
00:02:21,30 --> 00:02:23,90
you can see that we are calling post.php here,

60
00:02:23,90 --> 00:02:26,00
and if I click this in the network pane,

61
00:02:26,00 --> 00:02:27,40
I can see what happened.

62
00:02:27,40 --> 00:02:30,50
I can see my headers and preview the response.

63
00:02:30,50 --> 00:02:31,80
Here is the raw data.

64
00:02:31,80 --> 00:02:34,20
This is a little bit nicer looking version.

65
00:02:34,20 --> 00:02:37,10
So we get back, success is true,

66
00:02:37,10 --> 00:02:40,20
and then the ID for the new message.

67
00:02:40,20 --> 00:02:42,10
So we can see that the form posted the message,

68
00:02:42,10 --> 00:02:44,00
but the page isn't updating.

69
00:02:44,00 --> 00:02:46,90
To see any new messages, we have to refresh the page.

70
00:02:46,90 --> 00:02:47,80
It's not the end of the world,

71
00:02:47,80 --> 00:02:50,20
but not the behavior we want either.

72
00:02:50,20 --> 00:02:51,10
We'll fix that coming up.

73
00:02:51,10 --> 00:02:53,90
Now, I want to note something about the form.

74
00:02:53,90 --> 00:02:55,10
Because the goal of this course is

75
00:02:55,10 --> 00:02:56,90
to understand the basics of react,

76
00:02:56,90 --> 00:02:58,30
we're doing a lot of things by hand,

77
00:02:58,30 --> 00:03:00,20
writing our own event handlers for everything.

78
00:03:00,20 --> 00:03:02,30
Especially on large projects,

79
00:03:02,30 --> 00:03:03,90
you probably want extra help with that.

80
00:03:03,90 --> 00:03:06,40
If you're building even one form with a lot of fields,

81
00:03:06,40 --> 00:03:08,30
or you have a lot of forms.

82
00:03:08,30 --> 00:03:10,40
If you're working on a project with those needs,

83
00:03:10,40 --> 00:03:12,60
I'd recommend chekcing out something like Formik,

84
00:03:12,60 --> 00:03:14,10
which gives you third party components

85
00:03:14,10 --> 00:03:16,60
that are really helpful for building forms.

86
00:03:16,60 --> 00:03:18,50
For our needs, doing everything by hand

87
00:03:18,50 --> 00:03:20,50
on this little form was no big deal.

88
00:03:20,50 --> 00:03:22,40
And indeed, it's now all wired up,

89
00:03:22,40 --> 00:03:25,00
and can post messages to our API.

