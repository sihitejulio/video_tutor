1
00:00:00,60 --> 00:00:01,80
- [Instructor] When you're working with an API

2
00:00:01,80 --> 00:00:03,50
or other external data,

3
00:00:03,50 --> 00:00:06,80
ideally it's going to be super fast and no one will notice.

4
00:00:06,80 --> 00:00:08,90
But in reality, connections can be slow,

5
00:00:08,90 --> 00:00:10,90
so it's a good idea to plan for that.

6
00:00:10,90 --> 00:00:13,50
In this video, we're going to look at conditional rendering,

7
00:00:13,50 --> 00:00:15,60
setting up a loader animation that will display

8
00:00:15,60 --> 00:00:17,90
while our page waits for its live data.

9
00:00:17,90 --> 00:00:19,60
Right now, when we load this page.

10
00:00:19,60 --> 00:00:20,90
I'll refresh it.

11
00:00:20,90 --> 00:00:23,20
And it's pretty much instantaneously showing

12
00:00:23,20 --> 00:00:25,00
the information from our API.

13
00:00:25,00 --> 00:00:28,40
We never even see what's here before these messages appear.

14
00:00:28,40 --> 00:00:29,50
Back in our code,

15
00:00:29,50 --> 00:00:32,90
let's jump down to the StatusMessageList component,

16
00:00:32,90 --> 00:00:34,20
using Command + Shift + O.

17
00:00:34,20 --> 00:00:37,00
StatusMessageList, here we go.

18
00:00:37,00 --> 00:00:38,00
Right now, the way this works is

19
00:00:38,00 --> 00:00:39,50
we have the static data,

20
00:00:39,50 --> 00:00:41,10
here in stubStatuses,

21
00:00:41,10 --> 00:00:43,00
which is used first.

22
00:00:43,00 --> 00:00:44,20
And then after the component renders,

23
00:00:44,20 --> 00:00:46,00
we retrieve our status messages.

24
00:00:46,00 --> 00:00:47,90
Let's get rid of these stubStatuses.

25
00:00:47,90 --> 00:00:48,90
We're going to stop using them

26
00:00:48,90 --> 00:00:52,50
and just use the data from the API.

27
00:00:52,50 --> 00:00:54,90
So we'll start this off as an empty array.

28
00:00:54,90 --> 00:00:57,00
Next, to make sure we can see

29
00:00:57,00 --> 00:00:59,00
when something is not happening,

30
00:00:59,00 --> 00:01:01,00
we're going to add our little delay parameter

31
00:01:01,00 --> 00:01:02,50
onto our call to the API.

32
00:01:02,50 --> 00:01:04,70
Let's set a five-second delay.

33
00:01:04,70 --> 00:01:06,20
So we can save this now.

34
00:01:06,20 --> 00:01:07,50
Go back and reload to make sure

35
00:01:07,50 --> 00:01:09,50
that everything's working so far.

36
00:01:09,50 --> 00:01:11,10
I reload.

37
00:01:11,10 --> 00:01:14,60
We see nothing at all for a few seconds.

38
00:01:14,60 --> 00:01:16,50
And then the messages come through.

39
00:01:16,50 --> 00:01:18,60
So this is not the greatest user experience.

40
00:01:18,60 --> 00:01:20,70
We want to show that something is happening

41
00:01:20,70 --> 00:01:22,20
while we're waiting.

42
00:01:22,20 --> 00:01:23,80
Switching back to the code,

43
00:01:23,80 --> 00:01:25,60
what we'll do is we'll set up a piece of state

44
00:01:25,60 --> 00:01:28,70
that we can look at to know when our data have loaded.

45
00:01:28,70 --> 00:01:30,60
So I'll copy this used state hook,

46
00:01:30,60 --> 00:01:33,70
and we'll add a new piece of state called loaded,

47
00:01:33,70 --> 00:01:36,60
which can be updated with setLoaded.

48
00:01:36,60 --> 00:01:39,90
And we'll start it out with the value of false,

49
00:01:39,90 --> 00:01:41,20
the idea being that

50
00:01:41,20 --> 00:01:43,60
after we've successfully loaded the data from the API,

51
00:01:43,60 --> 00:01:45,80
we'll set this to true.

52
00:01:45,80 --> 00:01:47,30
In fact, let's do that now.

53
00:01:47,30 --> 00:01:48,70
After we get our data,

54
00:01:48,70 --> 00:01:51,20
we'll set the statuses to the response.data,

55
00:01:51,20 --> 00:01:53,80
and then we'll set loaded to true.

56
00:01:53,80 --> 00:01:55,10
So we've done what we needed to do

57
00:01:55,10 --> 00:01:57,80
as far as setting our flag goes,

58
00:01:57,80 --> 00:02:00,10
but we're not making any changes on the page yet.

59
00:02:00,10 --> 00:02:02,50
We want to add a little loading animation.

60
00:02:02,50 --> 00:02:04,90
There's a nice little collection of these called SpinKit.

61
00:02:04,90 --> 00:02:06,10
I grabbed this one,

62
00:02:06,10 --> 00:02:10,10
which includes all of this CSS and HTML,

63
00:02:10,10 --> 00:02:11,30
just an nice little snippet

64
00:02:11,30 --> 00:02:13,50
for rendering an animation like this.

65
00:02:13,50 --> 00:02:15,20
The CSS is already preloaded

66
00:02:15,20 --> 00:02:17,80
in the CSS that loads with this project.

67
00:02:17,80 --> 00:02:21,10
But the snippet of HTML is in loading-progress.js,

68
00:02:21,10 --> 00:02:23,90
with the exercise files for this video.

69
00:02:23,90 --> 00:02:25,60
So let's collect this snippet.

70
00:02:25,60 --> 00:02:26,60
Just copy it.

71
00:02:26,60 --> 00:02:31,40
And bring it over to our hotel.js file.

72
00:02:31,40 --> 00:02:33,50
And we're going to use this when we render the component

73
00:02:33,50 --> 00:02:36,00
down here in our return statement.

74
00:02:36,00 --> 00:02:39,90
So what we'll be doing is checking if the data have loaded.

75
00:02:39,90 --> 00:02:41,20
And if the data have loaded,

76
00:02:41,20 --> 00:02:43,20
all we need to do is what we were already doing,

77
00:02:43,20 --> 00:02:45,30
display those status messages.

78
00:02:45,30 --> 00:02:46,90
And if the data have not loaded yet,

79
00:02:46,90 --> 00:02:48,70
we'll return our snippet.

80
00:02:48,70 --> 00:02:50,80
Just paste that right in here.

81
00:02:50,80 --> 00:02:52,40
So again, in our side effect,

82
00:02:52,40 --> 00:02:54,30
we're retrieving the status messages.

83
00:02:54,30 --> 00:02:55,90
We collect our data.

84
00:02:55,90 --> 00:02:57,70
Set the status messages to the resulting data

85
00:02:57,70 --> 00:02:59,30
that come back from the API.

86
00:02:59,30 --> 00:03:00,80
And set loaded to true.

87
00:03:00,80 --> 00:03:02,70
When that is done, the component will re-render,

88
00:03:02,70 --> 00:03:04,50
and it'll show the status messages

89
00:03:04,50 --> 00:03:06,00
instead of our loading animation.

90
00:03:06,00 --> 00:03:07,80
So let's save this,

91
00:03:07,80 --> 00:03:09,90
and we'll switch back to the browser and try it out.

92
00:03:09,90 --> 00:03:11,10
Here we go.

93
00:03:11,10 --> 00:03:12,20
We have a nice loading animation

94
00:03:12,20 --> 00:03:13,80
that will show for a little while.

95
00:03:13,80 --> 00:03:16,30
And then once everything is loaded,

96
00:03:16,30 --> 00:03:17,70
we get the messages.

97
00:03:17,70 --> 00:03:19,70
So that is conditional rendering.

98
00:03:19,70 --> 00:03:22,50
Let's do one more little piece of cleanup before we move on.

99
00:03:22,50 --> 00:03:25,10
Currently, we have our URL hard-coded here.

100
00:03:25,10 --> 00:03:26,30
Let's get rid of that

101
00:03:26,30 --> 00:03:28,00
so we don't have this URL

102
00:03:28,00 --> 00:03:31,00
just kind of sitting randomly in some component.

103
00:03:31,00 --> 00:03:32,50
Let's create a configuration object

104
00:03:32,50 --> 00:03:34,10
up at the top of the file.

105
00:03:34,10 --> 00:03:36,20
Call it CONFIG.

106
00:03:36,20 --> 00:03:39,90
And we'll have apiUrl as a property in here.

107
00:03:39,90 --> 00:03:42,60
Now paste in that whole URL.

108
00:03:42,60 --> 00:03:44,50
We're going to trim off everything

109
00:03:44,50 --> 00:03:46,00
from get.php on,

110
00:03:46,00 --> 00:03:49,70
because we may use this for other purposes.

111
00:03:49,70 --> 00:03:53,10
Now, if we scroll down to our StatusMessageList component,

112
00:03:53,10 --> 00:03:57,40
we can use this here, CONFIG.apiUrl.

113
00:03:57,40 --> 00:04:01,50
Now paste in the get.php and the delay.

114
00:04:01,50 --> 00:04:02,80
Now let's just make sure we don't have this

115
00:04:02,80 --> 00:04:04,90
somewhere else on the page.

116
00:04:04,90 --> 00:04:07,60
I'll search for status_ with Command + F.

117
00:04:07,60 --> 00:04:10,20
Yeah, and we have the apiUrl down here

118
00:04:10,20 --> 00:04:13,40
in the StatusMessageManager component.

119
00:04:13,40 --> 00:04:14,90
It's not really being passed around anywhere,

120
00:04:14,90 --> 00:04:16,50
so we don't need to have it in the component.

121
00:04:16,50 --> 00:04:18,30
I guess the configuration object is

122
00:04:18,30 --> 00:04:20,50
a nice solution, I think.

123
00:04:20,50 --> 00:04:23,90
So we will get rid of that and save.

124
00:04:23,90 --> 00:04:26,70
Reload, and after a few seconds,

125
00:04:26,70 --> 00:04:28,00
everything comes back.

126
00:04:28,00 --> 00:04:28,80
Great.

127
00:04:28,80 --> 00:04:30,20
So everything is still working.

128
00:04:30,20 --> 00:04:32,70
With these changes, our app now acts more gracefully,

129
00:04:32,70 --> 00:04:34,10
offering some feedback to users

130
00:04:34,10 --> 00:04:36,00
while waiting for the data to load.

