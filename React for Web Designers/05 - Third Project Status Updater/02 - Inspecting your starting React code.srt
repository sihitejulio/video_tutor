1
00:00:00,60 --> 00:00:01,50
- [Instructor] We've gone through the steps

2
00:00:01,50 --> 00:00:03,50
of converting a mock-up into React components

3
00:00:03,50 --> 00:00:04,90
a couple times already.

4
00:00:04,90 --> 00:00:06,30
So this time, we're going to start with a fair

5
00:00:06,30 --> 00:00:07,90
amount of code already written

6
00:00:07,90 --> 00:00:09,80
so we can skip ahead to some of the newer aspects

7
00:00:09,80 --> 00:00:11,50
that we haven't seen before.

8
00:00:11,50 --> 00:00:13,60
Let's take a little tour of what we have.

9
00:00:13,60 --> 00:00:15,30
First of all, I want to make sure you've dropped

10
00:00:15,30 --> 00:00:18,30
your Work in Progress folder into PrePros.

11
00:00:18,30 --> 00:00:20,30
It should probably look something like this.

12
00:00:20,30 --> 00:00:21,90
And you'll just want to check the configuration

13
00:00:21,90 --> 00:00:24,40
of the hotel.js file to make sure

14
00:00:24,40 --> 00:00:27,10
it's auto-compiling, with source maps if you want them,

15
00:00:27,10 --> 00:00:28,70
and Babel, of course.

16
00:00:28,70 --> 00:00:31,50
And make sure that the hotel-dist file is not

17
00:00:31,50 --> 00:00:33,50
having anything happen to it because we don't

18
00:00:33,50 --> 00:00:35,70
need a second reprocessed version of this

19
00:00:35,70 --> 00:00:38,70
since hotel.js will be creating it for us.

20
00:00:38,70 --> 00:00:40,80
Switching over to my editor,

21
00:00:40,80 --> 00:00:43,00
I have the index.html file open.

22
00:00:43,00 --> 00:00:47,00
Let's scroll down all the way to my script tags.

23
00:00:47,00 --> 00:00:48,90
Of course I have my React tags down here

24
00:00:48,90 --> 00:00:52,00
and my hotel-dist JavaScript file.

25
00:00:52,00 --> 00:00:53,50
So I'm loading the processed version

26
00:00:53,50 --> 00:00:56,30
of my hotel.js file.

27
00:00:56,30 --> 00:00:58,10
But if I scroll up a little bit,

28
00:00:58,10 --> 00:00:59,00
I have my polyfils for react,

29
00:00:59,00 --> 00:01:00,40
just as we've seen before.

30
00:01:00,40 --> 00:01:02,20
But I have a couple of other libraries in here

31
00:01:02,20 --> 00:01:04,00
to help with this example.

32
00:01:04,00 --> 00:01:05,50
I have date and time,

33
00:01:05,50 --> 00:01:09,10
which is exposed globally as window.date.

34
00:01:09,10 --> 00:01:10,70
This is a small library that makes working

35
00:01:10,70 --> 00:01:12,90
with JavaScript dates much easier.

36
00:01:12,90 --> 00:01:16,00
Next I have another library called axios.

37
00:01:16,00 --> 00:01:19,40
If you've ever worked with asynchronous data fetching,

38
00:01:19,40 --> 00:01:20,80
or data posting in JavaScript,

39
00:01:20,80 --> 00:01:22,00
you've probably heard of this library.

40
00:01:22,00 --> 00:01:23,20
It's quite popular.

41
00:01:23,20 --> 00:01:26,10
It's exposed as window.axios.

42
00:01:26,10 --> 00:01:29,50
And for both of these, I have the online version

43
00:01:29,50 --> 00:01:31,50
and the offline fall-back.

44
00:01:31,50 --> 00:01:34,00
And then finally, if I scroll up just a smidge,

45
00:01:34,00 --> 00:01:36,40
here on line 50 I have react-statusmanager

46
00:01:36,40 --> 00:01:39,90
as our root div where everything is going to go.

47
00:01:39,90 --> 00:01:42,30
Now hotel.js is our main JavaScript file.

48
00:01:42,30 --> 00:01:44,70
So let's take a little look at what we have here.

49
00:01:44,70 --> 00:01:46,70
Scrolling all the way down, I have my main

50
00:01:46,70 --> 00:01:49,80
wrapper component here called StatusMessageManager.

51
00:01:49,80 --> 00:01:51,70
And I have my types messages which are used

52
00:01:51,70 --> 00:01:54,20
to populate that post form.

53
00:01:54,20 --> 00:01:55,40
These are just static data.

54
00:01:55,40 --> 00:01:56,80
They're not expected to change.

55
00:01:56,80 --> 00:02:00,00
So I've just made them a variable here in this function.

56
00:02:00,00 --> 00:02:02,80
I also have an API url for where I'm getting

57
00:02:02,80 --> 00:02:04,20
my API stuff.

58
00:02:04,20 --> 00:02:06,30
And then down here is where I'm returning

59
00:02:06,30 --> 00:02:07,70
the actual components.

60
00:02:07,70 --> 00:02:10,70
And I'm using React.Fragment here.

61
00:02:10,70 --> 00:02:13,20
Fragments are a way to avoid having to wrap

62
00:02:13,20 --> 00:02:16,70
all your jsx elements inside wrapper divs.

63
00:02:16,70 --> 00:02:18,40
If we refer to the mockup, which I can open

64
00:02:18,40 --> 00:02:21,30
by using command + p and starting to type mockup,

65
00:02:21,30 --> 00:02:23,50
I get all kinds of versions of it here,

66
00:02:23,50 --> 00:02:25,90
but the one we want is in the Work in Progress folder.

67
00:02:25,90 --> 00:02:27,60
I scroll down here.

68
00:02:27,60 --> 00:02:29,70
I collapse these down.

69
00:02:29,70 --> 00:02:31,00
We can see that these two items

70
00:02:31,00 --> 00:02:33,20
are side by side here,

71
00:02:33,20 --> 00:02:37,40
post-status and my statusmessage list, like this.

72
00:02:37,40 --> 00:02:39,50
If I don't want to add another div,

73
00:02:39,50 --> 00:02:41,80
I can use react.fragment,

74
00:02:41,80 --> 00:02:43,70
which will make this valid jsx,

75
00:02:43,70 --> 00:02:47,10
but won't cause another div to be added to my page.

76
00:02:47,10 --> 00:02:49,40
But if you're very careful about the html

77
00:02:49,40 --> 00:02:51,20
that you're putting out into your pages,

78
00:02:51,20 --> 00:02:53,10
this could be a very attractive little element

79
00:02:53,10 --> 00:02:55,30
to add to your toolbox.

80
00:02:55,30 --> 00:02:57,70
Okay, now we'll scroll up to the form.

81
00:02:57,70 --> 00:03:00,00
Here it is, my PostForm component.

82
00:03:00,00 --> 00:03:02,30
I have this snippet, which takes those message types,

83
00:03:02,30 --> 00:03:05,00
which are an object, and does a little bit

84
00:03:05,00 --> 00:03:08,50
of trickery to convert that object into an array

85
00:03:08,50 --> 00:03:10,20
that I can then map over.

86
00:03:10,20 --> 00:03:12,30
Specifically, I'm grabbing all the keys.

87
00:03:12,30 --> 00:03:14,10
And then I'm using the has own property method

88
00:03:14,10 --> 00:03:16,60
to make sure that this key is not some key

89
00:03:16,60 --> 00:03:19,50
that's come from somewhere up the prototypal chain,

90
00:03:19,50 --> 00:03:21,50
just to make sure I'm only iterating over

91
00:03:21,50 --> 00:03:23,40
the message types themselves.

92
00:03:23,40 --> 00:03:25,40
And then I'm creating all my option elements

93
00:03:25,40 --> 00:03:27,90
that go with those message types.

94
00:03:27,90 --> 00:03:30,00
And I'm setting a key and a value,

95
00:03:30,00 --> 00:03:32,40
and then I have my label right here.

96
00:03:32,40 --> 00:03:35,60
I've also saved the default type of message,

97
00:03:35,60 --> 00:03:37,20
just so I don't have to type this over and over again,

98
00:03:37,20 --> 00:03:38,70
just in case we need it later.

99
00:03:38,70 --> 00:03:40,40
And then here's the rendered form

100
00:03:40,40 --> 00:03:42,10
with all of these elements.

101
00:03:42,10 --> 00:03:44,10
And here's the type options that are rendered

102
00:03:44,10 --> 00:03:46,70
by this array map function.

103
00:03:46,70 --> 00:03:49,80
Then let's look at our status message list down here.

104
00:03:49,80 --> 00:03:52,30
I have some static data,

105
00:03:52,30 --> 00:03:54,30
since I'm not calling out to my API yet,

106
00:03:54,30 --> 00:03:55,70
but this can show you the structure of what

107
00:03:55,70 --> 00:03:59,00
we're expecting here with each status having an ID.

108
00:03:59,00 --> 00:04:02,30
The message, a type, and a time stamp.

109
00:04:02,30 --> 00:04:04,50
I'm using the useState hook to capture

110
00:04:04,50 --> 00:04:05,90
all those as state.

111
00:04:05,90 --> 00:04:07,70
Of course, when we'll be retrieving these

112
00:04:07,70 --> 00:04:10,40
from the API later, that state will be updating.

113
00:04:10,40 --> 00:04:13,60
And then I have a function that is rendering

114
00:04:13,60 --> 00:04:14,80
all of those status messages,

115
00:04:14,80 --> 00:04:16,70
again using array.map.

116
00:04:16,70 --> 00:04:18,70
And of course, my key is the ID

117
00:04:18,70 --> 00:04:20,80
that each status message has.

118
00:04:20,80 --> 00:04:23,20
And it's rendering a status message component,

119
00:04:23,20 --> 00:04:25,80
passing in all of these data as props.

120
00:04:25,80 --> 00:04:26,70
So let's look at that.

121
00:04:26,70 --> 00:04:29,60
I will command + click that component

122
00:04:29,60 --> 00:04:30,90
to take us up there.

123
00:04:30,90 --> 00:04:32,20
The main thing I want to point out here

124
00:04:32,20 --> 00:04:34,20
is that I'm using that date and time library

125
00:04:34,20 --> 00:04:36,10
to parse the time stamp

126
00:04:36,10 --> 00:04:37,60
into a JavaScript data object

127
00:04:37,60 --> 00:04:40,40
that I can then reformat however I want.

128
00:04:40,40 --> 00:04:42,80
So in this case, my time stamps all look like this

129
00:04:42,80 --> 00:04:45,60
in the database, but I'd like them to look more like this,

130
00:04:45,60 --> 00:04:49,70
month, date, year, the hour, the minute, and a.m., p.m.

131
00:04:49,70 --> 00:04:52,40
So I'm using dates format function

132
00:04:52,40 --> 00:04:55,00
to do that for me, right here.

133
00:04:55,00 --> 00:04:56,60
And I'm displaying the type of message

134
00:04:56,60 --> 00:04:58,00
and the message itself.

135
00:04:58,00 --> 00:04:59,40
And this is in the same kind of markup

136
00:04:59,40 --> 00:05:01,10
that I used in the mockup.

137
00:05:01,10 --> 00:05:02,20
So that's a quick overview of

138
00:05:02,20 --> 00:05:03,90
this project's starting point,

139
00:05:03,90 --> 00:05:06,40
which should all be pretty conceptually familiar.

140
00:05:06,40 --> 00:05:09,00
Now let's go off into some new territory.

