1
00:00:00,50 --> 00:00:02,20
- Over the course of the next several videos,

2
00:00:02,20 --> 00:00:04,30
we are going to start creating a little status update app

3
00:00:04,30 --> 00:00:06,70
for a hotel website- Landon Hotel.

4
00:00:06,70 --> 00:00:08,90
We're going to interface with a small

5
00:00:08,90 --> 00:00:11,50
and simple API to retrieve messages and post new ones,

6
00:00:11,50 --> 00:00:13,30
and we're going to delve a little deeper

7
00:00:13,30 --> 00:00:14,90
into React's function components

8
00:00:14,90 --> 00:00:17,20
to see more of what's possible.

9
00:00:17,20 --> 00:00:19,60
So we'll take a look at this mock-up first.

10
00:00:19,60 --> 00:00:21,80
I've loaded the mock-up file here in my browser,

11
00:00:21,80 --> 00:00:22,90
and if I scroll down,

12
00:00:22,90 --> 00:00:27,80
you can click this little button to do that,

13
00:00:27,80 --> 00:00:30,90
I'll go straight to the status updates section.

14
00:00:30,90 --> 00:00:32,90
This is a mock-up of what we'll be building.

15
00:00:32,90 --> 00:00:34,30
We have over here a little form

16
00:00:34,30 --> 00:00:35,60
that's going to post status updates

17
00:00:35,60 --> 00:00:37,50
from various parts of the hotel

18
00:00:37,50 --> 00:00:41,70
with who it's from and the date and time.

19
00:00:41,70 --> 00:00:43,00
So let's look at this and think about

20
00:00:43,00 --> 00:00:45,00
the components that we're going to need.

21
00:00:45,00 --> 00:00:48,10
We'll have one big component that wraps everything,

22
00:00:48,10 --> 00:00:50,60
we'll call that the status message manager,

23
00:00:50,60 --> 00:00:53,50
and then we'll have our post form over here,

24
00:00:53,50 --> 00:00:55,30
post form component.

25
00:00:55,30 --> 00:00:57,70
And we can see that this has some form elements,

26
00:00:57,70 --> 00:00:59,50
so we'll make those controlled form elements

27
00:00:59,50 --> 00:01:01,40
to stick with the recommendations of React

28
00:01:01,40 --> 00:01:04,30
and in doing so we know that this component

29
00:01:04,30 --> 00:01:06,20
is going to track its own state

30
00:01:06,20 --> 00:01:08,90
to keep track of the contents of those elements.

31
00:01:08,90 --> 00:01:11,70
Then over here we'll have the status message list.

32
00:01:11,70 --> 00:01:14,10
Just a list of all the status messages we have available,

33
00:01:14,10 --> 00:01:15,80
and each one of these will be a component

34
00:01:15,80 --> 00:01:18,00
called a status message.

35
00:01:18,00 --> 00:01:19,50
So that's part of the planning we need to do,

36
00:01:19,50 --> 00:01:20,80
just thinking about the components

37
00:01:20,80 --> 00:01:22,80
and how we're going to deal with them.

38
00:01:22,80 --> 00:01:23,90
Now we need the API,

39
00:01:23,90 --> 00:01:26,90
so let's get that installed real quick.

40
00:01:26,90 --> 00:01:28,60
In the exercise files for this course,

41
00:01:28,60 --> 00:01:31,10
there's a folder called status_api,

42
00:01:31,10 --> 00:01:35,30
which has just a few PHP files and a MySQL dump.

43
00:01:35,30 --> 00:01:37,10
This is going to serve as the little API

44
00:01:37,10 --> 00:01:39,60
for reading and writing status messages

45
00:01:39,60 --> 00:01:42,30
that will be displayed on that page.

46
00:01:42,30 --> 00:01:44,30
We already have a local web server installed

47
00:01:44,30 --> 00:01:48,20
from previous examples, so we're just going to use that now.

48
00:01:48,20 --> 00:01:51,20
All we need to do is update a configuration file

49
00:01:51,20 --> 00:01:53,10
and load the database.

50
00:01:53,10 --> 00:01:55,40
So first let's load the database.

51
00:01:55,40 --> 00:01:57,10
Almost any local web server is going to have

52
00:01:57,10 --> 00:02:00,40
a copy of phpMyAdmin or some relatively easy way

53
00:02:00,40 --> 00:02:03,40
to import MySQL data files.

54
00:02:03,40 --> 00:02:05,00
We'll use phpMyAdmin here,

55
00:02:05,00 --> 00:02:07,90
and we're going to go to the import tab

56
00:02:07,90 --> 00:02:11,20
and I'm going to choose a file,

57
00:02:11,20 --> 00:02:16,00
specifically, I'm going to open this database.sql file.

58
00:02:16,00 --> 00:02:18,70
Scroll down and click go,

59
00:02:18,70 --> 00:02:19,90
and it runs a bunch of queries

60
00:02:19,90 --> 00:02:23,20
and creates my database for me.

61
00:02:23,20 --> 00:02:25,50
Switching over to my database's tab,

62
00:02:25,50 --> 00:02:28,90
it's going to create a database called status_api.

63
00:02:28,90 --> 00:02:29,80
So word to the wise,

64
00:02:29,80 --> 00:02:32,40
if you happen to have a database already called this

65
00:02:32,40 --> 00:02:34,60
on your file system, you'll probably want

66
00:02:34,60 --> 00:02:37,90
to open that database schema file and change the name of it,

67
00:02:37,90 --> 00:02:41,00
otherwise your existing data might get wiped out.

68
00:02:41,00 --> 00:02:44,80
So we have this in here, let's take a look inside.

69
00:02:44,80 --> 00:02:46,80
It has a table called statuses,

70
00:02:46,80 --> 00:02:48,90
and I've seeded this database with a few messages

71
00:02:48,90 --> 00:02:51,60
just so we have something to work with right away.

72
00:02:51,60 --> 00:02:52,50
Okay, that's the database,

73
00:02:52,50 --> 00:02:55,20
now let's update the configuration file.

74
00:02:55,20 --> 00:02:59,00
Here in my editor, I've opened the config_setup file,

75
00:02:59,00 --> 00:03:02,00
and you might not need to make any changes here at all,

76
00:03:02,00 --> 00:03:03,80
but if you know that your connection details

77
00:03:03,80 --> 00:03:05,30
for your particular local web server

78
00:03:05,30 --> 00:03:07,50
are different from the ones here,

79
00:03:07,50 --> 00:03:08,90
you can update them accordingly.

80
00:03:08,90 --> 00:03:11,60
This is the host, usually local host.

81
00:03:11,60 --> 00:03:14,50
The username and password for most mySQL installations

82
00:03:14,50 --> 00:03:16,90
with local web servers are either root and root,

83
00:03:16,90 --> 00:03:18,90
or root and maybe a blank password.

84
00:03:18,90 --> 00:03:21,30
And here's the name of the database.

85
00:03:21,30 --> 00:03:24,60
These should be the only pieces that you need to update.

86
00:03:24,60 --> 00:03:25,40
Once that's all done,

87
00:03:25,40 --> 00:03:29,90
you can try loading this up in your server.

88
00:03:29,90 --> 00:03:35,10
I'm going to copy this URL and open a new tab .

89
00:03:35,10 --> 00:03:37,20
In my case, everything is installed here

90
00:03:37,20 --> 00:03:41,20
in localhost/reactjs and then what will be coming next

91
00:03:41,20 --> 00:03:46,60
is status_api and we'll load get.php.

92
00:03:46,60 --> 00:03:47,50
If everything is working,

93
00:03:47,50 --> 00:03:50,20
you'll see something like this in your browser.

94
00:03:50,20 --> 00:03:51,10
This is JSON data,

95
00:03:51,10 --> 00:03:52,70
so depending on which browser you're using

96
00:03:52,70 --> 00:03:55,50
it might apply a little bit of formatting,

97
00:03:55,50 --> 00:03:58,80
but the idea is you're getting back some data.

98
00:03:58,80 --> 00:03:59,90
One other thing I want to mention

99
00:03:59,90 --> 00:04:01,30
that we're going to see later

100
00:04:01,30 --> 00:04:04,10
is that you can also attack a parameter called delay

101
00:04:04,10 --> 00:04:07,70
on the end to insert a little bit of delay,

102
00:04:07,70 --> 00:04:09,60
to pretend that we have some network latency.

103
00:04:09,60 --> 00:04:11,40
This can be useful for making sure that your code

104
00:04:11,40 --> 00:04:15,00
handles a little bit of latency in the network gracefully.

105
00:04:15,00 --> 00:04:16,20
So if I hit this again you can see

106
00:04:16,20 --> 00:04:19,90
this spins for a few seconds

107
00:04:19,90 --> 00:04:22,90
and then finishes loading, specifically 5 seconds.

108
00:04:22,90 --> 00:04:24,80
So you can change this to whatever number you want,

109
00:04:24,80 --> 00:04:27,90
and that will be the delay before it responds.

110
00:04:27,90 --> 00:04:30,00
Okay, so at this point we've seen the project

111
00:04:30,00 --> 00:04:30,90
that we're going to build.

112
00:04:30,90 --> 00:04:33,00
A static mock-up for it anyway.

113
00:04:33,00 --> 00:04:34,70
We've installed our little API.

114
00:04:34,70 --> 00:04:40,00
And now we can get started writing some JavaScript.

