1
00:00:00,50 --> 00:00:03,50
- [Instructor] So let's go ahead and display

2
00:00:03,50 --> 00:00:06,40
our current ascending or descending order

3
00:00:06,40 --> 00:00:09,40
and order by in this drop down right here.

4
00:00:09,40 --> 00:00:12,60
Now to do that you'll need to add an active class

5
00:00:12,60 --> 00:00:17,00
to any element that has the correct value.

6
00:00:17,00 --> 00:00:20,90
So we're going to start the other way here from app.js

7
00:00:20,90 --> 00:00:22,70
and search appointments.

8
00:00:22,70 --> 00:00:27,20
We need to go ahead and add the order by prop.

9
00:00:27,20 --> 00:00:29,40
And so this will be matching whatever the state

10
00:00:29,40 --> 00:00:33,00
of the current application is will be for order by.

11
00:00:33,00 --> 00:00:43,30
And then we'll do the order direction in the same way.

12
00:00:43,30 --> 00:00:46,30
So this'll pass along these two variables

13
00:00:46,30 --> 00:00:50,80
into our subcomponent in search appointment.js.

14
00:00:50,80 --> 00:00:54,40
We will need to capture that and modify the classes

15
00:00:54,40 --> 00:00:57,30
dynamically based on those values.

16
00:00:57,30 --> 00:01:02,60
So we're going to start obviously right here on this pet name.

17
00:01:02,60 --> 00:01:05,10
And what I'm going to end up doing

18
00:01:05,10 --> 00:01:08,10
let's go ahead and make this bigger

19
00:01:08,10 --> 00:01:11,40
is just modifying that how this class name works

20
00:01:11,40 --> 00:01:14,60
so that it uses an expression.

21
00:01:14,60 --> 00:01:17,60
And I like to do single quotes

22
00:01:17,60 --> 00:01:22,40
when I'm doing Javascript or Javascript-y stuff.

23
00:01:22,40 --> 00:01:25,70
So I'll say sort by drop down item plus

24
00:01:25,70 --> 00:01:28,50
and I want to make sure I add a space here.

25
00:01:28,50 --> 00:01:35,70
And then I will say this props order by.

26
00:01:35,70 --> 00:01:38,00
So I'm going to use that order by variable.

27
00:01:38,00 --> 00:01:43,40
So if that variable is equal to pet name in this case.

28
00:01:43,40 --> 00:01:48,00
Then I'm going to add a class of active.

29
00:01:48,00 --> 00:01:51,80
Otherwise I'll just not add anything.

30
00:01:51,80 --> 00:01:54,40
And so we'll need to do this for all of them.

31
00:01:54,40 --> 00:01:57,70
I can copy this one, I just have to be really careful

32
00:01:57,70 --> 00:02:01,10
that I don't screw up the variable names here.

33
00:02:01,10 --> 00:02:02,80
So I'm going to copy this class name here

34
00:02:02,80 --> 00:02:08,60
and I'll paste it instead of this one.

35
00:02:08,60 --> 00:02:13,70
And let me go ahead and do that for all of them.

36
00:02:13,70 --> 00:02:17,30
I'm just going to have to be careful when replacing everything.

37
00:02:17,30 --> 00:02:21,20
So the first one is pet name, second one

38
00:02:21,20 --> 00:02:30,10
I know that the name of this field is appointment date.

39
00:02:30,10 --> 00:02:35,40
This one is going to be owner name.

40
00:02:35,40 --> 00:02:39,00
And then on these I'm actually looking for

41
00:02:39,00 --> 00:02:43,40
the order direction variable.

42
00:02:43,40 --> 00:02:45,40
And this will be ascending.

43
00:02:45,40 --> 00:02:47,60
And this one is descending.

44
00:02:47,60 --> 00:02:49,90
So I'll save that.

45
00:02:49,90 --> 00:02:53,00
And take a look at our little drop down.

46
00:02:53,00 --> 00:02:56,20
So now they are matching whatever the value

47
00:02:56,20 --> 00:03:00,50
in the app.js file, whatever the value of state is

48
00:03:00,50 --> 00:03:02,40
my drop down should match it.

49
00:03:02,40 --> 00:03:08,50
Let's go ahead and modify these owner name.

50
00:03:08,50 --> 00:03:10,70
So now it says owner here.

51
00:03:10,70 --> 00:03:16,80
And let's try appointment date.

52
00:03:16,80 --> 00:03:19,00
And these should also be sorting properly as well.

53
00:03:19,00 --> 00:03:20,40
So there's the date.

54
00:03:20,40 --> 00:03:25,30
And let's change this to descending.

55
00:03:25,30 --> 00:03:28,90
And you can see my drop downs are all working properly.

56
00:03:28,90 --> 00:03:33,40
So let's just reset this back to pet name

57
00:03:33,40 --> 00:03:37,60
and ascending just to keep it as the default.

58
00:03:37,60 --> 00:03:38,50
And that's all there is to it.

59
00:03:38,50 --> 00:03:41,80
You just have to use the conditional classes

60
00:03:41,80 --> 00:03:44,60
with expressions how we've been doing before

61
00:03:44,60 --> 00:03:49,20
but just make them match the value in our app.js.

62
00:03:49,20 --> 00:03:51,70
It's pretty cool now we can modify anything

63
00:03:51,70 --> 00:03:56,40
in our main component and when something in our state

64
00:03:56,40 --> 00:04:00,30
gets modified it will automatically redraw the list

65
00:04:00,30 --> 00:04:01,60
in the correct order.

66
00:04:01,60 --> 00:04:06,40
And our search component will display the proper value

67
00:04:06,40 --> 00:04:08,70
at least for right now in the drop down.

68
00:04:08,70 --> 00:04:10,00
That's pretty awesome.

