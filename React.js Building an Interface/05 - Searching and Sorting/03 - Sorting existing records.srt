1
00:00:00,50 --> 00:00:01,60
- [Instructor] So let's see if we can make

2
00:00:01,60 --> 00:00:05,20
these drop down items control how things are sorted.

3
00:00:05,20 --> 00:00:07,40
So to do that we'll go the other way around,

4
00:00:07,40 --> 00:00:10,70
we'll start with search appointments

5
00:00:10,70 --> 00:00:14,70
and let's make this bigger so we can see more things.

6
00:00:14,70 --> 00:00:18,60
What I want to do is go to the top of this

7
00:00:18,60 --> 00:00:24,00
and I'm going to add an event handler here.

8
00:00:24,00 --> 00:00:26,90
So this will be onClick when somebody clicks

9
00:00:26,90 --> 00:00:31,10
on one of these items, then what I'll do

10
00:00:31,10 --> 00:00:35,50
is execute an expression, that expression is going

11
00:00:35,50 --> 00:00:39,40
to trigger an event and so when that event happens.

12
00:00:39,40 --> 00:00:44,50
What we'll do is we will use the props to execute

13
00:00:44,50 --> 00:00:49,10
a function in the app.js file called changeOrder.

14
00:00:49,10 --> 00:00:50,60
We'll have to make that one,

15
00:00:50,60 --> 00:00:52,60
and we'll pass it along two things.

16
00:00:52,60 --> 00:00:55,00
In this case what we're going to order by

17
00:00:55,00 --> 00:00:58,60
so in this first one it's going to be actually

18
00:00:58,60 --> 00:01:02,70
looks like I'm on date so this will actually be

19
00:01:02,70 --> 00:01:07,80
aptDate here and then we can pass along how we want

20
00:01:07,80 --> 00:01:10,90
it to sort so I'll just use the props

21
00:01:10,90 --> 00:01:15,50
that we're already getting for the orderedDir here

22
00:01:15,50 --> 00:01:16,60
and that should work.

23
00:01:16,60 --> 00:01:18,70
So I need to do that everywhere,

24
00:01:18,70 --> 00:01:23,20
modifying each one of them for the value I need to pass.

25
00:01:23,20 --> 00:01:30,80
So this one over here is petName, then aptDate,

26
00:01:30,80 --> 00:01:37,40
and this one's going to be ownerName,

27
00:01:37,40 --> 00:01:40,90
and then down here we'll do something pretty similar

28
00:01:40,90 --> 00:01:42,80
so I'm just going to paste that same one

29
00:01:42,80 --> 00:01:47,60
and instead of passing of course the order by,

30
00:01:47,60 --> 00:01:50,00
we'll use the props once again.

31
00:01:50,00 --> 00:01:52,30
We're already getting those props

32
00:01:52,30 --> 00:01:56,60
and look for orderBy here and then pass along

33
00:01:56,60 --> 00:02:04,80
either ascending, or in the case of descending here.

34
00:02:04,80 --> 00:02:09,10
Oops I'm sorry that should be,

35
00:02:09,10 --> 00:02:11,50
I'm sort of breaking my rule a little bit here

36
00:02:11,50 --> 00:02:15,30
but I think it's okay of putting my React code

37
00:02:15,30 --> 00:02:19,20
after the last html attribute.

38
00:02:19,20 --> 00:02:21,80
I feel like className should sort of stay where it is,

39
00:02:21,80 --> 00:02:23,90
and I'm just keeping them together.

40
00:02:23,90 --> 00:02:26,20
So that's all we need, this is pretty cool because,

41
00:02:26,20 --> 00:02:29,30
since we're not really doing anything before

42
00:02:29,30 --> 00:02:33,60
we execute the parent method.

43
00:02:33,60 --> 00:02:36,40
So we don't really need to do anything

44
00:02:36,40 --> 00:02:39,10
in the local React component.

45
00:02:39,10 --> 00:02:42,00
We're just letting props manage everything.

46
00:02:42,00 --> 00:02:46,70
So save that, when we'll go into app.js.

47
00:02:46,70 --> 00:02:52,70
We'll start out by looking in SearchAppointments

48
00:02:52,70 --> 00:02:56,30
and we will create the prop, so this will be

49
00:02:56,30 --> 00:03:00,20
changeOrder and this is going to execute a local method

50
00:03:00,20 --> 00:03:06,40
that we'll create called this.changeOrder,

51
00:03:06,40 --> 00:03:08,00
and so now we have to make that.

52
00:03:08,00 --> 00:03:13,00
This will be the easiest function we've created so far.

53
00:03:13,00 --> 00:03:17,80
We'll do it right here, this is going to be called changeOrder

54
00:03:17,80 --> 00:03:20,10
it's going to receive two things the order

55
00:03:20,10 --> 00:03:24,70
and the direction, that we pass

56
00:03:24,70 --> 00:03:28,00
from the SearchAppointments.js component

57
00:03:28,00 --> 00:03:30,70
and then in here we will just set the state

58
00:03:30,70 --> 00:03:34,10
to whatever those values are that we get

59
00:03:34,10 --> 00:03:39,50
from the sub-component, so orderBy.

60
00:03:39,50 --> 00:03:42,40
That's going to be set order

61
00:03:42,40 --> 00:03:48,00
and then orderDir it's going to be set to the direction.

62
00:03:48,00 --> 00:03:54,00
So that is all we need to do for this method

63
00:03:54,00 --> 00:03:58,70
and we need to do of course this piece right here,

64
00:03:58,70 --> 00:04:02,10
so I'll copy that one and I'll modify this

65
00:04:02,10 --> 00:04:08,20
to say changeOrder, and that should do it.

66
00:04:08,20 --> 00:04:11,80
Let's try it out if we did everything correctly

67
00:04:11,80 --> 00:04:14,20
we should be able to use this dropdown now

68
00:04:14,20 --> 00:04:18,60
and let's do descending order, ascending,

69
00:04:18,60 --> 00:04:22,50
by date and by owner.

70
00:04:22,50 --> 00:04:25,30
Now the beautiful thing about React is it allows you

71
00:04:25,30 --> 00:04:28,70
to compartmentalize your functionality

72
00:04:28,70 --> 00:04:33,40
the ListAppointments component is only worrying

73
00:04:33,40 --> 00:04:35,60
about displaying a list.

74
00:04:35,60 --> 00:04:38,60
Whatever data you pass to it it's going to display

75
00:04:38,60 --> 00:04:41,60
a list with that data, if you sort the data

76
00:04:41,60 --> 00:04:43,50
before you pass it to it it'll just display

77
00:04:43,50 --> 00:04:45,50
a sorted piece of data.

78
00:04:45,50 --> 00:04:49,50
If you filter the data it'll filter it,

79
00:04:49,50 --> 00:04:51,90
and all you need to do in this main component

80
00:04:51,90 --> 00:04:54,80
is sort of control what's happening with state.

81
00:04:54,80 --> 00:04:58,20
What's the current value of orderBy,

82
00:04:58,20 --> 00:05:00,50
what's the current value of orderDir,

83
00:05:00,50 --> 00:05:02,90
and you can see that it lets us write

84
00:05:02,90 --> 00:05:06,30
a lot less code than we would have to

85
00:05:06,30 --> 00:05:09,00
if we were writing and managing events.

