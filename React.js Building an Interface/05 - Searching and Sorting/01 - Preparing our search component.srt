1
00:00:00,50 --> 00:00:02,50
- [Instructor] Let's start working on our last component,

2
00:00:02,50 --> 00:00:05,60
which is going to sort, as well as allow you to

3
00:00:05,60 --> 00:00:07,90
search through the different appointments.

4
00:00:07,90 --> 00:00:09,80
We'll get started by going to this gist.

5
00:00:09,80 --> 00:00:12,30
I'm going to click on the Raw button right here

6
00:00:12,30 --> 00:00:14,40
and select this bootstrap code,

7
00:00:14,40 --> 00:00:16,70
then bring it back into my application,

8
00:00:16,70 --> 00:00:19,80
and I'll open up SearchAppointments.js,

9
00:00:19,80 --> 00:00:23,50
and I need to replace this code right here.

10
00:00:23,50 --> 00:00:26,20
Once again, putting everything inside parentheses

11
00:00:26,20 --> 00:00:28,60
because I have a multi-line template.

12
00:00:28,60 --> 00:00:30,60
And then I'm going to paste that

13
00:00:30,60 --> 00:00:35,50
and I've already changed the classes to classNames

14
00:00:35,50 --> 00:00:38,40
and everything else is just standard bootstrap code

15
00:00:38,40 --> 00:00:41,30
so you can see we have a form here where we can

16
00:00:41,30 --> 00:00:43,40
type in some text that we want to filter

17
00:00:43,40 --> 00:00:45,80
all this stuff through, and we can also,

18
00:00:45,80 --> 00:00:50,20
or we should be also able to modify how we sort

19
00:00:50,20 --> 00:00:51,60
through this element.

20
00:00:51,60 --> 00:00:53,70
So let's go ahead and start working on

21
00:00:53,70 --> 00:00:55,80
just sorting a list.

22
00:00:55,80 --> 00:00:57,00
And this is going to be pretty cool

23
00:00:57,00 --> 00:01:00,60
and I think show you the power of React.

24
00:01:00,60 --> 00:01:03,10
And what we'll need to do is create some additional

25
00:01:03,10 --> 00:01:07,20
state variables to keep track of how we want our

26
00:01:07,20 --> 00:01:08,60
sorting to work.

27
00:01:08,60 --> 00:01:11,90
So we'll say a couple of variables here orderBy

28
00:01:11,90 --> 00:01:15,20
and then I'll set it to petName to begin with.

29
00:01:15,20 --> 00:01:17,90
And then I'm going to use another variable here

30
00:01:17,90 --> 00:01:22,60
called orderDir, for direction.

31
00:01:22,60 --> 00:01:28,00
And I'll set that to ascending, or asc for right now.

32
00:01:28,00 --> 00:01:31,50
Now, when I am actually displaying the component,

33
00:01:31,50 --> 00:01:35,60
I can do something to the data and we would do that

34
00:01:35,60 --> 00:01:38,30
in between this render statement and the return statement

35
00:01:38,30 --> 00:01:41,20
because the return is designed to display the template.

36
00:01:41,20 --> 00:01:44,00
So we can sort of do something in between when we start

37
00:01:44,00 --> 00:01:48,70
to render and when we actually display the template.

38
00:01:48,70 --> 00:01:50,90
So we'll put the code right here,

39
00:01:50,90 --> 00:01:53,30
and what we'll do is we'll create variable called order.

40
00:01:53,30 --> 00:01:56,30
This is going to be just a temporary variable that we're

41
00:01:56,30 --> 00:02:00,70
going to use to decide how we're going to order the data.

42
00:02:00,70 --> 00:02:02,20
You'll see in a minute how it works.

43
00:02:02,20 --> 00:02:04,90
And then we'll create, again, a temporary variable

44
00:02:04,90 --> 00:02:11,00
called filteredApts and just set it to the current

45
00:02:11,00 --> 00:02:15,00
appointments that we have in our state.

46
00:02:15,00 --> 00:02:18,30
And then, what we'll do is, depending on the value

47
00:02:18,30 --> 00:02:24,80
of the order direction,

48
00:02:24,80 --> 00:02:27,80
we will modify our order variable.

49
00:02:27,80 --> 00:02:32,10
So we'll say, if this state ordered there is ascending,

50
00:02:32,10 --> 00:02:36,40
so if it's an ascending order then we will set the order

51
00:02:36,40 --> 00:02:39,50
variable to one.

52
00:02:39,50 --> 00:02:43,60
Otherwise set it to minus one.

53
00:02:43,60 --> 00:02:47,00
And this will become a multiplier that will allow me

54
00:02:47,00 --> 00:02:50,10
to sort the element in ascending order,

55
00:02:50,10 --> 00:02:52,40
or descending order.

56
00:02:52,40 --> 00:02:55,20
So you'll see me using this in a minute.

57
00:02:55,20 --> 00:02:56,10
Right, so then,

58
00:02:56,10 --> 00:02:59,40
I'm going to use the javascript sort method

59
00:02:59,40 --> 00:03:01,40
on filteredApts.

60
00:03:01,40 --> 00:03:05,30
And this works pretty easily just by comparing

61
00:03:05,30 --> 00:03:07,70
two items.

62
00:03:07,70 --> 00:03:10,80
So it's going to go through all of the elements

63
00:03:10,80 --> 00:03:13,10
in my array

64
00:03:13,10 --> 00:03:16,30
and rearrange them depending on the value

65
00:03:16,30 --> 00:03:17,40
of the element.

66
00:03:17,40 --> 00:03:21,10
So I'll say if, I'll take the first element

67
00:03:21,10 --> 00:03:26,00
and I'll say this.state.orderBy

68
00:03:26,00 --> 00:03:27,30
So I'm going to look in each element for

69
00:03:27,30 --> 00:03:29,60
a specific field. Remember that the data

70
00:03:29,60 --> 00:03:33,20
each one of these elements has these different fields.

71
00:03:33,20 --> 00:03:36,70
So I want to be able to sort by different

72
00:03:36,70 --> 00:03:37,50
fields right here.

73
00:03:37,50 --> 00:03:39,40
So I will take one of the fields,

74
00:03:39,40 --> 00:03:42,00
say like the petName, if I'm ordering by the pet name,

75
00:03:42,00 --> 00:03:44,50
and compare the two records.

76
00:03:44,50 --> 00:03:46,20
The first record and the second record

77
00:03:46,20 --> 00:03:49,10
and see which one is bigger than the other one

78
00:03:49,10 --> 00:03:51,10
and then sort them accordingly.

79
00:03:51,10 --> 00:03:56,10
So, we'll say if a and then the orderBy variable

80
00:03:56,10 --> 00:04:01,80
right now. We'll convert it to lower case,

81
00:04:01,80 --> 00:04:04,40
and this is just a regular javascript function.

82
00:04:04,40 --> 00:04:05,90
And the reason we're converting to lower case

83
00:04:05,90 --> 00:04:07,90
is because we want to make sure that

84
00:04:07,90 --> 00:04:11,10
if somebody types in an upper case character

85
00:04:11,10 --> 00:04:14,60
in the search, or if we're comparing two different

86
00:04:14,60 --> 00:04:17,20
say pay pays, and one of them happens to be

87
00:04:17,20 --> 00:04:19,70
upper case and the other one lower case,

88
00:04:19,70 --> 00:04:21,00
we don't really care about that.

89
00:04:21,00 --> 00:04:24,10
So, that's why we convert everything to lowercase

90
00:04:24,10 --> 00:04:25,80
when we're comparing.

91
00:04:25,80 --> 00:04:29,30
And so we'll say compare it to b

92
00:04:29,30 --> 00:04:31,60
Same thing over here at this state.

93
00:04:31,60 --> 00:04:38,60
That orderBy, also to lowercase.

94
00:04:38,60 --> 00:04:42,40
And then,

95
00:04:42,40 --> 00:04:45,40
So, normally you would return minus one if you

96
00:04:45,40 --> 00:04:48,80
wanted this to sort accordingly,

97
00:04:48,80 --> 00:04:51,40
but in here we're going to also have an

98
00:04:51,40 --> 00:04:53,80
additional multiplier, which will be our

99
00:04:53,80 --> 00:04:57,30
order variable that will allow me to reverse the sort.

100
00:04:57,30 --> 00:04:58,60
So, normally what you say is

101
00:04:58,60 --> 00:05:03,20
if these two compares, then return minus 1, and otherwise

102
00:05:03,20 --> 00:05:04,30
return one.

103
00:05:04,30 --> 00:05:07,10
But we actually are going to add this additional

104
00:05:07,10 --> 00:05:09,70
order variable here, so that we can

105
00:05:09,70 --> 00:05:13,70
with a simple setting of ascending or descending,

106
00:05:13,70 --> 00:05:16,60
reorder the sort accordingly.

107
00:05:16,60 --> 00:05:23,00
So, okay, let's finish this up.

108
00:05:23,00 --> 00:05:24,20
Right so,

109
00:05:24,20 --> 00:05:27,20
We have now filtered our appointments

110
00:05:27,20 --> 00:05:29,70
and what we're going to do is instead of

111
00:05:29,70 --> 00:05:31,70
passing the actual appointments to

112
00:05:31,70 --> 00:05:34,20
the list of appointments, we're going to

113
00:05:34,20 --> 00:05:36,10
pass this variable that we just created

114
00:05:36,10 --> 00:05:38,00
that contains the list of sorted appointments

115
00:05:38,00 --> 00:05:42,90
by our orderBy field, and in the order

116
00:05:42,90 --> 00:05:48,70
that is contained in our order dir state variable.

117
00:05:48,70 --> 00:05:55,40
So, we'll say filtered appointments here

118
00:05:55,40 --> 00:05:56,50
and I'll take a look at the appointments

119
00:05:56,50 --> 00:05:59,00
and now they are ordered by pet name

120
00:05:59,00 --> 00:06:01,80
in ascending order, and if we want to change them,

121
00:06:01,80 --> 00:06:07,50
we can go ahead and show them by ownerName

122
00:06:07,50 --> 00:06:08,30
ascending,

123
00:06:08,30 --> 00:06:11,50
and you can see that now that first owner is Audry.

124
00:06:11,50 --> 00:06:14,30
Or we can leave petName and set this to anything

125
00:06:14,30 --> 00:06:16,70
other than ascending,

126
00:06:16,70 --> 00:06:20,10
and now Zera is the first pet that appears here.

127
00:06:20,10 --> 00:06:21,80
So, supercool.

128
00:06:21,80 --> 00:06:24,50
The interesting thing here is that all we did

129
00:06:24,50 --> 00:06:28,60
was pass a different value to our list of appointments.

130
00:06:28,60 --> 00:06:31,60
And appointments is actually using a variable

131
00:06:31,60 --> 00:06:33,80
called appointments.

132
00:06:33,80 --> 00:06:36,40
And it doesn't care that we're feeding it something else,

133
00:06:36,40 --> 00:06:40,40
it's just going to display the list based on

134
00:06:40,40 --> 00:06:42,70
what we feed it

135
00:06:42,70 --> 00:06:45,70
and we just modify what that was to our

136
00:06:45,70 --> 00:06:47,00
filter appointments.

