1
00:00:00,50 --> 00:00:02,00
- [Instructor] We already have searching

2
00:00:02,00 --> 00:00:03,80
working in our application,

3
00:00:03,80 --> 00:00:05,90
but we need to update the application

4
00:00:05,90 --> 00:00:09,80
so that this input field controls that search parameter.

5
00:00:09,80 --> 00:00:12,10
So in app.js, I'll start by getting rid

6
00:00:12,10 --> 00:00:14,40
of this default query text

7
00:00:14,40 --> 00:00:16,50
that I did in the previous video

8
00:00:16,50 --> 00:00:20,30
so that I can have a full list to start up with.

9
00:00:20,30 --> 00:00:24,10
So let's go back then into SearchAppointments.

10
00:00:24,10 --> 00:00:26,60
We'll work our way backwards from SearchAppointments

11
00:00:26,60 --> 00:00:29,70
back into app in the same way that we've been doing.

12
00:00:29,70 --> 00:00:32,10
So we're looking for this input field,

13
00:00:32,10 --> 00:00:37,10
and we're going to look for an onChange event.

14
00:00:37,10 --> 00:00:40,40
Whenever somebody modifies this input field,

15
00:00:40,40 --> 00:00:44,80
then we're going to execute here an expression,

16
00:00:44,80 --> 00:00:48,90
pass along an event,

17
00:00:48,90 --> 00:00:53,00
and we don't need to do anything to this input field

18
00:00:53,00 --> 00:00:54,10
in the local component.

19
00:00:54,10 --> 00:00:56,30
We're just going to use the props

20
00:00:56,30 --> 00:00:58,90
to go ahead and pass this along

21
00:00:58,90 --> 00:01:03,70
to a method that we're going to call search appointments,

22
00:01:03,70 --> 00:01:07,30
and that search appointments will receive

23
00:01:07,30 --> 00:01:09,40
the value of the input field.

24
00:01:09,40 --> 00:01:14,40
So that will be an e.target.value.

25
00:01:14,40 --> 00:01:16,20
And then once we have that,

26
00:01:16,20 --> 00:01:22,70
that's really all we need to do in here.

27
00:01:22,70 --> 00:01:25,90
Let's go back into app.js,

28
00:01:25,90 --> 00:01:30,90
and we'll need to make sure we add that to the component.

29
00:01:30,90 --> 00:01:36,20
So SearchAppointments

30
00:01:36,20 --> 00:01:39,00
is going to be equal to a local function,

31
00:01:39,00 --> 00:01:40,70
call also search appointments.

32
00:01:40,70 --> 00:01:43,70
Although you could obviously call this whatever you want.

33
00:01:43,70 --> 00:01:45,30
Remember, to the right of the equals sign

34
00:01:45,30 --> 00:01:48,10
will be the local thing, and to the left

35
00:01:48,10 --> 00:01:52,10
will be the thing that comes from the component,

36
00:01:52,10 --> 00:01:54,10
or the sub-component in this case.

37
00:01:54,10 --> 00:01:58,90
So once we have that, we clearly need to define that.

38
00:01:58,90 --> 00:02:04,50
And we'll do that right here.

39
00:02:04,50 --> 00:02:06,50
It really doesn't matter where you do it.

40
00:02:06,50 --> 00:02:10,40
So we'll create this method called search appointments.

41
00:02:10,40 --> 00:02:13,90
It's going to receive some sort of text

42
00:02:13,90 --> 00:02:17,30
that will have a query, and then all we have to do here

43
00:02:17,30 --> 00:02:21,50
is modify state

44
00:02:21,50 --> 00:02:25,00
to use this value.

45
00:02:25,00 --> 00:02:28,20
We're storing the current query in queryText,

46
00:02:28,20 --> 00:02:32,00
and we'll pass along what we send

47
00:02:32,00 --> 00:02:35,60
from the input field as it gets changed.

48
00:02:35,60 --> 00:02:36,40
All right, almost done.

49
00:02:36,40 --> 00:02:39,40
Don't forget that you have to do

50
00:02:39,40 --> 00:02:45,00
the rebinding of this always

51
00:02:45,00 --> 00:02:50,10
so that everything works well.

52
00:02:50,10 --> 00:02:53,80
Let's save that, and now,

53
00:02:53,80 --> 00:02:57,90
our application searches dynamically, which is amazing.

54
00:02:57,90 --> 00:03:00,10
I don't know if this really impresses you,

55
00:03:00,10 --> 00:03:03,30
but the fact that react uses this virtual dom

56
00:03:03,30 --> 00:03:07,00
makes it super awesome because you never worry about

57
00:03:07,00 --> 00:03:08,90
dealing with events directly

58
00:03:08,90 --> 00:03:10,90
or dealing with the dom directly.

59
00:03:10,90 --> 00:03:13,90
All you do is create these little components

60
00:03:13,90 --> 00:03:17,20
and then create these methods that modify state,

61
00:03:17,20 --> 00:03:19,10
and everything works as it should

62
00:03:19,10 --> 00:03:22,50
and you don't have to worry about all of the mess

63
00:03:22,50 --> 00:03:23,60
that you would normally have to do

64
00:03:23,60 --> 00:03:27,00
with regular JavaScript or jQuery or something like that.

