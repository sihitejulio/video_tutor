1
00:00:00,50 --> 00:00:02,10
- [Instructor] Next, we'll work on building

2
00:00:02,10 --> 00:00:04,70
the search functionality for our application.

3
00:00:04,70 --> 00:00:08,40
We won't quite code the search box right here,

4
00:00:08,40 --> 00:00:12,30
but we'll cause the list to be automatically searched

5
00:00:12,30 --> 00:00:14,00
by some query text.

6
00:00:14,00 --> 00:00:18,70
So we'll start off by creating an additional parameter here

7
00:00:18,70 --> 00:00:21,80
in our state, we'll call it queryText.

8
00:00:21,80 --> 00:00:25,30
We'll set that to begin with to nothing.

9
00:00:25,30 --> 00:00:27,80
And then, what we need to do is look in our component,

10
00:00:27,80 --> 00:00:30,70
where we are filtering the appointments.

11
00:00:30,70 --> 00:00:34,20
We're also going to search the appointments

12
00:00:34,20 --> 00:00:36,70
using the JavaScript filter function.

13
00:00:36,70 --> 00:00:39,00
Now, it looks like this, filter,

14
00:00:39,00 --> 00:00:42,80
and then you can use some arrow function magic here,

15
00:00:42,80 --> 00:00:44,80
which I'll explain in just a minute.

16
00:00:44,80 --> 00:00:46,00
But I just want you to know that

17
00:00:46,00 --> 00:00:48,10
this is a higher-order function,

18
00:00:48,10 --> 00:00:51,10
which means that it doesn't allow you to modify

19
00:00:51,10 --> 00:00:54,80
a array directly, like sort is doing here.

20
00:00:54,80 --> 00:00:59,70
So I need to say filteredApts

21
00:00:59,70 --> 00:01:02,00
is equal to filteredApts.sort,

22
00:01:02,00 --> 00:01:05,00
and then we filter right here.

23
00:01:05,00 --> 00:01:08,80
So with filter, you sort of create a temporary variable

24
00:01:08,80 --> 00:01:12,30
that represents each item as it is being filtered,

25
00:01:12,30 --> 00:01:14,90
so we'll call that eachItem.

26
00:01:14,90 --> 00:01:20,00
And then I'm going to use an arrow function here,

27
00:01:20,00 --> 00:01:23,80
and we'll use the return statement,

28
00:01:23,80 --> 00:01:26,70
and we'll take each of the different parameters

29
00:01:26,70 --> 00:01:28,50
that we want to search by,

30
00:01:28,50 --> 00:01:31,70
and we'll compare what we are searching for,

31
00:01:31,70 --> 00:01:35,70
with the value of that element in our data,

32
00:01:35,70 --> 00:01:38,00
so it looks like this.

33
00:01:38,00 --> 00:01:39,90
We'll look for each item,

34
00:01:39,90 --> 00:01:43,50
and then we want to search this just by petName.

35
00:01:43,50 --> 00:01:44,80
Because this is a text search,

36
00:01:44,80 --> 00:01:46,90
we want to look for things that people type in.

37
00:01:46,90 --> 00:01:52,30
Pet name, the owner name, or what's wrong with the pet.

38
00:01:52,30 --> 00:01:55,00
So we'll say eachItem, petName,

39
00:01:55,00 --> 00:01:57,00
convert that to lower case,

40
00:01:57,00 --> 00:02:00,90
just to prevent any capitalization differences,

41
00:02:00,90 --> 00:02:06,80
and then we will use this method called includes

42
00:02:06,80 --> 00:02:10,00
to test to see

43
00:02:10,00 --> 00:02:13,70
if it matches what's in our query string.

44
00:02:13,70 --> 00:02:18,20
Again, toLowerCase,

45
00:02:18,20 --> 00:02:22,00
and let's go ahead and make this bigger.

46
00:02:22,00 --> 00:02:24,90
We'll scroll it up a little bit.

47
00:02:24,90 --> 00:02:27,40
And we need to make sure that we compare it for each

48
00:02:27,40 --> 00:02:30,10
of the items that we want to test for,

49
00:02:30,10 --> 00:02:32,80
so we'll say we'll use the or statement,

50
00:02:32,80 --> 00:02:36,70
and go ahead and copy this section twice here,

51
00:02:36,70 --> 00:02:42,40
and we want to search by ownerName,

52
00:02:42,40 --> 00:02:45,10
and aptNotes only.

53
00:02:45,10 --> 00:02:50,20
We don't need this last or statement here.

54
00:02:50,20 --> 00:02:53,80
And let's go ahead and save that, take a look at our app.

55
00:02:53,80 --> 00:02:56,60
Doesn't look like we've made any glaring mistakes.

56
00:02:56,60 --> 00:02:57,80
Now, it's not filtering yet,

57
00:02:57,80 --> 00:03:02,40
but if we type in or modify our query text,

58
00:03:02,40 --> 00:03:05,20
let's put in ba, let's type in actually one of the names,

59
00:03:05,20 --> 00:03:08,50
bailey here, and save this.

60
00:03:08,50 --> 00:03:11,00
You'll see that only that record shows up.

61
00:03:11,00 --> 00:03:12,80
So let's try another one like Chip,

62
00:03:12,80 --> 00:03:14,20
I know that's another pet,

63
00:03:14,20 --> 00:03:15,90
and let's just try something that would match

64
00:03:15,90 --> 00:03:18,20
more than one thing, like ba.

65
00:03:18,20 --> 00:03:21,30
So that matches a bunch of different things,

66
00:03:21,30 --> 00:03:23,80
and that's all there is to a search.

67
00:03:23,80 --> 00:03:28,50
You're just using a series of JavaScript methods here,

68
00:03:28,50 --> 00:03:32,40
including the filter combine with the sort,

69
00:03:32,40 --> 00:03:38,20
and also a test to see if the item,

70
00:03:38,20 --> 00:03:41,00
with the record that we're checking for,

71
00:03:41,00 --> 00:03:44,00
matches something in our query string.

72
00:03:44,00 --> 00:03:46,30
So it's actually pretty easy to do a search.

73
00:03:46,30 --> 00:03:52,20
The cool thing again is, even when we sort things,

74
00:03:52,20 --> 00:03:54,70
since we're searching and filtering at the same time,

75
00:03:54,70 --> 00:03:56,80
we can actually test to see that

76
00:03:56,80 --> 00:04:00,00
even if we change the sorting algorithm,

77
00:04:00,00 --> 00:04:04,80
our query is still managing what displays here,

78
00:04:04,80 --> 00:04:09,00
and we can look at the sort of sorted list

79
00:04:09,00 --> 00:04:10,60
by our parameters.

80
00:04:10,60 --> 00:04:12,10
And everything is doing its own job.

81
00:04:12,10 --> 00:04:15,20
The dropdown here is modifying state,

82
00:04:15,20 --> 00:04:18,80
the list doesn't care, it just displays whatever you feed it

83
00:04:18,80 --> 00:04:21,30
even if it's a filtered, sorted list,

84
00:04:21,30 --> 00:04:23,10
and if we were to add an appointment,

85
00:04:23,10 --> 00:04:26,00
it would just add an appointment in here as well.

