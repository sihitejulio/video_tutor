1
00:00:00,50 --> 00:00:02,00
- [Instructor] To finish out this application,

2
00:00:02,00 --> 00:00:06,10
we're going to program how we can modify a record.

3
00:00:06,10 --> 00:00:08,70
And the ones that I'm going to modify

4
00:00:08,70 --> 00:00:09,50
are going to be pretty simple.

5
00:00:09,50 --> 00:00:12,70
I'm just going to let you modify the name of the pet,

6
00:00:12,70 --> 00:00:17,10
the owner, and also anything in the description

7
00:00:17,10 --> 00:00:18,40
to keep things simple.

8
00:00:18,40 --> 00:00:20,70
If we were going to modify something like the date

9
00:00:20,70 --> 00:00:23,10
we would need like a popup link or something like that.

10
00:00:23,10 --> 00:00:26,40
To take care of these we can use a feature in HTML

11
00:00:26,40 --> 00:00:28,50
called contentEditable.

12
00:00:28,50 --> 00:00:31,50
It allows any field to be editable.

13
00:00:31,50 --> 00:00:34,50
But we'll need to manage how that works within React,

14
00:00:34,50 --> 00:00:39,20
because it isn't something that React is used to doing.

15
00:00:39,20 --> 00:00:42,10
It involves directly having access to the dom,

16
00:00:42,10 --> 00:00:44,90
which is usually something that React manages for you.

17
00:00:44,90 --> 00:00:47,90
So we'll start by going to ListAppointments.

18
00:00:47,90 --> 00:00:53,20
And we'll add a contentEditable to those different fields.

19
00:00:53,20 --> 00:00:57,10
So here where I have the pet-name

20
00:00:57,10 --> 00:01:02,70
I'm going to just add contentEditable.

21
00:01:02,70 --> 00:01:05,50
And notice that it's spelled a little bit differently

22
00:01:05,50 --> 00:01:09,20
than what you would normally do in HTML.

23
00:01:09,20 --> 00:01:10,20
And as soon as I do that,

24
00:01:10,20 --> 00:01:12,60
let's just go ahead and save this,

25
00:01:12,60 --> 00:01:14,10
and I'm going to show you that the application

26
00:01:14,10 --> 00:01:15,40
is going to show up some warnings,

27
00:01:15,40 --> 00:01:17,50
actually it's going to throw up a bunch of errors.

28
00:01:17,50 --> 00:01:22,60
And that's because it really warns you that

29
00:01:22,60 --> 00:01:25,60
this has children and it's being managed by React,

30
00:01:25,60 --> 00:01:28,40
so you have to make sure that nothing

31
00:01:28,40 --> 00:01:30,90
is unexpectedly modified or duplicated.

32
00:01:30,90 --> 00:01:32,00
It's probably not intentional.

33
00:01:32,00 --> 00:01:34,90
It actually is intentional.

34
00:01:34,90 --> 00:01:37,10
And I want to show you that sometimes you really do

35
00:01:37,10 --> 00:01:39,70
need to change the way things are working.

36
00:01:39,70 --> 00:01:41,30
To override this error we're going to

37
00:01:41,30 --> 00:01:43,70
have to add an additional attribute here.

38
00:01:43,70 --> 00:01:47,10
It's called suppressContentEditableWarning,

39
00:01:47,10 --> 00:01:49,50
not surprisingly.

40
00:01:49,50 --> 00:01:52,50
And whenever you're doing something that react doesn't like

41
00:01:52,50 --> 00:01:56,10
there are a lot of other versions of these kind of things

42
00:01:56,10 --> 00:01:59,30
that you can include so that it won't complain.

43
00:01:59,30 --> 00:02:03,70
So let's see, let's save that and if I inspect this

44
00:02:03,70 --> 00:02:06,40
it should show you that we have no more errors,

45
00:02:06,40 --> 00:02:09,10
which is great, that's what we want.

46
00:02:09,10 --> 00:02:13,00
But we need to add a few more things in here.

47
00:02:13,00 --> 00:02:15,90
So in this span I need to detect

48
00:02:15,90 --> 00:02:19,30
when somebody modifies this field,

49
00:02:19,30 --> 00:02:22,40
which is detectable through an event called blur.

50
00:02:22,40 --> 00:02:25,90
So onBlur, and then it's going to be sort of like

51
00:02:25,90 --> 00:02:27,50
what we've done before.

52
00:02:27,50 --> 00:02:31,80
We're going to get an event as a result of this change

53
00:02:31,80 --> 00:02:36,40
and so we will use the props and pass along

54
00:02:36,40 --> 00:02:40,90
what we have here to a method called updateInfo

55
00:02:40,90 --> 00:02:44,30
that we'll be creating in the main app.

56
00:02:44,30 --> 00:02:48,60
Then we will pass along the thing that we want to change.

57
00:02:48,60 --> 00:02:52,80
In this case it's going to be the petName.

58
00:02:52,80 --> 00:02:54,40
Then we're also going to pass along

59
00:02:54,40 --> 00:02:56,20
the text that we want to change,

60
00:02:56,20 --> 00:02:58,10
which will be available

61
00:02:58,10 --> 00:03:01,20
in the target attribute of the event.

62
00:03:01,20 --> 00:03:05,20
And we will look for an attribute called innerText.

63
00:03:05,20 --> 00:03:10,60
And then finally we need to pass along the appointment ID

64
00:03:10,60 --> 00:03:11,90
of the current element.

65
00:03:11,90 --> 00:03:15,90
So I'll do item appointment ID,

66
00:03:15,90 --> 00:03:17,70
because that's what we're going to use

67
00:03:17,70 --> 00:03:20,60
to find the proper record.

68
00:03:20,60 --> 00:03:27,30
So I'm going to copy all of these

69
00:03:27,30 --> 00:03:30,50
and do the same thing for the other fields

70
00:03:30,50 --> 00:03:32,00
that I want to modify.

71
00:03:32,00 --> 00:03:34,60
Just going to go into span in here.

72
00:03:34,60 --> 00:03:36,50
So I'm just going to paste and I have

73
00:03:36,50 --> 00:03:39,50
a little utility called prettier

74
00:03:39,50 --> 00:03:41,40
that formats things when I save.

75
00:03:41,40 --> 00:03:44,30
So it's actually a really nice thing to have

76
00:03:44,30 --> 00:03:46,40
because it makes every piece of code

77
00:03:46,40 --> 00:03:48,80
that you create consistent.

78
00:03:48,80 --> 00:03:53,60
So we'll also add this right here in appointment notes,

79
00:03:53,60 --> 00:03:57,20
and we'll save it.

80
00:03:57,20 --> 00:04:00,60
And of course we'll need to modify the names of the fields.

81
00:04:00,60 --> 00:04:05,70
So this one is ownerName.

82
00:04:05,70 --> 00:04:10,30
And this one is aptNotes.

83
00:04:10,30 --> 00:04:12,80
All right, so now that we have the events

84
00:04:12,80 --> 00:04:16,20
and they're triggering something called updateInfo,

85
00:04:16,20 --> 00:04:19,60
we'll need to go to App.js and start programming that.

86
00:04:19,60 --> 00:04:26,10
We'll start off in the list of appointments.

87
00:04:26,10 --> 00:04:29,00
We'll reference that updateInfo method here.

88
00:04:29,00 --> 00:04:34,70
And we'll pass that along to a local version of that event.

89
00:04:34,70 --> 00:04:40,30
Right, so that means that we have to create that event.

90
00:04:40,30 --> 00:04:43,20
Let's go ahead and put it right here.

91
00:04:43,20 --> 00:04:45,00
We'll call the updateInfo.

92
00:04:45,00 --> 00:04:51,70
It's going to get a name, value, and an ID.

93
00:04:51,70 --> 00:04:55,40
And again, we'll have to make a copy of the appointments.

94
00:04:55,40 --> 00:05:00,00
So we'll put it in this variable.

95
00:05:00,00 --> 00:05:03,20
We'll pass along the value of the array

96
00:05:03,20 --> 00:05:05,40
of the current appointments.

97
00:05:05,40 --> 00:05:08,40
And then we will initialize another variable

98
00:05:08,40 --> 00:05:11,40
called the index.

99
00:05:11,40 --> 00:05:13,70
The appointment index.

100
00:05:13,70 --> 00:05:19,10
And here I'm going to use another fantastic Lodash method.

101
00:05:19,10 --> 00:05:21,70
The job of this is going to locate the index

102
00:05:21,70 --> 00:05:25,00
of some element in the array.

103
00:05:25,00 --> 00:05:27,70
So the way that my application works,

104
00:05:27,70 --> 00:05:32,10
when they data gets loaded,

105
00:05:32,10 --> 00:05:34,00
from this data that JSON file,

106
00:05:34,00 --> 00:05:37,30
I push a individual ID,

107
00:05:37,30 --> 00:05:40,90
but because I'm sorting and searching through an element,

108
00:05:40,90 --> 00:05:44,70
the actual current index of the list that is showing

109
00:05:44,70 --> 00:05:52,70
might be different than the ID that I assign to that record.

110
00:05:52,70 --> 00:05:56,70
So what I need to do is actually find the current

111
00:05:56,70 --> 00:06:00,50
and actual ID, not of the sorted list,

112
00:06:00,50 --> 00:06:02,70
but of the original list.

113
00:06:02,70 --> 00:06:04,50
So hopefully that makes sense.

114
00:06:04,50 --> 00:06:07,80
And so what I'm going to do is use a method

115
00:06:07,80 --> 00:06:10,20
from the handy dandy Lodash,

116
00:06:10,20 --> 00:06:12,80
which is a fantastic library, by the way.

117
00:06:12,80 --> 00:06:17,30
This ones called findIndex.

118
00:06:17,30 --> 00:06:25,20
And so what I'll do is I'll pass along the appointments.

119
00:06:25,20 --> 00:06:30,30
And then I need to pass along something in the object.

120
00:06:30,30 --> 00:06:32,40
So remember, I'm looking through this data,

121
00:06:32,40 --> 00:06:34,60
and this data is an array of objects.

122
00:06:34,60 --> 00:06:38,20
So I need to pass it along a unique identifier

123
00:06:38,20 --> 00:06:42,10
that is only going to be in that element,

124
00:06:42,10 --> 00:06:45,10
and it will return the specific record that I'm looking for.

125
00:06:45,10 --> 00:06:47,60
Now remember, I can't guarantee that I'm

126
00:06:47,60 --> 00:06:50,30
not going to have two pets with the same name,

127
00:06:50,30 --> 00:06:54,30
two of the same owners, even the appointment notes

128
00:06:54,30 --> 00:06:55,70
and the dates can't be guaranteed

129
00:06:55,70 --> 00:06:58,10
'cause I might be accepting more than one pet.

130
00:06:58,10 --> 00:07:01,70
But one of the reasons I passed along that unique index

131
00:07:01,70 --> 00:07:04,70
is so that I have a unique identifier

132
00:07:04,70 --> 00:07:08,00
for each pet and that's what I'm going to be testing

133
00:07:08,00 --> 00:07:11,70
in this findIndex method.

134
00:07:11,70 --> 00:07:16,10
So we'll say aptId, that's going to be the ID.

135
00:07:16,10 --> 00:07:18,70
So this is the ID that we get passed

136
00:07:18,70 --> 00:07:21,50
when we edit the record.

137
00:07:21,50 --> 00:07:25,30
And now this is going to return the actual ID

138
00:07:25,30 --> 00:07:29,20
in the current state

139
00:07:29,20 --> 00:07:33,20
so that I can modify it in my data.

140
00:07:33,20 --> 00:07:37,50
So now what I need to do is actually modify that record.

141
00:07:37,50 --> 00:07:40,50
So I'm going to say temp appointments

142
00:07:40,50 --> 00:07:47,60
using that appointment index and then passing along

143
00:07:47,60 --> 00:07:49,00
the name of the field that I want to edit.

144
00:07:49,00 --> 00:07:52,00
So when I edit something here I am passing along

145
00:07:52,00 --> 00:07:54,30
the name of the thing that I'm editing.

146
00:07:54,30 --> 00:07:58,10
If I am in the pet name part of the record

147
00:07:58,10 --> 00:07:59,90
then I'll pass pet name along.

148
00:07:59,90 --> 00:08:01,90
If I'm in the owner name,

149
00:08:01,90 --> 00:08:05,40
if I'm in some other place, it's going to pass along

150
00:08:05,40 --> 00:08:07,50
the name of the field that I'm trying to edit

151
00:08:07,50 --> 00:08:12,70
as I blur or exit one of those contentEditable fields.

152
00:08:12,70 --> 00:08:13,90
So that's what this name is.

153
00:08:13,90 --> 00:08:18,10
And then I'll pass along the new value.

154
00:08:18,10 --> 00:08:25,90
Then I will just use setState to modify the appointments

155
00:08:25,90 --> 00:08:28,20
so that they contain the new version

156
00:08:28,20 --> 00:08:36,50
of my appointment that I stored in temp appointment.

157
00:08:36,50 --> 00:08:38,80
All right, and again.

158
00:08:38,80 --> 00:08:39,90
I shouldn't have saved that.

159
00:08:39,90 --> 00:08:43,50
But essentially remember, that I always have to do this,

160
00:08:43,50 --> 00:08:46,50
and it's really easy to forget.

161
00:08:46,50 --> 00:08:48,30
Also findIndex.

162
00:08:48,30 --> 00:08:51,10
I haven't imported the findIndex method yet from Lodash.

163
00:08:51,10 --> 00:08:53,30
So I need to make sure I do that.

164
00:08:53,30 --> 00:08:56,90
FindIndex and without,

165
00:08:56,90 --> 00:08:59,40
that's why sometimes I don't look at this preview

166
00:08:59,40 --> 00:09:02,40
because sometimes it doesn't keep up

167
00:09:02,40 --> 00:09:04,20
with what I'm doing either.

168
00:09:04,20 --> 00:09:10,00
So let's see, this new one is called

169
00:09:10,00 --> 00:09:15,60
updateInfo.

170
00:09:15,60 --> 00:09:17,20
Find this.

171
00:09:17,20 --> 00:09:20,70
And now let's try to modify this one,

172
00:09:20,70 --> 00:09:22,70
let's call him Zed.

173
00:09:22,70 --> 00:09:24,70
There's that when I modify his name

174
00:09:24,70 --> 00:09:27,90
it actually takes me, it resorts the list

175
00:09:27,90 --> 00:09:30,00
and it takes me, actually, to that spot,

176
00:09:30,00 --> 00:09:31,00
which is really cool.

177
00:09:31,00 --> 00:09:34,10
It means that this version of the list

178
00:09:34,10 --> 00:09:37,60
that I'm seeing right now is the already sorted list.

179
00:09:37,60 --> 00:09:41,80
It noticed that I made a change by modifying the state.

180
00:09:41,80 --> 00:09:44,70
It changed the state and it's now showing me the new list.

181
00:09:44,70 --> 00:09:46,00
So this is working.

