1
00:00:00,50 --> 00:00:01,70
- [Instructor] It's time to starting working

2
00:00:01,70 --> 00:00:04,40
on how we process information in a form.

3
00:00:04,40 --> 00:00:07,50
To do that we need to learn how to manage the input fields

4
00:00:07,50 --> 00:00:10,50
that are in this form, and so what we'll need to do

5
00:00:10,50 --> 00:00:15,30
is create some variables in our AddAppointments.js to keep

6
00:00:15,30 --> 00:00:19,70
track of these different input fields as they are modified.

7
00:00:19,70 --> 00:00:23,50
We'll start by adding a constructor section here.

8
00:00:23,50 --> 00:00:28,50
And in here we're going to create a state.

9
00:00:28,50 --> 00:00:31,30
Now part of the reason we're doing this is because

10
00:00:31,30 --> 00:00:34,40
when we store the date, we're actually going to store

11
00:00:34,40 --> 00:00:38,40
a combination of both the date and the time together,

12
00:00:38,40 --> 00:00:41,70
but I've kept these fields in different places in the from

13
00:00:41,70 --> 00:00:44,60
because they're a little bit easier to fill out this way.

14
00:00:44,60 --> 00:00:47,00
When the data gets saved; however, if we take a look

15
00:00:47,00 --> 00:00:50,00
at the data.jason file, you'll see that everything

16
00:00:50,00 --> 00:00:51,90
gets stored together, so I'm going to need

17
00:00:51,90 --> 00:00:57,20
to combine the date and also the time.

18
00:00:57,20 --> 00:01:00,40
So let's go back here, we'll create variables for each

19
00:01:00,40 --> 00:01:06,20
of these in our local state, pet name, et cetera, et cetera.

20
00:01:06,20 --> 00:01:08,80
Now that we have those local variables, let's go into

21
00:01:08,80 --> 00:01:14,00
each one of the fields, and we'll make this bigger.

22
00:01:14,00 --> 00:01:18,20
And we'll create references to the values of the

23
00:01:18,20 --> 00:01:22,30
local state variables, as well as an event that will

24
00:01:22,30 --> 00:01:27,50
trigger whenever somebody changes any of the input fields.

25
00:01:27,50 --> 00:01:30,70
So right in the input field for the pet name,

26
00:01:30,70 --> 00:01:35,60
I'm going to create a value attribute, and this will also

27
00:01:35,60 --> 00:01:41,30
reference our local state, and then in addition to that

28
00:01:41,30 --> 00:01:43,90
for each one of these fields, we're going to trigger

29
00:01:43,90 --> 00:01:47,70
an event whenever any of them change.

30
00:01:47,70 --> 00:01:51,50
So we'll use the onChange event handler, and we'll

31
00:01:51,50 --> 00:01:54,90
create a method here called handleChange.

32
00:01:54,90 --> 00:01:57,60
We haven't made this one yet, so whenever any of these

33
00:01:57,60 --> 00:02:01,80
fields change, this event will be called, we can create

34
00:02:01,80 --> 00:02:06,70
one for all of them, so let's do that on

35
00:02:06,70 --> 00:02:11,50
every one of the different input fields,

36
00:02:11,50 --> 00:02:20,50
obviously changing the different variables.

37
00:02:20,50 --> 00:02:23,00
Now we need to create this handle

38
00:02:23,00 --> 00:02:31,20
change method, so we'll do that over here.

39
00:02:31,20 --> 00:02:35,10
Whenever an event takes place, we can receive that event

40
00:02:35,10 --> 00:02:38,20
in a variable in any of these methods, so

41
00:02:38,20 --> 00:02:41,50
that will create some variables to manage what is happening

42
00:02:41,50 --> 00:02:44,10
when somebody types in something in any

43
00:02:44,10 --> 00:02:45,90
of the fields that we're monitoring.

44
00:02:45,90 --> 00:02:49,90
So we'll say const here, and create a value equal target,

45
00:02:49,90 --> 00:02:53,20
we'll set that to the target of the current event.

46
00:02:53,20 --> 00:02:56,10
So this would be pointing to the input field

47
00:02:56,10 --> 00:02:59,80
that somebody is modifying at any point in time.

48
00:02:59,80 --> 00:03:02,60
We'll create another variable for keeping track of the

49
00:03:02,60 --> 00:03:09,70
value that somebody has changed in the input field.

50
00:03:09,70 --> 00:03:12,50
And finally, we'll create another one to keep track

51
00:03:12,50 --> 00:03:16,70
of the name of the input field.

52
00:03:16,70 --> 00:03:20,70
So, this is important too, every one of these different

53
00:03:20,70 --> 00:03:24,10
input fields has a name attribute, and that's what

54
00:03:24,10 --> 00:03:26,20
we're actually referring to here.

55
00:03:26,20 --> 00:03:29,40
At least, that's what we actually want to get a hold of

56
00:03:29,40 --> 00:03:33,50
so that we are modifying the right item.

57
00:03:33,50 --> 00:03:38,60
Then what we can do here is set the state of the current

58
00:03:38,60 --> 00:03:44,80
component, and we can use the name of the element,

59
00:03:44,80 --> 00:03:46,80
so this would be like the pet name, owner name,

60
00:03:46,80 --> 00:03:49,40
the name of the input name in other words, and set that

61
00:03:49,40 --> 00:03:52,50
to the value that we receive in the handle change method.

62
00:03:52,50 --> 00:03:56,10
And it looks like I need a parenthesis here around this

63
00:03:56,10 --> 00:03:59,80
entire thing, so let's do that.

64
00:03:59,80 --> 00:04:01,30
So there's a couple of other things I need to do

65
00:04:01,30 --> 00:04:04,30
since I created this method right here,

66
00:04:04,30 --> 00:04:07,00
and I'm using the this keyword.

67
00:04:07,00 --> 00:04:11,80
I need to make sure that in the constructor I also bind

68
00:04:11,80 --> 00:04:18,90
the keyword this properly, so I'm going to tell the method

69
00:04:18,90 --> 00:04:26,30
called handleChange to have a proper understanding

70
00:04:26,30 --> 00:04:30,30
by binding the keyword this of the parent class.

71
00:04:30,30 --> 00:04:33,70
So the keyword this is going to mean the class

72
00:04:33,70 --> 00:04:36,10
and therefore handleChange is now going to have

73
00:04:36,10 --> 00:04:40,90
access to be able to modify the state here.

74
00:04:40,90 --> 00:04:43,60
So, in addition to that I need to make sure that I call

75
00:04:43,60 --> 00:04:47,60
this super method whenever I use a constructor.

76
00:04:47,60 --> 00:04:53,20
Let's go ahead and save that and take a look at our form.

77
00:04:53,20 --> 00:04:55,90
And, actually let's make this form a lot bigger

78
00:04:55,90 --> 00:04:58,30
so that we can see more things on screen.

79
00:04:58,30 --> 00:05:01,20
And what I want to show you that we have now is,

80
00:05:01,20 --> 00:05:06,30
if you inspect this component, with the Chrome React

81
00:05:06,30 --> 00:05:09,90
extension, I want you to go ahead and click on

82
00:05:09,90 --> 00:05:12,80
add appointments to take a look at that component.

83
00:05:12,80 --> 00:05:14,90
So you can see that in state we have all the variables

84
00:05:14,90 --> 00:05:20,60
that we created, and if we modify one of these right here,

85
00:05:20,60 --> 00:05:24,10
you can see that as we're typing in the value here,

86
00:05:24,10 --> 00:05:29,60
the variable is actually syncing with the state that

87
00:05:29,60 --> 00:05:31,60
we have generated ourselves here.

88
00:05:31,60 --> 00:05:34,90
So let's go ahead and type all these in so you can see that

89
00:05:34,90 --> 00:05:43,80
they all get modified as we're entering them in here.

90
00:05:43,80 --> 00:05:45,70
And that's great, 'cause that's exactly what we need

91
00:05:45,70 --> 00:05:48,40
in order to be able to process the rest of this form

92
00:05:48,40 --> 00:05:51,00
which we'll do in the next video.

