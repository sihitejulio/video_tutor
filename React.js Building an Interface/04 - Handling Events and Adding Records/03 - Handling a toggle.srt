1
00:00:00,50 --> 00:00:02,40
- [Instructor] Let's go ahead and make

2
00:00:02,40 --> 00:00:04,60
our headline here clickable

3
00:00:04,60 --> 00:00:07,00
so that it displays or hides the form.

4
00:00:07,00 --> 00:00:08,40
This is going to be pretty much a review

5
00:00:08,40 --> 00:00:09,90
of the things that we've done before,

6
00:00:09,90 --> 00:00:12,30
but I think it's worthwhile to go over them.

7
00:00:12,30 --> 00:00:15,50
So we'll go back into AddAppointments.js,

8
00:00:15,50 --> 00:00:18,70
and I want to start by adding an icon here.

9
00:00:18,70 --> 00:00:20,50
So just like we did with other icons,

10
00:00:20,50 --> 00:00:22,90
I'm just going to import the one that I need.

11
00:00:22,90 --> 00:00:26,90
This one is called FaPlus,

12
00:00:26,90 --> 00:00:32,00
and it's coming from the react-icons

13
00:00:32,00 --> 00:00:36,20
component that is in the node_modules folder,

14
00:00:36,20 --> 00:00:38,60
and it's part of the Font Awesome family,

15
00:00:38,60 --> 00:00:41,30
so it gets encoded like this.

16
00:00:41,30 --> 00:00:43,10
So now that we have it there, we need to use it,

17
00:00:43,10 --> 00:00:45,60
and that's super easy.

18
00:00:45,60 --> 00:00:50,20
All we need to do is, right before Add Appointment,

19
00:00:50,20 --> 00:00:53,70
we'll just use an FaPlus

20
00:00:53,70 --> 00:00:59,30
tag here, which will call that component

21
00:00:59,30 --> 00:01:01,50
and give us a nice-looking plus sign here.

22
00:01:01,50 --> 00:01:03,80
So this will show or hide the form.

23
00:01:03,80 --> 00:01:07,80
And now it's time to add an event

24
00:01:07,80 --> 00:01:12,60
that tracks the clicking on this header right here.

25
00:01:12,60 --> 00:01:16,00
So this is going to go right here,

26
00:01:16,00 --> 00:01:18,80
and I tend to put all of my

27
00:01:18,80 --> 00:01:23,60
sort of application-specific code at the end of the tags.

28
00:01:23,60 --> 00:01:27,30
Here, we'll say onClick, then again use an expression,

29
00:01:27,30 --> 00:01:30,80
and then go ahead and look for, in the props,

30
00:01:30,80 --> 00:01:37,80
something that we'll create called toggleForm.

31
00:01:37,80 --> 00:01:39,60
And we'll go ahead and save this.

32
00:01:39,60 --> 00:01:41,80
This doesn't exist quite yet.

33
00:01:41,80 --> 00:01:43,20
I tend to call things that I'm going to do

34
00:01:43,20 --> 00:01:45,60
on my local components handle something.

35
00:01:45,60 --> 00:01:49,10
So if I were to take care of this in this local component,

36
00:01:49,10 --> 00:01:52,00
I would create a method called handle form,

37
00:01:52,00 --> 00:01:53,40
and then that would do something,

38
00:01:53,40 --> 00:01:56,20
possibly pass it to the main component.

39
00:01:56,20 --> 00:01:58,10
And I usually call it,

40
00:01:58,10 --> 00:01:59,90
in the main component, whatever I actually do.

41
00:01:59,90 --> 00:02:02,20
So you'll see that when I create local ones,

42
00:02:02,20 --> 00:02:04,00
they're always like handle something,

43
00:02:04,00 --> 00:02:07,20
and then the main ones have the actual reference

44
00:02:07,20 --> 00:02:09,30
that makes more sense.

45
00:02:09,30 --> 00:02:13,90
All right, so this is looking for something in App.js

46
00:02:13,90 --> 00:02:15,20
in the component.

47
00:02:15,20 --> 00:02:18,80
So in addition to this display of form here,

48
00:02:18,80 --> 00:02:21,50
then we need to pass this prop,

49
00:02:21,50 --> 00:02:24,90
and this is going to be called toggleForm

50
00:02:24,90 --> 00:02:28,10
because that's what I called it in the sub-component.

51
00:02:28,10 --> 00:02:30,10
And now this will execute

52
00:02:30,10 --> 00:02:34,20
something locally called toggleForm.

53
00:02:34,20 --> 00:02:39,10
All right, so now I need to write that function.

54
00:02:39,10 --> 00:02:41,00
Guess I'll do it right here.

55
00:02:41,00 --> 00:02:45,30
This will say toggleForm.

56
00:02:45,30 --> 00:02:50,40
It's going to be, it's just setting the state.

57
00:02:50,40 --> 00:02:53,00
Because all we have to do to make that thing display

58
00:02:53,00 --> 00:02:55,10
or not display is to modify

59
00:02:55,10 --> 00:02:57,30
this variable right here that's in state.

60
00:02:57,30 --> 00:02:58,70
So we're going to set to state,

61
00:02:58,70 --> 00:03:01,90
and we'll just set formDisplay

62
00:03:01,90 --> 00:03:05,80
to the opposite of whatever

63
00:03:05,80 --> 00:03:08,30
formDisplay currently is.

64
00:03:08,30 --> 00:03:09,40
That should almost do it,

65
00:03:09,40 --> 00:03:12,90
but there's a little bit of sort of an issue.

66
00:03:12,90 --> 00:03:14,80
Something that we always have to deal with is

67
00:03:14,80 --> 00:03:18,30
the meaning of the word this when I'm using state

68
00:03:18,30 --> 00:03:22,40
inside one of these sub-methods here.

69
00:03:22,40 --> 00:03:24,60
Whenever I do that, I need to make sure that I bind

70
00:03:24,60 --> 00:03:27,90
the proper value of the sort of classes,

71
00:03:27,90 --> 00:03:33,10
this statement, instead of the internal form version of it.

72
00:03:33,10 --> 00:03:36,10
So the meaning of this in the toggleForm method

73
00:03:36,10 --> 00:03:39,90
that I'm creating is the same thing as the meaning of this

74
00:03:39,90 --> 00:03:44,10
in the class that it is contained in.

75
00:03:44,10 --> 00:03:47,80
Right, let's save that, and

76
00:03:47,80 --> 00:03:51,30
now you can see that I can toggle the form on and off

77
00:03:51,30 --> 00:03:54,00
by clicking on this right here.

