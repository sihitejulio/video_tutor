1
00:00:00,50 --> 00:00:02,70
- [Narrator] Props allow us to pass something down

2
00:00:02,70 --> 00:00:06,30
from our main component into our sub-component.

3
00:00:06,30 --> 00:00:09,30
So we're passing along the list of appointments here,

4
00:00:09,30 --> 00:00:11,10
and we're doing it through this prop

5
00:00:11,10 --> 00:00:12,70
that we called appointments.

6
00:00:12,70 --> 00:00:15,80
We send it to this list of appointments here.

7
00:00:15,80 --> 00:00:17,60
And then we do something with it.

8
00:00:17,60 --> 00:00:21,30
Now you can pass something back to the original appointments

9
00:00:21,30 --> 00:00:23,90
in order to process something like an event.

10
00:00:23,90 --> 00:00:28,00
So what we want to do here is when we click on these Xs,

11
00:00:28,00 --> 00:00:34,00
we want to pass along the record to the parent app function,

12
00:00:34,00 --> 00:00:36,30
and then modify the data.

13
00:00:36,30 --> 00:00:39,50
That will actually cause a re-render of this list

14
00:00:39,50 --> 00:00:42,70
automatically, and it will update our list for us.

15
00:00:42,70 --> 00:00:44,70
So to do that, we're going to need

16
00:00:44,70 --> 00:00:48,30
to add something here on this button.

17
00:00:48,30 --> 00:00:52,30
And it's going to be an on click event.

18
00:00:52,30 --> 00:00:55,60
This is similar to the JavaScript on click event.

19
00:00:55,60 --> 00:00:58,30
Ans we'll use an arrow function here.

20
00:00:58,30 --> 00:01:00,00
Arrow functions are really convenient

21
00:01:00,00 --> 00:01:05,10
because they allow you to use the keyword this,

22
00:01:05,10 --> 00:01:08,20
without modifying what it means.

23
00:01:08,20 --> 00:01:10,00
And if you just created a normal function,

24
00:01:10,00 --> 00:01:12,10
that wouldn't be the case.

25
00:01:12,10 --> 00:01:20,80
And so we can say, this, props, and then delete appointment.

26
00:01:20,80 --> 00:01:22,60
And we'll pass it along the item.

27
00:01:22,60 --> 00:01:24,00
So delete appointment is something

28
00:01:24,00 --> 00:01:28,80
that we're going to create as a prop on app.js.

29
00:01:28,80 --> 00:01:32,90
And item is of course the individual item

30
00:01:32,90 --> 00:01:35,70
that as we go through the different appointments

31
00:01:35,70 --> 00:01:37,30
we're temporarily generating

32
00:01:37,30 --> 00:01:41,30
in this other arrow function here.

33
00:01:41,30 --> 00:01:44,70
You can also include an event variable if you want here.

34
00:01:44,70 --> 00:01:46,60
We don't need to do anything with this event.

35
00:01:46,60 --> 00:01:47,90
It's just a click.

36
00:01:47,90 --> 00:01:48,90
So we don't need to pass

37
00:01:48,90 --> 00:01:50,90
like the value of the input field or anything.

38
00:01:50,90 --> 00:01:52,80
So you don't actually need it.

39
00:01:52,80 --> 00:01:54,50
And that's why I didn't put it there.

40
00:01:54,50 --> 00:01:56,70
So once we have that, let's go ahead and save this.

41
00:01:56,70 --> 00:01:58,80
You may see an error here.

42
00:01:58,80 --> 00:02:01,00
Oh, it didn't really bother it so much.

43
00:02:01,00 --> 00:02:03,20
It doesn't care that this doesn't quite exist yet.

44
00:02:03,20 --> 00:02:05,80
So we'll go back into app.js

45
00:02:05,80 --> 00:02:07,80
and then we'll create the prop

46
00:02:07,80 --> 00:02:09,40
that we're using in list appointments.

47
00:02:09,40 --> 00:02:15,10
So this will be delete appointment.

48
00:02:15,10 --> 00:02:18,80
And it's going to be equal to something that we'll create

49
00:02:18,80 --> 00:02:22,50
in this app.js, called delete appointment.

50
00:02:22,50 --> 00:02:24,10
So just like before,

51
00:02:24,10 --> 00:02:28,10
this is the name of it in this component,

52
00:02:28,10 --> 00:02:29,90
or it's sort of the target.

53
00:02:29,90 --> 00:02:30,80
And this is actually the name

54
00:02:30,80 --> 00:02:33,80
of the event that is being sent.

55
00:02:33,80 --> 00:02:35,20
So when we call this event,

56
00:02:35,20 --> 00:02:37,00
we don't actually need to specify

57
00:02:37,00 --> 00:02:39,00
that we're receiving any parameters.

58
00:02:39,00 --> 00:02:43,50
We'll just catch them in the event that we create.

59
00:02:43,50 --> 00:02:47,50
Now we'll need to create delete appointment.

60
00:02:47,50 --> 00:02:51,00
So this will be like this.

61
00:02:51,00 --> 00:02:53,90
And we're going to receive an appointment,

62
00:02:53,90 --> 00:02:57,30
just a single appointment record.

63
00:02:57,30 --> 00:03:01,20
Now because I can't modify state directly,

64
00:03:01,20 --> 00:03:04,40
I can't just take the my appointments variable

65
00:03:04,40 --> 00:03:06,70
and change it so that it doesn't have

66
00:03:06,70 --> 00:03:08,30
the record that we want,

67
00:03:08,30 --> 00:03:11,00
we're going to need to create a temporary variable here.

68
00:03:11,00 --> 00:03:15,20
So we'll say, let temp appointment,

69
00:03:15,20 --> 00:03:16,80
I'll call it appointments.

70
00:03:16,80 --> 00:03:24,30
And then we'll say, this.state.my appointments.

71
00:03:24,30 --> 00:03:27,20
And I'm going to use a library called lodash,

72
00:03:27,20 --> 00:03:31,20
which has a fantastic method called without

73
00:03:31,20 --> 00:03:36,70
that allows me to take an array,

74
00:03:36,70 --> 00:03:41,10
and then feed it a record that I want to delete.

75
00:03:41,10 --> 00:03:46,30
And it will return the record without this appointment.

76
00:03:46,30 --> 00:03:48,40
And that's because honestly,

77
00:03:48,40 --> 00:03:53,10
creating a method that deletes matching records

78
00:03:53,10 --> 00:03:57,80
from a list of array objects, is a little bit difficult.

79
00:03:57,80 --> 00:03:59,00
And I love lodash.

80
00:03:59,00 --> 00:04:02,20
So we'll need to import it up here.

81
00:04:02,20 --> 00:04:04,40
And we can just import the part that we need.

82
00:04:04,40 --> 00:04:13,50
We can say, import without from lodash.

83
00:04:13,50 --> 00:04:19,00
So now we are able to use this method right here.

84
00:04:19,00 --> 00:04:23,40
Now once we do that, then we'll have an array

85
00:04:23,40 --> 00:04:26,00
without the record that we're deleting.

86
00:04:26,00 --> 00:04:33,80
So we'll have to go ahead and set state.

87
00:04:33,80 --> 00:04:38,10
We'll now set my appointments to temp appointments,

88
00:04:38,10 --> 00:04:38,90
which is great.

89
00:04:38,90 --> 00:04:41,40
So this is almost going to work.

90
00:04:41,40 --> 00:04:43,50
Let's go ahead and save it so you can see

91
00:04:43,50 --> 00:04:45,30
that there's going to be a problem

92
00:04:45,30 --> 00:04:47,50
when we try to delete something.

93
00:04:47,50 --> 00:04:50,50
And this is sort of a weird error.

94
00:04:50,50 --> 00:04:52,70
You'll see this error probably a lot

95
00:04:52,70 --> 00:04:53,90
when you work with react.

96
00:04:53,90 --> 00:04:55,70
It's saying that it can't read the property,

97
00:04:55,70 --> 00:04:56,90
my appointments of undefined.

98
00:04:56,90 --> 00:04:58,50
So this something that happens

99
00:04:58,50 --> 00:05:03,70
because of what we're doing with the keyword this,

100
00:05:03,70 --> 00:05:06,80
inside this method right here.

101
00:05:06,80 --> 00:05:09,00
So in this function the keyword this

102
00:05:09,00 --> 00:05:14,10
doesn't refer to the object that we're creating over here.

103
00:05:14,10 --> 00:05:18,10
It refers to something inside this method.

104
00:05:18,10 --> 00:05:20,90
And that's a tricky thing about the keyword this.

105
00:05:20,90 --> 00:05:24,30
So we'll need to pass the version of the this variable

106
00:05:24,30 --> 00:05:27,30
that has this entire component in it,

107
00:05:27,30 --> 00:05:30,00
which will have access to all of our other methods

108
00:05:30,00 --> 00:05:32,50
and our data and state.

109
00:05:32,50 --> 00:05:35,40
So the way you do that is in the constructor,

110
00:05:35,40 --> 00:05:36,90
after you set the state,

111
00:05:36,90 --> 00:05:39,00
you can create something like this.

112
00:05:39,00 --> 00:05:41,50
This delete appointment is going to be

113
00:05:41,50 --> 00:05:44,90
equal to this delete appointment.

114
00:05:44,90 --> 00:05:49,20
We're going to bind the current value of the keyword this,

115
00:05:49,20 --> 00:05:53,30
which then sends the entire current object,

116
00:05:53,30 --> 00:05:57,80
which means that when we are referring to it in this line.

117
00:05:57,80 --> 00:06:00,30
And I know whenever I explain

118
00:06:00,30 --> 00:06:02,00
something about the keyword this,

119
00:06:02,00 --> 00:06:05,60
I have to really watch what I say, because it's confusing.

120
00:06:05,60 --> 00:06:08,20
So now when we say this here,

121
00:06:08,20 --> 00:06:11,20
it actually means the current object,

122
00:06:11,20 --> 00:06:16,60
which will have the ability to use this set state method.

123
00:06:16,60 --> 00:06:19,20
So let's save that.

124
00:06:19,20 --> 00:06:21,70
And let's try deleting one of these.

125
00:06:21,70 --> 00:06:23,90
So now you can see that it works.

126
00:06:23,90 --> 00:06:25,40
I really love lodash.

127
00:06:25,40 --> 00:06:28,50
Also, this shows you that you can import libraries

128
00:06:28,50 --> 00:06:30,10
and not just use them in your templates,

129
00:06:30,10 --> 00:06:34,00
but you can use them in your regular JavaScript methods.

