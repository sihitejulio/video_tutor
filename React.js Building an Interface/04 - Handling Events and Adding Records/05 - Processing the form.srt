1
00:00:00,50 --> 00:00:02,80
- Let's finish up the AddAppointment component

2
00:00:02,80 --> 00:00:05,90
by allowing us to submit this form right here.

3
00:00:05,90 --> 00:00:09,80
So, I'm going to scroll down to the form tag,

4
00:00:09,80 --> 00:00:12,00
right here on this component,

5
00:00:12,00 --> 00:00:14,50
and I'm going to add an onSubmit event

6
00:00:14,50 --> 00:00:22,20
and this is going to call a method called handleAdd.

7
00:00:22,20 --> 00:00:26,70
So, we'll need to create that in this component.

8
00:00:26,70 --> 00:00:28,80
We'll go ahead and do that right here.

9
00:00:28,80 --> 00:00:31,20
This will be HandleAdd, it'll receive

10
00:00:31,20 --> 00:00:34,10
the event information from the submission.

11
00:00:34,10 --> 00:00:35,10
And the first thing I need to do

12
00:00:35,10 --> 00:00:38,50
is prevent the default behavior from happening here.

13
00:00:38,50 --> 00:00:41,30
This will prevent the form from submitting

14
00:00:41,30 --> 00:00:43,20
and reloading the page 'cause that's

15
00:00:43,20 --> 00:00:47,00
what will normally happen when somebody submits a form.

16
00:00:47,00 --> 00:00:49,40
We don't want a page reload, so we'll do that,

17
00:00:49,40 --> 00:00:54,40
and I'm going to create a temporary variable here.

18
00:00:54,40 --> 00:00:57,00
Again, I can modify the state directly,

19
00:00:57,00 --> 00:01:01,60
so this is just going to say, petName

20
00:01:01,60 --> 00:01:06,20
and reference the current value of the element in the form.

21
00:01:06,20 --> 00:01:11,80
So, this.state.petName has the information for the form,

22
00:01:11,80 --> 00:01:18,50
and then each of the other fields will follow.

23
00:01:18,50 --> 00:01:21,90
Now, when it comes to the date,

24
00:01:21,90 --> 00:01:29,70
I'm going to combine the date and the time together.

25
00:01:29,70 --> 00:01:37,40
Just separated by a space.

26
00:01:37,40 --> 00:01:40,10
Once we have this variable, then we will go ahead

27
00:01:40,10 --> 00:01:42,00
and pass it through a prop

28
00:01:42,00 --> 00:01:47,60
that we're going to create called addAppointment.

29
00:01:47,60 --> 00:01:50,60
We'll pass this temporary appointment,

30
00:01:50,60 --> 00:01:52,20
and then we'll clear out the form,

31
00:01:52,20 --> 00:01:55,90
so we'll just say, setState,

32
00:01:55,90 --> 00:01:58,60
pass along an object, and I'm actually going to just copy

33
00:01:58,60 --> 00:02:00,90
that from up here, 'cause it's going to be the same

34
00:02:00,90 --> 00:02:05,20
as the initialization of the form elements.

35
00:02:05,20 --> 00:02:08,60
And then finally, when we're done with this,

36
00:02:08,60 --> 00:02:11,20
then we're going to go ahead

37
00:02:11,20 --> 00:02:15,40
and issue the toggleForm method, so that will hide the form,

38
00:02:15,40 --> 00:02:18,00
or it will collapse the inside of this form

39
00:02:18,00 --> 00:02:21,10
so we can see the rest of the list

40
00:02:21,10 --> 00:02:23,30
that will have this new appointment.

41
00:02:23,30 --> 00:02:26,20
Course if we do this, we need to go ahead

42
00:02:26,20 --> 00:02:32,80
and make a change to the meaning of the variable, this,

43
00:02:32,80 --> 00:02:40,90
inside this component, so we'll say, handleAdd here,

44
00:02:40,90 --> 00:02:46,70
and bind the, this, keyword properly.

45
00:02:46,70 --> 00:02:51,30
So this is now ready, let's go ahead and save it,

46
00:02:51,30 --> 00:02:53,10
and it means that we have to go now

47
00:02:53,10 --> 00:02:56,80
into the app, that .js file.

48
00:02:56,80 --> 00:03:05,80
We'll start from the addAppointment component here.

49
00:03:05,80 --> 00:03:09,60
So this will execute the local addAppointment method

50
00:03:09,60 --> 00:03:10,90
that we'll create in just a minute,

51
00:03:10,90 --> 00:03:14,20
so this is the prop right here and it's going to receive

52
00:03:14,20 --> 00:03:19,40
from the internal component whatever we pass to this,

53
00:03:19,40 --> 00:03:24,30
which means that then we have to create this in here.

54
00:03:24,30 --> 00:03:27,90
It's going to receive an appointment and, again,

55
00:03:27,90 --> 00:03:31,80
we'll create a temporary appointment variable,

56
00:03:31,80 --> 00:03:35,00
it'll receive the current value of myAppointments,

57
00:03:35,00 --> 00:03:37,60
and I need to add to the appointment

58
00:03:37,60 --> 00:03:41,30
that I am creating an appointment ID,

59
00:03:41,30 --> 00:03:45,20
and it's going to reference the lastIndex variable

60
00:03:45,20 --> 00:03:48,80
that I've been keeping track of.

61
00:03:48,80 --> 00:03:52,20
So, the form doesn't have an appointment ID field,

62
00:03:52,20 --> 00:03:55,00
so I need to feed it, and I'm using this lastIndex

63
00:03:55,00 --> 00:03:58,00
that I use when I loop through the data

64
00:03:58,00 --> 00:04:00,00
that I receive from the file

65
00:04:00,00 --> 00:04:03,60
and created an index for each of the items.

66
00:04:03,60 --> 00:04:06,20
So once I have that, all I need to do

67
00:04:06,20 --> 00:04:12,70
is just push the element into the array.

68
00:04:12,70 --> 00:04:15,70
I'll use the unshift method from JavaScript,

69
00:04:15,70 --> 00:04:18,20
pass along the appointment, that'll put the appointment

70
00:04:18,20 --> 00:04:21,20
at the beginning of the array,

71
00:04:21,20 --> 00:04:32,90
and then I can setState with this new version of the array.

72
00:04:32,90 --> 00:04:33,90
I also need to go ahead

73
00:04:33,90 --> 00:04:38,40
and modify the lastIndex

74
00:04:38,40 --> 00:04:43,20
so that it increments,

75
00:04:43,20 --> 00:04:45,10
so that the next time we insert something,

76
00:04:45,10 --> 00:04:50,20
this will be ready.

77
00:04:50,20 --> 00:04:53,10
And finally, so I need to make sure

78
00:04:53,10 --> 00:04:58,00
that I modified the value of the keyword, this,

79
00:04:58,00 --> 00:05:02,30
inside addApointment,

80
00:05:02,30 --> 00:05:04,80
and then I'll save it.

81
00:05:04,80 --> 00:05:06,40
So, let's go ahead and try to add an appointment.

82
00:05:06,40 --> 00:05:15,30
We'll open this up, type in some information here.

83
00:05:15,30 --> 00:05:17,40
And we'll hit Add Appointment,

84
00:05:17,40 --> 00:05:19,40
and you can see the appointment is right here,

85
00:05:19,40 --> 00:05:22,60
also the form collapsed and if you open this up,

86
00:05:22,60 --> 00:05:26,50
you'll notice that it is a clear form.

87
00:05:26,50 --> 00:05:29,20
All that was handled through the addAppointment component

88
00:05:29,20 --> 00:05:31,90
and everything we just did is really just a review,

89
00:05:31,90 --> 00:05:34,60
but it's pretty much what you do all the time

90
00:05:34,60 --> 00:05:37,00
when you're working with react components.

