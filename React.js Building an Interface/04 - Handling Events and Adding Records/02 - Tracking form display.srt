1
00:00:00,50 --> 00:00:01,40
- [Instructor] It's time to start working on this

2
00:00:01,40 --> 00:00:03,40
AddAppointments component.

3
00:00:03,40 --> 00:00:05,90
So, to get things going I've got a special

4
00:00:05,90 --> 00:00:08,10
just for you right here.

5
00:00:08,10 --> 00:00:10,60
So, I'm going to hit this Raw button,

6
00:00:10,60 --> 00:00:12,20
copy all this code.

7
00:00:12,20 --> 00:00:15,10
I've already modified the class keyword in HTML

8
00:00:15,10 --> 00:00:16,80
to className for you.

9
00:00:16,80 --> 00:00:20,00
So, I'm going to take this div right here

10
00:00:20,00 --> 00:00:22,80
and I'm going to replace it with some parenthesis

11
00:00:22,80 --> 00:00:25,50
because we have multi-line content here.

12
00:00:25,50 --> 00:00:28,60
And then, I'm going to paste that code and save it.

13
00:00:28,60 --> 00:00:33,10
That's going to show you the form right here.

14
00:00:33,10 --> 00:00:35,30
And the way that we want to do this is,

15
00:00:35,30 --> 00:00:39,40
modify this form so that I can toggle it's display

16
00:00:39,40 --> 00:00:41,50
by clicking on this header right here.

17
00:00:41,50 --> 00:00:43,10
So if you look at the code I have,

18
00:00:43,10 --> 00:00:45,90
sort of this className here with the header.

19
00:00:45,90 --> 00:00:48,20
And then I have this other section with the body

20
00:00:48,20 --> 00:00:52,30
which is the actual form, and what I want to do is

21
00:00:52,30 --> 00:00:55,70
toggle the display by clicking on this header,

22
00:00:55,70 --> 00:01:00,00
but the header's actually inside this parent class.

23
00:01:00,00 --> 00:01:03,90
So what I'll do is I'll add a class in here,

24
00:01:03,90 --> 00:01:08,30
and this class is going to be called add-appointment.

25
00:01:08,30 --> 00:01:11,60
And when that class is on this form,

26
00:01:11,60 --> 00:01:15,40
you can see that the body, this section right here,

27
00:01:15,40 --> 00:01:17,00
will automatically hide.

28
00:01:17,00 --> 00:01:20,30
If we take a look at the CSS you'll see that I have

29
00:01:20,30 --> 00:01:22,60
that class happening right here at the very top.

30
00:01:22,60 --> 00:01:26,30
It says if there is an add-appointment class

31
00:01:26,30 --> 00:01:29,40
on a parent element and it has an element with a class

32
00:01:29,40 --> 00:01:31,60
of card-body, go ahead and hide it.

33
00:01:31,60 --> 00:01:34,10
So, we're going to need to do that but obviously

34
00:01:34,10 --> 00:01:36,70
we need to manage it with code,

35
00:01:36,70 --> 00:01:38,40
instead of manually like this.

36
00:01:38,40 --> 00:01:42,20
So, to take care of that we're going to need a variable

37
00:01:42,20 --> 00:01:45,20
that we're going to track instead of outputting

38
00:01:45,20 --> 00:01:49,30
this literal className, sort of group right here.

39
00:01:49,30 --> 00:01:53,60
So this is going to be an expression and in here

40
00:01:53,60 --> 00:01:56,80
we're going to say, okay, go ahead and output the normal

41
00:01:56,80 --> 00:01:59,00
set of classes here.

42
00:01:59,00 --> 00:02:01,00
I'm going to make sure that I add a space at the end

43
00:02:01,00 --> 00:02:05,00
so that I can add or not add a class at the end of this.

44
00:02:05,00 --> 00:02:10,30
And then I'm going to say, look for a variable,

45
00:02:10,30 --> 00:02:15,70
that I'm going to call formDisplay.

46
00:02:15,70 --> 00:02:18,40
And this is something I'll be passing through the props.

47
00:02:18,40 --> 00:02:21,20
The reason I'm doing that is because I want to be able

48
00:02:21,20 --> 00:02:23,00
to control that from the main component,

49
00:02:23,00 --> 00:02:25,90
not just from within this component,

50
00:02:25,90 --> 00:02:28,60
just in case I want to clear that class

51
00:02:28,60 --> 00:02:31,40
depending on what I'm doing with the rest

52
00:02:31,40 --> 00:02:33,00
of the application.

53
00:02:33,00 --> 00:02:37,00
So, this.props.formDisplay and if that's true,

54
00:02:37,00 --> 00:02:40,20
then don't output the class, if it's false

55
00:02:40,20 --> 00:02:45,80
then add-appointment will be added to this

56
00:02:45,80 --> 00:02:47,80
sort of parent class here.

57
00:02:47,80 --> 00:02:50,60
So, let's go ahead and save that.

58
00:02:50,60 --> 00:02:53,40
And you can see that now, even though I don't have

59
00:02:53,40 --> 00:02:56,50
that variable the form automatically hides

60
00:02:56,50 --> 00:03:00,20
because, technically, it's evaluating as false

61
00:03:00,20 --> 00:03:02,80
so this add-appointment should be there.

62
00:03:02,80 --> 00:03:06,40
It also means that we have to go into App.js

63
00:03:06,40 --> 00:03:10,90
and modify the state so that we actually have that variable

64
00:03:10,90 --> 00:03:14,30
and we're going to need to pass it to the child component.

65
00:03:14,30 --> 00:03:18,40
So right after this array I'm going to add

66
00:03:18,40 --> 00:03:25,50
a state variable called formDisplay, set it to false

67
00:03:25,50 --> 00:03:28,70
and then,

68
00:03:28,70 --> 00:03:32,60
right here in the component I'm going

69
00:03:32,60 --> 00:03:36,80
to add formDisplay,

70
00:03:36,80 --> 00:03:42,50
is equal to this.state.formDisplay.

71
00:03:42,50 --> 00:03:46,80
So now this AddAppointments component or sub-component

72
00:03:46,80 --> 00:03:50,40
is tracking a variable in this state

73
00:03:50,40 --> 00:03:52,70
and we've called this variable formDisplay,

74
00:03:52,70 --> 00:03:55,70
passing along as a prop and depending on the value

75
00:03:55,70 --> 00:03:58,30
of that variable our form is going to display

76
00:03:58,30 --> 00:03:59,40
or not display.

77
00:03:59,40 --> 00:04:03,20
So if I come over here and set this thing to true,

78
00:04:03,20 --> 00:04:05,70
you should see the form display.

79
00:04:05,70 --> 00:04:09,40
If it's set to false, then the form hides.

80
00:04:09,40 --> 00:04:12,50
That's cool but we also, obviously, need to find

81
00:04:12,50 --> 00:04:15,80
a way of toggling that, turning it on and off.

82
00:04:15,80 --> 00:04:18,20
And that's going to happen when we click on this header

83
00:04:18,20 --> 00:04:19,10
right here.

84
00:04:19,10 --> 00:04:21,30
And it may also be caused by doing different things,

85
00:04:21,30 --> 00:04:24,20
so for example, when we submit the form I want this

86
00:04:24,20 --> 00:04:26,10
to be hidden so that it doesn't take up

87
00:04:26,10 --> 00:04:27,50
so much vertical room.

88
00:04:27,50 --> 00:04:30,00
So, let's do that in the next video.

