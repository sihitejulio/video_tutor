1
00:00:00,50 --> 00:00:03,00
- [Ray] React is a revolutionary framework

2
00:00:03,00 --> 00:00:05,10
that introduces some important concepts

3
00:00:05,10 --> 00:00:07,50
that have quickly helped to dominate the world

4
00:00:07,50 --> 00:00:09,20
of front end developers.

5
00:00:09,20 --> 00:00:11,70
Hey there, I'm Ray Villalobos and in this course,

6
00:00:11,70 --> 00:00:14,70
I'm going to show you the basics of working with React

7
00:00:14,70 --> 00:00:16,50
by creating an interface

8
00:00:16,50 --> 00:00:19,00
that solves a common web development task.

9
00:00:19,00 --> 00:00:21,60
Managing appointments for a company.

10
00:00:21,60 --> 00:00:25,10
In our case, making appointments for a pet doctor.

11
00:00:25,10 --> 00:00:27,40
In order to take care of that, we'll create a form

12
00:00:27,40 --> 00:00:29,40
that lets people add appointments,

13
00:00:29,40 --> 00:00:31,70
read a list of appointments from a file

14
00:00:31,70 --> 00:00:34,80
and also delete, modify, search,

15
00:00:34,80 --> 00:00:36,90
and sort those appointments.

16
00:00:36,90 --> 00:00:40,20
Now this course is meant to be a practical hands on guide

17
00:00:40,20 --> 00:00:43,30
that shows you how React works using plain language.

18
00:00:43,30 --> 00:00:44,80
It's different than other courses,

19
00:00:44,80 --> 00:00:47,00
because building something realistic

20
00:00:47,00 --> 00:00:49,20
shows you how things are connected

21
00:00:49,20 --> 00:00:51,80
and how to solve real problems with React.

22
00:00:51,80 --> 00:00:53,60
I hope you take the time to work through the problems

23
00:00:53,60 --> 00:00:56,10
with me as I go through the course.

24
00:00:56,10 --> 00:00:58,90
Now this course is also part of a series of courses

25
00:00:58,90 --> 00:01:02,10
where I've built similar applications in different languages

26
00:01:02,10 --> 00:01:05,20
like JQuery, Angular and Vue.js.

27
00:01:05,20 --> 00:01:07,00
That means that going through these courses

28
00:01:07,00 --> 00:01:09,10
can help you look at the advantages

29
00:01:09,10 --> 00:01:11,70
that one framework has over the others.

30
00:01:11,70 --> 00:01:13,30
If that sounds like something you'd want to try,

31
00:01:13,30 --> 00:01:15,00
then it's time to get started.

