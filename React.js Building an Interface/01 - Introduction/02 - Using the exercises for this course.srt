1
00:00:00,50 --> 00:00:01,50
- [Instructor] All of the exercises

2
00:00:01,50 --> 00:00:04,00
for this course are freely available

3
00:00:04,00 --> 00:00:06,20
on GitHub at this URL.

4
00:00:06,20 --> 00:00:08,30
This repository has branches for each

5
00:00:08,30 --> 00:00:09,90
of the videos in the course.

6
00:00:09,90 --> 00:00:12,10
You can use the Branch popup menu

7
00:00:12,10 --> 00:00:14,20
to switch to a specific branch

8
00:00:14,20 --> 00:00:21,60
and take a look at the course at that stage.

9
00:00:21,60 --> 00:00:22,90
Most of the files that we're working

10
00:00:22,90 --> 00:00:26,80
on are in this source folder, and we're probably going to

11
00:00:26,80 --> 00:00:28,50
be working the most with the files

12
00:00:28,50 --> 00:00:32,80
on the Components folder.

13
00:00:32,80 --> 00:00:34,40
If you go back to this page, you can click

14
00:00:34,40 --> 00:00:36,60
on this drop-down link right here,

15
00:00:36,60 --> 00:00:39,50
and download a copy of what the files look

16
00:00:39,50 --> 00:00:43,70
at that branch by clicking on this button right here.

17
00:00:43,70 --> 00:00:45,70
The branches are structured so that they correspond

18
00:00:45,70 --> 00:00:47,30
to the videos in the course.

19
00:00:47,30 --> 00:00:51,20
So for example, if I have a branch named 02_03,

20
00:00:51,20 --> 00:00:53,70
then that branch corresponds to the second chapter

21
00:00:53,70 --> 00:00:56,10
and the third video in that chapter.

22
00:00:56,10 --> 00:00:58,10
The extra letter at the end of the name

23
00:00:58,10 --> 00:01:00,20
corresponds to the state of the branch.

24
00:01:00,20 --> 00:01:02,00
The letter B means that this is how

25
00:01:02,00 --> 00:01:04,20
the code looks at the beginning of the video,

26
00:01:04,20 --> 00:01:07,20
and the letter E means that is how the code look

27
00:01:07,20 --> 00:01:09,00
at the end of the video.

28
00:01:09,00 --> 00:01:11,80
The master branch usually has the final version

29
00:01:11,80 --> 00:01:19,00
of the code when I finish the course.

30
00:01:19,00 --> 00:01:22,20
It also has a detailed README document,

31
00:01:22,20 --> 00:01:24,60
including instructions for advanced users

32
00:01:24,60 --> 00:01:27,20
who know how to use Git so that they can download

33
00:01:27,20 --> 00:01:29,40
all the branches at once.

34
00:01:29,40 --> 00:01:30,90
Before you run the project, make sure

35
00:01:30,90 --> 00:01:34,60
that you have Node.js, Git as well as Create React

36
00:01:34,60 --> 00:01:37,10
App installed, use these links

37
00:01:37,10 --> 00:01:40,30
to get to those applications.

38
00:01:40,30 --> 00:01:43,70
If you do download a ZIP file to your local directory,

39
00:01:43,70 --> 00:01:46,30
make sure that you run the npm install

40
00:01:46,30 --> 00:01:49,00
on the folder that you downloaded.

41
00:01:49,00 --> 00:01:51,00
That creates the Node Modules folder

42
00:01:51,00 --> 00:01:52,60
for your application.

43
00:01:52,60 --> 00:01:56,00
After that, you can run npm start on your terminal.

