1
00:00:00,50 --> 00:00:01,90
- [Ray] Hey there, this is Ray Villalobos.

2
00:00:01,90 --> 00:00:03,60
Thanks for watchin' this course.

3
00:00:03,60 --> 00:00:06,80
We have a ton of other React content in the library,

4
00:00:06,80 --> 00:00:08,70
so if you want to learn more about it,

5
00:00:08,70 --> 00:00:12,60
make sure you do a search for React on our site.

6
00:00:12,60 --> 00:00:16,60
You may want to take a look at this React SPAs course.

7
00:00:16,60 --> 00:00:18,80
It's a course that takes what we've learned here

8
00:00:18,80 --> 00:00:21,80
a little bit further, showing you how to connect a database,

9
00:00:21,80 --> 00:00:24,60
and add registration and authentication

10
00:00:24,60 --> 00:00:26,30
to your React projects.

11
00:00:26,30 --> 00:00:28,80
As I mentioned before, this course is part of a series

12
00:00:28,80 --> 00:00:32,60
of related courses, where I build a similar application

13
00:00:32,60 --> 00:00:34,60
using different frameworks.

14
00:00:34,60 --> 00:00:36,30
It can give you a really good idea

15
00:00:36,30 --> 00:00:40,00
of what the differences between Vue, React, Agular,

16
00:00:40,00 --> 00:00:43,00
and even jQuery are, in a practical way.

17
00:00:43,00 --> 00:00:45,40
The best way to find those is to just do a search

18
00:00:45,40 --> 00:00:50,10
for by Ray Villalobos in the library.

19
00:00:50,10 --> 00:00:53,20
You should be able to see a list of most of my courses.

20
00:00:53,20 --> 00:00:56,80
You can also click on any existing course

21
00:00:56,80 --> 00:00:59,70
and then look for my photo where you can find a list

22
00:00:59,70 --> 00:01:02,60
of all of my latest courses.

23
00:01:02,60 --> 00:01:04,40
If you want to know what else I'm up to,

24
00:01:04,40 --> 00:01:07,20
check out my personal website at Raybo.org,

25
00:01:07,20 --> 00:01:09,50
where I have a list of all of my courses,

26
00:01:09,50 --> 00:01:13,00
as well as articles, and all the social media

27
00:01:13,00 --> 00:01:15,10
that you need to get in touch with me

28
00:01:15,10 --> 00:01:16,50
at the bottom of the page.

29
00:01:16,50 --> 00:01:19,00
Once again, thanks for watching this course.

