1
00:00:00,50 --> 00:00:01,70
- [Instructor] One thing that's interesting

2
00:00:01,70 --> 00:00:03,90
about creating lists like this

3
00:00:03,90 --> 00:00:08,80
is that React doesn't like them not to have an index

4
00:00:08,80 --> 00:00:12,20
or sort of an ID for each of the elements,

5
00:00:12,20 --> 00:00:14,60
so much so that you may have already noticed

6
00:00:14,60 --> 00:00:16,90
if you inspect this project,

7
00:00:16,90 --> 00:00:18,90
that there is an error all about it.

8
00:00:18,90 --> 00:00:20,80
So let's take a look at that.

9
00:00:20,80 --> 00:00:24,00
And it says each child in an array or iterator

10
00:00:24,00 --> 00:00:25,90
should have a unique key prop.

11
00:00:25,90 --> 00:00:28,50
So what that means is we need to create an index

12
00:00:28,50 --> 00:00:30,90
for each one of these elements.

13
00:00:30,90 --> 00:00:34,20
And we usually do that right here

14
00:00:34,20 --> 00:00:37,40
when we loop through the different events.

15
00:00:37,40 --> 00:00:41,80
So all you need to do is use the key keyword here

16
00:00:41,80 --> 00:00:44,40
and then pass along a unique ID.

17
00:00:44,40 --> 00:00:48,30
Now, our data doesn't have a unique ID in it.

18
00:00:48,30 --> 00:00:51,00
I mean, you could say maybe that the appointment date

19
00:00:51,00 --> 00:00:54,00
is sort of unique, but you could probably set up

20
00:00:54,00 --> 00:00:56,20
more than one appointment for a couple of pets

21
00:00:56,20 --> 00:00:59,10
if you have enough staff at the same time.

22
00:00:59,10 --> 00:01:02,10
Now, the names are also not going to be really unique

23
00:01:02,10 --> 00:01:05,40
because we could have two dogs named Pepe.

24
00:01:05,40 --> 00:01:08,90
So we really need to create our own index.

25
00:01:08,90 --> 00:01:10,60
Now, all you would need to do here

26
00:01:10,60 --> 00:01:14,10
is just put in the name of a unique ID,

27
00:01:14,10 --> 00:01:18,00
so we're going to create one and we'll call it appointment ID.

28
00:01:18,00 --> 00:01:21,70
And we'll have to feed it into our appointments

29
00:01:21,70 --> 00:01:23,40
before it gets to our subcomponent.

30
00:01:23,40 --> 00:01:28,30
So let's go ahead and go back into App.js.

31
00:01:28,30 --> 00:01:30,70
And when the component mounts,

32
00:01:30,70 --> 00:01:32,60
we are reading some JSON data.

33
00:01:32,60 --> 00:01:35,50
And then we are going through the data

34
00:01:35,50 --> 00:01:38,30
that we receive and mapping it over here.

35
00:01:38,30 --> 00:01:40,60
So right in here, what I'm going to do

36
00:01:40,60 --> 00:01:47,10
is add an appointment ID variable to my list.

37
00:01:47,10 --> 00:01:48,70
This is going to need an index

38
00:01:48,70 --> 00:01:50,50
that I'm going to increment every time

39
00:01:50,50 --> 00:01:52,30
I go through these items.

40
00:01:52,30 --> 00:01:55,70
So, I'm going to create a variable in the state

41
00:01:55,70 --> 00:01:58,00
that I'm going to call lastIndex.

42
00:01:58,00 --> 00:02:00,90
This is going to keep track of the index

43
00:02:00,90 --> 00:02:03,20
that I'm currently looping through.

44
00:02:03,20 --> 00:02:06,90
So that means that in state, I'm going to create

45
00:02:06,90 --> 00:02:08,90
something called lastIndex.

46
00:02:08,90 --> 00:02:12,30
And I'll initialize it with a value of zero.

47
00:02:12,30 --> 00:02:14,50
That will be my first element.

48
00:02:14,50 --> 00:02:17,90
So now, I can pass it right here.

49
00:02:17,90 --> 00:02:19,90
And now I need to modify this date.

50
00:02:19,90 --> 00:02:21,70
I can't do something like this,

51
00:02:21,70 --> 00:02:23,70
like you would do in normal JavaScript

52
00:02:23,70 --> 00:02:26,80
because we can't modify state directly.

53
00:02:26,80 --> 00:02:33,50
So what I can do is use the setState method.

54
00:02:33,50 --> 00:02:36,70
And I have to pass in an object with what I want

55
00:02:36,70 --> 00:02:40,00
to modify, which in this case would be lastIndex.

56
00:02:40,00 --> 00:02:46,90
And then I can do this.state.lastIndex plus one.

57
00:02:46,90 --> 00:02:50,50
So this, as it's looping through each of the elements,

58
00:02:50,50 --> 00:02:54,20
it's redefining the value of lastIndex.

59
00:02:54,20 --> 00:02:58,10
And let's go ahead and save this.

60
00:02:58,10 --> 00:03:01,50
Since we've created this as part of myAppointments,

61
00:03:01,50 --> 00:03:04,00
when we pass them into the subcomponent,

62
00:03:04,00 --> 00:03:06,20
it should understand them just fine.

63
00:03:06,20 --> 00:03:13,30
So let's go ahead and save this other ListAppointments here.

64
00:03:13,30 --> 00:03:16,60
And now, you should see that you don't receive the error.

65
00:03:16,60 --> 00:03:18,70
If you want to take a look at the indexes,

66
00:03:18,70 --> 00:03:20,30
you can actually print them out,

67
00:03:20,30 --> 00:03:25,60
at least temporarily, so that we can see them.

68
00:03:25,60 --> 00:03:29,90
We can add appointment ID here,

69
00:03:29,90 --> 00:03:32,60
and you can see them right here.

70
00:03:32,60 --> 00:03:36,70
So now, each one of these items has a unique ID.

71
00:03:36,70 --> 00:03:39,00
That's going to be really useful later on,

72
00:03:39,00 --> 00:03:42,00
so I'll point it out when we get to that part

73
00:03:42,00 --> 00:03:44,20
in the building of our application.

74
00:03:44,20 --> 00:03:45,90
But for right now, let's get rid of it

75
00:03:45,90 --> 00:03:49,00
so that we have a clean application.

