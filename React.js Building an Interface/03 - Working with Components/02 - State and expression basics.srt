1
00:00:00,50 --> 00:00:02,10
- [Ray] There's a couple of key concepts

2
00:00:02,10 --> 00:00:05,60
that make React a great place to develop applications,

3
00:00:05,60 --> 00:00:08,30
and one of those concepts is state.

4
00:00:08,30 --> 00:00:11,70
State is what's going on with your application,

5
00:00:11,70 --> 00:00:16,20
so it refers to essentially an object that has some data

6
00:00:16,20 --> 00:00:19,30
that defines what your application is doing,

7
00:00:19,30 --> 00:00:22,70
and that correlates to the virtual DOM,

8
00:00:22,70 --> 00:00:24,80
which is another feature in React

9
00:00:24,80 --> 00:00:28,10
that will automatically modify your components

10
00:00:28,10 --> 00:00:31,30
when that data, or that state, changes.

11
00:00:31,30 --> 00:00:33,00
So let's take a look at how that works.

12
00:00:33,00 --> 00:00:35,70
Now I've got this class here that I've created,

13
00:00:35,70 --> 00:00:38,90
and in order to initialize state,

14
00:00:38,90 --> 00:00:42,80
we need to go ahead and use the constructor method here.

15
00:00:42,80 --> 00:00:45,40
So this is common to object

16
00:00:45,40 --> 00:00:48,20
or enter applications with classes.

17
00:00:48,20 --> 00:00:50,60
The constructor is where you initialize things,

18
00:00:50,60 --> 00:00:52,70
and when you use a constructor

19
00:00:52,70 --> 00:00:54,90
and you want to use the this keyword

20
00:00:54,90 --> 00:00:57,90
to access things inside the component,

21
00:00:57,90 --> 00:01:00,90
you need to use this super method.

22
00:01:00,90 --> 00:01:03,10
So super allows you to get information

23
00:01:03,10 --> 00:01:06,40
from the parent component, and it's also

24
00:01:06,40 --> 00:01:09,60
where you can pass things from other components

25
00:01:09,60 --> 00:01:13,00
to this component, and it allows you to use

26
00:01:13,00 --> 00:01:14,20
the this keyword.

27
00:01:14,20 --> 00:01:19,00
So next we can modify this special variable called state

28
00:01:19,00 --> 00:01:22,90
by using this and then calling state

29
00:01:22,90 --> 00:01:26,70
and then initializing an object that defines

30
00:01:26,70 --> 00:01:29,90
what kind of information our application has.

31
00:01:29,90 --> 00:01:31,40
So in here I'll just create right now

32
00:01:31,40 --> 00:01:34,70
a variable called myName, and I'll assign it

33
00:01:34,70 --> 00:01:38,20
a value of Ray, 'cause that's my name.

34
00:01:38,20 --> 00:01:41,10
And so now this becomes an element

35
00:01:41,10 --> 00:01:43,80
that's available inside my components.

36
00:01:43,80 --> 00:01:47,10
So over here I can use an expression,

37
00:01:47,10 --> 00:01:50,50
and in JSX to use an expression uses curly braces.

38
00:01:50,50 --> 00:01:52,40
You've seen them before, we had them

39
00:01:52,40 --> 00:01:54,40
when we installed Create React App,

40
00:01:54,40 --> 00:01:57,90
we had this like logo that was assigned to a variable,

41
00:01:57,90 --> 00:02:00,50
and we used it inside an expression.

42
00:02:00,50 --> 00:02:03,10
So what we can do here is refer to that object,

43
00:02:03,10 --> 00:02:06,50
this.state, and then we created this variable

44
00:02:06,50 --> 00:02:12,70
called myName in there, and if we save that

45
00:02:12,70 --> 00:02:16,20
you'll see that now it will display my name.

46
00:02:16,20 --> 00:02:18,20
Now the cool thing about React is that

47
00:02:18,20 --> 00:02:22,90
if anything in the application modifies this variable,

48
00:02:22,90 --> 00:02:27,50
React will automatically redraw any part of the application

49
00:02:27,50 --> 00:02:29,60
that it needs to that uses that variable.

50
00:02:29,60 --> 00:02:32,70
And this is super cool, so the only way that I can

51
00:02:32,70 --> 00:02:37,10
show you this is by looking at the React developer tools,

52
00:02:37,10 --> 00:02:39,40
which you should have installed if you went through

53
00:02:39,40 --> 00:02:43,50
the installation process, and you should see this icon

54
00:02:43,50 --> 00:02:46,40
in your extensions in Google Chrome.

55
00:02:46,40 --> 00:02:50,20
To access that, I'm going to right click and select inspect.

56
00:02:50,20 --> 00:02:53,20
And we'll get the normal inspect sort of tab here.

57
00:02:53,20 --> 00:02:54,80
Let's see if I can put that at the bottom,

58
00:02:54,80 --> 00:02:57,70
so I'm going to click right here and put it at the bottom

59
00:02:57,70 --> 00:02:59,50
to get a little bit more room.

60
00:02:59,50 --> 00:03:03,10
So here is the default sort of developer tools,

61
00:03:03,10 --> 00:03:05,10
and if this was wide enough, you would see that

62
00:03:05,10 --> 00:03:08,30
the last one is this React tab.

63
00:03:08,30 --> 00:03:12,10
So if we click on that, you'll be able to see that

64
00:03:12,10 --> 00:03:14,80
the application right now, if we open this up,

65
00:03:14,80 --> 00:03:17,50
you'll notice that it has these different components

66
00:03:17,50 --> 00:03:20,00
over here, the components that we created.

67
00:03:20,00 --> 00:03:24,10
But because we've created this state in the main part

68
00:03:24,10 --> 00:03:26,20
of the application, on the right hand side,

69
00:03:26,20 --> 00:03:29,10
you can see that we have this state right here.

70
00:03:29,10 --> 00:03:30,60
There's also something called props,

71
00:03:30,60 --> 00:03:32,30
which we'll cover later.

72
00:03:32,30 --> 00:03:35,10
And you can see that the state

73
00:03:35,10 --> 00:03:38,70
of the current component has this thing called myName.

74
00:03:38,70 --> 00:03:41,80
So let's go ahead and modify this to something else.

75
00:03:41,80 --> 00:03:46,10
So I'll use my middle name, which happens to be Herman.

76
00:03:46,10 --> 00:03:48,50
And as soon as I modify that here,

77
00:03:48,50 --> 00:03:52,80
you'll notice that my application was automatically redrawn.

78
00:03:52,80 --> 00:03:55,10
And that's really, really powerful.

79
00:03:55,10 --> 00:03:58,20
It doesn't seem that way, but what's happening here

80
00:03:58,20 --> 00:04:02,40
is that because I've modified the value of this variable,

81
00:04:02,40 --> 00:04:05,30
React automatically noticed that I did that,

82
00:04:05,30 --> 00:04:08,00
and decided to redraw this component,

83
00:04:08,00 --> 00:04:11,60
because this component was using this variable

84
00:04:11,60 --> 00:04:13,60
automatically for us.

85
00:04:13,60 --> 00:04:16,60
And the reason that's powerful is because

86
00:04:16,60 --> 00:04:19,70
it allows you to build things like a component

87
00:04:19,70 --> 00:04:23,60
that displays a list of pieces of data,

88
00:04:23,60 --> 00:04:26,90
and if something like, for example, adding

89
00:04:26,90 --> 00:04:30,40
a piece of data changes that list,

90
00:04:30,40 --> 00:04:33,30
you don't have to tell this component to redraw itself.

91
00:04:33,30 --> 00:04:35,40
It'll automatically notice that

92
00:04:35,40 --> 00:04:37,90
you have added an element to that list,

93
00:04:37,90 --> 00:04:39,70
and will automatically redraw.

94
00:04:39,70 --> 00:04:42,00
And we'll see how that works in React.

95
00:04:42,00 --> 00:04:43,70
It's one of the most important features

96
00:04:43,70 --> 00:04:45,70
in React, and I just wanted to give you

97
00:04:45,70 --> 00:04:49,00
a quick introduction to how that works.

