1
00:00:00,80 --> 00:00:02,70
- [Instructor] Let's work on making this component

2
00:00:02,70 --> 00:00:04,90
a little bit more interesting.

3
00:00:04,90 --> 00:00:06,70
I'm going to go ahead and

4
00:00:06,70 --> 00:00:10,40
write all of this code that you see up here

5
00:00:10,40 --> 00:00:14,80
inside my template section, so in this return section.

6
00:00:14,80 --> 00:00:17,90
To do that, I'm going to need some HTML

7
00:00:17,90 --> 00:00:19,60
that I've created here for you.

8
00:00:19,60 --> 00:00:21,50
This is just Bootstrap code.

9
00:00:21,50 --> 00:00:24,90
I have already changed the keyword class

10
00:00:24,90 --> 00:00:27,00
that you would normally see in HTML,

11
00:00:27,00 --> 00:00:29,90
to class name as we need in react,

12
00:00:29,90 --> 00:00:33,40
but other than that, this is just normal HTML code.

13
00:00:33,40 --> 00:00:35,40
So click on the raw button.

14
00:00:35,40 --> 00:00:37,30
We'll copy this

15
00:00:37,30 --> 00:00:38,30
and then

16
00:00:38,30 --> 00:00:40,50
we're going to need to use parentheses here

17
00:00:40,50 --> 00:00:44,40
so I'm going to type some parentheses here, delete this,

18
00:00:44,40 --> 00:00:49,00
and then just paste my HTML in here.

19
00:00:49,00 --> 00:00:50,90
So you do need parentheses when you're using

20
00:00:50,90 --> 00:00:53,00
more than a single line.

21
00:00:53,00 --> 00:00:55,90
And let's go ahead and save this.

22
00:00:55,90 --> 00:00:57,90
Now you're just going to to see right now

23
00:00:57,90 --> 00:01:00,60
a single one because this actually doesn't have

24
00:01:00,60 --> 00:01:03,30
any of the looping mechanism

25
00:01:03,30 --> 00:01:05,90
to go through each of the elements.

26
00:01:05,90 --> 00:01:10,50
So this div right here is the main container and

27
00:01:10,50 --> 00:01:15,40
what I need to repeat is this next section right here.

28
00:01:15,40 --> 00:01:17,60
I'm going to do something similar to what you see up here,

29
00:01:17,60 --> 00:01:19,50
but it's just going to be in the template.

30
00:01:19,50 --> 00:01:22,10
So we'll create an expression and we'll save this

31
00:01:22,10 --> 00:01:27,00
dot props dot appointments.

32
00:01:27,00 --> 00:01:27,80
And then, just like before,

33
00:01:27,80 --> 00:01:30,30
we'll use the javascript map function

34
00:01:30,30 --> 00:01:33,00
to go through a series of items, so

35
00:01:33,00 --> 00:01:35,50
by mapping, we're taking the array

36
00:01:35,50 --> 00:01:37,50
and then taking each of the elements,

37
00:01:37,50 --> 00:01:41,70
putting them temporarily in this item variable,

38
00:01:41,70 --> 00:01:44,20
and then actually we need to do another set of parentheses

39
00:01:44,20 --> 00:01:47,30
'cause we're going to put the template that you see

40
00:01:47,30 --> 00:01:53,50
right inside this

41
00:01:53,50 --> 00:01:57,60
so all the way until this extra div down here

42
00:01:57,60 --> 00:01:58,80
and I'm going to cut that out

43
00:01:58,80 --> 00:02:01,80
and paste it in here.

44
00:02:01,80 --> 00:02:06,00
Right, so let's save that and let's take a peek.

45
00:02:06,00 --> 00:02:07,70
You can see that right now we just have a copy

46
00:02:07,70 --> 00:02:10,00
of the same element a bunch of times.

47
00:02:10,00 --> 00:02:14,60
So now we need to use expressions in here,

48
00:02:14,60 --> 00:02:18,70
so instead of pet name

49
00:02:18,70 --> 00:02:21,70
I need to call this item dot pet name

50
00:02:21,70 --> 00:02:29,90
and do the same for all the other ones.

51
00:02:29,90 --> 00:02:33,70
Let's go ahead and save that, take a peek.

52
00:02:33,70 --> 00:02:36,70
And you should see all the information now flowing properly

53
00:02:36,70 --> 00:02:40,80
which means that we don't need this temporary variable here.

54
00:02:40,80 --> 00:02:42,70
Sometimes it's more convenient to do things

55
00:02:42,70 --> 00:02:46,00
outside this return statement or outside the template

56
00:02:46,00 --> 00:02:48,10
and sometimes it's convenient to do it

57
00:02:48,10 --> 00:02:50,60
within the template itself.

58
00:02:50,60 --> 00:02:52,50
So we'll save that.

59
00:02:52,50 --> 00:02:54,80
And to make this look a little bit better,

60
00:02:54,80 --> 00:02:58,40
I have some extra CSS that I've prepared for you.

61
00:02:58,40 --> 00:03:01,70
So I'm going to click on this raw button right here,

62
00:03:01,70 --> 00:03:04,50
select all this

63
00:03:04,50 --> 00:03:07,20
and this particular CSS is going to apply

64
00:03:07,20 --> 00:03:09,40
to all of my components.

65
00:03:09,40 --> 00:03:12,90
So I'm going to replace what's in app dot CSS

66
00:03:12,90 --> 00:03:16,20
which was the old CSS from create react app

67
00:03:16,20 --> 00:03:18,30
and just paste this in there.

68
00:03:18,30 --> 00:03:21,40
And so that is going to make our application

69
00:03:21,40 --> 00:03:23,20
just look a little bit nicer.

70
00:03:23,20 --> 00:03:25,50
Some basic CSS to make things look good.

71
00:03:25,50 --> 00:03:31,30
So that's how you bring in some more complex HTML code

72
00:03:31,30 --> 00:03:36,40
and also how you modify the template by using an expression

73
00:03:36,40 --> 00:03:38,60
that's more complicated

74
00:03:38,60 --> 00:03:40,40
but allows you to sort of generate almost

75
00:03:40,40 --> 00:03:43,00
a template within a template.

