1
00:00:00,00 --> 00:00:03,40
- [Instructor] Now interact to simple variables

2
00:00:03,40 --> 00:00:06,90
we can use the expression like this

3
00:00:06,90 --> 00:00:08,60
and then I showed you before that you can

4
00:00:08,60 --> 00:00:11,50
put in something like a variable that's in the state.

5
00:00:11,50 --> 00:00:13,50
Right now we don't have one other than

6
00:00:13,50 --> 00:00:14,90
the array of Appointments,

7
00:00:14,90 --> 00:00:19,70
but if you try to just put in

8
00:00:19,70 --> 00:00:22,10
the Appointments variable here,

9
00:00:22,10 --> 00:00:24,10
you'll notice that you'll get an error.

10
00:00:24,10 --> 00:00:31,10
And the error is that you can't use objects as React child.

11
00:00:31,10 --> 00:00:33,40
So we need to put this in a simple expression.

12
00:00:33,40 --> 00:00:37,10
You may try to do something like this,

13
00:00:37,10 --> 00:00:39,50
'cause you're like well that might my

14
00:00:39,50 --> 00:00:41,80
Appointments an array of objects

15
00:00:41,80 --> 00:00:45,40
and maybe we can get the first item

16
00:00:45,40 --> 00:00:47,20
and then just output the petName.

17
00:00:47,20 --> 00:00:49,00
That's also not going to work.

18
00:00:49,00 --> 00:00:51,90
Again, it wants simple expressions,

19
00:00:51,90 --> 00:00:53,90
so what we need to do is convert this

20
00:00:53,90 --> 00:00:56,80
so that it outputs a simple expression.

21
00:00:56,80 --> 00:01:04,30
And to do that I'm going to create a variable here

22
00:01:04,30 --> 00:01:06,60
and it's just a temporary variable.

23
00:01:06,60 --> 00:01:08,90
I'll call it ListItems

24
00:01:08,90 --> 00:01:11,60
and this is going to refer to,

25
00:01:11,60 --> 00:01:16,60
go ahead and make this bigger,

26
00:01:16,60 --> 00:01:18,60
this is going to refer to myAppointments

27
00:01:18,60 --> 00:01:21,90
and then we're going to use Javascript map method

28
00:01:21,90 --> 00:01:23,60
to go through all the elements

29
00:01:23,60 --> 00:01:28,00
and we'll take each of the items.

30
00:01:28,00 --> 00:01:29,70
And I'm using a parenthesis here,

31
00:01:29,70 --> 00:01:33,80
which looks sort of odd,

32
00:01:33,80 --> 00:01:35,60
but it's essentially what we do here

33
00:01:35,60 --> 00:01:37,30
with this return statement.

34
00:01:37,30 --> 00:01:43,90
We return some jsx,

35
00:01:43,90 --> 00:01:44,70
and so we're going to do something similar here.

36
00:01:44,70 --> 00:01:46,40
We'll create a div

37
00:01:46,40 --> 00:01:52,00
and then inside here we'll use that item variable

38
00:01:52,00 --> 00:01:56,90
to output the petName.

39
00:01:56,90 --> 00:01:58,80
So now that we've taken each of the petNames

40
00:01:58,80 --> 00:02:01,00
and created a list of them

41
00:02:01,00 --> 00:02:02,60
and placed them into this variable

42
00:02:02,60 --> 00:02:05,90
then we can use that in our expression.

43
00:02:05,90 --> 00:02:10,00
ListItems and we'll save that,

44
00:02:10,00 --> 00:02:13,70
and now you can see that we get the list of pets

45
00:02:13,70 --> 00:02:15,10
which is pretty cool.

46
00:02:15,10 --> 00:02:19,90
And you may be tempted to do something like this.

47
00:02:19,90 --> 00:02:24,90
Say that other variable is ownerName,

48
00:02:24,90 --> 00:02:25,70
but you'll get an error

49
00:02:25,70 --> 00:02:27,70
'cause remember as I mentioned before

50
00:02:27,70 --> 00:02:32,30
jsx wants you to have a simple container

51
00:02:32,30 --> 00:02:35,70
around everything that is a single tag.

52
00:02:35,70 --> 00:02:38,80
So here if we wanted to do something like this

53
00:02:38,80 --> 00:02:41,60
we'd have to put sort of another div

54
00:02:41,60 --> 00:02:46,10
and then put these things in there.

55
00:02:46,10 --> 00:02:47,60
And that would work.

56
00:02:47,60 --> 00:02:49,40
Doesn't look that great

57
00:02:49,40 --> 00:02:52,80
but this is how you create a simple expression

58
00:02:52,80 --> 00:02:56,90
that you can then output in your components.

59
00:02:56,90 --> 00:02:59,90
What we really need to be doing is all this

60
00:02:59,90 --> 00:03:03,70
but inside the list Appointments component.

61
00:03:03,70 --> 00:03:06,00
We'll take care of that in the next video.

