1
00:00:00,50 --> 00:00:03,10
- [Instructor] The preferred way of developing in React is

2
00:00:03,10 --> 00:00:05,00
to create different components

3
00:00:05,00 --> 00:00:08,10
for each of the pieces of functionality that you want.

4
00:00:08,10 --> 00:00:12,50
And so we need to take this display of the list items here

5
00:00:12,50 --> 00:00:15,50
and move it into this ListAppointments components.

6
00:00:15,50 --> 00:00:17,70
Now to do that, we're going to use the component

7
00:00:17,70 --> 00:00:20,00
that we created earlier, but we need to be able

8
00:00:20,00 --> 00:00:22,20
to pass down to the component.

9
00:00:22,20 --> 00:00:25,00
Now we do that with something called props.

10
00:00:25,00 --> 00:00:29,30
So in React, we can create almost like an attribute in HTML,

11
00:00:29,30 --> 00:00:31,10
and we can type a variable that we want

12
00:00:31,10 --> 00:00:33,60
to be available to ListAppointments.

13
00:00:33,60 --> 00:00:37,30
So we'll call that appointments,

14
00:00:37,30 --> 00:00:42,50
and then we'll use an expression to pass along

15
00:00:42,50 --> 00:00:50,40
the myAppointments variable from this component.

16
00:00:50,40 --> 00:00:52,40
So now inside ListAppointments,

17
00:00:52,40 --> 00:00:54,10
there's going to be a new variable,

18
00:00:54,10 --> 00:00:57,60
and it's passed through this thing called a prop.

19
00:00:57,60 --> 00:00:59,70
And it's got the name of appointments.

20
00:00:59,70 --> 00:01:02,50
So we can now take all this right here

21
00:01:02,50 --> 00:01:05,40
and take it from this component.

22
00:01:05,40 --> 00:01:06,60
And let's go ahead and save that.

23
00:01:06,60 --> 00:01:07,60
This may crash.

24
00:01:07,60 --> 00:01:12,10
And also, we need to remove this listItems right here,

25
00:01:12,10 --> 00:01:14,40
and that's because that's

26
00:01:14,40 --> 00:01:16,60
what was displaying the list before.

27
00:01:16,60 --> 00:01:20,20
So we're back to this, and we're going to switch back

28
00:01:20,20 --> 00:01:23,10
to the ListAppointments component.

29
00:01:23,10 --> 00:01:26,00
We're going to paste what we had from the original component

30
00:01:26,00 --> 00:01:29,10
right here inside the render method

31
00:01:29,10 --> 00:01:31,00
before the return statement.

32
00:01:31,00 --> 00:01:34,60
So here is where you place things that

33
00:01:34,60 --> 00:01:40,30
you want to happen before the component gets displayed.

34
00:01:40,30 --> 00:01:44,00
And what we'll do here is

35
00:01:44,00 --> 00:01:47,10
we'll use this listItems

36
00:01:47,10 --> 00:01:48,70
just like before.

37
00:01:48,70 --> 00:01:52,80
But instead of getting the value of listItems

38
00:01:52,80 --> 00:01:54,60
by looking at myAppointments,

39
00:01:54,60 --> 00:01:56,40
since it's no longer available,

40
00:01:56,40 --> 00:02:00,10
we're going to use a special keyword here called props.

41
00:02:00,10 --> 00:02:02,60
And we're no longer calling it myAppointments.

42
00:02:02,60 --> 00:02:05,90
We just called it appointments.

43
00:02:05,90 --> 00:02:07,30
Now you may call it the same thing.

44
00:02:07,30 --> 00:02:10,40
It doesn't really matter, but I think that it's confusing

45
00:02:10,40 --> 00:02:12,70
if you actually called it myAppointments

46
00:02:12,70 --> 00:02:15,30
'cause you may think that it's the same thing

47
00:02:15,30 --> 00:02:19,10
as the myAppointments in the original component.

48
00:02:19,10 --> 00:02:23,40
And we gave it a special name when we passed it as a prop.

49
00:02:23,40 --> 00:02:24,20
If you don't want to do that,

50
00:02:24,20 --> 00:02:26,20
you could just call this here myAppointments,

51
00:02:26,20 --> 00:02:28,60
and then you could use the same code.

52
00:02:28,60 --> 00:02:30,20
So let's save that,

53
00:02:30,20 --> 00:02:32,60
and you can see that now our sub-component

54
00:02:32,60 --> 00:02:35,70
is displaying the data just fine.

55
00:02:35,70 --> 00:02:37,40
And actually, it replaced the text

56
00:02:37,40 --> 00:02:39,80
that used to say ListAppointments.

57
00:02:39,80 --> 00:02:42,00
Now one thing about props is

58
00:02:42,00 --> 00:02:44,60
that they are made to happen one way.

59
00:02:44,60 --> 00:02:47,90
So you pass data down to the sub-component,

60
00:02:47,90 --> 00:02:51,10
but it's not meant for you to modify

61
00:02:51,10 --> 00:02:53,20
once you're in the sub-component.

62
00:02:53,20 --> 00:02:56,20
So we can't take the appointments and reassign them here.

63
00:02:56,20 --> 00:02:58,00
We can really only display 'em.

64
00:02:58,00 --> 00:02:59,80
Now that sounds like a problem.

65
00:02:59,80 --> 00:03:01,90
In some older languages, you used to have the ability

66
00:03:01,90 --> 00:03:04,30
of creating two-way data binding,

67
00:03:04,30 --> 00:03:07,60
but it's actually much safer to use something

68
00:03:07,60 --> 00:03:11,90
that passes data down and doesn't let you modify

69
00:03:11,90 --> 00:03:14,00
the original data in the sub-component.

