1
00:00:00,50 --> 00:00:01,90
- [Instructor] Let's work on converting

2
00:00:01,90 --> 00:00:05,40
these div tags right here to actual sub-components

3
00:00:05,40 --> 00:00:06,80
in our application.

4
00:00:06,80 --> 00:00:10,20
To do that, I'm going to go into the components folder

5
00:00:10,20 --> 00:00:13,40
and I'm going to make a new file here.

6
00:00:13,40 --> 00:00:14,20
I'll do 'em in order.

7
00:00:14,20 --> 00:00:16,30
I'm going to start with add appointments.

8
00:00:16,30 --> 00:00:20,00
I usually capitalize add and then appointments

9
00:00:20,00 --> 00:00:22,60
and name 'em the same way that I would name my classes,

10
00:00:22,60 --> 00:00:25,30
which usually is what we call camel case

11
00:00:25,30 --> 00:00:27,80
with the first letter being capitalized

12
00:00:27,80 --> 00:00:29,00
and the second letter

13
00:00:29,00 --> 00:00:31,90
of the second word also being capitalized here.

14
00:00:31,90 --> 00:00:34,60
Add Appointments is going to be a simple component,

15
00:00:34,60 --> 00:00:37,50
just like our app.js component.

16
00:00:37,50 --> 00:00:41,00
I'm going to need to import the React library

17
00:00:41,00 --> 00:00:42,90
and then I'm going to also need

18
00:00:42,90 --> 00:00:46,80
to import specifically the Component class

19
00:00:46,80 --> 00:00:51,40
from the React library and remember this is importing it

20
00:00:51,40 --> 00:00:53,80
from our node modules folder.

21
00:00:53,80 --> 00:00:57,50
Next up, I need to go ahead and create the class

22
00:00:57,50 --> 00:01:00,10
that I'm creating with this file.

23
00:01:00,10 --> 00:01:04,80
This one is called AddAppointments.

24
00:01:04,80 --> 00:01:09,80
This appointments class is going to extend the Component class

25
00:01:09,80 --> 00:01:11,00
that we imported.

26
00:01:11,00 --> 00:01:15,60
It's going to be based on the default React Component class,

27
00:01:15,60 --> 00:01:19,50
but it's going to add a few things to it.

28
00:01:19,50 --> 00:01:20,80
In here, what we need to do

29
00:01:20,80 --> 00:01:22,60
is just issue a render statement.

30
00:01:22,60 --> 00:01:24,60
This is going to be a simple component,

31
00:01:24,60 --> 00:01:29,00
so all it needs to do is just draw something on the screen.

32
00:01:29,00 --> 00:01:32,20
Then, we'll need to return something here

33
00:01:32,20 --> 00:01:35,50
and we can return just the div that for right now says,

34
00:01:35,50 --> 00:01:39,80
AddAppointments and actually let's put a space in here.

35
00:01:39,80 --> 00:01:41,70
Normally, if this is more complicated,

36
00:01:41,70 --> 00:01:44,30
then you would put this in parentheses,

37
00:01:44,30 --> 00:01:45,90
but you'll see that the way

38
00:01:45,90 --> 00:01:49,10
that I have my editor set up right now, when I save it,

39
00:01:49,10 --> 00:01:51,40
it actually gets rid of those parentheses

40
00:01:51,40 --> 00:01:53,90
when things get a little more complicated.

41
00:01:53,90 --> 00:01:58,10
This is pretty much how all the components will be created.

42
00:01:58,10 --> 00:02:02,60
Let's go ahead and now go into app.js

43
00:02:02,60 --> 00:02:07,10
and replace this with our component.

44
00:02:07,10 --> 00:02:10,00
To do that, we need to import the AddAppointment component

45
00:02:10,00 --> 00:02:12,60
into this main component.

46
00:02:12,60 --> 00:02:21,00
We'll say import and we'll say AddAppointments.

47
00:02:21,00 --> 00:02:25,60
This is actually referring to the class, not the file.

48
00:02:25,60 --> 00:02:26,90
When we created AddAppointments,

49
00:02:26,90 --> 00:02:29,50
we created this thing right here

50
00:02:29,50 --> 00:02:32,50
and identified it as a class and gave it a name

51
00:02:32,50 --> 00:02:35,20
of AddAppointments, so that's what I'm referring to here.

52
00:02:35,20 --> 00:02:38,50
I could, if I want to, create all my components

53
00:02:38,50 --> 00:02:42,30
in this same file, just put them one right after the other,

54
00:02:42,30 --> 00:02:45,60
and then choose to import whichever component I wanted

55
00:02:45,60 --> 00:02:50,90
instead of this default one from the file.

56
00:02:50,90 --> 00:02:54,10
Whenever we refer to something that is in a file,

57
00:02:54,10 --> 00:02:56,80
not in the node modules folder,

58
00:02:56,80 --> 00:03:00,10
then we usually start it with a ./

59
00:03:00,10 --> 00:03:04,40
and then we refer to it by name.

60
00:03:04,40 --> 00:03:06,70
AddAppointments, we don't need to put .js,

61
00:03:06,70 --> 00:03:12,70
but it is the file and let's go ahead and save that.

62
00:03:12,70 --> 00:03:15,30
Then, notice that this editor is sort of nice.

63
00:03:15,30 --> 00:03:18,20
It grays things out if you're not using them.

64
00:03:18,20 --> 00:03:20,30
If you've created a variable, but you haven't used it,

65
00:03:20,30 --> 00:03:21,80
it grays it out which is cool.

66
00:03:21,80 --> 00:03:24,10
In order to use this component,

67
00:03:24,10 --> 00:03:27,80
what we need to do now is sort of code it

68
00:03:27,80 --> 00:03:30,40
as if it were an HTML tag

69
00:03:30,40 --> 00:03:34,30
and the thing that we want is called AddAppointments.

70
00:03:34,30 --> 00:03:35,60
Because this is jsx,

71
00:03:35,60 --> 00:03:39,20
it gets a slash and a greater than sign.

72
00:03:39,20 --> 00:03:42,20
That's because it doesn't have a closing tag,

73
00:03:42,20 --> 00:03:46,40
so it sort of all goes into a single tag and now, okay,

74
00:03:46,40 --> 00:03:48,10
so it looks like AddAppointments

75
00:03:48,10 --> 00:03:49,70
does not contain a default export.

76
00:03:49,70 --> 00:03:51,40
Oops, that's really important.

77
00:03:51,40 --> 00:03:53,10
Let's not forget that.

78
00:03:53,10 --> 00:03:54,60
Once you create this thing

79
00:03:54,60 --> 00:03:56,60
and this is actually why this is grayed out.

80
00:03:56,60 --> 00:04:01,80
In the last tab is to actually export the class,

81
00:04:01,80 --> 00:04:04,40
so we'll say export default.

82
00:04:04,40 --> 00:04:06,90
The default class of this component exports

83
00:04:06,90 --> 00:04:09,90
is called AddAppointments.

84
00:04:09,90 --> 00:04:17,40
Now, when the app.js file calls this AddAppointments.js file

85
00:04:17,40 --> 00:04:22,10
what it gets is this AddAppointments class.

86
00:04:22,10 --> 00:04:26,10
All this class does is just output AddAppointments

87
00:04:26,10 --> 00:04:30,70
and then we'll see, let's go ahead and add some dashes here,

88
00:04:30,70 --> 00:04:33,50
so we can make sure that it is different

89
00:04:33,50 --> 00:04:34,90
than the way that it used to be.

90
00:04:34,90 --> 00:04:37,30
It's actually reading things from this component.

91
00:04:37,30 --> 00:04:38,30
Let's go ahead and get rid of that

92
00:04:38,30 --> 00:04:41,90
because we don't really need that.

93
00:04:41,90 --> 00:04:43,50
That's how you create a sub-component.

94
00:04:43,50 --> 00:04:47,60
The other two are going to be pretty similar.

95
00:04:47,60 --> 00:04:49,20
Let's make this window bigger.

96
00:04:49,20 --> 00:04:51,50
Actually, let's go ahead and just make another file

97
00:04:51,50 --> 00:04:53,10
and the other two are going

98
00:04:53,10 --> 00:04:58,30
to be called ListAppointments.js

99
00:04:58,30 --> 00:05:00,20
and we'll create the other one too.

100
00:05:00,20 --> 00:05:05,70
Then, SearchAppointments.js

101
00:05:05,70 --> 00:05:09,90
and now I'm going to take AddAppointments, copy it,

102
00:05:09,90 --> 00:05:12,90
paste that into these other two

103
00:05:12,90 --> 00:05:15,80
and we'll need to modify, obviously, a few things.

104
00:05:15,80 --> 00:05:19,20
Instead of creating a class called AddAppointments,

105
00:05:19,20 --> 00:05:23,00
this one's going to be called ListAppointments

106
00:05:23,00 --> 00:05:26,10
and this needs to be, of course,

107
00:05:26,10 --> 00:05:28,60
exporting ListAppointments.

108
00:05:28,60 --> 00:05:32,10
Finally, it needs to say ListAppointments.

109
00:05:32,10 --> 00:05:34,30
Let's go ahead and add those double dashes here

110
00:05:34,30 --> 00:05:38,60
just so you know that it's updated properly.

111
00:05:38,60 --> 00:05:43,60
Now, I'm going to save this in my app.js file.

112
00:05:43,60 --> 00:05:47,10
It looks like I have a lot of tabs in here

113
00:05:47,10 --> 00:05:48,40
that are open that I don't need,

114
00:05:48,40 --> 00:05:55,90
so I'm going to get rid of some of them here.

115
00:05:55,90 --> 00:06:00,90
Let's go to app.js and we've just updated ListAppointments,

116
00:06:00,90 --> 00:06:06,70
so let's go ahead and try importing that one.

117
00:06:06,70 --> 00:06:10,00
We haven't imported that component here,

118
00:06:10,00 --> 00:06:12,70
so this is not going to work.

119
00:06:12,70 --> 00:06:14,50
We did import this here,

120
00:06:14,50 --> 00:06:16,40
so let's go ahead and save it

121
00:06:16,40 --> 00:06:20,50
and you can see ListAppointments is working.

122
00:06:20,50 --> 00:06:25,60
Let's try SearchAppointments and we will replace,

123
00:06:25,60 --> 00:06:33,50
again, this Add with Search with an R, make it say search.

124
00:06:33,50 --> 00:06:37,30
We'll put in the two dashes, why not?

125
00:06:37,30 --> 00:06:40,90
Save that one, go back into app,

126
00:06:40,90 --> 00:06:51,20
make sure we're also importing search.

127
00:06:51,20 --> 00:06:54,10
Actually, search should be in the middle here

128
00:06:54,10 --> 00:06:56,40
because our form's going to be at the top,

129
00:06:56,40 --> 00:06:59,40
then we're going to build like a little search area

130
00:06:59,40 --> 00:07:03,90
and then the list will go underneath.

131
00:07:03,90 --> 00:07:06,60
They're actually in the correct order here,

132
00:07:06,60 --> 00:07:15,50
so we'll replace this with SearchAppointments.

133
00:07:15,50 --> 00:07:17,80
Now, they're all looking pretty good

134
00:07:17,80 --> 00:07:20,70
and since I put dashes on, really on these two,

135
00:07:20,70 --> 00:07:28,00
now let's go ahead and put it on Add too.

136
00:07:28,00 --> 00:07:29,70
Now, we have the same thing,

137
00:07:29,70 --> 00:07:33,40
but app.js now allows you

138
00:07:33,40 --> 00:07:36,10
to call one of the other components

139
00:07:36,10 --> 00:07:38,80
and whatever we have in the HTML

140
00:07:38,80 --> 00:07:41,50
in those components is going to be displayed here.

141
00:07:41,50 --> 00:07:45,00
I could grab this one and actually add it a bunch of times.

142
00:07:45,00 --> 00:07:47,60
You'll see that it would appear several times here.

143
00:07:47,60 --> 00:07:50,70
You only need it once, so we'll just do that one time,

144
00:07:50,70 --> 00:07:52,70
but that's the whole purpose of working with components.

145
00:07:52,70 --> 00:07:54,80
They're reusable bits of code.

146
00:07:54,80 --> 00:07:56,00
It's almost like PHP enclosed

147
00:07:56,00 --> 00:07:58,40
where you're just grabbing another file

148
00:07:58,40 --> 00:08:02,30
and injecting the contents and in the case of React,

149
00:08:02,30 --> 00:08:06,50
it's going to be the process contents of that component

150
00:08:06,50 --> 00:08:08,00
into another component.

