1
00:00:00,50 --> 00:00:02,20
- [Instructor] It's relatively easy to use

2
00:00:02,20 --> 00:00:04,40
a component like this ListAppointments,

3
00:00:04,40 --> 00:00:06,40
sub-component that we've created.

4
00:00:06,40 --> 00:00:08,60
And it's even easier to use components

5
00:00:08,60 --> 00:00:11,20
that were pre-built by somebody else.

6
00:00:11,20 --> 00:00:12,90
Case in point, we've already installed

7
00:00:12,90 --> 00:00:15,90
a couple of them into our application.

8
00:00:15,90 --> 00:00:18,70
And I went over it when we worked

9
00:00:18,70 --> 00:00:22,00
with the installation of Create React App.

10
00:00:22,00 --> 00:00:23,80
You could see that they're here

11
00:00:23,80 --> 00:00:26,00
as something called react-icons

12
00:00:26,00 --> 00:00:28,90
as well as something called react-moments,

13
00:00:28,90 --> 00:00:31,40
and also this moment library,

14
00:00:31,40 --> 00:00:33,00
and even this lodash library.

15
00:00:33,00 --> 00:00:34,10
So let's take a look

16
00:00:34,10 --> 00:00:37,40
at how you can incorporate one of those in here,

17
00:00:37,40 --> 00:00:39,10
and the one that we'll be working with first

18
00:00:39,10 --> 00:00:41,00
is called React Icons.

19
00:00:41,00 --> 00:00:42,70
This is a really great library

20
00:00:42,70 --> 00:00:46,60
that combines a number of different icon packages,

21
00:00:46,60 --> 00:00:49,00
like the Font Awesome icon package,

22
00:00:49,00 --> 00:00:52,40
and let's you use them in your React applications.

23
00:00:52,40 --> 00:00:57,60
So what that does is allow you to use and import command

24
00:00:57,60 --> 00:01:01,50
to import a specific icon from this library.

25
00:01:01,50 --> 00:01:03,20
And you can see the names right here

26
00:01:03,20 --> 00:01:06,10
for each of the icons that you can use.

27
00:01:06,10 --> 00:01:09,70
And if you want to use, say, Ionicons,

28
00:01:09,70 --> 00:01:13,00
you can click on that and take a look at how you import them

29
00:01:13,00 --> 00:01:15,10
as well as the names of the icons,

30
00:01:15,10 --> 00:01:19,10
and there are a lot of icons in this library.

31
00:01:19,10 --> 00:01:22,70
So to make that really easy to use,

32
00:01:22,70 --> 00:01:25,40
this company has wrapped those icons

33
00:01:25,40 --> 00:01:28,60
into a component package for you.

34
00:01:28,60 --> 00:01:31,50
Now as I mentioned, we already installed this,

35
00:01:31,50 --> 00:01:34,30
so all we need to use is the import statement,

36
00:01:34,30 --> 00:01:37,20
and we can just do that inside the component

37
00:01:37,20 --> 00:01:39,00
that we want to use them in.

38
00:01:39,00 --> 00:01:41,50
So I'm going to switch back to ListAppointments

39
00:01:41,50 --> 00:01:47,00
and I'm going to use the import statement from that library.

40
00:01:47,00 --> 00:01:48,50
Let's also import,

41
00:01:48,50 --> 00:01:51,40
and then in here instead of importing

42
00:01:51,40 --> 00:01:53,90
the component from React,

43
00:01:53,90 --> 00:01:56,30
we're going to import a specific icon,

44
00:01:56,30 --> 00:02:00,60
and the one that I want to use is called FaTimes,

45
00:02:00,60 --> 00:02:03,10
notice the capitalization is according

46
00:02:03,10 --> 00:02:04,70
to what's on the website,

47
00:02:04,70 --> 00:02:08,00
and then we'll say from, and then in quotes,

48
00:02:08,00 --> 00:02:14,90
react-icons/fa, so fa is the specific library,

49
00:02:14,90 --> 00:02:18,10
this refers to Font Awesome in this case.

50
00:02:18,10 --> 00:02:20,70
So once we import that specific component,

51
00:02:20,70 --> 00:02:25,30
then all you need to do is use it in your application.

52
00:02:25,30 --> 00:02:26,80
So instead of this X right here,

53
00:02:26,80 --> 00:02:29,40
I'm going to use an icon.

54
00:02:29,40 --> 00:02:32,40
And you use it just like you did ListAppointments

55
00:02:32,40 --> 00:02:34,40
in the main app component.

56
00:02:34,40 --> 00:02:37,70
All you have to do is type in the tag name,

57
00:02:37,70 --> 00:02:40,70
which would be FaTimes in this case,

58
00:02:40,70 --> 00:02:42,40
and then close it like this,

59
00:02:42,40 --> 00:02:44,80
so this would be matching the name

60
00:02:44,80 --> 00:02:47,00
that we're importing right here.

61
00:02:47,00 --> 00:02:50,40
And once we do that, let's go and save that,

62
00:02:50,40 --> 00:02:53,20
and we should see sort of a fancier-looking X.

63
00:02:53,20 --> 00:02:55,10
It's not really all that different

64
00:02:55,10 --> 00:02:56,40
than the X in the font,

65
00:02:56,40 --> 00:02:59,50
but you can use something fancier if you want to.

66
00:02:59,50 --> 00:03:00,80
So that's actually pretty easy

67
00:03:00,80 --> 00:03:04,30
and a lot of these libraries will have additional options.

68
00:03:04,30 --> 00:03:07,10
One other library that I find pretty useful

69
00:03:07,10 --> 00:03:09,10
is a library called Moment.js.

70
00:03:09,10 --> 00:03:13,00
It's really useful for managing how dates work,

71
00:03:13,00 --> 00:03:16,00
and this one has a lot of different options.

72
00:03:16,00 --> 00:03:19,90
It's also quite big for the specifics of what you need,

73
00:03:19,90 --> 00:03:22,70
so it's importing a lot of things about dates and times,

74
00:03:22,70 --> 00:03:24,40
so you don't need the whole thing.

75
00:03:24,40 --> 00:03:27,40
And so there is the componetized version

76
00:03:27,40 --> 00:03:30,70
of this library called react-moment.

77
00:03:30,70 --> 00:03:34,70
So it's just a slightly different version of the framework

78
00:03:34,70 --> 00:03:36,00
that is a little bit simpler

79
00:03:36,00 --> 00:03:38,40
because it let's you import

80
00:03:38,40 --> 00:03:41,50
just what you need in your applications.

81
00:03:41,50 --> 00:03:45,00
So let's see how we can use that in our application.

82
00:03:45,00 --> 00:03:49,20
So right now you could see that my times are in a format

83
00:03:49,20 --> 00:03:52,00
that uses military time.

84
00:03:52,00 --> 00:03:55,60
And I find that confusing sometimes,

85
00:03:55,60 --> 00:03:57,80
but if you look at data.json,

86
00:03:57,80 --> 00:04:02,20
that's just how the dates were created.

87
00:04:02,20 --> 00:04:03,40
And this refers to the way

88
00:04:03,40 --> 00:04:05,90
that maybe a form will be creating this date.

89
00:04:05,90 --> 00:04:09,20
So we're going to go back into ListAppointments

90
00:04:09,20 --> 00:04:11,90
and we're going to import react-moment,

91
00:04:11,90 --> 00:04:14,10
we already installed it before,

92
00:04:14,10 --> 00:04:15,90
so we'll just actually import

93
00:04:15,90 --> 00:04:23,10
the Moment library from react-moment.

94
00:04:23,10 --> 00:04:26,00
And this needs to be in quotes here,

95
00:04:26,00 --> 00:04:28,20
then we'll put in a semicolon,

96
00:04:28,20 --> 00:04:32,10
and then we can use that library

97
00:04:32,10 --> 00:04:36,20
instead of doing the date like this.

98
00:04:36,20 --> 00:04:39,20
Let's go ahead and take this out,

99
00:04:39,20 --> 00:04:41,80
and I'll put in a return right here.

100
00:04:41,80 --> 00:04:44,10
And so just like using the icons,

101
00:04:44,10 --> 00:04:47,20
what you do is you type in the name of the tag

102
00:04:47,20 --> 00:04:49,00
that relates to the library.

103
00:04:49,00 --> 00:04:54,10
So we'll say Moment,

104
00:04:54,10 --> 00:04:56,70
and as I mentioned, this one actually requires you

105
00:04:56,70 --> 00:04:59,80
to pass along some props into the library,

106
00:04:59,80 --> 00:05:03,50
just like you did with ListAppointments.

107
00:05:03,50 --> 00:05:04,80
What we're going to need to pass

108
00:05:04,80 --> 00:05:07,50
is the actual date that we're using,

109
00:05:07,50 --> 00:05:11,10
so I'll just actually paste, I still have that item,

110
00:05:11,10 --> 00:05:13,60
that appointmentDate in my clipboard,

111
00:05:13,60 --> 00:05:16,70
and then I need to send two additional things.

112
00:05:16,70 --> 00:05:21,70
And first off is the format that the date is in,

113
00:05:21,70 --> 00:05:26,30
so that will be in a prop called parse.

114
00:05:26,30 --> 00:05:31,10
And in here I'm just going to pass it along a string

115
00:05:31,10 --> 00:05:34,70
and this will be the year with four characters,

116
00:05:34,70 --> 00:05:38,00
the month, the date,

117
00:05:38,00 --> 00:05:41,60
and you can take a look at the Moment documentation

118
00:05:41,60 --> 00:05:43,90
to see what these should be,

119
00:05:43,90 --> 00:05:47,00
but this is actually the format of the date

120
00:05:47,00 --> 00:05:49,40
in my data.json file.

121
00:05:49,40 --> 00:05:52,10
If it doesn't know what this format is,

122
00:05:52,10 --> 00:05:55,90
it will probably maybe work but not in all browsers,

123
00:05:55,90 --> 00:06:01,30
so it prefers that you give it how-to interpret

124
00:06:01,30 --> 00:06:04,70
or parse the date that it gets from the data.

125
00:06:04,70 --> 00:06:08,00
And then you can specify a format.

126
00:06:08,00 --> 00:06:10,20
And for this prop, we're going to pass along

127
00:06:10,20 --> 00:06:12,10
just a different-looking format.

128
00:06:12,10 --> 00:06:16,20
So the month in three letters, then dash,

129
00:06:16,20 --> 00:06:18,00
the day, just with one letter,

130
00:06:18,00 --> 00:06:21,90
and the hour as well as the minutes,

131
00:06:21,90 --> 00:06:25,30
and then a for am or pm.

132
00:06:25,30 --> 00:06:28,50
So save that.

133
00:06:28,50 --> 00:06:30,10
And now you can see that my date

134
00:06:30,10 --> 00:06:31,60
is reformatted with the month,

135
00:06:31,60 --> 00:06:34,50
again, in the format that I've specified here.

136
00:06:34,50 --> 00:06:37,00
The Moment library is pretty configurable,

137
00:06:37,00 --> 00:06:39,00
and so you can take a look at the documentation

138
00:06:39,00 --> 00:06:42,00
if you want to modify any of these things.

