1
00:00:00,50 --> 00:00:02,20
- [Instructor] In a real application, you would be

2
00:00:02,20 --> 00:00:04,40
using data from some other source.

3
00:00:04,40 --> 00:00:06,80
To do that in Javascript, you would normally use

4
00:00:06,80 --> 00:00:10,00
something like Ajax, and if you're using more modern

5
00:00:10,00 --> 00:00:12,90
Javascript, you would use the Fetch API.

6
00:00:12,90 --> 00:00:15,00
You can use the same type of things in the React

7
00:00:15,00 --> 00:00:16,80
application, but they have to be treated

8
00:00:16,80 --> 00:00:19,10
a little bit differently, and that brings me to

9
00:00:19,10 --> 00:00:22,00
the concept of lifecycle in React.

10
00:00:22,00 --> 00:00:24,60
Lifecycles are a function in Javascript that allow you to do

11
00:00:24,60 --> 00:00:28,50
things at certain point in your application's life.

12
00:00:28,50 --> 00:00:31,40
So as the component is created for example,

13
00:00:31,40 --> 00:00:33,30
you could do something before the

14
00:00:33,30 --> 00:00:35,70
component displays you could do something else,

15
00:00:35,70 --> 00:00:38,30
and so we need to use those to bring data.

16
00:00:38,30 --> 00:00:39,90
Let's start by getting some sample data

17
00:00:39,90 --> 00:00:42,80
that I've created for you at this URL.

18
00:00:42,80 --> 00:00:45,10
So I'm going to click on this raw button here

19
00:00:45,10 --> 00:00:50,20
and select all this and go back into my application

20
00:00:50,20 --> 00:00:53,00
and I'm going to need to put this in the public folder

21
00:00:53,00 --> 00:00:56,00
because this is going to be data that needs to be

22
00:00:56,00 --> 00:01:00,10
available to the application once it's processed.

23
00:01:00,10 --> 00:01:03,30
This source folder has a bunch of files that are

24
00:01:03,30 --> 00:01:05,90
all going to be converted into regular Javascript

25
00:01:05,90 --> 00:01:09,60
eventually when we use the build command, or even when we

26
00:01:09,60 --> 00:01:14,70
do the npm run start that is controlling this application.

27
00:01:14,70 --> 00:01:17,70
Files that you actually need in the server go in this

28
00:01:17,70 --> 00:01:20,40
public folder, so we'll create a new file here,

29
00:01:20,40 --> 00:01:24,70
call it data.json, and we'll paste all that information.

30
00:01:24,70 --> 00:01:27,50
So you can see this is an array of appointments

31
00:01:27,50 --> 00:01:32,20
for the pets, and each array element right here is an object

32
00:01:32,20 --> 00:01:36,70
and it has different fields for the different types of data.

33
00:01:36,70 --> 00:01:39,80
So save that, and we'll bring that into

34
00:01:39,80 --> 00:01:44,50
app JS using a lifecycle method.

35
00:01:44,50 --> 00:01:46,50
As I mentioned there are a few one them.

36
00:01:46,50 --> 00:01:52,50
The one that you need to use is called componentDidMount.

37
00:01:52,50 --> 00:01:54,60
And this is where we would bring in things like

38
00:01:54,60 --> 00:01:57,70
information from an external source, so you can

39
00:01:57,70 --> 00:02:01,20
issue here a get request to a server, or API,

40
00:02:01,20 --> 00:02:03,60
or anything that you use normally to bring in data.

41
00:02:03,60 --> 00:02:06,70
Here we're just going to look for something in a file.

42
00:02:06,70 --> 00:02:14,70
And the React lifecycle method is called componentDidMount.

43
00:02:14,70 --> 00:02:16,70
And you can actually see some of the lifecycle methods

44
00:02:16,70 --> 00:02:22,20
in here, so componentDidMount, and notice that this

45
00:02:22,20 --> 00:02:26,60
is going in between the constructor and the render method.

46
00:02:26,60 --> 00:02:31,20
So in here, what we'll do is we'll use the fetch API.

47
00:02:31,20 --> 00:02:33,50
If you had something like jQuery you could use that

48
00:02:33,50 --> 00:02:36,80
and use jQUery's like get or post methods,

49
00:02:36,80 --> 00:02:40,30
but fetch is a pretty easy way of doing things.

50
00:02:40,30 --> 00:02:45,60
So we'll look for the file in the directory that is

51
00:02:45,60 --> 00:02:48,90
the current directory, now that's weird because

52
00:02:48,90 --> 00:02:51,70
we can see that the file isn't here, but as far as the

53
00:02:51,70 --> 00:02:56,00
Javascript is concerned, once our application all gets

54
00:02:56,00 --> 00:02:58,70
assembled, the data.json file will be in

55
00:02:58,70 --> 00:03:01,40
the same folder as the current document.

56
00:03:01,40 --> 00:03:08,60
So we'll say ./data.json and the fetch API works

57
00:03:08,60 --> 00:03:14,40
with promises, so we can use the Javascript then method

58
00:03:14,40 --> 00:03:18,70
to get the value of the response that you get from

59
00:03:18,70 --> 00:03:21,90
the server, in this case it's going to be the file.

60
00:03:21,90 --> 00:03:27,70
And, you can also then specify that that response

61
00:03:27,70 --> 00:03:32,40
is going to come in as a json formatted object.

62
00:03:32,40 --> 00:03:36,00
So, once you do that, you can once again use the

63
00:03:36,00 --> 00:03:39,70
then promise method and then take that result

64
00:03:39,70 --> 00:03:47,30
and go ahead and covert it into something else.

65
00:03:47,30 --> 00:03:50,50
So what I'm going to do here is take the result that I get

66
00:03:50,50 --> 00:03:55,90
from receiving.json file, and process it by creating

67
00:03:55,90 --> 00:04:00,00
a variable here, I'm going to call this appointments.

68
00:04:00,00 --> 00:04:04,50
And I'm going to get the result and use the Javascript

69
00:04:04,50 --> 00:04:08,50
map function here to go through each of the elements

70
00:04:08,50 --> 00:04:15,30
in the file and return them, just unprocessed.

71
00:04:15,30 --> 00:04:17,70
I could process them and add something else to each

72
00:04:17,70 --> 00:04:20,70
element, but all I'm doing in essentially all the results

73
00:04:20,70 --> 00:04:24,60
into this appointments variable that we're creating here.

74
00:04:24,60 --> 00:04:28,50
Now the reason for that is, you don't want to modify

75
00:04:28,50 --> 00:04:32,30
the state directly by doing something like this.

76
00:04:32,30 --> 00:04:36,20
You want to create a variable, do things to it,

77
00:04:36,20 --> 00:04:38,40
and then once that variable is ready, you want to push that

78
00:04:38,40 --> 00:04:43,30
into the state by using a method called set state.

79
00:04:43,30 --> 00:04:46,50
This is another one of the features of React,

80
00:04:46,50 --> 00:04:50,30
you never modify state directly, so you may be tempted

81
00:04:50,30 --> 00:04:55,00
to do something like this.state and then myAppointments

82
00:04:55,00 --> 00:04:58,20
equals whatever, or try to map things directly

83
00:04:58,20 --> 00:05:00,20
into a variable, but you can't do that.

84
00:05:00,20 --> 00:05:01,90
Or, at least you shouldn't do that.

85
00:05:01,90 --> 00:05:05,50
You should sort of create a temporary variable and then

86
00:05:05,50 --> 00:05:09,70
push it into state by using a special method called

87
00:05:09,70 --> 00:05:14,90
setState, and if you don't do this you will get a warning.

88
00:05:14,90 --> 00:05:19,70
So let's try setState, and in setState we're going to

89
00:05:19,70 --> 00:05:23,70
push something into a variable that we'll create

90
00:05:23,70 --> 00:05:27,70
called myAppointments, and we'll push that

91
00:05:27,70 --> 00:05:32,20
temporary variable that we created.

92
00:05:32,20 --> 00:05:36,50
So, this needs a semicolon here, and this actually

93
00:05:36,50 --> 00:05:40,60
needs to go in here, so it actually should be

94
00:05:40,60 --> 00:05:45,30
within this result that we're getting.

95
00:05:45,30 --> 00:05:52,30
So we'll set the state there, and we are pushing something

96
00:05:52,30 --> 00:05:54,70
into a variable that doesn't exist quite yet,

97
00:05:54,70 --> 00:05:59,20
so this should be added to here, and we'll initialize

98
00:05:59,20 --> 00:06:03,00
it as an empty array, our data is an array of objects,

99
00:06:03,00 --> 00:06:05,20
so we'll just initialize it as an empty array.

100
00:06:05,20 --> 00:06:10,50
And then we'll push it into the state using this .setState.

101
00:06:10,50 --> 00:06:12,70
So let's go ahead and save that.

102
00:06:12,70 --> 00:06:16,70
And if we didn't make any errors, we should be able to

103
00:06:16,70 --> 00:06:23,10
inspect, and again go to the React tab, and now you can see

104
00:06:23,10 --> 00:06:26,40
that the appointments are right there.

105
00:06:26,40 --> 00:06:28,80
Now if we open this up, we'll see that they were read

106
00:06:28,80 --> 00:06:34,90
from the file, and they were placed into our state,

107
00:06:34,90 --> 00:06:36,70
just like our name was.

108
00:06:36,70 --> 00:06:38,90
Now we're to going to need name anymore,

109
00:06:38,90 --> 00:06:46,70
so let's go ahead and get rid of it.

110
00:06:46,70 --> 00:06:49,60
And we'll get rid of the call right here.

111
00:06:49,60 --> 00:06:55,50
Now I can't push an array directly into this template.

112
00:06:55,50 --> 00:06:57,80
I have to do something to it, and we'll

113
00:06:57,80 --> 00:07:00,00
talk about those in the next video.

