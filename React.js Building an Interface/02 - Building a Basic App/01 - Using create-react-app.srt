1
00:00:00,50 --> 00:00:03,00
- [Instructor] Create React App is a CLI

2
00:00:03,00 --> 00:00:05,00
or a Command Line Interface tool

3
00:00:05,00 --> 00:00:08,50
that was created by Facebook, the developers of React,

4
00:00:08,50 --> 00:00:10,40
to help you set up projects quickly.

5
00:00:10,40 --> 00:00:13,10
You can get a copy of it at this URL.

6
00:00:13,10 --> 00:00:14,40
Now that means that it's something

7
00:00:14,40 --> 00:00:17,80
that you'll need to run on a terminal application.

8
00:00:17,80 --> 00:00:19,80
I like to use Hyper, but you can use something

9
00:00:19,80 --> 00:00:23,60
like Git BASH on your PC or your Mac's terminal.

10
00:00:23,60 --> 00:00:24,90
Now before you take this course,

11
00:00:24,90 --> 00:00:27,30
make sure you've got a version of Git

12
00:00:27,30 --> 00:00:29,70
which you can get from this URL.

13
00:00:29,70 --> 00:00:31,70
And if you're at a PC you can download

14
00:00:31,70 --> 00:00:33,20
the Git BASH application

15
00:00:33,20 --> 00:00:36,50
that is an option when you install Git on your computer.

16
00:00:36,50 --> 00:00:41,10
That's going to allow you to run Linux like commands on a PC.

17
00:00:41,10 --> 00:00:43,80
You'll also need a copy of Node JS installed

18
00:00:43,80 --> 00:00:46,50
which you can get at this URL.

19
00:00:46,50 --> 00:00:49,70
More specifically, you're going to need something called NPM

20
00:00:49,70 --> 00:00:53,30
or NPX which requires a version

21
00:00:53,30 --> 00:00:56,50
of NPM that is greater than six.

22
00:00:56,50 --> 00:00:58,00
Or this stuff is not going to work.

23
00:00:58,00 --> 00:01:00,50
So make sure you download the latest versions.

24
00:01:00,50 --> 00:01:03,80
And then you're going to need to open up your terminal.

25
00:01:03,80 --> 00:01:05,20
Got one open right here.

26
00:01:05,20 --> 00:01:07,60
I'm going to make sure that I am on the desktop.

27
00:01:07,60 --> 00:01:12,10
So I'll do cd ~/desktop.

28
00:01:12,10 --> 00:01:14,70
And you may install this wherever you want.

29
00:01:14,70 --> 00:01:17,70
And then we're going to issue an NPX.

30
00:01:17,70 --> 00:01:22,80
And then use create react app.

31
00:01:22,80 --> 00:01:24,30
And then we're going to give this a name.

32
00:01:24,30 --> 00:01:28,90
I'll call mine React interface.

33
00:01:28,90 --> 00:01:31,20
Now it's not going to let you put in capital names

34
00:01:31,20 --> 00:01:33,90
or any other special characters in here.

35
00:01:33,90 --> 00:01:35,00
So just give it a simple name

36
00:01:35,00 --> 00:01:38,00
if you don't want to call it React interface.

37
00:01:38,00 --> 00:01:40,20
Now this is going to set up a project with a series

38
00:01:40,20 --> 00:01:42,80
of utilities like Webpack which is going

39
00:01:42,80 --> 00:01:45,00
to let you run a live server

40
00:01:45,00 --> 00:01:47,00
as well as a development environment

41
00:01:47,00 --> 00:01:50,30
that will automatically reload as you make changes.

42
00:01:50,30 --> 00:01:52,40
It's also going to install things like Babble

43
00:01:52,40 --> 00:01:54,60
and that's going to let you manage your code

44
00:01:54,60 --> 00:01:57,50
and make it write a version that is more compatible

45
00:01:57,50 --> 00:02:01,10
with older browsers.

46
00:02:01,10 --> 00:02:03,80
Once everything gets installed you should see a screen

47
00:02:03,80 --> 00:02:06,10
like this with the most common commands

48
00:02:06,10 --> 00:02:07,70
that you will use.

49
00:02:07,70 --> 00:02:09,40
Now the most popular command you'll be using

50
00:02:09,40 --> 00:02:12,20
is the NPM start command.

51
00:02:12,20 --> 00:02:14,40
That's going to start a development server

52
00:02:14,40 --> 00:02:16,80
that will live preview as you code.

53
00:02:16,80 --> 00:02:18,40
And it's going to give you all kinds of goodies

54
00:02:18,40 --> 00:02:22,40
like error detection and error information and warnings.

55
00:02:22,40 --> 00:02:25,80
If you are programming in React it's a fantastic thing.

56
00:02:25,80 --> 00:02:29,90
You can also run NPM run build in your terminal.

57
00:02:29,90 --> 00:02:32,50
That's going to build the application into a single folder

58
00:02:32,50 --> 00:02:35,00
that you can use to upload to your server.

59
00:02:35,00 --> 00:02:38,00
And then there are other commands like NPM test,

60
00:02:38,00 --> 00:02:42,90
and NPM run eject test starts a test runner.

61
00:02:42,90 --> 00:02:45,00
I'm not going to cover tests in this course.

62
00:02:45,00 --> 00:02:49,50
And NPM run eject gets the tool out of your computer.

63
00:02:49,50 --> 00:02:52,00
So in order to see what's installed

64
00:02:52,00 --> 00:02:55,20
you can switch by going cd as it says

65
00:02:55,20 --> 00:02:56,40
right there in the instructions,

66
00:02:56,40 --> 00:03:00,10
the name of the project, React interface.

67
00:03:00,10 --> 00:03:06,40
And then do NPM start.

68
00:03:06,40 --> 00:03:08,10
And if you've done everything correctly,

69
00:03:08,10 --> 00:03:11,50
you should have something like this in your browser.

70
00:03:11,50 --> 00:03:13,50
There's one more installation that you'll want to do

71
00:03:13,50 --> 00:03:15,30
before taking this course.

72
00:03:15,30 --> 00:03:18,20
You're going to want to load up the React developer

73
00:03:18,20 --> 00:03:21,40
tools extension in the Chrome browser.

74
00:03:21,40 --> 00:03:23,60
So make sure you go to this URL

75
00:03:23,60 --> 00:03:28,30
and then click on add to Chrome.

76
00:03:28,30 --> 00:03:31,60
Go ahead and add the extension.

77
00:03:31,60 --> 00:03:34,30
And the React developer tools is going

78
00:03:34,30 --> 00:03:37,00
to give you some additional developer tools

79
00:03:37,00 --> 00:03:39,80
when you inspect elements in React.

80
00:03:39,80 --> 00:03:42,00
Now that you've done that, we're ready to get started.

