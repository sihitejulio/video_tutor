1
00:00:00,90 --> 00:00:01,80
- [Instructor] Let's take a look

2
00:00:01,80 --> 00:00:03,70
at what we get when we create a project

3
00:00:03,70 --> 00:00:05,60
with Create React App.

4
00:00:05,60 --> 00:00:06,70
I have a folder right here

5
00:00:06,70 --> 00:00:09,10
with all the files that were installed.

6
00:00:09,10 --> 00:00:11,80
I'm going to pull this up in a text editor.

7
00:00:11,80 --> 00:00:14,90
I'm using Visual Studio Code for this course

8
00:00:14,90 --> 00:00:17,80
and you'll see that you get a number of files.

9
00:00:17,80 --> 00:00:20,80
Depending on what you did to get this folder,

10
00:00:20,80 --> 00:00:24,60
you're going to see a node_modules folder or not.

11
00:00:24,60 --> 00:00:28,00
If you downloaded the zip files from the GitHub repo,

12
00:00:28,00 --> 00:00:29,20
you won't see this folder

13
00:00:29,20 --> 00:00:31,60
until you do an npm install,

14
00:00:31,60 --> 00:00:34,40
so make sure you check out the Using the Exercises

15
00:00:34,40 --> 00:00:36,60
for This Course video.

16
00:00:36,60 --> 00:00:39,70
Now you also will see a public folder.

17
00:00:39,70 --> 00:00:43,60
This is essentially just your index.html file

18
00:00:43,60 --> 00:00:47,40
which is a little bit different than most HTML files

19
00:00:47,40 --> 00:00:49,70
that you may have seen.

20
00:00:49,70 --> 00:00:51,60
Notice that there are no linked tags

21
00:00:51,60 --> 00:00:54,80
for CSS or any JavaScript files

22
00:00:54,80 --> 00:00:58,10
that you would normally see being injected in here

23
00:00:58,10 --> 00:00:59,20
into the code.

24
00:00:59,20 --> 00:01:02,90
And that's because Webpack is going to automatically inject

25
00:01:02,90 --> 00:01:08,10
those for you but this is the HTML sort of page

26
00:01:08,10 --> 00:01:10,80
that you can use if you want to customize things

27
00:01:10,80 --> 00:01:13,00
a little bit, so you can, for example,

28
00:01:13,00 --> 00:01:14,50
change the title of the page

29
00:01:14,50 --> 00:01:19,50
or maybe add some basic code for the entire document.

30
00:01:19,50 --> 00:01:22,80
In addition to that, you get a manifest.json file

31
00:01:22,80 --> 00:01:25,80
which is useful for mobile devices

32
00:01:25,80 --> 00:01:27,70
when you save the application

33
00:01:27,70 --> 00:01:32,30
from your mobile browser as an application.

34
00:01:32,30 --> 00:01:34,60
There's also a favicon.ico file.

35
00:01:34,60 --> 00:01:36,90
This is just a little logo that will appear

36
00:01:36,90 --> 00:01:39,40
in your browser bar.

37
00:01:39,40 --> 00:01:41,50
This public folder is probably something

38
00:01:41,50 --> 00:01:43,90
you're not going to mess with too much.

39
00:01:43,90 --> 00:01:46,20
The folder where you're going to do most of your work

40
00:01:46,20 --> 00:01:48,40
is this source folder.

41
00:01:48,40 --> 00:01:52,60
We're going to go over how these files build your application

42
00:01:52,60 --> 00:01:54,70
in the next video but just know

43
00:01:54,70 --> 00:01:58,60
that the main files here are this index.js

44
00:01:58,60 --> 00:02:00,50
which is sort of your bootstrapper,

45
00:02:00,50 --> 00:02:03,30
it's the first thing that is going to load

46
00:02:03,30 --> 00:02:06,60
and it's essentially going to load your first piece

47
00:02:06,60 --> 00:02:08,70
of your application and everything else

48
00:02:08,70 --> 00:02:10,40
is going to be loaded from here

49
00:02:10,40 --> 00:02:13,30
and as you can see, you're loading a CSS file

50
00:02:13,30 --> 00:02:15,50
and you're actually loading this other component

51
00:02:15,50 --> 00:02:18,70
called App and there's not that many files in there

52
00:02:18,70 --> 00:02:20,10
and then there's like a logo here

53
00:02:20,10 --> 00:02:23,70
as well as a serviceWorker.

54
00:02:23,70 --> 00:02:27,80
That serviceWorker is just a sort of default installation

55
00:02:27,80 --> 00:02:31,10
that allows your application to work a little bit better

56
00:02:31,10 --> 00:02:34,40
on mobile devices when they are offline.

57
00:02:34,40 --> 00:02:38,30
You don't need to worry about this file too much.

58
00:02:38,30 --> 00:02:42,30
In addition to that, you will see a package.json file.

59
00:02:42,30 --> 00:02:45,70
This is very typical with Node.js type projects

60
00:02:45,70 --> 00:02:48,40
and you can see the name of our application right here

61
00:02:48,40 --> 00:02:51,20
as well as a version number

62
00:02:51,20 --> 00:02:53,70
and a set of dependencies,

63
00:02:53,70 --> 00:02:56,90
so these are essentially the plugins that React

64
00:02:56,90 --> 00:03:01,00
is going to use to do its work.

65
00:03:01,00 --> 00:03:02,50
Now, where are these plugins?

66
00:03:02,50 --> 00:03:05,80
These plugins are actually in the node_modules folder.

67
00:03:05,80 --> 00:03:06,60
So, if you look in here,

68
00:03:06,60 --> 00:03:09,70
you'll notice that if you scroll down quite a bit

69
00:03:09,70 --> 00:03:13,50
until you get to the Rs,

70
00:03:13,50 --> 00:03:16,20
you're going to see a folder for React

71
00:03:16,20 --> 00:03:17,90
and that's what this is referring to

72
00:03:17,90 --> 00:03:22,10
as well as the other files here, react-dom is right here,

73
00:03:22,10 --> 00:03:25,80
react-icons and react-scripts.

74
00:03:25,80 --> 00:03:28,00
I mentioned that React uses something

75
00:03:28,00 --> 00:03:30,70
called Webpack and Babel.

76
00:03:30,70 --> 00:03:33,60
Now normally you would see those in here perhaps

77
00:03:33,60 --> 00:03:36,90
in some other applications as dependencies

78
00:03:36,90 --> 00:03:38,80
but React does something pretty interesting

79
00:03:38,80 --> 00:03:42,70
in that it builds everything in a folder

80
00:03:42,70 --> 00:03:44,60
called react-scripts.

81
00:03:44,60 --> 00:03:47,50
The react-scripts folder has the dependencies

82
00:03:47,50 --> 00:03:51,00
for all the other extensions that you're seeing

83
00:03:51,00 --> 00:03:54,00
in this node_modules folder,

84
00:03:54,00 --> 00:03:56,00
so it's cool because it hides sort of the things

85
00:03:56,00 --> 00:03:57,50
that you don't need to see

86
00:03:57,50 --> 00:04:02,00
but it means that some things are not quite as apparent,

87
00:04:02,00 --> 00:04:04,80
so you can't really see the Webpack configuration

88
00:04:04,80 --> 00:04:07,80
although there are ways of customizing that if you need to

89
00:04:07,80 --> 00:04:09,90
and some of that is just all hidden

90
00:04:09,90 --> 00:04:12,60
within that react-scripts folder.

91
00:04:12,60 --> 00:04:15,50
In addition to that, there are some scripts

92
00:04:15,50 --> 00:04:17,40
that you can run from the terminal.

93
00:04:17,40 --> 00:04:20,70
We saw them before when we installed the application

94
00:04:20,70 --> 00:04:23,70
and when you issue the npm start command, for example,

95
00:04:23,70 --> 00:04:28,30
it runs the script called start and react-scripts

96
00:04:28,30 --> 00:04:31,50
and so on and so forth for the other one.

97
00:04:31,50 --> 00:04:32,90
And in addition to that,

98
00:04:32,90 --> 00:04:35,50
you can see that there is some configuration here

99
00:04:35,50 --> 00:04:36,90
for some of the packages

100
00:04:36,90 --> 00:04:41,30
and you can add more configuration here if you need is.

101
00:04:41,30 --> 00:04:44,10
So, this for example, configures how browsers

102
00:04:44,10 --> 00:04:45,50
can preview your files

103
00:04:45,50 --> 00:04:49,70
and what versions you are supporting in those browsers.

104
00:04:49,70 --> 00:04:52,30
There are some other configuration files,

105
00:04:52,30 --> 00:04:57,90
package-lock.json is a more explicit listing

106
00:04:57,90 --> 00:04:59,60
of all of the different things

107
00:04:59,60 --> 00:05:00,90
that are actually getting loaded.

108
00:05:00,90 --> 00:05:03,10
As I mentioned, must of them will be hidden

109
00:05:03,10 --> 00:05:05,90
in package.json, so this is actually does tell you

110
00:05:05,90 --> 00:05:10,00
every single folder that's in the mode_modules folder.

111
00:05:10,00 --> 00:05:11,20
There's also a gitignore file

112
00:05:11,20 --> 00:05:12,30
which you shouldn't mess with

113
00:05:12,30 --> 00:05:15,40
that essentially lets you hide files

114
00:05:15,40 --> 00:05:18,00
from being uploaded into GitHub.

115
00:05:18,00 --> 00:05:20,00
That's why the node_modules file

116
00:05:20,00 --> 00:05:21,60
which is listed right here

117
00:05:21,60 --> 00:05:23,90
doesn't actually get uploaded

118
00:05:23,90 --> 00:05:25,80
and any other files that you would see here,

119
00:05:25,80 --> 00:05:28,90
like for example, the Mac uses this DS_Store file,

120
00:05:28,90 --> 00:05:32,30
those files are not going to be uploaded to Git and GitHub

121
00:05:32,30 --> 00:05:36,50
and also not pushed into any server

122
00:05:36,50 --> 00:05:39,80
that you would push the application into.

123
00:05:39,80 --> 00:05:41,60
So, as I mentioned, in the source folder,

124
00:05:41,60 --> 00:05:45,80
the most important thing is this loader called index.js

125
00:05:45,80 --> 00:05:48,90
and we'll cover how the application actually works

126
00:05:48,90 --> 00:05:52,00
and git-build in the next video.

