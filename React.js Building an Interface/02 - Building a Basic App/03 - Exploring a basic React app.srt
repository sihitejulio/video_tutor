1
00:00:00,50 --> 00:00:02,60
- [Instructor] So let's take a look at this source folder

2
00:00:02,60 --> 00:00:06,30
in more details and find out how the application

3
00:00:06,30 --> 00:00:09,60
is actually getting created and then loaded up

4
00:00:09,60 --> 00:00:11,10
into a browser.

5
00:00:11,10 --> 00:00:14,40
Now I am going to need to start my application

6
00:00:14,40 --> 00:00:15,80
if it's not already going.

7
00:00:15,80 --> 00:00:18,10
We can do that on a separate terminal,

8
00:00:18,10 --> 00:00:20,50
but in Visual Studio code for the Mac,

9
00:00:20,50 --> 00:00:23,20
I can issue this terminal command right from here

10
00:00:23,20 --> 00:00:25,30
by going to the Terminal,

11
00:00:25,30 --> 00:00:27,50
and then choosing New Terminal here.

12
00:00:27,50 --> 00:00:32,50
And then I can run the npn start command.

13
00:00:32,50 --> 00:00:35,20
Now this will pull up a browser,

14
00:00:35,20 --> 00:00:38,20
and once you do that, you'll see the application run.

15
00:00:38,20 --> 00:00:41,20
I'm going to make this window a little bit smaller.

16
00:00:41,20 --> 00:00:42,60
So what you're seeing right here

17
00:00:42,60 --> 00:00:45,60
is the index of that HTML file

18
00:00:45,60 --> 00:00:49,50
that I showed you earlier which is in the Public folder.

19
00:00:49,50 --> 00:00:51,70
So that index.html file for example,

20
00:00:51,70 --> 00:00:53,60
has a name of React App,

21
00:00:53,60 --> 00:00:56,00
and the way that the automation runs,

22
00:00:56,00 --> 00:00:58,70
if you edit that file, you'll see it

23
00:00:58,70 --> 00:01:01,20
automatically update as soon as we save it.

24
00:01:01,20 --> 00:01:03,50
Now once you get the terminal going,

25
00:01:03,50 --> 00:01:05,70
you don't really need it to be showing,

26
00:01:05,70 --> 00:01:10,40
so we can hide that if we need to.

27
00:01:10,40 --> 00:01:12,50
And in here you'll notice that you'll see

28
00:01:12,50 --> 00:01:17,20
sort of typical HTML here like this link to the favicon

29
00:01:17,20 --> 00:01:19,70
as well as some metatags,

30
00:01:19,70 --> 00:01:23,40
and then the actual most important piece

31
00:01:23,40 --> 00:01:26,60
in this whole HTML page is this right here.

32
00:01:26,60 --> 00:01:30,00
This is a div that has an id of root,

33
00:01:30,00 --> 00:01:33,20
and this is where your application is going to run.

34
00:01:33,20 --> 00:01:37,10
If you were adding any additional HTML for your page,

35
00:01:37,10 --> 00:01:39,40
you would put that around,

36
00:01:39,40 --> 00:01:41,30
anywhere around in this area,

37
00:01:41,30 --> 00:01:44,20
while still sort of creating this div with an id of root.

38
00:01:44,20 --> 00:01:47,30
It's really the thing that loads the application.

39
00:01:47,30 --> 00:01:50,20
So that is being loaded from this index

40
00:01:50,20 --> 00:01:52,80
that JS file, and if you look at what it's doing,

41
00:01:52,80 --> 00:01:56,70
it's importing a class from a library.

42
00:01:56,70 --> 00:02:00,10
Now when you import things in React,

43
00:02:00,10 --> 00:02:01,50
use the Import command,

44
00:02:01,50 --> 00:02:04,30
and then you can import, and if the visual library

45
00:02:04,30 --> 00:02:06,80
or something from an individual library,

46
00:02:06,80 --> 00:02:10,60
and we'll be working in both ways later on,

47
00:02:10,60 --> 00:02:12,60
and when you see a name like this,

48
00:02:12,60 --> 00:02:14,20
just a name in quotes,

49
00:02:14,20 --> 00:02:15,80
it means that it's literally loading something

50
00:02:15,80 --> 00:02:17,20
from the node modules folders.

51
00:02:17,20 --> 00:02:19,40
So again if you open that up and we sort of

52
00:02:19,40 --> 00:02:21,70
scroll all the way down to the Rs,

53
00:02:21,70 --> 00:02:24,30
you would see that there is a React folder,

54
00:02:24,30 --> 00:02:27,40
and that it's loading up something called ReactDOM,

55
00:02:27,40 --> 00:02:30,30
which manages the document object model,

56
00:02:30,30 --> 00:02:32,30
which is how web pages are built,

57
00:02:32,30 --> 00:02:33,80
so that's from a different folder,

58
00:02:33,80 --> 00:02:36,70
and it's loading up this class right here.

59
00:02:36,70 --> 00:02:38,70
Now you also see that it's loading

60
00:02:38,70 --> 00:02:40,70
something called index.css.

61
00:02:40,70 --> 00:02:42,30
This is a literal file because you can see

62
00:02:42,30 --> 00:02:44,70
that it starts with a period and a slash,

63
00:02:44,70 --> 00:02:47,60
so if we, let's close this open editors

64
00:02:47,60 --> 00:02:53,40
little window here, and

65
00:02:53,40 --> 00:02:55,30
we'll close up node modules.

66
00:02:55,30 --> 00:02:57,20
Let's go ahead and close this Public folder,

67
00:02:57,20 --> 00:02:58,40
because right now we're looking at

68
00:02:58,40 --> 00:03:01,00
this index.js file right here.

69
00:03:01,00 --> 00:03:02,40
So this is actually loading another file

70
00:03:02,40 --> 00:03:04,80
called index.css in the same folder,

71
00:03:04,80 --> 00:03:07,20
which is right here and you can see that it's

72
00:03:07,20 --> 00:03:09,40
just sort of making the page look nice,

73
00:03:09,40 --> 00:03:12,60
and then in this index.js file,

74
00:03:12,60 --> 00:03:15,80
we're loading something from another file.

75
00:03:15,80 --> 00:03:20,20
Now we're loading a class called App from a file,

76
00:03:20,20 --> 00:03:22,50
and you'll notice that it doesn't have the extension

77
00:03:22,50 --> 00:03:24,90
because it doesn't need to be there.

78
00:03:24,90 --> 00:03:26,60
It assumes that it's a JS extension,

79
00:03:26,60 --> 00:03:28,70
so it's loading this file and a class

80
00:03:28,70 --> 00:03:30,40
that is created on that file.

81
00:03:30,40 --> 00:03:33,50
In addition to that, it imports the service worker file,

82
00:03:33,50 --> 00:03:35,20
again you don't need to have this,

83
00:03:35,20 --> 00:03:37,50
but you could just leave it, it's not going to hurt anything.

84
00:03:37,50 --> 00:03:38,80
It's probably going to help.

85
00:03:38,80 --> 00:03:43,10
And then what it does, is it uses the ReactDOM class

86
00:03:43,10 --> 00:03:45,20
to render the application,

87
00:03:45,20 --> 00:03:47,50
and the application it's going to render is

88
00:03:47,50 --> 00:03:50,00
something that looks like an HTML tag,

89
00:03:50,00 --> 00:03:51,80
but it's sort of custom.

90
00:03:51,80 --> 00:03:55,30
So this is coming from this app right here.

91
00:03:55,30 --> 00:03:59,10
When a component is created in React,

92
00:03:59,10 --> 00:04:03,10
it let's you call it like an HTML tag,

93
00:04:03,10 --> 00:04:05,90
and they're always going to match in name,

94
00:04:05,90 --> 00:04:07,70
so this app right here refers to this,

95
00:04:07,70 --> 00:04:10,50
and then when you load this and you render the page,

96
00:04:10,50 --> 00:04:14,80
you're going to target an element in your HTML page,

97
00:04:14,80 --> 00:04:17,40
and we created an element with an id of root,

98
00:04:17,40 --> 00:04:19,50
so we're loading it by using this document

99
00:04:19,50 --> 00:04:22,60
getElementById of root,

100
00:04:22,60 --> 00:04:24,60
so that loads everything up,

101
00:04:24,60 --> 00:04:27,20
and remember in the index.html file

102
00:04:27,20 --> 00:04:29,70
we saw that div with an id,

103
00:04:29,70 --> 00:04:33,80
so now if we look at the App.Js folder,

104
00:04:33,80 --> 00:04:35,20
you'll see that it does similar things,

105
00:04:35,20 --> 00:04:39,60
so here is a place where it's loading the react library,

106
00:04:39,60 --> 00:04:42,00
and then something called Component.

107
00:04:42,00 --> 00:04:45,50
So this is one of the other classes that React

108
00:04:45,50 --> 00:04:47,90
lets you use and it's pretty much

109
00:04:47,90 --> 00:04:50,20
the most common class that you'll be using

110
00:04:50,20 --> 00:04:53,00
and it's just a feature within the library

111
00:04:53,00 --> 00:04:55,10
that is the React library.

112
00:04:55,10 --> 00:04:56,90
And in addition to that, we can also,

113
00:04:56,90 --> 00:04:59,40
this is interesting, we're loading a logo,

114
00:04:59,40 --> 00:05:01,60
and we're putting it into a variable,

115
00:05:01,60 --> 00:05:03,80
so this import is a little bit different,

116
00:05:03,80 --> 00:05:06,40
so this is loading up this logo.svg file

117
00:05:06,40 --> 00:05:08,30
and putting it into a variable.

118
00:05:08,30 --> 00:05:11,70
And then also loading up app specific CSS,

119
00:05:11,70 --> 00:05:14,20
so in this line, the interesting thing about React

120
00:05:14,20 --> 00:05:19,60
is that the CSS for subcomponents only gets loaded

121
00:05:19,60 --> 00:05:24,00
if the application has that component showing,

122
00:05:24,00 --> 00:05:26,50
which is cool because in a lot of web pages

123
00:05:26,50 --> 00:05:29,30
you'll have pieces of forms and everything

124
00:05:29,30 --> 00:05:31,20
might be dynamic and showing up

125
00:05:31,20 --> 00:05:33,10
and getting created and deleted,

126
00:05:33,10 --> 00:05:37,70
and this means that when we use a component in React,

127
00:05:37,70 --> 00:05:41,90
the CSS can only be loaded if a component is showing.

128
00:05:41,90 --> 00:05:43,40
Right? So that's cool.

129
00:05:43,40 --> 00:05:46,90
So now, we can also start out by creating

130
00:05:46,90 --> 00:05:49,20
our custom class here called App,

131
00:05:49,20 --> 00:05:52,00
and that App is based on component,

132
00:05:52,00 --> 00:05:55,30
but it extends it, it adds some additional information.

133
00:05:55,30 --> 00:05:57,50
And then everything that happens in React

134
00:05:57,50 --> 00:06:01,30
happens in this render method or command,

135
00:06:01,30 --> 00:06:04,20
and the render command allows you to

136
00:06:04,20 --> 00:06:07,30
return what the application's actually going to look like.

137
00:06:07,30 --> 00:06:10,10
So this actually where this logo and this text

138
00:06:10,10 --> 00:06:13,30
is all coming from, notice that this logo's rotating here,

139
00:06:13,30 --> 00:06:16,10
and you can see that it looks like HTML,

140
00:06:16,10 --> 00:06:17,60
but don't be fooled,

141
00:06:17,60 --> 00:06:20,60
it's actually a language called JSX

142
00:06:20,60 --> 00:06:25,90
which is a combination of HTML and actually XHTML

143
00:06:25,90 --> 00:06:27,60
if you're going to be precise,

144
00:06:27,60 --> 00:06:32,00
and also JavaScript.

145
00:06:32,00 --> 00:06:34,10
So notice that for example instead of using

146
00:06:34,10 --> 00:06:37,90
the keyword class, which you would use in normal HTML,

147
00:06:37,90 --> 00:06:40,20
you have to use something called className,

148
00:06:40,20 --> 00:06:43,70
and that's because class means something in JavaScript,

149
00:06:43,70 --> 00:06:45,70
and in this language anything that is a keyword

150
00:06:45,70 --> 00:06:48,90
in JavaScript, is something that you don't end up using,

151
00:06:48,90 --> 00:06:50,50
so you just have to get used to

152
00:06:50,50 --> 00:06:52,80
that some things that you're used to calling,

153
00:06:52,80 --> 00:06:54,20
like for example in forms

154
00:06:54,20 --> 00:06:58,20
if you want to use the keyword four in a label,

155
00:06:58,20 --> 00:07:02,30
you can't use four because that obviously has a meaning

156
00:07:02,30 --> 00:07:06,10
of a looping sort of keyword in JavaScript,

157
00:07:06,10 --> 00:07:08,50
so you have to use something called HTML4.

158
00:07:08,50 --> 00:07:10,60
So that's one of the things that

159
00:07:10,60 --> 00:07:12,00
always gets people in React.

160
00:07:12,00 --> 00:07:15,20
Notice how we're actually using the logo that we imported,

161
00:07:15,20 --> 00:07:18,30
so here we imported this svg into this variable,

162
00:07:18,30 --> 00:07:20,80
and then we can just use it using an expression.

163
00:07:20,80 --> 00:07:22,80
So whenever you see these curly braces,

164
00:07:22,80 --> 00:07:24,90
that's known as an expression,

165
00:07:24,90 --> 00:07:27,90
and an expression just means almost like a formula,

166
00:07:27,90 --> 00:07:31,20
and in that formula you can put in normal,

167
00:07:31,20 --> 00:07:33,60
quasi-normal JavaScript,

168
00:07:33,60 --> 00:07:35,30
and what we're doing here is just saying

169
00:07:35,30 --> 00:07:37,00
output the logo here,

170
00:07:37,00 --> 00:07:38,50
and then we give it a class name

171
00:07:38,50 --> 00:07:40,40
and everything else looks normal.

172
00:07:40,40 --> 00:07:42,70
The only other thing that you have to watch out for

173
00:07:42,70 --> 00:07:46,00
is that in React, you have this return statement

174
00:07:46,00 --> 00:07:48,60
and this returns essentially your HTML.

175
00:07:48,60 --> 00:07:50,10
The key thing to remember here is that

176
00:07:50,10 --> 00:07:54,20
you can only return a single HTML element,

177
00:07:54,20 --> 00:07:58,00
so if you try to add another div here,

178
00:07:58,00 --> 00:08:00,60
it will not work, it will crash actually the application,

179
00:08:00,60 --> 00:08:03,80
so it's going to give you an error,

180
00:08:03,80 --> 00:08:06,70
and that's because you can't return two elements,

181
00:08:06,70 --> 00:08:08,90
you have to return a single element.

182
00:08:08,90 --> 00:08:12,10
So you just end up wrapping everything around

183
00:08:12,10 --> 00:08:14,00
a single container element.

184
00:08:14,00 --> 00:08:16,10
You could put another div in here,

185
00:08:16,10 --> 00:08:18,30
and wouldn't complain about it at all,

186
00:08:18,30 --> 00:08:23,10
it would just, the editor's actually rewriting that as div.

187
00:08:23,10 --> 00:08:25,90
And that's another thing in Jsx,

188
00:08:25,90 --> 00:08:29,10
if you have a single element like this image element,

189
00:08:29,10 --> 00:08:33,70
it gets a slash, which if you've ever used XHTML,

190
00:08:33,70 --> 00:08:35,50
that's how that works.

191
00:08:35,50 --> 00:08:36,90
So hopefully that gives you an idea

192
00:08:36,90 --> 00:08:38,50
of how the application works.

193
00:08:38,50 --> 00:08:41,50
The last thing you have to do is export the component

194
00:08:41,50 --> 00:08:44,50
because if you remember, we're importing this component

195
00:08:44,50 --> 00:08:48,00
into index.js, and that is how that information

196
00:08:48,00 --> 00:08:54,00
gets fed and how the tab with an id of root gets replaced.

