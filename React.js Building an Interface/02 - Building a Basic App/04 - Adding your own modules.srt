1
00:00:00,10 --> 00:00:02,40
- [Instructor] The default installation would be fine

2
00:00:02,40 --> 00:00:05,30
if I just wanted to show you how to use React itself.

3
00:00:05,30 --> 00:00:07,70
But in a realistic project,

4
00:00:07,70 --> 00:00:10,10
you're going to want to often install

5
00:00:10,10 --> 00:00:12,40
additional libraries or frameworks,

6
00:00:12,40 --> 00:00:14,50
so I'm going to show you how to do that in this video.

7
00:00:14,50 --> 00:00:16,70
Now essentially we're going to need to pull up a terminal

8
00:00:16,70 --> 00:00:18,90
and then issue an npm install command,

9
00:00:18,90 --> 00:00:21,60
and then bring in the frameworks and utilities

10
00:00:21,60 --> 00:00:23,90
that we need to make things work.

11
00:00:23,90 --> 00:00:25,60
So I'm going to pull up the terminal.

12
00:00:25,60 --> 00:00:27,80
Again, it's right here under Terminal menu

13
00:00:27,80 --> 00:00:30,70
and you can choose New Terminal.

14
00:00:30,70 --> 00:00:32,70
You can also use the Command key,

15
00:00:32,70 --> 00:00:35,30
so I'm going to do that to pull up the terminal.

16
00:00:35,30 --> 00:00:36,20
Again, if you're at a PC,

17
00:00:36,20 --> 00:00:39,70
you may want to use git bash in a separate terminal,

18
00:00:39,70 --> 00:00:43,40
and you can leave the original application running.

19
00:00:43,40 --> 00:00:46,30
So I actually have two terminals open

20
00:00:46,30 --> 00:00:48,10
and I can go to this and you can see that

21
00:00:48,10 --> 00:00:50,80
my application is running on this terminal,

22
00:00:50,80 --> 00:00:52,50
and then on the other one,

23
00:00:52,50 --> 00:00:55,20
it's just an empty terminal right here.

24
00:00:55,20 --> 00:00:57,70
So I'm going to need to do an npm install

25
00:00:57,70 --> 00:00:59,60
and you can type in npm install,

26
00:00:59,60 --> 00:01:02,90
or use the shortcut npm i,

27
00:01:02,90 --> 00:01:05,70
and then you can use --save,

28
00:01:05,70 --> 00:01:08,90
and that would save the installations

29
00:01:08,90 --> 00:01:11,90
as a dependency of the project.

30
00:01:11,90 --> 00:01:16,30
So that would actually put them in this package.json file.

31
00:01:16,30 --> 00:01:19,20
So they'll appear here under the dependency section

32
00:01:19,20 --> 00:01:20,80
as we install them.

33
00:01:20,80 --> 00:01:23,70
Now you can also use a shortcut as -s,

34
00:01:23,70 --> 00:01:25,90
and then you can type in the libraries

35
00:01:25,90 --> 00:01:27,20
that you want to bring in.

36
00:01:27,20 --> 00:01:28,70
So we're going to bring in a bunch of them.

37
00:01:28,70 --> 00:01:31,70
We're going to bring in bootstrap,

38
00:01:31,70 --> 00:01:36,40
also something called react-icons.

39
00:01:36,40 --> 00:01:38,50
We're going to bring in lodash,

40
00:01:38,50 --> 00:01:40,30
and I'll be covering this in just a minute

41
00:01:40,30 --> 00:01:42,00
while they're installing,

42
00:01:42,00 --> 00:01:47,20
so for right now let's just issue the command, so lodash.

43
00:01:47,20 --> 00:01:50,90
And bootstrap requires that you also install jquery

44
00:01:50,90 --> 00:01:54,50
as well as something called popper.js.

45
00:01:54,50 --> 00:01:58,20
And we're also going to install moment

46
00:01:58,20 --> 00:02:01,40
and also something called react-moment.

47
00:02:01,40 --> 00:02:03,40
Now that's quite a lot of stuff,

48
00:02:03,40 --> 00:02:05,10
so let's go ahead and get it started,

49
00:02:05,10 --> 00:02:06,00
and it's going to sit there

50
00:02:06,00 --> 00:02:08,80
and do a lot of installations in the background.

51
00:02:08,80 --> 00:02:10,30
In the meantime, I'm going to go over

52
00:02:10,30 --> 00:02:12,00
each one of those installation.

53
00:02:12,00 --> 00:02:16,50
So Bootstrap is a responsive, mobile-first framework

54
00:02:16,50 --> 00:02:19,00
that you can use to just make it easier

55
00:02:19,00 --> 00:02:20,60
to lay out your projects.

56
00:02:20,60 --> 00:02:21,50
I'm going to be using

57
00:02:21,50 --> 00:02:24,40
a lot of Bootstrap code in this course.

58
00:02:24,40 --> 00:02:26,80
And we've got courses on Bootstrap that you can check out

59
00:02:26,80 --> 00:02:30,10
if you want to see what everything means in the HTML.

60
00:02:30,10 --> 00:02:31,10
I'm not going to cover that,

61
00:02:31,10 --> 00:02:32,80
but you can take that course.

62
00:02:32,80 --> 00:02:35,20
And I'm using the regular version of Bootstrap.

63
00:02:35,20 --> 00:02:39,30
There are versions that are a little bit more Reactified,

64
00:02:39,30 --> 00:02:40,40
but I just want to focus

65
00:02:40,40 --> 00:02:43,30
on the React piece of how things work.

66
00:02:43,30 --> 00:02:45,10
So I want to show you how to install

67
00:02:45,10 --> 00:02:47,20
just a regular version of Bootstrap

68
00:02:47,20 --> 00:02:49,00
and you're going to see how I also use

69
00:02:49,00 --> 00:02:51,50
componetized version of libraries as well

70
00:02:51,50 --> 00:02:55,40
as we go through some of the different installations.

71
00:02:55,40 --> 00:02:56,80
And that's just realistic.

72
00:02:56,80 --> 00:03:00,10
Sometimes, you know, not everything is going to require you

73
00:03:00,10 --> 00:03:02,80
to have sort of componetized versions

74
00:03:02,80 --> 00:03:04,30
of modules and extensions,

75
00:03:04,30 --> 00:03:06,90
and so I'm going to show you both things.

76
00:03:06,90 --> 00:03:08,50
So in addition to that, as I mentioned,

77
00:03:08,50 --> 00:03:10,50
Bootstrap requires that you use jQuery.

78
00:03:10,50 --> 00:03:14,50
You can find out more about jQuery at this URL.

79
00:03:14,50 --> 00:03:15,90
And in addition to that,

80
00:03:15,90 --> 00:03:19,20
it also requires something called popper.js,

81
00:03:19,20 --> 00:03:22,40
which you can find out about more at this URL.

82
00:03:22,40 --> 00:03:25,00
And we're installing the Lodash library,

83
00:03:25,00 --> 00:03:28,40
which has a lot of utilities for managing arrays.

84
00:03:28,40 --> 00:03:30,30
We could create our own methods

85
00:03:30,30 --> 00:03:32,10
and functions for handling arrays,

86
00:03:32,10 --> 00:03:34,80
but this library does a fantastic job.

87
00:03:34,80 --> 00:03:37,10
I use it in a lot of projects.

88
00:03:37,10 --> 00:03:39,70
It works really well with React

89
00:03:39,70 --> 00:03:44,00
and you can import just the component that you need

90
00:03:44,00 --> 00:03:45,80
instead of importing the whole library.

91
00:03:45,80 --> 00:03:48,90
And again I think this is realistic to the way

92
00:03:48,90 --> 00:03:52,30
that you would work if you were building a project.

93
00:03:52,30 --> 00:03:54,80
Also, I'm going to use Moment.js,

94
00:03:54,80 --> 00:03:57,90
which is a library that allows you

95
00:03:57,90 --> 00:04:01,50
to manipulate dates and times in JavaScript.

96
00:04:01,50 --> 00:04:02,70
With that, I'll be installing

97
00:04:02,70 --> 00:04:05,30
something else called react-moment,

98
00:04:05,30 --> 00:04:10,00
which is a componetized version of the moment library

99
00:04:10,00 --> 00:04:13,60
because it happens to be a very large library

100
00:04:13,60 --> 00:04:15,80
and you only need a little piece

101
00:04:15,80 --> 00:04:17,60
that allows you to format dates.

102
00:04:17,60 --> 00:04:21,50
So react-moment needs moment,

103
00:04:21,50 --> 00:04:23,30
it's just a small version

104
00:04:23,30 --> 00:04:26,50
that let's you install the piece that you need.

105
00:04:26,50 --> 00:04:27,30
In addition to that,

106
00:04:27,30 --> 00:04:30,40
we're going to use this thing called React Icons,

107
00:04:30,40 --> 00:04:32,60
which is a library that allows you

108
00:04:32,60 --> 00:04:36,00
to import a number of different icon libraries.

109
00:04:36,00 --> 00:04:37,50
So you can see that they're all right here.

110
00:04:37,50 --> 00:04:41,70
Font Awesome, Ionicons, Material Design Icons,

111
00:04:41,70 --> 00:04:42,80
et cetera, et cetera.

112
00:04:42,80 --> 00:04:46,80
I'm mostly going to be using the Font Awesome Icons.

113
00:04:46,80 --> 00:04:51,00
And this is also a very sort of componetized library,

114
00:04:51,00 --> 00:04:53,00
which means that you're just going to install

115
00:04:53,00 --> 00:04:55,00
the little piece of whatever you need.

116
00:04:55,00 --> 00:04:58,20
In this case, we're going to use a couple of small icons

117
00:04:58,20 --> 00:05:02,10
that you're going to see as we build the application.

118
00:05:02,10 --> 00:05:03,80
So let's check this out.

119
00:05:03,80 --> 00:05:06,10
And it looks like everything is done.

120
00:05:06,10 --> 00:05:08,70
If you want to, you can issue a clear command.

121
00:05:08,70 --> 00:05:11,10
So once you've installed all of these things,

122
00:05:11,10 --> 00:05:14,30
they will appear here as a series of dependencies.

123
00:05:14,30 --> 00:05:16,30
You can see bootstrap, jquery,

124
00:05:16,30 --> 00:05:18,00
and the other things in here.

125
00:05:18,00 --> 00:05:22,10
Now these are added into the node_modules folder.

126
00:05:22,10 --> 00:05:23,20
Now if you don't see them,

127
00:05:23,20 --> 00:05:25,90
make sure you hit this icon in Visual Studio Code.

128
00:05:25,90 --> 00:05:29,80
Sometimes the refresh doesn't happen right away.

129
00:05:29,80 --> 00:05:31,90
And what we also need to do

130
00:05:31,90 --> 00:05:35,90
is actually bring them into the different components

131
00:05:35,90 --> 00:05:40,00
or the different files that we want to import them into.

132
00:05:40,00 --> 00:05:42,00
So in the case of bootstrap

133
00:05:42,00 --> 00:05:44,60
and some of these other main libraries,

134
00:05:44,60 --> 00:05:47,40
you're going to want to import them in the main component.

135
00:05:47,40 --> 00:05:49,90
And because this is going to apply

136
00:05:49,90 --> 00:05:52,00
to essentially the whole page,

137
00:05:52,00 --> 00:05:55,50
so in here, before we import our CSS,

138
00:05:55,50 --> 00:05:58,00
I'm going to go ahead and import,

139
00:05:58,00 --> 00:06:02,10
and from the bootstrap folder in our node_modules folder,

140
00:06:02,10 --> 00:06:05,00
and you don't have to put node_modules here, which is cool,

141
00:06:05,00 --> 00:06:08,10
you want to look for the distribution folder

142
00:06:08,10 --> 00:06:12,90
and then look for bootstrap.css,

143
00:06:12,90 --> 00:06:19,00
and you can leave this index.css alone for right now.

144
00:06:19,00 --> 00:06:20,30
And then over here,

145
00:06:20,30 --> 00:06:22,80
I'm going to import some of the other files.

146
00:06:22,80 --> 00:06:27,30
So in the order that they are required for working,

147
00:06:27,30 --> 00:06:29,30
just like what you would do with a bootstrap installation,

148
00:06:29,30 --> 00:06:31,80
you want to bring up jquery first,

149
00:06:31,80 --> 00:06:33,50
and this is going to be the JavaScript.

150
00:06:33,50 --> 00:06:38,40
So again this refers to the jquery folder in node_modules.

151
00:06:38,40 --> 00:06:41,60
So if we bring this back up here,

152
00:06:41,60 --> 00:06:44,20
you can see the bootstrap folder,

153
00:06:44,20 --> 00:06:47,30
and in here there's a distribution folder.

154
00:06:47,30 --> 00:06:49,30
And you can see that there's a css folder

155
00:06:49,30 --> 00:06:50,80
and a javascript folder.

156
00:06:50,80 --> 00:06:53,20
The same thing for the other ones that we're bringing.

157
00:06:53,20 --> 00:06:54,80
And we're going to bring in jquery

158
00:06:54,80 --> 00:06:56,80
so there's a jquery folder.

159
00:06:56,80 --> 00:06:59,60
And we're just given instructions

160
00:06:59,60 --> 00:07:04,00
for getting to the folder and the locations in the folders.

161
00:07:04,00 --> 00:07:08,70
So jquery, distribution, jquery.js,

162
00:07:08,70 --> 00:07:13,90
and then we're going to import the popper library,

163
00:07:13,90 --> 00:07:17,40
and popper actually has the .js name on it,

164
00:07:17,40 --> 00:07:18,40
distribution folder.

165
00:07:18,40 --> 00:07:20,20
And this one is a little bit different,

166
00:07:20,20 --> 00:07:24,50
we want to import the umd subfolder.

167
00:07:24,50 --> 00:07:27,40
And that's because popper is something

168
00:07:27,40 --> 00:07:30,20
that can be broken down into components as well,

169
00:07:30,20 --> 00:07:33,20
so we'll need the componetized version of it.

170
00:07:33,20 --> 00:07:35,90
And so it's just popper.js,

171
00:07:35,90 --> 00:07:40,60
and finally I want to put semicolons in here.

172
00:07:40,60 --> 00:07:43,90
Import the bootstrap javascript,

173
00:07:43,90 --> 00:07:47,70
so bootstrap, distribution, js,

174
00:07:47,70 --> 00:07:50,60
and bootstrap.javascript here,

175
00:07:50,60 --> 00:07:53,10
and save that with a semicolon.

176
00:07:53,10 --> 00:07:55,60
Save this.

177
00:07:55,60 --> 00:07:57,30
So looks like I made a mistake

178
00:07:57,30 --> 00:08:00,10
here in the bootstrap.css file.

179
00:08:00,10 --> 00:08:04,60
This is how React gives you errors.

180
00:08:04,60 --> 00:08:05,50
It's really nice,

181
00:08:05,50 --> 00:08:08,00
it just brings 'em up in the browser, which is cool,

182
00:08:08,00 --> 00:08:09,40
and you'll see different ways

183
00:08:09,40 --> 00:08:11,90
of getting to errors in the future.

184
00:08:11,90 --> 00:08:15,00
But I forgot actually to put in the css subfolder here,

185
00:08:15,00 --> 00:08:17,10
so let's go ahead and save that.

186
00:08:17,10 --> 00:08:19,00
And you may notice that now

187
00:08:19,00 --> 00:08:20,90
we get a slightly different color.

188
00:08:20,90 --> 00:08:22,30
This is the only way we can tell right now

189
00:08:22,30 --> 00:08:23,80
that it's loading properly.

190
00:08:23,80 --> 00:08:27,70
There's a slightly different color here to this section,

191
00:08:27,70 --> 00:08:32,20
but now I know that all of my libraries have been imported.

192
00:08:32,20 --> 00:08:34,50
And this is how you do different modules

193
00:08:34,50 --> 00:08:36,00
and different frameworks.

194
00:08:36,00 --> 00:08:38,60
You just kind of import the pieces that you need.

195
00:08:38,60 --> 00:08:39,40
You'll see in the future

196
00:08:39,40 --> 00:08:42,10
that when we need things that are more componetized

197
00:08:42,10 --> 00:08:46,70
like the icons and the moment.js library

198
00:08:46,70 --> 00:08:49,10
that we're just going to bring in the pieces that we need

199
00:08:49,10 --> 00:08:51,20
into the components that we need.

200
00:08:51,20 --> 00:08:53,50
But these are the global imports that we need to do

201
00:08:53,50 --> 00:08:56,00
in order for my project to work.

