1
00:00:00,00 --> 00:00:02,20
- [Instructor] Create React app gives you

2
00:00:02,20 --> 00:00:05,30
a nice template, but you usually want to put things

3
00:00:05,30 --> 00:00:07,00
in different folder structures,

4
00:00:07,00 --> 00:00:09,10
so let me show you how to do that.

5
00:00:09,10 --> 00:00:10,70
What I'm going to do is I'm going to create

6
00:00:10,70 --> 00:00:13,20
a couple of folders, one where I'll place

7
00:00:13,20 --> 00:00:16,60
all my CSS and then another one where I'll

8
00:00:16,60 --> 00:00:19,10
put in all my components.

9
00:00:19,10 --> 00:00:23,10
That means that App.css is going to go here

10
00:00:23,10 --> 00:00:25,30
and anything that's the app will go

11
00:00:25,30 --> 00:00:27,80
into the components folder.

12
00:00:27,80 --> 00:00:29,90
Before I move them though, I'm going to go ahead

13
00:00:29,90 --> 00:00:33,80
and stop the terminal if it's already running.

14
00:00:33,80 --> 00:00:36,50
Clear that out, because things will start

15
00:00:36,50 --> 00:00:39,40
breaking as soon as I move things around.

16
00:00:39,40 --> 00:00:41,70
First off, let's grab this App.css,

17
00:00:41,70 --> 00:00:45,60
put it in here, and also the index.css file

18
00:00:45,60 --> 00:00:46,90
will go in there.

19
00:00:46,90 --> 00:00:49,80
And then we'll take the App.js,

20
00:00:49,80 --> 00:00:51,40
put it in components.

21
00:00:51,40 --> 00:00:52,60
I'm going to get rid of this test

22
00:00:52,60 --> 00:00:55,70
because we won't be doing those in this course.

23
00:00:55,70 --> 00:00:58,90
The index.js, just leave it in the main folder.

24
00:00:58,90 --> 00:01:00,90
We're not going to use this logo anymore,

25
00:01:00,90 --> 00:01:02,80
so let's get rid of it.

26
00:01:02,80 --> 00:01:04,00
We'll need the serviceWorker.

27
00:01:04,00 --> 00:01:07,10
It's a nice feature and you won't have to

28
00:01:07,10 --> 00:01:09,20
mess around with it too much.

29
00:01:09,20 --> 00:01:12,50
Let's go ahead now and look at

30
00:01:12,50 --> 00:01:15,30
our index.js file.

31
00:01:15,30 --> 00:01:18,90
It has a reference to the app that is wrong now,

32
00:01:18,90 --> 00:01:23,60
so I need to make sure that I add components.

33
00:01:23,60 --> 00:01:29,10
And also, my CSS now will be in the CSS folder.

34
00:01:29,10 --> 00:01:31,60
In addition to that, let's make sure I save that.

35
00:01:31,60 --> 00:01:34,30
Then I need to make sure that my App.js

36
00:01:34,30 --> 00:01:37,10
also has the proper references.

37
00:01:37,10 --> 00:01:38,90
Here, I need to back up a folder

38
00:01:38,90 --> 00:01:40,60
because I'm in the components folder

39
00:01:40,60 --> 00:01:42,90
and look for the CSS sub-folder,

40
00:01:42,90 --> 00:01:46,40
and then look for the App.js file right there.

41
00:01:46,40 --> 00:01:47,60
I'm going to delete this logo

42
00:01:47,60 --> 00:01:49,40
because I'm not using it anymore.

43
00:01:49,40 --> 00:01:51,60
And then I'll also get rid of this

44
00:01:51,60 --> 00:01:55,90
image right here just so we don't have any problems.

45
00:01:55,90 --> 00:01:57,40
That's really all you need to do;

46
00:01:57,40 --> 00:02:02,90
re-create the lengths if you move things around.

47
00:02:02,90 --> 00:02:05,90
Next up, I'm going to install some code that

48
00:02:05,90 --> 00:02:08,30
I've created for you so that this can

49
00:02:08,30 --> 00:02:10,00
start looking more like the application

50
00:02:10,00 --> 00:02:11,40
that we'll be building.

51
00:02:11,40 --> 00:02:14,30
We'll start with the index.html document,

52
00:02:14,30 --> 00:02:16,20
and I've prepared this just for you.

53
00:02:16,20 --> 00:02:19,00
To copy this, I'll hit this Raw button.

54
00:02:19,00 --> 00:02:21,20
This is just some basic bootstrap code,

55
00:02:21,20 --> 00:02:23,00
so you can check out our course on bootstrap

56
00:02:23,00 --> 00:02:27,00
if you want to learn more about how this works.

57
00:02:27,00 --> 00:02:28,70
Here what I'll do is I'll replace this

58
00:02:28,70 --> 00:02:32,70
div right here and paste this code.

59
00:02:32,70 --> 00:02:34,60
You can see that the div for the root is

60
00:02:34,60 --> 00:02:36,10
actually there, but what I'm doing is I'm

61
00:02:36,10 --> 00:02:38,40
adding a header with some text

62
00:02:38,40 --> 00:02:41,10
and then a footer with some text down here

63
00:02:41,10 --> 00:02:42,70
with, again, some bootstrap stuff.

64
00:02:42,70 --> 00:02:45,40
So we'll save that.

65
00:02:45,40 --> 00:02:49,20
Next up, I need to load the CSS.

66
00:02:49,20 --> 00:02:52,20
I have the CSS at this gist.

67
00:02:52,20 --> 00:02:54,10
I'm going to hit this Raw button.

68
00:02:54,10 --> 00:02:57,60
Just some basic CSS there to make things look nice.

69
00:02:57,60 --> 00:03:01,90
I need to go into the index.css file

70
00:03:01,90 --> 00:03:05,20
and replace what I have there.

71
00:03:05,20 --> 00:03:09,20
And then finally, I need to go into

72
00:03:09,20 --> 00:03:12,00
this HTML that's going to replace

73
00:03:12,00 --> 00:03:14,20
what's in the (mumbles) folder.

74
00:03:14,20 --> 00:03:17,30
Click on Raw, copy that.

75
00:03:17,30 --> 00:03:20,00
And in App.js ...

76
00:03:20,00 --> 00:03:23,80
This is sort of interesting about React.

77
00:03:23,80 --> 00:03:27,60
You really end up creating all the CSS

78
00:03:27,60 --> 00:03:30,80
and HTML in the same place.

79
00:03:30,80 --> 00:03:33,50
So we will just replace

80
00:03:33,50 --> 00:03:35,60
this entire thing right here with

81
00:03:35,60 --> 00:03:39,80
just our placeholder bootstrap code.

82
00:03:39,80 --> 00:03:41,70
One thing that you have to remember;

83
00:03:41,70 --> 00:03:44,30
when you bring in code from

84
00:03:44,30 --> 00:03:47,10
something like bootstrap or some other framework,

85
00:03:47,10 --> 00:03:50,40
remember that class is not going to work by itself.

86
00:03:50,40 --> 00:03:53,30
You'd get an error if you tried to run this.

87
00:03:53,30 --> 00:03:57,20
We have to switch that to className in JSX

88
00:03:57,20 --> 00:04:00,10
and then have to do that for our index.html

89
00:04:00,10 --> 00:04:03,00
because this is actually regular HTML.

90
00:04:03,00 --> 00:04:05,30
But in the components, you do have to change

91
00:04:05,30 --> 00:04:08,50
class to className in your HTML.

92
00:04:08,50 --> 00:04:12,00
So I left the gist with the word

93
00:04:12,00 --> 00:04:14,20
class in there on purpose.

94
00:04:14,20 --> 00:04:17,20
Let's go ahead and save this one as well.

95
00:04:17,20 --> 00:04:19,00
Let's go ahead and pull up our terminal.

96
00:04:19,00 --> 00:04:20,80
You can go to Terminal, New Terminal

97
00:04:20,80 --> 00:04:22,00
if you're on a Mac,

98
00:04:22,00 --> 00:04:24,20
or use the Git BASH application that you got

99
00:04:24,20 --> 00:04:27,10
when you downloaded Git if you're on PC,

100
00:04:27,10 --> 00:04:29,90
or your favorite Linux-type terminal,

101
00:04:29,90 --> 00:04:34,40
and then run npm start.

102
00:04:34,40 --> 00:04:37,00
If we didn't make any mistakes,

103
00:04:37,00 --> 00:04:38,80
we should see something like this.

104
00:04:38,80 --> 00:04:41,10
What I've done here is created a placeholder

105
00:04:41,10 --> 00:04:43,60
for what will be my components.

106
00:04:43,60 --> 00:04:45,20
In the next video, I'm going to show you

107
00:04:45,20 --> 00:04:47,00
how to create those.

