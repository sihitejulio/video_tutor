1
00:00:00,50 --> 00:00:02,90
- [Presenter] So at this point

2
00:00:02,90 --> 00:00:04,90
and backend for our blog site.

3
00:00:04,90 --> 00:00:05,80
It's time to take a look at

4
00:00:05,80 --> 00:00:07,10
how to actually release our site

5
00:00:07,10 --> 00:00:09,20
so that people can access it.

6
00:00:09,20 --> 00:00:12,90
And for this, we're going to be using Amazon web services.

7
00:00:12,90 --> 00:00:14,30
But before we get to that,

8
00:00:14,30 --> 00:00:16,30
there are just a few final modifications

9
00:00:16,30 --> 00:00:18,10
that we have to make in order for our app

10
00:00:18,10 --> 00:00:20,40
to be ready to release.

11
00:00:20,40 --> 00:00:22,00
So the first thing we're going to change

12
00:00:22,00 --> 00:00:23,90
is the title that shows up on the tab

13
00:00:23,90 --> 00:00:27,60
that our app is open in.

14
00:00:27,60 --> 00:00:32,30
the default title from React, so we want to get rid of that.

15
00:00:32,30 --> 00:00:35,40
And ideally, we want to eliminate all those filler names

16
00:00:35,40 --> 00:00:38,20
that Create React app gave us.

17
00:00:38,20 --> 00:00:42,00
So there are two places we want to go to make these changes.

18
00:00:42,00 --> 00:00:45,60
The first is inside our front end's public directory,

19
00:00:45,60 --> 00:00:49,40
in the file index.html.

20
00:00:49,40 --> 00:00:53,00
We want to find the title tag here where it says React app,

21
00:00:53,00 --> 00:00:55,80
right here, and change that

22
00:00:55,80 --> 00:01:00,10
to whatever we want our app to be called.

23
00:01:00,10 --> 00:01:08,40
but feel free to name yours whatever you want.

24
00:01:08,40 --> 00:01:10,70
also in the public directory.

25
00:01:10,70 --> 00:01:15,30
So we want to change the short name and name properties here

26
00:01:15,30 --> 00:01:24,40
to something a little more personal.

27
00:01:24,40 --> 00:01:33,60
I'm going to put My Blog - A Blog about all things programming.

28
00:01:33,60 --> 00:01:36,40
And that's all our app needs to be ready to build.

29
00:01:36,40 --> 00:01:39,00
So now what we're going to do is build our app,

30
00:01:39,00 --> 00:01:40,80
and to do that, open a terminal,

31
00:01:40,80 --> 00:01:45,20
and make sure you're inside the My Blog directory.

32
00:01:45,20 --> 00:01:53,40
And we're going to run mpm run build, and hit enter.

33
00:01:53,40 --> 00:01:56,50
we'll see that our directory now contains a build folder,

34
00:01:56,50 --> 00:02:01,70
and this contains the complied source code of our React App.

35
00:02:01,70 --> 00:02:14,90
is copy and paste this build folder

36
00:02:14,90 --> 00:02:21,40
And this will allow our server

37
00:02:21,40 --> 00:02:25,30
both the frontend website,

38
00:02:25,30 --> 00:02:27,80
such as the article info.

39
00:02:27,80 --> 00:02:34,00
But before this will work,

40
00:02:34,00 --> 00:02:35,00
The first thing we want to do

41
00:02:35,00 --> 00:02:37,20
is tell our server where to serve static files,

42
00:02:37,20 --> 00:02:39,20
such as images, from.

43
00:02:39,20 --> 00:02:40,90
And we do this by adding a line that says

44
00:02:40,90 --> 00:03:01,10
app.use(express).static(path.join)__dirname, /build).

45
00:03:01,10 --> 00:03:05,40
Import path from path.

46
00:03:05,40 --> 00:03:08,50
And notice that we don't need to do mpm install path,

47
00:03:08,50 --> 00:03:12,00
since path is included standard with no js.

48
00:03:12,00 --> 00:03:14,60
Now, the other line that we need to add to our server

49
00:03:14,60 --> 00:03:19,60
needs to go near the end of our file

50
00:03:19,60 --> 00:03:22,10
And this line is going to look like this

51
00:03:22,10 --> 00:03:28,40
app.get.('*',.

52
00:03:28,40 --> 00:03:32,80
And then for our callback for this route, we're going to say

53
00:03:32,80 --> 00:03:48,60
res.sendFile(path.join(__dirname +/build/index.html)

54
00:03:48,60 --> 00:03:51,40
And what this line tells our app is that all of requests

55
00:03:51,40 --> 00:03:54,20
that aren't caught by any of our other API routes

56
00:03:54,20 --> 00:03:58,60
should be passes on to our app.

57
00:03:58,60 --> 00:04:01,30
to navigate between pages and process urls correctly,

58
00:04:01,30 --> 00:04:03,50
which is obviously very important.

59
00:04:03,50 --> 00:04:05,00
And that's all we need to do.

60
00:04:05,00 --> 00:04:07,90
Let's make sure our code is saved,

61
00:04:07,90 --> 00:04:14,80
and then make sure your mongo database

62
00:04:14,80 --> 00:04:18,40
And we're going to go into our My Blog backend folder.

63
00:04:18,40 --> 00:04:20,10
If you're in the My Blog frontendfolder,

64
00:04:20,10 --> 00:04:24,30
this is going to look like this.

65
00:04:24,30 --> 00:04:31,30
And we're going to run our server using mpm start.

66
00:04:31,30 --> 00:04:39,30
and navigate to localhost8000,

67
00:04:39,30 --> 00:04:40,40
If we click around to make sure

68
00:04:40,40 --> 00:04:50,70
everything's working correctly,

69
00:04:50,70 --> 00:04:54,30
instead of two separate servers as it was before.

70
00:04:54,30 --> 00:04:57,00
And now our app is ready to release.

