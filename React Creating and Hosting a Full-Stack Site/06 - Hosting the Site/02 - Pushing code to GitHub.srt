1
00:00:00,50 --> 00:00:01,80
- [Instructor] So now that we have our app set up

2
00:00:01,80 --> 00:00:07,20
for release, we have to get all the code

3
00:00:07,20 --> 00:00:08,60
And the way we're going to do this is

4
00:00:08,60 --> 00:00:20,30
by creating an empty Git repository,

5
00:00:20,30 --> 00:00:23,90
So to start off,

6
00:00:23,90 --> 00:00:25,80
as a Git repo.

7
00:00:25,80 --> 00:00:27,60
And to do this, let's make sure we're

8
00:00:27,60 --> 00:00:30,00
in the my-blog back end folder,

9
00:00:30,00 --> 00:00:35,80
and then run git init, and press enter.

10
00:00:35,80 --> 00:00:37,10
And now we get a message saying

11
00:00:37,10 --> 00:00:41,30
that it initialized an empty Git repository.

12
00:00:41,30 --> 00:00:44,30
And now, if we run git status,

13
00:00:44,30 --> 00:00:46,20
we can see all of our code that has yet

14
00:00:46,20 --> 00:00:49,80
to be committed to our Git repository.

15
00:00:49,80 --> 00:00:51,60
But before we commit anything,

16
00:00:51,60 --> 00:00:53,70
inside our my-blog back end folder,

17
00:00:53,70 --> 00:00:55,60
we want to create a new file,

18
00:00:55,60 --> 00:00:59,70
and we'll call that .gitignore.

19
00:00:59,70 --> 00:01:01,10
And inside this file,

20
00:01:01,10 --> 00:01:07,00
we want to type node_modules and hit save,

21
00:01:07,00 --> 00:01:08,60
and what this does is it prevents Git

22
00:01:08,60 --> 00:01:12,60
from adding our Node modules to the Git repository.

23
00:01:12,60 --> 00:01:15,30
This is essential since the Node modules folder is

24
00:01:15,30 --> 00:01:18,60
usually quite large, and we can just run npm install

25
00:01:18,60 --> 00:01:20,80
on our server once we have all the code,

26
00:01:20,80 --> 00:01:24,00
and it'll install the modules for us.

27
00:01:24,00 --> 00:01:26,50
So now that we have our gitignore set up,

28
00:01:26,50 --> 00:01:32,60
all we have to do is type git add,

29
00:01:32,60 --> 00:01:35,60
in our current directory, and hit enter.

30
00:01:35,60 --> 00:01:38,10
And if we do git status now,

31
00:01:38,10 --> 00:01:41,90
we see that all of our files are about to committed.

32
00:01:41,90 --> 00:01:43,60
In order to actually commit them,

33
00:01:43,60 --> 00:01:50,20
we just have to type git commit -m,

34
00:01:50,20 --> 00:01:55,10
We'll just do "First commit".

35
00:01:55,10 --> 00:01:56,20
Now that we've done that,

36
00:01:56,20 --> 00:01:58,10
we have to go to our GitHub account,

37
00:01:58,10 --> 00:02:00,60
and create a new repository here.

38
00:02:00,60 --> 00:02:13,80
This will be the repository that we push our code up to.

39
00:02:13,80 --> 00:02:18,80
For the repository name, we'll just call it my-blog,

40
00:02:18,80 --> 00:02:21,60
and then we'll leave the rest of the default settings.

41
00:02:21,60 --> 00:02:24,30
Let's click Create repository,

42
00:02:24,30 --> 00:02:28,30
and then GitHub takes us

43
00:02:28,30 --> 00:02:37,50
And what we're going to do is copy these two commands

44
00:02:37,50 --> 00:02:42,40
and run these two commands.

45
00:02:42,40 --> 00:02:45,70
And now if we go back to the code of my-blog,

46
00:02:45,70 --> 00:02:54,30
we see that our files are all there now.

47
00:02:54,30 --> 00:02:57,60
There's instructions on how to do that on GitHub's website.

48
00:02:57,60 --> 00:02:59,90
And that's all there is to it.

49
00:02:59,90 --> 00:03:02,80
Now we just have to create a server on Amazon Web Services,

50
00:03:02,80 --> 00:03:05,00
and clone this code.

51
00:03:05,00 --> 00:03:07,00
We'll see how to do that in a minute.

