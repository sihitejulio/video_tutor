1
00:00:02,30 --> 00:00:05,00
let's actually clone our code from GitHub,

2
00:00:05,00 --> 00:00:08,50
and the way we do that is by going back to our GitHub repo,

3
00:00:08,50 --> 00:00:11,00
clicking on Clone or download,

4
00:00:11,00 --> 00:00:15,10
and I'm using HTTPS here.

5
00:00:15,10 --> 00:00:17,80
you might have to use SSH.

6
00:00:17,80 --> 00:00:19,90
Going to click on Copy,

7
00:00:19,90 --> 00:00:22,70
and then, we're going to go back to our SSH terminal

8
00:00:22,70 --> 00:00:25,60
and type git clone

9
00:00:25,60 --> 00:00:29,60
and then paste the thing we just copied and press Enter.

10
00:00:29,60 --> 00:00:31,80
Now, if we type ls,

11
00:00:31,80 --> 00:00:34,70
we see that our my-blog repo is on our server,

12
00:00:34,70 --> 00:00:36,40
so now, let's do cd my-blog

13
00:00:36,40 --> 00:00:40,20
to go into that directory.

14
00:00:40,20 --> 00:00:42,70
Now, remember that we specifically left our Node modules

15
00:00:42,70 --> 00:00:45,00
out of our GitHub repo,

16
00:00:45,00 --> 00:00:49,80
so in order to run our code,

17
00:00:49,80 --> 00:00:56,40
and that should reinstall all of our Node modules for us,

18
00:00:56,40 --> 00:00:57,90
and now that we've got everything installed,

19
00:00:57,90 --> 00:01:02,90
we can actually run our server,

20
00:01:02,90 --> 00:01:04,70
but what we're going to do instead

21
00:01:04,70 --> 00:01:07,10
is use an npm package called forever

22
00:01:07,10 --> 00:01:12,30
to make sure that our server keeps running continuously,

23
00:01:12,30 --> 00:01:19,60
is by first typing npm install -g,

24
00:01:19,60 --> 00:01:20,90
Hit Enter,

25
00:01:20,90 --> 00:01:29,90
and that'll run for a little while,

26
00:01:29,90 --> 00:01:33,10
and then, in quotation marks, npm start,

27
00:01:33,10 --> 00:01:36,70
and finally, a period, and hit Enter,

28
00:01:36,70 --> 00:01:40,90
and now, our server should be running on port 8000.

29
00:01:40,90 --> 00:01:44,00
you can enter the command forever list.

30
00:01:44,00 --> 00:01:45,30
Hit Enter,

31
00:01:45,30 --> 00:01:51,20
and it shows that our my-blog repository

32
00:01:51,20 --> 00:01:58,60
Now there's just one more command we want to enter.

33
00:01:58,60 --> 00:02:01,10
Since our app is running on port 8000

34
00:02:01,10 --> 00:02:02,50
but we want to be able to access it

35
00:02:02,50 --> 00:02:05,90
at the default HTTP port which is 80,

36
00:02:05,90 --> 00:02:09,30
we're going to want to map port 80 on our AWS instance

37
00:02:09,30 --> 00:02:12,20
to port 8000 on our Node server,

38
00:02:12,20 --> 00:02:15,70
and the way we do this is with a rather lengthy command,

39
00:02:15,70 --> 00:02:17,40
so bear with me.

40
00:02:17,40 --> 00:02:19,10
It's sudo

41
00:02:19,10 --> 00:02:22,80
iptables -t

42
00:02:22,80 --> 00:02:26,70
nat -A,

43
00:02:26,70 --> 00:02:31,60
all in capitals here, PREROUTING -p

44
00:02:31,60 --> 00:02:36,50
tcp --dport

45
00:02:36,50 --> 00:02:42,00
80, this is the port we want to map from, -j

46
00:02:42,00 --> 00:02:50,40
and all in capitals again, REDIRECT,

47
00:02:50,40 --> 00:02:52,60
and then, the port that our Node server is running on,

48
00:02:52,60 --> 00:02:56,70
which is port 8000, and hit Enter,

49
00:02:56,70 --> 00:03:06,10
and we're not going to get any output, but the command worked,

50
00:03:06,10 --> 00:03:07,60
through a browser,

51
00:03:07,60 --> 00:03:11,50
we're going to go into our AWS console here.

52
00:03:11,50 --> 00:03:14,50
We're going to click on the running instance,

53
00:03:14,50 --> 00:03:15,90
and then, in the bottom pane here,

54
00:03:15,90 --> 00:03:21,60
we're going to scroll down to where we see Security groups,

55
00:03:21,60 --> 00:03:24,10
that this instance belongs to.

56
00:03:24,10 --> 00:03:28,90
In my case, it's launch-wizard-2.

57
00:03:28,90 --> 00:03:31,10
Remember that group for now

58
00:03:31,10 --> 00:03:37,00
because what we need to do is go into this side pane here

59
00:03:37,00 --> 00:03:40,80
under Network & Security and click on it,

60
00:03:40,80 --> 00:03:42,30
and then, depending on the security group

61
00:03:42,30 --> 00:03:44,10
that your app belonged to,

62
00:03:44,10 --> 00:03:46,40
you want to click on that security group.

63
00:03:46,40 --> 00:03:48,10
Mine in this case is launch-wizard-2.

64
00:03:48,10 --> 00:03:51,10
Yours might be launch-wizard-1 or something else,

65
00:03:51,10 --> 00:03:53,20
and click on it,

66
00:03:53,20 --> 00:03:54,40
and in this bottom pane here,

67
00:03:54,40 --> 00:03:57,70
we want to click on the Inbound tab,

68
00:03:57,70 --> 00:03:59,10
and once we're in the Inbound tab,

69
00:03:59,10 --> 00:04:00,60
we want to click Edit,

70
00:04:00,60 --> 00:04:03,10
and then, we want to go to Add Rule,

71
00:04:03,10 --> 00:04:04,50
and from this dropdown,

72
00:04:04,50 --> 00:04:09,10
we're going to scroll down and select HTTP,

73
00:04:09,10 --> 00:04:11,20
and from the Source, we're going to say Anywhere.

74
00:04:11,20 --> 00:04:12,70
Now, you might want to change this later on

75
00:04:12,70 --> 00:04:14,80
while you're still working on your blog,

76
00:04:14,80 --> 00:04:17,00
and you can always select My IP,

77
00:04:17,00 --> 00:04:19,10
but for now, I'm going to select Anywhere,

78
00:04:19,10 --> 00:04:22,20
and now that we've done that, we can click Save,

79
00:04:22,20 --> 00:04:23,90
and now, our app should be accessible

80
00:04:23,90 --> 00:04:26,10
from the outside world.

81
00:04:26,10 --> 00:04:27,90
So scroll back up in the side pane here

82
00:04:27,90 --> 00:04:30,70
and click on Instances

83
00:04:30,70 --> 00:04:33,10
and then, click on your instance here

84
00:04:33,10 --> 00:04:36,30
and go over to Public DNS and copy that,

85
00:04:36,30 --> 00:04:38,50
and then, open up a new browser window

86
00:04:38,50 --> 00:04:42,10
and paste that in and hit Enter,

87
00:04:42,10 --> 00:04:44,10
and our blog comes up.

88
00:04:44,10 --> 00:04:48,00
We can even go to our articles and click on one of them,

89
00:04:48,00 --> 00:04:50,70
and we can add upvotes to the article,

90
00:04:50,70 --> 00:04:57,10
and we can also go down and add a comment,

91
00:04:57,10 --> 00:04:59,20
and of course, if we refresh it,

92
00:04:59,20 --> 00:05:00,40
everything will be persisted

93
00:05:00,40 --> 00:05:03,10
just as it was on our local machine,

94
00:05:03,10 --> 00:05:05,30
and that's all there is to it for now.

95
00:05:05,30 --> 00:05:07,30
Congratulations on building and deploying

96
00:05:07,30 --> 00:05:09,00
your first full stack app.

