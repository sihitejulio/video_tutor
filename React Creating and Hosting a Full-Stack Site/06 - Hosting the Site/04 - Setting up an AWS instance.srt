1
00:00:00,50 --> 00:00:03,00
- [Instructor] Now that we've successfully SSHed

2
00:00:03,00 --> 00:00:04,70
there's a few things we need to do to make sure

3
00:00:04,70 --> 00:00:09,20
that our app will run.

4
00:00:09,20 --> 00:00:14,70
and if you followed my setup instructions,

5
00:00:14,70 --> 00:00:20,80
the package manager on this instance, install, git,

6
00:00:20,80 --> 00:00:25,70
and hit enter, and enter Y for yes,

7
00:00:25,70 --> 00:00:27,70
and now we have git and can successfully run

8
00:00:27,70 --> 00:00:30,40
the git command.

9
00:00:30,40 --> 00:00:33,90
We'll use this in a bit to clone our repository.

10
00:00:33,90 --> 00:00:35,40
The next thing we're going to do is install

11
00:00:35,40 --> 00:00:37,60
npm, and the way we're going to do that here,

12
00:00:37,60 --> 00:00:40,60
since this isn't Mac, is by installing NVM,

13
00:00:40,60 --> 00:00:43,40
the Node Version Manager, and using that to install

14
00:00:43,40 --> 00:00:45,40
the correct version of npm.

15
00:00:45,40 --> 00:00:50,20
So, in order to do that, Amazon has a tutorial

16
00:00:50,20 --> 00:00:52,70
that you can copy and paste.

17
00:00:52,70 --> 00:00:55,60
So, let's copy this command

18
00:00:55,60 --> 00:01:00,00
and paste that into the terminal,

19
00:01:00,00 --> 00:01:03,20
and then we need to run this other command that they give us

20
00:01:03,20 --> 00:01:05,90
which will activate the nvm command

21
00:01:05,90 --> 00:01:08,60
and paste that in here.

22
00:01:08,60 --> 00:01:10,50
And once we've done that we can install

23
00:01:10,50 --> 00:01:12,60
whatever version of npm we want by typing

24
00:01:12,60 --> 00:01:15,70
nvn, install, and then whatever version

25
00:01:15,70 --> 00:01:18,90
of node you have running on your computer.

26
00:01:18,90 --> 00:01:20,70
You can see this by going to a separate terminal

27
00:01:20,70 --> 00:01:24,40
on your computer and typing node -v.

28
00:01:24,40 --> 00:01:28,00
So the version I'm running is 10.15.3

29
00:01:28,00 --> 00:01:30,00
so that's what I'm going to use here.

30
00:01:30,00 --> 00:01:36,00
10.15.3 and hit enter.

31
00:01:36,00 --> 00:01:39,00
And that will run for a little while.

32
00:01:39,00 --> 00:01:40,70
And once that's done, let's install

33
00:01:40,70 --> 00:01:42,30
the latest version of npm by typing

34
00:01:42,30 --> 00:01:50,60
npm, install, -g, npm@latest

35
00:01:50,60 --> 00:01:51,80
And that will run for a little while

36
00:01:51,80 --> 00:01:58,80
while it installs the latest version of npm.

37
00:01:58,80 --> 00:02:03,30
in our instance, we need to install MongoDB as well.

38
00:02:03,30 --> 00:02:07,90
MongoDB has docs on how to do this,

39
00:02:07,90 --> 00:02:10,20
and if we scroll down we'll get to MongoDB's

40
00:02:10,20 --> 00:02:13,60
recommended way of installing MongoDB on Amazon.

41
00:02:13,60 --> 00:02:14,80
And the instance we're using is

42
00:02:14,80 --> 00:02:18,10
Amazon Linux 2 so click on that tab.

43
00:02:18,10 --> 00:02:19,50
And now what we have to do

44
00:02:19,50 --> 00:02:22,50
is create this file on our server.

45
00:02:22,50 --> 00:02:24,90
So what we're going to do is copy this

46
00:02:24,90 --> 00:02:27,40
and go back to our SSH terminal

47
00:02:27,40 --> 00:02:31,60
and then we're going to type sudo, nano,

48
00:02:31,60 --> 00:02:41,40
and paste our file path, enter,

49
00:02:41,40 --> 00:02:50,80
and then press CTRL + O to save it,

50
00:02:50,80 --> 00:02:55,40
And the next thing we need to do is copy this command

51
00:02:55,40 --> 00:03:00,80
into the SSH terminal as well.

52
00:03:00,80 --> 00:03:04,20
while it installs MongoDB on our server.

53
00:03:04,20 --> 00:03:13,40
And once that finishes we want to make sure

54
00:03:13,40 --> 00:03:20,80
mongod, start, and hit enter.

55
00:03:20,80 --> 00:03:22,50
Okay, now Mongo should be successfully

56
00:03:22,50 --> 00:03:25,40
running on our instance.

57
00:03:25,40 --> 00:03:27,20
And now we want to open a Mongo shell

58
00:03:27,20 --> 00:03:29,70
by typing mongo and hitting enter.

59
00:03:29,70 --> 00:03:33,10
And what we want to do, since this is a new Mongo database

60
00:03:33,10 --> 00:03:35,10
is insert some documents for our articles

61
00:03:35,10 --> 00:03:37,50
so that our pages have something to load.

62
00:03:37,50 --> 00:03:43,30
We can do that by typing use my-blog, hitting enter

63
00:03:43,30 --> 00:03:47,30
and then doing db.articles, which is what we called

64
00:03:47,30 --> 00:03:51,60
the collection on our local database, .insert,

65
00:03:51,60 --> 00:03:54,80
and then we're going to insert an array of articles.

66
00:03:54,80 --> 00:03:57,10
If you want to copy and paste this from the exercise files

67
00:03:57,10 --> 00:04:00,60
you're more than welcome to.

68
00:04:00,60 --> 00:04:02,20
And the output that MongoDB gives us

69
00:04:02,20 --> 00:04:05,40
says it successfully inserted three records.

70
00:04:05,40 --> 00:04:09,20
We can now do CTRL + C to exit the shell.

71
00:04:09,20 --> 00:04:12,00
And that should be all the setup we need for our app to run.

