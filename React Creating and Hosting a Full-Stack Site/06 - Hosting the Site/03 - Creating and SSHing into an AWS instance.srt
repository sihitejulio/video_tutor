1
00:00:03,30 --> 00:00:04,80
what we're going to do is create a server

2
00:00:04,80 --> 00:00:07,20
on Amazon Web Services.

3
00:00:07,20 --> 00:00:10,90
So first login to the AWS management console,

4
00:00:10,90 --> 00:00:12,60
and then under all services,

5
00:00:12,60 --> 00:00:15,90
we're going to click on EC2

6
00:00:15,90 --> 00:00:17,60
and then on the screen that comes up,

7
00:00:17,60 --> 00:00:23,10
we're going to click on launch instance.

8
00:00:23,10 --> 00:00:25,50
Amazon Linux 2 AMI.

9
00:00:25,50 --> 00:00:27,20
Click on select.

10
00:00:27,20 --> 00:00:36,20
And then just leave it with the defaults it gives us.

11
00:00:36,20 --> 00:00:38,70
And then we'll click launch.

12
00:00:38,70 --> 00:00:41,00
AWS is then going to bring up a window

13
00:00:41,00 --> 00:00:43,40
talking about key pairs.

14
00:00:43,40 --> 00:00:45,40
And what we're going to need these key pairs for

15
00:00:45,40 --> 00:00:48,70
is when we need to SSH into our server

16
00:00:48,70 --> 00:00:50,90
and make changes there.

17
00:00:50,90 --> 00:00:53,40
Now if you've never done that before,

18
00:00:53,40 --> 00:00:54,90
I'll show you how.

19
00:00:54,90 --> 00:00:59,00
So in this window, let's select create a new key pair.

20
00:00:59,00 --> 00:01:04,40
And we'll call this key pair my-blog-key.

21
00:01:04,40 --> 00:01:07,90
And then click download key pair.

22
00:01:07,90 --> 00:01:11,90
This will download this .pem file to our computer.

23
00:01:11,90 --> 00:01:12,70
Once we've done that,

24
00:01:12,70 --> 00:01:15,60
let's click on launch instances.

25
00:01:15,60 --> 00:01:16,70
Now it might take a little while

26
00:01:16,70 --> 00:01:18,50
for the instance to actually start up

27
00:01:18,50 --> 00:01:24,90
but while that's happening,

28
00:01:24,90 --> 00:01:41,70
and make sure we're at our home directory

29
00:01:41,70 --> 00:01:45,50
To see if you do, type ls -al

30
00:01:45,50 --> 00:01:52,20
and see if it shows up in this section here.

31
00:01:52,20 --> 00:01:57,80
all you have to do is type mkdir.ssh.

32
00:01:57,80 --> 00:02:00,90
Now once we've done that, we want to copy our .pem file

33
00:02:00,90 --> 00:02:03,40
into this SSH folder.

34
00:02:03,40 --> 00:02:06,20
And you can either do this by dragging and dropping files,

35
00:02:06,20 --> 00:02:08,60
or you can do it from the command line.

36
00:02:08,60 --> 00:02:28,80
I'mma do it from the command line here,

37
00:02:28,80 --> 00:02:40,20
which will be ~/.ssh

38
00:02:40,20 --> 00:02:52,00
In order to make sure that worked,

39
00:02:52,00 --> 00:02:55,20
So now that we've done that,

40
00:02:55,20 --> 00:03:03,90
Let's go back to our AWS console,

41
00:03:03,90 --> 00:03:20,10
we should see that we have one running instance.

42
00:03:20,10 --> 00:03:25,60
And to do that,

43
00:03:25,60 --> 00:03:29,90
the public DNS property here.

44
00:03:29,90 --> 00:03:45,50
And now we're going to go back to our terminal.

45
00:03:45,50 --> 00:04:01,10
~/.ssh

46
00:04:01,10 --> 00:04:13,70
And then the path to our key,

47
00:04:13,70 --> 00:04:29,00
and then paste the public DNS you just copied.

