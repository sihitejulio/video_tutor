1
00:00:00,50 --> 00:00:02,70
- [Shaun] So we've covered quite a bit in this course.

2
00:00:02,70 --> 00:00:05,50
We've created and hosted an entire full stack website

3
00:00:05,50 --> 00:00:08,70
from scratch, complete with database support,

4
00:00:08,70 --> 00:00:10,80
and that's a really big achievement.

5
00:00:10,80 --> 00:00:13,00
But before you start sharing links to your blog,

6
00:00:13,00 --> 00:00:16,40
there's a few next steps that I'd recommend you look into.

7
00:00:16,40 --> 00:00:17,70
The first thing I'd recommend is adding

8
00:00:17,70 --> 00:00:20,70
login/logout functionality to your website.

9
00:00:20,70 --> 00:00:21,90
This will help ensure that users

10
00:00:21,90 --> 00:00:23,70
only upvote your articles once,

11
00:00:23,70 --> 00:00:24,60
and it'll also help you

12
00:00:24,60 --> 00:00:27,60
to better control your comment section.

13
00:00:27,60 --> 00:00:28,70
It would also be a good idea

14
00:00:28,70 --> 00:00:30,60
to improve error handling in your app,

15
00:00:30,60 --> 00:00:34,90
to handle unexpected situations such as server shutdowns.

16
00:00:34,90 --> 00:00:36,00
I would also highly recommend

17
00:00:36,00 --> 00:00:38,80
that you look into securing your Mongo database.

18
00:00:38,80 --> 00:00:44,20
Mongo has instructions on how to do this on their website.

19
00:00:44,20 --> 00:00:45,50
At some point, you're also going to

20
00:00:45,50 --> 00:00:47,60
want to buy a domain name for your website,

21
00:00:47,60 --> 00:00:49,80
and this isn't hard either.

22
00:00:49,80 --> 00:00:52,70
And, last but not least, now that we've built out our blog,

23
00:00:52,70 --> 00:00:55,70
it needs some content, and I'll leave that to you.

24
00:00:55,70 --> 00:00:57,00
There's also videos on how to do

25
00:00:57,00 --> 00:00:59,40
most of these things in our library.

26
00:00:59,40 --> 00:01:00,40
And if you're interested in

27
00:01:00,40 --> 00:01:03,20
improving your JavaScript code with functional programming,

28
00:01:03,20 --> 00:01:05,00
I'd highly recommend you check out my course

29
00:01:05,00 --> 00:01:08,50
about functional programming with JavaScript ES6.

30
00:01:08,50 --> 00:01:10,30
Please feel free to connect with me on LinkedIn,

31
00:01:10,30 --> 00:01:13,10
where I post about development topics.

32
00:01:13,10 --> 00:01:15,80
Well, I hope this course has been really helpful for you.

33
00:01:15,80 --> 00:01:17,00
Thanks for watching.

