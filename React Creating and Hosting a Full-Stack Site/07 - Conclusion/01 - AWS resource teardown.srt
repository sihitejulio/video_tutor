1
00:00:00,50 --> 00:00:01,80
- [Instructor] So now you've got your site up

2
00:00:01,80 --> 00:00:04,50
and running on Amazon Web Services.

3
00:00:04,50 --> 00:00:06,70
Keep in mind that if you created a free tier account

4
00:00:06,70 --> 00:00:08,70
specifically for this course,

5
00:00:08,70 --> 00:00:12,20
that Amazon's free tier currently only lasts for 12 months.

6
00:00:12,20 --> 00:00:14,70
So if you're not planning

7
00:00:14,70 --> 00:00:19,70
it'd be a good idea to turn it off now

8
00:00:19,70 --> 00:00:22,30
And the way that you can do that is really simple.

9
00:00:22,30 --> 00:00:26,20
You simply click on this instance that's running,

10
00:00:26,20 --> 00:00:30,10
and go to Actions, and then under Instance State,

11
00:00:30,10 --> 00:00:33,50
you want to click Terminate.

12
00:00:33,50 --> 00:00:35,10
Once you've clicked on Terminate,

13
00:00:35,10 --> 00:00:36,50
it'll bring up a window asking

14
00:00:36,50 --> 00:00:38,60
if you really want to terminate it,

15
00:00:38,60 --> 00:00:40,70
I'm going to click Yes here,

16
00:00:40,70 --> 00:00:43,60
and that'll shut down our instance completely.

17
00:00:43,60 --> 00:00:44,80
So now you won't have to worry

18
00:00:44,80 --> 00:00:48,00
about any potential charges down the road,

19
00:00:48,00 --> 00:00:48,80
and here you can see

20
00:00:48,80 --> 00:00:51,00
that your instance has successfully terminated.

