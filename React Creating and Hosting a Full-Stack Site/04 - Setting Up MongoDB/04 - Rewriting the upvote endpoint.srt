1
00:00:00,50 --> 00:00:03,80
- [Instructor] So now that our back end

2
00:00:03,80 --> 00:00:05,50
to keep track of article info,

3
00:00:05,50 --> 00:00:06,90
you may have noticed that our endpoints

4
00:00:06,90 --> 00:00:08,50
for up-voting and adding comments

5
00:00:08,50 --> 00:00:11,00
to an article will no longer work.

6
00:00:11,00 --> 00:00:14,90
In order to make them work again

7
00:00:14,90 --> 00:00:18,30
as we did with our article info endpoint here.

8
00:00:18,30 --> 00:00:21,00
In this video, we'll be refactoring our up-vote endpoint

9
00:00:21,00 --> 00:00:24,00
and later on we'll refactor our comments endpoint.

10
00:00:24,00 --> 00:00:25,50
So basically, the steps we took

11
00:00:25,50 --> 00:00:29,80
while writing our article info endpoint

12
00:00:29,80 --> 00:00:32,90
with the up-voting endpoint, with one small difference.

13
00:00:32,90 --> 00:00:35,80
Instead of just returning the results of a find one query,

14
00:00:35,80 --> 00:00:37,20
we'll have to do a find one query

15
00:00:37,20 --> 00:00:38,80
to find the matching article,

16
00:00:38,80 --> 00:00:40,90
and then do an update query to increase

17
00:00:40,90 --> 00:00:43,90
the number of up-votes that the article has in the database,

18
00:00:43,90 --> 00:00:46,50
and finally, we'll send the updated article info

19
00:00:46,50 --> 00:00:48,50
as a response to the client so that the client

20
00:00:48,50 --> 00:00:52,30
can have the most up to date information about the article.

21
00:00:52,30 --> 00:00:53,30
The first thing we're going to do

22
00:00:53,30 --> 00:00:55,80
is remove these last two lines from our callback

23
00:00:55,80 --> 00:00:59,10
since they'll no longer work anyway.

24
00:00:59,10 --> 00:01:00,30
We can leave the first lines

25
00:01:00,30 --> 00:01:02,10
since that's where we extract the article name

26
00:01:02,10 --> 00:01:04,40
from the URL parameters.

27
00:01:04,40 --> 00:01:06,20
And next we have to connect to MongoDB

28
00:01:06,20 --> 00:01:10,80
and get the My Blog database

29
00:01:10,80 --> 00:01:19,40
In fact, we can just copy and paste it.

30
00:01:19,40 --> 00:01:21,60
As I mentioned, the first thing we're going to want to do

31
00:01:21,60 --> 00:01:27,20
is do a database query to find the article in the database

32
00:01:27,20 --> 00:01:35,80
And that'll look like this.

33
00:01:35,80 --> 00:01:41,10
('articles').findOne, and we want to find

34
00:01:41,10 --> 00:01:46,90
the article whose name property

35
00:01:46,90 --> 00:01:49,50
ArticleName.

36
00:01:49,50 --> 00:01:55,50
And now that we've got this article info,

37
00:01:55,50 --> 00:02:02,70
the number of upvotes for our article in the database.

38
00:02:02,70 --> 00:02:06,10
('articles'), and the command we're going to use here

39
00:02:06,10 --> 00:02:09,60
is updateOne, to find the article we use findOne,

40
00:02:09,60 --> 00:02:12,30
to update it we're going to use updateOne.

41
00:02:12,30 --> 00:02:14,00
And again we want to update the article

42
00:02:14,00 --> 00:02:18,30
whose name matches the article name URL parameter.

43
00:02:18,30 --> 00:02:20,80
And the second argument that we pass to updateOne

44
00:02:20,80 --> 00:02:22,40
is going to be the actual updates

45
00:02:22,40 --> 00:02:24,80
that we want to apply to the object.

46
00:02:24,80 --> 00:02:28,50
And MongoDB has a special syntax for this.

47
00:02:28,50 --> 00:02:30,40
Inside this object we have to have a property

48
00:02:30,40 --> 00:02:35,50
in between single quotes, that's dollar sign set,

49
00:02:35,50 --> 00:02:37,40
and the value of this is going to be an object

50
00:02:37,40 --> 00:02:39,80
with the actual changes we want to make.

51
00:02:39,80 --> 00:02:42,30
In this case we want to say upvotes

52
00:02:42,30 --> 00:02:50,20
and set it equal to articleInfo.upvotes + 1.

53
00:02:50,20 --> 00:02:52,30
And finally we want to get the updated version

54
00:02:52,30 --> 00:02:54,30
of the article we just modified.

55
00:02:54,30 --> 00:02:55,80
So we'll create a new constant

56
00:02:55,80 --> 00:03:03,10
called updatedArticleInfo

57
00:03:03,10 --> 00:03:08,60
We'll just copy and paste it.

58
00:03:08,60 --> 00:03:11,00
We can then send this updated article info

59
00:03:11,00 --> 00:03:19,10
back to the client using res.status(200).json

60
00:03:19,10 --> 00:03:23,80
with our updated article info.

61
00:03:23,80 --> 00:03:26,00
And last but not least, let's not forget to call

62
00:03:26,00 --> 00:03:29,70
client.close and again, since we've used

63
00:03:29,70 --> 00:03:32,10
the await keyword inside this callback

64
00:03:32,10 --> 00:03:36,50
we've got to remember to put the async keyword at the top.

65
00:03:36,50 --> 00:03:39,60
We'll probably also want to wrap all this in a tri catch block

66
00:03:39,60 --> 00:03:44,00
the same as we did for our get article info endpoint.

67
00:03:44,00 --> 00:03:45,40
And we'll just copy the catch block

68
00:03:45,40 --> 00:03:53,10
from our other endpoint.

69
00:03:53,10 --> 00:03:57,20
Let's open up Postman

70
00:03:57,20 --> 00:03:59,00
to the endpoint we just defined.

71
00:03:59,00 --> 00:04:09,40
And that'll be http//localhost8000/api/articles

72
00:04:09,40 --> 00:04:15,40
and we'll do learn-node/upvote.

73
00:04:15,40 --> 00:04:17,10
And now if we send this request

74
00:04:17,10 --> 00:04:19,60
we see that it's sent us back the updated version

75
00:04:19,60 --> 00:04:21,40
of our article in the database,

76
00:04:21,40 --> 00:04:23,70
and that our article has been up-voted.

77
00:04:23,70 --> 00:04:28,20
If we do this several times we can see

78
00:04:28,20 --> 00:04:35,40
and better yet, if we stop and restart our server,

79
00:04:35,40 --> 00:04:38,00
and then send a request again,

80
00:04:38,00 --> 00:04:40,00
we can see that unlike when we were storing

81
00:04:40,00 --> 00:04:45,00
data in a Javascript object on our server

