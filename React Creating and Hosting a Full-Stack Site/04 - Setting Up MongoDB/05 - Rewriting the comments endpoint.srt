1
00:00:26,20 --> 00:00:58,00
we want to use and then we have to call client.close

2
00:00:58,00 --> 00:00:59,90
and this function's going to take care of all

3
00:00:59,90 --> 00:01:34,40
of the setup and tear down for us,

4
00:01:34,40 --> 00:01:57,00
We'll call this function operations,

5
00:01:57,00 --> 00:02:08,50
Let's just copy and paste it from our get articles endpoint.

6
00:02:08,50 --> 00:02:11,80
We're getting our database, and then we're performing

7
00:02:11,80 --> 00:02:36,80
our operations and finally we're calling client.close

8
00:02:36,80 --> 00:02:42,90
Now that we're written this withDB function to take care

9
00:02:42,90 --> 00:02:45,10
let's refactor our two existing endpoints

10
00:02:45,10 --> 00:02:47,90
and then we'll move on to the comments section.

11
00:02:47,90 --> 00:02:50,90
So we'll start off with our get article info endpoint,

12
00:02:50,90 --> 00:02:54,90
and we're going to remove everything related to the database.

13
00:02:54,90 --> 00:03:30,80
So we'll remove the catch block and the client.close,

14
00:03:30,80 --> 00:03:34,00
we have to put async here as well,

15
00:03:34,00 --> 00:04:19,70
and that in turn means that we have to put the await keyword

16
00:04:19,70 --> 00:05:13,80
So it'll look like async db,

17
00:05:13,80 --> 00:05:26,30
Now that we're getting the hang of rewriting our code

18
00:05:26,30 --> 00:06:27,90
because they no longer work.

19
00:06:27,90 --> 00:07:19,20
to the existing comments which we get

20
00:07:19,20 --> 00:07:36,30
And finally we want to send this updated articleInfo

21
00:07:36,30 --> 00:07:55,80
Now let's go back and test it in Postman

22
00:07:55,80 --> 00:08:15,70
and we got to write our request body here,

23
00:08:15,70 --> 00:08:27,90
added a comment to our article in the database,

24
00:08:27,90 --> 00:08:33,00
We can just wrap whatever operations we want to make

