1
00:01:11,20 --> 00:02:04,60
We also have to make sure the permissions

2
00:02:04,60 --> 00:02:17,50
by simply clicking on this little plus icon here,

3
00:02:17,50 --> 00:02:19,70
we're going to run the command mongo,

4
00:02:19,70 --> 00:02:21,70
and you should see some output,

5
00:02:21,70 --> 00:02:30,00
and then it'll stop waiting for your command.

6
00:02:30,00 --> 00:02:34,10
Now there are quite a few commands that you can use

7
00:02:34,10 --> 00:02:38,30
and I definitely don't expect you to know them already.

8
00:02:38,30 --> 00:02:45,90
for this course, and if you're interested in learning more

9
00:02:45,90 --> 00:04:45,50
So the first thing we're going to do here,

10
00:04:45,50 --> 00:05:17,60
and we see all the documents

11
00:05:17,60 --> 00:05:21,00
named learn-react, we can simply type name

12
00:05:21,00 --> 00:05:44,50
and then learn-react,

13
00:05:44,50 --> 00:05:49,00
when you're done with the mongo shell,

