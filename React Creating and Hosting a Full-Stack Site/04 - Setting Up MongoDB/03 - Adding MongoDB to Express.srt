1
00:00:01,90 --> 00:00:07,50
and running on our computer, as well as some basic data

2
00:00:07,50 --> 00:00:38,90
into our Express server.

3
00:00:38,90 --> 00:00:55,10
In other words our data won't be wiped away every time

4
00:00:55,10 --> 00:01:01,50
So now that we've deleted the sort of fake database

5
00:01:01,50 --> 00:01:03,90
Instead of just modifying fields in an object

6
00:01:03,90 --> 00:01:37,40
like we've been doing in our endpoints.

7
00:01:37,40 --> 00:01:41,80
So we'll use app.get and it's also going to make use

8
00:01:41,80 --> 00:01:55,80
of URL parameters so the path we're going to define here

9
00:01:55,80 --> 00:01:59,70
of an article to get the associated information.

10
00:01:59,70 --> 00:02:01,90
For example we'd be able to send a get request

11
00:02:01,90 --> 00:02:05,40
to /api/articles/learn node

12
00:02:05,40 --> 00:02:08,60
and get info about our learn node article.

13
00:02:08,60 --> 00:02:10,70
Now let's define our callback.

14
00:02:10,70 --> 00:02:13,90
Before we connect and query our local MongoDB instance,

15
00:02:13,90 --> 00:02:20,40
we'll want to get the article name from the URL parameters,

16
00:02:20,40 --> 00:02:26,70
in our other endpoints so we'll just copy and paste this,

17
00:02:26,70 --> 00:02:32,70
The first thing we want to do is import the MongoDB tools

18
00:02:32,70 --> 00:02:42,30
a tool called MongoClient from MongoDB.

19
00:02:42,30 --> 00:02:43,80
and this MongoClient allows us to connect

20
00:02:43,80 --> 00:03:20,70
to our local database.

21
00:03:20,70 --> 00:03:26,90
is an options object which we can use to change

22
00:03:26,90 --> 00:03:29,60
Currently MongoDB will complain if we don't pass

23
00:03:29,60 --> 00:03:38,10
an option called useNewURLParser and set it to true.

24
00:03:38,10 --> 00:03:40,20
That's just something we have to do for now.

25
00:03:40,20 --> 00:03:42,50
Now this connect function is asynchronous,

26
00:03:42,50 --> 00:03:47,80
and it returns a promise which means that we can use

27
00:03:47,80 --> 00:03:51,90
dealing with asynchronous tasks much easier

28
00:03:51,90 --> 00:03:54,40
Now don't worry if you're not familiar with async await yet

29
00:03:54,40 --> 00:04:16,90
it's basically just an easier way of dealing with promises.

30
00:04:16,90 --> 00:04:31,80
Now in order to query the local my blog database

31
00:04:31,80 --> 00:04:38,30
and then our databases name, my-blog,

32
00:04:38,30 --> 00:04:40,70
that's going to look like this.

33
00:04:40,70 --> 00:04:45,60
We can say cont articleInfo and then await because

34
00:04:45,60 --> 00:04:57,50
reading from the database is asynchronous,

35
00:04:57,50 --> 00:05:23,00
and then we want to find the article in our database

36
00:05:23,00 --> 00:05:53,40
.send with our articleInfo.

37
00:05:53,40 --> 00:05:56,30
and we also want to wrap all our code in a try catch block

38
00:05:56,30 --> 00:06:04,70
in case something goes wrong with the database operations.

39
00:06:04,70 --> 00:06:25,00
And inside our catch block let's send a response

40
00:06:25,00 --> 00:06:28,30
which just contains the error that occurred.

41
00:06:28,30 --> 00:06:37,20
Okay so now that we have all our pieces in place,

42
00:06:37,20 --> 00:07:34,00
running on our machine.

