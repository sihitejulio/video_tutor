1
00:00:00,50 --> 00:00:01,60
- [Instructor] Since both our client

2
00:00:01,60 --> 00:00:03,70
and server are now written in JavaScript,

3
00:00:03,70 --> 00:00:06,20
it makes sense to persist our data in a database

4
00:00:06,20 --> 00:00:12,40
that plays very nicely with JavaScript.

5
00:00:12,40 --> 00:00:15,40
MongoDB is what we call a non-relational database,

6
00:00:15,40 --> 00:00:18,00
which means that we can basically just push data into it

7
00:00:18,00 --> 00:00:21,30
without worrying too much about how that data is formatted.

8
00:00:21,30 --> 00:00:24,50
In the case of MongoDB, we can simply hand our database

9
00:00:24,50 --> 00:00:27,10
any kind of JSON objects that we want it to store,

10
00:00:27,10 --> 00:00:30,40
and we can retrieve them later whenever we need them.

11
00:00:30,40 --> 00:00:33,60
This is in contrast to traditional relational databases

12
00:00:33,60 --> 00:00:36,00
where the structure of the data and the fields it contains

13
00:00:36,00 --> 00:00:38,30
usually have to be defined in advance

14
00:00:38,30 --> 00:00:41,70
and adhered to whenever we add new data.

15
00:00:41,70 --> 00:00:54,80
Working with relational databases

16
00:00:54,80 --> 00:00:56,70
Now I know plenty of people who still swear

17
00:00:56,70 --> 00:00:59,50
by using relational databases and SQL,

18
00:00:59,50 --> 00:01:01,80
and I'm not here to tell you that either MongoDB

19
00:01:01,80 --> 00:01:04,90
or SQL databases are better in all cases.

20
00:01:04,90 --> 00:01:07,50
But for our purposes, for the purpose of quickly building

21
00:01:07,50 --> 00:01:10,50
and releasing a full-stack JavaScript application,

22
00:01:10,50 --> 00:01:13,10
MongoDB will definitely make our lives easier,

23
00:01:13,10 --> 00:01:16,00
and so that's what we're going to use for our blog site.

