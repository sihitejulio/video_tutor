1
00:00:00,50 --> 00:00:02,50
- Now, the response that we get back from our server,

2
00:00:02,50 --> 00:00:04,50
which we've called Resolve is going to contain

3
00:00:04,50 --> 00:00:06,70
the updated version of our article info,

4
00:00:06,70 --> 00:00:10,30
which we can use to update our UI once we get the response.

5
00:00:10,30 --> 00:00:13,20
This is the same thing that we did with our Upvote button.

6
00:00:13,20 --> 00:00:15,70
So first we need to get the body of the response,

7
00:00:15,70 --> 00:00:18,20
which will do by saying const body

8
00:00:18,20 --> 00:00:22,90
equals await,

9
00:00:22,90 --> 00:00:25,10
and then we're going to want to call set article info

10
00:00:25,10 --> 00:00:27,30
with the updated info in the body.

11
00:00:27,30 --> 00:00:31,30
We'll say setArticleInfo, body.

12
00:00:31,30 --> 00:00:33,00
Now our AddCommentForm component

13
00:00:33,00 --> 00:00:34,60
doesn't have this function yet,

14
00:00:34,60 --> 00:00:36,50
so at some point we're going to need to pass it down

15
00:00:36,50 --> 00:00:39,60
from the top level article page component.

16
00:00:39,60 --> 00:00:41,30
So first let's define the props setter

17
00:00:41,30 --> 00:00:44,00
AddCommentForm is going to need.

18
00:00:44,00 --> 00:00:46,00
Remember that it will need this article name prop

19
00:00:46,00 --> 00:00:48,00
pass down to it,

20
00:00:48,00 --> 00:00:52,50
so up here we'll say, articleName,

21
00:00:52,50 --> 00:00:55,10
and it will also need the setArticleInfo function pass

22
00:00:55,10 --> 00:00:57,60
down to it.

23
00:00:57,60 --> 00:01:01,80
So we'll say setArticleInfo,

24
00:01:01,80 --> 00:01:03,90
and then we want to actually call this asynchronous

25
00:01:03,90 --> 00:01:10,40
AddComment function from our buttons on click prop

26
00:01:10,40 --> 00:01:15,20
onClick brackets, parenthesis

27
00:01:15,20 --> 00:01:19,00
and we'll say AddComment.

28
00:01:19,00 --> 00:01:22,40
Okay, so now that we have our AddCommentForm all built out,

29
00:01:22,40 --> 00:01:25,70
let's actually add it to our site.

30
00:01:25,70 --> 00:01:29,60
Let's go to article page and we can put the AddComment form

31
00:01:29,60 --> 00:01:32,30
either right above or right underneath the comments list

32
00:01:32,30 --> 00:01:33,40
component.

33
00:01:33,40 --> 00:01:36,00
I'm going to put it underneath.

34
00:01:36,00 --> 00:01:48,60
So first we have to import our AddCommentForm,

35
00:01:48,60 --> 00:01:52,50
slash AddCommentForm,

36
00:01:52,50 --> 00:01:54,90
and then we'll add our AddCommentForm.

37
00:01:54,90 --> 00:01:59,20
AddCommentForm and we'll pass at the correct props,

38
00:01:59,20 --> 00:02:01,50
remember that we need the articleName,

39
00:02:01,50 --> 00:02:04,10
and setArticleInfo.

40
00:02:04,10 --> 00:02:09,00
So we'll say articleName equals

41
00:02:09,00 --> 00:02:14,50
and we want to pass this name constant here,

42
00:02:14,50 --> 00:02:19,10
so ArticleName equals name and then setArticleInfo

43
00:02:19,10 --> 00:02:23,00
equals setArticleInfo.

44
00:02:23,00 --> 00:02:26,60
And close the tag.

45
00:02:26,60 --> 00:02:29,90
So now if we go back to our site and scroll down,

46
00:02:29,90 --> 00:02:32,70
we see that our add a comment form is displayed.

47
00:02:32,70 --> 00:02:36,40
Now, let's test it out.

48
00:02:36,40 --> 00:02:39,90
And it works.

49
00:02:39,90 --> 00:02:42,10
for the sake of good UI.

50
00:02:42,10 --> 00:02:44,00
When we click the add comment button,

51
00:02:44,00 --> 00:02:47,20
we want to reset the values of this text fields.

52
00:02:47,20 --> 00:02:49,30
So in order to do that, all we need to do

53
00:02:49,30 --> 00:02:52,70
is go back to our AddCommentForm,

54
00:02:52,70 --> 00:02:56,50
and then inside this AddComment function,

55
00:02:56,50 --> 00:02:59,70
we want to call setUsername and we'll set it

56
00:02:59,70 --> 00:03:02,20
to an empty string,

57
00:03:02,20 --> 00:03:06,00
and setCommentText, and we'll set that to an

58
00:03:06,00 --> 00:03:15,10
empty string as well.

59
00:03:15,10 --> 00:03:17,60
we can see that it works perfectly.

60
00:03:17,60 --> 00:03:19,00
And that's all there is to it.

61
00:03:19,00 --> 00:03:21,40
Now we have both a working comment form

62
00:03:21,40 --> 00:03:24,20
and the working Upvote button.

63
00:03:24,20 --> 00:03:26,30
And all of these are supported by our server

64
00:03:26,30 --> 00:03:28,00
and our database.

