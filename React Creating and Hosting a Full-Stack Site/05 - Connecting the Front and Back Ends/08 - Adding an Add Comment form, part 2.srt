1
00:00:44,60 --> 00:00:51,40
with event dot target dot value.

2
00:00:51,40 --> 00:01:02,80
And then we want to do the same thing

3
00:01:02,80 --> 00:01:11,30
arrow

4
00:01:11,30 --> 00:01:13,00
Closing bracket.

5
00:01:13,00 --> 00:01:17,70
And now all that remains

6
00:01:17,70 --> 00:01:30,10
by clicking the button

7
00:01:30,10 --> 00:01:31,50
We'll say const

8
00:01:31,50 --> 00:02:17,30
and we'll call our function add comment

9
00:02:17,30 --> 00:02:19,80
Now, first of all, we want this

10
00:02:19,80 --> 00:02:40,10
so we're going to want to add method

11
00:02:40,10 --> 00:02:55,00
But remember that our server's expecting this comment text

12
00:02:55,00 --> 00:03:32,30
which turns this JSON object

13
00:03:32,30 --> 00:03:35,00
and it allows it to parse our request body correctly.

