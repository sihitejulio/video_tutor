1
00:00:07,30 --> 00:00:11,60
to build a server for our backend

2
00:00:11,60 --> 00:00:24,90
and adding comments to articles on our blog.

3
00:00:24,90 --> 00:00:30,80
into a single full Stack application.

4
00:00:30,80 --> 00:00:39,10
as we developed it

5
00:00:39,10 --> 00:00:43,00
our main task is to make it

6
00:00:43,00 --> 00:01:50,40
is the one that makes these requests.

7
00:01:50,40 --> 00:01:52,10
So, if we don't specify this argument,

8
00:01:52,10 --> 00:01:58,90
fetch will send a GET request to the endpoint we specify.

9
00:01:58,90 --> 00:02:00,90
so generally we don't even need to import it

10
00:02:00,90 --> 00:02:07,50
into our code base.

11
00:02:07,50 --> 00:02:12,80
and you want to support Internet Explorer users,

12
00:02:12,80 --> 00:02:33,30
Let's make sure we're in our front end directory.

13
00:02:33,30 --> 00:03:04,00
and it's a polyfill library

