1
00:00:00,50 --> 00:00:03,20
- [Narrator] Before we move on and add the actual fetching,

2
00:00:03,20 --> 00:00:05,70
there are two more caveats we need to address.

3
00:00:05,70 --> 00:00:08,00
The first is pretty subtle, but when it shows up,

4
00:00:08,00 --> 00:00:10,00
it really shows up.

5
00:00:10,00 --> 00:00:12,80
To see it, let's say that for testing purposes,

6
00:00:12,80 --> 00:00:14,70
instead of hard-coding the number of upvotes

7
00:00:14,70 --> 00:00:17,50
for a given article, like we did here,

8
00:00:17,50 --> 00:00:19,60
we generated a random number of upvotes,

9
00:00:19,60 --> 00:00:24,60
say between one and 10, and that would look like this.

10
00:00:24,60 --> 00:00:30,90
Math.ceil(math.random() * 10.

11
00:00:30,90 --> 00:00:36,30
And then if we save and reload our page, yikes!

12
00:00:36,30 --> 00:00:38,80
It looks like our effect keeps getting called continuously,

13
00:00:38,80 --> 00:00:41,70
generating random number after random number.

14
00:00:41,70 --> 00:00:43,90
So why exactly is this happening?

15
00:00:43,90 --> 00:00:46,10
Well the useEffect hook runs not only when

16
00:00:46,10 --> 00:00:47,80
the component first mounts,

17
00:00:47,80 --> 00:00:50,00
but also whenever the component updates.

18
00:00:50,00 --> 00:00:53,40
In other words, if ArticleInfo changes.

19
00:00:53,40 --> 00:00:55,00
And since we're updating the component

20
00:00:55,00 --> 00:00:56,90
from inside the useEffect hook,

21
00:00:56,90 --> 00:00:59,90
continuously supplying a different number of upvotes,

22
00:00:59,90 --> 00:01:03,30
our component gets caught in an infinite loop.

23
00:01:03,30 --> 00:01:06,00
The good news is that there's an easy way to prevent this,

24
00:01:06,00 --> 00:01:07,70
and this is by adding an empty array

25
00:01:07,70 --> 00:01:11,10
as a second argument to useEffect,

26
00:01:11,10 --> 00:01:12,80
like this.

27
00:01:12,80 --> 00:01:15,00
We'll see why this helps in a minute.

28
00:01:15,00 --> 00:01:17,60
But for now we see that if we save our page,

29
00:01:17,60 --> 00:01:19,90
and look at our article page again,

30
00:01:19,90 --> 00:01:21,20
we see that it's no longer stuck

31
00:01:21,20 --> 00:01:23,40
in the infinite loop it was in before.

32
00:01:23,40 --> 00:01:27,90
However, now it doesn't update even when we change pages.

33
00:01:27,90 --> 00:01:30,40
For instance, if we go directly to another article

34
00:01:30,40 --> 00:01:34,10
via the other articles section,

35
00:01:34,10 --> 00:01:37,00
we see that this value stays the same.

36
00:01:37,00 --> 00:01:39,10
So now we went from updating continuously

37
00:01:39,10 --> 00:01:41,70
to not updating at all.

38
00:01:41,70 --> 00:01:43,70
And the reason for this is this little empty array

39
00:01:43,70 --> 00:01:46,50
that we passed to useEffect.

40
00:01:46,50 --> 00:01:48,80
What this array is, is an array of values

41
00:01:48,80 --> 00:01:52,20
that useEffect should watch, and if one of them changes,

42
00:01:52,20 --> 00:01:54,60
useEffect should be called again.

43
00:01:54,60 --> 00:01:56,70
And since we're passing an empty array,

44
00:01:56,70 --> 00:01:58,40
that's pretty much the same as telling useEffect

45
00:01:58,40 --> 00:02:00,30
that we only want it to be called

46
00:02:00,30 --> 00:02:02,90
when the component first loads.

47
00:02:02,90 --> 00:02:04,70
And since when we jump between articles,

48
00:02:04,70 --> 00:02:06,10
like we were doing before,

49
00:02:06,10 --> 00:02:09,40
we're not actually changing the component that's displayed,

50
00:02:09,40 --> 00:02:10,50
useEffect only got called

51
00:02:10,50 --> 00:02:14,10
on the very first time we loaded the article page.

52
00:02:14,10 --> 00:02:15,90
So if we want useEffect to be called

53
00:02:15,90 --> 00:02:19,00
every time we switch between articles,

54
00:02:19,00 --> 00:02:20,70
we have to look at what's actually changing

55
00:02:20,70 --> 00:02:23,50
whenever we switch articles.

56
00:02:23,50 --> 00:02:25,90
We want to look at the URL,

57
00:02:25,90 --> 00:02:27,90
and the changing of our URL

58
00:02:27,90 --> 00:02:32,20
is reflected in this name parameter here.

59
00:02:32,20 --> 00:02:35,60
So all we have to do is add name to this array,

60
00:02:35,60 --> 00:02:38,30
and useEffect will get called whenever name changes.

61
00:02:38,30 --> 00:02:41,50
In other words, whenever we change between articles.

62
00:02:41,50 --> 00:02:45,10
Now if we save our code and go back,

63
00:02:45,10 --> 00:02:47,30
we see that whenever we change between articles,

64
00:02:47,30 --> 00:02:49,40
we get a different number.

65
00:02:49,40 --> 00:02:51,80
So now we're sure that useEffect is being called

66
00:02:51,80 --> 00:02:55,00
at the right times, whenever we go to a different article.

