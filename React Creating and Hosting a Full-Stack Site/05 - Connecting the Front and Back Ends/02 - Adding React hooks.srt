1
00:00:00,10 --> 00:00:02,10
- [Teacher] Now that we know the basic syntax of

2
00:00:02,10 --> 00:00:04,70
the fetch function and have the polyfills set up.

3
00:00:04,70 --> 00:00:07,20
Lets look at how to load data into our article pages

4
00:00:07,20 --> 00:00:08,80
from the server.

5
00:00:08,80 --> 00:00:11,60
When we navigate to one of the article pages on our blog,

6
00:00:11,60 --> 00:00:14,80
we want our front end to automatically query the server

7
00:00:14,80 --> 00:00:17,20
and display the comments and number help votes

8
00:00:17,20 --> 00:00:19,40
that the article has.

9
00:00:19,40 --> 00:00:20,90
Now there are a few new concepts

10
00:00:20,90 --> 00:00:22,40
that we're going to introduce here,

11
00:00:22,40 --> 00:00:25,90
that together will allow us to implement this functionality.

12
00:00:25,90 --> 00:00:28,00
The first thing we're going to have to do is add state

13
00:00:28,00 --> 00:00:29,60
to our components.

14
00:00:29,60 --> 00:00:32,20
Currently our components don't have any form of memeory,

15
00:00:32,20 --> 00:00:34,30
they simply return some JSX depending on

16
00:00:34,30 --> 00:00:36,80
the props that we pass into them.

17
00:00:36,80 --> 00:00:38,60
What state gives us is a place to

18
00:00:38,60 --> 00:00:40,60
temporarily store information,

19
00:00:40,60 --> 00:00:44,10
for example information that we've loaded from our server.

20
00:00:44,10 --> 00:00:47,70
Now as a version 16.8, React has provided us with

21
00:00:47,70 --> 00:00:49,80
a new of adding state to our components,

22
00:00:49,80 --> 00:00:53,10
without using the class notation that you might of seen.

23
00:00:53,10 --> 00:00:56,30
And this is by using something called React hooks,

24
00:00:56,30 --> 00:00:58,90
React Hooks are basically just functions that we can call,

25
00:00:58,90 --> 00:01:02,30
that abstract the way state management for our components.

26
00:01:02,30 --> 00:01:04,80
And again this allows us to use them in components,

27
00:01:04,80 --> 00:01:06,30
which are defined without directly

28
00:01:06,30 --> 00:01:09,60
extending React's component class.

29
00:01:09,60 --> 00:01:11,80
So the syntax for keeping track of state

30
00:01:11,80 --> 00:01:14,40
using React hooks is fairly simple.

31
00:01:14,40 --> 00:01:19,20
First we have to import useState from React like this,

32
00:01:19,20 --> 00:01:22,20
and then inside our component we're going to use the following,

33
00:01:22,20 --> 00:01:25,80
somewhat strange syntax.

34
00:01:25,80 --> 00:01:29,40
We're going to say const, array,

35
00:01:29,40 --> 00:01:40,00
articleInfo, and setArticleInfo equals useState,

36
00:01:40,00 --> 00:01:41,80
and an empty object.

37
00:01:41,80 --> 00:01:44,60
Lets looks at all the pieces of this statement for a moment,

38
00:01:44,60 --> 00:01:47,20
here we're defining articleInfo,

39
00:01:47,20 --> 00:01:49,30
which we're going to populate by sending a request

40
00:01:49,30 --> 00:01:50,50
to the server,

41
00:01:50,50 --> 00:01:53,50
and setArticleInfo, which is a function

42
00:01:53,50 --> 00:01:57,40
that we can call to change the value of ArticleInfo.

43
00:01:57,40 --> 00:02:00,60
And finally the argument that we passed to useState is

44
00:02:00,60 --> 00:02:03,40
the initial value of ArticleInfo before we've loaded

45
00:02:03,40 --> 00:02:06,10
any data or changed the state.

46
00:02:06,10 --> 00:02:07,90
And in fact it'll make it easier for us,

47
00:02:07,90 --> 00:02:09,20
if we make our default value,

48
00:02:09,20 --> 00:02:11,10
not just an empty object

49
00:02:11,10 --> 00:02:12,60
but include the default values for

50
00:02:12,60 --> 00:02:16,00
the properties we expect to see in our ArticleInfo.

51
00:02:16,00 --> 00:02:18,80
For example we'll want upvotes property

52
00:02:18,80 --> 00:02:21,60
and we'll want the default value for that to be zero,

53
00:02:21,60 --> 00:02:23,20
and a comments property

54
00:02:23,20 --> 00:02:24,60
and we'll want the default value of

55
00:02:24,60 --> 00:02:28,90
that to be an empty array.

56
00:02:28,90 --> 00:02:31,10
So now how do we actually make the requests to

57
00:02:31,10 --> 00:02:33,90
the back end to load our ArticleInfo?

58
00:02:33,90 --> 00:02:35,50
Well to do that we're going to need

59
00:02:35,50 --> 00:02:38,50
another React hook called useEffect.

60
00:02:38,50 --> 00:02:40,90
useEffect gives us a place to perform all

61
00:02:40,90 --> 00:02:42,40
the side effects of our components,

62
00:02:42,40 --> 00:02:44,60
such as, in our fetching data

63
00:02:44,60 --> 00:02:47,00
and setting the state with the result,

64
00:02:47,00 --> 00:02:49,00
and I'll show you what that looks like in a minute.

65
00:02:49,00 --> 00:02:51,00
But first let's create an element to display

66
00:02:51,00 --> 00:02:54,00
how many upvotes our article has?

67
00:02:54,00 --> 00:02:57,00
We'll get to the comment section in a later video.

68
00:02:57,00 --> 00:02:59,60
So right here underneath our article.title,

69
00:02:59,60 --> 00:03:02,30
we're going to display a line that says

70
00:03:02,30 --> 00:03:07,80
this post has been upvoted,

71
00:03:07,80 --> 00:03:16,30
brackets and were going to do articleInfo.upvotes times.

72
00:03:16,30 --> 00:03:19,10
So now before we actually implement the fetching code,

73
00:03:19,10 --> 00:03:20,60
let's write out a little test code

74
00:03:20,60 --> 00:03:23,20
just to see useeffect works.

75
00:03:23,20 --> 00:03:32,30
First let's import useeffect from React,

76
00:03:32,30 --> 00:03:33,80
and then the way we use it

77
00:03:33,80 --> 00:03:36,70
is we just call it inside our component.

78
00:03:36,70 --> 00:03:41,10
useEffect and inside the parenthesis of useEffect,

79
00:03:41,10 --> 00:03:43,00
we're going to define an anonymous function

80
00:03:43,00 --> 00:03:44,60
that will basically get called whenever

81
00:03:44,60 --> 00:03:46,50
our component is loaded.

82
00:03:46,50 --> 00:03:49,70
So inside here we're going to call setArticleInfo

83
00:03:49,70 --> 00:03:52,30
and we're just going to pass it some dummy data

84
00:03:52,30 --> 00:03:56,90
that we want our ArticleInfo variable here to be set to.

85
00:03:56,90 --> 00:04:01,60
For now let's say upvotes 3,

86
00:04:01,60 --> 00:04:07,40
now if we run our front end site by typing mpm start,

87
00:04:07,40 --> 00:04:09,90
and navigate to one of our articles,

88
00:04:09,90 --> 00:04:12,40
we can see that ArticleInfo is now properly loaded

89
00:04:12,40 --> 00:04:14,60
and displayed up top here.

90
00:04:14,60 --> 00:04:16,70
And notice that if we refresh the page,

91
00:04:16,70 --> 00:04:19,20
we can see zero very briefly before our

92
00:04:19,20 --> 00:04:21,00
useEffect function is called.

