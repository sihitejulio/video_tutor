1
00:00:00,50 --> 00:00:02,30
- [Narrator] Our article page is now correctly loading

2
00:00:02,30 --> 00:00:04,60
information about the articles it displays,

3
00:00:04,60 --> 00:00:06,30
and it's correctly showing the number of up votes

4
00:00:06,30 --> 00:00:08,30
a given article has.

5
00:00:08,30 --> 00:00:10,50
The next step is for us to build a new component

6
00:00:10,50 --> 00:00:13,30
to display the comments for our article.

7
00:00:13,30 --> 00:00:15,20
So let's go into our components directory,

8
00:00:15,20 --> 00:00:16,70
and create a new file,

9
00:00:16,70 --> 00:00:20,60
and we'll call it CommentsList.js

10
00:00:20,60 --> 00:00:22,60
and then we're going to create a new component.

11
00:00:22,60 --> 00:00:27,10
So we'll import React, import react from react,

12
00:00:27,10 --> 00:00:29,90
and we'll define a component called comments list,

13
00:00:29,90 --> 00:00:36,20
const comments list equals, parentheses,

14
00:00:36,20 --> 00:00:39,30
and our comments list is only going to take one prop,

15
00:00:39,30 --> 00:00:43,60
which will be the comments we want it to display.

16
00:00:43,60 --> 00:00:46,40
So now inside our component,

17
00:00:46,40 --> 00:00:51,30
we're going to map our comments, comments.map

18
00:00:51,30 --> 00:00:57,30
and for each comment we're going to have a div

19
00:00:57,30 --> 00:00:58,90
and if you want to use my CSS

20
00:00:58,90 --> 00:01:03,70
add a class name property here, called comment.

21
00:01:03,70 --> 00:01:05,40
And inside this div we're just going to display

22
00:01:05,40 --> 00:01:08,70
the user name and text properties of our comment.

23
00:01:08,70 --> 00:01:14,10
So for the user name property, we'll use an H4 tag,

24
00:01:14,10 --> 00:01:18,30
and we're going to say comment.username

25
00:01:18,30 --> 00:01:19,50
and then underneath that

26
00:01:19,50 --> 00:01:21,60
we're going to have a paragraph tag,

27
00:01:21,60 --> 00:01:25,90
and inside that we're going to display comment.text

28
00:01:25,90 --> 00:01:28,60
and again, since we're returning more than one element here,

29
00:01:28,60 --> 00:01:30,50
we're going to want to wrap this in react fragments,

30
00:01:30,50 --> 00:01:34,10
like this.

31
00:01:34,10 --> 00:01:35,10
And last but not least,

32
00:01:35,10 --> 00:01:37,80
let's just add a heading to our comments section,

33
00:01:37,80 --> 00:01:41,70
it'll be an H3 tag, and it'll just say comments.

34
00:01:41,70 --> 00:01:43,30
Like this.

35
00:01:43,30 --> 00:01:45,40
And we're also going to want to add a key here,

36
00:01:45,40 --> 00:01:49,40
so that react doesn't yell at us, since we're using map,

37
00:01:49,40 --> 00:01:52,40
and just add that to the outer most element here.

38
00:01:52,40 --> 00:01:55,20
Key equals key.

39
00:01:55,20 --> 00:01:56,40
Great, so now that we've got

40
00:01:56,40 --> 00:01:58,50
our comments list component built out,

41
00:01:58,50 --> 00:02:02,20
let's go back to our article page component and add it.

42
00:02:02,20 --> 00:02:03,80
So first, let's import our comments list

43
00:02:03,80 --> 00:02:06,00
from the file that it's in.

44
00:02:06,00 --> 00:02:11,50
We'll say import comments list from dot dot slash

45
00:02:11,50 --> 00:02:17,70
components slash comments list.

46
00:02:17,70 --> 00:02:20,30
And then down below,

47
00:02:20,30 --> 00:02:22,20
right above our other article section,

48
00:02:22,20 --> 00:02:25,60
let's add our comments list component.

49
00:02:25,60 --> 00:02:28,00
We're going to say comments list

50
00:02:28,00 --> 00:02:30,90
and for the comments prop

51
00:02:30,90 --> 00:02:35,20
we're going to pass in article info, our state,

52
00:02:35,20 --> 00:02:37,70
dot comments.

53
00:02:37,70 --> 00:02:40,30
And close the tag.

54
00:02:40,30 --> 00:02:42,80
And we just need to go back to our comments list,

55
00:02:42,80 --> 00:02:44,80
and make sure that we export.

56
00:02:44,80 --> 00:02:50,00
So we'll say export default comments list,

57
00:02:50,00 --> 00:02:52,00
and save it.

58
00:02:52,00 --> 00:02:53,40
And then if we go back to our site

59
00:02:53,40 --> 00:02:55,20
and scroll down to the bottom,

60
00:02:55,20 --> 00:02:57,30
we see that we now have a working comments section

61
00:02:57,30 --> 00:02:59,50
that displays the comments for our article.

62
00:02:59,50 --> 00:03:01,00
Just as we wanted.

