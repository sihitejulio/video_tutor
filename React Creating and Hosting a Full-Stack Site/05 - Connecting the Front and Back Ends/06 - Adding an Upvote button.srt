1
00:00:10,40 --> 00:00:21,60
But this interaction is still one-sided.

2
00:00:21,60 --> 00:00:34,30
an upvote to a specific article.

3
00:00:34,30 --> 00:00:52,40
a new file, and we'll call it UpvotesSection.js.

4
00:00:52,40 --> 00:00:59,00
UpvotesSection, and for now it'll be empty.

5
00:00:59,00 --> 00:01:02,00
And, as usual, we're going to want to export it.

6
00:01:02,00 --> 00:01:10,00
Export default UpvotesSection.

7
00:01:10,00 --> 00:01:11,40
So the first thing we're going to want to do

8
00:01:11,40 --> 00:01:15,20
in our UpvotesSection is go to our article page component

9
00:01:15,20 --> 00:01:17,60
and pull out this text that tells us how many times

10
00:01:17,60 --> 00:01:19,80
an article has been upvoted.

11
00:01:19,80 --> 00:01:22,90
We're going to cut that,

12
00:01:22,90 --> 00:01:26,90
and we're going to paste it in our UpvotesSection.

13
00:01:26,90 --> 00:01:28,80
Next we want to add a button,

14
00:01:28,80 --> 00:01:32,20
and the text of this button will just say Add Upvote,

15
00:01:32,20 --> 00:01:37,30
and finally we'll wrap the whole thing in a div.

16
00:01:37,30 --> 00:01:40,60
purposes I'm going to use a div, and the id for this div

17
00:01:40,60 --> 00:01:50,90
is going to be upvotes-section.

18
00:01:50,90 --> 00:01:55,20
inside the div,

19
00:01:55,20 --> 00:01:58,10
and now for actually upvoting our article,

20
00:01:58,10 --> 00:01:59,80
the onClick method of this button is going to be

21
00:01:59,80 --> 00:02:04,10
where the magic happens.

22
00:02:04,10 --> 00:02:06,20
directly inside this method,

23
00:02:06,20 --> 00:02:07,30
but instead of cluttering up

24
00:02:07,30 --> 00:02:10,60
our UI elements here, let's define it up top.

25
00:02:10,60 --> 00:02:12,80
So we're going to add brackets to our function body,

26
00:02:12,80 --> 00:02:18,40
and a return statement,

27
00:02:18,40 --> 00:02:20,70
and then up here, we're going to write an asynchronous function

28
00:02:20,70 --> 00:02:23,70
that takes care of the fetching logic for us.

29
00:02:23,70 --> 00:02:32,20
So we're going to say const, and we'll call this function

30
00:02:32,20 --> 00:02:34,10
and inside the body of this function,

31
00:02:34,10 --> 00:02:37,30
we're going to want to make a post request using fetch.

32
00:02:37,30 --> 00:02:40,00
And the way we do that is by saying await, since fetch

33
00:02:40,00 --> 00:02:43,80
is asynchronous, say fetch, and then backticks,

34
00:02:43,80 --> 00:02:47,00
instead of single quotes, and then we want to write the path

35
00:02:47,00 --> 00:02:49,00
for our upvoteArticles endpoint.

36
00:02:49,00 --> 00:02:55,50
Which is /api/articles/ and then our article name,

37
00:02:55,50 --> 00:02:57,50
and we're going to pass this in as a prop,

38
00:02:57,50 --> 00:03:00,30
but for now, we'll call it articleName,

39
00:03:00,30 --> 00:03:03,80
and then / and upvote.

40
00:03:03,80 --> 00:03:07,20
Since we want this to be a post request, as we saw earlier,

41
00:03:07,20 --> 00:03:11,20
we have to pass an options object to fetch.

42
00:03:11,20 --> 00:03:13,00
This object is just going to have one property,

43
00:03:13,00 --> 00:03:15,60
and that property is going to be called method,

44
00:03:15,60 --> 00:03:47,80
and the value is going to be post.

45
00:03:47,80 --> 00:03:52,80
and then we're going to want to update

46
00:03:52,80 --> 00:04:02,70
That'll be done using setArticleInfo(body).

47
00:04:02,70 --> 00:04:07,60
that aren't defined yet.

48
00:04:07,60 --> 00:04:45,50
as a prop, which we don't have yet.

49
00:04:45,50 --> 00:04:49,10
we'll pass through our setArticleInfo as a prop, as well.

50
00:04:49,10 --> 00:04:51,60
So now that we have our upvote Article function defined,

51
00:04:51,60 --> 00:05:28,30
let's call it from our button's onClick method.

52
00:05:28,30 --> 00:05:34,20
the article's name, we need to pass through the number

53
00:05:34,20 --> 00:05:39,80
the setArticleInfo for our article page's state.

54
00:05:39,80 --> 00:05:51,70
Let's pass through the article name, articleName=,

55
00:05:51,70 --> 00:06:02,40
So articleName equals name upvotes,

56
00:06:02,40 --> 00:06:03,90
and then we want to pass through

57
00:06:03,90 --> 00:06:07,20
our setArticle info hook, like this.

58
00:06:07,20 --> 00:06:42,00
setArticleInfo=setArticleInfo,

