1
00:00:00,50 --> 00:00:02,20
- Our website now has a way for us to add

2
00:00:02,20 --> 00:00:03,50
up votes to articles,

3
00:00:03,50 --> 00:00:06,40
but it's still lacking a way to add comments.

4
00:00:06,40 --> 00:00:09,30
So let's figure out how to add that functionality.

5
00:00:09,30 --> 00:00:12,10
A good place to start, as with our other additions,

6
00:00:12,10 --> 00:00:16,50
would be to create a new file in our components directory.

7
00:00:16,50 --> 00:00:21,00
And we'll call it, add comment form.

8
00:00:21,00 --> 00:00:22,80
.js

9
00:00:22,80 --> 00:00:29,40
and then again, we'll add the necessary boiler plate.

10
00:00:29,40 --> 00:00:36,00
create our new component.

11
00:00:36,00 --> 00:00:42,30
And export our component.

12
00:00:42,30 --> 00:00:45,10
Now inside our component here,

13
00:00:45,10 --> 00:00:51,60
there's three basic things we're going to want

14
00:00:51,60 --> 00:00:56,50
We'll say input type equals text.

15
00:00:56,50 --> 00:00:59,80
We're going to want a text area for them

16
00:00:59,80 --> 00:01:02,20
We'll give this text are four rows.

17
00:01:02,20 --> 00:01:03,70
Rows equals four.

18
00:01:03,70 --> 00:01:08,20
We'll give it 50 columns.

19
00:01:08,20 --> 00:01:13,00
And last but not least we're going to want a submit button

20
00:01:13,00 --> 00:01:15,20
So we'll say button.

21
00:01:15,20 --> 00:01:18,20
And this button will say, add comment.

22
00:01:18,20 --> 00:01:21,80
Now let's wrap this whole thing in a div.

23
00:01:21,80 --> 00:01:24,50
And if you're using my styling let's add I-D.

24
00:01:24,50 --> 00:01:28,80
equals add comment form.

25
00:01:28,80 --> 00:01:38,30
And put all of our stuff inside the div.

26
00:01:38,30 --> 00:01:40,90
Which will just look like this.

27
00:01:40,90 --> 00:01:43,30
Say label

28
00:01:43,30 --> 00:01:46,20
Name

29
00:01:46,20 --> 00:01:51,50
and put our Name text box inside of here.

30
00:01:51,50 --> 00:01:54,00
And label

31
00:01:54,00 --> 00:01:55,80
Comment

32
00:01:55,80 --> 00:02:02,00
and we'll put our text area inside of there.

33
00:02:02,00 --> 00:02:04,60
And finally let's add a heading.

34
00:02:04,60 --> 00:02:11,80
We'll use h3.

35
00:02:11,80 --> 00:02:13,70
And now that we've got the basic structure of our

36
00:02:13,70 --> 00:02:15,50
add comment form set up,

37
00:02:15,50 --> 00:02:17,50
we're going to want to give our add comment form

38
00:02:17,50 --> 00:02:20,00
some sort of internal state to make it easier for us

39
00:02:20,00 --> 00:02:23,80
to keep track of the values of these two text fields.

40
00:02:23,80 --> 00:02:25,10
And the way we're going to do that,

41
00:02:25,10 --> 00:02:27,00
is going to be similar to how we did it with the

42
00:02:27,00 --> 00:02:28,80
article page component.

43
00:02:28,80 --> 00:02:31,90
We're going to use the use state react hook.

44
00:02:31,90 --> 00:02:35,60
So let's import that up here.

45
00:02:35,60 --> 00:02:39,80
In brackets, use state.

46
00:02:39,80 --> 00:02:43,00
And then we're going to add brackets to our

47
00:02:43,00 --> 00:02:46,60
Since we're doing more than just returning J-S-X.

48
00:02:46,60 --> 00:02:53,50
And we'll add this return key word, as well.

49
00:02:53,50 --> 00:02:56,30
And then we'll add a closing bracket.

50
00:02:56,30 --> 00:02:58,80
And now let's use the use state react hook

51
00:02:58,80 --> 00:03:01,70
to define some new state variables.

52
00:03:01,70 --> 00:03:03,20
And again that's going to look like this.

53
00:03:03,20 --> 00:03:06,00
Const, and we'll have a state variable

54
00:03:06,00 --> 00:03:08,10
that says user name.

55
00:03:08,10 --> 00:03:12,40
To hold the user name property from our common form.

56
00:03:12,40 --> 00:03:14,60
And we'll say set user name which will give us a function

57
00:03:14,60 --> 00:03:19,20
to actually change the value of this user name state.

58
00:03:19,20 --> 00:03:23,80
And then we'll say equals use state.

59
00:03:23,80 --> 00:03:26,50
an empty string here.

60
00:03:26,50 --> 00:03:29,30
And then we'll do the same thing for the comment text.

61
00:03:29,30 --> 00:03:32,30
So we'll say const and then an array.

62
00:03:32,30 --> 00:03:35,00
We'll say comment text.

63
00:03:35,00 --> 00:03:38,10
And set comment text.

64
00:03:38,10 --> 00:03:40,90
And we'll set that equal to use state.

65
00:03:40,90 --> 00:03:42,60
And the default value for this one will be an

66
00:03:42,60 --> 00:03:44,00
empty string, as well.

