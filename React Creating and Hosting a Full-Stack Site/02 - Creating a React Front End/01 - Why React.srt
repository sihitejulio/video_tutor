1
00:00:00,50 --> 00:00:05,00
- [Instructor] So as I mentioned,

2
00:00:05,00 --> 00:00:12,30
Now, you may be wondering why we'd choose to React

3
00:00:12,30 --> 00:00:13,90
Well React has many advantages

4
00:00:13,90 --> 00:00:20,70
over hand coding a static site.

5
00:00:20,70 --> 00:00:24,40
any of the fine-grained control from the programmer.

6
00:00:24,40 --> 00:00:28,60
Basically, it allows us to create modular,

7
00:00:28,60 --> 00:00:30,40
into a fully functional website,

8
00:00:30,40 --> 00:00:34,30
instead of hand coding each individual page in our site.

9
00:00:34,30 --> 00:00:36,10
The next question is why you'd use React

10
00:00:36,10 --> 00:00:39,50
over a platform like WordPress or Wix or Squarespace,

11
00:00:39,50 --> 00:00:43,30
all of which allow you to create a website

12
00:00:43,30 --> 00:00:45,30
Well first of all, since this course is aimed

13
00:00:45,30 --> 00:00:47,50
at primarily at developers,

14
00:00:47,50 --> 00:00:49,00
I'm assuming you have more of an interest

15
00:00:49,00 --> 00:00:54,80
in creating a website using code rather than drag and drop.

16
00:00:54,80 --> 00:00:57,20
and you're not already a fairly strong developer,

17
00:00:57,20 --> 00:00:58,40
I would recommend that you use

18
00:00:58,40 --> 00:01:01,60
one of these platforms instead of React.

19
00:01:01,60 --> 00:01:02,90
Although be warned,

20
00:01:02,90 --> 00:01:04,80
even though these websites have advertising

21
00:01:04,80 --> 00:01:07,00
that makes it sound like you can create a website

22
00:01:07,00 --> 00:01:09,20
in a few minutes, I've personally found

23
00:01:09,20 --> 00:01:14,20
that they have a bit of a learning curve as well.

24
00:01:14,20 --> 00:01:18,20
you usually end up sacrificing a lot of flexibility.

25
00:01:18,20 --> 00:01:21,10
So all that being said, React is a fantastic tool

26
00:01:21,10 --> 00:01:24,90
for programmers who want to create basically

27
00:01:24,90 --> 00:01:26,60
And with the advent of React Native,

28
00:01:26,60 --> 00:01:28,10
it's rapidly becoming a tool

29
00:01:28,10 --> 00:01:30,60
for creating mobile apps as well.

30
00:01:30,60 --> 00:01:33,60
React developers are also currently highly sought after

31
00:01:33,60 --> 00:01:38,40
by many companies who are looking

32
00:01:38,40 --> 00:01:40,30
So with all that knowledge, let's move on

33
00:01:40,30 --> 00:01:43,00
and see how we can quickly set up a React powered front end.

