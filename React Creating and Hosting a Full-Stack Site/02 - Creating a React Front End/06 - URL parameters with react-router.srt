1
00:00:02,40 --> 00:00:07,80
working as expected with React Router,

2
00:00:07,80 --> 00:00:18,30
The interesting part about this page is that

3
00:00:18,30 --> 00:00:45,00
it should show our article about learning React.

4
00:00:45,00 --> 00:00:55,30
We're going to use name for our URL parameter.

5
00:00:55,30 --> 00:00:57,80
in this case articlePage,

6
00:00:57,80 --> 00:00:58,90
that tells our component

7
00:00:58,90 --> 00:01:01,80
the value of the section of our URL.

8
00:01:01,80 --> 00:01:04,20
In other words, the string that occupies this spot

9
00:01:04,20 --> 00:01:07,40
in the URL in the user's browser.

10
00:01:07,40 --> 00:01:18,50
So for example if we navigate to article/learn-react

11
00:01:18,50 --> 00:01:30,50
inside our component?

12
00:01:30,50 --> 00:01:50,80
Now this match prop contains some useful information,

13
00:01:50,80 --> 00:01:54,10
And then we're going to pull our URL parameter, name,

14
00:01:54,10 --> 00:02:09,40
And that'll look like this.

15
00:02:09,40 --> 00:02:25,40
in our heading.

16
00:02:25,40 --> 00:02:28,20
we'll say learn-react,

17
00:02:28,20 --> 00:02:30,80
we see that heading correctly displays the string

18
00:02:30,80 --> 00:02:48,00
that we have in the part of the URL.

19
00:02:48,00 --> 00:02:51,40
and can make changes to itself accordingly.

20
00:02:51,40 --> 00:02:53,60
In order to see the real power of this setup,

21
00:02:53,60 --> 00:03:06,40
we need some content.

22
00:03:06,40 --> 00:03:08,20
in the exercise files,

23
00:03:08,20 --> 00:03:14,20
so I'm going to add that in right here.

24
00:03:14,20 --> 00:03:23,70
which represent articles,

25
00:03:23,70 --> 00:03:26,50
In reality we might choose to represent our article data

26
00:03:26,50 --> 00:03:29,40
in many other forms,

27
00:03:29,40 --> 00:03:42,60
this is the format we're going to use here.

28
00:03:42,60 --> 00:04:29,00
at the bottom of the file.

29
00:04:29,00 --> 00:04:34,40
is equal to the name property that we get

30
00:04:34,40 --> 00:04:45,20
And finally we'll convert this article information into JSX.

31
00:04:45,20 --> 00:05:07,20
And then underneath that

32
00:05:07,20 --> 00:05:31,10
and put the paragraph string in between it.

33
00:05:31,10 --> 00:05:38,50
This is displaying because the name hello

34
00:05:38,50 --> 00:05:48,20
But for now let's type in the name

35
00:05:48,20 --> 00:06:07,20
But the problem is, as we've already seen,

36
00:06:07,20 --> 00:06:23,70
We'll say if article doesn't exist

37
00:06:23,70 --> 00:06:25,00
we just get a heading that tells us

38
00:06:25,00 --> 00:06:27,00
that the article doesn't exist.

