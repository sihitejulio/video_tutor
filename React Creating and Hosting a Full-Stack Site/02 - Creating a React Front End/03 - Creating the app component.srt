1
00:00:06,40 --> 00:00:29,00
The first thing we're going to do is create another folder

2
00:00:29,00 --> 00:00:30,40
Then inside this file, we're going to create

3
00:00:30,40 --> 00:00:33,20
a very basic react component called homepage.

4
00:00:33,20 --> 00:00:37,40
First, we're going to import React

5
00:00:37,40 --> 00:00:43,70
and then we create our homepage component like this,

6
00:00:43,70 --> 00:00:45,70
And for now, this component is simply going to have a heading

7
00:00:45,70 --> 00:00:48,60
that says, hello, welcome to my blog,

8
00:00:48,60 --> 00:00:56,00
and that'll look like this, h1, hello, welcome to my blog.

9
00:00:56,00 --> 00:00:57,70
And then underneath this we're going to put a few

10
00:00:57,70 --> 00:01:21,60
paragraphs of lorem ipsum text which will help us build out

11
00:01:21,60 --> 00:03:54,30
make sure you're running at least react 16.2 which

12
00:03:54,30 --> 00:04:27,30
add to our elements across the course will be specifically

13
00:04:27,30 --> 00:04:30,30
Let's add react router to our project,

14
00:04:30,30 --> 00:04:44,70
to do that we need to run npm install dash dash save, react

15
00:04:44,70 --> 00:04:51,10
to add the correct router components to our app dot js file.

16
00:04:51,10 --> 00:04:54,80
the moment, and these are called browser router, and route.

17
00:04:54,80 --> 00:04:57,60
So let's do import,

18
00:04:57,60 --> 00:05:03,70
browser router, and we usually rename this to just router,

19
00:05:03,70 --> 00:05:06,60
and route, and we'll be importing those from

20
00:05:06,60 --> 00:05:10,40
react dash router dash dom.

21
00:05:10,40 --> 00:05:13,00
So first what we're going to do is wrap our entire app

22
00:05:13,00 --> 00:05:16,40
in the router component, since what this component does is

23
00:05:16,40 --> 00:05:19,20
make sure that our entire rendered app is kept up to date

24
00:05:19,20 --> 00:05:29,20
with the browsers current url.

25
00:05:29,20 --> 00:06:02,50
Then we're going to remove our homepage component

26
00:06:02,50 --> 00:06:04,90
And then there's just one more thing we have to do,

27
00:06:04,90 --> 00:06:08,10
because of the way react router matches routes, our slash

28
00:06:08,10 --> 00:06:11,30
route will match every single route, so in order to tell

29
00:06:11,30 --> 00:06:19,70
react router that it needs to match exactly, we just need to

30
00:06:19,70 --> 00:06:23,80
So let's start our app again using npm start.

31
00:06:23,80 --> 00:06:26,10
Now when our app comes up, we see that our homepage

32
00:06:26,10 --> 00:06:29,20
is displayed when the browsers url matches, if we change

33
00:06:29,20 --> 00:06:34,40
the url so that it doesn't match,

34
00:06:34,40 --> 00:06:37,00
which is exactly what we would expect.

