1
00:00:05,30 --> 00:00:12,80
based on the name parameter that's in the browser URL.

2
00:00:12,80 --> 00:00:15,20
a bunch of very similar HTML pages,

3
00:00:15,20 --> 00:00:33,30
one for each of our articles,

4
00:00:33,30 --> 00:00:38,90
instead of having to change all of our article files.

5
00:00:38,90 --> 00:00:49,50
to the one that we faced in an earlier video

6
00:00:49,50 --> 00:01:05,40
to be able to access each of our articles

7
00:01:05,40 --> 00:01:08,50
So let's open our ArticlesList component,

8
00:01:08,50 --> 00:01:09,60
and the first thing we're going to do

9
00:01:09,60 --> 00:01:47,10
is import our article content from our article content file,

10
00:01:47,10 --> 00:01:56,90
and we'll put that in between h3 tags,

11
00:01:56,90 --> 00:02:06,30
we can see that we have a list of our article titles,

12
00:02:06,30 --> 00:02:10,60
that we talked about in the previous episode.

13
00:02:10,60 --> 00:02:30,40
That's going to be import brackets Link

14
00:02:30,40 --> 00:02:33,40
And for the to prop for the link component,

15
00:02:33,40 --> 00:02:35,60
we're going to want it to link to slash articles,

16
00:02:35,60 --> 00:02:39,00
and then the article's name,

17
00:02:39,00 --> 00:02:41,00
We have to use brackets in this case,

18
00:02:41,00 --> 00:02:57,30
and also backticks instead of quotation marks,

19
00:02:57,30 --> 00:03:02,90
to the outermost element, like this.

20
00:03:02,90 --> 00:03:11,90
Key equals brackets key, and there we have it.

21
00:03:11,90 --> 00:03:16,40
and it will take us to the corresponding article page.

22
00:03:16,40 --> 00:03:20,40
And if you're using the CSS I've provided,

23
00:03:20,40 --> 00:04:02,30
let's add a className prop,

24
00:04:02,30 --> 00:04:04,70
is an array of the paragraphs of our article,

25
00:04:04,70 --> 00:04:34,00
so we're just going to want to get

