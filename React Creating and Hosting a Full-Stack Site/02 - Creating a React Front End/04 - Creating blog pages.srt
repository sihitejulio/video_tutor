1
00:00:00,00 --> 00:00:01,60
- [Narrator} So now that we have our app

2
00:00:01,60 --> 00:00:03,90
set up to use React Router, let's create

3
00:00:03,90 --> 00:00:06,60
the rest of the pages that we're going to have in our app.

4
00:00:06,60 --> 00:00:08,20
We're going to create each of these pages

5
00:00:08,20 --> 00:00:11,00
as its own component inside the pages folder.

6
00:00:11,00 --> 00:00:13,40
So, we already have our homepage,

7
00:00:13,40 --> 00:00:16,30
which is the landing page that you'll find on any blog,

8
00:00:16,30 --> 00:00:20,20
and we're going to create an about page,

9
00:00:20,20 --> 00:00:26,30
called AboutPage.js and an articles list page.

10
00:00:26,30 --> 00:00:32,30
We'll call it Articleslist.js,

11
00:00:32,30 --> 00:00:34,90
an article page, which will populate with the text

12
00:00:34,90 --> 00:00:36,40
from each of our articles. We'll see

13
00:00:36,40 --> 00:00:38,90
how to do that in the later video.

14
00:00:38,90 --> 00:00:41,80
ArticlePage.js.

15
00:00:41,80 --> 00:00:43,40
Now that we've created these files,

16
00:00:43,40 --> 00:00:45,70
let's do what we did with the home page component

17
00:00:45,70 --> 00:00:47,70
and create a very rudimentary component

18
00:00:47,70 --> 00:00:55,00
inside each of these that tells us what page we're on.

19
00:00:55,00 --> 00:00:58,10
and copy all this code, and go into

20
00:00:58,10 --> 00:01:05,50
the About page and paste it. And then

21
00:01:05,50 --> 00:01:10,30
and rename it here as well -- AboutPage.

22
00:01:10,30 --> 00:01:14,50
And then we're going to change this heading

23
00:01:14,50 --> 00:01:16,50
onto our articles list page and paste

24
00:01:16,50 --> 00:01:19,20
our HomePage code here as well.

25
00:01:19,20 --> 00:01:23,80
We'll rename it ArticlesList.

26
00:01:23,80 --> 00:01:25,50
And let's delete the lorem ipsum here

27
00:01:25,50 --> 00:01:26,50
'cause we're going to be displaying

28
00:01:26,50 --> 00:01:37,60
something different on the ArticlesList page.

29
00:01:37,60 --> 00:01:39,90
And then let's copy this code and go

30
00:01:39,90 --> 00:01:51,00
onto our ArticlePage and paste it here.

31
00:01:51,00 --> 00:01:54,70
'This is an article'. So now that we've got

32
00:01:54,70 --> 00:01:56,60
all of our pages created, let's go

33
00:01:56,60 --> 00:02:00,50
into our App.js file and add them to React Router.

34
00:02:00,50 --> 00:02:02,50
Basically, we're just going to add a new route component

35
00:02:02,50 --> 00:02:05,50
for each page. So first, let's import

36
00:02:05,50 --> 00:02:08,60
all of our pages, like this --

37
00:02:08,60 --> 00:02:17,20
import AboutPage from './pages/AboutPage',

38
00:02:17,20 --> 00:02:27,60
import ArticlesList from './pages/ArticlesList',

39
00:02:27,60 --> 00:02:37,20
and import ArticlePage from '.pages/ArticlePage'.

40
00:02:37,20 --> 00:02:38,50
Once we've done that, we're going to add

41
00:02:38,50 --> 00:02:40,50
a new route for each of these.

42
00:02:40,50 --> 00:02:47,70
So first our AboutPage, we'll create a new route for that.

43
00:02:47,70 --> 00:02:49,10
And we'll set the path here equal

44
00:02:49,10 --> 00:02:52,50
to /about and the component we want to display

45
00:02:52,50 --> 00:02:56,10
at that route is the AboutPage component.

46
00:02:56,10 --> 00:02:57,30
And notice that we don't need to add

47
00:02:57,30 --> 00:02:58,70
an exact prop to these routes

48
00:02:58,70 --> 00:03:00,80
like we do to the home page route.

49
00:03:00,80 --> 00:03:04,40
Now let's add a route for our ArticlesList,

50
00:03:04,40 --> 00:03:13,20
and the path for that will be /articles-list

51
00:03:13,20 --> 00:03:17,40
will be the ArticlesList component.

52
00:03:17,40 --> 00:03:22,10
And finally -- the ArticlePage.

53
00:03:22,10 --> 00:03:26,90
The path for that will be article,

54
00:03:26,90 --> 00:03:28,10
and the component we display there

55
00:03:28,10 --> 00:03:32,30
will be the ArticlePage component.

56
00:03:32,30 --> 00:03:35,80
Okay, now that we have all our routes in place,

57
00:03:35,80 --> 00:03:38,20
the different routes into the URL.

58
00:03:38,20 --> 00:03:40,20
First, we'll go to our about page,

59
00:03:40,20 --> 00:03:43,50
and we see it goes to the about page.

60
00:03:43,50 --> 00:03:45,80
Then, our articles list page,

61
00:03:45,80 --> 00:03:49,00
we see that it goes to the articles list page.

62
00:03:49,00 --> 00:03:51,50
And finally, the article page.

63
00:03:51,50 --> 00:03:55,90
And we see that they all display correctly.

64
00:03:55,90 --> 00:03:58,20
that our app doesn't have a page for,

65
00:03:58,20 --> 00:04:01,60
for example, 'hello', nothing shows up.

66
00:04:01,60 --> 00:04:04,80
In a future video, we'll see how to create a 404 page.

67
00:04:04,80 --> 00:04:07,90
But for now, we're just going to leave it.

68
00:04:07,90 --> 00:04:10,90
And the last we want to do, in order to make it fit

69
00:04:10,90 --> 00:04:13,40
with the CSS that I've provided,

70
00:04:13,40 --> 00:04:15,50
is we're going to wrap all of these routes

71
00:04:15,50 --> 00:04:29,00
in a div with the id 'page-body'.

72
00:04:29,00 --> 00:04:30,30
And if we go back and look at our app,

73
00:04:30,30 --> 00:04:33,00
we can see that it no longer spans the entire screen.

