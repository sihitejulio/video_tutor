1
00:00:02,30 --> 00:00:04,90
from scratch is by using a generator.

2
00:00:04,90 --> 00:00:07,70
There are several generators,

3
00:00:07,70 --> 00:00:10,60
out of getting a React site up and running.

4
00:00:10,60 --> 00:00:13,20
In this video we're going to use Create React App,

5
00:00:13,20 --> 00:00:16,00
which is an open source generator published by Facebook

6
00:00:16,00 --> 00:00:18,00
and currently one of the most popular ways

7
00:00:18,00 --> 00:00:24,30
to create a new React application.

8
00:00:24,30 --> 00:00:26,80
I'm going to use VS code's integrated terminal

9
00:00:26,80 --> 00:00:28,90
which I highly recommend.

10
00:00:28,90 --> 00:00:29,90
And then go to the directory

11
00:00:29,90 --> 00:00:32,60
you want your projects directory to be inside of.

12
00:00:32,60 --> 00:00:34,90
Now once you're there run this command,

13
00:00:34,90 --> 00:00:39,80
npx create-react-app

14
00:00:39,80 --> 00:00:42,30
and then whatever you want your project folder to be called.

15
00:00:42,30 --> 00:00:44,90
I'm just going to call it my-blog

16
00:00:44,90 --> 00:00:46,60
and finally we're going to run this command

17
00:00:46,60 --> 00:00:50,50
with the use-npm flag.

18
00:00:50,50 --> 00:00:52,00
And that's all there it to it.

19
00:00:52,00 --> 00:00:53,60
Now we just have to wait a little while

20
00:00:53,60 --> 00:00:57,30
while Create React App sets up our project for us.

21
00:00:57,30 --> 00:01:07,70
This may take a few minutes so just be patient.

22
00:01:07,70 --> 00:01:12,50
and run it by typing npm start.

23
00:01:12,50 --> 00:01:15,20
The terminal will then open our app in a browser tab

24
00:01:15,20 --> 00:01:16,10
and there we have it.

25
00:01:16,10 --> 00:01:19,10
A fully functional React app in only a few minutes.

26
00:01:19,10 --> 00:01:24,70
All we have to do now is modify the default code

27
00:01:24,70 --> 00:01:26,30
Let's take a closer at the project

28
00:01:26,30 --> 00:01:28,60
that it's created for us.

29
00:01:28,60 --> 00:01:31,70
If you haven't already, open your project in an IDE.

30
00:01:31,70 --> 00:01:33,70
Personally, I'm a fan of Visual Studio code,

31
00:01:33,70 --> 00:01:34,70
which is free to download,

32
00:01:34,70 --> 00:01:36,50
but if you have your own favorite,

33
00:01:36,50 --> 00:01:38,40
or if you're comfortable using Vim,

34
00:01:38,40 --> 00:01:41,10
feel free to use that instead.

35
00:01:41,10 --> 00:01:42,80
Once we've got our project open,

36
00:01:42,80 --> 00:01:44,40
notice that there's a source directory

37
00:01:44,40 --> 00:01:48,80
and a public directory.

38
00:01:48,80 --> 00:01:50,90
that Create React App has put there by default,

39
00:01:50,90 --> 00:01:53,40
such as the index.html file,

40
00:01:53,40 --> 00:01:56,20
which is html entry point for our app,

41
00:01:56,20 --> 00:01:58,30
as well as a manifest.json file,

42
00:01:58,30 --> 00:02:00,90
which is used with mobile phones.

43
00:02:00,90 --> 00:02:02,70
We won't go into too much detail with these,

44
00:02:02,70 --> 00:02:04,30
but there's plenty of information on the web

45
00:02:04,30 --> 00:02:06,20
if you're interested.

46
00:02:06,20 --> 00:02:09,10
The other directory that we've got is our source directory.

47
00:02:09,10 --> 00:02:12,20
This is where the bulk of our blog's code will go.

48
00:02:12,20 --> 00:02:14,30
We have our app.js component,

49
00:02:14,30 --> 00:02:16,40
which is the root component for our blog,

50
00:02:16,40 --> 00:02:19,70
as well as the css and tests that go with it.

51
00:02:19,70 --> 00:02:22,00
Those of you who are big fans of test-driven development

52
00:02:22,00 --> 00:02:23,50
will have to forgive me on this,

53
00:02:23,50 --> 00:02:25,80
but we're not going to be using tests in these videos,

54
00:02:25,80 --> 00:02:27,30
as testing React projects

55
00:02:27,30 --> 00:02:29,60
is beyond the scope of this course.

56
00:02:29,60 --> 00:02:31,20
For the time being, we don't need to worry

57
00:02:31,20 --> 00:02:33,10
about the other files in this directory.

58
00:02:33,10 --> 00:02:34,80
It's mostly just boiler plate code

59
00:02:34,80 --> 00:02:37,00
that Create React App set up for us.

