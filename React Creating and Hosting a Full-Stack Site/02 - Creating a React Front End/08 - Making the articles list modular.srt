1
00:00:15,80 --> 00:00:26,80
we just defined the list directly inside the page.

2
00:00:26,80 --> 00:00:35,70
of related articles at the bottom

3
00:00:35,70 --> 00:00:40,80
Instead of recoding what we've already coded here

4
00:00:40,80 --> 00:01:04,90
reusable articles list component.

5
00:01:04,90 --> 00:01:15,20
articles list page, and export articles list page.

6
00:01:15,20 --> 00:01:18,80
because we renamed the file, and also for consistency

7
00:01:18,80 --> 00:01:21,60
we want to rename articles list here to articles list page

8
00:01:21,60 --> 00:01:32,80
as well, and then update it when we use it here.

9
00:01:32,80 --> 00:01:44,00
So let's make sure our app is running,

10
00:01:44,00 --> 00:01:50,00
let's see how we can create a reusable articles list

11
00:01:50,00 --> 00:02:09,20
a separate directory to hold our reusable components

12
00:02:09,20 --> 00:02:15,70
called articles list dot js.

13
00:02:15,70 --> 00:02:47,00
called articles list and export it.

14
00:02:47,00 --> 00:02:52,40
And move that into our articles list, like this.

15
00:02:52,40 --> 00:03:14,40
And note that we're going to have to wrap this

16
00:03:14,40 --> 00:04:15,40
and then replace article content with that articles prop.

17
00:04:15,40 --> 00:04:17,30
And there's one thing that we have to do before we can

18
00:04:17,30 --> 00:04:51,40
get it to work, notice that we're no longer using

19
00:04:51,40 --> 00:05:23,30
component up and running, why don't we add a related

20
00:05:23,30 --> 00:05:54,80
And then we're going to put our articles list right under

21
00:05:54,80 --> 00:06:12,30
we're on, and that'll look like this.

22
00:06:12,30 --> 00:06:28,70
So we'll say article dot name does not equal name.

23
00:06:28,70 --> 00:06:33,40
So now if we navigate to any of our articles,

24
00:06:33,40 --> 00:06:39,20
at the bottom here, and the last thing we want to do

25
00:06:39,20 --> 00:06:58,00
So we're just going to have an h3 heading here,

