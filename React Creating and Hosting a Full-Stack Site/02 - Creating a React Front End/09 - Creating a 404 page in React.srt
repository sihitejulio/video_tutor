1
00:00:05,10 --> 00:00:13,90
we haven't taken care of yet.

2
00:00:13,90 --> 00:00:23,10
we'd display a message that told us so.

3
00:00:23,10 --> 00:00:30,20
we don't have any pages for, like this.

4
00:00:30,20 --> 00:00:34,10
So the first thing we're going to do

5
00:00:34,10 --> 00:00:47,00
and we'll call this file NotFoundPage.js

6
00:00:47,00 --> 00:00:53,00
and then we'll call our component NotFoundPage

7
00:00:53,00 --> 00:00:55,10
and this component's just going to display the text

8
00:00:55,10 --> 00:00:58,10
404 page not found, inside a heading tag,

9
00:00:58,10 --> 00:01:02,10
404 page not found.

10
00:01:02,10 --> 00:01:04,80
And finally we want to export our component,

11
00:01:04,80 --> 00:01:13,50
export default NotFoundPage.

12
00:01:13,50 --> 00:01:18,60
is by creating another route inside our app component.

13
00:01:18,60 --> 00:01:23,00
Route, and unlike our other routes,

14
00:01:23,00 --> 00:01:24,50
When we don't give a route a path,

15
00:01:24,50 --> 00:01:27,00
it will always match by default.

16
00:01:27,00 --> 00:01:32,30
And for our component, we want to display our not found page,

17
00:01:32,30 --> 00:01:38,00
let's import that up at the top here,

18
00:01:38,00 --> 00:01:46,50
from ./pages/NotFoundPage.

19
00:01:46,50 --> 00:01:48,90
we see that it's working correctly.

20
00:01:48,90 --> 00:01:59,20
However, if we go to any of our existing pages,

21
00:01:59,20 --> 00:02:02,30
is essentially, they display whatever component we give it,

22
00:02:02,30 --> 00:02:07,80
if the URL matches the route's path,

23
00:02:07,80 --> 00:02:31,30
And since we didn't give this route a path,

24
00:02:31,30 --> 00:02:39,20
with another react router component called Switch.

25
00:02:39,20 --> 00:02:50,80
and then we're going to wrap all of our routes

26
00:02:50,80 --> 00:02:54,40
that only one of the routes

27
00:02:54,40 --> 00:02:57,50
So it'll only render the first route that matches the URL

28
00:02:57,50 --> 00:03:01,00
and none of the others.

29
00:03:01,00 --> 00:03:03,60
that we put our not found page route last,

30
00:03:03,60 --> 00:03:05,80
otherwise that'll be the only thing that ever shows up,

31
00:03:05,80 --> 00:03:08,60
since it will always match.

32
00:03:08,60 --> 00:03:10,80
And let's make sure we add back the exact prop

33
00:03:10,80 --> 00:03:13,20
to our homepage route.

34
00:03:13,20 --> 00:03:14,40
And now if we go back to our blog,

35
00:03:14,40 --> 00:03:19,40
we can see that the 404 page is no longer

36
00:03:19,40 --> 00:03:47,50
And if we go to a route that doesn't exist,

37
00:03:47,50 --> 00:03:50,20
instead for consistency.

38
00:03:50,20 --> 00:03:56,30
And this is actually really simple to do,

39
00:03:56,30 --> 00:04:07,90
and import the not found page,

40
00:04:07,90 --> 00:04:11,40
and instead of using our hard coded message here,

41
00:04:11,40 --> 00:04:19,70
not found page component, like this.

42
00:04:19,70 --> 00:04:23,00
whenever we go to a URL that we don't have content for.

