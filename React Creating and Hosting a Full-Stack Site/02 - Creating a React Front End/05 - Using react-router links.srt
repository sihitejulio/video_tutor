1
00:00:00,50 --> 00:00:01,40
- [Instructor] So far on our app,

2
00:00:01,40 --> 00:00:03,30
we have all of our blog pages set up,

3
00:00:03,30 --> 00:00:04,90
and each page will show when we visit

4
00:00:04,90 --> 00:00:09,00
the URL that we've assigned to it

5
00:00:09,00 --> 00:00:11,30
But currently we still have to physically type

6
00:00:11,30 --> 00:00:13,10
the URLs into our browser,

7
00:00:13,10 --> 00:00:15,90
which is completely unacceptable for any website.

8
00:00:15,90 --> 00:00:19,20
What our website needs now is a nav bar.

9
00:00:19,20 --> 00:00:20,90
And instead of putting all the JSX

10
00:00:20,90 --> 00:00:23,50
for our nav bar inside our app.js file,

11
00:00:23,50 --> 00:00:26,40
let's create another component for it.

12
00:00:26,40 --> 00:00:27,50
Inside the source folder

13
00:00:27,50 --> 00:00:29,60
on the same level as app.js,

14
00:00:29,60 --> 00:00:34,00
let's create a new file called navbar.js,

15
00:00:34,00 --> 00:00:37,90
and inside it we'll create

16
00:00:37,90 --> 00:00:40,10
Const nav bar.

17
00:00:40,10 --> 00:00:44,00
Close parentheses and then our JSX will go in here.

18
00:00:44,00 --> 00:00:50,20
And then let's make sure we import React

19
00:00:50,20 --> 00:00:52,80
and also export our nav bar component.

20
00:00:52,80 --> 00:00:56,70
Export default nav bar.

21
00:00:56,70 --> 00:00:58,40
And now inside this component,

22
00:00:58,40 --> 00:01:00,90
we're going to create a very basic nav bar component

23
00:01:00,90 --> 00:01:03,00
using an unordered list.

24
00:01:03,00 --> 00:01:04,50
That'll look like this.

25
00:01:04,50 --> 00:01:06,20
We'll say nav,

26
00:01:06,20 --> 00:01:11,00
and inside these nav tags we'll put UL for unordered list

27
00:01:11,00 --> 00:01:13,80
and then we'll create each of our list items.

28
00:01:13,80 --> 00:01:15,60
And we're going to have several list items,

29
00:01:15,60 --> 00:01:19,20
each of which is a link to one of our blog pages.

30
00:01:19,20 --> 00:01:23,80
But first let's look at how to do links.

31
00:01:23,80 --> 00:01:27,00
when using react router we don't use an anchor element.

32
00:01:27,00 --> 00:01:29,20
Instead we need to import another component

33
00:01:29,20 --> 00:01:32,40
from react router dom that we haven't seen yet.

34
00:01:32,40 --> 00:01:34,60
This component is called Link.

35
00:01:34,60 --> 00:01:36,10
Let's import it up here.

36
00:01:36,10 --> 00:01:44,00
Say import, Link from react router dom.

37
00:01:44,00 --> 00:01:48,30
And the syntax for using link is like this.

38
00:01:48,30 --> 00:01:54,30
Link and then the Link tag takes a prop called to

39
00:01:54,30 --> 00:01:56,40
to link to when we click on it.

40
00:01:56,40 --> 00:01:58,70
Let's create our home page link first.

41
00:01:58,70 --> 00:02:01,90
This will go to the home route, just a slash

42
00:02:01,90 --> 00:02:05,40
and in between these tags we'll put Home.

43
00:02:05,40 --> 00:02:08,00
And then let's do this for our other pages.

44
00:02:08,00 --> 00:02:11,60
We'll do one for About, we'll change the link

45
00:02:11,60 --> 00:02:18,20
to slash About and the text to About.

46
00:02:18,20 --> 00:02:20,10
And then we'll do it to Articles

47
00:02:20,10 --> 00:02:22,70
which we'll link to our Articles list page.

48
00:02:22,70 --> 00:02:25,00
We'll change the text to Articles

49
00:02:25,00 --> 00:02:29,80
and the link to Articles dash list.

50
00:02:29,80 --> 00:02:33,80
And now what we have to do is go into our app.js file

51
00:02:33,80 --> 00:02:36,70
and import our nav bar component.

52
00:02:36,70 --> 00:02:46,00
Import, Nav bar from dot slash nav bar

53
00:02:46,00 --> 00:02:47,70
and we want to display it on all our pages

54
00:02:47,70 --> 00:02:53,00
so we'll put it up here, nav bar.

55
00:02:53,00 --> 00:03:00,30
Let's make sure our app is running.

56
00:03:00,30 --> 00:03:03,10
on any of the links, it takes us to the URL

57
00:03:03,10 --> 00:03:05,20
that we've linked it to.

58
00:03:05,20 --> 00:03:07,80
Notice that since the React router link component

59
00:03:07,80 --> 00:03:10,60
is designed specifically to be used in React,

60
00:03:10,60 --> 00:03:12,30
we'll want to use React router links

61
00:03:12,30 --> 00:03:15,40
instead of normal HTML links whenever we want to link

62
00:03:15,40 --> 00:03:19,00
to a different URL or page inside our app.

