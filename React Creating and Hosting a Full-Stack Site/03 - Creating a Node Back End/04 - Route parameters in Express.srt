1
00:00:00,50 --> 00:00:02,50
- [Speaker] So we've seen how to use post requests

2
00:00:02,50 --> 00:00:05,60
to carry extra information in the form of a JSON object,

3
00:00:05,60 --> 00:00:08,40
along with the request that we send to our server.

4
00:00:08,40 --> 00:00:10,50
This is generally the best way to add information

5
00:00:10,50 --> 00:00:11,60
to our requests,

6
00:00:11,60 --> 00:00:14,70
but there are several other ways to do so as well.

7
00:00:14,70 --> 00:00:16,80
Before we move on to implementing things like

8
00:00:16,80 --> 00:00:19,30
up-voting or comments for our blog site,

9
00:00:19,30 --> 00:00:21,10
we're going to take a look at a concept we learned

10
00:00:21,10 --> 00:00:24,20
when implementing the articles pages for our front end.

11
00:00:24,20 --> 00:00:26,40
Recall that for each of our article pages,

12
00:00:26,40 --> 00:00:28,70
we simply use the same component

13
00:00:28,70 --> 00:00:30,50
but we load it in different data

14
00:00:30,50 --> 00:00:34,10
depending on the page's name property up in the URL.

15
00:00:34,10 --> 00:00:35,90
Recall that the way we did this was by

16
00:00:35,90 --> 00:00:39,80
defining our route with this colon name property here,

17
00:00:39,80 --> 00:00:42,30
which meant that the value of this portion of the URL

18
00:00:42,30 --> 00:00:46,00
got passed to our component when it was rendered.

19
00:00:46,00 --> 00:00:48,10
And this allowed us to use the article's name

20
00:00:48,10 --> 00:00:51,20
to load the correct article.

21
00:00:51,20 --> 00:00:54,50
Using pieces of the URL in this way is called URL parameters

22
00:00:54,50 --> 00:00:57,40
or sometimes called route parameters.

23
00:00:57,40 --> 00:00:59,80
And as a matter of fact Express also allows us

24
00:00:59,80 --> 00:01:01,90
to use URL parameters.

25
00:01:01,90 --> 00:01:03,80
And this is useful in many cases,

26
00:01:03,80 --> 00:01:06,30
for example if we want to get a given user's data

27
00:01:06,30 --> 00:01:07,40
from the backend,

28
00:01:07,40 --> 00:01:09,80
we'd be able to make a get request

29
00:01:09,80 --> 00:01:13,50
to an endpoint called users slash one two three

30
00:01:13,50 --> 00:01:16,00
or whatever the user's ID is.

31
00:01:16,00 --> 00:01:18,10
Or if we wanted to load data for a given article

32
00:01:18,10 --> 00:01:20,80
from the backend we could make a get request

33
00:01:20,80 --> 00:01:26,00
to say, articles slash and then the article's name.

34
00:01:26,00 --> 00:01:29,10
For example, learn dash react.

35
00:01:29,10 --> 00:01:31,20
And in these examples, our server would parse out

36
00:01:31,20 --> 00:01:33,80
the one two three, the user's ID,

37
00:01:33,80 --> 00:01:36,40
or the learn react strings from the URL

38
00:01:36,40 --> 00:01:38,10
and provide that information to us

39
00:01:38,10 --> 00:01:40,50
inside our route callback.

40
00:01:40,50 --> 00:01:43,40
Let's take a look at exactly how this is done.

41
00:01:43,40 --> 00:01:45,60
So let's use the same simple example that used

42
00:01:45,60 --> 00:01:47,40
in the last video.

43
00:01:47,40 --> 00:01:49,70
We're simply going to return a hello message

44
00:01:49,70 --> 00:01:53,00
with the name that we passed through appended to it.

45
00:01:53,00 --> 00:01:55,30
And basically what we want to do is this,

46
00:01:55,30 --> 00:01:57,30
we want to send a get request.

47
00:01:57,30 --> 00:01:59,40
In this case we could use a post request as well,

48
00:01:59,40 --> 00:02:01,70
but we're just going to use a get here.

49
00:02:01,70 --> 00:02:03,10
And we're going to send it to an endpoint

50
00:02:03,10 --> 00:02:07,00
which is slash hello, slash a name,

51
00:02:07,00 --> 00:02:11,80
like slash Shaun or slash Bob.

52
00:02:11,80 --> 00:02:13,80
And as a response we want to get hello

53
00:02:13,80 --> 00:02:15,70
and then the name after it,

54
00:02:15,70 --> 00:02:18,40
just as we did before with request bodies.

55
00:02:18,40 --> 00:02:22,40
So to do this, let's define a new get route for our app.

56
00:02:22,40 --> 00:02:24,60
We'll do app.get.

57
00:02:24,60 --> 00:02:28,60
And for the URL, we're going to put slash hello,

58
00:02:28,60 --> 00:02:31,70
slash and then colon, and we'll use name

59
00:02:31,70 --> 00:02:34,60
just as we did with the front end.

60
00:02:34,60 --> 00:02:37,10
And then inside our callback function,

61
00:02:37,10 --> 00:02:38,60
we're going to do the same thing that we did

62
00:02:38,60 --> 00:02:40,30
with our post route here,

63
00:02:40,30 --> 00:02:43,10
except the location that we find the name property in

64
00:02:43,10 --> 00:02:45,60
is a little different.

65
00:02:45,60 --> 00:02:47,90
And now instead of saying req.body,

66
00:02:47,90 --> 00:02:49,80
the way we get access to the value of this

67
00:02:49,80 --> 00:02:54,50
name parameter here is by saying req.params

68
00:02:54,50 --> 00:02:57,50
and then whatever name we gave to that section of the URL.

69
00:02:57,50 --> 00:03:00,10
In this case it's just .name.

70
00:03:00,10 --> 00:03:05,30
And now if we restart our server.

71
00:03:05,30 --> 00:03:07,70
And if we go back to post man and send a get request

72
00:03:07,70 --> 00:03:11,90
to our slash hello slash our name,

73
00:03:11,90 --> 00:03:13,60
we see that the server successfully took

74
00:03:13,60 --> 00:03:16,40
the value out of this section of the URL

75
00:03:16,40 --> 00:03:19,00
and put it into the response using URL parameters.

