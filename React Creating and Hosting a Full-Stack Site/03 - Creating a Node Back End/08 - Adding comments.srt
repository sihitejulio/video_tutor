1
00:00:01,00 --> 00:00:05,20
- [Instructor] And that's going to look like this.

2
00:00:05,20 --> 00:00:07,00
Just as in our up vote endpoint,

3
00:00:07,00 --> 00:00:08,70
we're going to have to get the article name

4
00:00:08,70 --> 00:00:11,50
from the URL parameters.

5
00:00:11,50 --> 00:00:14,10
We'll just copy and paste this line.

6
00:00:14,10 --> 00:00:15,90
Now, it's just a matter of adding the new comment

7
00:00:15,90 --> 00:00:18,70
that's in the request body to the comments array

8
00:00:18,70 --> 00:00:23,60
of the appropriate article in our fake database.

9
00:00:23,60 --> 00:00:26,00
And that'll look like this.

10
00:00:26,00 --> 00:00:30,10
We'll say articlesInfo, articleName,

11
00:00:30,10 --> 00:00:32,10
again, this will get us the correct article info

12
00:00:32,10 --> 00:00:34,10
from our fake database,

13
00:00:34,10 --> 00:00:38,30
and then we'll say .comments.push,

14
00:00:38,30 --> 00:00:40,10
and we'll add a new object that contains

15
00:00:40,10 --> 00:00:44,10
the username and text properties from the request body.

16
00:00:44,10 --> 00:00:46,80
And, finally, we're going to send back a response.

17
00:00:46,80 --> 00:00:50,10
So we want to do res.status,

18
00:00:50,10 --> 00:00:52,00
again, we want to send a status of 200

19
00:00:52,00 --> 00:00:54,10
to say that everything went okay,

20
00:00:54,10 --> 00:00:57,40
.send, and then we want to send the appropriate article,

21
00:00:57,40 --> 00:01:02,90
which will be articlesInfo, articleName.

22
00:01:02,90 --> 00:01:04,80
Let's test this route now.

23
00:01:04,80 --> 00:01:07,20
We've already got our request set up in Postman

24
00:01:07,20 --> 00:01:14,50
with the correct endpoint and request body,

25
00:01:14,50 --> 00:01:16,10
So we can simply send this request

26
00:01:16,10 --> 00:01:24,40
and see what we get back.

27
00:01:24,40 --> 00:01:26,00
If we change our comment a little bit

28
00:01:26,00 --> 00:01:33,00
and send another request,

29
00:01:33,00 --> 00:01:38,20
we see that the updated object we get back

30
00:01:38,20 --> 00:01:40,40
Well, so far we have a server that our front end app

31
00:01:40,40 --> 00:01:41,70
will be able to query,

32
00:01:41,70 --> 00:01:43,10
although we still haven't implemented

33
00:01:43,10 --> 00:01:45,30
the front end functionality yet.

34
00:01:45,30 --> 00:01:47,40
We'll do that in a little while.

35
00:01:47,40 --> 00:01:50,40
But before we do that, there's a problem we have to solve.

36
00:01:50,40 --> 00:01:52,50
You may have noticed that because all our article data

37
00:01:52,50 --> 00:01:55,00
is stored inside our server code,

38
00:01:55,00 --> 00:01:56,60
every time our server restarts,

39
00:01:56,60 --> 00:02:00,90
all our data will be gone and reset to its original values.

40
00:02:00,90 --> 00:02:04,30
For example, if we kill our server

41
00:02:04,30 --> 00:02:09,90
and start it again,

42
00:02:09,90 --> 00:02:13,30
we can see that it got rid of the other comments.

43
00:02:13,30 --> 00:02:15,40
Now, the way we'll solve this is by using a database,

44
00:02:15,40 --> 00:02:17,00
which we'll take a look at soon.

