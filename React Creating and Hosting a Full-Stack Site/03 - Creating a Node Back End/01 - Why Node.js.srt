1
00:00:00,50 --> 00:00:01,30
- [Instructor] So now that we've got

2
00:00:01,30 --> 00:00:03,00
our client side working nicely,

3
00:00:03,00 --> 00:00:05,70
it's time to start building the back end of our blog,

4
00:00:05,70 --> 00:00:08,20
and to do this, we're going to use Node.js.

5
00:00:08,20 --> 00:00:10,80
You've probably already heard about Node.js by now,

6
00:00:10,80 --> 00:00:12,00
and it's very important to us

7
00:00:12,00 --> 00:00:14,60
because it allows us to write our back end in JavaScript

8
00:00:14,60 --> 00:00:18,40
instead of PHP or Python or Java.

9
00:00:18,40 --> 00:00:21,20
It used to be that in order to be a full-stack developer,

10
00:00:21,20 --> 00:00:22,50
you had to be fairly fluent

11
00:00:22,50 --> 00:00:25,00
in more than one programming language.

12
00:00:25,00 --> 00:00:26,10
But with Node.js,

13
00:00:26,10 --> 00:00:28,40
which has become an incredibly popular framework

14
00:00:28,40 --> 00:00:29,60
since it was released,

15
00:00:29,60 --> 00:00:32,80
our entire web application can be written in JavaScript,

16
00:00:32,80 --> 00:00:36,30
and this is great news for JavaScript developers.

17
00:00:36,30 --> 00:00:38,50
So there are many different npm's packages

18
00:00:38,50 --> 00:00:40,80
that can be used to write Node.js servers,

19
00:00:40,80 --> 00:00:42,10
and the one that we're going to use

20
00:00:42,10 --> 00:00:44,30
in these videos is called Express.

21
00:00:44,30 --> 00:00:47,40
Express is a good choice for us because it's unopinionated,

22
00:00:47,40 --> 00:00:49,00
it's very easy to get started with,

23
00:00:49,00 --> 00:00:51,80
and it's also very well documented.

24
00:00:51,80 --> 00:00:54,00
Additionally, there are a ton of third party add-ons

25
00:00:54,00 --> 00:00:56,30
that can be used to extend it's functionality,

26
00:00:56,30 --> 00:00:59,00
which as you'll see later really comes in handy.

27
00:00:59,00 --> 00:01:01,00
So without further ado, let's get started.

