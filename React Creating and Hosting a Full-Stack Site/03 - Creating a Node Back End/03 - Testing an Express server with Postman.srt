1
00:00:00,50 --> 00:00:01,40
- [Instructor] As we move forward

2
00:00:01,40 --> 00:00:02,80
with our server development,

3
00:00:02,80 --> 00:00:03,90
it's going to be very important

4
00:00:03,90 --> 00:00:06,90
for us to have an easy and reliable way to test it.

5
00:00:06,90 --> 00:00:10,10
So far just typing the URL into our browser has worked,

6
00:00:10,10 --> 00:00:12,00
but for some of the more complicated operations

7
00:00:12,00 --> 00:00:13,40
we're going to want to implement,

8
00:00:13,40 --> 00:00:15,50
this won't be quite good enough.

9
00:00:15,50 --> 00:00:17,90
What a lot of people do to test their backend,

10
00:00:17,90 --> 00:00:20,70
is develop a feature on their frontend first,

11
00:00:20,70 --> 00:00:23,10
and then use that to test the backend.

12
00:00:23,10 --> 00:00:25,50
But in practice I've found that it's generally much easier

13
00:00:25,50 --> 00:00:27,50
to develop the backend first,

14
00:00:27,50 --> 00:00:31,40
and then build out the corresponding frontend functionality.

15
00:00:31,40 --> 00:00:35,70
A tool that's used by many developers

16
00:00:35,70 --> 00:00:38,40
is a free piece of software called Postman.

17
00:00:38,40 --> 00:00:39,70
Let's download Postman now

18
00:00:39,70 --> 00:00:45,90
so that we can use it to develop our blog's backend.

19
00:00:45,90 --> 00:00:50,40
Once we've downloaded it, we're going to install it.

20
00:00:50,40 --> 00:00:53,00
Once we've downloaded and installed Postman,

21
00:00:53,00 --> 00:00:55,40
let's use it to query our backend.

22
00:00:55,40 --> 00:00:57,80
First let's make sure our server's still running,

23
00:00:57,80 --> 00:00:59,60
we see that it is,

24
00:00:59,60 --> 00:01:00,70
and then we'll use Postman

25
00:01:00,70 --> 00:01:04,00
to send a get request to our server on the hello end point.

26
00:01:04,00 --> 00:01:05,70
That'll look like this.

27
00:01:05,70 --> 00:01:07,80
We just want to type in the URL,

28
00:01:07,80 --> 00:01:15,90
and localhost:8000 where our server's running, and /hello.

29
00:01:15,90 --> 00:01:18,50
just like In the browser.

30
00:01:18,50 --> 00:01:22,90
For those of you who aren't familiar with

31
00:01:22,90 --> 00:01:24,20
all you need to know for this course

32
00:01:24,20 --> 00:01:26,00
is that get requests are usually used

33
00:01:26,00 --> 00:01:28,90
to get information from the server.

34
00:01:28,90 --> 00:01:31,90
Such as article content or a user's information.

35
00:01:31,90 --> 00:01:33,60
And post requests are usually used

36
00:01:33,60 --> 00:01:35,70
to modify something on the server,

37
00:01:35,70 --> 00:01:38,10
such as if we want to add a comment to an article,

38
00:01:38,10 --> 00:01:46,40
or change a user's username.

39
00:01:46,40 --> 00:01:49,20
While generally the only information that get requests carry

40
00:01:49,20 --> 00:01:53,60
is contained in the URL.

41
00:01:53,60 --> 00:01:55,30
you'll be able to get along very well

42
00:01:55,30 --> 00:02:05,00
without a full knowledge of network requests.

43
00:02:05,00 --> 00:02:19,60
allow us to pass extra data along to our backend.

44
00:02:19,60 --> 00:02:24,40
We're going to say app.post

45
00:02:24,40 --> 00:02:28,00
And this post route will be for the hello end point as well.

46
00:02:28,00 --> 00:02:56,90
And for now let's just have the same callback

47
00:02:56,90 --> 00:03:00,30
we see that we get the same response back that we expected.

48
00:03:00,30 --> 00:03:02,60
Now let's look at how to send some data to our server

49
00:03:02,60 --> 00:03:04,60
along with our post request.

50
00:03:04,60 --> 00:03:07,40
Postman has a little body window here,

51
00:03:07,40 --> 00:03:11,60
where we can define the request body for our post requests.

52
00:03:11,60 --> 00:03:13,40
So let's click on raw,

53
00:03:13,40 --> 00:03:15,80
and we usually want the extra data that we send along

54
00:03:15,80 --> 00:03:18,80
to be in the form of a JSON object.

55
00:03:18,80 --> 00:03:21,20
And then we can really add whatever data we want.

56
00:03:21,20 --> 00:03:25,70
For example we can create a JSON object with a name field,

57
00:03:25,70 --> 00:03:28,30
and set it equal to our name.

58
00:03:28,30 --> 00:03:29,90
So when we send this post request,

59
00:03:29,90 --> 00:03:32,00
we want the server to reply with hello

60
00:03:32,00 --> 00:03:35,00
and then our name inside the string.

61
00:03:35,00 --> 00:03:35,90
And in order to do this

62
00:03:35,90 --> 00:03:38,60
there's a few things we have to do first.

63
00:03:38,60 --> 00:03:42,20
The first thing we have to do is install another NPM module.

64
00:03:42,20 --> 00:03:44,70
This module is called body-parser,

65
00:03:44,70 --> 00:03:47,00
and it allows our server to extract the JSON data

66
00:03:47,00 --> 00:04:09,40
that we send along with our request.

67
00:04:09,40 --> 00:04:16,00
Import bodyParser from body-parser.

68
00:04:16,00 --> 00:04:20,30
And then we need to add a line,

69
00:04:20,30 --> 00:04:26,10
that says app.use bodyParser.json.

70
00:04:26,10 --> 00:04:27,30
And what this line does here

71
00:04:27,30 --> 00:04:28,90
is it parses the JSON object

72
00:04:28,90 --> 00:04:31,60
that we've included along with our post request,

73
00:04:31,60 --> 00:04:34,60
and it adds a body property to the request parameter

74
00:04:34,60 --> 00:04:37,80
of whatever the matching route is.

75
00:04:37,80 --> 00:04:39,40
So if our request body looks like

76
00:04:39,40 --> 00:04:41,70
the one that we're sending from Postman,

77
00:04:41,70 --> 00:04:43,40
with a name property,

78
00:04:43,40 --> 00:04:45,10
we can access the name property

79
00:04:45,10 --> 00:04:48,00
by doing something like this.

80
00:04:48,00 --> 00:04:49,10
First we'll change this to use

81
00:04:49,10 --> 00:05:01,60
back ticks instead of single quotes,

82
00:05:01,60 --> 00:05:04,10
If we restart out server now

83
00:05:04,10 --> 00:05:07,60
and send this post request to our hello end point,

84
00:05:07,60 --> 00:05:09,90
we can see that the server successfully gets the name

85
00:05:09,90 --> 00:05:16,00
out of the request body, and puts it into the response.

86
00:05:16,00 --> 00:05:19,70
but now that we understand the basic mechanics

87
00:05:19,70 --> 00:05:23,00
we can move on to making it do more helpful things.

