1
00:00:05,80 --> 00:00:15,40
the next common feature that we'll want to implement

2
00:00:15,40 --> 00:00:35,40
is add a new property to all the article objects

3
00:00:35,40 --> 00:00:57,80
And next, let's create another route.

4
00:00:57,80 --> 00:01:20,50
and then, of course, we'll have our callback

5
00:01:20,50 --> 00:01:27,20
that we have the raw and JSON selected here,

6
00:01:27,20 --> 00:01:35,20
for our add comment request to have two properties.

7
00:01:35,20 --> 00:01:37,70
for now we'll just add a fake value in there,

8
00:01:37,70 --> 00:01:40,70
and the second one will be the actual text of the comment,

9
00:01:40,70 --> 00:01:47,20
we'll call this property text,

10
00:01:47,20 --> 00:01:57,40
So now that we know what we want the body

11
00:01:57,40 --> 00:02:11,40
will be found in req.body, so what we need to do

12
00:02:11,40 --> 00:02:27,30
text, is equal to req.body.

13
00:02:27,30 --> 00:03:06,30
just as in our upvote endpoint, we're going to have

14
00:03:06,30 --> 00:03:29,40
And finally, we're going to send back a response,

15
00:03:29,40 --> 00:03:36,70
with the correct endpoint and request body,

16
00:03:36,70 --> 00:03:48,20
so we can simply send this request and see what we get back.

17
00:03:48,20 --> 00:03:57,10
and send another request,

18
00:03:57,10 --> 00:04:05,30
contains both comments, which is exactly what we want.

19
00:04:05,30 --> 00:04:10,50
the front-end functionality yet.

20
00:04:10,50 --> 00:04:12,50
there's a problem we have to solve.

21
00:04:12,50 --> 00:04:14,70
You may have noticed that because all our article data

22
00:04:14,70 --> 00:04:32,20
is stored inside our server code,

23
00:04:32,20 --> 00:04:35,50
we can see that it got rid of the other comments.

24
00:04:35,50 --> 00:04:39,00
Now, the way we'll solve this is by using a database,

