1
00:00:00,50 --> 00:00:01,30
- [Instructor] Now that we've seen

2
00:00:01,30 --> 00:00:04,00
some basic examples of how an express server works,

3
00:00:04,00 --> 00:00:05,20
let's make our server do something

4
00:00:05,20 --> 00:00:07,40
more relevant to our blog.

5
00:00:07,40 --> 00:00:09,70
We'll want users to be able to upvote our articles

6
00:00:09,70 --> 00:00:11,90
to help other users see which of our articles

7
00:00:11,90 --> 00:00:14,70
is the most popular or most helpful.

8
00:00:14,70 --> 00:00:15,80
And in order to do that,

9
00:00:15,80 --> 00:00:18,50
we'll need to add some code to our back end.

10
00:00:18,50 --> 00:00:19,80
Now, later on, we'll look at how

11
00:00:19,80 --> 00:00:22,10
to add an actual database to our server,

12
00:00:22,10 --> 00:00:24,60
but for now, we're just going to create a sort of fake database

13
00:00:24,60 --> 00:00:27,60
which will keep track of the upvotes for our articles.

14
00:00:27,60 --> 00:00:29,60
And, that's going to look like this.

15
00:00:29,60 --> 00:00:31,90
Our fake database will simply be a JSON object

16
00:00:31,90 --> 00:00:35,00
that we modify whenever specific routes are hit.

17
00:00:35,00 --> 00:00:36,60
And the keys of this JSON object

18
00:00:36,60 --> 00:00:37,90
will be the unique name fields

19
00:00:37,90 --> 00:00:40,00
of our articles from our front end,

20
00:00:40,00 --> 00:00:44,20
and the values will be JSON objects

21
00:00:44,20 --> 00:00:53,30
Right now, this will just be the number of upvotes.

22
00:00:53,30 --> 00:00:54,80
and we're going to define a new endpoint

23
00:00:54,80 --> 00:00:56,50
that we can send requests to in order

24
00:00:56,50 --> 00:00:59,70
to update the number of upvotes on a given article.

25
00:00:59,70 --> 00:01:01,80
Now, we're going to use POST request for this,

26
00:01:01,80 --> 00:01:05,60
so we'll do app.post, and for the endpoint path,

27
00:01:05,60 --> 00:01:07,00
we're going to use url parameters

28
00:01:07,00 --> 00:01:09,00
like we talked about beforehand,

29
00:01:09,00 --> 00:01:14,70
and we're going to do /api/articles/:name.

30
00:01:14,70 --> 00:01:17,10
This is our url parameter here,

31
00:01:17,10 --> 00:01:18,20
and we'll add upvote on the end

32
00:01:18,20 --> 00:01:21,00
just to make it more clear what this route is for.

33
00:01:21,00 --> 00:01:23,30
Now, note that the url for all of our endpoints

34
00:01:23,30 --> 00:01:25,90
here is going to start with /api.

35
00:01:25,90 --> 00:01:34,20
This will be helpful for us down the road

36
00:01:34,20 --> 00:01:36,40
So now, let's define our callback.

37
00:01:36,40 --> 00:01:40,30
It's going to have a request and response arguments,

38
00:01:40,30 --> 00:01:41,50
and the first thing we'll need

39
00:01:41,50 --> 00:01:43,90
is the name of the article we want to upvote,

40
00:01:43,90 --> 00:01:47,50
which we'll get from our url parameter,

41
00:01:47,50 --> 00:01:55,80
const articleName = req.params.name,

42
00:01:55,80 --> 00:01:57,70
and once we've got the name of the article

43
00:01:57,70 --> 00:01:59,20
that the request is upvoting,

44
00:01:59,20 --> 00:02:01,60
we can use that to increment the number of upvotes

45
00:02:01,60 --> 00:02:04,70
in our article's info object here.

46
00:02:04,70 --> 00:02:08,00
And, that's going to look like this.

47
00:02:08,00 --> 00:02:11,30
We'll say articlesInfo, and we want the key

48
00:02:11,30 --> 00:02:15,80
to be the article name, and then we want to say .upvotes,

49
00:02:15,80 --> 00:02:18,70
and we want to increment that number by one.

50
00:02:18,70 --> 00:02:21,90
And finally, just for development purposes right now,

51
00:02:21,90 --> 00:02:23,70
we're going to send a response telling the client

52
00:02:23,70 --> 00:02:26,40
how many upvotes the article has,

53
00:02:26,40 --> 00:02:30,00
and that's going to look like this: res.status.

54
00:02:30,00 --> 00:02:49,20
We're going to use res.status to say that everything went okay,

55
00:02:49,20 --> 00:02:53,00
with the key articleName.upvotes.

56
00:02:53,00 --> 00:02:54,80
This gets our upvotes property from

57
00:02:54,80 --> 00:02:59,30
the article we just updated, and then we'll say upvotes.

58
00:02:59,30 --> 00:03:08,10
So again, this will print out our article name,

59
00:03:08,10 --> 00:03:14,70
Now, if we restart our server and then go to postman,

60
00:03:14,70 --> 00:03:17,30
and then we want to send a POST request

61
00:03:17,30 --> 00:03:27,30
to http://localhost8000/api/articles,

62
00:03:27,30 --> 00:03:29,00
and let's upvote our learn react article

63
00:03:29,00 --> 00:03:31,00
by putting learn-react in the place

64
00:03:31,00 --> 00:03:35,30
of the url parameter, and finally, upvote.

65
00:03:35,30 --> 00:03:37,20
And then, if we hit send,

66
00:03:37,20 --> 00:03:49,00
we see that we got back the expected response.

67
00:03:49,00 --> 00:03:50,80
We can upvote other articles too.

68
00:03:50,80 --> 00:03:52,00
For example, if we want to upvote

69
00:03:52,00 --> 00:03:55,80
our learn note article and hit send,

70
00:03:55,80 --> 00:03:58,00
we see that it works for that as well.

