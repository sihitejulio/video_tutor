1
00:00:05,80 --> 00:00:19,10
of our front-end directory.

2
00:00:19,10 --> 00:00:20,70
to keep the front-end and backend code

3
00:00:20,70 --> 00:00:25,80
of our apps separated, especially as our codebase

4
00:00:25,80 --> 00:00:27,40
So first, lets make sure we're inside

5
00:00:27,40 --> 00:00:31,70
the my-blog backend folder.

6
00:00:31,70 --> 00:00:44,30
this is going to look like this,

7
00:00:44,30 --> 00:01:05,40
using npm install --save express,

8
00:01:05,40 --> 00:01:14,40
but before we write any code in here,

9
00:01:14,40 --> 00:01:33,70
so we'll have to make a few small changes

10
00:01:33,70 --> 00:01:35,10
is a transpiler that allows us

11
00:01:35,10 --> 00:01:56,30
to write our codebase in ES6 syntax

12
00:01:56,30 --> 00:02:01,50
and @babel/preset-env, and then press Enter

13
00:02:01,50 --> 00:02:06,70
and it will install these packages for you,

14
00:02:06,70 --> 00:02:53,80
inside our backend directory,

15
00:02:53,80 --> 00:02:58,60
Express server that just sends us a message

16
00:02:58,60 --> 00:03:01,30
The first thing we have to do is import Express,

17
00:03:01,30 --> 00:03:44,30
import express from 'express',

18
00:03:44,30 --> 00:04:01,10
is hit with a get request,

19
00:04:01,10 --> 00:04:05,10
to whoever sent the request, and for our purposes,

20
00:04:05,10 --> 00:04:07,90
the body of our callback is just going to look like this,

21
00:04:07,90 --> 00:04:11,40
we're going to say res.send, this sends a request

22
00:04:11,40 --> 00:04:32,80
back to whoever hit the endpoint,

23
00:04:32,80 --> 00:04:59,10
we'll just use this to log a message to the console

24
00:04:59,10 --> 00:05:21,20
telling us that the app is listening on port 8000.

25
00:05:21,20 --> 00:05:23,50
from our Express server as we hit the hello endpoint

26
00:05:23,50 --> 00:05:27,20
with our browser, so everything's working well so far.

27
00:05:27,20 --> 00:05:29,40
Next, we're going to see how we can actually make our app

28
00:05:29,40 --> 00:05:31,90
do some useful things, as well as a better tool

29
00:05:31,90 --> 00:05:33,00
for testing our server.

