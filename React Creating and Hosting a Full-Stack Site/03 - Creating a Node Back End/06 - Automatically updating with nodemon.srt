1
00:00:00,50 --> 00:00:01,70
- [Instructor] So you may have noticed

2
00:00:01,70 --> 00:00:04,90
that until now we've had to manually restart our server

3
00:00:04,90 --> 00:00:12,00
every time we make a change to our code

4
00:00:12,00 --> 00:00:15,30
when our files changed and automatically restart itself.

5
00:00:15,30 --> 00:00:17,70
Well, as a matter of fact, there's an MPM package

6
00:00:17,70 --> 00:00:21,10
that does this for us, and it's called nodemon.

7
00:00:21,10 --> 00:00:25,20
Let's install this package into our project

8
00:00:25,20 --> 00:00:27,90
dash dash save dev

9
00:00:27,90 --> 00:00:31,10
nodemon.

10
00:00:31,10 --> 00:00:36,00
And once that installs all you have to do is

11
00:00:36,00 --> 00:00:38,10
Now instead of running NPX Battle Node,

12
00:00:38,10 --> 00:00:40,80
we're going to write NPX

13
00:00:40,80 --> 00:00:50,00
nodemon

14
00:00:50,00 --> 00:00:51,90
And all we're doing here is telling nodemon

15
00:00:51,90 --> 00:00:54,00
to rerun whatever command we put after

16
00:00:54,00 --> 00:00:59,20
the dash dash exec flag whenever our files change.

17
00:00:59,20 --> 00:01:00,50
If we hit enter

18
00:01:00,50 --> 00:01:04,60
we can see that our server will start up.

19
00:01:04,60 --> 00:01:09,90
to our files and save it,

20
00:01:09,90 --> 00:01:12,50
And this makes our life a lot easier.

21
00:01:12,50 --> 00:01:13,80
So now if we change this and add,

22
00:01:13,80 --> 00:01:19,50
for example, and exclamation point after it

23
00:01:19,50 --> 00:01:22,00
And then if we go here and make a request,

24
00:01:22,00 --> 00:01:27,10
we can see that it automatically restarted

25
00:01:27,10 --> 00:01:31,50
Now this nodemon command might be

26
00:01:31,50 --> 00:01:33,10
we're going to make one more small change

27
00:01:33,10 --> 00:01:38,10
to our code base that'll make our lives easier.

28
00:01:38,10 --> 00:01:43,00
from our my-blog-backend folder,

29
00:01:43,00 --> 00:01:45,70
with a default test script inside it.

30
00:01:45,70 --> 00:01:47,90
Now you may or may not be familiar with this part

31
00:01:47,90 --> 00:01:50,20
of the package Json, but these scripts are

32
00:01:50,20 --> 00:01:52,70
basically aliases for a longer commands.

33
00:01:52,70 --> 00:01:54,50
And we can create our own scripts,

34
00:01:54,50 --> 00:01:56,20
and call them whatever we want.

35
00:01:56,20 --> 00:01:57,30
What we're going to do is create an

36
00:01:57,30 --> 00:02:00,30
alias for our long nodemon command.

37
00:02:00,30 --> 00:02:02,40
We're going to call our script start,

38
00:02:02,40 --> 00:02:05,10
and for our start script we just want it to run

39
00:02:05,10 --> 00:02:07,90
NPX

40
00:02:07,90 --> 00:02:09,90
dash dash exec

41
00:02:09,90 --> 00:02:12,30
NPX babel node

42
00:02:12,30 --> 00:02:16,40
src slash server dot JS.

43
00:02:16,40 --> 00:02:22,70
Now if we want run that command

44
00:02:22,70 --> 00:02:28,30
and it'll run this command for us.

45
00:02:28,30 --> 00:02:30,00
since it's a lot easier to remember.

