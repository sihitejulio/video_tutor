1
00:00:00,50 --> 00:00:02,10
- [Instructor] Let's install the tools necessary

2
00:00:02,10 --> 00:00:06,40
for this course.

3
00:00:06,40 --> 00:00:09,90
and installing a few libraries we'll be using

4
00:00:09,90 --> 00:00:12,20
If you're not already familiar with Node.js,

5
00:00:12,20 --> 00:00:16,40
all you need to know for this course is

6
00:00:16,40 --> 00:00:18,20
that allows you to run JavaScript code

7
00:00:18,20 --> 00:00:20,10
outside of the browser.

8
00:00:20,10 --> 00:00:22,50
This is really helpful

9
00:00:22,50 --> 00:00:25,70
to be loaded and executed as part of a webpage.

10
00:00:25,70 --> 00:00:28,10
In other words, you can write a single JavaScript file,

11
00:00:28,10 --> 00:00:30,20
and run it from the command line.

12
00:00:30,20 --> 00:00:33,00
If you don't already have Node.js installed,

13
00:00:33,00 --> 00:00:37,40
you can download it from Node.js' website, nodejs.org.

14
00:00:37,40 --> 00:00:41,50
We're going to click on this button here, and download it.

15
00:00:41,50 --> 00:00:45,30
And once it downloads, we're going to click on it to open it,

16
00:00:45,30 --> 00:00:47,90
through the steps required for installation.

17
00:00:47,90 --> 00:00:50,80
Note that if you're using Windows or a Unix distribution,

18
00:00:50,80 --> 00:00:52,40
the steps might be a little different,

19
00:00:52,40 --> 00:00:55,40
but Node.js has instructions for all of these as well.

20
00:00:55,40 --> 00:00:58,30
And once you have Node.js installed,

21
00:00:58,30 --> 00:01:01,50
that you have the most recent version of npm installed.

22
00:01:01,50 --> 00:01:03,40
We'll be using a few libraries in this course,

23
00:01:03,40 --> 00:01:06,10
and npm is a package manager for Node.js

24
00:01:06,10 --> 00:01:09,00
that allows us to easily install them.

25
00:01:09,00 --> 00:01:11,60
To install the most recent version of npm,

26
00:01:11,60 --> 00:01:13,30
just open your terminal,

27
00:01:13,30 --> 00:01:20,40
and type npm install -g npm@latest,

28
00:01:20,40 --> 00:01:28,20
and you might need to run this with sudo as well.

29
00:01:28,20 --> 00:01:31,10
After that, type npm -v,

30
00:01:31,10 --> 00:01:33,40
and it should show the latest version.

31
00:01:33,40 --> 00:01:34,70
If the npm and Node versions

32
00:01:34,70 --> 00:01:36,40
on your computer are higher than mine,

33
00:01:36,40 --> 00:01:39,40
don't worry about it, everything should still run correctly.

34
00:01:39,40 --> 00:01:40,70
But I do highly recommend having

35
00:01:40,70 --> 00:01:44,60
at least these version numbers here,

36
00:01:44,60 --> 00:01:48,00
Node.js and npm are now installed on your computer.

