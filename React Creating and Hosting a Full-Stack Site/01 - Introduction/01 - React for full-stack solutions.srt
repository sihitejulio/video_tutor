1
00:00:00,50 --> 00:00:01,50
- [Shaun] It's an exciting time

2
00:00:01,50 --> 00:00:03,20
to be a JavaScript developer.

3
00:00:03,20 --> 00:00:04,90
Where companies used to need separate teams

4
00:00:04,90 --> 00:00:07,50
to work on their front end, back end, and databases,

5
00:00:07,50 --> 00:00:09,90
since all of those required experience with different types

6
00:00:09,90 --> 00:00:12,40
of programming, JavaScript can now be used

7
00:00:12,40 --> 00:00:15,50
to develop on all of these platforms, meaning that one team

8
00:00:15,50 --> 00:00:18,00
of JavaScript developers can now create something

9
00:00:18,00 --> 00:00:20,40
that used to take many teams.

10
00:00:20,40 --> 00:00:22,80
The rise of front-end frameworks, such as React,

11
00:00:22,80 --> 00:00:25,70
and servers such as node js, mean that JavaScript

12
00:00:25,70 --> 00:00:27,30
has become something of a common language

13
00:00:27,30 --> 00:00:29,40
in the development world.

14
00:00:29,40 --> 00:00:31,90
In this course, I'll show you how to create and deploy

15
00:00:31,90 --> 00:00:35,70
a full-stack React-powered website from scratch.

16
00:00:35,70 --> 00:00:37,70
We'll start off by using React to create

17
00:00:37,70 --> 00:00:39,90
an interactive front end for our site,

18
00:00:39,90 --> 00:00:42,20
and then we'll move on to writing a node js server,

19
00:00:42,20 --> 00:00:45,80
tied in with MongoDB, that will support our front end.

20
00:00:45,80 --> 00:00:47,80
We'll then close out by seeing how you can host

21
00:00:47,80 --> 00:00:50,30
your full-stack site on Amazon Web Services,

22
00:00:50,30 --> 00:00:52,40
and access it anywhere.

23
00:00:52,40 --> 00:00:55,50
Hi, I'm Shaun Wassell, and I'm a senior JavaScript

24
00:00:55,50 --> 00:00:57,10
and React developer.

25
00:00:57,10 --> 00:00:59,50
Join me in my LinkedIn learning course to learn how to build

26
00:00:59,50 --> 00:01:03,00
a full-stack React-powered blog site from scratch.

