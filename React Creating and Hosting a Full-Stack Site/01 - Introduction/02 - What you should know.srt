1
00:00:00,50 --> 00:00:02,50
- [Instructor] In order to get the most out of this course,

2
00:00:02,50 --> 00:00:05,40
there are a few basic things that you should know.

3
00:00:05,40 --> 00:00:07,30
The first is basic JavaScript,

4
00:00:07,30 --> 00:00:11,70
and preferably ES6, the most modern syntax.

5
00:00:11,70 --> 00:00:14,50
The second is basic React syntax.

6
00:00:14,50 --> 00:00:16,70
If you're not already familiar with React syntax

7
00:00:16,70 --> 00:00:19,20
such as JSX, it's not hard to learn,

8
00:00:19,20 --> 00:00:22,90
especially if you already have experience with HTML.

9
00:00:22,90 --> 00:00:24,20
It would also be helpful for you

10
00:00:24,20 --> 00:00:27,30
to have some experience with Node.js and npm,

11
00:00:27,30 --> 00:00:29,70
especially installing npm packages

12
00:00:29,70 --> 00:00:32,60
since we'll be doing a lot of that in this course.

13
00:00:32,60 --> 00:00:34,50
In order to complete this course,

14
00:00:34,50 --> 00:00:36,60
you'll also need a GitHub account

15
00:00:36,60 --> 00:00:39,40
which you can open for free on github.com,

16
00:00:39,40 --> 00:00:41,40
and an AWS Free Tier account

17
00:00:41,40 --> 00:00:44,00
which will be using to host our website.

18
00:00:44,00 --> 00:00:46,10
I also highly recommend that you follow along with me

19
00:00:46,10 --> 00:00:49,20
as I write code, but I've also included the finished state

20
00:00:49,20 --> 00:00:51,10
for all the code that I write in this course

21
00:00:51,10 --> 00:00:53,80
in the exercise files for your convenience.

22
00:00:53,80 --> 00:00:56,00
Keep in mind that if you follow along with me,

23
00:00:56,00 --> 00:00:59,30
you'll only need to install the npm packages when I say so.

24
00:00:59,30 --> 00:01:01,60
But if you skip ahead in the exercise files,

25
00:01:01,60 --> 00:01:03,60
you'll need to run npm install inside

26
00:01:03,60 --> 00:01:06,00
both your front and back end folders.

