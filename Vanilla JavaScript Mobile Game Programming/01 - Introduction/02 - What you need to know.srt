1
00:00:00,50 --> 00:00:01,40
- [Instructor] For this course,

2
00:00:01,40 --> 00:00:03,80
you should have a basic knowledge of web development,

3
00:00:03,80 --> 00:00:06,70
including HTML and CSS.

4
00:00:06,70 --> 00:00:08,50
We'll be creating a mobile game

5
00:00:08,50 --> 00:00:10,60
using plain vanilla JavaScript

6
00:00:10,60 --> 00:00:13,20
so I'll assume you know JavaScript syntax

7
00:00:13,20 --> 00:00:16,50
and have some basic programming experience writing functions

8
00:00:16,50 --> 00:00:18,00
and event handlers.

9
00:00:18,00 --> 00:00:21,20
But since we're using plain vanilla JavaScript,

10
00:00:21,20 --> 00:00:23,50
what you won't need is any experience

11
00:00:23,50 --> 00:00:25,90
with JavaScript game frameworks.

12
00:00:25,90 --> 00:00:28,70
Experience with frameworks certainly won't hurt,

13
00:00:28,70 --> 00:00:30,00
but you'll be pleasantly surprised

14
00:00:30,00 --> 00:00:32,20
with what you can accomplish without them.

15
00:00:32,20 --> 00:00:33,70
And the result will be a lightweight

16
00:00:33,70 --> 00:00:36,00
and fast gaming experience.

