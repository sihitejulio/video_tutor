1
00:00:00,10 --> 00:00:01,70
- [Tom] We're web developers.

2
00:00:01,70 --> 00:00:03,90
And we all want to write better JavaScript code.

3
00:00:03,90 --> 00:00:05,80
Wouldn't it be cool if we could learn

4
00:00:05,80 --> 00:00:07,00
how to optimize our JavaScript,

5
00:00:07,00 --> 00:00:08,90
and do it in a fun way?

6
00:00:08,90 --> 00:00:12,00
I'm going to show you how to optimize your JavaScript,

7
00:00:12,00 --> 00:00:14,20
while creating a mobile game that runs

8
00:00:14,20 --> 00:00:16,80
on nearly any device you can throw at it.

9
00:00:16,80 --> 00:00:17,80
I'm Tom Duffy.

10
00:00:17,80 --> 00:00:19,70
And I've been teaching web programming

11
00:00:19,70 --> 00:00:22,50
and creating websites for more than 20 years.

12
00:00:22,50 --> 00:00:24,20
I'll be your guide as we explore

13
00:00:24,20 --> 00:00:26,30
how to write better JavaScript code

14
00:00:26,30 --> 00:00:28,10
through game programming.

15
00:00:28,10 --> 00:00:30,10
If you're ready to take your JavaScript skills

16
00:00:30,10 --> 00:00:31,40
to the next level,

17
00:00:31,40 --> 00:00:34,50
and you'd like to do so in a fun, engaging way,

18
00:00:34,50 --> 00:00:36,90
join me for Vanilla JavaScript Game Programming

19
00:00:36,90 --> 00:00:38,40
on LinkedIn Learning.

20
00:00:38,40 --> 00:00:39,40
Ready?

21
00:00:39,40 --> 00:00:41,00
Let's roll.

