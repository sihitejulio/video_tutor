1
00:00:00,50 --> 00:00:02,60
- [Instructor] Now that we have a settings panel,

2
00:00:02,60 --> 00:00:05,60
we'll need to implement the behavior to make it work.

3
00:00:05,60 --> 00:00:09,20
We'll need to get references to each of the HTML dom objects

4
00:00:09,20 --> 00:00:11,50
and assign them to instance variables,

5
00:00:11,50 --> 00:00:14,40
and eventually, we'll need to add event list in there

6
00:00:14,40 --> 00:00:16,90
so we can handle the appropriate events.

7
00:00:16,90 --> 00:00:18,60
OK, let's start by adding variables

8
00:00:18,60 --> 00:00:22,40
for each of the HTML objects on this settings screen.

9
00:00:22,40 --> 00:00:24,30
I'm going to do that at the top of the script

10
00:00:24,30 --> 00:00:26,90
with all the other HTML elements.

11
00:00:26,90 --> 00:00:31,80
So, let's do a variable for the gear.

12
00:00:31,80 --> 00:00:36,20
We'll do a variable for the controls div,

13
00:00:36,20 --> 00:00:40,70
a variable for the new button,

14
00:00:40,70 --> 00:00:45,40
a variable for the difficulty select,

15
00:00:45,40 --> 00:00:50,10
and a variable for the done button.

16
00:00:50,10 --> 00:00:51,60
Now that we have these variables,

17
00:00:51,60 --> 00:00:54,90
we need to initialize them in the init function.

18
00:00:54,90 --> 00:00:57,50
So, scroll down to the init function,

19
00:00:57,50 --> 00:01:00,10
and then, before the call to layout page,

20
00:01:00,10 --> 00:01:04,10
we need to initialize each of the variables we just created.

21
00:01:04,10 --> 00:01:14,10
So, the gear is document.getElementById,

22
00:01:14,10 --> 00:01:18,10
and we'll be looking at the gear ID,

23
00:01:18,10 --> 00:01:25,10
and we'll do the same thing for controls,

24
00:01:25,10 --> 00:01:32,30
getElementById, and this will point to the controls div.

25
00:01:32,30 --> 00:01:40,90
New button, document.getElementById, and that will point

26
00:01:40,90 --> 00:01:44,30
to the new button at HTML.

27
00:01:44,30 --> 00:01:56,10
Difficulty select, document.getElementById,

28
00:01:56,10 --> 00:02:00,60
and it will point to the difficulty select,

29
00:02:00,60 --> 00:02:08,30
and the last one is the done button,

30
00:02:08,30 --> 00:02:11,30
document.getElementById,

31
00:02:11,30 --> 00:02:15,40
and it will look for the done HTML ID.

32
00:02:15,40 --> 00:02:19,00
That should do it for initializing the EHTML objects.

