1
00:00:00,50 --> 00:00:01,80
- [Narrator] The only thing left to do is to

2
00:00:01,80 --> 00:00:03,60
handle starting a new game.

3
00:00:03,60 --> 00:00:06,30
I have good news, we've already done all the work

4
00:00:06,30 --> 00:00:07,70
to start a new game.

5
00:00:07,70 --> 00:00:09,70
All we'll need to do is reset the game

6
00:00:09,70 --> 00:00:11,40
to its initial conditions.

7
00:00:11,40 --> 00:00:16,40
So lets add the new game function.

8
00:00:16,40 --> 00:00:20,00
Okay lets reset the value of balltop

9
00:00:20,00 --> 00:00:24,90
back to its initial state, which was 8.

10
00:00:24,90 --> 00:00:29,20
Then we can reset the score back to 0.

11
00:00:29,20 --> 00:00:32,70
I'll reset dx back to to 2.

12
00:00:32,70 --> 00:00:34,40
And that will set the difficulty by

13
00:00:34,40 --> 00:00:37,80
grabbing the selected option from the dropdown.

14
00:00:37,80 --> 00:00:41,30
So that's set difficulty.

15
00:00:41,30 --> 00:00:45,30
And to that will pass in difficulty select

16
00:00:45,30 --> 00:00:49,20
dot selected index.

17
00:00:49,20 --> 00:00:53,20
I'll change the background color of the score label to green

18
00:00:53,20 --> 00:00:55,60
if its already red.

19
00:00:55,60 --> 00:00:58,00
So score dot,

20
00:00:58,00 --> 00:01:00,50
style dot,

21
00:01:00,50 --> 00:01:03,00
background color.

22
00:01:03,00 --> 00:01:06,10
Well set that back equal to

23
00:01:06,10 --> 00:01:07,40
rgb

24
00:01:07,40 --> 00:01:11,30
and then the values were 32

25
00:01:11,30 --> 00:01:13,20
128

26
00:01:13,20 --> 00:01:15,30
64

27
00:01:15,30 --> 00:01:19,80
And then finally, well call hide settings.

28
00:01:19,80 --> 00:01:24,30
This has the added benefit of restarting the game loop.

29
00:01:24,30 --> 00:01:27,80
So lets go save that and see it in action.

30
00:01:27,80 --> 00:01:29,60
There's the setting screen.

31
00:01:29,60 --> 00:01:33,50
Notice that the animation has paused.

32
00:01:33,50 --> 00:01:38,50
We can click new game and that starts the game over again.

33
00:01:38,50 --> 00:01:43,40
When we click done, the setting screen is hidden.

34
00:01:43,40 --> 00:01:47,00
Lets try modifying the difficulty, I'll set it to hard.

35
00:01:47,00 --> 00:01:48,30
And then click done.

36
00:01:48,30 --> 00:01:55,90
And you can see how fast the ball moves.

37
00:01:55,90 --> 00:01:57,20
Try a new game.

38
00:01:57,20 --> 00:02:01,70
Set it back to easy.

39
00:02:01,70 --> 00:02:06,30
Notice the score.

40
00:02:06,30 --> 00:02:08,00
Was reset to zero.

41
00:02:08,00 --> 00:02:10,50
And then finally lets try a new game one more time

42
00:02:10,50 --> 00:02:14,70
with the score label having a background color of red.

43
00:02:14,70 --> 00:02:19,00
And when I do that you'll see it switches back to green.

