1
00:00:00,50 --> 00:00:02,00
- [Instructor] Let's add event listeners

2
00:00:02,00 --> 00:00:03,70
for each of the objects the player could

3
00:00:03,70 --> 00:00:06,50
interact with on the control screen.

4
00:00:06,50 --> 00:00:08,40
So I'm going to do that

5
00:00:08,40 --> 00:00:11,80
just beneath the mouse and touch events,

6
00:00:11,80 --> 00:00:14,00
and so let's listen for

7
00:00:14,00 --> 00:00:16,80
the click event on the gear object,

8
00:00:16,80 --> 00:00:22,40
so gear dot add event listener,

9
00:00:22,40 --> 00:00:25,70
and we want to listen for the click event,

10
00:00:25,70 --> 00:00:31,80
and, we'll call a show settings function,

11
00:00:31,80 --> 00:00:34,50
which we'll write momentarily.

12
00:00:34,50 --> 00:00:38,00
And the third optional argument will be false,

13
00:00:38,00 --> 00:00:41,50
we need a event handle for the new button,

14
00:00:41,50 --> 00:00:45,60
so we'll add an event listener again,

15
00:00:45,60 --> 00:00:48,40
we'll listen for the click event,

16
00:00:48,40 --> 00:00:49,80
and we'll write a function called

17
00:00:49,80 --> 00:00:52,40
New Game in a little while,

18
00:00:52,40 --> 00:00:55,10
and again false for the third parameter.

19
00:00:55,10 --> 00:00:57,60
Let's do the done button.

20
00:00:57,60 --> 00:01:02,20
So, done button dot add event listener,

21
00:01:02,20 --> 00:01:07,20
again, click event, and we'll write a function called

22
00:01:07,20 --> 00:01:11,40
hide settings, and false.

23
00:01:11,40 --> 00:01:13,60
Since we'll need to grab the item selected

24
00:01:13,60 --> 00:01:15,90
in the drop-down to set the difficulty,

25
00:01:15,90 --> 00:01:17,90
we'll need to wrap the handler

26
00:01:17,90 --> 00:01:20,40
in an anonymous function call.

27
00:01:20,40 --> 00:01:24,50
So, for difficulty select,

28
00:01:24,50 --> 00:01:27,20
we'll add an event listener,

29
00:01:27,20 --> 00:01:30,60
and this time we'll listen for the change event,

30
00:01:30,60 --> 00:01:34,80
and we need an anonymous function so that we can

31
00:01:34,80 --> 00:01:40,20
pass parameters to the function we'd like to write.

32
00:01:40,20 --> 00:01:45,40
So, start with the function, and then parentheses,

33
00:01:45,40 --> 00:01:48,50
and then, open a curly bracket,

34
00:01:48,50 --> 00:01:50,00
and we'll write a function called,

35
00:01:50,00 --> 00:01:55,90
set difficulty, and to that function, we're going to pass in

36
00:01:55,90 --> 00:02:00,40
difficulty select dot

37
00:02:00,40 --> 00:02:02,10
selected index.

38
00:02:02,10 --> 00:02:05,80
And what that does is it passes in a zero for easy,

39
00:02:05,80 --> 00:02:10,00
a one for medium, and a two for hard.

40
00:02:10,00 --> 00:02:12,90
Okay, and then as a third parameter,

41
00:02:12,90 --> 00:02:15,70
we'll need to pass in false,

42
00:02:15,70 --> 00:02:20,00
and then a semi-colon on the end.

43
00:02:20,00 --> 00:02:22,80
Okay, I'm going to scroll down to the bottom of the file,

44
00:02:22,80 --> 00:02:26,80
and let's write the show settings function.

45
00:02:26,80 --> 00:02:29,00
So this allows the player to touch the gear

46
00:02:29,00 --> 00:02:30,80
to view the settings panel.

47
00:02:30,80 --> 00:02:35,80
So add a function called show settings,

48
00:02:35,80 --> 00:02:37,70
and so what I'll do here is I'll just set

49
00:02:37,70 --> 00:02:43,90
the controls div, set the style objects display property,

50
00:02:43,90 --> 00:02:47,90
equal to block.

51
00:02:47,90 --> 00:02:51,50
And then while we're here, we should also pause the game,

52
00:02:51,50 --> 00:02:54,20
while the settings panel is visible.

53
00:02:54,20 --> 00:02:59,30
So, let's call cancel animation frame,

54
00:02:59,30 --> 00:03:01,90
and we'll pass in our timer.

55
00:03:01,90 --> 00:03:06,60
So while the settings screen is visible, the game is paused.

56
00:03:06,60 --> 00:03:12,30
And then let's add our hide settings function.

57
00:03:12,30 --> 00:03:13,50
And this one will be sort of

58
00:03:13,50 --> 00:03:16,10
the direct opposite of show settings.

59
00:03:16,10 --> 00:03:18,70
Instead of setting the display properties of the

60
00:03:18,70 --> 00:03:21,90
controls div to block, we set it back to none.

61
00:03:21,90 --> 00:03:28,10
So that's controls dot style dot display,

62
00:03:28,10 --> 00:03:32,50
set that back equal to none,

63
00:03:32,50 --> 00:03:36,30
and then, now that the controls panel is no longer visible,

64
00:03:36,30 --> 00:03:38,20
we need to start the game up again,

65
00:03:38,20 --> 00:03:40,60
so that's done by setting a timer

66
00:03:40,60 --> 00:03:45,70
back equal to request animation frame,

67
00:03:45,70 --> 00:03:49,80
and then we pass in a point or two our game loop function,

68
00:03:49,80 --> 00:03:51,80
which is start.

69
00:03:51,80 --> 00:03:54,00
Save that, and let's move on.

