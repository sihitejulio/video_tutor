1
00:00:00,50 --> 00:00:01,50
- [Instructor] Let's handle setting

2
00:00:01,50 --> 00:00:03,60
the difficulty of the game next.

3
00:00:03,60 --> 00:00:04,70
Here's the thing.

4
00:00:04,70 --> 00:00:07,80
Making it harder is somewhat subjective.

5
00:00:07,80 --> 00:00:10,10
In our case, we have a few things that we can do

6
00:00:10,10 --> 00:00:12,00
to make the game more difficult.

7
00:00:12,00 --> 00:00:13,70
We could speed up the ball.

8
00:00:13,70 --> 00:00:16,30
We could slow down the paddle for keyboard players.

9
00:00:16,30 --> 00:00:18,20
We can make the ball smaller.

10
00:00:18,20 --> 00:00:20,30
We can make the paddle smaller

11
00:00:20,30 --> 00:00:23,30
and we can combine two or more of those.

12
00:00:23,30 --> 00:00:27,40
For now, let's speed up the ball and slow down the paddle.

13
00:00:27,40 --> 00:00:29,50
I'll leave the other strategies up to you

14
00:00:29,50 --> 00:00:32,00
if you want to implement them.

15
00:00:32,00 --> 00:00:33,70
At the bottom of the script,

16
00:00:33,70 --> 00:00:38,20
let's add the setDifficulty function.

17
00:00:38,20 --> 00:00:44,50
That one will take a parameter which I'll call diff.

18
00:00:44,50 --> 00:00:46,80
Right now, the only values the parameter can take

19
00:00:46,80 --> 00:00:49,10
are zero, one and two

20
00:00:49,10 --> 00:00:53,40
because there are only three choices in the dropdown

21
00:00:53,40 --> 00:00:55,00
so let's write a switch statement

22
00:00:55,00 --> 00:00:57,30
to handle those three cases.

23
00:00:57,30 --> 00:01:03,40
So we'll switch on the diff parameter

24
00:01:03,40 --> 00:01:07,40
and for the case that's zero,

25
00:01:07,40 --> 00:01:10,50
let's set dy equal to two,

26
00:01:10,50 --> 00:01:12,10
that's the default setting

27
00:01:12,10 --> 00:01:14,90
and we'll set pdx

28
00:01:14,90 --> 00:01:16,50
equal to 48.

29
00:01:16,50 --> 00:01:18,60
Those are the original settings of the game

30
00:01:18,60 --> 00:01:21,80
and then we'll add a break statement

31
00:01:21,80 --> 00:01:25,70
to prevent the cascade to the next case.

32
00:01:25,70 --> 00:01:30,70
Okay, for case one, that's the medium case,

33
00:01:30,70 --> 00:01:33,40
to make the game harder, we would speed up

34
00:01:33,40 --> 00:01:36,10
whatever the dy parameter is.

35
00:01:36,10 --> 00:01:38,80
So, let's set dy

36
00:01:38,80 --> 00:01:41,70
equal to four here

37
00:01:41,70 --> 00:01:44,50
and then to slow the paddle down,

38
00:01:44,50 --> 00:01:47,60
we would decrease the value of pdx.

39
00:01:47,60 --> 00:01:49,80
So let's set pdx

40
00:01:49,80 --> 00:01:52,80
equal to 32

41
00:01:52,80 --> 00:01:55,10
and then we'll add our break statement

42
00:01:55,10 --> 00:01:57,30
to prevent the cascade.

43
00:01:57,30 --> 00:02:02,20
And for the difficult case, that's case two,

44
00:02:02,20 --> 00:02:04,10
let's set dy

45
00:02:04,10 --> 00:02:06,40
equal to six,

46
00:02:06,40 --> 00:02:11,30
pdx equal to 16

47
00:02:11,30 --> 00:02:13,30
and then we'll add our break

48
00:02:13,30 --> 00:02:15,30
and then of course, with all switch statements,

49
00:02:15,30 --> 00:02:18,90
it's good form to add a default case

50
00:02:18,90 --> 00:02:21,40
where I'll go back to the original settings.

51
00:02:21,40 --> 00:02:25,60
So dy equals two and pdx

52
00:02:25,60 --> 00:02:29,70
equals 48.

53
00:02:29,70 --> 00:02:32,10
If you want to implement further changes,

54
00:02:32,10 --> 00:02:35,50
you would do so in case blocks for each value

55
00:02:35,50 --> 00:02:38,00
and then you could also add more cases

56
00:02:38,00 --> 00:02:42,00
by increasing the number of options in the HTML element.

