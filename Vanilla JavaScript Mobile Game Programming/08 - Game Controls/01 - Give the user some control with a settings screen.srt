1
00:00:00,50 --> 00:00:02,20
- [Instructor] Let's give the user more ability

2
00:00:02,20 --> 00:00:05,50
to control the game by adding a settings screen.

3
00:00:05,50 --> 00:00:08,70
To do that, we'll add the HTML elements for the screen

4
00:00:08,70 --> 00:00:10,70
to the HTML file.

5
00:00:10,70 --> 00:00:12,70
Let's keep it simple and offer the user

6
00:00:12,70 --> 00:00:16,20
to start a new game and change the difficulty.

7
00:00:16,20 --> 00:00:18,90
So I'm going to jump down beneath the playingArea

8
00:00:18,90 --> 00:00:25,20
and a div for controls.

9
00:00:25,20 --> 00:00:29,50
And then inside that div, let's add each h3

10
00:00:29,50 --> 00:00:33,50
as a header for Settings.

11
00:00:33,50 --> 00:00:37,10
And then beneath that, let's add a button

12
00:00:37,10 --> 00:00:41,40
and I'll give that an id of new.

13
00:00:41,40 --> 00:00:46,60
And the button will allow the user to start a new game.

14
00:00:46,60 --> 00:00:49,80
How about a line break?

15
00:00:49,80 --> 00:00:52,40
And then beneath that we'll write our select.

16
00:00:52,40 --> 00:00:55,00
So we're going to allow the user to select

17
00:00:55,00 --> 00:00:58,90
easy, medium or hard as a difficulty level.

18
00:00:58,90 --> 00:01:05,10
So add the select element, set the size equal to one,

19
00:01:05,10 --> 00:01:06,40
and then add an id.

20
00:01:06,40 --> 00:01:10,00
I'm going to call this difficulty.

21
00:01:10,00 --> 00:01:13,60
And then inside the select element, we need three options.

22
00:01:13,60 --> 00:01:18,40
The first one will be for Easy.

23
00:01:18,40 --> 00:01:22,50
Then we need an option for Medium.

24
00:01:22,50 --> 00:01:28,60
And then finally an option for Hard.

25
00:01:28,60 --> 00:01:32,70
Okay, then jump beneath the select element

26
00:01:32,70 --> 00:01:35,40
and add a button.

27
00:01:35,40 --> 00:01:39,80
We'll set the id equal to done for this,

28
00:01:39,80 --> 00:01:43,00
and then this will be the button that dismisses

29
00:01:43,00 --> 00:01:44,70
the Settings screen.

30
00:01:44,70 --> 00:01:47,80
And before I forget, let's add a line break

31
00:01:47,80 --> 00:01:52,30
between the select and the done button.

32
00:01:52,30 --> 00:01:54,80
Okay, so I'll leave the controls visible

33
00:01:54,80 --> 00:01:57,80
while I style it but eventually we want the Settings screen

34
00:01:57,80 --> 00:01:59,90
to be hidden and to provide a button

35
00:01:59,90 --> 00:02:03,20
the user can touch to show the Settings screen.

36
00:02:03,20 --> 00:02:07,10
So let's add directly beneath the controls div,

37
00:02:07,10 --> 00:02:09,80
let's add a button to do that.

38
00:02:09,80 --> 00:02:11,80
So I'll add a button.

39
00:02:11,80 --> 00:02:15,60
I'm going to set the id equal to gear.

40
00:02:15,60 --> 00:02:19,60
And then I'll use the entity that represents a gear icon.

41
00:02:19,60 --> 00:02:25,70
So that's ampersand and then hashtag 9881

42
00:02:25,70 --> 00:02:28,70
as the content between the button elements.

43
00:02:28,70 --> 00:02:30,60
So let's save that.

44
00:02:30,60 --> 00:02:33,30
I've added the style rules to the CSS file

45
00:02:33,30 --> 00:02:36,10
for the elements we just added for you.

46
00:02:36,10 --> 00:02:38,80
You can download the updated CSS file

47
00:02:38,80 --> 00:02:41,00
from the exercise files.

48
00:02:41,00 --> 00:02:44,70
Looking at the CSS file, you'll see there are rules

49
00:02:44,70 --> 00:02:49,50
for the controls div, for the h3 inside the controls,

50
00:02:49,50 --> 00:02:52,70
for the button and the select inside the controls

51
00:02:52,70 --> 00:02:54,60
and for the gear.

52
00:02:54,60 --> 00:02:57,90
When we look at the file at run time,

53
00:02:57,90 --> 00:02:59,50
that's the Settings screen.

54
00:02:59,50 --> 00:03:02,70
And you can see the gear that we'll use to fire

55
00:03:02,70 --> 00:03:06,70
showing and hiding the Settings screen.

56
00:03:06,70 --> 00:03:08,50
Okay, once we're happy with the way

57
00:03:08,50 --> 00:03:10,10
the Settings screen looks though,

58
00:03:10,10 --> 00:03:12,30
we don't want it visible at run time,

59
00:03:12,30 --> 00:03:19,00
so we need to go back and add a rule to the controls style.

60
00:03:19,00 --> 00:03:23,40
And we'll set the display to none.

61
00:03:23,40 --> 00:03:26,40
And setting display to none tells a browser

62
00:03:26,40 --> 00:03:29,50
it does not have to set aside any space for the div.

63
00:03:29,50 --> 00:03:33,00
So don't be tempted to use visibility hidden here.

