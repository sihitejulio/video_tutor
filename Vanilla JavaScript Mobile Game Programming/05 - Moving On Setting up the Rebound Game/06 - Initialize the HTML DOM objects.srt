1
00:00:00,50 --> 00:00:02,60
- [Instructor] Let's add the init function to our script,

2
00:00:02,60 --> 00:00:05,10
to initialize the DOM objects.

3
00:00:05,10 --> 00:00:08,60
So let's add a function, called Init.

4
00:00:08,60 --> 00:00:11,20
And let's initialize those DOM objects.

5
00:00:11,20 --> 00:00:17,90
So I'm going to write ball = document. getElementById.

6
00:00:17,90 --> 00:00:19,50
And we'll point to the ball.

7
00:00:19,50 --> 00:00:21,60
Do the same thing for the paddle.

8
00:00:21,60 --> 00:00:27,30
Paddle = document.getElementById.

9
00:00:27,30 --> 00:00:30,10
Point to the paddle HTML object.

10
00:00:30,10 --> 00:00:36,60
Another for the score, .getElementById.

11
00:00:36,60 --> 00:00:38,60
Score variable.

12
00:00:38,60 --> 00:00:41,50
And then the playing area.

13
00:00:41,50 --> 00:00:44,30
The getElementById method of the document object

14
00:00:44,30 --> 00:00:46,30
is relatively expensive.

15
00:00:46,30 --> 00:00:48,40
Meaning, that improper use of it

16
00:00:48,40 --> 00:00:51,10
can slow your pages way down.

17
00:00:51,10 --> 00:00:52,80
Since we're creating a game,

18
00:00:52,80 --> 00:00:56,30
we'll need that processing power to do other, cooler stuff

19
00:00:56,30 --> 00:01:00,00
besides traversing the DOM, looking for matching nodes.

20
00:01:00,00 --> 00:01:03,30
Whenever you can minimize calls to getElementById,

21
00:01:03,30 --> 00:01:04,50
you should.

22
00:01:04,50 --> 00:01:07,60
The code we've written does the lookup once per object,

23
00:01:07,60 --> 00:01:10,20
and then stores the object in a variable.

24
00:01:10,20 --> 00:01:12,10
Now we can use a variable

25
00:01:12,10 --> 00:01:14,70
every time we need to manipulate the object,

26
00:01:14,70 --> 00:01:17,10
instead of getting another reference.

27
00:01:17,10 --> 00:01:21,20
If you take one thing away from this course, let it be this.

28
00:01:21,20 --> 00:01:24,20
Store your DOM objects in local variables,

29
00:01:24,20 --> 00:01:26,00
whenever possible.

