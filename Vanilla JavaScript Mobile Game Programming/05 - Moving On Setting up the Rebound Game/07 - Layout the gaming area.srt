1
00:00:00,50 --> 00:00:03,10
- [Instructor] So we have references to the DOM objects,

2
00:00:03,10 --> 00:00:05,60
but we still need to render them on the screen,

3
00:00:05,60 --> 00:00:09,70
so let's add a call to a function named layoutPage

4
00:00:09,70 --> 00:00:12,50
to our init function and then we'll add

5
00:00:12,50 --> 00:00:16,00
the layoutPage function directly beneath that.

6
00:00:16,00 --> 00:00:19,00
So let's add our layoutPage function

7
00:00:19,00 --> 00:00:21,60
and here is where we'll determine

8
00:00:21,60 --> 00:00:24,40
all the sizing needed for the game.

9
00:00:24,40 --> 00:00:26,80
So the first thing to do is to initialize

10
00:00:26,80 --> 00:00:31,10
available width and available height.

11
00:00:31,10 --> 00:00:33,50
Those we'll get directly from the window objects,

12
00:00:33,50 --> 00:00:38,80
inner width and inner height properties.

13
00:00:38,80 --> 00:00:40,40
Next, we'll do a little bit of math

14
00:00:40,40 --> 00:00:43,40
on the available width and height and assign those values

15
00:00:43,40 --> 00:00:46,40
to the playing area's width and height.

16
00:00:46,40 --> 00:00:49,50
So, we have margins of 10 pixels per side

17
00:00:49,50 --> 00:00:54,00
on the playing area and then a one pixel border all around,

18
00:00:54,00 --> 00:00:57,70
so the playing area's width should be

19
00:00:57,70 --> 00:01:02,40
whatever's in the available width minus 22.

20
00:01:02,40 --> 00:01:05,60
And the playing area height

21
00:01:05,60 --> 00:01:08,40
is going to be the same thing

22
00:01:08,40 --> 00:01:11,20
with respect to available height.

23
00:01:11,20 --> 00:01:12,80
Now that we have the playing area's

24
00:01:12,80 --> 00:01:14,90
width and height calculated,

25
00:01:14,90 --> 00:01:20,50
we can assign those values to our DOM object.

26
00:01:20,50 --> 00:01:23,50
So, use the playing area variable

27
00:01:23,50 --> 00:01:28,60
and then we can set the style objects width property

28
00:01:28,60 --> 00:01:33,60
equal to our playing area width variable

29
00:01:33,60 --> 00:01:36,10
and then we'll append our label.

30
00:01:36,10 --> 00:01:38,90
And we'll do the same thing with respects

31
00:01:38,90 --> 00:01:45,00
to the height and playing area height.

32
00:01:45,00 --> 00:01:48,00
And append the label.

33
00:01:48,00 --> 00:01:50,30
Okay, let's save that

34
00:01:50,30 --> 00:01:52,90
and then jump back into our HTML file

35
00:01:52,90 --> 00:01:55,50
and add a reference to the script.

36
00:01:55,50 --> 00:01:57,20
We can do that directly beneath

37
00:01:57,20 --> 00:01:59,60
the reference to the style sheet we added

38
00:01:59,60 --> 00:02:02,00
by adding a script element.

39
00:02:02,00 --> 00:02:06,80
Set the type equal to text JavaScript

40
00:02:06,80 --> 00:02:09,40
and then set the source attribute

41
00:02:09,40 --> 00:02:12,70
and point it to rebound.js.

42
00:02:12,70 --> 00:02:15,60
Then we'll close the script element.

43
00:02:15,60 --> 00:02:20,70
Save the HTML file and then let's fire it up.

44
00:02:20,70 --> 00:02:24,70
Right-click rebound.html, choose Run File,

45
00:02:24,70 --> 00:02:26,00
and here we can see that the page

46
00:02:26,00 --> 00:02:28,20
has been laid out properly.

47
00:02:28,20 --> 00:02:32,40
Notice that the margins all around are 10 pixels.

48
00:02:32,40 --> 00:02:34,30
I'm going to close the dev tools for now.

49
00:02:34,30 --> 00:02:35,60
We won't need 'em for a little bit.

50
00:02:35,60 --> 00:02:37,80
When we do, we can always reopen them.

51
00:02:37,80 --> 00:02:39,60
Reload the page and you can see

52
00:02:39,60 --> 00:02:42,70
that the playing area changes size.

53
00:02:42,70 --> 00:02:45,40
Okay, to capture that resizing event,

54
00:02:45,40 --> 00:02:49,10
we can add a second event listener to the window object.

55
00:02:49,10 --> 00:02:54,20
So jump back into NetBeans and then open rebound.js

56
00:02:54,20 --> 00:02:57,40
and directly beneath where we added the event listener

57
00:02:57,40 --> 00:02:59,50
to listen for the load event,

58
00:02:59,50 --> 00:03:02,60
we can add a second event listener,

59
00:03:02,60 --> 00:03:08,00
only this time we'll listen to the resize event

60
00:03:08,00 --> 00:03:12,60
and in this case, we'll also fire init.

61
00:03:12,60 --> 00:03:17,80
The resize event fires whenever the user resizes the window.

62
00:03:17,80 --> 00:03:20,40
We want to call init to reposition

63
00:03:20,40 --> 00:03:23,30
the elements on the screen and the cool part is

64
00:03:23,30 --> 00:03:27,60
that the resize event also fires on a mobile device

65
00:03:27,60 --> 00:03:30,10
when the user changes the orientation.

66
00:03:30,10 --> 00:03:32,80
So if the user goes from portrait to landscape,

67
00:03:32,80 --> 00:03:37,00
all of the elements on our page will resize appropriately.

68
00:03:37,00 --> 00:03:40,20
So let's save that and then we can jump back into Chrome

69
00:03:40,20 --> 00:03:43,40
and now we can resize the window.

70
00:03:43,40 --> 00:03:45,70
I can reload the window

71
00:03:45,70 --> 00:03:51,00
and if I fire up the dev tools again,

72
00:03:51,00 --> 00:03:53,60
you can see that the size of the game

73
00:03:53,60 --> 00:03:57,10
automatically is calculated correctly.

74
00:03:57,10 --> 00:04:00,80
And if I were to move the window for the dev tools,

75
00:04:00,80 --> 00:04:05,00
you can see the game changes size appropriately.

