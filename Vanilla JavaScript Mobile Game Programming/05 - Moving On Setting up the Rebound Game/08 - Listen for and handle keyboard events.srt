1
00:00:00,50 --> 00:00:01,60
- [Instructor] Since we want to support

2
00:00:01,60 --> 00:00:03,50
as many devices as possible,

3
00:00:03,50 --> 00:00:06,70
including devices that may not include a touch interface,

4
00:00:06,70 --> 00:00:10,30
let's make the keyboard a source of user input.

5
00:00:10,30 --> 00:00:12,60
To do that, we'll need to add an event listener

6
00:00:12,60 --> 00:00:15,80
for the key down event of the document object.

7
00:00:15,80 --> 00:00:18,80
Since the document object must first be loaded,

8
00:00:18,80 --> 00:00:21,70
we'll need to add to listener an init function

9
00:00:21,70 --> 00:00:24,40
just below the call to layout page.

10
00:00:24,40 --> 00:00:26,80
So, find the call to layout page,

11
00:00:26,80 --> 00:00:30,50
and put your cursor directly above it.

12
00:00:30,50 --> 00:00:34,90
Let's add an event listener to the document object.

13
00:00:34,90 --> 00:00:39,40
So, document dot add event listener, and to that

14
00:00:39,40 --> 00:00:43,80
we'll want to listen for the key down event

15
00:00:43,80 --> 00:00:45,90
and we'll handle the event with a function

16
00:00:45,90 --> 00:00:50,50
we'll write momentarily, which I'll call key listener.

17
00:00:50,50 --> 00:00:52,40
And then the third argument is optional

18
00:00:52,40 --> 00:00:54,40
and it's specifies whether to execute

19
00:00:54,40 --> 00:00:57,90
the event during a capture or the bubbling phase.

20
00:00:57,90 --> 00:00:59,60
True specifies a capture phase

21
00:00:59,60 --> 00:01:01,50
and false, the bubbling phase.

22
00:01:01,50 --> 00:01:03,00
Since we want the keyboard events

23
00:01:03,00 --> 00:01:05,40
to fire the same way on every element,

24
00:01:05,40 --> 00:01:10,00
we'll specify false to execute during the bubbling phase.

25
00:01:10,00 --> 00:01:12,50
Okay, next, let's add the key listener function

26
00:01:12,50 --> 00:01:13,60
we just referred to.

27
00:01:13,60 --> 00:01:16,40
So, I'll put my cursor down at the bottom

28
00:01:16,40 --> 00:01:22,60
and we'll add that key listener function.

29
00:01:22,60 --> 00:01:24,80
The key listener function will receive

30
00:01:24,80 --> 00:01:28,50
a keyboard event by default.

31
00:01:28,50 --> 00:01:30,80
We'll call that variable E.

32
00:01:30,80 --> 00:01:33,30
Okay, so the key listener grabs the key code

33
00:01:33,30 --> 00:01:35,70
for the pressed key and then reacts to it

34
00:01:35,70 --> 00:01:37,30
by moving the paddle.

35
00:01:37,30 --> 00:01:39,90
Since I'm old school and right handed,

36
00:01:39,90 --> 00:01:43,10
I tend to think of using the arrow keys first.

37
00:01:43,10 --> 00:01:45,70
Most modern games include both keyboard

38
00:01:45,70 --> 00:01:47,90
and mouse functionality and so use

39
00:01:47,90 --> 00:01:51,90
the A S D and W keys for keyboard input.

40
00:01:51,90 --> 00:01:55,00
Let's include both just in case.

41
00:01:55,00 --> 00:01:59,40
So, we'll first check for the A and the left key.

42
00:01:59,40 --> 00:02:02,70
Let's write a conditional statement

43
00:02:02,70 --> 00:02:07,70
and we'll grab the key code from the event object

44
00:02:07,70 --> 00:02:09,40
and we'll compare it to first,

45
00:02:09,40 --> 00:02:15,20
the value for the left key and that's 37.

46
00:02:15,20 --> 00:02:18,70
And we want to handle the event identically

47
00:02:18,70 --> 00:02:21,90
if the key code is the A key,

48
00:02:21,90 --> 00:02:26,90
so let's get the key code again

49
00:02:26,90 --> 00:02:31,90
and we'll check for the value of 65.

50
00:02:31,90 --> 00:02:33,20
And then, of course, we don't want

51
00:02:33,20 --> 00:02:37,50
to do anything if the paddle is already

52
00:02:37,50 --> 00:02:39,80
on the left edge of the screen

53
00:02:39,80 --> 00:02:44,00
and so let's add an and clause and we'll say

54
00:02:44,00 --> 00:02:47,10
paddle left

55
00:02:47,10 --> 00:02:50,80
is greater than zero.

56
00:02:50,80 --> 00:02:52,80
So, our event will fire and we'll handle it

57
00:02:52,80 --> 00:02:56,90
if the key code is A or the left arrow

58
00:02:56,90 --> 00:03:00,90
and the paddle is off the left edge of the keyboard.

59
00:03:00,90 --> 00:03:04,10
Okay, so all we'll have to do once we get here

60
00:03:04,10 --> 00:03:07,60
is to take the paddle left variable

61
00:03:07,60 --> 00:03:10,90
and we'll subtract whatever we have

62
00:03:10,90 --> 00:03:16,40
in our pdx variable, that's the speed of the paddle.

63
00:03:16,40 --> 00:03:18,10
And then we'll do a sanity check,

64
00:03:18,10 --> 00:03:20,00
because there's a chance that paddle left

65
00:03:20,00 --> 00:03:23,00
could now be outside of the game area.

66
00:03:23,00 --> 00:03:25,60
Let's say if

67
00:03:25,60 --> 00:03:28,80
and then paddle left

68
00:03:28,80 --> 00:03:37,30
is less than zero, we'll just set it equal to zero.

69
00:03:37,30 --> 00:03:40,00
So assign zero to paddle left.

70
00:03:40,00 --> 00:03:43,10
Now that we have a valid paddle left value,

71
00:03:43,10 --> 00:03:45,20
let's just assign it to the left property

72
00:03:45,20 --> 00:03:47,30
of the paddle dom object.

73
00:03:47,30 --> 00:03:48,10
So, that would be

74
00:03:48,10 --> 00:03:54,10
paddle dot style dot left

75
00:03:54,10 --> 00:03:56,50
and we'll assign to that whatever's

76
00:03:56,50 --> 00:04:02,30
in our paddle left variable plus

77
00:04:02,30 --> 00:04:05,40
a label and that's pixels.

78
00:04:05,40 --> 00:04:07,00
Okay, that's a great start.

79
00:04:07,00 --> 00:04:08,80
Now we'll need to handle

80
00:04:08,80 --> 00:04:12,40
the right arrow key and the W key.

81
00:04:12,40 --> 00:04:19,30
So, I'm going to copy and paste this conditional

82
00:04:19,30 --> 00:04:21,00
and then make some modifications to it

83
00:04:21,00 --> 00:04:26,50
to suit my needs for the right edge of the gaming area.

84
00:04:26,50 --> 00:04:30,90
Okay, so, rather than key code equals 37

85
00:04:30,90 --> 00:04:34,30
the right arrow key is key code 39

86
00:04:34,30 --> 00:04:40,20
and the W key is 68.

87
00:04:40,20 --> 00:04:42,20
And instead of checking whether to not

88
00:04:42,20 --> 00:04:45,30
the paddle is on the left edge of the screen,

89
00:04:45,30 --> 00:04:48,40
we'll have to do a check for the paddle

90
00:04:48,40 --> 00:04:51,20
on the right edge of the screen.

91
00:04:51,20 --> 00:04:55,10
Okay, so instead of saying greater than zero,

92
00:04:55,10 --> 00:04:58,50
we need to check whether or not the paddle

93
00:04:58,50 --> 00:05:03,90
is less than what's in our playing width variable

94
00:05:03,90 --> 00:05:07,50
and then minus the width of the paddle.

95
00:05:07,50 --> 00:05:09,60
So that's our conditional statement.

96
00:05:09,60 --> 00:05:12,00
Since we're moving the paddle to the right this time

97
00:05:12,00 --> 00:05:17,60
instead of subtracting pdx, we want to add pdx

98
00:05:17,60 --> 00:05:19,40
and our conditional statement to make sure

99
00:05:19,40 --> 00:05:22,10
we don't go past the right edge of the screen

100
00:05:22,10 --> 00:05:26,50
should read if paddle left is greater than

101
00:05:26,50 --> 00:05:30,00
and then it's P width minus 64.

102
00:05:30,00 --> 00:05:34,60
So, that's P width minus 64.

103
00:05:34,60 --> 00:05:38,90
We're going to assign P width minus 64

104
00:05:38,90 --> 00:05:42,20
to the left of the paddle.

105
00:05:42,20 --> 00:05:44,60
And we'll still need to assign

106
00:05:44,60 --> 00:05:47,70
that left property by using what's

107
00:05:47,70 --> 00:05:50,50
in the current paddle left variable

108
00:05:50,50 --> 00:05:53,00
plus our pixels label.

