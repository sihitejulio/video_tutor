1
00:00:00,50 --> 00:00:02,80
- Let's create the CSS doc to style,

2
00:00:02,80 --> 00:00:06,40
and position the HTML objects we've already created.

3
00:00:06,40 --> 00:00:09,10
So right-click the Site Root, choose New,

4
00:00:09,10 --> 00:00:11,30
Cascading Style Sheet.

5
00:00:11,30 --> 00:00:14,00
I'll call it Rebound, remember NetBeans will

6
00:00:14,00 --> 00:00:17,30
add the extension, we can click Finish.

7
00:00:17,30 --> 00:00:20,80
Let's add a body rule just to set the font.

8
00:00:20,80 --> 00:00:24,30
So I'll say font-family, and we'll just set that

9
00:00:24,30 --> 00:00:27,10
to sans-serif, we'll let the browser decide.

10
00:00:27,10 --> 00:00:28,50
Okay, we're going to position everything

11
00:00:28,50 --> 00:00:31,00
on the screen absolutely, because that will give us

12
00:00:31,00 --> 00:00:34,40
the required control for all of the objects on the screen.

13
00:00:34,40 --> 00:00:37,40
Just to reiterate, the best advice I have for you,

14
00:00:37,40 --> 00:00:39,10
is that if you need to position anything

15
00:00:39,10 --> 00:00:41,60
on the screen absolutely, you should position

16
00:00:41,60 --> 00:00:43,90
everything on the screen absolutely.

17
00:00:43,90 --> 00:00:45,90
Absolute positioning removes an element

18
00:00:45,90 --> 00:00:48,10
from the normal document flow.

19
00:00:48,10 --> 00:00:49,80
This can have some odd effects

20
00:00:49,80 --> 00:00:51,70
if you mix absolute positioning

21
00:00:51,70 --> 00:00:54,10
with other positioning values.

22
00:00:54,10 --> 00:00:55,90
All right, let's start with the playing area,

23
00:00:55,90 --> 00:00:59,10
so we'll add a playing area rule.

24
00:00:59,10 --> 00:01:02,70
As I said, we'll position it absolutely,

25
00:01:02,70 --> 00:01:05,40
let's give it a border.

26
00:01:05,40 --> 00:01:09,10
Black, one pixel, and solid.

27
00:01:09,10 --> 00:01:11,00
We'll give it a background color so we can

28
00:01:11,00 --> 00:01:13,50
distinguish it from the browser's background.

29
00:01:13,50 --> 00:01:19,00
So background-color, and I've got a light Gray,

30
00:01:19,00 --> 00:01:24,90
so I'm going to do the RGB function, and 216's all around.

31
00:01:24,90 --> 00:01:27,00
Okay, I'm going to give it a top,

32
00:01:27,00 --> 00:01:29,50
and that's going to be ten pixels,

33
00:01:29,50 --> 00:01:33,50
and a left of ten pixels as well.

34
00:01:33,50 --> 00:01:36,10
Notice that there's no width and height.

35
00:01:36,10 --> 00:01:38,30
Those will be determined at runtime,

36
00:01:38,30 --> 00:01:43,00
and we'll get those values using JavaScript,

