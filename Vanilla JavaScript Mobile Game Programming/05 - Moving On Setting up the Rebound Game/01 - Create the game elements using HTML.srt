1
00:00:00,50 --> 00:00:02,60
- Defining the objects required for Rebound

2
00:00:02,60 --> 00:00:04,40
is actually pretty simple.

3
00:00:04,40 --> 00:00:06,50
Since it is a ball and paddle game,

4
00:00:06,50 --> 00:00:08,80
we'll need ball and paddle objects.

5
00:00:08,80 --> 00:00:10,40
Go figure, right?

6
00:00:10,40 --> 00:00:13,30
We'll also need to define the playing area for the game

7
00:00:13,30 --> 00:00:17,00
and set aside some space to display and update the score.

8
00:00:17,00 --> 00:00:19,60
OK Let's get started by creating a new project.

9
00:00:19,60 --> 00:00:23,30
You can click the New Project icon in the toolbar.

10
00:00:23,30 --> 00:00:27,20
Make sure you're choosing HTML5JavaScript in the categories,

11
00:00:27,20 --> 00:00:30,40
and HTML5JSApplication in the projects,

12
00:00:30,40 --> 00:00:31,90
click Next.

13
00:00:31,90 --> 00:00:35,20
Let's name this one Rebound.

14
00:00:35,20 --> 00:00:37,20
We can click Next.

15
00:00:37,20 --> 00:00:39,90
No Site Template is needed

16
00:00:39,90 --> 00:00:41,90
and you can uncheck all the tools,

17
00:00:41,90 --> 00:00:43,90
we won't need those either.

18
00:00:43,90 --> 00:00:46,60
Go ahead and click Finish.

19
00:00:46,60 --> 00:00:49,90
Let's rename our indexed HTML file to be Rebound.

20
00:00:49,90 --> 00:00:52,20
So I'll right click that file,

21
00:00:52,20 --> 00:00:55,80
choose Refactor and then Rename

22
00:00:55,80 --> 00:00:59,00
and then change the name from indexed...

23
00:00:59,00 --> 00:01:00,30
To...

24
00:01:00,30 --> 00:01:01,50
Rebound.

25
00:01:01,50 --> 00:01:04,60
Go ahead and click Refactor.

26
00:01:04,60 --> 00:01:07,60
You can change the title.

27
00:01:07,60 --> 00:01:09,80
Let's change that to be Rebound

28
00:01:09,80 --> 00:01:13,50
and I don't need the TODO div.

29
00:01:13,50 --> 00:01:14,60
Okay...

30
00:01:14,60 --> 00:01:15,90
What I'm going to do in this game is a little

31
00:01:15,90 --> 00:01:17,50
bit different than Button Chaser.

32
00:01:17,50 --> 00:01:20,30
I'm going to define a playing area that's going to

33
00:01:20,30 --> 00:01:23,10
enclose all of the elements of the game.

34
00:01:23,10 --> 00:01:27,30
So let's first add a div for the playing area.

35
00:01:27,30 --> 00:01:29,10
I'll give that an ID,

36
00:01:29,10 --> 00:01:32,00
set that equal to playing area.

37
00:01:32,00 --> 00:01:34,40
And then inside the playing area

38
00:01:34,40 --> 00:01:38,40
is where we will add all of the other div's for the game.

39
00:01:38,40 --> 00:01:43,60
So let's start with a div for the paddle,

40
00:01:43,60 --> 00:01:45,10
and that will be empty.

41
00:01:45,10 --> 00:01:47,20
We'll style it using CSS.

42
00:01:47,20 --> 00:01:51,10
Do the same thing for the ball.

43
00:01:51,10 --> 00:01:52,10
And then finally,

44
00:01:52,10 --> 00:01:55,10
we need a div for the score.

45
00:01:55,10 --> 00:01:57,30
Div ID equals...

46
00:01:57,30 --> 00:01:58,70
Score.

47
00:01:58,70 --> 00:02:01,70
That one does have some content though, right?

48
00:02:01,70 --> 00:02:03,70
Because initially...

49
00:02:03,70 --> 00:02:04,70
when the game starts

50
00:02:04,70 --> 00:02:07,30
the score is zero.

51
00:02:07,30 --> 00:02:09,40
So let's save that

52
00:02:09,40 --> 00:02:12,00
and then we can move on to doing the CSS.

