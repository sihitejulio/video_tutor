1
00:00:00,50 --> 00:00:01,80
- [Instructor] Let's see if we can optimize

2
00:00:01,80 --> 00:00:04,90
the key list in here so it is as efficient as possible.

3
00:00:04,90 --> 00:00:07,70
The first optimization is an easy one.

4
00:00:07,70 --> 00:00:09,80
Notice that we are looking up the key code property

5
00:00:09,80 --> 00:00:12,40
of the event object four times,

6
00:00:12,40 --> 00:00:14,80
and it never changes inside the listener.

7
00:00:14,80 --> 00:00:17,60
It would be way more efficient to look it up once,

8
00:00:17,60 --> 00:00:19,20
store it in a variable,

9
00:00:19,20 --> 00:00:22,10
and use the variable in the conditional statements.

10
00:00:22,10 --> 00:00:26,30
So let's add a variable to hold the key code.

11
00:00:26,30 --> 00:00:28,70
I'll call that variable key,

12
00:00:28,70 --> 00:00:34,00
and then I'll assign to it e.keyCode.

13
00:00:34,00 --> 00:00:38,10
And then going forward, wherever we reference the key code

14
00:00:38,10 --> 00:00:40,40
inside our conditional statements,

15
00:00:40,40 --> 00:00:44,50
we can simply replace it with our key variable.

16
00:00:44,50 --> 00:00:47,90
There are four of those.

17
00:00:47,90 --> 00:00:53,00
And one more.

18
00:00:53,00 --> 00:00:54,60
Now's a good time to talk about

19
00:00:54,60 --> 00:00:58,20
optimizing conditional statements in JavaScript.

20
00:00:58,20 --> 00:01:01,40
The JavaScript "or" and "and" operators

21
00:01:01,40 --> 00:01:03,70
are optimized by default.

22
00:01:03,70 --> 00:01:06,50
Take a look at the first conditional statement.

23
00:01:06,50 --> 00:01:10,50
The first clause is separated by the "or" operator.

24
00:01:10,50 --> 00:01:13,80
With "or" operations, only one of the operands

25
00:01:13,80 --> 00:01:17,90
needs to be true for the clause to evaluate it's true.

26
00:01:17,90 --> 00:01:21,50
So if the key value is 37,

27
00:01:21,50 --> 00:01:24,80
the second half of that clause is never evaluated.

28
00:01:24,80 --> 00:01:26,50
If you are careful with the order

29
00:01:26,50 --> 00:01:29,00
of the operands in an "or" statement,

30
00:01:29,00 --> 00:01:32,40
and place the operand more likely to be true first,

31
00:01:32,40 --> 00:01:35,00
your code would be more efficient.

32
00:01:35,00 --> 00:01:36,90
Again in the first conditional,

33
00:01:36,90 --> 00:01:40,40
the main operator is the "and" operator.

34
00:01:40,40 --> 00:01:43,40
For an "and" operation to evaluate to true,

35
00:01:43,40 --> 00:01:46,40
both clauses must evaluate to true.

36
00:01:46,40 --> 00:01:49,00
To optimize the "and" operator,

37
00:01:49,00 --> 00:01:52,60
place the operand that is more likely to be false first,

38
00:01:52,60 --> 00:01:56,10
and the second operand won't be executed.

39
00:01:56,10 --> 00:01:58,90
Now look at the second conditional statement.

40
00:01:58,90 --> 00:02:00,90
Notice how we've put the comparison

41
00:02:00,90 --> 00:02:04,80
that requires a calculation in the second operand.

42
00:02:04,80 --> 00:02:07,00
That makes it more likely to be skipped,

43
00:02:07,00 --> 00:02:12,80
as it will only execute if the key value is 39 or 68.

44
00:02:12,80 --> 00:02:15,20
Calculations are always more expensive

45
00:02:15,20 --> 00:02:17,80
than simple checks for equality.

46
00:02:17,80 --> 00:02:20,20
If you place them as the second operand

47
00:02:20,20 --> 00:02:22,00
in an "and" operation,

48
00:02:22,00 --> 00:02:24,50
your code will be more efficient.

49
00:02:24,50 --> 00:02:27,60
Our code currently executes both conditional statements

50
00:02:27,60 --> 00:02:29,70
every time a key is pressed.

51
00:02:29,70 --> 00:02:31,40
That's not necessary.

52
00:02:31,40 --> 00:02:34,60
If the first conditional statement evaluates to true,

53
00:02:34,60 --> 00:02:36,90
the second can never be true.

54
00:02:36,90 --> 00:02:39,60
We can fix that by simply adding an else

55
00:02:39,60 --> 00:02:42,40
in between the two conditional statements.

56
00:02:42,40 --> 00:02:48,70
So I'm going to add an else clause

57
00:02:48,70 --> 00:02:51,50
in between the two conditional statements.

58
00:02:51,50 --> 00:02:53,60
And then finally, notice that we assigned

59
00:02:53,60 --> 00:02:55,20
the paddle left value

60
00:02:55,20 --> 00:02:58,90
to the paddle's left property in both cases.

61
00:02:58,90 --> 00:03:01,50
Since we want that to happen in both cases,

62
00:03:01,50 --> 00:03:02,70
we can factor it out of

63
00:03:02,70 --> 00:03:04,70
the conditional statements completely

64
00:03:04,70 --> 00:03:07,20
and put it at the end of the function.

65
00:03:07,20 --> 00:03:10,00
This won't necessarily optimize anything,

66
00:03:10,00 --> 00:03:13,40
but it will make our code easier to read.

67
00:03:13,40 --> 00:03:17,50
So let's grab that assignment,

68
00:03:17,50 --> 00:03:19,60
and then I'll cut it,

69
00:03:19,60 --> 00:03:24,00
and then I will add it outside the conditional statement,

70
00:03:24,00 --> 00:03:30,10
and of course remember to take it out of the else clause.

71
00:03:30,10 --> 00:03:31,90
That's about the best we can do

72
00:03:31,90 --> 00:03:35,00
for optimizing our code so far.

