1
00:00:00,50 --> 00:00:02,00
- [Instructor] Now that we've got our objects

2
00:00:02,00 --> 00:00:05,20
set up in HTML and styles with CSS,

3
00:00:05,20 --> 00:00:06,80
it's time to get them to interact

4
00:00:06,80 --> 00:00:09,00
on the screen using JavaScript.

5
00:00:09,00 --> 00:00:13,30
Right-click the site root and choose New, JavaScript File.

6
00:00:13,30 --> 00:00:15,10
I'll call it rebound.

7
00:00:15,10 --> 00:00:18,50
Remember, NetBeans will add the extension for us.

8
00:00:18,50 --> 00:00:20,70
Go ahead and click Finish.

9
00:00:20,70 --> 00:00:24,10
We're going to need references to the HTML object we created

10
00:00:24,10 --> 00:00:26,40
in multiple locations in our script.

11
00:00:26,40 --> 00:00:29,40
So let's create some variables to hold references

12
00:00:29,40 --> 00:00:32,00
that point to those DOM objects.

13
00:00:32,00 --> 00:00:35,70
Accessing the DOM is a very expensive operation.

14
00:00:35,70 --> 00:00:38,80
Rather than access a DOM node multiple times,

15
00:00:38,80 --> 00:00:41,00
it makes sense to access a node once

16
00:00:41,00 --> 00:00:42,70
and store the reference to the node

17
00:00:42,70 --> 00:00:44,90
in a variable for later use.

18
00:00:44,90 --> 00:00:46,80
That minimizes the number of times

19
00:00:46,80 --> 00:00:49,00
the JavaScript engine needs to traverse

20
00:00:49,00 --> 00:00:51,10
the DOM looking for nodes.

21
00:00:51,10 --> 00:00:53,60
So, in our script, let's create some variables

22
00:00:53,60 --> 00:00:56,40
that will point to the HTML objects.

23
00:00:56,40 --> 00:00:58,70
I'll start with the ball.

24
00:00:58,70 --> 00:01:02,00
Let's do a variable for the paddle.

25
00:01:02,00 --> 00:01:05,60
Let's do a variable for the score.

26
00:01:05,60 --> 00:01:10,60
And then let's do a variable for the playing area.

27
00:01:10,60 --> 00:01:12,40
We'll also need variables to hold

28
00:01:12,40 --> 00:01:15,10
the available width and height of the screen,

29
00:01:15,10 --> 00:01:17,70
the width and height of the playing area,

30
00:01:17,70 --> 00:01:20,70
speed values for the ball and paddle,

31
00:01:20,70 --> 00:01:23,20
a timer to fire off events,

32
00:01:23,20 --> 00:01:25,70
and the location values for the ball and paddle

33
00:01:25,70 --> 00:01:29,10
that we set in the CSS file.

34
00:01:29,10 --> 00:01:33,50
So let's add variables for available width,

35
00:01:33,50 --> 00:01:35,90
available height.

36
00:01:35,90 --> 00:01:39,60
Let's do a variable for the playing area width,

37
00:01:39,60 --> 00:01:43,50
another for the playing area height.

38
00:01:43,50 --> 00:01:44,90
Next, we'll add a variable

39
00:01:44,90 --> 00:01:48,80
for the horizontal speed of the ball

40
00:01:48,80 --> 00:01:52,10
and I'll initialize that to two.

41
00:01:52,10 --> 00:01:55,20
We'll do the same thing for the vertical speed.

42
00:01:55,20 --> 00:01:58,90
Set that also equal to two.

43
00:01:58,90 --> 00:02:01,00
I'm going to create a variable

44
00:02:01,00 --> 00:02:04,50
that's going to hold the paddle's speed

45
00:02:04,50 --> 00:02:07,80
and I'll set that to be 48.

46
00:02:07,80 --> 00:02:10,70
The paddle speed will be the number of pixels

47
00:02:10,70 --> 00:02:16,40
that the paddle moves on the screen using the keyboard.

48
00:02:16,40 --> 00:02:19,10
We also need a variable to hold the score of the game.

49
00:02:19,10 --> 00:02:24,10
I'll call that current score and I'll initialize it to zero

50
00:02:24,10 --> 00:02:28,90
so that the JavaScript engine knows it's a number.

51
00:02:28,90 --> 00:02:31,50
We'll need a timer to fire off all these events,

52
00:02:31,50 --> 00:02:34,20
so create a variable for that

53
00:02:34,20 --> 00:02:37,20
and then let's add some initial conditions

54
00:02:37,20 --> 00:02:40,20
that we'll get directly from the CSS file.

55
00:02:40,20 --> 00:02:44,90
So, the paddle left variable,

56
00:02:44,90 --> 00:02:49,30
make that a capital L, is 228

57
00:02:49,30 --> 00:02:53,10
and that's because we set that value in our CSS file.

58
00:02:53,10 --> 00:02:57,20
Let's create a ball left variable

59
00:02:57,20 --> 00:03:00,60
and we'll set that equal to 100

60
00:03:00,60 --> 00:03:06,70
and then we'll also set a ball top variable equal to

61
00:03:06,70 --> 00:03:07,90
eight.

62
00:03:07,90 --> 00:03:10,30
Okay, for the variables that don't have values

63
00:03:10,30 --> 00:03:17,60
assigned to them, they'll need to be initialized

64
00:03:17,60 --> 00:03:20,60
and listen for the windows load event,

65
00:03:20,60 --> 00:03:24,60
so window.add event listener.

66
00:03:24,60 --> 00:03:27,30
We're going to listen for the load event

67
00:03:27,30 --> 00:03:28,90
and when the load even fires,

68
00:03:28,90 --> 00:03:32,70
we'll write a function called init.

69
00:03:32,70 --> 00:03:37,00
The init function will initialize all of the variables.

