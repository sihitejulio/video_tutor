1
00:00:00,30 --> 00:00:02,40
- [Instructor] Let's style the game elements.

2
00:00:02,40 --> 00:00:04,50
Okay, I'll add a rule for the paddle.

3
00:00:04,50 --> 00:00:06,40
We'll do that one first.

4
00:00:06,40 --> 00:00:07,80
So for the paddle, we're going to position

5
00:00:07,80 --> 00:00:09,20
that one absolutely as well.

6
00:00:09,20 --> 00:00:11,70
Remember, when we position one thing absolutely,

7
00:00:11,70 --> 00:00:16,10
we should position everything absolutely.

8
00:00:16,10 --> 00:00:17,70
I'm going to use the bottom property

9
00:00:17,70 --> 00:00:19,90
instead of the top, and that's because

10
00:00:19,90 --> 00:00:24,80
I want the bottom of the paddle to stick to the score label.

11
00:00:24,80 --> 00:00:28,60
So we'll set the bottom to be 30 pixels.

12
00:00:28,60 --> 00:00:31,40
I'll set the left property to an arbitrary value.

13
00:00:31,40 --> 00:00:37,10
That's going to be 228 pixels.

14
00:00:37,10 --> 00:00:41,30
The width of the paddle will be 64 pixels.

15
00:00:41,30 --> 00:00:45,00
The height of the paddle is 16 pixels,

16
00:00:45,00 --> 00:00:49,00
and let's give it a background color.

17
00:00:49,00 --> 00:00:51,50
And I'm going to use coral.

18
00:00:51,50 --> 00:00:54,30
You can choose whatever color you like.

19
00:00:54,30 --> 00:01:00,30
Okay, let's now add a rule for the ball.

20
00:01:00,30 --> 00:01:03,80
Position: absolute, just like everything else.

21
00:01:03,80 --> 00:01:05,60
I'll specify initial conditions

22
00:01:05,60 --> 00:01:08,60
by including top and left properties.

23
00:01:08,60 --> 00:01:10,40
This will be the location of the object

24
00:01:10,40 --> 00:01:12,00
when the game starts.

25
00:01:12,00 --> 00:01:14,10
So just remember that these are arbitrary.

26
00:01:14,10 --> 00:01:15,70
We only need to make sure that the ball

27
00:01:15,70 --> 00:01:17,50
starts high enough on the screen

28
00:01:17,50 --> 00:01:20,40
to allow the user to intersect it with the paddle.

29
00:01:20,40 --> 00:01:23,20
So I'll give the top property for the ball,

30
00:01:23,20 --> 00:01:25,20
ah, how 'about eight pixels.

31
00:01:25,20 --> 00:01:29,20
Left property, let's start it at 100 pixels.

32
00:01:29,20 --> 00:01:32,70
So let's give it a width of 16 pixels,

33
00:01:32,70 --> 00:01:36,50
and a height of 16 pixels as well.

34
00:01:36,50 --> 00:01:39,20
Now in order to make it a ball and not a rectangle,

35
00:01:39,20 --> 00:01:41,00
we need a border radius,

36
00:01:41,00 --> 00:01:46,40
so let's set that to be eight pixels.

37
00:01:46,40 --> 00:01:48,20
And what that does is it makes it a circle

38
00:01:48,20 --> 00:01:50,20
instead of some kind of oval.

39
00:01:50,20 --> 00:01:51,80
Notice that eight is half of 16,

40
00:01:51,80 --> 00:01:55,10
so the radius is constant all the way around.

41
00:01:55,10 --> 00:01:59,20
And then finally we need a background color for the ball.

42
00:01:59,20 --> 00:02:01,50
And I'm going to use cornflower blue,

43
00:02:01,50 --> 00:02:03,60
just because I like it.

44
00:02:03,60 --> 00:02:06,70
Okay, last game element is the score label.

45
00:02:06,70 --> 00:02:13,50
So let's do that one, #score, position: absolute,

46
00:02:13,50 --> 00:02:17,80
set the bottom to be zero pixels.

47
00:02:17,80 --> 00:02:19,30
We want the score label to adhere

48
00:02:19,30 --> 00:02:21,50
to the bottom of the playing area.

49
00:02:21,50 --> 00:02:25,00
I'll set the left property to be zero pixels as well,

50
00:02:25,00 --> 00:02:28,80
so that it aligns with the left edge of the playing area,

51
00:02:28,80 --> 00:02:31,20
and then we'll set the width property.

52
00:02:31,20 --> 00:02:34,20
So for the width property, I'm going to use a calc function,

53
00:02:34,20 --> 00:02:40,40
and that's going to be 100% minus 10 pixels.

54
00:02:40,40 --> 00:02:42,40
And now we'll insert the score label

55
00:02:42,40 --> 00:02:44,50
inside the playing area.

56
00:02:44,50 --> 00:02:48,10
Let's give it a height, 20 pixels.

57
00:02:48,10 --> 00:02:52,60
I'm going to set the foreground color to be white,

58
00:02:52,60 --> 00:02:58,50
and the background color to be a shade of green,

59
00:02:58,50 --> 00:03:06,60
so rgb, I have 32, 128, 64.

60
00:03:06,60 --> 00:03:08,70
And the background color will be green

61
00:03:08,70 --> 00:03:10,70
while the ball's in play, and then we'll turn it to red

62
00:03:10,70 --> 00:03:12,30
when the game's over.

63
00:03:12,30 --> 00:03:14,50
And then I'm going to set some padding,

64
00:03:14,50 --> 00:03:18,20
and that's going to be five pixels all around.

65
00:03:18,20 --> 00:03:22,00
Okay, that wraps up styling the game elements.

