1
00:00:00,50 --> 00:00:02,60
- [Instructor] Let's test out our style rules.

2
00:00:02,60 --> 00:00:05,00
If you haven't saved a CSS file yet,

3
00:00:05,00 --> 00:00:06,90
do that first.

4
00:00:06,90 --> 00:00:09,70
The visual cue in NetBeans for unsaved files

5
00:00:09,70 --> 00:00:12,60
is bold text on the Files tab.

6
00:00:12,60 --> 00:00:15,20
So I'll click the Save All button.

7
00:00:15,20 --> 00:00:17,70
Then we'll jump into the HTML file

8
00:00:17,70 --> 00:00:20,70
and link to the stylesheet so it loads at runtime.

9
00:00:20,70 --> 00:00:23,40
So we'll add a link element,

10
00:00:23,40 --> 00:00:28,10
let's set the relative style sheet,

11
00:00:28,10 --> 00:00:31,80
set the type to be Text CSS,

12
00:00:31,80 --> 00:00:37,30
and then set the href to point to our rebound.css file.

13
00:00:37,30 --> 00:00:39,00
Save that.

14
00:00:39,00 --> 00:00:41,30
Before we run the file, let's make sure

15
00:00:41,30 --> 00:00:42,80
we're going to run it correctly

16
00:00:42,80 --> 00:00:44,80
by right-clicking on the project name

17
00:00:44,80 --> 00:00:47,10
and choosing Properties.

18
00:00:47,10 --> 00:00:49,00
Click on the Run node,

19
00:00:49,00 --> 00:00:52,00
and then change Chrome with NetBeans Connector

20
00:00:52,00 --> 00:00:53,70
to be just Chrome.

21
00:00:53,70 --> 00:00:57,10
Make sure Auto-refresh is checked.

22
00:00:57,10 --> 00:00:59,80
Then click OK.

23
00:00:59,80 --> 00:01:03,30
Now we can right-click our rebound.html file

24
00:01:03,30 --> 00:01:06,90
and choose Run File.

25
00:01:06,90 --> 00:01:08,90
There's not much to see as the playing area

26
00:01:08,90 --> 00:01:10,90
hasn't yet been sized.

27
00:01:10,90 --> 00:01:13,40
Remember, we're going to do that at runtime

28
00:01:13,40 --> 00:01:15,90
using Javascript.

29
00:01:15,90 --> 00:01:18,20
But to get a feel for Rebound's appearance,

30
00:01:18,20 --> 00:01:20,60
we can open Chrome's devtools

31
00:01:20,60 --> 00:01:23,60
by clicking the ellipsis for the Context menu

32
00:01:23,60 --> 00:01:27,60
and then choose More Tools and Developer Tools.

33
00:01:27,60 --> 00:01:33,20
Choose the Elements tab and then click on the playing area,

34
00:01:33,20 --> 00:01:35,00
and then where it says Element Style

35
00:01:35,00 --> 00:01:37,00
we can add some temporary styles,

36
00:01:37,00 --> 00:01:40,70
so let's just give it a width and a height,

37
00:01:40,70 --> 00:01:44,10
how about 90% for now,

38
00:01:44,10 --> 00:01:47,00
and then add a height,

39
00:01:47,00 --> 00:01:50,70
and that, it can be 90% too.

40
00:01:50,70 --> 00:01:53,50
Don't worry, those values are just temporary.

41
00:01:53,50 --> 00:01:57,00
We'll supply the correct values using Javascript.

