1
00:00:00,50 --> 00:00:02,90
- [Instructor] The general process for replacement animation

2
00:00:02,90 --> 00:00:05,90
is to create an array of images to cycle through,

3
00:00:05,90 --> 00:00:07,90
then swap the images fast enough

4
00:00:07,90 --> 00:00:10,50
so that the user perceives motion.

5
00:00:10,50 --> 00:00:14,00
Just remember, there really isn't any motion taking place.

6
00:00:14,00 --> 00:00:15,90
You are simply controlling which frame

7
00:00:15,90 --> 00:00:18,00
the user is currently viewing.

8
00:00:18,00 --> 00:00:20,40
All right, let's do an animation project.

9
00:00:20,40 --> 00:00:23,90
So I'm going to click on the New Project button in NetBeans,

10
00:00:23,90 --> 00:00:28,40
make sure I have HTML5 JavaScript in the category

11
00:00:28,40 --> 00:00:31,60
and HTMLD JS APPLICATION in the projects.

12
00:00:31,60 --> 00:00:33,40
Go ahead and click next.

13
00:00:33,40 --> 00:00:37,40
I'll call this project Animation.

14
00:00:37,40 --> 00:00:38,90
Click next.

15
00:00:38,90 --> 00:00:41,20
No site template is needed

16
00:00:41,20 --> 00:00:44,30
and no tools.

17
00:00:44,30 --> 00:00:47,60
Go ahead and click finish.

18
00:00:47,60 --> 00:00:51,30
Okay, let's rename the index.HTML file.

19
00:00:51,30 --> 00:00:55,00
So right click on it and choose

20
00:00:55,00 --> 00:00:58,40
Refactor, Rename,

21
00:00:58,40 --> 00:01:03,90
and I'm going to change this to be replacement.

22
00:01:03,90 --> 00:01:05,70
Go ahead and click Refactor

23
00:01:05,70 --> 00:01:08,80
and now we have replacement.html.

24
00:01:08,80 --> 00:01:13,00
Okay, before we begin, we'll need an image set to work with.

25
00:01:13,00 --> 00:01:17,00
You can download a set I prepared from the exercise files.

26
00:01:17,00 --> 00:01:21,10
This file contains 24 frames of a baseball spinning.

27
00:01:21,10 --> 00:01:24,60
I downloaded the zip file and unzipped it to my desktop.

28
00:01:24,60 --> 00:01:27,40
You'll see the images folder here.

29
00:01:27,40 --> 00:01:30,20
It's customary to name the individual frames

30
00:01:30,20 --> 00:01:32,90
of an image set with the same prefix

31
00:01:32,90 --> 00:01:35,90
and then a frame counter for the number.

32
00:01:35,90 --> 00:01:39,50
You can see I have the frame starting at ball zero

33
00:01:39,50 --> 00:01:42,40
and then they cycle up through ball 23

34
00:01:42,40 --> 00:01:45,80
giving me 24 total files.

35
00:01:45,80 --> 00:01:48,60
This construct works nicely as we'll create an array

36
00:01:48,60 --> 00:01:50,40
to hold the images.

37
00:01:50,40 --> 00:01:52,90
The good news is, we won't have to declare

38
00:01:52,90 --> 00:01:55,60
each individual image object.

39
00:01:55,60 --> 00:01:58,10
Rather, because the names of the files are indexed,

40
00:01:58,10 --> 00:01:59,50
we can create a simple loop

41
00:01:59,50 --> 00:02:02,20
that does all of the work for us.

42
00:02:02,20 --> 00:02:04,50
So the first thing I'm going to do is I'm going to open up

43
00:02:04,50 --> 00:02:07,00
the images folder that I just created

44
00:02:07,00 --> 00:02:12,20
and I'm going to grab ball zero through ball 23

45
00:02:12,20 --> 00:02:13,20
and then I'm going to right click

46
00:02:13,20 --> 00:02:16,80
and I'm going to choose Copy 24 Items.

47
00:02:16,80 --> 00:02:19,80
Then I'll navigate back to NetBeans

48
00:02:19,80 --> 00:02:21,70
and inside the Site Root,

49
00:02:21,70 --> 00:02:25,80
let's right click Site Root and choose New and Folder

50
00:02:25,80 --> 00:02:28,90
and we'll add an images folder.

51
00:02:28,90 --> 00:02:30,60
Then inside the images folder,

52
00:02:30,60 --> 00:02:33,30
we can right click it and choose Paste

53
00:02:33,30 --> 00:02:34,70
and NetBeans is smart enough

54
00:02:34,70 --> 00:02:38,60
to load all of the images in the folder for us.

55
00:02:38,60 --> 00:02:41,60
Okay, let's modify the title

56
00:02:41,60 --> 00:02:46,10
and this is going to be our Replacement Animation file

57
00:02:46,10 --> 00:02:50,40
and we won't need the TODO div.

58
00:02:50,40 --> 00:02:54,40
I'm going to add an image element instead.

59
00:02:54,40 --> 00:02:56,30
I'll give it an id.

60
00:02:56,30 --> 00:02:59,30
Let's set that to ball

61
00:02:59,30 --> 00:03:02,10
and then a source attribute

62
00:03:02,10 --> 00:03:10,40
and it's going to be images/ball0.gif

63
00:03:10,40 --> 00:03:12,10
and then some alt text.

64
00:03:12,10 --> 00:03:16,00
Let's set that equal to something meaningful like baseball.

