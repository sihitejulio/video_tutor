1
00:00:00,50 --> 00:00:02,50
- [Narrator] Animation on the web is very much like

2
00:00:02,50 --> 00:00:05,00
those flip books you created as a kid.

3
00:00:05,00 --> 00:00:07,10
You know, you place an individual image

4
00:00:07,10 --> 00:00:09,10
on the corner of each page in the book

5
00:00:09,10 --> 00:00:12,30
then flip through the book rapidly to see the animation.

6
00:00:12,30 --> 00:00:15,90
If you flip rapidly enough, the animation is smooth.

7
00:00:15,90 --> 00:00:19,70
If you don't flip rapidly enough, the animation is jerky.

8
00:00:19,70 --> 00:00:23,40
Animators call the speed of the flipping, the frame rate.

9
00:00:23,40 --> 00:00:26,20
Using JavaScript, you can manipulate the properties

10
00:00:26,20 --> 00:00:28,30
of an image to simulate motion.

11
00:00:28,30 --> 00:00:31,60
You might change the source attribute of the image.

12
00:00:31,60 --> 00:00:34,20
Focus on the horse and its rider.

13
00:00:34,20 --> 00:00:36,60
The animation cycles through a set of images

14
00:00:36,60 --> 00:00:39,00
in a series, then repeats.

15
00:00:39,00 --> 00:00:41,00
This is how changing the source attribute

16
00:00:41,00 --> 00:00:42,80
of an image element, works.

17
00:00:42,80 --> 00:00:44,30
We'll explore this technique,

18
00:00:44,30 --> 00:00:48,10
which I call replacement animation.

19
00:00:48,10 --> 00:00:51,40
You might also change the images location on the screen.

20
00:00:51,40 --> 00:00:53,60
Now focus on the cactus.

21
00:00:53,60 --> 00:00:55,80
Notice how the image doesn't change

22
00:00:55,80 --> 00:00:58,80
but the location of the image onscreen does.

23
00:00:58,80 --> 00:01:03,20
The cactus follows a well-defined path in the animation.

24
00:01:03,20 --> 00:01:05,40
We'll explore creating a path for objects

25
00:01:05,40 --> 00:01:09,60
onscreen using mathematical formulas.

26
00:01:09,60 --> 00:01:12,40
Notice how the cactus sits behind the horse and rider

27
00:01:12,40 --> 00:01:15,80
when they occupy the same location onscreen.

28
00:01:15,80 --> 00:01:19,40
This is how we can simulate depth in an animation.

29
00:01:19,40 --> 00:01:21,70
Although outside the scope of this course,

30
00:01:21,70 --> 00:01:24,10
you can manipulate an objects Z index

31
00:01:24,10 --> 00:01:27,00
to accomplish layering objects.

32
00:01:27,00 --> 00:01:29,00
The cool part is that you can often

33
00:01:29,00 --> 00:01:32,60
combine these techniques to produce compelling animations.

34
00:01:32,60 --> 00:01:36,00
This simple, short animation does exactly that.

