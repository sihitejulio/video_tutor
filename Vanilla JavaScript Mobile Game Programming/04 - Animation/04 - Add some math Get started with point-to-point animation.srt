1
00:00:00,50 --> 00:00:01,70
- [Instructor] Point-to-point animation

2
00:00:01,70 --> 00:00:05,10
is done by calculating and objects coordinates on the screen

3
00:00:05,10 --> 00:00:07,20
using a mathematical formula.

4
00:00:07,20 --> 00:00:09,60
In essence, the point-to-point animation

5
00:00:09,60 --> 00:00:12,70
keeps track of the objects top and left properties

6
00:00:12,70 --> 00:00:16,00
and then modifies their values according to the formula.

7
00:00:16,00 --> 00:00:18,10
Let's take a look at a simple example,

8
00:00:18,10 --> 00:00:19,50
and then we'll see if we can write something

9
00:00:19,50 --> 00:00:21,40
more interesting.

10
00:00:21,40 --> 00:00:23,50
So first, right-click on the site route

11
00:00:23,50 --> 00:00:29,60
in our animation project, and choose New HTML file.

12
00:00:29,60 --> 00:00:32,70
Let's name it point.

13
00:00:32,70 --> 00:00:35,00
Go ahead and click Finish.

14
00:00:35,00 --> 00:00:37,60
Let's change the title.

15
00:00:37,60 --> 00:00:41,20
I'll just call it Point to Point Animation,

16
00:00:41,20 --> 00:00:44,00
and then we can ditch the TODO div,

17
00:00:44,00 --> 00:00:48,60
and we can add our image.

18
00:00:48,60 --> 00:00:53,50
I'm going to give the image an id attribute of ball,

19
00:00:53,50 --> 00:00:56,30
and I'm going to set the source attribute

20
00:00:56,30 --> 00:01:02,10
to be images/orb.gif,

21
00:01:02,10 --> 00:01:04,80
and we should give it an alt as well,

22
00:01:04,80 --> 00:01:07,20
and we'll just call it the ball.

23
00:01:07,20 --> 00:01:08,90
Okay, you probably notice that I don't have

24
00:01:08,90 --> 00:01:11,50
that orb.gif file inside my project.

25
00:01:11,50 --> 00:01:13,00
I need to go get that.

26
00:01:13,00 --> 00:01:16,60
The good news is I've included an images zip file

27
00:01:16,60 --> 00:01:18,70
in the exercise files.

28
00:01:18,70 --> 00:01:20,90
You can go grab it.

29
00:01:20,90 --> 00:01:24,10
I've unzipped that file to my desktop.

30
00:01:24,10 --> 00:01:28,30
When I open it, you'll see at the bottom orb.gif.

31
00:01:28,30 --> 00:01:31,60
I'll right-click it and choose Copy

32
00:01:31,60 --> 00:01:34,70
and then navigate back to NetBeans,

33
00:01:34,70 --> 00:01:37,40
and then right-click the images folder

34
00:01:37,40 --> 00:01:41,80
and choose Paste, and now you'll see the orb file.

35
00:01:41,80 --> 00:01:44,20
Let's style the orb,

36
00:01:44,20 --> 00:01:47,30
so that we can place it on the screen in its starting point.

37
00:01:47,30 --> 00:01:49,60
Right-click the site route, choose New.

38
00:01:49,60 --> 00:01:53,10
This time choose Cascading Style Sheet.

39
00:01:53,10 --> 00:01:55,90
Let's name it point.

40
00:01:55,90 --> 00:01:58,60
Remember, NetBeans will put the extension on for us.

41
00:01:58,60 --> 00:02:01,10
Go ahead and click Finish.

42
00:02:01,10 --> 00:02:04,90
So, let's style the ball,

43
00:02:04,90 --> 00:02:08,10
and I'll position it absolutely,

44
00:02:08,10 --> 00:02:15,00
and I'll set the top and left both to 100 pixels.

45
00:02:15,00 --> 00:02:17,00
That should do it for initial conditions.

46
00:02:17,00 --> 00:02:19,80
The top and left are completely arbitrary.

47
00:02:19,80 --> 00:02:22,00
You're welcome to choose any values you want.

