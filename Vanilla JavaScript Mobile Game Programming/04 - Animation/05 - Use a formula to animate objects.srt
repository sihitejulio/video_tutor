1
00:00:00,50 --> 00:00:03,30
- [Instructor] Okay it's time to add a script element

2
00:00:03,30 --> 00:00:05,30
to get our animation running.

3
00:00:05,30 --> 00:00:07,20
So let's right click the Site Root,

4
00:00:07,20 --> 00:00:10,10
choose New, JavaScript File.

5
00:00:10,10 --> 00:00:11,80
You can name it point,

6
00:00:11,80 --> 00:00:15,30
and remember Netscape will add the extension for this.

7
00:00:15,30 --> 00:00:18,10
Let's first create variables to hold

8
00:00:18,10 --> 00:00:23,00
the current values of our x and y coordinates.

9
00:00:23,00 --> 00:00:27,00
So I'm going to create a current X value so that equal to 100

10
00:00:27,00 --> 00:00:29,70
and let's do the same thing for current Y value

11
00:00:29,70 --> 00:00:31,80
so that equal to 100.

12
00:00:31,80 --> 00:00:35,20
Those values come directly from the CSS file.

13
00:00:35,20 --> 00:00:37,50
Remember the CSS file is the thing that specifies

14
00:00:37,50 --> 00:00:39,40
our initial conditions.

15
00:00:39,40 --> 00:00:42,70
Okay, then we just need to create a function

16
00:00:42,70 --> 00:00:44,90
that will call to do the animation.

17
00:00:44,90 --> 00:00:47,90
So I'm going to call that animate.

18
00:00:47,90 --> 00:00:49,70
The first thing to do in the animate function

19
00:00:49,70 --> 00:00:51,90
is to actually place the ball on the screen

20
00:00:51,90 --> 00:00:55,30
using the values toward in current X and current Y.

21
00:00:55,30 --> 00:01:00,20
So that means we need to use document.getElementById

22
00:01:00,20 --> 00:01:04,00
and we're going to get the ball, div,

23
00:01:04,00 --> 00:01:07,30
and then we'll access the style object,

24
00:01:07,30 --> 00:01:11,00
and we'll set the left property equal to

25
00:01:11,00 --> 00:01:17,80
whatever is in current X and then we have pins our label.

26
00:01:17,80 --> 00:01:19,80
We'll do the same thing for current Y

27
00:01:19,80 --> 00:01:22,10
with respect to the top property

28
00:01:22,10 --> 00:01:26,70
so let's copy and paste.

29
00:01:26,70 --> 00:01:33,50
Changing left to top and current X to current Y.

30
00:01:33,50 --> 00:01:35,90
Okay, now it's time to do the math, right?

31
00:01:35,90 --> 00:01:39,60
For linear movement, we're just going to use simple addition.

32
00:01:39,60 --> 00:01:42,80
You could use any kind of math formula that you like though

33
00:01:42,80 --> 00:01:44,70
and get the objects on your screen

34
00:01:44,70 --> 00:01:47,20
to follow some pretty complex paths.

35
00:01:47,20 --> 00:01:49,80
So, for us though, it's pretty simple.

36
00:01:49,80 --> 00:01:54,80
We're just going to take current X

37
00:01:54,80 --> 00:01:57,50
and increment by five.

38
00:01:57,50 --> 00:01:59,40
We'll do the same thing for current Y

39
00:01:59,40 --> 00:02:02,60
and just increment that by five as well.

40
00:02:02,60 --> 00:02:05,20
In essence what we're doing

41
00:02:05,20 --> 00:02:07,20
for the next frame.

42
00:02:07,20 --> 00:02:09,40
Once we have the animation set up,

43
00:02:09,40 --> 00:02:11,20
let's do our end conditions.

44
00:02:11,20 --> 00:02:12,90
How about a simple condition,

45
00:02:12,90 --> 00:02:17,40
I'll just say if the current X value

46
00:02:17,40 --> 00:02:20,30
is greater than 800

47
00:02:20,30 --> 00:02:24,80
or the current Y value

48
00:02:24,80 --> 00:02:27,80
is greater than 600.

49
00:02:27,80 --> 00:02:30,60
If either one of those things evaluates the true,

50
00:02:30,60 --> 00:02:34,90
will simply end the animation, I'll call return.

51
00:02:34,90 --> 00:02:39,20
Otherwise, we'll make a call to set timeout,

52
00:02:39,20 --> 00:02:41,90
we'll call animate

53
00:02:41,90 --> 00:02:44,40
and we'll cycle through this pretty quickly.

54
00:02:44,40 --> 00:02:48,60
How about every 10 milliseconds?

55
00:02:48,60 --> 00:02:53,70
Okay, then jump outside of the animate function

56
00:02:53,70 --> 00:02:59,40
and let's add an event listener for the load event,

57
00:02:59,40 --> 00:03:05,20
so let's window.addEventListener

58
00:03:05,20 --> 00:03:07,20
and we'll listen for the load events

59
00:03:07,20 --> 00:03:14,80
and when that fires, will run our animate function.

60
00:03:14,80 --> 00:03:16,70
So let's save that.

61
00:03:16,70 --> 00:03:18,90
And then one thing to do now is to just jump back

62
00:03:18,90 --> 00:03:22,80
into our HTML file and we have to link to the CSS file

63
00:03:22,80 --> 00:03:25,40
and then we have to link to the script as well.

64
00:03:25,40 --> 00:03:29,00
So let's add a link element,

65
00:03:29,00 --> 00:03:32,20
set the relation,

66
00:03:32,20 --> 00:03:34,20
the style sheet,

67
00:03:34,20 --> 00:03:38,70
set the type, to text/css,

68
00:03:38,70 --> 00:03:44,60
and then the href should point to point.css.

69
00:03:44,60 --> 00:03:48,40
And for the script we'll set the type for this one

70
00:03:48,40 --> 00:03:52,30
to be text/javascript

71
00:03:52,30 --> 00:04:01,30
and set the source attribute to be point.js.

72
00:04:01,30 --> 00:04:06,10
Save that and then we'll right click our point.html file

73
00:04:06,10 --> 00:04:09,10
and choose Run File.

74
00:04:09,10 --> 00:04:10,00
And there you go.

75
00:04:10,00 --> 00:04:13,40
Let me refresh that for you so you can see it.

76
00:04:13,40 --> 00:04:15,40
That's point to point animation.

77
00:04:15,40 --> 00:04:19,70
Essentially, we've move the ball from 100, 100

78
00:04:19,70 --> 00:04:23,00
to something that approaches the end conditions.

