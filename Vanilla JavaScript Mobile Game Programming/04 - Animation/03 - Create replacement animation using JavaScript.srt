1
00:00:00,50 --> 00:00:02,00
- [Instructor] It's time to add the JavaScript

2
00:00:02,00 --> 00:00:04,00
that makes our animation work.

3
00:00:04,00 --> 00:00:06,10
Right click the Site Root and choose New

4
00:00:06,10 --> 00:00:08,60
and choose JavaScript File.

5
00:00:08,60 --> 00:00:12,40
Let's call this replacement, remember NetBeans

6
00:00:12,40 --> 00:00:14,10
will add the file extension for us,

7
00:00:14,10 --> 00:00:17,50
you can go ahead and click Finish.

8
00:00:17,50 --> 00:00:20,10
So, the first thing to do is to create an array

9
00:00:20,10 --> 00:00:23,50
to hold all of the image objects for us.

10
00:00:23,50 --> 00:00:29,00
So, that looks like var, I'll call it imgArray

11
00:00:29,00 --> 00:00:34,10
and we'll initialize it as a new Array.

12
00:00:34,10 --> 00:00:36,70
Then, we can create a loop that will add

13
00:00:36,70 --> 00:00:39,70
all of the image files into individual

14
00:00:39,70 --> 00:00:42,40
image objects inside the array.

15
00:00:42,40 --> 00:00:44,20
So, I'll use a for loop, because I know

16
00:00:44,20 --> 00:00:46,80
how many images I have.

17
00:00:46,80 --> 00:00:51,10
And I'll just say i=0,

18
00:00:51,10 --> 00:00:56,50
i<24,

19
00:00:56,50 --> 00:00:59,80
i++.

20
00:00:59,80 --> 00:01:03,40
And then, inside the loop, we'll just say,

21
00:01:03,40 --> 00:01:08,20
imgArray[i]

22
00:01:08,20 --> 00:01:12,00
is a new Image object.

23
00:01:12,00 --> 00:01:14,00
Now, using an image object is smart,

24
00:01:14,00 --> 00:01:15,80
especially when doing animation,

25
00:01:15,80 --> 00:01:17,20
because it forces the browser

26
00:01:17,20 --> 00:01:19,80
to preload each of the objects.

27
00:01:19,80 --> 00:01:22,10
What that does is it removes all the latency

28
00:01:22,10 --> 00:01:26,50
from transferring from one frame to the next.

29
00:01:26,50 --> 00:01:28,50
Now that we have an image object,

30
00:01:28,50 --> 00:01:35,20
we can take the image object inside the array

31
00:01:35,20 --> 00:01:40,10
and set its source attribute and that will be equal to,

32
00:01:40,10 --> 00:01:44,90
as a string, the path to our images folder

33
00:01:44,90 --> 00:01:50,30
and then append to that the ball prefix,

34
00:01:50,30 --> 00:01:55,10
plus whatever is the current i variable,

35
00:01:55,10 --> 00:01:59,30
and then plus and now we need to finish the file name,

36
00:01:59,30 --> 00:02:03,30
so, that's going to be .gif.

37
00:02:03,30 --> 00:02:08,00
And the result now is image array holds 24 image objects,

38
00:02:08,00 --> 00:02:10,80
each of which has its own source attribute

39
00:02:10,80 --> 00:02:14,90
that points to a frame in our animation.

40
00:02:14,90 --> 00:02:19,10
Okay, directly below that, let's create a variable

41
00:02:19,10 --> 00:02:23,50
that holds the value of our current frame.

42
00:02:23,50 --> 00:02:26,00
So, that starts out at frame zero,

43
00:02:26,00 --> 00:02:29,00
because we're going to be displaying ball zero.

44
00:02:29,00 --> 00:02:30,70
And then, we just need to write the function

45
00:02:30,70 --> 00:02:33,10
that actually does the rotating for us.

46
00:02:33,10 --> 00:02:36,60
So, I'm going to create a function called rotate,

47
00:02:36,60 --> 00:02:39,70
which will take no parameters.

48
00:02:39,70 --> 00:02:42,10
And let's talk about the end of the animation.

49
00:02:42,10 --> 00:02:43,50
We have a couple of choices to make,

50
00:02:43,50 --> 00:02:45,60
we can either just stop the animation

51
00:02:45,60 --> 00:02:48,90
and show the last frame, or we can loop it.

52
00:02:48,90 --> 00:02:51,20
And in this case, I think I'm just going to loop it,

53
00:02:51,20 --> 00:02:55,30
that way the spinning baseball never stops spinning.

54
00:02:55,30 --> 00:03:00,60
So, let's just say, if the counter variable

55
00:03:00,60 --> 00:03:08,60
is greater than imgArray.length-1.

56
00:03:08,60 --> 00:03:11,40
Because remember, the length is always going to be one more

57
00:03:11,40 --> 00:03:13,10
than the last frame.

58
00:03:13,10 --> 00:03:15,50
If that's the case, then what we want to do

59
00:03:15,50 --> 00:03:18,20
is we want to set the counter back to zero

60
00:03:18,20 --> 00:03:21,10
to start the animation over again.

61
00:03:21,10 --> 00:03:23,00
Okay, that's cool.

62
00:03:23,00 --> 00:03:27,40
Now all we have to do is set the next frame

63
00:03:27,40 --> 00:03:30,00
into the image html element.

64
00:03:30,00 --> 00:03:37,10
So, let's do document.getElementById, we named it ball.

65
00:03:37,10 --> 00:03:42,50
And then, we want to set the source attribute, no space,

66
00:03:42,50 --> 00:03:50,60
equal to whatever is in the imageArray's counter spot

67
00:03:50,60 --> 00:03:53,90
and grab the source attribute from there.

68
00:03:53,90 --> 00:03:56,60
Then, once we've done that we can increment

69
00:03:56,60 --> 00:03:58,90
the counter variable.

70
00:03:58,90 --> 00:04:03,30
I misspelled imgArray, it's just img.

71
00:04:03,30 --> 00:04:06,30
And then, we'll do counter++.

72
00:04:06,30 --> 00:04:11,10
And then finally, let's call setTimeout

73
00:04:11,10 --> 00:04:16,20
and we want to call the rotate function

74
00:04:16,20 --> 00:04:20,10
and I'll do 50 milliseconds.

75
00:04:20,10 --> 00:04:21,00
And we'll see what that looks like.

76
00:04:21,00 --> 00:04:22,80
If we need to adjust the delay,

77
00:04:22,80 --> 00:04:26,50
we can always make that number larger or smaller.

78
00:04:26,50 --> 00:04:31,70
Okay, last thing to do, jump outside of the rotate function

79
00:04:31,70 --> 00:04:33,10
and let's add our event listener

80
00:04:33,10 --> 00:04:34,80
to get the whole thing rolling.

81
00:04:34,80 --> 00:04:40,40
So, we want window.addEventListener.

82
00:04:40,40 --> 00:04:44,60
And we're going to use the load event again.

83
00:04:44,60 --> 00:04:48,20
And from there we're going to call a rotate function.

84
00:04:48,20 --> 00:04:51,00
And please, just remember that the load event fires

85
00:04:51,00 --> 00:04:53,80
after all of the resources have been downloaded.

86
00:04:53,80 --> 00:04:57,20
And that's important, because we're using the image array,

87
00:04:57,20 --> 00:05:01,30
storing image objects, that means all of the frames

88
00:05:01,30 --> 00:05:03,30
in the animation will be downloaded

89
00:05:03,30 --> 00:05:05,60
before anything gets going.

90
00:05:05,60 --> 00:05:07,90
Okay, so let's save that.

91
00:05:07,90 --> 00:05:12,00
And then finally, let's jump back into our html file

92
00:05:12,00 --> 00:05:15,20
and we need to set a reference

93
00:05:15,20 --> 00:05:18,50
to the script we just created.

94
00:05:18,50 --> 00:05:21,90
So, let's add a script element

95
00:05:21,90 --> 00:05:26,10
set the type=text/javascript and then

96
00:05:26,10 --> 00:05:32,00
set the src= and it's in the same folder this time,

97
00:05:32,00 --> 00:05:37,50
so it's going to just be replacement.js.

98
00:05:37,50 --> 00:05:41,80
And then, just close the script element.

99
00:05:41,80 --> 00:05:44,00
Okay, save that.

100
00:05:44,00 --> 00:05:45,80
And then, I'm going to actually make sure

101
00:05:45,80 --> 00:05:48,20
that I have the runtime set correctly,

102
00:05:48,20 --> 00:05:51,70
by right clicking the Animation project name,

103
00:05:51,70 --> 00:05:55,30
scroll down to properties, on the Run tab

104
00:05:55,30 --> 00:05:58,50
if you see Chrome with NetBeans Connector,

105
00:05:58,50 --> 00:06:01,50
click on it and choose just Chrome,

106
00:06:01,50 --> 00:06:08,30
make sure auto-refresh is still checked, then click OK.

107
00:06:08,30 --> 00:06:12,30
And now, we can right click the replacement.html file

108
00:06:12,30 --> 00:06:15,70
and choose Run File.

109
00:06:15,70 --> 00:06:17,80
Okay, I'd call that a successful test.

110
00:06:17,80 --> 00:06:19,70
Notice that the ball is spinning,

111
00:06:19,70 --> 00:06:23,10
but remember, all we're doing is cycling through

112
00:06:23,10 --> 00:06:24,80
all of the frames in the animation.

113
00:06:24,80 --> 00:06:27,30
It's not really spinning, we're just showing

114
00:06:27,30 --> 00:06:29,00
a different picture of the ball.

