1
00:00:00,50 --> 00:00:02,70
- [Instructor] Let's see if we can make a few modifications

2
00:00:02,70 --> 00:00:04,80
to the point to point animation

3
00:00:04,80 --> 00:00:07,10
so that it can be useful for our game.

4
00:00:07,10 --> 00:00:10,40
I'm going to try and simulate the ball hitting a boundary.

5
00:00:10,40 --> 00:00:13,00
So the first thing to do is to add a couple of variables

6
00:00:13,00 --> 00:00:16,40
to hold the distance the ball travels between the frames.

7
00:00:16,40 --> 00:00:18,00
So I'm going to drop those right underneath

8
00:00:18,00 --> 00:00:20,90
my current x and current y values.

9
00:00:20,90 --> 00:00:26,90
Now let's do var dx and we'll set that equal to five

10
00:00:26,90 --> 00:00:30,20
and then we'll do the same thing for a dy variable

11
00:00:30,20 --> 00:00:33,00
and we'll set that equal to five as well.

12
00:00:33,00 --> 00:00:36,10
Okay, then jump down into the animate function

13
00:00:36,10 --> 00:00:39,60
and instead of hard coding the increment

14
00:00:39,60 --> 00:00:43,30
to current x and current y to five,

15
00:00:43,30 --> 00:00:46,60
let's replace that with

16
00:00:46,60 --> 00:00:50,60
dx and dy.

17
00:00:50,60 --> 00:00:54,40
Okay, finally, if we modify the end conditions,

18
00:00:54,40 --> 00:00:56,80
we can simulate the ball touching a barrier

19
00:00:56,80 --> 00:01:00,30
and bouncing back by simply changing the sign

20
00:01:00,30 --> 00:01:02,80
of dx and dy.

21
00:01:02,80 --> 00:01:05,10
Now, this barrier will be invisible

22
00:01:05,10 --> 00:01:08,20
but you'll get the idea once you see the animation running.

23
00:01:08,20 --> 00:01:11,50
Let's jump down into the end conditions and rewrite them.

24
00:01:11,50 --> 00:01:14,20
So I'm going to ditch this entire conditional

25
00:01:14,20 --> 00:01:17,00
and rewrite the end conditions.

26
00:01:17,00 --> 00:01:18,80
So here's how I think it should go.

27
00:01:18,80 --> 00:01:20,70
I'm going to say

28
00:01:20,70 --> 00:01:24,30
if current x

29
00:01:24,30 --> 00:01:27,30
is greater than 800.

30
00:01:27,30 --> 00:01:31,80
That hasn't changed but what I'm also going to say is

31
00:01:31,80 --> 00:01:33,30
or

32
00:01:33,30 --> 00:01:35,30
current x

33
00:01:35,30 --> 00:01:40,20
is less than 100

34
00:01:40,20 --> 00:01:42,20
and that's my first conditional.

35
00:01:42,20 --> 00:01:45,30
That's going to represent the boundary in the x direction

36
00:01:45,30 --> 00:01:47,40
or the left and right direction.

37
00:01:47,40 --> 00:01:49,70
Then I'm going to have an or sign

38
00:01:49,70 --> 00:01:52,60
and then I'm going to have another set of parenthesis

39
00:01:52,60 --> 00:01:56,30
and I'm going to say current y

40
00:01:56,30 --> 00:02:00,10
is greater than 600

41
00:02:00,10 --> 00:02:02,20
or

42
00:02:02,20 --> 00:02:04,70
current y

43
00:02:04,70 --> 00:02:07,90
is less than 100.

44
00:02:07,90 --> 00:02:09,80
And what that's going to do is it going to take

45
00:02:09,80 --> 00:02:13,20
care of the ball's up and down movement.

46
00:02:13,20 --> 00:02:17,30
So, I have four conditionals,

47
00:02:17,30 --> 00:02:20,30
any one of which can be true.

48
00:02:20,30 --> 00:02:24,30
And then what I want to do is I want to change the sign

49
00:02:24,30 --> 00:02:26,20
of dx and dy.

50
00:02:26,20 --> 00:02:28,80
So let's do dx

51
00:02:28,80 --> 00:02:30,80
times equals

52
00:02:30,80 --> 00:02:32,10
minus one.

53
00:02:32,10 --> 00:02:35,10
And we'll do the same thing for dy.

54
00:02:35,10 --> 00:02:37,40
Now conceptually if you think about what this is happening,

55
00:02:37,40 --> 00:02:41,10
it just changed the direction of the ball.

56
00:02:41,10 --> 00:02:43,30
If we change the value of dy,

57
00:02:43,30 --> 00:02:46,70
from positive to negative instead of moving from

58
00:02:46,70 --> 00:02:49,10
top to bottom, it'll move from bottom to top

59
00:02:49,10 --> 00:02:51,80
and the same thing is analogous with dx

60
00:02:51,80 --> 00:02:54,40
relative to horizontal movement.

61
00:02:54,40 --> 00:02:57,10
So let's save that

62
00:02:57,10 --> 00:03:01,30
and then we can go look at our animation

63
00:03:01,30 --> 00:03:04,60
and we can see now we're hitting a boundary.

64
00:03:04,60 --> 00:03:06,30
Now we're cooking.

65
00:03:06,30 --> 00:03:08,10
This is going to be a technique

66
00:03:08,10 --> 00:03:10,90
that we can use in our rebound game

67
00:03:10,90 --> 00:03:12,90
to simulate when the ball touches

68
00:03:12,90 --> 00:03:15,00
the edge of the gaming area.

