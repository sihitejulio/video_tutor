1
00:00:00,50 --> 00:00:02,00
- [Instructor] Let's take a moment to talk about

2
00:00:02,00 --> 00:00:04,70
the JavaScript coordinate system.

3
00:00:04,70 --> 00:00:09,10
The browser's viewable area is the canvas of the page.

4
00:00:09,10 --> 00:00:11,00
The origin of the coordinate system

5
00:00:11,00 --> 00:00:13,70
is the top left corner of the canvas.

6
00:00:13,70 --> 00:00:16,10
This is really the only point on the canvas

7
00:00:16,10 --> 00:00:18,60
that we can be sure exists.

8
00:00:18,60 --> 00:00:21,20
Since users can have varying viewports,

9
00:00:21,20 --> 00:00:23,10
we can't assume any values for things

10
00:00:23,10 --> 00:00:26,50
like the right or bottom edges of the screen.

11
00:00:26,50 --> 00:00:28,40
When thinking of the coordinates,

12
00:00:28,40 --> 00:00:30,90
moving from right to left represents an increment

13
00:00:30,90 --> 00:00:33,20
in the X value of the pair.

14
00:00:33,20 --> 00:00:34,50
This is pretty much the same

15
00:00:34,50 --> 00:00:37,70
as the geometry class you took in school.

16
00:00:37,70 --> 00:00:40,20
The Y values are somewhat counterintuitive,

17
00:00:40,20 --> 00:00:42,70
relative to your geometry class.

18
00:00:42,70 --> 00:00:45,80
Movement from top to bottom represents positive movement

19
00:00:45,80 --> 00:00:48,10
along the Y-axis.

20
00:00:48,10 --> 00:00:49,80
You must keep these things in mind

21
00:00:49,80 --> 00:00:52,00
when moving objects on the screen.

