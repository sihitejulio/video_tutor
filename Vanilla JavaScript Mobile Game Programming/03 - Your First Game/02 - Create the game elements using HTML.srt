1
00:00:00,50 --> 00:00:01,50
- [Instructor] Okay, let's get busy

2
00:00:01,50 --> 00:00:03,60
creating that button chaser game.

3
00:00:03,60 --> 00:00:06,20
Fire up NetBeans if you haven't already,

4
00:00:06,20 --> 00:00:09,30
and then click on the new project button in the toolbar.

5
00:00:09,30 --> 00:00:10,30
It's the one that looks like

6
00:00:10,30 --> 00:00:12,60
a yellow rectangle with a plus sign.

7
00:00:12,60 --> 00:00:13,50
Once you've done that,

8
00:00:13,50 --> 00:00:17,40
choose the HTML five Javascript category,

9
00:00:17,40 --> 00:00:20,90
and then the project is an HTML five JS application.

10
00:00:20,90 --> 00:00:22,40
Click next.

11
00:00:22,40 --> 00:00:25,70
Let's name it button chaser.

12
00:00:25,70 --> 00:00:26,90
Then we'll click next.

13
00:00:26,90 --> 00:00:29,40
For this one we won't need a site template,

14
00:00:29,40 --> 00:00:33,10
nor will we need any tools files.

15
00:00:33,10 --> 00:00:35,50
Once you've done that you can click finish,

16
00:00:35,50 --> 00:00:39,90
and NetBeans will create the project for us.

17
00:00:39,90 --> 00:00:43,10
NetBeans will automatically open the index.html file for us,

18
00:00:43,10 --> 00:00:44,70
but if not, you can always double click it

19
00:00:44,70 --> 00:00:47,40
in the projects window.

20
00:00:47,40 --> 00:00:51,20
Let's change the title to be button chaser,

21
00:00:51,20 --> 00:00:52,90
and then I'll remove the div

22
00:00:52,90 --> 00:00:56,70
that tells me that I need to write content.

23
00:00:56,70 --> 00:00:57,50
Okay.

24
00:00:57,50 --> 00:00:58,80
Inside the body we're going to add

25
00:00:58,80 --> 00:01:01,10
a few divs to keep track of the objects.

26
00:01:01,10 --> 00:01:02,90
So the first one,

27
00:01:02,90 --> 00:01:08,10
let's add a div and give it an ID of score label.

28
00:01:08,10 --> 00:01:10,30
This is where we'll display the score.

29
00:01:10,30 --> 00:01:12,90
The starting point will be score equal to zero,

30
00:01:12,90 --> 00:01:15,30
so I'm just going to write score

31
00:01:15,30 --> 00:01:17,10
and then a colon and then a zero.

32
00:01:17,10 --> 00:01:18,80
We'll add a variable to our script

33
00:01:18,80 --> 00:01:22,40
that actually keeps track of the score.

34
00:01:22,40 --> 00:01:23,40
Directly beneath that,

35
00:01:23,40 --> 00:01:25,80
let's add a page title.

36
00:01:25,80 --> 00:01:28,20
So I'm going to put another div in.

37
00:01:28,20 --> 00:01:33,10
We'll write ID equals page title this time.

38
00:01:33,10 --> 00:01:38,20
And our page title is button chaser.

39
00:01:38,20 --> 00:01:39,80
Next we'll need to add a div

40
00:01:39,80 --> 00:01:41,80
for the playing area of the game.

41
00:01:41,80 --> 00:01:43,70
So I'll add a div.

42
00:01:43,70 --> 00:01:46,60
Let's set the ID for this one to be,

43
00:01:46,60 --> 00:01:49,10
how about, game area.

44
00:01:49,10 --> 00:01:50,80
And then inside the game area,

45
00:01:50,80 --> 00:01:52,20
let's add our button.

46
00:01:52,20 --> 00:01:56,00
And this is the thing that users are going to have to tap,

47
00:01:56,00 --> 00:01:57,70
so let's give that an ID.

48
00:01:57,70 --> 00:02:01,80
And we'll set that equal to dot.

49
00:02:01,80 --> 00:02:03,80
One of the things that I make sure to tell new students

50
00:02:03,80 --> 00:02:05,50
is that anytime you're going to

51
00:02:05,50 --> 00:02:08,10
access an object in your Javascript,

52
00:02:08,10 --> 00:02:09,80
you should give it an ID.

53
00:02:09,80 --> 00:02:12,00
It just makes it a lot easier to access.

54
00:02:12,00 --> 00:02:13,00
So I'll save that.

