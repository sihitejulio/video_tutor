1
00:00:00,50 --> 00:00:02,80
- [Instructor] Okay, let's add that set

2
00:00:02,80 --> 00:00:04,70
game area bounds function.

3
00:00:04,70 --> 00:00:09,10
So, we'll add that, no parameters.

4
00:00:09,10 --> 00:00:11,60
Okay, the first thing to do is to grab the inner

5
00:00:11,60 --> 00:00:14,20
width and inner height properties of the window

6
00:00:14,20 --> 00:00:16,40
object and assign them to our available width

7
00:00:16,40 --> 00:00:22,40
and available height variables.

8
00:00:22,40 --> 00:00:25,10
We'll take the inner width value and assign it

9
00:00:25,10 --> 00:00:28,90
to the available width and then we'll take the

10
00:00:28,90 --> 00:00:36,30
available height and to that we'll assign inner height.

11
00:00:36,30 --> 00:00:38,00
Once we have those values we need to do

12
00:00:38,00 --> 00:00:39,60
a little bit of math on them

13
00:00:39,60 --> 00:00:41,50
to position the screen.

14
00:00:41,50 --> 00:00:44,40
So we have ten pixels from the left edge of the border

15
00:00:44,40 --> 00:00:45,90
in our CSS file.

16
00:00:45,90 --> 00:00:48,10
We need ten pixels on the right and then there's

17
00:00:48,10 --> 00:00:50,90
a one pixel border around the whole thing.

18
00:00:50,90 --> 00:00:58,50
So let's subtract 22 from the available width

19
00:00:58,50 --> 00:01:00,10
and then if we do a little bit of math on the

20
00:01:00,10 --> 00:01:03,50
available height we have to subtract out the height

21
00:01:03,50 --> 00:01:08,10
of the page title and the score label.

22
00:01:08,10 --> 00:01:12,20
So that is 97 or so.

23
00:01:12,20 --> 00:01:14,50
Once we have the available width and the available

24
00:01:14,50 --> 00:01:19,40
height values, we can then assign them to the html objects.

25
00:01:19,40 --> 00:01:25,20
So let's do document.getElementById and we'll be

26
00:01:25,20 --> 00:01:31,80
looking for the game area div and to that we'll

27
00:01:31,80 --> 00:01:37,10
access the style object and then the width property

28
00:01:37,10 --> 00:01:40,00
and we'll set that equal to the available

29
00:01:40,00 --> 00:01:46,40
width and then we need a label and that's pixels.

30
00:01:46,40 --> 00:01:48,40
And we'll do the same thing for the height with

31
00:01:48,40 --> 00:01:50,70
available height let's be smart and we'll do a little

32
00:01:50,70 --> 00:01:54,70
copy and paste only instead of setting the width

33
00:01:54,70 --> 00:02:01,80
we'll set the height and we'll use a height as our value.

34
00:02:01,80 --> 00:02:06,50
Okay, next thing to do is to add the event list in there

35
00:02:06,50 --> 00:02:07,40
for clicks on the button.

36
00:02:07,40 --> 00:02:09,80
This will keep track of the number of times the user

37
00:02:09,80 --> 00:02:11,20
clicks the button.

38
00:02:11,20 --> 00:02:13,80
So first let's get a reference to the button by

39
00:02:13,80 --> 00:02:20,90
doing getElementById and we called that a dot

40
00:02:20,90 --> 00:02:24,10
and then we'll add the event listener just like we

41
00:02:24,10 --> 00:02:27,60
did for the window object in the load event only

42
00:02:27,60 --> 00:02:32,20
this time we'll listen for click events on the dot

43
00:02:32,20 --> 00:02:38,70
and we'll call a detect hit function that we'll

44
00:02:38,70 --> 00:02:41,30
create in just a moment.

45
00:02:41,30 --> 00:02:42,80
In fact let's do that right now.

46
00:02:42,80 --> 00:02:46,30
Just beneath where we did set area bounds

47
00:02:46,30 --> 00:02:50,80
let's add our function called detect hit.

48
00:02:50,80 --> 00:02:54,20
All we have to do when the user clicks the button

49
00:02:54,20 --> 00:02:56,60
is increment the score and display it on the screen.

50
00:02:56,60 --> 00:03:00,00
So incrementing the score is actually pretty simple.

51
00:03:00,00 --> 00:03:03,30
We'll just do score += 1 and then all we have

52
00:03:03,30 --> 00:03:05,60
to do is display it on the screen.

53
00:03:05,60 --> 00:03:12,70
So first we need to get a reference to our score label.

54
00:03:12,70 --> 00:03:16,70
We'll use getElementById for that, get a reference

55
00:03:16,70 --> 00:03:18,90
to the score label and then we'll set

56
00:03:18,90 --> 00:03:21,30
the inner html property.

57
00:03:21,30 --> 00:03:26,30
And that's going to be as a string, the word score and a colon

58
00:03:26,30 --> 00:03:28,60
and then a space, and then whatever

59
00:03:28,60 --> 00:03:31,50
is in our score variable.

60
00:03:31,50 --> 00:03:36,10
Okay, finally in the set game area bounds function,

61
00:03:36,10 --> 00:03:38,50
let's start the game loop and we'll do that

62
00:03:38,50 --> 00:03:42,00
in a function called move dot which we'll write

63
00:03:42,00 --> 00:03:43,00
in just a second.

