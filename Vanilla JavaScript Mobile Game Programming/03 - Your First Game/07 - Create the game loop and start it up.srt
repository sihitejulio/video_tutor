1
00:00:00,50 --> 00:00:02,10
- Okay there's not much left to do

2
00:00:02,10 --> 00:00:03,80
in our moveDot function

3
00:00:03,80 --> 00:00:07,30
except for setting the end conditions

4
00:00:07,30 --> 00:00:11,00
and then getting the game to chug along as needed.

5
00:00:11,00 --> 00:00:12,50
So I'm going to use thirty seconds

6
00:00:12,50 --> 00:00:15,10
as arbitrary end to the game,

7
00:00:15,10 --> 00:00:17,40
so let's add a simple conditional statement,

8
00:00:17,40 --> 00:00:21,30
and I'll say if iterations

9
00:00:21,30 --> 00:00:24,00
is less than 30,

10
00:00:24,00 --> 00:00:25,90
that means the game is still going

11
00:00:25,90 --> 00:00:29,60
and we should call our move dot function again.

12
00:00:29,60 --> 00:00:34,10
So I'm going to set the timer object equal to

13
00:00:34,10 --> 00:00:36,60
a call to set timeout,

14
00:00:36,60 --> 00:00:38,90
and then set timeout takes two parameters,

15
00:00:38,90 --> 00:00:42,90
the first one is the code that you want to execute,

16
00:00:42,90 --> 00:00:45,50
and we want to call move dot again,

17
00:00:45,50 --> 00:00:48,80
and then how long to wait a delay in milliseconds,

18
00:00:48,80 --> 00:00:52,00
so since we want the dot to move once per second,

19
00:00:52,00 --> 00:00:56,00
I'll put in 1000 milliseconds.

20
00:00:56,00 --> 00:00:58,30
Okay let's just not make the mistake

21
00:00:58,30 --> 00:01:01,00
of thinking that set timeout is recursive,

22
00:01:01,00 --> 00:01:03,80
because it appears that we're calling the moveDot function

23
00:01:03,80 --> 00:01:05,70
from within the moveDot function,

24
00:01:05,70 --> 00:01:07,00
that would be recursive,

25
00:01:07,00 --> 00:01:09,20
but set timeout doesn't work that way,

26
00:01:09,20 --> 00:01:11,50
set timeout returns immediately

27
00:01:11,50 --> 00:01:14,20
and tells the browser to wait the specified delay,

28
00:01:14,20 --> 00:01:16,00
and then execute the code.

29
00:01:16,00 --> 00:01:17,70
In this case though,

30
00:01:17,70 --> 00:01:19,90
we need to add an else clause

31
00:01:19,90 --> 00:01:22,20
for when the game is actually over,

32
00:01:22,20 --> 00:01:23,70
and in that case what we want to do

33
00:01:23,70 --> 00:01:26,70
is just simply notify the user the game has ended,

34
00:01:26,70 --> 00:01:29,60
and then not allow them to click the button,

35
00:01:29,60 --> 00:01:31,10
and to clear the timer.

36
00:01:31,10 --> 00:01:36,60
So let's do document dot get element by ID,

37
00:01:36,60 --> 00:01:39,00
and we'll modify the score label

38
00:01:39,00 --> 00:01:41,30
to tell the user that the game is over,

39
00:01:41,30 --> 00:01:45,90
so add a reference to the score label,

40
00:01:45,90 --> 00:01:49,90
and then we'll modify the inner HTML property,

41
00:01:49,90 --> 00:01:51,10
and I'm going to append

42
00:01:51,10 --> 00:01:52,70
so that'll be plus equals,

43
00:01:52,70 --> 00:01:54,30
and then as a string,

44
00:01:54,30 --> 00:01:58,80
maybe five spaces and then game over,

45
00:01:58,80 --> 00:02:00,10
and I'll add an exclamation point

46
00:02:00,10 --> 00:02:02,20
so they know it's important.

47
00:02:02,20 --> 00:02:04,60
Okay, now once we've done that,

48
00:02:04,60 --> 00:02:05,50
we don't want the user

49
00:02:05,50 --> 00:02:07,20
to be able to keep clicking the button

50
00:02:07,20 --> 00:02:08,70
after the game is over,

51
00:02:08,70 --> 00:02:13,40
so let's remove the click event from the dot.

52
00:02:13,40 --> 00:02:19,50
So first let's do document dot get element by ID,

53
00:02:19,50 --> 00:02:21,80
ask for the dot,

54
00:02:21,80 --> 00:02:25,30
and then remove event listener,

55
00:02:25,30 --> 00:02:26,80
again, two parameters,

56
00:02:26,80 --> 00:02:29,40
we want to remove the click listeners,

57
00:02:29,40 --> 00:02:32,20
so that's the event we're looking for,

58
00:02:32,20 --> 00:02:35,50
and then the code that we don't want to call anymore,

59
00:02:35,50 --> 00:02:39,90
that was detect hit,

60
00:02:39,90 --> 00:02:45,40
and then finally, we want to call clear timeout,

61
00:02:45,40 --> 00:02:48,00
and we want to clear the timer.

62
00:02:48,00 --> 00:02:50,70
So that means either the game is still running,

63
00:02:50,70 --> 00:02:54,90
and we're going to call set timeout moveDot each second,

64
00:02:54,90 --> 00:02:56,70
or the game has ended,

65
00:02:56,70 --> 00:02:58,50
so the only thing left to do now,

66
00:02:58,50 --> 00:03:03,20
is to make sure we increment the iterations variable.

67
00:03:03,20 --> 00:03:06,00
And I'll do that outside the else clause

68
00:03:06,00 --> 00:03:09,10
by calling iterations plus plus,

69
00:03:09,10 --> 00:03:12,20
but you may have noticed that I made a mistake

70
00:03:12,20 --> 00:03:13,60
a few videos back,

71
00:03:13,60 --> 00:03:16,10
when I declared the iterations variable,

72
00:03:16,10 --> 00:03:18,40
I didn't give it an initial value

73
00:03:18,40 --> 00:03:20,20
and what that does in JavaScript

74
00:03:20,20 --> 00:03:24,00
is it doesn't apply a type to the iterations variable.

75
00:03:24,00 --> 00:03:26,90
Since I want to do some math to the iterations variable,

76
00:03:26,90 --> 00:03:28,40
I need to make it a number,

77
00:03:28,40 --> 00:03:31,80
what I'll do is I'll do that by assigning an initial value

78
00:03:31,80 --> 00:03:34,30
to iterations and I'll set it equal to zero.

79
00:03:34,30 --> 00:03:37,10
Essentially the iterations keeps track of the frames

80
00:03:37,10 --> 00:03:38,20
in the game.

81
00:03:38,20 --> 00:03:41,80
We start on frame zero and then we increment once per second

82
00:03:41,80 --> 00:03:45,00
until we get to 30 frames,

83
00:03:45,00 --> 00:03:48,30
so let's save that and then finally we need to add

84
00:03:48,30 --> 00:03:51,20
a reference to the script on our HTML page,

85
00:03:51,20 --> 00:03:53,90
so we'll go back to index dot HTML,

86
00:03:53,90 --> 00:03:57,90
and just below where we linked to the style sheet,

87
00:03:57,90 --> 00:04:00,70
let's add a script element,

88
00:04:00,70 --> 00:04:05,90
let's set the type equal to text/JavaScript,

89
00:04:05,90 --> 00:04:07,40
and then the source,

90
00:04:07,40 --> 00:04:09,30
we'll set that equal to

91
00:04:09,30 --> 00:04:14,50
our button chaser dot JS file,

92
00:04:14,50 --> 00:04:17,40
and then you're not allowed to have empty script elements,

93
00:04:17,40 --> 00:04:21,80
so we need a closing script tag.

94
00:04:21,80 --> 00:04:25,90
Okay, when I save that and then go look at the game,

95
00:04:25,90 --> 00:04:28,20
we now can see the dot bouncing along

96
00:04:28,20 --> 00:04:30,40
and we can click and increment the score,

97
00:04:30,40 --> 00:04:32,50
it's not easy if the dot doesn't come near

98
00:04:32,50 --> 00:04:33,40
the middle of the screen,

99
00:04:33,40 --> 00:04:36,00
in a large screen,

100
00:04:36,00 --> 00:04:50,90
let's just make sure it ends within thirty seconds,

101
00:04:50,90 --> 00:04:53,10
and there's our game over message.

102
00:04:53,10 --> 00:04:54,40
No more clicks on the button,

103
00:04:54,40 --> 00:04:55,50
that's a good thing.

104
00:04:55,50 --> 00:04:59,00
Congratulations, you are now a JavaScript game programmer.

