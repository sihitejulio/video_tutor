1
00:00:00,50 --> 00:00:01,90
- [Instructor] Okay let's get this party started.

2
00:00:01,90 --> 00:00:04,80
It's time to add a java script file to the project.

3
00:00:04,80 --> 00:00:06,80
So right click the button chaser project,

4
00:00:06,80 --> 00:00:10,70
choose new, this time choose java script file.

5
00:00:10,70 --> 00:00:13,30
And again I'll call this one button chaser just like

6
00:00:13,30 --> 00:00:15,20
I did the CSS file.

7
00:00:15,20 --> 00:00:17,30
Remember no extension is needed and net b's

8
00:00:17,30 --> 00:00:18,50
will do that for us.

9
00:00:18,50 --> 00:00:21,80
Then you can go ahead and click finish.

10
00:00:21,80 --> 00:00:24,00
The first thing to do is to add the variables

11
00:00:24,00 --> 00:00:26,30
that keep track of the objects in the game.

12
00:00:26,30 --> 00:00:29,60
So I'll start with a variable called score

13
00:00:29,60 --> 00:00:32,70
and I'll initialize that to be zero because that's

14
00:00:32,70 --> 00:00:35,20
the score at the beginning of the game.

15
00:00:35,20 --> 00:00:37,70
Then I'm going to create two variables for the

16
00:00:37,70 --> 00:00:41,00
available width and the available height.

17
00:00:41,00 --> 00:00:43,80
These things will be determined at run time.

18
00:00:43,80 --> 00:00:47,60
Let's create a timer object that will run the game

19
00:00:47,60 --> 00:00:51,30
loop for us and I'm going to create

20
00:00:51,30 --> 00:00:55,00
a variable for the number of iterations.

21
00:00:55,00 --> 00:00:57,20
This is the number of times that the button will

22
00:00:57,20 --> 00:00:59,20
jump around on the page.

23
00:00:59,20 --> 00:01:03,00
Okay, then finally we're going to add an event listener.

24
00:01:03,00 --> 00:01:05,70
I want to listen for the load event and the load

25
00:01:05,70 --> 00:01:08,10
event fires after all of the resources

26
00:01:08,10 --> 00:01:10,40
for the page are downloaded.

27
00:01:10,40 --> 00:01:16,20
So let's do that by calling window.addEventListener

28
00:01:16,20 --> 00:01:19,90
and that's going to take two parameters.

29
00:01:19,90 --> 00:01:23,00
The first one is the event we're listening to

30
00:01:23,00 --> 00:01:26,10
and that's the load event and then the second one

31
00:01:26,10 --> 00:01:29,90
is the code we want to execute when the load event fires.

32
00:01:29,90 --> 00:01:36,30
So I'm going to create a function called set game area bounds.

33
00:01:36,30 --> 00:01:39,00
And we'll write that in just a moment.

