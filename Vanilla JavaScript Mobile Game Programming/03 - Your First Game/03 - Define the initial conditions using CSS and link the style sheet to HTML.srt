1
00:00:00,50 --> 00:00:01,40
- [Instructor] The next thing to do

2
00:00:01,40 --> 00:00:04,40
is to add a cascading style sheet to the project.

3
00:00:04,40 --> 00:00:06,50
So we're going to right-click the project title

4
00:00:06,50 --> 00:00:10,50
and choose New, and then choose Cascading Style Sheet.

5
00:00:10,50 --> 00:00:12,30
We can name it anything we want,

6
00:00:12,30 --> 00:00:14,80
but buttonChaser certainly makes sense.

7
00:00:14,80 --> 00:00:16,50
You don't need a file extension.

8
00:00:16,50 --> 00:00:18,90
NetBeans will put that in for you.

9
00:00:18,90 --> 00:00:20,70
Go ahead and click Finish.

10
00:00:20,70 --> 00:00:22,10
One of the more important things

11
00:00:22,10 --> 00:00:23,90
that the Cascading Style Sheet does

12
00:00:23,90 --> 00:00:26,50
is it supplies the initial conditions for the game

13
00:00:26,50 --> 00:00:29,20
through positioning the objects on the screen.

14
00:00:29,20 --> 00:00:31,50
Let's run through each of the objects on the screen

15
00:00:31,50 --> 00:00:33,50
and style them individually.

16
00:00:33,50 --> 00:00:36,20
I'll start with the scoreLabel.

17
00:00:36,20 --> 00:00:37,80
So let's set a font-family

18
00:00:37,80 --> 00:00:41,70
and I have that set to Arial,

19
00:00:41,70 --> 00:00:45,90
Helvetica, and then Sans-Serif.

20
00:00:45,90 --> 00:00:51,10
So at the font-size, I have that set to 14 points.

21
00:00:51,10 --> 00:00:52,80
I'm going to use a color.

22
00:00:52,80 --> 00:00:55,40
That's navy.

23
00:00:55,40 --> 00:00:59,90
Set the font-weight to bold.

24
00:00:59,90 --> 00:01:02,30
And then, we'll do some positioning.

25
00:01:02,30 --> 00:01:06,10
So let's first set the position to be absolute.

26
00:01:06,10 --> 00:01:12,60
And then, I'll set the top and left to 10 pixels.

27
00:01:12,60 --> 00:01:14,10
And then finally,

28
00:01:14,10 --> 00:01:19,00
I'm going to give the div a height of 25 pixels.

29
00:01:19,00 --> 00:01:20,70
So the one piece of advice I have for you

30
00:01:20,70 --> 00:01:24,10
when positioning objects on the screen absolutely,

31
00:01:24,10 --> 00:01:25,60
is that a good rule of thumb

32
00:01:25,60 --> 00:01:28,50
is if you position one thing absolutely,

33
00:01:28,50 --> 00:01:32,80
then you should consider positioning everything absolutely.

34
00:01:32,80 --> 00:01:35,60
Okay, next is the pageTitle.

35
00:01:35,60 --> 00:01:38,70
And the font-family for that one is identical

36
00:01:38,70 --> 00:01:42,20
so I'm just going to copy and paste.

37
00:01:42,20 --> 00:01:46,20
This time, the font-size will be 24 points.

38
00:01:46,20 --> 00:01:50,50
The font-weight will be bold.

39
00:01:50,50 --> 00:01:52,90
Color, again navy.

40
00:01:52,90 --> 00:01:55,60
Again, position: absolute.

41
00:01:55,60 --> 00:02:01,00
Only this time, we're going to put the top at 35 pixels,

42
00:02:01,00 --> 00:02:02,70
and that make sense when we realize

43
00:02:02,70 --> 00:02:06,40
that the scoreLabel's top is 10 pixels,

44
00:02:06,40 --> 00:02:08,70
and it has a height of 25 pixels,

45
00:02:08,70 --> 00:02:10,40
so when we add the 10 and the 25 together,

46
00:02:10,40 --> 00:02:13,40
we can calculate the top of the pageTitle.

47
00:02:13,40 --> 00:02:17,90
Set the left to be 10 pixels.

48
00:02:17,90 --> 00:02:24,00
And we can give the height of this one also 25 pixels.

49
00:02:24,00 --> 00:02:28,90
Next object is the gameArea.

50
00:02:28,90 --> 00:02:31,60
Position is absolute.

51
00:02:31,60 --> 00:02:35,30
Top, I have that set to 75 pixels.

52
00:02:35,30 --> 00:02:36,70
It's going to give me a little bit of room

53
00:02:36,70 --> 00:02:40,70
between the pageTitle and the gameArea.

54
00:02:40,70 --> 00:02:43,10
Let's set the left to be 10 pixels.

55
00:02:43,10 --> 00:02:44,80
This will delineate the rectangle

56
00:02:44,80 --> 00:02:48,30
where the button will be displayed.

57
00:02:48,30 --> 00:02:51,40
So that's one pixel,

58
00:02:51,40 --> 00:02:53,80
navy,

59
00:02:53,80 --> 00:02:55,30
solid.

60
00:02:55,30 --> 00:02:57,70
And then the last thing to style is the button.

61
00:02:57,70 --> 00:03:01,10
So, I think we called that dot.

62
00:03:01,10 --> 00:03:05,30
Again, position: absolute.

63
00:03:05,30 --> 00:03:07,30
Initial conditions, it doesn't really matter

64
00:03:07,30 --> 00:03:09,90
where we position the dot 'cause it's going to move,

65
00:03:09,90 --> 00:03:14,90
so we'll start it at top and left each at 25 pixels.

66
00:03:14,90 --> 00:03:19,00
Let's set the background-color to that navy color.

67
00:03:19,00 --> 00:03:29,10
And we'll give it a width and a height of both of 64 pixels.

68
00:03:29,10 --> 00:03:32,30
Save that and we'll be ready to move on.

69
00:03:32,30 --> 00:03:34,70
Next thing and last thing to do with the style sheet

70
00:03:34,70 --> 00:03:38,00
is to link it inside the HTML file.

71
00:03:38,00 --> 00:03:40,80
So click on index.html

72
00:03:40,80 --> 00:03:42,40
and then we'll need to add a link tag

73
00:03:42,40 --> 00:03:44,20
that specifies our CSS file.

74
00:03:44,20 --> 00:03:47,90
So I'm going to move beneath the meta tag,

75
00:03:47,90 --> 00:03:50,40
the second meta tag provided by NetBeans,

76
00:03:50,40 --> 00:03:53,20
and I'm going to add a link element,

77
00:03:53,20 --> 00:03:59,80
and we're going to say the rel, the relation is stylesheet,

78
00:03:59,80 --> 00:04:06,30
and the type is text/css,

79
00:04:06,30 --> 00:04:13,70
and the href is buttonChaser.css.

80
00:04:13,70 --> 00:04:16,70
So now we can save the index.html file

81
00:04:16,70 --> 00:04:19,00
and then we can actually test it.

82
00:04:19,00 --> 00:04:21,10
So to run the file in the browser,

83
00:04:21,10 --> 00:04:25,70
you right-click the file, and choose Run File.

84
00:04:25,70 --> 00:04:28,40
NetBeans needs the Chrome Connector

85
00:04:28,40 --> 00:04:31,80
so I'll have to install that first.

86
00:04:31,80 --> 00:04:33,70
So we'll add to Chrome

87
00:04:33,70 --> 00:04:36,40
and then we'll add the extension.

88
00:04:36,40 --> 00:04:39,70
Okay, now that I have the Chrome Connector installed,

89
00:04:39,70 --> 00:04:42,10
I have to rerun my project.

90
00:04:42,10 --> 00:04:44,50
This might actually fail, watch.

91
00:04:44,50 --> 00:04:48,50
So if you see a message where the file being loaded

92
00:04:48,50 --> 00:04:50,90
and it doesn't run the buttonChaser,

93
00:04:50,90 --> 00:04:53,10
it's because the NetBeans Chrome Connector

94
00:04:53,10 --> 00:04:55,50
has a known glitch on Macs.

95
00:04:55,50 --> 00:04:59,40
So if we go back to our NetBeans project,

96
00:04:59,40 --> 00:05:03,40
we're going to right-click it and choose Properties.

97
00:05:03,40 --> 00:05:05,90
Then, we're going to click on the Run tab,

98
00:05:05,90 --> 00:05:09,40
and we're going to change Chrome with NetBeans Connector

99
00:05:09,40 --> 00:05:12,40
to be just Chrome.

100
00:05:12,40 --> 00:05:15,40
Notice that Auto-refresh is still checked.

101
00:05:15,40 --> 00:05:17,70
That means anytime we make changes in NetBeans,

102
00:05:17,70 --> 00:05:20,30
they will be immediately reflected in Chrome.

103
00:05:20,30 --> 00:05:22,70
So I'll click OK.

104
00:05:22,70 --> 00:05:28,30
And then, I'll have to rerun the project.

105
00:05:28,30 --> 00:05:29,50
And so it's hard to see

106
00:05:29,50 --> 00:05:30,80
because we haven't done any scripting yet.

107
00:05:30,80 --> 00:05:32,30
We haven't laid out the game area,

108
00:05:32,30 --> 00:05:34,80
but you can see the styles applied to the button

109
00:05:34,80 --> 00:05:37,80
and to the page title, and to the score label.

110
00:05:37,80 --> 00:05:39,20
If you look closely though,

111
00:05:39,20 --> 00:05:41,60
you'll see a small blue dot.

112
00:05:41,60 --> 00:05:43,40
That's actually the gaming area,

113
00:05:43,40 --> 00:05:46,40
but since we didn't give it a size, a width, and a height,

114
00:05:46,40 --> 00:05:48,20
it isn't laid out on the screen yet.

115
00:05:48,20 --> 00:05:50,00
We'll take care of that in script.

