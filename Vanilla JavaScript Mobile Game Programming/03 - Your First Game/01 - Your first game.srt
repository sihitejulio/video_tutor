1
00:00:00,50 --> 00:00:02,20
- [Instructor] Before we can implement all the features

2
00:00:02,20 --> 00:00:03,90
of the Rebound game, we need to add

3
00:00:03,90 --> 00:00:05,80
some tools to our toolset.

4
00:00:05,80 --> 00:00:08,10
We need to crawl before we can walk.

5
00:00:08,10 --> 00:00:10,70
Let's start by creating a simple game.

6
00:00:10,70 --> 00:00:12,90
Button Chaser is a Whac-A-Mole type game

7
00:00:12,90 --> 00:00:14,60
where the player needs to tap a button

8
00:00:14,60 --> 00:00:17,20
on screen that is randomly placed.

9
00:00:17,20 --> 00:00:19,00
The button will move once per second,

10
00:00:19,00 --> 00:00:20,90
and the score is increased each time

11
00:00:20,90 --> 00:00:22,30
the button is tapped.

12
00:00:22,30 --> 00:00:24,40
The game ends after 30 seconds,

13
00:00:24,40 --> 00:00:27,10
and the player is informed that the game is over.

14
00:00:27,10 --> 00:00:29,90
In this game, we'll first create the objects needed

15
00:00:29,90 --> 00:00:31,80
to play the game in HTML.

16
00:00:31,80 --> 00:00:34,80
Then, we'll style them appropriately using CSS.

17
00:00:34,80 --> 00:00:37,00
Our script will include variables

18
00:00:37,00 --> 00:00:39,20
to hold the objects needed to play.

19
00:00:39,20 --> 00:00:41,50
It will lay out the playing area on the screen

20
00:00:41,50 --> 00:00:43,20
and start the game loop.

21
00:00:43,20 --> 00:00:45,20
The game loop will be the engine that moves

22
00:00:45,20 --> 00:00:46,40
the button on the screen

23
00:00:46,40 --> 00:00:48,50
and includes the end conditions.

24
00:00:48,50 --> 00:00:51,10
We'll register an event handler with the Window object

25
00:00:51,10 --> 00:00:53,40
to fire the layout routine, and we'll register

26
00:00:53,40 --> 00:00:55,20
an event handler with the button

27
00:00:55,20 --> 00:00:58,00
to keep track of when the user taps it.

