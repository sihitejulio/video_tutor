1
00:00:00,00 --> 00:00:02,50
- [Instructor] Before we write the moveDot function,

2
00:00:02,50 --> 00:00:05,40
let's make sure that the dot stays within the playing area

3
00:00:05,40 --> 00:00:08,20
by subtracting the width of the dot plus the border

4
00:00:08,20 --> 00:00:11,30
from both available width and available height.

5
00:00:11,30 --> 00:00:15,40
Let's do that inside a setGameAreaBounds function,

6
00:00:15,40 --> 00:00:18,40
just before we call moveDot.

7
00:00:18,40 --> 00:00:24,30
So let's set available width, and subtract 74 pixels

8
00:00:24,30 --> 00:00:29,60
from that, 64 pixels for the width of the button,

9
00:00:29,60 --> 00:00:34,10
and then 10 pixels as a border around the playing area.

10
00:00:34,10 --> 00:00:37,30
And I'll do the same for available height.

11
00:00:37,30 --> 00:00:38,90
Okay, after we've got that done,

12
00:00:38,90 --> 00:00:43,70
let's add the moveDot function.

13
00:00:43,70 --> 00:00:45,80
So the first thing to do is to calculate

14
00:00:45,80 --> 00:00:48,80
the next x, y coordinates of the dot.

15
00:00:48,80 --> 00:00:50,60
So I'm going to create some interim variables.

16
00:00:50,60 --> 00:00:55,10
The first one I'll do is x, and I'll set that equal to,

17
00:00:55,10 --> 00:00:58,00
so I want a random number between zero

18
00:00:58,00 --> 00:01:00,60
and the available width, so let's do this,

19
00:01:00,60 --> 00:01:10,20
we'll say Math.floor, and to that we'll pass in Math.random,

20
00:01:10,20 --> 00:01:13,20
and we'll multiply that times whatever's

21
00:01:13,20 --> 00:01:16,60
in the available width.

22
00:01:16,60 --> 00:01:18,90
And then let's do the same thing for another variable

23
00:01:18,90 --> 00:01:23,50
called y, with respect to available height.

24
00:01:23,50 --> 00:01:26,50
So I'm going to copy and paste,

25
00:01:26,50 --> 00:01:30,00
and I'll change the variable name from x to y,

26
00:01:30,00 --> 00:01:36,10
and instead of available width, we'll do available height.

27
00:01:36,10 --> 00:01:38,40
Okay, I'm going to do a simple conditional statement

28
00:01:38,40 --> 00:01:41,10
to make sure the dot stays within the gaming area.

29
00:01:41,10 --> 00:01:44,30
Now, we've already taken care of the right and the bottom

30
00:01:44,30 --> 00:01:48,20
by subtracting 74 from available width and available height

31
00:01:48,20 --> 00:01:51,40
in setGameAreaBounds, but we now need to take care

32
00:01:51,40 --> 00:01:52,80
of the top and the left.

33
00:01:52,80 --> 00:01:54,60
So all I'm going to do, it's pretty simple,

34
00:01:54,60 --> 00:02:01,80
is say, if x is less than 10, I'm going to set it equal to 10.

35
00:02:01,80 --> 00:02:06,40
So we'll just assign 10 to the x variable,

36
00:02:06,40 --> 00:02:09,50
and I'll do the same thing for the y variable.

37
00:02:09,50 --> 00:02:19,80
So if y is less than 10, let's assign 10 to y.

38
00:02:19,80 --> 00:02:23,60
So now that we know we have valid values for x and y,

39
00:02:23,60 --> 00:02:26,70
all that's left to do is to assign those values

40
00:02:26,70 --> 00:02:29,90
to the top and left properties of the dot.

41
00:02:29,90 --> 00:02:36,80
So we'll do that by using document.getElementById,

42
00:02:36,80 --> 00:02:45,00
we'll refer to the dot, then access the style property,

43
00:02:45,00 --> 00:02:49,40
and then the left property, and we'll set that

44
00:02:49,40 --> 00:02:52,30
equal to whatever is in our x variable,

45
00:02:52,30 --> 00:02:57,40
and then append to that our label, pixels.

46
00:02:57,40 --> 00:03:01,80
Let's do the same thing for the top property

47
00:03:01,80 --> 00:03:05,20
with respects to the y value.

48
00:03:05,20 --> 00:03:07,40
So I'll copy and paste in,

49
00:03:07,40 --> 00:03:10,10
only instead of setting the left property,

50
00:03:10,10 --> 00:03:14,20
we'll do the top property, and instead of the x value,

51
00:03:14,20 --> 00:03:17,30
we'll use the y value.

52
00:03:17,30 --> 00:03:20,50
Okay, that will move the dot around on the screen randomly,

53
00:03:20,50 --> 00:03:22,60
within the gaming area.

54
00:03:22,60 --> 00:03:25,10
Next, we'll get this party started

55
00:03:25,10 --> 00:03:27,00
by implementing the game loop.

