1
00:00:00,50 --> 00:00:02,10
- [Instructor] Nearly every computer program

2
00:00:02,10 --> 00:00:04,60
ever written accepts input from the user,

3
00:00:04,60 --> 00:00:08,30
processes that input in some way, produces some output,

4
00:00:08,30 --> 00:00:11,20
and provides feedback to the user.

5
00:00:11,20 --> 00:00:13,20
Game programming is no different.

6
00:00:13,20 --> 00:00:16,30
Usually, game programs create event listeners

7
00:00:16,30 --> 00:00:19,30
that listen for user input via touch, the mouse,

8
00:00:19,30 --> 00:00:23,00
or keyboard, and a game loop that manages processing.

9
00:00:23,00 --> 00:00:25,20
Once launched, the game loop continues

10
00:00:25,20 --> 00:00:27,40
until it encounters some end condition.

11
00:00:27,40 --> 00:00:29,00
If end conditions aren't met,

12
00:00:29,00 --> 00:00:32,50
objects are rendered to the screen and the game continues.

13
00:00:32,50 --> 00:00:35,50
In this course, we'll create a simple game called Rebound

14
00:00:35,50 --> 00:00:37,60
that should get you started with game programming

15
00:00:37,60 --> 00:00:39,50
using Vanilla JavaScript.

16
00:00:39,50 --> 00:00:41,50
Rebound is a ball and paddle game

17
00:00:41,50 --> 00:00:44,70
where the user controls a paddle and must keep a moving ball

18
00:00:44,70 --> 00:00:46,40
from getting beyond the paddle.

19
00:00:46,40 --> 00:00:49,10
We'll cover how to gather input via the keyboard,

20
00:00:49,10 --> 00:00:51,70
the mouse, and through touch events on mobile.

21
00:00:51,70 --> 00:00:53,80
We'll also look at how to implement changes

22
00:00:53,80 --> 00:00:55,30
to the level of difficulty

23
00:00:55,30 --> 00:00:58,70
and how to intelligently add sound to the game.

24
00:00:58,70 --> 00:01:00,50
Game programming forces developers

25
00:01:00,50 --> 00:01:04,20
to examine code optimization for the best user experience,

26
00:01:04,20 --> 00:01:07,00
something all web developers should consider.

27
00:01:07,00 --> 00:01:08,70
Rebound is no different.

28
00:01:08,70 --> 00:01:10,70
In this course, we'll cover techniques

29
00:01:10,70 --> 00:01:14,50
for optimizing the Document Object Model, or DOM.

30
00:01:14,50 --> 00:01:16,60
We'll also look at how optimizing conditional statements

31
00:01:16,60 --> 00:01:19,00
can result in better performance.

32
00:01:19,00 --> 00:01:21,50
And finally, we'll look at some animation techniques

33
00:01:21,50 --> 00:01:24,00
that can be used in any web setting.

