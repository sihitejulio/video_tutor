1
00:00:00,30 --> 00:00:02,00
- For developers, choosing an editor

2
00:00:02,00 --> 00:00:04,40
is an extremely personal decision.

3
00:00:04,40 --> 00:00:07,00
I use NetBeans for all of my programming classes

4
00:00:07,00 --> 00:00:10,20
with the exception of Android and iOS programming.

5
00:00:10,20 --> 00:00:11,00
Here's why.

6
00:00:11,00 --> 00:00:13,30
NetBeans is free and open source.

7
00:00:13,30 --> 00:00:16,30
Students can download and install NetBeans,

8
00:00:16,30 --> 00:00:19,10
and NCC can install NetBeans in computer labs

9
00:00:19,10 --> 00:00:21,10
free of charge.

10
00:00:21,10 --> 00:00:22,80
NetBeans is a Java application

11
00:00:22,80 --> 00:00:25,90
so it runs on just about any platform Java runs on,

12
00:00:25,90 --> 00:00:29,90
including Windows, Mac OS 10, and Linux.

13
00:00:29,90 --> 00:00:33,80
NetBeans provides specialized editors for all the file types

14
00:00:33,80 --> 00:00:35,40
needed in our programming classes.

15
00:00:35,40 --> 00:00:39,10
These include HTML, CSS, and JavaScript,

16
00:00:39,10 --> 00:00:43,70
as well as Java, SQL, PHP, XML, and more.

17
00:00:43,70 --> 00:00:46,60
NetBeans uses a project-based metaphor,

18
00:00:46,60 --> 00:00:49,70
helping students to separate their projects and assignments

19
00:00:49,70 --> 00:00:52,20
into logical, discrete pieces.

20
00:00:52,20 --> 00:00:55,80
NetBeans provides all the features of a full-blown IDE,

21
00:00:55,80 --> 00:00:59,20
including code completion, syntax highlighting,

22
00:00:59,20 --> 00:01:01,80
and real-time debugging.

23
00:01:01,80 --> 00:01:04,90
You can download NetBeans from NetBeans.org.

24
00:01:04,90 --> 00:01:08,30
Just click on the download link.

25
00:01:08,30 --> 00:01:10,50
Recently, NetBeans has been turned over

26
00:01:10,50 --> 00:01:12,80
to the Apache Software Foundation.

27
00:01:12,80 --> 00:01:14,10
They provide some newer versions,

28
00:01:14,10 --> 00:01:16,20
but I've run into trouble using them.

29
00:01:16,20 --> 00:01:19,60
For this course, I'll be using version 8.2,

30
00:01:19,60 --> 00:01:23,00
the last version released before the migration to Apache,

31
00:01:23,00 --> 00:01:28,20
which you can access by scrolling down.

32
00:01:28,20 --> 00:01:31,00
Just make sure you download a bundle

33
00:01:31,00 --> 00:01:34,80
that supports HTML5 JavaScript technologies.

34
00:01:34,80 --> 00:01:38,00
I usually counsel students to download and install

35
00:01:38,00 --> 00:01:41,60
the all-package as it includes most of the tools needed

36
00:01:41,60 --> 00:01:44,00
at NCC in one download.

37
00:01:44,00 --> 00:01:46,90
Also, remember that NetBeans is a Java application

38
00:01:46,90 --> 00:01:50,10
so you'll need a suitable JDK installed

39
00:01:50,10 --> 00:01:52,00
to get NetBeans up and running.

