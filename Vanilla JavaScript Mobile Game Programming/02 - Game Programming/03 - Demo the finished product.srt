1
00:00:00,50 --> 00:00:02,80
- [Instructor] Rebound is a simple paddle and ball game

2
00:00:02,80 --> 00:00:04,30
where the player must keep the ball

3
00:00:04,30 --> 00:00:06,80
from reaching the bottom of the playing area.

4
00:00:06,80 --> 00:00:10,00
The score is reflected at the bottom of the playing area.

5
00:00:10,00 --> 00:00:12,60
As the game progresses, difficulty is increased

6
00:00:12,60 --> 00:00:14,80
by moving the ball more quickly.

7
00:00:14,80 --> 00:00:18,00
Rebound includes some simple collision detection routines

8
00:00:18,00 --> 00:00:20,40
to reward the player for using the middle of the paddle.

9
00:00:20,40 --> 00:00:21,30
(game buzzes)

10
00:00:21,30 --> 00:00:23,40
It also includes some basic sound effects

11
00:00:23,40 --> 00:00:25,50
and a background music track.

12
00:00:25,50 --> 00:00:27,70
A settings panel lets players decide

13
00:00:27,70 --> 00:00:31,00
if they want to hear the sounds and start a new game.

14
00:00:31,00 --> 00:00:33,00
(game bloops)

