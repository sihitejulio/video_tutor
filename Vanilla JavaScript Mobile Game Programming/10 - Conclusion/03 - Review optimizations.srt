1
00:00:00,50 --> 00:00:02,20
- [Instructor] Let's review some of the JavaScript

2
00:00:02,20 --> 00:00:05,10
optimizations we covered starting with conditional

3
00:00:05,10 --> 00:00:06,60
statements.

4
00:00:06,60 --> 00:00:09,90
The logical AND operator is optimized by checking

5
00:00:09,90 --> 00:00:11,40
the first argument.

6
00:00:11,40 --> 00:00:15,10
If it is false, the statement can't be true so the second

7
00:00:15,10 --> 00:00:17,40
argument is evaluated.

8
00:00:17,40 --> 00:00:20,60
The same thing occurs for a logical OR operator.

9
00:00:20,60 --> 00:00:23,60
Only in this case, if the first argument is true

10
00:00:23,60 --> 00:00:26,40
then the statement is true regardless of the value

11
00:00:26,40 --> 00:00:27,80
of the second statement.

12
00:00:27,80 --> 00:00:30,20
So the second statement is skipped.

13
00:00:30,20 --> 00:00:32,70
We can take advantage of this by paying attention

14
00:00:32,70 --> 00:00:36,10
to the order of the operands and conditional statements.

15
00:00:36,10 --> 00:00:39,10
Our code will be faster if we put the condition most likely

16
00:00:39,10 --> 00:00:43,40
to be false first in any logical AND statements

17
00:00:43,40 --> 00:00:46,00
and if we put the condition most likely to be true first

18
00:00:46,00 --> 00:00:49,70
in any logical OR statements.

19
00:00:49,70 --> 00:00:53,50
We realize that although JavaScript is weakly typed,

20
00:00:53,50 --> 00:00:56,30
we needed to avoid asking the browser to switch data types.

21
00:00:56,30 --> 00:01:00,30
CSS properties are strings because of the need

22
00:01:00,30 --> 00:01:03,10
to attach a unit to any measurement.

23
00:01:03,10 --> 00:01:06,10
Since we needed to do some math on the CSS properties

24
00:01:06,10 --> 00:01:09,80
to move objects on the screen, we use numeric data

25
00:01:09,80 --> 00:01:13,30
to store the values and updated the numeric data as needed.

26
00:01:13,30 --> 00:01:18,10
Then we assign the updated values to the CSS properties.

27
00:01:18,10 --> 00:01:21,20
This allowed us to avoid getting the CSS property

28
00:01:21,20 --> 00:01:25,50
as a string, converting it to a number, manipulating it,

29
00:01:25,50 --> 00:01:28,00
and reassigning the new value.

30
00:01:28,00 --> 00:01:31,20
We started the course using the tried and true setTimeout

31
00:01:31,20 --> 00:01:34,70
method to control frame rates in our animations.

32
00:01:34,70 --> 00:01:38,00
What we discovered was that setTimeout wasn't a reliable

33
00:01:38,00 --> 00:01:40,70
way to ensure that frames were drawn when needed.

34
00:01:40,70 --> 00:01:45,60
So we switched to the newer requestAnimationFrame method

35
00:01:45,60 --> 00:01:49,00
to maximize the frame rate of our animations by allowing

36
00:01:49,00 --> 00:01:53,00
the browser to manage when to draw objects on the screen.

37
00:01:53,00 --> 00:01:55,30
This resulted in much higher frame rates

38
00:01:55,30 --> 00:01:58,50
and much smoother animations.

39
00:01:58,50 --> 00:02:01,60
We discussed how accessing the document object model

40
00:02:01,60 --> 00:02:03,90
is an expensive operation.

41
00:02:03,90 --> 00:02:07,70
Each time we access dom objects, we force the browser

42
00:02:07,70 --> 00:02:11,40
to traverse the dom tree until it finds a match.

43
00:02:11,40 --> 00:02:14,60
Instead, we created global variables for the objects

44
00:02:14,60 --> 00:02:17,10
that are reused.

45
00:02:17,10 --> 00:02:20,90
Initialize them with a single call to getElementById

46
00:02:20,90 --> 00:02:23,70
and stored them in global variables.

47
00:02:23,70 --> 00:02:26,60
And then used the variables stored in memory to access

48
00:02:26,60 --> 00:02:29,20
dom object properties.

49
00:02:29,20 --> 00:02:32,00
The optimizations discussed in this course can be applied

50
00:02:32,00 --> 00:02:34,50
to any website development project.

51
00:02:34,50 --> 00:02:36,80
If you are careful about writing the most efficient

52
00:02:36,80 --> 00:02:40,00
code possible, your site will only get better.

