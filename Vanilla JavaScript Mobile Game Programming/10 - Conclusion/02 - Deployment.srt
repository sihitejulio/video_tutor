1
00:00:02,60 --> 00:00:05,40
and respective app store.

2
00:00:05,40 --> 00:00:07,70
If you want to develop high quality apps, your team

3
00:00:07,70 --> 00:00:11,00
will necessarily need to understand the underlying platform

4
00:00:11,00 --> 00:00:13,40
on which they will be deployed.

5
00:00:13,40 --> 00:00:16,00
Deployment is mobile development's dirty little secret.

6
00:00:16,00 --> 00:00:20,00
You'll need to sign your code, provision your app,

7
00:00:20,00 --> 00:00:23,00
and jump through multiple hoops to get your app available

8
00:00:23,00 --> 00:00:24,70
on an app store.

9
00:00:24,70 --> 00:00:27,70
In short, deploying mobile apps to an app store

10
00:00:27,70 --> 00:00:30,80
is not trivial regardless of the platform.

11
00:00:30,80 --> 00:00:33,70
And of course there is that cut of the price each app

12
00:00:33,70 --> 00:00:36,00
store takes out of your pocket.

13
00:00:36,00 --> 00:00:39,10
Web apps are an order of magnitude easier to deploy

14
00:00:39,10 --> 00:00:40,70
than native apps.

15
00:00:40,70 --> 00:00:44,00
Web apps are associated with a URL which is publicly

16
00:00:44,00 --> 00:00:47,10
available, you can share a URL in many ways including

17
00:00:47,10 --> 00:00:49,70
embedding it in an email.

18
00:00:49,70 --> 00:00:53,10
And with the evolution of Progression Web Apps your web app

19
00:00:53,10 --> 00:00:56,60
can be installed on mobile platforms and behave exactly

20
00:00:56,60 --> 00:00:58,00
like a native app.

21
00:00:58,00 --> 00:01:00,40
You can search the course library for my recent

22
00:01:00,40 --> 00:01:03,60
progressive web app course for more information.

23
00:01:03,60 --> 00:01:06,80
If you absolutely must deploy your web app in an app store,

24
00:01:06,80 --> 00:01:10,10
there are tools available to you to convert your web app

25
00:01:10,10 --> 00:01:11,60
into a native app.

26
00:01:11,60 --> 00:01:14,20
Again we have plenty of resources available

27
00:01:14,20 --> 00:01:15,70
in the course library.

28
00:01:15,70 --> 00:01:18,90
You can search for my cross-platform survey course

29
00:01:18,90 --> 00:01:22,20
as well as a course I did on Cordova, the most mature

30
00:01:22,20 --> 00:01:25,00
cross-platform tool available.

