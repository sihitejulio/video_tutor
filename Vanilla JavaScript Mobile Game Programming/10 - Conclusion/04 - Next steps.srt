1
00:00:00,50 --> 00:00:02,40
- [Thomas] Rebound is just a starting point

2
00:00:02,40 --> 00:00:05,10
and it certainly can use some more features.

3
00:00:05,10 --> 00:00:07,50
The most obvious place to improve Rebound

4
00:00:07,50 --> 00:00:09,70
is in making it more difficult.

5
00:00:09,70 --> 00:00:12,10
The only things we did involved changing

6
00:00:12,10 --> 00:00:13,90
the speed of the objects in the game,

7
00:00:13,90 --> 00:00:16,20
namely the ball and the paddle.

8
00:00:16,20 --> 00:00:19,20
You might want to explore ways to increase the difficulty

9
00:00:19,20 --> 00:00:22,30
without moving the ball so fast that it's unfair.

10
00:00:22,30 --> 00:00:24,80
So, for example, it might be cool

11
00:00:24,80 --> 00:00:27,00
after a while to add more moving objects

12
00:00:27,00 --> 00:00:29,50
to the screen as the game progresses,

13
00:00:29,50 --> 00:00:31,20
making it more difficult for the player

14
00:00:31,20 --> 00:00:33,50
to keep track of the ball.

15
00:00:33,50 --> 00:00:36,40
Another possible way to make Rebound more playable

16
00:00:36,40 --> 00:00:39,10
is to change the way the score is calculated.

17
00:00:39,10 --> 00:00:41,10
You might add bonuses for keeping the ball

18
00:00:41,10 --> 00:00:43,60
in play longer, for example.

19
00:00:43,60 --> 00:00:45,80
Or you might add more objects to the screen

20
00:00:45,80 --> 00:00:48,60
to force the user to think about strategies.

21
00:00:48,60 --> 00:00:50,50
Those things might drop objects

22
00:00:50,50 --> 00:00:52,80
that either increase or decrease the score,

23
00:00:52,80 --> 00:00:55,40
or even change the size of the paddle.

24
00:00:55,40 --> 00:00:57,20
You might even consider adding more

25
00:00:57,20 --> 00:00:59,60
than one ball in play at a time.

26
00:00:59,60 --> 00:01:02,20
In short, adding these dynamic objects

27
00:01:02,20 --> 00:01:05,10
will only make the game more playable.

28
00:01:05,10 --> 00:01:08,40
Finally, you might add objects to the playing area

29
00:01:08,40 --> 00:01:12,20
that modify the trajectory of the ball while it's in play.

30
00:01:12,20 --> 00:01:14,40
You could then change the behavior of the ball

31
00:01:14,40 --> 00:01:17,10
after colliding with one of these objects.

32
00:01:17,10 --> 00:01:19,60
This could also lead to a leveling system

33
00:01:19,60 --> 00:01:21,20
where you could design levels

34
00:01:21,20 --> 00:01:24,60
and ask players to complete them sequentially.

35
00:01:24,60 --> 00:01:27,20
Playing around with games is fun for every coder,

36
00:01:27,20 --> 00:01:28,80
but I suspect that most viewers

37
00:01:28,80 --> 00:01:31,50
aren't game programmers, and that's okay.

38
00:01:31,50 --> 00:01:33,30
We can all use the things we learn

39
00:01:33,30 --> 00:01:36,20
while game programming to do our jobs better.

40
00:01:36,20 --> 00:01:37,70
I hope this course helped point you

41
00:01:37,70 --> 00:01:40,30
in the right direction to write better code

42
00:01:40,30 --> 00:01:42,70
and you had some fun along the way.

43
00:01:42,70 --> 00:01:45,30
Take a look at every conditional statement you write

44
00:01:45,30 --> 00:01:48,50
and try to make it as efficient as possible.

45
00:01:48,50 --> 00:01:50,50
Try to avoid swapping data types.

46
00:01:50,50 --> 00:01:53,30
You know the deal, avoid getting a string,

47
00:01:53,30 --> 00:01:55,90
converting it to a number, doing some math,

48
00:01:55,90 --> 00:01:58,40
and then converting it back to a string.

49
00:01:58,40 --> 00:02:00,60
Instead, keep some numbers handy

50
00:02:00,60 --> 00:02:03,70
to do the math without the conversion.

51
00:02:03,70 --> 00:02:05,90
Whenever possible, allow the browser

52
00:02:05,90 --> 00:02:07,80
to help you when it can.

53
00:02:07,80 --> 00:02:09,50
Stay on top of browser developments

54
00:02:09,50 --> 00:02:11,60
so that you'll know when things get better,

55
00:02:11,60 --> 00:02:15,70
and more importantly, how to use the new techniques.

56
00:02:15,70 --> 00:02:18,30
And finally, if you take nothing else away

57
00:02:18,30 --> 00:02:21,30
from this class, remember that accessing the DOM

58
00:02:21,30 --> 00:02:23,10
is an expensive operation.

59
00:02:23,10 --> 00:02:25,60
Try to do it as little as possible.

60
00:02:25,60 --> 00:02:28,00
Instead, create global variables

61
00:02:28,00 --> 00:02:30,30
that hold references to DOM objects

62
00:02:30,30 --> 00:02:32,70
and use the global variables when needed.

63
00:02:32,70 --> 00:02:34,70
Thanks for tagging along in the course.

64
00:02:34,70 --> 00:02:37,00
I wish you the best and happy coding.

