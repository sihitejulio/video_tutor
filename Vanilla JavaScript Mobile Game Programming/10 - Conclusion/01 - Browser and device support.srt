1
00:00:00,50 --> 00:00:02,80
- [Announcer] Developing native apps for mobile platforms

2
00:00:02,80 --> 00:00:04,30
can be frustrating.

3
00:00:04,30 --> 00:00:05,30
You need to be proficient

4
00:00:05,30 --> 00:00:08,10
in three different object-oriented languages,

5
00:00:08,10 --> 00:00:10,60
master various different IDEs,

6
00:00:10,60 --> 00:00:13,00
and maintain different code bases.

7
00:00:13,00 --> 00:00:15,40
Android apps are written in Java or Kotlin

8
00:00:15,40 --> 00:00:19,40
using Google's Android Studio as a development tool.

9
00:00:19,40 --> 00:00:22,50
iOS apps are written in Objective-C or Swift

10
00:00:22,50 --> 00:00:25,50
using Apple's Xcode IDE.

11
00:00:25,50 --> 00:00:27,40
In addition, you'll need to support

12
00:00:27,40 --> 00:00:30,30
a variety of form factors and screen sizes,

13
00:00:30,30 --> 00:00:33,60
each of which may have multiple input mechanisms.

14
00:00:33,60 --> 00:00:37,00
When you throw in the need to support desktops and laptops,

15
00:00:37,00 --> 00:00:40,50
the job can get even more unmanageable.

16
00:00:40,50 --> 00:00:42,50
Smart developers will realize

17
00:00:42,50 --> 00:00:44,80
that all mobile and desktop platforms

18
00:00:44,80 --> 00:00:47,00
include a capable web browser.

19
00:00:47,00 --> 00:00:52,20
Android has Chrome, iOS has Safari, Windows has Edge,

20
00:00:52,20 --> 00:00:55,20
and you can always install a third-party browser,

21
00:00:55,20 --> 00:00:58,40
such as Firefox or Opera.

22
00:00:58,40 --> 00:00:59,80
Capable browsers means

23
00:00:59,80 --> 00:01:02,10
that developers can use web technologies

24
00:01:02,10 --> 00:01:06,10
to deliver cross-platform apps with near-native speed.

25
00:01:06,10 --> 00:01:09,10
Using HTML, CSS, and JavaScript,

26
00:01:09,10 --> 00:01:11,90
web developers have been supporting different view ports

27
00:01:11,90 --> 00:01:14,30
since the very beginning of the web.

28
00:01:14,30 --> 00:01:17,10
The latest responsive techniques make it possible

29
00:01:17,10 --> 00:01:21,00
to support multiple form factors relatively easy.

30
00:01:21,00 --> 00:01:23,70
I would argue that using web technologies

31
00:01:23,70 --> 00:01:27,00
to target mobile platforms is the most efficient strategy.

