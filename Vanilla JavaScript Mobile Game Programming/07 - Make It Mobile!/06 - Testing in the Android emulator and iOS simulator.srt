1
00:00:00,50 --> 00:00:02,00
- [Instructor] Chrome does a nice job of showing

2
00:00:02,00 --> 00:00:05,20
what your web pages will look like on multiple devices,

3
00:00:05,20 --> 00:00:06,70
but there's no substitute for testing

4
00:00:06,70 --> 00:00:08,80
on the devices themselves.

5
00:00:08,80 --> 00:00:11,00
Since we can't really do that in this course,

6
00:00:11,00 --> 00:00:12,70
the next best thing to do is to test

7
00:00:12,70 --> 00:00:14,50
on mobile emulators.

8
00:00:14,50 --> 00:00:18,40
You should test on iOS and Android at the very least.

9
00:00:18,40 --> 00:00:21,10
If you're on a Mac, you can can start the simulator

10
00:00:21,10 --> 00:00:24,70
by right-clicking on Xcode in the applications folder

11
00:00:24,70 --> 00:00:27,50
and choosing Show Package Contents.

12
00:00:27,50 --> 00:00:32,40
Then navigate to Contents, Developer, Applications,

13
00:00:32,40 --> 00:00:34,90
and you'll find the simulator there.

14
00:00:34,90 --> 00:00:37,10
Double-click on it to launch it.

15
00:00:37,10 --> 00:00:40,50
Note that this assumes you have Xcode installed.

16
00:00:40,50 --> 00:00:42,80
Xcode is available in the app store.

17
00:00:42,80 --> 00:00:45,00
Just do a search for Xcode and it should be

18
00:00:45,00 --> 00:00:47,00
the first app displayed.

19
00:00:47,00 --> 00:00:50,60
You'll need to install Xcode version 9.3 or greater

20
00:00:50,60 --> 00:00:54,50
and the iOS SDK version 11.3 or greater.

21
00:00:54,50 --> 00:00:59,80
And you'll need OS10 High Sierra to run Xcode 9.3.

22
00:00:59,80 --> 00:01:04,20
Once the simulator starts, we'll need to start Safari

23
00:01:04,20 --> 00:01:08,00
and then assuming you're running Rebound from NetBeans,

24
00:01:08,00 --> 00:01:12,60
we need to navigate to the server that holds our project.

25
00:01:12,60 --> 00:01:17,30
So, that's going to be local host,

26
00:01:17,30 --> 00:01:22,00
and then colon, the port number is 8383,

27
00:01:22,00 --> 00:01:26,80
and then slash, the project name, that's Rebound

28
00:01:26,80 --> 00:01:35,80
with a capital R, and then the file name is rebound.html.

29
00:01:35,80 --> 00:01:40,00
And here's our game running in the iOS simulator

30
00:01:40,00 --> 00:01:45,40
and the touch events are still being recognized.

31
00:01:45,40 --> 00:01:48,30
The easiest way to get started with the Android emulator

32
00:01:48,30 --> 00:01:51,50
is to download and install Android Studio.

33
00:01:51,50 --> 00:01:55,50
You can find that at developer.android.com.

34
00:01:55,50 --> 00:01:58,50
To configure and run an Android emulator,

35
00:01:58,50 --> 00:02:01,60
head over to the Android Studio Users Guide,

36
00:02:01,60 --> 00:02:05,40
also available at developer.android.com.

37
00:02:05,40 --> 00:02:06,90
That will give you instructions for using

38
00:02:06,90 --> 00:02:11,00
the Android Virtual Device, or AVD Manager.

39
00:02:11,00 --> 00:02:13,70
There you can create emulators for any platforms

40
00:02:13,70 --> 00:02:16,60
you'll need to support.

41
00:02:16,60 --> 00:02:18,60
If you need help with Android Studio,

42
00:02:18,60 --> 00:02:20,60
you can search our content library.

43
00:02:20,60 --> 00:02:23,90
We've got courses dedicated to Android Studio.

44
00:02:23,90 --> 00:02:26,90
Okay, I created a blank app so I wouldn't have

45
00:02:26,90 --> 00:02:29,90
to write any code, let me just launch

46
00:02:29,90 --> 00:02:33,30
the AVD Manager from the Tools menu

47
00:02:33,30 --> 00:02:37,60
and I also created an AVD, an emulator,

48
00:02:37,60 --> 00:02:42,30
based on the Pixel 2 XL running Android Pie.

49
00:02:42,30 --> 00:02:46,60
So I can start the emulator from the Actions column.

50
00:02:46,60 --> 00:02:50,00
Once the emulator starts, open Chrome.

51
00:02:50,00 --> 00:02:52,40
I'll always help make Chrome better,

52
00:02:52,40 --> 00:02:54,60
and I won't need to sign in.

53
00:02:54,60 --> 00:02:58,00
The emulator uses a loopback to target local host

54
00:02:58,00 --> 00:03:02,10
with the IP address 10.0.2.2.

55
00:03:02,10 --> 00:03:03,90
Assuming you are running Rebound in

56
00:03:03,90 --> 00:03:10,60
the server started by NetBeans, :8383 for the port number,

57
00:03:10,60 --> 00:03:13,50
the project name, Rebound,

58
00:03:13,50 --> 00:03:17,10
and then the file name rebound.html.

59
00:03:17,10 --> 00:03:19,50
You can see that Rebound works beautifully

60
00:03:19,50 --> 00:03:23,00
in the Android emulator.

