1
00:00:00,50 --> 00:00:02,20
- [Instructor] Now that you've learned that touch events

2
00:00:02,20 --> 00:00:05,40
and mouse events are different, I have good news.

3
00:00:05,40 --> 00:00:07,30
We're going to treat them exactly the same

4
00:00:07,30 --> 00:00:08,90
in our rebound game.

5
00:00:08,90 --> 00:00:10,80
The main reason we can get away with this,

6
00:00:10,80 --> 00:00:13,80
is because there's no scrolling on our page.

7
00:00:13,80 --> 00:00:16,00
We'll still want to call preventDefault

8
00:00:16,00 --> 00:00:18,20
in the touchmove event handler

9
00:00:18,20 --> 00:00:20,80
to prevent both mousemove and touchmove events

10
00:00:20,80 --> 00:00:23,10
from being handled one after the other.

11
00:00:23,10 --> 00:00:26,50
That can potentially mess up scrolling on a page

12
00:00:26,50 --> 00:00:29,90
because the event won't propagate to the next handler.

13
00:00:29,90 --> 00:00:32,20
Be aware of that in your future projects,

14
00:00:32,20 --> 00:00:34,80
but we won't have to worry about it here.

15
00:00:34,80 --> 00:00:37,40
Okay, let's add some event listeners

16
00:00:37,40 --> 00:00:40,10
to handle our mouse and touch events.

17
00:00:40,10 --> 00:00:46,00
So I'm going to scroll up to where I added the keyListeners.

18
00:00:46,00 --> 00:00:48,00
That's in the init function.

19
00:00:48,00 --> 00:00:49,70
And then I'll add the event listeners

20
00:00:49,70 --> 00:00:52,20
for mouse and touch events here.

21
00:00:52,20 --> 00:00:53,70
I'm going to add the listeners

22
00:00:53,70 --> 00:00:56,20
to the playingArea and not the paddle.

23
00:00:56,20 --> 00:00:58,20
And the reason for this is simple.

24
00:00:58,20 --> 00:01:00,80
Since we only have one object to drag around,

25
00:01:00,80 --> 00:01:02,60
we don't have to worry about determining

26
00:01:02,60 --> 00:01:05,00
which object is being dragged.

27
00:01:05,00 --> 00:01:07,50
And if we were to add listeners to the paddle

28
00:01:07,50 --> 00:01:10,40
it would be too easy for the mouse or the touch

29
00:01:10,40 --> 00:01:12,70
to lose contact with the paddle.

30
00:01:12,70 --> 00:01:15,00
So these event listeners look like this.

31
00:01:15,00 --> 00:01:17,70
We're going to add them to the playingArea.

32
00:01:17,70 --> 00:01:21,80
So we'll call playingArea.addEventListener,

33
00:01:21,80 --> 00:01:24,20
and I'll do the mouse ones first.

34
00:01:24,20 --> 00:01:28,60
So the first one we'll listen for is mousedown,

35
00:01:28,60 --> 00:01:30,10
and we'll call a function

36
00:01:30,10 --> 00:01:33,60
which I'll name intuitively, mouseDown,

37
00:01:33,60 --> 00:01:37,40
and then we'll specify false as the third parameter,

38
00:01:37,40 --> 00:01:39,10
third parameter's optional

39
00:01:39,10 --> 00:01:42,30
but this we'll handle in the capturing phase.

40
00:01:42,30 --> 00:01:43,80
Okay, let's be smart.

41
00:01:43,80 --> 00:01:46,80
I'm going to copy that line,

42
00:01:46,80 --> 00:01:48,60
and then paste it in,

43
00:01:48,60 --> 00:01:54,40
only this time I'll listen for the mousemove event,

44
00:01:54,40 --> 00:01:58,30
and we'll create a function called mouseMove,

45
00:01:58,30 --> 00:02:00,70
and then we'll paste it in one more time,

46
00:02:00,70 --> 00:02:04,00
and we'll listen for the mouseup event,

47
00:02:04,00 --> 00:02:07,80
and we'll create a function named mouseUp.

48
00:02:07,80 --> 00:02:09,50
Okay, the good news is

49
00:02:09,50 --> 00:02:14,40
that we're going to handle touch events exactly the same way

50
00:02:14,40 --> 00:02:16,20
as we handle mouse events.

51
00:02:16,20 --> 00:02:18,80
So I'm actually going to copy

52
00:02:18,80 --> 00:02:21,70
all three of the lines this time,

53
00:02:21,70 --> 00:02:23,60
and then I'll paste them in again,

54
00:02:23,60 --> 00:02:26,80
and change the event names that we're listening for.

55
00:02:26,80 --> 00:02:31,10
So the first touch event will be touchstart.

56
00:02:31,10 --> 00:02:37,60
The analogous touch event to mousemove will be touchmove,

57
00:02:37,60 --> 00:02:41,90
and then the final one will be touchend.

58
00:02:41,90 --> 00:02:45,00
That will correspond to mouseup.

