1
00:00:00,50 --> 00:00:02,90
- [Instructor] At this point, the game is playable,

2
00:00:02,90 --> 00:00:05,70
but we should talk about improving playability.

3
00:00:05,70 --> 00:00:07,20
What if we rewarded players

4
00:00:07,20 --> 00:00:09,90
for rebounding the ball in the middle of the paddle?

5
00:00:09,90 --> 00:00:12,70
If we could divide the paddle into three regions,

6
00:00:12,70 --> 00:00:14,90
we could change the dx variable

7
00:00:14,90 --> 00:00:18,30
to change the trajectory of the ball in the game.

8
00:00:18,30 --> 00:00:21,30
The first region could be the left edge of the paddle,

9
00:00:21,30 --> 00:00:24,20
the middle of the paddle would remain unchanged,

10
00:00:24,20 --> 00:00:27,20
and the third region could be the right side of the paddle.

11
00:00:27,20 --> 00:00:29,90
The paddle was 64 pixels wide.

12
00:00:29,90 --> 00:00:31,70
So if we do a little math,

13
00:00:31,70 --> 00:00:35,30
that makes the left edge boundary 16 pixels,

14
00:00:35,30 --> 00:00:38,00
and the right edge boundary 48 pixels.

