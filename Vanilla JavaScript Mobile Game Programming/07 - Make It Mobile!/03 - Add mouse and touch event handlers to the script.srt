1
00:00:00,50 --> 00:00:03,00
- [Instructor] Okay, next let's scroll to the top

2
00:00:03,00 --> 00:00:04,20
of our scripts

3
00:00:04,20 --> 00:00:06,40
and add a variable that keeps track

4
00:00:06,40 --> 00:00:09,30
of whether or not the user is dragging.

5
00:00:09,30 --> 00:00:12,70
So I'll create a variable which I'll call drag

6
00:00:12,70 --> 00:00:16,70
and I'll initialize that to be false.

7
00:00:16,70 --> 00:00:20,30
Okay, so you'll notice that in our EventListeners,

8
00:00:20,30 --> 00:00:21,70
I only have three functions

9
00:00:21,70 --> 00:00:25,70
that I need to define, mouseDown, mouseMove, mouseUp.

10
00:00:25,70 --> 00:00:27,50
So let's do those next.

11
00:00:27,50 --> 00:00:29,50
At the bottom of the script,

12
00:00:29,50 --> 00:00:32,00
I'll add the function for mouseDown,

13
00:00:32,00 --> 00:00:35,90
so function mouseDown.

14
00:00:35,90 --> 00:00:38,80
That gets an event object from the browser.

15
00:00:38,80 --> 00:00:40,30
And the only thing we need to do

16
00:00:40,30 --> 00:00:44,20
is specify that drag is true.

17
00:00:44,20 --> 00:00:47,20
The next function will be mouseUp.

18
00:00:47,20 --> 00:00:49,80
That also gets an event object

19
00:00:49,80 --> 00:00:52,40
and you've probably figured out what happens here.

20
00:00:52,40 --> 00:00:55,90
We just simply set drag equal to false

21
00:00:55,90 --> 00:00:56,70
and so what's happening

22
00:00:56,70 --> 00:00:58,60
is when the user clicks the mouseDown,

23
00:00:58,60 --> 00:01:00,40
we set drag to true

24
00:01:00,40 --> 00:01:01,80
and then when he clicks mouseUp,

25
00:01:01,80 --> 00:01:03,70
we set drag to false,

26
00:01:03,70 --> 00:01:06,00
you can see I've misspelled mouseUp,

27
00:01:06,00 --> 00:01:08,10
there should be an E in there.

28
00:01:08,10 --> 00:01:09,60
Okay, the last thing to do

29
00:01:09,60 --> 00:01:12,10
is to do the mouseMove function.

30
00:01:12,10 --> 00:01:13,40
That one's a little bit more involved,

31
00:01:13,40 --> 00:01:16,70
so let's add the signature first.

32
00:01:16,70 --> 00:01:19,90
That also gets an event object.

33
00:01:19,90 --> 00:01:21,00
Great, the first thing to do

34
00:01:21,00 --> 00:01:22,40
is to check the flag.

35
00:01:22,40 --> 00:01:24,00
If the flag is true,

36
00:01:24,00 --> 00:01:26,60
the mouseDown event handler is fired

37
00:01:26,60 --> 00:01:29,50
but the mouseUp event handler hasn't.

38
00:01:29,50 --> 00:01:30,80
So a simple conditional statement.

39
00:01:30,80 --> 00:01:35,40
We'll just say if drag.

40
00:01:35,40 --> 00:01:37,70
Okay and remember, the first thing we wanted to do

41
00:01:37,70 --> 00:01:40,20
is we didn't want to have both mouse

42
00:01:40,20 --> 00:01:44,20
and touch move events fire at the same time.

43
00:01:44,20 --> 00:01:49,70
So let's call e.preventDefault.

44
00:01:49,70 --> 00:01:51,20
And that prevents both touch

45
00:01:51,20 --> 00:01:53,70
and mouse events from occurring.

46
00:01:53,70 --> 00:01:54,60
The next thing to do

47
00:01:54,60 --> 00:01:56,80
is to get the value of the location

48
00:01:56,80 --> 00:01:58,70
of the mouse or touch event.

49
00:01:58,70 --> 00:02:03,00
Okay, we're going to assign that to the paddleLeft variable.

50
00:02:03,00 --> 00:02:04,50
And here's what we're going to do,

51
00:02:04,50 --> 00:02:06,10
if it's a mouse event,

52
00:02:06,10 --> 00:02:12,10
we'll get it from the e.clientX value

53
00:02:12,10 --> 00:02:14,40
but we're going to subtract 32 pixels from that

54
00:02:14,40 --> 00:02:17,20
and what that'll do is it'll move the paddle

55
00:02:17,20 --> 00:02:20,20
to the middle of where the mouse event occurs.

56
00:02:20,20 --> 00:02:23,10
But we're then going to create an or statement

57
00:02:23,10 --> 00:02:25,30
and now we'll listen for the touch event.

58
00:02:25,30 --> 00:02:32,60
So that's e.targetTouches sub zero

59
00:02:32,60 --> 00:02:36,30
and then from that, we're going to get the pageX value.

60
00:02:36,30 --> 00:02:40,10
Because most touchscreens are multi-touch enabled,

61
00:02:40,10 --> 00:02:43,10
there can be more than one touch on the screen.

62
00:02:43,10 --> 00:02:45,30
Those are kept in the targetTouches array,

63
00:02:45,30 --> 00:02:48,50
we're only interested in the first touch.

64
00:02:48,50 --> 00:02:49,40
Okay?

65
00:02:49,40 --> 00:02:54,20
So we'll subtract 32 from that one as well.

66
00:02:54,20 --> 00:02:57,40
Okay, you should notice that there is an or operator.

67
00:02:57,40 --> 00:02:59,20
If the mouse event fires,

68
00:02:59,20 --> 00:03:01,20
then the touch event is ignored

69
00:03:01,20 --> 00:03:02,90
and if the mouse event is null,

70
00:03:02,90 --> 00:03:05,60
then the touch event is used.

71
00:03:05,60 --> 00:03:06,70
Okay, underneath that,

72
00:03:06,70 --> 00:03:08,70
let's add a sanity check

73
00:03:08,70 --> 00:03:12,30
to make sure that the paddle stays within the playing area.

74
00:03:12,30 --> 00:03:17,10
So I'm going to say if paddleLeft

75
00:03:17,10 --> 00:03:18,90
is less than zero,

76
00:03:18,90 --> 00:03:24,00
in that case, we're just going to set paddleLeft

77
00:03:24,00 --> 00:03:25,50
equal to zero.

78
00:03:25,50 --> 00:03:26,80
And that will prevent the paddle

79
00:03:26,80 --> 00:03:28,80
from moving off the left edge of the screen.

80
00:03:28,80 --> 00:03:31,60
Let's do the same thing for the right edge of the screen.

81
00:03:31,60 --> 00:03:35,30
So in this case we'll say if paddleLeft

82
00:03:35,30 --> 00:03:38,20
is greater than well,

83
00:03:38,20 --> 00:03:41,30
we need the playing area width

84
00:03:41,30 --> 00:03:44,40
minus the width of the paddle which is 64.

85
00:03:44,40 --> 00:03:46,00
If that's the case,

86
00:03:46,00 --> 00:03:49,30
then we'll set paddleLeft equal

87
00:03:49,30 --> 00:03:54,40
to the playing area width minus 64.

88
00:03:54,40 --> 00:03:57,10
Okay, the only thing left to do now

89
00:03:57,10 --> 00:03:59,40
is assign a value of paddleLeft

90
00:03:59,40 --> 00:04:02,50
to the HTML object that represents the paddle.

91
00:04:02,50 --> 00:04:10,60
So that's paddle.style.left equals

92
00:04:10,60 --> 00:04:15,10
paddleLeft plus our units.

93
00:04:15,10 --> 00:04:17,10
Okay, let's save that

94
00:04:17,10 --> 00:04:20,00
and then let's fire it up, see what it looks like.

95
00:04:20,00 --> 00:04:23,00
We should now be able to drag the paddle with the mouse.

96
00:04:23,00 --> 00:04:30,50
Very cool.

97
00:04:30,50 --> 00:04:32,30
And the game over still works.

98
00:04:32,30 --> 00:04:33,80
If you'd like to see what it looks like

99
00:04:33,80 --> 00:04:41,80
on a mobile device, open up the dev tools,

100
00:04:41,80 --> 00:04:44,30
choose the device view

101
00:04:44,30 --> 00:04:49,50
and then refresh the page.

102
00:04:49,50 --> 00:04:55,00
And we can still drag the paddle around.

103
00:04:55,00 --> 00:04:57,00
Nice, things are shaping up.

