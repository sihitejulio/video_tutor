1
00:00:00,50 --> 00:00:01,70
- [Instructor] Let's see if we can implement

2
00:00:01,70 --> 00:00:03,90
the new collision detection.

3
00:00:03,90 --> 00:00:05,30
All the work will need to be done

4
00:00:05,30 --> 00:00:07,30
in the collisionY function,

5
00:00:07,30 --> 00:00:09,90
so let's go find the collisionY function

6
00:00:09,90 --> 00:00:11,00
and then you know what?

7
00:00:11,00 --> 00:00:13,20
All of this work will also be done

8
00:00:13,20 --> 00:00:14,70
in the conditional statement

9
00:00:14,70 --> 00:00:15,70
that checks whether or not

10
00:00:15,70 --> 00:00:17,80
the ball collided with the paddle.

11
00:00:17,80 --> 00:00:20,70
So that's this block right here.

12
00:00:20,70 --> 00:00:21,70
So what I'm going to do

13
00:00:21,70 --> 00:00:26,20
is I think I'm going to comment out the existing code,

14
00:00:26,20 --> 00:00:27,40
and the reason I'm going to do that

15
00:00:27,40 --> 00:00:29,60
is 'cause when I make a change, if it doesn't work,

16
00:00:29,60 --> 00:00:32,30
I can always go back to what I had.

17
00:00:32,30 --> 00:00:33,80
Okay, the next think we'll need to do

18
00:00:33,80 --> 00:00:36,20
is we're going to have three conditionals this time.

19
00:00:36,20 --> 00:00:37,90
A conditional that detects whether or not

20
00:00:37,90 --> 00:00:40,00
the ball collided with the left edge of the paddle,

21
00:00:40,00 --> 00:00:41,20
the middle of the paddle,

22
00:00:41,20 --> 00:00:43,00
or the right edge of the paddle.

23
00:00:43,00 --> 00:00:45,80
Since the middle of the paddle is the largest area,

24
00:00:45,80 --> 00:00:47,60
it's most likely to be true.

25
00:00:47,60 --> 00:00:50,00
So let's write that conditional first.

26
00:00:50,00 --> 00:00:53,60
So that one looks like if, and then we'll say

27
00:00:53,60 --> 00:01:04,00
ballLeft >= paddleLeft + 16

28
00:01:04,00 --> 00:01:13,30
&& ballLeft > paddleLeft + 48.

29
00:01:13,30 --> 00:01:14,80
Essentially what we're checking for

30
00:01:14,80 --> 00:01:17,60
is whether or not the ball has collided with the paddle

31
00:01:17,60 --> 00:01:20,50
between 16 and 48 pixels.

32
00:01:20,50 --> 00:01:22,50
Okay, if that's true,

33
00:01:22,50 --> 00:01:26,10
what we're going to do is modify dx.

34
00:01:26,10 --> 00:01:27,30
We really don't know whether

35
00:01:27,30 --> 00:01:29,30
or not dx is positive or negative, though.

36
00:01:29,30 --> 00:01:31,10
The ball could be moving from left to right

37
00:01:31,10 --> 00:01:33,10
or from right to left.

38
00:01:33,10 --> 00:01:35,70
So we'll need a simple conditional statement here, too.

39
00:01:35,70 --> 00:01:38,20
And I'll just check whether or not dx is

40
00:01:38,20 --> 00:01:40,00
positive or negative.

41
00:01:40,00 --> 00:01:43,70
If dx > 0, then the ball is moving from right to left.

42
00:01:43,70 --> 00:01:46,50
We're going to keep it moving in that direction.

43
00:01:46,50 --> 00:01:48,00
So in order to do that,

44
00:01:48,00 --> 00:01:54,40
we're just going to set dx = -2.

45
00:01:54,40 --> 00:01:56,50
But if it's moving in the other direction

46
00:01:56,50 --> 00:01:58,10
we'll need an else clause

47
00:01:58,10 --> 00:02:02,20
and we'll set dx = 2.

48
00:02:02,20 --> 00:02:04,10
In essence, we're just going to

49
00:02:04,10 --> 00:02:06,80
modify the magnitude of dx,

50
00:02:06,80 --> 00:02:08,50
and we'll set it equal to 2,

51
00:02:08,50 --> 00:02:11,20
but we're not going to change the direction.

52
00:02:11,20 --> 00:02:13,30
Okay, if that's the case though,

53
00:02:13,30 --> 00:02:16,40
remember we need to return true;

54
00:02:16,40 --> 00:02:20,50
because a collision happened.

55
00:02:20,50 --> 00:02:22,90
So that represents the middle of the paddle,

56
00:02:22,90 --> 00:02:25,60
let's do the left edge of the paddle.

57
00:02:25,60 --> 00:02:27,80
I'm going to add an else clause

58
00:02:27,80 --> 00:02:29,80
because I don't want to evaluate this

59
00:02:29,80 --> 00:02:32,30
unless it's not hitting the middle of the paddle.

60
00:02:32,30 --> 00:02:35,00
So let's do else if.

61
00:02:35,00 --> 00:02:36,50
Now the left edge of the paddle

62
00:02:36,50 --> 00:02:38,50
is actually marginally easier to write

63
00:02:38,50 --> 00:02:40,10
as a conditional statement.

64
00:02:40,10 --> 00:02:42,50
Because there's one less calculation.

65
00:02:42,50 --> 00:02:44,10
We're just going to say

66
00:02:44,10 --> 00:02:50,30
if ballLeft >= paddleLeft

67
00:02:50,30 --> 00:02:59,60
&& ballLeft < paddleLeft + 16.

68
00:02:59,60 --> 00:03:02,00
So this represents the left edge of the paddle.

69
00:03:02,00 --> 00:03:06,80
Notice I did > paddleLeft + 16 here,

70
00:03:06,80 --> 00:03:08,60
and in the conditional statement above it

71
00:03:08,60 --> 00:03:11,80
I did >= 16.

72
00:03:11,80 --> 00:03:15,80
That way if the ball collides at the 16 pixel mark exactly,

73
00:03:15,80 --> 00:03:19,00
we'll set the x to 2 or -2.

74
00:03:19,00 --> 00:03:22,40
Here though, what we want to do

75
00:03:22,40 --> 00:03:24,20
is increase the magnitude of dx.

76
00:03:24,20 --> 00:03:26,10
That will flatten out the trajectory of the ball

77
00:03:26,10 --> 00:03:28,10
and make it more difficult to track.

78
00:03:28,10 --> 00:03:36,10
So, if dx < 0, let's set dx = -8;.

79
00:03:36,10 --> 00:03:39,00
Otherwise it's greater than 0,

80
00:03:39,00 --> 00:03:44,20
we can set dx = 8;.

81
00:03:44,20 --> 00:03:45,90
And I'm just going to add a space there

82
00:03:45,90 --> 00:03:47,70
to make it easier to read.

83
00:03:47,70 --> 00:03:50,00
Okay, in this case again, a collision happened.

84
00:03:50,00 --> 00:03:51,90
We need to return true;

85
00:03:51,90 --> 00:03:54,40
and that represents the left edge of the paddle.

86
00:03:54,40 --> 00:03:56,70
Now all we have to do is implement the right edge

87
00:03:56,70 --> 00:03:57,80
of the paddle.

88
00:03:57,80 --> 00:04:00,40
So that's another else if clause,

89
00:04:00,40 --> 00:04:03,50
and the conditional statement here looks like

90
00:04:03,50 --> 00:04:11,30
if(ballLeft >= paddleLeft + 48

91
00:04:11,30 --> 00:04:19,50
&& ballLeft <= paddleLeft = 64).

92
00:04:19,50 --> 00:04:21,40
That's the edge of the paddle.

93
00:04:21,40 --> 00:04:23,80
Okay, I'm going to treat this conditional statement

94
00:04:23,80 --> 00:04:25,80
exactly like I treated the other one,

95
00:04:25,80 --> 00:04:29,60
so I'm going to copy the code for the left edge of the paddle,

96
00:04:29,60 --> 00:04:32,20
and then I'm going to paste it in.

97
00:04:32,20 --> 00:04:33,00
And that should do it!

98
00:04:33,00 --> 00:04:34,60
Let's go see what it looks like.

99
00:04:34,60 --> 00:04:35,80
Save your work,

100
00:04:35,80 --> 00:04:38,90
and let's go back and view. Let's see if I can get it to hit

101
00:04:38,90 --> 00:04:43,20
on the edge of a paddle.

102
00:04:43,20 --> 00:04:44,40
There it goes.

103
00:04:44,40 --> 00:04:45,80
See how it flattens the trajectory

104
00:04:45,80 --> 00:04:48,80
and makes it more difficult to track.

105
00:04:48,80 --> 00:04:50,10
And then you can always get it back

106
00:04:50,10 --> 00:04:52,00
with the middle of the paddle.

