1
00:00:00,50 --> 00:00:02,20
- [Narrator] The Touch Even Specification

2
00:00:02,20 --> 00:00:06,20
has been a W3C recommendation since 2013.

3
00:00:06,20 --> 00:00:10,40
You can read the entire spec at w3.org.

4
00:00:10,40 --> 00:00:13,00
There you'll find four kinds of touch events

5
00:00:13,00 --> 00:00:14,80
that we can listen for and handle,

6
00:00:14,80 --> 00:00:21,30
touchstart, touchmove, touchend, and touchcancel.

7
00:00:21,30 --> 00:00:22,70
The keyboard and mouse have been

8
00:00:22,70 --> 00:00:26,70
the main user-input mechanism for more than 30 years,

9
00:00:26,70 --> 00:00:28,90
and web developers have been handling mouse events

10
00:00:28,90 --> 00:00:31,00
for just about that long.

11
00:00:31,00 --> 00:00:33,10
Handling touch events is very similar

12
00:00:33,10 --> 00:00:36,30
to handling mouse events, with a few exceptions.

13
00:00:36,30 --> 00:00:38,80
Don't make the mistake of detecting touch events,

14
00:00:38,80 --> 00:00:41,00
and then ignoring mouse events.

15
00:00:41,00 --> 00:00:43,00
Just because touch events are present,

16
00:00:43,00 --> 00:00:45,50
doesn't mean the user is exclusively interacting

17
00:00:45,50 --> 00:00:47,80
with your app via touch.

18
00:00:47,80 --> 00:00:51,00
Devices like Chromebooks, and even some Windows laptops,

19
00:00:51,00 --> 00:00:54,70
may have both touch and mouse interfaces.

20
00:00:54,70 --> 00:00:58,50
On those devices, users will do what's most natural,

21
00:00:58,50 --> 00:01:01,30
manipulate the mouse or touch the screen,

22
00:01:01,30 --> 00:01:03,10
often in the same app.

23
00:01:03,10 --> 00:01:05,40
The main difference when considering rebound

24
00:01:05,40 --> 00:01:09,30
is how touch and mouse events handle dragging objects.

25
00:01:09,30 --> 00:01:13,90
Using mouse events you typically set a flag on mousedown,

26
00:01:13,90 --> 00:01:16,90
and in the mousemove event you check the flag.

27
00:01:16,90 --> 00:01:18,50
If the flag is true,

28
00:01:18,50 --> 00:01:21,40
you drag the object around with the mouse.

29
00:01:21,40 --> 00:01:24,40
In the mouseup event, you'd stop dragging,

30
00:01:24,40 --> 00:01:27,20
and set the flag to false.

31
00:01:27,20 --> 00:01:28,80
There is no touch event that is

32
00:01:28,80 --> 00:01:32,60
an aligist to either mouseover or mouseout,

33
00:01:32,60 --> 00:01:36,60
and the touchmove event actually implies dragging.

34
00:01:36,60 --> 00:01:40,00
The user is moving the touch event, right?

35
00:01:40,00 --> 00:01:44,00
On devices that support both touch and mouse events,

36
00:01:44,00 --> 00:01:45,80
often you'll want to prevent the browser

37
00:01:45,80 --> 00:01:48,70
from firing a mouse event after a touch event,

38
00:01:48,70 --> 00:01:50,30
and vice versa.

39
00:01:50,30 --> 00:01:53,30
You do this by calling the prevent default method

40
00:01:53,30 --> 00:01:55,00
on the event object.

