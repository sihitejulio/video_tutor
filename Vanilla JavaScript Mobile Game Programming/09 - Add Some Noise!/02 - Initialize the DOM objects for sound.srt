1
00:00:00,50 --> 00:00:02,60
- [Instructor] Since we now have HTML elements

2
00:00:02,60 --> 00:00:05,20
for playing sounds, we'll need to initialize them

3
00:00:05,20 --> 00:00:07,20
in our script.

4
00:00:07,20 --> 00:00:09,10
I'm going to do that at the top of the script

5
00:00:09,10 --> 00:00:11,40
with all of the other HTML objects,

6
00:00:11,40 --> 00:00:14,00
so let's add some instance variables

7
00:00:14,00 --> 00:00:16,50
for those HTML objects first.

8
00:00:16,50 --> 00:00:20,00
I'll create a variable which I'll call sound,

9
00:00:20,00 --> 00:00:24,30
and a second one which I'll call music.

10
00:00:24,30 --> 00:00:26,60
Since these are going to be DOM objects,

11
00:00:26,60 --> 00:00:30,30
they'll need to be initialized inside the init function,

12
00:00:30,30 --> 00:00:33,00
so scroll down to the init function

13
00:00:33,00 --> 00:00:35,60
and just before the call to layout page,

14
00:00:35,60 --> 00:00:38,50
let's initialize our DOM objects,

15
00:00:38,50 --> 00:00:41,30
so I'll say sound equals and then it's

16
00:00:41,30 --> 00:00:46,10
document dot get element by ID,

17
00:00:46,10 --> 00:00:48,80
and this one points to that sound checkbox

18
00:00:48,80 --> 00:00:53,20
that we created, and we'll do the same thing for music.

19
00:00:53,20 --> 00:00:59,10
So it's document dot get element by ID,

20
00:00:59,10 --> 00:01:02,80
and this one will point to our music checkbox.

21
00:01:02,80 --> 00:01:05,10
Okay, next we'll add flags to the script

22
00:01:05,10 --> 00:01:07,60
for tracking whether sound effects and music

23
00:01:07,60 --> 00:01:09,20
have been enabled.

24
00:01:09,20 --> 00:01:12,90
So I'm going to do that just above where we created

25
00:01:12,90 --> 00:01:15,70
the event listeners for the window object,

26
00:01:15,70 --> 00:01:19,10
just below drag, I'm going to create a variable called

27
00:01:19,10 --> 00:01:23,50
sound enabled, and initially that will be false,

28
00:01:23,50 --> 00:01:27,00
as we're not playing sound effects at start up.

29
00:01:27,00 --> 00:01:32,20
Let's create a second variable for music enabled,

30
00:01:32,20 --> 00:01:33,80
and the same holds true.

31
00:01:33,80 --> 00:01:39,40
We are not going to play the music on start up.

32
00:01:39,40 --> 00:01:41,90
Okay, and then after that, we're going to need

33
00:01:41,90 --> 00:01:44,60
some instance variables for the audio objects

34
00:01:44,60 --> 00:01:47,40
so they'll actually play the sounds for us.

35
00:01:47,40 --> 00:01:50,20
So I have a few beeps and a background music track

36
00:01:50,20 --> 00:01:51,90
that we're going to make use of,

37
00:01:51,90 --> 00:01:54,50
so let's create variables for all of those, too.

38
00:01:54,50 --> 00:01:59,10
The first one is going to be a variable called beep X,

39
00:01:59,10 --> 00:02:03,20
I have another one for beep Y,

40
00:02:03,20 --> 00:02:06,20
we're going to have a separate sound for when

41
00:02:06,20 --> 00:02:08,80
the ball intersects with the paddle,

42
00:02:08,80 --> 00:02:12,00
so we'll create a beep paddle variable.

43
00:02:12,00 --> 00:02:15,20
We'll play a sound when the game is over,

44
00:02:15,20 --> 00:02:19,20
so let's do a beep game over variable,

45
00:02:19,20 --> 00:02:21,10
and then finally we'll do a variable

46
00:02:21,10 --> 00:02:26,20
for the background music, which I'll just call BG music.

47
00:02:26,20 --> 00:02:27,80
Okay, that's good.

48
00:02:27,80 --> 00:02:30,90
Let's save that and now let's add the sound files

49
00:02:30,90 --> 00:02:33,20
to our project.

50
00:02:33,20 --> 00:02:38,00
In the exercise files, you'll find a sounds.zip file.

51
00:02:38,00 --> 00:02:41,60
You can unzip that and get the sound files for the game.

52
00:02:41,60 --> 00:02:43,70
I've done that on my desktop.

53
00:02:43,70 --> 00:02:45,30
What I'm going to do is I'm going to right click

54
00:02:45,30 --> 00:02:50,10
on this sounds folder and choose copy sounds.

55
00:02:50,10 --> 00:02:53,10
Then I'm going to jump back into NetBeans

56
00:02:53,10 --> 00:02:55,70
and then I'm going to right click on the site root

57
00:02:55,70 --> 00:02:59,70
and choose paste, and you'll see now the sounds folder

58
00:02:59,70 --> 00:03:03,10
which contains all five of the audio files

59
00:03:03,10 --> 00:03:04,00
needed for the game.

