1
00:00:00,50 --> 00:00:01,60
- [Instructor] Playing background music

2
00:00:01,60 --> 00:00:04,70
is actually much easier then playing sound effects.

3
00:00:04,70 --> 00:00:07,30
The music is either on or off.

4
00:00:07,30 --> 00:00:09,30
We won't have to play multiple files and

5
00:00:09,30 --> 00:00:11,60
multiple times during the game.

6
00:00:11,60 --> 00:00:14,50
Okay. The first thing to do is to add an event listener

7
00:00:14,50 --> 00:00:17,60
for the DOM object inside the init function.

8
00:00:17,60 --> 00:00:19,50
So let's find the init function.

9
00:00:19,50 --> 00:00:21,90
And then just beneath where we added

10
00:00:21,90 --> 00:00:24,10
the event listener for the sound object,

11
00:00:24,10 --> 00:00:27,10
let's do the same for the music object.

12
00:00:27,10 --> 00:00:31,00
So it'll be music.addEventListener.

13
00:00:31,00 --> 00:00:34,70
We'll listen for the click event.

14
00:00:34,70 --> 00:00:37,50
And we'll call a toggleMusic function

15
00:00:37,50 --> 00:00:44,00
which we'll write in just a moment.

16
00:00:44,00 --> 00:00:47,60
Okay. Then we can scroll down to the bottom of the script,

17
00:00:47,60 --> 00:00:55,30
and add that toggleMusic function.

18
00:00:55,30 --> 00:00:57,70
Just like we did in the toggleSound function,

19
00:00:57,70 --> 00:01:00,10
we'll need to check whether or not our background music

20
00:01:00,10 --> 00:01:02,20
has been initialized.

21
00:01:02,20 --> 00:01:09,00
So, I'll say if bgMusic is equal to null.

22
00:01:09,00 --> 00:01:12,70
We'll need to call initAudio.

23
00:01:12,70 --> 00:01:15,40
Then, once the music's been initialized,

24
00:01:15,40 --> 00:01:16,70
we can play it.

25
00:01:16,70 --> 00:01:20,30
So let's say if musicEnabled.

26
00:01:20,30 --> 00:01:23,10
If that's now true, the user has clicked on it

27
00:01:23,10 --> 00:01:24,60
and wants it to stop.

28
00:01:24,60 --> 00:01:31,40
So we'll just call bgMusic.pause.

29
00:01:31,40 --> 00:01:35,80
Otherwise, I'm going to take the bgMusic object

30
00:01:35,80 --> 00:01:40,00
and set it's loop property to true.

31
00:01:40,00 --> 00:01:45,40
And then play the file.

32
00:01:45,40 --> 00:01:47,60
Last thing to do, is to jump outside

33
00:01:47,60 --> 00:01:50,60
of the conditional statement and actually do the toggling.

34
00:01:50,60 --> 00:02:00,30
So let's write musicEnabled is equal to not musicEnabled.

35
00:02:00,30 --> 00:02:02,50
Okay. Save that.

36
00:02:02,50 --> 00:02:05,10
And I'll apologize in advance for that audio track.

37
00:02:05,10 --> 00:02:08,80
I spent about five minutes with it in GarageBand.

38
00:02:08,80 --> 00:02:10,20
Let's go make some noise.

39
00:02:10,20 --> 00:02:12,80
So, first we'll turn on the sound effects.

40
00:02:12,80 --> 00:02:16,80
And I'll click Done so you can hear all the beeps.

41
00:02:16,80 --> 00:02:18,30
(ball bounce)

42
00:02:18,30 --> 00:02:19,70
That's the game over.

43
00:02:19,70 --> 00:02:21,10
Let's do a New Game.

44
00:02:21,10 --> 00:02:22,20
(ball bounce)

45
00:02:22,20 --> 00:02:24,40
See if I can get it to hit the paddle.

46
00:02:24,40 --> 00:02:29,20
(paddle bounce)

47
00:02:29,20 --> 00:02:31,10
Then we'll turn the music on.

48
00:02:31,10 --> 00:02:37,10
(upbeat music)

49
00:02:37,10 --> 00:02:39,90
Then a New Game in all it's glory.

50
00:02:39,90 --> 00:02:50,90
(upbeat music with ball and paddle bounces)

51
00:02:50,90 --> 00:02:53,00
Okay, that's enough of that.

