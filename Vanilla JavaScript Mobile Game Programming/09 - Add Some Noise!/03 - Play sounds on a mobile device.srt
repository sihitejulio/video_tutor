1
00:00:00,50 --> 00:00:03,90
- [Instructor] Most games that play on PCs or game consoles

2
00:00:03,90 --> 00:00:06,00
auto-play the sounds and provide a way

3
00:00:06,00 --> 00:00:08,40
for users to turn them off.

4
00:00:08,40 --> 00:00:11,60
Unfortunately, that won't work on mobile devices

5
00:00:11,60 --> 00:00:14,80
as auto-play is disabled in browsers.

6
00:00:14,80 --> 00:00:19,00
Players must enable sound capabilities with a UI gesture.

7
00:00:19,00 --> 00:00:21,40
The good news is that we've already set up a place

8
00:00:21,40 --> 00:00:23,60
for players to supply a gesture.

9
00:00:23,60 --> 00:00:27,60
Toggling the sound check boxes works beautifully.

10
00:00:27,60 --> 00:00:30,00
Before we implement those event handlers,

11
00:00:30,00 --> 00:00:31,90
let's add a function to our script

12
00:00:31,90 --> 00:00:34,10
that initializes the audio.

13
00:00:34,10 --> 00:00:36,40
So I'm going to scroll to the bottom

14
00:00:36,40 --> 00:00:40,20
and add a function which I'll call

15
00:00:40,20 --> 00:00:43,70
initAudio.

16
00:00:43,70 --> 00:00:46,30
So this is where we're going to trick mobile browsers

17
00:00:46,30 --> 00:00:49,50
into playing sounds for us based on a gesture.

18
00:00:49,50 --> 00:00:52,20
We'll call init audio in the click event handler

19
00:00:52,20 --> 00:00:54,90
for both the sound effects and the music.

20
00:00:54,90 --> 00:00:56,50
The trick goes like this.

21
00:00:56,50 --> 00:01:00,80
First, load the sound files, then lower the volume to zero,

22
00:01:00,80 --> 00:01:05,20
play and pause each file, then reset the volume to one.

23
00:01:05,20 --> 00:01:07,60
This will grant permission to play sounds

24
00:01:07,60 --> 00:01:09,80
and stores the objects in memory.

25
00:01:09,80 --> 00:01:13,50
Let's see how it works for the beepX sound file.

26
00:01:13,50 --> 00:01:17,10
So, we're going to first initialize beepX

27
00:01:17,10 --> 00:01:21,30
to be a new audio object.

28
00:01:21,30 --> 00:01:25,20
To that, we'll supply the path to our audio file

29
00:01:25,20 --> 00:01:27,50
that happens to be in the sounds folder

30
00:01:27,50 --> 00:01:31,60
and then the name of the file is beepX

31
00:01:31,60 --> 00:01:34,10
.mp3.

32
00:01:34,10 --> 00:01:38,10
Next, we'll take the beepX object

33
00:01:38,10 --> 00:01:42,20
and set the volume property to zero.

34
00:01:42,20 --> 00:01:46,90
Then we'll call the beepX play method

35
00:01:46,90 --> 00:01:51,20
and then we'll call beepX pause.

36
00:01:51,20 --> 00:01:57,00
And then finally, we'll set the beepX volume property

37
00:01:57,00 --> 00:01:58,90
back to one.

38
00:01:58,90 --> 00:02:02,60
Now you can see that we've got five lines of code

39
00:02:02,60 --> 00:02:05,60
for just the beepX sound file.

40
00:02:05,60 --> 00:02:09,90
We're going to need to do that for each of the sound files.

41
00:02:09,90 --> 00:02:12,90
So, in the exercise files, you'll find a file

42
00:02:12,90 --> 00:02:18,70
called initAudio.js where I've done the typing for you.

43
00:02:18,70 --> 00:02:21,80
So I put that on my desktop.

44
00:02:21,80 --> 00:02:26,90
I'm going to open that with a text editor

45
00:02:26,90 --> 00:02:32,20
and then I'm going to select all and copy it

46
00:02:32,20 --> 00:02:37,40
and then jump back into NetBeans and paste it in.

47
00:02:37,40 --> 00:02:38,90
And here, if we just look at it,

48
00:02:38,90 --> 00:02:41,60
you can see I've done initialization

49
00:02:41,60 --> 00:02:45,20
for all five audio files, then turn the volume down

50
00:02:45,20 --> 00:02:50,70
for all five, played all five, paused all five,

51
00:02:50,70 --> 00:02:53,00
and set the volume back to one.

