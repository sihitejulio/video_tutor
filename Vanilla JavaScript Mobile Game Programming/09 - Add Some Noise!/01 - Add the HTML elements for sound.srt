1
00:00:00,50 --> 00:00:01,30
- [Instructor] Right now,

2
00:00:01,30 --> 00:00:03,80
our game is the quietest game ever invented.

3
00:00:03,80 --> 00:00:05,90
Let's see if we can add some noise to the game

4
00:00:05,90 --> 00:00:08,90
with sound effects and a background music track.

5
00:00:08,90 --> 00:00:10,90
Okay, in the html page,

6
00:00:10,90 --> 00:00:16,50
let's add some check boxes to our controls div.

7
00:00:16,50 --> 00:00:18,20
So, put your cursor right after

8
00:00:18,20 --> 00:00:21,60
the select elements line break, okay?

9
00:00:21,60 --> 00:00:24,00
And let's add a field set.

10
00:00:24,00 --> 00:00:25,00
And a field set elements

11
00:00:25,00 --> 00:00:27,10
just going to give me a nice visual que.

12
00:00:27,10 --> 00:00:29,20
Let's set a legend

13
00:00:29,20 --> 00:00:32,60
and the legend will just say, sound.

14
00:00:32,60 --> 00:00:35,40
And then underneath that we'll add an input.

15
00:00:35,40 --> 00:00:39,50
Let's set the type equal to check box

16
00:00:39,50 --> 00:00:42,90
and let's give it an ID equal to,

17
00:00:42,90 --> 00:00:46,30
I'm going to use snd for sound.

18
00:00:46,30 --> 00:00:49,10
And then directly beneath that we'll add a label

19
00:00:49,10 --> 00:00:51,20
and we'll add the label for

20
00:00:51,20 --> 00:00:54,10
the sound check box we just created.

21
00:00:54,10 --> 00:00:55,70
And that will be,

22
00:00:55,70 --> 00:00:58,10
I'll use the word effects for sound effects.

23
00:00:58,10 --> 00:00:59,60
And then outside the label,

24
00:00:59,60 --> 00:01:03,60
lets add a break and add a second input element.

25
00:01:03,60 --> 00:01:06,10
Type is also check box.

26
00:01:06,10 --> 00:01:08,70
Let's give this one a ID

27
00:01:08,70 --> 00:01:12,30
and we'll set that equal to music.

28
00:01:12,30 --> 00:01:14,70
And the label for that one

29
00:01:14,70 --> 00:01:18,50
will be for music.

30
00:01:18,50 --> 00:01:22,00
And the text I'll use there is the word music.

31
00:01:22,00 --> 00:01:25,60
And then let's make sure we can still differentiate

32
00:01:25,60 --> 00:01:29,80
between our field set and the button.

33
00:01:29,80 --> 00:01:31,80
Okay, save that.

34
00:01:31,80 --> 00:01:34,30
And then jump to the CSS file.

35
00:01:34,30 --> 00:01:37,40
And we'll add a few quick rules for the field set.

36
00:01:37,40 --> 00:01:39,60
So add a field set rule.

37
00:01:39,60 --> 00:01:42,40
And I'm just going to make the text align.

38
00:01:42,40 --> 00:01:44,20
Set that to left.

39
00:01:44,20 --> 00:01:48,60
Let's set the width to be 70%

40
00:01:48,60 --> 00:01:54,00
and then let's set the margin to be 10 pixels top and bottom

41
00:01:54,00 --> 00:01:57,20
and then auto left and right.

42
00:01:57,20 --> 00:01:58,70
Save that.

43
00:01:58,70 --> 00:01:59,60
And then we can go see

44
00:01:59,60 --> 00:02:03,40
what it looks like by clicking the gear icon.

45
00:02:03,40 --> 00:02:05,00
That looks pretty good to me.

