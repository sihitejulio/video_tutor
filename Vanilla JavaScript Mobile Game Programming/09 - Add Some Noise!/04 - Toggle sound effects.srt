1
00:00:00,50 --> 00:00:01,30
- [Instructor] Let's see if

2
00:00:01,30 --> 00:00:03,80
we can toggle the sound effects first.

3
00:00:03,80 --> 00:00:04,90
We're going to need to add

4
00:00:04,90 --> 00:00:07,00
an event listener to the DOM object

5
00:00:07,00 --> 00:00:08,80
inside the init function.

6
00:00:08,80 --> 00:00:12,40
So let's scroll up and find the init function,

7
00:00:12,40 --> 00:00:14,60
and just below where we did

8
00:00:14,60 --> 00:00:18,20
the event listeners for the controls screen,

9
00:00:18,20 --> 00:00:24,50
let's add an event listener for the sound check box.

10
00:00:24,50 --> 00:00:29,20
So it's SND dot add event listener,

11
00:00:29,20 --> 00:00:33,90
and we'll listen for the click event,

12
00:00:33,90 --> 00:00:35,20
and when that fires,

13
00:00:35,20 --> 00:00:38,30
we'll call a method which you'll write in just a moment

14
00:00:38,30 --> 00:00:41,60
called toggle sound.

15
00:00:41,60 --> 00:00:45,90
And we'll pass in false as the third parameter.

16
00:00:45,90 --> 00:00:47,20
Okay, next we need to add

17
00:00:47,20 --> 00:00:49,60
that toggle sound function to our script,

18
00:00:49,60 --> 00:00:52,10
so I'll scroll down to the bottom,

19
00:00:52,10 --> 00:00:58,20
and let's add our function called toggle sound.

20
00:00:58,20 --> 00:00:59,50
Okay, the first thing to do

21
00:00:59,50 --> 00:01:00,80
is to check whether or not

22
00:01:00,80 --> 00:01:03,60
our sound files have been initialized.

23
00:01:03,60 --> 00:01:05,80
Since we're doing all the initialization

24
00:01:05,80 --> 00:01:08,20
for every file all at once,

25
00:01:08,20 --> 00:01:09,20
we only really have to check

26
00:01:09,20 --> 00:01:11,90
if one of them has been initialized.

27
00:01:11,90 --> 00:01:14,00
So, simple conditional,

28
00:01:14,00 --> 00:01:20,00
if beep X is equal to null,

29
00:01:20,00 --> 00:01:23,30
that means that the audio has not been initialized.

30
00:01:23,30 --> 00:01:24,50
If that's the case,

31
00:01:24,50 --> 00:01:29,30
then we can call our init audio function.

32
00:01:29,30 --> 00:01:30,90
And then once we're sure

33
00:01:30,90 --> 00:01:33,80
that the audio has been initialized,

34
00:01:33,80 --> 00:01:38,40
we can then toggle that sound enabled variable

35
00:01:38,40 --> 00:01:39,50
that we created.

36
00:01:39,50 --> 00:01:42,00
And we're just going to switch it from false to true

37
00:01:42,00 --> 00:01:45,60
or from true to false.

38
00:01:45,60 --> 00:01:47,50
Okay, let's create a helper function

39
00:01:47,50 --> 00:01:50,00
to play the different sounds when appropriate.

40
00:01:50,00 --> 00:01:53,90
I'm going to create a play sound function.

41
00:01:53,90 --> 00:01:57,50
And that will accept some sound object

42
00:01:57,50 --> 00:02:00,30
that we'll want to play.

43
00:02:00,30 --> 00:02:04,00
We'll do a quick check to make sure sound is enabled.

44
00:02:04,00 --> 00:02:06,50
If that evaluates to true,

45
00:02:06,50 --> 00:02:10,80
then we'll just call the play method on the sound object.

46
00:02:10,80 --> 00:02:14,20
Next we'll need to decide when to play the sound effects.

47
00:02:14,20 --> 00:02:15,70
We have sounds to play for when

48
00:02:15,70 --> 00:02:19,10
the ball bounces off the sides and top of the playing area,

49
00:02:19,10 --> 00:02:21,00
for when it bounces off the paddle,

50
00:02:21,00 --> 00:02:22,90
and for when the game is over.

51
00:02:22,90 --> 00:02:26,30
Most of these occur in the collision detection phase.

52
00:02:26,30 --> 00:02:32,50
So let's find the collision X method,

53
00:02:32,50 --> 00:02:35,60
and just before we return true,

54
00:02:35,60 --> 00:02:37,10
let's play our sound.

55
00:02:37,10 --> 00:02:40,80
So we'll call the play sound function,

56
00:02:40,80 --> 00:02:45,30
and we'll pass in beep X.

57
00:02:45,30 --> 00:02:48,40
Next, find the collision Y function.

58
00:02:48,40 --> 00:02:50,60
Let's play the beep Y sound file

59
00:02:50,60 --> 00:02:53,20
when the ball collides with the top boundary.

60
00:02:53,20 --> 00:02:55,70
So in our first conditional statement,

61
00:02:55,70 --> 00:02:58,40
we'll just call play sound,

62
00:02:58,40 --> 00:03:04,80
and we'll pass in a reference to our beep Y object.

63
00:03:04,80 --> 00:03:06,20
For the paddle sound,

64
00:03:06,20 --> 00:03:09,90
we need to play the sound file just before we return true

65
00:03:09,90 --> 00:03:12,70
in the improved collision detection routine.

66
00:03:12,70 --> 00:03:16,50
So three times we'll need play sound,

67
00:03:16,50 --> 00:03:18,80
and it'll be beep paddle.

68
00:03:18,80 --> 00:03:21,40
So here's the first one,

69
00:03:21,40 --> 00:03:26,30
and we pass in our beep paddle object.

70
00:03:26,30 --> 00:03:28,00
And then let's be smart.

71
00:03:28,00 --> 00:03:31,30
I'll copy that and I'll paste it in

72
00:03:31,30 --> 00:03:36,10
for each part of the paddle.

73
00:03:36,10 --> 00:03:37,40
Okay, finally we're going to need

74
00:03:37,40 --> 00:03:39,30
to modify the game over function

75
00:03:39,30 --> 00:03:41,50
to play the game over sound.

76
00:03:41,50 --> 00:03:45,20
So let's find our game over function,

77
00:03:45,20 --> 00:03:47,80
and at the end of the game over function,

78
00:03:47,80 --> 00:03:50,30
we can call play sound,

79
00:03:50,30 --> 00:03:56,00
and we'll pass in beep game over.

