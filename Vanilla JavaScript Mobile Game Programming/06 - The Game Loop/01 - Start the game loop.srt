1
00:00:00,50 --> 00:00:02,30
- It's time to get this game moving!

2
00:00:02,30 --> 00:00:05,10
Let's set up the game loop and get it started.

3
00:00:05,10 --> 00:00:07,70
We have a few things to do in a game loop.

4
00:00:07,70 --> 00:00:10,40
We need to render the game objects on the screen,

5
00:00:10,40 --> 00:00:12,70
detect any collisions between the ball,

6
00:00:12,70 --> 00:00:14,90
paddle, and game boundaries,

7
00:00:14,90 --> 00:00:17,10
manage the difficulty of the game,

8
00:00:17,10 --> 00:00:20,30
and supply end conditions that, when eventually met,

9
00:00:20,30 --> 00:00:22,90
notify the user that the game is over.

10
00:00:22,90 --> 00:00:25,90
I'm going to jump down just beneath the keyListener,

11
00:00:25,90 --> 00:00:29,50
and I'm going to add a start function.

12
00:00:29,50 --> 00:00:32,00
This will be our game loop function.

13
00:00:32,00 --> 00:00:34,90
We're going to write functions that render the objects

14
00:00:34,90 --> 00:00:38,70
on the screen, detect collisions,

15
00:00:38,70 --> 00:00:41,80
and manage the difficulty.

16
00:00:41,80 --> 00:00:45,60
Eventually, though, we want to reach end conditions,

17
00:00:45,60 --> 00:00:48,30
so that's a simple conditional statement.

18
00:00:48,30 --> 00:00:52,20
I'll say, if ballTop...

19
00:00:52,20 --> 00:00:57,40
is less than the playing area height minus

20
00:00:57,40 --> 00:01:00,00
the playing area height minus the height

21
00:01:00,00 --> 00:01:03,20
of the score label and the paddle.

22
00:01:03,20 --> 00:01:06,00
If that's the case, the ball is still in play,

23
00:01:06,00 --> 00:01:09,10
and we'll say that the timer is equal to,

24
00:01:09,10 --> 00:01:11,20
I'll call it the setTimeout,

25
00:01:11,20 --> 00:01:14,70
and we will call our start function,

26
00:01:14,70 --> 00:01:19,50
and we want this to be quick, so 50 milliseconds.

27
00:01:19,50 --> 00:01:22,60
Okay, but if the ballTop is not

28
00:01:22,60 --> 00:01:27,40
above the score label, we need end conditions,

29
00:01:27,40 --> 00:01:29,80
so I'll include an else clause,

30
00:01:29,80 --> 00:01:33,00
and then we'll write a function called gameOver.

