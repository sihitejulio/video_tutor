1
00:00:00,50 --> 00:00:01,30
- [Instructor] Detecting collisions

2
00:00:01,30 --> 00:00:03,50
is really just a math exercise.

3
00:00:03,50 --> 00:00:07,10
We know the precise dimensions and location of the ball

4
00:00:07,10 --> 00:00:08,30
and the paddle.

5
00:00:08,30 --> 00:00:10,80
We know the same information about the playing area.

6
00:00:10,80 --> 00:00:13,50
Detecting whether the ball collided with the boundary

7
00:00:13,50 --> 00:00:16,00
or the paddle is a simple comparison

8
00:00:16,00 --> 00:00:19,90
between the ball's location and the location of the objects

9
00:00:19,90 --> 00:00:21,20
it could collide with.

10
00:00:21,20 --> 00:00:28,20
Okay, let's add that detectCollisions function.

11
00:00:28,20 --> 00:00:31,20
When a collision occurs, I think I'll just reflect the ball.

12
00:00:31,20 --> 00:00:36,60
We can accomplish this by multiplying dy or dx by minus one.

13
00:00:36,60 --> 00:00:40,20
A more robust engine might change the trajectory of the ball

14
00:00:40,20 --> 00:00:42,90
depending on where it intersects with the paddle.

15
00:00:42,90 --> 00:00:45,20
Okay, so I'm going to write a couple of conditional statements,

16
00:00:45,20 --> 00:00:50,10
and I'm just going to say if and then collisionX,

17
00:00:50,10 --> 00:00:53,10
and that's going to be a function we'll write in just a moment.

18
00:00:53,10 --> 00:00:57,20
If we have a collision horizontally, we'll just take dx

19
00:00:57,20 --> 00:01:01,40
and change its sign, and we'll do the same thing

20
00:01:01,40 --> 00:01:05,20
for a function that we'll write in just a moment

21
00:01:05,20 --> 00:01:08,70
for collisionY, only instead of changing dx,

22
00:01:08,70 --> 00:01:13,30
we'll change the sign of dy.

23
00:01:13,30 --> 00:01:15,50
Okay, let's write the collisionX function first

24
00:01:15,50 --> 00:01:17,60
because I think that one's the easy one.

25
00:01:17,60 --> 00:01:23,50
So, here's our function, and we'll call it collisionX.

26
00:01:23,50 --> 00:01:26,20
So, that's got to return a Boolean 'cause we're using it as

27
00:01:26,20 --> 00:01:27,60
the basis for the conditional statements

28
00:01:27,60 --> 00:01:30,40
in detectCollisions.

29
00:01:30,40 --> 00:01:33,20
Okay, so all we have to is we have to check whether or not

30
00:01:33,20 --> 00:01:35,60
the ball has collided with either the left edge

31
00:01:35,60 --> 00:01:38,30
or right edge of the playing area.

32
00:01:38,30 --> 00:01:41,80
There's no easy way to optimize the conditional statement

33
00:01:41,80 --> 00:01:45,50
so we'll just write them in the order from left to right.

34
00:01:45,50 --> 00:01:51,20
So, I'll just say if ballLeft is less than four.

35
00:01:51,20 --> 00:01:53,80
So, there's going to be a four pixel margin

36
00:01:53,80 --> 00:01:56,90
so that when the ball gets near the left edge,

37
00:01:56,90 --> 00:01:59,60
we're going to say a collision occurred,

38
00:01:59,60 --> 00:02:03,70
and then I'll add an or statement, and we'll say

39
00:02:03,70 --> 00:02:10,00
ballLeft is greater than the playing area width minus 20.

40
00:02:10,00 --> 00:02:14,10
Again, the ball is 16 pixels wide, four pixel cushion

41
00:02:14,10 --> 00:02:15,80
on the edge.

42
00:02:15,80 --> 00:02:19,60
So, if either of those things is true,

43
00:02:19,60 --> 00:02:23,60
we know a collision happened, and we'll return true.

44
00:02:23,60 --> 00:02:27,00
Otherwise, we'll return false.

45
00:02:27,00 --> 00:02:28,70
No collision happened.

46
00:02:28,70 --> 00:02:31,80
Okay, now that we have the collisionX done,

47
00:02:31,80 --> 00:02:34,70
we need to do the function for collisionY.

48
00:02:34,70 --> 00:02:36,60
Checking the top is easy.

49
00:02:36,60 --> 00:02:38,40
We can use the same construct

50
00:02:38,40 --> 00:02:40,80
as checking the left and right boundaries.

51
00:02:40,80 --> 00:02:46,30
So, the first statement will be if ballTop

52
00:02:46,30 --> 00:02:52,40
is less than four, if that's the case,

53
00:02:52,40 --> 00:02:54,10
we know a collision happened with the top

54
00:02:54,10 --> 00:02:56,40
of the gaming area.

55
00:02:56,40 --> 00:02:59,20
We'll return true.

56
00:02:59,20 --> 00:03:01,20
Checking for the intersection with the paddles,

57
00:03:01,20 --> 00:03:03,10
a little more difficult.

58
00:03:03,10 --> 00:03:05,20
First, we need to check when the ball approaches

59
00:03:05,20 --> 00:03:06,50
the top of the paddle.

60
00:03:06,50 --> 00:03:08,90
Then, we need to compare the ball's left property

61
00:03:08,90 --> 00:03:12,30
to the left and right edges of the paddle.

62
00:03:12,30 --> 00:03:16,00
Okay, so that's going to be a nested conditional statement.

63
00:03:16,00 --> 00:03:21,60
Let's start with if ballTop is greater than

64
00:03:21,60 --> 00:03:22,70
the top of the paddle.

65
00:03:22,70 --> 00:03:27,90
So, that's playing area height minus 64.

66
00:03:27,90 --> 00:03:29,90
Then, inside that conditional, we need to check

67
00:03:29,90 --> 00:03:32,20
whether or not the ball intersects with the paddle.

68
00:03:32,20 --> 00:03:38,90
So, that'll be if ballLeft is greater than or equal to

69
00:03:38,90 --> 00:03:41,40
the left edge of the paddle, which we have stored

70
00:03:41,40 --> 00:03:44,10
in our paddleLeft variable.

71
00:03:44,10 --> 00:03:47,70
Then, we need an and statement because the ball

72
00:03:47,70 --> 00:03:51,50
has to be in between the paddleLeft and the paddleRight.

73
00:03:51,50 --> 00:03:56,30
So, in this case, the ballLeft value has to be less than

74
00:03:56,30 --> 00:03:58,60
or equal to the paddleLeft,

75
00:03:58,60 --> 00:04:02,20
and then, the paddle is 64 pixels wide

76
00:04:02,20 --> 00:04:06,20
so we'll say paddleLeft plus 64.

77
00:04:06,20 --> 00:04:08,80
Okay, so if both those things are true,

78
00:04:08,80 --> 00:04:12,80
we know we've collided with the paddle, we'll return true,

79
00:04:12,80 --> 00:04:15,10
and of course, if none of them are true,

80
00:04:15,10 --> 00:04:17,80
we're going to return false.

81
00:04:17,80 --> 00:04:19,60
Notice though that the return statement

82
00:04:19,60 --> 00:04:21,70
in the first conditional statement negates the need

83
00:04:21,70 --> 00:04:25,20
for an else clause as if it evaluates to true,

84
00:04:25,20 --> 00:04:29,00
then the second conditional statement is never executed.

