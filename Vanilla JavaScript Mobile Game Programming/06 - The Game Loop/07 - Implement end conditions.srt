1
00:00:00,30 --> 00:00:01,40
- Okay, we still need to write

2
00:00:01,40 --> 00:00:03,90
the game over function, so let's do that now.

3
00:00:03,90 --> 00:00:06,60
Add the game over function to the script,

4
00:00:06,60 --> 00:00:08,20
and here we'll describe what happens

5
00:00:08,20 --> 00:00:10,00
when the game ends.

6
00:00:10,00 --> 00:00:12,40
Okay, the first thing to do, is to stop

7
00:00:12,40 --> 00:00:14,40
the animation, so let's make a call

8
00:00:14,40 --> 00:00:18,50
to cancel animation frame, and to that

9
00:00:18,50 --> 00:00:20,60
we'll pass in the timer that we initialize

10
00:00:20,60 --> 00:00:23,60
when call request animation frame.

11
00:00:23,60 --> 00:00:26,60
Next, let's modify the score label,

12
00:00:26,60 --> 00:00:29,50
to let the user know the game has ended.

13
00:00:29,50 --> 00:00:32,10
So, let's set the inner HTML property

14
00:00:32,10 --> 00:00:35,60
of the score object by appending

15
00:00:35,60 --> 00:00:40,60
a few spaces, and then the words game over.

16
00:00:40,60 --> 00:00:41,50
And then finally,

17
00:00:41,50 --> 00:00:45,10
let's set the background color of the score label

18
00:00:45,10 --> 00:00:48,60
to a medium red.

19
00:00:48,60 --> 00:00:54,50
So how about score.style.background color,

20
00:00:54,50 --> 00:00:57,60
and we'll set that equal to, as a string,

21
00:00:57,60 --> 00:01:05,00
let's use the RGB function, and I'll do 12800.

22
00:01:05,00 --> 00:01:08,60
Okay, so, we should have a functioning game working now.

23
00:01:08,60 --> 00:01:11,90
Save that, make sure all of your files are saved,

24
00:01:11,90 --> 00:01:14,80
and then we can navigate back to Chrome,

25
00:01:14,80 --> 00:01:18,10
and the ball should start moving,

26
00:01:18,10 --> 00:01:19,70
see it changed directions,

27
00:01:19,70 --> 00:01:22,10
and then when the ball gets past the paddle,

28
00:01:22,10 --> 00:01:25,10
the game over function fires.

29
00:01:25,10 --> 00:01:28,90
By reloading the page, we can start the game over,

30
00:01:28,90 --> 00:01:34,20
and we can see that the score increments,

31
00:01:34,20 --> 00:01:36,10
and then the ball starts to move faster

32
00:01:36,10 --> 00:01:40,90
for each thousand points.

33
00:01:40,90 --> 00:01:41,70
Pretty cool.

34
00:01:41,70 --> 00:01:44,00
We've got a fully functioning game.

