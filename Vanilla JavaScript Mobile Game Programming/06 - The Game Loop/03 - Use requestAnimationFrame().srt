1
00:00:00,30 --> 00:00:02,20
- [Instructor] Using request the animation frame

2
00:00:02,20 --> 00:00:03,60
is pretty simple.

3
00:00:03,60 --> 00:00:05,00
We'll just need to change the call

4
00:00:05,00 --> 00:00:06,80
to set time out in the end conditions,

5
00:00:06,80 --> 00:00:09,50
to call request animation frame.

6
00:00:09,50 --> 00:00:13,50
So, find the start function and then in the end conditions

7
00:00:13,50 --> 00:00:15,50
where set time out is called,

8
00:00:15,50 --> 00:00:18,00
we're going to set the timer equal to

9
00:00:18,00 --> 00:00:21,80
a call to request animation frame.

10
00:00:21,80 --> 00:00:23,90
And then to request animation frame

11
00:00:23,90 --> 00:00:26,50
we'll pass in a single parameter

12
00:00:26,50 --> 00:00:28,30
and that's the code we're going to run

13
00:00:28,30 --> 00:00:31,30
for each frame in the animation.

14
00:00:31,30 --> 00:00:33,80
Notice it's a single call back,

15
00:00:33,80 --> 00:00:37,00
for us that's the game loop function, our start function.

16
00:00:37,00 --> 00:00:39,00
Also notice that there's no delay

17
00:00:39,00 --> 00:00:40,70
to establish the frame rate.

18
00:00:40,70 --> 00:00:42,90
That's good for us because that delay

19
00:00:42,90 --> 00:00:46,00
the frame rate is managed by the browser.

20
00:00:46,00 --> 00:00:49,10
Okay, to get the game rolling all we now need to do

21
00:00:49,10 --> 00:00:51,40
is add a call to request animation frame

22
00:00:51,40 --> 00:00:52,90
in the init function.

23
00:00:52,90 --> 00:00:56,70
So I'm going to copy the assignment of the timer

24
00:00:56,70 --> 00:00:58,90
to request animation frame.

25
00:00:58,90 --> 00:01:01,60
And then I'm going to find the init function

26
00:01:01,60 --> 00:01:03,40
and at the bottom of the init function

27
00:01:03,40 --> 00:01:08,80
I'm going to start the game loop by pasting in

28
00:01:08,80 --> 00:01:11,60
the call to request animation frame.

29
00:01:11,60 --> 00:01:13,20
This is the normal flow of events

30
00:01:13,20 --> 00:01:15,50
when using request animation frame.

31
00:01:15,50 --> 00:01:18,10
You call initially to get things going

32
00:01:18,10 --> 00:01:19,80
and then it's called repeatedly

33
00:01:19,80 --> 00:01:22,00
from within the call back function.

