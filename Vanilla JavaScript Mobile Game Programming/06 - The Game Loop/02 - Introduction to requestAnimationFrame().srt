1
00:00:00,50 --> 00:00:01,30
- [Instructor] Before we move on

2
00:00:01,30 --> 00:00:03,30
to write the four functions we just called

3
00:00:03,30 --> 00:00:06,20
from the start method we need to address the shortcomings

4
00:00:06,20 --> 00:00:09,70
of setTimeout and its cousin setInterval.

5
00:00:09,70 --> 00:00:12,10
Although both are convenient, they can pose

6
00:00:12,10 --> 00:00:14,20
some significant problems especially

7
00:00:14,20 --> 00:00:16,60
on low end or mobile platforms.

8
00:00:16,60 --> 00:00:18,70
Both functions are resource hogs,

9
00:00:18,70 --> 00:00:20,60
eating up processor cycles even

10
00:00:20,60 --> 00:00:23,90
when the tab running the animation is invisible.

11
00:00:23,90 --> 00:00:25,90
Even worse, this will deplete

12
00:00:25,90 --> 00:00:29,20
your mobile users' batteries alarmingly fast.

13
00:00:29,20 --> 00:00:32,10
Both functions require a delay in milliseconds set

14
00:00:32,10 --> 00:00:34,70
by the developer before executing.

15
00:00:34,70 --> 00:00:36,70
The delay establishes the frame rate,

16
00:00:36,70 --> 00:00:39,10
or the number of frames per second.

17
00:00:39,10 --> 00:00:42,30
Since the frame rate for all devices can't be known

18
00:00:42,30 --> 00:00:45,80
compromises must be made to make the animation good enough

19
00:00:45,80 --> 00:00:47,50
instead of the best it can be.

20
00:00:47,50 --> 00:00:50,10
The result is often choppy animations

21
00:00:50,10 --> 00:00:52,60
because the browser can't accommodate

22
00:00:52,60 --> 00:00:55,10
the frame rate set by the developer.

23
00:00:55,10 --> 00:00:58,10
When set timeout fires sometimes the browser

24
00:00:58,10 --> 00:01:00,20
isn't ready to redraw the screen,

25
00:01:00,20 --> 00:01:02,40
so the frame is skipped.

26
00:01:02,40 --> 00:01:04,60
There has to be a better way, right?

27
00:01:04,60 --> 00:01:06,90
And there is, the window object

28
00:01:06,90 --> 00:01:10,20
in all new or major browsers, including mobile browsers,

29
00:01:10,20 --> 00:01:13,90
has a method named requestAnimationFrame,

30
00:01:13,90 --> 00:01:16,40
requestAnimationFrame is optimized

31
00:01:16,40 --> 00:01:18,70
by the browser to run at peak speed.

32
00:01:18,70 --> 00:01:21,10
Usually, 60 frames per second,

33
00:01:21,10 --> 00:01:24,90
the optimal speed for smooth animations.

34
00:01:24,90 --> 00:01:29,00
In addition, using requestAnimationFrame allows the browser

35
00:01:29,00 --> 00:01:33,30
to suspend animations executing on inactive tabs.

36
00:01:33,30 --> 00:01:36,00
The result is better page responsiveness

37
00:01:36,00 --> 00:01:39,30
on low end platforms, and increased battery life

38
00:01:39,30 --> 00:01:41,30
on mobile platforms.

39
00:01:41,30 --> 00:01:42,50
You can check out the docs

40
00:01:42,50 --> 00:01:46,00
at the Mozilla Developer Network for more information.

