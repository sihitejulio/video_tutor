1
00:00:00,50 --> 00:00:02,30
- [Professor] Rendering, is the process of drawing

2
00:00:02,30 --> 00:00:04,20
objects on the screen.

3
00:00:04,20 --> 00:00:05,20
In our case,

4
00:00:05,20 --> 00:00:07,70
we call a render function for each frame

5
00:00:07,70 --> 00:00:09,90
using request animation frame.

6
00:00:09,90 --> 00:00:12,50
So, our render function will need to update the state

7
00:00:12,50 --> 00:00:16,30
of the object that change programmatically during game play.

8
00:00:16,30 --> 00:00:20,20
Those objects are the balls position and the score.

9
00:00:20,20 --> 00:00:24,90
So, let's add a render function to this script.

10
00:00:24,90 --> 00:00:26,60
Inside the render function,

11
00:00:26,60 --> 00:00:28,60
lets add calls

12
00:00:28,60 --> 00:00:31,00
to move ball

13
00:00:31,00 --> 00:00:34,60
and update score.

14
00:00:34,60 --> 00:00:37,50
If we decide to render more objects on this screen

15
00:00:37,50 --> 00:00:41,00
we can add calls to functions here, later.

16
00:00:41,00 --> 00:00:43,50
I find this kind of organization to be a good way to

17
00:00:43,50 --> 00:00:48,00
manage what happens on a frame by frame basis.

18
00:00:48,00 --> 00:00:48,80
Okay.

19
00:00:48,80 --> 00:00:53,10
Now, we'll need to add the move ball function.

20
00:00:53,10 --> 00:00:56,10
We are storing the top and left values of the ball,

21
00:00:56,10 --> 00:00:58,50
in ball left and ball top.

22
00:00:58,50 --> 00:01:02,30
And the amount of change in DUI and DX.

23
00:01:02,30 --> 00:01:05,00
When we add DUI and DX to the ball left

24
00:01:05,00 --> 00:01:06,70
and ball top values,

25
00:01:06,70 --> 00:01:11,30
we're producing linear point to point animation on the ball.

26
00:01:11,30 --> 00:01:15,90
So, let's take our ball left value

27
00:01:15,90 --> 00:01:17,10
and

28
00:01:17,10 --> 00:01:18,30
add

29
00:01:18,30 --> 00:01:20,60
DX to it.

30
00:01:20,60 --> 00:01:24,90
We'll do this same thing for our ball top value.

31
00:01:24,90 --> 00:01:26,50
Only we'll add

32
00:01:26,50 --> 00:01:29,20
DUI to it.

33
00:01:29,20 --> 00:01:31,30
Once, we've calculated the new position

34
00:01:31,30 --> 00:01:33,20
of the ball for the next frame,

35
00:01:33,20 --> 00:01:35,40
we then need to draw it on the screen.

36
00:01:35,40 --> 00:01:38,20
So, we can say ball dot

37
00:01:38,20 --> 00:01:39,80
style

38
00:01:39,80 --> 00:01:40,80
dot

39
00:01:40,80 --> 00:01:41,90
left

40
00:01:41,90 --> 00:01:43,70
equals

41
00:01:43,70 --> 00:01:45,60
ball left

42
00:01:45,60 --> 00:01:47,30
plus our label

43
00:01:47,30 --> 00:01:48,30
pixels.

44
00:01:48,30 --> 00:01:50,60
And we can do the same thing,

45
00:01:50,60 --> 00:01:52,60
for the top property.

46
00:01:52,60 --> 00:01:54,50
Only we'll assign

47
00:01:54,50 --> 00:01:56,80
ball top

48
00:01:56,80 --> 00:01:59,90
and append the label as well.

49
00:01:59,90 --> 00:02:03,90
Dropping beneath that, lets add the update score function.

50
00:02:03,90 --> 00:02:08,00
So, function update score.

51
00:02:08,00 --> 00:02:09,80
Okay, here's where we are going to need to decide

52
00:02:09,80 --> 00:02:11,60
on the scoring system.

53
00:02:11,60 --> 00:02:13,80
To keep things as simple as possible,

54
00:02:13,80 --> 00:02:16,30
I'm just going to increment the current score value by

55
00:02:16,30 --> 00:02:19,40
five points while the ball is in play.

56
00:02:19,40 --> 00:02:22,00
So, that is going to increment for each frame

57
00:02:22,00 --> 00:02:23,10
of the animation.

58
00:02:23,10 --> 00:02:27,30
So, that will say current score plus equals

59
00:02:27,30 --> 00:02:28,70
five

60
00:02:28,70 --> 00:02:31,70
and then once we have a new current score value.

61
00:02:31,70 --> 00:02:34,00
We need to display it to the user.

62
00:02:34,00 --> 00:02:34,90
So,

63
00:02:34,90 --> 00:02:38,30
lets grab out dom object and set it's inner

64
00:02:38,30 --> 00:02:41,30
HTML property

65
00:02:41,30 --> 00:02:42,70
equal to

66
00:02:42,70 --> 00:02:44,20
the score

67
00:02:44,20 --> 00:02:45,90
as a string

68
00:02:45,90 --> 00:02:50,00
and then we'll append to that our current score.

