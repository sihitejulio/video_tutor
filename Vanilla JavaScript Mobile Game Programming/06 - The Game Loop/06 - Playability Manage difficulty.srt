1
00:00:00,30 --> 00:00:01,90
- [Instructor] At this point, if we were

2
00:00:01,90 --> 00:00:04,80
to start playing Rebound, the ball would trudge along

3
00:00:04,80 --> 00:00:07,40
a pre-defined path and never change.

4
00:00:07,40 --> 00:00:09,10
That's not very playable.

5
00:00:09,10 --> 00:00:11,50
Users would be bored with the game pretty quickly.

6
00:00:11,50 --> 00:00:13,20
To make the game more playable,

7
00:00:13,20 --> 00:00:16,20
the difficulty should increase during game play.

8
00:00:16,20 --> 00:00:18,20
There are number of ways to accomplish this,

9
00:00:18,20 --> 00:00:21,70
as making it harder is somewhat subjective.

10
00:00:21,70 --> 00:00:24,10
With regards to Rebound, a simple way

11
00:00:24,10 --> 00:00:25,60
to make the game harder would be

12
00:00:25,60 --> 00:00:27,00
to move the ball more quickly

13
00:00:27,00 --> 00:00:29,10
toward the bottom of the playing area.

14
00:00:29,10 --> 00:00:31,10
Let's implement that now.

15
00:00:31,10 --> 00:00:34,60
Let's add the difficulty function to our script.

16
00:00:34,60 --> 00:00:36,50
So I'm going to change the value of dy,

17
00:00:36,50 --> 00:00:38,40
which represents the vertical speed

18
00:00:38,40 --> 00:00:41,80
for each 1,000 points scored by the user.

19
00:00:41,80 --> 00:00:44,60
So we'll do a simple conditional that says

20
00:00:44,60 --> 00:00:52,40
if(currentScore_% 1000 ==0).

21
00:00:52,40 --> 00:00:54,30
In other words, every time the user hits

22
00:00:54,30 --> 00:00:58,20
that thousand point mark, that re-evaluates the true.

23
00:00:58,20 --> 00:01:02,40
In that case, what I'm going to do is

24
00:01:02,40 --> 00:01:06,00
increase the magnitude of dy.

25
00:01:06,00 --> 00:01:10,70
So first we have to check whether or not a dy is positive.

26
00:01:10,70 --> 00:01:14,80
If dy is positive, it will be greater than zero.

27
00:01:14,80 --> 00:01:20,40
In that case, we'll add two to dy.

28
00:01:20,40 --> 00:01:25,20
Otherwise, we'll subtract two from dy.

29
00:01:25,20 --> 00:01:28,90
In either case, it will make the ball move more quickly

30
00:01:28,90 --> 00:01:31,50
towards the bottom of the screen,

31
00:01:31,50 --> 00:01:33,90
but it will also change the path.

32
00:01:33,90 --> 00:01:37,10
And that means that the path won't be pre-determined

33
00:01:37,10 --> 00:01:39,00
based on the timing of the game.

