1
00:00:03,70 --> 00:00:04,90
- [Instructor] Let's have a look.

2
00:00:04,90 --> 00:00:07,40
We have a function which is a returnType.

3
00:00:07,40 --> 00:00:11,30
It gets a URL, and now it gets an additional parameter,

4
00:00:11,30 --> 00:00:15,00
it's the output channel which is going to send the result.

5
00:00:15,00 --> 00:00:18,00
We do a get, and if there is an error,

6
00:00:18,00 --> 00:00:21,00
we just send an error to the channel,

7
00:00:21,00 --> 00:00:24,80
otherwise we do the content-type

8
00:00:24,80 --> 00:00:30,00
and we send the result to the channel.

9
00:00:30,00 --> 00:00:32,40
In the main, we have the list of URLs

10
00:00:32,40 --> 00:00:36,70
and we create the channel, which is the result channel.

11
00:00:36,70 --> 00:00:41,60
And every time we call go with returnType,

12
00:00:41,60 --> 00:00:44,00
which we give it the URL and the channel

13
00:00:44,00 --> 00:00:47,00
to send the result on.

14
00:00:47,00 --> 00:00:50,40
Line 34, since we know the number of go routines,

15
00:00:50,40 --> 00:00:53,00
which is how many URLs we want to look,

16
00:00:53,00 --> 00:00:55,40
we can do for range urls,

17
00:00:55,40 --> 00:00:59,50
and then we get the result from the result channel

18
00:00:59,50 --> 00:01:01,90
and we print it out.

19
00:01:01,90 --> 00:01:04,50
Let's save it and run.

20
00:01:04,50 --> 00:01:09,10
Go run sites.go.

21
00:01:09,10 --> 00:01:11,00
And we get one from every site.

