1
00:00:03,60 --> 00:00:05,10
- [Narrator] In the goroutines video

2
00:00:05,10 --> 00:00:08,60
you ran several goroutines to get data from websites.

3
00:00:08,60 --> 00:00:10,90
You used sync.WaitGroup to wait

4
00:00:10,90 --> 00:00:12,60
for the goroutines to finish.

5
00:00:12,60 --> 00:00:16,00
Change it to use channels instead.

