1
00:00:00,60 --> 00:00:02,40
- [Instructor] In this exercise, you'll calculate

2
00:00:02,40 --> 00:00:06,50
the MD5 signature of several files concurrently.

3
00:00:06,50 --> 00:00:08,90
Your input is a file where every line has

4
00:00:08,90 --> 00:00:11,40
an expected signature and a file name,

5
00:00:11,40 --> 00:00:14,00
very much like the example below.

6
00:00:14,00 --> 00:00:16,90
You need to parse the file, and then for every file,

7
00:00:16,90 --> 00:00:20,60
calculate the actual MD5 signature of the file.

8
00:00:20,60 --> 00:00:21,50
And if it doesn't match

9
00:00:21,50 --> 00:00:25,60
the expected value, print an error message.

10
00:00:25,60 --> 00:00:31,70
Your data is in nasa-logs.zip file in the exercise files.

11
00:00:31,70 --> 00:00:34,10
You'll find implementation of MD5

12
00:00:34,10 --> 00:00:38,20
in the crypto.md5 package in the standard library.

13
00:00:38,20 --> 00:00:42,00
Use bufio.Scanner to iterate over lines in a file,

14
00:00:42,00 --> 00:00:45,00
and use the strings.Fields to split a line.

