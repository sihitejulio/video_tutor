1
00:00:00,60 --> 00:00:02,20
- [Instructor] The preferred way to communicate

2
00:00:02,20 --> 00:00:05,40
between go routines is by using channels.

3
00:00:05,40 --> 00:00:08,80
Channels are typed, one-directional pipes.

4
00:00:08,80 --> 00:00:10,80
You send values at one end

5
00:00:10,80 --> 00:00:13,80
and receive them in the other end.

6
00:00:13,80 --> 00:00:15,50
If you try to receive and there's nothing

7
00:00:15,50 --> 00:00:18,90
in the channel, you'll get blocked.

8
00:00:18,90 --> 00:00:21,90
The pushing side is a bit more complicated.

9
00:00:21,90 --> 00:00:24,00
There are actually two kinds of channels,

10
00:00:24,00 --> 00:00:26,50
buffered and unbuffered.

11
00:00:26,50 --> 00:00:29,10
When you push a value through an unbuffered channel

12
00:00:29,10 --> 00:00:32,80
you'll get blocked until someone receives on the other hand.

13
00:00:32,80 --> 00:00:35,50
With buffer channels it's a bit different.

14
00:00:35,50 --> 00:00:38,10
Each buffer channel has a capacity.

15
00:00:38,10 --> 00:00:39,80
If you send to the buffer channel

16
00:00:39,80 --> 00:00:42,90
until the capacity is filled you won't block.

17
00:00:42,90 --> 00:00:44,90
After the capacity is full,

18
00:00:44,90 --> 00:00:48,20
ascent to a channel will be blocked.

19
00:00:48,20 --> 00:00:52,50
In a way, buffer channels are somewhat like bonded queues.

20
00:00:52,50 --> 00:00:54,60
Here is an example, we created

21
00:00:54,60 --> 00:00:57,90
the buffer channel with a capacity of three.

22
00:00:57,90 --> 00:01:01,50
When we send the first three values we are not blocked.

23
00:01:01,50 --> 00:01:04,60
However, once the capacity is full

24
00:01:04,60 --> 00:01:09,40
and we try to send the value four we will get blocked.

25
00:01:09,40 --> 00:01:11,20
Let's start with a simple example

26
00:01:11,20 --> 00:01:14,30
and grow it to something practical.

27
00:01:14,30 --> 00:01:16,30
In line nine we create a channel

28
00:01:16,30 --> 00:01:19,40
using the make builtin function.

29
00:01:19,40 --> 00:01:22,80
This channel is a channel of integers

30
00:01:22,80 --> 00:01:24,20
and we are trying to receive from

31
00:01:24,20 --> 00:01:27,90
the channel and print out we are here.

32
00:01:27,90 --> 00:01:31,70
Let's save this, (typing) go run chan.go,

33
00:01:31,70 --> 00:01:34,50
and go is telling us that all go routines

34
00:01:34,50 --> 00:01:37,40
are asleep, there is a deadlock.

35
00:01:37,40 --> 00:01:41,50
This is because nothing was pushed to the channel.

36
00:01:41,50 --> 00:01:45,30
Let's comment this out and try something else.

37
00:01:45,30 --> 00:01:47,10
(typing)

38
00:01:47,10 --> 00:01:51,90
Here we create a go routine that sends a value

39
00:01:51,90 --> 00:01:54,90
to the channel, and in the main we receive

40
00:01:54,90 --> 00:01:58,70
the value from the channel and print out the value.

41
00:01:58,70 --> 00:02:00,80
(typing)

42
00:02:00,80 --> 00:02:03,10
Let's save this and run.

43
00:02:03,10 --> 00:02:05,40
(typing)

44
00:02:05,40 --> 00:02:09,20
And we got the number that we sent to the channel.

45
00:02:09,20 --> 00:02:13,40
Let's see how we're blocked on multiple send/receive.

46
00:02:13,40 --> 00:02:15,60
So, we create a go routine that runs

47
00:02:15,60 --> 00:02:17,90
three times, every time sending a value

48
00:02:17,90 --> 00:02:21,70
to the channel and sleeping for a second,

49
00:02:21,70 --> 00:02:25,60
and in the main we receive three times

50
00:02:25,60 --> 00:02:29,10
from the channel and print out what was received.

51
00:02:29,10 --> 00:02:33,50
Of course we need to import also the time package,

52
00:02:33,50 --> 00:02:43,70
(typing) so we can use it.

53
00:02:43,70 --> 00:02:48,20
Let's save this and run.

54
00:02:48,20 --> 00:02:50,30
We see sending zero and receiving zero,

55
00:02:50,30 --> 00:02:55,20
sending one, receiving one, sending two, and receiving two.

56
00:02:55,20 --> 00:02:59,40
Everything is in order, first we send then we receive.

57
00:02:59,40 --> 00:03:01,80
If you don't know the number of items in the channel,

58
00:03:01,80 --> 00:03:06,70
the sender can signal it is done by closing the channel.

59
00:03:06,70 --> 00:03:10,80
(typing)

60
00:03:10,80 --> 00:03:13,40
So, we create a go routine, and again,

61
00:03:13,40 --> 00:03:15,70
we push three times to the channel

62
00:03:15,70 --> 00:03:19,00
and at the end we close the channel,

63
00:03:19,00 --> 00:03:21,20
and this time we are going to use the builtin

64
00:03:21,20 --> 00:03:25,00
range keyword to iterate over the channel,

65
00:03:25,00 --> 00:03:28,80
and every time we will print the value we received.

66
00:03:28,80 --> 00:03:29,80
(typing)

67
00:03:29,80 --> 00:03:33,70
Let's run this one.

68
00:03:33,70 --> 00:03:37,00
And again, we see sending and receiving, in order.

