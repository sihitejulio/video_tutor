1
00:00:00,60 --> 00:00:02,40
- [Instructor] One of Go's many advantages

2
00:00:02,40 --> 00:00:04,30
over traditional languages,

3
00:00:04,30 --> 00:00:07,00
is the way it can handle concurrency.

4
00:00:07,00 --> 00:00:09,30
To quote Rob Pike, in his excellent

5
00:00:09,30 --> 00:00:12,30
Concurrency is not Parallelism talk,

6
00:00:12,30 --> 00:00:14,30
which I highly recommend you watch,

7
00:00:14,30 --> 00:00:16,10
concurrency is the composition

8
00:00:16,10 --> 00:00:18,90
of independently executed processes,

9
00:00:18,90 --> 00:00:22,70
while parallelism, is the simultaneous execution.

10
00:00:22,70 --> 00:00:26,40
Go's concurrency primitive is a go routine.

11
00:00:26,40 --> 00:00:30,30
It's very lightweight, and unlike processes or threads,

12
00:00:30,30 --> 00:00:32,10
you can spin tens of thousands

13
00:00:32,10 --> 00:00:35,60
of go routines on a single machine.

14
00:00:35,60 --> 00:00:38,00
Say you'd like to query a bunch of URLs,

15
00:00:38,00 --> 00:00:39,10
and see whether they return

16
00:00:39,10 --> 00:00:42,60
HTML, JSON, XML, or other format.

17
00:00:42,60 --> 00:00:45,60
You'll do it by looking into the content type

18
00:00:45,60 --> 00:00:49,60
http header in the response.

19
00:00:49,60 --> 00:00:53,00
So we have our function, returnType,

20
00:00:53,00 --> 00:00:56,10
which does an http call, and then,

21
00:00:56,10 --> 00:01:00,10
looks at the header and prints it out.

22
00:01:00,10 --> 00:01:03,90
In our main, we define a slice of URLs,

23
00:01:03,90 --> 00:01:06,20
and then for every URL,

24
00:01:06,20 --> 00:01:10,20
we call the function returnType for these URLs.

25
00:01:10,20 --> 00:01:13,30
Let's save this and time it.

26
00:01:13,30 --> 00:01:16,80
So time go run sites dot go.

27
00:01:16,80 --> 00:01:21,70
So time go run sites dot go.

28
00:01:21,70 --> 00:01:26,60
And we can see that it took about 5.7 seconds, to execute.

29
00:01:26,60 --> 00:01:28,80
Because we are blocked on every site,

30
00:01:28,80 --> 00:01:33,00
this is a very promising kind of data for concurrency.

31
00:01:33,00 --> 00:01:36,20
What we're going to do is going to use the Go keyword,

32
00:01:36,20 --> 00:01:40,50
to generate a go routines, and create an anonymous function.

33
00:01:40,50 --> 00:01:43,80
Okay, so get the URL as a string,

34
00:01:43,80 --> 00:01:48,40
and calls returnType with this URL,

35
00:01:48,40 --> 00:01:54,90
and we are going to call this function with the URL we get.

36
00:01:54,90 --> 00:02:00,70
Let's save and run this one.

37
00:02:00,70 --> 00:02:02,80
Nothing is printed out.

38
00:02:02,80 --> 00:02:05,20
The go run time will not wait for go routines

39
00:02:05,20 --> 00:02:07,10
that are still working.

40
00:02:07,10 --> 00:02:09,00
You can do a sleep to wait for them,

41
00:02:09,00 --> 00:02:12,50
but this is a very bad form of synchronization.

42
00:02:12,50 --> 00:02:15,60
Go has the sync package with a bunch of utilities

43
00:02:15,60 --> 00:02:17,90
synchronized between go routines,

44
00:02:17,90 --> 00:02:20,90
but the recommended way is to use channels,

45
00:02:20,90 --> 00:02:24,50
which are covered in another chapter.

46
00:02:24,50 --> 00:02:26,70
You can use WaitGroup, which is exactly

47
00:02:26,70 --> 00:02:31,50
for waiting for a bunch of go routines to finish.

48
00:02:31,50 --> 00:02:36,80
So we are going to import sync,

49
00:02:36,80 --> 00:02:46,80
and then in our main, we are going to create a WaitGroup.

50
00:02:46,80 --> 00:02:50,10
And then for every go routine that we are going to spin,

51
00:02:50,10 --> 00:02:51,60
so we are going to tell the WaitGroup

52
00:02:51,60 --> 00:02:54,60
that we added another worker,

53
00:02:54,60 --> 00:02:56,50
and inside the go routine,

54
00:02:56,50 --> 00:03:00,50
we are going to signal that we are done.

55
00:03:00,50 --> 00:03:02,00
And lastly we are going to wait

56
00:03:02,00 --> 00:03:05,60
for all the go routines to be finished.

57
00:03:05,60 --> 00:03:10,60
Let's save this and run.

58
00:03:10,60 --> 00:03:13,10
And this time, we see the execution,

59
00:03:13,10 --> 00:03:21,00
and it's 1.75 seconds, which is the original 5.7 seconds.

