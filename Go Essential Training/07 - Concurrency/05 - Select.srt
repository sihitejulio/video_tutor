1
00:00:00,60 --> 00:00:02,40
- [Instructor] The built-in select function

2
00:00:02,40 --> 00:00:06,10
allow us to work with several channels together.

3
00:00:06,10 --> 00:00:08,40
Every time a channel with selecting on

4
00:00:08,40 --> 00:00:12,70
become free, either for sending or receiving,

5
00:00:12,70 --> 00:00:15,10
you'll do an action on this channel.

6
00:00:15,10 --> 00:00:16,40
Let's have a look.

7
00:00:16,40 --> 00:00:19,90
We'll create two channels, channel one and channel two.

8
00:00:19,90 --> 00:00:22,70
And then we run a function that sends a value

9
00:00:22,70 --> 00:00:26,00
to channel one, and now we can do a select.

10
00:00:26,00 --> 00:00:28,90
The select statement is select,

11
00:00:28,90 --> 00:00:32,20
then the curly braces, and then,

12
00:00:32,20 --> 00:00:35,60
a case for every channel that you'd like to select on.

13
00:00:35,60 --> 00:00:39,10
In our case, we expect value from channel one,

14
00:00:39,10 --> 00:00:41,00
and we print out that we got the value

15
00:00:41,00 --> 00:00:44,10
from channel one, and another case,

16
00:00:44,10 --> 00:00:46,00
we get the value from channel two,

17
00:00:46,00 --> 00:00:50,50
and we print that we got the value from channel two.

18
00:00:50,50 --> 00:00:52,30
Let's save it and run it.

19
00:00:52,30 --> 00:00:56,00
Go run select dot go, and we see

20
00:00:56,00 --> 00:00:58,50
that we got 42 from channel one.

21
00:00:58,50 --> 00:01:00,10
One of the common uses for select

22
00:01:00,10 --> 00:01:04,00
is to implement timeouts.

23
00:01:04,00 --> 00:01:07,30
So we're going to make a channel,

24
00:01:07,30 --> 00:01:09,80
and then create a function that sleeps

25
00:01:09,80 --> 00:01:15,90
for 100 milliseconds, and then sends a value on the channel.

26
00:01:15,90 --> 00:01:19,00
And then we do a select.

27
00:01:19,00 --> 00:01:22,00
One case, we get the value from the output channel,

28
00:01:22,00 --> 00:01:24,20
and we print it out, and the other one,

29
00:01:24,20 --> 00:01:26,50
we're using time dot after.

30
00:01:26,50 --> 00:01:28,90
Time dot after, creates a channel

31
00:01:28,90 --> 00:01:33,60
that a value will be sent on it after 200 milliseconds.

32
00:01:33,60 --> 00:01:38,60
And we print out the timeout, if we got the timeout.

33
00:01:38,60 --> 00:01:41,20
And of course, we need to import

34
00:01:41,20 --> 00:01:45,00
the time package in order to work with it.

35
00:01:45,00 --> 00:01:48,60
Let's save it and run it: go run select dot go.

36
00:01:48,60 --> 00:01:51,20
Let's save it and run it: go run select dot go.

37
00:01:51,20 --> 00:01:54,10
And we see that we got the value.

38
00:01:54,10 --> 00:01:55,20
Now let's go ahead and change

39
00:01:55,20 --> 00:01:57,80
the timeout to 20 milliseconds.

40
00:01:57,80 --> 00:02:01,30
We are going to save it and run it again.

41
00:02:01,30 --> 00:02:03,80
And this time, we get a timeout.

42
00:02:03,80 --> 00:02:06,40
Note, that this does not kill the go routine

43
00:02:06,40 --> 00:02:07,90
It will work in the background,

44
00:02:07,90 --> 00:02:12,00
but since go routines are cheap, this is not a big deal.

45
00:02:12,00 --> 00:02:13,70
The context package provides

46
00:02:13,70 --> 00:02:15,80
consolation and timeout policies.

47
00:02:15,80 --> 00:02:19,10
It provides a context object with down channel

48
00:02:19,10 --> 00:02:20,00
that you can select on.

