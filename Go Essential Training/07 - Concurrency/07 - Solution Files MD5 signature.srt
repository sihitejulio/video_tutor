1
00:00:00,60 --> 00:00:01,50
- [Instructor] Let's start with

2
00:00:01,50 --> 00:00:03,20
a function to parse the signature file.

3
00:00:03,20 --> 00:00:08,00
As a reminder, the signature files looks like this.

4
00:00:08,00 --> 00:00:09,90
We have a line, and after the line,

5
00:00:09,90 --> 00:00:14,00
there's a signature, and the name of the file.

6
00:00:14,00 --> 00:00:16,30
Parse signatures file gets a path

7
00:00:16,30 --> 00:00:18,60
which is a string and we return a map

8
00:00:18,60 --> 00:00:21,10
from the string, which is the path

9
00:00:21,10 --> 00:00:23,70
to the expected signature, and also an error

10
00:00:23,70 --> 00:00:25,60
if an error happened.

11
00:00:25,60 --> 00:00:27,30
At first, we try to open the file,

12
00:00:27,30 --> 00:00:30,00
and if there is an error, we return the error,

13
00:00:30,00 --> 00:00:32,60
and nil for the map itself.

14
00:00:32,60 --> 00:00:36,20
We use defer to make sure the file is closed,

15
00:00:36,20 --> 00:00:40,20
and then we create the map and use a scanner

16
00:00:40,20 --> 00:00:43,10
to go over the file.

17
00:00:43,10 --> 00:00:46,90
We use a for loop, we say that the line number is one,

18
00:00:46,90 --> 00:00:51,00
and as long as the scanner.Scan returns something true,

19
00:00:51,00 --> 00:00:53,60
we increment the line number.

20
00:00:53,60 --> 00:00:59,30
Use a strings.Fields to split the line into several fields,

21
00:00:59,30 --> 00:01:02,30
and we get the line from scanner.Text.

22
00:01:02,30 --> 00:01:04,00
If the length of the fields is not two,

23
00:01:04,00 --> 00:01:06,30
there is something wrong in the file.

24
00:01:06,30 --> 00:01:09,30
So, we return nil, and an error saying

25
00:01:09,30 --> 00:01:12,00
that this was a bad line.

26
00:01:12,00 --> 00:01:13,90
It's important to add as much information

27
00:01:13,90 --> 00:01:16,20
as you can inside the error message.

28
00:01:16,20 --> 00:01:19,00
In our case, we put exactly the path

29
00:01:19,00 --> 00:01:21,80
and the line number where the error happened.

30
00:01:21,80 --> 00:01:29,00
Otherwise, we set the signature for this file in the map.

31
00:01:29,00 --> 00:01:30,80
At the end of the for loop, we need to check

32
00:01:30,80 --> 00:01:32,90
if there was any error while scanning.

33
00:01:32,90 --> 00:01:35,90
If there was any error, we just return this error.

34
00:01:35,90 --> 00:01:37,80
Otherwise, everything went well,

35
00:01:37,80 --> 00:01:43,50
so return the map of signature and nil for an error.

36
00:01:43,50 --> 00:01:45,30
Another function is the function

37
00:01:45,30 --> 00:01:49,00
to calculate the MD5 signature of the file.

38
00:01:49,00 --> 00:01:53,20
It gets a path, which is a string, and return a string,

39
00:01:53,20 --> 00:01:57,50
which is the MD5 signature and a possible error.

40
00:01:57,50 --> 00:01:59,80
We open the file, and if there is an error,

41
00:01:59,80 --> 00:02:03,00
we return the empty string and the error.

42
00:02:03,00 --> 00:02:06,60
We defer the file.Close to make sure the file is closed,

43
00:02:06,60 --> 00:02:10,70
and then, we create a new md5 hash signature,

44
00:02:10,70 --> 00:02:15,80
and we use the io.Copy, io.Copy copies data

45
00:02:15,80 --> 00:02:18,90
from a reader to a writer, and we check

46
00:02:18,90 --> 00:02:22,60
if there is an error while copying, we return the error.

47
00:02:22,60 --> 00:02:25,50
Otherwise, we return the signature

48
00:02:25,50 --> 00:02:29,20
as a hexadecimal string, and a nil for error,

49
00:02:29,20 --> 00:02:31,20
and now, we are going to create a worker.

50
00:02:31,20 --> 00:02:35,70
The worker will run in a go team, and we process the file.

51
00:02:35,70 --> 00:02:38,30
We are going to use an output channel

52
00:02:38,30 --> 00:02:39,50
which holds the results in it.

53
00:02:39,50 --> 00:02:42,10
The result is the path, the file path,

54
00:02:42,10 --> 00:02:45,80
and match for whether it matched or not,

55
00:02:45,80 --> 00:02:48,20
the expected signature, and an error

56
00:02:48,20 --> 00:02:49,90
if there was some kind of an error.

57
00:02:49,90 --> 00:02:53,00
For example, it couldn't open the file.

58
00:02:53,00 --> 00:02:55,70
The md5Worker gets the path of the file,

59
00:02:55,70 --> 00:02:58,50
the expected signature, and then the output channel

60
00:02:58,50 --> 00:02:59,60
to send the result.

61
00:02:59,60 --> 00:03:01,70
We start by creating a result object

62
00:03:01,70 --> 00:03:04,50
with the path set in it, and then

63
00:03:04,50 --> 00:03:08,60
we call the fileMD5 to calculate the signature.

64
00:03:08,60 --> 00:03:10,30
If there was an error, we set the error

65
00:03:10,30 --> 00:03:14,60
in the result object, and send it over on the channel.

66
00:03:14,60 --> 00:03:18,50
Otherwise, we set the match if the signature

67
00:03:18,50 --> 00:03:20,80
matches the expected signature,

68
00:03:20,80 --> 00:03:27,30
and we sent the result on the channel.

69
00:03:27,30 --> 00:03:31,30
In the main, we first start by passing the signature file,

70
00:03:31,30 --> 00:03:33,60
getting the signature's map and an error.

71
00:03:33,60 --> 00:03:36,40
If there is an error, we use the log of Fatalf

72
00:03:36,40 --> 00:03:39,50
to create an error and abort the program.

73
00:03:39,50 --> 00:03:41,40
Then, we create our results channel,

74
00:03:41,40 --> 00:03:45,40
and for every path and signature in the signature map,

75
00:03:45,40 --> 00:03:49,60
or phrase, we create a go team that runs our worker.

76
00:03:49,60 --> 00:03:52,20
We have a global ok flag telling us

77
00:03:52,20 --> 00:03:56,30
if there was an error or not, and

78
00:03:56,30 --> 00:03:58,20
for every signature, because we know

79
00:03:58,20 --> 00:04:00,60
exactly how many workers are there,

80
00:04:00,60 --> 00:04:04,40
we receive from the result channel,

81
00:04:04,40 --> 00:04:05,90
and then we're saying switch.

82
00:04:05,90 --> 00:04:08,60
If there was an error while processing this file,

83
00:04:08,60 --> 00:04:12,90
we print out the error, and we signal that ok is false.

84
00:04:12,90 --> 00:04:15,40
If there was no match, we print out

85
00:04:15,40 --> 00:04:18,80
that there was a signature mismatch at the path,

86
00:04:18,80 --> 00:04:22,30
and again, sets the global flag to false.

87
00:04:22,30 --> 00:04:26,70
At the end of processing, we check

88
00:04:26,70 --> 00:04:31,20
if the global flag is not ok, we exit with a non zero value.

89
00:04:31,20 --> 00:04:33,20
In order for this to work, you need

90
00:04:33,20 --> 00:04:38,30
to unpack nasa-logs.zip in the current directory,

91
00:04:38,30 --> 00:04:42,70
and now we can do go run md5.go,

92
00:04:42,70 --> 00:04:46,80
and everything ran as expected.

93
00:04:46,80 --> 00:04:48,10
Let's try and change the signature

94
00:04:48,10 --> 00:04:51,50
in the signature file.

95
00:04:51,50 --> 00:04:54,70
Let's change the first character to a instead of six,

96
00:04:54,70 --> 00:04:59,70
and save, and now when I run it,

97
00:04:59,70 --> 00:05:03,00
I will get that the first file had a signature mismatch.

