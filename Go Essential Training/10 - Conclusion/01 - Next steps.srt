1
00:00:00,60 --> 00:00:02,60
- [Instructor] We've covered a lot of ground here.

2
00:00:02,60 --> 00:00:04,80
From language basics to writing

3
00:00:04,80 --> 00:00:07,40
a key value in memory database.

4
00:00:07,40 --> 00:00:09,00
I hope you had fun along the way

5
00:00:09,00 --> 00:00:12,10
and that you found Go a language fitting

6
00:00:12,10 --> 00:00:13,80
for the modern day, multi-code,

7
00:00:13,80 --> 00:00:15,60
highly connected environment.

8
00:00:15,60 --> 00:00:18,60
There's more to learn about Go.

9
00:00:18,60 --> 00:00:21,00
Writing Go code is the best way to go.

10
00:00:21,00 --> 00:00:23,80
I encourage you to find a problem that's bothering you

11
00:00:23,80 --> 00:00:27,00
and use go to solve it.

12
00:00:27,00 --> 00:00:28,80
There are some great online resources

13
00:00:28,80 --> 00:00:30,90
that provide further information about Go

14
00:00:30,90 --> 00:00:34,70
to help you continue your learning.

15
00:00:34,70 --> 00:00:38,30
The Go language tutorial is a great start.

16
00:00:38,30 --> 00:00:40,20
It's an interactive tutorial

17
00:00:40,20 --> 00:00:44,90
that you can write Go to solve questions as you go.

18
00:00:44,90 --> 00:00:47,60
Effective Go is a great summary.

19
00:00:47,60 --> 00:00:51,30
I recommend rereading it frequently.

20
00:00:51,30 --> 00:00:55,90
The Package documentation is excellent, with many examples.

21
00:00:55,90 --> 00:00:58,30
And you can use the Go tag

22
00:00:58,30 --> 00:01:01,20
in stack overflow to find answers.

23
00:01:01,20 --> 00:01:06,30
And finally, feel free to contact me at 353 solutions.

24
00:01:06,30 --> 00:01:09,10
I'd love to tailor a Go workshop for your needs

25
00:01:09,10 --> 00:01:12,00
or kickstart your Go project with some consulting.

