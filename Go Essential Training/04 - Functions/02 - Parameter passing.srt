1
00:00:00,80 --> 00:00:02,70
- [Instructor] DoubleAt is a function

2
00:00:02,70 --> 00:00:04,90
that takes a slice of integers

3
00:00:04,90 --> 00:00:10,20
and an index and doubles the value at that index.

4
00:00:10,20 --> 00:00:12,80
Here in main we create a slice of values,

5
00:00:12,80 --> 00:00:14,60
one, two, three and four,

6
00:00:14,60 --> 00:00:16,80
then call our function

7
00:00:16,80 --> 00:00:21,30
saying that we want to double the value at index number two

8
00:00:21,30 --> 00:00:23,80
and then we are going to print it out.

9
00:00:23,80 --> 00:00:26,20
Let's save it and run.

10
00:00:26,20 --> 00:00:29,70
Go run params.go

11
00:00:29,70 --> 00:00:31,60
and we see one, two, six and four.

12
00:00:31,60 --> 00:00:34,80
Remember that Go uses zero-based indexing,

13
00:00:34,80 --> 00:00:37,50
so two is the third element.

14
00:00:37,50 --> 00:00:39,40
Here's another function.

15
00:00:39,40 --> 00:00:42,20
This one gets an integer

16
00:00:42,20 --> 00:00:44,30
and doubles its value.

17
00:00:44,30 --> 00:00:46,20
Let's use double in our code.

18
00:00:46,20 --> 00:00:49,60
We say that val equals to 10,

19
00:00:49,60 --> 00:00:51,90
we call double(val)

20
00:00:51,90 --> 00:00:56,70
and then we'll print the value out.

21
00:00:56,70 --> 00:01:01,80
Let's save this one and run it.

22
00:01:01,80 --> 00:01:05,60
And we see that the original value hasn't changed.

23
00:01:05,60 --> 00:01:07,40
What happened?

24
00:01:07,40 --> 00:01:09,90
When Go passes an integer to a function,

25
00:01:09,90 --> 00:01:12,40
Go passes it by value

26
00:01:12,40 --> 00:01:15,90
which means Go will create a copy of this integer

27
00:01:15,90 --> 00:01:18,20
and pass it to the function.

28
00:01:18,20 --> 00:01:21,60
Any changes to the integer inside the function

29
00:01:21,60 --> 00:01:24,50
won't affect the original value.

30
00:01:24,50 --> 00:01:29,30
However, when Go passes a slice or a map to a function,

31
00:01:29,30 --> 00:01:32,20
it passes it by reference.

32
00:01:32,20 --> 00:01:34,20
This means that inside the function body,

33
00:01:34,20 --> 00:01:36,20
you're working with the exact same object

34
00:01:36,20 --> 00:01:38,60
that was passed and any changes

35
00:01:38,60 --> 00:01:41,00
to the slice you make inside the function

36
00:01:41,00 --> 00:01:43,30
stay after the function is over.

37
00:01:43,30 --> 00:01:45,40
How can you make double work?

38
00:01:45,40 --> 00:01:48,40
You can do this by using pointers.

39
00:01:48,40 --> 00:01:50,90
If you're coming from C or C++,

40
00:01:50,90 --> 00:01:53,40
the word pointer might be associated

41
00:01:53,40 --> 00:01:56,30
with obscure code and crashes.

42
00:01:56,30 --> 00:02:00,40
Don't worry, Go's pointers are not like C or C++.

43
00:02:00,40 --> 00:02:01,90
They are much safer.

44
00:02:01,90 --> 00:02:04,20
You can pass a pointer to an object

45
00:02:04,20 --> 00:02:08,80
but you can't do the infamous pointer arithmetic.

46
00:02:08,80 --> 00:02:11,00
If you're coming from C or C++

47
00:02:11,00 --> 00:02:13,60
and really want to do pointer arithmetic,

48
00:02:13,60 --> 00:02:16,40
take a look at unsafe package

49
00:02:16,40 --> 00:02:20,30
but this is usually not what you want to do in Go.

50
00:02:20,30 --> 00:02:23,80
Let's see how this work.

51
00:02:23,80 --> 00:02:27,80
Here we have a new function called doublePtr.

52
00:02:27,80 --> 00:02:32,20
This function gets a pointer

53
00:02:32,20 --> 00:02:34,10
to an integer

54
00:02:34,10 --> 00:02:35,70
and inside the function body

55
00:02:35,70 --> 00:02:38,00
we are de-referencing the pointer

56
00:02:38,00 --> 00:02:41,20
and assign a value to it.

57
00:02:41,20 --> 00:02:46,20
Now let's see how we can use it.

58
00:02:46,20 --> 00:02:48,50
We call doublePtr with ampersand value,

59
00:02:48,50 --> 00:02:52,10
so we pass a pointer to a value

60
00:02:52,10 --> 00:02:56,70
and now we're going to print it again.

61
00:02:56,70 --> 00:02:59,10
Let's see how it work.

62
00:02:59,10 --> 00:03:01,40
And you see that this time the value of val

63
00:03:01,40 --> 00:03:03,00
was actually doubled.

