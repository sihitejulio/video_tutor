1
00:00:00,70 --> 00:00:04,00
- [Instructor] Go functions can return more than one value.

2
00:00:04,00 --> 00:00:07,50
This is used extensively in Go to signal errors.

3
00:00:07,50 --> 00:00:10,30
The function that can error will usually return

4
00:00:10,30 --> 00:00:14,00
the error value as the last value returned.

5
00:00:14,00 --> 00:00:16,30
Here we have an s, q, r, t function,

6
00:00:16,30 --> 00:00:19,00
which calculates the square root of a number.

7
00:00:19,00 --> 00:00:22,00
But, unlike the one in the math standard library

8
00:00:22,00 --> 00:00:26,70
this one will return error on negative numbers.

9
00:00:26,70 --> 00:00:29,80
As we can see, we return two values.

10
00:00:29,80 --> 00:00:33,30
One is float 64, which is the result,

11
00:00:33,30 --> 00:00:36,70
and the other one is of type error.

12
00:00:36,70 --> 00:00:40,80
Error is a built-in type that's used throughout Go.

13
00:00:40,80 --> 00:00:41,80
First we check.

14
00:00:41,80 --> 00:00:44,10
If n is smaller than zero,

15
00:00:44,10 --> 00:00:46,00
we return zero point zero because

16
00:00:46,00 --> 00:00:48,50
we have to return some value for the float,

17
00:00:48,50 --> 00:00:52,10
but also use the fmt dot error f function

18
00:00:52,10 --> 00:00:53,70
to create a new error.

19
00:00:53,70 --> 00:00:57,00
We'll reach line 13 if n is not negative,

20
00:00:57,00 --> 00:01:01,30
and then we return the square root of n and nil.

21
00:01:01,30 --> 00:01:04,90
Nil is the value Go uses to signal nothing.

22
00:01:04,90 --> 00:01:10,00
It is very much like null or none in other languages.

23
00:01:10,00 --> 00:01:11,90
Now, let's use our function.

24
00:01:11,90 --> 00:01:14,50
First with a positive number.

25
00:01:14,50 --> 00:01:17,90
So, s one and err equal the square root of

26
00:01:17,90 --> 00:01:20,20
two point zero, and then we check.

27
00:01:20,20 --> 00:01:23,10
In line 18, if the err is not nil,

28
00:01:23,10 --> 00:01:25,00
it means some error happened.

29
00:01:25,00 --> 00:01:26,80
We're going to print out the error

30
00:01:26,80 --> 00:01:30,60
otherwise, we're going to print out the result.

31
00:01:30,60 --> 00:01:32,40
You are going to write a lot

32
00:01:32,40 --> 00:01:35,10
of if err does not equal nil code.

33
00:01:35,10 --> 00:01:37,70
You might find this tedious, but I personally

34
00:01:37,70 --> 00:01:40,30
found it made my code much more robust,

35
00:01:40,30 --> 00:01:43,70
and forced me to think about every error.

36
00:01:43,70 --> 00:01:48,50
Now we try with the negative number in line 24.

37
00:01:48,50 --> 00:01:51,00
Because square root minus two point zero

38
00:01:51,00 --> 00:01:54,00
and then if the err is not nil, we print the error,

39
00:01:54,00 --> 00:01:57,50
otherwise, we print the result.

40
00:01:57,50 --> 00:01:59,90
Let's save it and run.

41
00:01:59,90 --> 00:02:01,90
And we see that in the first case,

42
00:02:01,90 --> 00:02:04,40
with the positive number, we printed the result.

43
00:02:04,40 --> 00:02:07,30
And with the negative number, we printed the error

44
00:02:07,30 --> 00:02:12,40
which was formatted nicely with the percent s verb.

45
00:02:12,40 --> 00:02:14,60
Go also has something called panic,

46
00:02:14,60 --> 00:02:18,50
which is somewhat similar to exceptions in other languages.

47
00:02:18,50 --> 00:02:21,10
The use of panic is discouraged in Go and

48
00:02:21,10 --> 00:02:24,00
is considered an anti-pattern.

49
00:02:24,00 --> 00:02:27,00
The way to signal an error is to return it.

