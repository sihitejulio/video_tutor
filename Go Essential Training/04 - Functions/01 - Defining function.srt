1
00:00:00,60 --> 00:00:02,50
- [Instructor] Functions are the building blocks

2
00:00:02,50 --> 00:00:04,70
of most programming languages.

3
00:00:04,70 --> 00:00:08,50
Functions give us modularity and reusability of code.

4
00:00:08,50 --> 00:00:11,60
You can code without functions, but it's painful.

5
00:00:11,60 --> 00:00:14,00
You already wrote many main functions.

6
00:00:14,00 --> 00:00:16,90
This is the function the Go runtime executes

7
00:00:16,90 --> 00:00:18,80
when the program starts.

8
00:00:18,80 --> 00:00:21,10
Let's define a simple function.

9
00:00:21,10 --> 00:00:24,70
In line nine, we use the "func" keyword

10
00:00:24,70 --> 00:00:26,70
to define a function.

11
00:00:26,70 --> 00:00:30,50
And then, we give it a name, the "add".

12
00:00:30,50 --> 00:00:32,90
After that, inside parentheses,

13
00:00:32,90 --> 00:00:35,50
we put the variables and their type.

14
00:00:35,50 --> 00:00:38,80
And last at the end, we specify the return type.

15
00:00:38,80 --> 00:00:42,30
Inside the function body, we use a return statement

16
00:00:42,30 --> 00:00:45,30
to return the value.

17
00:00:45,30 --> 00:00:49,30
To use our function, we can do what we do in line 19,

18
00:00:49,30 --> 00:00:51,10
when we assign to value the result

19
00:00:51,10 --> 00:00:53,70
of calling add one and two.

20
00:00:53,70 --> 00:00:56,90
And then in line 20, we're going to print value.

21
00:00:56,90 --> 00:00:58,80
Unlike many other languages,

22
00:00:58,80 --> 00:01:03,10
Go Functions can return more than one value.

23
00:01:03,10 --> 00:01:05,00
Let's define a divmod function,

24
00:01:05,00 --> 00:01:08,20
that return the quotient and a reminder.

25
00:01:08,20 --> 00:01:11,80
Again we use the "func" keyword to define a function.

26
00:01:11,80 --> 00:01:14,80
And we give it the name, "divmod".

27
00:01:14,80 --> 00:01:19,40
Inside the parentheses, we have two values, A and B.

28
00:01:19,40 --> 00:01:23,00
And finally, we specify the return types,

29
00:01:23,00 --> 00:01:25,00
this time again in parentheses,

30
00:01:25,00 --> 00:01:28,30
to say that we have more than one return value.

31
00:01:28,30 --> 00:01:31,50
And in the function body, we return the values,

32
00:01:31,50 --> 00:01:33,80
separated by a comma.

33
00:01:33,80 --> 00:01:37,80
To use the function, we do what we do in line 22.

34
00:01:37,80 --> 00:01:41,20
We assign to div N mod at the same time,

35
00:01:41,20 --> 00:01:43,20
the return value from divmod.

36
00:01:43,20 --> 00:01:48,20
And in line 23, we print out the results.

37
00:01:48,20 --> 00:01:50,60
Let's save it and run it.

38
00:01:50,60 --> 00:01:53,50
Go run func dot go.

39
00:01:53,50 --> 00:01:57,00
And we get three, and div equal three, and mod equal one.

