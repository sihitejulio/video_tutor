1
00:00:00,60 --> 00:00:03,00
- Go has a garbage collector.

2
00:00:03,00 --> 00:00:03,80
Which means you don't have to

3
00:00:03,80 --> 00:00:05,70
deal with memory management.

4
00:00:05,70 --> 00:00:07,10
When you allocate an object

5
00:00:07,10 --> 00:00:08,90
and then stop using it,

6
00:00:08,90 --> 00:00:10,20
Go's garbage collector

7
00:00:10,20 --> 00:00:11,90
will clear it up.

8
00:00:11,90 --> 00:00:15,60
However, memory is just one kind of resource.

9
00:00:15,60 --> 00:00:17,40
And you may have other resources

10
00:00:17,40 --> 00:00:18,80
you use in your program.

11
00:00:18,80 --> 00:00:19,60
For example,

12
00:00:19,60 --> 00:00:23,70
files, sockets, virtual machines and others.

13
00:00:23,70 --> 00:00:25,20
You'd like to make sure

14
00:00:25,20 --> 00:00:26,80
that these resources are closed

15
00:00:26,80 --> 00:00:29,20
when you're done with them as well.

16
00:00:29,20 --> 00:00:31,40
To make sure a resource is closed,

17
00:00:31,40 --> 00:00:32,50
use defer.

18
00:00:32,50 --> 00:00:35,10
Let's see an example.

19
00:00:35,10 --> 00:00:38,20
Cleanup is a function that will free your resource.

20
00:00:38,20 --> 00:00:40,30
You can pass any (mumbles).

21
00:00:40,30 --> 00:00:41,20
For example,

22
00:00:41,20 --> 00:00:43,80
the resource to free.

23
00:00:43,80 --> 00:00:45,80
Worker is the actual code.

24
00:00:45,80 --> 00:00:47,90
Assume you acquired the resource "A",

25
00:00:47,90 --> 00:00:49,20
and you'd like to free it,

26
00:00:49,20 --> 00:00:51,00
at the end of worker.

27
00:00:51,00 --> 00:00:52,50
We use a defer code

28
00:00:52,50 --> 00:00:54,20
in line 12,

29
00:00:54,20 --> 00:00:56,20
to free this resource.

30
00:00:56,20 --> 00:00:57,00
We do defer,

31
00:00:57,00 --> 00:00:59,60
(typing)

32
00:00:59,60 --> 00:01:01,00
and then the function code

33
00:01:01,00 --> 00:01:04,10
that we'd like to happen when a function exits.

34
00:01:04,10 --> 00:01:05,70
The fmt.Println land line

35
00:01:05,70 --> 00:01:08,30
stands for code using these resources.

36
00:01:08,30 --> 00:01:10,00
Now, let's run it.

37
00:01:10,00 --> 00:01:13,90
Go, run, defer, then go.

38
00:01:13,90 --> 00:01:15,60
You see first the worker code

39
00:01:15,60 --> 00:01:17,80
and the the defer's code.

40
00:01:17,80 --> 00:01:20,30
Defer will be code even if you have an error

41
00:01:20,30 --> 00:01:22,30
or a panic in your code.

42
00:01:22,30 --> 00:01:24,50
Making sure that your resources will be freed,

43
00:01:24,50 --> 00:01:26,50
in any case.

44
00:01:26,50 --> 00:01:27,80
What's nice about defer

45
00:01:27,80 --> 00:01:28,70
is that you write it

46
00:01:28,70 --> 00:01:31,80
just after you acquire the resource.

47
00:01:31,80 --> 00:01:32,60
This way,

48
00:01:32,60 --> 00:01:35,40
you don't forget to make sure it's freed.

49
00:01:35,40 --> 00:01:37,80
Defer the code in reverse order.

50
00:01:37,80 --> 00:01:40,70
Let's add another defer and see.

51
00:01:40,70 --> 00:01:43,70
So, we'll also free the resource B.

52
00:01:43,70 --> 00:01:46,80
We'll save and run.

53
00:01:46,80 --> 00:01:49,00
(typing)

54
00:01:49,00 --> 00:01:50,70
And we see that the first we got

55
00:01:50,70 --> 00:01:51,70
the cleaning of B,

56
00:01:51,70 --> 00:01:53,30
and then cleaning of A,

57
00:01:53,30 --> 00:01:54,20
and of course,

58
00:01:54,20 --> 00:01:57,00
the worker code is happening before any defer.

