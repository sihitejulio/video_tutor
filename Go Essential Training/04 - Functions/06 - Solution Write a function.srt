1
00:00:00,00 --> 00:00:02,10
- [Instructor] So here is our function.

2
00:00:02,10 --> 00:00:06,90
contentType gets a url and returns a string and an error.

3
00:00:06,90 --> 00:00:10,90
At the beginning, we make a http.Get request,

4
00:00:10,90 --> 00:00:14,80
and the result is a response and an error.

5
00:00:14,80 --> 00:00:18,00
And as usual in Go, we always check for the error.

6
00:00:18,00 --> 00:00:21,70
If it's not new, this means that some error has happened.

7
00:00:21,70 --> 00:00:25,30
We return the empty string and the error that happened.

8
00:00:25,30 --> 00:00:28,70
In line 17, we use defer to make sure

9
00:00:28,70 --> 00:00:32,80
that the response body is closed.

10
00:00:32,80 --> 00:00:36,50
In line 19, we get the response Content-Type header.

11
00:00:36,50 --> 00:00:40,60
In Line 20 we check, if it is the empty string

12
00:00:40,60 --> 00:00:45,30
we return an error that we couldn't find the Content-Type,

13
00:00:45,30 --> 00:00:47,40
otherwise we return the content type,

14
00:00:47,40 --> 00:00:51,20
and nil signifying there was no error.

15
00:00:51,20 --> 00:00:54,80
In the main function, we call contentType

16
00:00:54,80 --> 00:00:57,70
and assign it to ctype and error.

17
00:00:57,70 --> 00:01:00,60
We check, if there was an error, will print an error,

18
00:01:00,60 --> 00:01:04,30
otherwise will print the content type.

19
00:01:04,30 --> 00:01:05,90
Let's run it.

20
00:01:05,90 --> 00:01:09,20
Go run ctype.go.

21
00:01:09,20 --> 00:01:14,00
And we got text/html in utf-8 in code.

