1
00:00:00,60 --> 00:00:02,50
- [Instructor] Write a function that gets a URL

2
00:00:02,50 --> 00:00:07,00
and returns the value of Content-Type response HTTP header.

3
00:00:07,00 --> 00:00:08,70
The function should return an error

4
00:00:08,70 --> 00:00:12,10
if it can't perform a GET request.

5
00:00:12,10 --> 00:00:14,80
The signature of the function is contentType,

6
00:00:14,80 --> 00:00:16,40
it gets a URL as a string,

7
00:00:16,40 --> 00:00:19,90
and returns a string and an error.

8
00:00:19,90 --> 00:00:25,20
Use net HTP package GET function to make an HTP call.

9
00:00:25,20 --> 00:00:31,20
Use the resp.Header.Get method to get the value of a header.

10
00:00:31,20 --> 00:00:34,00
And make sure the response body is closed properly.

