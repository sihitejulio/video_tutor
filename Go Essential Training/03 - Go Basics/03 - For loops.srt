1
00:00:00,60 --> 00:00:02,00
- [Instructor] When you'd like to iterate,

2
00:00:02,00 --> 00:00:05,70
you have one option in Go, the for loop.

3
00:00:05,70 --> 00:00:08,70
The first version of the for loop will look very familiar

4
00:00:08,70 --> 00:00:12,60
to anyone who comes from C++ or Java background.

5
00:00:12,60 --> 00:00:16,70
We create a loop variable, i, and assign an initial value.

6
00:00:16,70 --> 00:00:19,40
Then you specify the running condition,

7
00:00:19,40 --> 00:00:22,20
as long as i is smaller than three,

8
00:00:22,20 --> 00:00:25,10
and last, you specify how to change i

9
00:00:25,10 --> 00:00:27,00
at the end of every loop.

10
00:00:27,00 --> 00:00:28,70
Let's run it.

11
00:00:28,70 --> 00:00:31,90
go run for.go

12
00:00:31,90 --> 00:00:35,10
and we zero, one, and two are printed out.

13
00:00:35,10 --> 00:00:36,80
I'm going to paste now another example

14
00:00:36,80 --> 00:00:39,00
from the exercise files.

15
00:00:39,00 --> 00:00:42,00
You can use break to exit the loop early.

16
00:00:42,00 --> 00:00:45,40
So in line 15, I'm checking if i is bigger than one,

17
00:00:45,40 --> 00:00:47,00
I am breaking.

18
00:00:47,00 --> 00:00:50,00
This means that I'm going to print zero and one

19
00:00:50,00 --> 00:00:51,80
but not two.

20
00:00:51,80 --> 00:00:56,50
Let's save it and run again.

21
00:00:56,50 --> 00:01:00,60
So I'm pasting another example from the exercise file.

22
00:01:00,60 --> 00:01:03,90
You can use continue to move to the next iteration

23
00:01:03,90 --> 00:01:06,50
without executing the code below.

24
00:01:06,50 --> 00:01:09,40
In this case, in line 23, we say that

25
00:01:09,40 --> 00:01:13,30
if x is smaller than one, we will continue.

26
00:01:13,30 --> 00:01:16,90
So we won't print the zero value.

27
00:01:16,90 --> 00:01:20,60
Let's save it and run.

28
00:01:20,60 --> 00:01:23,60
We see one and two are printed out.

29
00:01:23,60 --> 00:01:27,00
I've pasted another example from the exercise file.

30
00:01:27,00 --> 00:01:31,00
The second format of for is just a condition,

31
00:01:31,00 --> 00:01:34,80
and this is very much like a while loop in other languages.

32
00:01:34,80 --> 00:01:39,60
So a is equal to zero, and for a smaller than three,

33
00:01:39,60 --> 00:01:42,20
meaning as long as a is smaller than three,

34
00:01:42,20 --> 00:01:43,30
we print out.

35
00:01:43,30 --> 00:01:47,80
And we need to manually increment a in line 33.

36
00:01:47,80 --> 00:01:50,80
Let's save this and run.

37
00:01:50,80 --> 00:01:54,70
And we see zero, one, and two, as before, printed out.

38
00:01:54,70 --> 00:01:58,90
I've pasted another example from the exercise files.

39
00:01:58,90 --> 00:02:00,30
In the last format,

40
00:02:00,30 --> 00:02:03,00
we have a for loop without any condition.

41
00:02:03,00 --> 00:02:07,40
This is like a while true loop in other languages.

42
00:02:07,40 --> 00:02:09,50
You need to increment the value and also

43
00:02:09,50 --> 00:02:12,20
check the exit condition inside the loop.

44
00:02:12,20 --> 00:02:14,00
Let's save it and run.

