1
00:00:00,40 --> 00:00:03,20
- [Instructor] A slice is a sequence of items.

2
00:00:03,20 --> 00:00:06,50
All items in a slice must be of the same type.

3
00:00:06,50 --> 00:00:10,40
In line 10, we define a slice of strings.

4
00:00:10,40 --> 00:00:13,50
We specify the type by the square brackets,

5
00:00:13,50 --> 00:00:15,70
and then the type.

6
00:00:15,70 --> 00:00:17,60
This is a slice of strings.

7
00:00:17,60 --> 00:00:22,00
And then we can fill the slice with the curly brackets,

8
00:00:22,00 --> 00:00:24,80
and then a list of items that are there.

9
00:00:24,80 --> 00:00:28,40
So we have a slice of strings with bugs, daffy, and taz.

10
00:00:28,40 --> 00:00:30,00
And we can print it out.

11
00:00:30,00 --> 00:00:35,00
The %v verb will print out the slice as well.

12
00:00:35,00 --> 00:00:36,60
To find the length of a slice,

13
00:00:36,60 --> 00:00:39,60
use the len built in function.

14
00:00:39,60 --> 00:00:42,90
You can access sliced elements with the square brackets.

15
00:00:42,90 --> 00:00:44,70
Indexing starts at zero,

16
00:00:44,70 --> 00:00:50,10
so printing loons[1] will print daffy, and not bugs.

17
00:00:50,10 --> 00:00:55,00
And we can use slice notation on the slices.

18
00:00:55,00 --> 00:00:57,20
So in line 22, we are going to print

19
00:00:57,20 --> 00:01:02,10
everything from index one, so we'll print daffy and taz.

20
00:01:02,10 --> 00:01:07,10
Let's save it and run it.

21
00:01:07,10 --> 00:01:09,40
And we see that we got the loons,

22
00:01:09,40 --> 00:01:11,20
and then three is the length,

23
00:01:11,20 --> 00:01:14,20
daffy is the second one, in index one,

24
00:01:14,20 --> 00:01:16,70
and we printed with the slice

25
00:01:16,70 --> 00:01:19,70
only the last part of the slice.

26
00:01:19,70 --> 00:01:22,40
I've pasted some code from the exercise files.

27
00:01:22,40 --> 00:01:24,60
To iterate over the elements in the slice,

28
00:01:24,60 --> 00:01:26,60
you can use the conditional for loop,

29
00:01:26,60 --> 00:01:30,40
as can be seen in line 26.

30
00:01:30,40 --> 00:01:33,80
We start with zero, up to the length of the loons,

31
00:01:33,80 --> 00:01:37,70
and then i++, print the loon.

32
00:01:37,70 --> 00:01:41,20
However, in Go, we also have a range keyword,

33
00:01:41,20 --> 00:01:43,50
as we can see in line 32.

34
00:01:43,50 --> 00:01:47,80
If we do for i equal range of loons and print i,

35
00:01:47,80 --> 00:01:51,00
we will print the indexes of the slice,

36
00:01:51,00 --> 00:01:55,30
so we'll print zero, one, and two.

37
00:01:55,30 --> 00:01:59,00
If you want both the index and the value, we can use range

38
00:01:59,00 --> 00:02:02,00
with a double value context on the left side.

39
00:02:02,00 --> 00:02:06,00
So here in line 38 I have both i and name,

40
00:02:06,00 --> 00:02:09,90
and then I can print that the name is at a certain position.

41
00:02:09,90 --> 00:02:14,00
If you want just the values, we have to use the underscore,

42
00:02:14,00 --> 00:02:15,30
as in line 44,

43
00:02:15,30 --> 00:02:18,30
because unused variables in Go

44
00:02:18,30 --> 00:02:20,50
are a compilation error.

45
00:02:20,50 --> 00:02:25,80
Let's save it and run.

46
00:02:25,80 --> 00:02:28,10
Right, and we see zero, one, two

47
00:02:28,10 --> 00:02:29,50
when we did the first range,

48
00:02:29,50 --> 00:02:32,10
and then bugs at zero, daffy at zero.

49
00:02:32,10 --> 00:02:35,70
And then at the end we print just the values.

50
00:02:35,70 --> 00:02:37,40
If you want to extend the slice,

51
00:02:37,40 --> 00:02:40,90
we can use the built in append function.

52
00:02:40,90 --> 00:02:44,90
Append will add another element to the end of the slice.

53
00:02:44,90 --> 00:02:47,00
And if you're going to save this code.

54
00:02:47,00 --> 00:02:48,70
And if you run the code again,

55
00:02:48,70 --> 00:02:51,20
we can see that the slice contains bugs, daffy,

56
00:02:51,20 --> 00:02:54,60
taz, and also elmer from the append.

57
00:02:54,60 --> 00:02:57,40
There is another type in Go called array,

58
00:02:57,40 --> 00:03:00,10
and slices are built on top of arrays.

59
00:03:00,10 --> 00:03:03,30
However, in practice, you'll seldom use arrays,

60
00:03:03,30 --> 00:03:05,00
so we'll skip them here.

