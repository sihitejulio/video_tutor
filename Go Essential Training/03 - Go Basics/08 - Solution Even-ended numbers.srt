1
00:00:00,20 --> 00:00:01,80
- [Instructor] Welcome back.

2
00:00:01,80 --> 00:00:03,40
Let's have a look at the solution.

3
00:00:03,40 --> 00:00:05,80
We start by creating a counter,

4
00:00:05,80 --> 00:00:09,50
initializing it with the value zero.

5
00:00:09,50 --> 00:00:11,90
And then we need to go over all the pairs

6
00:00:11,90 --> 00:00:13,30
of four-digit numbers.

7
00:00:13,30 --> 00:00:18,30
We start with the variable a, from 1000 to 9999.

8
00:00:18,30 --> 00:00:21,50
And then in line 19, we start with the value b,

9
00:00:21,50 --> 00:00:24,50
from a to 9999.

10
00:00:24,50 --> 00:00:26,90
This will eliminate duplicates,

11
00:00:26,90 --> 00:00:29,90
because if you multiply 1000 by 2000,

12
00:00:29,90 --> 00:00:34,20
or multiply 2000 by 1000, it's the same for us.

13
00:00:34,20 --> 00:00:40,50
Then in line 20, n is the result of multiplying a with b.

14
00:00:40,50 --> 00:00:42,90
And then to check if the number is even ended,

15
00:00:42,90 --> 00:00:47,60
we first in line 23, convert it to a string,

16
00:00:47,60 --> 00:00:49,90
with the Sprint function.

17
00:00:49,90 --> 00:00:54,60
And then in line 24, we check if the first digit,

18
00:00:54,60 --> 00:00:58,70
which is in index zero equals to the last digit

19
00:00:58,70 --> 00:01:01,30
which is in the index of the length minus one

20
00:01:01,30 --> 00:01:04,40
will increment the counter.

21
00:01:04,40 --> 00:01:08,40
And last, at the end, we print out the count.

22
00:01:08,40 --> 00:01:11,30
Let's save the file and see.

23
00:01:11,30 --> 00:01:15,40
Go run even_ended.go.

24
00:01:15,40 --> 00:01:18,00
So a little bit more than three million.

