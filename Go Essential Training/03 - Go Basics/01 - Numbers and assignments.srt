1
00:00:00,60 --> 00:00:01,70
- [Instructor] Let's write a program

2
00:00:01,70 --> 00:00:05,30
that calculates the mean, or average, of two numbers.

3
00:00:05,30 --> 00:00:08,60
We declare two variables, x and y, of type int.

4
00:00:08,60 --> 00:00:13,00
And like C or Java, the type comes after the variable name.

5
00:00:13,00 --> 00:00:17,70
In Go, you have int8, int16, int32, int64,

6
00:00:17,70 --> 00:00:20,70
and the unsigned versions of all of them.

7
00:00:20,70 --> 00:00:22,90
The int type, without size,

8
00:00:22,90 --> 00:00:24,90
depends on the system you are using,

9
00:00:24,90 --> 00:00:27,40
and is the one you will usually use.

10
00:00:27,40 --> 00:00:29,80
If you don't assign a value to a variable,

11
00:00:29,80 --> 00:00:32,90
Go will assign the zero value for this type.

12
00:00:32,90 --> 00:00:36,10
In this case, it will be zero.

13
00:00:36,10 --> 00:00:43,00
In lines 12 and 13, we assign values to x and y.

14
00:00:43,00 --> 00:00:47,30
In line 15 and 16, we use fmt.Printf.

15
00:00:47,30 --> 00:00:50,50
The Printf function gets a template to print

16
00:00:50,50 --> 00:00:53,30
and then a value to fill this template.

17
00:00:53,30 --> 00:00:56,80
The %v verb will print a Go object

18
00:00:56,80 --> 00:01:01,00
and the %t verb will print its type.

19
00:01:01,00 --> 00:01:04,90
In line 18, we define mean and in line 19,

20
00:01:04,90 --> 00:01:09,00
we assign the value of x plus y divided by two.

21
00:01:09,00 --> 00:01:12,60
And in line 20, again we print the value of mean

22
00:01:12,60 --> 00:01:14,50
and its type.

23
00:01:14,50 --> 00:01:15,40
So let's run it.

24
00:01:15,40 --> 00:01:18,90
go run mean.go.

25
00:01:18,90 --> 00:01:21,20
And we see x is one with type of integer,

26
00:01:21,20 --> 00:01:23,60
y is two with type of integer,

27
00:01:23,60 --> 00:01:26,80
and the result is one, type of integer.

28
00:01:26,80 --> 00:01:30,60
The result is one, not 1.5 as expected.

29
00:01:30,60 --> 00:01:34,20
The reason is that integer division returns an integer.

30
00:01:34,20 --> 00:01:37,20
Let's correct this by changing one of the variables

31
00:01:37,20 --> 00:01:38,70
to float.

32
00:01:38,70 --> 00:01:41,90
So I'll go to line nine and change the variable x

33
00:01:41,90 --> 00:01:44,90
to float64.

34
00:01:44,90 --> 00:01:49,40
There are two kinds of floats in Go, float64 and float32.

35
00:01:49,40 --> 00:01:54,40
Save the file and now, we will run it again.

36
00:01:54,40 --> 00:01:57,80
go run mean.go.

37
00:01:57,80 --> 00:01:59,70
And we get an error.

38
00:01:59,70 --> 00:02:02,20
Go's type system is very strict.

39
00:02:02,20 --> 00:02:05,30
It doesn't allow to add integer to a float.

40
00:02:05,30 --> 00:02:11,10
We will fix this by converting everything to floats.

41
00:02:11,10 --> 00:02:14,70
So we'll make y float64 as well,

42
00:02:14,70 --> 00:02:19,00
and int float64 as well,

43
00:02:19,00 --> 00:02:23,40
and divide by 2.0 which is float.

44
00:02:23,40 --> 00:02:28,60
Save the file and then, let's run it again.

45
00:02:28,60 --> 00:02:31,80
And this time, everything works as expected.

46
00:02:31,80 --> 00:02:34,90
What's nice about Go is that the compiler can infer

47
00:02:34,90 --> 00:02:37,10
the type of variables for you.

48
00:02:37,10 --> 00:02:39,50
It has a syntax for creating a variable

49
00:02:39,50 --> 00:02:41,80
and assigning to it in one line

50
00:02:41,80 --> 00:02:45,70
using the colon equal operator.

51
00:02:45,70 --> 00:02:48,90
You can get rid of lines nine and 10

52
00:02:48,90 --> 00:02:52,80
and then say that x equal 1.0

53
00:02:52,80 --> 00:02:55,70
and y equal 2.0.

54
00:02:55,70 --> 00:02:58,00
And then the same way, we can get rid of mean

55
00:02:58,00 --> 00:03:04,40
and say that equals, colon equals, x plus y divided by two.

56
00:03:04,40 --> 00:03:06,60
Let's save the file,

57
00:03:06,60 --> 00:03:09,70
and run it again.

58
00:03:09,70 --> 00:03:11,30
And we see the same output.

59
00:03:11,30 --> 00:03:15,20
You can also assign two variables in the same line.

60
00:03:15,20 --> 00:03:17,50
So we can go to our code.

61
00:03:17,50 --> 00:03:23,30
We can say x, comma y, colon equal, 1.0 comma 2.0.

62
00:03:23,30 --> 00:03:26,40
Let's save it and run it again.

63
00:03:26,40 --> 00:03:29,20
And it works as expected.

64
00:03:29,20 --> 00:03:31,50
If you declare a variable but don't use it,

65
00:03:31,50 --> 00:03:33,50
Go will treat this as an error.

66
00:03:33,50 --> 00:03:36,00
This might be annoying, but it will protect you

67
00:03:36,00 --> 00:03:38,00
from a lot of bugs.

