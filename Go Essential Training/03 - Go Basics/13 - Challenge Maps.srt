1
00:00:00,10 --> 00:00:03,70
(whooshes)

2
00:00:03,70 --> 00:00:05,10
- [Instructor] Let's count how many times

3
00:00:05,10 --> 00:00:07,50
each word appears in a text.

4
00:00:07,50 --> 00:00:08,90
This is a very basic step

5
00:00:08,90 --> 00:00:11,60
in many text-processing algorithms.

6
00:00:11,60 --> 00:00:13,40
To split the text to words,

7
00:00:13,40 --> 00:00:16,40
use the fields function from strings package.

8
00:00:16,40 --> 00:00:19,40
Also use ToLower from the same package

9
00:00:19,40 --> 00:00:22,00
to convert all the words to lowercase.

