1
00:00:00,60 --> 00:00:03,20
- [Instructor] You define a string with double quote.

2
00:00:03,20 --> 00:00:06,90
Here we assign the the colour of magic to the variable book

3
00:00:06,90 --> 00:00:10,00
and in line 10 we print it out.

4
00:00:10,00 --> 00:00:12,00
To know how many bytes are in a string

5
00:00:12,00 --> 00:00:13,90
use the len built in function.

6
00:00:13,90 --> 00:00:18,00
You can access individual bytes with square brackets.

7
00:00:18,00 --> 00:00:20,10
Indexing starts at zero.

8
00:00:20,10 --> 00:00:23,20
Let's run this.

9
00:00:23,20 --> 00:00:25,90
And we see that we get the name of the book printed out,

10
00:00:25,90 --> 00:00:29,40
we get that it says 19 bytes.

11
00:00:29,40 --> 00:00:34,60
And the first one is 84 type uint8.

12
00:00:34,60 --> 00:00:39,70
Uint8 in go is a byte.

13
00:00:39,70 --> 00:00:41,50
Let's see what happens if I try

14
00:00:41,50 --> 00:00:44,50
to assign something to another character.

15
00:00:44,50 --> 00:00:48,90
So I want to change the first character to be 116.

16
00:00:48,90 --> 00:00:52,70
When I try to run the code now I will get an error.

17
00:00:52,70 --> 00:00:55,90
I can not assign to book zero.

18
00:00:55,90 --> 00:00:58,10
And you need to remember that

19
00:00:58,10 --> 00:01:06,20
and I'm going to write it in a comment

20
00:01:06,20 --> 00:01:11,90
that strings in go are immutable.

21
00:01:11,90 --> 00:01:17,10
Meaning you can not change them once you've created them.

22
00:01:17,10 --> 00:01:20,80
To access parts of a string you can use Slicing.

23
00:01:20,80 --> 00:01:24,40
In line 22 we see the full format of a Slice.

24
00:01:24,40 --> 00:01:27,70
We specify a start and an end index.

25
00:01:27,70 --> 00:01:30,80
Slices are half empty.

26
00:01:30,80 --> 00:01:33,00
Meaning we'll get index number four

27
00:01:33,00 --> 00:01:36,10
but we won't get number 11.

28
00:01:36,10 --> 00:01:39,00
You can also, as you see in line 25,

29
00:01:39,00 --> 00:01:41,20
use Slices without the end.

30
00:01:41,20 --> 00:01:42,70
Then you will get everything in the string

31
00:01:42,70 --> 00:01:46,30
from character number four up to the end.

32
00:01:46,30 --> 00:01:48,80
And in line 28 you can see the other option

33
00:01:48,80 --> 00:01:52,30
which is to omit the start and just specify the end

34
00:01:52,30 --> 00:01:54,80
you'll get everything after four

35
00:01:54,80 --> 00:01:57,70
and not including the fourth character.

36
00:01:57,70 --> 00:02:02,70
Let's save and run this one.

37
00:02:02,70 --> 00:02:06,60
Here are some other things you can do with strings.

38
00:02:06,60 --> 00:02:09,80
You can use the plus sign to concatenate two strings.

39
00:02:09,80 --> 00:02:14,10
So I can do t and then book from one

40
00:02:14,10 --> 00:02:18,20
getting The colour of magic with a lower case t.

41
00:02:18,20 --> 00:02:21,70
In line 33 we see that strings are Unicode.

42
00:02:21,70 --> 00:02:23,80
I can enter Unicode characters

43
00:02:23,80 --> 00:02:27,90
like the half sign inside the string.

44
00:02:27,90 --> 00:02:29,30
And in line 36 we could see

45
00:02:29,30 --> 00:02:36,40
how to construct multiple line strings with a backtick sign.

46
00:02:36,40 --> 00:02:39,70
Let's save this one and run.

47
00:02:39,70 --> 00:02:42,80
And we see that we get The colour of magic

48
00:02:42,80 --> 00:02:45,00
with the lowercase t.

49
00:02:45,00 --> 00:02:47,20
We see that is was half price

50
00:02:47,20 --> 00:02:49,30
and the half sign is printed out.

51
00:02:49,30 --> 00:02:53,00
And we see our poem printed out as well.

