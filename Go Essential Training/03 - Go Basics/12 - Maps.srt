1
00:00:00,60 --> 00:00:02,20
- [Instructor] A map is a data structure

2
00:00:02,20 --> 00:00:05,00
where keys points the values.

3
00:00:05,00 --> 00:00:07,80
In go, the keys must be of the same type

4
00:00:07,80 --> 00:00:10,90
and the values must be of the same type.

5
00:00:10,90 --> 00:00:13,10
In line nine, we define a map.

6
00:00:13,10 --> 00:00:17,60
This map is from strings, as we can see,

7
00:00:17,60 --> 00:00:22,20
inside the square brackets to floats.

8
00:00:22,20 --> 00:00:26,10
And we can se the initial values inside curly braces.

9
00:00:26,10 --> 00:00:29,90
We write the key, AMZN then column

10
00:00:29,90 --> 00:00:33,50
and then the value of that stock price.

11
00:00:33,50 --> 00:00:37,10
Note that we must have a trailing comma on the last line.

12
00:00:37,10 --> 00:00:39,00
By the way, these are all stock prices.

13
00:00:39,00 --> 00:00:43,40
Prices probably have changed since I wrote this program.

14
00:00:43,40 --> 00:00:46,40
To find out how many items are there in a map

15
00:00:46,40 --> 00:00:49,20
you can use the len built in keyword.

16
00:00:49,20 --> 00:00:52,90
This is going to print out three and not six.

17
00:00:52,90 --> 00:00:55,60
In maps we count only how many keys there are.

18
00:00:55,60 --> 00:01:00,10
To get the specific values we use the square brackets.

19
00:01:00,10 --> 00:01:04,00
So when we do print the line of stocks in square brackets,

20
00:01:04,00 --> 00:01:09,10
MSFT, you're going to print the stock price for Microsoft.

21
00:01:09,10 --> 00:01:12,10
If you are going to access a non-existing key

22
00:01:12,10 --> 00:01:15,40
go will give us the zero value.

23
00:01:15,40 --> 00:01:18,90
So if you are going to print stocks of TSLA

24
00:01:18,90 --> 00:01:21,20
we are going to get zero.

25
00:01:21,20 --> 00:01:23,60
This can come handy in some cases.

26
00:01:23,60 --> 00:01:25,90
But in some cases we would like to know

27
00:01:25,90 --> 00:01:30,10
if something is inside the map or not.

28
00:01:30,10 --> 00:01:33,90
To do that we can do what we do in line 25.

29
00:01:33,90 --> 00:01:36,80
We use double value context when we get the value.

30
00:01:36,80 --> 00:01:40,90
Value itself will get the value of stocks

31
00:01:40,90 --> 00:01:45,20
of TSLA if it exists and zero value if not.

32
00:01:45,20 --> 00:01:49,50
And the okay variable will either get true or false

33
00:01:49,50 --> 00:01:54,30
depending if the key is inside a map or not.

34
00:01:54,30 --> 00:01:56,10
To set the value we use the square brackets

35
00:01:56,10 --> 00:01:58,40
and then assign a value to it.

36
00:01:58,40 --> 00:02:03,50
So we assign to TSLA the value of 322.12.

37
00:02:03,50 --> 00:02:08,50
To delete the value we use the built in delete function.

38
00:02:08,50 --> 00:02:10,90
If you want to iterate over a map

39
00:02:10,90 --> 00:02:13,10
we can use a single value for

40
00:02:13,10 --> 00:02:15,90
and this will bring us only the keys.

41
00:02:15,90 --> 00:02:19,00
So in our case we'll print only the keys of the map.

42
00:02:19,00 --> 00:02:21,40
And if you want both the keys and the values

43
00:02:21,40 --> 00:02:25,00
we can use a double value context for the range.

