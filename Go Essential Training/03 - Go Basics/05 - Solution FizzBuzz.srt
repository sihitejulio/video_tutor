1
00:00:00,70 --> 00:00:03,20
- [Instructor] I usually start by solving the problem

2
00:00:03,20 --> 00:00:05,40
by writing the solution in English

3
00:00:05,40 --> 00:00:08,10
and then rewriting it in Go.

4
00:00:08,10 --> 00:00:10,30
This helps me focus on the solution

5
00:00:10,30 --> 00:00:13,30
and not struggle with the syntax while thinking.

6
00:00:13,30 --> 00:00:15,10
While writing the English solution,

7
00:00:15,10 --> 00:00:17,30
I also found out that I need to start

8
00:00:17,30 --> 00:00:20,70
with a case of dividing by three and five

9
00:00:20,70 --> 00:00:23,50
before the other cases.

10
00:00:23,50 --> 00:00:25,20
So what we do?

11
00:00:25,20 --> 00:00:31,40
In line 17, we do for i from three to 20, remove i.

12
00:00:31,40 --> 00:00:33,90
In the first case, at line 18, we check if i

13
00:00:33,90 --> 00:00:36,60
is divisible by three and by five

14
00:00:36,60 --> 00:00:39,00
by using the model operator.

15
00:00:39,00 --> 00:00:42,50
And if it does, we print fizz buzz.

16
00:00:42,50 --> 00:00:46,00
Otherwise, we check if i is divisible by three,

17
00:00:46,00 --> 00:00:49,50
and if it does, we print fizz.

18
00:00:49,50 --> 00:00:52,70
Otherwise, we check if i is divisible by five,

19
00:00:52,70 --> 00:00:55,90
and if it does, we print buzz.

20
00:00:55,90 --> 00:00:59,20
Otherwise, we just print the number.

21
00:00:59,20 --> 00:01:02,00
Let's see it running.

22
00:01:02,00 --> 00:01:06,00
Go run fizzbuzz.go.

23
00:01:06,00 --> 00:01:08,60
And one, two, instead of three, I see fizz,

24
00:01:08,60 --> 00:01:12,00
instead of five, I see buzz, et cetera, et cetera.

