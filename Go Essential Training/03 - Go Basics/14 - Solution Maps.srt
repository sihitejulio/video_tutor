1
00:00:00,10 --> 00:00:01,40
- [Instructor] Welcome back.

2
00:00:01,40 --> 00:00:03,80
In line 16 we split the text to words

3
00:00:03,80 --> 00:00:06,30
using the strings.Fields function.

4
00:00:06,30 --> 00:00:09,80
It will ignore white space and leave us just with the words.

5
00:00:09,80 --> 00:00:13,80
Then in line 17, we create an empty map,

6
00:00:13,80 --> 00:00:17,20
but the keys are strings, which will be the words,

7
00:00:17,20 --> 00:00:20,30
and the values are integers, which will be the count

8
00:00:20,30 --> 00:00:24,20
of how many times each word was seen.

9
00:00:24,20 --> 00:00:27,30
Next in line 18, we go over the words.

10
00:00:27,30 --> 00:00:29,70
We're not interested in the indexes,

11
00:00:29,70 --> 00:00:32,80
so we use the underscore to ignore them.

12
00:00:32,80 --> 00:00:35,70
And then in line 19, we increment the counter

13
00:00:35,70 --> 00:00:40,00
for the specific word by first converting it to lower case,

14
00:00:40,00 --> 00:00:42,40
and then using the plus/plus.

15
00:00:42,40 --> 00:00:46,70
Note that since accessing a map on a non-existing key

16
00:00:46,70 --> 00:00:50,40
will give us zero, this will work exactly as expected.

17
00:00:50,40 --> 00:00:52,20
And at the end, we print out the map

18
00:00:52,20 --> 00:00:55,10
to see how many times each word appears in the text.

19
00:00:55,10 --> 00:00:58,40
Let's save it and run it.

20
00:00:58,40 --> 00:01:02,00
And we see how many times every word appears in the text.

