1
00:00:00,60 --> 00:00:01,80
- [Instructor] Welcome back.

2
00:00:01,80 --> 00:00:04,80
First you need a value for the the first maximal values.

3
00:00:04,80 --> 00:00:06,00
You can't use zero,

4
00:00:06,00 --> 00:00:09,60
since values in the slice might be negative.

5
00:00:09,60 --> 00:00:12,30
The solution is to take the first value,

6
00:00:12,30 --> 00:00:15,00
which is in index zero.

7
00:00:15,00 --> 00:00:18,90
Next, in line 13, we iterate over the rest of the slice.

8
00:00:18,90 --> 00:00:22,50
We use a slice notation to skip the first value,

9
00:00:22,50 --> 00:00:24,40
and we use the double context

10
00:00:24,40 --> 00:00:27,10
to get the values, and not the indexes.

11
00:00:27,10 --> 00:00:28,60
Then in line 14 we check

12
00:00:28,60 --> 00:00:31,50
if the value that we found in the slice currently,

13
00:00:31,50 --> 00:00:34,10
is bigger then the current maximal value,

14
00:00:34,10 --> 00:00:37,50
and if so, we update the current maximal value.

15
00:00:37,50 --> 00:00:41,40
And at the end, we print out the maximal value.

16
00:00:41,40 --> 00:00:44,10
Let's save this, and run,

17
00:00:44,10 --> 00:00:47,50
go run max.go.

18
00:00:47,50 --> 00:00:50,00
And the maximal value is 42.

