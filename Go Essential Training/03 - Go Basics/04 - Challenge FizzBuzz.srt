1
00:00:00,60 --> 00:00:03,00
- [Instructor] Let's try a classic programming question.

2
00:00:03,00 --> 00:00:05,30
It's called fizz buzz.

3
00:00:05,30 --> 00:00:08,90
What you need to do is print the numbers between one and 20.

4
00:00:08,90 --> 00:00:13,10
However, if the number is divisible by three, say nine,

5
00:00:13,10 --> 00:00:15,90
you should print the word fizz instead of the number.

6
00:00:15,90 --> 00:00:18,90
If the number is divisible by five, say 10,

7
00:00:18,90 --> 00:00:21,50
you should print the word buzz instead of the number.

8
00:00:21,50 --> 00:00:25,40
And if the number is divisible by both three and five,

9
00:00:25,40 --> 00:00:30,00
say 15, it should print fizz buzz instead of the number.

10
00:00:30,00 --> 00:00:33,00
Here is an example of the beginning of the output.

11
00:00:33,00 --> 00:00:35,50
We print the number one, we print the number two,

12
00:00:35,50 --> 00:00:38,10
and then because three is divisible by three,

13
00:00:38,10 --> 00:00:39,70
we print out fizz.

14
00:00:39,70 --> 00:00:42,50
Then we print four, and instead of five,

15
00:00:42,50 --> 00:00:45,90
we print buzz, instead of six, which is divisible by three,

16
00:00:45,90 --> 00:00:48,00
we print fizz, etc., etc.

