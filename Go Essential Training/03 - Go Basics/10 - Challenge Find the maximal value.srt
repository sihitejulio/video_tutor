1
00:00:03,60 --> 00:00:05,10
- [Narrator] Let's use what you've learned

2
00:00:05,10 --> 00:00:08,40
to collect the maximal value in a slice.

3
00:00:08,40 --> 00:00:12,50
Assume you have a slice with not randomly chosen numbers,

4
00:00:12,50 --> 00:00:14,90
the slice nums.

5
00:00:14,90 --> 00:00:17,00
I would like you to write a code

6
00:00:17,00 --> 00:00:19,10
that finds the maximal value in the slice

7
00:00:19,10 --> 00:00:20,00
and prints it out.

