1
00:00:00,60 --> 00:00:01,50
- [Instructor] There are two ways

2
00:00:01,50 --> 00:00:05,60
to specify conditions in Go, if and switch.

3
00:00:05,60 --> 00:00:07,60
Let's start with if.

4
00:00:07,60 --> 00:00:11,60
In line nine, I'm assigning 10 to the variable x.

5
00:00:11,60 --> 00:00:16,30
And then in line 11, I can ask if x is bigger than five.

6
00:00:16,30 --> 00:00:19,30
Unlike Java or C++, you don't need parenthesis

7
00:00:19,30 --> 00:00:21,70
around the condition.

8
00:00:21,70 --> 00:00:25,20
And if you run it, go run if.go,

9
00:00:25,20 --> 00:00:28,90
we'll see that x is big is being printed out.

10
00:00:28,90 --> 00:00:30,50
I'm going to paste another example

11
00:00:30,50 --> 00:00:33,50
of a switch statement from the exercise file.

12
00:00:33,50 --> 00:00:35,90
You can use else to specify what to do

13
00:00:35,90 --> 00:00:39,40
when a condition is not met.

14
00:00:39,40 --> 00:00:43,60
So we asked in line 15 if x is bigger than a hundred,

15
00:00:43,60 --> 00:00:45,60
it will print that it's very big

16
00:00:45,60 --> 00:00:48,20
but this is going to be false, so we'll go to the else

17
00:00:48,20 --> 00:00:51,90
section and print that x is not that big.

18
00:00:51,90 --> 00:00:53,10
Let's save it.

19
00:00:53,10 --> 00:00:56,30
And when we run it now, we'll see x is big from the previous

20
00:00:56,30 --> 00:01:00,30
statement and x is not that big from the current one.

21
00:01:00,30 --> 00:01:04,20
You can use double ampersand for a logical and.

22
00:01:04,20 --> 00:01:07,30
So if x is bigger than five

23
00:01:07,30 --> 00:01:10,10
and x is smaller than 15,

24
00:01:10,10 --> 00:01:13,70
it will print out x is just right.

25
00:01:13,70 --> 00:01:16,60
When we run it here, we'll see that the last statement

26
00:01:16,60 --> 00:01:20,50
is x is just right.

27
00:01:20,50 --> 00:01:24,20
In the same way that we used double ampersand for and,

28
00:01:24,20 --> 00:01:28,00
we can use double pipe for logical or.

29
00:01:28,00 --> 00:01:31,80
So we set it, if x is smaller than 20

30
00:01:31,80 --> 00:01:37,00
or x is bigger than 30, we print it x is out of range.

31
00:01:37,00 --> 00:01:39,80
And again if we save, we can run it and to see

32
00:01:39,80 --> 00:01:44,20
that the last line says that x is out of range.

33
00:01:44,20 --> 00:01:47,40
If can have an optional initialization statement.

34
00:01:47,40 --> 00:01:51,20
We assigned 11 to a and 20 to b,

35
00:01:51,20 --> 00:01:55,40
and then we asked, if fraction which is a divided by b,

36
00:01:55,40 --> 00:01:58,60
and then semicolon and then asking the condition,

37
00:01:58,60 --> 00:02:01,40
is fraction bigger than 0.5,

38
00:02:01,40 --> 00:02:07,80
it will print out that a is more than half of b.

39
00:02:07,80 --> 00:02:10,10
And when we run this one, we'll see that we get

40
00:02:10,10 --> 00:02:13,50
a is more than half of b printed out.

41
00:02:13,50 --> 00:02:15,70
And now we have switch.go opened.

42
00:02:15,70 --> 00:02:18,10
In line 12 we have switch x.

43
00:02:18,10 --> 00:02:20,80
We start with an initialization statement.

44
00:02:20,80 --> 00:02:23,40
In our case it's just the value of x.

45
00:02:23,40 --> 00:02:25,40
And then we list the cases.

46
00:02:25,40 --> 00:02:29,00
In line 13, when x is one we're going to print one.

47
00:02:29,00 --> 00:02:31,80
In line 15, when x is two we're going to print two.

48
00:02:31,80 --> 00:02:33,20
Et cetera, Et cetera.

49
00:02:33,20 --> 00:02:36,20
And lastly at line 19, we have default,

50
00:02:36,20 --> 00:02:39,20
which means if none of the other cases matched,

51
00:02:39,20 --> 00:02:42,30
we're going to execute this code.

52
00:02:42,30 --> 00:02:45,20
Unlike some other languages, we don't have to specify

53
00:02:45,20 --> 00:02:47,60
break after each case.

54
00:02:47,60 --> 00:02:50,90
A case value also don't have to be a number.

55
00:02:50,90 --> 00:02:54,10
It can be a string or something else.

56
00:02:54,10 --> 00:02:56,00
Let's save it

57
00:02:56,00 --> 00:02:57,40
and run.

58
00:02:57,40 --> 00:02:59,20
So go run

59
00:02:59,20 --> 00:03:01,30
switch.go.

60
00:03:01,30 --> 00:03:05,10
And we see that the two is being printed out.

61
00:03:05,10 --> 00:03:09,40
The other option is to use switch without an expression.

62
00:03:09,40 --> 00:03:12,90
Then the case statement will have a condition.

63
00:03:12,90 --> 00:03:17,10
In our case at line 23, we do a naked switch

64
00:03:17,10 --> 00:03:20,00
and then in the case in line 24 and others,

65
00:03:20,00 --> 00:03:22,20
we specify the condition.

66
00:03:22,20 --> 00:03:25,70
In line 24 is if it's bigger than a hundred.

67
00:03:25,70 --> 00:03:28,30
Let's save this one as well

68
00:03:28,30 --> 00:03:32,00
and run.

69
00:03:32,00 --> 00:03:34,60
And this time we get x is small,

70
00:03:34,60 --> 00:03:37,00
that is coming from the default statement.

