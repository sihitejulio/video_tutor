1
00:00:00,60 --> 00:00:03,20
- [Instructor] An even-ended number is a number

2
00:00:03,20 --> 00:00:05,80
whose first and last digits are the same.

3
00:00:05,80 --> 00:00:10,30
For example, one, 11, 121.

4
00:00:10,30 --> 00:00:13,10
How many even-ended numbers are there

5
00:00:13,10 --> 00:00:16,40
that are multiplication of four-digit numbers?

6
00:00:16,40 --> 00:00:21,30
For example, if you multiply 1,001 by 1,011,

7
00:00:21,30 --> 00:00:25,60
you'll get a number which is even-ended.

8
00:00:25,60 --> 00:00:28,20
It's easier to check if a number is even-ended,

9
00:00:28,20 --> 00:00:30,30
by converting it to a string.

10
00:00:30,30 --> 00:00:35,40
This can be done by the func.Sprintf function.

11
00:00:35,40 --> 00:00:38,50
So far, we've seen Println and Printf,

12
00:00:38,50 --> 00:00:41,40
which prints formatted output.

13
00:00:41,40 --> 00:00:45,20
The same function, starting with s instead of f,

14
00:00:45,20 --> 00:00:47,70
will return a formatted string.

15
00:00:47,70 --> 00:00:51,50
Let's see how it can work with Sprintf.

16
00:00:51,50 --> 00:00:55,50
In line 10, we have the number 42, and then in line 11,

17
00:00:55,50 --> 00:00:59,30
we use Sprintf to convert this number to a string.

18
00:00:59,30 --> 00:01:04,00
And finally, in line 13, we print out the value for s,

19
00:01:04,00 --> 00:01:06,60
and the type of s.

20
00:01:06,60 --> 00:01:08,70
Let's save,

21
00:01:08,70 --> 00:01:10,30
and see this in action.

22
00:01:10,30 --> 00:01:14,60
Go run sprintf.go.

23
00:01:14,60 --> 00:01:17,60
And we see that s is 42.

24
00:01:17,60 --> 00:01:22,50
It's hard to distinguish if s is a string,

25
00:01:22,50 --> 00:01:27,70
or an integer just by looking the number 42.

26
00:01:27,70 --> 00:01:33,30
So, the printf, also has a verb q,

27
00:01:33,30 --> 00:01:34,90
the %q verb.

28
00:01:34,90 --> 00:01:39,60
And if we switch the %s to %q,

29
00:01:39,60 --> 00:01:42,80
and run it again,

30
00:01:42,80 --> 00:01:44,30
we can see now we have quotes around the 42.

31
00:01:44,30 --> 00:01:48,00
This helps a lot when debugging.

32
00:01:48,00 --> 00:01:50,90
Now you have enough tools to solve the problem yourself.

33
00:01:50,90 --> 00:01:52,00
I'll wait.

