1
00:00:00,60 --> 00:00:02,20
- [Programmer] So here is our function.

2
00:00:02,20 --> 00:00:03,30
Kill server.

3
00:00:03,30 --> 00:00:08,70
It gets a pid file as a string, and returns an error.

4
00:00:08,70 --> 00:00:13,20
First, we try to read the file using the ioutil.ReadFile.

5
00:00:13,20 --> 00:00:15,50
Note that in real life, this might be dangerous

6
00:00:15,50 --> 00:00:17,10
if the file is very big.

7
00:00:17,10 --> 00:00:20,10
Have a look at io.limitreader if you want to make sure

8
00:00:20,10 --> 00:00:23,00
you don't read too much into memory.

9
00:00:23,00 --> 00:00:25,50
If there was an error reading the file,

10
00:00:25,50 --> 00:00:28,70
we use errors.Wrap to wrap the original error

11
00:00:28,70 --> 00:00:31,80
and give it a meaningful error message.

12
00:00:31,80 --> 00:00:35,40
After that, we try to remove the pid file.

13
00:00:35,40 --> 00:00:38,70
And if we error here, we just print out an error,

14
00:00:38,70 --> 00:00:40,30
but we don't fail.

15
00:00:40,30 --> 00:00:43,50
Since it's okay, we can continue.

16
00:00:43,50 --> 00:00:47,50
Use string to wrap the data, which is a byte slice.

17
00:00:47,50 --> 00:00:52,80
And then we use strings.TrimSpace to trim all the spaces

18
00:00:52,80 --> 00:00:55,00
from the pid.

19
00:00:55,00 --> 00:00:59,40
Then we convert the pid from a string to an integer.

20
00:00:59,40 --> 00:01:02,60
And if there is an error there, again, we abort

21
00:01:02,60 --> 00:01:05,90
with an error "bad process ID".

22
00:01:05,90 --> 00:01:09,60
Otherwise, everything is fine, so we simulate the queue

23
00:01:09,60 --> 00:01:12,10
by printing out that we're killing the server

24
00:01:12,10 --> 00:01:14,00
and returning nil.

25
00:01:14,00 --> 00:01:17,40
In the main function, we just call killServer with the file

26
00:01:17,40 --> 00:01:20,80
called "server.pid", and if there was an error,

27
00:01:20,80 --> 00:01:24,90
we print the error, and we exit with a non-zero value.

28
00:01:24,90 --> 00:01:26,10
Let's have a look.

29
00:01:26,10 --> 00:01:28,20
First there's no pid file.

30
00:01:28,20 --> 00:01:31,00
So when we run our code, we will get the error

31
00:01:31,00 --> 00:01:35,20
"cannot open pid file".

32
00:01:35,20 --> 00:01:38,80
Now let's create a file with a bad pid.

33
00:01:38,80 --> 00:01:40,50
So, echo hi

34
00:01:40,50 --> 00:01:42,80
from server.pid

35
00:01:42,80 --> 00:01:45,40
and then we'll run our server.

36
00:01:45,40 --> 00:01:49,20
And this time we get the error, which is a bad process ID.

37
00:01:49,20 --> 00:01:51,90
It cannot process.

38
00:01:51,90 --> 00:01:55,50
And last, let's put a valid number inside the pid file

39
00:01:55,50 --> 00:01:58,00
and run our code.

40
00:01:58,00 --> 00:02:00,00
And this time, it works as expected.

