1
00:00:00,60 --> 00:00:03,60
- [Instructor] The usual method in Go for signaling errors

2
00:00:03,60 --> 00:00:05,70
is to return an error value.

3
00:00:05,70 --> 00:00:08,10
However, there are cases

4
00:00:08,10 --> 00:00:09,90
when there's an error you can't handle,

5
00:00:09,90 --> 00:00:12,20
and would like to signal this.

6
00:00:12,20 --> 00:00:14,80
In Go, there's a built-in panic function

7
00:00:14,80 --> 00:00:17,00
that will do just that.

8
00:00:17,00 --> 00:00:18,80
Let's cause a panic.

9
00:00:18,80 --> 00:00:21,60
We create a slice with three elements,

10
00:00:21,60 --> 00:00:24,80
and then we try to access the tenth element,

11
00:00:24,80 --> 00:00:27,20
which is out of bounds.

12
00:00:27,20 --> 00:00:32,00
When we run this code,

13
00:00:32,00 --> 00:00:35,50
we will see that we get panic, index out of range,

14
00:00:35,50 --> 00:00:37,80
and we will see where it happened,

15
00:00:37,80 --> 00:00:41,10
in line 10 in our code.

16
00:00:41,10 --> 00:00:43,90
I mostly use panic when trying out code.

17
00:00:43,90 --> 00:00:47,40
This way, I get the error and the stack trace.

18
00:00:47,40 --> 00:00:49,20
However, when the code shapes up,

19
00:00:49,20 --> 00:00:51,70
I remove all panics from my code.

20
00:00:51,70 --> 00:00:54,00
Let's see an example.

21
00:00:54,00 --> 00:00:58,40
I'm going to comment out the section with the panic,

22
00:00:58,40 --> 00:01:04,40
and then file, err, equal os.Open

23
00:01:04,40 --> 00:01:08,50
no-such-file.

24
00:01:08,50 --> 00:01:12,80
And then if err is not nil,

25
00:01:12,80 --> 00:01:16,10
I'm just going to panic with a err.

26
00:01:16,10 --> 00:01:20,20
If there was no error, I'm going to defer file.Close

27
00:01:20,20 --> 00:01:22,20
to make sure that the file is closed,

28
00:01:22,20 --> 00:01:29,50
and I'm going to print out that the file was opened.

29
00:01:29,50 --> 00:01:34,10
And of course, I need to import the os package.

30
00:01:34,10 --> 00:01:37,90
Let's run this code, go run panic.go.

31
00:01:37,90 --> 00:01:40,70
And we see panic, we can't open the file,

32
00:01:40,70 --> 00:01:46,00
and I know exactly the line number where the error happened.

33
00:01:46,00 --> 00:01:50,00
So here I define a function called safeValue.

34
00:01:50,00 --> 00:01:53,90
It gets values, which is a slice of integer,

35
00:01:53,90 --> 00:01:57,30
and an index where I want to get the value from,

36
00:01:57,30 --> 00:02:00,20
and returns an integer.

37
00:02:00,20 --> 00:02:02,40
And we do a defer of a function,

38
00:02:02,40 --> 00:02:04,80
and this is an anonymous function,

39
00:02:04,80 --> 00:02:07,00
that gets no argument.

40
00:02:07,00 --> 00:02:09,40
And it checks, if there was an error,

41
00:02:09,40 --> 00:02:11,20
the built-in recover function,

42
00:02:11,20 --> 00:02:12,80
and the error was not nil,

43
00:02:12,80 --> 00:02:17,10
I'm going just to print out that there was an error.

44
00:02:17,10 --> 00:02:18,90
And what I'm going to do is try

45
00:02:18,90 --> 00:02:24,20
and return values at index.

46
00:02:24,20 --> 00:02:26,00
Now let's call it.

47
00:02:26,00 --> 00:02:31,30
So I'm going to do v equal safeValue

48
00:02:31,30 --> 00:02:35,40
of a slice of integers with one, two, and three

49
00:02:35,40 --> 00:02:37,90
at index number 10.

50
00:02:37,90 --> 00:02:43,80
And I'm going to print what we get.

51
00:02:43,80 --> 00:02:49,10
Let's save it and run it.

52
00:02:49,10 --> 00:02:51,60
And we see our error being printed out,

53
00:02:51,60 --> 00:02:54,10
and we get the value zero.

54
00:02:54,10 --> 00:02:58,20
We got zero because this is the zero value for integers.

55
00:02:58,20 --> 00:03:01,00
Since we didn't set value in our panic handler,

56
00:03:01,00 --> 00:03:03,40
we got zero value for integers.

57
00:03:03,40 --> 00:03:06,30
I usually use recover to wrap untrusted code,

58
00:03:06,30 --> 00:03:10,40
such as third-party libraries, which misbehave.

59
00:03:10,40 --> 00:03:12,90
The use of panic is discouraged in Go,

60
00:03:12,90 --> 00:03:15,40
so follow the Hitchhikers Guide to the Galaxy,

61
00:03:15,40 --> 00:03:17,00
and don't panic.

