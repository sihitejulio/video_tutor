1
00:00:03,60 --> 00:00:05,10
- [Instructor] When writing servers,

2
00:00:05,10 --> 00:00:06,60
it's common for the server to store

3
00:00:06,60 --> 00:00:11,60
its process identifier and integer in a file.

4
00:00:11,60 --> 00:00:13,40
When you want to shut down the server,

5
00:00:13,40 --> 00:00:16,30
you read the file, and issue a kill command

6
00:00:16,30 --> 00:00:19,90
to the identifier you found in the file.

7
00:00:19,90 --> 00:00:22,40
Write a function called killServer,

8
00:00:22,40 --> 00:00:26,90
which here, a pidFile and returns an error.

9
00:00:26,90 --> 00:00:29,70
It should read the process ID from the file,

10
00:00:29,70 --> 00:00:31,60
convert it to an integer,

11
00:00:31,60 --> 00:00:36,50
and instead of killing it, print just killing process ID.

12
00:00:36,50 --> 00:00:41,80
Use github.com/pkg/errors to wrap errors.

13
00:00:41,80 --> 00:00:46,40
Use io/ioutil ReadFile to read the file.

14
00:00:46,40 --> 00:00:50,70
And use strconv.Atoi to convert the file content

15
00:00:50,70 --> 00:00:52,80
to an integer.

16
00:00:52,80 --> 00:00:55,20
Try to think of all the errors that can happen

17
00:00:55,20 --> 00:00:57,00
and how you can handle them.

