1
00:00:00,60 --> 00:00:02,60
- [Instructor] It's common to have some configuration

2
00:00:02,60 --> 00:00:03,90
stored in a file.

3
00:00:03,90 --> 00:00:05,60
Your program will usually start

4
00:00:05,60 --> 00:00:09,30
by reading the configuration and then continue to execute.

5
00:00:09,30 --> 00:00:11,80
Let's have a look.

6
00:00:11,80 --> 00:00:14,00
We define a configuration struct

7
00:00:14,00 --> 00:00:17,40
which holds the configuration values.

8
00:00:17,40 --> 00:00:18,90
And then we have a function

9
00:00:18,90 --> 00:00:21,10
to read the configuration from the file.

10
00:00:21,10 --> 00:00:23,90
It returns a pointer to a configuration object

11
00:00:23,90 --> 00:00:25,90
and a possible error.

12
00:00:25,90 --> 00:00:28,80
We try to open the configuration file.

13
00:00:28,80 --> 00:00:30,80
If you cannot open the file,

14
00:00:30,80 --> 00:00:33,00
we return a nil for the configuration

15
00:00:33,00 --> 00:00:35,10
and the error that happened.

16
00:00:35,10 --> 00:00:37,90
We use defer to make sure that the file is closed

17
00:00:37,90 --> 00:00:40,70
when readConfig is done.

18
00:00:40,70 --> 00:00:43,30
And we create the configuration,

19
00:00:43,30 --> 00:00:46,10
parse it, and return the configuration object,

20
00:00:46,10 --> 00:00:49,40
and a nil signifying no errors.

21
00:00:49,40 --> 00:00:52,30
In our main, we call readConfig.

22
00:00:52,30 --> 00:00:53,80
And we are going to call it

23
00:00:53,80 --> 00:00:55,90
with a path to a configuration file

24
00:00:55,90 --> 00:00:59,00
that does not exist on purpose.

25
00:00:59,00 --> 00:00:59,80
If there is an error,

26
00:00:59,80 --> 00:01:02,80
we're going to print the error to standard error

27
00:01:02,80 --> 00:01:05,60
and exit the program.

28
00:01:05,60 --> 00:01:08,50
Otherwise, we continue with the normal operation,

29
00:01:08,50 --> 00:01:11,90
and in our case, we'll just print out the configuration.

30
00:01:11,90 --> 00:01:13,50
Let's run it

31
00:01:13,50 --> 00:01:19,60
go run errors.go.

32
00:01:19,60 --> 00:01:21,90
You will get an error and an exit code

33
00:01:21,90 --> 00:01:25,90
which is not zero which is good.

34
00:01:25,90 --> 00:01:28,20
But we're missing the context of why

35
00:01:28,20 --> 00:01:31,90
you wanted to open the file and where it happened.

36
00:01:31,90 --> 00:01:34,50
In languages, such as Python or Java,

37
00:01:34,50 --> 00:01:36,90
you'd use exceptions which will show you

38
00:01:36,90 --> 00:01:39,80
the stack trace where the problem was.

39
00:01:39,80 --> 00:01:41,50
To help us with this,

40
00:01:41,50 --> 00:01:45,30
Dave Cheney wrote a package called pkg/errors.

41
00:01:45,30 --> 00:01:47,70
This package is not in the standard library,

42
00:01:47,70 --> 00:01:51,80
and you'd use the go tool to install it.

43
00:01:51,80 --> 00:02:01,50
In a terminal, we write go get github.com/pkg/errors

44
00:02:01,50 --> 00:02:03,30
and Enter.

45
00:02:03,30 --> 00:02:04,90
Now, it's installed in our machine,

46
00:02:04,90 --> 00:02:07,30
and we can use it in our code.

47
00:02:07,30 --> 00:02:09,50
So we input it,

48
00:02:09,50 --> 00:02:14,80
and again, github.com/pkg/errors.

49
00:02:14,80 --> 00:02:17,70
And in our code,

50
00:02:17,70 --> 00:02:24,20
when we return to error, we use errors.Wrap the error

51
00:02:24,20 --> 00:02:31,20
and state the reason, can't open configuration file.

52
00:02:31,20 --> 00:02:37,50
Save the code and run it again.

53
00:02:37,50 --> 00:02:38,90
And this time we see

54
00:02:38,90 --> 00:02:42,90
can't open configuration file and then the reason of error

55
00:02:42,90 --> 00:02:45,80
which is really nice and explain what happens.

56
00:02:45,80 --> 00:02:48,10
If you'd like to see the full call stack,

57
00:02:48,10 --> 00:02:50,70
you can use the +v formatting verb.

58
00:02:50,70 --> 00:02:53,80
Be careful about showing stack traces to users.

59
00:02:53,80 --> 00:02:55,10
Once they see a stack trace,

60
00:02:55,10 --> 00:02:57,40
they will think it's a bug in your program.

61
00:02:57,40 --> 00:03:01,50
That's why you usually save stack traces in log files.

62
00:03:01,50 --> 00:03:06,90
We will use the built in log package for this.

63
00:03:06,90 --> 00:03:10,60
So we are going to import log.

64
00:03:10,60 --> 00:03:13,80
By default, the log package will print to standard error.

65
00:03:13,80 --> 00:03:20,90
We're going to change destination of the log file.

66
00:03:20,90 --> 00:03:23,80
Here's the function for setting up the logging.

67
00:03:23,80 --> 00:03:28,10
We open a file app.log which will read the log file

68
00:03:28,10 --> 00:03:30,30
in a mode for appending,

69
00:03:30,30 --> 00:03:32,80
meaning that anything that we are going to write

70
00:03:32,80 --> 00:03:36,40
will be appended at the end of the log file.

71
00:03:36,40 --> 00:03:43,80
And we set the output of the logging package to this file.

72
00:03:43,80 --> 00:03:49,40
In our main, we call setupLogging

73
00:03:49,40 --> 00:03:51,90
and then apart from printing to standard error

74
00:03:51,90 --> 00:03:56,50
uses log.Printf("error

75
00:03:56,50 --> 00:04:00,80
and then we use %+v

76
00:04:00,80 --> 00:04:04,70
and the err.

77
00:04:04,70 --> 00:04:10,50
Let's save this and run it again.

78
00:04:10,50 --> 00:04:12,10
We see the same output,

79
00:04:12,10 --> 00:04:15,80
but this time we also have a file called app.log

80
00:04:15,80 --> 00:04:20,00
and inside we can see the full stack trace of what happened.

