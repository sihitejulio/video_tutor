1
00:00:00,70 --> 00:00:01,70
- [Instructor] Let's take a look

2
00:00:01,70 --> 00:00:05,10
at the small Go program and break it down line by line.

3
00:00:05,10 --> 00:00:07,50
The first line is a comment.

4
00:00:07,50 --> 00:00:10,70
You can either use single-line comments, like this,

5
00:00:10,70 --> 00:00:12,40
or multi-line comments, which start

6
00:00:12,40 --> 00:00:14,30
with a forward slash and an asterisk

7
00:00:14,30 --> 00:00:16,60
and end with an asterisk and a forward slash,

8
00:00:16,60 --> 00:00:20,20
very much like C++ or Java.

9
00:00:20,20 --> 00:00:22,60
Line three is package main.

10
00:00:22,60 --> 00:00:24,80
Go code is organized in packages.

11
00:00:24,80 --> 00:00:27,40
This helps in big projects, allowing teams

12
00:00:27,40 --> 00:00:30,10
to work on parts of the system independently.

13
00:00:30,10 --> 00:00:32,80
The main package has a special meaning in Go and

14
00:00:32,80 --> 00:00:34,70
it will make your code compile

15
00:00:34,70 --> 00:00:38,20
for an executable and not the package.

16
00:00:38,20 --> 00:00:41,60
Line five is an import statement.

17
00:00:41,60 --> 00:00:44,60
Go is a very simple language, and by itself,

18
00:00:44,60 --> 00:00:47,10
does not have many built-in functions.

19
00:00:47,10 --> 00:00:50,80
Most of the code you'll be using will be in packages.

20
00:00:50,80 --> 00:00:52,90
Go comes with a start library

21
00:00:52,90 --> 00:00:57,00
which contains many packages.

22
00:00:57,00 --> 00:01:00,10
In line six, we import the f m t package,

23
00:01:00,10 --> 00:01:03,20
which is pronounced fmt.

24
00:01:03,20 --> 00:01:08,50
The fmt package contains functions for formatted printing.

25
00:01:08,50 --> 00:01:11,10
In line nine we have function main.

26
00:01:11,10 --> 00:01:14,00
We define a function with the func keyword.

27
00:01:14,00 --> 00:01:18,10
The body of the function is enclosed in curly braces.

28
00:01:18,10 --> 00:01:21,70
The function main also has a special meaning in Go.

29
00:01:21,70 --> 00:01:23,90
It will be executed by the Go runtime

30
00:01:23,90 --> 00:01:27,00
when the program starts.

31
00:01:27,00 --> 00:01:29,50
The print l n function from the fmt package

32
00:01:29,50 --> 00:01:32,20
prints its argument in a new line.

33
00:01:32,20 --> 00:01:34,00
You need to prefix the function name

34
00:01:34,00 --> 00:01:37,60
print l n with the package it came from.

35
00:01:37,60 --> 00:01:39,70
Unlike C++ or Java, you don't need

36
00:01:39,70 --> 00:01:43,10
to place a semicolon at the end of the line.

37
00:01:43,10 --> 00:01:45,40
The message printed out is a string.

38
00:01:45,40 --> 00:01:49,00
Strings in Go starts and end with double quotes.

39
00:01:49,00 --> 00:01:51,30
Go strings are Unicode, which means

40
00:01:51,30 --> 00:01:53,00
you don't need special code to

41
00:01:53,00 --> 00:01:55,00
support non-English languages.

