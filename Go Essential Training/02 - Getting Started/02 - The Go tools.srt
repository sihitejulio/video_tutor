1
00:00:00,60 --> 00:00:03,30
- Go comes with a tool called Go.

2
00:00:03,30 --> 00:00:05,60
You use the Go tool for most everyday tasks

3
00:00:05,60 --> 00:00:08,50
such as building, testing, benchmarking,

4
00:00:08,50 --> 00:00:11,10
and starting third party packages and more.

5
00:00:11,10 --> 00:00:13,40
Open a terminal and change directory

6
00:00:13,40 --> 00:00:15,50
to where you saved the exercise file.

7
00:00:15,50 --> 00:00:20,20
Now type, go run welcome.go.

8
00:00:20,20 --> 00:00:21,80
You should see welcome gophers

9
00:00:21,80 --> 00:00:24,50
and a smiley face printed out.

10
00:00:24,50 --> 00:00:26,90
What the Go tool did behind-the-scenes

11
00:00:26,90 --> 00:00:29,80
is to compile your program and then run it.

12
00:00:29,80 --> 00:00:32,90
Since by design, the Go compiler is fast,

13
00:00:32,90 --> 00:00:37,40
go run works quickly and enables fast development cycles.

14
00:00:37,40 --> 00:00:39,20
Let's clear the screen.

15
00:00:39,20 --> 00:00:41,50
Once you're done developing, you'd like to build

16
00:00:41,50 --> 00:00:43,90
an executable that you can distribute.

17
00:00:43,90 --> 00:00:46,80
This is done with the go build command.

18
00:00:46,80 --> 00:00:51,20
Go build welcome.go.

19
00:00:51,20 --> 00:00:52,00
And Enter.

20
00:00:52,00 --> 00:00:54,60
You created a file called welcome.

21
00:00:54,60 --> 00:00:58,10
On Windows, it will be welcome.exe.

22
00:00:58,10 --> 00:01:01,60
And if you run it, welcome,

23
00:01:01,60 --> 00:01:04,60
you will see the same output for our program.

24
00:01:04,60 --> 00:01:06,30
Let's clear the screen again.

25
00:01:06,30 --> 00:01:09,20
The Go tool can execute several other commands.

26
00:01:09,20 --> 00:01:12,30
To see all of them, run go help.

27
00:01:12,30 --> 00:01:15,40
Let's go over some of these commands.

28
00:01:15,40 --> 00:01:17,30
Test will run the tests.

29
00:01:17,30 --> 00:01:20,90
Go has a built-in test suite and you'll learn how to use it.

30
00:01:20,90 --> 00:01:23,50
Test can also run benchmarks so you'll be able to

31
00:01:23,50 --> 00:01:27,40
measure performance of your code.

32
00:01:27,40 --> 00:01:30,60
Get is for installing third-party packages.

33
00:01:30,60 --> 00:01:32,40
If you need to connect to a database,

34
00:01:32,40 --> 00:01:37,00
pass file format such as yaml, and many other things,

35
00:01:37,00 --> 00:01:41,00
there's already a package that does it for you.

36
00:01:41,00 --> 00:01:45,00
Fmt, pronouned fuhmt, will format your code.

37
00:01:45,00 --> 00:01:47,50
Most editors run fmt and save.

38
00:01:47,50 --> 00:01:50,20
This eliminates these long and boring discussions

39
00:01:50,20 --> 00:01:52,30
about code format.

40
00:01:52,30 --> 00:01:54,30
There are many other commands and with time,

41
00:01:54,30 --> 00:01:56,70
you'll probably use more of them.

42
00:01:56,70 --> 00:02:01,20
Modern IDEs such as Visual Studio Code or Golang

43
00:02:01,20 --> 00:02:04,00
will run a lot of the Go tool commands for you.

