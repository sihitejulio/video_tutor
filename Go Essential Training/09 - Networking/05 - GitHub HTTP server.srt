1
00:00:00,60 --> 00:00:02,80
- [Instructor] Go comes with a powerful HTTP server

2
00:00:02,80 --> 00:00:04,20
in the standard library.

3
00:00:04,20 --> 00:00:06,90
It's easy to build the REST APIs with it.

4
00:00:06,90 --> 00:00:09,20
We write handler functions.

5
00:00:09,20 --> 00:00:11,00
A handler function is a function

6
00:00:11,00 --> 00:00:14,30
that gets an http.ResponseWriter

7
00:00:14,30 --> 00:00:16,10
where you write the response to

8
00:00:16,10 --> 00:00:19,00
and a pointer to an http.Request,

9
00:00:19,00 --> 00:00:22,10
which represents the current request.

10
00:00:22,10 --> 00:00:26,20
Since the http.ResponseWriter implements io.writer,

11
00:00:26,20 --> 00:00:30,60
we can use the Fprintf to print the output to it.

12
00:00:30,60 --> 00:00:34,70
We need to mount the endpoint to the Handler.

13
00:00:34,70 --> 00:00:38,80
This means that every time someone accesses /hello

14
00:00:38,80 --> 00:00:39,60
on our web server,

15
00:00:39,60 --> 00:00:42,40
helloHandler is going to get called.

16
00:00:42,40 --> 00:00:44,00
And now we tell the HTTP server

17
00:00:44,00 --> 00:00:47,50
to listen and serve on port 8080.

18
00:00:47,50 --> 00:00:49,80
This is potentially an endless loop.

19
00:00:49,80 --> 00:00:53,60
But if there is an error, it will exit with an error value.

20
00:00:53,60 --> 00:00:56,20
And in our case, we're going to print out the error

21
00:00:56,20 --> 00:00:58,30
and exit the program.

22
00:00:58,30 --> 00:01:00,90
Let's save it and run it,

23
00:01:00,90 --> 00:01:05,00
go run httpd.go

24
00:01:05,00 --> 00:01:08,60
and now when we go to local host 8080/hello,

25
00:01:08,60 --> 00:01:11,10
we see Hello Gophers!

26
00:01:11,10 --> 00:01:13,40
We built a production-ready HTTP server

27
00:01:13,40 --> 00:01:15,50
in 20 lines of code.

28
00:01:15,50 --> 00:01:18,70
I find it pretty impressive.

29
00:01:18,70 --> 00:01:19,60
Let's add the handler

30
00:01:19,60 --> 00:01:22,80
that receives a request from a thematical operation

31
00:01:22,80 --> 00:01:24,70
in a JSON format

32
00:01:24,70 --> 00:01:27,90
and sends back a JSON response.

33
00:01:27,90 --> 00:01:29,50
These handlers are very common

34
00:01:29,50 --> 00:01:30,90
and have three steps,

35
00:01:30,90 --> 00:01:34,70
decoder request, do the work, and encoder reply.

36
00:01:34,70 --> 00:01:37,30
First, we define the structures

37
00:01:37,30 --> 00:01:40,40
for the request and the response.

38
00:01:40,40 --> 00:01:43,60
The request has an operand, like plus, minus,

39
00:01:43,60 --> 00:01:45,80
a left and a right side of the operand.

40
00:01:45,80 --> 00:01:50,60
And the response has an error value and a result.

41
00:01:50,60 --> 00:01:55,30
And now the handler.

42
00:01:55,30 --> 00:01:59,50
The handler again receives an http.ResponseWriter

43
00:01:59,50 --> 00:02:01,60
and a pointer to a request.

44
00:02:01,60 --> 00:02:03,80
The first step is decode the request.

45
00:02:03,80 --> 00:02:07,30
We defer the Body.Close to make sure it's closed,

46
00:02:07,30 --> 00:02:09,30
create a new json decoder,

47
00:02:09,30 --> 00:02:10,80
an empty request,

48
00:02:10,80 --> 00:02:13,00
and then try to decode the request.

49
00:02:13,00 --> 00:02:15,80
If there is an error, we use the http.Error

50
00:02:15,80 --> 00:02:19,40
to send the error back with the appropriate error value.

51
00:02:19,40 --> 00:02:21,00
Otherwise, we do the work.

52
00:02:21,00 --> 00:02:22,80
We create a response

53
00:02:22,80 --> 00:02:24,40
and then depending on the operand,

54
00:02:24,40 --> 00:02:26,50
if it's a plus, we'll do a plus operation,

55
00:02:26,50 --> 00:02:28,80
if it's a minus, we'll do a minus operation,

56
00:02:28,80 --> 00:02:30,20
et cetera, et cetera.

57
00:02:30,20 --> 00:02:32,50
And in the case of division,

58
00:02:32,50 --> 00:02:34,60
if we are asked to divide by zero,

59
00:02:34,60 --> 00:02:36,60
we will return an error.

60
00:02:36,60 --> 00:02:38,60
The same if it's an unknown operation,

61
00:02:38,60 --> 00:02:40,00
the response and error.

62
00:02:40,00 --> 00:02:42,90
And the final step is to encode the result.

63
00:02:42,90 --> 00:02:44,90
We set the Content-Type header

64
00:02:44,90 --> 00:02:46,50
to application/json

65
00:02:46,50 --> 00:02:48,30
and if there was an error,

66
00:02:48,30 --> 00:02:52,60
we are going to write http BadRequest status

67
00:02:52,60 --> 00:02:56,50
and at the end, we're going to encode the response

68
00:02:56,50 --> 00:02:59,60
in a json format into w.

69
00:02:59,60 --> 00:03:01,10
If you failed encoding,

70
00:03:01,10 --> 00:03:07,30
we're just going to log that we failed to encode.

71
00:03:07,30 --> 00:03:10,90
Don't forget to mount your new handler.

72
00:03:10,90 --> 00:03:14,70
This time everyone going to /math on our server,

73
00:03:14,70 --> 00:03:19,90
the math handler is going to get called.

74
00:03:19,90 --> 00:03:21,70
And of course, we need to import the encoding

75
00:03:21,70 --> 00:03:25,60
through json package.

76
00:03:25,60 --> 00:03:29,20
Let's go back to our server and we start it.

77
00:03:29,20 --> 00:03:32,30
So go run httpd.go.

78
00:03:32,30 --> 00:03:35,30
I'm going to use the curl command line

79
00:03:35,30 --> 00:03:37,50
to make a post request.

80
00:03:37,50 --> 00:03:39,50
If you're not familiar with the command line,

81
00:03:39,50 --> 00:03:43,90
you can use Postman to issue post requests.

82
00:03:43,90 --> 00:03:46,00
Here is an example of the request

83
00:03:46,00 --> 00:03:47,70
called req.json.

84
00:03:47,70 --> 00:03:48,90
The operand is division

85
00:03:48,90 --> 00:03:51,80
and we divide 1 by 2.

86
00:03:51,80 --> 00:03:57,10
So we do curl -d@req.json,

87
00:03:57,10 --> 00:03:59,00
this is the data that it's going to send,

88
00:03:59,00 --> 00:04:05,50
http://localhost:8080/math.

89
00:04:05,50 --> 00:04:07,50
And we got back that there was no error

90
00:04:07,50 --> 00:04:10,30
and the result is 0.5.

91
00:04:10,30 --> 00:04:11,70
Let's go back

92
00:04:11,70 --> 00:04:17,20
and change the right operand to be 0.0

93
00:04:17,20 --> 00:04:22,20
and call it again.

94
00:04:22,20 --> 00:04:25,30
And this time, we got the error which is division by 0

95
00:04:25,30 --> 00:04:28,20
and 0 for the result.

96
00:04:28,20 --> 00:04:29,40
Looks good.

97
00:04:29,40 --> 00:04:31,10
Once you're done with manual testing,

98
00:04:31,10 --> 00:04:33,70
check out the http test package

99
00:04:33,70 --> 00:04:36,00
on how to automate these tests.

