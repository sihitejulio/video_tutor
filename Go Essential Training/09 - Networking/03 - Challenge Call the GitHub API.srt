1
00:00:00,60 --> 00:00:03,30
- [Miki] GitHub has an API that you can query.

2
00:00:03,30 --> 00:00:06,20
One of the endpoints is slash user.

3
00:00:06,20 --> 00:00:08,30
Here's an example.

4
00:00:08,30 --> 00:00:12,90
When you go to api.github.com/users.tebeka,

5
00:00:12,90 --> 00:00:16,10
you can see some information about my GitHub User.

6
00:00:16,10 --> 00:00:18,50
This is the login that I provided.

7
00:00:18,50 --> 00:00:21,60
You can see that my name is Miki Tebeka,

8
00:00:21,60 --> 00:00:27,20
and you can see that I have 104 public repositories.

9
00:00:27,20 --> 00:00:31,50
Yeah, I should probably clean that up.

10
00:00:31,50 --> 00:00:34,70
Write the function that queries the GitHub User's API

11
00:00:34,70 --> 00:00:36,10
for a given login.

12
00:00:36,10 --> 00:00:38,40
It should return a struct of User

13
00:00:38,40 --> 00:00:42,60
with the name and number of public repositories.

14
00:00:42,60 --> 00:00:44,90
Here's an example of the URL.

15
00:00:44,90 --> 00:00:47,00
Here's the type User struct

16
00:00:47,00 --> 00:00:50,20
with name and public repositories,

17
00:00:50,20 --> 00:00:51,90
and here's the signature of the function.

18
00:00:51,90 --> 00:00:54,40
User information gets a login as a string

19
00:00:54,40 --> 00:00:58,00
and returns a point or two User and a possible error.

