1
00:00:00,60 --> 00:00:03,50
- [Instructor] A common way to communicate between services

2
00:00:03,50 --> 00:00:06,50
is by using HTTP and JSON,

3
00:00:06,50 --> 00:00:09,00
also known as REST API.

4
00:00:09,00 --> 00:00:12,10
This is not the only way to communicate over the network,

5
00:00:12,10 --> 00:00:14,10
and you should do your homework and pick the method

6
00:00:14,10 --> 00:00:16,40
that is right for you.

7
00:00:16,40 --> 00:00:19,30
HTTP plus JSON is very common,

8
00:00:19,30 --> 00:00:21,70
and chances are you'll have to work with them

9
00:00:21,70 --> 00:00:23,30
at one point or another.

10
00:00:23,30 --> 00:00:26,50
You'll find most of what you need in the net/http

11
00:00:26,50 --> 00:00:29,70
and then called in JSON packages.

12
00:00:29,70 --> 00:00:31,80
Let's start by making a get request.

13
00:00:31,80 --> 00:00:34,40
We'll use the httpbin.org service,

14
00:00:34,40 --> 00:00:38,90
which over several endpoints to test your HTTP client.

15
00:00:38,90 --> 00:00:42,20
We import net/http,

16
00:00:42,20 --> 00:00:45,10
and then in main, we do an http.Get

17
00:00:45,10 --> 00:00:48,50
to httpbin.org/get.

18
00:00:48,50 --> 00:00:53,00
Notice we're making an https call, Go can handle that.

19
00:00:53,00 --> 00:00:55,10
We get the response and an error.

20
00:00:55,10 --> 00:01:00,00
If there is an error, we use log.Fatalf to exit the program.

21
00:01:00,00 --> 00:01:02,40
Make sure to close the response body.

22
00:01:02,40 --> 00:01:06,30
We use defer to make sure the response body is closed.

23
00:01:06,30 --> 00:01:08,20
In that case, we're just going to print out

24
00:01:08,20 --> 00:01:11,20
what the server sent us, using io.Copy

25
00:01:11,20 --> 00:01:14,80
from the response body to standard output.

26
00:01:14,80 --> 00:01:16,80
Lets save and run.

27
00:01:16,80 --> 00:01:21,20
Go run httpc.go.

28
00:01:21,20 --> 00:01:23,60
And we see that we got no argument, and header,

29
00:01:23,60 --> 00:01:27,90
and the origin, and the url.

30
00:01:27,90 --> 00:01:32,40
Now lets make a post request with the JSON body.

31
00:01:32,40 --> 00:01:37,50
We define a Job structure, the User, the Action,

32
00:01:37,50 --> 00:01:40,30
and how many times you want the action to be performed,

33
00:01:40,30 --> 00:01:41,70
a Count.

34
00:01:41,70 --> 00:01:47,60
(typing)

35
00:01:47,60 --> 00:01:51,80
In our main, we are going to create a new job object.

36
00:01:51,80 --> 00:01:54,60
The user is Saitama, the action is a punch,

37
00:01:54,60 --> 00:01:58,40
and Saitama always needs just one punch.

38
00:01:58,40 --> 00:02:01,40
The http post function will cause the request body

39
00:02:01,40 --> 00:02:03,60
to be io.writter.

40
00:02:03,60 --> 00:02:06,00
We're going to use bytes with buffer,

41
00:02:06,00 --> 00:02:09,80
which is an in memory writer or reader.

42
00:02:09,80 --> 00:02:14,60
So we create buffer, we use a JSON encoder with the buffer,

43
00:02:14,60 --> 00:02:18,50
and we encode the job inside the in memory buffer,

44
00:02:18,50 --> 00:02:22,30
and if there is an error, we exit.

45
00:02:22,30 --> 00:02:25,10
And now, we create the http.Post.

46
00:02:25,10 --> 00:02:29,60
We tell it what is the url that we want to send,

47
00:02:29,60 --> 00:02:33,50
we tell it the content type, and it's really important

48
00:02:33,50 --> 00:02:36,80
for a lot of APIs to send the right content type,

49
00:02:36,80 --> 00:02:39,90
and we send the in memory buffer which now will be

50
00:02:39,90 --> 00:02:41,90
an io.reader,

51
00:02:41,90 --> 00:02:44,80
and if there is an error making the post request,

52
00:02:44,80 --> 00:02:49,50
we are going to fail, and again, defer the Body.Close,

53
00:02:49,50 --> 00:02:55,80
and copy the output to standard output.

54
00:02:55,80 --> 00:02:58,20
And we are going to add the necessary import,

55
00:02:58,20 --> 00:03:03,50
the bytes package, encoding/json, and fmt.

56
00:03:03,50 --> 00:03:07,40
Once we save this, we're going to run it again.

57
00:03:07,40 --> 00:03:11,20
Go run httpc.go

58
00:03:11,20 --> 00:03:12,80
And we see that we got the arguments,

59
00:03:12,80 --> 00:03:15,40
we got the data that we sent,

60
00:03:15,40 --> 00:03:19,50
and httpbin also tells us to the data that was sent

61
00:03:19,50 --> 00:03:23,90
is a JSON data, and this is the data.

62
00:03:23,90 --> 00:03:27,70
net::HTTP has much more options for making HTTP calls,

63
00:03:27,70 --> 00:03:30,90
passing parameters, authentication and more.

64
00:03:30,90 --> 00:03:32,10
The documentation is excellent,

65
00:03:32,10 --> 00:03:34,00
and there are many examples online.

