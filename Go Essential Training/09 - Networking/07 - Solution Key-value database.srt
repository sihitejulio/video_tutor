1
00:00:00,60 --> 00:00:02,30
- [Instructor] We first define a database

2
00:00:02,30 --> 00:00:05,00
which is a map from string to the empty interface

3
00:00:05,00 --> 00:00:07,70
and we initialize it with an empty map.

4
00:00:07,70 --> 00:00:10,80
This the second curly braces.

5
00:00:10,80 --> 00:00:11,90
And then we create a lock

6
00:00:11,90 --> 00:00:15,10
which is Mutex which will give exclusive read

7
00:00:15,10 --> 00:00:18,30
or write to the database.

8
00:00:18,30 --> 00:00:20,20
We define an Entry struct.

9
00:00:20,20 --> 00:00:22,80
This will match both the POST and the GET request.

10
00:00:22,80 --> 00:00:24,80
It has a key in the database

11
00:00:24,80 --> 00:00:27,30
and the value which is an empty interface

12
00:00:27,30 --> 00:00:31,60
meaning it can be a string or an integer or a float.

13
00:00:31,60 --> 00:00:34,30
Since both the POST and the GET

14
00:00:34,30 --> 00:00:36,70
are sending back an entry as JSON,

15
00:00:36,70 --> 00:00:38,00
I wrote a utility function

16
00:00:38,00 --> 00:00:42,50
called sendResponse that gets an entry and ReponseWriter.

17
00:00:42,50 --> 00:00:44,60
It sets the content type of the response

18
00:00:44,60 --> 00:00:46,50
to application/json,

19
00:00:46,50 --> 00:00:48,40
create a JSON encoder

20
00:00:48,40 --> 00:00:50,80
and then encodes the entry.

21
00:00:50,80 --> 00:00:55,00
If there is an error, it will log the error.

22
00:00:55,00 --> 00:00:58,30
The PostHandler handles a request for setting

23
00:00:58,30 --> 00:01:01,60
or altering a value in the database.

24
00:01:01,60 --> 00:01:05,30
We make sure that the body is closed with a defer

25
00:01:05,30 --> 00:01:08,00
and then we create a new JSON decoder

26
00:01:08,00 --> 00:01:11,40
and decode the request to the entry.

27
00:01:11,40 --> 00:01:12,20
If there is an error,

28
00:01:12,20 --> 00:01:15,60
it will send an http.Error and return.

29
00:01:15,60 --> 00:01:19,40
Now require the lock so we can safely access the database.

30
00:01:19,40 --> 00:01:21,00
We use a defer to make sure

31
00:01:21,00 --> 00:01:23,40
that we unlock the lock

32
00:01:23,40 --> 00:01:26,60
and not get into a deadlock.

33
00:01:26,60 --> 00:01:30,30
And finally, we set the entry to the new value.

34
00:01:30,30 --> 00:01:31,80
This will create a new entry

35
00:01:31,80 --> 00:01:33,70
if it doesn't exists.

36
00:01:33,70 --> 00:01:38,10
And finally, we encode a response with sendResponse.

37
00:01:38,10 --> 00:01:42,30
The GetHandler will return the value of an existing key.

38
00:01:42,30 --> 00:01:43,90
The URL that it's going to get

39
00:01:43,90 --> 00:01:47,10
is /db/ and then the key.

40
00:01:47,10 --> 00:01:52,60
So, we are using a slice between the leading /db/ prefix.

41
00:01:52,60 --> 00:01:54,40
Then again, we gain the lock

42
00:01:54,40 --> 00:01:58,20
and we defer, we make sure that the lock is released.

43
00:01:58,20 --> 00:01:59,90
We use value, okay

44
00:01:59,90 --> 00:02:02,90
to check if the value is in the database or not.

45
00:02:02,90 --> 00:02:04,30
If it's not in the database,

46
00:02:04,30 --> 00:02:08,90
we send an error with status not found.

47
00:02:08,90 --> 00:02:12,30
Otherwise we encode the response with the key and the value

48
00:02:12,30 --> 00:02:14,80
and we send it to the client.

49
00:02:14,80 --> 00:02:20,50
In main we mount the PostHandler to /db

50
00:02:20,50 --> 00:02:24,60
and we mount the GetHandler to /db/.

51
00:02:24,60 --> 00:02:28,40
The final slash means that everything under /db/

52
00:02:28,40 --> 00:02:32,30
will be matched, so /db/ x, /db/ y,

53
00:02:32,30 --> 00:02:35,90
all of them are going to get matched.

54
00:02:35,90 --> 00:02:41,60
And finally, we start the server on port 8080.

55
00:02:41,60 --> 00:02:43,70
Let's save this and run.

56
00:02:43,70 --> 00:02:47,00
So, go run http.go.

57
00:02:47,00 --> 00:02:50,80
I'm going to use curl to send a request.

58
00:02:50,80 --> 00:02:53,70
So, if you look at the request,

59
00:02:53,70 --> 00:02:56,20
the key is x and the value is one.

60
00:02:56,20 --> 00:03:05,70
So, curl -d@req.json http://localhost:8080/db

61
00:03:05,70 --> 00:03:08,40
and I got the response that the key is x

62
00:03:08,40 --> 00:03:11,00
and the value is one.

63
00:03:11,00 --> 00:03:13,40
Now let's make a GET request.

64
00:03:13,40 --> 00:03:20,90
So, curl httP://localhost:8080/db/x

65
00:03:20,90 --> 00:03:25,30
and we got that the value is one.

66
00:03:25,30 --> 00:03:27,70
If we try to find the nonexisting key,

67
00:03:27,70 --> 00:03:33,50
such as y, we will get the error key y is not found.

68
00:03:33,50 --> 00:03:35,90
That's it, a functioning key value

69
00:03:35,90 --> 00:03:38,90
in memory database in less than 80 lines of code.

70
00:03:38,90 --> 00:03:40,30
This is not a toy server.

71
00:03:40,30 --> 00:03:42,80
Let's load it with the WRK tool.

72
00:03:42,80 --> 00:03:45,80
The WRK tool need to be installed on your system

73
00:03:45,80 --> 00:03:47,10
in order to work.

74
00:03:47,10 --> 00:03:57,00
So wrk http://localhost:8080/db/x

75
00:03:57,00 --> 00:04:00,40
and we are doing about 43,000 requests per second.

76
00:04:00,40 --> 00:04:01,00
Not bad.

