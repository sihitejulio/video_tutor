1
00:00:00,70 --> 00:00:02,20
- [Instructor] Write an HTTP server

2
00:00:02,20 --> 00:00:05,50
that functions as a key/value database.

3
00:00:05,50 --> 00:00:08,50
Users will make a POST request to /db

4
00:00:08,50 --> 00:00:11,80
to create or assign a value.

5
00:00:11,80 --> 00:00:17,10
User will make a GET request /db/<key> to get the value.

6
00:00:17,10 --> 00:00:21,80
Both request and response will be in JSON format.

7
00:00:21,80 --> 00:00:27,10
Use a map from string to the empty interface to hold values.

8
00:00:27,10 --> 00:00:29,60
And since we can't access the Go data structure

9
00:00:29,60 --> 00:00:31,40
from two different Go routines,

10
00:00:31,40 --> 00:00:35,50
limit the access to the map with a sync.Mutex.

11
00:00:35,50 --> 00:00:38,50
You can see an example of a POST body below.

12
00:00:38,50 --> 00:00:41,00
The key is x, and the value is 1.

