1
00:00:00,70 --> 00:00:01,50
- [Instructor] When you're working

2
00:00:01,50 --> 00:00:04,70
in a distributed environment usually over the network

3
00:00:04,70 --> 00:00:07,30
you send and receive messages.

4
00:00:07,30 --> 00:00:10,30
Messages can be encoded in various formats

5
00:00:10,30 --> 00:00:13,50
such as json, messages pack, particle buffer, XML

6
00:00:13,50 --> 00:00:14,70
and more.

7
00:00:14,70 --> 00:00:18,20
Json is a good default since it's widely used

8
00:00:18,20 --> 00:00:21,10
in many languages and services supported.

9
00:00:21,10 --> 00:00:23,70
Json can also easily be passed from the browser

10
00:00:23,70 --> 00:00:27,90
making it the preferred encoding is rest APIs.

11
00:00:27,90 --> 00:00:31,80
Go comes with a built in encoding json library.

12
00:00:31,80 --> 00:00:35,40
We can either encode the code to IO reader, IO writer

13
00:00:35,40 --> 00:00:38,50
or work with byte slices.

14
00:00:38,50 --> 00:00:40,40
Say you start an online bank,

15
00:00:40,40 --> 00:00:43,30
you get request with withdraw deposit money

16
00:00:43,30 --> 00:00:45,40
and you need to pass them and handle them.

17
00:00:45,40 --> 00:00:50,10
Here's an example, so we import encoding.json,

18
00:00:50,10 --> 00:00:54,50
we have an example of a request, the user is Scrooge McDuck,

19
00:00:54,50 --> 00:00:59,70
he's doing a deposit of $1,000,000.30.

20
00:00:59,70 --> 00:01:03,50
We create a request with the final request structure

21
00:01:03,50 --> 00:01:06,00
which mimics the the json object.

22
00:01:06,00 --> 00:01:09,70
All the fields must start with an uppercase.

23
00:01:09,70 --> 00:01:14,30
So we have login string and then we use a field tag

24
00:01:14,30 --> 00:01:20,50
to say to json that in json the name of this field is user.

25
00:01:20,50 --> 00:01:23,30
Then we have the type and we say to the json

26
00:01:23,30 --> 00:01:25,90
this is a lower case type in the json.

27
00:01:25,90 --> 00:01:29,10
And again the amount which is the lowercase amount

28
00:01:29,10 --> 00:01:31,60
in the json.

29
00:01:31,60 --> 00:01:34,70
In our main program we create a new

30
00:01:34,70 --> 00:01:38,00
buffered string from bytes.

31
00:01:38,00 --> 00:01:39,40
This create an eye reader

32
00:01:39,40 --> 00:01:42,20
that we can use with the json decoder.

33
00:01:42,20 --> 00:01:45,90
We create a new json decoder with a reader,

34
00:01:45,90 --> 00:01:50,40
create an empty request and then ask the decoder

35
00:01:50,40 --> 00:01:52,30
to decode the request.

36
00:01:52,30 --> 00:01:57,30
And if the decode resulted in an error we use log.Fatalf

37
00:01:57,30 --> 00:02:00,10
to print error and abort the program.

38
00:02:00,10 --> 00:02:03,50
Otherwise we print what we got.

39
00:02:03,50 --> 00:02:05,60
Let's save it and write.

40
00:02:05,60 --> 00:02:08,80
Go run json.go.

41
00:02:08,80 --> 00:02:12,60
And we see that we got login Scrooge McDuck, type deposit

42
00:02:12,60 --> 00:02:16,20
and the amount which is 1,000,000.30.

43
00:02:16,20 --> 00:02:18,10
After you've processed the transaction

44
00:02:18,10 --> 00:02:20,40
we'd like to respond with json as well.

45
00:02:20,40 --> 00:02:24,50
This time instead of struct, we're going to use amount.

46
00:02:24,50 --> 00:02:27,60
So we're going to create a response.

47
00:02:27,60 --> 00:02:31,10
We say that the previous balance was 8 1/2 million

48
00:02:31,10 --> 00:02:33,10
that was loaded from a database

49
00:02:33,10 --> 00:02:36,10
and then you say the response is amount.

50
00:02:36,10 --> 00:02:40,10
Where the keys are strings and the values are what is known

51
00:02:40,10 --> 00:02:43,00
in Go as the empty interface.

52
00:02:43,00 --> 00:02:45,30
Because values can be of different type.

53
00:02:45,30 --> 00:02:49,00
In our case it's going to be a bullion N flow,

54
00:02:49,00 --> 00:02:53,60
we use the empty interface which in Go means any type.

55
00:02:53,60 --> 00:02:58,00
And now we create a new encoder for the standard output

56
00:02:58,00 --> 00:03:02,50
just to print it out and we encode the response.

57
00:03:02,50 --> 00:03:06,00
And of course if there is an error in the encoding,

58
00:03:06,00 --> 00:03:10,30
we're going to use log.fatalf to print it out.

59
00:03:10,30 --> 00:03:13,50
Don't forget to import the OS package.

60
00:03:13,50 --> 00:03:18,40
Let's run this; go run json.go.

61
00:03:18,40 --> 00:03:23,30
And we see that we got, balance $9,500,000.30

62
00:03:23,30 --> 00:03:25,00
and okay is true.

