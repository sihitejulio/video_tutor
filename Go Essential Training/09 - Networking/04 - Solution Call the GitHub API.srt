1
00:00:03,70 --> 00:00:05,10
- [Programmer] Welcome back.

2
00:00:05,10 --> 00:00:06,70
Here's the User struct.

3
00:00:06,70 --> 00:00:10,20
It has a name, which is a string, and using a tag,

4
00:00:10,20 --> 00:00:14,20
we tell json that it's going to be name in lower case.

5
00:00:14,20 --> 00:00:16,90
It has PublicRepos, which is an integer,

6
00:00:16,90 --> 00:00:20,20
and again, using a tag, we tell json, it's going to be

7
00:00:20,20 --> 00:00:22,50
public underscore repos.

8
00:00:22,50 --> 00:00:24,10
And here's the function.

9
00:00:24,10 --> 00:00:26,30
User info gets a login, and returns

10
00:00:26,30 --> 00:00:29,00
a point it to user and an error.

11
00:00:29,00 --> 00:00:30,80
First we construct the URL.

12
00:00:30,80 --> 00:00:35,70
Use fmt.Sprintf to create the full URL

13
00:00:35,70 --> 00:00:37,60
to the user.

14
00:00:37,60 --> 00:00:41,60
Then we make an http code, getting a response and an error.

15
00:00:41,60 --> 00:00:44,50
If we got an error, we return nil for the user

16
00:00:44,50 --> 00:00:46,10
and the error.

17
00:00:46,10 --> 00:00:50,50
Using defer, we make sure that the response body is closed.

18
00:00:50,50 --> 00:00:52,70
And now we're going to decode the json.

19
00:00:52,70 --> 00:00:55,80
We create an empty user object.

20
00:00:55,80 --> 00:01:01,60
And then we create a json decoder over the response body.

21
00:01:01,60 --> 00:01:04,40
Now we decode into the user

22
00:01:04,40 --> 00:01:06,20
the data from the json, checking

23
00:01:06,20 --> 00:01:09,60
if there was an error returning it, otherwise,

24
00:01:09,60 --> 00:01:13,80
we return the user and nil for an error.

25
00:01:13,80 --> 00:01:18,00
In the main, we call user info with tebeka, which is

26
00:01:18,00 --> 00:01:21,70
my gate hub user, and we get back a user and an error.

27
00:01:21,70 --> 00:01:26,90
If there is an error, use log.Fatalf to exit the program.

28
00:01:26,90 --> 00:01:30,90
Otherwise, just print out the user that we got.

29
00:01:30,90 --> 00:01:32,90
Let's save it, and run it.

30
00:01:32,90 --> 00:01:35,10
Go run

31
00:01:35,10 --> 00:01:38,10
github api.

32
00:01:38,10 --> 00:01:42,00
And we got Miki Tebeka and 104 public repositories.

