1
00:00:00,70 --> 00:00:02,70
- [Tutor] Say you are writing a geometry package.

2
00:00:02,70 --> 00:00:04,30
You have some shapes defined,

3
00:00:04,30 --> 00:00:08,70
like square and circle.

4
00:00:08,70 --> 00:00:10,40
Both the square and the circle,

5
00:00:10,40 --> 00:00:13,30
we find an area method.

6
00:00:13,30 --> 00:00:14,90
Now we'd like to write the function

7
00:00:14,90 --> 00:00:16,60
that receives a slice of shapes

8
00:00:16,60 --> 00:00:18,50
and return the sum of the areas.

9
00:00:18,50 --> 00:00:23,40
This function will look something like this.

10
00:00:23,40 --> 00:00:26,30
Some areas get something,

11
00:00:26,30 --> 00:00:29,20
which is a slice of shapes

12
00:00:29,20 --> 00:00:31,40
and returns a float64.

13
00:00:31,40 --> 00:00:33,60
Inside, we create a total,

14
00:00:33,60 --> 00:00:35,60
and then for every shape,

15
00:00:35,60 --> 00:00:38,50
we added the shape area to the total

16
00:00:38,50 --> 00:00:40,60
and returned a total.

17
00:00:40,60 --> 00:00:42,70
How can you do that?

18
00:00:42,70 --> 00:00:46,60
You need to define something of pipe has an area

19
00:00:46,60 --> 00:00:50,50
and that both square and circle will be of this type.

20
00:00:50,50 --> 00:00:51,80
So what is shape?

21
00:00:51,80 --> 00:00:53,60
It's an interface.

22
00:00:53,60 --> 00:00:55,30
Let's have a look.

23
00:00:55,30 --> 00:00:59,90
An interface is a collection of methods, and each type,

24
00:00:59,90 --> 00:01:02,70
it implements all the methods in the interface.

25
00:01:02,70 --> 00:01:04,90
You're satisfying the interface.

26
00:01:04,90 --> 00:01:08,20
A method must match in name and also in signature,

27
00:01:08,20 --> 00:01:10,30
parameters, and return value.

28
00:01:10,30 --> 00:01:13,90
In our case, the interface is just one method,

29
00:01:13,90 --> 00:01:18,30
which is area without arguments and returning float64,

30
00:01:18,30 --> 00:01:24,50
and both circle and square satisfy this interface.

31
00:01:24,50 --> 00:01:27,20
Here's how we can use it.

32
00:01:27,20 --> 00:01:31,10
We create a square

33
00:01:31,10 --> 00:01:33,70
and we create a circle.

34
00:01:33,70 --> 00:01:37,10
And now, we create a slice of shapes

35
00:01:37,10 --> 00:01:40,70
that holds both the circle and the square.

36
00:01:40,70 --> 00:01:45,20
And then we can call sumAreas on the slice of shapes

37
00:01:45,20 --> 00:01:49,20
and print out the total area.

38
00:01:49,20 --> 00:01:51,50
Let's save this and run.

39
00:01:51,50 --> 00:01:55,10
Go run shapes.go.

40
00:01:55,10 --> 00:01:58,40
And we see that we have the areas of the circle

41
00:01:58,40 --> 00:01:59,90
and the square printed,

42
00:01:59,90 --> 00:02:04,00
and the total is the sum of these two.

43
00:02:04,00 --> 00:02:06,70
Interfaces are a very useful abstraction

44
00:02:06,70 --> 00:02:09,40
and you'll see them a lot.

45
00:02:09,40 --> 00:02:12,30
For example, the io.reader and io.Writer

46
00:02:12,30 --> 00:02:13,70
from the I/O package,

47
00:02:13,70 --> 00:02:15,40
define something you can read from

48
00:02:15,40 --> 00:02:17,70
and something you can write to.

49
00:02:17,70 --> 00:02:20,50
With these interfaces, you can use the same code

50
00:02:20,50 --> 00:02:23,10
to read from files, socket, compressed files,

51
00:02:23,10 --> 00:02:26,00
hash signatures, and more.

