1
00:00:00,70 --> 00:00:03,10
- [Instructor] Structs are nice for organizing data,

2
00:00:03,10 --> 00:00:05,10
but they have more power.

3
00:00:05,10 --> 00:00:07,90
We can define methods on structs.

4
00:00:07,90 --> 00:00:12,80
Assume that we have a Trade struct.

5
00:00:12,80 --> 00:00:15,60
We can now define a value method on it

6
00:00:15,60 --> 00:00:19,00
that will return how much money this trade is worth.

7
00:00:19,00 --> 00:00:22,10
We define a method, again with the func, but unlike

8
00:00:22,10 --> 00:00:25,70
other method, we have the receiver,

9
00:00:25,70 --> 00:00:28,30
which is a pointer to a Trade object.

10
00:00:28,30 --> 00:00:31,20
And then we have the method name.

11
00:00:31,20 --> 00:00:35,10
And last, we have the return type.

12
00:00:35,10 --> 00:00:41,10
And the value is the trade.Volume times the trade.Price.

13
00:00:41,10 --> 00:00:43,70
And since Go type system is strict, we need to convert

14
00:00:43,70 --> 00:00:47,30
the value to float64.

15
00:00:47,30 --> 00:00:49,20
Then, if it's a Buy trade,

16
00:00:49,20 --> 00:00:50,50
we need to reverse the value,

17
00:00:50,50 --> 00:00:52,20
because we spent money.

18
00:00:52,20 --> 00:00:55,50
And at the end, we return the value.

19
00:00:55,50 --> 00:00:57,30
Now let's use it.

20
00:00:57,30 --> 00:01:00,90
In line 27, we create a Trade object.

21
00:01:00,90 --> 00:01:06,20
And then in line 33, we print out the value of this trade.

22
00:01:06,20 --> 00:01:07,00
Let's have a look.

23
00:01:07,00 --> 00:01:09,20
Go run

24
00:01:09,20 --> 00:01:11,60
trade dot go.

25
00:01:11,60 --> 00:01:14,90
And this is the value of the trade.

26
00:01:14,90 --> 00:01:17,30
The value method got the pointer

27
00:01:17,30 --> 00:01:18,90
to a trade as a receiver.

28
00:01:18,90 --> 00:01:21,50
You'll usually use a pointer receiver.

29
00:01:21,50 --> 00:01:25,90
Let's see why.

30
00:01:25,90 --> 00:01:29,80
I've opened point.go from the exercise files.

31
00:01:29,80 --> 00:01:34,00
Assume we define a point which has an X and a Y.

32
00:01:34,00 --> 00:01:38,20
And then, we define a function for moving the point

33
00:01:38,20 --> 00:01:41,00
with delta X and delta Y.

34
00:01:41,00 --> 00:01:45,20
And at main, we create a point, and we move it.

35
00:01:45,20 --> 00:01:48,10
What do you think will be printed out when we try it out?

36
00:01:48,10 --> 00:01:51,90
So let's save it, and run it.

37
00:01:51,90 --> 00:01:53,20
Go run

38
00:01:53,20 --> 00:01:54,90
point dot go.

39
00:01:54,90 --> 00:01:58,60
And we got X is one, and Y is two,

40
00:01:58,60 --> 00:02:00,90
even though we expected X to be one plus two,

41
00:02:00,90 --> 00:02:04,20
which is three, and Y to be five.

42
00:02:04,20 --> 00:02:05,10
Why is that?

43
00:02:05,10 --> 00:02:08,20
The reason is that you're not using a pointer receiver,

44
00:02:08,20 --> 00:02:11,20
which means your move method will get a copy

45
00:02:11,20 --> 00:02:12,30
of the point struct.

46
00:02:12,30 --> 00:02:16,10
This is why you'll usually use the pointer receiver.

47
00:02:16,10 --> 00:02:20,00
Let's change move to accept the pointer to a point and see.

48
00:02:20,00 --> 00:02:24,20
I'm going to have it get a pointer to a point.

49
00:02:24,20 --> 00:02:25,90
And it main now,

50
00:02:25,90 --> 00:02:30,60
I'm going to make p a pointer to a point.

51
00:02:30,60 --> 00:02:35,40
Let's save it, and run it.

52
00:02:35,40 --> 00:02:38,00
And this time, we get the expected values.

