1
00:00:00,60 --> 00:00:02,50
- [Instructor] io.Writer has only one method

2
00:00:02,50 --> 00:00:04,70
you need to implement, write.

3
00:00:04,70 --> 00:00:06,80
If you find a lowercase letter,

4
00:00:06,80 --> 00:00:08,50
you'll need to difference between

5
00:00:08,50 --> 00:00:11,40
lower case a and upper case a.

6
00:00:11,40 --> 00:00:14,00
In code, characters are called roons.

7
00:00:14,00 --> 00:00:16,90
We need to convert a difference to a byte.

8
00:00:16,90 --> 00:00:21,90
So we said diff is a byte

9
00:00:21,90 --> 00:00:25,00
of lower case a minus uppercase A.

10
00:00:25,00 --> 00:00:27,00
We create an alternate buffer

11
00:00:27,00 --> 00:00:30,30
using the built in make function.

12
00:00:30,30 --> 00:00:33,50
We tell it the type, which is a slice of bytes

13
00:00:33,50 --> 00:00:38,50
and the length and how many we bytes we want it to be.

14
00:00:38,50 --> 00:00:45,00
And now, in line 18, we iterate of the original slice

15
00:00:45,00 --> 00:00:49,20
and every time that we encounter the lowercase character

16
00:00:49,20 --> 00:00:51,70
which is bigger or equal to a

17
00:00:51,70 --> 00:00:54,10
or smaller and equal to z,

18
00:00:54,10 --> 00:00:57,00
we subtract the difference from c.

19
00:00:57,00 --> 00:01:01,50
And then we put the new letter inside out.

20
00:01:01,50 --> 00:01:03,40
At the end, we use the write method

21
00:01:03,40 --> 00:01:05,00
from the underlined write.

22
00:01:05,00 --> 00:01:08,20
And return the number of bytes written and the error.

23
00:01:08,20 --> 00:01:10,80
Let's see how we can use it.

24
00:01:10,80 --> 00:01:15,60
In line 29, we create a capper and we us os.Stdout.

25
00:01:15,60 --> 00:01:19,40
The standard output as the underlined writer.

26
00:01:19,40 --> 00:01:22,50
And since capper is implementing io.Writer,

27
00:01:22,50 --> 00:01:25,60
we can use the methods from the fnt package with it.

28
00:01:25,60 --> 00:01:31,70
In this case, we use Fprintln into c Hello There.

29
00:01:31,70 --> 00:01:34,20
If we say this and run,

30
00:01:34,20 --> 00:01:39,00
go run capper.go, we'll see upper case hello there.

