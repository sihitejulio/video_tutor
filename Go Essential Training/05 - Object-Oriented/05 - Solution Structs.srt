1
00:00:00,10 --> 00:00:01,70
- [Instructor] Welcome back.

2
00:00:01,70 --> 00:00:04,40
We define type Square struct,

3
00:00:04,40 --> 00:00:07,00
which is a center, which is a point,

4
00:00:07,00 --> 00:00:10,20
and length, which is an integer.

5
00:00:10,20 --> 00:00:14,00
To create a square, we create NewSquare function.

6
00:00:14,00 --> 00:00:17,70
We get x and y as the center and the length.

7
00:00:17,70 --> 00:00:20,30
We check that the length is a positive number,

8
00:00:20,30 --> 00:00:22,60
otherwise we error.

9
00:00:22,60 --> 00:00:26,00
We create a square, assigning the center

10
00:00:26,00 --> 00:00:30,40
to a new point, with x and y, and the length.

11
00:00:30,40 --> 00:00:32,00
And return the NewSquare,

12
00:00:32,00 --> 00:00:35,30
and nil is signifying there was no error.

13
00:00:35,30 --> 00:00:38,80
When we move the square, we just pass along the data

14
00:00:38,80 --> 00:00:41,00
to the move method of the center,

15
00:00:41,00 --> 00:00:43,10
which is of type Point.

16
00:00:43,10 --> 00:00:44,70
To calculate the Area of the square,

17
00:00:44,70 --> 00:00:47,40
we just return the length times f.

18
00:00:47,40 --> 00:00:48,50
And now we can use it.

19
00:00:48,50 --> 00:00:51,70
We create a NewSquare with the NewSquare function.

20
00:00:51,70 --> 00:00:54,20
And if there is an error, use Fatalf

21
00:00:54,20 --> 00:00:55,80
from the log package.

22
00:00:55,80 --> 00:00:59,40
Fatalf will print an error message and exit the program

23
00:00:59,40 --> 00:01:01,00
with a non-zero value.

24
00:01:01,00 --> 00:01:02,40
It is very handy.

25
00:01:02,40 --> 00:01:04,30
Then we move the square.

26
00:01:04,30 --> 00:01:07,70
And at the end, we print out the square itself

27
00:01:07,70 --> 00:01:11,10
and we print out the area of the square.

28
00:01:11,10 --> 00:01:17,00
Let's save this and run it.

29
00:01:17,00 --> 00:01:19,40
As we see that the +v verb in the Printf

30
00:01:19,40 --> 00:01:21,80
has printed the X and the Y from the center,

31
00:01:21,80 --> 00:01:22,70
and the length.

32
00:01:22,70 --> 00:01:25,70
So we can see everything inside the square.

33
00:01:25,70 --> 00:01:29,00
And we printed out the area, which is 100.

