1
00:00:00,10 --> 00:00:01,50
- [Instructor] If you're coming from

2
00:00:01,50 --> 00:00:02,90
an object-oriented language,

3
00:00:02,90 --> 00:00:05,50
such as Python, Java, C++, and others,

4
00:00:05,50 --> 00:00:09,40
you are used to having a constructor or initializer method

5
00:00:09,40 --> 00:00:12,50
that is called when an object is created.

6
00:00:12,50 --> 00:00:16,10
In Go, you write a function, usually starting with New,

7
00:00:16,10 --> 00:00:18,10
that returns a new object.

8
00:00:18,10 --> 00:00:19,90
This New function usually returns

9
00:00:19,90 --> 00:00:21,90
the pointer to the created object,

10
00:00:21,90 --> 00:00:24,50
and optionally, an error value,

11
00:00:24,50 --> 00:00:27,90
If it's possible there was an error creating the object.

12
00:00:27,90 --> 00:00:33,70
We have our Trade struct here, and now we define NewTrade.

13
00:00:33,70 --> 00:00:36,10
That gets the symbol, the volume, the price,

14
00:00:36,10 --> 00:00:38,80
and the float, and returns at the end,

15
00:00:38,80 --> 00:00:45,20
a pointer to a trade, and possible error value.

16
00:00:45,20 --> 00:00:46,70
And here we validate the data.

17
00:00:46,70 --> 00:00:50,40
If the symbol is empty we return nil for no object

18
00:00:50,40 --> 00:00:53,80
and an arrow that the symbol can not be empty.

19
00:00:53,80 --> 00:00:56,20
We also check the volume, that it's not negative

20
00:00:56,20 --> 00:00:59,00
and we check the price, that it's not negative.

21
00:00:59,00 --> 00:01:01,20
And once we've validated the input,

22
00:01:01,20 --> 00:01:06,70
we create a pointer to a trade object,

23
00:01:06,70 --> 00:01:10,60
and at the end, we return the trade object,

24
00:01:10,60 --> 00:01:14,80
and nil, signifying no error.

25
00:01:14,80 --> 00:01:16,70
If you're coming from Zero, C++,

26
00:01:16,70 --> 00:01:19,20
you might cringe at returning the pointer

27
00:01:19,20 --> 00:01:22,20
from an object allocated inside the function.

28
00:01:22,20 --> 00:01:25,10
Don't worry, Go's compiler doesn't escape analysis

29
00:01:25,10 --> 00:01:28,30
and will know to allocate trade on the heap.

30
00:01:28,30 --> 00:01:31,20
And now, we can use NewTrade in our code.

31
00:01:31,20 --> 00:01:35,60
In line 51, we create a trade, with NewTrade,

32
00:01:35,60 --> 00:01:39,30
and then we check if there was any error creating the trade,

33
00:01:39,30 --> 00:01:42,80
we print out the error and we exit the program,

34
00:01:42,80 --> 00:01:44,20
with a non zero value.

35
00:01:44,20 --> 00:01:48,50
Otherwise, we print the value of the trade,

36
00:01:48,50 --> 00:01:53,00
let's save it, and run, go run trade.go

