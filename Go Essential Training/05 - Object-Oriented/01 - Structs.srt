1
00:00:00,60 --> 00:00:01,60
- [Instructor] You can do a lot

2
00:00:01,60 --> 00:00:03,60
with the built in pipes in Go.

3
00:00:03,60 --> 00:00:05,20
But there are times you'd like to define

4
00:00:05,20 --> 00:00:07,20
your own data structures.

5
00:00:07,20 --> 00:00:09,80
In Go you'll use Struct to combine

6
00:00:09,80 --> 00:00:12,80
several fields into a datatype.

7
00:00:12,80 --> 00:00:14,40
Let's see an example.

8
00:00:14,40 --> 00:00:16,00
Assuming you're writing an algorithm

9
00:00:16,00 --> 00:00:17,70
to trade the stock market.

10
00:00:17,70 --> 00:00:21,90
Every time you buy or sell you keep a record of this trade.

11
00:00:21,90 --> 00:00:25,00
We define a new type which is a struct

12
00:00:25,00 --> 00:00:27,50
and it's called Trade

13
00:00:27,50 --> 00:00:30,30
and then we list the fields with their type.

14
00:00:30,30 --> 00:00:32,80
We have the symbol which is a string.

15
00:00:32,80 --> 00:00:35,30
We have the volume of the number of stocks

16
00:00:35,30 --> 00:00:37,30
which is an Integer.

17
00:00:37,30 --> 00:00:39,90
We have the price which is a float

18
00:00:39,90 --> 00:00:45,70
and the flag saying whether it was a buy or a sell trade.

19
00:00:45,70 --> 00:00:49,00
And finally a closing brackets.

20
00:00:49,00 --> 00:00:52,90
And now we can use this type.

21
00:00:52,90 --> 00:00:58,10
Here we create a Trade object and we assign by position

22
00:00:58,10 --> 00:01:00,40
the Symbol, the Volume, the Price

23
00:01:00,40 --> 00:01:03,50
and whether it was a Buy or a Sell trade

24
00:01:03,50 --> 00:01:05,70
and we can print it out.

25
00:01:05,70 --> 00:01:09,30
Sometimes, we'd like better view of the object

26
00:01:09,30 --> 00:01:12,90
and we can use the plus v verb in Printf

27
00:01:12,90 --> 00:01:18,20
to get a better description of how the object looks like.

28
00:01:18,20 --> 00:01:21,40
To access an individual field, you use the dot

29
00:01:21,40 --> 00:01:24,40
like the t1.Symbol.

30
00:01:24,40 --> 00:01:25,90
Another option to create trades

31
00:01:25,90 --> 00:01:28,00
is by specifying the field names.

32
00:01:28,00 --> 00:01:32,70
Here we create a trade and we say that the Symbol is MSFT,

33
00:01:32,70 --> 00:01:35,90
the Volume, the Price and the Buy.

34
00:01:35,90 --> 00:01:37,60
If you specify the field names,

35
00:01:37,60 --> 00:01:40,70
you don't have to follow the order of declaration.

36
00:01:40,70 --> 00:01:44,60
And last, you can omit some or all of the field names.

37
00:01:44,60 --> 00:01:49,10
Go will assign the zero value for every field.

38
00:01:49,10 --> 00:01:52,70
If you're coming from languages such as Java or C++,

39
00:01:52,70 --> 00:01:54,70
you might wonder if you can have private

40
00:01:54,70 --> 00:01:57,60
or protective fields which are not exposed

41
00:01:57,60 --> 00:01:59,90
outside of your package.

42
00:01:59,90 --> 00:02:01,60
In Go, this is very simple.

43
00:02:01,60 --> 00:02:04,00
Everything that starts with an uppercase letter,

44
00:02:04,00 --> 00:02:06,70
is accessible from other packages.

45
00:02:06,70 --> 00:02:09,40
Otherwise, it's accessible only from within

46
00:02:09,40 --> 00:02:11,10
the current package.

47
00:02:11,10 --> 00:02:13,50
Let's save this and run.

48
00:02:13,50 --> 00:02:16,50
Go run trade.go.

49
00:02:16,50 --> 00:02:19,90
The first print is from the Print

50
00:02:19,90 --> 00:02:23,40
and the second one is from the plus V verb

51
00:02:23,40 --> 00:02:27,00
which states also the field name and not just the value.

