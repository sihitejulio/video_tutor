1
00:00:03,60 --> 00:00:07,90
- In this exercise, we will implement io.Writer interface.

2
00:00:07,90 --> 00:00:10,10
Write a struct called Capper

3
00:00:10,10 --> 00:00:12,60
that has a field to another io.Writer

4
00:00:12,60 --> 00:00:16,00
and transforms everything written to uppercase.

5
00:00:16,00 --> 00:00:18,50
Capper should implement io.Writer.

6
00:00:18,50 --> 00:00:21,00
The definition of the struct is shown,

7
00:00:21,00 --> 00:00:24,00
and also the signature of the write method.

