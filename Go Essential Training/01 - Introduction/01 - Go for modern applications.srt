1
00:00:01,00 --> 00:00:02,30
- [Instructor] Go is becoming one

2
00:00:02,30 --> 00:00:05,00
of the most dominant programming languages.

3
00:00:05,00 --> 00:00:08,70
Companies like Google, Netflix, Amazon, and others,

4
00:00:08,70 --> 00:00:12,00
are leveraging Go's ability to handle concurrency

5
00:00:12,00 --> 00:00:14,70
and connectivity in a simple and elegant way,

6
00:00:14,70 --> 00:00:18,10
and exciting projects like Docker and Kubernetes,

7
00:00:18,10 --> 00:00:20,40
are being written in Go.

8
00:00:20,40 --> 00:00:22,10
Go has many additional features

9
00:00:22,10 --> 00:00:24,60
that will help you develop modern applications,

10
00:00:24,60 --> 00:00:26,90
and it can also save you money.

11
00:00:26,90 --> 00:00:28,80
For example, by switching to Go,

12
00:00:28,80 --> 00:00:34,50
the company iron.io went from 30 servers down to just two.

13
00:00:34,50 --> 00:00:36,20
In my LinkedIn Learning course,

14
00:00:36,20 --> 00:00:39,30
I'll share my years of experience in developing in Go,

15
00:00:39,30 --> 00:00:42,60
I'll teach you what you need to know and get up and running.

16
00:00:42,60 --> 00:00:44,80
We'll start with the basics and progress through

17
00:00:44,80 --> 00:00:46,50
concurrency and networking,

18
00:00:46,50 --> 00:00:50,00
and finally, we'll write a highly concurrent server.

19
00:00:50,00 --> 00:00:53,20
So join me, Miki Tebeka, in my course,

20
00:00:53,20 --> 00:00:54,40
Go Essential Training.

21
00:00:54,40 --> 00:00:56,00
Let's get hacking.

