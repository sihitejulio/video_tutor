1
00:00:00,60 --> 00:00:02,70
- [Instructor] To get most out of this course,

2
00:00:02,70 --> 00:00:04,80
it would be helpful to have some experience

3
00:00:04,80 --> 00:00:06,80
with programming and the command line.

4
00:00:06,80 --> 00:00:10,20
This course is designed for beginner Go programmers.

5
00:00:10,20 --> 00:00:13,20
I will teach you everything you need to know to get started.

6
00:00:13,20 --> 00:00:15,50
I highly recommend that you follow along with me

7
00:00:15,50 --> 00:00:18,30
and write code as we progress through the course.

8
00:00:18,30 --> 00:00:19,60
There's some magic happening

9
00:00:19,60 --> 00:00:21,20
between the fingers and the brain

10
00:00:21,20 --> 00:00:24,10
that make learning much more effective.

11
00:00:24,10 --> 00:00:26,00
You'll need the following.

12
00:00:26,00 --> 00:00:28,90
A working installation of the Go toolchain.

13
00:00:28,90 --> 00:00:34,30
Head over to golang.org, click on Download Go

14
00:00:34,30 --> 00:00:38,40
and select the installer for your operating system.

15
00:00:38,40 --> 00:00:39,90
You'll also need Git.

16
00:00:39,90 --> 00:00:42,80
We're not going to work with Git but Go uses Git

17
00:00:42,80 --> 00:00:45,20
to install third-party packages.

18
00:00:45,20 --> 00:00:46,80
If you're on a Mac or Linux,

19
00:00:46,80 --> 00:00:49,30
chances are you already have Git.

20
00:00:49,30 --> 00:00:53,80
If you're on Windows, head over to git-scm.com

21
00:00:53,80 --> 00:00:55,90
to the Downloads section

22
00:00:55,90 --> 00:00:59,60
and click for the downloader for Windows.

23
00:00:59,60 --> 00:01:02,10
You will also need a text editor.

24
00:01:02,10 --> 00:01:05,80
I'm going to use Vim because it has a nice, clean interface,

25
00:01:05,80 --> 00:01:09,00
which allows us to focus on the code itself.

26
00:01:09,00 --> 00:01:13,40
But feel free to use any editor or ID that you would like.

27
00:01:13,40 --> 00:01:16,60
To make sure the installation works, open a command prompt

28
00:01:16,60 --> 00:01:19,40
and type go version.

29
00:01:19,40 --> 00:01:22,10
If all went well, you should see something like

30
00:01:22,10 --> 00:01:26,30
go version go 111 and then the operating system

31
00:01:26,30 --> 00:01:28,60
and the architecture.

32
00:01:28,60 --> 00:01:31,40
Last, you should extract the exercise files

33
00:01:31,40 --> 00:01:34,00
into a directory of your choice.

