1
00:00:00,60 --> 00:00:04,00
- The Go Get tool installs one dependency at a time. And it

2
00:00:04,00 --> 00:00:07,50
installs the latest version of the package.

3
00:00:07,50 --> 00:00:10,50
When working as a team, we'd like a way to document

4
00:00:10,50 --> 00:00:13,50
dependencies and their versions.

5
00:00:13,50 --> 00:00:16,40
Using a specific version of the package will also protect

6
00:00:16,40 --> 00:00:19,90
you from breaking changes the package might have.

7
00:00:19,90 --> 00:00:23,60
In Go 111, we got a new mod tool to handle

8
00:00:23,60 --> 00:00:27,40
dependency management. It is still considered an

9
00:00:27,40 --> 00:00:30,50
experimental tool but is the evolution of several

10
00:00:30,50 --> 00:00:34,10
dependency management tools for Go and is the new official

11
00:00:34,10 --> 00:00:37,00
tool from the Go team.

12
00:00:37,00 --> 00:00:41,80
Let's say we'd like to work with toml in our code.

13
00:00:41,80 --> 00:00:45,50
To start, we need to create a module.

14
00:00:45,50 --> 00:00:49,00
We'll run go, (typing) mod, (typing) init (typing)

15
00:00:49,00 --> 00:00:51,80
and will give it the name of the package.

16
00:00:51,80 --> 00:00:55,00
And Go will tell us we created the new Go dot modifier.

17
00:00:55,00 --> 00:00:57,00
Let's have a look at this file.

18
00:00:57,00 --> 00:01:00,10
(typing)

19
00:01:00,10 --> 00:01:04,10
This file currently just have the name of the module.

20
00:01:04,10 --> 00:01:07,90
Let's add the requirements. We're adding a requirement

21
00:01:07,90 --> 00:01:11,60
so we say require, we say the name of the package, which

22
00:01:11,60 --> 00:01:15,60
is the go-toml package, and we say the version

23
00:01:15,60 --> 00:01:17,30
of the package.

24
00:01:17,30 --> 00:01:19,80
(typing)

25
00:01:19,80 --> 00:01:23,60
Let's save this file, and now let's run the program,

26
00:01:23,60 --> 00:01:27,60
Go run cfg.go. And we see that Go is finding the

27
00:01:27,60 --> 00:01:32,40
dependencies and then running our program. We can also see

28
00:01:32,40 --> 00:01:37,90
that the new file is generated, called Go.sum.

29
00:01:37,90 --> 00:01:41,10
You can think of Go.mod as the packages you want and

30
00:01:41,10 --> 00:01:44,90
Go.sum as the packages that you actually require.

31
00:01:44,90 --> 00:01:47,40
Once you want to upgrade the package, or add a new

32
00:01:47,40 --> 00:01:53,10
dependency, edit (typing) Go.mod with a new dependency

33
00:01:53,10 --> 00:01:57,00
of the new version. Don't forget to run your tests

34
00:01:57,00 --> 00:01:59,00
after upgrading to a new version.

