1
00:00:00,60 --> 00:00:02,80
- [Instructor] Here we have our csv file.

2
00:00:02,80 --> 00:00:06,50
It has the value and the expected result

3
00:00:06,50 --> 00:00:08,30
in every line.

4
00:00:08,30 --> 00:00:11,00
Let's have a look at the code.

5
00:00:11,00 --> 00:00:13,30
Because we are working with csv files,

6
00:00:13,30 --> 00:00:16,40
we are going to use encoding.csv package.

7
00:00:16,40 --> 00:00:19,40
In the test function, we first open the file,

8
00:00:19,40 --> 00:00:23,00
and if it can't open the file, we fail the test.

9
00:00:23,00 --> 00:00:27,00
Then we use defer to make sure the file is closed.

10
00:00:27,00 --> 00:00:32,60
Now we create a new csv reader and run an infinite loop.

11
00:00:32,60 --> 00:00:34,90
Because we don't know how many records are there

12
00:00:34,90 --> 00:00:36,40
in the csv file.

13
00:00:36,40 --> 00:00:40,20
We get the next reader from the csv reader.

14
00:00:40,20 --> 00:00:44,20
If the error that was returned is a special error called EOF

15
00:00:44,20 --> 00:00:47,50
meaning End Of File, we know there are no more records

16
00:00:47,50 --> 00:00:49,30
and we can break the loop.

17
00:00:49,30 --> 00:00:51,70
Otherwise, we have a different error,

18
00:00:51,70 --> 00:00:55,00
and in this case, we fail the test.

19
00:00:55,00 --> 00:00:56,20
And now we parse.

20
00:00:56,20 --> 00:00:59,50
Because we're dealing with csv, everything is text.

21
00:00:59,50 --> 00:01:01,90
So we need to parse the first value

22
00:01:01,90 --> 00:01:04,60
and we need to parse the second value.

23
00:01:04,60 --> 00:01:08,90
Every time, failing the test if you can't parse it to float.

24
00:01:08,90 --> 00:01:11,30
Once we have both values as floating points,

25
00:01:11,30 --> 00:01:14,60
we can use t.Run to run a subtest.

26
00:01:14,60 --> 00:01:15,90
We give it a test name,

27
00:01:15,90 --> 00:01:18,70
which will be just a value that we passed on,

28
00:01:18,70 --> 00:01:21,30
and a function to run the test.

29
00:01:21,30 --> 00:01:24,60
And as before, we call our function, check for an error,

30
00:01:24,60 --> 00:01:27,90
and check for a match.

31
00:01:27,90 --> 00:01:29,70
Let's save this and run.

32
00:01:29,70 --> 00:01:30,60
And now let's run.

33
00:01:30,60 --> 00:01:32,60
Go test

34
00:01:32,60 --> 00:01:33,70
- v.

35
00:01:33,70 --> 00:01:36,00
And everything is running from the csv file.

