1
00:00:00,60 --> 00:00:03,60
- [Instructor] Go comes with a built-in testing framework.

2
00:00:03,60 --> 00:00:06,40
I won't try to convince you that testing is important

3
00:00:06,40 --> 00:00:07,60
and it is.

4
00:00:07,60 --> 00:00:10,50
I'll show you how to use the test framework.

5
00:00:10,50 --> 00:00:13,10
As everything in Go, it is very simple

6
00:00:13,10 --> 00:00:16,20
but covers what you'll need.

7
00:00:16,20 --> 00:00:18,70
Say you write your own little math library

8
00:00:18,70 --> 00:00:22,30
which has an Sqrt function.

9
00:00:22,30 --> 00:00:25,80
This Sqrt function gets a value as a floating point number

10
00:00:25,80 --> 00:00:27,80
and returns a floating point number

11
00:00:27,80 --> 00:00:29,60
and a possible error.

12
00:00:29,60 --> 00:00:32,20
Now let's write a test for this.

13
00:00:32,20 --> 00:00:37,30
Test files in Go end with the suffix _test.go.

14
00:00:37,30 --> 00:00:41,00
Here is sqrt_test.go.

15
00:00:41,00 --> 00:00:42,90
Since we are dealing with floating points

16
00:00:42,90 --> 00:00:45,20
which are inexact, not only on Go

17
00:00:45,20 --> 00:00:47,90
but in any other programing languages,

18
00:00:47,90 --> 00:00:50,10
we create an almostEqual function

19
00:00:50,10 --> 00:00:54,70
that compares two floating points up to a precision

20
00:00:54,70 --> 00:00:56,10
and we define our test.

21
00:00:56,10 --> 00:00:57,70
It's a simple function.

22
00:00:57,70 --> 00:01:01,30
It start with the word Test with a capital T

23
00:01:01,30 --> 00:01:05,20
and receives one argument which is a pointer

24
00:01:05,20 --> 00:01:07,50
to testing the T structure.

25
00:01:07,50 --> 00:01:08,70
We write our code.

26
00:01:08,70 --> 00:01:10,30
We do square root of two,

27
00:01:10,30 --> 00:01:12,80
get a value and error.

28
00:01:12,80 --> 00:01:15,90
If there is an error, which we didn't expect,

29
00:01:15,90 --> 00:01:18,20
we use t.Fatalf.

30
00:01:18,20 --> 00:01:21,00
Fatalf will stop the execution right here

31
00:01:21,00 --> 00:01:22,90
and will print the error message

32
00:01:22,90 --> 00:01:26,90
error in calculation and the error.

33
00:01:26,90 --> 00:01:30,70
After that we compare the value with the expected value

34
00:01:30,70 --> 00:01:34,10
and again if it's not equal up to a precision,

35
00:01:34,10 --> 00:01:37,80
we use Fatalf to print out the error.

36
00:01:37,80 --> 00:01:41,40
Let's save this and run the tests.

37
00:01:41,40 --> 00:01:44,60
So, run go test.

38
00:01:44,60 --> 00:01:48,30
And we see that the tests are passing.

39
00:01:48,30 --> 00:01:50,20
If you'd like to see the test names,

40
00:01:50,20 --> 00:01:55,70
you can pass the -v parameter, go test -v.

41
00:01:55,70 --> 00:01:58,20
And now we see that it's running TestSimple

42
00:01:58,20 --> 00:02:00,00
and the test has passed.

43
00:02:00,00 --> 00:02:02,80
In some cases, we like to test the same function

44
00:02:02,80 --> 00:02:04,40
with many values.

45
00:02:04,40 --> 00:02:07,70
For this we are going to testCase.

46
00:02:07,70 --> 00:02:09,80
We define a testCase structure

47
00:02:09,80 --> 00:02:11,80
which holds the value to compute

48
00:02:11,80 --> 00:02:14,20
and the expected result.

49
00:02:14,20 --> 00:02:16,20
And then in our TestMany function,

50
00:02:16,20 --> 00:02:18,90
we define a slice of these testCases

51
00:02:18,90 --> 00:02:22,80
with the value, so for zero we expect zero,

52
00:02:22,80 --> 00:02:25,20
for two we expect 1.4

53
00:02:25,20 --> 00:02:28,40
and for nine we expect three.

54
00:02:28,40 --> 00:02:30,20
And then we use the Run method

55
00:02:30,20 --> 00:02:32,20
from the testing.T

56
00:02:32,20 --> 00:02:36,60
to run another test, so we give it a test name,

57
00:02:36,60 --> 00:02:39,80
we use the Sprintf to give it the test name

58
00:02:39,80 --> 00:02:43,00
and then we pass it a function to run

59
00:02:43,00 --> 00:02:45,20
which is a function which gets a testing.T

60
00:02:45,20 --> 00:02:48,00
and perform the test like every other test.

61
00:02:48,00 --> 00:02:51,90
So, this is very similar to TestSimple above.

62
00:02:51,90 --> 00:02:54,40
And of course we need to import the fmt package

63
00:02:54,40 --> 00:02:57,20
for our test tool.

64
00:02:57,20 --> 00:03:01,70
Once we save this, we can run the tests.

65
00:03:01,70 --> 00:03:03,80
And we see that we run all the tests

66
00:03:03,80 --> 00:03:06,30
and we have the name of the test printed out

67
00:03:06,30 --> 00:03:09,80
with the value that was passed on.

68
00:03:09,80 --> 00:03:12,30
In some cases, we'd like to run just a single test.

69
00:03:12,30 --> 00:03:15,40
This can be done with the -run switch.

70
00:03:15,40 --> 00:03:22,30
So, go test -run TestSimple

71
00:03:22,30 --> 00:03:25,40
and we can say also -v

72
00:03:25,40 --> 00:03:29,70
and see that we ran only our first test.

73
00:03:29,70 --> 00:03:31,90
These are the essentials of writing

74
00:03:31,90 --> 00:03:33,30
and running tests.

75
00:03:33,30 --> 00:03:36,00
If you are using an IDE such as Visual Studio Code

76
00:03:36,00 --> 00:03:40,20
or GoLand, you can run tests from inside the IDE.

77
00:03:40,20 --> 00:03:41,60
No more excuses.

78
00:03:41,60 --> 00:03:43,00
Go and test your code.

