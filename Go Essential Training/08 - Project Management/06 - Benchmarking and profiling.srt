1
00:00:00,60 --> 00:00:01,80
- [Instructor] One of the advantages

2
00:00:01,80 --> 00:00:04,10
of using GO is that it's fast.

3
00:00:04,10 --> 00:00:06,70
When you write code, you sometimes want to benchmark it

4
00:00:06,70 --> 00:00:09,10
and make sure that you're not making it slower

5
00:00:09,10 --> 00:00:12,10
when adding features or fixing bugs.

6
00:00:12,10 --> 00:00:14,90
Let's benchmark the sqrt function.

7
00:00:14,90 --> 00:00:17,40
We create a benchmark in a test file ending

8
00:00:17,40 --> 00:00:20,10
with _test.go.

9
00:00:20,10 --> 00:00:24,70
And we call it Benchmark and then the name of the benchmark.

10
00:00:24,70 --> 00:00:25,90
It gets one parameter,

11
00:00:25,90 --> 00:00:29,30
which is a pointer to testing.B.

12
00:00:29,30 --> 00:00:33,40
So what we do in the benchmark is we iterate b.N times

13
00:00:33,40 --> 00:00:36,00
calling our function.

14
00:00:36,00 --> 00:00:38,20
In our case, we make sure there was no error

15
00:00:38,20 --> 00:00:41,10
and if there is, we'll fatal the test,

16
00:00:41,10 --> 00:00:43,90
meaning stop it right here.

17
00:00:43,90 --> 00:00:45,70
Let's run this,

18
00:00:45,70 --> 00:00:49,00
go test -v.

19
00:00:49,00 --> 00:00:50,20
So it ran our test

20
00:00:50,20 --> 00:00:52,30
but it didn't run our benchmark,

21
00:00:52,30 --> 00:00:55,80
go test, by default, will not run benchmarks.

22
00:00:55,80 --> 00:00:57,40
So let's run the benchmarks.

23
00:00:57,40 --> 00:01:01,90
go test -v, we need to tell it -bench

24
00:01:01,90 --> 00:01:03,20
which benchmarks to run.

25
00:01:03,20 --> 00:01:06,90
And dot means all the benchmarks.

26
00:01:06,90 --> 00:01:08,90
And you see now it's running the test

27
00:01:08,90 --> 00:01:10,90
and also running our benchmark.

28
00:01:10,90 --> 00:01:16,70
And we are running 112 nanoseconds per operation.

29
00:01:16,70 --> 00:01:19,40
If you're interested only in the benchmarks

30
00:01:19,40 --> 00:01:20,80
and not in the tests,

31
00:01:20,80 --> 00:01:23,10
you can specify -run

32
00:01:23,10 --> 00:01:27,10
with a name that does not match any other test.

33
00:01:27,10 --> 00:01:29,90
This way you'll get only the benchmarks running

34
00:01:29,90 --> 00:01:32,80
and not the tests themself.

35
00:01:32,80 --> 00:01:34,50
Before optimizing a program,

36
00:01:34,50 --> 00:01:38,70
you need to profile it to see where it spends its time.

37
00:01:38,70 --> 00:01:41,90
You can use your benchmark for profiling.

38
00:01:41,90 --> 00:01:42,80
We'll do the benchmark

39
00:01:42,80 --> 00:01:43,80
and we say

40
00:01:43,80 --> 00:01:50,80
- Cpuprofile=prof.out.

41
00:01:50,80 --> 00:01:52,90
And now that we have prof.out,

42
00:01:52,90 --> 00:01:54,80
we can use the pprof too.

43
00:01:54,80 --> 00:01:59,20
So go tool pprof

44
00:01:59,20 --> 00:02:02,20
and then prof.out.

45
00:02:02,20 --> 00:02:06,30
To see our function, we do list and Sqrt

46
00:02:06,30 --> 00:02:09,40
and we see that the run time of our function.

47
00:02:09,40 --> 00:02:11,10
Here we can see every line

48
00:02:11,10 --> 00:02:14,90
and how much time it took flat and commutative.

49
00:02:14,90 --> 00:02:16,80
If you need more profiling options,

50
00:02:16,80 --> 00:02:19,00
Go has them, read the documentation.

