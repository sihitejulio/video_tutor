1
00:00:00,20 --> 00:00:01,50
- [Instructor] When you install Go,

2
00:00:01,50 --> 00:00:03,80
it comes with many handy libraries.

3
00:00:03,80 --> 00:00:05,50
However, there are many other

4
00:00:05,50 --> 00:00:08,00
third-party libraries out there.

5
00:00:08,00 --> 00:00:11,00
From working with databases, to passing file formats,

6
00:00:11,00 --> 00:00:14,70
to fancy web frameworks, and many more.

7
00:00:14,70 --> 00:00:18,60
Say we'd like to use toml as a configuration format.

8
00:00:18,60 --> 00:00:23,50
There is nothing in the standard library, so we use go.toml.

9
00:00:23,50 --> 00:00:26,20
We create a configuration object

10
00:00:26,20 --> 00:00:29,30
which has the login, which has the user and the password,

11
00:00:29,30 --> 00:00:32,70
we open the file,

12
00:00:32,70 --> 00:00:35,00
create the configuration object,

13
00:00:35,00 --> 00:00:40,00
and then use a new decoder for toml, and we run.

14
00:00:40,00 --> 00:00:41,50
At the end we're just going to print out

15
00:00:41,50 --> 00:00:44,50
the configuration that we got.

16
00:00:44,50 --> 00:00:45,80
Let's run it.

17
00:00:45,80 --> 00:00:48,50
So go run cfg.go.

18
00:00:48,50 --> 00:00:50,40
And this will fail, since Go

19
00:00:50,40 --> 00:00:53,90
cannot find the library we want.

20
00:00:53,90 --> 00:00:57,60
To install packages, we use the go get command,

21
00:00:57,60 --> 00:01:03,40
and we tell it that we want to install the go-toml package.

22
00:01:03,40 --> 00:01:05,20
Once the package is installed,

23
00:01:05,20 --> 00:01:09,90
we can run our code as expected.

24
00:01:09,90 --> 00:01:13,90
If you notice, the library name is go-toml.

25
00:01:13,90 --> 00:01:18,00
However, when we use it, we use toml.

26
00:01:18,00 --> 00:01:20,50
A package can have a different name

27
00:01:20,50 --> 00:01:23,10
than the directory it is found on.

28
00:01:23,10 --> 00:01:26,10
When you input something, you can give it a name.

29
00:01:26,10 --> 00:01:31,30
In our case, I can say that this package is called toml.

30
00:01:31,30 --> 00:01:32,90
I find this more explicit

31
00:01:32,90 --> 00:01:34,00
than going to the sources

32
00:01:34,00 --> 00:01:38,40
and figure out what is the package.

33
00:01:38,40 --> 00:01:46,60
And again, we can save this one, and run it.

34
00:01:46,60 --> 00:01:48,60
Renaming packages also allows us

35
00:01:48,60 --> 00:01:50,80
to use two packages with the same name

36
00:01:50,80 --> 00:01:52,60
in the same program.

37
00:01:52,60 --> 00:01:54,60
Once a package is installed,

38
00:01:54,60 --> 00:01:56,80
we can only use functions and variables

39
00:01:56,80 --> 00:01:59,70
that start with a capital letter in this package.

40
00:01:59,70 --> 00:02:01,20
Everything else is private,

41
00:02:01,20 --> 00:02:04,70
and can be used only from inside the package.

42
00:02:04,70 --> 00:02:07,20
Some packages rely on other packages.

43
00:02:07,20 --> 00:02:10,00
For example, when you install gorilla/mux,

44
00:02:10,00 --> 00:02:12,20
which is a popular HTTP router,

45
00:02:12,20 --> 00:02:15,20
it depends on gorilla context.

46
00:02:15,20 --> 00:02:16,70
Go get will install both,

47
00:02:16,70 --> 00:02:18,70
so it can work with gorilla/mux

48
00:02:18,70 --> 00:02:21,00
right after installing it.

49
00:02:21,00 --> 00:02:22,40
Once your program is built,

50
00:02:22,40 --> 00:02:24,60
all the packages it depends on

51
00:02:24,60 --> 00:02:26,70
are packed into the executable.

52
00:02:26,70 --> 00:02:28,80
This means when you deploy a program,

53
00:02:28,80 --> 00:02:31,00
you don't need any other installation step.

