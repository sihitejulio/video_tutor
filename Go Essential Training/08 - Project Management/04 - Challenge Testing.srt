1
00:00:00,60 --> 00:00:02,30
- [Narrator] Test your SQRT function

2
00:00:02,30 --> 00:00:04,90
with values from a CSV file.

3
00:00:04,90 --> 00:00:08,00
The CSV file will have a value and expected result

4
00:00:08,00 --> 00:00:08,90
in every line.

5
00:00:08,90 --> 00:00:11,00
You can see an example below.

