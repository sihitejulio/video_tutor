1
00:00:00,30 --> 00:00:06,50
(gentle, pensive music)

2
00:00:06,50 --> 00:00:07,70
- [Instructor] So now we're going to get started

3
00:00:07,70 --> 00:00:09,90
with actually doing some styling directly

4
00:00:09,90 --> 00:00:13,40
of an HTML element, and a React element.

5
00:00:13,40 --> 00:00:17,00
HTML elements are styled using the style attribute,

6
00:00:17,00 --> 00:00:20,90
and you can assign a string of CSS to that attribute,

7
00:00:20,90 --> 00:00:23,50
to actually style that particular element.

8
00:00:23,50 --> 00:00:27,30
React elements can be styled directly using the style prop.

9
00:00:27,30 --> 00:00:29,10
When you're actually using JSX

10
00:00:29,10 --> 00:00:30,70
to implement your React components,

11
00:00:30,70 --> 00:00:33,70
the syntax for styling with the style prop is actually

12
00:00:33,70 --> 00:00:36,60
quite similar to the HTML syntax.

13
00:00:36,60 --> 00:00:38,10
The only real difference is that,

14
00:00:38,10 --> 00:00:40,80
when you're working with React in the style prop,

15
00:00:40,80 --> 00:00:44,60
instead of assigning a string of styles,

16
00:00:44,60 --> 00:00:48,20
you're actually going to be assigning a JavaScript object

17
00:00:48,20 --> 00:00:50,40
that has each of the various style properties

18
00:00:50,40 --> 00:00:52,70
in camelcase format, and then, of course,

19
00:00:52,70 --> 00:00:57,10
the values assigned to each of those properties.

20
00:00:57,10 --> 00:01:00,30
So here's an example of styling an HTML element

21
00:01:00,30 --> 00:01:01,60
with a string of CSS.

22
00:01:01,60 --> 00:01:04,80
We actually have a div tag here with a style attribute,

23
00:01:04,80 --> 00:01:07,10
and then we're using the familiar syntax

24
00:01:07,10 --> 00:01:09,00
for CSS style declarations.

25
00:01:09,00 --> 00:01:11,20
So we have color: blue.

26
00:01:11,20 --> 00:01:14,50
And so the text there, some content, will be colored blue.

27
00:01:14,50 --> 00:01:16,60
Now this string of text that we've assigned

28
00:01:16,60 --> 00:01:19,00
cannot be changed without the use of some type

29
00:01:19,00 --> 00:01:21,40
of JavaScript code, so that this is going

30
00:01:21,40 --> 00:01:23,20
to be blue no matter what.

31
00:01:23,20 --> 00:01:26,70
There is no CSS that will actually override this value.

32
00:01:26,70 --> 00:01:28,00
It will be blue because

33
00:01:28,00 --> 00:01:30,70
that's what the style attribute has been set to.

34
00:01:30,70 --> 00:01:33,10
This is one of the reasons we don't like using this approach

35
00:01:33,10 --> 00:01:35,80
is it makes it impossible to be able to change

36
00:01:35,80 --> 00:01:39,00
that style value by using some additional stylesheet

37
00:01:39,00 --> 00:01:41,10
or something other than actually changing it

38
00:01:41,10 --> 00:01:42,40
through the JavaScript code

39
00:01:42,40 --> 00:01:46,30
and manipulating the DOM structure.

40
00:01:46,30 --> 00:01:49,10
For our React style prop, take a look here.

41
00:01:49,10 --> 00:01:53,00
We have some JSX code, which is what the div element is,

42
00:01:53,00 --> 00:01:55,80
and we have style, but now it looks a little bit different.

43
00:01:55,80 --> 00:01:58,20
We have the curly braces on the outside

44
00:01:58,20 --> 00:02:02,20
for the JSX expression, and then the JSX expression happens

45
00:02:02,20 --> 00:02:04,70
to be an object literal, which is then, of course,

46
00:02:04,70 --> 00:02:06,70
another set of curly braces, and then inside of

47
00:02:06,70 --> 00:02:08,40
that we have the name of the property

48
00:02:08,40 --> 00:02:10,10
that we're setting, and then, of course,

49
00:02:10,10 --> 00:02:12,30
the value that we're setting it to.

50
00:02:12,30 --> 00:02:15,10
Now, this object can be dynamically created.

51
00:02:15,10 --> 00:02:17,40
I've hard-coded it here in the slide,

52
00:02:17,40 --> 00:02:20,50
but you can actually dynamically generate that object,

53
00:02:20,50 --> 00:02:24,70
and be able to swap that out inside of your React component.

54
00:02:24,70 --> 00:02:26,70
And so this will actually,

55
00:02:26,70 --> 00:02:29,00
sets it up so that it's basically dynamic

56
00:02:29,00 --> 00:02:31,20
in the sense that the JavaScript code

57
00:02:31,20 --> 00:02:33,00
that's producing the object is dynamic.

