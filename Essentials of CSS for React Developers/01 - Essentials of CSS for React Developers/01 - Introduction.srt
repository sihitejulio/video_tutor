1
00:00:03,90 --> 00:00:05,50
- [Eric] Welcome to the Mastering CSS

2
00:00:05,50 --> 00:00:07,10
for React Developers series.

3
00:00:07,10 --> 00:00:09,40
In this course, we're going to take a look at the essentials

4
00:00:09,40 --> 00:00:11,50
of using CSS in React.

5
00:00:11,50 --> 00:00:12,50
My name is Eric Greene.

6
00:00:12,50 --> 00:00:16,30
This video is brought to you by WintellectNOW.

7
00:00:16,30 --> 00:00:18,90
So we're going to take a look at an overview of using CSS.

8
00:00:18,90 --> 00:00:21,20
We'll take a look at how to set up a development environment

9
00:00:21,20 --> 00:00:23,30
so we can do some CSS coding.

10
00:00:23,30 --> 00:00:25,70
Then we'll take a look at styling HTML

11
00:00:25,70 --> 00:00:29,10
and React elements and some of the syntaxes and stuff

12
00:00:29,10 --> 00:00:31,20
that are used to do that directly.

13
00:00:31,20 --> 00:00:33,30
Then we're going to take a look at actually moving

14
00:00:33,30 --> 00:00:36,30
our CSS to an external file,

15
00:00:36,30 --> 00:00:38,30
and then incorporating that into our project.

16
00:00:38,30 --> 00:00:40,70
And we'll take a look at how that works with Webpack.

17
00:00:40,70 --> 00:00:42,70
And then, finally, we'll take a look at some basic

18
00:00:42,70 --> 00:00:45,60
selectors and styles and try to understand

19
00:00:45,60 --> 00:00:47,50
some of the basics of doing some styling

20
00:00:47,50 --> 00:00:49,00
of our React components in a way.

