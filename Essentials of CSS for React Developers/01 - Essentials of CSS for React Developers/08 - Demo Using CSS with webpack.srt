1
00:00:00,00 --> 00:00:01,00
(light music)

2
00:00:01,00 --> 00:00:02,10
- [Instructor] Let's do a demonstration now

3
00:00:02,10 --> 00:00:04,30
of using CSS with webpack.

4
00:00:04,30 --> 00:00:07,00
So let's go ahead and take a look at how we can define

5
00:00:07,00 --> 00:00:10,70
our styles external to the actual elements themselves.

6
00:00:10,70 --> 00:00:13,20
We'll start off first with a style element

7
00:00:13,20 --> 00:00:15,60
then we're going to link in an external style sheet,

8
00:00:15,60 --> 00:00:19,20
and then we're going to dive into the webpack importing process

9
00:00:19,20 --> 00:00:22,10
and take a look at what the development and production

10
00:00:22,10 --> 00:00:24,20
modes look like for that.

11
00:00:24,20 --> 00:00:27,20
So, right now we have this demo component that's generating

12
00:00:27,20 --> 00:00:29,30
this div element and I'm actually

13
00:00:29,30 --> 00:00:32,10
going to just modify this slightly.

14
00:00:32,10 --> 00:00:33,90
We're going to keep the actual div,

15
00:00:33,90 --> 00:00:35,90
but I'm going to take out the styling part.

16
00:00:35,90 --> 00:00:40,00
So, we'll just simply have the div itself generating there.

17
00:00:40,00 --> 00:00:43,70
If we come back over to our webpage,

18
00:00:43,70 --> 00:00:45,80
we'll see that we just have this Some Content

19
00:00:45,80 --> 00:00:48,10
inside of our main, and what we're going to do

20
00:00:48,10 --> 00:00:50,60
is we're going to style it from a style element

21
00:00:50,60 --> 00:00:52,00
within the head.

22
00:00:52,00 --> 00:00:55,30
So let's head back over to our index html file,

23
00:00:55,30 --> 00:00:59,50
and let's come into the head section here and say style.

24
00:00:59,50 --> 00:01:02,00
There we go, and then we're going to come into here

25
00:01:02,00 --> 00:01:05,10
and we're going to set this up to basically make

26
00:01:05,10 --> 00:01:06,90
the text blue.

27
00:01:06,90 --> 00:01:10,30
So, we'll say div, that's going to be our selector,

28
00:01:10,30 --> 00:01:14,70
and then we're going to say color blue.

29
00:01:14,70 --> 00:01:16,60
When we do that and head back over to our browser

30
00:01:16,60 --> 00:01:18,90
you'll see now we have blue text there.

31
00:01:18,90 --> 00:01:20,30
That looks good.

32
00:01:20,30 --> 00:01:23,20
And if we actually right click on this and Inspect

33
00:01:23,20 --> 00:01:26,50
you'll see that it's actually coming from this div selector

34
00:01:26,50 --> 00:01:31,00
that's defined here on line 12 in our index html file.

35
00:01:31,00 --> 00:01:34,10
You'll notice though it's not attached to the element style.

36
00:01:34,10 --> 00:01:36,70
So, we do have the ability to override this color

37
00:01:36,70 --> 00:01:38,40
with a more specific selector.

38
00:01:38,40 --> 00:01:41,20
We talk more about that in future courses,

39
00:01:41,20 --> 00:01:42,80
being able to override this value,

40
00:01:42,80 --> 00:01:44,60
because it's not specifically attached

41
00:01:44,60 --> 00:01:46,00
to the element itself.

42
00:01:46,00 --> 00:01:47,90
If we actually came into the element

43
00:01:47,90 --> 00:01:53,10
and said color and then we did red

44
00:01:53,10 --> 00:01:56,40
you would now see that the actual style is going to override.

45
00:01:56,40 --> 00:01:59,80
The style attribute overrides the color on the style sheet.

46
00:01:59,80 --> 00:02:01,70
This is why we don't like using the style

47
00:02:01,70 --> 00:02:05,10
because you can't really override this from a style sheet.

48
00:02:05,10 --> 00:02:07,10
It's better to actually use a style sheet

49
00:02:07,10 --> 00:02:09,00
even if it's inside of a style element

50
00:02:09,00 --> 00:02:13,10
than it is to use the style attribute on an HTML element.

51
00:02:13,10 --> 00:02:26,80
But we'll go ahead and get rid of this here.

52
00:02:26,80 --> 00:02:29,40
There we go, and now that's gone, okay.

53
00:02:29,40 --> 00:02:32,30
So now, let's take a look at our style element

54
00:02:32,30 --> 00:02:34,60
and apply some media to this.

55
00:02:34,60 --> 00:02:37,90
So, we can come into here and we could say media,

56
00:02:37,90 --> 00:02:41,20
and we could do print for blue,

57
00:02:41,20 --> 00:02:46,90
and then make a copy of this, and we could say screen,

58
00:02:46,90 --> 00:02:50,50
and on screen we'll make it red.

59
00:02:50,50 --> 00:02:53,30
So, if we save this and go back over to our web browser

60
00:02:53,30 --> 00:02:55,00
you'll see there it's red.

61
00:02:55,00 --> 00:02:59,60
If I were to come over to here and say Print

62
00:02:59,60 --> 00:03:02,30
and do Print Preview you can now see it's blue.

63
00:03:02,30 --> 00:03:04,80
So, depending upon the media in which we're working

64
00:03:04,80 --> 00:03:08,40
with it'll apply one style sheet over another.

65
00:03:08,40 --> 00:03:09,70
But we're not actually going to print this.

66
00:03:09,70 --> 00:03:11,30
We'll just click Cancel.

67
00:03:11,30 --> 00:03:14,50
All right, so, here we're using style sheets

68
00:03:14,50 --> 00:03:15,90
but the style sheets are still

69
00:03:15,90 --> 00:03:19,20
inside of the actual HTML file.

70
00:03:19,20 --> 00:03:20,60
Now what we're going to do is define

71
00:03:20,60 --> 00:03:22,10
some external style sheets.

72
00:03:22,10 --> 00:03:24,90
So let's come over here and we'll create a New File

73
00:03:24,90 --> 00:03:29,10
and we're going to call this screen.css for screen

74
00:03:29,10 --> 00:03:31,10
and we'll also do one for print.

75
00:03:31,10 --> 00:03:33,50
We'll call this one print.css.

76
00:03:33,50 --> 00:03:35,20
And you can name these anything you'd like.

77
00:03:35,20 --> 00:03:37,50
I'm naming them this way so it's clear

78
00:03:37,50 --> 00:03:39,50
for the demonstration what we're doing.

79
00:03:39,50 --> 00:03:42,80
But there's nothing special about the actual name.

80
00:03:42,80 --> 00:03:45,90
So, I'm going to take my print CSS and pull that out

81
00:03:45,90 --> 00:03:48,70
and drop that over here in my style sheet,

82
00:03:48,70 --> 00:03:52,70
then I'm going to come over to index and grab my div

83
00:03:52,70 --> 00:03:56,80
from my red and put that in my screen CSS there.

84
00:03:56,80 --> 00:03:59,10
I'm going to get rid of my two style elements.

85
00:03:59,10 --> 00:04:01,10
I don't need those.

86
00:04:01,10 --> 00:04:04,30
But I will need some link tags.

87
00:04:04,30 --> 00:04:08,00
So, we're going to say link href

88
00:04:08,00 --> 00:04:10,80
and then we'll do our print CSS.

89
00:04:10,80 --> 00:04:19,50
This will be a relationship of a style sheet.

90
00:04:19,50 --> 00:04:27,50
And then here we'll say media print.

91
00:04:27,50 --> 00:04:33,30
And then here we'll do another link, href screen.css

92
00:04:33,30 --> 00:04:35,00
and we'll specify our relationship,

93
00:04:35,00 --> 00:04:41,70
which is going to be a style sheet, media equal to screen.

94
00:04:41,70 --> 00:04:44,20
Excellent, so, now that we have those style sheets there

95
00:04:44,20 --> 00:04:46,00
we'll just save that.

96
00:04:46,00 --> 00:04:48,40
Come back to over to our web browser and reload.

97
00:04:48,40 --> 00:04:50,60
And now, if we were to actually right click on this

98
00:04:50,60 --> 00:04:52,30
and Inspect you'll see it actually lists

99
00:04:52,30 --> 00:04:55,20
that this comes from the screen.css file.

100
00:04:55,20 --> 00:04:57,10
So now with this being in an external file

101
00:04:57,10 --> 00:04:59,40
it can be cached and preprocessed,

102
00:04:59,40 --> 00:05:01,40
and so, if we go to reload this page again

103
00:05:01,40 --> 00:05:05,00
it doesn't have to reprocess this file at all.

104
00:05:05,00 --> 00:05:07,40
If we want to go ahead and do our Print Preview again,

105
00:05:07,40 --> 00:05:11,00
so, we'll say Print, and once again we have blue.

106
00:05:11,00 --> 00:05:15,70
The media attribute also works with the link tag as well.

107
00:05:15,70 --> 00:05:16,80
So there we go.

108
00:05:16,80 --> 00:05:20,00
So now what we want to do is instead of actually pulling

109
00:05:20,00 --> 00:05:23,00
in the style sheets like this we actually want to set up

110
00:05:23,00 --> 00:05:25,70
a style sheet for our component itself.

111
00:05:25,70 --> 00:05:29,00
So, we'll get rid of these link tags there,

112
00:05:29,00 --> 00:05:31,10
and we're going to come, actually, let's do this.

113
00:05:31,10 --> 00:05:34,30
I'm going to

114
00:05:34,30 --> 00:05:35,30
do this real quick.

115
00:05:35,30 --> 00:05:40,90
I'm going to take these guys, copy them,

116
00:05:40,90 --> 00:05:48,00
then undo.

117
00:05:48,00 --> 00:05:50,70
Perfect, and then I'm just going to take all of this

118
00:05:50,70 --> 00:05:51,90
and I'll just comment it out.

119
00:05:51,90 --> 00:05:54,70
That way when you go back to look at the code,

120
00:05:54,70 --> 00:05:57,60
you can reference both of these.

121
00:05:57,60 --> 00:05:58,70
Perfect.

122
00:05:58,70 --> 00:06:01,50
Okay, so, now what we want to do is no longer pull

123
00:06:01,50 --> 00:06:04,00
our styles in like this, but actually pull them

124
00:06:04,00 --> 00:06:06,10
in at the component level.

125
00:06:06,10 --> 00:06:07,40
So I'm going to come over to Demo

126
00:06:07,40 --> 00:06:12,50
and I'm going to create a new CSS file called Demo.css,

127
00:06:12,50 --> 00:06:17,90
and inside of Demo I'm going to simply put a div element here,

128
00:06:17,90 --> 00:06:21,70
and then with our div element here we'll say color

129
00:06:21,70 --> 00:06:23,70
and we'll say blue.

130
00:06:23,70 --> 00:06:25,80
So now I'm going to come over to Demo JS

131
00:06:25,80 --> 00:06:36,60
and I'm going to say import and we're going to say ./Demo.css

132
00:06:36,60 --> 00:06:39,50
and it'll actually import our CSS file.

133
00:06:39,50 --> 00:06:40,90
Head back over to here, reload.

134
00:06:40,90 --> 00:06:43,50
You will see we now have Some Content.

135
00:06:43,50 --> 00:06:45,50
Now, we are running this in development mode.

136
00:06:45,50 --> 00:06:47,80
So if we come up to the head section

137
00:06:47,80 --> 00:06:50,10
and scroll past our comments you'll see here

138
00:06:50,10 --> 00:06:52,10
is the blue color.

139
00:06:52,10 --> 00:06:53,90
Now, over here in our Network you can see

140
00:06:53,90 --> 00:06:57,40
where we loaded up everything on that initial page load.

141
00:06:57,40 --> 00:06:59,60
I'm going to clear all of this out

142
00:06:59,60 --> 00:07:01,30
and you'll notice we don't, all of the network

143
00:07:01,30 --> 00:07:03,10
activity has been cleared.

144
00:07:03,10 --> 00:07:05,80
I'm going to come over to Demo CSS and I'm going to change

145
00:07:05,80 --> 00:07:09,30
this from blue to red.

146
00:07:09,30 --> 00:07:11,90
Come back over to the web browser, what do you notice?

147
00:07:11,90 --> 00:07:14,40
It does a hot reload, a hot update.

148
00:07:14,40 --> 00:07:16,10
Notice it did not reload the page,

149
00:07:16,10 --> 00:07:18,40
yet I still have my new red color here

150
00:07:18,40 --> 00:07:20,30
showing up for my style.

151
00:07:20,30 --> 00:07:24,00
If I Inspect this you'll see that the actual style element

152
00:07:24,00 --> 00:07:28,50
here has had its content replaced with color set to red.

153
00:07:28,50 --> 00:07:30,90
So, this was the hot reloading of where it's using

154
00:07:30,90 --> 00:07:33,40
this style element to reload the styles

155
00:07:33,40 --> 00:07:35,90
without reloading the page.

156
00:07:35,90 --> 00:07:38,30
Now, let's say I wanted to actually produce

157
00:07:38,30 --> 00:07:40,00
a production version of this,

158
00:07:40,00 --> 00:07:43,90
and actually have a real CSS file.

159
00:07:43,90 --> 00:07:46,20
I'm going to go ahead and stop the application,

160
00:07:46,20 --> 00:07:50,10
and I'm going to say NPM run build.

161
00:07:50,10 --> 00:07:52,60
This will build a production version.

162
00:07:52,60 --> 00:07:54,70
Inside of our build folder here you'll see

163
00:07:54,70 --> 00:07:59,10
there's actually a CSS folder with our main CSS file.

164
00:07:59,10 --> 00:08:02,20
There's our div color red, and if we come

165
00:08:02,20 --> 00:08:11,50
into index over here you will see

166
00:08:11,50 --> 00:08:14,10
the actual link element that actually pulls

167
00:08:14,10 --> 00:08:16,90
in that style sheet in our production version.

168
00:08:16,90 --> 00:08:22,70
So now I'm going to say serve-s build.

169
00:08:22,70 --> 00:08:28,10
Oh, make sure we install our serve.

170
00:08:28,10 --> 00:08:30,90
Now we'll do our, oh,

171
00:08:30,90 --> 00:08:33,70
do our serve-s build.

172
00:08:33,70 --> 00:08:36,40
Ah, already have port 5000 in use.

173
00:08:36,40 --> 00:08:44,80
Let me go ahead and change my port number to 5050.

174
00:08:44,80 --> 00:08:48,90
So now I'll go to 5050 here,

175
00:08:48,90 --> 00:08:51,00
and you can see there's my content loaded up

176
00:08:51,00 --> 00:08:53,70
and if I Inspect this, you will see

177
00:08:53,70 --> 00:08:57,50
this is coming from the Demo CSS file,

178
00:08:57,50 --> 00:09:00,10
which is here, all right?

179
00:09:00,10 --> 00:09:01,90
But this is really a source map.

180
00:09:01,90 --> 00:09:04,50
It's really using a source mapping to actually load up

181
00:09:04,50 --> 00:09:07,70
this content and say that it's from Demo CSS.

182
00:09:07,70 --> 00:09:10,60
In reality, it's actually from that one CSS file

183
00:09:10,60 --> 00:09:12,30
that was produced by a webpack,

184
00:09:12,30 --> 00:09:14,60
but because webpack is using source maps

185
00:09:14,60 --> 00:09:16,90
it's actually loading up the source map of this,

186
00:09:16,90 --> 00:09:19,00
which is pointing to Demo.css.

