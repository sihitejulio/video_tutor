1
00:00:00,20 --> 00:00:06,30
(quiet, pensive music)

2
00:00:06,30 --> 00:00:07,50
- [Instructor] So, in conclusion, we've learned

3
00:00:07,50 --> 00:00:10,20
that HTML and React utilize CSS

4
00:00:10,20 --> 00:00:12,90
to style their user interface elements.

5
00:00:12,90 --> 00:00:14,60
CSS styles can be applied directly

6
00:00:14,60 --> 00:00:17,50
to elements using the style attribute,

7
00:00:17,50 --> 00:00:20,20
or the style prop in the case of React.

8
00:00:20,20 --> 00:00:21,80
External style sheets can be linked

9
00:00:21,80 --> 00:00:24,30
to an HTML file using the link element.

10
00:00:24,30 --> 00:00:25,90
When we use Webpack, we actually

11
00:00:25,90 --> 00:00:28,60
import our external stylesheets into JavaScript,

12
00:00:28,60 --> 00:00:30,00
where it is optimally processed

13
00:00:30,00 --> 00:00:33,00
for either development or production modes.

14
00:00:33,00 --> 00:00:35,60
Now commonly, CSS stylesheets are defined for,

15
00:00:35,60 --> 00:00:38,40
and imported with, the ES2015 syntax

16
00:00:38,40 --> 00:00:40,50
into specific React components, allowing us

17
00:00:40,50 --> 00:00:44,00
to co-locate the CSS with the actual component,

18
00:00:44,00 --> 00:00:46,50
but all of that magic is handled by Webpack.

19
00:00:46,50 --> 00:00:50,20
JavaScript itself doesn't know anything about CSS styles.

20
00:00:50,20 --> 00:00:51,60
Styles are applied using selectors

21
00:00:51,60 --> 00:00:56,10
where each selector is a collection of style declarations.

22
00:00:56,10 --> 00:00:59,30
A style declaration is a property name and value pair.

23
00:00:59,30 --> 00:01:01,90
Examples of simple selectors include the type selector,

24
00:01:01,90 --> 00:01:04,30
the class selector, and the id selector.

25
00:01:04,30 --> 00:01:06,70
When we use multiple simple selectors together,

26
00:01:06,70 --> 00:01:14,00
we actually create something called a compound selector.

