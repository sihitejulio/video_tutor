1
00:00:00,00 --> 00:00:07,60
(soft music)

2
00:00:07,60 --> 00:00:09,60
- [Instructor] A good practice when actually working

3
00:00:09,60 --> 00:00:12,50
with HTML and styling it with CSS,

4
00:00:12,50 --> 00:00:16,40
is to decouple the styles from the actual elements

5
00:00:16,40 --> 00:00:18,00
and the style attribute.

6
00:00:18,00 --> 00:00:20,40
And actually move those style declarations

7
00:00:20,40 --> 00:00:23,80
to a style sheet where we use CSS selectors

8
00:00:23,80 --> 00:00:25,60
to connect the style declarations

9
00:00:25,60 --> 00:00:28,40
to the elements which are being styled.

10
00:00:28,40 --> 00:00:30,30
Now there are several ways to do this.

11
00:00:30,30 --> 00:00:31,30
One way of doing it is

12
00:00:31,30 --> 00:00:33,90
to use something called the style element.

13
00:00:33,90 --> 00:00:36,60
The style element goes into the HTML documents,

14
00:00:36,60 --> 00:00:39,50
so we're still not benefiting from caching.

15
00:00:39,50 --> 00:00:43,10
But it does allow us to actually extract the style sheet out

16
00:00:43,10 --> 00:00:45,80
of the HTML elements of the page

17
00:00:45,80 --> 00:00:51,10
and put all of the styled declarations in the CSS selectors,

18
00:00:51,10 --> 00:00:53,80
up into a style sheet inside of the style element

19
00:00:53,80 --> 00:00:56,00
which is typically in the head part of the page.

20
00:00:56,00 --> 00:00:58,60
And it does allow us to do some decoupling

21
00:00:58,60 --> 00:01:00,20
of the styles from those elements

22
00:01:00,20 --> 00:01:02,30
and even be able to override those styles

23
00:01:02,30 --> 00:01:04,30
with other style sheets.

24
00:01:04,30 --> 00:01:07,50
This is still not a recommend way to do your styling,

25
00:01:07,50 --> 00:01:08,80
but it is an option.

26
00:01:08,80 --> 00:01:11,40
In fact you will see the style element being used

27
00:01:11,40 --> 00:01:14,70
by things like Webpack to do the whole hot reloading.

28
00:01:14,70 --> 00:01:17,20
Where it's dynamically adding and removing style elements

29
00:01:17,20 --> 00:01:21,00
with new style sheets based upon changes being made

30
00:01:21,00 --> 00:01:25,00
by the developer.

31
00:01:25,00 --> 00:01:27,40
Now if you're writing static HTML documents

32
00:01:27,40 --> 00:01:29,90
and you want to apply CSS to them,

33
00:01:29,90 --> 00:01:32,10
instead of setting the style attribute

34
00:01:32,10 --> 00:01:34,40
or using the style element the preferred way is

35
00:01:34,40 --> 00:01:37,00
to actually have an external style sheet file

36
00:01:37,00 --> 00:01:40,30
that is linked to the document using the link element.

37
00:01:40,30 --> 00:01:41,90
The link element has the ability

38
00:01:41,90 --> 00:01:45,60
to download a style sheet either from the origin web server

39
00:01:45,60 --> 00:01:48,40
or to download it from any web server.

40
00:01:48,40 --> 00:01:51,10
And what's nice is you can have multiple link elements

41
00:01:51,10 --> 00:01:53,30
to pull in multiple style sheets.

42
00:01:53,30 --> 00:01:56,30
In order to set this up you'll set the href attribute

43
00:01:56,30 --> 00:01:59,00
of the link to the location of the style sheet itself.

44
00:01:59,00 --> 00:02:02,70
And then you'll also set the r e l attribute

45
00:02:02,70 --> 00:02:03,90
to be set to style sheet.

46
00:02:03,90 --> 00:02:05,90
Because the link element can actually be used

47
00:02:05,90 --> 00:02:08,00
to link in many different types of content,

48
00:02:08,00 --> 00:02:10,40
style sheets are just one of those things.

49
00:02:10,40 --> 00:02:12,20
So we want to denote the relationship

50
00:02:12,20 --> 00:02:15,10
between the HTML document and that external file,

51
00:02:15,10 --> 00:02:19,90
by indicating that it is in fact a style sheet.

52
00:02:19,90 --> 00:02:21,10
So let's take a look at some code here,

53
00:02:21,10 --> 00:02:23,10
the style element itself.

54
00:02:23,10 --> 00:02:24,50
You can see here in the head section

55
00:02:24,50 --> 00:02:27,50
we have a style element and we have a selector, the div.

56
00:02:27,50 --> 00:02:31,10
And then we have a style declaration color colon blue.

57
00:02:31,10 --> 00:02:33,00
And this will be applied to the div element inside

58
00:02:33,00 --> 00:02:35,70
of the body and it will be blue.

59
00:02:35,70 --> 00:02:38,00
But it is possible with additional style sheets

60
00:02:38,00 --> 00:02:40,20
to override this styling.

61
00:02:40,20 --> 00:02:42,40
So while we're still not benefiting from the caching,

62
00:02:42,40 --> 00:02:45,30
we still have kind of a tightly coupled styling

63
00:02:45,30 --> 00:02:48,60
and HTML document scenario going on here.

64
00:02:48,60 --> 00:02:51,60
We do at least have the ability to override the colors

65
00:02:51,60 --> 00:02:56,50
with additional style sheets.

66
00:02:56,50 --> 00:02:57,70
Now a better alternative is

67
00:02:57,70 --> 00:02:59,40
to actually use that link element.

68
00:02:59,40 --> 00:03:01,30
So here, we're going to have the link element

69
00:03:01,30 --> 00:03:05,10
and we're going to reference a file called styles dot css.

70
00:03:05,10 --> 00:03:07,20
And then you can see here in styles dot css

71
00:03:07,20 --> 00:03:08,80
we have the same div selector

72
00:03:08,80 --> 00:03:10,80
with the same style declaration.

73
00:03:10,80 --> 00:03:13,50
But now instead of tightly coupling the CSS file

74
00:03:13,50 --> 00:03:16,40
to the HTML document, it is external.

75
00:03:16,40 --> 00:03:17,30
What's nice about this is,

76
00:03:17,30 --> 00:03:19,50
the web browser will download this file,

77
00:03:19,50 --> 00:03:23,50
parse it, process it, and then cache it.

78
00:03:23,50 --> 00:03:25,50
So later on at this document,

79
00:03:25,50 --> 00:03:27,40
this HTML document is reloaded,

80
00:03:27,40 --> 00:03:31,30
it will actually reuse the existing style sheet.

81
00:03:31,30 --> 00:03:34,10
Also we can take the style sheet and actually push it up

82
00:03:34,10 --> 00:03:36,40
to something called a content delivery network.

83
00:03:36,40 --> 00:03:38,00
Which will optimize the delivery

84
00:03:38,00 --> 00:03:41,90
of static assets like CSS or JavaScript,

85
00:03:41,90 --> 00:03:46,00
to the end-user's web browser.

86
00:03:46,00 --> 00:03:48,80
Now both link and style elements have support

87
00:03:48,80 --> 00:03:51,30
for something called a media attribute.

88
00:03:51,30 --> 00:03:52,70
And what this allows us to do is

89
00:03:52,70 --> 00:03:55,90
to selectively apply the style sheet based upon the media

90
00:03:55,90 --> 00:03:57,90
which is displaying the page.

91
00:03:57,90 --> 00:04:01,60
For example, let's say that we have a print friendly page.

92
00:04:01,60 --> 00:04:04,40
We might have one set of styles that's used for printing

93
00:04:04,40 --> 00:04:07,50
and one set of styles that are used for the screen.

94
00:04:07,50 --> 00:04:10,30
Now downloading a style sheet is synchronous,

95
00:04:10,30 --> 00:04:12,80
which means it does block the rendering of the page.

96
00:04:12,80 --> 00:04:13,90
So we want to limit the number

97
00:04:13,90 --> 00:04:16,70
of style sheets we're actually having to download

98
00:04:16,70 --> 00:04:22,50
to our page, so that we can have faster page loads.

99
00:04:22,50 --> 00:04:27,00
Still yet another way of incorporating CSS into our webpage,

100
00:04:27,00 --> 00:04:30,40
is through the use of JavaScript.

101
00:04:30,40 --> 00:04:33,80
Now JavaScript itself has no support and no concept,

102
00:04:33,80 --> 00:04:36,70
and no idea of what to do with CSS files.

103
00:04:36,70 --> 00:04:38,60
But in modern JavaScript programming,

104
00:04:38,60 --> 00:04:42,20
we can utilize tooling like Webpack to be able

105
00:04:42,20 --> 00:04:45,80
to look at an import statement that refers to a CSS file.

106
00:04:45,80 --> 00:04:48,20
And actually process that import,

107
00:04:48,20 --> 00:04:51,40
differently from the other JavaScript and process it,

108
00:04:51,40 --> 00:04:54,90
before the actual JavaScript code itself is processed.

109
00:04:54,90 --> 00:04:56,50
This is nice because it allows us

110
00:04:56,50 --> 00:04:59,80
to basically import our style sheet in the same place

111
00:04:59,80 --> 00:05:01,30
where we define our component,

112
00:05:01,30 --> 00:05:03,90
allowing us to co-locate the two.

113
00:05:03,90 --> 00:05:05,40
So we could actually have a style sheet

114
00:05:05,40 --> 00:05:08,50
that has a bunch of styles specifically for our component,

115
00:05:08,50 --> 00:05:11,90
and then having them referenced together.

116
00:05:11,90 --> 00:05:14,60
Now the way this will work is, depending upon the mode

117
00:05:14,60 --> 00:05:17,20
that you're in, Webpack will do different things

118
00:05:17,20 --> 00:05:18,60
with the style sheet.

119
00:05:18,60 --> 00:05:19,80
If you're in development mode,

120
00:05:19,80 --> 00:05:21,50
it will do what's called hot reloading.

121
00:05:21,50 --> 00:05:23,50
It'll actually take that import statement

122
00:05:23,50 --> 00:05:26,40
and load the styles into an actual style element.

123
00:05:26,40 --> 00:05:27,70
Which is loaded up into the head

124
00:05:27,70 --> 00:05:30,40
and can be replaced whenever the style sheet is changed.

125
00:05:30,40 --> 00:05:32,00
What's even cooler is that it can be replaced,

126
00:05:32,00 --> 00:05:33,90
without even reloading the page.

127
00:05:33,90 --> 00:05:37,30
It'll basically pull in those styles, replace that element

128
00:05:37,30 --> 00:05:40,70
and then you have new styles being applied to your page.

129
00:05:40,70 --> 00:05:43,20
A second thing that it can do is in production mode,

130
00:05:43,20 --> 00:05:44,90
it'll actually take all those imports

131
00:05:44,90 --> 00:05:47,40
and produce a single CSS file.

132
00:05:47,40 --> 00:05:51,20
Which is pulled into the webpage or the HTML document,

133
00:05:51,20 --> 00:05:53,90
like any other external CSS file would be.

134
00:05:53,90 --> 00:05:57,30
There's even a link tag in the head for pulling that in.

135
00:05:57,30 --> 00:05:59,70
So what's nice about Webpack is depending upon the mode

136
00:05:59,70 --> 00:06:00,60
that you're operating in,

137
00:06:00,60 --> 00:06:02,30
it can load the styles different ways

138
00:06:02,30 --> 00:06:06,70
depending upon what makes the most sense.

139
00:06:06,70 --> 00:06:08,90
So here's an example of working in development mode.

140
00:06:08,90 --> 00:06:12,10
You can see where we have our Some Content inside

141
00:06:12,10 --> 00:06:13,10
of our div.

142
00:06:13,10 --> 00:06:14,40
But then you'll notice at the top

143
00:06:14,40 --> 00:06:16,10
we actually have a style element

144
00:06:16,10 --> 00:06:19,10
with the div color blue located inside of it.

145
00:06:19,10 --> 00:06:20,40
If we go back to the previous slide

146
00:06:20,40 --> 00:06:25,10
you'll see how we have our, demo CSS and our demo component.

147
00:06:25,10 --> 00:06:27,90
And you can see how it's taking that import demo CSS

148
00:06:27,90 --> 00:06:29,20
and actually using it

149
00:06:29,20 --> 00:06:32,40
to populate the style element there in the head.

150
00:06:32,40 --> 00:06:34,40
And then when ever we make changes to the style sheet,

151
00:06:34,40 --> 00:06:36,50
that style element will be replaced

152
00:06:36,50 --> 00:06:37,70
with a new set of styles,

153
00:06:37,70 --> 00:06:40,70
that reflect what ever the coding changes were to the file

154
00:06:40,70 --> 00:06:45,40
and it does all of this without actually reloading the page.

155
00:06:45,40 --> 00:06:47,30
Here's an example in production mode.

156
00:06:47,30 --> 00:06:50,60
Same exact components, same exact CSS file.

157
00:06:50,60 --> 00:06:51,90
But this time you'll notice

158
00:06:51,90 --> 00:06:53,70
there's not actual style element,

159
00:06:53,70 --> 00:06:55,10
instead we're actually pulling this

160
00:06:55,10 --> 00:06:57,00
from an external demo CSS file.

