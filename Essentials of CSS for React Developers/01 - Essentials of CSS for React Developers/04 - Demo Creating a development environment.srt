1
00:00:00,00 --> 00:00:01,10
(upbeat music)

2
00:00:01,10 --> 00:00:02,20
- [Instructor] Let's do a demonstration now

3
00:00:02,20 --> 00:00:05,80
of creating a development environment.

4
00:00:05,80 --> 00:00:09,10
In order to get started with using CSS in a React project,

5
00:00:09,10 --> 00:00:11,30
we have to be able to first create a React project.

6
00:00:11,30 --> 00:00:14,30
And to do that, we need to have Node JS installed.

7
00:00:14,30 --> 00:00:15,90
I'll be doing all the demonstrations here

8
00:00:15,90 --> 00:00:18,90
with Node version 10.12.0, which is actually the latest,

9
00:00:18,90 --> 00:00:22,40
current version of Node at the time of this recording.

10
00:00:22,40 --> 00:00:24,50
If you're using a future version of Node

11
00:00:24,50 --> 00:00:27,70
that does not work with this particular project,

12
00:00:27,70 --> 00:00:31,00
I recommend you go back to version 10.12.0

13
00:00:31,00 --> 00:00:33,00
and install that on your system

14
00:00:33,00 --> 00:00:37,30
to do your development during the demonstrations.

15
00:00:37,30 --> 00:00:39,80
I also recommend using Visual Studio Code

16
00:00:39,80 --> 00:00:42,60
to be able to do the editing of your project.

17
00:00:42,60 --> 00:00:44,10
Visual Studio Code is by far

18
00:00:44,10 --> 00:00:46,80
the most popular Java Script editor out there.

19
00:00:46,80 --> 00:00:48,30
It's totally cross-platform.

20
00:00:48,30 --> 00:00:50,20
I'm recording this on Linux.

21
00:00:50,20 --> 00:00:51,50
And you can see you can download

22
00:00:51,50 --> 00:00:55,50
.deb and an .rmp packages for Linux.

23
00:00:55,50 --> 00:00:58,20
But it also installs on Mac and Windows as well.

24
00:00:58,20 --> 00:01:00,70
It's a great development environment.

25
00:01:00,70 --> 00:01:02,80
Now, to create our project, we're going to be

26
00:01:02,80 --> 00:01:06,40
using our own custom version of custom-React-scripts.

27
00:01:06,40 --> 00:01:10,70
We created this in the first video of this course series.

28
00:01:10,70 --> 00:01:13,40
They include a bunch of additional post CSS plug-ins,

29
00:01:13,40 --> 00:01:16,60
as well as to use stage zero and later plug-ins.

30
00:01:16,60 --> 00:01:19,20
So I'm going to click on this here.

31
00:01:19,20 --> 00:01:20,80
I'm going to bring up the actual project.

32
00:01:20,80 --> 00:01:23,30
It's actually going to be located here at

33
00:01:23,30 --> 00:01:31,50
https://github.com/wnow-t4d-io/custom-React-scripts

34
00:01:31,50 --> 00:01:34,70
I'm going to take the URL from here

35
00:01:34,70 --> 00:01:38,00
and we're going to use this with our scripts version option

36
00:01:38,00 --> 00:01:40,40
to actually create our React application.

37
00:01:40,40 --> 00:01:42,60
So I'm going to copy that.

38
00:01:42,60 --> 00:01:47,00
Come over to our terminal window.

39
00:01:47,00 --> 00:01:53,90
I'm going to say npx create-React-app.

40
00:01:53,90 --> 00:01:55,50
We'll call this r.

41
00:01:55,50 --> 00:01:58,20
Actually, we'll call it demo-app.

42
00:01:58,20 --> 00:02:00,30
And then we're going to come into here and say,

43
00:02:00,30 --> 00:02:04,80
scripts-version.

44
00:02:04,80 --> 00:02:11,40
And then we'll do our git+ssh.

45
00:02:11,40 --> 00:02:13,50
And then paste that in there like that.

46
00:02:13,50 --> 00:02:20,40
And now we'll be able to use our custom React scripts.

47
00:02:20,40 --> 00:02:22,30
So now it's using our custom React scripts

48
00:02:22,30 --> 00:02:29,30
and downloading all of the packages that we need.

49
00:02:29,30 --> 00:02:31,30
Okay, so now that we have all of that set up,

50
00:02:31,30 --> 00:02:34,20
let's go ahead and open up our project in our editor.

51
00:02:34,20 --> 00:02:36,30
I'm going to be using Visual Studio Code,

52
00:02:36,30 --> 00:02:38,50
so I'm just going to type in code and then dot

53
00:02:38,50 --> 00:02:40,70
to open up this ticket or folder.

54
00:02:40,70 --> 00:02:41,80
But actually before I do that,

55
00:02:41,80 --> 00:02:44,80
let me actually change into the demo app folder.

56
00:02:44,80 --> 00:02:46,10
Then I'll say code dot,

57
00:02:46,10 --> 00:02:52,60
and we'll fire up Visual Studio Code.

58
00:02:52,60 --> 00:02:54,10
And we'll zoom out a little.

59
00:02:54,10 --> 00:02:55,10
And then what I'm going to do is

60
00:02:55,10 --> 00:02:56,60
actually open up the terminal window.

61
00:02:56,60 --> 00:02:58,60
I can do that by toggling the terminal with

62
00:02:58,60 --> 00:03:01,10
control back-tick, like this.

63
00:03:01,10 --> 00:03:03,10
And I'll get a terminal window.

64
00:03:03,10 --> 00:03:08,20
Or, if you prefer the menu, you can say view, terminal.

65
00:03:08,20 --> 00:03:11,50
Now, once I'm inside of here, I can say npm start.

66
00:03:11,50 --> 00:03:14,70
This will fire up my React application.

67
00:03:14,70 --> 00:03:17,20
And I should now get my

68
00:03:17,20 --> 00:03:20,40
customized version of Create React App uploaded here.

69
00:03:20,40 --> 00:03:21,70
And there we go.

70
00:03:21,70 --> 00:03:24,00
WintellectNOW Custom React S--

