1
00:00:00,20 --> 00:00:06,40
(enchanting piano music)

2
00:00:06,40 --> 00:00:07,20
- [Instructor] To get started here,

3
00:00:07,20 --> 00:00:10,90
let's take a look at a quick overview of using CSS.

4
00:00:10,90 --> 00:00:13,60
There are several ways to actually incorporate CSS

5
00:00:13,60 --> 00:00:15,00
into the user interface.

6
00:00:15,00 --> 00:00:17,00
Some of the ways are standard approaches,

7
00:00:17,00 --> 00:00:18,40
other ways are made available

8
00:00:18,40 --> 00:00:20,60
through modern development tooling.

9
00:00:20,60 --> 00:00:22,60
Each of these approaches has various benefits

10
00:00:22,60 --> 00:00:24,60
and drawbacks depending upon the context

11
00:00:24,60 --> 00:00:26,40
in which it is used.

12
00:00:26,40 --> 00:00:29,60
Fundamentally, to apply any type of style with CSS

13
00:00:29,60 --> 00:00:31,10
requires two pieces of data,

14
00:00:31,10 --> 00:00:34,20
the element to be styled, as well as the style property

15
00:00:34,20 --> 00:00:36,40
and value to be applied.

16
00:00:36,40 --> 00:00:38,60
Now, HTML and React provide several ways

17
00:00:38,60 --> 00:00:41,40
to identify which element is being styled.

18
00:00:41,40 --> 00:00:43,50
One way that we can do it is that we can actually

19
00:00:43,50 --> 00:00:47,50
populate a style attribute on an HTML element,

20
00:00:47,50 --> 00:00:50,10
or a style prop on a React element,

21
00:00:50,10 --> 00:00:52,40
with the actual properties and values

22
00:00:52,40 --> 00:00:54,30
that we want to have applied.

23
00:00:54,30 --> 00:00:57,20
Another approach is to create something called a selector,

24
00:00:57,20 --> 00:00:59,60
and a selector is just a special kind of expression

25
00:00:59,60 --> 00:01:01,70
that can select the DOM object,

26
00:01:01,70 --> 00:01:05,20
so the DOM elements that we want to apply the styling to.

27
00:01:05,20 --> 00:01:07,40
So we can define our properties and values

28
00:01:07,40 --> 00:01:10,00
directly on the style attribute/prop,

29
00:01:10,00 --> 00:01:13,90
or we can apply them through a selector.

30
00:01:13,90 --> 00:01:17,20
Originally, HTML actually had elements

31
00:01:17,20 --> 00:01:20,00
that would apply styling to the document.

32
00:01:20,00 --> 00:01:21,90
Things like the font tag.

33
00:01:21,90 --> 00:01:23,60
But over the course of time, it was determined

34
00:01:23,60 --> 00:01:26,10
that it would be better to actually move the styling

35
00:01:26,10 --> 00:01:28,50
to its own separate markup language,

36
00:01:28,50 --> 00:01:30,40
and allow HTML to focus just on

37
00:01:30,40 --> 00:01:33,80
the sematic structure of the document.

38
00:01:33,80 --> 00:01:36,10
When a web browser loads an HTML document,

39
00:01:36,10 --> 00:01:38,50
it actually parses the HTML elements

40
00:01:38,50 --> 00:01:41,30
and produces something called a DOM tree.

41
00:01:41,30 --> 00:01:43,90
DOM is short for Document Object Model.

42
00:01:43,90 --> 00:01:47,20
The CSS is actually applied to this DOM tree

43
00:01:47,20 --> 00:01:51,00
when the browser renders out the HTML document.

44
00:01:51,00 --> 00:01:54,10
Now, traditionally applying styles to HTML elements

45
00:01:54,10 --> 00:01:56,80
using the style attribute was discouraged.

46
00:01:56,80 --> 00:01:58,60
The reason for this is that,

47
00:01:58,60 --> 00:02:03,40
when you're actually applying the styles on that attribute,

48
00:02:03,40 --> 00:02:05,60
you're actually putting the style information

49
00:02:05,60 --> 00:02:07,60
inside of the document itself,

50
00:02:07,60 --> 00:02:11,10
which prevents the reuse of those styling declarations,

51
00:02:11,10 --> 00:02:13,00
as well as it prevents the overriding

52
00:02:13,00 --> 00:02:17,00
of those styling declarations by other stylesheets.

53
00:02:17,00 --> 00:02:18,90
Also, style values that are stored

54
00:02:18,90 --> 00:02:20,70
in the markup of the HTML document

55
00:02:20,70 --> 00:02:22,20
are not cached by the browser,

56
00:02:22,20 --> 00:02:24,00
which means each time the page is loaded,

57
00:02:24,00 --> 00:02:27,20
the styles have to be reprocessed and applied.

58
00:02:27,20 --> 00:02:30,30
Now, React, with its style prop

59
00:02:30,30 --> 00:02:34,00
allows us to programmatically set those style values

60
00:02:34,00 --> 00:02:37,50
directly on the React element itself.

61
00:02:37,50 --> 00:02:41,80
This is not viewed as bad as doing it with the HTML,

62
00:02:41,80 --> 00:02:45,30
in fact, in some cases, styling your React elements this way

63
00:02:45,30 --> 00:02:47,30
can be quite useful in terms of ensuring

64
00:02:47,30 --> 00:02:49,10
the style being applied is the thing

65
00:02:49,10 --> 00:02:51,30
that is actually going to be applied

66
00:02:51,30 --> 00:02:54,30
regardless of any other stylesheets that might be present.

67
00:02:54,30 --> 00:02:57,20
So it is quite common to see where the style prop

68
00:02:57,20 --> 00:02:59,30
is used in React, but that's only because all of that

69
00:02:59,30 --> 00:03:01,50
is being controlled programmatically,

70
00:03:01,50 --> 00:03:04,30
and we're talking about something called a component,

71
00:03:04,30 --> 00:03:07,50
which is really a piece of reusable logic,

72
00:03:07,50 --> 00:03:10,10
not so much a bunch of static styling

73
00:03:10,10 --> 00:03:15,10
being applied to HTML elements.

74
00:03:15,10 --> 00:03:17,30
Now, another traditional HTML approach

75
00:03:17,30 --> 00:03:19,70
was to use the style element.

76
00:03:19,70 --> 00:03:20,60
Typically this would be put

77
00:03:20,60 --> 00:03:23,00
in the head section of the webpage.

78
00:03:23,00 --> 00:03:25,60
And the style element allowed the use of selectors,

79
00:03:25,60 --> 00:03:29,20
so we were moving the styles from the individual elements

80
00:03:29,20 --> 00:03:33,00
up into this stylesheet that's inside of the style tag,

81
00:03:33,00 --> 00:03:36,90
but the problem is that while the styles can be overwritten,

82
00:03:36,90 --> 00:03:40,90
the actual styles themselves are still in the HTML document,

83
00:03:40,90 --> 00:03:44,20
and are not cached by the web browser.

84
00:03:44,20 --> 00:03:45,80
Now, you will find that third-party

85
00:03:45,80 --> 00:03:48,10
development tools like Webpack

86
00:03:48,10 --> 00:03:49,90
actually do make used of style elements,

87
00:03:49,90 --> 00:03:51,80
but not for the same reasons that people

88
00:03:51,80 --> 00:03:54,70
would do static HTML coding with them.

89
00:03:54,70 --> 00:03:56,60
They would actually use the style elements

90
00:03:56,60 --> 00:03:59,80
to support dynamic reloading of the styles

91
00:03:59,80 --> 00:04:02,90
as the developer is making changes to the stylesheets.

92
00:04:02,90 --> 00:04:06,20
This hot-reloading is not actually done in production,

93
00:04:06,20 --> 00:04:08,50
production is still an external stylesheet,

94
00:04:08,50 --> 00:04:10,60
but in terms of developing the web page,

95
00:04:10,60 --> 00:04:13,00
you'll see these style elements pop up.

96
00:04:13,00 --> 00:04:15,40
The usage of style elements in this way

97
00:04:15,40 --> 00:04:18,10
is just really a development tooling thing,

98
00:04:18,10 --> 00:04:19,80
it's not the same type of thing

99
00:04:19,80 --> 00:04:22,30
as putting a style element itself on the page,

100
00:04:22,30 --> 00:04:27,60
which is generally regarded as a bad practice.

101
00:04:27,60 --> 00:04:31,10
Another way that we can incorporate styles into our page

102
00:04:31,10 --> 00:04:32,80
is to actually take our styles

103
00:04:32,80 --> 00:04:35,00
and put them into an external stylesheet,

104
00:04:35,00 --> 00:04:37,60
and then link them into our HTML document

105
00:04:37,60 --> 00:04:39,30
using the link element.

106
00:04:39,30 --> 00:04:40,70
Typically, the link element's placed

107
00:04:40,70 --> 00:04:43,40
into the HTML document head,

108
00:04:43,40 --> 00:04:45,90
and when the web browser loads the HTML page,

109
00:04:45,90 --> 00:04:48,60
it actually downloads this linked stylesheet,

110
00:04:48,60 --> 00:04:50,70
processes it, and caches it,

111
00:04:50,70 --> 00:04:52,90
so that if the HTML document is reloaded,

112
00:04:52,90 --> 00:04:55,50
it can reuse that stylesheet.

113
00:04:55,50 --> 00:04:56,90
This stylesheet's going to be a collection

114
00:04:56,90 --> 00:04:59,40
of CSS rule selectors with style declarations

115
00:04:59,40 --> 00:05:01,40
just like the style element,

116
00:05:01,40 --> 00:05:04,20
but it's all stored in an external file.

117
00:05:04,20 --> 00:05:07,20
Now, you can actually pull in many stylesheets

118
00:05:07,20 --> 00:05:09,30
through the use of multiple link elements.

119
00:05:09,30 --> 00:05:10,90
In production, we tend to avoid this,

120
00:05:10,90 --> 00:05:13,40
we tend to put all the stylesheets into one file.

121
00:05:13,40 --> 00:05:14,60
But you could actually have

122
00:05:14,60 --> 00:05:18,60
multiple stylesheets get loaded up.

123
00:05:18,60 --> 00:05:19,90
Another option that we have

124
00:05:19,90 --> 00:05:22,40
when working with tools like React and Webpack

125
00:05:22,40 --> 00:05:24,40
is that we can actually define stylesheets

126
00:05:24,40 --> 00:05:26,20
per React component.

127
00:05:26,20 --> 00:05:27,80
In fact, there's actually two ways

128
00:05:27,80 --> 00:05:29,30
in which this is supported.

129
00:05:29,30 --> 00:05:32,00
One is using something called CSS modules.

130
00:05:32,00 --> 00:05:36,90
Another is just simply importing an actual CSS file.

131
00:05:36,90 --> 00:05:39,30
Through the use of plugins like PostCSS,

132
00:05:39,30 --> 00:05:43,00
we actually will use the JavaScript syntax

133
00:05:43,00 --> 00:05:45,50
for importing ES2015 modules,

134
00:05:45,50 --> 00:05:48,80
to actually import these external stylesheets

135
00:05:48,80 --> 00:05:51,90
directly into our React component JavaScript files.

136
00:05:51,90 --> 00:05:54,90
This allows us to co-locate our style definitions

137
00:05:54,90 --> 00:05:57,20
with our actual React component.

138
00:05:57,20 --> 00:06:00,00
Now, when these React components run in development mode,

139
00:06:00,00 --> 00:06:01,50
these same styles will be

140
00:06:01,50 --> 00:06:04,30
loaded into style elements for hot-reloading,

141
00:06:04,30 --> 00:06:06,30
and in production mode, these same styles

142
00:06:06,30 --> 00:06:09,00
will actually all be put out into a single CSS file,

143
00:06:09,00 --> 00:06:11,60
which is then referenced via a link element

144
00:06:11,60 --> 00:06:16,50
in the main index.html's head section.

145
00:06:16,50 --> 00:06:18,20
Now, in addition to having stylesheets

146
00:06:18,20 --> 00:06:21,00
which get linked into our webpage,

147
00:06:21,00 --> 00:06:22,50
we also have to be able to connect

148
00:06:22,50 --> 00:06:25,40
these style declarations, those properties and values,

149
00:06:25,40 --> 00:06:27,40
to the elements that need to be styled.

150
00:06:27,40 --> 00:06:30,20
There are roughly three kinds of selectors used in CSS:

151
00:06:30,20 --> 00:06:33,50
Simple, compound, and complex selectors.

152
00:06:33,50 --> 00:06:35,40
The three most common simple selectors

153
00:06:35,40 --> 00:06:37,40
allow the selection of an element by its type,

154
00:06:37,40 --> 00:06:42,60
or also known as its tag name, its ID, and class name.

155
00:06:42,60 --> 00:06:45,00
Now, when multiple simple selectors

156
00:06:45,00 --> 00:06:48,50
are used in conjunction with each other to select elements,

157
00:06:48,50 --> 00:06:50,50
this is known as a compound selector,

158
00:06:50,50 --> 00:06:51,90
so if you were to use, for example,

159
00:06:51,90 --> 00:06:56,60
a selector that included both the type and the class name

160
00:06:56,60 --> 00:06:58,50
together as a single selector,

161
00:06:58,50 --> 00:07:01,10
this would be known as a compound selector.

162
00:07:01,10 --> 00:07:02,30
Now, there are other selectors

163
00:07:02,30 --> 00:07:04,60
which allow selecting by element attributes,

164
00:07:04,60 --> 00:07:07,00
the structural relationship between elements,

165
00:07:07,00 --> 00:07:09,50
and even information about elements which exist

166
00:07:09,50 --> 00:07:10,70
outside the DOM structure,

167
00:07:10,70 --> 00:07:13,20
such as which element has the focus.

168
00:07:13,20 --> 00:07:15,70
Many aspects of an element can be styled.

169
00:07:15,70 --> 00:07:17,70
We can style the color of the fonts, the borders,

170
00:07:17,70 --> 00:07:21,30
positioning, transformations, animations, et cetera.

171
00:07:21,30 --> 00:07:26,00
A style declaration is a colon-separated name value pair,

172
00:07:26,00 --> 00:07:29,60
so for example, if we have a paragraph of text,

173
00:07:29,60 --> 00:07:31,10
to set the color of that text,

174
00:07:31,10 --> 00:07:33,70
the name of the property that you set is called color,

175
00:07:33,70 --> 00:07:36,50
and then the value would be some type of color value

176
00:07:36,50 --> 00:07:38,80
that would then set the color of the text of that paragraph

177
00:07:38,80 --> 00:07:40,90
to some particular color.

178
00:07:40,90 --> 00:07:43,60
A CSS rule is a combination of a selector,

179
00:07:43,60 --> 00:07:45,80
and one or more style declarations.

180
00:07:45,80 --> 00:07:47,60
And you can have multiple style declarations

181
00:07:47,60 --> 00:07:49,50
within that selector's scope,

182
00:07:49,50 --> 00:07:51,00
and those are separated by a semicolon.

