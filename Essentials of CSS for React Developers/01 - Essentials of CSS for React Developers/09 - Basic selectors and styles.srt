1
00:00:00,00 --> 00:00:06,60
(calming music)

2
00:00:06,60 --> 00:00:08,20
- [Instructor] Once we have our style sheets

3
00:00:08,20 --> 00:00:10,00
imported into our application,

4
00:00:10,00 --> 00:00:12,60
we need to be able to connect the styles

5
00:00:12,60 --> 00:00:15,50
to the actual elements that need the styling.

6
00:00:15,50 --> 00:00:16,50
We're going to do this by using

7
00:00:16,50 --> 00:00:18,20
something called a selector.

8
00:00:18,20 --> 00:00:20,30
The selector does exactly what it says.

9
00:00:20,30 --> 00:00:23,30
It selects the elements, that need to be styled.

10
00:00:23,30 --> 00:00:25,60
And then has associated with it a number

11
00:00:25,60 --> 00:00:28,70
of different style declarations with the actual styles

12
00:00:28,70 --> 00:00:30,40
that are being applied.

13
00:00:30,40 --> 00:00:33,20
Using selectors is better than the style attribute

14
00:00:33,20 --> 00:00:36,20
because you can actually override styles,

15
00:00:36,20 --> 00:00:39,20
as well as move your style declarations

16
00:00:39,20 --> 00:00:41,90
to an external file.

17
00:00:41,90 --> 00:00:44,80
Now, there are many kinds of selectors,

18
00:00:44,80 --> 00:00:46,90
roughly organized into three categories,

19
00:00:46,90 --> 00:00:50,20
simple, compound and complex.

20
00:00:50,20 --> 00:00:52,30
Compound selectors are nothing more than

21
00:00:52,30 --> 00:00:54,60
a combination of simple selectors.

22
00:00:54,60 --> 00:00:56,90
And complex selectors are nothing more

23
00:00:56,90 --> 00:01:00,10
than a combination of compound selectors,

24
00:01:00,10 --> 00:01:04,40
that are set up with special rules called combinators.

25
00:01:04,40 --> 00:01:06,10
Now, in this particular course,

26
00:01:06,10 --> 00:01:07,50
we're going to take a look at a subset

27
00:01:07,50 --> 00:01:10,60
of the most important simple and compound selectors.

28
00:01:10,60 --> 00:01:12,30
We're going to take a look at the type selector,

29
00:01:12,30 --> 00:01:16,00
which is basically selecting by tag or element name.

30
00:01:16,00 --> 00:01:18,60
We're going to take a look at selecting

31
00:01:18,60 --> 00:01:21,60
based upon class and you can specify multiple classes,

32
00:01:21,60 --> 00:01:24,70
that would be an example of a compound selector

33
00:01:24,70 --> 00:01:26,50
or selecting based upon ID.

34
00:01:26,50 --> 00:01:29,70
Each element in the webpage can have a unique ID

35
00:01:29,70 --> 00:01:31,80
that you can use for selecting.

36
00:01:31,80 --> 00:01:33,20
Now, the next course in the series

37
00:01:33,20 --> 00:01:35,70
is actually going to explore a much richer set

38
00:01:35,70 --> 00:01:38,80
of selectors, than what we're covering here.

39
00:01:38,80 --> 00:01:41,10
Selectors are defined in their own CSS Selector

40
00:01:41,10 --> 00:01:43,90
specification, the current version is version three.

41
00:01:43,90 --> 00:01:46,40
Version four of CSS Selectors is still a W3C

42
00:01:46,40 --> 00:01:49,10
Working Draft but we will be using that

43
00:01:49,10 --> 00:01:52,50
in this particular course.

44
00:01:52,50 --> 00:01:55,30
Now, in addition to selectors, there are

45
00:01:55,30 --> 00:01:57,50
CSS style declarations.

46
00:01:57,50 --> 00:01:59,40
So the selector is what selects the elements

47
00:01:59,40 --> 00:02:01,20
but the style declarations are what tell us

48
00:02:01,20 --> 00:02:03,80
what styles to apply to all of the elements

49
00:02:03,80 --> 00:02:05,60
matching the selector.

50
00:02:05,60 --> 00:02:08,10
A declaration is a pairing of a CSS property

51
00:02:08,10 --> 00:02:11,10
and a value separated by a colon.

52
00:02:11,10 --> 00:02:13,10
Now, you can have a list of style declarations

53
00:02:13,10 --> 00:02:15,80
associated with a particular selector.

54
00:02:15,80 --> 00:02:19,30
And that list of style declarations

55
00:02:19,30 --> 00:02:21,90
is delimited by a semi-colon.

56
00:02:21,90 --> 00:02:25,20
There are literally hundreds of style properties in CSS.

57
00:02:25,20 --> 00:02:27,30
In this course, a few of the more common ones

58
00:02:27,30 --> 00:02:30,40
will be introduced.

59
00:02:30,40 --> 00:02:32,60
So taking a look at some of the most essential

60
00:02:32,60 --> 00:02:36,10
selectors here, we have the type, class and ID.

61
00:02:36,10 --> 00:02:39,30
These are probably the three most important

62
00:02:39,30 --> 00:02:40,60
selectors, that you'll work with,

63
00:02:40,60 --> 00:02:42,80
in terms of working with CSS.

64
00:02:42,80 --> 00:02:45,30
To select something by its type or its element

65
00:02:45,30 --> 00:02:47,80
or tag name, you simply just use the tag name.

66
00:02:47,80 --> 00:02:49,10
So in this case, we're going to select

67
00:02:49,10 --> 00:02:51,10
all of the divs on a page.

68
00:02:51,10 --> 00:02:52,60
If you wanted to select all of the paragraphs,

69
00:02:52,60 --> 00:02:55,10
you would have a P instead of a div.

70
00:02:55,10 --> 00:02:58,60
The class selector, you denote that with a period

71
00:02:58,60 --> 00:02:59,60
in front of a class name.

72
00:02:59,60 --> 00:03:01,20
So if I want to select all the elements

73
00:03:01,20 --> 00:03:04,20
with an info class, I could say dot info.

74
00:03:04,20 --> 00:03:06,60
And then if I want to select something by ID,

75
00:03:06,60 --> 00:03:09,10
I could say hashtag and the ID, in this case

76
00:03:09,10 --> 00:03:10,60
hashtag message.

77
00:03:10,60 --> 00:03:14,20
Now, the ID should only exist once in the DOM structure.

78
00:03:14,20 --> 00:03:15,70
So when you select something by ID,

79
00:03:15,70 --> 00:03:17,90
you're being very specific and only choosing

80
00:03:17,90 --> 00:03:20,00
that one particular element.

81
00:03:20,00 --> 00:03:21,70
And of course, you'll see next to each selector,

82
00:03:21,70 --> 00:03:24,00
we have curly braces and then inside of the curly braces,

83
00:03:24,00 --> 00:03:26,30
we have the actual style declarations

84
00:03:26,30 --> 00:03:28,00
associated with that selector.

85
00:03:28,00 --> 00:03:29,70
In this case, we're making everything

86
00:03:29,70 --> 00:03:33,40
have the color of blue.

87
00:03:33,40 --> 00:03:35,10
Now we can have compound selectors

88
00:03:35,10 --> 00:03:36,90
when we take multiple simple selectors

89
00:03:36,90 --> 00:03:38,50
and combine them together.

90
00:03:38,50 --> 00:03:39,50
For example, I could say,

91
00:03:39,50 --> 00:03:42,30
"All the div elements with the info class

92
00:03:42,30 --> 00:03:44,90
or the div element, that has an ID of message,

93
00:03:44,90 --> 00:03:48,80
or the element with a ID of message

94
00:03:48,80 --> 00:03:51,20
that also has the info class applied."

95
00:03:51,20 --> 00:03:52,80
Or I could say, "The div element

96
00:03:52,80 --> 00:03:55,90
with an ID of message and a class of info."

97
00:03:55,90 --> 00:03:57,80
So you could combine all of these together

98
00:03:57,80 --> 00:03:59,90
to create compound selectors.

99
00:03:59,90 --> 00:04:04,80
Now, you can only have one type selector

100
00:04:04,80 --> 00:04:06,10
in the actual selector.

101
00:04:06,10 --> 00:04:07,70
You can actually have zero type selectors

102
00:04:07,70 --> 00:04:08,90
or you can have one.

103
00:04:08,90 --> 00:04:11,70
You cannot have multiple type selectors.

104
00:04:11,70 --> 00:04:15,30
The ID should only be one ID selector

105
00:04:15,30 --> 00:04:17,70
because you can't have multiple ID's

106
00:04:17,70 --> 00:04:19,00
on the same element.

107
00:04:19,00 --> 00:04:21,60
But you can have many class selectors.

108
00:04:21,60 --> 00:04:23,90
So it's very common to have many class selectors

109
00:04:23,90 --> 00:04:25,70
and actually add them together.

110
00:04:25,70 --> 00:04:27,80
That becomes important in something called selector

111
00:04:27,80 --> 00:04:33,30
specificity, which we cover in a future course.

112
00:04:33,30 --> 00:04:35,40
Now, here's a big, long list of some basic styles,

113
00:04:35,40 --> 00:04:37,40
that everybody can use on a webpage,

114
00:04:37,40 --> 00:04:40,10
to start adding some color and some clarity

115
00:04:40,10 --> 00:04:42,10
to the content being presented.

116
00:04:42,10 --> 00:04:43,60
For example, if you want to set the text color,

117
00:04:43,60 --> 00:04:46,30
the name of the CSS property is color.

118
00:04:46,30 --> 00:04:49,10
And then you can assign it, one of the HTML color names,

119
00:04:49,10 --> 00:04:50,90
a hex code, what have you.

120
00:04:50,90 --> 00:04:53,70
Background color and it'll set the background color

121
00:04:53,70 --> 00:04:56,00
of whatever the element is that you're styling.

122
00:04:56,00 --> 00:04:57,50
A number of different things you can set

123
00:04:57,50 --> 00:05:00,10
for font, the family of font, like arial,

124
00:05:00,10 --> 00:05:02,20
the font size like 16 pixels,

125
00:05:02,20 --> 00:05:04,70
the weight, whether you want it bold or normal.

126
00:05:04,70 --> 00:05:07,40
The font style, do you want just regular characters

127
00:05:07,40 --> 00:05:09,40
or do you want them to be italic?

128
00:05:09,40 --> 00:05:10,80
You can add decorations like

129
00:05:10,80 --> 00:05:12,80
strike throughs and underlines.

130
00:05:12,80 --> 00:05:15,00
And you can also put borders around certain elements

131
00:05:15,00 --> 00:05:16,90
and set the border style to solid

132
00:05:16,90 --> 00:05:19,00
or you can have like a dash line if you like that

133
00:05:19,00 --> 00:05:20,60
or a border with the two pixels

134
00:05:20,60 --> 00:05:22,30
and a border color of green.

135
00:05:22,30 --> 00:05:23,70
You can even set border radius

136
00:05:23,70 --> 00:05:25,40
and have curved borders.

137
00:05:25,40 --> 00:05:27,20
All types of different things that you can set

138
00:05:27,20 --> 00:05:29,00
to add a little bit of style and pizazz.

