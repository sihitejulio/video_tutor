1
00:00:01,30 --> 00:00:02,40
- [Instructor] Let's do a demonstration now

2
00:00:02,40 --> 00:00:05,90
of applying styling to HTML and React Elements.

3
00:00:05,90 --> 00:00:07,20
Let's go ahead now and take a look

4
00:00:07,20 --> 00:00:09,00
at how we can apply some styles to

5
00:00:09,00 --> 00:00:12,70
our HTML elements as well as our React elements.

6
00:00:12,70 --> 00:00:15,40
Now what we're going to do in this particular demonstration

7
00:00:15,40 --> 00:00:18,20
is not necessarily a best practice always,

8
00:00:18,20 --> 00:00:19,70
but it's going to be a demonstration

9
00:00:19,70 --> 00:00:21,90
of how you can actually set those style attributes

10
00:00:21,90 --> 00:00:24,70
and kind of see how the code works and looks.

11
00:00:24,70 --> 00:00:26,70
So inside of our source folder here

12
00:00:26,70 --> 00:00:29,70
we have our index.js code.

13
00:00:29,70 --> 00:00:31,00
And what I'd like to do is actually

14
00:00:31,00 --> 00:00:33,20
just kind of comment all of this out.

15
00:00:33,20 --> 00:00:35,20
I'll just kind of make all that go away.

16
00:00:35,20 --> 00:00:38,10
And we're going to hop up here into our public folder

17
00:00:38,10 --> 00:00:40,70
where we have our index.html file.

18
00:00:40,70 --> 00:00:42,10
And we're going to do all of our coding here

19
00:00:42,10 --> 00:00:45,00
directly inside of our index.html.

20
00:00:45,00 --> 00:00:46,90
Now we have a lot of code in this file

21
00:00:46,90 --> 00:00:48,10
that's not really doing anything.

22
00:00:48,10 --> 00:00:49,30
Like all of these comments.

23
00:00:49,30 --> 00:00:52,50
And I'm going to go ahead and just take those out.

24
00:00:52,50 --> 00:00:54,30
And we'll get rid of the comments.

25
00:00:54,30 --> 00:00:56,00
That'll make things a little smaller.

26
00:00:56,00 --> 00:00:57,90
Down here where we have our noscript tag,

27
00:00:57,90 --> 00:01:00,20
this is generally a best practice to have that in there,

28
00:01:00,20 --> 00:01:02,50
but I'm going to go ahead and take that out anyways

29
00:01:02,50 --> 00:01:04,30
'cause we're not going to be running this

30
00:01:04,30 --> 00:01:08,00
without JavaScript being enabled in our web browser at all.

31
00:01:08,00 --> 00:01:09,20
And I'm going to come down here

32
00:01:09,20 --> 00:01:12,20
and I'm going to get rid of these comments as well.

33
00:01:12,20 --> 00:01:14,60
Now what we want to do is take a look at what it looks like

34
00:01:14,60 --> 00:01:18,40
to actually specify some styles directly inside

35
00:01:18,40 --> 00:01:20,70
of the actual style attribute.

36
00:01:20,70 --> 00:01:22,90
So I'm going to get rid of the div id root

37
00:01:22,90 --> 00:01:24,00
and we'll add that back later

38
00:01:24,00 --> 00:01:26,90
when we're ready to fire up a React application.

39
00:01:26,90 --> 00:01:28,30
And I'm going to go ahead and put

40
00:01:28,30 --> 00:01:31,50
a div tag in here with some content.

41
00:01:31,50 --> 00:01:32,90
And I'm going to do a little styling on it.

42
00:01:32,90 --> 00:01:34,80
So I'm going to say style

43
00:01:34,80 --> 00:01:39,80
and then I can say color equal to blue.

44
00:01:39,80 --> 00:01:41,20
There we go.

45
00:01:41,20 --> 00:01:42,20
So we'll save that.

46
00:01:42,20 --> 00:01:45,60
And we're going to flip back over to our web browser here.

47
00:01:45,60 --> 00:01:48,40
And you will see that we have Some Content,

48
00:01:48,40 --> 00:01:50,10
which is now blue.

49
00:01:50,10 --> 00:01:52,30
If we right click on this and inspect it

50
00:01:52,30 --> 00:01:54,60
we can use our developer tools

51
00:01:54,60 --> 00:01:56,80
to kind of inspect this a little closer.

52
00:01:56,80 --> 00:02:01,10
And let's see, we'll move this actually down to the bottom.

53
00:02:01,10 --> 00:02:02,50
We'll zoom in a little.

54
00:02:02,50 --> 00:02:05,00
So you can see directly here on our element.style

55
00:02:05,00 --> 00:02:06,80
it actually says blue.

56
00:02:06,80 --> 00:02:08,90
So this is the user agent stylesheet here.

57
00:02:08,90 --> 00:02:12,30
This is the actual default style sheet that's applied

58
00:02:12,30 --> 00:02:15,40
to the actual page by the web browser.

59
00:02:15,40 --> 00:02:17,10
You'll notice the user agent stylesheet

60
00:02:17,10 --> 00:02:18,10
says nothing about color.

61
00:02:18,10 --> 00:02:20,20
It just talks about display block,

62
00:02:20,20 --> 00:02:22,80
which we'll cover later in a future course.

63
00:02:22,80 --> 00:02:25,80
But here our element style has blue assigned to it.

64
00:02:25,80 --> 00:02:29,30
If we were to uncheck this you can see the blue goes away

65
00:02:29,30 --> 00:02:31,40
and then if we check it again

66
00:02:31,40 --> 00:02:34,30
you can see the blue comes back.

67
00:02:34,30 --> 00:02:36,40
We can actually come into here and we can do some editing.

68
00:02:36,40 --> 00:02:40,40
We could say text-decoration.

69
00:02:40,40 --> 00:02:43,70
And then we could do underline.

70
00:02:43,70 --> 00:02:46,40
So now you can see now we have an underline blue text.

71
00:02:46,40 --> 00:02:49,80
And we're actually editing this style element here.

72
00:02:49,80 --> 00:02:51,90
Now editing the style element,

73
00:02:51,90 --> 00:02:52,70
well actually coding

74
00:02:52,70 --> 00:02:55,40
with the style element is a bad practice.

75
00:02:55,40 --> 00:02:57,50
And you can do editing on it if you'd like

76
00:02:57,50 --> 00:02:59,30
to see what a particular style would look like

77
00:02:59,30 --> 00:03:03,20
without having to worry about conflicting with other styles.

78
00:03:03,20 --> 00:03:05,80
But this is not a generally a best practice

79
00:03:05,80 --> 00:03:07,50
to come in here and be hard coding

80
00:03:07,50 --> 00:03:09,30
the style attribute like we are.

81
00:03:09,30 --> 00:03:11,80
But you can see we do have the ability to change the style

82
00:03:11,80 --> 00:03:14,10
and make things look blue in color.

83
00:03:14,10 --> 00:03:17,50
So that's what it looks like to use

84
00:03:17,50 --> 00:03:20,10
the actual style attribute directly.

85
00:03:20,10 --> 00:03:23,10
What we're going to do now is we're going to work

86
00:03:23,10 --> 00:03:26,10
with our style prop and actually style a React element.

87
00:03:26,10 --> 00:03:28,10
So I'm going to say main.

88
00:03:28,10 --> 00:03:29,50
I like to use the main element

89
00:03:29,50 --> 00:03:32,20
to bootstrap my document against,

90
00:03:32,20 --> 00:03:34,70
or my React application against.

91
00:03:34,70 --> 00:03:36,70
So we're going to come back over to index,

92
00:03:36,70 --> 00:03:39,00
and I'm going to uncomment some of this.

93
00:03:39,00 --> 00:03:42,10
I'm going to uncomment this part.

94
00:03:42,10 --> 00:03:45,10
And I'm going to uncomment this part.

95
00:03:45,10 --> 00:03:46,10
And I am actually going to leave

96
00:03:46,10 --> 00:03:48,40
the style sheet itself commented out.

97
00:03:48,40 --> 00:03:52,00
I do need to change my code here.

98
00:03:52,00 --> 00:03:53,70
Instead of grabbing it by the root

99
00:03:53,70 --> 00:03:58,70
I'm going to actually say document.querySelector.

100
00:03:58,70 --> 00:04:03,50
And I'm going to query this based upon the actual main element.

101
00:04:03,50 --> 00:04:06,90
querySelector allows us to use the CSS selector engine

102
00:04:06,90 --> 00:04:08,70
within our JavaScript code.

103
00:04:08,70 --> 00:04:11,20
We actually cover this feature in a later course as well.

104
00:04:11,20 --> 00:04:14,20
And you can use any CSS selector that you want here

105
00:04:14,20 --> 00:04:16,50
to actually select the element.

106
00:04:16,50 --> 00:04:19,10
So instead of App, I'm going to come over to App here,

107
00:04:19,10 --> 00:04:24,60
and I'm going to get rid of all of this default content.

108
00:04:24,60 --> 00:04:28,20
And inside of here

109
00:04:28,20 --> 00:04:31,60
we're going to make use of a new,

110
00:04:31,60 --> 00:04:35,20
a new element called Demo, our new component.

111
00:04:35,20 --> 00:04:39,70
And there'll be our Demo.

112
00:04:39,70 --> 00:04:42,20
And I'm going to come over to here and create a new file

113
00:04:42,20 --> 00:04:45,80
called demo.js.

114
00:04:45,80 --> 00:04:47,70
And then here from my demo.js

115
00:04:47,70 --> 00:04:52,70
I'm going to say import React from react,

116
00:04:52,70 --> 00:04:58,30
export const Demo,

117
00:04:58,30 --> 00:05:04,00
and then here we'll have our return div style.

118
00:05:04,00 --> 00:05:11,70
And then I'm going to say color blue.

119
00:05:11,70 --> 00:05:13,60
And then I'm also going to say,

120
00:05:13,60 --> 00:05:16,40
actually put that in quotes, blue.

121
00:05:16,40 --> 00:05:20,60
And then we'll do textDecoration,

122
00:05:20,60 --> 00:05:23,60
and we'll say underline.

123
00:05:23,60 --> 00:05:25,00
There we go.

124
00:05:25,00 --> 00:05:28,20
And then here we'll go ahead and close that off.

125
00:05:28,20 --> 00:05:35,00
And we'll just simply say props.children.

126
00:05:35,00 --> 00:05:40,40
And then up here I'll specify props.

127
00:05:40,40 --> 00:05:41,30
There we go.

128
00:05:41,30 --> 00:05:42,70
So there's props.children

129
00:05:42,70 --> 00:05:47,30
being passed into my div with my style settings there.

130
00:05:47,30 --> 00:05:49,00
We'll come back over to App.

131
00:05:49,00 --> 00:05:50,70
We're going to get rid of the logo.

132
00:05:50,70 --> 00:05:52,90
We don't need that.

133
00:05:52,90 --> 00:05:56,20
We're also going to take out the style sheet here for App j,

134
00:05:56,20 --> 00:05:58,40
App.css, we don't need that.

135
00:05:58,40 --> 00:06:02,10
But I do need to import the actual Demo component

136
00:06:02,10 --> 00:06:04,70
from ./Demo.

137
00:06:04,70 --> 00:06:08,20
So now my App is going to call Demo here and render that out.

138
00:06:08,20 --> 00:06:10,50
And then inside of Demo I'm using a style prop

139
00:06:10,50 --> 00:06:12,10
to set the color as blue

140
00:06:12,10 --> 00:06:14,70
and the textDeocration as underline.

141
00:06:14,70 --> 00:06:20,90
So if I come back over to my webpage here.

142
00:06:20,90 --> 00:06:23,10
We'll come in, ope, I got to do one other thing.

143
00:06:23,10 --> 00:06:25,60
Make sure on my App when I call Demo

144
00:06:25,60 --> 00:06:29,40
we'll actually put some Content in here like this.

145
00:06:29,40 --> 00:06:31,30
There we go.

146
00:06:31,30 --> 00:06:33,80
And then come back over to here and you can see once again

147
00:06:33,80 --> 00:06:35,10
we have this styled.

148
00:06:35,10 --> 00:06:37,40
And if we come into main you can see there's our div.

149
00:06:37,40 --> 00:06:39,00
And it actually applied this

150
00:06:39,00 --> 00:06:40,80
directly to the style attribute.

151
00:06:40,80 --> 00:06:44,20
Now remember in this case we're not the ones directly coding

152
00:06:44,20 --> 00:06:46,40
the style attribute on the element.

153
00:06:46,40 --> 00:06:50,10
We're using, we're using JavaScript logic here

154
00:06:50,10 --> 00:06:52,20
to actually apply this object.

155
00:06:52,20 --> 00:06:53,60
So for example, I could come out to here

156
00:06:53,60 --> 00:06:57,10
and say const styles,

157
00:06:57,10 --> 00:07:01,80
and then I could take this object here,

158
00:07:01,80 --> 00:07:06,40
assign it like that, and then simply just take styles

159
00:07:06,40 --> 00:07:07,70
and pass it like that and now

160
00:07:07,70 --> 00:07:10,00
I'm passing the styles that way.

161
00:07:10,00 --> 00:07:12,00
So now if I come back in here and reload this

162
00:07:12,00 --> 00:07:14,00
you'll see I get the same result.

163
00:07:14,00 --> 00:07:16,10
So I can programmatically determine

164
00:07:16,10 --> 00:07:18,00
what I want this to be.

165
00:07:18,00 --> 00:07:20,50
For example, I could come into here and I could say,

166
00:07:20,50 --> 00:07:25,50
you know what, make the text color equal to blue.

167
00:07:25,50 --> 00:07:27,00
And then I can come back over to Demo

168
00:07:27,00 --> 00:07:34,30
and I could say, okay, this is going to be props.textColor.

169
00:07:34,30 --> 00:07:36,60
So if I go back to here and reload you'll see

170
00:07:36,60 --> 00:07:38,00
there is some content blue.

171
00:07:38,00 --> 00:07:41,40
But I could come into here and I could say,

172
00:07:41,40 --> 00:07:44,80
you know what, let's make this be red.

173
00:07:44,80 --> 00:07:46,50
And now it's red.

174
00:07:46,50 --> 00:07:49,70
So even though I am setting the actual style prop

175
00:07:49,70 --> 00:07:51,60
you can see it's very dynamic in terms

176
00:07:51,60 --> 00:07:54,20
of how those styles are actually being set.

177
00:07:54,20 --> 00:07:56,10
If there are other style sheets on the page,

178
00:07:56,10 --> 00:07:58,00
of course these styles will override them

179
00:07:58,00 --> 00:08:00,90
because they are, in fact, being applied ultimately

180
00:08:00,90 --> 00:08:03,30
to the style attribute of the dom element.

181
00:08:03,30 --> 00:08:06,50
Nevertheless, from a JavaScript programming standpoint

182
00:08:06,50 --> 00:08:10,00
I have the ability to change these values at runtime.

