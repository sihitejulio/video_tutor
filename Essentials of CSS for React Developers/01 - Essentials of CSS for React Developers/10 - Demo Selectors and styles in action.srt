1
00:00:02,30 --> 00:00:03,40
- [Instructor] Let's do a demonstration now

2
00:00:03,40 --> 00:00:06,70
of using essential selectors and styles.

3
00:00:06,70 --> 00:00:08,30
Let's go ahead and take a look at some basic

4
00:00:08,30 --> 00:00:13,50
simple selectors, as well as do some basic styling.

5
00:00:13,50 --> 00:00:16,30
So we have our demo component here,

6
00:00:16,30 --> 00:00:18,00
and what I'm going to do is I'm actually going to

7
00:00:18,00 --> 00:00:26,60
wrap this in react dot fragment, there we go.

8
00:00:26,60 --> 00:00:30,10
Then I'm going to take this div and pop this in there like now.

9
00:00:30,10 --> 00:00:32,90
And we're just going to have a couple of different divs.

10
00:00:32,90 --> 00:00:36,50
We'll have that div, and then we'll give a class name

11
00:00:36,50 --> 00:00:39,20
on this one.

12
00:00:39,20 --> 00:00:42,40
Whenever you set a class in react JSX,

13
00:00:42,40 --> 00:00:44,70
you have to call it class name instead of class,

14
00:00:44,70 --> 00:00:47,40
and we'll just call this info.

15
00:00:47,40 --> 00:00:48,70
And then I'm going to change the ID of this one

16
00:00:48,70 --> 00:00:51,80
to be message.

17
00:00:51,80 --> 00:00:54,10
And we'll keep our text the same in all three cases,

18
00:00:54,10 --> 00:00:55,80
doesn't matter what the actual content is.

19
00:00:55,80 --> 00:00:56,80
But we're going to style these differently

20
00:00:56,80 --> 00:00:59,60
based upon different selectors.

21
00:00:59,60 --> 00:01:02,20
So up here in our demo CSS,

22
00:01:02,20 --> 00:01:04,90
right now all of the text should be red,

23
00:01:04,90 --> 00:01:06,40
actually let's change that to blue,

24
00:01:06,40 --> 00:01:08,80
I like blue better, there we go.

25
00:01:08,80 --> 00:01:11,10
And I'm going to stop our production version.

26
00:01:11,10 --> 00:01:15,40
We're going to get a website and restart our development,

27
00:01:15,40 --> 00:01:19,00
build, there we go.

28
00:01:19,00 --> 00:01:21,90
And we'll close these other windows.

29
00:01:21,90 --> 00:01:24,20
So you can see all three pieces of content are blue.

30
00:01:24,20 --> 00:01:26,00
So if we actually come in here and inspect this,

31
00:01:26,00 --> 00:01:28,80
you can see that this one,

32
00:01:28,80 --> 00:01:32,60
these are all blue because of the div selector.

33
00:01:32,60 --> 00:01:34,60
Now what I'm going to do is I'm going to come into here

34
00:01:34,60 --> 00:01:39,50
and I'm going to specify the class selector.

35
00:01:39,50 --> 00:01:42,60
So we'll do one for info, and I'm going to say

36
00:01:42,60 --> 00:01:45,40
color is green.

37
00:01:45,40 --> 00:01:47,40
Come back over to our page and you'll notice

38
00:01:47,40 --> 00:01:50,10
the second one now has a green color.

39
00:01:50,10 --> 00:01:52,50
And you'll see we have class info on that second one,

40
00:01:52,50 --> 00:01:53,90
and if you actually click on this,

41
00:01:53,90 --> 00:01:57,20
you can see where the dot info color green overrides

42
00:01:57,20 --> 00:01:59,00
the div color blue.

43
00:01:59,00 --> 00:02:01,40
This has to do with something called CSS selector

44
00:02:01,40 --> 00:02:02,70
specificity.

45
00:02:02,70 --> 00:02:04,60
We cover this in great detail later on,

46
00:02:04,60 --> 00:02:06,60
but to generally think of it like this,

47
00:02:06,60 --> 00:02:08,90
anything that is more specific will override

48
00:02:08,90 --> 00:02:11,50
something that is less specific.

49
00:02:11,50 --> 00:02:15,20
So a class name is less specific than a generic

50
00:02:15,20 --> 00:02:19,00
class type or class tag,

51
00:02:19,00 --> 00:02:22,20
or element type or element tag.

52
00:02:22,20 --> 00:02:26,80
So the blue is being overridden here.

53
00:02:26,80 --> 00:02:28,70
Now we'll come in and add another one.

54
00:02:28,70 --> 00:02:31,40
We'll use an ID which is actually the most specific of all,

55
00:02:31,40 --> 00:02:34,00
and we'll do message,

56
00:02:34,00 --> 00:02:39,60
and we're going to say color red for that one.

57
00:02:39,60 --> 00:02:40,70
And now you can see this one down here

58
00:02:40,70 --> 00:02:47,10
is red, it's actually overriding the div color blue.

59
00:02:47,10 --> 00:02:50,10
Now we can also take these and combine these together.

60
00:02:50,10 --> 00:02:55,70
So for example I could say a div that is also in info

61
00:02:55,70 --> 00:03:03,40
should have a color of orange.

62
00:03:03,40 --> 00:03:05,60
Now what I'm going to do to show you how this works

63
00:03:05,60 --> 00:03:08,20
is I'm actually going to grab this one

64
00:03:08,20 --> 00:03:09,80
that has info on it, and I'm going to change it

65
00:03:09,80 --> 00:03:15,10
to be a section element.

66
00:03:15,10 --> 00:03:16,60
Now take a look.

67
00:03:16,60 --> 00:03:18,50
This one is green, the section,

68
00:03:18,50 --> 00:03:20,50
because it has an info.

69
00:03:20,50 --> 00:03:25,40
This one here is orange because it has a div and an info.

70
00:03:25,40 --> 00:03:27,00
So it actually overrides the div,

71
00:03:27,00 --> 00:03:30,70
it overrides the info, because it's more specific.

72
00:03:30,70 --> 00:03:33,00
So a div that has an info element on it

73
00:03:33,00 --> 00:03:35,70
will be orange.

74
00:03:35,70 --> 00:03:38,60
So these down here, like dot info, div,

75
00:03:38,60 --> 00:03:43,80
dot info id message, these are all simple selectors.

76
00:03:43,80 --> 00:03:46,90
This is a compound selector, because we're combining

77
00:03:46,90 --> 00:03:51,20
two or more simple selectors together.

78
00:03:51,20 --> 00:03:52,60
Now let's go ahead and do a little bit of styling

79
00:03:52,60 --> 00:03:54,20
on our text here.

80
00:03:54,20 --> 00:03:59,10
For example, I could come in and I could say font family,

81
00:03:59,10 --> 00:04:01,30
Arial.

82
00:04:01,30 --> 00:04:03,30
And now if we reload this, you'll see that this

83
00:04:03,30 --> 00:04:05,70
has a font family here of Arial.

84
00:04:05,70 --> 00:04:08,70
This one also has a font family of Arial.

85
00:04:08,70 --> 00:04:12,00
You'll notice the orange overrides the green,

86
00:04:12,00 --> 00:04:15,00
but the font family is not overridded by the other selector,

87
00:04:15,00 --> 00:04:16,30
so it's also an Arial type font.

88
00:04:16,30 --> 00:04:22,50
These other two are the default Times font.

89
00:04:22,50 --> 00:04:24,90
So now, we'll come into here,

90
00:04:24,90 --> 00:04:28,80
and let's go ahead and do font size,

91
00:04:28,80 --> 00:04:33,10
and we'll say 32 pixels.

92
00:04:33,10 --> 00:04:34,80
So now you can see that's much larger.

93
00:04:34,80 --> 00:04:38,40
We can also do some text decoration.

94
00:04:38,40 --> 00:04:41,60
So text decoration underline.

95
00:04:41,60 --> 00:04:46,50
So now these three all have underlines

96
00:04:46,50 --> 00:04:48,50
because those are the div tags,

97
00:04:48,50 --> 00:04:52,40
but the section tag does not have underline,

98
00:04:52,40 --> 00:04:54,60
because it doesn't have an underline applied to the section.

99
00:04:54,60 --> 00:04:57,30
I could come into here and say section,

100
00:04:57,30 --> 00:05:03,40
and we could say font style.

101
00:05:03,40 --> 00:05:08,00
Italic, so now that's going to be italic.

102
00:05:08,00 --> 00:05:09,90
If we wanted to we could put a border around that.

103
00:05:09,90 --> 00:05:18,10
We can say border, two pixel solid blue.

104
00:05:18,10 --> 00:05:20,30
Now that has a border around it.

105
00:05:20,30 --> 00:05:23,30
And there's a whole bunch of different rules in CSS

106
00:05:23,30 --> 00:05:25,90
as to why this takes up the full width

107
00:05:25,90 --> 00:05:27,70
for a block level element.

108
00:05:27,70 --> 00:05:30,10
We'll talk more about that in future courses.

109
00:05:30,10 --> 00:05:32,60
But you can see we have this blue border.

110
00:05:32,60 --> 00:05:38,20
We can also say for example, dashed, and now it's dashed

111
00:05:38,20 --> 00:05:41,10
border around it.

112
00:05:41,10 --> 00:05:49,30
And finally let's go ahead and specify a background color.

113
00:05:49,30 --> 00:05:51,40
And we'll just make the background color for this

114
00:05:51,40 --> 00:05:54,00
to be gray.

115
00:05:54,00 --> 00:05:56,90
So there's our gray background color.

116
00:05:56,90 --> 00:05:58,50
So just some different styles and stuff

117
00:05:58,50 --> 00:06:01,00
that we can set and start applying some colors

118
00:06:01,00 --> 00:06:04,00
and borders and things like that, to our webpage,

119
00:06:04,00 --> 00:06:06,20
hopefully to make it a little bit prettier.

120
00:06:06,20 --> 00:06:09,00
That's not a really pretty.

