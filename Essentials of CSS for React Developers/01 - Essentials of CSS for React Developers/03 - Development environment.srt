1
00:00:00,00 --> 00:00:06,70
(casual classical music)

2
00:00:06,70 --> 00:00:08,00
- [Instructor] Now in order to get started

3
00:00:08,00 --> 00:00:11,00
with working with CSS, we need

4
00:00:11,00 --> 00:00:13,60
to actually create an actual React project

5
00:00:13,60 --> 00:00:16,40
that will allow us to do our CSS coding.

6
00:00:16,40 --> 00:00:17,90
In the first course, we actually created

7
00:00:17,90 --> 00:00:19,80
a custom React scripts package

8
00:00:19,80 --> 00:00:21,80
that provided expanded support

9
00:00:21,80 --> 00:00:24,10
for various Post-CSS plugins and all

10
00:00:24,10 --> 00:00:26,40
of the CSS Next features.

11
00:00:26,40 --> 00:00:28,50
In this particular course, we're going

12
00:00:28,50 --> 00:00:30,50
to create a new React application

13
00:00:30,50 --> 00:00:33,30
using that custom React scripts package

14
00:00:33,30 --> 00:00:35,10
so that we'll have the latest and greatest features

15
00:00:35,10 --> 00:00:37,00
made available to us.

16
00:00:37,00 --> 00:00:38,70
Now in this course, we're going

17
00:00:38,70 --> 00:00:40,90
to need to have Node.js installed,

18
00:00:40,90 --> 00:00:42,80
because Node.js is what powers all

19
00:00:42,80 --> 00:00:46,30
of our development tooling in modern JavaScript

20
00:00:46,30 --> 00:00:48,20
web development projects.

21
00:00:48,20 --> 00:00:50,90
And Version 8 is the minimum required version,

22
00:00:50,90 --> 00:00:52,80
but I'll be doing all of the demonstrations

23
00:00:52,80 --> 00:00:54,40
using Version 10.

24
00:00:54,40 --> 00:00:55,60
So if you're watching this video

25
00:00:55,60 --> 00:00:57,20
at some point in the future, and you're using

26
00:00:57,20 --> 00:01:00,40
a later version of Node and it doesn't work,

27
00:01:00,40 --> 00:01:02,60
make sure you go back and install Version 10,

28
00:01:02,60 --> 00:01:04,20
and that way you'll be using the same version

29
00:01:04,20 --> 00:01:06,70
that I'm using to do the recordings.

30
00:01:06,70 --> 00:01:09,70
We'll be specifying our custom react-scripts package

31
00:01:09,70 --> 00:01:11,60
using the Scripts Version option

32
00:01:11,60 --> 00:01:16,40
when we actually go to create our React application.

33
00:01:16,40 --> 00:01:17,80
Now in addition to Node, we're going

34
00:01:17,80 --> 00:01:20,20
to need a text editor and a web browser.

35
00:01:20,20 --> 00:01:23,10
I recommend using Visual Studio Code.

36
00:01:23,10 --> 00:01:26,20
You can download that from code.visualstudio.com.

37
00:01:26,20 --> 00:01:28,50
I recommend the Google Chrome web browser.

38
00:01:28,50 --> 00:01:30,80
That's kind of the de facto standard web browser

39
00:01:30,80 --> 00:01:34,30
for web development, it's got great CSS developer tools,

40
00:01:34,30 --> 00:01:35,70
and so I highly encourage you to make

41
00:01:35,70 --> 00:01:36,00
use of that web browser.

