1
00:00:00,50 --> 00:00:01,90
- [Instructor] At this point we're aware

2
00:00:01,90 --> 00:00:04,10
of many data types in JavaScript.

3
00:00:04,10 --> 00:00:06,60
We know about numbers, strings, booleans,

4
00:00:06,60 --> 00:00:09,20
objects, arrays, and so forth.

5
00:00:09,20 --> 00:00:11,20
There will be times in JavaScript where you want

6
00:00:11,20 --> 00:00:13,10
to find out what a variable contains,

7
00:00:13,10 --> 00:00:15,00
what type of data it contains.

8
00:00:15,00 --> 00:00:17,90
We're going to look at that in this lesson.

9
00:00:17,90 --> 00:00:19,50
JavaScript doesn't care about data types

10
00:00:19,50 --> 00:00:21,90
to the same extent that some languages do.

11
00:00:21,90 --> 00:00:24,00
In other words, it's loosely typed.

12
00:00:24,00 --> 00:00:25,80
There's a video about strongly versus loosely

13
00:00:25,80 --> 00:00:28,20
typed languages later in this course but for now

14
00:00:28,20 --> 00:00:29,70
just know that JavaScript lets you do what

15
00:00:29,70 --> 00:00:32,20
you want with variables.

16
00:00:32,20 --> 00:00:35,70
So I've made this variable called thing

17
00:00:35,70 --> 00:00:38,20
and I've put the number 12 into it.

18
00:00:38,20 --> 00:00:39,90
But if I decide later I want to change thing

19
00:00:39,90 --> 00:00:44,50
to a string, that's fine, I can do that.

20
00:00:44,50 --> 00:00:47,00
JavaScript doesn't care at all.

21
00:00:47,00 --> 00:00:48,40
During the execution of a program there

22
00:00:48,40 --> 00:00:51,10
will be many times when you're dealing with uncertainty.

23
00:00:51,10 --> 00:00:53,00
You can get to a variable in a particular portion

24
00:00:53,00 --> 00:00:54,60
of your program and you don't necessarily know

25
00:00:54,60 --> 00:00:55,70
what's in there.

26
00:00:55,70 --> 00:00:57,90
And before you act on that data you might need

27
00:00:57,90 --> 00:00:59,90
to check what type it is to make sure

28
00:00:59,90 --> 00:01:01,40
that anything that happens afterwards

29
00:01:01,40 --> 00:01:03,30
is something you can actually do.

30
00:01:03,30 --> 00:01:05,40
You need to check the data type.

31
00:01:05,40 --> 00:01:08,50
To do that, JavaScript offers the typeof operator.

32
00:01:08,50 --> 00:01:12,70
It looks like this, typeof, all lower case,

33
00:01:12,70 --> 00:01:16,60
one word, and then the variable that I want to check.

34
00:01:16,60 --> 00:01:19,40
So we'll say thing in this case.

35
00:01:19,40 --> 00:01:21,40
It currently has a string in it so I would expect

36
00:01:21,40 --> 00:01:25,60
to get back string and indeed that's what I get down here.

37
00:01:25,60 --> 00:01:27,50
Typeof always returns a string.

38
00:01:27,50 --> 00:01:29,10
It isn't always the word string

39
00:01:29,10 --> 00:01:31,50
but it is a string with the name of the data type

40
00:01:31,50 --> 00:01:33,50
in all lower case.

41
00:01:33,50 --> 00:01:40,10
So if I set thing back to the number 12,

42
00:01:40,10 --> 00:01:43,60
typeof thing will give me back number.

43
00:01:43,60 --> 00:01:45,50
I'll hit the up arrow a couple of times

44
00:01:45,50 --> 00:01:48,70
and we'll reset thing to false.

45
00:01:48,70 --> 00:01:52,40
Run typeof again, and now I get a boolean.

46
00:01:52,40 --> 00:01:57,20
If I reset thing to an empty object

47
00:01:57,20 --> 00:02:00,30
and try typeof, I get object.

48
00:02:00,30 --> 00:02:03,00
All this is exactly what we would expect.

49
00:02:03,00 --> 00:02:05,90
But now let's try something else.

50
00:02:05,90 --> 00:02:10,00
Let's change thing to an empty array.

51
00:02:10,00 --> 00:02:13,10
And I'll try typeof again.

52
00:02:13,10 --> 00:02:15,60
It still gives me object.

53
00:02:15,60 --> 00:02:17,50
Now, we know that technically JavaScript arrays

54
00:02:17,50 --> 00:02:19,50
are objects but there are differences

55
00:02:19,50 --> 00:02:21,60
that can be important to us so if we want

56
00:02:21,60 --> 00:02:24,90
a way to know whether thing really is an array

57
00:02:24,90 --> 00:02:26,30
we have to do something that's a little bit

58
00:02:26,30 --> 00:02:29,60
different than simply using typeof.

59
00:02:29,60 --> 00:02:36,60
We can check if the type is object and then

60
00:02:36,60 --> 00:02:42,10
use a method that all objects have called hasOwnProperty.

61
00:02:42,10 --> 00:02:46,10
So I'm going to put in an and here

62
00:02:46,10 --> 00:02:47,10
and the browser is completing all

63
00:02:47,10 --> 00:02:48,40
these methods for me, which is nice.

64
00:02:48,40 --> 00:02:52,10
I can just hit a right arrow to expand that out.

65
00:02:52,10 --> 00:02:54,30
I'll add on some parentheses, and now I need

66
00:02:54,30 --> 00:02:56,50
to pass in the name of the property that I want

67
00:02:56,50 --> 00:02:59,50
to see if thing has.

68
00:02:59,50 --> 00:03:03,80
Arrays are objects that also have the property of length.

69
00:03:03,80 --> 00:03:05,30
That's not the only thing that differentiates

70
00:03:05,30 --> 00:03:07,50
them but it's a convenient thing to check.

71
00:03:07,50 --> 00:03:09,80
So if I want to know if this is an array

72
00:03:09,80 --> 00:03:13,00
I can check whether thing hasOwnProperty length.

73
00:03:13,00 --> 00:03:14,40
This entire expression will tell me

74
00:03:14,40 --> 00:03:16,10
whether thing is an array.

75
00:03:16,10 --> 00:03:18,40
Let's try changing thing back to an object.

76
00:03:18,40 --> 00:03:22,00
And hit up arrow a few times to get back to that.

77
00:03:22,00 --> 00:03:24,60
And let's confirm that I get back the correct answer here.

78
00:03:24,60 --> 00:03:26,30
I'm expecting that this expression should now

79
00:03:26,30 --> 00:03:28,70
return false because this is an empty object

80
00:03:28,70 --> 00:03:32,20
that shouldn't have the property of length.

81
00:03:32,20 --> 00:03:35,40
And there we go, I can see it comes back false.

82
00:03:35,40 --> 00:03:37,20
There are other instances where typeof will not

83
00:03:37,20 --> 00:03:39,70
return something as you might expect it to.

84
00:03:39,70 --> 00:03:41,30
So for example, we know that there's a thing

85
00:03:41,30 --> 00:03:45,60
in JavaScript called not a number, spelled like that.

86
00:03:45,60 --> 00:03:48,90
It's called not a number, so if I check what

87
00:03:48,90 --> 00:03:51,40
it is with typeof, what do I get back?

88
00:03:51,40 --> 00:03:52,70
Number.

89
00:03:52,70 --> 00:03:55,10
That's a little counterintuitive.

90
00:03:55,10 --> 00:03:56,70
So if you're doing some mathematical work

91
00:03:56,70 --> 00:03:59,30
and you want to see if your result is not a number

92
00:03:59,30 --> 00:04:03,00
using typeof on not a number is not the way to do that.

93
00:04:03,00 --> 00:04:05,60
The built-in number object that JavaScript has

94
00:04:05,60 --> 00:04:08,10
has a method called isNaN that you can use

95
00:04:08,10 --> 00:04:09,90
on your variables to check whether they

96
00:04:09,90 --> 00:04:12,90
have actually turned into not a number.

97
00:04:12,90 --> 00:04:16,30
Likewise, we can do typeof null.

98
00:04:16,30 --> 00:04:19,30
We get back object, which is a little weird.

99
00:04:19,30 --> 00:04:21,60
So if we want to check whether a variable is null

100
00:04:21,60 --> 00:04:25,70
we're really better off using a strict equality operator.

101
00:04:25,70 --> 00:04:29,40
So let me set thing to null.

102
00:04:29,40 --> 00:04:32,00
And what I'd really want to do is check

103
00:04:32,00 --> 00:04:39,10
is thing null, which in this case is true.

104
00:04:39,10 --> 00:04:42,60
Set thing to not a number and check whether

105
00:04:42,60 --> 00:04:46,60
thing is strictly null, and it come back false.

106
00:04:46,60 --> 00:04:48,60
So that'll work a lot better when you're wanting

107
00:04:48,60 --> 00:04:51,20
to check whether something is null.

108
00:04:51,20 --> 00:04:52,80
With type checking there are many subtleties

109
00:04:52,80 --> 00:04:54,30
that you'll run into with different special

110
00:04:54,30 --> 00:04:56,30
values in JavaScript that won't necessarily

111
00:04:56,30 --> 00:04:58,40
give you back the answer you expect.

112
00:04:58,40 --> 00:04:59,70
So you need to be careful with it

113
00:04:59,70 --> 00:05:02,80
but it's still useful for doing basic type checking.

114
00:05:02,80 --> 00:05:05,60
If you need or want to get more serious about

115
00:05:05,60 --> 00:05:07,40
type checking in your JavaScript code you might

116
00:05:07,40 --> 00:05:09,30
consider using helper functions from popular

117
00:05:09,30 --> 00:05:12,00
libraries like Lodash which includes functions

118
00:05:12,00 --> 00:05:14,50
like isNumber and lots of others that can check

119
00:05:14,50 --> 00:05:16,20
for the type of data in a way that's a little

120
00:05:16,20 --> 00:05:19,40
more robust than JavaScript by itself.

121
00:05:19,40 --> 00:05:21,40
Or you could even consider using TypeScript,

122
00:05:21,40 --> 00:05:23,50
which we'll talk about later, but that's well

123
00:05:23,50 --> 00:05:24,90
past where we are right now.

124
00:05:24,90 --> 00:05:26,90
Just know that basic type checking is included

125
00:05:26,90 --> 00:05:28,60
in JavaScript and there are options out there

126
00:05:28,60 --> 00:05:30,00
for more if you need them.

