1
00:00:00,50 --> 00:00:01,70
- [Narrator] In this video we're going to look at

2
00:00:01,70 --> 00:00:04,10
simple comparisons, which are the beginning of the way

3
00:00:04,10 --> 00:00:06,60
we can start to ask the computer a question about something

4
00:00:06,60 --> 00:00:08,70
and get a meaningful answer back.

5
00:00:08,70 --> 00:00:10,70
First we're going to look at equality,

6
00:00:10,70 --> 00:00:11,80
that is knowing weather something

7
00:00:11,80 --> 00:00:14,00
is the same as something else.

8
00:00:14,00 --> 00:00:15,50
We can start out really easy,

9
00:00:15,50 --> 00:00:16,90
I'll create variables for one and two

10
00:00:16,90 --> 00:00:19,10
because most of the time at least one of the items

11
00:00:19,10 --> 00:00:22,30
we're asking a question about will be a variable.

12
00:00:22,30 --> 00:00:28,10
So we'll start out by asking if one is the same as one.

13
00:00:28,10 --> 00:00:31,20
We're using three equals signs to do this.

14
00:00:31,20 --> 00:00:33,70
This operator is called the strict equality operator

15
00:00:33,70 --> 00:00:36,30
and it tests whether the object on the left

16
00:00:36,30 --> 00:00:40,10
is identical to the thing on the right.

17
00:00:40,10 --> 00:00:42,50
And we can see that this is true.

18
00:00:42,50 --> 00:00:44,00
There's a similar operator that asks

19
00:00:44,00 --> 00:00:46,40
whether this is not equal to that.

20
00:00:46,40 --> 00:00:49,50
So I'm going to type an up arrow to get back what I had before

21
00:00:49,50 --> 00:00:51,70
and I'm going to change that first equals sign

22
00:00:51,70 --> 00:00:54,90
to an exclamation mark or a bang.

23
00:00:54,90 --> 00:00:57,20
This is called strict inequality,

24
00:00:57,20 --> 00:00:58,60
so in this case we're just asking

25
00:00:58,60 --> 00:01:01,10
if one is not identical to one,

26
00:01:01,10 --> 00:01:04,10
and as we would expect the answer is false.

27
00:01:04,10 --> 00:01:07,10
Now if I change the one to a two,

28
00:01:07,10 --> 00:01:11,80
I'm asking if one is not identical to two and that is true.

29
00:01:11,80 --> 00:01:14,50
And then finally, if I change

30
00:01:14,50 --> 00:01:17,10
the not identical back to identical.

31
00:01:17,10 --> 00:01:18,40
Is one identical to two?

32
00:01:18,40 --> 00:01:20,90
The answer is false, no.

33
00:01:20,90 --> 00:01:23,30
These kinds of comparisons are all over the place

34
00:01:23,30 --> 00:01:24,70
in most programs.

35
00:01:24,70 --> 00:01:28,10
You might be asking, "Is this page's url about dot html?"

36
00:01:28,10 --> 00:01:29,80
Or, "Is the button that was just clicked

37
00:01:29,80 --> 00:01:31,10
"the main menu button?"

38
00:01:31,10 --> 00:01:33,00
Checking whether something is equal to something else

39
00:01:33,00 --> 00:01:36,40
is a comparison you'll be doing a lot of.

40
00:01:36,40 --> 00:01:38,90
There's another pair of operators that are similar,

41
00:01:38,90 --> 00:01:40,20
but act differently.

42
00:01:40,20 --> 00:01:42,60
You'll see them a lot in programs out in the wild,

43
00:01:42,60 --> 00:01:46,40
these are simply the equality and not equality operators,

44
00:01:46,40 --> 00:01:48,80
with on two equals signs.

45
00:01:48,80 --> 00:01:52,10
So for example, one double equals one.

46
00:01:52,10 --> 00:01:54,80
This also checks equality but in a way that's not as strict,

47
00:01:54,80 --> 00:01:56,60
hence the difference in the names.

48
00:01:56,60 --> 00:01:58,30
This first comparison still comes back true

49
00:01:58,30 --> 00:02:00,30
because they are exactly the same.

50
00:02:00,30 --> 00:02:01,60
You start to see a little bit of weirdness

51
00:02:01,60 --> 00:02:04,40
when testing a number with a string.

52
00:02:04,40 --> 00:02:07,40
So let's say I want to check against the string one

53
00:02:07,40 --> 00:02:09,30
instead of the number one.

54
00:02:09,30 --> 00:02:12,10
This is true-ish I suppose, I mean there is a one in there

55
00:02:12,10 --> 00:02:14,40
but it's a string not a number.

56
00:02:14,40 --> 00:02:15,60
Strictly they're different,

57
00:02:15,60 --> 00:02:17,70
but this operator sort of converts the data types

58
00:02:17,70 --> 00:02:20,20
and says, "Eh, sure, they're the same."

59
00:02:20,20 --> 00:02:22,50
There's also a not equal operator,

60
00:02:22,50 --> 00:02:24,40
where I change that first equal sign

61
00:02:24,40 --> 00:02:27,00
into an exclamation mark again.

62
00:02:27,00 --> 00:02:29,10
And that's how we do inequality.

63
00:02:29,10 --> 00:02:31,50
It's generally recommended that you use the strict versions

64
00:02:31,50 --> 00:02:33,60
of the operators for the most predictable results

65
00:02:33,60 --> 00:02:35,70
in your code, but people still use

66
00:02:35,70 --> 00:02:37,50
the not strict versions a lot as well.

67
00:02:37,50 --> 00:02:40,10
The particulars of which kind of values will return true

68
00:02:40,10 --> 00:02:42,70
or false for the equality or inequality operator

69
00:02:42,70 --> 00:02:44,10
are a little too detailed to go into,

70
00:02:44,10 --> 00:02:46,20
especially for people who are just starting out.

71
00:02:46,20 --> 00:02:48,10
You can look at the comparison operators page

72
00:02:48,10 --> 00:02:51,40
on the Mozilla developer network for more on this.

73
00:02:51,40 --> 00:02:54,80
But do note that if I change this to strict equality,

74
00:02:54,80 --> 00:02:56,50
I'm starting too get results

75
00:02:56,50 --> 00:02:59,60
that are maybe more what you expect.

76
00:02:59,60 --> 00:03:05,60
So one is not that same as the string, it is different.

77
00:03:05,60 --> 00:03:07,80
JavaScript also has relational operators,

78
00:03:07,80 --> 00:03:09,70
which are straight out of your math class.

79
00:03:09,70 --> 00:03:15,60
So we can ask, for example, is one less than two.

80
00:03:15,60 --> 00:03:17,60
And yes it is.

81
00:03:17,60 --> 00:03:20,40
Then we can say, is one greater than two

82
00:03:20,40 --> 00:03:22,60
and of course it is not.

83
00:03:22,60 --> 00:03:24,70
We can also ask a combination,

84
00:03:24,70 --> 00:03:28,80
so I can say is one less than or equal to two.

85
00:03:28,80 --> 00:03:31,60
This is an angel bracket followed by an equal sign

86
00:03:31,60 --> 00:03:33,40
with no space in between.

87
00:03:33,40 --> 00:03:37,00
So here we are asking is one less than or equal to two.

88
00:03:37,00 --> 00:03:40,00
And yes it is.

89
00:03:40,00 --> 00:03:42,70
If I change this to a one, is one less than

90
00:03:42,70 --> 00:03:47,70
or equal to one, yes, and then I can flip this angle bracket

91
00:03:47,70 --> 00:03:51,90
and ask is one greater than of equal to one, still yes.

92
00:03:51,90 --> 00:03:54,00
What about, is one greater than or equal to two?

93
00:03:54,00 --> 00:03:57,90
And now we get an answer of false.

94
00:03:57,90 --> 00:04:01,70
Of course, change this to a 10 and I get back true.

95
00:04:01,70 --> 00:04:03,70
So that was a look at simple comparisons,

96
00:04:03,70 --> 00:04:05,20
the various quality operators

97
00:04:05,20 --> 00:04:06,70
and the relational operators.

98
00:04:06,70 --> 00:04:10,00
Expect to use all of these very often.

