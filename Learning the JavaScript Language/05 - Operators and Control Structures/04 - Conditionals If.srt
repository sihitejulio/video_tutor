1
00:00:00,50 --> 00:00:02,40
- [Instructor] A control structure in programming is a way

2
00:00:02,40 --> 00:00:04,60
to modify the flow of execution.

3
00:00:04,60 --> 00:00:07,20
Like, it's deciding whether to execute at all

4
00:00:07,20 --> 00:00:09,60
or how many times to execute something.

5
00:00:09,60 --> 00:00:10,80
In this video, we're going to look at one

6
00:00:10,80 --> 00:00:14,00
of the fundamental control structures, the if statement.

7
00:00:14,00 --> 00:00:16,70
An if statement is what you use to branch in your code,

8
00:00:16,70 --> 00:00:18,70
depending on certain conditions.

9
00:00:18,70 --> 00:00:20,80
What we're going to do first is assign a variable based

10
00:00:20,80 --> 00:00:23,00
on a prompt for user input.

11
00:00:23,00 --> 00:00:26,30
Here's a piece of code right here at the top of this file.

12
00:00:26,30 --> 00:00:29,90
I'm going to assign a variable called answer

13
00:00:29,90 --> 00:00:32,60
to the result of this method called confirmed,

14
00:00:32,60 --> 00:00:34,20
which is part of the window object

15
00:00:34,20 --> 00:00:37,30
that JavaScript has available in all web browsers.

16
00:00:37,30 --> 00:00:39,50
I'm going to copy this and switch over to the browser

17
00:00:39,50 --> 00:00:44,10
and watch what happens when I execute this.

18
00:00:44,10 --> 00:00:45,30
You can see that the string I passed

19
00:00:45,30 --> 00:00:49,10
into the confirm function is rendered right here.

20
00:00:49,10 --> 00:00:50,90
Based on which of these two buttons I click,

21
00:00:50,90 --> 00:00:53,80
I'm going to get a different value in the answer variable.

22
00:00:53,80 --> 00:00:57,80
I'll click OK first and let's see what answer contains.

23
00:00:57,80 --> 00:00:59,40
True.

24
00:00:59,40 --> 00:01:04,50
I'll run this again and I'll click cancel this time.

25
00:01:04,50 --> 00:01:08,70
We'll check the value of answer now and it's false.

26
00:01:08,70 --> 00:01:09,70
In the course of a program,

27
00:01:09,70 --> 00:01:12,10
you might want to ask the user for an answer to something

28
00:01:12,10 --> 00:01:14,00
and this window.confirm function is a way

29
00:01:14,00 --> 00:01:15,60
that you could do that.

30
00:01:15,60 --> 00:01:16,60
Having retrieved an answer,

31
00:01:16,60 --> 00:01:18,40
now I need to take some action based

32
00:01:18,40 --> 00:01:20,00
on what the user said.

33
00:01:20,00 --> 00:01:23,00
One way I can do that is with an if statement.

34
00:01:23,00 --> 00:01:25,40
Here's what that looks like.

35
00:01:25,40 --> 00:01:29,00
I type the word if followed by parentheses

36
00:01:29,00 --> 00:01:33,40
and in those parentheses, I put my condition or conditions.

37
00:01:33,40 --> 00:01:37,50
If whatever's in these parentheses evaluates down to true,

38
00:01:37,50 --> 00:01:42,20
I'll do whatever is in these curly braces that follow it.

39
00:01:42,20 --> 00:01:44,50
Here, if the answer is true, I'm going to log a message

40
00:01:44,50 --> 00:01:47,10
to the console using the console.log function

41
00:01:47,10 --> 00:01:51,70
and it will say, "You said true!"

42
00:01:51,70 --> 00:02:00,70
Let's copy this whole thing and try it.

43
00:02:00,70 --> 00:02:02,70
Click OK.

44
00:02:02,70 --> 00:02:04,60
Notice, you said true.

45
00:02:04,60 --> 00:02:07,70
Let's execute the same block again

46
00:02:07,70 --> 00:02:12,00
by up arrow and then return and click cancel this time.

47
00:02:12,00 --> 00:02:14,10
Nothing happened, no message.

48
00:02:14,10 --> 00:02:15,50
You can see that by asking this question

49
00:02:15,50 --> 00:02:16,80
in the form of an if statement,

50
00:02:16,80 --> 00:02:19,70
I get to execute this code or do nothing.

51
00:02:19,70 --> 00:02:21,00
This is actually really useful

52
00:02:21,00 --> 00:02:23,40
and you'll see it all the time.

53
00:02:23,40 --> 00:02:25,70
Let's extend this to take a different action based

54
00:02:25,70 --> 00:02:30,10
on whether the answer was true or whether it was false.

55
00:02:30,10 --> 00:02:33,30
Here's a block that does that.

56
00:02:33,30 --> 00:02:34,10
Here's a longer block.

57
00:02:34,10 --> 00:02:35,20
We're going to copy this whole thing

58
00:02:35,20 --> 00:02:37,10
and bring it over into the browser.

59
00:02:37,10 --> 00:02:39,80
I'll clear this, paste this in,

60
00:02:39,80 --> 00:02:43,00
and I'll get rid of what we don't need.

61
00:02:43,00 --> 00:02:45,80
This looks just like the previous if statement

62
00:02:45,80 --> 00:02:50,40
except now I also have an else block that follows it.

63
00:02:50,40 --> 00:02:52,50
The if statement has curly braces around it,

64
00:02:52,50 --> 00:02:57,50
then else, another set of curly braces, and another line.

65
00:02:57,50 --> 00:03:00,70
If this evaluates to true, the first block will execute,

66
00:03:00,70 --> 00:03:05,70
and if it's false, the else block will execute.

67
00:03:05,70 --> 00:03:06,90
Let's try this.

68
00:03:06,90 --> 00:03:09,30
I hit return to execute it, click OK.

69
00:03:09,30 --> 00:03:11,50
It says, "You said true!"

70
00:03:11,50 --> 00:03:13,70
I'll hit the up arrow again to bring the whole thing back,

71
00:03:13,70 --> 00:03:16,90
return to execute, click cancel,

72
00:03:16,90 --> 00:03:19,10
and now it says, "You said something else."

73
00:03:19,10 --> 00:03:21,00
When it was true, "You said true!"

74
00:03:21,00 --> 00:03:24,10
When it was false, "You said something else."

75
00:03:24,10 --> 00:03:26,10
This is how you can branch in your code

76
00:03:26,10 --> 00:03:28,10
and do certain things under certain conditions

77
00:03:28,10 --> 00:03:30,60
and other stuff in other conditions.

78
00:03:30,60 --> 00:03:32,80
If statements can be extended even further.

79
00:03:32,80 --> 00:03:38,00
Let's take another, longer block of code.

80
00:03:38,00 --> 00:03:39,20
Right here.

81
00:03:39,20 --> 00:03:42,50
I'm going to copy this and bring it over into the browser.

82
00:03:42,50 --> 00:03:44,50
This time we're using window.prompt

83
00:03:44,50 --> 00:03:47,10
instead of window.confirm.

84
00:03:47,10 --> 00:03:51,60
This is going to prompt me for an answer using a text field.

85
00:03:51,60 --> 00:03:54,80
I'm instructing the user to type yes, no, or maybe,

86
00:03:54,80 --> 00:03:56,60
then click OK.

87
00:03:56,60 --> 00:03:58,50
Then, I'll take some various actions

88
00:03:58,50 --> 00:04:00,80
based on what the user said.

89
00:04:00,80 --> 00:04:04,00
Let's try it first to see what it does.

90
00:04:04,00 --> 00:04:07,30
Here we go, type yes, no, or maybe.

91
00:04:07,30 --> 00:04:11,00
I'll type yes and it says, "You said yes!"

92
00:04:11,00 --> 00:04:12,20
Let's just do this a few times.

93
00:04:12,20 --> 00:04:13,90
Now, I'll say no.

94
00:04:13,90 --> 00:04:17,00
"You said no."

95
00:04:17,00 --> 00:04:20,90
Try it one more time and I'll say, hey.

96
00:04:20,90 --> 00:04:22,50
Now, it says, "You rebel, you!"

97
00:04:22,50 --> 00:04:24,70
Okay, now that we've done this,

98
00:04:24,70 --> 00:04:26,30
let's look at the code a little more carefully

99
00:04:26,30 --> 00:04:28,50
and break it down.

100
00:04:28,50 --> 00:04:31,50
What's happening here is I'm checking each of these in turn

101
00:04:31,50 --> 00:04:35,60
and instead of just using else, I'm using else-if blocks.

102
00:04:35,60 --> 00:04:37,70
First, I'm checking this one.

103
00:04:37,70 --> 00:04:38,90
If answer is yes,

104
00:04:38,90 --> 00:04:41,90
I'll just do what's here and skip everything else.

105
00:04:41,90 --> 00:04:44,50
If it's false, then I'll jump down to the next one

106
00:04:44,50 --> 00:04:46,90
and check if answer is maybe instead.

107
00:04:46,90 --> 00:04:49,30
If it is, I'll do what's here.

108
00:04:49,30 --> 00:04:51,60
If not, I skip down to the next one and so on,

109
00:04:51,60 --> 00:04:53,80
until I get to the else.

110
00:04:53,80 --> 00:04:55,00
If I strip any of these out,

111
00:04:55,00 --> 00:04:57,00
it won't be taken into account anymore.

112
00:04:57,00 --> 00:05:00,10
Say I decided that I don't need to specify maybe anymore.

113
00:05:00,10 --> 00:05:04,50
I can just remove this condition.

114
00:05:04,50 --> 00:05:06,40
Now, I'm only checking for yes or no.

115
00:05:06,40 --> 00:05:11,10
Anything else is going to be considered rebel talk.

116
00:05:11,10 --> 00:05:14,00
If I type maybe here, it says, "You rebel you,"

117
00:05:14,00 --> 00:05:17,50
instead of responding specifically to maybe.

118
00:05:17,50 --> 00:05:19,10
You can have as many lines as you want

119
00:05:19,10 --> 00:05:21,90
inside these if blocks and the conditions

120
00:05:21,90 --> 00:05:23,00
in these parentheses don't have

121
00:05:23,00 --> 00:05:24,80
to be a single, simple comparison.

122
00:05:24,80 --> 00:05:27,70
You can add in logical operators.

123
00:05:27,70 --> 00:05:29,60
Switching back to my code editor one more time,

124
00:05:29,60 --> 00:05:33,30
here's a block that's got a lot more going on.

125
00:05:33,30 --> 00:05:36,60
First, it will check this entire expression,

126
00:05:36,60 --> 00:05:39,90
which in this case would be acting on yes or no,

127
00:05:39,90 --> 00:05:42,30
and doing some common actions potentially

128
00:05:42,30 --> 00:05:44,10
if either of these answers is true.

129
00:05:44,10 --> 00:05:47,40
Then, I have some nested if blocks which checks specifically

130
00:05:47,40 --> 00:05:49,70
for whether the answer is yes here

131
00:05:49,70 --> 00:05:52,60
and does some specific actions for when we say yes,

132
00:05:52,60 --> 00:05:55,10
and I can simply use else to cover anything else

133
00:05:55,10 --> 00:05:58,90
which, because of this condition, would only be no.

134
00:05:58,90 --> 00:06:01,10
Of course, if it was not yes or no,

135
00:06:01,10 --> 00:06:05,70
I would move on and cascade down these other blocks.

136
00:06:05,70 --> 00:06:07,50
You can see, there's a lot of power in being able

137
00:06:07,50 --> 00:06:09,50
to branch off and do different things in your code

138
00:06:09,50 --> 00:06:11,00
based on different conditions.

139
00:06:11,00 --> 00:06:12,70
Whether it's input from the user

140
00:06:12,70 --> 00:06:16,00
or conditions that you created in your own code yourself,

141
00:06:16,00 --> 00:06:17,60
but this is how you can use if statements

142
00:06:17,60 --> 00:06:21,00
to execute different code under different conditions.

