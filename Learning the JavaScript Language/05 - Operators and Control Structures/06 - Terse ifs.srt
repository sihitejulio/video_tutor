1
00:00:00,50 --> 00:00:02,00
- [Instructor] In this video we're going to look at a few

2
00:00:02,00 --> 00:00:05,20
of the forms in which if statements will be made shorter.

3
00:00:05,20 --> 00:00:06,60
You may write these in your own code,

4
00:00:06,60 --> 00:00:08,50
and you will certainly see them if you start looking

5
00:00:08,50 --> 00:00:10,60
at other people's JavaScript code.

6
00:00:10,60 --> 00:00:13,20
First, the one liner.

7
00:00:13,20 --> 00:00:15,50
So I'm setting a variable up here, for a cherub,

8
00:00:15,50 --> 00:00:18,40
who is going to start out as Cupid.

9
00:00:18,40 --> 00:00:22,80
I have a comment for making the cherub Some Other Guy.

10
00:00:22,80 --> 00:00:25,60
So first we're going to copy these first few lines

11
00:00:25,60 --> 00:00:28,10
and bring them over to the browser.

12
00:00:28,10 --> 00:00:30,00
You'll notice that I've put this if statement entirely

13
00:00:30,00 --> 00:00:34,20
on one line with no curly braces, and it will still work.

14
00:00:34,20 --> 00:00:37,50
There, so I executed it, and the cherub is Cupid,

15
00:00:37,50 --> 00:00:39,40
so having checked that, I still log this message

16
00:00:39,40 --> 00:00:42,80
to the console, even though there's no curly braces.

17
00:00:42,80 --> 00:00:45,50
Likewise, I can include an else block on the next line,

18
00:00:45,50 --> 00:00:48,30
again with no curly braces, and it'll still work.

19
00:00:48,30 --> 00:00:49,40
Let's paste this in.

20
00:00:49,40 --> 00:00:52,10
We know the cherub is still cupid.

21
00:00:52,10 --> 00:00:54,80
So, I'm still getting the same message,

22
00:00:54,80 --> 00:00:58,30
but if I change the cherub to some other guy,

23
00:00:58,30 --> 00:01:02,10
and then I execute this line again,

24
00:01:02,10 --> 00:01:03,80
I'm getting my else block.

25
00:01:03,80 --> 00:01:05,40
One liners are fine for quick checks,

26
00:01:05,40 --> 00:01:07,10
but if you want multiple lines of actions

27
00:01:07,10 --> 00:01:09,20
to be executed after a condition is evaluated

28
00:01:09,20 --> 00:01:12,60
by an if statement you should add curly braces back.

29
00:01:12,60 --> 00:01:14,70
Only the first line that comes afterwards

30
00:01:14,70 --> 00:01:17,60
is bound by the if statement when there are no braces.

31
00:01:17,60 --> 00:01:20,10
For that reason, and just generally to avoid confusion,

32
00:01:20,10 --> 00:01:22,10
I rarely use these one line versions,

33
00:01:22,10 --> 00:01:23,80
and pretty much always add curly braces

34
00:01:23,80 --> 00:01:28,80
in case I need to add more to an if block later.

35
00:01:28,80 --> 00:01:30,80
Another thing you'll see is a condition

36
00:01:30,80 --> 00:01:33,60
that's just a variable by itself.

37
00:01:33,60 --> 00:01:35,80
Here's a block using that.

38
00:01:35,80 --> 00:01:38,00
I'm going to copy everything here,

39
00:01:38,00 --> 00:01:41,00
bring it over, paste this in.

40
00:01:41,00 --> 00:01:42,50
Let's talk about it first.

41
00:01:42,50 --> 00:01:45,80
We have some variables here for what we want for Christmas,

42
00:01:45,80 --> 00:01:47,00
what we got for Christmas,

43
00:01:47,00 --> 00:01:49,10
and whether we will get upset about it.

44
00:01:49,10 --> 00:01:52,40
So, what I'm asking is, if what I want for Christmas

45
00:01:52,40 --> 00:01:54,40
is identical to what I got for Christmas

46
00:01:54,40 --> 00:01:56,30
we're going to log a console message that says, Yay!

47
00:01:56,30 --> 00:01:58,60
The little children are so pleased,

48
00:01:58,60 --> 00:02:01,60
and set whether they will cry about it to false,

49
00:02:01,60 --> 00:02:03,30
which it already was.

50
00:02:03,30 --> 00:02:06,00
If it's anything else, I'm going to log a message

51
00:02:06,00 --> 00:02:07,30
that says, Oh no! Sad Christmas!

52
00:02:07,30 --> 00:02:10,60
and set that we will cry about it to true.

53
00:02:10,60 --> 00:02:12,60
Now here's the new part.

54
00:02:12,60 --> 00:02:14,50
In a normal if statement that we've seen so far,

55
00:02:14,50 --> 00:02:16,90
like this first one, we have a full expression that

56
00:02:16,90 --> 00:02:19,70
then evaluates down to true or false.

57
00:02:19,70 --> 00:02:23,30
We can also just check a single variable in the same way.

58
00:02:23,30 --> 00:02:26,90
So we can say, is this variable true, or rather, truthy.

59
00:02:26,90 --> 00:02:28,70
Just writing the variable name in here is enough.

60
00:02:28,70 --> 00:02:30,20
I say truthy because JavaScript

61
00:02:30,20 --> 00:02:33,40
doesn't just interpret the Boolean true as true.

62
00:02:33,40 --> 00:02:35,70
Almost any non-empty value will be interpreted

63
00:02:35,70 --> 00:02:38,30
as true for this purpose.

64
00:02:38,30 --> 00:02:40,00
Let me copy in another block here.

65
00:02:40,00 --> 00:02:42,10
Copy and paste this.

66
00:02:42,10 --> 00:02:44,20
Likewise, if we wanted to check whether it was false,

67
00:02:44,20 --> 00:02:48,10
or falsy, we could put the not in front of it like this.

68
00:02:48,10 --> 00:02:50,90
Of course, if I'm checking whether to not cry about it

69
00:02:50,90 --> 00:02:54,60
the child will say something happier like YAY!

70
00:02:54,60 --> 00:02:56,30
So you will often see entire expressions

71
00:02:56,30 --> 00:02:58,70
that can be expressed in a single variable collapsed down

72
00:02:58,70 --> 00:02:59,90
like this in JavaScript code,

73
00:02:59,90 --> 00:03:02,30
and you can feel free to do it this way yourself.

74
00:03:02,30 --> 00:03:07,00
Let's execute this entire block now.

75
00:03:07,00 --> 00:03:08,80
So, what we wanted for Christmas was a puppy,

76
00:03:08,80 --> 00:03:10,70
and what we got was a puppy.

77
00:03:10,70 --> 00:03:13,00
So that meant that this first block executed,

78
00:03:13,00 --> 00:03:14,70
and the children were so pleased.

79
00:03:14,70 --> 00:03:17,30
We skipped this, came down here,

80
00:03:17,30 --> 00:03:20,70
cryAboutIt was set to false.

81
00:03:20,70 --> 00:03:23,90
So the children said YAY!

82
00:03:23,90 --> 00:03:33,30
Now let's change what we got for Christmas to, say, a fish.

83
00:03:33,30 --> 00:03:36,50
Next, you get that entire block,

84
00:03:36,50 --> 00:03:40,10
without the variable assignments, again.

85
00:03:40,10 --> 00:03:41,60
So this time we get the opposite.

86
00:03:41,60 --> 00:03:45,30
Oh no! Sad Christmas! and the children cried.

87
00:03:45,30 --> 00:03:46,80
Because cryAboutIt was set to true,

88
00:03:46,80 --> 00:03:48,90
and that's a truthy value.

89
00:03:48,90 --> 00:03:50,50
The Mozilla Network has a glossary

90
00:03:50,50 --> 00:03:53,90
defining what JavaScript interrupts as truthy and falsy.

91
00:03:53,90 --> 00:03:54,80
For maximum clarity you can stick

92
00:03:54,80 --> 00:03:56,90
to conditions involving strict equality,

93
00:03:56,90 --> 00:03:59,60
like these, or inequality, of course.

94
00:03:59,60 --> 00:04:00,80
But these short forms work,

95
00:04:00,80 --> 00:04:03,80
and are very much in popular usage.

96
00:04:03,80 --> 00:04:06,40
Let's look at one more thing, the ternary operator,

97
00:04:06,40 --> 00:04:10,00
or the conditional operator.

98
00:04:10,00 --> 00:04:16,20
I'm going to copy this block into the browser, paste it in.

99
00:04:16,20 --> 00:04:18,20
The ternary operator is an expression,

100
00:04:18,20 --> 00:04:20,60
followed by a question mark,

101
00:04:20,60 --> 00:04:24,70
followed by an action to take if this condition is true.

102
00:04:24,70 --> 00:04:26,60
Then there's a colon, and the action to take

103
00:04:26,60 --> 00:04:28,80
if the expression is false.

104
00:04:28,80 --> 00:04:31,20
So let's execute this.

105
00:04:31,20 --> 00:04:34,80
The animal was a cat, so then we checked, is animal a cat?

106
00:04:34,80 --> 00:04:38,50
And if so, we executed this first line.

107
00:04:38,50 --> 00:04:42,00
And it says, You will be a cat herder.

108
00:04:42,00 --> 00:04:48,60
If we change animal to a dog, now we will be a dog catcher.

109
00:04:48,60 --> 00:04:50,50
The ternary operator is pretty terse,

110
00:04:50,50 --> 00:04:52,10
and remembering which order the colon

111
00:04:52,10 --> 00:04:54,70
and the question mark can go in is a little tricky at first.

112
00:04:54,70 --> 00:04:56,00
I avoided it myself for awhile,

113
00:04:56,00 --> 00:04:57,30
but at this point I've gotten used to it

114
00:04:57,30 --> 00:04:59,00
and started using it more.

