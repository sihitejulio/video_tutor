1
00:00:00,50 --> 00:00:01,80
- [Instructor] In this video we're going to look at some

2
00:00:01,80 --> 00:00:04,40
simple operators that are used for doing math.

3
00:00:04,40 --> 00:00:06,20
You're not going to need to do a lot of math in the examples

4
00:00:06,20 --> 00:00:07,50
we're going through in this course,

5
00:00:07,50 --> 00:00:10,30
but simple arithmetic comes up pretty often.

6
00:00:10,30 --> 00:00:12,00
The first four operators are pretty simple,

7
00:00:12,00 --> 00:00:13,70
and if you've ever used a calculator program

8
00:00:13,70 --> 00:00:16,00
on a computer, they'll probably look familiar.

9
00:00:16,00 --> 00:00:18,00
We of course have the plus operator.

10
00:00:18,00 --> 00:00:21,80
So, two plus five, that equals seven.

11
00:00:21,80 --> 00:00:24,30
We also have minus, which is technically the hyphen,

12
00:00:24,30 --> 00:00:27,90
so four minus three equals one.

13
00:00:27,90 --> 00:00:29,70
Of course we're not restricted to the counting numbers,

14
00:00:29,70 --> 00:00:31,30
we can do negatives as well, so like,

15
00:00:31,30 --> 00:00:35,60
let's say five minutes nine equals negative four.

16
00:00:35,60 --> 00:00:37,70
To do multiplication we don't use an X,

17
00:00:37,70 --> 00:00:45,50
we use an asterisk, so, three times five is fifteen.

18
00:00:45,50 --> 00:00:47,90
And then finally we use a slash to do division,

19
00:00:47,90 --> 00:00:52,80
so say 36 divided by six equals six.

20
00:00:52,80 --> 00:00:54,30
And when you start getting into fractions,

21
00:00:54,30 --> 00:00:58,50
the result is expressed using a decimal point.

22
00:00:58,50 --> 00:01:00,20
So these are all fairly straightforward.

23
00:01:00,20 --> 00:01:01,90
Probably nothing you haven't seen before.

24
00:01:01,90 --> 00:01:03,20
But now we're going to look at a few things

25
00:01:03,20 --> 00:01:04,80
that might be a little unusual.

26
00:01:04,80 --> 00:01:07,10
The first one is the modulus operator,

27
00:01:07,10 --> 00:01:10,30
which is the percent sign.

28
00:01:10,30 --> 00:01:11,90
It looks like this.

29
00:01:11,90 --> 00:01:14,60
The modulus operator takes the first operand,

30
00:01:14,60 --> 00:01:17,00
which in this case is 20, divides it by the second,

31
00:01:17,00 --> 00:01:19,40
and returns the remainder of that division.

32
00:01:19,40 --> 00:01:21,30
This is often used in programming

33
00:01:21,30 --> 00:01:24,20
to determine whether a number is even or odd.

34
00:01:24,20 --> 00:01:25,70
But it could also tell you something like,

35
00:01:25,70 --> 00:01:28,30
if I'm showing a list of search results in groups of 20,

36
00:01:28,30 --> 00:01:31,30
and there are 254 of them, how many will be on the last

37
00:01:31,30 --> 00:01:33,10
page of results?

38
00:01:33,10 --> 00:01:35,70
Anyway, this is what the usage looks like.

39
00:01:35,70 --> 00:01:40,20
This is 20 divided by two, which has a remainder of zero,

40
00:01:40,20 --> 00:01:44,80
but if I change this to 19, the remainder is one.

41
00:01:44,80 --> 00:01:46,60
If we wanted to find out if a number was even,

42
00:01:46,60 --> 00:01:49,20
we do a comparison that looks like this one.

43
00:01:49,20 --> 00:01:51,50
Normally we'd use a variable instead of a scaler value

44
00:01:51,50 --> 00:01:54,30
like 20, that's what we call it when we're using a value

45
00:01:54,30 --> 00:01:57,20
directly instead of assigning it to a variable first.

46
00:01:57,20 --> 00:02:00,50
But doing it this way makes what we're checking very clear.

47
00:02:00,50 --> 00:02:02,60
So that's the modulus operator.

48
00:02:02,60 --> 00:02:05,00
Now let's look at incrementing and decrementing.

49
00:02:05,00 --> 00:02:07,80
Which is something you do all the time in programming.

50
00:02:07,80 --> 00:02:10,20
Let's say I have a variable called counter,

51
00:02:10,20 --> 00:02:13,10
and I'll set it to zero to start.

52
00:02:13,10 --> 00:02:14,70
Many times when we're working through something

53
00:02:14,70 --> 00:02:16,00
in programming, you need to keep track

54
00:02:16,00 --> 00:02:17,80
of how often you've done it.

55
00:02:17,80 --> 00:02:19,80
Every time you do whatever that unit of work is,

56
00:02:19,80 --> 00:02:21,90
you tell a counter, all right I've done this one more time,

57
00:02:21,90 --> 00:02:23,50
go increment yourself.

58
00:02:23,50 --> 00:02:25,40
There are various ways that we can do that.

59
00:02:25,40 --> 00:02:27,20
The first is to take counter,

60
00:02:27,20 --> 00:02:32,20
and assign it to itself plus one.

61
00:02:32,20 --> 00:02:34,10
That works, but it's a little verbose,

62
00:02:34,10 --> 00:02:35,70
and there's always this tension in programming

63
00:02:35,70 --> 00:02:38,30
between keeping things verbose so they're easy to read,

64
00:02:38,30 --> 00:02:40,50
and making it so you don't have to type a lot of stuff

65
00:02:40,50 --> 00:02:42,80
over and over again for something that you do often.

66
00:02:42,80 --> 00:02:45,40
So there are various ways to express the same thing

67
00:02:45,40 --> 00:02:46,90
that are a little shorter.

68
00:02:46,90 --> 00:02:49,60
The first one is to take your variable name and use the plus

69
00:02:49,60 --> 00:02:54,70
equals, also known as the addition assignment operator.

70
00:02:54,70 --> 00:02:58,20
So in this case, if I say counter plus equals one,

71
00:02:58,20 --> 00:03:00,60
this is expressing exactly what the previous statement

72
00:03:00,60 --> 00:03:02,50
expresses, but it's a little shorter,

73
00:03:02,50 --> 00:03:04,60
still pretty clear, though.

74
00:03:04,60 --> 00:03:07,00
So now counter is two.

75
00:03:07,00 --> 00:03:09,40
Another common way is to take the variable name

76
00:03:09,40 --> 00:03:12,10
and simply add two plus signs on the end of it.

77
00:03:12,10 --> 00:03:13,60
Like this.

78
00:03:13,60 --> 00:03:16,10
Now this returns two, but the actual value of counter

79
00:03:16,10 --> 00:03:20,00
now is three.

80
00:03:20,00 --> 00:03:22,10
This will only work for incrementing by one,

81
00:03:22,10 --> 00:03:24,20
whereas with the plus equals,

82
00:03:24,20 --> 00:03:26,40
you can increment by whatever you want.

83
00:03:26,40 --> 00:03:30,90
So I could say counter plus equals five if I wanted to.

84
00:03:30,90 --> 00:03:32,60
And if even works with negative numbers.

85
00:03:32,60 --> 00:03:36,80
So I could say counter plus equals negative four.

86
00:03:36,80 --> 00:03:39,40
You can use whichever of these feels right to you.

87
00:03:39,40 --> 00:03:41,50
Similarly to incrementing, there's also decrementing

88
00:03:41,50 --> 00:03:43,60
operators, which are exactly the same thing,

89
00:03:43,60 --> 00:03:47,60
except instead of using the plus, you use a minus.

90
00:03:47,60 --> 00:03:49,20
So if I want to count down from somewhere,

91
00:03:49,20 --> 00:03:53,30
I can say counter, minus equals one.

92
00:03:53,30 --> 00:03:57,20
And then counter minus minus.

93
00:03:57,20 --> 00:03:59,40
So again, it returns the previous value,

94
00:03:59,40 --> 00:04:03,00
but the actual value now is two.

95
00:04:03,00 --> 00:04:04,60
One thing that's worth mentioning is that all the

96
00:04:04,60 --> 00:04:06,90
arithmetic operators we've seen so far

97
00:04:06,90 --> 00:04:09,20
can work with this assignment syntax.

98
00:04:09,20 --> 00:04:15,80
So for example, if I say counter asterisk equals two,

99
00:04:15,80 --> 00:04:17,60
that will double it.

100
00:04:17,60 --> 00:04:20,50
It was two before and now it's four.

101
00:04:20,50 --> 00:04:22,20
I don't see this kind of construct very often,

102
00:04:22,20 --> 00:04:24,70
outside of incrementing and decrementing,

103
00:04:24,70 --> 00:04:26,40
but if you see it, that's what it means.

104
00:04:26,40 --> 00:04:29,60
And this works for pretty much all arithmetic operators.

105
00:04:29,60 --> 00:04:31,20
One more note about these.

106
00:04:31,20 --> 00:04:34,00
Plus also works on strings.

107
00:04:34,00 --> 00:04:35,10
But when you use it with strings,

108
00:04:35,10 --> 00:04:37,60
it's called concatenation operator.

109
00:04:37,60 --> 00:04:43,60
So if I say cat plus dog, it gives me catdog.

110
00:04:43,60 --> 00:04:45,10
They're stuck together.

111
00:04:45,10 --> 00:04:47,10
If you want to combine strings as words,

112
00:04:47,10 --> 00:04:48,20
you need to make sure that spaces

113
00:04:48,20 --> 00:04:51,20
are included somewhere in there.

114
00:04:51,20 --> 00:04:53,80
So in this case, cat space plus dog

115
00:04:53,80 --> 00:04:56,90
will give you cat and dog as separate words

116
00:04:56,90 --> 00:04:59,20
but in a single string.

117
00:04:59,20 --> 00:05:00,70
And this doesn't work with just two strings,

118
00:05:00,70 --> 00:05:03,00
you can add more strings onto the end here.

119
00:05:03,00 --> 00:05:07,30
So we can say cat and

120
00:05:07,30 --> 00:05:08,90
dog.

121
00:05:08,90 --> 00:05:10,20
Like this.

122
00:05:10,20 --> 00:05:12,30
So the plus operator works on strings to concatenate

123
00:05:12,30 --> 00:05:14,60
them together, while working in arithmetic terms

124
00:05:14,60 --> 00:05:16,20
just as simple addition.

125
00:05:16,20 --> 00:05:17,80
And that's an overview of the most common

126
00:05:17,80 --> 00:05:20,10
and probably most useful arithmetic operators

127
00:05:20,10 --> 00:05:22,10
and what they do, including a little exception

128
00:05:22,10 --> 00:05:24,00
on how they work with strings.

