1
00:00:00,50 --> 00:00:01,90
- [Instructor] In this video, we're going to talk about

2
00:00:01,90 --> 00:00:04,50
the main logical operators in JavaScript,

3
00:00:04,50 --> 00:00:10,00
the logical AND, the logical OR, and the logical NOT.

4
00:00:10,00 --> 00:00:11,60
When you're working through JavaScript,

5
00:00:11,60 --> 00:00:13,50
there will be times when you want to do comparisons

6
00:00:13,50 --> 00:00:15,70
on more than one thing at a time,

7
00:00:15,70 --> 00:00:18,30
basically asking a question in multiple parts.

8
00:00:18,30 --> 00:00:20,70
And the way you can do that is by grouping things together

9
00:00:20,70 --> 00:00:23,20
with either an AND or an OR.

10
00:00:23,20 --> 00:00:27,80
The AND in JavaScript looks like this,

11
00:00:27,80 --> 00:00:29,60
two ampersands.

12
00:00:29,60 --> 00:00:31,70
And the OR is two pipe characters,

13
00:00:31,70 --> 00:00:33,50
which on an American keyboard you can find

14
00:00:33,50 --> 00:00:36,90
right above the Return key, on the Backslash key.

15
00:00:36,90 --> 00:00:40,70
You hold down Shift, type a backslash, and you get a pipe.

16
00:00:40,70 --> 00:00:43,90
So how do we use these?

17
00:00:43,90 --> 00:00:46,70
I'm going to copy a few variable assignments

18
00:00:46,70 --> 00:00:50,50
and bring them over into my browser, paste them in.

19
00:00:50,50 --> 00:00:52,80
animal1 is a monkey, animal2 is a bear,

20
00:00:52,80 --> 00:00:55,00
and animal3 is a tiger.

21
00:00:55,00 --> 00:00:56,60
Let's say there's a bunch of code

22
00:00:56,60 --> 00:00:58,60
that does some other stuff somewhere in the middle,

23
00:00:58,60 --> 00:01:00,30
and later on I want to come back

24
00:01:00,30 --> 00:01:02,70
and check the state of these variables.

25
00:01:02,70 --> 00:01:05,40
I can use the strict equality operator

26
00:01:05,40 --> 00:01:09,30
to ask JavaScript is animal1 still a monkey?

27
00:01:09,30 --> 00:01:11,50
And maybe I want to ask at the same time,

28
00:01:11,50 --> 00:01:13,30
is animal2 still a bear?

29
00:01:13,30 --> 00:01:15,60
If it's important to ask these two things together

30
00:01:15,60 --> 00:01:17,60
and then execute a piece of code,

31
00:01:17,60 --> 00:01:19,60
only when that whole thing is true,

32
00:01:19,60 --> 00:01:21,90
then I can use the logical AND,

33
00:01:21,90 --> 00:01:25,60
and add on my second condition, like this.

34
00:01:25,60 --> 00:01:32,50
I'm asking is animal1 a monkey and is animal2 a bear?

35
00:01:32,50 --> 00:01:35,60
And that whole statement evaluates down to true.

36
00:01:35,60 --> 00:01:39,70
JavaScript is saying yes, this is all true.

37
00:01:39,70 --> 00:01:43,60
Now if I wanted to find out if animal1 is an ape

38
00:01:43,60 --> 00:01:46,90
and animal2 is a bear, JavaScript returns false.

39
00:01:46,90 --> 00:01:50,40
The idea here is that the AND operator will return true

40
00:01:50,40 --> 00:01:52,40
only if every condition you pass to it,

41
00:01:52,40 --> 00:02:02,10
separated by that AND, is true.

42
00:02:02,10 --> 00:02:05,10
This is the case no matter how many comparisons I add on.

43
00:02:05,10 --> 00:02:06,80
If any one of them is false,

44
00:02:06,80 --> 00:02:12,10
the entire thing still evaluates to false.

45
00:02:12,10 --> 00:02:14,90
For a bunch of these ANDs to come back true,

46
00:02:14,90 --> 00:02:18,80
every single one of them must be true.

47
00:02:18,80 --> 00:02:21,20
If I only care about one of these conditions being true,

48
00:02:21,20 --> 00:02:23,70
I can use OR instead.

49
00:02:23,70 --> 00:02:25,70
I can ask is animal1 a monkey

50
00:02:25,70 --> 00:02:30,10
or, using two pipes,

51
00:02:30,10 --> 00:02:31,90
is animal2 a bear?

52
00:02:31,90 --> 00:02:33,90
And that's true.

53
00:02:33,90 --> 00:02:36,30
If I change monkey into an ape,

54
00:02:36,30 --> 00:02:40,00
now I'm asking is animal1 an ape or is animal2 a bear?

55
00:02:40,00 --> 00:02:42,50
animal2 is a bear, so this part is true,

56
00:02:42,50 --> 00:02:46,00
which makes the whole thing true.

57
00:02:46,00 --> 00:03:01,90
For ORs to return false, the entire thing needs to be false.

58
00:03:01,90 --> 00:03:05,50
You can also combine these ANDs and ORs on a single line.

59
00:03:05,50 --> 00:03:07,70
But now things get a little more complicated.

60
00:03:07,70 --> 00:03:10,20
JavaScript has a notion of operator precedence,

61
00:03:10,20 --> 00:03:11,70
which determines which of these things

62
00:03:11,70 --> 00:03:13,10
will be evaluated first

63
00:03:13,10 --> 00:03:15,80
and then collapsed into its true or false value

64
00:03:15,80 --> 00:03:18,50
before being compared against the others.

65
00:03:18,50 --> 00:03:20,30
And in JavaScript, with operator precedence,

66
00:03:20,30 --> 00:03:24,50
the ANDs are always evaluated down before the ORs.

67
00:03:24,50 --> 00:03:25,70
So what that means

68
00:03:25,70 --> 00:03:28,00
is

69
00:03:28,00 --> 00:03:29,80
these two, because they have an AND between them,

70
00:03:29,80 --> 00:03:30,80
would be collapsed down

71
00:03:30,80 --> 00:03:35,20
if their value of true or false, whatever it is,

72
00:03:35,20 --> 00:03:40,30
and then this comparison would be checked.

73
00:03:40,30 --> 00:03:41,70
So if I have a series of comparisons

74
00:03:41,70 --> 00:03:44,80
and I need to make sure that some ORs are kept together

75
00:03:44,80 --> 00:03:47,20
instead of being eaten up by these ANDs,

76
00:03:47,20 --> 00:03:54,40
I need to add some parenthesis, something like this.

77
00:03:54,40 --> 00:03:57,20
So this would make sure that these ORs stick together

78
00:03:57,20 --> 00:04:00,00
for the purpose of evaluating down to true or false,

79
00:04:00,00 --> 00:04:02,20
whichever it is.

80
00:04:02,20 --> 00:04:03,10
Now I know at certain points,

81
00:04:03,10 --> 00:04:04,70
your eyes could be rolling into the back of your head

82
00:04:04,70 --> 00:04:06,70
from seeing true and false all the time here,

83
00:04:06,70 --> 00:04:08,50
but there will be times when running your own code

84
00:04:08,50 --> 00:04:09,80
or when reading other folks' code,

85
00:04:09,80 --> 00:04:11,70
when you're going to see lots of comparisons,

86
00:04:11,70 --> 00:04:13,10
and hopefully they will have made things

87
00:04:13,10 --> 00:04:15,30
a little easier for you by putting in lots of parenthesis

88
00:04:15,30 --> 00:04:18,90
and generally trying to make things as clear as possible.

89
00:04:18,90 --> 00:04:20,80
One more thing about logical operators.

90
00:04:20,80 --> 00:04:23,40
You can also use the logical NOT,

91
00:04:23,40 --> 00:04:26,60
an exclamation mark or bang, to invert things.

92
00:04:26,60 --> 00:04:28,10
Here's how that works.

93
00:04:28,10 --> 00:04:31,30
So we would say NOT true,

94
00:04:31,30 --> 00:04:33,10
which is false.

95
00:04:33,10 --> 00:04:36,10
Likewise, NOT false is true.

96
00:04:36,10 --> 00:04:38,80
You can apply NOT to entire comparisons as well,

97
00:04:38,80 --> 00:04:42,30
wrapping the whole expression in parenthesis

98
00:04:42,30 --> 00:04:45,90
and sticking a NOT in front of it.

99
00:04:45,90 --> 00:04:47,70
That takes the entire expression,

100
00:04:47,70 --> 00:04:49,40
whether it evaluates to true or false,

101
00:04:49,40 --> 00:04:51,40
and then inverts it to the other one.

102
00:04:51,40 --> 00:04:52,60
And just to be clear,

103
00:04:52,60 --> 00:04:57,20
the NOT isn't just altering the strict equality operators.

104
00:04:57,20 --> 00:05:07,00
So this is not the same

105
00:05:07,00 --> 00:05:08,60
as this.

106
00:05:08,60 --> 00:05:12,00
It also flips any ANDs to ORs.

107
00:05:12,00 --> 00:05:13,30
So it's sort of like a magic wand

108
00:05:13,30 --> 00:05:15,20
that touches each of these pieces

109
00:05:15,20 --> 00:05:17,70
and changes it to the other one.

110
00:05:17,70 --> 00:05:20,20
Being able to asked these questions with logical operators

111
00:05:20,20 --> 00:05:21,60
is something that you're mostly going to see

112
00:05:21,60 --> 00:05:23,60
when we start getting into control structures,

113
00:05:23,60 --> 00:05:25,90
which are how you execute certain portions of your code

114
00:05:25,90 --> 00:05:27,80
and maybe not others.

115
00:05:27,80 --> 00:05:29,00
You won't as often be writing out

116
00:05:29,00 --> 00:05:31,10
these statements like this, bear,

117
00:05:31,10 --> 00:05:33,10
without some sort of control structure

118
00:05:33,10 --> 00:05:35,80
like an if statement or something along those lines.

119
00:05:35,80 --> 00:05:36,90
But now you've had an introduction

120
00:05:36,90 --> 00:05:40,20
to the main logical operators, AND, OR, and NOT,

121
00:05:40,20 --> 00:05:43,00
and some of the various things they can do in your code.

