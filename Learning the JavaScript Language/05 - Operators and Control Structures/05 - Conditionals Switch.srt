1
00:00:00,60 --> 00:00:01,50
- [Narrator] In this video we're going to look

2
00:00:01,50 --> 00:00:03,50
at another way of doing conditional branching

3
00:00:03,50 --> 00:00:06,70
in Javascript, which is the switch statement.

4
00:00:06,70 --> 00:00:08,50
These are similar to if statements and results,

5
00:00:08,50 --> 00:00:09,60
but they look different

6
00:00:09,60 --> 00:00:12,10
and act differently at times.

7
00:00:12,10 --> 00:00:15,30
Here's our example using if statements.

8
00:00:15,30 --> 00:00:18,40
We have a bunch of fs and elses in here.

9
00:00:18,40 --> 00:00:21,10
Works great, but if you have a lot of else ifs,

10
00:00:21,10 --> 00:00:22,20
it can start to get a little tedious

11
00:00:22,20 --> 00:00:23,20
writing that over and over again

12
00:00:23,20 --> 00:00:25,30
and adding all the braces and so forth.

13
00:00:25,30 --> 00:00:27,00
At times like this, you might consider using

14
00:00:27,00 --> 00:00:28,90
a switch block instead.

15
00:00:28,90 --> 00:00:31,20
Here's what this same thing looks like

16
00:00:31,20 --> 00:00:33,70
written using switch.

17
00:00:33,70 --> 00:00:34,90
We start by saying switch,

18
00:00:34,90 --> 00:00:37,40
and then in the parentheses we put an expression

19
00:00:37,40 --> 00:00:39,20
that we want to evaluate.

20
00:00:39,20 --> 00:00:41,10
Most of the time it's going to be a variable,

21
00:00:41,10 --> 00:00:42,60
but it can be anything that evaluates down

22
00:00:42,60 --> 00:00:45,70
to an expression that you can check below.

23
00:00:45,70 --> 00:00:48,80
But, as I say, usually it's a variable.

24
00:00:48,80 --> 00:00:50,40
Each of these subsequent blocks

25
00:00:50,40 --> 00:00:52,40
is called a case block.

26
00:00:52,40 --> 00:00:55,50
It's the keyword "case" followed by a value

27
00:00:55,50 --> 00:00:57,30
and then a colon.

28
00:00:57,30 --> 00:00:59,00
The colon can have spaces before it or not.

29
00:00:59,00 --> 00:01:00,40
Doesn't matter.

30
00:01:00,40 --> 00:01:01,80
And then after that "case"

31
00:01:01,80 --> 00:01:03,40
is what's supposed to happen.

32
00:01:03,40 --> 00:01:04,20
Just like with an if block,

33
00:01:04,20 --> 00:01:06,60
except there are no surrounding curly braces.

34
00:01:06,60 --> 00:01:07,80
Instead, each block ends

35
00:01:07,80 --> 00:01:10,00
with the keyword "break."

36
00:01:10,00 --> 00:01:11,10
So in this case I'm asking,

37
00:01:11,10 --> 00:01:12,90
"what is the value of answer?"

38
00:01:12,90 --> 00:01:14,80
I start here, and if the answer if "yes,"

39
00:01:14,80 --> 00:01:16,40
I'll execute what happens here

40
00:01:16,40 --> 00:01:19,30
and then break out of the switch block.

41
00:01:19,30 --> 00:01:21,50
If it's not yes, I'll check here next.

42
00:01:21,50 --> 00:01:22,90
Is it "maybe"?

43
00:01:22,90 --> 00:01:25,40
If it is, then I'll execute whatever's in here

44
00:01:25,40 --> 00:01:27,90
and break out of the switch block.

45
00:01:27,90 --> 00:01:28,70
Same thing.

46
00:01:28,70 --> 00:01:32,40
If it's "no," execute here, break out, and so on.

47
00:01:32,40 --> 00:01:33,50
Now as with an if block,

48
00:01:33,50 --> 00:01:35,60
where we can have an else condition at the end,

49
00:01:35,60 --> 00:01:36,90
with a switch statement we have

50
00:01:36,90 --> 00:01:38,90
a default at the end.

51
00:01:38,90 --> 00:01:40,60
Default in a switch is just like else

52
00:01:40,60 --> 00:01:41,60
in an if block.

53
00:01:41,60 --> 00:01:42,90
If none of the other stuff matches,

54
00:01:42,90 --> 00:01:45,30
we execute this block and finally,

55
00:01:45,30 --> 00:01:49,40
also, break out of the switch.

56
00:01:49,40 --> 00:01:51,20
So let's copy everything.

57
00:01:51,20 --> 00:01:53,50
I'll switch over to my browser.

58
00:01:53,50 --> 00:01:57,10
Going to remove the if block.

59
00:01:57,10 --> 00:02:00,40
And we'll execute this whole thing.

60
00:02:00,40 --> 00:02:02,50
Let's try "yes."

61
00:02:02,50 --> 00:02:04,50
You said yes.

62
00:02:04,50 --> 00:02:06,30
And up arrow to try it again.

63
00:02:06,30 --> 00:02:08,00
Try "maybe."

64
00:02:08,00 --> 00:02:10,00
You said "maybe," I don't know what to make of that.

65
00:02:10,00 --> 00:02:12,80
And one more time.

66
00:02:12,80 --> 00:02:14,80
Cookie.

67
00:02:14,80 --> 00:02:16,00
You rebel, you.

68
00:02:16,00 --> 00:02:17,20
Okay.

69
00:02:17,20 --> 00:02:20,50
So far, this works exactly like an if block.

70
00:02:20,50 --> 00:02:24,10
It's just written a little differently.

71
00:02:24,10 --> 00:02:25,50
But there's one thing that's a little tricky

72
00:02:25,50 --> 00:02:27,50
with switch statements, and actually useful,

73
00:02:27,50 --> 00:02:29,60
that you need to be aware of.

74
00:02:29,60 --> 00:02:30,50
They can cascade in a way

75
00:02:30,50 --> 00:02:32,60
that if statements don't.

76
00:02:32,60 --> 00:02:33,50
So I'll use the up arrow to get

77
00:02:33,50 --> 00:02:35,10
this whole block back.

78
00:02:35,10 --> 00:02:36,10
And I'm just going to do one thing.

79
00:02:36,10 --> 00:02:39,60
I'm going to remove "break" from the "yes" block.

80
00:02:39,60 --> 00:02:41,30
Now I'll execute it.

81
00:02:41,30 --> 00:02:45,40
Watch what happens when I type in "yes."

82
00:02:45,40 --> 00:02:47,00
Not only do I get "you said yes,"

83
00:02:47,00 --> 00:02:49,90
I also get "you said maybe."

84
00:02:49,90 --> 00:02:51,20
Just to be clear of what's happening here,

85
00:02:51,20 --> 00:02:54,40
I'm going to try this again and type "maybe,"

86
00:02:54,40 --> 00:02:57,80
and I only get the "maybe" block this time.

87
00:02:57,80 --> 00:02:59,30
So this is the thing about switch statements:

88
00:02:59,30 --> 00:03:00,50
if you don't include a break,

89
00:03:00,50 --> 00:03:02,90
the execution will continue

90
00:03:02,90 --> 00:03:06,70
down subsequent case blocks until a break is hit.

91
00:03:06,70 --> 00:03:08,30
I typed in "yes."

92
00:03:08,30 --> 00:03:11,00
So first I hit the "yes" block and it executed

93
00:03:11,00 --> 00:03:12,40
"you said yes,"

94
00:03:12,40 --> 00:03:14,80
and then it continued down into the "maybe" block

95
00:03:14,80 --> 00:03:16,50
and did whatever's next.

96
00:03:16,50 --> 00:03:19,60
And it kept going until it got to a break.

97
00:03:19,60 --> 00:03:21,00
So you want to be careful with this.

98
00:03:21,00 --> 00:03:22,30
If you just want a single block

99
00:03:22,30 --> 00:03:23,90
to execute for each case,

100
00:03:23,90 --> 00:03:26,30
then you need to include that break statement.

101
00:03:26,30 --> 00:03:28,20
You can also leave it off on purpose, of course,

102
00:03:28,20 --> 00:03:30,10
if you want a block to be able to cascade

103
00:03:30,10 --> 00:03:33,00
down several conditions in certain cases.

104
00:03:33,00 --> 00:03:35,70
If statements kind of can't do that easily.

105
00:03:35,70 --> 00:03:37,50
Most of the way that ifs and switches work

106
00:03:37,50 --> 00:03:39,30
is the same in popular usage,

107
00:03:39,30 --> 00:03:41,90
and which you want will be a matter of taste.

108
00:03:41,90 --> 00:03:43,40
The curly braces of if blocks

109
00:03:43,40 --> 00:03:44,70
can be easier to read for some people

110
00:03:44,70 --> 00:03:47,20
and can be better supported by some code editors

111
00:03:47,20 --> 00:03:48,50
than switch blocks.

112
00:03:48,50 --> 00:03:49,80
But for lots of branching,

113
00:03:49,80 --> 00:03:51,90
a switch can be more pleasant to read and write.

114
00:03:51,90 --> 00:03:53,00
It's really up to you.

