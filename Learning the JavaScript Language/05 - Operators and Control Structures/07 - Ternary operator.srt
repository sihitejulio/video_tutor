1
00:00:00,50 --> 00:00:01,30
- [Instructor] In this video

2
00:00:01,30 --> 00:00:04,40
we'll look at one more quite terse, if construct,

3
00:00:04,40 --> 00:00:06,20
which is called the ternary operator

4
00:00:06,20 --> 00:00:08,40
or the conditional operator.

5
00:00:08,40 --> 00:00:12,40
I'm going to copy this block and bring it over into Chrome.

6
00:00:12,40 --> 00:00:14,70
So this is that conditional operator in action.

7
00:00:14,70 --> 00:00:19,20
It's an expression, followed by a question mark,

8
00:00:19,20 --> 00:00:20,70
followed by some other expression,

9
00:00:20,70 --> 00:00:21,90
which is what you want to happen

10
00:00:21,90 --> 00:00:26,30
when this expression is true, then a colon,

11
00:00:26,30 --> 00:00:30,10
and what you want to happen with this is false.

12
00:00:30,10 --> 00:00:32,80
Let's try executing this.

13
00:00:32,80 --> 00:00:37,40
Because the animal is a cat, I check whether it was true,

14
00:00:37,40 --> 00:00:40,10
and if so, I do what appears between the question mark

15
00:00:40,10 --> 00:00:44,50
and the colon.

16
00:00:44,50 --> 00:00:47,40
If the animal were a dog,

17
00:00:47,40 --> 00:00:49,20
I would do what appears after the colon,

18
00:00:49,20 --> 00:00:53,90
ignoring what is between the question mark and the colon.

19
00:00:53,90 --> 00:00:56,60
The ternary operator is often used to set a variable,

20
00:00:56,60 --> 00:01:00,20
based on certain conditions.

21
00:01:00,20 --> 00:01:01,30
Here's a line that does that.

22
00:01:01,30 --> 00:01:05,70
We're going to copy the whole thing and bring it over.

23
00:01:05,70 --> 00:01:08,90
So having executed this, animal is a cat.

24
00:01:08,90 --> 00:01:12,70
Here's my console message right here from this first block

25
00:01:12,70 --> 00:01:15,60
and then I set the job variable based on the same condition,

26
00:01:15,60 --> 00:01:16,50
pretty much.

27
00:01:16,50 --> 00:01:21,10
The job right now is cat herder.

28
00:01:21,10 --> 00:01:24,50
So, if I change animal to dog,

29
00:01:24,50 --> 00:01:27,60
(clicking)

30
00:01:27,60 --> 00:01:29,50
and I reassign the job variable,

31
00:01:29,50 --> 00:01:32,80
(clicking)

32
00:01:32,80 --> 00:01:35,50
now I get dog catcher.

33
00:01:35,50 --> 00:01:37,80
The ternary operator is pretty terse

34
00:01:37,80 --> 00:01:39,50
and remember which order the question mark

35
00:01:39,50 --> 00:01:42,10
and the colon go in can be a little tricky at first.

36
00:01:42,10 --> 00:01:44,30
I know I avoided it myself for a while,

37
00:01:44,30 --> 00:01:45,50
but at this point I've gotten used to it

38
00:01:45,50 --> 00:01:48,10
and I've started using it more often.

39
00:01:48,10 --> 00:01:49,70
You can add parentheses to this

40
00:01:49,70 --> 00:01:51,60
to help make it a little clearer.

41
00:01:51,60 --> 00:01:53,30
So if I type an up arrow,

42
00:01:53,30 --> 00:01:56,20
I can add parentheses here.

43
00:01:56,20 --> 00:01:57,40
This really makes it obvious

44
00:01:57,40 --> 00:01:59,20
that I'm checking a condition here

45
00:01:59,20 --> 00:02:01,20
before trying these other actions.

46
00:02:01,20 --> 00:02:03,90
I can even add parentheses here if I wanted to.

47
00:02:03,90 --> 00:02:07,90
(clicking)

48
00:02:07,90 --> 00:02:10,20
Adding parentheses to surround expressions like this,

49
00:02:10,20 --> 00:02:12,10
if there's no order of operations to care about,

50
00:02:12,10 --> 00:02:14,30
can really enhance readability.

51
00:02:14,30 --> 00:02:16,50
But as with anything in any kind of programing,

52
00:02:16,50 --> 00:02:18,40
there's always a trade off between terseness

53
00:02:18,40 --> 00:02:19,70
and readability.

54
00:02:19,70 --> 00:02:21,60
But all these terse if statements,

55
00:02:21,60 --> 00:02:23,40
one liners, one variable conditions,

56
00:02:23,40 --> 00:02:26,60
and now these ternaries, are very common in the wild,

57
00:02:26,60 --> 00:02:27,40
and you may decide

58
00:02:27,40 --> 00:02:30,00
that you want to use them in your code as well.

