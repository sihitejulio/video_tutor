1
00:00:00,50 --> 00:00:01,70
- [Instructor] In this video, we're going to look at

2
00:00:01,70 --> 00:00:04,80
another type of for loop, the enumerative for loop.

3
00:00:04,80 --> 00:00:07,30
This is one that you can use on arrays or objects,

4
00:00:07,30 --> 00:00:08,50
and it's very useful,

5
00:00:08,50 --> 00:00:10,80
but you may need to be a little careful with it.

6
00:00:10,80 --> 00:00:12,90
So here I have a couple of code samples.

7
00:00:12,90 --> 00:00:16,80
First we'll look at iterating over an array with this thing.

8
00:00:16,80 --> 00:00:18,70
I have a variable called pageNames.

9
00:00:18,70 --> 00:00:20,80
It's an array with six items.

10
00:00:20,80 --> 00:00:22,60
Now to iterate over this using the techniques

11
00:00:22,60 --> 00:00:24,50
we already know, I could use a for loop

12
00:00:24,50 --> 00:00:26,50
where I'm sending I to zero, going up to the length

13
00:00:26,50 --> 00:00:29,80
of the pageNames array and incrementing every time I go.

14
00:00:29,80 --> 00:00:31,10
There's another way to do this,

15
00:00:31,10 --> 00:00:34,10
that uses the enumerative for loop.

16
00:00:34,10 --> 00:00:35,40
This is what it looks like.

17
00:00:35,40 --> 00:00:38,30
We start out with for followed by parentheses again,

18
00:00:38,30 --> 00:00:39,90
and I'm initializing a variable,

19
00:00:39,90 --> 00:00:41,40
but instead of doing all the other stuff

20
00:00:41,40 --> 00:00:43,30
where I'm setting an upper bound

21
00:00:43,30 --> 00:00:45,20
and increment and decrement or however it is

22
00:00:45,20 --> 00:00:47,40
that I need to traverse that item.

23
00:00:47,40 --> 00:00:49,40
I'm just using the key word in,

24
00:00:49,40 --> 00:00:51,80
and then the name of the array.

25
00:00:51,80 --> 00:00:53,00
Each time through this loop,

26
00:00:53,00 --> 00:00:56,30
the variable p gets set to a different key from this array.

27
00:00:56,30 --> 00:00:58,00
With an array, the keys are numbers,

28
00:00:58,00 --> 00:01:00,60
and I'm expecting p to get set to a number each time

29
00:01:00,60 --> 00:01:02,80
for as many of them as there are.

30
00:01:02,80 --> 00:01:05,30
Now you'll notice I'm not saying anything about order.

31
00:01:05,30 --> 00:01:06,50
This is a caveat.

32
00:01:06,50 --> 00:01:09,50
The order is not guaranteed by the JavaScript specification

33
00:01:09,50 --> 00:01:12,00
when you're dealing with a for in loop like this.

34
00:01:12,00 --> 00:01:14,10
In my experience, you do tend to get the keys back

35
00:01:14,10 --> 00:01:15,70
in a sensible order with arras.

36
00:01:15,70 --> 00:01:17,70
We just can't depend on it.

37
00:01:17,70 --> 00:01:20,30
This is shorter and maybe a little more pleasant to read

38
00:01:20,30 --> 00:01:21,70
than a sequential for loop,

39
00:01:21,70 --> 00:01:23,90
but if the order of the original array items matters

40
00:01:23,90 --> 00:01:25,70
for your for loop, you can't trust

41
00:01:25,70 --> 00:01:27,90
these four in loops exactly.

42
00:01:27,90 --> 00:01:30,00
So let's copy all this code in,

43
00:01:30,00 --> 00:01:31,20
switch back to the browser,

44
00:01:31,20 --> 00:01:34,30
paste it in, and try it.

45
00:01:34,30 --> 00:01:35,20
So here I go.

46
00:01:35,20 --> 00:01:38,60
I have all of my keys, zero through five,

47
00:01:38,60 --> 00:01:40,80
'cause there are six items in here.

48
00:01:40,80 --> 00:01:42,90
And the names.

49
00:01:42,90 --> 00:01:45,00
Notice that my console.log here

50
00:01:45,00 --> 00:01:48,20
passed two items with a comma in between them.

51
00:01:48,20 --> 00:01:49,60
The console.log function can take

52
00:01:49,60 --> 00:01:51,50
as many arguments as you want to pass to it,

53
00:01:51,50 --> 00:01:53,90
and just prints them out like this.

54
00:01:53,90 --> 00:01:58,30
So zero is home, one is about us, and so forth.

55
00:01:58,30 --> 00:02:00,00
Like I said, often with arrays,

56
00:02:00,00 --> 00:02:01,90
a sequential for loop is more reliable,

57
00:02:01,90 --> 00:02:03,60
but you might see this in other folks' code,

58
00:02:03,60 --> 00:02:06,30
so I wanted you to see what it looks like.

59
00:02:06,30 --> 00:02:09,00
A for in loop can also be used on an object

60
00:02:09,00 --> 00:02:12,10
in a way that's harder to do with sequential for loops.

61
00:02:12,10 --> 00:02:16,30
So here's a pages object with various properties.

62
00:02:16,30 --> 00:02:20,20
I've just used ordinal words here and then names.

63
00:02:20,20 --> 00:02:23,80
Down here I have my for in loop that goes with it.

64
00:02:23,80 --> 00:02:25,80
I'm using a variable named p again,

65
00:02:25,80 --> 00:02:28,00
and this time in in pages.

66
00:02:28,00 --> 00:02:30,70
We're going to skip this line for a second,

67
00:02:30,70 --> 00:02:33,40
and it basically does the same thing.

68
00:02:33,40 --> 00:02:35,20
Just iterates over all of the keys

69
00:02:35,20 --> 00:02:37,40
that are in this array.

70
00:02:37,40 --> 00:02:40,20
So each time through, p is going to be set to first,

71
00:02:40,20 --> 00:02:42,60
second, third, and fourth,

72
00:02:42,60 --> 00:02:44,60
and then I'm using that to access that property

73
00:02:44,60 --> 00:02:47,40
inside the pages object.

74
00:02:47,40 --> 00:02:51,20
Let's copy this one and try it.

75
00:02:51,20 --> 00:02:54,60
Clear this, paste it in.

76
00:02:54,60 --> 00:02:56,40
There we go, here are all my keys.

77
00:02:56,40 --> 00:02:58,50
First, second, third, fourth, and fifth.

78
00:02:58,50 --> 00:03:01,30
And all the pages names that go with them.

79
00:03:01,30 --> 00:03:03,20
Now again, you cannot plan on these keys

80
00:03:03,20 --> 00:03:06,50
being in the order that you set them in the original object.

81
00:03:06,50 --> 00:03:07,70
That's very important.

82
00:03:07,70 --> 00:03:09,90
If the order of the keys matters,

83
00:03:09,90 --> 00:03:11,20
you should probably use an aray

84
00:03:11,20 --> 00:03:13,80
and use a sequential for loop.

85
00:03:13,80 --> 00:03:15,90
All right now let's go back and look at this loop

86
00:03:15,90 --> 00:03:18,20
a little more thoroughly.

87
00:03:18,20 --> 00:03:19,70
Specifically this line here.

88
00:03:19,70 --> 00:03:24,00
Where I'm checking if pages has own property p.

89
00:03:24,00 --> 00:03:25,30
So what I'm asking is,

90
00:03:25,30 --> 00:03:27,50
does this object, pages,

91
00:03:27,50 --> 00:03:30,50
have its own, that is an un-inherited property

92
00:03:30,50 --> 00:03:32,60
called whatever p is?

93
00:03:32,60 --> 00:03:34,80
Which would be first, second, third, and so on,

94
00:03:34,80 --> 00:03:36,90
each time through the lop.

95
00:03:36,90 --> 00:03:38,40
Without going to far into the weeds,

96
00:03:38,40 --> 00:03:40,40
JavaScript is an object oriented language

97
00:03:40,40 --> 00:03:43,00
that uses a model called prototypal inheritance.

98
00:03:43,00 --> 00:03:44,80
There's a video about that later in this course,

99
00:03:44,80 --> 00:03:46,30
but for our purposes right now,

100
00:03:46,30 --> 00:03:49,10
it means that the language can be modified very deeply,

101
00:03:49,10 --> 00:03:50,70
and an object like this,

102
00:03:50,70 --> 00:03:52,30
an object that you've created

103
00:03:52,30 --> 00:03:54,40
could have inherited properties that other parts

104
00:03:54,40 --> 00:03:56,50
of the code that's active have inserted,

105
00:03:56,50 --> 00:03:57,80
and those could potentially show up

106
00:03:57,80 --> 00:04:01,00
when you try to enumerate it using a loop like this.

107
00:04:01,00 --> 00:04:02,60
Using has own property

108
00:04:02,60 --> 00:04:04,30
is your way of safeguarding yourself

109
00:04:04,30 --> 00:04:07,50
against enumerating over things that you didn't mean to.

110
00:04:07,50 --> 00:04:09,30
As I say, the details of this are a little

111
00:04:09,30 --> 00:04:10,40
on the esoteric side,

112
00:04:10,40 --> 00:04:12,20
and there's certainty a lot of code out there

113
00:04:12,20 --> 00:04:15,00
where you won't run into this problem at all,

114
00:04:15,00 --> 00:04:17,40
but if you're going to enumerating over an object

115
00:04:17,40 --> 00:04:18,60
using a for in loop,

116
00:04:18,60 --> 00:04:20,70
it's pretty important to include this check.

117
00:04:20,70 --> 00:04:22,90
So just file that away and know that

118
00:04:22,90 --> 00:04:24,10
it's something to look for,

119
00:04:24,10 --> 00:04:25,60
and if you ever do see it in someone else's code,

120
00:04:25,60 --> 00:04:27,70
you'll know what it means.

121
00:04:27,70 --> 00:04:29,80
So that is how you can use enumerative for loops

122
00:04:29,80 --> 00:04:33,00
to iterate over an object or even an array.

