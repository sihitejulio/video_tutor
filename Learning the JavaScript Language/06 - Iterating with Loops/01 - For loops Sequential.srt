1
00:00:00,40 --> 00:00:02,10
- [Instructor] When we talk about programming computers

2
00:00:02,10 --> 00:00:04,00
it's always with a goal of making the machine

3
00:00:04,00 --> 00:00:05,50
do something that maybe we're not so good at

4
00:00:05,50 --> 00:00:08,00
or maybe we just don't want to.

5
00:00:08,00 --> 00:00:09,90
One of the things that a computer can do really well

6
00:00:09,90 --> 00:00:11,90
is the same thing over and over again.

7
00:00:11,90 --> 00:00:14,40
So what I'm talking about is called looping.

8
00:00:14,40 --> 00:00:16,60
In this lesson we're going to talk about for-loops,

9
00:00:16,60 --> 00:00:19,70
specifically we're going to talk about sequential for-loops.

10
00:00:19,70 --> 00:00:21,10
This is a way of telling the machine

11
00:00:21,10 --> 00:00:22,70
to do something over and over and over again

12
00:00:22,70 --> 00:00:24,40
a certain number of times.

13
00:00:24,40 --> 00:00:26,20
And it looks like this.

14
00:00:26,20 --> 00:00:29,40
You write the word for and then in some parentheses,

15
00:00:29,40 --> 00:00:33,50
you'll put three statements, followed by a curly brace.

16
00:00:33,50 --> 00:00:36,30
What we're planning to do here is something 10 times.

17
00:00:36,30 --> 00:00:37,80
And the first thing I'm going to do

18
00:00:37,80 --> 00:00:39,50
is initialize a variable.

19
00:00:39,50 --> 00:00:42,90
It's kind of a convention in for-loops to use I,

20
00:00:42,90 --> 00:00:46,30
which stands for index, and set it to zero.

21
00:00:46,30 --> 00:00:48,80
What this is called and what value you set it to

22
00:00:48,80 --> 00:00:49,80
are completely up to you

23
00:00:49,80 --> 00:00:53,20
and the requirements of the program that you're writing.

24
00:00:53,20 --> 00:00:55,30
Then we add a semicolon.

25
00:00:55,30 --> 00:00:56,80
So that's the initialization.

26
00:00:56,80 --> 00:00:59,00
The next part is when to stop.

27
00:00:59,00 --> 00:01:01,40
So if we're starting I at zero

28
00:01:01,40 --> 00:01:03,30
and we want this to execute 10 times,

29
00:01:03,30 --> 00:01:06,90
we want I to be less than 10.

30
00:01:06,90 --> 00:01:09,20
I could've phrased this as one

31
00:01:09,20 --> 00:01:14,00
and I is less than or equal to 10.

32
00:01:14,00 --> 00:01:16,00
It seems a little bit more common to use zero

33
00:01:16,00 --> 00:01:18,30
and then using less than instead

34
00:01:18,30 --> 00:01:19,60
of less than or equal too here.

35
00:01:19,60 --> 00:01:22,40
But again, stylistically it's up to you.

36
00:01:22,40 --> 00:01:25,10
That is also followed by a semicolon.

37
00:01:25,10 --> 00:01:27,60
So we have our initialization and our ending condition,

38
00:01:27,60 --> 00:01:29,90
but how do we get from there to there?

39
00:01:29,90 --> 00:01:31,00
Every time through this loop,

40
00:01:31,00 --> 00:01:33,30
we're going to increment by one.

41
00:01:33,30 --> 00:01:34,30
There are different ways

42
00:01:34,30 --> 00:01:36,00
that you can phrase this, of course.

43
00:01:36,00 --> 00:01:38,50
We're going to use the plus equals one version here,

44
00:01:38,50 --> 00:01:39,40
but you'll also see it

45
00:01:39,40 --> 00:01:41,40
very commonly expressed as I plus plus.

46
00:01:41,40 --> 00:01:45,00
Okay, and then comes our curly brace.

47
00:01:45,00 --> 00:01:48,40
I'm going to hit shift and return a couple of times

48
00:01:48,40 --> 00:01:50,90
and add a closing curly brace.

49
00:01:50,90 --> 00:01:52,10
And in between those curly braces,

50
00:01:52,10 --> 00:01:56,00
we put whatever we want to happen every time this loop runs.

51
00:01:56,00 --> 00:01:57,20
In this case we'll do something simple,

52
00:01:57,20 --> 00:02:00,70
we'll just log a console message

53
00:02:00,70 --> 00:02:03,30
and we'll send the current value of I,

54
00:02:03,30 --> 00:02:06,70
each time through the loop, to the console.

55
00:02:06,70 --> 00:02:08,50
So just to recap, what we're doing here

56
00:02:08,50 --> 00:02:10,90
is we're initializing a variable to zero.

57
00:02:10,90 --> 00:02:13,30
We're incrementing it by one every time through

58
00:02:13,30 --> 00:02:16,90
and stopping when it is no longer less than 10.

59
00:02:16,90 --> 00:02:21,00
So this should execute 10 times, let's try it.

60
00:02:21,00 --> 00:02:22,50
So it goes from zero through nine,

61
00:02:22,50 --> 00:02:24,80
for a total of 10 executions, great.

62
00:02:24,80 --> 00:02:28,00
Now of course, you can have as much stuff as you need

63
00:02:28,00 --> 00:02:30,20
inside these curly braces, not just one line

64
00:02:30,20 --> 00:02:32,40
and not something just this simple.

65
00:02:32,40 --> 00:02:33,90
Let's take a look at another example loop

66
00:02:33,90 --> 00:02:35,20
that's a little more like something

67
00:02:35,20 --> 00:02:37,80
you might use in the real world.

68
00:02:37,80 --> 00:02:40,30
I'm looking at this whole thing here.

69
00:02:40,30 --> 00:02:42,20
A very common use case for for-loops

70
00:02:42,20 --> 00:02:44,40
is to loop over the contents of an array.

71
00:02:44,40 --> 00:02:46,60
So here we have an array of page names,

72
00:02:46,60 --> 00:02:50,30
home, about us, and so forth, six items total.

73
00:02:50,30 --> 00:02:51,60
Now in the real world, we might come up

74
00:02:51,60 --> 00:02:53,80
with some clever way of populating this page titles array

75
00:02:53,80 --> 00:02:55,60
instead of writing them in statically like this,

76
00:02:55,60 --> 00:02:58,90
which is called hard coding, but for now this will work.

77
00:02:58,90 --> 00:03:00,50
So we have this array of page names

78
00:03:00,50 --> 00:03:02,10
and now we're going to look at a piece of code

79
00:03:02,10 --> 00:03:03,40
that will go through this array

80
00:03:03,40 --> 00:03:06,10
and tell us when we found the one that we're on.

81
00:03:06,10 --> 00:03:08,70
To do this on a webpage in a browser,

82
00:03:08,70 --> 00:03:10,30
we'd be accessing the document object

83
00:03:10,30 --> 00:03:11,90
the web browsers provide us,

84
00:03:11,90 --> 00:03:15,50
through something called the document object model or DOM,

85
00:03:15,50 --> 00:03:17,00
which is the interface that a web browser

86
00:03:17,00 --> 00:03:18,40
provides JavaScript developers

87
00:03:18,40 --> 00:03:20,70
for interacting with what's on the page.

88
00:03:20,70 --> 00:03:22,90
The document object has a property called title,

89
00:03:22,90 --> 00:03:27,20
which is the contents of the title tag on that page.

90
00:03:27,20 --> 00:03:28,80
So here's what we're doing with our for-loop,

91
00:03:28,80 --> 00:03:30,20
we're starting out at zero again

92
00:03:30,20 --> 00:03:32,20
and we're stopping when we get

93
00:03:32,20 --> 00:03:35,60
to the length of the page names array here.

94
00:03:35,60 --> 00:03:38,00
So we're checking pagenames.length

95
00:03:38,00 --> 00:03:41,80
instead of just saying six or whatever.

96
00:03:41,80 --> 00:03:43,10
If the length of the array changes,

97
00:03:43,10 --> 00:03:45,30
this piece of code does not have to.

98
00:03:45,30 --> 00:03:48,10
And then we're incrementing by one, just like we did before.

99
00:03:48,10 --> 00:03:50,80
So the first time that this loop executes,

100
00:03:50,80 --> 00:03:54,90
we're going to be checking page names sub zero, which is home.

101
00:03:54,90 --> 00:03:58,60
And if we were on the home page, this would execute,

102
00:03:58,60 --> 00:04:00,60
and if not, this one would execute

103
00:04:00,60 --> 00:04:03,60
and so on and so forth, all the way through all of these.

104
00:04:03,60 --> 00:04:06,70
So let's actually try this and see how it works.

105
00:04:06,70 --> 00:04:07,80
I'm going to copy this whole thing

106
00:04:07,80 --> 00:04:09,60
and switch back to my browser.

107
00:04:09,60 --> 00:04:10,90
To do this, we're going to need

108
00:04:10,90 --> 00:04:13,40
an actual page to execute it against.

109
00:04:13,40 --> 00:04:15,90
So what I'm going to do is switch over to this tab

110
00:04:15,90 --> 00:04:18,20
where I've loaded the sandbox.html file

111
00:04:18,20 --> 00:04:20,80
from the exercise files for this video.

112
00:04:20,80 --> 00:04:22,70
And I'm going to use command option I

113
00:04:22,70 --> 00:04:24,70
to open up my developer tools

114
00:04:24,70 --> 00:04:27,30
and switch to my JavaScript console.

115
00:04:27,30 --> 00:04:29,50
You can see, here's my tiny little page over here.

116
00:04:29,50 --> 00:04:32,10
This is basically just a little stub file,

117
00:04:32,10 --> 00:04:33,70
it doesn't have anything really meaningful in it,

118
00:04:33,70 --> 00:04:34,70
except that once it's loaded,

119
00:04:34,70 --> 00:04:37,70
I do have access to the document object model,

120
00:04:37,70 --> 00:04:39,60
including the page title that I can use

121
00:04:39,60 --> 00:04:41,30
through document.title.

122
00:04:41,30 --> 00:04:43,90
So this current page is JavaScript Playground

123
00:04:43,90 --> 00:04:45,60
as you can see up here in this tab.

124
00:04:45,60 --> 00:04:47,90
So let's paste in this code

125
00:04:47,90 --> 00:04:50,50
and execute it to see what happens.

126
00:04:50,50 --> 00:04:53,00
So there we go, we are not at any of these other places

127
00:04:53,00 --> 00:04:57,00
but we are here on the JavaScript Playground.

128
00:04:57,00 --> 00:05:00,80
I went through each of these items in the array, in turn

129
00:05:00,80 --> 00:05:02,80
because of the way this was constructed.

130
00:05:02,80 --> 00:05:07,40
And then my if statements executed this one or this one

131
00:05:07,40 --> 00:05:09,70
depending on where we were.

132
00:05:09,70 --> 00:05:11,00
If I want it to stop

133
00:05:11,00 --> 00:05:12,70
after I've found the page that we're on,

134
00:05:12,70 --> 00:05:14,40
I'll bring this whole block back,

135
00:05:14,40 --> 00:05:17,50
I'm going to hit shift return here

136
00:05:17,50 --> 00:05:21,10
and I could add a break to this if block,

137
00:05:21,10 --> 00:05:22,80
just the way we do with switches.

138
00:05:22,80 --> 00:05:25,70
Only in this case, it'll break the for-loop.

139
00:05:25,70 --> 00:05:27,50
So let me hit return and execute that.

140
00:05:27,50 --> 00:05:29,70
Once we got to the JavaScript Playground this time,

141
00:05:29,70 --> 00:05:34,00
the loop stopped, that's called short-circuiting the loop.

142
00:05:34,00 --> 00:05:35,70
One more quick note about this code.

143
00:05:35,70 --> 00:05:39,10
You can see that I have page names sub I

144
00:05:39,10 --> 00:05:43,30
reused three times in this little block.

145
00:05:43,30 --> 00:05:47,20
We could rewrite this to use a variable instead,

146
00:05:47,20 --> 00:05:49,80
which makes it more obvious what we're doing each time.

147
00:05:49,80 --> 00:05:51,20
Instead of page name sub I,

148
00:05:51,20 --> 00:05:53,90
we could say okay, yeah this is the current page title,

149
00:05:53,90 --> 00:05:56,00
so we're checking whether document.title

150
00:05:56,00 --> 00:05:59,20
is the current page title from this array.

151
00:05:59,20 --> 00:06:00,70
That just makes it a little more obvious

152
00:06:00,70 --> 00:06:01,90
and a little more readable,

153
00:06:01,90 --> 00:06:03,70
when we're looking through our loops.

154
00:06:03,70 --> 00:06:06,20
So that's a look at sequential for-loops,

155
00:06:06,20 --> 00:06:09,00
which do the same thing over and over again.

