1
00:00:00,50 --> 00:00:01,30
- [Instructor] In this video,

2
00:00:01,30 --> 00:00:02,50
we're going to talk about one more kind

3
00:00:02,50 --> 00:00:04,90
of loop called the while loop.

4
00:00:04,90 --> 00:00:08,40
To set up a comparison, let's look at a for loop first.

5
00:00:08,40 --> 00:00:11,50
This is the sequential for loop we've seen before

6
00:00:11,50 --> 00:00:13,90
where we have initialize variable,

7
00:00:13,90 --> 00:00:15,70
some bound where it should stop,

8
00:00:15,70 --> 00:00:18,40
and the way that we get to that bound.

9
00:00:18,40 --> 00:00:21,50
Then, inside the curly braces, we take some actions.

10
00:00:21,50 --> 00:00:24,00
This is the same thing expressed as a for loop.

11
00:00:24,00 --> 00:00:26,30
I'm initializing a variable

12
00:00:26,30 --> 00:00:29,10
and then I have the keyword while,

13
00:00:29,10 --> 00:00:32,30
and then in the parentheses is my terminating condition.

14
00:00:32,30 --> 00:00:35,00
Then, inside these curly braces,

15
00:00:35,00 --> 00:00:36,80
this will keep happening over and over again,

16
00:00:36,80 --> 00:00:38,20
whatever is in here,

17
00:00:38,20 --> 00:00:40,50
until I reach this terminating condition.

18
00:00:40,50 --> 00:00:41,50
To make sure I reach it,

19
00:00:41,50 --> 00:00:45,60
I include my incrementing inside the curly braces.

20
00:00:45,60 --> 00:00:50,00
These two loops do exactly the same thing.

21
00:00:50,00 --> 00:00:53,40
I'm going to copy this over into my browser just to prove

22
00:00:53,40 --> 00:00:55,90
that it works.

23
00:00:55,90 --> 00:00:58,20
There we go, zero through nine,

24
00:00:58,20 --> 00:01:02,10
just like we would have done with that for loop.

25
00:01:02,10 --> 00:01:03,10
This might raise the question,

26
00:01:03,10 --> 00:01:04,40
why would you use a while loop

27
00:01:04,40 --> 00:01:07,80
when you could just use a for loop or vice versa even?

28
00:01:07,80 --> 00:01:09,70
The primary reason you might use a while loop

29
00:01:09,70 --> 00:01:11,40
other than just maybe you like to write things

30
00:01:11,40 --> 00:01:13,30
in a certain way is when you don't know

31
00:01:13,30 --> 00:01:15,80
how many times you're going to need to loop over something.

32
00:01:15,80 --> 00:01:17,70
You don't have a particular object over which you're going

33
00:01:17,70 --> 00:01:19,20
to iterate a certain number of times

34
00:01:19,20 --> 00:01:21,00
or you don't have a particular counter

35
00:01:21,00 --> 00:01:23,10
where you know the end point.

36
00:01:23,10 --> 00:01:25,70
Here is an example of that.

37
00:01:25,70 --> 00:01:27,60
I have an array, it's called myArray,

38
00:01:27,60 --> 00:01:29,20
and it's full of Booleans.

39
00:01:29,20 --> 00:01:32,10
Now, of course, I just made this so I know what's in it,

40
00:01:32,10 --> 00:01:34,20
but there will be many, many times, as I've said before,

41
00:01:34,20 --> 00:01:36,10
in your programs where you won't necessarily know

42
00:01:36,10 --> 00:01:38,60
at any particular time during a program's execution

43
00:01:38,60 --> 00:01:41,00
what the contents of a given variable might be.

44
00:01:41,00 --> 00:01:44,40
Let's pretend I don't know what's in myArray.

45
00:01:44,40 --> 00:01:46,80
I have a variable called myItem here

46
00:01:46,80 --> 00:01:48,80
and here is my while loop.

47
00:01:48,80 --> 00:01:51,80
MyItem starts as null so my condition is

48
00:01:51,80 --> 00:01:55,40
that as long as myItem is not strictly equal to false,

49
00:01:55,40 --> 00:01:58,90
do some stuff here in the curly braces and that stuff is,

50
00:01:58,90 --> 00:02:01,40
first, I'm logging a message to the console,

51
00:02:01,40 --> 00:02:03,30
the state of myArray, how long it is,

52
00:02:03,30 --> 00:02:05,80
and then it will loop over and over again

53
00:02:05,80 --> 00:02:09,60
until we pop a false value off of this array.

54
00:02:09,60 --> 00:02:12,20
I'm checking the value of myItem each time

55
00:02:12,20 --> 00:02:14,00
and as long as it's not equal to false,

56
00:02:14,00 --> 00:02:16,30
I'm going to keep going.

57
00:02:16,30 --> 00:02:18,20
Just to walk through how this would work,

58
00:02:18,20 --> 00:02:21,30
the first time myItem is null, not false.

59
00:02:21,30 --> 00:02:24,80
So, I do what's in here and finish by popping off a value

60
00:02:24,80 --> 00:02:26,90
from myArray.

61
00:02:26,90 --> 00:02:29,00
The first time I do that, it's true.

62
00:02:29,00 --> 00:02:32,30
So, I go again through the loop, pop again,

63
00:02:32,30 --> 00:02:34,10
we get another true.

64
00:02:34,10 --> 00:02:38,40
We go again, pop one more time and this time I get false.

65
00:02:38,40 --> 00:02:42,70
When I check it here, this condition evaluates to false

66
00:02:42,70 --> 00:02:45,30
because myItem is false this time

67
00:02:45,30 --> 00:02:48,70
and so the statements inside the braces don't execute.

68
00:02:48,70 --> 00:02:50,40
If we did this with a for loop that's based

69
00:02:50,40 --> 00:02:54,00
on how long the array is, we'd still get that false value,

70
00:02:54,00 --> 00:02:56,50
but we'd go until we exhausted the whole thing

71
00:02:56,50 --> 00:02:58,90
unless we include a break statement.

72
00:02:58,90 --> 00:03:03,20
A while loop can express this idea a little more clearly.

73
00:03:03,20 --> 00:03:09,10
Let's copy this whole thing to the browser and try it.

74
00:03:09,10 --> 00:03:10,50
There we go.

75
00:03:10,50 --> 00:03:14,90
MyArray had six items, then we popped off true

76
00:03:14,90 --> 00:03:16,70
and at five, we popped off true again.

77
00:03:16,70 --> 00:03:19,60
There were four items left and then we stopped.

78
00:03:19,60 --> 00:03:21,20
This is really good in situations

79
00:03:21,20 --> 00:03:23,30
where you don't know the contents of whatever the thing is

80
00:03:23,30 --> 00:03:25,50
that you're going to loop over and you have a condition

81
00:03:25,50 --> 00:03:26,40
where you're sure you're going to be able

82
00:03:26,40 --> 00:03:30,20
to break at some point.

83
00:03:30,20 --> 00:03:32,80
Here's the danger with while loops, really with any loop,

84
00:03:32,80 --> 00:03:35,60
but I think it's a particular danger with while loops is

85
00:03:35,60 --> 00:03:38,30
that it's easy to create what's called an infinite loop.

86
00:03:38,30 --> 00:03:40,80
That is one that never breaks or stops.

87
00:03:40,80 --> 00:03:44,10
Here's a really contrived example of an infinite loop.

88
00:03:44,10 --> 00:03:46,40
I have a counter here set to one

89
00:03:46,40 --> 00:03:49,90
and then my condition here in my while loop is true.

90
00:03:49,90 --> 00:03:52,60
I have nothing else here, it's just true,

91
00:03:52,60 --> 00:03:55,40
which is the trickiest value there is.

92
00:03:55,40 --> 00:03:57,60
This will always be true and it will just keep going,

93
00:03:57,60 --> 00:03:59,00
and keep going, and keep going.

94
00:03:59,00 --> 00:04:00,80
Counting and counting and counting.

95
00:04:00,80 --> 00:04:03,50
Fortunately, I have a break statement in here.

96
00:04:03,50 --> 00:04:05,90
If I were to really execute this right now,

97
00:04:05,90 --> 00:04:08,10
it would only go through once and stop,

98
00:04:08,10 --> 00:04:12,60
but if I took out this break or commented it out

99
00:04:12,60 --> 00:04:14,50
and tried executing this in a browser,

100
00:04:14,50 --> 00:04:16,00
it would just go nuts.

101
00:04:16,00 --> 00:04:16,90
It would just print out numbers

102
00:04:16,90 --> 00:04:18,70
until there was nothing to print anymore.

103
00:04:18,70 --> 00:04:20,80
Your machine or at least your browser might run out

104
00:04:20,80 --> 00:04:22,60
of memory or at least your browser might say, hey,

105
00:04:22,60 --> 00:04:23,60
your script is running away.

106
00:04:23,60 --> 00:04:25,30
Would you like to abort it?

107
00:04:25,30 --> 00:04:26,50
Thankfully with modern computers,

108
00:04:26,50 --> 00:04:28,30
we don't have to worry so much about infinite loops

109
00:04:28,30 --> 00:04:29,70
taking down the entire system,

110
00:04:29,70 --> 00:04:31,40
but it's still something you want to watch out for

111
00:04:31,40 --> 00:04:35,20
because it's certainly a pain to debug these.

112
00:04:35,20 --> 00:04:37,30
Okay, there's one more formulation of the while loop

113
00:04:37,30 --> 00:04:41,50
that I'd like to show you which is called the do while loop.

114
00:04:41,50 --> 00:04:44,60
MyItem is false here and we're going to skip over this

115
00:04:44,60 --> 00:04:47,20
for a second and just look at the while part

116
00:04:47,20 --> 00:04:51,10
which says do stuff if myItem is not false.

117
00:04:51,10 --> 00:04:53,40
With a normal while loop under these conditions,

118
00:04:53,40 --> 00:04:56,70
nothing inside the curly braces would execute.

119
00:04:56,70 --> 00:04:58,80
It would just be skipped over.

120
00:04:58,80 --> 00:05:02,20
A do block before your while guarantees

121
00:05:02,20 --> 00:05:04,20
that what's in the curly braces will be executed

122
00:05:04,20 --> 00:05:06,30
at least once no matter what

123
00:05:06,30 --> 00:05:08,60
and then the condition is checked

124
00:05:08,60 --> 00:05:11,20
and the while loop takes over.

125
00:05:11,20 --> 00:05:13,20
If there's a situation where you're writing a while loop

126
00:05:13,20 --> 00:05:14,40
and you want to make sure that everything

127
00:05:14,40 --> 00:05:17,00
in the curly braces is executed at least once,

128
00:05:17,00 --> 00:05:19,30
you need to use a do while loop instead

129
00:05:19,30 --> 00:05:21,40
of a regular while loop.

130
00:05:21,40 --> 00:05:24,70
Now, you've seen the while loop and the do while loop,

131
00:05:24,70 --> 00:05:27,00
yet another way of iterating in JavaScript.

132
00:05:27,00 --> 00:05:28,90
Use it with caution because sometimes you can

133
00:05:28,90 --> 00:05:30,90
create an infinite loop without meaning to,

134
00:05:30,90 --> 00:05:32,20
but it's a great way to do some looping

135
00:05:32,20 --> 00:05:33,70
when you don't know how many times you're going

136
00:05:33,70 --> 00:05:36,00
to need to execute that loop.

