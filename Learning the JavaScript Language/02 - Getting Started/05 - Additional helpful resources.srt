1
00:00:00,50 --> 00:00:01,60
- [Instructor] In this video we're going to look

2
00:00:01,60 --> 00:00:05,10
at a few more helpful resources for learning JavaScript.

3
00:00:05,10 --> 00:00:06,40
As you're proceeding through this course

4
00:00:06,40 --> 00:00:07,80
you may find that there are somethings

5
00:00:07,80 --> 00:00:09,00
that aren't totally clear,

6
00:00:09,00 --> 00:00:11,50
and that you might want to read a little bit more about.

7
00:00:11,50 --> 00:00:14,40
And so, here are a few resources to help you do that.

8
00:00:14,40 --> 00:00:15,30
First a few books,

9
00:00:15,30 --> 00:00:18,10
many of which can be read online for free.

10
00:00:18,10 --> 00:00:20,40
Eloquent JavaScript by Marijn Haverbeke

11
00:00:20,40 --> 00:00:22,30
is available online for free.

12
00:00:22,30 --> 00:00:24,30
It's in its third edition as of this recording,

13
00:00:24,30 --> 00:00:25,60
and it's a modern classic.

14
00:00:25,60 --> 00:00:26,50
It's not short,

15
00:00:26,50 --> 00:00:28,50
but it is excellent.

16
00:00:28,50 --> 00:00:31,20
Dr. Axel Rauschmayer has a series of JavaScript books

17
00:00:31,20 --> 00:00:34,00
available on his website exploringjs.com

18
00:00:34,00 --> 00:00:35,50
for free reading in your browser.

19
00:00:35,50 --> 00:00:37,10
He goes into tremendous depth,

20
00:00:37,10 --> 00:00:38,70
so I think of these as reference material

21
00:00:38,70 --> 00:00:40,90
more than books that I would read straight through.

22
00:00:40,90 --> 00:00:43,00
I always look to Dr. Rauschmayer's work

23
00:00:43,00 --> 00:00:45,70
when I need to understand some small detail.

24
00:00:45,70 --> 00:00:47,80
He leaves no stone unturned.

25
00:00:47,80 --> 00:00:49,50
His books cover all current,

26
00:00:49,50 --> 00:00:51,40
and upcoming versions of JavaScript.

27
00:00:51,40 --> 00:00:53,30
So if you're wondering where JavaScript is going,

28
00:00:53,30 --> 00:00:54,90
look no further.

29
00:00:54,90 --> 00:00:58,30
Kyle Simpson has a series of books called You Don't Know JS.

30
00:00:58,30 --> 00:01:00,90
They're short, very readable, and excellent.

31
00:01:00,90 --> 00:01:01,80
Every book I've read

32
00:01:01,80 --> 00:01:03,80
has helped me reach a better understanding.

33
00:01:03,80 --> 00:01:05,90
You can read them for free on his GitHub site.

34
00:01:05,90 --> 00:01:09,60
He also has a series of excellent JavaScript video courses.

35
00:01:09,60 --> 00:01:10,60
Finally, for the books

36
00:01:10,60 --> 00:01:13,10
there's JavaScript: The Good Parts by Douglas Crockford.

37
00:01:13,10 --> 00:01:14,90
It's a classic, older than the others here,

38
00:01:14,90 --> 00:01:17,30
and while it's pretty technical, no words are wasted.

39
00:01:17,30 --> 00:01:19,80
And I thought it was a pretty enjoyable tech book to read.

40
00:01:19,80 --> 00:01:20,90
I found it to be very helpful

41
00:01:20,90 --> 00:01:22,50
when I had a little bit of experience,

42
00:01:22,50 --> 00:01:25,50
but still was very much figuring out the basics.

43
00:01:25,50 --> 00:01:27,20
JavaScript has some excentricities in knowing

44
00:01:27,20 --> 00:01:28,50
which things are good to use,

45
00:01:28,50 --> 00:01:31,10
and which are better to avoid is helpful.

46
00:01:31,10 --> 00:01:33,90
There's an unofficial companion site on GitHub

47
00:01:33,90 --> 00:01:36,10
called JavaScript the Good Parts notes,

48
00:01:36,10 --> 00:01:37,70
which has a constantly updated summary

49
00:01:37,70 --> 00:01:39,20
of what's in this book.

50
00:01:39,20 --> 00:01:41,60
I just found this when I was doing research for this course,

51
00:01:41,60 --> 00:01:43,50
and it looks great.

52
00:01:43,50 --> 00:01:45,90
Now, a few other online resources.

53
00:01:45,90 --> 00:01:47,40
I think the Mozilla Developer Network

54
00:01:47,40 --> 00:01:50,00
has the finest online reference material on JavaScript,

55
00:01:50,00 --> 00:01:53,60
and other web technologies, HTML, CSS, and so forth.

56
00:01:53,60 --> 00:01:55,30
There's good example code for almost everything

57
00:01:55,30 --> 00:01:57,30
you might want to know about.

58
00:01:57,30 --> 00:01:59,80
Caniuse.com is a compendium of information

59
00:01:59,80 --> 00:02:02,30
on browser support for various features of JavaScript,

60
00:02:02,30 --> 00:02:04,20
and other web technologies.

61
00:02:04,20 --> 00:02:06,00
When you're learning newer versions of JavaScript

62
00:02:06,00 --> 00:02:07,40
it's helpful to have a place to look

63
00:02:07,40 --> 00:02:08,80
for whether that thing that you're learning

64
00:02:08,80 --> 00:02:10,90
can be used in the wild.

65
00:02:10,90 --> 00:02:13,80
Similarly, quirksmode.org has been around

66
00:02:13,80 --> 00:02:15,70
for a very long time in internet years,

67
00:02:15,70 --> 00:02:19,40
and remains a great place to find out what works where.

68
00:02:19,40 --> 00:02:21,20
As you're working through this course, or afterward,

69
00:02:21,20 --> 00:02:22,60
I'd encourage you to bookmark

70
00:02:22,60 --> 00:02:25,00
and refer to any, or all of these.

