1
00:00:00,50 --> 00:00:01,40
- [Narrator] Programming languages

2
00:00:01,40 --> 00:00:04,00
go through revisions over time, just like apps do.

3
00:00:04,00 --> 00:00:06,20
Bugs are fixed, new features are added.

4
00:00:06,20 --> 00:00:07,80
In JavaScript, the feature additions

5
00:00:07,80 --> 00:00:09,30
are what people tend to get excited about,

6
00:00:09,30 --> 00:00:10,70
no surprise probably.

7
00:00:10,70 --> 00:00:12,70
Let's talk briefly about the versions of JavaScript

8
00:00:12,70 --> 00:00:14,30
that you'll hear about.

9
00:00:14,30 --> 00:00:17,20
Though JavaScript is the language's most commonly used name,

10
00:00:17,20 --> 00:00:20,40
the body that maintains its standards is ECMA International,

11
00:00:20,40 --> 00:00:22,50
so versions are named accordingly.

12
00:00:22,50 --> 00:00:25,60
ECMAScript 5, or ES5, is the version that runs

13
00:00:25,60 --> 00:00:29,60
on all modern web browsers, really since 2012 or so.

14
00:00:29,60 --> 00:00:30,80
There are older versions of course,

15
00:00:30,80 --> 00:00:32,70
but this one is the general baseline,

16
00:00:32,70 --> 00:00:34,10
and the one that most people mean

17
00:00:34,10 --> 00:00:36,30
when they think of mere JavaScript.

18
00:00:36,30 --> 00:00:37,70
Most of what we'll be talking about in this course

19
00:00:37,70 --> 00:00:40,20
will be ES5.

20
00:00:40,20 --> 00:00:41,80
The other most commonly discussed version

21
00:00:41,80 --> 00:00:45,30
is the one after ES5, which is sometimes called ES6,

22
00:00:45,30 --> 00:00:48,40
and officially is ECMAScript 2015.

23
00:00:48,40 --> 00:00:50,00
There are a lot of new features in this version

24
00:00:50,00 --> 00:00:51,60
and browser support is pretty good,

25
00:00:51,60 --> 00:00:53,10
but not good enough that web developers

26
00:00:53,10 --> 00:00:56,30
can use it directly in browsers safely.

27
00:00:56,30 --> 00:00:59,20
As you might guess, there are other newer versions as well,

28
00:00:59,20 --> 00:01:00,90
finalized pretty much every year,

29
00:01:00,90 --> 00:01:02,40
that continually add more features

30
00:01:02,40 --> 00:01:05,60
and make the language feel new and shiny all over again.

31
00:01:05,60 --> 00:01:06,40
But for the most part,

32
00:01:06,40 --> 00:01:09,20
ES5 and ES6 still make up the lion's share

33
00:01:09,20 --> 00:01:11,50
of JavaScript in common use.

34
00:01:11,50 --> 00:01:13,40
If you want to use the features of a newer version

35
00:01:13,40 --> 00:01:16,10
of JavaScript without sacrificing compatibility,

36
00:01:16,10 --> 00:01:18,40
there exist tools called transpilers,

37
00:01:18,40 --> 00:01:20,70
which can take your newer, less compatible JavaScript

38
00:01:20,70 --> 00:01:24,00
and convert it into ES5, so it can run on most web browsers

39
00:01:24,00 --> 00:01:25,70
or whatever else you're targeting.

40
00:01:25,70 --> 00:01:27,70
The most popular of these is called Babel,

41
00:01:27,70 --> 00:01:30,20
available at babeljs.io.

42
00:01:30,20 --> 00:01:32,00
Because we're just getting our feet wet in this course,

43
00:01:32,00 --> 00:01:33,70
we'll be sticking to ES5.

44
00:01:33,70 --> 00:01:35,80
All subsequent versions add on to it,

45
00:01:35,80 --> 00:01:38,50
so what you're learning is still useful and foundational,

46
00:01:38,50 --> 00:01:40,40
and you won't have to worry about compatibility

47
00:01:40,40 --> 00:01:43,00
or the need for extra tools to use it.

