1
00:00:00,50 --> 00:00:01,60
- [Instructor] In the next several chapters

2
00:00:01,60 --> 00:00:03,30
of this course, we're going to be going over

3
00:00:03,30 --> 00:00:05,40
the syntax of JavaScript.

4
00:00:05,40 --> 00:00:06,90
This is not going to be a complete coverage

5
00:00:06,90 --> 00:00:09,00
of every single element of JavaScript syntax,

6
00:00:09,00 --> 00:00:10,70
because the goal here is to allow you

7
00:00:10,70 --> 00:00:13,10
to get conversant in JavaScript.

8
00:00:13,10 --> 00:00:15,00
Just as with any other spoken language,

9
00:00:15,00 --> 00:00:17,90
you won't be totally fluent in a matter of a few hours,

10
00:00:17,90 --> 00:00:20,70
but you will be able to read code like this,

11
00:00:20,70 --> 00:00:23,10
which has some comments and some actual functionality,

12
00:00:23,10 --> 00:00:25,50
and you'll be able to make sense of it.

13
00:00:25,50 --> 00:00:28,00
You'll understand control structures,

14
00:00:28,00 --> 00:00:30,10
you'll understand the different kinds of data types

15
00:00:30,10 --> 00:00:31,50
that are available,

16
00:00:31,50 --> 00:00:34,20
and you'll know how to use variables.

17
00:00:34,20 --> 00:00:35,90
You'll understand comments in white space

18
00:00:35,90 --> 00:00:37,90
and generally have a pretty solid understanding

19
00:00:37,90 --> 00:00:39,50
of how the language is put together,

20
00:00:39,50 --> 00:00:42,40
enough that you can start working with it on your own.

21
00:00:42,40 --> 00:00:43,50
That's the goal.

22
00:00:43,50 --> 00:00:47,30
Not perfect fluency, but to become conversant.

23
00:00:47,30 --> 00:00:50,00
Programming languages are a lot like human spoken languages.

24
00:00:50,00 --> 00:00:52,50
You need to learn the grammar in order to be able to use it,

25
00:00:52,50 --> 00:00:55,30
but that's not enough to become totally fluent.

26
00:00:55,30 --> 00:00:57,90
You need to put in a lot of time reading the language,

27
00:00:57,90 --> 00:01:00,00
writing the language, generally using it

28
00:01:00,00 --> 00:01:01,70
in whatever way you want to.

29
00:01:01,70 --> 00:01:03,20
With human spoken languages, of course,

30
00:01:03,20 --> 00:01:04,60
you could simply speak it with your voice

31
00:01:04,60 --> 00:01:07,00
and get a lot of use from it, but with JavaScript,

32
00:01:07,00 --> 00:01:09,20
you need to be able to read and write it.

33
00:01:09,20 --> 00:01:10,50
And after going through this course,

34
00:01:10,50 --> 00:01:12,50
you should be able to read and write code,

35
00:01:12,50 --> 00:01:15,00
and begin to do what you need to do using JavaScript.

36
00:01:15,00 --> 00:01:17,00
So, let's get started.

