1
00:00:00,50 --> 00:00:02,30
- [Instructor] When you're beginning to work with JavaScript

2
00:00:02,30 --> 00:00:05,00
it's important to start trying it out as soon as possible.

3
00:00:05,00 --> 00:00:07,70
Fortunately, there are lots of easy options for doing this.

4
00:00:07,70 --> 00:00:08,70
In this video, we're going to look

5
00:00:08,70 --> 00:00:10,50
at a couple of the easiest ways.

6
00:00:10,50 --> 00:00:12,50
JavaScript consoles and web browsers

7
00:00:12,50 --> 00:00:14,90
and the node.js console.

8
00:00:14,90 --> 00:00:16,10
We'll start with web browsers since

9
00:00:16,10 --> 00:00:17,80
everyone has a web browser they use,

10
00:00:17,80 --> 00:00:19,40
and as far as I know, every single browser

11
00:00:19,40 --> 00:00:21,30
has a JavaScript console.

12
00:00:21,30 --> 00:00:23,80
Let's look at the console in Chrome first.

13
00:00:23,80 --> 00:00:27,10
In the Chrome menu, I can open up more tools,

14
00:00:27,10 --> 00:00:28,60
and open the developer tools.

15
00:00:28,60 --> 00:00:29,80
On a Mac, you can also open it with

16
00:00:29,80 --> 00:00:34,60
command option I, on Windows it's control alt I.

17
00:00:34,60 --> 00:00:37,40
And here's the console tab, which has a JavaScript console.

18
00:00:37,40 --> 00:00:41,30
So I can type two plus two,

19
00:00:41,30 --> 00:00:44,30
hit return and it executes giving me back four.

20
00:00:44,30 --> 00:00:45,80
That's JavaScript.

21
00:00:45,80 --> 00:00:47,10
Couple of notes.

22
00:00:47,10 --> 00:00:49,00
The console only shows one executable

23
00:00:49,00 --> 00:00:50,70
line at a time normally.

24
00:00:50,70 --> 00:00:53,10
But you can hit shift return to get

25
00:00:53,10 --> 00:00:55,60
more lines if you need them.

26
00:00:55,60 --> 00:00:57,60
Wherever your cursor is, if you just hit return,

27
00:00:57,60 --> 00:01:00,60
whatever you've typed should execute.

28
00:01:00,60 --> 00:01:02,20
The way you activate the console varies

29
00:01:02,20 --> 00:01:03,40
from browser to browser.

30
00:01:03,40 --> 00:01:05,40
So let's take a quick tour of the major ones

31
00:01:05,40 --> 00:01:08,60
to see how to get a JavaScript console open.

32
00:01:08,60 --> 00:01:10,40
We've already seen Chrome, so let's take a look at

33
00:01:10,40 --> 00:01:13,60
Firefox, Safari, and Microsoft Edge.

34
00:01:13,60 --> 00:01:15,40
Over here in Firefox, I can use the same

35
00:01:15,40 --> 00:01:19,40
key combination as in Chrome, command option I.

36
00:01:19,40 --> 00:01:21,50
That will open the developer tools for me,

37
00:01:21,50 --> 00:01:23,40
and I have a similar set of tools,

38
00:01:23,40 --> 00:01:25,30
including a JavaScript console.

39
00:01:25,30 --> 00:01:31,00
Type two plus two here, hit return and it works.

40
00:01:31,00 --> 00:01:34,20
You can also find this up in the Firefox menu

41
00:01:34,20 --> 00:01:37,20
under web developer and toggle tools.

42
00:01:37,20 --> 00:01:39,30
We'll look at Safari next.

43
00:01:39,30 --> 00:01:40,30
Safari's a little different,

44
00:01:40,30 --> 00:01:43,40
it requires a little bit of configuration first,

45
00:01:43,40 --> 00:01:44,70
If you haven't done this before.

46
00:01:44,70 --> 00:01:47,50
So you can open the Safari preferences,

47
00:01:47,50 --> 00:01:49,80
go to the advanced tab, and check show

48
00:01:49,80 --> 00:01:51,60
develop menu in the menu bar.

49
00:01:51,60 --> 00:01:53,20
Once you've done that, the key combination

50
00:01:53,20 --> 00:01:57,10
that we've used already will work, command option I,

51
00:01:57,10 --> 00:01:59,20
which opens or closes the web inspector.

52
00:01:59,20 --> 00:02:00,70
There's also a separate command

53
00:02:00,70 --> 00:02:03,70
that will take you straight to the JavaScript console.

54
00:02:03,70 --> 00:02:06,90
Once I'm here, I can type two plus two again,

55
00:02:06,90 --> 00:02:08,50
and I get back four.

56
00:02:08,50 --> 00:02:12,30
Finally, let's look at Microsoft Edge.

57
00:02:12,30 --> 00:02:13,80
I have that running over here

58
00:02:13,80 --> 00:02:16,00
on browserstack.com, which lets me run

59
00:02:16,00 --> 00:02:18,60
Windows browsers from my Mac.

60
00:02:18,60 --> 00:02:20,20
Microsoft's developer tools are called

61
00:02:20,20 --> 00:02:21,00
the F12 developer tools.

62
00:02:21,00 --> 00:02:23,80
So named because you can open them

63
00:02:23,80 --> 00:02:26,30
using F12 on your keyboard.

64
00:02:26,30 --> 00:02:28,00
When you do that, everything looks very similar.

65
00:02:28,00 --> 00:02:29,80
Here's the JavaScript console tab,

66
00:02:29,80 --> 00:02:32,60
I can type two plus two,

67
00:02:32,60 --> 00:02:34,30
get back four.

68
00:02:34,30 --> 00:02:35,90
This is over on the side right now.

69
00:02:35,90 --> 00:02:38,00
Most of these tools let you change their location

70
00:02:38,00 --> 00:02:40,10
using similar controls to this.

71
00:02:40,10 --> 00:02:41,30
I'm going to keep this over on the side

72
00:02:41,30 --> 00:02:42,50
so it's out of the way of the

73
00:02:42,50 --> 00:02:45,70
BrowserStack controls over here.

74
00:02:45,70 --> 00:02:48,60
And you can hit F12 again, it appears to shrink it,

75
00:02:48,60 --> 00:02:51,10
but then also makes it go away.

76
00:02:51,10 --> 00:02:52,60
If you're interested in using JavaScript

77
00:02:52,60 --> 00:02:54,20
for back end development, you'll probably

78
00:02:54,20 --> 00:02:56,10
want to look at Node.js, which is installed

79
00:02:56,10 --> 00:02:58,00
as a command line utility called node.

80
00:02:58,00 --> 00:03:01,00
You can get it from nodejs.org.

81
00:03:01,00 --> 00:03:02,60
I have it installed here on my Mac,

82
00:03:02,60 --> 00:03:04,60
and so in the terminal app,

83
00:03:04,60 --> 00:03:08,20
I can run it with no options, just type node.

84
00:03:08,20 --> 00:03:10,50
And it immediately drops me into a repl,

85
00:03:10,50 --> 00:03:12,00
which is like a JavaScript console.

86
00:03:12,00 --> 00:03:15,10
It's called a read evaluate print loop,

87
00:03:15,10 --> 00:03:18,00
where you can play with it just like you would in a browser.

88
00:03:18,00 --> 00:03:20,10
Let's try typing two plus two.

89
00:03:20,10 --> 00:03:22,10
We get back four.

90
00:03:22,10 --> 00:03:23,60
It works just the same for our purposes,

91
00:03:23,60 --> 00:03:25,20
and if this is the direction you plan to take

92
00:03:25,20 --> 00:03:26,50
in your journey with language,

93
00:03:26,50 --> 00:03:28,60
you might as well install it and start playing with it here

94
00:03:28,60 --> 00:03:30,10
instead of in a browser.

95
00:03:30,10 --> 00:03:32,40
One nice thing about node, is that it detects when something

96
00:03:32,40 --> 00:03:34,90
you're typing could extend to multiple lines,

97
00:03:34,90 --> 00:03:36,70
the way we normally would write it.

98
00:03:36,70 --> 00:03:38,80
So if I start defining a function,

99
00:03:38,80 --> 00:03:41,50
let's call this function myFunction.

100
00:03:41,50 --> 00:03:42,70
I know you don't know what this means yet,

101
00:03:42,70 --> 00:03:44,10
but just bear with me.

102
00:03:44,10 --> 00:03:46,30
Normally at this point I would hit return,

103
00:03:46,30 --> 00:03:48,50
and if I were typing this in a browser console,

104
00:03:48,50 --> 00:03:50,10
I'd probably get a syntax error

105
00:03:50,10 --> 00:03:52,30
because it's on one line and I need more.

106
00:03:52,30 --> 00:03:54,70
But node puts in this ellipse to indicate

107
00:03:54,70 --> 00:03:58,30
that it knows I want to type more stuff here.

108
00:03:58,30 --> 00:03:59,40
So node knows what I'm doing,

109
00:03:59,40 --> 00:04:00,60
it gives me the chance to write more

110
00:04:00,60 --> 00:04:02,20
before it executes what I've typed.

111
00:04:02,20 --> 00:04:03,60
It's pretty neat.

112
00:04:03,60 --> 00:04:08,00
And I can hit control D to get out of this.

113
00:04:08,00 --> 00:04:09,50
Now you've seen some of the possibilities

114
00:04:09,50 --> 00:04:11,20
for where to play around with JavaScript.

115
00:04:11,20 --> 00:04:12,70
I'm expecting most of you will probably use

116
00:04:12,70 --> 00:04:16,00
web browsers at first, but node is available to you as well.

