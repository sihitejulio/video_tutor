1
00:00:00,60 --> 00:00:01,60
- [Instructor] In this video, I'm going

2
00:00:01,60 --> 00:00:03,70
to extol the virtues of a good text editor.

3
00:00:03,70 --> 00:00:05,40
Give you some reasons why you might actually want

4
00:00:05,40 --> 00:00:07,00
to have one in your tool kit, whatever

5
00:00:07,00 --> 00:00:08,90
your level of experience.

6
00:00:08,90 --> 00:00:10,60
If you're a web designer coming to JavaScript,

7
00:00:10,60 --> 00:00:13,80
you might have an editor you like for HTML and CSS,

8
00:00:13,80 --> 00:00:15,10
and that editor will probably be great

9
00:00:15,10 --> 00:00:16,70
for JavaScript as well.

10
00:00:16,70 --> 00:00:18,10
I've been teaching new developers for years,

11
00:00:18,10 --> 00:00:19,70
and I'm continuously impressed with

12
00:00:19,70 --> 00:00:23,30
how many high-quality, free editors, are available now.

13
00:00:23,30 --> 00:00:25,40
We'll mostly talk about the features that are noticeable

14
00:00:25,40 --> 00:00:27,10
when you're working on very small projects,

15
00:00:27,10 --> 00:00:29,60
with a small number of files, and not a lot of code.

16
00:00:29,60 --> 00:00:30,40
Editors can of course work

17
00:00:30,40 --> 00:00:32,70
with projects including thousands of files,

18
00:00:32,70 --> 00:00:33,70
but they're great for the small things

19
00:00:33,70 --> 00:00:36,10
you'll be working on when you start out as well.

20
00:00:36,10 --> 00:00:37,50
The first thing everyone notices

21
00:00:37,50 --> 00:00:40,30
when using a code editor, is syntax highlighting.

22
00:00:40,30 --> 00:00:42,40
The editor uses colors and styles

23
00:00:42,40 --> 00:00:44,10
to mark the different kinds of code

24
00:00:44,10 --> 00:00:46,20
to help us know what we're looking at at a glance.

25
00:00:46,20 --> 00:00:47,70
This makes the difference between

26
00:00:47,70 --> 00:00:51,00
something like this, which has syntax highlighting,

27
00:00:51,00 --> 00:00:53,70
and something like this, which doesn't.

28
00:00:53,70 --> 00:00:55,70
This editor has no awareness of code whatsoever,

29
00:00:55,70 --> 00:00:58,60
so there are little spell checking squiggles

30
00:00:58,60 --> 00:01:00,50
all over the place, and there's no color.

31
00:01:00,50 --> 00:01:05,80
It's just much harder to read and understand than this.

32
00:01:05,80 --> 00:01:08,80
Code completion is the next big one.

33
00:01:08,80 --> 00:01:10,00
Your editor saves you typing by giving

34
00:01:10,00 --> 00:01:12,10
you options for how to finish what you're typing based

35
00:01:12,10 --> 00:01:13,70
on what it knows about JavaScript,

36
00:01:13,70 --> 00:01:15,90
and about the code in the files you're working on.

37
00:01:15,90 --> 00:01:18,90
So if I start to write the word function,

38
00:01:18,90 --> 00:01:20,40
this editor completes it for me.

39
00:01:20,40 --> 00:01:25,20
I can type myFunction,

40
00:01:25,20 --> 00:01:26,60
and then inside this function

41
00:01:26,60 --> 00:01:31,00
I might start referring to a JavaScript array.

42
00:01:31,00 --> 00:01:32,20
Then I type a dot, and it gives me

43
00:01:32,20 --> 00:01:34,70
all kinds of other completions.

44
00:01:34,70 --> 00:01:36,60
Most editors these days have some awareness

45
00:01:36,60 --> 00:01:37,90
of complete projects, and can draw

46
00:01:37,90 --> 00:01:40,20
on that knowledge to save you work.

47
00:01:40,20 --> 00:01:42,20
Editors can also help with the tedious parts

48
00:01:42,20 --> 00:01:45,60
of writing code, like managing special punctuation.

49
00:01:45,60 --> 00:01:48,10
JavaScript uses lots of parenthesis, quotation marks,

50
00:01:48,10 --> 00:01:50,70
braces, and other punctuation that often come in pairs.

51
00:01:50,70 --> 00:01:53,20
Pretty much all of them will auto-type these for you.

52
00:01:53,20 --> 00:01:55,10
So if I type just a left curly brace,

53
00:01:55,10 --> 00:01:57,30
the right one is inserted for me automatically.

54
00:01:57,30 --> 00:01:59,80
And if I delete it, both of them go away as well.

55
00:01:59,80 --> 00:02:02,80
This works for parenthesis, and quote marks,

56
00:02:02,80 --> 00:02:05,20
and all sorts of things.

57
00:02:05,20 --> 00:02:06,50
There are also other features to help

58
00:02:06,50 --> 00:02:08,20
you avoid common errors.

59
00:02:08,20 --> 00:02:09,80
Reformat code so it's more readable,

60
00:02:09,80 --> 00:02:11,90
but still executes correctly, and more.

61
00:02:11,90 --> 00:02:13,20
Eventually you might even step up

62
00:02:13,20 --> 00:02:16,00
to an integrated development environment, or IDE,

63
00:02:16,00 --> 00:02:18,00
which has all the features of a great editor,

64
00:02:18,00 --> 00:02:20,00
but also adds features to help professional,

65
00:02:20,00 --> 00:02:21,70
and enthusiastic amateur programmers get

66
00:02:21,70 --> 00:02:24,20
their work done faster.

67
00:02:24,20 --> 00:02:25,60
I said there are lots of great options,

68
00:02:25,60 --> 00:02:27,80
let's look at a few, all of which are available

69
00:02:27,80 --> 00:02:30,60
for macOS, Widows, and Linux.

70
00:02:30,60 --> 00:02:32,60
Visual Studio Code is from Microsoft,

71
00:02:32,60 --> 00:02:33,70
it's open source and free, and

72
00:02:33,70 --> 00:02:35,50
particularly JavaScript friendly.

73
00:02:35,50 --> 00:02:37,70
It's updated every month, with lots of useful features,

74
00:02:37,70 --> 00:02:39,20
and is very popular right now.

75
00:02:39,20 --> 00:02:42,90
It's the editor you'll see the most in this course.

76
00:02:42,90 --> 00:02:45,20
Sublime Text is another very popular editor.

77
00:02:45,20 --> 00:02:46,80
It's not free, but it can be used

78
00:02:46,80 --> 00:02:48,80
without payment for a long time.

79
00:02:48,80 --> 00:02:50,60
Before VS Code came along, this is the editor

80
00:02:50,60 --> 00:02:52,00
people talked about the most.

81
00:02:52,00 --> 00:02:53,80
Now, maybe it's a tie.

82
00:02:53,80 --> 00:02:55,20
Unlike most other current options,

83
00:02:55,20 --> 00:02:57,20
which are kind of like high-powered web apps running

84
00:02:57,20 --> 00:02:59,60
on your machine, Sublime is native, which makes

85
00:02:59,60 --> 00:03:01,80
it feel faster for some.

86
00:03:01,80 --> 00:03:04,30
Atom is from GitHub, free and open source,

87
00:03:04,30 --> 00:03:06,00
and another one a lot of people love.

88
00:03:06,00 --> 00:03:06,90
They're currently promoting

89
00:03:06,90 --> 00:03:08,10
their remote collaboration tools,

90
00:03:08,10 --> 00:03:11,60
that let people work on the same file, at the same time.

91
00:03:11,60 --> 00:03:14,90
Brackets is Adobe's free and open source offering.

92
00:03:14,90 --> 00:03:16,30
It's targeted at web designers with support

93
00:03:16,30 --> 00:03:19,30
for CS preprocessors out of the box, among other things,

94
00:03:19,30 --> 00:03:21,30
and of course, it's great for JavaScript.

95
00:03:21,30 --> 00:03:24,20
If you're wondering what I use in my own daily work,

96
00:03:24,20 --> 00:03:26,50
I mostly work in PhpStorm, an IDE

97
00:03:26,50 --> 00:03:28,90
that supports JavaScript and PHP equally well,

98
00:03:28,90 --> 00:03:30,40
which is important for me.

99
00:03:30,40 --> 00:03:31,70
It's a little heavier to work with

100
00:03:31,70 --> 00:03:33,80
than these other editors, it takes a very little bit

101
00:03:33,80 --> 00:03:36,10
of configuration for any project I add to it,

102
00:03:36,10 --> 00:03:38,20
but once it's set up, I get so much useful help,

103
00:03:38,20 --> 00:03:39,40
that I miss it every time I use

104
00:03:39,40 --> 00:03:41,70
another editor for serious work.

105
00:03:41,70 --> 00:03:43,90
As you grow in your skills as a JavaScript programmer,

106
00:03:43,90 --> 00:03:45,70
you might consider looking at WebStorm,

107
00:03:45,70 --> 00:03:47,80
the JavaScript only IDE from the same company,

108
00:03:47,80 --> 00:03:50,40
Jet Brains, and see if you like it.

109
00:03:50,40 --> 00:03:51,70
My recommendation for now is that

110
00:03:51,70 --> 00:03:54,10
you pick one of these editors, download it,

111
00:03:54,10 --> 00:03:56,00
and stick with it for at least a couple of weeks.

112
00:03:56,00 --> 00:03:57,30
Learning it's keyboard shortcuts,

113
00:03:57,30 --> 00:03:59,00
and maybe even reading some documentations

114
00:03:59,00 --> 00:04:01,10
so you can really get the most out of it.

115
00:04:01,10 --> 00:04:03,00
You'll probably start to recognize the things you like,

116
00:04:03,00 --> 00:04:04,90
and what you wish you could change.

117
00:04:04,90 --> 00:04:06,20
If the editor doesn't work in a way

118
00:04:06,20 --> 00:04:09,00
that feels natural to you, try another one.

