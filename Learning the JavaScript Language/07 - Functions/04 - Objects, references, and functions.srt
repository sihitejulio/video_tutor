1
00:00:00,50 --> 00:00:01,60
- [Instructor] In this video we're going to talk

2
00:00:01,60 --> 00:00:03,40
about objects and functions.

3
00:00:03,40 --> 00:00:06,10
How objects are passed by reference into functions

4
00:00:06,10 --> 00:00:08,10
and what that means.

5
00:00:08,10 --> 00:00:09,80
So to start off, let's look at this object.

6
00:00:09,80 --> 00:00:13,20
It's called Calvin, and it has this form.

7
00:00:13,20 --> 00:00:16,10
Its name is Calvin, its best friend is Hobbes,

8
00:00:16,10 --> 00:00:18,70
and its form is human boy.

9
00:00:18,70 --> 00:00:22,20
Now along with this object I have a function I've written,

10
00:00:22,20 --> 00:00:23,90
which is the transmogrifier.

11
00:00:23,90 --> 00:00:25,70
And what this does is it takes a Calvin

12
00:00:25,70 --> 00:00:30,10
and transforms its form into something else.

13
00:00:30,10 --> 00:00:38,70
So let's try running this.

14
00:00:38,70 --> 00:00:40,50
So I can see now that the form has been changed

15
00:00:40,50 --> 00:00:43,20
into tyrannosaurus from human boy.

16
00:00:43,20 --> 00:00:45,90
Let's look at the definition of this function.

17
00:00:45,90 --> 00:00:48,20
And here it is.

18
00:00:48,20 --> 00:00:50,70
It accepts a Calvin object, and then we do

19
00:00:50,70 --> 00:00:52,50
a little bit of type checking

20
00:00:52,50 --> 00:00:54,00
to make sure that the argument is at least

21
00:00:54,00 --> 00:00:56,10
something like we expect it to be.

22
00:00:56,10 --> 00:00:57,60
When you're writing functions, it's a good idea

23
00:00:57,60 --> 00:00:59,30
to make sure that the input that you're accepting

24
00:00:59,30 --> 00:01:02,80
into the function is of a structure that you expect.

25
00:01:02,80 --> 00:01:04,30
Otherwise your code could later break down

26
00:01:04,30 --> 00:01:06,20
and you'd have a lot of debugging to do.

27
00:01:06,20 --> 00:01:08,10
This is especially important if you're writing a function

28
00:01:08,10 --> 00:01:09,90
that could accept input directly from a user

29
00:01:09,90 --> 00:01:11,70
not from you, the programmer.

30
00:01:11,70 --> 00:01:14,10
But in any case, for now we're doing fairly simple

31
00:01:14,10 --> 00:01:16,90
type checking just to make sure it's a object.

32
00:01:16,90 --> 00:01:18,50
If it isn't, we're just bailing out

33
00:01:18,50 --> 00:01:20,70
with an error and returning.

34
00:01:20,70 --> 00:01:23,20
After that, we're generating a random number

35
00:01:23,20 --> 00:01:25,30
between one and five.

36
00:01:25,30 --> 00:01:27,10
So what we're doing to generate this random number

37
00:01:27,10 --> 00:01:29,10
is we're using the math.random method

38
00:01:29,10 --> 00:01:31,90
which will give you a number between zero and one.

39
00:01:31,90 --> 00:01:34,00
We're multiplying that times the number

40
00:01:34,00 --> 00:01:38,30
of our upper bound and taking the math.floor of that

41
00:01:38,30 --> 00:01:41,60
which will give you the largest integer

42
00:01:41,60 --> 00:01:43,10
that's smaller than whatever you have.

43
00:01:43,10 --> 00:01:46,60
So it basically chops off the decimal points.

44
00:01:46,60 --> 00:01:48,90
So this will give me a number between zero and four

45
00:01:48,90 --> 00:01:50,50
and so I'm adding one to it to make sure

46
00:01:50,50 --> 00:01:53,90
that my number is between one and five.

47
00:01:53,90 --> 00:01:56,00
And then for any of these five numbers,

48
00:01:56,00 --> 00:01:58,10
I'm using a switch statement here

49
00:01:58,10 --> 00:02:00,70
to get a new form for the Calvin to take

50
00:02:00,70 --> 00:02:03,20
and assigning it directly to that argument Calvin

51
00:02:03,20 --> 00:02:04,90
that was passed in.

52
00:02:04,90 --> 00:02:07,20
Now something that you should note about this function

53
00:02:07,20 --> 00:02:09,40
if we look down here to the end

54
00:02:09,40 --> 00:02:11,70
is there's no return statement at the bottom.

55
00:02:11,70 --> 00:02:13,10
Why is that?

56
00:02:13,10 --> 00:02:15,20
The reason is that the argument being passed in

57
00:02:15,20 --> 00:02:17,20
is an object and objects are passed

58
00:02:17,20 --> 00:02:20,00
by reference into functions.

59
00:02:20,00 --> 00:02:22,30
That means that the object I've created,

60
00:02:22,30 --> 00:02:24,60
this Calvin in this case, has a location

61
00:02:24,60 --> 00:02:25,90
in the computer's memory.

62
00:02:25,90 --> 00:02:29,10
And when I pass it in to a transmogrify function like this

63
00:02:29,10 --> 00:02:31,40
I'm passing in not a copy of the object

64
00:02:31,40 --> 00:02:33,30
but the original object.

65
00:02:33,30 --> 00:02:35,00
In a sense I'm passing in its location

66
00:02:35,00 --> 00:02:36,70
in memory specifically.

67
00:02:36,70 --> 00:02:38,80
And then my function right here,

68
00:02:38,80 --> 00:02:40,90
these lines that change the form,

69
00:02:40,90 --> 00:02:44,50
are actually operating on the original object.

70
00:02:44,50 --> 00:02:46,90
So when I executed my transmogrifier function

71
00:02:46,90 --> 00:02:50,30
and passed Calvin into it, he started out as a human boy

72
00:02:50,30 --> 00:02:53,60
and then he came out as a tyrannosaurus.

73
00:02:53,60 --> 00:02:55,90
So this whole discussion is important because

74
00:02:55,90 --> 00:02:57,60
there may be times where you don't want

75
00:02:57,60 --> 00:03:00,80
your original object to be modified the way this one was.

76
00:03:00,80 --> 00:03:03,00
You might want a copy to come back out.

77
00:03:03,00 --> 00:03:05,30
So for example, you might instead want

78
00:03:05,30 --> 00:03:07,50
a copy to come back out.

79
00:03:07,50 --> 00:03:09,00
So you might want to have a version of this

80
00:03:09,00 --> 00:03:11,80
where instead of passing a Calvin in like this

81
00:03:11,80 --> 00:03:13,90
and getting him back directly,

82
00:03:13,90 --> 00:03:15,30
you might want set up a new Calvin

83
00:03:15,30 --> 00:03:17,20
that gets returned from the transmogrifier,

84
00:03:17,20 --> 00:03:19,30
leaving the original.

85
00:03:19,30 --> 00:03:20,60
I've defined a version of the function

86
00:03:20,60 --> 00:03:24,10
that does that as well.

87
00:03:24,10 --> 00:03:28,20
So I'm going to create a new Calvin.

88
00:03:28,20 --> 00:03:32,10
It starts out as human boy.

89
00:03:32,10 --> 00:03:35,30
And then I have another function called transmogrify copy.

90
00:03:35,30 --> 00:03:38,20
Now I'll pass in new Calvin

91
00:03:38,20 --> 00:03:40,60
and I'll assign the result to a new variable

92
00:03:40,60 --> 00:03:45,00
called Calvin copy.

93
00:03:45,00 --> 00:03:47,40
Like this.

94
00:03:47,40 --> 00:03:49,50
Now let's check new Calvin.

95
00:03:49,50 --> 00:03:52,00
New Calvin is still a human boy.

96
00:03:52,00 --> 00:03:57,50
But Calvin copy has turned into a canary.

97
00:03:57,50 --> 00:03:59,90
So let's take a look at how this works.

98
00:03:59,90 --> 00:04:02,10
Now you can see a lot of the code

99
00:04:02,10 --> 00:04:04,30
for these two functions is quite similar.

100
00:04:04,30 --> 00:04:07,80
In fact, a lot of it is exactly the same.

101
00:04:07,80 --> 00:04:09,10
We're still doing the argument checking

102
00:04:09,10 --> 00:04:10,90
we're still generating a random number.

103
00:04:10,90 --> 00:04:12,80
But here in transmogrify copy

104
00:04:12,80 --> 00:04:17,50
I'm saving the form as a variable called New Form.

105
00:04:17,50 --> 00:04:20,60
And then in my case blocks, instead of modifying

106
00:04:20,60 --> 00:04:22,00
the original object directly

107
00:04:22,00 --> 00:04:25,50
I'm just modifying this new form variable.

108
00:04:25,50 --> 00:04:27,90
Once we get to the end of the function

109
00:04:27,90 --> 00:04:29,50
instead of not returning anything

110
00:04:29,50 --> 00:04:32,80
I'm returning a new object that has all the same fields

111
00:04:32,80 --> 00:04:37,20
copied in, except for form, which becomes the new form.

112
00:04:37,20 --> 00:04:39,00
So now I get a totally fresh object

113
00:04:39,00 --> 00:04:41,00
and I don't modify the original one.

114
00:04:41,00 --> 00:04:44,00
I'm not changing the reference.

115
00:04:44,00 --> 00:04:45,70
Now note that this pass by reference business

116
00:04:45,70 --> 00:04:48,40
is not just for objects that are clearly objects.

117
00:04:48,40 --> 00:04:50,60
This principle also applies to arrays

118
00:04:50,60 --> 00:04:52,90
which you may remember, are really objects.

119
00:04:52,90 --> 00:04:54,90
So if you pass an array into a function

120
00:04:54,90 --> 00:04:57,70
and you modify that array, you're modifying the original.

121
00:04:57,70 --> 00:04:59,10
Always keep that in mind.

122
00:04:59,10 --> 00:05:02,00
That's what passing by reference means.

123
00:05:02,00 --> 00:05:04,20
So to sum up, when you're working with functions

124
00:05:04,20 --> 00:05:05,90
and objects just be careful about whether you're

125
00:05:05,90 --> 00:05:08,20
passing in and modifying the original

126
00:05:08,20 --> 00:05:11,00
or whether you want to get a copy back instead

127
00:05:11,00 --> 00:05:12,40
and write your functions accordingly

128
00:05:12,40 --> 00:05:15,00
to make sure that you're getting back exactly what you want.

