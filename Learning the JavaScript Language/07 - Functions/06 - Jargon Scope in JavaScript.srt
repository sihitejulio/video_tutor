1
00:00:00,50 --> 00:00:02,10
- [Instructor] It's time for a brief digression to deal

2
00:00:02,10 --> 00:00:04,20
with a little bit of jargon you might hear about.

3
00:00:04,20 --> 00:00:06,40
JavaScript is super popular and draws a lot of attention

4
00:00:06,40 --> 00:00:08,90
from developers of many skill and experience levels,

5
00:00:08,90 --> 00:00:11,00
so knowing where some of this jargon comes from can help you

6
00:00:11,00 --> 00:00:13,80
at possibly boring parties.

7
00:00:13,80 --> 00:00:17,00
Here we're going to talk about scope.

8
00:00:17,00 --> 00:00:20,00
Scope refers to where variables are defined and what parts

9
00:00:20,00 --> 00:00:23,80
of our code can access the variables we define.

10
00:00:23,80 --> 00:00:25,10
Often when we talk about scope,

11
00:00:25,10 --> 00:00:28,80
we differentiate between global scope and local scope.

12
00:00:28,80 --> 00:00:31,40
Global refers to variables that are defined in such a way

13
00:00:31,40 --> 00:00:33,30
that they're accessible anywhere.

14
00:00:33,30 --> 00:00:35,20
Local means that a variable is accessible

15
00:00:35,20 --> 00:00:36,60
in a way that's more limited,

16
00:00:36,60 --> 00:00:39,90
perhaps only inside a particular function.

17
00:00:39,90 --> 00:00:42,50
One piece of guidance almost every beginning programmer gets

18
00:00:42,50 --> 00:00:45,10
is to avoid creating global variables.

19
00:00:45,10 --> 00:00:46,90
Global variables are accessible everywhere,

20
00:00:46,90 --> 00:00:48,30
which is very convenient,

21
00:00:48,30 --> 00:00:50,00
but the global namespace, as it's called,

22
00:00:50,00 --> 00:00:52,30
is a resource that's shared among all scripts

23
00:00:52,30 --> 00:00:54,00
that are in use at any given time,

24
00:00:54,00 --> 00:00:56,20
and there may be a lot of them.

25
00:00:56,20 --> 00:00:57,80
Global variables can be accessed

26
00:00:57,80 --> 00:01:00,60
and also modified by any piece of code,

27
00:01:00,60 --> 00:01:02,20
so your little island that you're working on

28
00:01:02,20 --> 00:01:04,30
could have bugs introduced by somebody else's code

29
00:01:04,30 --> 00:01:06,30
if you're both trying to use a global variable

30
00:01:06,30 --> 00:01:08,60
with the same name.

31
00:01:08,60 --> 00:01:10,90
Unfortunately, in JavaScript it's kind of easy

32
00:01:10,90 --> 00:01:12,50
to create global variables.

33
00:01:12,50 --> 00:01:14,40
If I just create a variable like this

34
00:01:14,40 --> 00:01:15,70
with no var keyword in front of it,

35
00:01:15,70 --> 00:01:18,80
it will always be a global variable,

36
00:01:18,80 --> 00:01:21,00
unless I've enabled strict mode by putting this string

37
00:01:21,00 --> 00:01:23,40
at the top of my file, in which case I'll get a warning.

38
00:01:23,40 --> 00:01:27,30
But the point is it's easy to do this accidentally.

39
00:01:27,30 --> 00:01:29,00
To avoid creating global variables,

40
00:01:29,00 --> 00:01:30,90
we can use the keyword var.

41
00:01:30,90 --> 00:01:32,70
This can keep the variable in local scope,

42
00:01:32,70 --> 00:01:35,30
about which we'll talk more in the next video.

43
00:01:35,30 --> 00:01:38,60
In ECMAScript 2015 and above, there are other keywords

44
00:01:38,60 --> 00:01:41,30
that can create locally scoped variables as well,

45
00:01:41,30 --> 00:01:43,00
let and const.

46
00:01:43,00 --> 00:01:44,60
You'll see these in modern JavaScript code.

47
00:01:44,60 --> 00:01:45,60
And the way they handle scope

48
00:01:45,60 --> 00:01:47,00
is a little different from var,

49
00:01:47,00 --> 00:01:48,60
but we'll leave that aside for now.

50
00:01:48,60 --> 00:01:52,20
They're worth recognizing, but using var is just fine.

51
00:01:52,20 --> 00:01:53,70
We'll get into more detail about scope

52
00:01:53,70 --> 00:01:55,00
with some code examples next.

