1
00:00:00,60 --> 00:00:03,10
- In this video, we're going to talk about variable scope,

2
00:00:03,10 --> 00:00:05,30
and how functions relate to it.

3
00:00:05,30 --> 00:00:06,90
Here's an example.

4
00:00:06,90 --> 00:00:08,80
I have two variables set here:

5
00:00:08,80 --> 00:00:14,10
myNym is set to 32, and myResult is set to "Success!"

6
00:00:14,10 --> 00:00:16,20
And then, I write a function.

7
00:00:16,20 --> 00:00:17,20
It's called randomizer,

8
00:00:17,20 --> 00:00:19,60
and it takes a limit as its argument.

9
00:00:19,60 --> 00:00:21,30
Inside the function, I create

10
00:00:21,30 --> 00:00:22,80
a variable called randomNumber,

11
00:00:22,80 --> 00:00:25,50
and assign a randomly generated number to it.

12
00:00:25,50 --> 00:00:27,10
Then I create another variable,

13
00:00:27,10 --> 00:00:31,00
which is called myNum again, just like out here,

14
00:00:31,00 --> 00:00:34,10
and I'm assigning that random number to it.

15
00:00:34,10 --> 00:00:36,80
Then down here, I'm logging a couple of lines,

16
00:00:36,80 --> 00:00:39,50
and finally returning this number.

17
00:00:39,50 --> 00:00:42,30
So let's execute this and see what we get.

18
00:00:42,30 --> 00:00:45,40
Then we'll discuss it a little more.

19
00:00:45,40 --> 00:00:47,40
So I'm going to paste this all in.

20
00:00:47,40 --> 00:00:51,10
And now, I'll have a variable called myNum set to 32,

21
00:00:51,10 --> 00:00:54,50
and myResult set to "Success!"

22
00:00:54,50 --> 00:00:57,40
Now I'm going to invoke the randomizer function.

23
00:00:57,40 --> 00:00:59,60
I'll pass in 10.

24
00:00:59,60 --> 00:01:01,00
So, this is going to set myNum

25
00:01:01,00 --> 00:01:03,80
to a random number between zero and 10.

26
00:01:03,80 --> 00:01:06,70
Now the question is, what exactly is going to happen here?

27
00:01:06,70 --> 00:01:08,90
We have a variable called myNum up here,

28
00:01:08,90 --> 00:01:10,50
and one inside this function.

29
00:01:10,50 --> 00:01:12,50
Are they the same thing?

30
00:01:12,50 --> 00:01:16,00
We can see from these messages that apparently they're not.

31
00:01:16,00 --> 00:01:18,10
myNum kept its original value of 32,

32
00:01:18,10 --> 00:01:21,10
even though the message says it was set to three.

33
00:01:21,10 --> 00:01:22,70
What we're seeing here is a difference

34
00:01:22,70 --> 00:01:25,40
in the scope of these two versions of myNum.

35
00:01:25,40 --> 00:01:27,20
In common JavaScript ES5,

36
00:01:27,20 --> 00:01:29,00
along with everything else they do,

37
00:01:29,00 --> 00:01:31,30
functions create scope.

38
00:01:31,30 --> 00:01:33,70
This version of myNum inside the function

39
00:01:33,70 --> 00:01:36,10
is its own separate variable, even though

40
00:01:36,10 --> 00:01:38,30
it uses the same name, because that name

41
00:01:38,30 --> 00:01:41,60
only applies inside the function.

42
00:01:41,60 --> 00:01:43,90
If I hadn't used var to define this,

43
00:01:43,90 --> 00:01:45,10
I would have been resetting

44
00:01:45,10 --> 00:01:46,90
the value of this version on top,

45
00:01:46,90 --> 00:01:47,90
which is a global variable

46
00:01:47,90 --> 00:01:51,50
because it's not inside a function.

47
00:01:51,50 --> 00:01:52,90
If I have another function,

48
00:01:52,90 --> 00:01:55,00
in this case I have one called doubleIt,

49
00:01:55,00 --> 00:01:56,40
I can reuse the same variable name

50
00:01:56,40 --> 00:01:59,00
again as long as I use "var."

51
00:01:59,00 --> 00:02:01,00
The version inside each function

52
00:02:01,00 --> 00:02:04,20
is boxed off from all the other ones.

53
00:02:04,20 --> 00:02:05,90
Of course, you might have noticed that

54
00:02:05,90 --> 00:02:08,60
the randomizer function also references myResult.

55
00:02:08,60 --> 00:02:10,80
That variable is not defined inside this function,

56
00:02:10,80 --> 00:02:13,90
but because it's a global variable, also known as

57
00:02:13,90 --> 00:02:16,50
being in the global scope, or global namespace,

58
00:02:16,50 --> 00:02:20,10
I can still use it inside my functions.

59
00:02:20,10 --> 00:02:21,80
Also note, that if I want to refer

60
00:02:21,80 --> 00:02:24,40
to the two versions of myNum, the plain version

61
00:02:24,40 --> 00:02:27,30
here refers to the local version,

62
00:02:27,30 --> 00:02:29,20
and I can reference the global one

63
00:02:29,20 --> 00:02:30,70
as a property of window,

64
00:02:30,70 --> 00:02:33,60
which in a web browser, is the global namespace.

65
00:02:33,60 --> 00:02:35,20
And know that same namespace

66
00:02:35,20 --> 00:02:38,30
is actually called global, in all lowercase.

67
00:02:38,30 --> 00:02:40,70
Any local variables I create inside a function

68
00:02:40,70 --> 00:02:43,20
are inaccessible outside the context of the function.

69
00:02:43,20 --> 00:02:45,60
So even after executing randomizer,

70
00:02:45,60 --> 00:02:50,50
any variables that were created inside, like random number,

71
00:02:50,50 --> 00:02:53,10
I get a reference error here.

72
00:02:53,10 --> 00:02:55,90
Not available in the global namespace.

73
00:02:55,90 --> 00:02:58,40
Functions are the main delimiters of scope

74
00:02:58,40 --> 00:03:01,40
in JavaScript classically, but with ECMAScript 2015

75
00:03:01,40 --> 00:03:04,20
the "let" and "const" keywords

76
00:03:04,20 --> 00:03:06,00
let you create variables that are scoped

77
00:03:06,00 --> 00:03:08,30
to any block, not just functions.

78
00:03:08,30 --> 00:03:10,10
We're mainly concerned with functions right now,

79
00:03:10,10 --> 00:03:12,00
but that's something you can investigate later

80
00:03:12,00 --> 00:03:13,80
if you're curious, or as you start to learn more

81
00:03:13,80 --> 00:03:16,00
about newer versions of JavaScript.

82
00:03:16,00 --> 00:03:18,10
And that concludes our look at the interplay

83
00:03:18,10 --> 00:03:20,00
of scope and functions in JavaScript.

