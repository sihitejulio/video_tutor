1
00:00:00,50 --> 00:00:01,70
- [Instructor] In this video we're going to begin

2
00:00:01,70 --> 00:00:02,90
looking at functions,

3
00:00:02,90 --> 00:00:05,70
which are a way of packaging up lines of code into one unit

4
00:00:05,70 --> 00:00:09,40
that you can name, invoke, and maintain separately.

5
00:00:09,40 --> 00:00:10,80
We've been dancing around the idea

6
00:00:10,80 --> 00:00:12,40
of functions a little bit in this course.

7
00:00:12,40 --> 00:00:14,40
It's kind of difficult to talk about anything in JavaScript

8
00:00:14,40 --> 00:00:15,90
that's meaningful without touching functions

9
00:00:15,90 --> 00:00:17,00
at least a little bit,

10
00:00:17,00 --> 00:00:19,00
so I'm glad we've gotten to it at last.

11
00:00:19,00 --> 00:00:20,50
Any time I've talked about methods

12
00:00:20,50 --> 00:00:23,20
that are part of objects, those have been functions.

13
00:00:23,20 --> 00:00:26,30
And now it's time to talk about making our own.

14
00:00:26,30 --> 00:00:29,10
We're going to start out with these four lines of code here.

15
00:00:29,10 --> 00:00:32,60
Just four lines of console.log that make animal noises.

16
00:00:32,60 --> 00:00:34,70
Really mission critical stuff.

17
00:00:34,70 --> 00:00:38,50
Course, if I'm going to do this repeatedly in real-time,

18
00:00:38,50 --> 00:00:41,20
if I take this over to the browser console,

19
00:00:41,20 --> 00:00:43,10
and paste it in.

20
00:00:43,10 --> 00:00:45,90
It's easy enough to do things over and over again.

21
00:00:45,90 --> 00:00:48,40
I can just hit the up arrow and I get it back,

22
00:00:48,40 --> 00:00:50,40
hit return, and it goes.

23
00:00:50,40 --> 00:00:52,00
As many times as I might want to do this,

24
00:00:52,00 --> 00:00:54,90
I can just do it over and over again manually.

25
00:00:54,90 --> 00:00:56,20
If I'm writing a JavaScript file

26
00:00:56,20 --> 00:00:57,50
I could just copy and paste

27
00:00:57,50 --> 00:00:59,40
the same thing over and over again.

28
00:00:59,40 --> 00:01:01,70
But if I ever need to change how it's implemented,

29
00:01:01,70 --> 00:01:04,00
say, make the first one not arf,

30
00:01:04,00 --> 00:01:06,40
but neigh, or whatever,

31
00:01:06,40 --> 00:01:08,40
I would have to go back and change each instance

32
00:01:08,40 --> 00:01:09,60
of that in my code.

33
00:01:09,60 --> 00:01:11,40
And that's tedious.

34
00:01:11,40 --> 00:01:13,90
What we would do in that case is define a function

35
00:01:13,90 --> 00:01:16,20
which would wrap all these lines into a single unit

36
00:01:16,20 --> 00:01:19,20
that I can name and then use whenever I needed to.

37
00:01:19,20 --> 00:01:20,20
And if I need to change it,

38
00:01:20,20 --> 00:01:23,40
I just have one place where I would make that change.

39
00:01:23,40 --> 00:01:26,30
So here's how we'd do that.

40
00:01:26,30 --> 00:01:28,50
Here's what a function is at its most basic.

41
00:01:28,50 --> 00:01:32,40
You simply write the word function, then a space,

42
00:01:32,40 --> 00:01:35,00
and then the name of the function.

43
00:01:35,00 --> 00:01:38,50
Next, a pair of parentheses, and a curly brace.

44
00:01:38,50 --> 00:01:40,00
You have another curly brace to end it,

45
00:01:40,00 --> 00:01:42,10
and then inside those curly braces

46
00:01:42,10 --> 00:01:45,50
you just put the lines of code that comprise the function.

47
00:01:45,50 --> 00:01:47,20
In this case we've put our animal noises

48
00:01:47,20 --> 00:01:49,10
into a function called speak.

49
00:01:49,10 --> 00:01:54,80
And if I copy this, and bring it over to my console,

50
00:01:54,80 --> 00:01:58,00
I'll paste it in, hit return.

51
00:01:58,00 --> 00:01:59,70
Now I have this function defined.

52
00:01:59,70 --> 00:02:01,50
It's called speak.

53
00:02:01,50 --> 00:02:03,40
And now to actually make these lines execute

54
00:02:03,40 --> 00:02:05,10
in the context of that function,

55
00:02:05,10 --> 00:02:07,50
what we do is called invoking the function,

56
00:02:07,50 --> 00:02:10,90
which is to write out its name, speak,

57
00:02:10,90 --> 00:02:15,10
followed by the two parentheses, and a semicolon.

58
00:02:15,10 --> 00:02:16,50
There we go.

59
00:02:16,50 --> 00:02:18,80
So now instead of writing those four lines out,

60
00:02:18,80 --> 00:02:20,30
if I wanted to do this again

61
00:02:20,30 --> 00:02:23,60
I can just use my one line, speak.

62
00:02:23,60 --> 00:02:24,90
As we'll see in upcoming lessons

63
00:02:24,90 --> 00:02:27,50
this is not nearly everything you can do with functions.

64
00:02:27,50 --> 00:02:28,30
But even if it were,

65
00:02:28,30 --> 00:02:30,40
this by itself would be pretty useful.

66
00:02:30,40 --> 00:02:31,80
Functions at their most basic

67
00:02:31,80 --> 00:02:33,20
can tie together lines of code,

68
00:02:33,20 --> 00:02:36,50
and give you one executable unit that has its own name,

69
00:02:36,50 --> 00:02:39,10
and can be executed as many times as you need.

70
00:02:39,10 --> 00:02:40,00
Really quite useful.

