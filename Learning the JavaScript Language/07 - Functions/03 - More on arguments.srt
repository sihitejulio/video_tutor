1
00:00:00,50 --> 00:00:01,70
- [Instructor] In this video we're going to look

2
00:00:01,70 --> 00:00:04,10
at arguments and functions a little more and see

3
00:00:04,10 --> 00:00:05,90
how you can have functions with variable numbers

4
00:00:05,90 --> 00:00:07,80
of arguments and how you can provide default

5
00:00:07,80 --> 00:00:11,40
values for arguments that you want to be optional.

6
00:00:11,40 --> 00:00:12,80
First, I'm going to show this function

7
00:00:12,80 --> 00:00:15,70
that I've defined called speakSomething.

8
00:00:15,70 --> 00:00:18,20
And it takes two arguments.

9
00:00:18,20 --> 00:00:20,00
Your functions don't have to have only one argument.

10
00:00:20,00 --> 00:00:22,10
They can have as many as you want.

11
00:00:22,10 --> 00:00:24,20
So anyway, speakSomething takes two.

12
00:00:24,20 --> 00:00:26,40
It takes what to say.

13
00:00:26,40 --> 00:00:29,10
We'll pass in hey, hey.

14
00:00:29,10 --> 00:00:31,80
And how many times to say it?

15
00:00:31,80 --> 00:00:34,10
We'll go with five.

16
00:00:34,10 --> 00:00:36,60
And so when I call this, it says, "Hey, hey,"

17
00:00:36,60 --> 00:00:38,70
and it tells me how many times it's said it

18
00:00:38,70 --> 00:00:42,10
so far starting with zero.

19
00:00:42,10 --> 00:00:43,50
So you can see this works fine.

20
00:00:43,50 --> 00:00:44,70
You can have more than one argument

21
00:00:44,70 --> 00:00:47,20
in your functions, but what if say you want

22
00:00:47,20 --> 00:00:50,10
to provide default values for some of these.

23
00:00:50,10 --> 00:00:51,90
So what if I know that I want to say, "Hey, hey,"

24
00:00:51,90 --> 00:00:55,90
but I don't care how many times I said it?

25
00:00:55,90 --> 00:01:00,10
I can let the function do that for me and that works fine.

26
00:01:00,10 --> 00:01:03,90
In this case, the default value is 10, so it goes 10 times.

27
00:01:03,90 --> 00:01:07,30
I can even do it with no arguments.

28
00:01:07,30 --> 00:01:09,70
And it just says, "Default speech."

29
00:01:09,70 --> 00:01:12,30
So how do we do this with JavaScript?

30
00:01:12,30 --> 00:01:15,50
Here's the definition of the speakSomething function.

31
00:01:15,50 --> 00:01:19,10
I have the argument what and the other argument how many.

32
00:01:19,10 --> 00:01:21,50
We're going to talk about how to set defaults for these.

33
00:01:21,50 --> 00:01:23,40
In some programming languages there are ways

34
00:01:23,40 --> 00:01:25,30
to specify default values right here

35
00:01:25,30 --> 00:01:27,80
where you specify the parameters, usually phrased

36
00:01:27,80 --> 00:01:31,60
something like this.

37
00:01:31,60 --> 00:01:33,90
Now in some programming languages there are ways

38
00:01:33,90 --> 00:01:35,70
to specify default values right here

39
00:01:35,70 --> 00:01:37,80
where you specify the parameters, and now

40
00:01:37,80 --> 00:01:41,60
in ECMAScript 2015, JavaScript can do that too.

41
00:01:41,60 --> 00:01:47,30
But for the greatest compatibility let's see another way.

42
00:01:47,30 --> 00:01:48,60
So here what I'm doing is using

43
00:01:48,60 --> 00:01:50,90
the ternary operator, and the condition

44
00:01:50,90 --> 00:01:52,40
that I'm checking against is what

45
00:01:52,40 --> 00:01:56,20
type what is, this argument.

46
00:01:56,20 --> 00:01:58,20
And if I haven't passed a value for the parameter

47
00:01:58,20 --> 00:02:00,60
its type will be undefined.

48
00:02:00,60 --> 00:02:03,00
So what I'm doing here is checking whether

49
00:02:03,00 --> 00:02:07,30
what is not undefined, and if that's true

50
00:02:07,30 --> 00:02:09,30
I use the value of what and set it

51
00:02:09,30 --> 00:02:12,70
to this variable inside my function called what.

52
00:02:12,70 --> 00:02:15,60
If it is undefined, I provide my default value,

53
00:02:15,60 --> 00:02:17,50
default speech.

54
00:02:17,50 --> 00:02:19,30
And I do the same thing for the how many

55
00:02:19,30 --> 00:02:22,70
parameter with its default value being 10.

56
00:02:22,70 --> 00:02:24,10
And then of course I have my for loop

57
00:02:24,10 --> 00:02:26,80
down here that does the printing to the console.

58
00:02:26,80 --> 00:02:28,60
Now, when you do your default arguments this way

59
00:02:28,60 --> 00:02:30,40
the order of them matters.

60
00:02:30,40 --> 00:02:35,30
So I can't just say speakSomething and pass in 10.

61
00:02:35,30 --> 00:02:38,20
Well, actually I can but that only works

62
00:02:38,20 --> 00:02:41,80
if I meant to print out 10 10 times.

63
00:02:41,80 --> 00:02:43,80
If I meant for this to be the number of times

64
00:02:43,80 --> 00:02:46,10
that this thing were printed, it's not working

65
00:02:46,10 --> 00:02:48,10
the way I would expect.

66
00:02:48,10 --> 00:02:50,20
10 will always be used as the first argument

67
00:02:50,20 --> 00:02:52,20
if it's the only one.

68
00:02:52,20 --> 00:02:53,60
So you can provide default values

69
00:02:53,60 --> 00:02:55,20
but the order still matters.

70
00:02:55,20 --> 00:02:57,10
There are more advanced techniques for defining

71
00:02:57,10 --> 00:02:58,90
functions that can handle arguments in different

72
00:02:58,90 --> 00:03:00,50
orders and quantities, but when you're just

73
00:03:00,50 --> 00:03:02,10
starting out it probably is more likely

74
00:03:02,10 --> 00:03:03,70
to turn your brain into a pretzel

75
00:03:03,70 --> 00:03:06,10
so we'll just leave that aside for now.

76
00:03:06,10 --> 00:03:07,50
One other thing I'll note is that you'll

77
00:03:07,50 --> 00:03:09,20
sometimes see default arguments phrased

78
00:03:09,20 --> 00:03:11,20
as these much shorter expressions right here

79
00:03:11,20 --> 00:03:12,80
in the comments.

80
00:03:12,80 --> 00:03:13,90
This is a little trick you can do

81
00:03:13,90 --> 00:03:15,80
with the logical or argument that assumes

82
00:03:15,80 --> 00:03:17,30
a valid argument you're passing into your

83
00:03:17,30 --> 00:03:19,90
function will be truth-y because an expression

84
00:03:19,90 --> 00:03:23,10
involving the logical or will return the first

85
00:03:23,10 --> 00:03:25,50
value if that value is truth-y.

86
00:03:25,50 --> 00:03:27,70
It works most of the time and looks very clean

87
00:03:27,70 --> 00:03:29,80
and it's certainly shorter than this

88
00:03:29,80 --> 00:03:32,00
but if false-y values like zero are allowed

89
00:03:32,00 --> 00:03:34,40
as valid arguments, this version would convert

90
00:03:34,40 --> 00:03:36,70
those valid arguments into your default values.

91
00:03:36,70 --> 00:03:39,30
So watch out for that.

92
00:03:39,30 --> 00:03:41,40
Back here in my JavaScript console you might

93
00:03:41,40 --> 00:03:43,00
have seen in that file that I have another

94
00:03:43,00 --> 00:03:45,00
function called addingMachine.

95
00:03:45,00 --> 00:03:46,60
We're going to try that now.

96
00:03:46,60 --> 00:03:49,70
Here it is, and addingMachine can take

97
00:03:49,70 --> 00:03:50,90
any number of arguments.

98
00:03:50,90 --> 00:03:52,20
It just doesn't care.

99
00:03:52,20 --> 00:03:56,80
I can add one, two, and three, and it returns six.

100
00:03:56,80 --> 00:03:59,10
Or I can go totally crazy and add a whole bunch

101
00:03:59,10 --> 00:04:01,20
of numbers in here.

102
00:04:01,20 --> 00:04:06,90
Make sure they're all filled in.

103
00:04:06,90 --> 00:04:08,40
So how does this work?

104
00:04:08,40 --> 00:04:10,70
Let's switch back to the editor and check it out.

105
00:04:10,70 --> 00:04:12,60
Here's the addingMachine function.

106
00:04:12,60 --> 00:04:14,50
And it's making use of an object that every

107
00:04:14,50 --> 00:04:17,10
function knows about, arguments,

108
00:04:17,10 --> 00:04:19,60
which is an array-like object that gives us

109
00:04:19,60 --> 00:04:21,30
some of the tools of an array.

110
00:04:21,30 --> 00:04:23,30
Like real arrays, it has a length property

111
00:04:23,30 --> 00:04:25,40
that I'm using here.

112
00:04:25,40 --> 00:04:27,90
So what I'm doing is I'm using a for loop

113
00:04:27,90 --> 00:04:30,20
to iterate over the arguments array

114
00:04:30,20 --> 00:04:31,90
and then pick up the number that is each

115
00:04:31,90 --> 00:04:33,90
individual argument.

116
00:04:33,90 --> 00:04:36,60
And I'm adding it to a running total down here

117
00:04:36,60 --> 00:04:39,00
using the addition assignment operator.

118
00:04:39,00 --> 00:04:41,10
And of course I'm also checking to make sure

119
00:04:41,10 --> 00:04:43,80
that it's a number before doing that.

120
00:04:43,80 --> 00:04:46,70
So to run this down, we initialize the total as zero.

121
00:04:46,70 --> 00:04:48,40
Then we loop over the arguments array,

122
00:04:48,40 --> 00:04:50,70
grab each number, check its type,

123
00:04:50,70 --> 00:04:53,40
see if it's a number, and add it to the total.

124
00:04:53,40 --> 00:04:56,20
And once that's all done, return it.

125
00:04:56,20 --> 00:04:58,40
So that's how that function works.

126
00:04:58,40 --> 00:05:00,50
You can use the arguments array not just to run

127
00:05:00,50 --> 00:05:02,00
through everything that's passed in

128
00:05:02,00 --> 00:05:04,10
but you could also pull out individual arguments

129
00:05:04,10 --> 00:05:06,00
using their index.

130
00:05:06,00 --> 00:05:07,80
So for example, in this speakSomething function

131
00:05:07,80 --> 00:05:10,90
I have these two arguments, what and how many.

132
00:05:10,90 --> 00:05:12,80
And I can still specify them by name up here

133
00:05:12,80 --> 00:05:14,30
but if I wanted to I could address them

134
00:05:14,30 --> 00:05:25,10
in my function as arguments sub zero and arguments sub one.

135
00:05:25,10 --> 00:05:27,40
That would also work.

136
00:05:27,40 --> 00:05:29,30
So now you know that functions can take

137
00:05:29,30 --> 00:05:31,60
an arbitrary number of arguments and you can set

138
00:05:31,60 --> 00:05:33,30
default values for them.

139
00:05:33,30 --> 00:05:35,40
You can name the arguments or you can use

140
00:05:35,40 --> 00:05:38,70
the arguments array-like object to address them

141
00:05:38,70 --> 00:05:40,60
and you can use a for loop to iterate over

142
00:05:40,60 --> 00:05:43,30
the entire arguments array, allowing completely

143
00:05:43,30 --> 00:05:45,00
arbitrary numbers of arguments.

