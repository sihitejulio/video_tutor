1
00:00:00,50 --> 00:00:01,80
- [Narrator] In this video we're going to talk about

2
00:00:01,80 --> 00:00:03,10
callback functions.

3
00:00:03,10 --> 00:00:05,00
We've seen these in use a couple of times,

4
00:00:05,00 --> 00:00:06,90
but I want to give them a little more specific attention

5
00:00:06,90 --> 00:00:10,80
because they're used a lot in modern JavaScript.

6
00:00:10,80 --> 00:00:12,60
Callback function is a piece of jargon referring to

7
00:00:12,60 --> 00:00:14,50
a function that's passed as an argument

8
00:00:14,50 --> 00:00:18,20
into another function and executed in that function.

9
00:00:18,20 --> 00:00:20,30
Because functions are first-class citizens in JavaScript

10
00:00:20,30 --> 00:00:22,90
you'll see this all over the place.

11
00:00:22,90 --> 00:00:24,60
Here's a function called doubleIt

12
00:00:24,60 --> 00:00:26,20
which takes a number and returns it,

13
00:00:26,20 --> 00:00:29,80
multiplied by two using the multiply assignment operator.

14
00:00:29,80 --> 00:00:32,10
And here's another variable called myNumbers

15
00:00:32,10 --> 00:00:34,30
with the digits one through five in it.

16
00:00:34,30 --> 00:00:36,90
Then here's a variable called myNumbers

17
00:00:36,90 --> 00:00:40,70
with an array with the digits from one to five in it.

18
00:00:40,70 --> 00:00:42,90
JavaScript arrays have a method called map

19
00:00:42,90 --> 00:00:44,40
that takes a callback

20
00:00:44,40 --> 00:00:47,60
with that callback having one required specified parameter,

21
00:00:47,60 --> 00:00:50,10
the item in the array that we're acting on.

22
00:00:50,10 --> 00:00:53,60
The callback is executed on every item in the array in turn,

23
00:00:53,60 --> 00:00:56,90
creating a new array with a result of each execution.

24
00:00:56,90 --> 00:00:58,90
Here I've made an array of all the same numbers

25
00:00:58,90 --> 00:01:02,70
with each one doubled using array.map

26
00:01:02,70 --> 00:01:05,20
and passing in doubleIt as my callback.

27
00:01:05,20 --> 00:01:06,30
As long as the callback has its arguments

28
00:01:06,30 --> 00:01:08,50
set up correctly this works great.

29
00:01:08,50 --> 00:01:12,10
So let's select all this, copy it,

30
00:01:12,10 --> 00:01:13,90
and I'm going to bring it over to my browser,

31
00:01:13,90 --> 00:01:16,50
paste it in and try it.

32
00:01:16,50 --> 00:01:20,50
So, myNumbers

33
00:01:20,50 --> 00:01:23,20
is still set to one two three four five

34
00:01:23,20 --> 00:01:28,40
but myDoubles is set to two four six eight ten.

35
00:01:28,40 --> 00:01:31,90
So this map function took another function as its argument,

36
00:01:31,90 --> 00:01:35,60
a callback, and it executed it on each one of these numbers

37
00:01:35,60 --> 00:01:37,70
creating a new array.

38
00:01:37,70 --> 00:01:40,70
Array map is used a lot in modern JavaScript frameworks like

39
00:01:40,70 --> 00:01:45,00
React for rendering lists of items in a user interface.

40
00:01:45,00 --> 00:01:46,60
Another example of an array method that takes

41
00:01:46,60 --> 00:01:49,70
a callback function is forEach.

42
00:01:49,70 --> 00:01:51,30
forEach doesn't return anything,

43
00:01:51,30 --> 00:01:52,80
it just iterates over the array

44
00:01:52,80 --> 00:01:55,40
passing each value from the array as the argument

45
00:01:55,40 --> 00:01:57,20
into that callback function.

46
00:01:57,20 --> 00:01:59,50
It's a shorter way, perhaps a more expressive way, even,

47
00:01:59,50 --> 00:02:02,30
of looping over an array than a for loop.

48
00:02:02,30 --> 00:02:04,80
It's not as easy to stop as a for loop,

49
00:02:04,80 --> 00:02:06,50
the break keyword we can use to stop a for loop

50
00:02:06,50 --> 00:02:09,30
doesn't do it, but you might find it more pleasant to use

51
00:02:09,30 --> 00:02:10,80
as long as you definitely want to loop through

52
00:02:10,80 --> 00:02:13,00
the entire array.

53
00:02:13,00 --> 00:02:14,90
In this example here I'm using an anonymous

54
00:02:14,90 --> 00:02:17,90
callback function inserted directly as the argument

55
00:02:17,90 --> 00:02:20,40
instead of defining it in advance.

56
00:02:20,40 --> 00:02:23,30
This will log the value of each item in the array

57
00:02:23,30 --> 00:02:25,90
without stopping for a break.

58
00:02:25,90 --> 00:02:29,70
Let's copy that and try it.

59
00:02:29,70 --> 00:02:32,80
There it goes.

60
00:02:32,80 --> 00:02:35,30
One other thing I'll mention about callbacks.

61
00:02:35,30 --> 00:02:37,70
In ECMAScript 2015 there's another kind of function

62
00:02:37,70 --> 00:02:41,20
called an arrow function that you'll see sometimes.

63
00:02:41,20 --> 00:02:43,50
Here's the same doubleIt function as up here,

64
00:02:43,50 --> 00:02:46,70
rewritten as an arrow function.

65
00:02:46,70 --> 00:02:48,90
As you can see it removes a lot of boiler plate

66
00:02:48,90 --> 00:02:50,10
from the function definition,

67
00:02:50,10 --> 00:02:52,50
stripping it down to its essentials.

68
00:02:52,50 --> 00:02:54,90
The parameters, you can leave off the parentheses that we

69
00:02:54,90 --> 00:02:58,30
use up here if there's only one,

70
00:02:58,30 --> 00:03:01,40
then an arrow and what the function returns.

71
00:03:01,40 --> 00:03:03,00
You can leave off the curly braces

72
00:03:03,00 --> 00:03:05,20
if it's very simple like this.

73
00:03:05,20 --> 00:03:06,90
The parentheses are also optional but they help

74
00:03:06,90 --> 00:03:09,20
clarify what's going on.

75
00:03:09,20 --> 00:03:10,80
Because it's so short it can be popular

76
00:03:10,80 --> 00:03:13,00
for writing callbacks for functions like array map,

77
00:03:13,00 --> 00:03:14,50
for each, and others

78
00:03:14,50 --> 00:03:16,90
that can take a function as a parameter.

79
00:03:16,90 --> 00:03:18,70
But because it's a feature of ES6,

80
00:03:18,70 --> 00:03:20,60
it's not something that's really safe to use

81
00:03:20,60 --> 00:03:22,80
in browsers right now,

82
00:03:22,80 --> 00:03:26,20
But it is safe to use in recent versions of Node.

83
00:03:26,20 --> 00:03:28,10
So that's another look at callback functions

84
00:03:28,10 --> 00:03:31,00
and some of the ways that they are used commonly.

