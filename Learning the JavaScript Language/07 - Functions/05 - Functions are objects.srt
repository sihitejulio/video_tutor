1
00:00:00,50 --> 00:00:01,40
- [Instructor] In this video we're going to talk about

2
00:00:01,40 --> 00:00:04,40
how functions are first class citizens in JavaScript.

3
00:00:04,40 --> 00:00:07,50
Indeed, there are objects that have the power to be invoked.

4
00:00:07,50 --> 00:00:10,50
Let's talk about what that means and look a little bit

5
00:00:10,50 --> 00:00:13,00
at how useful it can be.

6
00:00:13,00 --> 00:00:14,30
Here's the definition of a function

7
00:00:14,30 --> 00:00:16,20
called speakSomething, we've seen this

8
00:00:16,20 --> 00:00:17,80
kind of construction before,

9
00:00:17,80 --> 00:00:18,90
you just use the word function,

10
00:00:18,90 --> 00:00:21,70
followed by a space, followed by the name of the function

11
00:00:21,70 --> 00:00:23,80
and its definition.

12
00:00:23,80 --> 00:00:26,50
And so all this says is just take this "what" parameter

13
00:00:26,50 --> 00:00:30,80
and then log it to the console, using a for-loop.

14
00:00:30,80 --> 00:00:33,10
Written this way, this is called a function expression.

15
00:00:33,10 --> 00:00:36,50
I can rewrite this function definition a different way.

16
00:00:36,50 --> 00:00:39,10
In this version I'm creating a variable

17
00:00:39,10 --> 00:00:41,00
called speakSomething, and then I'm assigning

18
00:00:41,00 --> 00:00:43,90
to that what's called an anonymous function.

19
00:00:43,90 --> 00:00:46,70
Function definition inside the curly braces

20
00:00:46,70 --> 00:00:49,30
is the same but you can see here

21
00:00:49,30 --> 00:00:52,10
the function, it's just "function" followed by

22
00:00:52,10 --> 00:00:55,10
the parameters with no name,

23
00:00:55,10 --> 00:00:57,00
hence, an anonymous function.

24
00:00:57,00 --> 00:00:59,20
And for our purposes, these two constructions

25
00:00:59,20 --> 00:01:02,60
are basically equivalent, but they illustrate the idea

26
00:01:02,60 --> 00:01:04,60
that a function is actually an object.

27
00:01:04,60 --> 00:01:07,30
It can be passed around as a value in JavaScript

28
00:01:07,30 --> 00:01:11,20
and assigned two variables as one example.

29
00:01:11,20 --> 00:01:12,80
One of the things that makes this interesting,

30
00:01:12,80 --> 00:01:15,30
is that you can feed functions into other functions

31
00:01:15,30 --> 00:01:17,40
as arguments, and why would you want to do that?

32
00:01:17,40 --> 00:01:19,40
I'll give you an example.

33
00:01:19,40 --> 00:01:21,70
When you're working with JavaScript on the web,

34
00:01:21,70 --> 00:01:23,90
there's a global window object available.

35
00:01:23,90 --> 00:01:27,00
All global variables are properties of the window object

36
00:01:27,00 --> 00:01:28,70
and there are a bunch of functions that are built

37
00:01:28,70 --> 00:01:30,70
into the window object as well.

38
00:01:30,70 --> 00:01:33,40
One of them is called setTimeout.

39
00:01:33,40 --> 00:01:34,60
It's a function that takes

40
00:01:34,60 --> 00:01:37,40
as its first argument, a function.

41
00:01:37,40 --> 00:01:39,80
What setTimeout out does, is it sets a timer

42
00:01:39,80 --> 00:01:41,90
after which, a function will execute,

43
00:01:41,90 --> 00:01:43,80
and so for its first argument,

44
00:01:43,80 --> 00:01:45,70
it takes that actual function.

45
00:01:45,70 --> 00:01:48,30
So in this case if I want to speak something

46
00:01:48,30 --> 00:01:51,50
after a certain time, I'd feed it as the first argument

47
00:01:51,50 --> 00:01:54,80
into setTimeout just like I'm doing here.

48
00:01:54,80 --> 00:01:56,20
It doesn't matter which of these ways

49
00:01:56,20 --> 00:01:57,60
I've defined speakSomething,

50
00:01:57,60 --> 00:01:59,60
I can pass it in either way by its name.

51
00:01:59,60 --> 00:02:01,30
Note that I'm not including the parenthesis here,

52
00:02:01,30 --> 00:02:03,90
because I'm not invoking it right now,

53
00:02:03,90 --> 00:02:07,00
setTimeout does that for me later on.

54
00:02:07,00 --> 00:02:07,90
Then the second parameter

55
00:02:07,90 --> 00:02:09,00
is the number of milliseconds

56
00:02:09,00 --> 00:02:11,30
after which I want this function to execute.

57
00:02:11,30 --> 00:02:14,90
So with five thousand here that would be five seconds later.

58
00:02:14,90 --> 00:02:16,70
If you've ever worked with another programming language,

59
00:02:16,70 --> 00:02:18,40
this could look quite interesting.

60
00:02:18,40 --> 00:02:19,60
You're actually taking a function

61
00:02:19,60 --> 00:02:21,40
and passing it as a value.

62
00:02:21,40 --> 00:02:23,30
Functions are all objects in JavaScript,

63
00:02:23,30 --> 00:02:26,40
they actually inherit from a global function object,

64
00:02:26,40 --> 00:02:30,30
spelled like this.

65
00:02:30,30 --> 00:02:31,80
That's just a technical detail,

66
00:02:31,80 --> 00:02:34,00
the important thing to note, is that functions are objects

67
00:02:34,00 --> 00:02:36,40
and that you can do stuff like this with them.

68
00:02:36,40 --> 00:02:39,80
Let's try it out.

69
00:02:39,80 --> 00:02:43,90
Going to copy all this and bring it over to my browser.

70
00:02:43,90 --> 00:02:45,30
Watch what happens.

71
00:02:45,30 --> 00:02:48,30
We'll wait five seconds.

72
00:02:48,30 --> 00:02:50,00
And there it goes.

73
00:02:50,00 --> 00:02:51,90
The ten here is a detail of the way

74
00:02:51,90 --> 00:02:54,10
Chrome and some other web inspectors work.

75
00:02:54,10 --> 00:02:56,60
If they get the same thing multiple times in the console

76
00:02:56,60 --> 00:02:58,40
it'll just increment a number

77
00:02:58,40 --> 00:03:00,60
instead of displaying it a whole bunch of times,

78
00:03:00,60 --> 00:03:02,40
Otherwise your console window would just sort of,

79
00:03:02,40 --> 00:03:03,90
fill up with junk.

80
00:03:03,90 --> 00:03:05,50
So now you can see that you can pass functions

81
00:03:05,50 --> 00:03:08,00
into other functions like this.

82
00:03:08,00 --> 00:03:09,60
Another thing that you can do with functions,

83
00:03:09,60 --> 00:03:12,70
is just like how setTimeout itself is defined.

84
00:03:12,70 --> 00:03:16,20
It's a member function or a method of the window object.

85
00:03:16,20 --> 00:03:18,50
You can create your own objects and assign functions

86
00:03:18,50 --> 00:03:20,30
as members of those objects.

87
00:03:20,30 --> 00:03:22,30
This happens all over the place in JavaScript,

88
00:03:22,30 --> 00:03:24,40
that's how all the built-in methods are defined,

89
00:03:24,40 --> 00:03:26,70
and you can do it for yourself as well.

90
00:03:26,70 --> 00:03:29,70
So here for example, I can define an object,

91
00:03:29,70 --> 00:03:33,80
call it obj, and then I can give it a property

92
00:03:33,80 --> 00:03:39,10
called "say hello", and that property can be a function.

93
00:03:39,10 --> 00:03:40,80
This is an anonymous function here

94
00:03:40,80 --> 00:03:41,80
and it just logged the message

95
00:03:41,80 --> 00:03:44,10
to the console saying "hello".

96
00:03:44,10 --> 00:03:45,70
And then I can invoke this function

97
00:03:45,70 --> 00:03:50,50
as a property of my object.

98
00:03:50,50 --> 00:03:53,90
Let's copy and try this out as well.

99
00:03:53,90 --> 00:03:56,50
There we go it says "hello" back.

100
00:03:56,50 --> 00:03:58,50
So the key lesson here is that functions

101
00:03:58,50 --> 00:03:59,80
are objects and they can be passed around

102
00:03:59,80 --> 00:04:03,20
as values just like any other value in JavaScript.

103
00:04:03,20 --> 00:04:05,30
This comes up constantly in modern JavaScript

104
00:04:05,30 --> 00:04:06,40
as more developers have come

105
00:04:06,40 --> 00:04:08,00
to understand how function oriented

106
00:04:08,00 --> 00:04:11,60
JavaScript really is and started to leverage that power.

107
00:04:11,60 --> 00:04:13,80
You may not use it yourself in your initial forays

108
00:04:13,80 --> 00:04:16,00
into using JavaScript, but you'll certainly see it

109
00:04:16,00 --> 00:04:18,00
in the code other people write.

