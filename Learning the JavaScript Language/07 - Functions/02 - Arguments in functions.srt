1
00:00:00,50 --> 00:00:01,50
- [Instructor] In this video, we're going to

2
00:00:01,50 --> 00:00:03,00
extend our knowledge of functions by

3
00:00:03,00 --> 00:00:04,80
looking at using arguments to solve

4
00:00:04,80 --> 00:00:07,10
a couple of different kinds of problems.

5
00:00:07,10 --> 00:00:09,30
One is to take some data and modify it.

6
00:00:09,30 --> 00:00:10,70
The other is get the answer to a

7
00:00:10,70 --> 00:00:13,40
question in a way we'll need to use repeatedly.

8
00:00:13,40 --> 00:00:14,60
So, we'll start out by looking at

9
00:00:14,60 --> 00:00:16,20
modifying some data.

10
00:00:16,20 --> 00:00:18,90
Let's say I want to be able to take any string and

11
00:00:18,90 --> 00:00:20,50
change it so it looked like it was being said

12
00:00:20,50 --> 00:00:23,30
by Elmer Fudd from the Bugs Bunny cartoons.

13
00:00:23,30 --> 00:00:27,30
I have a function called fuddify that will do this.

14
00:00:27,30 --> 00:00:29,50
Let's see, how 'about

15
00:00:29,50 --> 00:00:37,70
"Be very quiet, I'm hunting rabbits." and

16
00:00:37,70 --> 00:00:39,40
it comes back with, "Be vwey quiet,

17
00:00:39,40 --> 00:00:41,20
I'm hunting wabbits."

18
00:00:41,20 --> 00:00:43,60
Okay, I can put in any string.

19
00:00:43,60 --> 00:00:49,60
Like, how 'about, "You screwy rabbit."

20
00:00:49,60 --> 00:00:51,00
"You scwewy wabbit."

21
00:00:51,00 --> 00:00:53,40
Yep, so by having this function,

22
00:00:53,40 --> 00:00:55,40
I don't have to write the lines of code

23
00:00:55,40 --> 00:00:57,10
that would do the processing on these strings

24
00:00:57,10 --> 00:00:58,10
over and over again.

25
00:00:58,10 --> 00:01:00,00
All I need to do is invoke the function and

26
00:01:00,00 --> 00:01:02,00
pass it all the different strings I want.

27
00:01:02,00 --> 00:01:04,00
So, how does it work?

28
00:01:04,00 --> 00:01:05,50
Let's take a look at the definition

29
00:01:05,50 --> 00:01:07,50
of this fuddify function.

30
00:01:07,50 --> 00:01:09,00
Here it is.

31
00:01:09,00 --> 00:01:10,30
I have the word function, followed

32
00:01:10,30 --> 00:01:12,40
by fuddify and then here

33
00:01:12,40 --> 00:01:15,80
inside the parenthesis, I have speech.

34
00:01:15,80 --> 00:01:17,90
This is called an argument or a parameter and

35
00:01:17,90 --> 00:01:19,20
it's just a variable that in the

36
00:01:19,20 --> 00:01:20,90
context of this function, will be known

37
00:01:20,90 --> 00:01:23,40
without my having to define it separately.

38
00:01:23,40 --> 00:01:25,50
So I'm passing in a string,

39
00:01:25,50 --> 00:01:27,40
which will be named speech,

40
00:01:27,40 --> 00:01:29,10
at least a string is what I want.

41
00:01:29,10 --> 00:01:31,50
So the first line inside the function,

42
00:01:31,50 --> 00:01:33,50
the one that's not a comment,

43
00:01:33,50 --> 00:01:34,60
checks to make sure that this is

44
00:01:34,60 --> 00:01:35,80
actually a string,

45
00:01:35,80 --> 00:01:37,40
using type of and

46
00:01:37,40 --> 00:01:39,90
if it isn't, it logs an error message,

47
00:01:39,90 --> 00:01:43,70
says, "nice twy, wabbit" and returns.

48
00:01:43,70 --> 00:01:46,20
Return short circuits the rest of this function.

49
00:01:46,20 --> 00:01:48,10
If this line executes, the rest of

50
00:01:48,10 --> 00:01:50,90
the function body down below is ignored.

51
00:01:50,90 --> 00:01:53,40
But assuming speech is a string,

52
00:01:53,40 --> 00:01:55,80
we process it by making some changes to it and

53
00:01:55,80 --> 00:01:57,80
then, returning it.

54
00:01:57,80 --> 00:01:59,80
Because we're using the Java Script console,

55
00:01:59,80 --> 00:02:01,90
the return will just be shown right in

56
00:02:01,90 --> 00:02:03,80
the console unless I capture it somehow,

57
00:02:03,80 --> 00:02:10,30
like, say, in a variable, like this,

58
00:02:10,30 --> 00:02:13,80
Switch back over to the browser and try this.

59
00:02:13,80 --> 00:02:15,70
So now that I've captured the output,

60
00:02:15,70 --> 00:02:17,20
it doesn't get returned directly.

61
00:02:17,20 --> 00:02:23,10
It goes into this variable called utterance.

62
00:02:23,10 --> 00:02:24,40
So that's how you can take data and

63
00:02:24,40 --> 00:02:26,00
process it using a function.

64
00:02:26,00 --> 00:02:27,20
The other main case for which

65
00:02:27,20 --> 00:02:29,10
you'll be using functions, at the beginning,

66
00:02:29,10 --> 00:02:30,20
would be to answer a question

67
00:02:30,20 --> 00:02:32,90
that you need the answer to over and over again.

68
00:02:32,90 --> 00:02:34,50
So for example, we know that we can

69
00:02:34,50 --> 00:02:36,70
determine whether a number is odd or even

70
00:02:36,70 --> 00:02:38,30
using the modulus operator.

71
00:02:38,30 --> 00:02:41,70
So twelve mod two, return zero and

72
00:02:41,70 --> 00:02:44,40
that's how we know that the number is even.

73
00:02:44,40 --> 00:02:45,80
But using that expression directly doesn't

74
00:02:45,80 --> 00:02:47,10
really illustrate clearly what we're

75
00:02:47,10 --> 00:02:49,10
trying to do, it's much better to

76
00:02:49,10 --> 00:02:50,40
create a function that's named

77
00:02:50,40 --> 00:02:53,10
something meaningful, same as with variables.

78
00:02:53,10 --> 00:02:54,20
In this case, what I am doing is

79
00:02:54,20 --> 00:02:56,00
checking whether a number is odd or even

80
00:02:56,00 --> 00:02:58,40
so I can define a function called is even

81
00:02:58,40 --> 00:03:00,40
that returns true for even numbers,

82
00:03:00,40 --> 00:03:02,20
false for odds.

83
00:03:02,20 --> 00:03:06,90
Here's what that could look like.

84
00:03:06,90 --> 00:03:08,90
So here's how I've defined is even.

85
00:03:08,90 --> 00:03:10,50
I have function and then the name

86
00:03:10,50 --> 00:03:13,30
of the function is even and it takes

87
00:03:13,30 --> 00:03:17,30
the one argument, num, then I'm asking,

88
00:03:17,30 --> 00:03:21,90
is this number, mod two, equal to zero?

89
00:03:21,90 --> 00:03:24,70
In other words, is the remainder zero?

90
00:03:24,70 --> 00:03:26,80
In other words, is the number even?

91
00:03:26,80 --> 00:03:28,40
And if so, I return true.

92
00:03:28,40 --> 00:03:29,90
Otherwise, it's an odd number, so

93
00:03:29,90 --> 00:03:32,50
I'll return false and when you have

94
00:03:32,50 --> 00:03:34,00
a really simple comparison like this

95
00:03:34,00 --> 00:03:35,90
that either returns true or false,

96
00:03:35,90 --> 00:03:37,10
you can collapse this whole thing

97
00:03:37,10 --> 00:03:41,00
down even more by returning this value directly.

98
00:03:41,00 --> 00:03:43,30
Cause as we know, a comparison like this

99
00:03:43,30 --> 00:03:46,30
evaluates down to true or false.

100
00:03:46,30 --> 00:03:50,60
So this one line is equivalent to this five lines.

101
00:03:50,60 --> 00:03:53,10
In either case, it's very simple but

102
00:03:53,10 --> 00:03:55,10
being able to read clearly named functions

103
00:03:55,10 --> 00:03:58,00
instead of mathematical expressions is very nice.

104
00:03:58,00 --> 00:03:59,50
My general guidance is, don't be shy

105
00:03:59,50 --> 00:04:01,30
about creating lots of functions.

106
00:04:01,30 --> 00:04:02,70
There are some programmers who have

107
00:04:02,70 --> 00:04:04,50
nearly every single line of code

108
00:04:04,50 --> 00:04:06,10
they write wrapped in a function.

109
00:04:06,10 --> 00:04:08,50
How deep into it you go is like so many things,

110
00:04:08,50 --> 00:04:10,40
personal preference.

111
00:04:10,40 --> 00:04:11,90
Now we've seen two ways that functions

112
00:04:11,90 --> 00:04:13,30
can come in immediately handy when

113
00:04:13,30 --> 00:04:14,90
you're working with your Java Script.

114
00:04:14,90 --> 00:04:16,50
One is to process data and return

115
00:04:16,50 --> 00:04:18,30
back the modified version and the other

116
00:04:18,30 --> 00:04:20,40
is package up a way to ask a question and

117
00:04:20,40 --> 00:04:23,00
get back an answer to it over and over again.

