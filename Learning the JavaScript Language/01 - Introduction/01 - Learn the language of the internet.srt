1
00:00:00,50 --> 00:00:02,10
- [Joe] Hi I'm Joe Chellman

2
00:00:02,10 --> 00:00:04,90
and welcome to Learning the JavaScript Language.

3
00:00:04,90 --> 00:00:07,40
I've been using JavaScript for almost 20 years now.

4
00:00:07,40 --> 00:00:10,60
I not only teach, I use it daily to create websites

5
00:00:10,60 --> 00:00:13,40
and other goodies for my clients and myself.

6
00:00:13,40 --> 00:00:16,60
As with any language, whether it's for computers or humans,

7
00:00:16,60 --> 00:00:18,90
a key to really grasping the power behind JavaScript

8
00:00:18,90 --> 00:00:20,80
is understanding its syntax.

9
00:00:20,80 --> 00:00:22,50
Luckily it's not very complicated

10
00:00:22,50 --> 00:00:25,30
but still has a lot of expressive power.

11
00:00:25,30 --> 00:00:26,80
This course will walk you through the basics

12
00:00:26,80 --> 00:00:28,90
of JavaScript syntax with the goal

13
00:00:28,90 --> 00:00:30,90
of becoming conversant in the language.

14
00:00:30,90 --> 00:00:33,00
We'll start with variables, learning how to declare

15
00:00:33,00 --> 00:00:35,20
and assign them, and then running through the main types

16
00:00:35,20 --> 00:00:38,30
of data JavaScript uses and how we can use them.

17
00:00:38,30 --> 00:00:40,50
We'll then move on to exploring operators,

18
00:00:40,50 --> 00:00:43,40
both arithmetic and logical

19
00:00:43,40 --> 00:00:45,40
and seeing how conditionals work

20
00:00:45,40 --> 00:00:47,90
and getting a solid feel for how to use loops.

21
00:00:47,90 --> 00:00:50,20
Finally, we'll dive into understanding functions

22
00:00:50,20 --> 00:00:52,30
and how they not only fit into JavaScript

23
00:00:52,30 --> 00:00:53,60
but how they're a little different

24
00:00:53,60 --> 00:00:56,80
from functions in other popular programming languages.

25
00:00:56,80 --> 00:00:58,50
Once you finish this course you'll be off

26
00:00:58,50 --> 00:01:00,50
to a good start understanding JavaScript

27
00:01:00,50 --> 00:01:02,30
and be able to read a lot of what's out there

28
00:01:02,30 --> 00:01:04,00
as well as begin to write your own

29
00:01:04,00 --> 00:01:06,70
to make websites or apps, write scripts for node

30
00:01:06,70 --> 00:01:08,90
or anything where JavaScript is available.

31
00:01:08,90 --> 00:01:10,00
Let's get started.

