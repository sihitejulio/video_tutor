1
00:00:00,50 --> 00:00:02,10
- [Instructor] The exercise files for this course

2
00:00:02,10 --> 00:00:05,30
include everything you'll need to follow along.

3
00:00:05,30 --> 00:00:06,80
Because this is a course about learning how

4
00:00:06,80 --> 00:00:09,20
to read JavaScript as well as write it

5
00:00:09,20 --> 00:00:11,90
the exercise files are mostly in the form of transcripts

6
00:00:11,90 --> 00:00:14,00
of what you'll see, as well as some examples

7
00:00:14,00 --> 00:00:15,60
for copying and pasting.

8
00:00:15,60 --> 00:00:18,20
There are folders for each chapter

9
00:00:18,20 --> 00:00:20,70
and each video containing anything you'll need

10
00:00:20,70 --> 00:00:22,60
to try what you've seen.

11
00:00:22,60 --> 00:00:24,30
You can open these and read through them

12
00:00:24,30 --> 00:00:26,00
at your own pace.

