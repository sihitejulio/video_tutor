1
00:00:00,50 --> 00:00:01,30
- [Narrator] Here's what you should know

2
00:00:01,30 --> 00:00:03,70
to use this course successfully.

3
00:00:03,70 --> 00:00:05,30
Basic computer literacy.

4
00:00:05,30 --> 00:00:07,50
This course doesn't really have a lot of prerequisites.

5
00:00:07,50 --> 00:00:09,40
We'll be coming at this from a web perspective

6
00:00:09,40 --> 00:00:10,30
for the most part,

7
00:00:10,30 --> 00:00:12,30
so mainly I assume that you've used the internet

8
00:00:12,30 --> 00:00:14,40
for a while and that you have a pretty solid grasp

9
00:00:14,40 --> 00:00:16,30
of how a web browser works.

10
00:00:16,30 --> 00:00:18,00
There will be a couple of very small mentions

11
00:00:18,00 --> 00:00:21,20
of HTML but prior experience with it is not necessary.

12
00:00:21,20 --> 00:00:24,00
Beyond that, you don't need much.

