1
00:00:00,50 --> 00:00:02,10
- [Instructor] Programming languages are categorized

2
00:00:02,10 --> 00:00:05,00
in lots of ways, one of which is whether they're strongly

3
00:00:05,00 --> 00:00:06,70
or loosely typed.

4
00:00:06,70 --> 00:00:08,00
JavaScript is loosely typed

5
00:00:08,00 --> 00:00:09,30
and I want to give you a little information

6
00:00:09,30 --> 00:00:13,50
on what that means and why, at some point, you might care.

7
00:00:13,50 --> 00:00:15,50
I'm bringing up loose versus strong typing

8
00:00:15,50 --> 00:00:17,40
because it comes up with variables.

9
00:00:17,40 --> 00:00:20,10
In JavaScript you've seen that we can create a variable,

10
00:00:20,10 --> 00:00:22,30
assign it a number, and then turn right around

11
00:00:22,30 --> 00:00:25,30
and assign it to a word or a string later.

12
00:00:25,30 --> 00:00:27,70
Being able to reuse variables however you want

13
00:00:27,70 --> 00:00:29,70
makes JavaScript very flexible.

14
00:00:29,70 --> 00:00:32,00
Very experienced programmers can have strong opinions

15
00:00:32,00 --> 00:00:34,30
on whether this is good, but either way JavaScript

16
00:00:34,30 --> 00:00:37,00
is a loosely typed language.

17
00:00:37,00 --> 00:00:39,50
In contrast, with a strongly typed language

18
00:00:39,50 --> 00:00:41,50
your variables know about and enforce

19
00:00:41,50 --> 00:00:44,30
what kind of data they can store, broadly speaking.

20
00:00:44,30 --> 00:00:46,10
The language will trigger some sort of error

21
00:00:46,10 --> 00:00:47,70
if you try to say put a number

22
00:00:47,70 --> 00:00:49,60
where you originally had a string.

23
00:00:49,60 --> 00:00:52,80
This can feel constraining, but it can also help avoid bugs,

24
00:00:52,80 --> 00:00:56,50
especially in larger programs and applications.

25
00:00:56,50 --> 00:00:58,10
Say you have a variable called authorName

26
00:00:58,10 --> 00:00:59,50
that will be used later in some way

27
00:00:59,50 --> 00:01:02,20
that makes sense for a person's name.

28
00:01:02,20 --> 00:01:04,60
If you later assign a number, your program might

29
00:01:04,60 --> 00:01:06,70
do something weird later because you fed it a number

30
00:01:06,70 --> 00:01:08,10
instead of string.

31
00:01:08,10 --> 00:01:09,90
A strongly typed language tries to help you

32
00:01:09,90 --> 00:01:12,10
avoid these problems.

33
00:01:12,10 --> 00:01:13,70
One of the most popular solutions

34
00:01:13,70 --> 00:01:15,10
to this issue is TypeScript,

35
00:01:15,10 --> 00:01:17,70
which is JavaScript but with some extra features added on,

36
00:01:17,70 --> 00:01:19,30
like stronger typing.

37
00:01:19,30 --> 00:01:21,40
Many large JavaScript projects and frameworks

38
00:01:21,40 --> 00:01:23,40
write their original code in TypeScript

39
00:01:23,40 --> 00:01:25,00
and transform it into JavaScript

40
00:01:25,00 --> 00:01:26,60
for running on the web.

41
00:01:26,60 --> 00:01:28,90
So you may see this discussed or become curious about it

42
00:01:28,90 --> 00:01:31,00
at some point and I wanted you to know about it.

