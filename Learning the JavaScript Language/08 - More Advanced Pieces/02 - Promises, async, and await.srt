1
00:00:00,50 --> 00:00:01,50
- [Instructor] In this video we're going to look at

2
00:00:01,50 --> 00:00:03,00
what various approaches to writing

3
00:00:03,00 --> 00:00:04,80
asynchronous code look like.

4
00:00:04,80 --> 00:00:07,20
So you will recognize the approaches in the wild.

5
00:00:07,20 --> 00:00:09,30
We'll look at using call backs, promises,

6
00:00:09,30 --> 00:00:12,10
and the new A-sync and await key words.

7
00:00:12,10 --> 00:00:13,50
So here's some example code

8
00:00:13,50 --> 00:00:15,60
that shows these three approaches.

9
00:00:15,60 --> 00:00:18,10
The first one here uses the get method from the jQuery

10
00:00:18,10 --> 00:00:19,20
job ascript library.

11
00:00:19,20 --> 00:00:22,50
Which a function that takes the url as it's first perimeter,

12
00:00:22,50 --> 00:00:25,30
and a call back function as it's second.

13
00:00:25,30 --> 00:00:27,00
That call back gets the response from the server

14
00:00:27,00 --> 00:00:30,00
in it's argument here, which you can then act on

15
00:00:30,00 --> 00:00:32,80
in the body of this call back function.

16
00:00:32,80 --> 00:00:35,90
These urls are to httpbin.

17
00:00:35,90 --> 00:00:38,40
A real website that provides an API for testing just

18
00:00:38,40 --> 00:00:40,40
this sort of code.

19
00:00:40,40 --> 00:00:42,20
This works fine for a single call,

20
00:00:42,20 --> 00:00:44,70
but if there's another request that depends on the first one

21
00:00:44,70 --> 00:00:47,00
and maybe more after that.

22
00:00:47,00 --> 00:00:49,00
You get something that looks like this.

23
00:00:49,00 --> 00:00:50,90
Where you end up having to nest all these call back

24
00:00:50,90 --> 00:00:52,20
functions within one another.

25
00:00:52,20 --> 00:00:54,20
And it gets to be a bit of a mess to write.

26
00:00:54,20 --> 00:00:56,20
And it can get hard to understand if these functions

27
00:00:56,20 --> 00:00:57,70
get at all long.

28
00:00:57,70 --> 00:01:01,00
Programmers call this kind of thing, call back hell.

29
00:01:01,00 --> 00:01:02,30
Scrolling down.

30
00:01:02,30 --> 00:01:04,50
We have some examples of promises.

31
00:01:04,50 --> 00:01:07,40
Promises are a much more pleasant way to work.

32
00:01:07,40 --> 00:01:09,40
Promises are object that capture the result of

33
00:01:09,40 --> 00:01:11,80
an asynchronous action, with a particular programing

34
00:01:11,80 --> 00:01:15,00
interface or API, for handing when the promised data

35
00:01:15,00 --> 00:01:18,40
successfully came through or failed for some reason.

36
00:01:18,40 --> 00:01:21,20
Axios is a popular library for making network requests

37
00:01:21,20 --> 00:01:22,50
and returning promises.

38
00:01:22,50 --> 00:01:24,40
And it looks like this.

39
00:01:24,40 --> 00:01:26,20
This looks a lot like the jQuery version,

40
00:01:26,20 --> 00:01:27,70
in that there's a get method,

41
00:01:27,70 --> 00:01:29,90
but the way it's handled is different.

42
00:01:29,90 --> 00:01:32,10
I make the request and then instead of a call back

43
00:01:32,10 --> 00:01:33,70
pasted in as a second argument,

44
00:01:33,70 --> 00:01:36,00
I directly chain on a method called then.

45
00:01:36,00 --> 00:01:38,00
Which is part of the promise specification,

46
00:01:38,00 --> 00:01:42,00
and that contains the call back where my data comes back.

47
00:01:42,00 --> 00:01:44,10
This works because axios' get method,

48
00:01:44,10 --> 00:01:47,80
and pretty much all the methods in axios return a promise.

49
00:01:47,80 --> 00:01:51,60
And promises have a method called then available.

50
00:01:51,60 --> 00:01:53,90
If once I'm inside that then function,

51
00:01:53,90 --> 00:01:55,60
I need to make another request,

52
00:01:55,60 --> 00:01:57,70
I just return another promise like this.

53
00:01:57,70 --> 00:02:00,10
Allowing me to chain on another then method

54
00:02:00,10 --> 00:02:03,70
at the same level as the first one.

55
00:02:03,70 --> 00:02:05,60
I know this might sound like a lot right now,

56
00:02:05,60 --> 00:02:07,90
but the point is that this is how most of the better code,

57
00:02:07,90 --> 00:02:09,70
involving network requests is written.

58
00:02:09,70 --> 00:02:12,00
And I want you to be able to recognize it when you see it.

59
00:02:12,00 --> 00:02:14,60
This whole business of then followed by a return,

60
00:02:14,60 --> 00:02:18,10
then followed by a return, and so on.

61
00:02:18,10 --> 00:02:20,20
Browser support for promises isn't perfect.

62
00:02:20,20 --> 00:02:22,20
But there are job ascript libraries we can add on

63
00:02:22,20 --> 00:02:25,30
to out projects to poly fill or patch in support for.

64
00:02:25,30 --> 00:02:27,40
The parts of the promise spec that your older browser

65
00:02:27,40 --> 00:02:31,50
of interest doesn't support out of the box.

66
00:02:31,50 --> 00:02:35,40
And then finally, there's async and await,

67
00:02:35,40 --> 00:02:39,20
which are part of the ECMAScript 2017 specification.

68
00:02:39,20 --> 00:02:41,90
These make working with promises even easier to read.

69
00:02:41,90 --> 00:02:45,50
Functions are marked as asynchronous with the async keyword.

70
00:02:45,50 --> 00:02:48,20
And then network requests can have their return values

71
00:02:48,20 --> 00:02:50,30
stored directly, instead of being handled

72
00:02:50,30 --> 00:02:51,50
inside a call back function,

73
00:02:51,50 --> 00:02:55,50
using await before that promise.

74
00:02:55,50 --> 00:02:57,20
This can really shorten code that makes

75
00:02:57,20 --> 00:02:58,50
a lot of network requests.

76
00:02:58,50 --> 00:03:01,20
Like this get lots of things function right here.

77
00:03:01,20 --> 00:03:03,30
Instead of having anything nested or chained,

78
00:03:03,30 --> 00:03:05,40
I just mark each promise with an await

79
00:03:05,40 --> 00:03:08,50
so I can just get the response as soon as they're ready.

80
00:03:08,50 --> 00:03:10,00
This is all relatively new,

81
00:03:10,00 --> 00:03:11,80
and many developers are still coming to grips

82
00:03:11,80 --> 00:03:12,70
with how it works.

83
00:03:12,70 --> 00:03:15,10
But many browsers and the current version of Node,

84
00:03:15,10 --> 00:03:16,20
all support this.

85
00:03:16,20 --> 00:03:18,20
So it's worth being aware of.

86
00:03:18,20 --> 00:03:20,90
So now you've seen some examples of asynchronous java script

87
00:03:20,90 --> 00:03:23,90
in the form of old school call backs, regular promises,

88
00:03:23,90 --> 00:03:26,00
and the new async and await

