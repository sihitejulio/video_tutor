1
00:00:00,50 --> 00:00:01,90
- [Instructor] In this video we're going to talk briefly

2
00:00:01,90 --> 00:00:03,90
about object-oriented JavaScript,

3
00:00:03,90 --> 00:00:06,10
because the term object-oriented appears often

4
00:00:06,10 --> 00:00:08,80
in programming so it's good to have a bit of exposure to it

5
00:00:08,80 --> 00:00:11,40
and what it means for JavaScript.

6
00:00:11,40 --> 00:00:13,30
We've seen JavaScript objects already.

7
00:00:13,30 --> 00:00:15,20
They're a generic way of bundling up data

8
00:00:15,20 --> 00:00:16,50
that belong together.

9
00:00:16,50 --> 00:00:18,70
When we talk about object-oriented programming

10
00:00:18,70 --> 00:00:20,20
we're talking about the same thing

11
00:00:20,20 --> 00:00:22,30
but particularly at a conceptual level.

12
00:00:22,30 --> 00:00:24,60
Being able to reuse code by setting a template

13
00:00:24,60 --> 00:00:27,20
for a type of data and then every time I need to use it

14
00:00:27,20 --> 00:00:29,80
getting an instance of that template.

15
00:00:29,80 --> 00:00:32,10
Different languages handle this concept in different ways

16
00:00:32,10 --> 00:00:36,00
especially in the way objects can be related to on another.

17
00:00:36,00 --> 00:00:37,90
Say I want to make a cake template,

18
00:00:37,90 --> 00:00:40,90
which has some features like the number of layers,

19
00:00:40,90 --> 00:00:43,70
the primary flavor, like chocolate, vanilla or lemon,

20
00:00:43,70 --> 00:00:47,00
and some functions like bake and cool,

21
00:00:47,00 --> 00:00:49,80
all things that any cake is going to need.

22
00:00:49,80 --> 00:00:51,70
Any time I want to make a cake programmatically

23
00:00:51,70 --> 00:00:53,60
I could just get an instance of my cake object

24
00:00:53,60 --> 00:00:55,00
and be off to a good start,

25
00:00:55,00 --> 00:00:56,60
just writing the code for whatever features

26
00:00:56,60 --> 00:00:58,90
are unique to that instance.

27
00:00:58,90 --> 00:01:02,30
If I'm going to make a lot of say chocolate cakes

28
00:01:02,30 --> 00:01:04,30
I might want an object called ChocolateCake

29
00:01:04,30 --> 00:01:07,00
that inherits the shared features of the base cake

30
00:01:07,00 --> 00:01:09,30
but has its own features that are more specific

31
00:01:09,30 --> 00:01:11,60
than the original cake, but still general enough

32
00:01:11,60 --> 00:01:14,50
to be reusable among all chocolate cakes,

33
00:01:14,50 --> 00:01:17,20
like making its primary flavor chocolate by default

34
00:01:17,20 --> 00:01:19,40
so I don't have to set it every time.

35
00:01:19,40 --> 00:01:22,00
This relationship between chocolate cake and cake

36
00:01:22,00 --> 00:01:24,60
is called inheritance and the way inheritance works

37
00:01:24,60 --> 00:01:28,10
is central to any object-oriented language.

38
00:01:28,10 --> 00:01:30,80
So the question is, what mechanism is used

39
00:01:30,80 --> 00:01:33,70
to relate these objects?

40
00:01:33,70 --> 00:01:35,40
The way JavaScript works is different

41
00:01:35,40 --> 00:01:37,50
from most other popular languages.

42
00:01:37,50 --> 00:01:40,60
It uses a model called prototypal inheritance

43
00:01:40,60 --> 00:01:44,00
where every object has a link to a parent object

44
00:01:44,00 --> 00:01:46,60
from which it inherited some data and functions

45
00:01:46,60 --> 00:01:49,00
and that parent has the same link

46
00:01:49,00 --> 00:01:52,40
to its parent, and so on.

47
00:01:52,40 --> 00:01:55,50
These links are called double underscore proto internally

48
00:01:55,50 --> 00:01:59,00
and comprise what's called the prototype chain.

49
00:01:59,00 --> 00:02:00,40
This topic is one that's worth looking into

50
00:02:00,40 --> 00:02:03,00
in more detail as you become a more proficient developer,

51
00:02:03,00 --> 00:02:05,10
but in terms of our goal of making you conversant

52
00:02:05,10 --> 00:02:07,40
and recognizing common code idioms

53
00:02:07,40 --> 00:02:09,90
I want you to recognize something like this.

54
00:02:09,90 --> 00:02:11,80
This is how you add a method to an object

55
00:02:11,80 --> 00:02:14,00
using its prototype, which makes it available

56
00:02:14,00 --> 00:02:16,30
to all objects that use that prototype

57
00:02:16,30 --> 00:02:19,60
wherever they are down the prototype chain.

58
00:02:19,60 --> 00:02:21,50
Newer versions of JavaScript add some keywords

59
00:02:21,50 --> 00:02:23,10
that make JavaScript's prototype stuff

60
00:02:23,10 --> 00:02:25,30
look more like what people coming from other languages

61
00:02:25,30 --> 00:02:27,50
are used to, like the class keyword.

62
00:02:27,50 --> 00:02:28,80
But ultimately it all gets turned

63
00:02:28,80 --> 00:02:30,40
into prototypes eventually.

64
00:02:30,40 --> 00:02:31,70
Because these are enhancements

65
00:02:31,70 --> 00:02:33,00
to the way the language looks

66
00:02:33,00 --> 00:02:35,10
but don't really change how it works underneath

67
00:02:35,10 --> 00:02:37,90
these kinds of additions are called syntactic sugar.

68
00:02:37,90 --> 00:02:39,00
There's a lot more to learn about

69
00:02:39,00 --> 00:02:41,10
object-oriented JavaScript if you're interested,

70
00:02:41,10 --> 00:02:43,60
but this quick overview gave you a few things to look for

71
00:02:43,60 --> 00:02:46,00
as you're developing familiarity with the language.

