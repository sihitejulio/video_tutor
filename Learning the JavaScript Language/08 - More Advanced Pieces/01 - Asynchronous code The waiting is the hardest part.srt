1
00:00:00,50 --> 00:00:01,60
- [Instructor] In the next several videos

2
00:00:01,60 --> 00:00:03,40
we're going to talk about some pieces of JavaScript

3
00:00:03,40 --> 00:00:05,40
that are more on the advanced side

4
00:00:05,40 --> 00:00:06,50
but that I think are important enough

5
00:00:06,50 --> 00:00:08,10
to introduce conceptually.

6
00:00:08,10 --> 00:00:11,80
In this video we're going to talk about the term asynchronous.

7
00:00:11,80 --> 00:00:13,60
When we think about giving instructions to a computer

8
00:00:13,60 --> 00:00:15,50
we generally think linearly,

9
00:00:15,50 --> 00:00:19,00
here's step one followed by step two and so forth.

10
00:00:19,00 --> 00:00:23,40
We don't move on to step two until step one is finished.

11
00:00:23,40 --> 00:00:25,70
This is easy to follow conceptually and a lot of code

12
00:00:25,70 --> 00:00:27,30
that we write follows this mode of thinking

13
00:00:27,30 --> 00:00:28,90
which is all fine and good.

14
00:00:28,90 --> 00:00:31,00
Code written this way is said to be synchronous

15
00:00:31,00 --> 00:00:33,70
with every step blocking the ones that come after it.

16
00:00:33,70 --> 00:00:37,20
Step three doesn't run until step two is totally finished

17
00:00:37,20 --> 00:00:39,10
and so on.

18
00:00:39,10 --> 00:00:42,40
But what if say step four is a step that takes awhile

19
00:00:42,40 --> 00:00:43,60
and involves waiting for something

20
00:00:43,60 --> 00:00:46,70
that steps five and after don't need right away?

21
00:00:46,70 --> 00:00:49,10
In order for the entire task to be complete

22
00:00:49,10 --> 00:00:50,90
step four does need to be finished

23
00:00:50,90 --> 00:00:52,10
but it doesn't need to be finished

24
00:00:52,10 --> 00:00:55,40
before step five can start.

25
00:00:55,40 --> 00:00:57,30
If we're making a cake for example

26
00:00:57,30 --> 00:01:00,20
we need to get the cake in the oven so it can bake

27
00:01:00,20 --> 00:01:02,40
but we don't have to wait for the cake to be finished

28
00:01:02,40 --> 00:01:05,60
before we can make the frosting.

29
00:01:05,60 --> 00:01:07,60
We need the cake and the frosting to be finished

30
00:01:07,60 --> 00:01:09,60
and cooled before we can put them together

31
00:01:09,60 --> 00:01:12,00
and call the whole job done.

32
00:01:12,00 --> 00:01:14,50
But if we waited for the cake to be baked and cooled

33
00:01:14,50 --> 00:01:16,10
before we even started the frosting

34
00:01:16,10 --> 00:01:18,00
everything would just take a lot longer.

35
00:01:18,00 --> 00:01:20,70
Code involving steps that can run at the same time

36
00:01:20,70 --> 00:01:23,00
with the results of some steps coming in later

37
00:01:23,00 --> 00:01:25,40
is called asynchronous because some steps

38
00:01:25,40 --> 00:01:28,10
can be run in a way that doesn't block the others.

39
00:01:28,10 --> 00:01:30,30
Eventually the results will all be needed

40
00:01:30,30 --> 00:01:32,80
just not immediately, so we can let other steps happen

41
00:01:32,80 --> 00:01:36,20
while waiting for the stuff that takes awhile.

42
00:01:36,20 --> 00:01:37,50
In JavaScript the main area

43
00:01:37,50 --> 00:01:39,30
where asynchronous code comes up

44
00:01:39,30 --> 00:01:41,20
is when we need to load data from somewhere,

45
00:01:41,20 --> 00:01:43,30
usually somewhere on the internet.

46
00:01:43,30 --> 00:01:44,80
This could be loading some inventory data

47
00:01:44,80 --> 00:01:48,20
from a store's back office, a photo from the Instagram API,

48
00:01:48,20 --> 00:01:51,40
anything where our code needs to make a request.

49
00:01:51,40 --> 00:01:54,00
If you're using node loading files from disk

50
00:01:54,00 --> 00:01:55,70
is an asynchronous operation by default

51
00:01:55,70 --> 00:01:57,40
because disks can be slow.

52
00:01:57,40 --> 00:01:59,70
We always assume that the network or other sources

53
00:01:59,70 --> 00:02:01,50
of data can be slow sometimes

54
00:02:01,50 --> 00:02:03,30
and if all the code on a page had to wait

55
00:02:03,30 --> 00:02:05,70
for that data to load, everything would feel slow

56
00:02:05,70 --> 00:02:08,50
and unresponsive, which is bad.

57
00:02:08,50 --> 00:02:09,80
As you start writing intermediate

58
00:02:09,80 --> 00:02:12,10
to advanced JavaScript code you will almost certainly

59
00:02:12,10 --> 00:02:15,30
need to deal with various ways of writing asynchronous code.

60
00:02:15,30 --> 00:02:18,00
In the next video we'll look at some examples.

