1
00:00:00,50 --> 00:00:02,00
- [Narrator] In this video we're going to talk about some

2
00:00:02,00 --> 00:00:04,30
of the popular tools in the JavaScript ecosystem

3
00:00:04,30 --> 00:00:06,40
that help with various tasks.

4
00:00:06,40 --> 00:00:07,40
When you're just starting out,

5
00:00:07,40 --> 00:00:09,20
you don't need any of this stuff.

6
00:00:09,20 --> 00:00:11,00
But if you want to get into JavaScript professionally

7
00:00:11,00 --> 00:00:13,40
at some point, I want you to know about these tools

8
00:00:13,40 --> 00:00:17,00
so you can start exploring them in more detail.

9
00:00:17,00 --> 00:00:20,00
JavaScript powers so many things on the Web and elsewhere.

10
00:00:20,00 --> 00:00:21,90
Some projects are enormous, involving millions

11
00:00:21,90 --> 00:00:23,80
of lines of code and they're handled by teams

12
00:00:23,80 --> 00:00:25,50
of dozens or hundreds of people.

13
00:00:25,50 --> 00:00:27,70
Open source JavaScript projects can have contributors

14
00:00:27,70 --> 00:00:31,00
of many experience levels all over the world.

15
00:00:31,00 --> 00:00:32,10
With the continual maturation

16
00:00:32,10 --> 00:00:33,80
and proliferation of Javascript,

17
00:00:33,80 --> 00:00:35,90
it was inevitable that developers would start writing tools

18
00:00:35,90 --> 00:00:38,70
to help solve various issues that come up.

19
00:00:38,70 --> 00:00:41,50
One issue is how to handle JavaScript libraries and modules.

20
00:00:41,50 --> 00:00:44,00
As projects grow, it makes sense to start splitting files up

21
00:00:44,00 --> 00:00:46,30
into smaller ones that contain related code.

22
00:00:46,30 --> 00:00:48,20
In an object-oriented project there could be files

23
00:00:48,20 --> 00:00:50,90
for each type or class of object or just a file containing

24
00:00:50,90 --> 00:00:52,80
all the helper functions you've written.

25
00:00:52,80 --> 00:00:53,70
Once you've broken them up,

26
00:00:53,70 --> 00:00:56,50
you need a way to load the ones you need.

27
00:00:56,50 --> 00:00:58,00
Browsers load script with script tags,

28
00:00:58,00 --> 00:01:00,20
but managing those manually can be a pain.

29
00:01:00,20 --> 00:01:01,40
What if script A doesn't work

30
00:01:01,40 --> 00:01:04,40
without script B having been loaded first?

31
00:01:04,40 --> 00:01:05,30
Oh and, by the way,

32
00:01:05,30 --> 00:01:07,80
what if script B needs Script N before it?

33
00:01:07,80 --> 00:01:09,50
And so on, and so on.

34
00:01:09,50 --> 00:01:11,80
Until recently there was no official specification

35
00:01:11,80 --> 00:01:14,90
for how to load shared JavaScript libraries.

36
00:01:14,90 --> 00:01:18,30
That specification exists now, it's called dynamic imports.

37
00:01:18,30 --> 00:01:20,10
But browser support isn't very good right now,

38
00:01:20,10 --> 00:01:21,90
and the problem has been around much longer than

39
00:01:21,90 --> 00:01:23,60
the specification has existed,

40
00:01:23,60 --> 00:01:27,10
so tools have been written to handle this.

41
00:01:27,10 --> 00:01:31,40
The most popular ones are probably Webpack and Rollup.

42
00:01:31,40 --> 00:01:32,90
Module loading is just a small part

43
00:01:32,90 --> 00:01:34,30
of what these tools can do.

44
00:01:34,30 --> 00:01:36,50
Both have plug-ins that can handle lots of other tasks

45
00:01:36,50 --> 00:01:37,80
like minifying Javascript,

46
00:01:37,80 --> 00:01:40,20
which removes comments and white space for faster downloads

47
00:01:40,20 --> 00:01:42,20
and tons and tons more.

48
00:01:42,20 --> 00:01:43,70
There are videos and courses available

49
00:01:43,70 --> 00:01:45,80
on both of these tools.

50
00:01:45,80 --> 00:01:48,10
There are also tools called package managers

51
00:01:48,10 --> 00:01:50,70
for managing the installation of JavaScript libraries.

52
00:01:50,70 --> 00:01:52,20
If that doesn't sound important to you,

53
00:01:52,20 --> 00:01:53,60
think about a simple example.

54
00:01:53,60 --> 00:01:55,90
Installing a jQuery slideshow plug-in.

55
00:01:55,90 --> 00:01:59,40
If you don't already have jQuery, you'll need that.

56
00:01:59,40 --> 00:02:01,10
And then the slideshow plug-in,

57
00:02:01,10 --> 00:02:02,00
and then maybe you need a plug-in

58
00:02:02,00 --> 00:02:04,20
to support touch input for mobile devices.

59
00:02:04,20 --> 00:02:05,90
For something small, there's still a lot

60
00:02:05,90 --> 00:02:08,30
to download and deal with.

61
00:02:08,30 --> 00:02:13,20
To help with this, there exists NPM and Yarn,

62
00:02:13,20 --> 00:02:15,70
which handle downloading JavaScript libraries and packages

63
00:02:15,70 --> 00:02:16,80
making sure to install everything

64
00:02:16,80 --> 00:02:19,40
you need in a standard folder structure.

65
00:02:19,40 --> 00:02:22,10
Webpack and Rollup are aware of those standards as well,

66
00:02:22,10 --> 00:02:24,10
making dynamic importing of files installed

67
00:02:24,10 --> 00:02:25,40
this way very easy.

68
00:02:25,40 --> 00:02:27,10
Learning NPM, the Note Package Manager,

69
00:02:27,10 --> 00:02:29,30
and learning package management with Yarn,

70
00:02:29,30 --> 00:02:32,40
will tell you a lot more about both of these.

71
00:02:32,40 --> 00:02:34,40
One other kind of tool that's used all the time

72
00:02:34,40 --> 00:02:36,80
is a transfiler or a compiler.

73
00:02:36,80 --> 00:02:38,50
If you want to use ECMAScript 2015

74
00:02:38,50 --> 00:02:40,00
or newer in your projects,

75
00:02:40,00 --> 00:02:41,80
or you just want to use TypeScript,

76
00:02:41,80 --> 00:02:43,70
you'll need one of these to convert that code

77
00:02:43,70 --> 00:02:45,70
into the kind of compatible JavaScript

78
00:02:45,70 --> 00:02:47,80
that will run in any browser.

79
00:02:47,80 --> 00:02:49,90
Babble is a transfiler that's wildly popular

80
00:02:49,90 --> 00:02:52,60
for converting ES6 and other modern Javascript script specs

81
00:02:52,60 --> 00:02:54,80
into ECMAScript 5.

82
00:02:54,80 --> 00:02:56,90
And TypeScript has its own compiler as well

83
00:02:56,90 --> 00:02:59,20
to convert into vanilla JavaScript.

84
00:02:59,20 --> 00:03:01,60
Either of these can be integrated with Webpack or Rollup,

85
00:03:01,60 --> 00:03:03,30
giving you a one-stop shop

86
00:03:03,30 --> 00:03:05,10
for building your finalized JavaScript

87
00:03:05,10 --> 00:03:07,50
when working on a larger project.

88
00:03:07,50 --> 00:03:08,90
As I said earlier you don't have

89
00:03:08,90 --> 00:03:11,10
to use any of these tools at first.

90
00:03:11,10 --> 00:03:12,80
Depending on how you use JavaScript,

91
00:03:12,80 --> 00:03:14,60
you might not ever need any of them.

92
00:03:14,60 --> 00:03:16,20
But if you work with the language for awhile,

93
00:03:16,20 --> 00:03:18,70
you might start to notice some friction in your work flow,

94
00:03:18,70 --> 00:03:21,00
and tools like this might be able to help.

