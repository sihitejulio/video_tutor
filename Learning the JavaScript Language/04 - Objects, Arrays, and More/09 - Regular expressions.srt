1
00:00:00,20 --> 00:00:01,90
- [Instructor] In this video, we're going to take a look

2
00:00:01,90 --> 00:00:04,20
at regular expressions which are a way

3
00:00:04,20 --> 00:00:06,60
of searching text for patterns.

4
00:00:06,60 --> 00:00:08,40
When you're working with text in JavaScript

5
00:00:08,40 --> 00:00:10,70
working with strings but also if you're working

6
00:00:10,70 --> 00:00:12,30
with long runs of text in pros

7
00:00:12,30 --> 00:00:14,30
or pretty much in any situation,

8
00:00:14,30 --> 00:00:15,50
there may be times when you want to find

9
00:00:15,50 --> 00:00:19,70
a particular something but it may not be an exact something.

10
00:00:19,70 --> 00:00:22,20
Maybe you can describe a pattern that you should follow,

11
00:00:22,20 --> 00:00:24,20
two digits followed by any number of letters

12
00:00:24,20 --> 00:00:27,50
or anywhere that starts with A and ends with S.

13
00:00:27,50 --> 00:00:29,70
A regular expression is a way of describing something

14
00:00:29,70 --> 00:00:32,30
you're searching for in an abstract way.

15
00:00:32,30 --> 00:00:35,60
In JavaScript, a regular expression is another data type

16
00:00:35,60 --> 00:00:40,10
just like numbers or strings and it looks like this.

17
00:00:40,10 --> 00:00:42,20
This is a regular expression literal.

18
00:00:42,20 --> 00:00:44,80
Now if we just have two slashes, of course,

19
00:00:44,80 --> 00:00:46,30
that's a line comment.

20
00:00:46,30 --> 00:00:47,30
To make it a regular expression,

21
00:00:47,30 --> 00:00:50,40
we put something in between those slashes.

22
00:00:50,40 --> 00:00:52,00
That can be words, it can be numbers,

23
00:00:52,00 --> 00:00:55,60
it can be various combinations and it can even be patterns.

24
00:00:55,60 --> 00:00:57,20
We're just going to look at some simple examples

25
00:00:57,20 --> 00:00:58,60
in this lesson but entire books

26
00:00:58,60 --> 00:01:01,50
have been written on regular expressions,

27
00:01:01,50 --> 00:01:03,10
including this one.

28
00:01:03,10 --> 00:01:05,30
If searching text on a regular basis

29
00:01:05,30 --> 00:01:07,50
is something that you do because you're a programmer

30
00:01:07,50 --> 00:01:10,30
or for whatever reason, it could definitely be worthwhile

31
00:01:10,30 --> 00:01:12,30
to check out a book on regular expressions.

32
00:01:12,30 --> 00:01:14,80
They're tremendously powerful.

33
00:01:14,80 --> 00:01:16,30
Anyway, here's how they work,

34
00:01:16,30 --> 00:01:19,70
but what we're going to do here is take these four strings

35
00:01:19,70 --> 00:01:22,50
which as you can see have different stuff in them

36
00:01:22,50 --> 00:01:24,50
and then we have this regular expression

37
00:01:24,50 --> 00:01:28,60
and a variable called regex and it's just a very simple one

38
00:01:28,60 --> 00:01:32,40
with the word this in between the two slashes.

39
00:01:32,40 --> 00:01:33,20
So what we're going to do

40
00:01:33,20 --> 00:01:34,60
is we're going to take this regular expression

41
00:01:34,60 --> 00:01:39,90
and we're going to execute the test method on it.

42
00:01:39,90 --> 00:01:42,40
Passing in each of these strings separately,

43
00:01:42,40 --> 00:01:44,70
one, two, three, and four.

44
00:01:44,70 --> 00:01:46,20
Then we're going to take the result of that

45
00:01:46,20 --> 00:01:48,50
and print it to the JavaScript console from our code

46
00:01:48,50 --> 00:01:50,50
using the Console.log function

47
00:01:50,50 --> 00:01:52,40
which is built-in to the browser.

48
00:01:52,40 --> 00:01:54,10
It's basically equivalent to just typing out

49
00:01:54,10 --> 00:01:56,80
the same expression right in the JavaScript console.

50
00:01:56,80 --> 00:01:57,60
You can use this function

51
00:01:57,60 --> 00:01:59,80
anytime you're working in your code editor

52
00:01:59,80 --> 00:02:01,50
to get whatever that expression is

53
00:02:01,50 --> 00:02:04,90
that's in these parentheses into the JavaScript console.

54
00:02:04,90 --> 00:02:08,40
It's very basic but it's a very useful debugging tool.

55
00:02:08,40 --> 00:02:10,10
We're going to work directly in the JavaScript console

56
00:02:10,10 --> 00:02:11,40
so this is a little bit redundant

57
00:02:11,40 --> 00:02:12,70
but I wanted you to be aware

58
00:02:12,70 --> 00:02:15,30
of this built-in function for later use.

59
00:02:15,30 --> 00:02:17,10
So what happens when we use this?

60
00:02:17,10 --> 00:02:19,00
Is the test method looks through whatever string

61
00:02:19,00 --> 00:02:21,10
it's passed in for this pattern

62
00:02:21,10 --> 00:02:23,70
which is expressed in the regular expression.

63
00:02:23,70 --> 00:02:26,40
And it reports back true or false whether that pattern

64
00:02:26,40 --> 00:02:28,10
can be found in the string.

65
00:02:28,10 --> 00:02:34,70
So let's copy and run all of this code and see what happens.

66
00:02:34,70 --> 00:02:36,90
Here are the results of my four tests.

67
00:02:36,90 --> 00:02:40,80
The first one came out false, we're searching for this.

68
00:02:40,80 --> 00:02:43,90
The word is in there but not in this specific case,

69
00:02:43,90 --> 00:02:45,40
so that's accurate.

70
00:02:45,40 --> 00:02:47,70
Likewise, the second one came back false, same thing,

71
00:02:47,70 --> 00:02:50,30
this appears but it's all lowercase here

72
00:02:50,30 --> 00:02:52,40
and title case here.

73
00:02:52,40 --> 00:02:57,10
Then here it does appear in lowercase so that returned true

74
00:02:57,10 --> 00:03:00,60
and the same thing for returning false on this fourth one.

75
00:03:00,60 --> 00:03:03,00
This can be useful in a lot of different ways.

76
00:03:03,00 --> 00:03:04,80
You can check for a word or phrase or whatever

77
00:03:04,80 --> 00:03:06,60
in a block of text of any length

78
00:03:06,60 --> 00:03:08,60
and get your results back pretty quickly.

79
00:03:08,60 --> 00:03:10,80
Now there are a lot of modifiers you can add

80
00:03:10,80 --> 00:03:13,30
to these regular expressions to change how they work.

81
00:03:13,30 --> 00:03:15,40
Let's try a few.

82
00:03:15,40 --> 00:03:17,20
So I'm going to clear the console but bring back

83
00:03:17,20 --> 00:03:21,00
my giant block of code by typing the up arrow.

84
00:03:21,00 --> 00:03:23,70
First thing I'm going to do is add an I

85
00:03:23,70 --> 00:03:25,80
onto the end of this regular expression.

86
00:03:25,80 --> 00:03:28,50
You can see that it's still colored in red

87
00:03:28,50 --> 00:03:29,90
just like the rest of the expression

88
00:03:29,90 --> 00:03:31,80
so I know that this is going to work.

89
00:03:31,80 --> 00:03:33,70
This I is called a flag when added

90
00:03:33,70 --> 00:03:35,80
onto the end of a regular expression like this.

91
00:03:35,80 --> 00:03:37,70
I stands for case insensitive.

92
00:03:37,70 --> 00:03:40,30
I'm going to run this again.

93
00:03:40,30 --> 00:03:43,90
Every test comes back true because the word this appears

94
00:03:43,90 --> 00:03:47,60
as long as we don't care about what case it's in.

95
00:03:47,60 --> 00:03:49,90
I'm going to clear this again, bring the whole thing back

96
00:03:49,90 --> 00:03:52,10
by typing the up arrow.

97
00:03:52,10 --> 00:03:53,40
Here are some other basic modifiers

98
00:03:53,40 --> 00:03:55,40
we can add to this expression.

99
00:03:55,40 --> 00:03:57,60
I'm going to leave the case insensitive modifier

100
00:03:57,60 --> 00:04:00,80
but I'm going to add what's called a caret

101
00:04:00,80 --> 00:04:01,80
to the front of this.

102
00:04:01,80 --> 00:04:04,20
That's Shift + 6 on an American keyboard.

103
00:04:04,20 --> 00:04:06,80
Now what this does is check if this appears

104
00:04:06,80 --> 00:04:10,00
at the beginning of the string.

105
00:04:10,00 --> 00:04:12,40
So comparing in a case insensitivity way,

106
00:04:12,40 --> 00:04:15,30
these three all have this at the very beginning,

107
00:04:15,30 --> 00:04:19,20
this one does not, so it goes true, true, false, true.

108
00:04:19,20 --> 00:04:20,70
There's a similar modifier that I can use

109
00:04:20,70 --> 00:04:23,10
to check for whether something appears

110
00:04:23,10 --> 00:04:26,30
at the end of a string so I'll get rid of the caret,

111
00:04:26,30 --> 00:04:28,00
put a dollar sign here at the end

112
00:04:28,00 --> 00:04:29,70
and this will check whether the word this

113
00:04:29,70 --> 00:04:32,50
appears at the end of any of these strings.

114
00:04:32,50 --> 00:04:34,80
Execute this again and they all come back false

115
00:04:34,80 --> 00:04:37,30
because this does not appear at the end.

116
00:04:37,30 --> 00:04:42,50
Clear one more time, bring this whole thing back again.

117
00:04:42,50 --> 00:04:44,20
So I'm going to try something different,

118
00:04:44,20 --> 00:04:49,40
I'm going to check for ever with the dollar sign.

119
00:04:49,40 --> 00:04:51,10
Hit Return.

120
00:04:51,10 --> 00:04:53,50
Now I get true and true for the first two,

121
00:04:53,50 --> 00:04:54,90
and false for the second two

122
00:04:54,90 --> 00:04:57,90
because ever does in fact appear there at the end.

123
00:04:57,90 --> 00:04:59,50
Now there's something a little tricky

124
00:04:59,50 --> 00:05:01,10
about this particular regular expression

125
00:05:01,10 --> 00:05:03,50
because a dot has a special meaning here.

126
00:05:03,50 --> 00:05:04,90
A dot means any character,

127
00:05:04,90 --> 00:05:08,10
so I'm not actually matching whether ever and period

128
00:05:08,10 --> 00:05:10,00
appears in these strings,

129
00:05:10,00 --> 00:05:11,10
I'm checking whether ever

130
00:05:11,10 --> 00:05:14,90
followed by any single character appears here.

131
00:05:14,90 --> 00:05:16,90
If I wanted to check for a literal dot,

132
00:05:16,90 --> 00:05:22,80
I can do that but I need to escape this little dot.

133
00:05:22,80 --> 00:05:26,00
If I run this again, I get the same true, true, false, false

134
00:05:26,00 --> 00:05:28,70
but now I'm really checking for the period

135
00:05:28,70 --> 00:05:31,70
instead of just any old character.

136
00:05:31,70 --> 00:05:34,20
And that is an introduction to regular expressions.

137
00:05:34,20 --> 00:05:36,90
This topic has applications way beyond JavaScript.

138
00:05:36,90 --> 00:05:38,30
I know when I first learned how to use them,

139
00:05:38,30 --> 00:05:40,40
it felt like the text world was my oyster

140
00:05:40,40 --> 00:05:43,00
and I hope they're very helpful to you as well.

