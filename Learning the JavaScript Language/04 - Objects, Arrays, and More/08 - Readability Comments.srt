1
00:00:00,50 --> 00:00:02,80
- [Narrator] In this video, we're going to comments,

2
00:00:02,80 --> 00:00:05,80
which are used for documenting and disabling

3
00:00:05,80 --> 00:00:07,80
parts of your code.

4
00:00:07,80 --> 00:00:08,90
The most important function for

5
00:00:08,90 --> 00:00:11,20
comments probably is documentation.

6
00:00:11,20 --> 00:00:14,00
That is, adding pieces of code that don't execute,

7
00:00:14,00 --> 00:00:15,90
that can be read by other human beings

8
00:00:15,90 --> 00:00:18,20
to understand what's going on in your program.

9
00:00:18,20 --> 00:00:20,10
These come in two styles in JavaScript.

10
00:00:20,10 --> 00:00:21,80
One is the line comment

11
00:00:21,80 --> 00:00:24,80
which starts with two slash characters like this

12
00:00:24,80 --> 00:00:26,90
and then anything after that on the same line

13
00:00:26,90 --> 00:00:28,30
doesn't execute.

14
00:00:28,30 --> 00:00:31,10
These can be at the beginning of a line like this,

15
00:00:31,10 --> 00:00:33,40
or anywhere else.

16
00:00:33,40 --> 00:00:34,80
So if I wanted to document one of

17
00:00:34,80 --> 00:00:37,00
these variable assignments for some reason,

18
00:00:37,00 --> 00:00:38,70
I can put a line comment right here

19
00:00:38,70 --> 00:00:39,70
towards the end of the line

20
00:00:39,70 --> 00:00:41,50
and that works just fine.

21
00:00:41,50 --> 00:00:43,50
This is not really documentation of the year.

22
00:00:43,50 --> 00:00:45,80
It's clear enough from the variable name what it's doing

23
00:00:45,80 --> 00:00:48,40
but this does work, is the point.

24
00:00:48,40 --> 00:00:49,70
The other function of a JavaScript

25
00:00:49,70 --> 00:00:51,90
comment is to disable parts.

26
00:00:51,90 --> 00:00:56,10
so if I want say, this line, the day

27
00:00:56,10 --> 00:00:57,70
not to execute anymore.

28
00:00:57,70 --> 00:00:59,60
I can comment it out.

29
00:00:59,60 --> 00:01:01,80
Let's copy this

30
00:01:01,80 --> 00:01:04,50
and bring it over the browser.

31
00:01:04,50 --> 00:01:06,60
Now if I paste this in,

32
00:01:06,60 --> 00:01:09,10
this line did not execute because the whole thing

33
00:01:09,10 --> 00:01:10,50
is actually a comment.

34
00:01:10,50 --> 00:01:12,60
So now year is defined,

35
00:01:12,60 --> 00:01:14,30
month is defined

36
00:01:14,30 --> 00:01:16,40
and holidays defined

37
00:01:16,40 --> 00:01:19,10
but day is not.

38
00:01:19,10 --> 00:01:21,00
I get a reference error.

39
00:01:21,00 --> 00:01:23,30
So when you're working through your code and fixing things,

40
00:01:23,30 --> 00:01:25,30
being able to comment things out

41
00:01:25,30 --> 00:01:26,80
so they don't execute anymore

42
00:01:26,80 --> 00:01:28,60
can help you isolate what might be wrong.

43
00:01:28,60 --> 00:01:32,00
It's a really handy debugging tool.

44
00:01:32,00 --> 00:01:33,20
There's one more style of comment

45
00:01:33,20 --> 00:01:36,00
you can use in JavaScript called a block comment.

46
00:01:36,00 --> 00:01:38,00
It starts with a slash

47
00:01:38,00 --> 00:01:40,40
followed by an asterisk

48
00:01:40,40 --> 00:01:41,30
and then you can write comments

49
00:01:41,30 --> 00:01:43,60
across multiple lines, like this.

50
00:01:43,60 --> 00:01:46,40
and finally it ends with an asterisk and another slash,

51
00:01:46,40 --> 00:01:48,30
like that.

52
00:01:48,30 --> 00:01:50,00
You'll see these all the time in JavaScript

53
00:01:50,00 --> 00:01:52,50
for inserting long paragraphs or just other elements

54
00:01:52,50 --> 00:01:54,60
where a lot of text is needed in one place

55
00:01:54,60 --> 00:01:56,00
and maybe it would get a little bit tedious

56
00:01:56,00 --> 00:01:59,20
to put double slashes in front of every single line.

57
00:01:59,20 --> 00:02:01,40
Now you can see that these will break across lines

58
00:02:01,40 --> 00:02:02,80
so you can do this to disable many

59
00:02:02,80 --> 00:02:05,20
lines of your code at once.

60
00:02:05,20 --> 00:02:06,60
My Editor makes it really obvious

61
00:02:06,60 --> 00:02:07,70
what's contained in the comment,

62
00:02:07,70 --> 00:02:09,70
thank you syntax highlighting.

63
00:02:09,70 --> 00:02:11,20
Right now this is commented from here

64
00:02:11,20 --> 00:02:15,80
all the way down to the next asterisk slash right here.

65
00:02:15,80 --> 00:02:17,30
Block comments are really useful and

66
00:02:17,30 --> 00:02:19,00
you'll see them all the time.

67
00:02:19,00 --> 00:02:20,20
But you need to be careful with these

68
00:02:20,20 --> 00:02:22,40
because every once in awhile,

69
00:02:22,40 --> 00:02:26,00
an asterisk and a slash will appear as actual valid code

70
00:02:26,00 --> 00:02:27,90
that's not part of a comment

71
00:02:27,90 --> 00:02:30,20
and this is not a problem with line comments.

72
00:02:30,20 --> 00:02:32,40
So if you're commenting out something like

73
00:02:32,40 --> 00:02:34,70
this down here

74
00:02:34,70 --> 00:02:37,00
and I try to comment this out using a block comment,

75
00:02:37,00 --> 00:02:39,40
you can see that it doesn't work

76
00:02:39,40 --> 00:02:41,10
because there's an asterisk and a slash

77
00:02:41,10 --> 00:02:45,70
in the middle of this totally valid line.

78
00:02:45,70 --> 00:02:47,10
For this reason some people recommend

79
00:02:47,10 --> 00:02:49,60
against using block comments at all.

80
00:02:49,60 --> 00:02:51,10
I'm not going to go that far

81
00:02:51,10 --> 00:02:53,60
but it's something to watch out for.

82
00:02:53,60 --> 00:02:56,40
Now let's look at a very large JavaScript file

83
00:02:56,40 --> 00:02:58,40
with a lot of documentation in it.

84
00:02:58,40 --> 00:03:01,60
This is jQuery, a very popular JavaScript library.

85
00:03:01,60 --> 00:03:03,60
This is a version of it that hasn't been optimized

86
00:03:03,60 --> 00:03:05,20
so it's loaded with comments

87
00:03:05,20 --> 00:03:07,70
starting with the big block comment at the top

88
00:03:07,70 --> 00:03:10,80
and then lots and lots of line comments below.

89
00:03:10,80 --> 00:03:12,00
As you scroll down you can see that

90
00:03:12,00 --> 00:03:15,20
nearly every useful block is documented with a comment.

91
00:03:15,20 --> 00:03:16,50
This is very nice for people wanting

92
00:03:16,50 --> 00:03:18,40
to contribute to the project and for developers

93
00:03:18,40 --> 00:03:20,10
wanting to learn how it works.

94
00:03:20,10 --> 00:03:24,20
Of course, jQuery is a very large JavaScript library.

95
00:03:24,20 --> 00:03:25,70
There's a lot going on here.

96
00:03:25,70 --> 00:03:27,10
No amount of comments is going to make it

97
00:03:27,10 --> 00:03:29,20
perfectly clear the first time you read through it.

98
00:03:29,20 --> 00:03:31,30
You still have to do a lot of reading and actually

99
00:03:31,30 --> 00:03:33,40
read the code and understand it

100
00:03:33,40 --> 00:03:36,50
but having these comments in here is a huge huge help.

101
00:03:36,50 --> 00:03:38,40
I really can't recommend highly enough

102
00:03:38,40 --> 00:03:40,40
making sure that even if you're the only person

103
00:03:40,40 --> 00:03:42,50
that you ever expect to read the code,

104
00:03:42,50 --> 00:03:43,80
add lots of comments

105
00:03:43,80 --> 00:03:45,90
because it will always help you.

106
00:03:45,90 --> 00:03:47,40
It's just a little bit of extra effort

107
00:03:47,40 --> 00:03:49,00
and it can pay off huge if you

108
00:03:49,00 --> 00:03:50,80
set your code aside for awhile.

109
00:03:50,80 --> 00:03:52,20
To be able to come back and have something

110
00:03:52,20 --> 00:03:53,80
to read other than the code itself

111
00:03:53,80 --> 00:03:57,10
that tells you what your intentions where is really useful.

112
00:03:57,10 --> 00:03:59,50
So that's a look at comments in JavaScript,

113
00:03:59,50 --> 00:04:01,00
what they can do,

114
00:04:01,00 --> 00:04:02,80
which is allowing you to document your code

115
00:04:02,80 --> 00:04:04,10
for your own readability

116
00:04:04,10 --> 00:04:05,60
and to disable portions of it,

117
00:04:05,60 --> 00:04:07,00
which can be helpful in debugging.

