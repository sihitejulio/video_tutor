1
00:00:00,50 --> 00:00:01,80
- [Instructor] In this video we're going to continue

2
00:00:01,80 --> 00:00:04,10
looking at objects and see some of the things that we

3
00:00:04,10 --> 00:00:07,00
can actually do with and to them.

4
00:00:07,00 --> 00:00:10,40
I have my bird object here with bunch of properties.

5
00:00:10,40 --> 00:00:12,50
If I want to inspect the object and find out what one of

6
00:00:12,50 --> 00:00:16,10
its properties is, here's how I can do that.

7
00:00:16,10 --> 00:00:20,20
I can type the variable name and then a dot.

8
00:00:20,20 --> 00:00:22,80
So if I want to see what the quote is, I can search type

9
00:00:22,80 --> 00:00:25,80
quote and I can see that's "nevermore".

10
00:00:25,80 --> 00:00:27,00
I'll execute that.

11
00:00:27,00 --> 00:00:29,70
And indeed, I get back "nevermore".

12
00:00:29,70 --> 00:00:32,70
Now remember I said that each of these keys is a string.

13
00:00:32,70 --> 00:00:34,90
And when you're creating these objects, to be safe,

14
00:00:34,90 --> 00:00:36,80
we can surround each key with quote marks,

15
00:00:36,80 --> 00:00:38,60
single or double quotes work.

16
00:00:38,60 --> 00:00:40,90
But, when I'm retrieving a value from an object,

17
00:00:40,90 --> 00:00:45,80
using dot notation, this will not work.

18
00:00:45,80 --> 00:00:47,80
I get a syntax error when I try that.

19
00:00:47,80 --> 00:00:50,10
But there's another syntax that I can use where I actually

20
00:00:50,10 --> 00:00:52,20
do use the full string.

21
00:00:52,20 --> 00:00:55,40
So if I try this again, by typing an up arrow,

22
00:00:55,40 --> 00:00:59,40
instead of using the dot, I can use square braces.

23
00:00:59,40 --> 00:01:01,20
And that does work.

24
00:01:01,20 --> 00:01:04,10
If the keys that I've used include special characters

25
00:01:04,10 --> 00:01:08,40
like spaces, or other things, I have to use this notation.

26
00:01:08,40 --> 00:01:12,00
It's exactly the same, just with a different syntax.

27
00:01:12,00 --> 00:01:14,10
So that's how you get stuff out of an object.

28
00:01:14,10 --> 00:01:16,70
Now let's say you wanted to modify this bird object and

29
00:01:16,70 --> 00:01:19,50
add some other information to it.

30
00:01:19,50 --> 00:01:21,10
The same syntax that you used to get things

31
00:01:21,10 --> 00:01:23,30
out of an object, you can use to get things back in,

32
00:01:23,30 --> 00:01:27,10
using assignment, the same way that we assign variables.

33
00:01:27,10 --> 00:01:29,60
So I'm going to clear this console.

34
00:01:29,60 --> 00:01:33,60
Let's say I want to give this bird a color.

35
00:01:33,60 --> 00:01:37,10
I can use dot notation the same way without the quote marks

36
00:01:37,10 --> 00:01:40,10
and I can assign that new property that doesn't yet exist

37
00:01:40,10 --> 00:01:42,20
to "black".

38
00:01:42,20 --> 00:01:46,50
And now if I inspect the bird object, hit return here.

39
00:01:46,50 --> 00:01:48,40
You can disclose this whole thing, and now I have a new

40
00:01:48,40 --> 00:01:52,80
property called "color", with a value of "black".

41
00:01:52,80 --> 00:01:55,70
And if I want to use the same string syntax, with the square

42
00:01:55,70 --> 00:01:59,50
braces, that also works.

43
00:01:59,50 --> 00:02:01,40
So let's say I wanted to add a property called

44
00:02:01,40 --> 00:02:05,30
"where it lives" with spaces and so forth.

45
00:02:05,30 --> 00:02:06,20
I can also do that.

46
00:02:06,20 --> 00:02:10,50
Let's say it lives "in a tree".

47
00:02:10,50 --> 00:02:15,80
Ill inspect "bird' again.

48
00:02:15,80 --> 00:02:16,60
And there we go.

49
00:02:16,60 --> 00:02:18,90
"Where it lives" is "in a tree".

50
00:02:18,90 --> 00:02:21,10
One way you can get around having to use this square

51
00:02:21,10 --> 00:02:24,10
braces notation would be to phrase your keys like this;

52
00:02:24,10 --> 00:02:28,00
let's say I have a bird and I want to say "where it lives".

53
00:02:28,00 --> 00:02:28,90
I can do it like this.

54
00:02:28,90 --> 00:02:31,30
There's no spaces or other characters that can cause the dot

55
00:02:31,30 --> 00:02:33,80
notation to break.

56
00:02:33,80 --> 00:02:35,60
This is called camelcase notation.

57
00:02:35,60 --> 00:02:37,60
First letter's lowercase and each subsequent letter that

58
00:02:37,60 --> 00:02:40,90
starts every jammed-together word is uppercase.

59
00:02:40,90 --> 00:02:43,00
The particulars of camelcase are not required,

60
00:02:43,00 --> 00:02:44,70
it's just a common way of writing variables

61
00:02:44,70 --> 00:02:47,50
and properties in java script.

62
00:02:47,50 --> 00:02:54,40
So I could also set this to "in a tree".

63
00:02:54,40 --> 00:02:56,70
And you can use either notation to get that same property

64
00:02:56,70 --> 00:03:02,20
out of the object.

65
00:03:02,20 --> 00:03:04,50
That's how you add things to an object.

66
00:03:04,50 --> 00:03:06,60
If I want to remove a property from an object,

67
00:03:06,60 --> 00:03:08,20
I can do that as well.

68
00:03:08,20 --> 00:03:11,50
This is done using the delete keyword.

69
00:03:11,50 --> 00:03:14,30
So I type delete and then the property that I want to

70
00:03:14,30 --> 00:03:15,40
delete from the object.

71
00:03:15,40 --> 00:03:17,20
This would be bird dot color,

72
00:03:17,20 --> 00:03:21,20
If I want to remove color from that bird.

73
00:03:21,20 --> 00:03:27,30
Now let's look at bird again.

74
00:03:27,30 --> 00:03:29,70
Color is gone.

75
00:03:29,70 --> 00:03:32,60
And delete works with either notation.

76
00:03:32,60 --> 00:03:35,00
Finally, I'll just note that whether an object starts

77
00:03:35,00 --> 00:03:37,60
as an empty object or whether it already has some properties

78
00:03:37,60 --> 00:03:40,20
in it, all this adding and deleting business works exactly

79
00:03:40,20 --> 00:03:43,50
the same with exactly the same syntax.

80
00:03:43,50 --> 00:03:46,20
So that is an overview of some of the basic manipulations

81
00:03:46,20 --> 00:03:47,90
that you can perform on objects.

82
00:03:47,90 --> 00:03:50,70
Retrieving things from them, adding things to them and

83
00:03:50,70 --> 00:03:52,00
deleting things from them.

