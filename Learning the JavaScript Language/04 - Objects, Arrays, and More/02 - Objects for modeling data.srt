1
00:00:00,50 --> 00:00:01,40
- [Instructor] In this video, we'll look

2
00:00:01,40 --> 00:00:03,40
at some objects that are a little more substantial

3
00:00:03,40 --> 00:00:07,30
to get a sense of how we can use them to do data modeling.

4
00:00:07,30 --> 00:00:11,10
I'm here in my code editor looking at a few objects.

5
00:00:11,10 --> 00:00:14,20
These show some ways to model data with objects.

6
00:00:14,20 --> 00:00:16,60
These are still pretty simple, and we're still talking

7
00:00:16,60 --> 00:00:18,80
at a somewhat abstract level, because the requirements

8
00:00:18,80 --> 00:00:20,90
of any given program are going to be up to you,

9
00:00:20,90 --> 00:00:22,90
in your own work, but these will at least

10
00:00:22,90 --> 00:00:24,90
start to give you an idea.

11
00:00:24,90 --> 00:00:27,30
I've used these objects to represent two animals,

12
00:00:27,30 --> 00:00:29,60
a bird and a bear.

13
00:00:29,60 --> 00:00:31,00
We'll look at the bird first.

14
00:00:31,00 --> 00:00:33,50
What pieces of data I choose to include in an object

15
00:00:33,50 --> 00:00:35,80
are totally up to me, and because I don't have

16
00:00:35,80 --> 00:00:37,40
an actual program to write at this point,

17
00:00:37,40 --> 00:00:38,90
I just picked a few for fun.

18
00:00:38,90 --> 00:00:43,00
I have genus, a species, common name, call type,

19
00:00:43,00 --> 00:00:45,20
which I've made squawky for this raven,

20
00:00:45,20 --> 00:00:48,50
something that the animal might say, Nevermore,

21
00:00:48,50 --> 00:00:51,00
how many offspring it might have and a couple of flags

22
00:00:51,00 --> 00:00:53,50
for whether it's noisy or deadly.

23
00:00:53,50 --> 00:00:56,10
These are Booleans, of course, but developers also

24
00:00:56,10 --> 00:00:58,10
call them flags from time to time.

25
00:00:58,10 --> 00:01:01,80
Let's copy this and try it in the browser console.

26
00:01:01,80 --> 00:01:05,20
And paste it in over here.

27
00:01:05,20 --> 00:01:08,20
And now my browser knows about this bird.

28
00:01:08,20 --> 00:01:11,10
Incidentally, might want to watch out for things like

29
00:01:11,10 --> 00:01:14,90
a comma being missing.

30
00:01:14,90 --> 00:01:19,70
I try that, I'm going to get a syntax error.

31
00:01:19,70 --> 00:01:24,30
So I'm going to redo this making sure all the commas are there.

32
00:01:24,30 --> 00:01:28,00
And there we go, no errors.

33
00:01:28,00 --> 00:01:30,90
Over here with my second animal, the bear,

34
00:01:30,90 --> 00:01:33,80
I've made all the key names the same,

35
00:01:33,80 --> 00:01:35,60
but they store different data.

36
00:01:35,60 --> 00:01:37,40
This is something that you can expect to do often,

37
00:01:37,40 --> 00:01:38,80
where you create objects that have

38
00:01:38,80 --> 00:01:40,80
a similar structure, but have different data

39
00:01:40,80 --> 00:01:42,40
contained inside.

40
00:01:42,40 --> 00:01:43,80
With this idea, you're starting to edge

41
00:01:43,80 --> 00:01:45,10
a little bit into the territory

42
00:01:45,10 --> 00:01:46,60
of object-oriented programming,

43
00:01:46,60 --> 00:01:49,00
which we'll talk about a little bit later.

44
00:01:49,00 --> 00:01:51,30
These animal objects map onto things in the real world,

45
00:01:51,30 --> 00:01:53,40
but you can also make objects of anything you need,

46
00:01:53,40 --> 00:01:55,60
concrete or abstract.

47
00:01:55,60 --> 00:01:58,30
Here, I've made a sort of book of knowledge.

48
00:01:58,30 --> 00:02:00,00
I can refer to it to find out things like

49
00:02:00,00 --> 00:02:04,00
when's lunchtime and what's the ultimate answer,

50
00:02:04,00 --> 00:02:07,20
what the best song is or what Earth is.

51
00:02:07,20 --> 00:02:10,20
So what you actually do with objects is going to vary a lot

52
00:02:10,20 --> 00:02:12,50
depending on the kind of program that you're writing,

53
00:02:12,50 --> 00:02:14,10
but objects are a very powerful way,

54
00:02:14,10 --> 00:02:16,40
despite their simplicity, of representing data

55
00:02:16,40 --> 00:02:18,10
and making sure the computer understands

56
00:02:18,10 --> 00:02:20,00
the things that you need it to.

