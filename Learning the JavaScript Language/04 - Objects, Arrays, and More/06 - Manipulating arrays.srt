1
00:00:00,50 --> 00:00:01,30
- [Instructor] In this video we're going

2
00:00:01,30 --> 00:00:03,40
to get into manipulating arrays,

3
00:00:03,40 --> 00:00:04,70
how you can see what's in an array,

4
00:00:04,70 --> 00:00:08,20
and how we can take action on the contents of an array.

5
00:00:08,20 --> 00:00:11,00
We'll use this counties array with a few counties in it,

6
00:00:11,00 --> 00:00:12,60
for demonstration purposes.

7
00:00:12,60 --> 00:00:15,90
I'm going to copy this and bring it over to the browser

8
00:00:15,90 --> 00:00:18,40
and paste it in.

9
00:00:18,40 --> 00:00:20,20
If I want to get a specific item from an array,

10
00:00:20,20 --> 00:00:22,60
I need to retrieve it using the square braces notation

11
00:00:22,60 --> 00:00:24,20
we saw with objects.

12
00:00:24,20 --> 00:00:26,00
Although arrays are technically objects,

13
00:00:26,00 --> 00:00:29,10
the dot notation doesn't work, only the square braces.

14
00:00:29,10 --> 00:00:34,20
So to get the first item from counties, I use zero.

15
00:00:34,20 --> 00:00:37,80
Always remember, arrays are what we call zero indexed.

16
00:00:37,80 --> 00:00:40,00
Each item in an array is assigned an index,

17
00:00:40,00 --> 00:00:41,10
same as the key in an object,

18
00:00:41,10 --> 00:00:43,50
but with arrays we just usually call it an index.

19
00:00:43,50 --> 00:00:46,70
And those indexes start with zero, always.

20
00:00:46,70 --> 00:00:49,30
Just to be 100% clear on this, the first item is zero

21
00:00:49,30 --> 00:00:51,80
and any other item would be retrieved

22
00:00:51,80 --> 00:00:54,80
by its index from there, one, two, three.

23
00:00:54,80 --> 00:00:57,40
So let's try two here.

24
00:00:57,40 --> 00:01:00,60
Zero, one, two, and we get Carroll.

25
00:01:00,60 --> 00:01:02,50
The way this syntax is often read out loud,

26
00:01:02,50 --> 00:01:06,40
where two here is the index, would be counties sub two.

27
00:01:06,40 --> 00:01:07,80
We can also use this notation

28
00:01:07,80 --> 00:01:09,80
to modify the values in an array.

29
00:01:09,80 --> 00:01:11,80
So I can say that I want index two

30
00:01:11,80 --> 00:01:15,90
in the counties array to be Cheshire.

31
00:01:15,90 --> 00:01:20,40
And then if I look at counties, zero, one, two, there it is.

32
00:01:20,40 --> 00:01:22,40
I've replaced Carroll with Cheshire.

33
00:01:22,40 --> 00:01:24,30
Geography buffs may know that these are counties

34
00:01:24,30 --> 00:01:26,10
in the state of New Hampshire.

35
00:01:26,10 --> 00:01:27,50
So that's how you manipulate things

36
00:01:27,50 --> 00:01:29,10
that are already in there.

37
00:01:29,10 --> 00:01:30,70
Now I want to add to this array

38
00:01:30,70 --> 00:01:32,20
and there's different ways I can do that,

39
00:01:32,20 --> 00:01:34,40
including using the same notation.

40
00:01:34,40 --> 00:01:37,40
If I want to add something onto the end of the array,

41
00:01:37,40 --> 00:01:38,70
I can do it by hand,

42
00:01:38,70 --> 00:01:41,00
counting to what the last index should be.

43
00:01:41,00 --> 00:01:44,90
I know that this is zero, one, two, three

44
00:01:44,90 --> 00:01:47,60
and so the next available index would be four.

45
00:01:47,60 --> 00:01:55,10
So I can add a new county there, we'll add Carroll back on.

46
00:01:55,10 --> 00:01:56,60
But in the context of programming,

47
00:01:56,60 --> 00:01:58,90
doing things this way is not always going to work.

48
00:01:58,90 --> 00:02:00,20
There's going to be a lot of unknowns,

49
00:02:00,20 --> 00:02:01,80
a lot of uncertainty at any point

50
00:02:01,80 --> 00:02:03,80
during the execution of your program.

51
00:02:03,80 --> 00:02:06,30
You're going to want to do things in a more abstract way.

52
00:02:06,30 --> 00:02:09,60
So if I want to stick something on the end of an array,

53
00:02:09,60 --> 00:02:12,90
I can type counties like this, with the square braces.

54
00:02:12,90 --> 00:02:14,70
And now inside the square braces,

55
00:02:14,70 --> 00:02:17,90
I can use an expression that will evaluate to a number.

56
00:02:17,90 --> 00:02:23,60
So I can say counties.length in here.

57
00:02:23,60 --> 00:02:26,10
And this would evaluate down to the next number,

58
00:02:26,10 --> 00:02:27,00
which in this case,

59
00:02:27,00 --> 00:02:30,60
because I've already gone up to four, would be five.

60
00:02:30,60 --> 00:02:36,60
And I'll add Merrimack.

61
00:02:36,60 --> 00:02:40,40
So I've added on to the end of the array this way.

62
00:02:40,40 --> 00:02:42,30
There's another way that I can add something on

63
00:02:42,30 --> 00:02:44,20
to the end of an array using the push method

64
00:02:44,20 --> 00:02:46,10
that every array knows about.

65
00:02:46,10 --> 00:02:51,10
So I can write counties.push, with some parentheses

66
00:02:51,10 --> 00:02:52,40
and then in those parentheses,

67
00:02:52,40 --> 00:02:54,80
I can add the name of the thing I want to push.

68
00:02:54,80 --> 00:02:59,20
This time I'll add Coos, yes that's Coos, not Coos.

69
00:02:59,20 --> 00:03:01,30
So that's a few ways of pushing things

70
00:03:01,30 --> 00:03:05,40
onto the end of an array, and therefore, adding to it.

71
00:03:05,40 --> 00:03:08,30
Now if I want to remove something from the array,

72
00:03:08,30 --> 00:03:09,90
I can use a similar method to push,

73
00:03:09,90 --> 00:03:14,30
only in this case, it's called pop.

74
00:03:14,30 --> 00:03:16,70
Counties.pop with parentheses

75
00:03:16,70 --> 00:03:18,60
and I put nothing inside those parentheses.

76
00:03:18,60 --> 00:03:23,10
And I get back Coos.

77
00:03:23,10 --> 00:03:24,90
And if I look at the array again,

78
00:03:24,90 --> 00:03:26,00
I can see that it's modified,

79
00:03:26,00 --> 00:03:27,80
Coos is no longer in there at all.

80
00:03:27,80 --> 00:03:29,50
It's not just that it gave me the last item,

81
00:03:29,50 --> 00:03:31,60
it actually took it out.

82
00:03:31,60 --> 00:03:32,80
These methods, push and pop,

83
00:03:32,80 --> 00:03:34,70
are used often in combination with arrays,

84
00:03:34,70 --> 00:03:36,90
to grab something from wherever it is.

85
00:03:36,90 --> 00:03:38,60
We're talking in abstract terms, I realize,

86
00:03:38,60 --> 00:03:39,70
but sometimes in our program,

87
00:03:39,70 --> 00:03:41,10
you'll need to store some items

88
00:03:41,10 --> 00:03:42,60
and then retrieve them later.

89
00:03:42,60 --> 00:03:44,60
You can push an item onto an array

90
00:03:44,60 --> 00:03:47,00
and then pull it back off later, when you need it.

91
00:03:47,00 --> 00:03:49,10
So for example, maybe you're writing a piece of code

92
00:03:49,10 --> 00:03:51,70
that's going to look through the entire content of a webpage

93
00:03:51,70 --> 00:03:53,90
and look for images of a certain size.

94
00:03:53,90 --> 00:03:54,80
Every time you find one,

95
00:03:54,80 --> 00:03:57,00
you want to store a reference to it.

96
00:03:57,00 --> 00:03:58,80
So you could push a reference onto an array,

97
00:03:58,80 --> 00:04:00,70
every time you find one and then later,

98
00:04:00,70 --> 00:04:02,00
when you finally collected them all

99
00:04:02,00 --> 00:04:03,40
and you want to do something with them,

100
00:04:03,40 --> 00:04:05,60
you can pop them back off.

101
00:04:05,60 --> 00:04:06,90
Push and pop work on the end of an array

102
00:04:06,90 --> 00:04:09,60
and there are equivalent methods called shift and unshift

103
00:04:09,60 --> 00:04:11,90
that work on the front of an array.

104
00:04:11,90 --> 00:04:14,40
I'm going to show you just one more of the ways

105
00:04:14,40 --> 00:04:16,20
that you can manipulate arrays.

106
00:04:16,20 --> 00:04:18,20
What if you wanted to remove something from the middle?

107
00:04:18,20 --> 00:04:22,10
We've seen this with objects where we used delete

108
00:04:22,10 --> 00:04:23,70
to get rid of a particular item,

109
00:04:23,70 --> 00:04:24,90
but this doesn't work with arrays

110
00:04:24,90 --> 00:04:26,80
the same way it does with regular objects.

111
00:04:26,80 --> 00:04:30,10
If I try to delete index two from counties,

112
00:04:30,10 --> 00:04:31,40
that's the third item in the array.

113
00:04:31,40 --> 00:04:34,20
And then I look at counties, it's still the same length,

114
00:04:34,20 --> 00:04:36,30
it's just that the item that used to be here

115
00:04:36,30 --> 00:04:38,50
has been replaced with an empty value.

116
00:04:38,50 --> 00:04:40,20
So the array hasn't changed in size,

117
00:04:40,20 --> 00:04:44,00
it's just that this one index has changed in its value.

118
00:04:44,00 --> 00:04:45,50
If we want to actually delete something

119
00:04:45,50 --> 00:04:46,40
from the middle of an array,

120
00:04:46,40 --> 00:04:48,90
we need to use another method called splice,

121
00:04:48,90 --> 00:04:50,70
which looks like this.

122
00:04:50,70 --> 00:04:53,30
We could say counties.splice

123
00:04:53,30 --> 00:04:57,20
and then it takes two parameters inside the parentheses.

124
00:04:57,20 --> 00:04:58,60
The first one is which index

125
00:04:58,60 --> 00:05:01,60
of the array we want to do the operation to.

126
00:05:01,60 --> 00:05:04,00
So we want to get rid of that undefined item,

127
00:05:04,00 --> 00:05:07,40
so we'll put in two here and then a comma.

128
00:05:07,40 --> 00:05:08,70
And the second parameter

129
00:05:08,70 --> 00:05:10,40
is how many items we want to get rid of.

130
00:05:10,40 --> 00:05:12,80
So in this case, that's going to be one.

131
00:05:12,80 --> 00:05:14,90
And this returns the item that we removed,

132
00:05:14,90 --> 00:05:17,20
which in this case is just empty.

133
00:05:17,20 --> 00:05:20,10
Now if I look at the counties array again, here we go.

134
00:05:20,10 --> 00:05:21,50
We've actually cut the array down,

135
00:05:21,50 --> 00:05:22,80
so its length is what we expect it

136
00:05:22,80 --> 00:05:24,50
to be with an item removed.

137
00:05:24,50 --> 00:05:26,40
This is important to remember with arrays.

138
00:05:26,40 --> 00:05:27,50
The length always refers

139
00:05:27,50 --> 00:05:29,30
to the number of slots in the array.

140
00:05:29,30 --> 00:05:30,60
It does not matter whether

141
00:05:30,60 --> 00:05:33,30
the slots have anything meaningful in them.

142
00:05:33,30 --> 00:05:34,60
For more on arrays, check out

143
00:05:34,60 --> 00:05:36,20
the Mozilla JavaScript documentation

144
00:05:36,20 --> 00:05:37,90
for a full list of what's available.

145
00:05:37,90 --> 00:05:39,80
But now you've had a taste of how to manipulate arrays,

146
00:05:39,80 --> 00:05:43,00
how to read what's in them, add to, and remove from them.

