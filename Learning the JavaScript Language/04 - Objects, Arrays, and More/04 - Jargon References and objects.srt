1
00:00:00,30 --> 00:00:02,10
- [Instructor] When working with JavaScript objects,

2
00:00:02,10 --> 00:00:04,80
there's an important concept called references.

3
00:00:04,80 --> 00:00:07,30
Objects are references to particular locations

4
00:00:07,30 --> 00:00:08,50
in the computer's memory,

5
00:00:08,50 --> 00:00:10,40
and that impacts your ability to work with them

6
00:00:10,40 --> 00:00:12,90
in different contexts.

7
00:00:12,90 --> 00:00:17,20
So here in my editor, I'm going to copy this animal object,

8
00:00:17,20 --> 00:00:19,20
bring it over into the browser.

9
00:00:19,20 --> 00:00:21,50
I'll paste this.

10
00:00:21,50 --> 00:00:25,40
Just check its value here.

11
00:00:25,40 --> 00:00:28,10
Okay, looks fine.

12
00:00:28,10 --> 00:00:30,00
Now I'm going to create another variable

13
00:00:30,00 --> 00:00:32,00
called animal two.

14
00:00:32,00 --> 00:00:36,00
And I'm going to assign animal to that.

15
00:00:36,00 --> 00:00:38,10
Now if I check its value,

16
00:00:38,10 --> 00:00:39,70
here's what I get.

17
00:00:39,70 --> 00:00:40,90
It's all the same stuff.

18
00:00:40,90 --> 00:00:42,50
Now the question here is

19
00:00:42,50 --> 00:00:45,00
what have I actually done when I executed

20
00:00:45,00 --> 00:00:47,00
this statement up here?

21
00:00:47,00 --> 00:00:49,20
Did I make a copy of the animal object?

22
00:00:49,20 --> 00:00:51,20
Let's find out.

23
00:00:51,20 --> 00:00:53,60
So you might think that if I say,

24
00:00:53,60 --> 00:00:59,20
animal2.genus equals,

25
00:00:59,20 --> 00:01:00,80
let's change this to ursus,

26
00:01:00,80 --> 00:01:03,10
which is a bear.

27
00:01:03,10 --> 00:01:07,70
Okay, now I'll inspect animal two.

28
00:01:07,70 --> 00:01:10,40
I can see the genus is set to ursus,

29
00:01:10,40 --> 00:01:12,20
okay, that's what we expect.

30
00:01:12,20 --> 00:01:14,30
Let's check the original animal.

31
00:01:14,30 --> 00:01:17,40
Hmm, I don't see anything changing down here.

32
00:01:17,40 --> 00:01:22,80
Yeah, I have changed both animal two, and animal.

33
00:01:22,80 --> 00:01:25,20
So what we've exposed here is references.

34
00:01:25,20 --> 00:01:27,40
This statement does not actually make a copy

35
00:01:27,40 --> 00:01:29,60
of the same object.

36
00:01:29,60 --> 00:01:33,00
This is saying assign this animal two location in memory

37
00:01:33,00 --> 00:01:34,80
to the same location in memory

38
00:01:34,80 --> 00:01:37,10
as the original variable, animal.

39
00:01:37,10 --> 00:01:39,60
So these two variables reference the same memory,

40
00:01:39,60 --> 00:01:42,30
and really are the same thing.

41
00:01:42,30 --> 00:01:43,70
You should only expect to run into this

42
00:01:43,70 --> 00:01:44,80
when you're working with objects,

43
00:01:44,80 --> 00:01:47,10
but it's an important point.

44
00:01:47,10 --> 00:01:49,20
The statement does not make a copy of the object,

45
00:01:49,20 --> 00:01:50,50
it just gives you two variables

46
00:01:50,50 --> 00:01:54,60
that point to the same value, the exact same value.

47
00:01:54,60 --> 00:01:58,60
So if I want animal two to be a copy of all the same stuff,

48
00:01:58,60 --> 00:02:00,60
I have to execute the same assignment line

49
00:02:00,60 --> 00:02:04,30
to put in all that data.

50
00:02:04,30 --> 00:02:05,60
I'm hitting up arrow a bunch of items

51
00:02:05,60 --> 00:02:07,60
to get back to this original statement.

52
00:02:07,60 --> 00:02:11,50
Now if I change this to animal two,

53
00:02:11,50 --> 00:02:13,30
we'd see that animal is ursus,

54
00:02:13,30 --> 00:02:16,10
and animal two has the genus of corvus.

55
00:02:16,10 --> 00:02:19,60
So these really are two different things now.

56
00:02:19,60 --> 00:02:21,10
If I want to be really clever,

57
00:02:21,10 --> 00:02:25,00
I have another snippet here in my JavaScript file.

58
00:02:25,00 --> 00:02:27,30
I can use this.

59
00:02:27,30 --> 00:02:30,10
It uses a built-in JavaScript object called JSON

60
00:02:30,10 --> 00:02:32,70
to convert the object to a string of JSON,

61
00:02:32,70 --> 00:02:34,90
and then reparse it into an object.

62
00:02:34,90 --> 00:02:37,00
I don't you expect you to know how to use this right now,

63
00:02:37,00 --> 00:02:40,10
but if you want to make a complete copy of an object safely,

64
00:02:40,10 --> 00:02:40,90
this will do it.

65
00:02:40,90 --> 00:02:41,80
This is pretty much the way

66
00:02:41,80 --> 00:02:44,50
most JavaScript developers do it now.

67
00:02:44,50 --> 00:02:47,60
Anyway, once I've done this copying,

68
00:02:47,60 --> 00:02:53,50
I can change animal two however I want to.

69
00:02:53,50 --> 00:02:58,70
So it's genus two human.

70
00:02:58,70 --> 00:03:00,80
And now animal stays the way it was,

71
00:03:00,80 --> 00:03:03,70
and animal two stays the way it is as well.

72
00:03:03,70 --> 00:03:07,20
The two are now really actually different objects.

73
00:03:07,20 --> 00:03:08,10
Always keep in mind

74
00:03:08,10 --> 00:03:09,90
that when you execute a statement like this,

75
00:03:09,90 --> 00:03:10,90
where this one is an object,

76
00:03:10,90 --> 00:03:12,60
you're not making a copy of it,

77
00:03:12,60 --> 00:03:14,90
you're reusing a reference to the same value,

78
00:03:14,90 --> 00:03:17,00
the same location in the computer's memory.

