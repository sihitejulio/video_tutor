1
00:00:00,50 --> 00:00:01,30
- [Instructor] In this video we're going

2
00:00:01,30 --> 00:00:03,60
to talk about arrays, which are a way of storing

3
00:00:03,60 --> 00:00:06,20
lists of data that preserves their order.

4
00:00:06,20 --> 00:00:07,50
We've already looked at objects,

5
00:00:07,50 --> 00:00:09,30
which let you group data together.

6
00:00:09,30 --> 00:00:11,30
But there are some cases where you want to store say,

7
00:00:11,30 --> 00:00:13,60
a list of things and the order that those things

8
00:00:13,60 --> 00:00:15,70
appear in is important.

9
00:00:15,70 --> 00:00:17,30
For example, let's say you wanted to store

10
00:00:17,30 --> 00:00:18,60
the days of the week which of course

11
00:00:18,60 --> 00:00:19,60
appear in a certain order.

12
00:00:19,60 --> 00:00:22,40
Or the months of the year, something like that.

13
00:00:22,40 --> 00:00:25,00
In cases like that, you want to use an array.

14
00:00:25,00 --> 00:00:26,90
So how do we make one?

15
00:00:26,90 --> 00:00:29,70
Let's make a variable.

16
00:00:29,70 --> 00:00:32,10
Call it My Array.

17
00:00:32,10 --> 00:00:34,50
Instead of using curly braces like we do with objects

18
00:00:34,50 --> 00:00:38,20
we just use square braces like this.

19
00:00:38,20 --> 00:00:40,90
So now if I get my array

20
00:00:40,90 --> 00:00:44,40
I get back an empty set of square braces.

21
00:00:44,40 --> 00:00:47,10
If I want to create my list of days of the week

22
00:00:47,10 --> 00:00:53,00
I'll make another variable, call this Days of the Week.

23
00:00:53,00 --> 00:00:54,70
Two square braces, and now I'm going to fill it in

24
00:00:54,70 --> 00:00:56,20
with some days of the week.

25
00:00:56,20 --> 00:00:58,50
Sunday,

26
00:00:58,50 --> 00:01:00,60
Monday,

27
00:01:00,60 --> 00:01:01,80
Tuesday.

28
00:01:01,80 --> 00:01:03,50
I'm not going to bore you by going all the way through these

29
00:01:03,50 --> 00:01:07,20
I'll just make a few.

30
00:01:07,20 --> 00:01:09,20
And then if I type out the name of the variable,

31
00:01:09,20 --> 00:01:11,70
I can see the whole thing here.

32
00:01:11,70 --> 00:01:13,60
and Chrome even shows me how many things

33
00:01:13,60 --> 00:01:15,80
are in the array.

34
00:01:15,80 --> 00:01:17,30
As you can see, unlike a object where

35
00:01:17,30 --> 00:01:19,60
the order of the keys isn't guaranteed,

36
00:01:19,60 --> 00:01:22,90
an array preserves the order that I put things in.

37
00:01:22,90 --> 00:01:24,90
And there are no keys, only values.

38
00:01:24,90 --> 00:01:26,20
Actually that's not entirely true.

39
00:01:26,20 --> 00:01:28,60
There are keys, they're just not displayed here.

40
00:01:28,60 --> 00:01:31,80
Secretly, an array in JavaScript is actually an object

41
00:01:31,80 --> 00:01:33,30
that gets special treatment.

42
00:01:33,30 --> 00:01:35,30
This is not the case in every programming language.

43
00:01:35,30 --> 00:01:37,80
In many programming languages there are objects

44
00:01:37,80 --> 00:01:41,10
and there are arrays and they are totally different.

45
00:01:41,10 --> 00:01:43,10
In JavaScript they're effectively the same.

46
00:01:43,10 --> 00:01:45,70
But arrays have a few special qualities.

47
00:01:45,70 --> 00:01:47,60
One is that the order is preserved.

48
00:01:47,60 --> 00:01:50,40
But another is that the keys are automatically assigned.

49
00:01:50,40 --> 00:01:53,10
And they're assigned as numbers starting with zero.

50
00:01:53,10 --> 00:01:55,40
So even if I don't care about order I can fill an array

51
00:01:55,40 --> 00:01:57,20
with a list of stuff and not have to think about

52
00:01:57,20 --> 00:02:00,30
naming the key for each stored value.

53
00:02:00,30 --> 00:02:04,30
Again, you can create arrays with any kind of data.

54
00:02:04,30 --> 00:02:09,30
So I'm going to reassign my array

55
00:02:09,30 --> 00:02:14,60
and I'm going to fill in some numbers.

56
00:02:14,60 --> 00:02:22,40
Let's put in some strings.

57
00:02:22,40 --> 00:02:26,00
Maybe a couple of Booleans.

58
00:02:26,00 --> 00:02:27,50
There we go, okay.

59
00:02:27,50 --> 00:02:30,60
Let's check the whole array, and here it is.

60
00:02:30,60 --> 00:02:32,00
All different types of data

61
00:02:32,00 --> 00:02:35,20
and they're all in the same order that I put them in.

62
00:02:35,20 --> 00:02:37,00
Arrays are often used for lists.

63
00:02:37,00 --> 00:02:38,70
So if for example you wanted to store

64
00:02:38,70 --> 00:02:40,00
the counties for some area

65
00:02:40,00 --> 00:02:41,50
because you're making a program that needs

66
00:02:41,50 --> 00:02:42,70
some kind of geographic knowledge,

67
00:02:42,70 --> 00:02:45,60
arrays are a good fit.

68
00:02:45,60 --> 00:02:48,10
I'm going to save you watching me type these out.

69
00:02:48,10 --> 00:02:52,50
I'm just going to grab this from my editor.

70
00:02:52,50 --> 00:02:55,20
I'll paste it in.

71
00:02:55,20 --> 00:02:57,50
So here are a few counties.

72
00:02:57,50 --> 00:03:00,60
And now my program would be aware of this list of counties.

73
00:03:00,60 --> 00:03:02,70
You could put them in in any order you want.

74
00:03:02,70 --> 00:03:03,80
In the case of something like this,

75
00:03:03,80 --> 00:03:05,80
the order might not, for the purposes of your program,

76
00:03:05,80 --> 00:03:07,30
make any particular difference.

77
00:03:07,30 --> 00:03:09,00
But if it did, you'd have already guaranteed

78
00:03:09,00 --> 00:03:12,60
that they appear in this order.

79
00:03:12,60 --> 00:03:18,00
Let's make one more new one, call it array of stuff.

80
00:03:18,00 --> 00:03:19,00
And here's the thing.

81
00:03:19,00 --> 00:03:24,20
Arrays can contain objects.

82
00:03:24,20 --> 00:03:29,40
So I can put an object in here.

83
00:03:29,40 --> 00:03:32,70
So the first item in my array is an object.

84
00:03:32,70 --> 00:03:35,60
You can even, if you want to,

85
00:03:35,60 --> 00:03:38,30
have another array inside your array.

86
00:03:38,30 --> 00:03:39,70
Along with any other values that you might

87
00:03:39,70 --> 00:03:41,60
want to put in there.

88
00:03:41,60 --> 00:03:43,30
JavaScript doesn't care what type of data

89
00:03:43,30 --> 00:03:48,70
anything inside your array is.

90
00:03:48,70 --> 00:03:52,10
So here we are, here's my array of stuff.

91
00:03:52,10 --> 00:03:53,40
First I have my object,

92
00:03:53,40 --> 00:03:57,10
then my nested array, then my Boolean and my string.

93
00:03:57,10 --> 00:03:59,10
You can nest arrays inside other arrays

94
00:03:59,10 --> 00:04:00,50
as deeply as you need to.

95
00:04:00,50 --> 00:04:02,30
You can make an inception array.

96
00:04:02,30 --> 00:04:04,20
There's one more thing I want to point out about arrays

97
00:04:04,20 --> 00:04:05,60
that's different from regular objects

98
00:04:05,60 --> 00:04:08,50
which is that they know how many things are in them.

99
00:04:08,50 --> 00:04:10,40
Just as strings know how long they are,

100
00:04:10,40 --> 00:04:12,80
any array also knows how long it is.

101
00:04:12,80 --> 00:04:14,40
So if I want to see how many things are in

102
00:04:14,40 --> 00:04:18,30
my array of stuff,

103
00:04:18,30 --> 00:04:21,70
I can access the length property like this.

104
00:04:21,70 --> 00:04:23,30
All arrays have a length property.

105
00:04:23,30 --> 00:04:26,20
I can see that this one is four long.

106
00:04:26,20 --> 00:04:28,70
This doesn't account for all the nested data,

107
00:04:28,70 --> 00:04:30,70
it's only how many objects are at the root level

108
00:04:30,70 --> 00:04:32,10
of the array.

109
00:04:32,10 --> 00:04:33,70
There will be some occasions in your programming

110
00:04:33,70 --> 00:04:35,60
where you're going to want to use arrays.

111
00:04:35,60 --> 00:04:36,50
And there will be sometimes

112
00:04:36,50 --> 00:04:38,30
when you're going to want to use objects.

113
00:04:38,30 --> 00:04:41,00
And you'll want to combine them in various interesting ways.

114
00:04:41,00 --> 00:04:42,10
It all depends on the requirements

115
00:04:42,10 --> 00:04:43,70
of whatever you're writing at the time.

116
00:04:43,70 --> 00:04:45,60
Right now we're just going through syntax

117
00:04:45,60 --> 00:04:46,70
so you can know some of the tools

118
00:04:46,70 --> 00:04:49,60
that are available to you in the JavaScript toolbox.

119
00:04:49,60 --> 00:04:50,80
And that was a look at arrays,

120
00:04:50,80 --> 00:04:53,00
which can store lists of anything you want

121
00:04:53,00 --> 00:04:55,00
preserving the order that you put them in.

