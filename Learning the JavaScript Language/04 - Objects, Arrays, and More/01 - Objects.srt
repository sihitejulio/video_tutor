1
00:00:00,50 --> 00:00:01,70
- [Instructor] So far we've looked at some of

2
00:00:01,70 --> 00:00:04,60
the simplest types of data that Javascript knows about.

3
00:00:04,60 --> 00:00:06,60
In this video we're going to look at objects,

4
00:00:06,60 --> 00:00:07,90
which are a little more complex

5
00:00:07,90 --> 00:00:11,30
but completely foundational and super useful.

6
00:00:11,30 --> 00:00:12,90
A lot of of what we're doing in the code we write

7
00:00:12,90 --> 00:00:15,70
could be viewed as teaching the machine to do things for us.

8
00:00:15,70 --> 00:00:18,50
Javascript comes with a lot of simple data types built in.

9
00:00:18,50 --> 00:00:22,20
We've seen numbers, we've seen strings,

10
00:00:22,20 --> 00:00:24,90
and we've seen booleans.

11
00:00:24,90 --> 00:00:26,80
But if we want to represent things that are more complex

12
00:00:26,80 --> 00:00:28,60
that's where objects come in.

13
00:00:28,60 --> 00:00:30,30
Remember that we're assuming the scripts we're writing

14
00:00:30,30 --> 00:00:33,30
know nothing unless we give them something to know.

15
00:00:33,30 --> 00:00:35,40
Figuring out what knowledge our programs need,

16
00:00:35,40 --> 00:00:36,90
in other words how structured the data

17
00:00:36,90 --> 00:00:38,40
that we're feeding into our programs

18
00:00:38,40 --> 00:00:41,40
and getting back out of them, is a key decision.

19
00:00:41,40 --> 00:00:43,30
Objects are the main way in Javascript

20
00:00:43,30 --> 00:00:45,50
and various other languages to encapsulate

21
00:00:45,50 --> 00:00:47,30
that knowledge in ways that are more complex

22
00:00:47,30 --> 00:00:50,20
than simple data types will allow on their own.

23
00:00:50,20 --> 00:00:51,60
The simplest way to make an object

24
00:00:51,60 --> 00:00:53,80
is with an object literal, similar to

25
00:00:53,80 --> 00:00:55,50
the string literals we saw before.

26
00:00:55,50 --> 00:00:57,20
Just type a curly brace and then

27
00:00:57,20 --> 00:00:59,50
another curly brace that closes it.

28
00:00:59,50 --> 00:01:01,20
That is an object.

29
00:01:01,20 --> 00:01:03,90
Now, we can inspect this and we can see that

30
00:01:03,90 --> 00:01:06,50
there's nothing inside except for this little proto thing.

31
00:01:06,50 --> 00:01:08,00
We're going to ignore that for now.

32
00:01:08,00 --> 00:01:10,40
It's effectively an empty object.

33
00:01:10,40 --> 00:01:13,10
You can also, of course, assign variables.

34
00:01:13,10 --> 00:01:15,70
You could say, myEmptyObject equals

35
00:01:15,70 --> 00:01:18,70
another of these empty object literals.

36
00:01:18,70 --> 00:01:22,10
Look at myEmptyObject as well, it's the same thing.

37
00:01:22,10 --> 00:01:24,30
I just have this magical proto object

38
00:01:24,30 --> 00:01:26,80
which we will leave alone for now.

39
00:01:26,80 --> 00:01:27,80
For all intents and purposes

40
00:01:27,80 --> 00:01:29,80
this is an object with nothing in it.

41
00:01:29,80 --> 00:01:31,10
To be really useful, of course,

42
00:01:31,10 --> 00:01:33,20
an object needs to have some stuff.

43
00:01:33,20 --> 00:01:34,50
You can think of an object sort of like

44
00:01:34,50 --> 00:01:37,10
a box of labeled folders.

45
00:01:37,10 --> 00:01:39,20
Every useful object has a series of labels

46
00:01:39,20 --> 00:01:42,20
known as keys and values, which are generally

47
00:01:42,20 --> 00:01:45,70
the data that we care about that are all grouped together.

48
00:01:45,70 --> 00:01:48,50
So here's what that looks like with actual data.

49
00:01:48,50 --> 00:01:50,70
We're going to assign this to another variable

50
00:01:50,70 --> 00:01:54,40
which we'll call, notEmptyObject.

51
00:01:54,40 --> 00:01:56,70
And when I say that it's like a filing system

52
00:01:56,70 --> 00:01:58,60
there are labels inside the object.

53
00:01:58,60 --> 00:02:00,50
So we're going to hit shift and return

54
00:02:00,50 --> 00:02:03,40
and then type my first label.

55
00:02:03,40 --> 00:02:05,90
The label is a string and when you're working in Javascript

56
00:02:05,90 --> 00:02:10,90
you'll see these strings with quote marks and without

57
00:02:10,90 --> 00:02:12,40
followed by a colon.

58
00:02:12,40 --> 00:02:14,20
It's safest to use quote marks so you don't

59
00:02:14,20 --> 00:02:17,00
accidentally use a Javascript reserve word for a key

60
00:02:17,00 --> 00:02:18,20
but because it's fewer key strokes

61
00:02:18,20 --> 00:02:21,60
you'll see that a lot as well, a return accidentally there.

62
00:02:21,60 --> 00:02:23,40
So anyway you put in the name of the key,

63
00:02:23,40 --> 00:02:25,60
then a colon followed by the value.

64
00:02:25,60 --> 00:02:27,30
The value can be any type of data

65
00:02:27,30 --> 00:02:29,10
that Javascript knows about, numbers, strings,

66
00:02:29,10 --> 00:02:32,40
booleans, other objects, anything.

67
00:02:32,40 --> 00:02:35,10
Put another string in here for this one.

68
00:02:35,10 --> 00:02:39,30
To add more folders to your box you just add a comma.

69
00:02:39,30 --> 00:02:43,70
Here's a second label called label2

70
00:02:43,70 --> 00:02:45,90
and I'll give it a value of value2.

71
00:02:45,90 --> 00:02:50,50
So again, string followed by a colon followed by the value.

72
00:02:50,50 --> 00:02:53,60
And you do this for as many items as you need in the object.

73
00:02:53,60 --> 00:02:56,40
The last value can also have a comma on the end if you want.

74
00:02:56,40 --> 00:02:57,40
That used to cause some issues

75
00:02:57,40 --> 00:03:01,30
in the older versions of Javascript but not anymore.

76
00:03:01,30 --> 00:03:04,60
And then you close it with that closed curly brace.

77
00:03:04,60 --> 00:03:06,70
You return to execute it and now I can type

78
00:03:06,70 --> 00:03:09,60
the variable name and see my not empty object

79
00:03:09,60 --> 00:03:13,30
with its first label, second label, and the two values.

80
00:03:13,30 --> 00:03:15,70
Objects are very simple to create but you can make

81
00:03:15,70 --> 00:03:18,30
some very sophisticated data structures using nothing

82
00:03:18,30 --> 00:03:21,00
but objects, and we'll be looking at that next.

