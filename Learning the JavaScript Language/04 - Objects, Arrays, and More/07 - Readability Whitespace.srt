1
00:00:00,50 --> 00:00:01,80
- [Instructor] In this video we're going to talk about

2
00:00:01,80 --> 00:00:03,30
how to make your code more readable

3
00:00:03,30 --> 00:00:05,20
through the use of white space.

4
00:00:05,20 --> 00:00:07,40
I have here a little block

5
00:00:07,40 --> 00:00:10,40
that assigns a few variables individually.

6
00:00:10,40 --> 00:00:13,10
There's pretty much no white space at all, but it does work.

7
00:00:13,10 --> 00:00:16,50
If I copy this, switch over to the browser,

8
00:00:16,50 --> 00:00:20,90
and paste it in,

9
00:00:20,90 --> 00:00:28,40
all of my variables have been assigned here.

10
00:00:28,40 --> 00:00:31,20
So it works, but it's not that easy to read.

11
00:00:31,20 --> 00:00:33,00
What I want to do here is add some white space

12
00:00:33,00 --> 00:00:34,90
which is generally ignored by JavaScript,

13
00:00:34,90 --> 00:00:38,00
makes reading code much easier.

14
00:00:38,00 --> 00:00:41,80
So this can take the form of putting some spaces in between,

15
00:00:41,80 --> 00:00:45,30
or I can put assignments on multiple lines, like this.

16
00:00:45,30 --> 00:00:47,20
Sometimes you'll see code where people have really

17
00:00:47,20 --> 00:00:48,60
curated their white space,

18
00:00:48,60 --> 00:00:50,80
making sure all the equal signs line up

19
00:00:50,80 --> 00:00:52,30
in a series of assignments.

20
00:00:52,30 --> 00:00:53,90
This looks pretty nice, but it's a little tedious

21
00:00:53,90 --> 00:00:55,00
to do by hand.

22
00:00:55,00 --> 00:00:56,90
So if you like this style, look for a plugin

23
00:00:56,90 --> 00:01:00,10
for your code editor that can help you do this.

24
00:01:00,10 --> 00:01:02,80
Likewise, when you're creating an object,

25
00:01:02,80 --> 00:01:05,00
here we have no white space

26
00:01:05,00 --> 00:01:06,00
and you can read it for the most part,

27
00:01:06,00 --> 00:01:07,80
it's a pretty simple object.

28
00:01:07,80 --> 00:01:09,60
But if I add some white space to it,

29
00:01:09,60 --> 00:01:14,40
and some carriage returns, it becomes a lot easier to read.

30
00:01:14,40 --> 00:01:16,20
We saw previously that you can break strings up

31
00:01:16,20 --> 00:01:18,40
across multiple lines using backslashes,

32
00:01:18,40 --> 00:01:20,50
which is another use of white space.

33
00:01:20,50 --> 00:01:23,10
When you do this, make sure that each of your lines

34
00:01:23,10 --> 00:01:25,40
really ends with a backslash, that there's no

35
00:01:25,40 --> 00:01:27,70
spaces sneaking in after those.

36
00:01:27,70 --> 00:01:30,50
If I add one in here, you can see all these squiggly lines

37
00:01:30,50 --> 00:01:32,20
showing up in my editor, which tells me that

38
00:01:32,20 --> 00:01:35,90
this is not valid JavaScript anymore, something's broken.

39
00:01:35,90 --> 00:01:39,50
If I remove the space, everything goes back to normal.

40
00:01:39,50 --> 00:01:42,10
If your editor doesn't provide a feature like this,

41
00:01:42,10 --> 00:01:44,00
where all these squiggly lines show up,

42
00:01:44,00 --> 00:01:46,70
finding a bug like this can get very tricky.

43
00:01:46,70 --> 00:01:48,00
So just watch out when you're using

44
00:01:48,00 --> 00:01:50,70
backslash notation like this.

45
00:01:50,70 --> 00:01:52,10
Now the way that you set up your white space

46
00:01:52,10 --> 00:01:53,40
is something that programmers will,

47
00:01:53,40 --> 00:01:56,00
for lack of a better word, fight over.

48
00:01:56,00 --> 00:01:57,70
There are elements of personal preference,

49
00:01:57,70 --> 00:01:59,40
or if you're working on a team,

50
00:01:59,40 --> 00:02:01,50
there may be a coding standard everyone is expected

51
00:02:01,50 --> 00:02:03,40
to follow to make sure that everybody can read

52
00:02:03,40 --> 00:02:05,20
each other's code well, and that the results

53
00:02:05,20 --> 00:02:07,60
are nice and uniform.

54
00:02:07,60 --> 00:02:10,40
There are even tools like Prettier, that can take your code,

55
00:02:10,40 --> 00:02:12,90
however you've written it, and automatically add white space

56
00:02:12,90 --> 00:02:15,70
and do other formatting to it to make it readable.

57
00:02:15,70 --> 00:02:17,30
I've become a pretty big fan of Prettier

58
00:02:17,30 --> 00:02:20,20
and use it in all my JavaScript projects.

59
00:02:20,20 --> 00:02:22,10
In any case, when you're starting out,

60
00:02:22,10 --> 00:02:24,30
you might just be working on your own projects for yourself

61
00:02:24,30 --> 00:02:26,10
and the main issue is to keep your code

62
00:02:26,10 --> 00:02:27,90
readable for yourself.

63
00:02:27,90 --> 00:02:29,40
However carefully you want to do it,

64
00:02:29,40 --> 00:02:31,50
I recommend embracing the use of white space

65
00:02:31,50 --> 00:02:33,10
to make sure your code is readable,

66
00:02:33,10 --> 00:02:35,00
without effecting how it's executed.

