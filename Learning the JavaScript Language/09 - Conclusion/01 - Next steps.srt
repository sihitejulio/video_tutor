1
00:00:00,50 --> 00:00:01,40
- [Instructor] Your next step from here

2
00:00:01,40 --> 00:00:02,70
could go many directions.

3
00:00:02,70 --> 00:00:04,20
In this video, we'll look at some of the courses

4
00:00:04,20 --> 00:00:06,00
that can be helpful for some of the kinds of things

5
00:00:06,00 --> 00:00:06,90
you might try to build

6
00:00:06,90 --> 00:00:09,40
with your newfound knowledge of JavaScript.

7
00:00:09,40 --> 00:00:10,80
If you're interested in writing JavaScript

8
00:00:10,80 --> 00:00:12,30
the way it was originally intended

9
00:00:12,30 --> 00:00:14,20
to make web pages more interactive,

10
00:00:14,20 --> 00:00:15,80
I'd recommend you learn its companions,

11
00:00:15,80 --> 00:00:18,10
HTML and CSS next.

12
00:00:18,10 --> 00:00:21,20
Making static HTML pages is not as common as it used to be,

13
00:00:21,20 --> 00:00:24,10
but it's still an important skill for working on the web.

14
00:00:24,10 --> 00:00:25,70
HTML Essential Training will give you

15
00:00:25,70 --> 00:00:27,50
a very thorough understanding of the language

16
00:00:27,50 --> 00:00:29,10
so you can start building your own pages

17
00:00:29,10 --> 00:00:31,20
to experiment with JavaScript.

18
00:00:31,20 --> 00:00:33,10
Likewise, Introduction to CSS will give you

19
00:00:33,10 --> 00:00:34,10
an overview of the language

20
00:00:34,10 --> 00:00:36,10
that makes web pages look how they do.

21
00:00:36,10 --> 00:00:37,30
That's the simplest way to put it,

22
00:00:37,30 --> 00:00:41,20
but there's a lot you can do with CSS, even animation.

23
00:00:41,20 --> 00:00:43,60
Knowledge of CSS is essential for any web designer

24
00:00:43,60 --> 00:00:46,40
and there is a series of CSS Essential Training courses

25
00:00:46,40 --> 00:00:48,10
to get you fully up to speed.

26
00:00:48,10 --> 00:00:50,30
The rest of these deal with JavaScript itself.

27
00:00:50,30 --> 00:00:51,50
I'm a working web developer,

28
00:00:51,50 --> 00:00:53,60
so the main lens through which I look at JavaScript is

29
00:00:53,60 --> 00:00:54,90
how it's used on the web.

30
00:00:54,90 --> 00:00:55,80
So that's been the impetus

31
00:00:55,80 --> 00:00:57,80
for the lion's share of my courses.

32
00:00:57,80 --> 00:01:00,20
If you haven't tired of the mellifluous tone of my voice,

33
00:01:00,20 --> 00:01:03,40
you could check out several other courses of mine next.

34
00:01:03,40 --> 00:01:05,20
JavaScript for Web Designers is a beginner

35
00:01:05,20 --> 00:01:06,40
to intermediate level course

36
00:01:06,40 --> 00:01:08,10
where you build small but useful pages

37
00:01:08,10 --> 00:01:10,80
to come to grips with plain JavaScript on the web.

38
00:01:10,80 --> 00:01:12,30
jQuery for Web Designers is similar

39
00:01:12,30 --> 00:01:13,70
but makes use of jQuery,

40
00:01:13,70 --> 00:01:15,10
a collection of JavaScript functions

41
00:01:15,10 --> 00:01:17,50
that's used on probably millions of websites.

42
00:01:17,50 --> 00:01:19,50
Many people beginning with JavaScript actually start

43
00:01:19,50 --> 00:01:22,00
with jQuery because of how easy it makes certain aspects

44
00:01:22,00 --> 00:01:23,30
of working with web pages,

45
00:01:23,30 --> 00:01:24,70
so it's worth a look.

46
00:01:24,70 --> 00:01:28,00
Vue and React for Web Designers are companion courses

47
00:01:28,00 --> 00:01:30,40
at a solid intermediate to more advanced level,

48
00:01:30,40 --> 00:01:33,00
so you'll most likely want to bookmark those for later.

49
00:01:33,00 --> 00:01:35,10
Vue and React are two very popular frameworks

50
00:01:35,10 --> 00:01:37,20
for making sites that are mostly driven by JavaScript

51
00:01:37,20 --> 00:01:39,70
and can also be used to add data-centric interactivity

52
00:01:39,70 --> 00:01:41,50
to smaller parts of sites.

53
00:01:41,50 --> 00:01:42,90
Even if you're not quite at the level

54
00:01:42,90 --> 00:01:44,20
of wanting to try them yet,

55
00:01:44,20 --> 00:01:45,30
you could check out either one

56
00:01:45,30 --> 00:01:47,20
to see what these tools are like.

57
00:01:47,20 --> 00:01:48,70
And one more course of mine,

58
00:01:48,70 --> 00:01:50,90
in Learning JavaScript Debugging we go in-depth

59
00:01:50,90 --> 00:01:53,50
with tools for debugging JavaScript using web browsers,

60
00:01:53,50 --> 00:01:56,50
Node, integrated development environments, and more.

61
00:01:56,50 --> 00:01:58,30
I recommend watching this course fairly early

62
00:01:58,30 --> 00:01:59,40
in your JavaScript journey,

63
00:01:59,40 --> 00:02:01,80
but only after you've tried writing some code of your own

64
00:02:01,80 --> 00:02:03,20
so you'll have had some chance to feel

65
00:02:03,20 --> 00:02:05,60
the need for debugging skills.

66
00:02:05,60 --> 00:02:06,60
I'm certainly not the only one

67
00:02:06,60 --> 00:02:08,20
with JavaScript courses available.

68
00:02:08,20 --> 00:02:10,30
JavaScript Essential Training is a comprehensive course

69
00:02:10,30 --> 00:02:11,90
to take you more deeply into the language

70
00:02:11,90 --> 00:02:12,80
than we covered here

71
00:02:12,80 --> 00:02:14,20
and can be another helpful reference

72
00:02:14,20 --> 00:02:16,50
as you start writing code of your own.

73
00:02:16,50 --> 00:02:18,60
There's a lot more you can do with JavaScript too.

74
00:02:18,60 --> 00:02:21,00
Here are a few more courses to give you a taste.

75
00:02:21,00 --> 00:02:22,40
Data Visualization is the art

76
00:02:22,40 --> 00:02:24,00
of presenting information visually,

77
00:02:24,00 --> 00:02:25,90
charts, infographics, and so forth.

78
00:02:25,90 --> 00:02:27,50
One of the most popular JavaScript toolkits

79
00:02:27,50 --> 00:02:29,50
for creating such things is called D3

80
00:02:29,50 --> 00:02:31,30
and you can learn all about it in this course,

81
00:02:31,30 --> 00:02:34,40
Learning Data Visualization with D3.js.

82
00:02:34,40 --> 00:02:35,80
Interactive Animations will teach you

83
00:02:35,80 --> 00:02:37,80
about the capabilities of CSS animation

84
00:02:37,80 --> 00:02:40,40
and how to combine that with the extra power of JavaScript

85
00:02:40,40 --> 00:02:43,90
to make animations with precise timing and user control.

86
00:02:43,90 --> 00:02:45,50
Learning TensorFlow will give you a window

87
00:02:45,50 --> 00:02:48,00
into the world of machine learning using JavaScript.

88
00:02:48,00 --> 00:02:49,80
JavaScript is probably not the most popular language

89
00:02:49,80 --> 00:02:50,70
for that purpose out there,

90
00:02:50,70 --> 00:02:52,60
but it can be done.

91
00:02:52,60 --> 00:02:55,20
You can also use JavaScript to make 3D graphics.

92
00:02:55,20 --> 00:02:56,90
Learning 3D Graphics will show you how

93
00:02:56,90 --> 00:02:59,90
using Three.js, another popular JavaScript toolkit.

94
00:02:59,90 --> 00:03:01,10
All right, two more.

95
00:03:01,10 --> 00:03:03,10
If you're interested in using JavaScript outside

96
00:03:03,10 --> 00:03:04,30
the confines of a web browser,

97
00:03:04,30 --> 00:03:07,10
I recommend Learning Node.js as the next step.

98
00:03:07,10 --> 00:03:09,10
It's a relatively brief course that will get you introduced

99
00:03:09,10 --> 00:03:13,00
to how JavaScript is run on servers instead of in browsers.

100
00:03:13,00 --> 00:03:14,90
Node.js: Test-Driven Development is another

101
00:03:14,90 --> 00:03:16,50
very short course that can introduce you

102
00:03:16,50 --> 00:03:17,60
to test-driven development

103
00:03:17,60 --> 00:03:19,60
which is a great way to approach programming projects,

104
00:03:19,60 --> 00:03:21,40
but not the most immediately intuitive way

105
00:03:21,40 --> 00:03:22,80
for a lot of people.

106
00:03:22,80 --> 00:03:24,90
I'd recommend looking at this relatively early,

107
00:03:24,90 --> 00:03:26,60
but as with the debugging course,

108
00:03:26,60 --> 00:03:27,70
after you've written a little code

109
00:03:27,70 --> 00:03:29,40
to get your feet wet.

110
00:03:29,40 --> 00:03:32,20
There are, of course, tons of JavaScript courses available.

111
00:03:32,20 --> 00:03:33,40
Start writing a little code of your own

112
00:03:33,40 --> 00:03:35,20
in whatever domain interests you

113
00:03:35,20 --> 00:03:37,10
and see where your curiosity takes you.

114
00:03:37,10 --> 00:03:38,00
Thanks for watching!

