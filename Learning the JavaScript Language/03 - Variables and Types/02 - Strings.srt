1
00:00:00,60 --> 00:00:02,90
- [Narrator] In this video we're going to look at strings.

2
00:00:02,90 --> 00:00:05,70
A string is character data meaning things like

3
00:00:05,70 --> 00:00:09,20
sentences, words, single letters, numbers, or punctuation.

4
00:00:09,20 --> 00:00:12,30
Basically it means anything between quote marks.

5
00:00:12,30 --> 00:00:16,70
So, for example, I could say, "This is a string".

6
00:00:16,70 --> 00:00:19,00
Chrome highlights it in red for me.

7
00:00:19,00 --> 00:00:20,40
When you formulate a string like this,

8
00:00:20,40 --> 00:00:22,60
it's actually called a string literal.

9
00:00:22,60 --> 00:00:24,70
Literal here basically means you're making data

10
00:00:24,70 --> 00:00:28,10
of some type using the simplest notation possible.

11
00:00:28,10 --> 00:00:30,00
So making a string literal like this,

12
00:00:30,00 --> 00:00:33,00
means I'm putting string data inside quote marks.

13
00:00:33,00 --> 00:00:35,00
There are other less convenient ways to make strings as well

14
00:00:35,00 --> 00:00:37,90
but this is what we use 99% of the time.

15
00:00:37,90 --> 00:00:39,50
A string doesn't have to be a sentence,

16
00:00:39,50 --> 00:00:41,60
it can be a number inside quotes.

17
00:00:41,60 --> 00:00:45,50
So it's a string of one two instead of twelve.

18
00:00:45,50 --> 00:00:46,50
These are two different things

19
00:00:46,50 --> 00:00:48,40
as far as JavaScript is concerned.

20
00:00:48,40 --> 00:00:52,60
Twelve verses the string twelve.

21
00:00:52,60 --> 00:00:54,60
You can make strings using these double quote characters.

22
00:00:54,60 --> 00:00:57,10
You can also make strings using single quote characters.

23
00:00:57,10 --> 00:01:00,00
'This is also a string.'

24
00:01:00,00 --> 00:01:02,50
Whichever you use, you need to begin and end the string

25
00:01:02,50 --> 00:01:04,30
using the same character.

26
00:01:04,30 --> 00:01:06,10
So I can't do this, a single quote at the beginning,

27
00:01:06,10 --> 00:01:08,00
double quote at the end.

28
00:01:08,00 --> 00:01:12,00
I try and execute this, I get a syntax error. Doesn't work.

29
00:01:12,00 --> 00:01:13,80
Sometimes you're going to need to write a string

30
00:01:13,80 --> 00:01:16,00
that will have a quotation mark inside it.

31
00:01:16,00 --> 00:01:17,50
This comes up a lot if you're working

32
00:01:17,50 --> 00:01:19,20
with HTML in JavaScript.

33
00:01:19,20 --> 00:01:24,40
So if we're writing a piece of HTML like this,

34
00:01:24,40 --> 00:01:26,50
it works because I put single quotes at the beginning

35
00:01:26,50 --> 00:01:30,40
and the end with the attribute using double quotes.

36
00:01:30,40 --> 00:01:31,40
This is one of many times where

37
00:01:31,40 --> 00:01:33,20
syntax highlighting is helpful.

38
00:01:33,20 --> 00:01:34,60
Chrome offers some degree of this

39
00:01:34,60 --> 00:01:36,60
but let's switch over to our code editor

40
00:01:36,60 --> 00:01:39,80
so we have a little more flexibility.

41
00:01:39,80 --> 00:01:42,20
I have here a blank file and I'm going to use

42
00:01:42,20 --> 00:01:47,10
the default command, Command + K followed by typing M.

43
00:01:47,10 --> 00:01:49,70
This lets me set the language mode for the file.

44
00:01:49,70 --> 00:01:52,80
I'm going to type Java to get down to JavaScript

45
00:01:52,80 --> 00:01:54,10
and hit return.

46
00:01:54,10 --> 00:01:55,60
Now it knows that I'm trying to type some JavaScript.

47
00:01:55,60 --> 00:01:58,20
The color scheme that I have is going to

48
00:01:58,20 --> 00:02:00,70
show strings in red so I can see

49
00:02:00,70 --> 00:02:02,30
whether a string is enclosed correctly.

50
00:02:02,30 --> 00:02:04,50
If it is, everything inside stays red.

51
00:02:04,50 --> 00:02:10,20
If not, it starts to turn black.

52
00:02:10,20 --> 00:02:12,60
It starts to give me little red squiggles.

53
00:02:12,60 --> 00:02:15,40
So I'll try that little snippet of HTML again.

54
00:02:15,40 --> 00:02:17,90
The double quotes contained in the single quotes works fine

55
00:02:17,90 --> 00:02:20,50
and the opposite works as well.

56
00:02:20,50 --> 00:02:26,10
"This is Joe's favorite string."

57
00:02:26,10 --> 00:02:28,40
You will at some point need to include quote marks

58
00:02:28,40 --> 00:02:31,00
of both kinds inside the string.

59
00:02:31,00 --> 00:02:34,70
Like, say, "This is Joe's "favorite" string".

60
00:02:34,70 --> 00:02:36,70
You can see that the color starts to change

61
00:02:36,70 --> 00:02:40,00
because I'm breaking the way that the string is delimited.

62
00:02:40,00 --> 00:02:42,90
To JavaScript what I have here right now is a string

63
00:02:42,90 --> 00:02:45,00
followed by some data in the middle that's going to

64
00:02:45,00 --> 00:02:48,10
make JavaScript mad and then another string.

65
00:02:48,10 --> 00:02:50,10
To fix this, I need to use a backslash

66
00:02:50,10 --> 00:02:52,60
to do what's called escaping the string.

67
00:02:52,60 --> 00:02:53,90
So I'm going to put a backslash

68
00:02:53,90 --> 00:02:55,40
in front of each of these quotes

69
00:02:55,40 --> 00:02:59,40
that's supposed to be an inside of this entire string.

70
00:02:59,40 --> 00:03:01,00
So for every quote mark that's causing trouble

71
00:03:01,00 --> 00:03:03,10
I add that slash.

72
00:03:03,10 --> 00:03:05,40
Just to prove this works I'm going to select this,

73
00:03:05,40 --> 00:03:09,90
copy it, return to the browser, paste it in.

74
00:03:09,90 --> 00:03:11,30
There, it works.

75
00:03:11,30 --> 00:03:13,80
And it's printed without those backslash marks

76
00:03:13,80 --> 00:03:15,10
which is a little bit confusing.

77
00:03:15,10 --> 00:03:18,80
I couldn't just copy this and re-execute it, it won't work.

78
00:03:18,80 --> 00:03:21,90
As you can see, this is turning black again.

79
00:03:21,90 --> 00:03:24,30
Backslashes let us do one more helpful thing,

80
00:03:24,30 --> 00:03:26,60
breaking string up into multiple lines.

81
00:03:26,60 --> 00:03:29,50
So I'll switch back to my editor

82
00:03:29,50 --> 00:03:31,50
and if I want to write another string called

83
00:03:31,50 --> 00:03:38,40
"This is Joe's favorite String EVER"

84
00:03:38,40 --> 00:03:40,30
I've got all these squiggly lines

85
00:03:40,30 --> 00:03:42,60
telling me that this is not working.

86
00:03:42,60 --> 00:03:46,00
But if I put a backslash on the end of each line,

87
00:03:46,00 --> 00:03:47,30
I'm putting a little extra space in there

88
00:03:47,30 --> 00:03:49,50
just to make it a little more readable,

89
00:03:49,50 --> 00:03:51,00
all the squiggles go away,

90
00:03:51,00 --> 00:03:53,40
the whole string is highlighted in red.

91
00:03:53,40 --> 00:03:54,80
And then I just end the string with

92
00:03:54,80 --> 00:03:57,20
the same kind of quote mark that I started it with.

93
00:03:57,20 --> 00:03:58,60
For something this short it doesn't really matter

94
00:03:58,60 --> 00:04:00,20
but when you need really long runs of text

95
00:04:00,20 --> 00:04:02,60
this can be pretty helpful.

96
00:04:02,60 --> 00:04:05,90
So that's an overview of how to create strings

97
00:04:05,90 --> 00:04:07,90
which are character data or things like

98
00:04:07,90 --> 00:04:10,00
sentences, words, and so forth.

