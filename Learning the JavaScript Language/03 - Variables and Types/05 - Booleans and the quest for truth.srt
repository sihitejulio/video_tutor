1
00:00:00,50 --> 00:00:01,40
- [Narrator] In this video we're going

2
00:00:01,40 --> 00:00:04,00
to talk about booleans, tat is, true and false

3
00:00:04,00 --> 00:00:06,30
and why booleans matter to us.

4
00:00:06,30 --> 00:00:07,70
You may know from the way people talk

5
00:00:07,70 --> 00:00:09,90
about computers sort of colloquially,

6
00:00:09,90 --> 00:00:11,90
that computers deal with ones and zeros.

7
00:00:11,90 --> 00:00:15,00
On and off, true and false, that sort of dichotomy.

8
00:00:15,00 --> 00:00:16,30
Boolean is a data type that

9
00:00:16,30 --> 00:00:19,00
can capture true or false for us.

10
00:00:19,00 --> 00:00:24,80
So, in JavaScript, a boolean is one of either true or false.

11
00:00:24,80 --> 00:00:26,60
They need to be spelled exactly like this.

12
00:00:26,60 --> 00:00:28,80
All lowercase and spelled correctly

13
00:00:28,80 --> 00:00:30,20
in order for JavaScript to know

14
00:00:30,20 --> 00:00:32,30
that you mean true and false.

15
00:00:32,30 --> 00:00:34,30
If you write true with a capital T,

16
00:00:34,30 --> 00:00:36,60
JavaScript is going to think you mean a variable by that name.

17
00:00:36,60 --> 00:00:38,40
Notice that it is not colored as

18
00:00:38,40 --> 00:00:41,10
a language keyword here in crumb.

19
00:00:41,10 --> 00:00:43,00
Because it's so similar to that existing keyword,

20
00:00:43,00 --> 00:00:46,00
you would do really well to avoid naming any variable True.

21
00:00:46,00 --> 00:00:48,20
Likewise, all-caps will not work either.

22
00:00:48,20 --> 00:00:52,10
Or any combination of characters.

23
00:00:52,10 --> 00:00:54,50
Only all lowercase for both.

24
00:00:54,50 --> 00:00:57,00
I can set variables to true or false.

25
00:00:57,00 --> 00:01:01,70
Like, say button has been clicked equals false.

26
00:01:01,70 --> 00:01:05,10
And by doing so I would say that this variable is a boolean.

27
00:01:05,10 --> 00:01:06,30
I could use this variable to track

28
00:01:06,30 --> 00:01:09,50
whether someone has clicked a particular button on a page.

29
00:01:09,50 --> 00:01:12,10
Started out set to false, and then sometime later when I

30
00:01:12,10 --> 00:01:15,50
know that the button has been clicked, set it to true.

31
00:01:15,50 --> 00:01:18,10
And I can act on that information somehow.

32
00:01:18,10 --> 00:01:20,40
Booleans are important because they're one main way

33
00:01:20,40 --> 00:01:21,90
of getting answers from your programs

34
00:01:21,90 --> 00:01:23,90
about the state of your programs world.

35
00:01:23,90 --> 00:01:25,60
What I mean is, you can ask questions in the form

36
00:01:25,60 --> 00:01:28,20
of is this true or is it false?

37
00:01:28,20 --> 00:01:29,90
It's one of the questions that JavaScript can answer

38
00:01:29,90 --> 00:01:32,50
for you without having to do a bunch of extra work.

39
00:01:32,50 --> 00:01:34,10
These questions that you can ask JavaScript

40
00:01:34,10 --> 00:01:35,70
and get back the answers for,

41
00:01:35,70 --> 00:01:37,90
are generally in the form of comparisons.

42
00:01:37,90 --> 00:01:39,70
We'll look at these in more detail a little later on.

43
00:01:39,70 --> 00:01:42,70
But just to give you a preview, it looks like this.

44
00:01:42,70 --> 00:01:44,20
I'll make two variables.

45
00:01:44,20 --> 00:01:49,30
Say my location equals Santa Barbara

46
00:01:49,30 --> 00:01:55,70
and my other location will be Los Angeles.

47
00:01:55,70 --> 00:01:57,70
And now I can ask a question of JavaScript.

48
00:01:57,70 --> 00:01:59,30
Are these the same?

49
00:01:59,30 --> 00:02:02,20
The way I can ask that question is by writing a statement

50
00:02:02,20 --> 00:02:03,60
and using three equals signs

51
00:02:03,60 --> 00:02:05,80
to tell me are these exactly the same?

52
00:02:05,80 --> 00:02:08,70
So, my location.

53
00:02:08,70 --> 00:02:12,40
With three equals signs in between my other location.

54
00:02:12,40 --> 00:02:17,30
So I'm saying, is this exactly the same as this?

55
00:02:17,30 --> 00:02:21,10
And you can see that the answer is false.

56
00:02:21,10 --> 00:02:23,10
JavaScript knows the values of these two variables

57
00:02:23,10 --> 00:02:25,40
and can tell me whether or not they're the same.

58
00:02:25,40 --> 00:02:27,10
I define them right next to each other myself

59
00:02:27,10 --> 00:02:28,80
so this is not all that exciting.

60
00:02:28,80 --> 00:02:30,10
But if there is a lot of other code

61
00:02:30,10 --> 00:02:32,30
in between the definition and the comparison

62
00:02:32,30 --> 00:02:34,00
where the values might change at some point,

63
00:02:34,00 --> 00:02:37,20
this can be a very useful question to get the answer to.

64
00:02:37,20 --> 00:02:44,10
So now if I change my other location to Santa Barbara,

65
00:02:44,10 --> 00:02:47,30
like this, and ask the same question again,

66
00:02:47,30 --> 00:02:50,80
we'll add the up arrow, I get back true.

67
00:02:50,80 --> 00:02:52,80
We're going to look at comparisons later,

68
00:02:52,80 --> 00:02:54,30
but one thing to know for now,

69
00:02:54,30 --> 00:02:57,90
variables are assigned with one equals sign

70
00:02:57,90 --> 00:02:59,80
and comparisons use more than one.

71
00:02:59,80 --> 00:03:02,90
This is a very popular typo that introduces fun little bugs

72
00:03:02,90 --> 00:03:07,10
in the programs of developers of all levels of experience.

73
00:03:07,10 --> 00:03:09,80
So that's a look at booleans, true and false,

74
00:03:09,80 --> 00:03:13,00
very useful for answering questions in your programs.

