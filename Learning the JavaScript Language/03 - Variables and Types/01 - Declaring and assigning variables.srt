1
00:00:00,50 --> 00:00:01,90
- [Instructor] In this video, we are going to talk about

2
00:00:01,90 --> 00:00:04,10
variables and how to make them.

3
00:00:04,10 --> 00:00:06,50
So here we are in the Chrome JavaScript console.

4
00:00:06,50 --> 00:00:09,60
I've made it really big by typing command equal sign

5
00:00:09,60 --> 00:00:11,30
a whole bunch of times.

6
00:00:11,30 --> 00:00:14,20
And I've changed the dock settings

7
00:00:14,20 --> 00:00:16,10
so it's over on the left side.

8
00:00:16,10 --> 00:00:17,90
And I've pretty much minimized the entire

9
00:00:17,90 --> 00:00:19,80
rest of the regular browser window away

10
00:00:19,80 --> 00:00:22,60
so we can concentrate on the console.

11
00:00:22,60 --> 00:00:24,50
A variable, as you may have heard in math class,

12
00:00:24,50 --> 00:00:26,70
is a place to keep things that are unknown.

13
00:00:26,70 --> 00:00:28,10
That's one way of looking at it.

14
00:00:28,10 --> 00:00:30,00
For our purposes, you can think of a variable

15
00:00:30,00 --> 00:00:31,60
as a box with a label on it.

16
00:00:31,60 --> 00:00:33,20
We can label the box whatever we want.

17
00:00:33,20 --> 00:00:35,40
And put pretty much whatever we want in there.

18
00:00:35,40 --> 00:00:39,20
And we end up with a convenient way to refer to some data.

19
00:00:39,20 --> 00:00:41,30
Remember, that as a programmer, you're in charge.

20
00:00:41,30 --> 00:00:44,00
The computer is going to do just what you tell it to do.

21
00:00:44,00 --> 00:00:45,90
Nothing more, nothing less.

22
00:00:45,90 --> 00:00:47,30
You should assume the computer, basically,

23
00:00:47,30 --> 00:00:48,10
doesn't know anything

24
00:00:48,10 --> 00:00:49,70
unless you tell it something.

25
00:00:49,70 --> 00:00:53,00
Variables are an important part of how we do this.

26
00:00:53,00 --> 00:00:54,20
In the course of programming, we're dealing with

27
00:00:54,20 --> 00:00:55,00
a lot unknowns.

28
00:00:55,00 --> 00:00:57,60
At least a lot of things that are unknown at first.

29
00:00:57,60 --> 00:00:59,40
We then use variables to accumulate the knowledge

30
00:00:59,40 --> 00:01:02,50
we need to accomplish whatever the goals of our program are.

31
00:01:02,50 --> 00:01:04,40
Here's an example.

32
00:01:04,40 --> 00:01:06,00
Say we want to write a function that's going to let

33
00:01:06,00 --> 00:01:07,80
someone draw a line on screen.

34
00:01:07,80 --> 00:01:09,20
Some of the things we don't know at first

35
00:01:09,20 --> 00:01:12,10
are the points where the line will start and end.

36
00:01:12,10 --> 00:01:13,50
We could have variables for those points

37
00:01:13,50 --> 00:01:15,60
and refer to them later by name when it comes time

38
00:01:15,60 --> 00:01:16,70
to draw the line.

39
00:01:16,70 --> 00:01:18,50
Okay, so that's the theory.

40
00:01:18,50 --> 00:01:20,40
Here's how it works.

41
00:01:20,40 --> 00:01:22,00
If we're approaching this the way you would approach

42
00:01:22,00 --> 00:01:23,30
it in math class, you'd have variables

43
00:01:23,30 --> 00:01:25,60
called things like x.

44
00:01:25,60 --> 00:01:26,60
That's enough to create a variable

45
00:01:26,60 --> 00:01:28,60
but it's an empty box right now.

46
00:01:28,60 --> 00:01:31,60
To give this variable x, a value, we add a space

47
00:01:31,60 --> 00:01:33,00
then an equal sign.

48
00:01:33,00 --> 00:01:35,00
And then we give it a value.

49
00:01:35,00 --> 00:01:39,00
So, we could type 32 and we end every line in JavaScript

50
00:01:39,00 --> 00:01:40,80
programming with a semicolon if we're being good.

51
00:01:40,80 --> 00:01:43,00
And by good, I mean JavaScript doesn't require it

52
00:01:43,00 --> 00:01:45,70
but semicolons help avoid confusion.

53
00:01:45,70 --> 00:01:48,20
What I've done here is called assigning a value

54
00:01:48,20 --> 00:01:49,20
to a variable.

55
00:01:49,20 --> 00:01:52,80
I'm assigning a value of 32 to the variable x.

56
00:01:52,80 --> 00:01:55,30
So now in the console, I hit Return.

57
00:01:55,30 --> 00:01:58,00
And what I've just typed is executed by the browser.

58
00:01:58,00 --> 00:02:01,00
Now, nothing here happens except the assignment.

59
00:02:01,00 --> 00:02:03,80
I get back the value of 32 in this case.

60
00:02:03,80 --> 00:02:05,40
But in the console I can type the name of the variable

61
00:02:05,40 --> 00:02:07,10
I just created again

62
00:02:07,10 --> 00:02:10,50
and do nothing else except end the line with a semicolon

63
00:02:10,50 --> 00:02:12,30
and I get back the value I just assigned.

64
00:02:12,30 --> 00:02:15,10
And in Chrome, it actually preemptively

65
00:02:15,10 --> 00:02:17,10
executes this for me and gives me a little preview

66
00:02:17,10 --> 00:02:20,20
of what will happen if I were to execute this.

67
00:02:20,20 --> 00:02:22,80
So I hit Return and there's 32 again.

68
00:02:22,80 --> 00:02:25,20
Calling things x is something that gets a lot of complaints

69
00:02:25,20 --> 00:02:26,40
in math classes.

70
00:02:26,40 --> 00:02:28,60
And we can certainly do better in our programs.

71
00:02:28,60 --> 00:02:30,00
Variables can be named almost anything

72
00:02:30,00 --> 00:02:32,80
that starts with a letter, underscore, or dollar sign.

73
00:02:32,80 --> 00:02:36,00
So I can create a variable called whereAmI.

74
00:02:36,00 --> 00:02:40,70
And set it to Santa Barbara, California,

75
00:02:40,70 --> 00:02:42,10
more or less.

76
00:02:42,10 --> 00:02:44,90
The part in quotes is a string, which we'll get to later,

77
00:02:44,90 --> 00:02:48,30
but that's how you store things like words or sentences.

78
00:02:48,30 --> 00:02:49,60
Now that I've stored a couple of variables,

79
00:02:49,60 --> 00:02:52,10
I can see their values and the JavaScript console

80
00:02:52,10 --> 00:02:54,00
now knows about my variables and will start to

81
00:02:54,00 --> 00:02:55,90
complete the names for me if I start to type them.

82
00:02:55,90 --> 00:02:57,00
Like this.

83
00:02:57,00 --> 00:02:58,80
Neat.

84
00:02:58,80 --> 00:03:02,10
I can also keep the label, but change what's in the box.

85
00:03:02,10 --> 00:03:04,50
So I can just do the assignment again

86
00:03:04,50 --> 00:03:09,20
with an equal sign and change x to 45.

87
00:03:09,20 --> 00:03:12,30
And I can change whereAmI,

88
00:03:12,30 --> 00:03:16,80
I start to type this, and then I complete it by hitting Tab.

89
00:03:16,80 --> 00:03:19,40
An equal sign and again, Chrome is previewing

90
00:03:19,40 --> 00:03:21,50
what's actually in there right now.

91
00:03:21,50 --> 00:03:24,80
I can change it to Los Angeles.

92
00:03:24,80 --> 00:03:28,40
Or, I could change it to 75.

93
00:03:28,40 --> 00:03:31,50
I did this by hitting up from a blank line

94
00:03:31,50 --> 00:03:34,60
to get the last thing that I typed.

95
00:03:34,60 --> 00:03:38,00
So whereAmI, set that to 75.

96
00:03:38,00 --> 00:03:40,60
I can set it to whatever I want.

97
00:03:40,60 --> 00:03:44,10
Now, of course, saying where I am is 75 doesn't make sense

98
00:03:44,10 --> 00:03:46,50
in real world logical terms

99
00:03:46,50 --> 00:03:48,50
but JavaScript doesn't care what I assign in there.

100
00:03:48,50 --> 00:03:51,30
We'll have a short digression on what that means later.

101
00:03:51,30 --> 00:03:53,10
So far I've been assigning variables

102
00:03:53,10 --> 00:03:55,20
just by typing the names.

103
00:03:55,20 --> 00:03:58,80
I can also type the keyword var to create variables.

104
00:03:58,80 --> 00:04:00,60
Generally speaking in actual code,

105
00:04:00,60 --> 00:04:02,30
outside the console this is what you'll do.

106
00:04:02,30 --> 00:04:04,20
So we'll do it here.

107
00:04:04,20 --> 00:04:05,90
I'm going to create several variables at once

108
00:04:05,90 --> 00:04:08,20
and I'll start by using the var keyword.

109
00:04:08,20 --> 00:04:11,00
And I'm going to create a few monsters.

110
00:04:11,00 --> 00:04:15,70
So we'll say monster1 equals Grover.

111
00:04:15,70 --> 00:04:17,90
Now I'll type a comma.

112
00:04:17,90 --> 00:04:22,80
Moster2 is Cookie Monster.

113
00:04:22,80 --> 00:04:28,90
Monster3 is Animal.

114
00:04:28,90 --> 00:04:31,00
End that with a semicolon.

115
00:04:31,00 --> 00:04:32,50
This just saves me a little bit of typing

116
00:04:32,50 --> 00:04:36,00
without having to do all these on separate lines.

117
00:04:36,00 --> 00:04:40,00
If I type each variables name and hit Return,

118
00:04:40,00 --> 00:04:44,30
I get exactly what I put in.

119
00:04:44,30 --> 00:04:45,60
A couple more details.

120
00:04:45,60 --> 00:04:48,00
Your variables can be named almost anything you want.

121
00:04:48,00 --> 00:04:49,90
And I recommend you use names that are descriptive

122
00:04:49,90 --> 00:04:51,70
because programs aren't just for computers.

123
00:04:51,70 --> 00:04:53,60
They're for other humans to read as well.

124
00:04:53,60 --> 00:04:55,40
A computer isn't going to care what you call your variables

125
00:04:55,40 --> 00:04:57,90
for the most part, but humans definitely will.

126
00:04:57,90 --> 00:04:59,70
So make sure you name your variables something

127
00:04:59,70 --> 00:05:01,50
that indicates the sort of thing that they're actually

128
00:05:01,50 --> 00:05:03,10
going to contain.

129
00:05:03,10 --> 00:05:05,20
If I were writing a program dealing with how much it costs

130
00:05:05,20 --> 00:05:07,30
to ship a package, for example, I might have variables

131
00:05:07,30 --> 00:05:11,00
named things like packageDimensions.

132
00:05:11,00 --> 00:05:13,20
Or taxRate.

133
00:05:13,20 --> 00:05:14,50
And so on.

134
00:05:14,50 --> 00:05:16,50
Starting a name with a lowercase letter and mashing on

135
00:05:16,50 --> 00:05:18,30
additional words with capital letters like this,

136
00:05:18,30 --> 00:05:21,30
is called camelCase.

137
00:05:21,30 --> 00:05:24,60
And it's a pretty common way to name things in JavaScript.

138
00:05:24,60 --> 00:05:27,10
There are a few words you can't use for your variable names

139
00:05:27,10 --> 00:05:29,20
because they're part of JavaScript itself.

140
00:05:29,20 --> 00:05:30,80
They're called reserved words.

141
00:05:30,80 --> 00:05:33,40
From what we've seen so far, you wouldn't be able to use

142
00:05:33,40 --> 00:05:36,70
var, because JavaScript already claimed it.

143
00:05:36,70 --> 00:05:38,60
If you're curious, there's a list of reserved words

144
00:05:38,60 --> 00:05:41,30
available on the Mozilla JavaScript reference site.

145
00:05:41,30 --> 00:05:43,20
And of course, if you're using a nice code editor,

146
00:05:43,20 --> 00:05:44,80
it will highlight those words for you so you won't

147
00:05:44,80 --> 00:05:46,10
accidentally use them.

148
00:05:46,10 --> 00:05:48,00
Chrome highlights them in purple here.

149
00:05:48,00 --> 00:05:50,60
Things like var or function.

150
00:05:50,60 --> 00:05:51,90
So that's an overview of variables

151
00:05:51,90 --> 00:05:54,30
which are the way we give names to data.

152
00:05:54,30 --> 00:05:56,50
If we're thinking of JavaScript like a spoken language,

153
00:05:56,50 --> 00:05:58,00
they're sort of like nouns.

