1
00:00:00,50 --> 00:00:01,80
- [Instructor] In this video, we're going to discover

2
00:00:01,80 --> 00:00:04,20
some things that JavaScript knows about strings

3
00:00:04,20 --> 00:00:06,70
and some tools we can use with them.

4
00:00:06,70 --> 00:00:08,70
Along with the data we've put in the quote marks,

5
00:00:08,70 --> 00:00:11,10
strings have a few tricks up their sleeve.

6
00:00:11,10 --> 00:00:13,50
We can access what are called properties and functions,

7
00:00:13,50 --> 00:00:16,10
also called methods, on any string.

8
00:00:16,10 --> 00:00:17,40
They're like a toolbox we can open

9
00:00:17,40 --> 00:00:19,10
to do things to our strings.

10
00:00:19,10 --> 00:00:20,80
A property is a single piece of data,

11
00:00:20,80 --> 00:00:23,30
like the length of a string, and a method

12
00:00:23,30 --> 00:00:25,10
is a bit of code to do something more involved,

13
00:00:25,10 --> 00:00:28,00
like convert string to uppercase.

14
00:00:28,00 --> 00:00:30,30
So I can assign a string to a variable,

15
00:00:30,30 --> 00:00:34,60
let's make one called myString.

16
00:00:34,60 --> 00:00:38,00
Say This is my string.

17
00:00:38,00 --> 00:00:40,70
Leave it alone.

18
00:00:40,70 --> 00:00:42,90
And now if I start to type myString

19
00:00:42,90 --> 00:00:45,80
and type a dot, I can see all kinds of information

20
00:00:45,80 --> 00:00:47,90
that's available on this string.

21
00:00:47,90 --> 00:00:50,20
So that includes the length, which you can see

22
00:00:50,20 --> 00:00:55,30
from this little preview is 34, I'll still hit return.

23
00:00:55,30 --> 00:00:57,60
So length is a property, and then a method would be,

24
00:00:57,60 --> 00:01:01,10
say, to convert this to uppercase, toUpperCase,

25
00:01:01,10 --> 00:01:03,00
I start to type that and it's completed,

26
00:01:03,00 --> 00:01:05,30
so I'll hit a right arrow to finish that off

27
00:01:05,30 --> 00:01:08,10
and type two parentheses to tell JavaScript that

28
00:01:08,10 --> 00:01:11,70
I want to execute that method, and actually do it.

29
00:01:11,70 --> 00:01:13,90
And that returns the same string,

30
00:01:13,90 --> 00:01:16,30
but with the whole thing turned into uppercase.

31
00:01:16,30 --> 00:01:17,90
It doesn't change the original value.

32
00:01:17,90 --> 00:01:21,10
So if I type myString, hit a right arrow, hit return,

33
00:01:21,10 --> 00:01:23,80
my original string is unchanged.

34
00:01:23,80 --> 00:01:26,50
Interestingly, you can do this without variables, too.

35
00:01:26,50 --> 00:01:28,30
JavaScript can take a string literal,

36
00:01:28,30 --> 00:01:31,40
like This is my string, and grant you access

37
00:01:31,40 --> 00:01:33,70
to the same properties and methods.

38
00:01:33,70 --> 00:01:35,40
It's much more common to do this with variables,

39
00:01:35,40 --> 00:01:37,70
but you'll occasionally see this usage as well.

40
00:01:37,70 --> 00:01:41,50
So I can see that this string is 17 characters long.

41
00:01:41,50 --> 00:01:43,20
All of JavaScript's built-in data types

42
00:01:43,20 --> 00:01:44,90
have properties and methods, and string

43
00:01:44,90 --> 00:01:47,10
has many, many more than we've covered here.

44
00:01:47,10 --> 00:01:50,00
But now you have some idea of what's available.

