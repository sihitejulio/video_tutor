1
00:00:00,50 --> 00:00:01,70
- [Instructor] In this video, we're going to talk

2
00:00:01,70 --> 00:00:03,10
about another of the basic types

3
00:00:03,10 --> 00:00:05,10
of data in JavaScript, the number.

4
00:00:05,10 --> 00:00:07,80
And we're also going to talk a little bit about math.

5
00:00:07,80 --> 00:00:09,20
You know what a number is.

6
00:00:09,20 --> 00:00:10,60
12 is a number.

7
00:00:10,60 --> 00:00:12,90
Some programming languages have different types of numbers,

8
00:00:12,90 --> 00:00:15,80
like integers, floats, doubles and so forth.

9
00:00:15,80 --> 00:00:17,20
All numbers in JavaScript

10
00:00:17,20 --> 00:00:20,10
are of the same type called Number.

11
00:00:20,10 --> 00:00:25,10
So, 12 in JavaScript is the same thing as 12.0,

12
00:00:25,10 --> 00:00:26,10
which makes sense in terms of how

13
00:00:26,10 --> 00:00:27,80
we think of numbers in the real world.

14
00:00:27,80 --> 00:00:29,30
In programming, that's not always the case,

15
00:00:29,30 --> 00:00:30,80
but in JavaScript, it is.

16
00:00:30,80 --> 00:00:32,60
They're both the same thing.

17
00:00:32,60 --> 00:00:35,90
You can have numbers with lots and lots of decimal points.

18
00:00:35,90 --> 00:00:39,20
Let's just add a whole bunch of decimal points to this guy.

19
00:00:39,20 --> 00:00:40,50
And you can see that at a certain point,

20
00:00:40,50 --> 00:00:42,60
it starts to get cut off.

21
00:00:42,60 --> 00:00:44,30
Although you can see I put this in with,

22
00:00:44,30 --> 00:00:45,80
I don't know how many digits this is.

23
00:00:45,80 --> 00:00:47,10
It's a lot of digits.

24
00:00:47,10 --> 00:00:48,40
After the decimal point,

25
00:00:48,40 --> 00:00:50,40
JavaScript can't handle that much precision.

26
00:00:50,40 --> 00:00:52,30
You probably won't care about that most of the time,

27
00:00:52,30 --> 00:00:55,00
unless you're getting into some pretty serious math.

28
00:00:55,00 --> 00:00:56,60
Along with regular numbers of course,

29
00:00:56,60 --> 00:00:58,90
you can have negative numbers, all that sort of thing.

30
00:00:58,90 --> 00:01:00,60
Negative 12 works.

31
00:01:00,60 --> 00:01:03,50
JavaScript can also represent infinity if you need it.

32
00:01:03,50 --> 00:01:05,80
There's a built-in keyword called Infinity

33
00:01:05,80 --> 00:01:07,50
with a capital letter.

34
00:01:07,50 --> 00:01:09,20
In JavaScript, infinity is any number

35
00:01:09,20 --> 00:01:12,40
that's larger than a very, very, very large upper bound.

36
00:01:12,40 --> 00:01:13,70
Odds are, you won't run into it,

37
00:01:13,70 --> 00:01:15,20
but there is such a thing in JavaScript

38
00:01:15,20 --> 00:01:19,30
as infinity and even negative infinity.

39
00:01:19,30 --> 00:01:23,00
There's also this, which stands for not a number.

40
00:01:23,00 --> 00:01:24,70
And this is what you get when you try to do math

41
00:01:24,70 --> 00:01:26,80
and the result, because of an error in your code,

42
00:01:26,80 --> 00:01:29,00
most of the time, or for various other reasons,

43
00:01:29,00 --> 00:01:31,90
you can end up with something which is not a number.

44
00:01:31,90 --> 00:01:33,20
Be on the lookout for this if you're trying

45
00:01:33,20 --> 00:01:34,80
to do any kind of math and you're starting

46
00:01:34,80 --> 00:01:37,30
to get unexpected behaviors.

47
00:01:37,30 --> 00:01:39,60
If you assign a number to a variable,

48
00:01:39,60 --> 00:01:45,00
let's call this myNumber.

49
00:01:45,00 --> 00:01:47,50
If you assign a number to a variable,

50
00:01:47,50 --> 00:01:50,10
like I have here with myNumber,

51
00:01:50,10 --> 00:01:52,40
and I'll retype this and type a dot,

52
00:01:52,40 --> 00:01:54,50
you can see that there are methods available on numbers

53
00:01:54,50 --> 00:01:55,70
just like there are with strings.

54
00:01:55,70 --> 00:01:57,80
Although, I don't find myself using these

55
00:01:57,80 --> 00:02:02,00
quite as often as I do with strings.

56
00:02:02,00 --> 00:02:03,90
More interesting than the methods that numbers have,

57
00:02:03,90 --> 00:02:06,70
is a built-in global object called Math.

58
00:02:06,70 --> 00:02:07,70
We'll talk about objects later,

59
00:02:07,70 --> 00:02:09,20
but this is like another toolbox

60
00:02:09,20 --> 00:02:11,20
with functions and methods for doing things to numbers,

61
00:02:11,20 --> 00:02:13,00
like rounding, chopping off decimal points,

62
00:02:13,00 --> 00:02:15,20
taking square roots, and so forth.

63
00:02:15,20 --> 00:02:17,70
So if I type math, capitalized like this,

64
00:02:17,70 --> 00:02:21,10
then a dot, that opens up the toolbox,

65
00:02:21,10 --> 00:02:25,20
and all of these methods and properties are shown.

66
00:02:25,20 --> 00:02:26,90
Next comes the method name and then I can send

67
00:02:26,90 --> 00:02:29,20
the number into the toolbox with parentheses.

68
00:02:29,20 --> 00:02:35,90
So, let's say I wanted to round a number.

69
00:02:35,90 --> 00:02:37,00
And so, that rounds this number

70
00:02:37,00 --> 00:02:41,10
with all this decimal precision down to 12.

71
00:02:41,10 --> 00:02:42,70
Or if I change the decimal point

72
00:02:42,70 --> 00:02:46,60
to something over .5, it rounds upward.

73
00:02:46,60 --> 00:02:48,00
Math has a lot of useful functions

74
00:02:48,00 --> 00:02:49,70
when working with numbers.

75
00:02:49,70 --> 00:02:51,70
Including one for generating a random number.

76
00:02:51,70 --> 00:02:53,30
Okay, technically pseudo-random,

77
00:02:53,30 --> 00:02:56,10
but good enough for our purpose.

78
00:02:56,10 --> 00:02:59,50
It's called Math.random.

79
00:02:59,50 --> 00:03:03,20
This generates a random number between zero and one.

80
00:03:03,20 --> 00:03:05,80
If I type up, I can execute it again

81
00:03:05,80 --> 00:03:08,30
and I get a different result every time.

82
00:03:08,30 --> 00:03:10,10
And you'll notice that the pre-execution

83
00:03:10,10 --> 00:03:12,90
that Chrome does gives me a different result

84
00:03:12,90 --> 00:03:15,50
than when I actually execute it later.

85
00:03:15,50 --> 00:03:16,80
It gives a different result every time,

86
00:03:16,80 --> 00:03:19,10
which is handy if you need some randomness in your program,

87
00:03:19,10 --> 00:03:22,30
like simulating the role of a die or something like that.

88
00:03:22,30 --> 00:03:23,90
This is not nearly everything that pertains

89
00:03:23,90 --> 00:03:25,50
to numbers in math in JavaScript.

90
00:03:25,50 --> 00:03:26,90
But now we know a little bit about them

91
00:03:26,90 --> 00:03:28,70
and can recognize them in code.

92
00:03:28,70 --> 00:03:30,10
We'll see a little bit more in this course,

93
00:03:30,10 --> 00:03:31,80
and of course, you can refer to the documentation

94
00:03:31,80 --> 00:03:34,00
on the Mozilla Developer Network for more.

