1
00:00:01,90 --> 00:00:04,00
- [Instructor] In the previous videos, we have replaced

2
00:00:04,00 --> 00:00:07,80
our HTTP server which we're using to serve our content

3
00:00:07,80 --> 00:00:09,60
with Webpack Dev Server.

4
00:00:09,60 --> 00:00:12,30
We've also looked at the basic web running

5
00:00:12,30 --> 00:00:15,80
our Webpack Dev Server as well as changing the scripts

6
00:00:15,80 --> 00:00:18,70
so we had set up in our package to JSON to use

7
00:00:18,70 --> 00:00:22,10
the Dev Server instead of the HTTP server

8
00:00:22,10 --> 00:00:25,40
to serve the project to the browser.

9
00:00:25,40 --> 00:00:30,00
In this video, we will be covering Webpack Dev Server again

10
00:00:30,00 --> 00:00:32,40
and we'll have a bit look at some more

11
00:00:32,40 --> 00:00:34,30
advanced configuration that is available

12
00:00:34,30 --> 00:00:36,20
for the Webpack Dev Server.

13
00:00:36,20 --> 00:00:40,30
First of all, we will look at the basic Dev Server config

14
00:00:40,30 --> 00:00:43,20
and we will twig some common parameters

15
00:00:43,20 --> 00:00:45,60
that you should probably always tweak

16
00:00:45,60 --> 00:00:50,10
whenever creating a Webpack Dev Server project.

17
00:00:50,10 --> 00:00:51,80
Then we'll have a look at a few

18
00:00:51,80 --> 00:00:54,40
of the more advanced features of Webpack servers.

19
00:00:54,40 --> 00:00:55,80
And towards the end of the video,

20
00:00:55,80 --> 00:00:58,40
we'll then look at hot loading

21
00:00:58,40 --> 00:01:02,40
and how we can hot load components with Webpack Dev Server

22
00:01:02,40 --> 00:01:07,50
and why we should do that as often as possible.

23
00:01:07,50 --> 00:01:11,90
So with that in mind, let's go to our Webpack config JS file

24
00:01:11,90 --> 00:01:17,20
and start tweaking our Dev Server configuration.

25
00:01:17,20 --> 00:01:18,40
If you have seen one installing

26
00:01:18,40 --> 00:01:21,50
the Webpack Dev Server package, it will automatically run.

27
00:01:21,50 --> 00:01:24,20
It does need a default configuration to exist.

28
00:01:24,20 --> 00:01:27,10
However, let's go into our Webpack config JS

29
00:01:27,10 --> 00:01:29,60
into our project and change that

30
00:01:29,60 --> 00:01:32,50
so we can actually configure our Dev Server

31
00:01:32,50 --> 00:01:35,10
to run the way we wanted.

32
00:01:35,10 --> 00:01:40,40
Into our config variable, we will go all the way to bottom

33
00:01:40,40 --> 00:01:43,80
between the module and plugins

34
00:01:43,80 --> 00:01:48,80
and we will add a new property which is called Dev Server.

35
00:01:48,80 --> 00:01:51,80
Webpack will know that this is the config object

36
00:01:51,80 --> 00:01:53,30
for our Dev Server

37
00:01:53,30 --> 00:01:57,20
so make sure not to forget the comma at the end.

38
00:01:57,20 --> 00:02:02,50
In here we will add a few property, a few basic properties.

39
00:02:02,50 --> 00:02:08,50
First one will be called contentBase.

40
00:02:08,50 --> 00:02:10,20
As you can imagine, this is the place

41
00:02:10,20 --> 00:02:12,60
where Webpack Dev Server will be looking

42
00:02:12,60 --> 00:02:16,20
for the files just served into the browser.

43
00:02:16,20 --> 00:02:18,20
Keep in mind as I said in our project,

44
00:02:18,20 --> 00:02:21,60
we're using the disc folder to serve all the content.

45
00:02:21,60 --> 00:02:24,60
However, in case if you're already using a different

46
00:02:24,60 --> 00:02:28,00
directory name, this is where you can specify.

47
00:02:28,00 --> 00:02:31,60
So in this case, if you remember we set our variable

48
00:02:31,60 --> 00:02:35,20
at the beginning which was build, build directory.

49
00:02:35,20 --> 00:02:37,40
So we will tell Webpack Dev Server to look

50
00:02:37,40 --> 00:02:39,80
into this directory for files.

51
00:02:39,80 --> 00:02:42,90
Keep in mind as I mentioned Webpack Dev Server

52
00:02:42,90 --> 00:02:43,80
runs of memory.

53
00:02:43,80 --> 00:02:45,60
However, if you have any static files

54
00:02:45,60 --> 00:02:48,50
that are in there in the Webpack Dev Server needs to load,

55
00:02:48,50 --> 00:02:52,00
it will be able to search for them in this directory.

56
00:02:52,00 --> 00:02:54,60
Another thing that you will want to enable

57
00:02:54,60 --> 00:02:56,90
is the compress property.

58
00:02:56,90 --> 00:02:58,30
So compress.

59
00:02:58,30 --> 00:02:59,90
And we will set that to true.

60
00:02:59,90 --> 00:03:02,50
This will make sure that whenever the Dev Server

61
00:03:02,50 --> 00:03:05,10
is sending file is out to when you use browser,

62
00:03:05,10 --> 00:03:08,20
it will always compress those files with the zip

63
00:03:08,20 --> 00:03:10,20
just so they load quicker over network.

64
00:03:10,20 --> 00:03:13,10
This is extremely useful if you're working in a company

65
00:03:13,10 --> 00:03:14,80
and you have a common Dev Server

66
00:03:14,80 --> 00:03:17,00
on which everybody works on.

67
00:03:17,00 --> 00:03:20,00
If your app is running within a Webpack Dev Server

68
00:03:20,00 --> 00:03:22,30
having compress will reduce load times

69
00:03:22,30 --> 00:03:25,00
for all your developers in the company.

70
00:03:25,00 --> 00:03:28,60
Also another thing that we can set in this object

71
00:03:28,60 --> 00:03:31,80
is the port on which the server will run.

72
00:03:31,80 --> 00:03:35,20
By default, it will always run on port 8080.

73
00:03:35,20 --> 00:03:38,50
However, sometimes you might have other applications

74
00:03:38,50 --> 00:03:41,10
running on your server that might be using the same port.

75
00:03:41,10 --> 00:03:43,80
So this is where you can configure the port.

76
00:03:43,80 --> 00:03:47,40
Let's configurate to 9000 for now.

77
00:03:47,40 --> 00:03:50,80
Let's run our project with Webpack Dev Server

78
00:03:50,80 --> 00:03:53,60
with this configuration object.

79
00:03:53,60 --> 00:03:55,60
So we will save our Webpack config.

80
00:03:55,60 --> 00:03:57,40
We will cancel our old build.

81
00:03:57,40 --> 00:04:01,00
And then we'll do yarn run dev.

82
00:04:01,00 --> 00:04:02,80
If you remember we've changed the Dev script

83
00:04:02,80 --> 00:04:05,60
to actually run the Webpack Dev Server now.

84
00:04:05,60 --> 00:04:09,00
Once we do this, Webpack will tell you

85
00:04:09,00 --> 00:04:12,30
that the project is running on local host 9000

86
00:04:12,30 --> 00:04:15,10
because we've changed the port.

87
00:04:15,10 --> 00:04:18,90
Then it will also tell you that the output is served

88
00:04:18,90 --> 00:04:21,90
from the base directory and any content

89
00:04:21,90 --> 00:04:24,00
that is not included in Webpack will be served

90
00:04:24,00 --> 00:04:28,00
from this folder which is specified in the configuration.

91
00:04:28,00 --> 00:04:30,10
Now we should go back into the browser.

92
00:04:30,10 --> 00:04:34,90
If we go to port 9000 on local host,

93
00:04:34,90 --> 00:04:39,60
you'll be able to see that the MyReads app load as expected.

94
00:04:39,60 --> 00:04:44,20
So we've set our Dev Server to load the MyReads app

95
00:04:44,20 --> 00:04:46,50
on a different port than default one.

96
00:04:46,50 --> 00:04:50,20
We're also compressing all the files that are coming in.

97
00:04:50,20 --> 00:04:53,70
As you can see, one thing you should not be worried about

98
00:04:53,70 --> 00:04:56,40
is you will probably get this topic

99
00:04:56,40 --> 00:04:59,10
and I wrote it most of the times to Dev Server

100
00:04:59,10 --> 00:05:00,70
because Dev Server does not know

101
00:05:00,70 --> 00:05:03,30
where to get the topic came from.

102
00:05:03,30 --> 00:05:05,10
You do not have to be alarmed about that.

103
00:05:05,10 --> 00:05:08,50
That will always happen whenever setting Dev Server up.

104
00:05:08,50 --> 00:05:10,40
Now those are the basic configurations

105
00:05:10,40 --> 00:05:13,30
that you've probably always want to change

106
00:05:13,30 --> 00:05:16,70
whenever adding the Dev Server to your project.

107
00:05:16,70 --> 00:05:19,10
Now let's have a look at a few other ones

108
00:05:19,10 --> 00:05:22,60
that are available for you.

109
00:05:22,60 --> 00:05:27,50
We'll just cover the most important ones that are available.

110
00:05:27,50 --> 00:05:29,50
First of all, let's go back to the config file

111
00:05:29,50 --> 00:05:32,10
and let's say you want to serve content

112
00:05:32,10 --> 00:05:33,60
from multiple directories,

113
00:05:33,60 --> 00:05:35,50
not just from your build directory.

114
00:05:35,50 --> 00:05:38,20
What you can do is you can change the content base.

115
00:05:38,20 --> 00:05:40,30
Instead of being one single string,

116
00:05:40,30 --> 00:05:42,00
you can make it an array.

117
00:05:42,00 --> 00:05:43,90
So for example, if you make this an array

118
00:05:43,90 --> 00:05:46,10
and let's say you have your build there

119
00:05:46,10 --> 00:05:49,80
and then you also have a public assets directory

120
00:05:49,80 --> 00:05:51,60
or something on a different server,

121
00:05:51,60 --> 00:05:53,40
for example, a CDN.

122
00:05:53,40 --> 00:05:57,70
We can do a path to join in here.

123
00:05:57,70 --> 00:06:00,90
Then you can do dirname.

124
00:06:00,90 --> 00:06:02,80
Then whatever the name of your folder is.

125
00:06:02,80 --> 00:06:04,90
So it can be, for example, assets.

126
00:06:04,90 --> 00:06:07,40
If you're keeping all your images and message folder,

127
00:06:07,40 --> 00:06:10,70
you can do this as well and Webpack Dev Server will now load

128
00:06:10,70 --> 00:06:13,00
those assets at the same time.

129
00:06:13,00 --> 00:06:15,30
For our project, we do not have such a folder

130
00:06:15,30 --> 00:06:18,60
so we'll just revert back to our build directory

131
00:06:18,60 --> 00:06:20,70
but keep in mind you can do that if you're using

132
00:06:20,70 --> 00:06:23,90
multiple folders to store your assets.

133
00:06:23,90 --> 00:06:25,90
Another property you need to be aware of

134
00:06:25,90 --> 00:06:28,80
is called disable host check.

135
00:06:28,80 --> 00:06:34,10
So disableHostCheck.

136
00:06:34,10 --> 00:06:36,30
You can set this to true or to false.

137
00:06:36,30 --> 00:06:37,50
By default, it's set to true.

138
00:06:37,50 --> 00:06:39,70
However, if you're using Webpack Dev Server

139
00:06:39,70 --> 00:06:45,90
to serve an application, this should always be set to false.

140
00:06:45,90 --> 00:06:48,10
It is always recommended to set it to false

141
00:06:48,10 --> 00:06:51,20
as apps that denote check host are usually vulnerable

142
00:06:51,20 --> 00:06:53,60
to DNS rebonding at that

143
00:06:53,60 --> 00:06:56,80
so make sure you set this property in your Dev Server

144
00:06:56,80 --> 00:07:00,20
if you're using it to serve your application to your users.

145
00:07:00,20 --> 00:07:03,10
Another property you can set with the Dev Server,

146
00:07:03,10 --> 00:07:05,00
again, this is for very specific cases

147
00:07:05,00 --> 00:07:06,40
but it's good to know about,

148
00:07:06,40 --> 00:07:09,80
is you can set the headers for the server.

149
00:07:09,80 --> 00:07:11,30
So you can set this the headers.

150
00:07:11,30 --> 00:07:14,50
The headers will be basically an object

151
00:07:14,50 --> 00:07:17,90
in which we can add a custom header that we need.

152
00:07:17,90 --> 00:07:22,60
So let's call our header X-Custom-header, for example,

153
00:07:22,60 --> 00:07:26,10
and the value of this header will be just custom.

154
00:07:26,10 --> 00:07:29,40
So whenever Webpack Dev Server is serving your files,

155
00:07:29,40 --> 00:07:36,00
it will automatically add these custom servers to the files.

156
00:07:36,00 --> 00:07:38,20
This is useful if you have applications we call

157
00:07:38,20 --> 00:07:42,90
authentication that requires specific headers as well.

158
00:07:42,90 --> 00:07:45,60
Another very useful feature for whenever working

159
00:07:45,60 --> 00:07:48,40
in development mode is the open property.

160
00:07:48,40 --> 00:07:51,60
If we set open to true, this will ensure that

161
00:07:51,60 --> 00:07:52,90
whenever you're running your project,

162
00:07:52,90 --> 00:07:54,30
whenever you're running your Dev Server,

163
00:07:54,30 --> 00:07:56,10
it will automatically open the top

164
00:07:56,10 --> 00:07:59,90
in your default browser with the page.

165
00:07:59,90 --> 00:08:03,60
So as I've mentioned, we've had these properties here.

166
00:08:03,60 --> 00:08:06,60
I will bring the headers just so I can run

167
00:08:06,60 --> 00:08:08,10
the application in my browser.

168
00:08:08,10 --> 00:08:10,60
I will let the disableHostCheck as false

169
00:08:10,60 --> 00:08:13,20
because that's usually a good idea to have it.

170
00:08:13,20 --> 00:08:14,80
I will set my Webpack config.

171
00:08:14,80 --> 00:08:16,50
Go back into the terminal

172
00:08:16,50 --> 00:08:20,10
and cancel the build and rerun it.

173
00:08:20,10 --> 00:08:23,10
As you can see, now what happened is

174
00:08:23,10 --> 00:08:27,30
a file has been automatically opened in my browser

175
00:08:27,30 --> 00:08:29,30
when I run the command.

176
00:08:29,30 --> 00:08:31,40
So let's do that again

177
00:08:31,40 --> 00:08:35,00
just to make sure everything is clear.

178
00:08:35,00 --> 00:08:40,30
So I will cancel the build and then run the app.

179
00:08:40,30 --> 00:08:44,00
When I run the app, it will automatically open

180
00:08:44,00 --> 00:08:45,50
the project in my browser.

181
00:08:45,50 --> 00:08:48,30
It will compile, it will bundle the whole project

182
00:08:48,30 --> 00:08:49,80
and serve it to my browser.

183
00:08:49,80 --> 00:08:52,40
As you can see, this is a very useful quality

184
00:08:52,40 --> 00:08:56,70
of life improvement that you can add to your Dev Server.

185
00:08:56,70 --> 00:09:00,10
Now the most powerful feature of the Webpack Dev Server

186
00:09:00,10 --> 00:09:02,80
is the ability to hot load react modules.

187
00:09:02,80 --> 00:09:06,30
So in order to do that, we will need to add another property

188
00:09:06,30 --> 00:09:09,60
in here called hot and then set this to true.

189
00:09:09,60 --> 00:09:12,40
If you leave it like this and you cancel the build process,

190
00:09:12,40 --> 00:09:14,90
this will not work by default.

191
00:09:14,90 --> 00:09:17,00
We need to do a bit more of additional

192
00:09:17,00 --> 00:09:18,60
configuration for this to work.

193
00:09:18,60 --> 00:09:20,80
So the site has been opened.

194
00:09:20,80 --> 00:09:23,30
However, it will not hot load any modules.

195
00:09:23,30 --> 00:09:26,30
If you refresh this, you can see have an arrow

196
00:09:26,30 --> 00:09:30,40
saying that hot module replacement is disabled.

197
00:09:30,40 --> 00:09:35,30
So let's have a look at how to quickly fix that issue.

198
00:09:35,30 --> 00:09:39,00
For that we will need to add

199
00:09:39,00 --> 00:09:41,70
a new configuration in our plugins.

200
00:09:41,70 --> 00:09:43,40
That is already included in Webpack

201
00:09:43,40 --> 00:09:46,10
so we should be able to just go all the way to the bottom

202
00:09:46,10 --> 00:09:48,20
into our plugins array.

203
00:09:48,20 --> 00:09:51,30
Then just create a new configuration in here.

204
00:09:51,30 --> 00:09:56,60
We will do a new and this will be part of Webpack

205
00:09:56,60 --> 00:09:59,30
so it's Webpack and then dots

206
00:09:59,30 --> 00:10:03,40
and it's called hot module replacement plugin.

207
00:10:03,40 --> 00:10:10,60
So HotModuleReplacementPlugin

208
00:10:10,60 --> 00:10:14,20
and then just do some brackets in the end.

209
00:10:14,20 --> 00:10:16,50
Make sure you save your config file.

210
00:10:16,50 --> 00:10:22,20
Now if we go back into our terminal, we rebuild our app,

211
00:10:22,20 --> 00:10:25,10
we refresh it, and you will see that the hot module

212
00:10:25,10 --> 00:10:28,20
replacements is enabled.

213
00:10:28,20 --> 00:10:31,60
Now as you can see there are few dependencies missing.

214
00:10:31,60 --> 00:10:34,00
So let's go fix those as well.

215
00:10:34,00 --> 00:10:35,10
Let's go into our terminal.

216
00:10:35,10 --> 00:10:36,80
See what the problem is.

217
00:10:36,80 --> 00:10:39,80
It says you cannot use chunkhash for chank

218
00:10:39,80 --> 00:10:41,00
in name of chankhash

219
00:10:41,00 --> 00:10:43,80
especially when using hot module replacement.

220
00:10:43,80 --> 00:10:47,40
So let's go fix this situation.

221
00:10:47,40 --> 00:10:51,90
It's just use, it recommends we only use hash instead.

222
00:10:51,90 --> 00:10:54,60
So let's use hash.

223
00:10:54,60 --> 00:10:57,00
Save it and run it again.

224
00:10:57,00 --> 00:10:58,60
Hopefully this time we will get

225
00:10:58,60 --> 00:11:01,30
the hot module replacement available.

226
00:11:01,30 --> 00:11:03,20
And everything compiled successfully

227
00:11:03,20 --> 00:11:04,90
and our page has been loaded.

228
00:11:04,90 --> 00:11:08,00
The reason you might be interested in using hot modules

229
00:11:08,00 --> 00:11:09,50
especially when developing reacts.

230
00:11:09,50 --> 00:11:14,90
It has a few advantages over just regular Dev Server config.

231
00:11:14,90 --> 00:11:17,90
First of all, it will let you retain the application state

232
00:11:17,90 --> 00:11:21,40
which is not the case when using the full reload

233
00:11:21,40 --> 00:11:24,70
which what Webpack Dev Server does by default.

234
00:11:24,70 --> 00:11:27,20
We'll also help you save a lot of time

235
00:11:27,20 --> 00:11:29,80
but only updating the components that changed.

236
00:11:29,80 --> 00:11:34,00
It also allows you to tweak styles much faster.

237
00:11:34,00 --> 00:11:37,00
It's almost comparable to changing the styles

238
00:11:37,00 --> 00:11:38,50
in the browser inspector.

239
00:11:38,50 --> 00:11:41,00
So let's have a look at that quickly.

240
00:11:41,00 --> 00:11:44,20
I'll just load the browser next to the editor

241
00:11:44,20 --> 00:11:46,10
and we'll have a look at changing those styles

242
00:11:46,10 --> 00:11:48,10
with the hot modules.

243
00:11:48,10 --> 00:11:51,10
Okay, so we've opened our app.css.

244
00:11:51,10 --> 00:11:53,10
I'm just going to change the color to be

245
00:11:53,10 --> 00:11:57,40
from red to green, the color of the title on the right.

246
00:11:57,40 --> 00:12:00,60
So what you can do is you can try this yourself as well.

247
00:12:00,60 --> 00:12:02,80
If you ended list books title,

248
00:12:02,80 --> 00:12:06,70
just find the background color and replace the green color

249
00:12:06,70 --> 00:12:10,40
with, let's say orange.

250
00:12:10,40 --> 00:12:12,80
The moment you save the file, I'll save the file.

251
00:12:12,80 --> 00:12:14,70
Just make sure you look at the browser

252
00:12:14,70 --> 00:12:17,70
and you will see that instead of refreshing,

253
00:12:17,70 --> 00:12:19,60
it just stop basing the style automatically.

254
00:12:19,60 --> 00:12:22,40
So that's one of the advantages using hot loader.

255
00:12:22,40 --> 00:12:26,50
It will not refresh unless, we'll not refresh everything

256
00:12:26,50 --> 00:12:27,70
unless everything is changed.

257
00:12:27,70 --> 00:12:30,80
So we only refresh to components that are changed.

258
00:12:30,80 --> 00:12:32,60
So going back to green.

259
00:12:32,60 --> 00:12:34,20
Again it just stop base the header.

260
00:12:34,20 --> 00:12:36,20
It does not do a full reload

261
00:12:36,20 --> 00:12:39,30
which is what the Dev Server not really does.

262
00:12:39,30 --> 00:12:42,10
So whenever working with react, I highly recommend

263
00:12:42,10 --> 00:12:46,10
having this hot module enabled at all times

264
00:12:46,10 --> 00:12:48,80
and make sure that whenever you're doing the output,

265
00:12:48,80 --> 00:12:53,10
you'll also do hash instead of chunkhash in here.

266
00:12:53,10 --> 00:12:55,60
Whenever you're deploying your app to production,

267
00:12:55,60 --> 00:12:58,00
you do not need to necessarily use the Dev Server

268
00:12:58,00 --> 00:13:00,90
as you're probably serving it from the different platform.

269
00:13:00,90 --> 00:13:04,10
So in that case, you can go back to chunkhash in here

270
00:13:04,10 --> 00:13:07,60
but for now while developing, this is the best,

271
00:13:07,60 --> 00:13:10,90
the quickest way to develop your react application.

272
00:13:10,90 --> 00:13:13,30
So in this video, we have looked at setting up

273
00:13:13,30 --> 00:13:17,00
our Webpack Dev Server and we have also looked

274
00:13:17,00 --> 00:13:20,40
at the most important features and the most advance features

275
00:13:20,40 --> 00:13:22,60
that you need Webpack Dev Server has

276
00:13:22,60 --> 00:13:24,50
including the hot module replacement

277
00:13:24,50 --> 00:13:26,90
which I always recommend having enabled.

278
00:13:26,90 --> 00:13:29,60
In the next video, we'll have a quick look

279
00:13:29,60 --> 00:13:32,60
at react router and code splitting

280
00:13:32,60 --> 00:13:34,70
and how Webpack can help with that

281
00:13:34,70 --> 00:13:37,90
and we'll also see a bit more about how code splitting

282
00:13:37,90 --> 00:13:41,10
with Webpack and react router will help decrease

283
00:13:41,10 --> 00:13:43,00
our load times and increase the performance

284
00:13:43,00 --> 00:13:45,00
of our application.

