1
00:00:00,70 --> 00:00:02,50
- [Alexandru] Hi, and welcome to the third section

2
00:00:02,50 --> 00:00:06,00
of boosting your React productivity with Webpack.

3
00:00:06,00 --> 00:00:08,00
In this section, we will be covering

4
00:00:08,00 --> 00:00:10,80
a few more quality-of-life issues that arise

5
00:00:10,80 --> 00:00:14,00
whenever using Webpack, in combination with React projects.

6
00:00:14,00 --> 00:00:16,80
First of all, we will discuss about

7
00:00:16,80 --> 00:00:18,50
how we want to serve our project,

8
00:00:18,50 --> 00:00:20,60
and we will find out the best way

9
00:00:20,60 --> 00:00:24,20
to serve and view project files while in development.

10
00:00:24,20 --> 00:00:26,40
In order to do that, we will have a look at

11
00:00:26,40 --> 00:00:29,10
a Webpack plug-in called Webpack Dev Server.

12
00:00:29,10 --> 00:00:31,40
So, we will have a look at what the Dev Server does

13
00:00:31,40 --> 00:00:32,50
and how it runs.

14
00:00:32,50 --> 00:00:34,60
And after that, we will have a look at

15
00:00:34,60 --> 00:00:36,40
how to configure it so that

16
00:00:36,40 --> 00:00:39,30
it will help us have a better workflow

17
00:00:39,30 --> 00:00:41,30
when developing React applications.

18
00:00:41,30 --> 00:00:42,40
Towards the end of this section,

19
00:00:42,40 --> 00:00:46,20
we'll also have a quick look at code splitting

20
00:00:46,20 --> 00:00:47,50
with React Router.

21
00:00:47,50 --> 00:00:48,60
So we will try again

22
00:00:48,60 --> 00:00:50,70
to reduce the load times of our application,

23
00:00:50,70 --> 00:00:53,60
and we'll have a look at how to refactor React components

24
00:00:53,60 --> 00:00:55,40
to load only when they're needed.

25
00:00:55,40 --> 00:00:58,00
With that in mind, let's jump straight

26
00:00:58,00 --> 00:00:59,50
into using the Dev Server

27
00:00:59,50 --> 00:01:02,50
and getting it set up for our project.

28
00:01:02,50 --> 00:01:05,30
First of all, we will be removing our HTTP server

29
00:01:05,30 --> 00:01:07,80
that we've been using so far to serve the content.

30
00:01:07,80 --> 00:01:10,20
Then, we will add our Webpack Dev Server,

31
00:01:10,20 --> 00:01:12,70
and we will make sure we update all the scripts

32
00:01:12,70 --> 00:01:15,30
in our project to reflect those changes.

33
00:01:15,30 --> 00:01:18,00
Before we jump into the coding and configuration,

34
00:01:18,00 --> 00:01:19,20
let's have a quick recap

35
00:01:19,20 --> 00:01:22,20
of how we've been serving content so far.

36
00:01:22,20 --> 00:01:25,40
So in here, we have a small diagram of

37
00:01:25,40 --> 00:01:27,40
how our system works at this point.

38
00:01:27,40 --> 00:01:30,40
So we have our dependencies libraries in here,

39
00:01:30,40 --> 00:01:33,60
we have our components that we've written in App.js.

40
00:01:33,60 --> 00:01:36,90
All those files, all those JavaScript modules and files

41
00:01:36,90 --> 00:01:38,30
go into Webpack,

42
00:01:38,30 --> 00:01:41,10
Webpack, then, will output a vendor.js file,

43
00:01:41,10 --> 00:01:44,60
which is our dependencies libraries,

44
00:01:44,60 --> 00:01:47,90
and we'll generate a bundle.js file,

45
00:01:47,90 --> 00:01:51,20
which Webpack will then place

46
00:01:51,20 --> 00:01:53,30
in this directory of our project.

47
00:01:53,30 --> 00:01:55,10
Now, in a separate process,

48
00:01:55,10 --> 00:01:58,30
we're also running our HTTP server,

49
00:01:58,30 --> 00:02:01,20
which constantly checks our dist folder

50
00:02:01,20 --> 00:02:03,60
to see if there have been any changes made.

51
00:02:03,60 --> 00:02:05,00
Once the change is loaded,

52
00:02:05,00 --> 00:02:07,40
then the client can refresh the browser,

53
00:02:07,40 --> 00:02:12,00
and you will receive the new files from HTTP server.

54
00:02:12,00 --> 00:02:13,80
So as we can see, at the moment,

55
00:02:13,80 --> 00:02:16,80
we have two processes running in parallel.

56
00:02:16,80 --> 00:02:18,90
So we have Webpack watching out files

57
00:02:18,90 --> 00:02:21,90
for any changes and make sure it generates

58
00:02:21,90 --> 00:02:26,00
new vendor.js and bundle.js whenever needed.

59
00:02:26,00 --> 00:02:28,40
And we also have our HTTP server,

60
00:02:28,40 --> 00:02:31,50
which constantly watches this folder,

61
00:02:31,50 --> 00:02:34,20
and then serves the files to the client.

62
00:02:34,20 --> 00:02:35,20
What we would like to do

63
00:02:35,20 --> 00:02:37,00
is simplify this process a little bit,

64
00:02:37,00 --> 00:02:39,10
and Webpack has the right tool

65
00:02:39,10 --> 00:02:42,60
that does exactly that, which is called Webpack Dev Server.

66
00:02:42,60 --> 00:02:45,50
So, the first part of our workflow

67
00:02:45,50 --> 00:02:47,10
will be the same as previously.

68
00:02:47,10 --> 00:02:49,50
We have our dependencies, we have our files,

69
00:02:49,50 --> 00:02:52,60
and they'll all get bundled together into Webpack.

70
00:02:52,60 --> 00:02:54,80
Now, one thing that Webpack has access to

71
00:02:54,80 --> 00:02:56,10
is the Webpack Dev Server.

72
00:02:56,10 --> 00:02:57,90
What Webpack Dev Server will do,

73
00:02:57,90 --> 00:02:59,90
it will run in parallel with Webpack.

74
00:02:59,90 --> 00:03:02,30
However, it will be part of the Webpack process,

75
00:03:02,30 --> 00:03:05,80
and this will allow Webpack to serve the files

76
00:03:05,80 --> 00:03:07,50
directly to the client's browser.

77
00:03:07,50 --> 00:03:08,90
This means that we can remove

78
00:03:08,90 --> 00:03:12,30
the extra HTTP server module from our project,

79
00:03:12,30 --> 00:03:13,70
which is no longer necessary

80
00:03:13,70 --> 00:03:16,40
because Webpack can perform the same function

81
00:03:16,40 --> 00:03:18,00
using the Dev Server.

82
00:03:18,00 --> 00:03:22,50
So, let's go back to our code files and do just that.

83
00:03:22,50 --> 00:03:25,70
So first of all, make sure you close the Dev Server.

84
00:03:25,70 --> 00:03:27,00
If it you have it running usually,

85
00:03:27,00 --> 00:03:29,70
just press Control+C, it will kill the server,

86
00:03:29,70 --> 00:03:32,20
and then, you will be able to work with it.

87
00:03:32,20 --> 00:03:34,60
So, let's firstly remove it from our application.

88
00:03:34,60 --> 00:03:37,50
This step is not necessary, but it's good practice

89
00:03:37,50 --> 00:03:39,10
to remove unwanted packages.

90
00:03:39,10 --> 00:03:41,50
So we will do yarn, remove,

91
00:03:41,50 --> 00:03:45,40
and then, we have our http-server.

92
00:03:45,40 --> 00:03:47,60
So let's get it out of our package,

93
00:03:47,60 --> 00:03:49,30
as we do not need it anymore.

94
00:03:49,30 --> 00:03:50,30
Once that is done,

95
00:03:50,30 --> 00:03:54,60
we can have a look into our Code Editor in package.json

96
00:03:54,60 --> 00:03:59,20
and the http-server will be removed from our dependencies.

97
00:03:59,20 --> 00:04:01,10
One thing we will also need to do is

98
00:04:01,10 --> 00:04:02,60
make sure we remove the script,

99
00:04:02,60 --> 00:04:04,40
as this package is no longer installed.

100
00:04:04,40 --> 00:04:06,40
So we do not need the script anymore.

101
00:04:06,40 --> 00:04:08,80
Let's make sure we do that as well.

102
00:04:08,80 --> 00:04:10,30
Now that we have removed our server,

103
00:04:10,30 --> 00:04:12,70
there is no way to serve our files to the client,

104
00:04:12,70 --> 00:04:14,60
so let's deal with that

105
00:04:14,60 --> 00:04:17,40
by quickly installing our Webpack Dev Server.

106
00:04:17,40 --> 00:04:18,60
So, in order to do that,

107
00:04:18,60 --> 00:04:22,00
you do yarn add webpack-dev-server,

108
00:04:22,00 --> 00:04:27,00
and then make sure you install it as a dev dependency.

109
00:04:27,00 --> 00:04:30,50
And once the package is installed, as you can see,

110
00:04:30,50 --> 00:04:32,40
it's been added to our project.

111
00:04:32,40 --> 00:04:35,80
What you can do in order to serve your application

112
00:04:35,80 --> 00:04:41,40
is go into node_modules,

113
00:04:41,40 --> 00:04:43,60
and then in bin,

114
00:04:43,60 --> 00:04:46,70
and then just run webpack-dev-server.

115
00:04:46,70 --> 00:04:49,00
This is where Webpack Dev Server is installed by default.

116
00:04:49,00 --> 00:04:50,70
So if we run this command,

117
00:04:50,70 --> 00:04:54,80
we'll see that our project is running at Local Host 8080.

118
00:04:54,80 --> 00:04:57,10
And then, Webpack will automatically

119
00:04:57,10 --> 00:04:59,90
run and build our project at this point.

120
00:04:59,90 --> 00:05:02,30
So, if we go back into the browser, as you can see,

121
00:05:02,30 --> 00:05:04,60
the page has just refreshed,

122
00:05:04,60 --> 00:05:08,20
and we have access to our project in the browser

123
00:05:08,20 --> 00:05:12,20
the same way we did with the HTTP server.

124
00:05:12,20 --> 00:05:15,00
The advantage of doing this is, as you can see,

125
00:05:15,00 --> 00:05:19,20
Webpack is being run by default in the watch mode,

126
00:05:19,20 --> 00:05:21,80
so we do not have to set that command anymore.

127
00:05:21,80 --> 00:05:25,40
So if we make any changes to our application,

128
00:05:25,40 --> 00:05:27,10
let's say we change the Home,

129
00:05:27,10 --> 00:05:29,30
or let's change the SearchPage,

130
00:05:29,30 --> 00:05:31,80
let's just add some text somewhere in it.

131
00:05:31,80 --> 00:05:34,30
Call this Back instead of Close.

132
00:05:34,30 --> 00:05:36,60
And when we go into the browser,

133
00:05:36,60 --> 00:05:39,20
you will see Webpack Dev Server will automatically

134
00:05:39,20 --> 00:05:42,60
reload the page, and when we go into here,

135
00:05:42,60 --> 00:05:47,80
we can go, it will automatically update the page

136
00:05:47,80 --> 00:05:50,70
or the component you want to work with.

137
00:05:50,70 --> 00:05:52,40
So, that is a huge advantage.

138
00:05:52,40 --> 00:05:54,60
We do not have to refresh the page anymore.

139
00:05:54,60 --> 00:05:58,60
I'll add this browser to my Editor window,

140
00:05:58,60 --> 00:06:02,60
just so you can see exactly how it works in real time.

141
00:06:02,60 --> 00:06:07,20
Let me just quickly close the Inspector,

142
00:06:07,20 --> 00:06:12,40
and you will see that once I make a change in my code,

143
00:06:12,40 --> 00:06:14,80
the page will refresh automatically.

144
00:06:14,80 --> 00:06:19,10
So, let's change something on the Home screen.

145
00:06:19,10 --> 00:06:21,70
We'll call this MyRead app again,

146
00:06:21,70 --> 00:06:23,30
and once you save,

147
00:06:23,30 --> 00:06:25,40
you'll see the browser reloads automatically

148
00:06:25,40 --> 00:06:27,00
and updates your changes.

149
00:06:27,00 --> 00:06:28,80
Now, this is the most basic way

150
00:06:28,80 --> 00:06:30,40
of running Webpack Dev Server.

151
00:06:30,40 --> 00:06:32,40
However, just typing that command

152
00:06:32,40 --> 00:06:34,40
every time in the terminal might not be ideal.

153
00:06:34,40 --> 00:06:36,00
So, as you can imagine,

154
00:06:36,00 --> 00:06:39,50
we can write a script in our package.json

155
00:06:39,50 --> 00:06:41,60
that will handle this command.

156
00:06:41,60 --> 00:06:43,70
So, going back into our package.json,

157
00:06:43,70 --> 00:06:46,30
what I usually do is replace the dev script

158
00:06:46,30 --> 00:06:48,20
whenever I have a dev script,

159
00:06:48,20 --> 00:06:50,50
with my Webpack Dev Server.

160
00:06:50,50 --> 00:06:51,90
So, let's do just that.

161
00:06:51,90 --> 00:06:54,90
We will type in webpack-dev-server in here.

162
00:06:54,90 --> 00:06:57,10
And now, if we go into our terminal,

163
00:06:57,10 --> 00:06:58,60
if we close the build

164
00:06:58,60 --> 00:07:02,20
and just do yarn run dev,

165
00:07:02,20 --> 00:07:04,10
it will automatically build the project,

166
00:07:04,10 --> 00:07:07,10
run the Dev Server, and serve the application

167
00:07:07,10 --> 00:07:09,10
into our browser.

168
00:07:09,10 --> 00:07:11,60
So, as we can see, we have the application running

169
00:07:11,60 --> 00:07:15,00
on Local Host 8080.

170
00:07:15,00 --> 00:07:16,50
One thing to keep in mind is that

171
00:07:16,50 --> 00:07:18,50
when you're using the Webpack Dev Server,

172
00:07:18,50 --> 00:07:21,40
the files are not being served off the hard drive,

173
00:07:21,40 --> 00:07:23,60
so the build process never happens.

174
00:07:23,60 --> 00:07:27,10
Webpack Dev Server keeps all the bundle.js and the vendor.js

175
00:07:27,10 --> 00:07:30,10
and all the other files generated by Webpack in memory.

176
00:07:30,10 --> 00:07:33,70
So you will not have those files into dist.

177
00:07:33,70 --> 00:07:37,90
So, if we go in here, and we just run our clean script,

178
00:07:37,90 --> 00:07:40,90
which just removes the dist folder,

179
00:07:40,90 --> 00:07:43,00
so if we go into terminal,

180
00:07:43,00 --> 00:07:47,60
let us run yarn run clean, and, as you can see,

181
00:07:47,60 --> 00:07:50,70
it has removed our dist directory from the project.

182
00:07:50,70 --> 00:07:55,30
However, if we refresh our application, it is still running.

183
00:07:55,30 --> 00:07:57,20
That's because Webpack Dev Server

184
00:07:57,20 --> 00:07:59,40
does not load files off the dist,

185
00:07:59,40 --> 00:08:02,30
it loads them from memory, and it will always generate them.

186
00:08:02,30 --> 00:08:03,40
Since we've done that,

187
00:08:03,40 --> 00:08:07,20
we also will need to create a build script

188
00:08:07,20 --> 00:08:09,80
that will allow us to actually generate those files

189
00:08:09,80 --> 00:08:13,10
in order for us to be able to distribute them.

190
00:08:13,10 --> 00:08:16,50
So, let's do a build script as well.

191
00:08:16,50 --> 00:08:21,20
And in this build script, we'll just run webpack,

192
00:08:21,20 --> 00:08:24,10
and we will add a slash d,

193
00:08:24,10 --> 00:08:26,80
'cause we're running it in development.

194
00:08:26,80 --> 00:08:28,80
And that's pretty much the basics of

195
00:08:28,80 --> 00:08:30,60
configuring Webpack Dev Server.

196
00:08:30,60 --> 00:08:32,70
As you can see, the install is really simple,

197
00:08:32,70 --> 00:08:35,60
and it can help you replace other packages

198
00:08:35,60 --> 00:08:36,80
that you might have been using

199
00:08:36,80 --> 00:08:39,30
for serving the content before,

200
00:08:39,30 --> 00:08:41,80
such as, in our case, the HTTP server.

201
00:08:41,80 --> 00:08:44,40
Of course, this is just the basic way

202
00:08:44,40 --> 00:08:45,70
of running the Dev Server,

203
00:08:45,70 --> 00:08:47,90
and it can also be configured

204
00:08:47,90 --> 00:08:50,50
to perform some more advanced tasks as well

205
00:08:50,50 --> 00:08:51,80
and running it,

206
00:08:51,80 --> 00:08:55,90
which is exactly what we will discuss in our next video,

207
00:08:55,90 --> 00:09:00,00
which is best practices when using Dev Server.

