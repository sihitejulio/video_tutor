1
00:00:00,80 --> 00:00:01,70
- [Instructor] In the previous video

2
00:00:01,70 --> 00:00:04,00
we have looked at how to set up our dev-server

3
00:00:04,00 --> 00:00:06,30
and how to configure our dev-server

4
00:00:06,30 --> 00:00:10,00
to make our development cycle much easier.

5
00:00:10,00 --> 00:00:12,20
In this video we'll have a more in-depth look

6
00:00:12,20 --> 00:00:15,40
at react router and code splitting

7
00:00:15,40 --> 00:00:19,10
and why this is important to have in your application.

8
00:00:19,10 --> 00:00:21,30
The topics we're going to cover in this video

9
00:00:21,30 --> 00:00:22,50
are lazy loading.

10
00:00:22,50 --> 00:00:25,00
We'll discuss about a bit about what lazy loading is

11
00:00:25,00 --> 00:00:26,10
and why it is used.

12
00:00:26,10 --> 00:00:28,90
Then we will have a look at react-loadable components

13
00:00:28,90 --> 00:00:31,90
and changing the routes in our application

14
00:00:31,90 --> 00:00:35,40
so that they load lazily into our app.

15
00:00:35,40 --> 00:00:38,40
First of all, let's quickly talk about what lazy loading is.

16
00:00:38,40 --> 00:00:42,70
Lazy loading is process available in web development

17
00:00:42,70 --> 00:00:44,90
and very popular in the React world

18
00:00:44,90 --> 00:00:47,70
in which you only load components when you need them.

19
00:00:47,70 --> 00:00:50,40
So remember when we spoke about our bundles,

20
00:00:50,40 --> 00:00:53,10
everything gets put together by webpack

21
00:00:53,10 --> 00:00:54,80
into a bundle.js.

22
00:00:54,80 --> 00:00:56,20
What lazy loading will do,

23
00:00:56,20 --> 00:00:58,20
it will fragment that bundle.js

24
00:00:58,20 --> 00:01:00,20
into multiple smaller files

25
00:01:00,20 --> 00:01:02,40
that are only loaded when needed.

26
00:01:02,40 --> 00:01:04,60
For example, if you have two

27
00:01:04,60 --> 00:01:06,50
or three routes in your application,

28
00:01:06,50 --> 00:01:09,80
only one of the routes will be displayed at one point,

29
00:01:09,80 --> 00:01:13,00
so there is no reason to load everything ahead of time.

30
00:01:13,00 --> 00:01:15,00
Whenever a user clicks on the link

31
00:01:15,00 --> 00:01:17,10
and that takes him to a different route,

32
00:01:17,10 --> 00:01:19,10
you can then lazily load that component

33
00:01:19,10 --> 00:01:21,00
and display to the user.

34
00:01:21,00 --> 00:01:22,60
This will make sure that whenever you're

35
00:01:22,60 --> 00:01:24,90
serving the application for the first time,

36
00:01:24,90 --> 00:01:28,10
loading times will be shorter

37
00:01:28,10 --> 00:01:31,60
and the app will only load what is displayed on the screen.

38
00:01:31,60 --> 00:01:34,60
So let's have a look at how we can apply this technique

39
00:01:34,60 --> 00:01:36,80
to our project.

40
00:01:36,80 --> 00:01:38,30
Going back to our code editor,

41
00:01:38,30 --> 00:01:40,40
there are a few changes we need to make.

42
00:01:40,40 --> 00:01:43,90
We need to install a few packages from yarn.

43
00:01:43,90 --> 00:01:46,10
So let's go into our terminal

44
00:01:46,10 --> 00:01:51,10
and let us install the following two packages.

45
00:01:51,10 --> 00:01:55,30
The first one we need install is a package called

46
00:01:55,30 --> 00:01:58,80
babel-plugin-syntax-dynamic-import.

47
00:01:58,80 --> 00:02:00,60
I know it's quite a long name,

48
00:02:00,60 --> 00:02:03,00
but that's how it's called.

49
00:02:03,00 --> 00:02:05,50
So we will add this with yarn.

50
00:02:05,50 --> 00:02:07,40
And then we also need to make sure

51
00:02:07,40 --> 00:02:12,50
that this is added as an development dependency.

52
00:02:12,50 --> 00:02:13,90
Once this is installed,

53
00:02:13,90 --> 00:02:18,60
we will also need to add another package,

54
00:02:18,60 --> 00:02:21,10
which is called react-loadable.

55
00:02:21,10 --> 00:02:23,20
Much easier name to remember.

56
00:02:23,20 --> 00:02:27,30
Just wait a bit for the syntax-dynamic-import to install.

57
00:02:27,30 --> 00:02:31,60
So now we will need to add the react-loadable.

58
00:02:31,60 --> 00:02:34,90
We need to add this as a project dependency,

59
00:02:34,90 --> 00:02:36,50
so this will not be a dev dependency

60
00:02:36,50 --> 00:02:41,00
so just add it like this.

61
00:02:41,00 --> 00:02:44,20
And those are the two packages we need to install.

62
00:02:44,20 --> 00:02:48,60
Now let's go back into our webpack.config.js

63
00:02:48,60 --> 00:02:51,10
and do a few tweaks to the config file

64
00:02:51,10 --> 00:02:53,80
in order to make sure we can take advantage

65
00:02:53,80 --> 00:02:57,70
of both the babel-plugin-syntax-dynamic-import

66
00:02:57,70 --> 00:03:01,60
as well as the react-loadable module.

67
00:03:01,60 --> 00:03:03,50
So into our webpack.config

68
00:03:03,50 --> 00:03:06,60
where we are testing for, if you remember,

69
00:03:06,60 --> 00:03:08,20
the loader, the babel-loader,

70
00:03:08,20 --> 00:03:10,20
where we have configured a bit.

71
00:03:10,20 --> 00:03:15,30
We'll make a few changes to the way this rule works.

72
00:03:15,30 --> 00:03:16,20
So first of all,

73
00:03:16,20 --> 00:03:18,10
let me just remove this commented line

74
00:03:18,10 --> 00:03:19,90
as we do not need it any more.

75
00:03:19,90 --> 00:03:23,70
We will change the use with loader.

76
00:03:23,70 --> 00:03:25,90
So they are equivalents, using loader,

77
00:03:25,90 --> 00:03:29,70
however loader will always just specify just one loader

78
00:03:29,70 --> 00:03:31,70
instead of allowing you to use either

79
00:03:31,70 --> 00:03:32,90
a loader or an array

80
00:03:32,90 --> 00:03:35,80
the same way we're doing with the styles loader.

81
00:03:35,80 --> 00:03:39,30
And we will add another property called options.

82
00:03:39,30 --> 00:03:43,20
So we will give babel-loader a set of options.

83
00:03:43,20 --> 00:03:46,30
First of all, we will disable our babelrc file.

84
00:03:46,30 --> 00:03:49,30
If you remember we had that set up in the beginning.

85
00:03:49,30 --> 00:03:52,10
We will disable it at this point,

86
00:03:52,10 --> 00:03:56,70
babelrc will do false.

87
00:03:56,70 --> 00:04:01,30
So this way, babel will not look at the babelrc file.

88
00:04:01,30 --> 00:04:03,70
Then we will do our presets.

89
00:04:03,70 --> 00:04:05,90
If you remember we had some presets

90
00:04:05,90 --> 00:04:09,50
configured in here which were babel-preset-env,

91
00:04:09,50 --> 00:04:10,90
react and stage-2.

92
00:04:10,90 --> 00:04:14,20
So we can happily copy this from here and here.

93
00:04:14,20 --> 00:04:16,60
So as you can see, basically we're just rewriting

94
00:04:16,60 --> 00:04:17,70
this config object,

95
00:04:17,70 --> 00:04:20,00
but we are doing it inside of webpack

96
00:04:20,00 --> 00:04:22,80
instead of doing it in some separate file.

97
00:04:22,80 --> 00:04:25,40
And we will add one more thing in here.

98
00:04:25,40 --> 00:04:28,00
So this is supposed to be an array, first of all.

99
00:04:28,00 --> 00:04:31,90
And we will add one more thing called plugins.

100
00:04:31,90 --> 00:04:35,40
So each loader has access to various plugins as well

101
00:04:35,40 --> 00:04:41,80
and this one will be syntax-dynamic-import.

102
00:04:41,80 --> 00:04:44,70
That is how this plugin is called.

103
00:04:44,70 --> 00:04:46,50
Once we have this, make sure you save

104
00:04:46,50 --> 00:04:48,70
your webpack.config.js file.

105
00:04:48,70 --> 00:04:50,40
Let's run our dev script.

106
00:04:50,40 --> 00:04:51,70
For now nothing will happen

107
00:04:51,70 --> 00:04:53,50
because we haven't changed our React code.

108
00:04:53,50 --> 00:04:57,90
But we can do that as soon as the server is running.

109
00:04:57,90 --> 00:05:01,80
And let's make sure our browser is loading as well.

110
00:05:01,80 --> 00:05:03,40
So we have our project running.

111
00:05:03,40 --> 00:05:05,90
As you can see, we have no error at the moment.

112
00:05:05,90 --> 00:05:07,90
So now let's have a look at how we can

113
00:05:07,90 --> 00:05:10,90
split the code in our application

114
00:05:10,90 --> 00:05:12,90
using those packages and configurations

115
00:05:12,90 --> 00:05:14,90
we've just added in webpack.

116
00:05:14,90 --> 00:05:16,40
So we will go first of all,

117
00:05:16,40 --> 00:05:18,90
we will need to create a loading component

118
00:05:18,90 --> 00:05:22,10
that will be displayed while our component is loading.

119
00:05:22,10 --> 00:05:24,40
So we will create a new file.

120
00:05:24,40 --> 00:05:28,60
We will call this one Loading.js.

121
00:05:28,60 --> 00:05:31,50
Let's just call it Loading.js to make it easy.

122
00:05:31,50 --> 00:05:33,40
And in here I'm just going to create

123
00:05:33,40 --> 00:05:35,20
a basic React component.

124
00:05:35,20 --> 00:05:36,90
You can feel free to pause the video

125
00:05:36,90 --> 00:05:38,20
and just copy the code.

126
00:05:38,20 --> 00:05:41,50
If not it will be available in the archive as well.

127
00:05:41,50 --> 00:05:43,40
So basically what we are doing,

128
00:05:43,40 --> 00:05:45,30
we are just creating a function

129
00:05:45,30 --> 00:05:47,10
that takes a few properties in

130
00:05:47,10 --> 00:05:50,10
isLoading, delay and error.

131
00:05:50,10 --> 00:05:52,00
And if isLoading and there's a delay,

132
00:05:52,00 --> 00:05:53,20
it will display Loading.

133
00:05:53,20 --> 00:05:55,20
If not, if the component cannot be loaded,

134
00:05:55,20 --> 00:05:57,60
it will just display Error!

135
00:05:57,60 --> 00:05:59,30
So we will save this

136
00:05:59,30 --> 00:06:00,50
and now what we will do,

137
00:06:00,50 --> 00:06:04,90
we will go into our app.js where our routes are located.

138
00:06:04,90 --> 00:06:07,20
As you can see, we are using routes

139
00:06:07,20 --> 00:06:08,80
from react-router-dom.

140
00:06:08,80 --> 00:06:12,80
This project is using react-router version four.

141
00:06:12,80 --> 00:06:14,60
So if you're using a different version

142
00:06:14,60 --> 00:06:17,20
there are other ways of doing this as well.

143
00:06:17,20 --> 00:06:21,00
However for react-router four, this is the best solution.

144
00:06:21,00 --> 00:06:28,10
And we are going to import Loadable from react-loadable.

145
00:06:28,10 --> 00:06:29,00
Save it.

146
00:06:29,00 --> 00:06:31,70
We are going to also import path.

147
00:06:31,70 --> 00:06:34,80
Remember we used import path in the webpack.config as well.

148
00:06:34,80 --> 00:06:38,00
Now we will import path as well.

149
00:06:38,00 --> 00:06:40,30
And we will need to import our newly created

150
00:06:40,30 --> 00:06:41,70
Loading component as well.

151
00:06:41,70 --> 00:06:45,30
So we will do import Loading from

152
00:06:45,30 --> 00:06:49,60
and this is located in ./Components/Loading.

153
00:06:49,60 --> 00:06:50,50
There we go.

154
00:06:50,50 --> 00:06:54,30
So we have imported everything we need.

155
00:06:54,30 --> 00:06:57,10
Now one thing we can also do at this point,

156
00:06:57,10 --> 00:06:59,60
we need to do at this point,

157
00:06:59,60 --> 00:07:02,10
decide which of those components here

158
00:07:02,10 --> 00:07:05,40
we want to lazy load.

159
00:07:05,40 --> 00:07:07,10
So looking through the code,

160
00:07:07,10 --> 00:07:09,30
home should not be lazily loaded

161
00:07:09,30 --> 00:07:10,90
because home will always be displayed

162
00:07:10,90 --> 00:07:13,10
when the user enters our site.

163
00:07:13,10 --> 00:07:15,90
So what we can do is we can do the SearchPage

164
00:07:15,90 --> 00:07:18,80
and the BookDetails and lazy load those.

165
00:07:18,80 --> 00:07:20,10
So if we go in here,

166
00:07:20,10 --> 00:07:21,90
you see when you click on more,

167
00:07:21,90 --> 00:07:23,50
since there is an error because

168
00:07:23,50 --> 00:07:25,90
we haven't finished the code,

169
00:07:25,90 --> 00:07:28,20
just one quick second.

170
00:07:28,20 --> 00:07:30,10
Let's fix that error.

171
00:07:30,10 --> 00:07:31,90
It seems I've misspelled from

172
00:07:31,90 --> 00:07:35,30
so fix that quickly.

173
00:07:35,30 --> 00:07:38,10
Go back, as you can see webpack's compiling.

174
00:07:38,10 --> 00:07:40,60
And let's go back to our localhost

175
00:07:40,60 --> 00:07:43,50
and if we click on more we will end up on this page,

176
00:07:43,50 --> 00:07:45,10
which loads just one book.

177
00:07:45,10 --> 00:07:47,40
So we want to make this page load

178
00:07:47,40 --> 00:07:51,20
only after the user navigates to this place.

179
00:07:51,20 --> 00:07:53,90
To do that, we will create a

180
00:07:53,90 --> 00:07:55,70
loadable BookDetails.

181
00:07:55,70 --> 00:07:58,40
So we will take this BookDetails from here.

182
00:07:58,40 --> 00:08:03,20
We will create a new variable called

183
00:08:03,20 --> 00:08:06,10
loadableBookDetails.

184
00:08:06,10 --> 00:08:08,80
It is up to you how you name those components.

185
00:08:08,80 --> 00:08:11,20
I do prefer keeping loadable

186
00:08:11,20 --> 00:08:13,90
and then the name of the component that I will load,

187
00:08:13,90 --> 00:08:16,70
but again, it is up to you how you want to do it.

188
00:08:16,70 --> 00:08:18,40
And we will do Loadable

189
00:08:18,40 --> 00:08:22,10
and then add a config object inside Loadable

190
00:08:22,10 --> 00:08:24,90
which will contain the following things.

191
00:08:24,90 --> 00:08:27,50
So first of all, we do a loader,

192
00:08:27,50 --> 00:08:29,30
which is what we want to load.

193
00:08:29,30 --> 00:08:31,90
This is going to be a function.

194
00:08:31,90 --> 00:08:36,70
And this function is going to use the import statement.

195
00:08:36,70 --> 00:08:42,00
And in here we will be importing our

196
00:08:42,00 --> 00:08:48,40
BookDetails component, so ./Component/BookDetails.

197
00:08:48,40 --> 00:08:50,60
And this is Components.

198
00:08:50,60 --> 00:08:53,60
And we will also need to provide

199
00:08:53,60 --> 00:08:56,90
the loadableBookDetails with one more thing,

200
00:08:56,90 --> 00:09:01,30
which is what it will display while loading the components.

201
00:09:01,30 --> 00:09:02,90
And loading, if you remember,

202
00:09:02,90 --> 00:09:05,60
we've just created our Loading component at the top.

203
00:09:05,60 --> 00:09:11,10
So we will add that into our object here.

204
00:09:11,10 --> 00:09:12,50
Now the last thing we need to do

205
00:09:12,50 --> 00:09:14,70
is wherever we're using our BookDetails,

206
00:09:14,70 --> 00:09:18,20
replace with with the loadableBookDetails.

207
00:09:18,20 --> 00:09:21,80
So let's go down and find our routes.

208
00:09:21,80 --> 00:09:25,00
As you can see, this is the SearchPage route

209
00:09:25,00 --> 00:09:27,20
and this is the BookDetails route.

210
00:09:27,20 --> 00:09:30,70
So we're replacing what was in component

211
00:09:30,70 --> 00:09:35,10
with the new loadableBookDetails that we have set.

212
00:09:35,10 --> 00:09:37,60
Now as you can see, everything compiles

213
00:09:37,60 --> 00:09:42,00
and if we go back into our browser, refresh the page,

214
00:09:42,00 --> 00:09:44,40
if we look at the network tab,

215
00:09:44,40 --> 00:09:46,40
when we click on more,

216
00:09:46,40 --> 00:09:50,70
you will see it will load a new bundle,

217
00:09:50,70 --> 00:09:56,60
a new JavaScript file which represents the BookDetails page.

218
00:09:56,60 --> 00:09:58,20
What we can also do at this point

219
00:09:58,20 --> 00:10:00,10
is go back into our application

220
00:10:00,10 --> 00:10:03,10
and actually remove our BookDetails from here.

221
00:10:03,10 --> 00:10:05,00
This will most likely cause an error,

222
00:10:05,00 --> 00:10:07,40
because webpack will not know, as I'm saying,

223
00:10:07,40 --> 00:10:09,80
webpack will not know where to find this

224
00:10:09,80 --> 00:10:11,20
new JavaScript file from.

225
00:10:11,20 --> 00:10:15,60
So let us quickly fix that error.

226
00:10:15,60 --> 00:10:20,00
And we will do that by tweaking our webpack.config.js again,

227
00:10:20,00 --> 00:10:24,20
tweaking our output and our output object.

228
00:10:24,20 --> 00:10:27,30
So let's go back into our webpack.config.js

229
00:10:27,30 --> 00:10:29,40
and make a few changes in here.

230
00:10:29,40 --> 00:10:32,10
First of all, we will replace the output folder

231
00:10:32,10 --> 00:10:36,10
so that webpack knows where serve the new components from.

232
00:10:36,10 --> 00:10:38,40
We'll just comment this line out

233
00:10:38,40 --> 00:10:41,20
and we'll create some new properties.

234
00:10:41,20 --> 00:10:43,50
So first of all, path is this time,

235
00:10:43,50 --> 00:10:47,20
is going to be path.resolve.

236
00:10:47,20 --> 00:10:50,90
And in here we will again use dirname

237
00:10:50,90 --> 00:10:53,40
and then our dist holder.

238
00:10:53,40 --> 00:10:56,00
Now the name of the file can be the same,

239
00:10:56,00 --> 00:10:57,50
so we will just copy it over.

240
00:10:57,50 --> 00:11:01,00
So ever file it creates will be the same name.

241
00:11:01,00 --> 00:11:07,00
And our publicPath we will set to current directory.

242
00:11:07,00 --> 00:11:09,80
This publicPath is where webpack dev-server

243
00:11:09,80 --> 00:11:13,50
and wepack will load all the additional

244
00:11:13,50 --> 00:11:15,10
modules that are being created.

245
00:11:15,10 --> 00:11:16,60
So let's save this.

246
00:11:16,60 --> 00:11:21,10
And now let us run our webpack dev-server.

247
00:11:21,10 --> 00:11:24,30
As you can see, it will open in the browser.

248
00:11:24,30 --> 00:11:27,20
And once it opens,

249
00:11:27,20 --> 00:11:29,70
whenever we click on the more,

250
00:11:29,70 --> 00:11:34,40
you will see, you quickly saw there the Loading book element

251
00:11:34,40 --> 00:11:36,50
and you will also see that our browser,

252
00:11:36,50 --> 00:11:39,30
let me just zoom in on it a little bit more,

253
00:11:39,30 --> 00:11:41,20
is loading this file,

254
00:11:41,20 --> 00:11:43,40
which is a JavaScript file

255
00:11:43,40 --> 00:11:46,70
containing our lazily loaded component.

256
00:11:46,70 --> 00:11:49,30
And you can do that for pretty much all the components

257
00:11:49,30 --> 00:11:50,60
that you have in your routes.

258
00:11:50,60 --> 00:11:53,50
So we can do that in this application, for example,

259
00:11:53,50 --> 00:11:56,20
for the Search component and everything thing else.

260
00:11:56,20 --> 00:11:58,00
So feel free to do that,

261
00:11:58,00 --> 00:12:01,40
just as an exercise in lazy loading.

262
00:12:01,40 --> 00:12:02,90
This is a very useful technique,

263
00:12:02,90 --> 00:12:05,10
especially if you have really large applications.

264
00:12:05,10 --> 00:12:07,80
So for example, the way it would work normally

265
00:12:07,80 --> 00:12:10,30
is whenever the user goes to the log in page,

266
00:12:10,30 --> 00:12:12,10
they would have to download,

267
00:12:12,10 --> 00:12:14,10
the browser would have to download the entire app,

268
00:12:14,10 --> 00:12:15,70
which could be quite big.

269
00:12:15,70 --> 00:12:17,40
However, they only see a log in form,

270
00:12:17,40 --> 00:12:18,30
so there's no point.

271
00:12:18,30 --> 00:12:21,10
So if you have a log in form and a dashboard,

272
00:12:21,10 --> 00:12:24,50
you can load the log in form as default

273
00:12:24,50 --> 00:12:26,50
and then lazily load the dashboard

274
00:12:26,50 --> 00:12:28,90
once the user as successfully inputted

275
00:12:28,90 --> 00:12:31,60
their username and password.

276
00:12:31,60 --> 00:12:33,90
So in order to do that, just a quick recap.

277
00:12:33,90 --> 00:12:37,20
We will need install two new modules,

278
00:12:37,20 --> 00:12:39,40
one of them called react-loadable

279
00:12:39,40 --> 00:12:43,20
and one of them babel-plugin-syntax-dynamic-import.

280
00:12:43,20 --> 00:12:45,10
And then configure our wepback

281
00:12:45,10 --> 00:12:47,10
with the publicPath in the output

282
00:12:47,10 --> 00:12:50,40
just so webpack knows where to serve the files from.

283
00:12:50,40 --> 00:12:53,70
And configure our rules

284
00:12:53,70 --> 00:12:58,10
so that our babel-loaders uses these following settings.

285
00:12:58,10 --> 00:12:59,60
With that out of the way,

286
00:12:59,60 --> 00:13:03,80
it's time to end this third section of the title.

287
00:13:03,80 --> 00:13:05,00
Let's do a quick recap

288
00:13:05,00 --> 00:13:06,80
of everything we've learned in this section.

289
00:13:06,80 --> 00:13:08,60
First of all, we have looked at

290
00:13:08,60 --> 00:13:11,10
how to use webpack dev-server into our project

291
00:13:11,10 --> 00:13:13,40
and why we should use it instead of using

292
00:13:13,40 --> 00:13:16,10
a third-party HTTP server.

293
00:13:16,10 --> 00:13:18,40
We have managed to remove our HTTP server

294
00:13:18,40 --> 00:13:20,40
from our dependency tree.

295
00:13:20,40 --> 00:13:23,10
Then we have gone through the dev-server configuration

296
00:13:23,10 --> 00:13:25,20
and looked at the most useful configs

297
00:13:25,20 --> 00:13:28,60
that are available to you whenever running dev-server.

298
00:13:28,60 --> 00:13:31,40
That is especially important when working with React.

299
00:13:31,40 --> 00:13:33,10
It's important to have your hot modules

300
00:13:33,10 --> 00:13:36,30
loaded just so you make sure that you do not refresh

301
00:13:36,30 --> 00:13:39,50
the whole page and only refresh the components that change.

302
00:13:39,50 --> 00:13:40,90
And then towards the end of the section,

303
00:13:40,90 --> 00:13:43,70
we've looked at React Router and code splitting

304
00:13:43,70 --> 00:13:45,80
and why this is beneficial

305
00:13:45,80 --> 00:13:48,20
and how you can do it, how you can achieve it

306
00:13:48,20 --> 00:13:52,20
using react-loadable components.

307
00:13:52,20 --> 00:13:54,80
In the next section, we will talk a bit more

308
00:13:54,80 --> 00:13:56,30
about wepback based deployments

309
00:13:56,30 --> 00:13:58,70
and what you need to do in order to get your application

310
00:13:58,70 --> 00:14:03,00
ready to deploy and online.

