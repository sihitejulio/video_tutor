1
00:00:00,80 --> 00:00:04,40
- Hi and welcome to the first section of this cycle.

2
00:00:04,40 --> 00:00:07,40
We're going to have a quick look at the project

3
00:00:07,40 --> 00:00:10,50
we're going to build during the course of this cycle

4
00:00:10,50 --> 00:00:12,90
as well as have with you a small discussion

5
00:00:12,90 --> 00:00:16,80
about why we build web applications this way.

6
00:00:16,80 --> 00:00:20,00
In this section we're going to have a quick look at

7
00:00:20,00 --> 00:00:22,10
the single page applications.

8
00:00:22,10 --> 00:00:24,50
We will have a look at how they work

9
00:00:24,50 --> 00:00:26,30
and I will also give you some examples

10
00:00:26,30 --> 00:00:29,20
of really successful single page applications.

11
00:00:29,20 --> 00:00:32,20
Then we will also have a look at the build process

12
00:00:32,20 --> 00:00:34,70
that happens when using, whenever using Webpack.

13
00:00:34,70 --> 00:00:37,40
So we'll talk about module bundling

14
00:00:37,40 --> 00:00:41,30
as well as the reasoning behind using Webpack

15
00:00:41,30 --> 00:00:44,50
in modern web applications and web projects.

16
00:00:44,50 --> 00:00:49,30
Then we will create a very simple application

17
00:00:49,30 --> 00:00:52,40
that we will compile and build using Webpack

18
00:00:52,40 --> 00:00:55,70
and also have a quick look at one of my projects

19
00:00:55,70 --> 00:00:59,20
which is called, MyReads which we will be using

20
00:00:59,20 --> 00:01:03,30
during the videos in future sections in order to demonstrate

21
00:01:03,30 --> 00:01:07,10
and show the way Webpack and React work together

22
00:01:07,10 --> 00:01:09,20
in front end development.

23
00:01:09,20 --> 00:01:12,10
After looking at the application we will also look at

24
00:01:12,10 --> 00:01:15,20
what we want to accomplish by the end of the project.

25
00:01:15,20 --> 00:01:18,50
We want to see what we want to obtain,

26
00:01:18,50 --> 00:01:22,30
what type of files we're expecting to get at the end

27
00:01:22,30 --> 00:01:26,40
as well as why we want those files in that specific format

28
00:01:26,40 --> 00:01:29,70
and then towards the end of the section we will also look at

29
00:01:29,70 --> 00:01:33,60
how to install and setup Webpack on your computers

30
00:01:33,60 --> 00:01:36,50
and make sure that you have everything ready

31
00:01:36,50 --> 00:01:40,10
for the projects that are following.

32
00:01:40,10 --> 00:01:43,70
In our first video we will talk in a bit more generic terms

33
00:01:43,70 --> 00:01:46,50
about React applications and single page applications

34
00:01:46,50 --> 00:01:49,40
and we will have a quick introduction into the project

35
00:01:49,40 --> 00:01:52,80
we're going to build over the course of this cycle.

36
00:01:52,80 --> 00:01:55,20
So first of all, the topics that we're going to cover

37
00:01:55,20 --> 00:01:57,80
in this video are single page applications

38
00:01:57,80 --> 00:02:01,70
and I will talk a bit more about how they work

39
00:02:01,70 --> 00:02:04,90
and why more and more companies are starting to use

40
00:02:04,90 --> 00:02:07,60
single page applications instead of the old,

41
00:02:07,60 --> 00:02:09,80
traditional, server side rendering.

42
00:02:09,80 --> 00:02:12,60
We will have a quick look at the differences between those

43
00:02:12,60 --> 00:02:18,10
and why a single page applications are being preferred

44
00:02:18,10 --> 00:02:21,50
these days and why people started developing them.

45
00:02:21,50 --> 00:02:24,40
Then we will also have a quick look at how Webpack runs

46
00:02:24,40 --> 00:02:28,10
in a project and how it builds and optimizes the project

47
00:02:28,10 --> 00:02:30,00
and then we'll have that first look

48
00:02:30,00 --> 00:02:33,30
at the MyReads app, which is going to be

49
00:02:33,30 --> 00:02:36,20
the application we will be developing together

50
00:02:36,20 --> 00:02:38,80
during the course of this title.

51
00:02:38,80 --> 00:02:41,70
To better understand why people have started using

52
00:02:41,70 --> 00:02:45,40
pro tools like Webpack in their single page applications

53
00:02:45,40 --> 00:02:48,30
we need to have a look first at what the main differences

54
00:02:48,30 --> 00:02:50,30
between server side rendering

55
00:02:50,30 --> 00:02:52,60
and single page applications are.

56
00:02:52,60 --> 00:02:54,50
If you've done front end development before

57
00:02:54,50 --> 00:02:57,70
then you're probably familiar with website frameworks

58
00:02:57,70 --> 00:03:00,90
such as WordPress or Drupal or anything similar

59
00:03:00,90 --> 00:03:02,60
and you've probably used them before

60
00:03:02,60 --> 00:03:05,70
so those types of frameworks are

61
00:03:05,70 --> 00:03:08,10
server side rendering frameworks.

62
00:03:08,10 --> 00:03:10,70
Ah, let's have a quick look at this diagram

63
00:03:10,70 --> 00:03:14,00
and see how server side rendering differs,

64
00:03:14,00 --> 00:03:15,90
is different from single page applications.

65
00:03:15,90 --> 00:03:17,90
Let's have a look at server side rendering,

66
00:03:17,90 --> 00:03:20,90
so first of all, imagine you've built your application,

67
00:03:20,90 --> 00:03:23,50
you've launched it, it's online, it's sitting on the server

68
00:03:23,50 --> 00:03:26,00
which is processing user requests.

69
00:03:26,00 --> 00:03:28,30
At that point, whenever a user makes a request

70
00:03:28,30 --> 00:03:31,10
for your website, so they try to access your website,

71
00:03:31,10 --> 00:03:32,50
or they land on your website,

72
00:03:32,50 --> 00:03:36,30
from a page like Google or from a link they've received,

73
00:03:36,30 --> 00:03:39,00
they'll generate a request to your server.

74
00:03:39,00 --> 00:03:41,40
At that point your server will receive the request

75
00:03:41,40 --> 00:03:45,50
from your users and at that point the server will say,

76
00:03:45,50 --> 00:03:48,80
okay what type of page do I need to send this user.

77
00:03:48,80 --> 00:03:51,00
Is it a homepage, is it an application page,

78
00:03:51,00 --> 00:03:52,60
is it anything like that?

79
00:03:52,60 --> 00:03:55,20
After that decision is made, the server will build

80
00:03:55,20 --> 00:03:58,90
a full page and send the whole page back to the user.

81
00:03:58,90 --> 00:04:00,90
So page is sent to the user browser,

82
00:04:00,90 --> 00:04:04,30
the browser will make sure to load all the necessary

83
00:04:04,30 --> 00:04:07,90
resources and then build the whole page again.

84
00:04:07,90 --> 00:04:11,60
Now at this point the user has a full page in his browser,

85
00:04:11,60 --> 00:04:14,30
if he performs an action, for example,

86
00:04:14,30 --> 00:04:17,10
he navigates to a menu or he submits a form,

87
00:04:17,10 --> 00:04:20,40
or anything like that, or changes the address

88
00:04:20,40 --> 00:04:22,90
in the address bar or anything like that,

89
00:04:22,90 --> 00:04:27,10
what will happen is when that action is performed

90
00:04:27,10 --> 00:04:30,10
the server will receive a new request and then

91
00:04:30,10 --> 00:04:31,90
it will build another full page,

92
00:04:31,90 --> 00:04:35,00
the page will be again sent to the user and so on.

93
00:04:35,00 --> 00:04:37,60
So basically every time the user performs an action

94
00:04:37,60 --> 00:04:42,30
on your website, the server has to rebuild the whole page,

95
00:04:42,30 --> 00:04:43,60
even though some of the elements

96
00:04:43,60 --> 00:04:46,10
in the page are the same all the time.

97
00:04:46,10 --> 00:04:48,20
So for example, imagine you have a header, footer,

98
00:04:48,20 --> 00:04:51,70
you might have a gallery of images or anything like that.

99
00:04:51,70 --> 00:04:54,80
Every time those are being sent to the user,

100
00:04:54,80 --> 00:04:57,10
the server has to build the whole page again,

101
00:04:57,10 --> 00:04:59,60
reusing elements over and over again.

102
00:04:59,60 --> 00:05:03,00
Now in order to deal with this issue

103
00:05:03,00 --> 00:05:06,50
and to deal with increasing number of visits on sites,

104
00:05:06,50 --> 00:05:07,90
and everything like that,

105
00:05:07,90 --> 00:05:10,30
and to release the load of the servers,

106
00:05:10,30 --> 00:05:14,20
people have started building single page applications.

107
00:05:14,20 --> 00:05:17,70
The basics of how single page applications work

108
00:05:17,70 --> 00:05:20,10
is completely different from server side renderings

109
00:05:20,10 --> 00:05:21,90
so let's have a look through the diagram

110
00:05:21,90 --> 00:05:25,00
and see exactly what a single page application does.

111
00:05:25,00 --> 00:05:28,20
So whenever a user sends a request there is still a server,

112
00:05:28,20 --> 00:05:30,00
backing server, that exists there

113
00:05:30,00 --> 00:05:31,80
which will receive the requests;

114
00:05:31,80 --> 00:05:34,70
however, instead of building a whole page

115
00:05:34,70 --> 00:05:37,70
for the user to, to send to the user,

116
00:05:37,70 --> 00:05:41,20
the server will create just a skeleton of the application,

117
00:05:41,20 --> 00:05:45,50
which is a very basic html file and send that page

118
00:05:45,50 --> 00:05:49,50
to the user, that page will not contain a lot of code,

119
00:05:49,50 --> 00:05:51,30
it will not contain a lot of display elements,

120
00:05:51,30 --> 00:05:54,90
it will just contain a lot of java script files

121
00:05:54,90 --> 00:05:58,20
that need to be loaded by the browser.

122
00:05:58,20 --> 00:05:59,80
Now the page is sent to our user,

123
00:05:59,80 --> 00:06:01,90
the browser will note the page

124
00:06:01,90 --> 00:06:05,30
and all the java script files associated with it

125
00:06:05,30 --> 00:06:08,30
and from that point onwards

126
00:06:08,30 --> 00:06:10,80
the framework that you've chosen, the React,

127
00:06:10,80 --> 00:06:14,20
Angular, Vue.js, or anything like that,

128
00:06:14,20 --> 00:06:17,10
the framework itself will take care of rendering

129
00:06:17,10 --> 00:06:19,10
the page and the elements in the page

130
00:06:19,10 --> 00:06:20,90
that need to be shown to the user.

131
00:06:20,90 --> 00:06:25,00
So from that point onwards, the interaction with the web

132
00:06:25,00 --> 00:06:28,30
server disappears so from this point onwards

133
00:06:28,30 --> 00:06:30,30
whenever a user performs an action,

134
00:06:30,30 --> 00:06:32,30
the action will be captured by the framework

135
00:06:32,30 --> 00:06:34,60
and the framework will make sure it handles

136
00:06:34,60 --> 00:06:37,60
rendering of elements as well as rendering

137
00:06:37,60 --> 00:06:42,10
of any new links or any new form or any new menu, links,

138
00:06:42,10 --> 00:06:44,10
or anything like that.

139
00:06:44,10 --> 00:06:47,10
As you can see, the main difference between those two

140
00:06:47,10 --> 00:06:50,40
methodologies of building web applications is that

141
00:06:50,40 --> 00:06:52,70
whenever you're doing server side rendering,

142
00:06:52,70 --> 00:06:54,90
it is much more stressful on the sever to deal with

143
00:06:54,90 --> 00:06:57,40
excessive requests and that is true,

144
00:06:57,40 --> 00:06:59,90
especially in the case where you have really popular apps

145
00:06:59,90 --> 00:07:02,00
which generate a lot of traffic

146
00:07:02,00 --> 00:07:04,20
and everybody's clicking everywhere.

147
00:07:04,20 --> 00:07:06,70
So in that case you'd want to build a single page

148
00:07:06,70 --> 00:07:09,60
application in which the load is taken off your server

149
00:07:09,60 --> 00:07:13,80
and then added onto the browser and the framework,

150
00:07:13,80 --> 00:07:15,70
the java script framework that you

151
00:07:15,70 --> 00:07:18,30
decided to use in your project.

152
00:07:18,30 --> 00:07:21,00
Now as I have mentioned in the previous diagram,

153
00:07:21,00 --> 00:07:24,80
this is how let's say your index.html might look

154
00:07:24,80 --> 00:07:27,50
in an application, so basically let's say

155
00:07:27,50 --> 00:07:30,00
we have our photo gallery, we have some functions,

156
00:07:30,00 --> 00:07:32,90
we have an app java script that handles

157
00:07:32,90 --> 00:07:35,40
let's say basic animations and stuff

158
00:07:35,40 --> 00:07:39,70
and then we also have an api.js which contains some

159
00:07:39,70 --> 00:07:41,90
functions that we need to load data

160
00:07:41,90 --> 00:07:44,20
for our gallery for example.

161
00:07:44,20 --> 00:07:47,20
At this point, all of those files are being loaded

162
00:07:47,20 --> 00:07:50,70
into our index.html so when that happens

163
00:07:50,70 --> 00:07:53,50
the browser is doing a lot, a lot of requests

164
00:07:53,50 --> 00:07:56,10
for these small java script files

165
00:07:56,10 --> 00:07:59,30
and while all those file are small files,

166
00:07:59,30 --> 00:08:01,90
just making a lot of requests usually

167
00:08:01,90 --> 00:08:04,80
takes the browser a bit longer to load the page.

168
00:08:04,80 --> 00:08:07,10
This is especially true whenever developing

169
00:08:07,10 --> 00:08:08,70
web applications for mobiles.

170
00:08:08,70 --> 00:08:10,20
Mobiles are sometimes,

171
00:08:10,20 --> 00:08:13,00
if the internet connection is not good, or is very slow,

172
00:08:13,00 --> 00:08:16,00
those small files will take a long time to load

173
00:08:16,00 --> 00:08:18,40
and load time is one of the most important things

174
00:08:18,40 --> 00:08:20,90
when developing websites and web apps

175
00:08:20,90 --> 00:08:23,30
because a lot of users do not have patience

176
00:08:23,30 --> 00:08:25,00
to wait for the site to load

177
00:08:25,00 --> 00:08:28,10
so they need to get a really performant application

178
00:08:28,10 --> 00:08:31,20
that loads quick and works quick as well.

179
00:08:31,20 --> 00:08:35,70
This is one of the reasons why build tools such as Webpack

180
00:08:35,70 --> 00:08:39,40
have been invented, to take the stress of the load process

181
00:08:39,40 --> 00:08:44,50
and try and increase the speed of which your apps work.

182
00:08:44,50 --> 00:08:46,90
So we will have a quick look at what Webpack does

183
00:08:46,90 --> 00:08:49,40
in order to simplify this load process

184
00:08:49,40 --> 00:08:51,30
and make it a bit faster.

185
00:08:51,30 --> 00:08:53,70
So what build tools, and especially Webpack,

186
00:08:53,70 --> 00:08:55,70
will do for you, they will, as you can see,

187
00:08:55,70 --> 00:08:58,60
all the same files we had loading previously

188
00:08:58,60 --> 00:09:01,50
like our gallery, functions, app, and api

189
00:09:01,50 --> 00:09:04,40
and all the other files that you have for your project

190
00:09:04,40 --> 00:09:07,10
will go through Webpack which will put them all together

191
00:09:07,10 --> 00:09:11,00
into one large java script file,

192
00:09:11,00 --> 00:09:13,90
one larger java script file, which is usually called

193
00:09:13,90 --> 00:09:17,10
either main.js, upgo.js, or bundle.js,

194
00:09:17,10 --> 00:09:19,50
it depends on how you'd like to call it.

195
00:09:19,50 --> 00:09:24,10
And then that bundle.js is then loaded into our

196
00:09:24,10 --> 00:09:27,10
index page so at this point,

197
00:09:27,10 --> 00:09:30,00
instead of loading a lot of tiny files

198
00:09:30,00 --> 00:09:34,80
we would load one large file which will not be much larger

199
00:09:34,80 --> 00:09:37,60
than the sum of the other ones combined

200
00:09:37,60 --> 00:09:39,80
however it will load much faster just because

201
00:09:39,80 --> 00:09:43,60
the browser only needs to do very few requests

202
00:09:43,60 --> 00:09:46,30
in order to retrieve this bundle.js file.

203
00:09:46,30 --> 00:09:49,30
So that's one of the reasons why build tools,

204
00:09:49,30 --> 00:09:51,60
and especially Webpack, are very popular

205
00:09:51,60 --> 00:09:55,40
in today's front end development because they increase

206
00:09:55,40 --> 00:09:57,20
the load speed of your application

207
00:09:57,20 --> 00:10:00,00
and they also help to make sure that your application

208
00:10:00,00 --> 00:10:01,90
always loads the same for all the users.

209
00:10:01,90 --> 00:10:05,40
So for example, no files are missed, no files are loaded.

210
00:10:05,40 --> 00:10:08,50
The only thing that the user's browser needs to worry

211
00:10:08,50 --> 00:10:12,20
about is loading the index.html and the bundle.js.

212
00:10:12,20 --> 00:10:16,70
That bundle.js will also contain React and any other

213
00:10:16,70 --> 00:10:20,00
development tools that you, or any other frameworks

214
00:10:20,00 --> 00:10:22,30
that you decide to use for the user.

215
00:10:22,30 --> 00:10:24,20
So as you can see, everything is packaged

216
00:10:24,20 --> 00:10:27,50
into one large file that only needs to be loaded once

217
00:10:27,50 --> 00:10:30,90
in order to increase the performance of your application.

218
00:10:30,90 --> 00:10:33,10
As an example of big companies using this process

219
00:10:33,10 --> 00:10:35,90
in order to optimize and improve their web applications

220
00:10:35,90 --> 00:10:37,80
we can have a look at Airbnb,

221
00:10:37,80 --> 00:10:41,80
which is a platform built, which is a web application

222
00:10:41,80 --> 00:10:44,80
based on React that is bundled together with Webpack

223
00:10:44,80 --> 00:10:49,50
and also you can also have look if you do not know Discord.

224
00:10:49,50 --> 00:10:51,80
Discord is another one of those applications,

225
00:10:51,80 --> 00:10:54,40
is a chat application that works in the browser

226
00:10:54,40 --> 00:10:59,60
which is also built with React and bundled with Webpack.

227
00:10:59,60 --> 00:11:01,80
We will have a look in the next video quickly

228
00:11:01,80 --> 00:11:05,60
and see how Webpack is helping build those applications

229
00:11:05,60 --> 00:11:07,80
but before we do that, let's have a quick look

230
00:11:07,80 --> 00:11:09,80
at the project we're going to develop

231
00:11:09,80 --> 00:11:12,90
over the course of this title which I've called MyReads.

232
00:11:12,90 --> 00:11:15,20
It's a project I've done for a course recently,

233
00:11:15,20 --> 00:11:18,10
it's not very very complex, React application.

234
00:11:18,10 --> 00:11:21,10
However, it contains everything that we need

235
00:11:21,10 --> 00:11:28,10
in order to make sure that everything Webpack does

236
00:11:28,10 --> 00:11:30,20
is being introduced properly and that you would then

237
00:11:30,20 --> 00:11:34,90
can apply those lessons to your own React projects.

238
00:11:34,90 --> 00:11:38,00
So the MyReads application it's a simple,

239
00:11:38,00 --> 00:11:40,70
I call it a bookshelf application.

240
00:11:40,70 --> 00:11:45,00
What you have in here is basically a page in which you

241
00:11:45,00 --> 00:11:47,30
can save the books that you want to read

242
00:11:47,30 --> 00:11:49,50
and also keep an history of the books

243
00:11:49,50 --> 00:11:51,40
that you've already read.

244
00:11:51,40 --> 00:11:53,90
As you can see in the bottom you have a plus button.

245
00:11:53,90 --> 00:11:57,70
You can use this button to search for other books

246
00:11:57,70 --> 00:11:59,40
that are available in the library.

247
00:11:59,40 --> 00:12:02,40
So there are a few searches, I recommend searching

248
00:12:02,40 --> 00:12:05,60
art for example and then once those load

249
00:12:05,60 --> 00:12:08,40
you can, let's say, pick a book from the library

250
00:12:08,40 --> 00:12:10,20
and add it into your "Read".

251
00:12:10,20 --> 00:12:13,30
To add it you just click on this and then let's say

252
00:12:13,30 --> 00:12:15,50
you want to add it to your Read list,

253
00:12:15,50 --> 00:12:18,10
you'll add it to "Want to Read" and now

254
00:12:18,10 --> 00:12:20,60
when we go back into our homepage you'll see that

255
00:12:20,60 --> 00:12:24,40
our book has been added to our "Want to Read".

256
00:12:24,40 --> 00:12:27,00
At the same time you can always switch a book

257
00:12:27,00 --> 00:12:29,50
from one shelf to another, so for example,

258
00:12:29,50 --> 00:12:31,90
if you started reading this book you put it into

259
00:12:31,90 --> 00:12:34,70
"Currently Reading" and when you're done

260
00:12:34,70 --> 00:12:36,50
you'll just add it to "Read".

261
00:12:36,50 --> 00:12:38,60
So as you can see, the functionality is really basic

262
00:12:38,60 --> 00:12:41,70
however this project encapsulates a lot of React

263
00:12:41,70 --> 00:12:43,80
techniques that you can use to get Webpack

264
00:12:43,80 --> 00:12:46,50
in order to improve performance.

265
00:12:46,50 --> 00:12:49,40
You also have another page, if you go under any book

266
00:12:49,40 --> 00:12:52,70
you can click on "More" and obtain a few more details

267
00:12:52,70 --> 00:12:56,90
about that specific book, so it's nothing too fancy

268
00:12:56,90 --> 00:13:01,60
however, this project will help you understand

269
00:13:01,60 --> 00:13:04,70
how you can combine Webpack and React to obtain

270
00:13:04,70 --> 00:13:06,90
a really powerful application.

271
00:13:06,90 --> 00:13:11,20
So that covers the first video of this section.

272
00:13:11,20 --> 00:13:12,90
In the next video we will have a look

273
00:13:12,90 --> 00:13:15,70
at the deliverables that we are expecting to have

274
00:13:15,70 --> 00:13:16,70
at the end of the project

275
00:13:16,70 --> 00:13:21,00
and how we will obtain that using Webpack too.

