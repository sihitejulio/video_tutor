1
00:00:08,00 --> 00:00:10,00
- [Alex] Hi and welcome to our Webpack

2
00:00:10,00 --> 00:00:13,20
for React Applications course.

3
00:00:13,20 --> 00:00:15,40
If you have never used Webpack before,

4
00:00:15,40 --> 00:00:18,10
if you've previously worked on React projects

5
00:00:18,10 --> 00:00:21,20
containing strange webpack.config.js file

6
00:00:21,20 --> 00:00:23,90
but never quite wanted to change it

7
00:00:23,90 --> 00:00:25,20
or didn't know how to change it,

8
00:00:25,20 --> 00:00:27,40
or if you just want to find out more about

9
00:00:27,40 --> 00:00:31,30
how to better optimize and run your React applications,

10
00:00:31,30 --> 00:00:33,20
you are in the right place.

11
00:00:33,20 --> 00:00:36,80
In this title, you'll learn all about how Webpack works,

12
00:00:36,80 --> 00:00:41,10
its core functionality as well as how you can improve

13
00:00:41,10 --> 00:00:43,50
your React application with Webpack,

14
00:00:43,50 --> 00:00:46,40
as well as how you can make the development process

15
00:00:46,40 --> 00:00:48,80
much easier process and make it much easier

16
00:00:48,80 --> 00:00:52,30
for you to develop React applications when using Webpack.

17
00:00:52,30 --> 00:00:55,60
I'm sure you're all excited to start using Webpack

18
00:00:55,60 --> 00:00:59,40
with React, however, before we dive into the videos,

19
00:00:59,40 --> 00:01:02,20
it's time for a quick introduction.

20
00:01:02,20 --> 00:01:05,00
My name is Alex and I work as a senior front-end developer

21
00:01:05,00 --> 00:01:08,50
from one of the leading delivery companies in the UK.

22
00:01:08,50 --> 00:01:10,60
I have been working on front-end projects

23
00:01:10,60 --> 00:01:13,40
for the past six years, and during all this time,

24
00:01:13,40 --> 00:01:16,00
I have developed and deployed numerous sites

25
00:01:16,00 --> 00:01:19,20
and React applications for both start-ups

26
00:01:19,20 --> 00:01:21,50
and large companies alike.

27
00:01:21,50 --> 00:01:25,00
In every project I worked on, I've had two main goals.

28
00:01:25,00 --> 00:01:28,30
First of all, to make sure that the user journey is as close

29
00:01:28,30 --> 00:01:29,80
to perfection as possible.

30
00:01:29,80 --> 00:01:33,10
Obviously you know a perfect application does not exist,

31
00:01:33,10 --> 00:01:35,90
and if it does, please let me know about it.

32
00:01:35,90 --> 00:01:39,50
And second, to ensure that all the apps are deployed,

33
00:01:39,50 --> 00:01:45,20
are user-performant, and are easy to deploy and maintain.

34
00:01:45,20 --> 00:01:47,40
In this title, we will be focusing

35
00:01:47,40 --> 00:01:50,60
mostly on the latter point.

36
00:01:50,60 --> 00:01:54,20
We will explore some ways in which Webpack helps you

37
00:01:54,20 --> 00:01:56,90
make your React application better,

38
00:01:56,90 --> 00:01:59,90
how it makes your development faster,

39
00:01:59,90 --> 00:02:03,40
and how it makes it easier to manage in the long run.

40
00:02:03,40 --> 00:02:07,90
To do this, we will cover the following topics.

41
00:02:07,90 --> 00:02:10,80
First of all, because I believe in learning by doing

42
00:02:10,80 --> 00:02:13,30
and not just learning by reading,

43
00:02:13,30 --> 00:02:14,70
I will give you a quick introduction

44
00:02:14,70 --> 00:02:18,20
to the project we are going to develop over the course

45
00:02:18,20 --> 00:02:20,20
of this title.

46
00:02:20,20 --> 00:02:22,90
We will take a preexisting React app

47
00:02:22,90 --> 00:02:26,80
that handles some bookshelf management and apply Webpack

48
00:02:26,80 --> 00:02:29,40
and all the techniques available in Webpack to it

49
00:02:29,40 --> 00:02:31,50
in order to increase its performance

50
00:02:31,50 --> 00:02:34,80
as well as make it easier to deploy to some

51
00:02:34,80 --> 00:02:40,90
of the most common hosting providers using Webpack.

52
00:02:40,90 --> 00:02:43,90
After that, we will import our preexisting React project

53
00:02:43,90 --> 00:02:47,60
into our basic Webpack configuration

54
00:02:47,60 --> 00:02:50,20
and we will see what we need to do

55
00:02:50,20 --> 00:02:52,30
in order to get the application running,

56
00:02:52,30 --> 00:02:54,80
and we will also make some performance tweaks

57
00:02:54,80 --> 00:02:58,60
to the application to make it more faster for the users.

58
00:02:58,60 --> 00:03:00,90
In the third section, we will learn a bit

59
00:03:00,90 --> 00:03:03,50
more about the webpack-dev-server

60
00:03:03,50 --> 00:03:06,40
and a few more React-specific topics, again,

61
00:03:06,40 --> 00:03:08,30
in order to increase your productivity

62
00:03:08,30 --> 00:03:13,00
when writing React applications using Webpack.

63
00:03:13,00 --> 00:03:16,50
And in the final section of this title,

64
00:03:16,50 --> 00:03:19,70
we will learn about deploying the app we've just created

65
00:03:19,70 --> 00:03:21,90
with the help of Webpack to a few

66
00:03:21,90 --> 00:03:25,30
of the most common hosting providers,

67
00:03:25,30 --> 00:03:29,90
such as GitHub pages and Amazon AWS

68
00:03:29,90 --> 00:03:32,70
and Elastic Beanstalk.

69
00:03:32,70 --> 00:03:35,20
The requirements for this course is

70
00:03:35,20 --> 00:03:39,60
that own a Mac or Linux or a Windows machine,

71
00:03:39,60 --> 00:03:41,30
so basically any computer will do,

72
00:03:41,30 --> 00:03:44,90
as Webpack is universal across all platforms.

73
00:03:44,90 --> 00:03:47,80
Also, you will need knowledge of JavaScript syntax.

74
00:03:47,80 --> 00:03:49,50
Do not worry if you're not familiar

75
00:03:49,50 --> 00:03:56,60
with ES6 or ES2015, cover bits of that during the course.

76
00:03:56,60 --> 00:03:59,90
The goals of this title are to make sure

77
00:03:59,90 --> 00:04:02,60
that by the end you're able to install

78
00:04:02,60 --> 00:04:05,20
and configure Webpack in any of your projects,

79
00:04:05,20 --> 00:04:08,90
to use Webpack to create a basic React application,

80
00:04:08,90 --> 00:04:11,70
we also want to make sure you know

81
00:04:11,70 --> 00:04:14,50
how to develop React apps faster using Webpack

82
00:04:14,50 --> 00:04:17,00
and the help of webpack-dev-server.

83
00:04:17,00 --> 00:04:20,30
Also, how to handle and optimize your assets,

84
00:04:20,30 --> 00:04:23,00
that's your stylesheets, images, and data,

85
00:04:23,00 --> 00:04:26,60
and you will also learn how to boost your app performance

86
00:04:26,60 --> 00:04:30,10
by optimizing the output files.

87
00:04:30,10 --> 00:04:31,90
So, hopefully by the end of the title,

88
00:04:31,90 --> 00:04:34,20
you'll have learned everything you need to know

89
00:04:34,20 --> 00:04:36,90
in order to be comfortable using Webpack,

90
00:04:36,90 --> 00:04:41,10
you have a better understanding of what Webpack does,

91
00:04:41,10 --> 00:04:43,80
and how it can really help you up

92
00:04:43,80 --> 00:04:47,50
when developing React applications.

93
00:04:47,50 --> 00:04:51,00
So, without further ado, let's move on to section one.

