1
00:00:01,30 --> 00:00:03,70
- [Instructor] We will install Webpack by doing

2
00:00:03,70 --> 00:00:06,80
'yarn global' so this will give you access

3
00:00:06,80 --> 00:00:09,30
to Webpack command in the terminals,

4
00:00:09,30 --> 00:00:11,80
so 'global' and then we will add 'Webpack'

5
00:00:11,80 --> 00:00:13,80
at version 2.6.1.

6
00:00:13,80 --> 00:00:15,90
This is the longterm sport for Webpack,

7
00:00:15,90 --> 00:00:17,30
there might be some different versions

8
00:00:17,30 --> 00:00:20,10
by the time you see this, but if you follow this course,

9
00:00:20,10 --> 00:00:24,30
if you install Webpack at 2.6.1, everything presented

10
00:00:24,30 --> 00:00:28,90
in the following sections will work without any issues.

11
00:00:28,90 --> 00:00:31,50
So make sure you install that, I already have it installed,

12
00:00:31,50 --> 00:00:35,10
however the yarn will try to update it,

13
00:00:35,10 --> 00:00:38,10
and now what will happen is we will have access

14
00:00:38,10 --> 00:00:41,30
to the Webpack command in terminal.

15
00:00:41,30 --> 00:00:43,30
So if we type in Webpack and press enter,

16
00:00:43,30 --> 00:00:45,50
as you can see, at the moment it will do nothing,

17
00:00:45,50 --> 00:00:48,00
because there is no configuration file found.

18
00:00:48,00 --> 00:00:50,40
However now you have access to Webpack

19
00:00:50,40 --> 00:00:53,70
across all your projects if you need it.

20
00:00:53,70 --> 00:00:57,00
Now let's have a look at what else we need to configure

21
00:00:57,00 --> 00:01:00,40
in this project in order to get Webpack running,

22
00:01:00,40 --> 00:01:04,00
and to build something using Webpack nodals.

23
00:01:04,00 --> 00:01:08,80
First of all, we will want to create a source folder

24
00:01:08,80 --> 00:01:11,50
in here, so I will go from this back into the editor

25
00:01:11,50 --> 00:01:15,00
and inside the basic app I will create a new folder.

26
00:01:15,00 --> 00:01:15,90
I'll call this source-

27
00:01:15,90 --> 00:01:17,30
you can call it whatever you want,

28
00:01:17,30 --> 00:01:20,00
however it's good to follow some industry standards

29
00:01:20,00 --> 00:01:23,20
and always the source files that you create

30
00:01:23,20 --> 00:01:25,30
will always be in the source file.

31
00:01:25,30 --> 00:01:28,00
So we will create the SRC folder,

32
00:01:28,00 --> 00:01:31,80
and inside our SRC folder, since Webpack bundles JavaScript,

33
00:01:31,80 --> 00:01:35,90
we will create a small JavaScript file called app.js

34
00:01:35,90 --> 00:01:37,90
so it's not going to be anything complicated,

35
00:01:37,90 --> 00:01:41,10
we're just going to log something into the terminal

36
00:01:41,10 --> 00:01:44,50
and make sure everything is working as intended.

37
00:01:44,50 --> 00:01:49,00
So, we will call this file 'app.js'

38
00:01:49,00 --> 00:01:52,50
and in our app.js file, we will just, as I said,

39
00:01:52,50 --> 00:01:55,30
add a console log, and in that console log,

40
00:01:55,30 --> 00:01:58,00
we'll just say, let's do a 'Hello world,'

41
00:01:58,00 --> 00:02:00,10
everybody likes it, 'Hello world.'

42
00:02:00,10 --> 00:02:04,80
We will save this file, now we will go back into our

43
00:02:04,80 --> 00:02:07,50
basic app folder, and let's create

44
00:02:07,50 --> 00:02:13,80
a basic configuration file for our Webpack project.

45
00:02:13,80 --> 00:02:16,00
Webpack on its own will not run

46
00:02:16,00 --> 00:02:18,00
because it doesn't know exactly what to do,

47
00:02:18,00 --> 00:02:21,90
so we will need to create a Webpack configuration file

48
00:02:21,90 --> 00:02:25,00
which is always called, and should always be called,

49
00:02:25,00 --> 00:02:30,90
'Webpack.config.js' so let's create that file,

50
00:02:30,90 --> 00:02:34,60
so going to our basic app,

51
00:02:34,60 --> 00:02:43,30
new file, and we will call this, 'Webpack.config.js'.

52
00:02:43,30 --> 00:02:46,90
So this title you cannot change it, you need to use this

53
00:02:46,90 --> 00:02:49,50
file name for Webpack configuration.

54
00:02:49,50 --> 00:02:51,20
And in here we'll write a few lines,

55
00:02:51,20 --> 00:02:54,80
so we will do 'module.exports'

56
00:02:54,80 --> 00:02:57,70
We'll do 'equal' and then we'll create an objects,

57
00:02:57,70 --> 00:03:00,70
and let's do an entry point for our application,

58
00:03:00,70 --> 00:03:06,40
so this entry point will be in Source and App.js

59
00:03:06,40 --> 00:03:09,80
which is the file we've just created previously.

60
00:03:09,80 --> 00:03:14,60
And let us also create an output for Webpack,

61
00:03:14,60 --> 00:03:19,10
so the output would be an object, and in here

62
00:03:19,10 --> 00:03:23,70
we will provide the file name of where our app.js-

63
00:03:23,70 --> 00:03:26,40
our bundled project is sent.

64
00:03:26,40 --> 00:03:30,20
So we will do file name and then in here, again,

65
00:03:30,20 --> 00:03:33,30
what I usually do is create a distribution folder

66
00:03:33,30 --> 00:03:36,40
in which all the compiled JavaScript will go,

67
00:03:36,40 --> 00:03:40,40
so we will create a dist folder, and then in here

68
00:03:40,40 --> 00:03:44,40
we will do an app.bundle.js.

69
00:03:44,40 --> 00:03:47,30
This again is up to you, how you want to call your files,

70
00:03:47,30 --> 00:03:49,70
I prefer to stick with standard notation so that

71
00:03:49,70 --> 00:03:53,70
everybody understands the code whenever they look at it.

72
00:03:53,70 --> 00:03:56,10
We'll save this file, and then we will go back

73
00:03:56,10 --> 00:03:59,40
into our terminal, and let's try and run Webpack again,

74
00:03:59,40 --> 00:04:02,30
and as you can see, let's do it again,

75
00:04:02,30 --> 00:04:05,40
at the top of the window, let's run Webpack,

76
00:04:05,40 --> 00:04:08,20
and as you can see, Webpack has not generated an error

77
00:04:08,20 --> 00:04:12,20
anymore, and it has actually created a file

78
00:04:12,20 --> 00:04:15,30
in 'distribution app.bundle.js'.

79
00:04:15,30 --> 00:04:17,70
If we go in our terminal, in our editor,

80
00:04:17,70 --> 00:04:20,90
you will see we have this folder that has been created,

81
00:04:20,90 --> 00:04:22,80
and app.bundle.js.

82
00:04:22,80 --> 00:04:25,50
When you open this bundle, initially, do not get scared,

83
00:04:25,50 --> 00:04:28,10
there's a lot of boilerplate that Webpack needs to create,

84
00:04:28,10 --> 00:04:32,50
however this boilerplate is, for what we've done now,

85
00:04:32,50 --> 00:04:36,40
it's just a console log, it's not necessary, ideal,

86
00:04:36,40 --> 00:04:38,20
however whenever you have big projects,

87
00:04:38,20 --> 00:04:40,70
this boilerplate will represent

88
00:04:40,70 --> 00:04:43,80
maybe 0.1% of the entire code in the project,

89
00:04:43,80 --> 00:04:46,50
so do not be scared of what you're seeing here.

90
00:04:46,50 --> 00:04:48,40
However if we scroll all the way down,

91
00:04:48,40 --> 00:04:52,00
you'll see that our app.js has been included

92
00:04:52,00 --> 00:04:54,30
in the app.bundle.js, so if at this point,

93
00:04:54,30 --> 00:04:57,50
we were to include this into an HTML file,

94
00:04:57,50 --> 00:05:01,80
we will obtain our 'Hello world' from Webpack

95
00:05:01,80 --> 00:05:03,60
in the console.

96
00:05:03,60 --> 00:05:07,70
Now let's do exactly that and create an index.html file

97
00:05:07,70 --> 00:05:12,00
in which we will be including our app.bundle.js

98
00:05:12,00 --> 00:05:14,20
that we're generating with Webpack,

99
00:05:14,20 --> 00:05:16,00
so let us create that.

100
00:05:16,00 --> 00:05:18,60
We will go into the Dist folder, we will create

101
00:05:18,60 --> 00:05:21,60
a new file called this, one second.

102
00:05:21,60 --> 00:05:24,60
We will call this 'index.html'

103
00:05:24,60 --> 00:05:27,50
and in this 'index.html' we will just paste

104
00:05:27,50 --> 00:05:30,50
some basic boilerplate HTML.

105
00:05:30,50 --> 00:05:33,90
Feel free to copy it from wherever you want.

106
00:05:33,90 --> 00:05:36,60
So actually I'll just paste some boilerplate in

107
00:05:36,60 --> 00:05:39,10
as you can see, got a title in,

108
00:05:39,10 --> 00:05:41,20
I'll put 'Hello from webpack' and at the same time

109
00:05:41,20 --> 00:05:43,20
at the bottom of the body, I will also need

110
00:05:43,20 --> 00:05:49,60
to add in my script, which is a JavaScript type script.

111
00:05:49,60 --> 00:06:01,10
Okay, and the source of the script will be 'app.bundle.js'.

112
00:06:01,10 --> 00:06:02,90
We'll save this, we'll go into our terminal

113
00:06:02,90 --> 00:06:07,30
and open the file, so we'll go, just do 'open'

114
00:06:07,30 --> 00:06:09,90
and then we will go into our dist, and then

115
00:06:09,90 --> 00:06:14,30
the 'index.html' and hopefully this will load up

116
00:06:14,30 --> 00:06:16,40
in a browser window, as you can see,

117
00:06:16,40 --> 00:06:19,90
it has loaded here, and if we go and inspect,

118
00:06:19,90 --> 00:06:22,90
we go to our console, we can see the JavaScript

119
00:06:22,90 --> 00:06:25,70
bundled together with Webpack, this worked perfectly

120
00:06:25,70 --> 00:06:28,10
as intended, so we have our page, which we have

121
00:06:28,10 --> 00:06:30,90
bundled together with Webpack.

122
00:06:30,90 --> 00:06:33,40
In future sections we will have a look at how to

123
00:06:33,40 --> 00:06:36,00
automate this process so that you do not have to

124
00:06:36,00 --> 00:06:38,00
create this index file automatically,

125
00:06:38,00 --> 00:06:41,00
but until then, we will work based on this file.

126
00:06:41,00 --> 00:06:43,10
So, we have spoken about the advantages

127
00:06:43,10 --> 00:06:44,70
of having single page applications,

128
00:06:44,70 --> 00:06:49,00
we've looked at a few examples, such as Airbnb and Discord,

129
00:06:49,00 --> 00:06:52,80
then we have also installed and set up the most basic

130
00:06:52,80 --> 00:06:54,70
Webpack project that we can.

131
00:06:54,70 --> 00:06:56,90
And then we've also created our very first

132
00:06:56,90 --> 00:06:59,80
Webpack application, which doesn't do anything,

133
00:06:59,80 --> 00:07:01,80
however it's there and that's the basis

134
00:07:01,80 --> 00:07:04,30
of what we're going to build during the course

135
00:07:04,30 --> 00:07:05,50
of this title.

136
00:07:05,50 --> 00:07:08,30
In the next section, we will talk a bit more

137
00:07:08,30 --> 00:07:09,90
about configuration of the project

138
00:07:09,90 --> 00:07:13,40
and how to make sure that Webpack builds our projects

139
00:07:13,40 --> 00:07:16,10
correctly, and then towards the end of that section,

140
00:07:16,10 --> 00:07:19,90
we will add in all the react goodness that we have

141
00:07:19,90 --> 00:07:23,00
from the Myreads app, that will be provided to you.

