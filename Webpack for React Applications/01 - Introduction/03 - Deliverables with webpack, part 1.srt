1
00:00:00,50 --> 00:00:01,70
- [Narrator] In this video we will have

2
00:00:01,70 --> 00:00:04,40
a quick look at the deliverables that

3
00:00:04,40 --> 00:00:07,80
we're expecting to get to by the end of the project

4
00:00:07,80 --> 00:00:10,70
and how we accomplish that using Webpack.

5
00:00:10,70 --> 00:00:12,20
The topics we are going to cover

6
00:00:12,20 --> 00:00:14,90
are the advantages of single page applications

7
00:00:14,90 --> 00:00:16,90
with examples of how real

8
00:00:16,90 --> 00:00:19,20
single page applications are served.

9
00:00:19,20 --> 00:00:21,90
Then we will initialize our project

10
00:00:21,90 --> 00:00:23,40
and install Webpack and

11
00:00:23,40 --> 00:00:26,70
get the basic configuration for it setup.

12
00:00:26,70 --> 00:00:28,50
And then we will create a first

13
00:00:28,50 --> 00:00:31,90
very, very basic app that we will run with Webpack

14
00:00:31,90 --> 00:00:36,30
in order to prepare for our MyReads application.

15
00:00:36,30 --> 00:00:38,50
If you remember in the previous video

16
00:00:38,50 --> 00:00:40,60
of this section we have spoken about a few

17
00:00:40,60 --> 00:00:44,80
real life applications such as Airbnb and Discord

18
00:00:44,80 --> 00:00:46,60
which are using React and Webpack

19
00:00:46,60 --> 00:00:49,00
in order to serve their apps.

20
00:00:49,00 --> 00:00:53,20
So let's have a quick look at those websites

21
00:00:53,20 --> 00:00:56,00
and see exactly what does that mean.

22
00:00:56,00 --> 00:00:57,50
So first of all just open a

23
00:00:57,50 --> 00:01:00,40
Chrome browser preferably and then inspect.

24
00:01:00,40 --> 00:01:02,20
If we go to Airbnb for example

25
00:01:02,20 --> 00:01:04,30
we will be able to inspect the page

26
00:01:04,30 --> 00:01:06,80
and then what we will look is if we go

27
00:01:06,80 --> 00:01:09,30
all the way to the bottom of the code

28
00:01:09,30 --> 00:01:12,30
that you will find under the elements section

29
00:01:12,30 --> 00:01:13,80
you'll see there is a lot of code

30
00:01:13,80 --> 00:01:15,80
but what we're interested in

31
00:01:15,80 --> 00:01:17,90
is those two scripts at the bottom.

32
00:01:17,90 --> 00:01:20,40
So as you can see what Airbnb is doing

33
00:01:20,40 --> 00:01:23,10
they are loading some bundled Javascripts

34
00:01:23,10 --> 00:01:27,30
of here and here which are taking care

35
00:01:27,30 --> 00:01:29,50
of rendering the entire page.

36
00:01:29,50 --> 00:01:33,50
Those are, as I said, Airbnb was built with React

37
00:01:33,50 --> 00:01:38,30
so this is how you will serve React using Webpack

38
00:01:38,30 --> 00:01:39,90
in your web application.

39
00:01:39,90 --> 00:01:41,80
If we have a look at the network tab

40
00:01:41,80 --> 00:01:46,10
in the developer tools and refresh the page,

41
00:01:46,10 --> 00:01:47,70
when loading Javascript you will see

42
00:01:47,70 --> 00:01:50,00
that even though the page is extremely complex

43
00:01:50,00 --> 00:01:53,00
and it has a lot of forms, it also has some photo galleries,

44
00:01:53,00 --> 00:01:56,10
some ratings systems and everything like that,

45
00:01:56,10 --> 00:01:58,70
it is only loading very few Javascript files

46
00:01:58,70 --> 00:02:01,20
which even though they are a bit bigger

47
00:02:01,20 --> 00:02:04,20
than what you'd load on a standard site,

48
00:02:04,20 --> 00:02:06,60
they will load much faster in a browser

49
00:02:06,60 --> 00:02:08,20
and especially on mobile.

50
00:02:08,20 --> 00:02:11,10
If we compared that to a standard site

51
00:02:11,10 --> 00:02:15,00
that loads 50 gigs query library for example

52
00:02:15,00 --> 00:02:17,20
you can obviously start seeing the difference

53
00:02:17,20 --> 00:02:21,10
and seeing why build tools are so important.

54
00:02:21,10 --> 00:02:23,30
Let's also have a look at Discord

55
00:02:23,30 --> 00:02:24,80
and again we will do the same thing.

56
00:02:24,80 --> 00:02:27,80
We will inspect the page and then as you can see

57
00:02:27,80 --> 00:02:32,00
Discord has a much more focused approach

58
00:02:32,00 --> 00:02:33,70
on bundling everything together.

59
00:02:33,70 --> 00:02:36,30
Whenever you open the site they have a

60
00:02:36,30 --> 00:02:38,80
small section with some text which is being

61
00:02:38,80 --> 00:02:41,10
served as part of their HTML skeleton

62
00:02:41,10 --> 00:02:43,20
and then at the bottom they only have

63
00:02:43,20 --> 00:02:45,30
one single script so everything they have

64
00:02:45,30 --> 00:02:48,80
running on the site is bundled into one single asset.

65
00:02:48,80 --> 00:02:52,80
Now if we open the app itself, once it loads,

66
00:02:52,80 --> 00:02:58,00
as you can see there will be nothing in the body

67
00:02:58,00 --> 00:03:00,80
of the application so nothing in the HTML

68
00:03:00,80 --> 00:03:03,30
except for the base skeleton which we can see here

69
00:03:03,30 --> 00:03:05,70
which is the app mount.

70
00:03:05,70 --> 00:03:07,10
So you can see it's the React group

71
00:03:07,10 --> 00:03:09,20
so this is basically a React application.

72
00:03:09,20 --> 00:03:12,10
It's telling you it's a React application.

73
00:03:12,10 --> 00:03:14,80
And if we have a look at the bottom of the body tag

74
00:03:14,80 --> 00:03:17,30
you will be able to see a few scripts that they are loading,

75
00:03:17,30 --> 00:03:19,00
so again they are doing three scripts

76
00:03:19,00 --> 00:03:21,90
I assume one of them for each, for CSS,

77
00:03:21,90 --> 00:03:23,90
one of them for Javascript, one of them

78
00:03:23,90 --> 00:03:26,60
for any other logic that they need to load.

79
00:03:26,60 --> 00:03:28,40
And then if we go to the network tab,

80
00:03:28,40 --> 00:03:31,20
as you can see we only have three scripts being loaded.

81
00:03:31,20 --> 00:03:33,50
Now imagine instead of three you have to load

82
00:03:33,50 --> 00:03:35,90
50 or 100 again and the

83
00:03:35,90 --> 00:03:38,50
load time would increase significantly.

84
00:03:38,50 --> 00:03:41,40
So those are the main reasons we use build tools.

85
00:03:41,40 --> 00:03:44,00
So now that this whole introduction

86
00:03:44,00 --> 00:03:47,10
and theoretical stuff is over let's actually

87
00:03:47,10 --> 00:03:51,20
get coding and see what we can do in order

88
00:03:51,20 --> 00:03:53,80
to create an application or add Webpack

89
00:03:53,80 --> 00:03:55,80
to an application and improve the performance

90
00:03:55,80 --> 00:03:58,30
and make it so that whenever your app is loading

91
00:03:58,30 --> 00:04:01,40
it's quick, it's performing, and it works

92
00:04:01,40 --> 00:04:04,20
well with React or any other front end

93
00:04:04,20 --> 00:04:07,40
development framework that you've chosen.

94
00:04:07,40 --> 00:04:10,90
So before we start working with the MyReads application

95
00:04:10,90 --> 00:04:15,90
let us go onto our computer and create a simple

96
00:04:15,90 --> 00:04:19,50
very, very basic Javascript app just to

97
00:04:19,50 --> 00:04:22,50
get accustomed to the way Webpack works.

98
00:04:22,50 --> 00:04:26,70
So just go wherever you want on your computer

99
00:04:26,70 --> 00:04:29,80
and create the folder, we'll create the folder

100
00:04:29,80 --> 00:04:31,60
called BasicApp in this case.

101
00:04:31,60 --> 00:04:33,20
We will change into the BasicApp

102
00:04:33,20 --> 00:04:35,00
and in here we will build

103
00:04:35,00 --> 00:04:38,60
our very first Webpack application.

104
00:04:38,60 --> 00:04:40,70
Keep in mind in order for you to

105
00:04:40,70 --> 00:04:44,90
be able to use Webpack you will need to have MPM installed.

106
00:04:44,90 --> 00:04:46,40
To check if you have MPM installed

107
00:04:46,40 --> 00:04:51,80
what you'll need to do is do MPM, MPM dash V.

108
00:04:51,80 --> 00:04:54,70
And if you have a number, if it shows you a number in here,

109
00:04:54,70 --> 00:04:57,00
it means you have MPM installed.

110
00:04:57,00 --> 00:05:00,40
If you do not have MPM installed make sure

111
00:05:00,40 --> 00:05:02,00
you go into Google and install it.

112
00:05:02,00 --> 00:05:06,90
So just go to Google and search for MPM

113
00:05:06,90 --> 00:05:09,80
and follow the installations at the top

114
00:05:09,80 --> 00:05:13,50
that you can find on the MPM website.

115
00:05:13,50 --> 00:05:16,10
MPM is basically a Javascript runtime

116
00:05:16,10 --> 00:05:17,50
and without it you will not be able

117
00:05:17,50 --> 00:05:21,00
to use any Webpack or any of the, not even React.

118
00:05:21,00 --> 00:05:22,80
So make sure you have MPM set up.

119
00:05:22,80 --> 00:05:25,70
What I personally use and I'll also personally recommend,

120
00:05:25,70 --> 00:05:28,40
is using a package manager.

121
00:05:28,40 --> 00:05:32,50
MPM and Note will come with MPM,

122
00:05:32,50 --> 00:05:37,20
however I do prefer using Yarn

123
00:05:37,20 --> 00:05:38,30
just because it gives you

124
00:05:38,30 --> 00:05:40,50
better control over your dependencies.

125
00:05:40,50 --> 00:05:43,40
So you can also look for Yarn.

126
00:05:43,40 --> 00:05:46,50
Let's do Yarn package manager.

127
00:05:46,50 --> 00:05:50,60
And make sure you go in here and also install Yarn.

128
00:05:50,60 --> 00:05:53,30
It has all the instructions in here

129
00:05:53,30 --> 00:05:58,40
on how to install it on both MacOS, Windows, and Linux.

130
00:05:58,40 --> 00:06:01,70
If you have, one more thing to note,

131
00:06:01,70 --> 00:06:05,30
if you have a MacOS or a Linux computer or device

132
00:06:05,30 --> 00:06:08,70
they should have both Node and MPM installed.

133
00:06:08,70 --> 00:06:11,10
So we should do a Node dash V as well.

134
00:06:11,10 --> 00:06:13,20
You must make sure Node is installed as well

135
00:06:13,20 --> 00:06:15,40
otherwise MPM will not work.

136
00:06:15,40 --> 00:06:17,90
And if not, make sure you install them first

137
00:06:17,90 --> 00:06:19,70
before starting this project just so you

138
00:06:19,70 --> 00:06:21,70
can get coding straight away.

139
00:06:21,70 --> 00:06:24,90
Now that we have installed and we make sure

140
00:06:24,90 --> 00:06:28,00
we have both MPM and Node and Yarn installed

141
00:06:28,00 --> 00:06:31,00
let's initialize our project.

142
00:06:31,00 --> 00:06:35,10
So if we go into our basic app we'll do Yarn in it

143
00:06:35,10 --> 00:06:37,30
in order to create a new project.

144
00:06:37,30 --> 00:06:40,50
And we will, it will ask a name, just call it,

145
00:06:40,50 --> 00:06:42,00
you can just press Enter for everything

146
00:06:42,00 --> 00:06:43,80
especially for this project.

147
00:06:43,80 --> 00:06:46,80
Press Enter, Enter and once we're done

148
00:06:46,80 --> 00:06:49,50
if we look at the folder structure

149
00:06:49,50 --> 00:06:54,90
you will see Yarn has created a file called Package.Json.

150
00:06:54,90 --> 00:06:57,00
If we open that in our code editor

151
00:06:57,00 --> 00:06:58,90
for this title line using Atom but

152
00:06:58,90 --> 00:07:01,60
you can use anything like Sublime or Notepack

153
00:07:01,60 --> 00:07:03,60
even if you want.

154
00:07:03,60 --> 00:07:06,20
I would recommend Atom or Sublime,

155
00:07:06,20 --> 00:07:08,30
they are both free and they're quite good editors

156
00:07:08,30 --> 00:07:10,20
so feel free to use that.

157
00:07:10,20 --> 00:07:12,10
So as you can see in our basic app folder

158
00:07:12,10 --> 00:07:16,40
we have our Package to Json with the name and the version

159
00:07:16,40 --> 00:07:21,70
and a few other things that Yarn will add for us by default.

160
00:07:21,70 --> 00:07:24,50
Now, if you have never used Webpack before

161
00:07:24,50 --> 00:07:25,90
now is the time to install it.

162
00:07:25,90 --> 00:07:28,00
If you plan on using Webpack a lot

163
00:07:28,00 --> 00:07:31,00
I would recommend installing it globally.

