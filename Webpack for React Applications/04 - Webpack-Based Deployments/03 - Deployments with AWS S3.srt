1
00:00:01,10 --> 00:00:03,50
- [Instructor] In the previous video we have looked at

2
00:00:03,50 --> 00:00:06,50
how to deploy our application to GitHub Pages.

3
00:00:06,50 --> 00:00:09,10
We have created our GitHub repository

4
00:00:09,10 --> 00:00:13,80
and uploaded our, this folder into specific branch

5
00:00:13,80 --> 00:00:19,90
called GH Pages, which allows us to serve it from GitHub.io.

6
00:00:19,90 --> 00:00:22,50
In this video we will look at deploying our application

7
00:00:22,50 --> 00:00:28,50
to an AWS S3 bucket and all the configurations

8
00:00:28,50 --> 00:00:33,90
we need in order to get that done.

9
00:00:33,90 --> 00:00:36,30
First of all we will have a look at what

10
00:00:36,30 --> 00:00:39,20
S3 buckets are and then we will list a utility plugin

11
00:00:39,20 --> 00:00:41,70
for S3, which comes in very useful

12
00:00:41,70 --> 00:00:44,20
in a lot of other projects, and then we'll

13
00:00:44,20 --> 00:00:46,50
have a look at how to create the credential

14
00:00:46,50 --> 00:00:48,90
and all the access permissions necessary

15
00:00:48,90 --> 00:00:52,00
in order to deploy our application

16
00:00:52,00 --> 00:00:56,20
into S3, so let's get started.

17
00:00:56,20 --> 00:00:58,70
First of all, let's go back into

18
00:00:58,70 --> 00:01:02,80
our terminal and install this utility.

19
00:01:02,80 --> 00:01:06,90
We'll do a yarn global add, and

20
00:01:06,90 --> 00:01:13,90
the utility is called s3-website.

21
00:01:13,90 --> 00:01:16,90
As the name, I'm installing it globally because as I said,

22
00:01:16,90 --> 00:01:20,00
it's very useful to have across various projects.

23
00:01:20,00 --> 00:01:22,20
As the name suggested, it will launch

24
00:01:22,20 --> 00:01:27,30
to deploy a project into an S3 bucket.

25
00:01:27,30 --> 00:01:30,10
Before we get on with that we need to do

26
00:01:30,10 --> 00:01:34,50
a few configurations into our Amazon account,

27
00:01:34,50 --> 00:01:37,70
so make sure you have an Amazon AWS account set up.

28
00:01:37,70 --> 00:01:41,60
So, I've already opened mine, so once you go

29
00:01:41,60 --> 00:01:47,70
into your AWS settings we will need to use S3,

30
00:01:47,70 --> 00:01:50,10
which is basically just a storage bucket.

31
00:01:50,10 --> 00:01:52,50
However, before we go in there we need to make

32
00:01:52,50 --> 00:01:55,10
a few changes to our account to make sure

33
00:01:55,10 --> 00:01:58,80
that Amazon knows to allow us to upload static files

34
00:01:58,80 --> 00:02:02,20
into a bucket using the S3 website.

35
00:02:02,20 --> 00:02:04,60
So, let's do that right now, so we'll need

36
00:02:04,60 --> 00:02:08,30
to create some API keys then give them

37
00:02:08,30 --> 00:02:11,30
to the S3 website tools that we've just installed,

38
00:02:11,30 --> 00:02:13,00
and that should be pretty much it.

39
00:02:13,00 --> 00:02:15,00
The tool will then handle the bucket

40
00:02:15,00 --> 00:02:18,60
creation and file upload after that.

41
00:02:18,60 --> 00:02:20,60
Now, going into our Amazon console

42
00:02:20,60 --> 00:02:23,10
all you have to do is click on your username

43
00:02:23,10 --> 00:02:26,80
and then go into the My Security Credentials.

44
00:02:26,80 --> 00:02:31,80
Once this loads we will need to just make sure...

45
00:02:31,80 --> 00:02:33,50
You hide this message.

46
00:02:33,50 --> 00:02:36,30
We will need to have a look at the

47
00:02:36,30 --> 00:02:40,70
access keys, so let's just click on that.

48
00:02:40,70 --> 00:02:43,40
And in here, keep in mind that those keys

49
00:02:43,40 --> 00:02:46,40
I'm going to generate I will delete immediately after,

50
00:02:46,40 --> 00:02:48,90
so make sure that whenever you generate those

51
00:02:48,90 --> 00:02:51,70
you also do not make them public, and if you

52
00:02:51,70 --> 00:02:54,60
happen to at any point make them public

53
00:02:54,60 --> 00:02:56,60
make sure you delete them just to prevent

54
00:02:56,60 --> 00:02:58,40
someone else from gaining access to your accounts.

55
00:02:58,40 --> 00:03:02,40
So, we will create a new access key at this point,

56
00:03:02,40 --> 00:03:05,50
so let's create a key by creating a new access key.

57
00:03:05,50 --> 00:03:09,80
At this point you see you have two options.

58
00:03:09,80 --> 00:03:12,50
You can either download or show access key.

59
00:03:12,50 --> 00:03:15,70
In this case, just for this we will show the access key.

60
00:03:15,70 --> 00:03:17,50
Again, keep in mind those are really

61
00:03:17,50 --> 00:03:20,50
sensitive information, so if you happen

62
00:03:20,50 --> 00:03:22,50
to make them public at any point to anyone

63
00:03:22,50 --> 00:03:24,60
just go back in here and deactivate

64
00:03:24,60 --> 00:03:27,40
them, don't ever make those public.

65
00:03:27,40 --> 00:03:28,40
As I said, after I finish recording

66
00:03:28,40 --> 00:03:30,80
I will disable those keys as well.

67
00:03:30,80 --> 00:03:32,20
However, for the purposes of the video

68
00:03:32,20 --> 00:03:37,50
let's just copy those and after we've copied them

69
00:03:37,50 --> 00:03:40,90
let's go back into our code editor

70
00:03:40,90 --> 00:03:44,00
and make a few changes in here.

71
00:03:44,00 --> 00:03:46,30
Now, whenever we run the S3 website command

72
00:03:46,30 --> 00:03:48,80
that will look into the current directory

73
00:03:48,80 --> 00:03:52,30
for a file called .env, so let's go

74
00:03:52,30 --> 00:03:55,30
ahead and create that new file.

75
00:03:55,30 --> 00:03:59,40
So, we will create a .env and in here we will

76
00:03:59,40 --> 00:04:02,60
paste the keys that we got from Amazon,

77
00:04:02,60 --> 00:04:04,50
not in this format but let's have

78
00:04:04,50 --> 00:04:06,60
a look at how we need to format them.

79
00:04:06,60 --> 00:04:09,80
So, first we will need to define

80
00:04:09,80 --> 00:04:14,00
AWS access key ID, so let's do that.

81
00:04:14,00 --> 00:04:21,60
AWS_KEY_ID and then =, and then the next thing

82
00:04:21,60 --> 00:04:25,40
we need to configure is the AWS secret access key.

83
00:04:25,40 --> 00:04:30,10
So, in here add the AWS secret access key,

84
00:04:30,10 --> 00:04:33,70
and as you can imagine, those are the exact keys

85
00:04:33,70 --> 00:04:37,90
that we've just copied from our AWS console.

86
00:04:37,90 --> 00:04:42,80
So, let's copy those over from here, add them

87
00:04:42,80 --> 00:04:47,90
in here, and then the secret access key as well.

88
00:04:47,90 --> 00:04:51,20
And delete everything else from the file

89
00:04:51,20 --> 00:04:55,50
and save our .env file, now if we flip back over

90
00:04:55,50 --> 00:04:57,10
to our terminal we should be able

91
00:04:57,10 --> 00:05:00,80
to deploy our application with AWS.

92
00:05:00,80 --> 00:05:03,30
So, first of all we will need to create our bucket,

93
00:05:03,30 --> 00:05:06,10
which is the one that we will be deploying to.

94
00:05:06,10 --> 00:05:12,00
So, to do that we will just do S3-website,

95
00:05:12,00 --> 00:05:15,20
web-site, and then create, and then

96
00:05:15,20 --> 00:05:18,00
we'll also need a name for our app.

97
00:05:18,00 --> 00:05:19,80
In this case we will go with

98
00:05:19,80 --> 00:05:22,10
webpack-test-app and then -deploy.

99
00:05:22,10 --> 00:05:25,10
Keep in mind this bucket, this name that you choose

100
00:05:25,10 --> 00:05:27,70
for your bucket will also appear in the URL

101
00:05:27,70 --> 00:05:30,50
that the website is searched from,

102
00:05:30,50 --> 00:05:31,90
so make sure you choose a sensible name.

103
00:05:31,90 --> 00:05:35,40
Press enter, and that will update your

104
00:05:35,40 --> 00:05:38,90
website config and you will see the link

105
00:05:38,90 --> 00:05:43,30
that Amazon will give you for that bucket.

106
00:05:43,30 --> 00:05:47,30
Keep in mind also that S3 buckets need to be

107
00:05:47,30 --> 00:05:48,70
uniquely named, so if somebody else has used

108
00:05:48,70 --> 00:05:51,50
this particular name, for example, you will need

109
00:05:51,50 --> 00:05:55,70
to create a different one or choose a different name.

110
00:05:55,70 --> 00:05:58,50
That's just a heads-up in case you get any errors

111
00:05:58,50 --> 00:06:02,90
at this stage, that is the cause for it,

112
00:06:02,90 --> 00:06:06,40
the bucket might already be in use by someone else,

113
00:06:06,40 --> 00:06:10,40
so just choose a different name for your bucket.

114
00:06:10,40 --> 00:06:14,60
Now, in order to deploy our code

115
00:06:14,60 --> 00:06:18,80
if we go back and check we already have

116
00:06:18,80 --> 00:06:21,90
our dist folder generated by webpack,

117
00:06:21,90 --> 00:06:25,90
so at this point we will do S3-website

118
00:06:25,90 --> 00:06:30,50
and then we'll type in deploy, and we will

119
00:06:30,50 --> 00:06:35,40
use our dist folder, and this should upload

120
00:06:35,40 --> 00:06:39,30
all our files into the bucket we've just created.

121
00:06:39,30 --> 00:06:42,00
So, as you can see it's uploaded all the files

122
00:06:42,00 --> 00:06:49,10
that we have in our dist into the bucket in AWS S3.

123
00:06:49,10 --> 00:06:51,50
And now if we go back up at the bucket

124
00:06:51,50 --> 00:06:55,20
and just copy over the URL, so let's copy this URL.

125
00:06:55,20 --> 00:06:57,90
Let's go back into our browser and try and access it.

126
00:06:57,90 --> 00:07:00,70
We will just paste this in, and once we've

127
00:07:00,70 --> 00:07:04,00
pasted it in, as you can see, our application

128
00:07:04,00 --> 00:07:08,70
is being hosted on this URL, and that's it.

129
00:07:08,70 --> 00:07:14,40
That basically covers deployments with AWS to AWS S3.

130
00:07:14,40 --> 00:07:16,40
It's quite a simple process, again,

131
00:07:16,40 --> 00:07:18,50
make sure you never make those keys public.

132
00:07:18,50 --> 00:07:20,10
I know I have, but I will just delete them

133
00:07:20,10 --> 00:07:24,20
after this video, so make sure you always

134
00:07:24,20 --> 00:07:27,10
keep those keys private to you or your

135
00:07:27,10 --> 00:07:31,80
organization depending on how you use this.

136
00:07:31,80 --> 00:07:36,00
So, this covers deployments to AWS.

137
00:07:36,00 --> 00:07:38,50
Again, you can create your node scripts

138
00:07:38,50 --> 00:07:41,30
into your package JSON, the same as we did

139
00:07:41,30 --> 00:07:43,50
with GitHub, in order for you to easily

140
00:07:43,50 --> 00:07:46,80
deploy with a simple yarn command.

141
00:07:46,80 --> 00:07:49,00
You can also do the build script and then

142
00:07:49,00 --> 00:07:54,80
at the end just do the S3 website deploy dist command,

143
00:07:54,80 --> 00:07:57,70
and that covers of this about deploying

144
00:07:57,70 --> 00:08:01,20
statics pages, pure front-end applications

145
00:08:01,20 --> 00:08:04,00
to both GitHub Pages and Amazon S3.

146
00:08:04,00 --> 00:08:05,60
Of course, as I mentioned, there are a lot

147
00:08:05,60 --> 00:08:07,70
more hosting options for your sites, however these

148
00:08:07,70 --> 00:08:11,80
are the most accessible and the easiest one to use.

149
00:08:11,80 --> 00:08:14,00
In the next video we will have a look at dynamic sites

150
00:08:14,00 --> 00:08:17,00
and we will have a quick look at the integration

151
00:08:17,00 --> 00:08:20,60
between node and webpack, and also how to deploy

152
00:08:20,60 --> 00:08:26,00
a more complex application to AWS EC2 instances.

