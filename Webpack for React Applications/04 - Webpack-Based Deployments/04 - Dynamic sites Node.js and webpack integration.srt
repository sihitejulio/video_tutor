1
00:00:01,50 --> 00:00:03,50
- [Instructor] So far in this section we have looked at

2
00:00:03,50 --> 00:00:06,00
how to get our application production ready.

3
00:00:06,00 --> 00:00:08,80
How to export the bundled app that is optimized

4
00:00:08,80 --> 00:00:11,50
and ready for production.

5
00:00:11,50 --> 00:00:13,60
And in the last videos we have looked at deploying

6
00:00:13,60 --> 00:00:17,00
a static frontend on the application, to get app pages,

7
00:00:17,00 --> 00:00:20,00
and AWS as three buckets.

8
00:00:20,00 --> 00:00:23,50
Now sometimes, even though that is a preferable way

9
00:00:23,50 --> 00:00:27,10
of deploying frontend, pure front as pure frontend

10
00:00:27,10 --> 00:00:29,50
applications, sometimes you might need to include

11
00:00:29,50 --> 00:00:34,30
a server in your deployment, and deploy the server as well.

12
00:00:34,30 --> 00:00:37,50
So we will have a look at that in this video

13
00:00:37,50 --> 00:00:41,40
and how to integrate node and the node js servers

14
00:00:41,40 --> 00:00:44,70
in Webpack whenever deploying our application.

15
00:00:44,70 --> 00:00:46,40
We will have a quick look at setting up

16
00:00:46,40 --> 00:00:49,10
a very very basic express server,

17
00:00:49,10 --> 00:00:51,00
we will then look at something called

18
00:00:51,00 --> 00:00:54,60
Webpack development middleware, which is what we'll use

19
00:00:54,60 --> 00:00:59,30
in order to connect our express server to our node server,

20
00:00:59,30 --> 00:01:02,30
to our static application, and the bundling process

21
00:01:02,30 --> 00:01:03,90
of Webpack.

22
00:01:03,90 --> 00:01:06,80
And then we look at how to configure the Webpack

23
00:01:06,80 --> 00:01:09,50
and the express server to run Webpack whenever

24
00:01:09,50 --> 00:01:12,40
the server is run.

25
00:01:12,40 --> 00:01:15,50
So to do that, let's quickly go back into our projects files

26
00:01:15,50 --> 00:01:20,60
into our terminal, and install express into our project.

27
00:01:20,60 --> 00:01:24,40
So install express, we will do that by doing yarn

28
00:01:24,40 --> 00:01:28,20
at express, let's quickly also change our branch

29
00:01:28,20 --> 00:01:31,00
to master as well.

30
00:01:31,00 --> 00:01:34,10
Now that we have installed express, let's make sure

31
00:01:34,10 --> 00:01:37,30
it is in our package json, so express should show up

32
00:01:37,30 --> 00:01:39,70
in here at any point now, as you can see,

33
00:01:39,70 --> 00:01:42,40
there we go.

34
00:01:42,40 --> 00:01:45,50
Let's create our server.js file and simulate

35
00:01:45,50 --> 00:01:48,30
a very simple server application and node.

36
00:01:48,30 --> 00:01:50,70
So for that, we will just create a new file

37
00:01:50,70 --> 00:01:54,10
that we will call, 'Server.js' and in here,

38
00:01:54,10 --> 00:01:57,40
we will simply listen to a port, and serve files

39
00:01:57,40 --> 00:01:59,50
on that port to a browser.

40
00:01:59,50 --> 00:02:01,80
So let's quickly do that here,

41
00:02:01,80 --> 00:02:05,60
we will import the express, so you can require express,

42
00:02:05,60 --> 00:02:08,90
and let's create our app which is an express app,

43
00:02:08,90 --> 00:02:11,70
and let's make our app listen, let's choose a port,

44
00:02:11,70 --> 00:02:16,30
say, 80 80 in our case, and once we listen,

45
00:02:16,30 --> 00:02:23,00
we will just console log listening on this port.

46
00:02:23,00 --> 00:02:26,40
Once we've done that, let's go back to our terminal

47
00:02:26,40 --> 00:02:28,40
and run our server, make sure everything

48
00:02:28,40 --> 00:02:30,00
is running correctly.

49
00:02:30,00 --> 00:02:35,00
So in our terminal, we will just run node server.js

50
00:02:35,00 --> 00:02:40,20
and as you can see, the express is listening.

51
00:02:40,20 --> 00:02:42,30
Now, before we move into including Webpack,

52
00:02:42,30 --> 00:02:45,10
let's quickly go for the diagram we had to look

53
00:02:45,10 --> 00:02:47,00
at the beginning of the section,

54
00:02:47,00 --> 00:02:49,40
this is related to deploying our applications

55
00:02:49,40 --> 00:02:51,30
with a server backend.

56
00:02:51,30 --> 00:02:55,00
So we have our app here, which is built with Webpack,

57
00:02:55,00 --> 00:02:57,90
so this is our built app, and then we also have

58
00:02:57,90 --> 00:03:00,50
a node server, which will handle multiple things

59
00:03:00,50 --> 00:03:06,80
like for example, authentication, API result handlers, etc.

60
00:03:06,80 --> 00:03:09,60
And then, we have the user browsers.

61
00:03:09,60 --> 00:03:11,70
The user browsers which will always connect

62
00:03:11,70 --> 00:03:16,40
to the node server and not straight to our application.

63
00:03:16,40 --> 00:03:19,30
So the user browser will receive our static files

64
00:03:19,30 --> 00:03:20,60
from the server.

65
00:03:20,60 --> 00:03:24,10
In order to do that, we need to introduce one thing

66
00:03:24,10 --> 00:03:28,80
in the node server, which is called the Webpack Middleware.

67
00:03:28,80 --> 00:03:31,30
So we will need to install this Webpack middleware

68
00:03:31,30 --> 00:03:33,60
and add it to our server just so our server,

69
00:03:33,60 --> 00:03:36,70
or node js servers, knows to run Webpack

70
00:03:36,70 --> 00:03:39,50
whenever it's deployed,

71
00:03:39,50 --> 00:03:41,60
And create the static assets that we need

72
00:03:41,60 --> 00:03:43,00
for our application to run.

73
00:03:43,00 --> 00:03:48,80
So let's do that.

74
00:03:48,80 --> 00:03:51,60
For that, we will flip back to our terminal,

75
00:03:51,60 --> 00:03:53,60
and in here we will cancel the server,

76
00:03:53,60 --> 00:03:54,90
we will install a package

77
00:03:54,90 --> 00:03:57,60
called 'Webpack Development Middleware'

78
00:03:57,60 --> 00:04:01,40
so we will go 'yarn at,' and in here,

79
00:04:01,40 --> 00:04:06,90
'webpack-dev-middleware'.

80
00:04:06,90 --> 00:04:08,70
And we will make sure to install this

81
00:04:08,70 --> 00:04:11,40
as a development dependency.

82
00:04:11,40 --> 00:04:15,40
Once this is installed, again,

83
00:04:15,40 --> 00:04:17,80
make sure if it's any warnings or anything

84
00:04:17,80 --> 00:04:20,30
about pure dependencies, just ignore them,

85
00:04:20,30 --> 00:04:24,60
they're usually fine, node can handle that on its own.

86
00:04:24,60 --> 00:04:27,60
Now that we've done this, we need to change our servers

87
00:04:27,60 --> 00:04:30,60
as well in order to work with Webpack Middleware.

88
00:04:30,60 --> 00:04:34,50
So let's do that and let's flip back to the code editor

89
00:04:34,50 --> 00:04:39,90
and add our Webpack configuration in here.

90
00:04:39,90 --> 00:04:41,50
We will need to add three things,

91
00:04:41,50 --> 00:04:45,10
first of all we'll need to include Webpack Middleware,

92
00:04:45,10 --> 00:04:47,40
then we will also need to import Webpack,

93
00:04:47,40 --> 00:04:50,00
and we'll also need to import the Webpack config

94
00:04:50,00 --> 00:04:52,20
that we have created.

95
00:04:52,20 --> 00:04:54,00
And then set the app to use it.

96
00:04:54,00 --> 00:04:56,40
So let's do that, first of all, let's import

97
00:04:56,40 --> 00:05:00,20
our Webpack Middleware, so Webpack Middleware,

98
00:05:00,20 --> 00:05:03,70
this will be required, and we will require

99
00:05:03,70 --> 00:05:06,80
'webpack-dev-middleware'.

100
00:05:06,80 --> 00:05:10,20
Now we will also import Webpack which is simply require,

101
00:05:10,20 --> 00:05:14,00
and we will require Webpack, and we will also need

102
00:05:14,00 --> 00:05:18,40
our config file, which is our Webpack.config.js

103
00:05:18,40 --> 00:05:21,50
so let's import that one, as well.

104
00:05:21,50 --> 00:05:23,50
You don't necessarily need to give these names

105
00:05:23,50 --> 00:05:26,80
to the constants, but it's a bit easier to work with

106
00:05:26,80 --> 00:05:29,50
at a later date if you just keep the naming conventions

107
00:05:29,50 --> 00:05:31,30
fairly straightforward.

108
00:05:31,30 --> 00:05:34,40
And this will be a local file, so let's choose

109
00:05:34,40 --> 00:05:38,90
Webpack.config.js.

110
00:05:38,90 --> 00:05:41,30
So we've imported everything we need,

111
00:05:41,30 --> 00:05:43,60
we have our app, which is an express app,

112
00:05:43,60 --> 00:05:45,80
and now we also need to make sure that our app

113
00:05:45,80 --> 00:05:51,00
uses Webpack Middleware with the configs from above.

114
00:05:51,00 --> 00:05:55,10
So we'll write 'app.use' and this might seem a bit tricky

115
00:05:55,10 --> 00:05:57,80
but it's quite an easy process, once you write it,

116
00:05:57,80 --> 00:06:00,60
so we'll do Webpack Middleware,

117
00:06:00,60 --> 00:06:04,00
the argument for this will be Webpack,

118
00:06:04,00 --> 00:06:06,60
which is the Webpack package itself,

119
00:06:06,60 --> 00:06:08,70
and then the argument that we pass to Webpack

120
00:06:08,70 --> 00:06:11,50
is going to be our Webpack config

121
00:06:11,50 --> 00:06:14,00
that we have declared above.

122
00:06:14,00 --> 00:06:17,10
So once again, we're taking our Webpack config,

123
00:06:17,10 --> 00:06:20,10
we're passing it to the Webpack,

124
00:06:20,10 --> 00:06:22,70
which is going to build our static assets,

125
00:06:22,70 --> 00:06:25,90
and then we're passing that to the Webpack Middleware

126
00:06:25,90 --> 00:06:30,50
which is then in use by our express app.

127
00:06:30,50 --> 00:06:32,20
So hopefully that all makes sense,

128
00:06:32,20 --> 00:06:34,80
it's just this is how we do the basic config

129
00:06:34,80 --> 00:06:39,60
for a node js server to run with Webpack.

130
00:06:39,60 --> 00:06:41,50
Now if we go back to our terminal and run,

131
00:06:41,50 --> 00:06:46,00
save this file, go into terminal and run the server again.

132
00:06:46,00 --> 00:06:48,90
Node js server will see we have listening,

133
00:06:48,90 --> 00:06:52,00
and now we should be able to access our application

134
00:06:52,00 --> 00:06:54,60
on local host 80 80.

135
00:06:54,60 --> 00:06:56,80
Again, as you can see in my terminal,

136
00:06:56,80 --> 00:07:00,10
now we can see that the output of Webpack,

137
00:07:00,10 --> 00:07:02,10
so Webpack has been run when the server

138
00:07:02,10 --> 00:07:04,50
started running as well, and it has created

139
00:07:04,50 --> 00:07:08,70
all the chunks that we expect to have in here.

140
00:07:08,70 --> 00:07:13,60
So, let's go back to our browser, access our 80 80 port,

141
00:07:13,60 --> 00:07:15,80
again, keep in mind this is 80 80

142
00:07:15,80 --> 00:07:21,00
because we've made it 80 80, in our server.js file.

143
00:07:21,00 --> 00:07:24,50
So let's open up a new tab, and let's do a local host 80 80,

144
00:07:24,50 --> 00:07:27,30
and as you can see, the MyReads app has been deployed

145
00:07:27,30 --> 00:07:30,40
to node js server with Webpack.

146
00:07:30,40 --> 00:07:33,70
So, we have used successfully our Webpack Dev Middleware

147
00:07:33,70 --> 00:07:36,60
in order to tell node to compile our application

148
00:07:36,60 --> 00:07:39,40
and then serve it to the user browser

149
00:07:39,40 --> 00:07:43,60
via the express app.

150
00:07:43,60 --> 00:07:45,30
So in this video, we have covered

151
00:07:45,30 --> 00:07:47,80
a quick architectural diagram

152
00:07:47,80 --> 00:07:50,30
about deploying Webapp based applications

153
00:07:50,30 --> 00:07:53,80
with node js servers attached as well.

154
00:07:53,80 --> 00:07:57,20
In this case, we don't just have a simple static page,

155
00:07:57,20 --> 00:08:00,50
we have a static page, plus a server as well.

156
00:08:00,50 --> 00:08:02,80
This server can do a lot of things,

157
00:08:02,80 --> 00:08:06,20
such as API routing, authentication handling, and so on,

158
00:08:06,20 --> 00:08:08,60
it depends on your application.

159
00:08:08,60 --> 00:08:12,90
We have done that by installing a Webpack Dev Middleware

160
00:08:12,90 --> 00:08:15,10
and configuring our express server

161
00:08:15,10 --> 00:08:18,90
to work with the said Webpack Dev Middleware.

162
00:08:18,90 --> 00:08:20,50
In the next video of this section,

163
00:08:20,50 --> 00:08:21,80
we'll have a look at how to deploy

164
00:08:21,80 --> 00:08:25,00
the server-based application to an Amazon instance,

165
00:08:25,00 --> 00:08:30,00
and how to get Webpack working with that as well.

