1
00:00:01,40 --> 00:00:03,70
- [Instructor] Welcome to the fourth and final section

2
00:00:03,70 --> 00:00:07,40
of boosting your React productivity with webpack.

3
00:00:07,40 --> 00:00:10,20
In the previous section we have discussed

4
00:00:10,20 --> 00:00:14,40
a few React-specific topics, as well as webpack

5
00:00:14,40 --> 00:00:16,80
dev server in order to improve your

6
00:00:16,80 --> 00:00:20,10
productivity when creating React applications.

7
00:00:20,10 --> 00:00:23,80
In this section we will talk all about deployments

8
00:00:23,80 --> 00:00:28,20
and how we can use webpack to deploy your applications.

9
00:00:28,20 --> 00:00:31,50
We will provide a good overview of deploying

10
00:00:31,50 --> 00:00:33,30
both static and dynamic sites.

11
00:00:33,30 --> 00:00:35,30
We'll talk a bit more about the differences

12
00:00:35,30 --> 00:00:38,20
between them and look at the most common platform

13
00:00:38,20 --> 00:00:42,00
you could deploy your React application on with webpack.

14
00:00:42,00 --> 00:00:44,70
First of all we will talk about building our application

15
00:00:44,70 --> 00:00:48,50
and what we need to do to get the app production, ready?

16
00:00:48,50 --> 00:00:50,10
After that we will quickly cover GitHub Pages

17
00:00:50,10 --> 00:00:52,60
and how we can deploy our app to GitHub Pages

18
00:00:52,60 --> 00:00:56,00
very easily, then we will also look at deploying

19
00:00:56,00 --> 00:01:00,50
static application to AWS S3, so we will just

20
00:01:00,50 --> 00:01:02,30
host an app in our bucket and make sure

21
00:01:02,30 --> 00:01:04,70
we have the configuration set up

22
00:01:04,70 --> 00:01:06,90
so that the bucket can serve the files.

23
00:01:06,90 --> 00:01:08,80
After that, for more complex applications

24
00:01:08,80 --> 00:01:10,70
that also involve deploying a backend,

25
00:01:10,70 --> 00:01:13,50
we will look at node and webpack integration

26
00:01:13,50 --> 00:01:15,90
and how we can integrate the server

27
00:01:15,90 --> 00:01:18,40
into our webpack projects, and at the end

28
00:01:18,40 --> 00:01:23,70
we will also look at deploying dynamic sites to AWS.

29
00:01:23,70 --> 00:01:25,70
In the first part let's have a look at

30
00:01:25,70 --> 00:01:27,40
what do we need to do in order to get

31
00:01:27,40 --> 00:01:29,20
our application production ready.

32
00:01:29,20 --> 00:01:31,30
So, if you remember in the previous section

33
00:01:31,30 --> 00:01:33,90
we have built our application, we have optimized it,

34
00:01:33,90 --> 00:01:36,20
now we need to be able to deploy it.

35
00:01:36,20 --> 00:01:38,70
So, first of all I will quickly go with you

36
00:01:38,70 --> 00:01:40,80
through the differences between

37
00:01:40,80 --> 00:01:42,50
static app and server deployments.

38
00:01:42,50 --> 00:01:44,90
We will see the main distinction between

39
00:01:44,90 --> 00:01:48,00
those two types of processes, then we will

40
00:01:48,00 --> 00:01:51,30
quickly update our config files with some

41
00:01:51,30 --> 00:01:54,40
pre-deploy configurations and talk about

42
00:01:54,40 --> 00:01:56,30
what we need to do there, and then

43
00:01:56,30 --> 00:01:58,70
we will also update our build scripts

44
00:01:58,70 --> 00:02:01,60
so that we can build a production-ready app

45
00:02:01,60 --> 00:02:04,70
and create a production-ready bundle .js.

46
00:02:04,70 --> 00:02:07,60
So, first of all let's talk about

47
00:02:07,60 --> 00:02:09,40
a statically served application.

48
00:02:09,40 --> 00:02:13,30
Basically a static application is a project

49
00:02:13,30 --> 00:02:16,40
in which webpack produces a built app

50
00:02:16,40 --> 00:02:18,90
that consists of our index HTML file

51
00:02:18,90 --> 00:02:21,70
as well as our bundle .js, vendor .js,

52
00:02:21,70 --> 00:02:24,00
and any other chunks that we've generated,

53
00:02:24,00 --> 00:02:27,70
so basically just the HTML and all the JavaScript

54
00:02:27,70 --> 00:02:31,30
required for application, and then this app

55
00:02:31,30 --> 00:02:33,80
is exactly what runs inside the users' browsers.

56
00:02:33,80 --> 00:02:36,00
So, we have no server component,

57
00:02:36,00 --> 00:02:38,70
we have no corresponding backend for this type

58
00:02:38,70 --> 00:02:40,80
of application, of course if we need to

59
00:02:40,80 --> 00:02:43,40
connect this application to a backend API

60
00:02:43,40 --> 00:02:46,00
we can do that, and that backend API

61
00:02:46,00 --> 00:02:49,10
is developed separately but is not tied

62
00:02:49,10 --> 00:02:50,80
into this application, so basically

63
00:02:50,80 --> 00:02:54,40
with static asset applications you have your app

64
00:02:54,40 --> 00:02:57,30
containing your files that you're exported,

65
00:02:57,30 --> 00:03:01,00
and that is exactly what runs in the users' browsers.

66
00:03:01,00 --> 00:03:02,90
Now, on the other option, the other hand,

67
00:03:02,90 --> 00:03:05,80
we have more complex applications,

68
00:03:05,80 --> 00:03:09,70
which also involve a backend component.

69
00:03:09,70 --> 00:03:13,10
In this case we will have our front-end application,

70
00:03:13,10 --> 00:03:16,10
so it's exactly the same as the static files,

71
00:03:16,10 --> 00:03:19,50
however, there is an extra layer between our application

72
00:03:19,50 --> 00:03:23,40
and the user browsers, which is the server.

73
00:03:23,40 --> 00:03:26,30
This server can be pretty much anything,

74
00:03:26,30 --> 00:03:29,30
can be a node server, ruby rails, a .net server,

75
00:03:29,30 --> 00:03:33,50
jango, anything else like that, and this server

76
00:03:33,50 --> 00:03:36,40
will be used to serve these pre-built assets,

77
00:03:36,40 --> 00:03:40,00
but it can also be used to handle things like accessing

78
00:03:40,00 --> 00:03:44,00
a database, user authentication, and so on.

79
00:03:44,00 --> 00:03:47,20
In this case the user browsers will always communicate,

80
00:03:47,20 --> 00:03:50,70
will primarily interact with our server

81
00:03:50,70 --> 00:03:53,60
and then the server will provide the browser

82
00:03:53,60 --> 00:03:58,50
with our static assets that we generate with webpack.

83
00:03:58,50 --> 00:04:01,20
So, when talking about webpack deployments

84
00:04:01,20 --> 00:04:03,40
we need to consider whether we have a static,

85
00:04:03,40 --> 00:04:06,40
front-end application or if we need to

86
00:04:06,40 --> 00:04:08,10
also consider a backend server as well.

87
00:04:08,10 --> 00:04:10,60
Most of the times what is recommended

88
00:04:10,60 --> 00:04:13,30
is go with pure front-end applications,

89
00:04:13,30 --> 00:04:16,00
basically just have a static application

90
00:04:16,00 --> 00:04:17,90
and then the backend for your service can be

91
00:04:17,90 --> 00:04:20,60
developed separately and the app can interact

92
00:04:20,60 --> 00:04:23,20
with the backend via some sort of API.

93
00:04:23,20 --> 00:04:27,90
So, this is my preferred method of deploying applications,

94
00:04:27,90 --> 00:04:31,30
so let's go, let's quickly go over some of the providers

95
00:04:31,30 --> 00:04:33,50
that you can deploy your application with,

96
00:04:33,50 --> 00:04:36,40
and then let's have a look at what we need

97
00:04:36,40 --> 00:04:40,00
to do with webpack before we can deploy.

98
00:04:40,00 --> 00:04:42,20
Now, there are a few differences between the platforms,

99
00:04:42,20 --> 00:04:45,80
however, handling the webpack part of the project

100
00:04:45,80 --> 00:04:48,40
is mostly the same for all of them.

101
00:04:48,40 --> 00:04:50,50
Let's quickly go through a few

102
00:04:50,50 --> 00:04:53,10
example of static sites deployments.

103
00:04:53,10 --> 00:04:57,20
First of all we will talk about GitHub Pages,

104
00:04:57,20 --> 00:04:59,70
which is obviously supported by GitHub,

105
00:04:59,70 --> 00:05:01,80
it's one of GitHub's products and it makes it

106
00:05:01,80 --> 00:05:04,70
really easy for you to deploy in server static application.

107
00:05:04,70 --> 00:05:08,60
You can find more information about it at Pages.GitHub.com,

108
00:05:08,60 --> 00:05:12,40
then another really easy way for you to store a static,

109
00:05:12,40 --> 00:05:15,40
front-end-only application is to use an Amazon S3 bucket

110
00:05:15,40 --> 00:05:20,10
and configure it so that it can serve files to the web.

111
00:05:20,10 --> 00:05:23,30
You can also use some of Microsoft Azure.

112
00:05:23,30 --> 00:05:26,00
Again, as I said, this is up to preference

113
00:05:26,00 --> 00:05:28,50
and it depends a lot on the rest of your project

114
00:05:28,50 --> 00:05:31,20
where you decide to store it, and we'll also

115
00:05:31,20 --> 00:05:35,10
go for server-based applications in which

116
00:05:35,10 --> 00:05:37,70
we need to deploy a server as well.

117
00:05:37,70 --> 00:05:41,90
We'll also have a look at the Amazon EC2 instances,

118
00:05:41,90 --> 00:05:45,70
as well as the Elastic Block Store from Amazon

119
00:05:45,70 --> 00:05:48,30
and how to deploy it in there as well.

120
00:05:48,30 --> 00:05:50,10
Keep in mind you can also deploy server applications

121
00:05:50,10 --> 00:05:54,70
to other services, such as Microsoft Azure

122
00:05:54,70 --> 00:06:00,90
or even Heroku or anything like that.

123
00:06:00,90 --> 00:06:02,90
So, with that out of the way let's go

124
00:06:02,90 --> 00:06:06,00
into our project, into our code,

125
00:06:06,00 --> 00:06:09,70
and let's make the necessary requirements

126
00:06:09,70 --> 00:06:13,10
into our webpack config .js, as well as

127
00:06:13,10 --> 00:06:14,70
our package JSON in order to get

128
00:06:14,70 --> 00:06:17,40
our application production ready.

129
00:06:17,40 --> 00:06:21,20
So, in webpack config .js we will need to go

130
00:06:21,20 --> 00:06:23,10
all the way to the bottom into our plugins.

131
00:06:23,10 --> 00:06:25,20
As you can see here we have our HTML WebpackPlugin,

132
00:06:25,20 --> 00:06:28,50
our optimize CommonsChunkPlugin, as well as

133
00:06:28,50 --> 00:06:32,80
our HotModuleReplacementPlugin for quick React development.

134
00:06:32,80 --> 00:06:35,50
We will create a new plugin here, and I will just

135
00:06:35,50 --> 00:06:38,10
type this out and then explain it to you.

136
00:06:38,10 --> 00:06:42,90
So, we will need to create a new

137
00:06:42,90 --> 00:06:50,80
webpack.DefinePlugin, and in here we will pass

138
00:06:50,80 --> 00:06:54,60
a config objects, and make sure you type this in

139
00:06:54,60 --> 00:07:05,30
exactly as I do, so we will have a process.env.NODE_ENV,

140
00:07:05,30 --> 00:07:14,40
and this one we will do JSON.stringify,

141
00:07:14,40 --> 00:07:23,20
and then in here we will do process.env.NODE_ENV.

142
00:07:23,20 --> 00:07:25,20
Now, this might not necessarily make a lot

143
00:07:25,20 --> 00:07:27,70
of sense right now, but let's go over why

144
00:07:27,70 --> 00:07:31,40
we need this added into our config file.

145
00:07:31,40 --> 00:07:33,40
First of all, in our project we have

146
00:07:33,40 --> 00:07:36,90
several libraries, including rag .js.

147
00:07:36,90 --> 00:07:41,30
Those libraries make use of this NODE_ENV

148
00:07:41,30 --> 00:07:45,60
flag that we have defined in here.

149
00:07:45,60 --> 00:07:48,00
When React runs it will always look for

150
00:07:48,00 --> 00:07:53,70
a Windows variable called process.env.NODE_ENV.

151
00:07:53,70 --> 00:07:56,70
If this variable is equal to production, React will behave

152
00:07:56,70 --> 00:08:00,10
a little differently than it does in development.

153
00:08:00,10 --> 00:08:02,20
The biggest difference will be that React

154
00:08:02,20 --> 00:08:05,40
will do fewer error and warning, error checking

155
00:08:05,40 --> 00:08:08,30
and warning procedure while it renders your application.

156
00:08:08,30 --> 00:08:10,50
This is obviously preferable in production

157
00:08:10,50 --> 00:08:12,80
because you don't want to overload

158
00:08:12,80 --> 00:08:14,60
the users' browsers with any errors or so.

159
00:08:14,60 --> 00:08:18,20
However, in development you want to keep

160
00:08:18,20 --> 00:08:21,80
all the error reporting that React has by default.

161
00:08:21,80 --> 00:08:24,70
So, to achieve this we will make

162
00:08:24,70 --> 00:08:27,80
use of webpack's DefinePlugin.

163
00:08:27,80 --> 00:08:32,30
This basically just lets us define a Windows variable

164
00:08:32,30 --> 00:08:35,70
that will be defined whenever bundle .js runs,

165
00:08:35,70 --> 00:08:39,80
so whenever your app gets loaded into

166
00:08:39,80 --> 00:08:44,20
a browser webpack's DefinePlugin will set this

167
00:08:44,20 --> 00:08:47,60
Window variable to whatever we tell it to.

168
00:08:47,60 --> 00:08:51,20
Now, the NODE_ENV is a variable that we need to set

169
00:08:51,20 --> 00:08:53,40
on our machine on our own before making

170
00:08:53,40 --> 00:08:56,00
any builds deployments, so we will have

171
00:08:56,00 --> 00:08:58,10
a look at how to do that as well.

172
00:08:58,10 --> 00:09:03,20
So, let's say for webpack config .js,

173
00:09:03,20 --> 00:09:06,10
and let's go back into our package JSON.

174
00:09:06,10 --> 00:09:08,00
As you can see, now we have the dev

175
00:09:08,00 --> 00:09:10,80
script, the clean, and the build.

176
00:09:10,80 --> 00:09:13,30
Let's make a few changes to our build script

177
00:09:13,30 --> 00:09:17,80
so that we can build the application in production mode.

178
00:09:17,80 --> 00:09:20,40
So, first of all we will need to define

179
00:09:20,40 --> 00:09:23,30
the NODE_ENV variable as I mentioned before,

180
00:09:23,30 --> 00:09:32,10
so we will just define it as NODE_ENV=production,

181
00:09:32,10 --> 00:09:34,90
so this way webpack will know to define

182
00:09:34,90 --> 00:09:37,60
this global variable as production.

183
00:09:37,60 --> 00:09:42,70
Now, we will also do an &&, we will run

184
00:09:42,70 --> 00:09:45,50
our yarn clean script as well, just so we clean

185
00:09:45,50 --> 00:09:50,10
all the disc folder and then we will also run webpack.

186
00:09:50,10 --> 00:09:53,50
However, instead of running it with the flag -d

187
00:09:53,50 --> 00:09:56,60
we will run it with a flag -p,

188
00:09:56,60 --> 00:09:58,60
which means, which stands for production.

189
00:09:58,60 --> 00:10:02,00
Let's save our package JSON, and now we have

190
00:10:02,00 --> 00:10:06,00
everything we need to make sure that whenever

191
00:10:06,00 --> 00:10:09,00
the project is built webpack will know

192
00:10:09,00 --> 00:10:11,40
that this is the production version.

193
00:10:11,40 --> 00:10:13,30
If we do not add this NODE_ENV=production

194
00:10:13,30 --> 00:10:16,50
and webpack -p, webpack will always assume

195
00:10:16,50 --> 00:10:19,70
the build is a development build.

196
00:10:19,70 --> 00:10:22,90
Now, this -p at the end will also tell webpack

197
00:10:22,90 --> 00:10:24,70
we want production version of our project,

198
00:10:24,70 --> 00:10:27,40
then this will mean that webpack will automatically

199
00:10:27,40 --> 00:10:29,90
minify the .js code and it will automatically

200
00:10:29,90 --> 00:10:33,20
compact our code into the minimum size possible.

201
00:10:33,20 --> 00:10:35,50
Well, this will increase the build time,

202
00:10:35,50 --> 00:10:38,40
it will also ensure the production assets

203
00:10:38,40 --> 00:10:40,60
are significantly smaller, sometimes even

204
00:10:40,60 --> 00:10:44,80
30% to 40% smaller than our typical file.

205
00:10:44,80 --> 00:10:47,20
So, with this, with all this config added

206
00:10:47,20 --> 00:10:51,00
let's go into our terminal and run our script.

207
00:10:51,00 --> 00:10:53,80
Keep in mind that this production build

208
00:10:53,80 --> 00:10:55,60
will take significantly longer than our dev builds,

209
00:10:55,60 --> 00:10:58,20
so let's do a quick comparison of that.

210
00:10:58,20 --> 00:11:02,40
Let's build our project with webpack -d,

211
00:11:02,40 --> 00:11:05,60
which is the development build,

212
00:11:05,60 --> 00:11:08,90
and as you can see, in this case

213
00:11:08,90 --> 00:11:14,30
it has created our, all of our chunks and everything,

214
00:11:14,30 --> 00:11:18,00
and now let us also run the new build script,

215
00:11:18,00 --> 00:11:22,80
so we will do that by running yarn build.

216
00:11:22,80 --> 00:11:25,00
And now this time you will see we set

217
00:11:25,00 --> 00:11:28,10
the NODE_ENV to production, we will do yarn clean,

218
00:11:28,10 --> 00:11:32,20
and then a webpack -p, as you can see it's

219
00:11:32,20 --> 00:11:35,00
taking a bit longer to build, so it took almost

220
00:11:35,00 --> 00:11:37,70
double the time of the previous one to build.

221
00:11:37,70 --> 00:11:40,50
However, now we have all the transcript minified

222
00:11:40,50 --> 00:11:46,20
and our project is ready to be deployed in production.

223
00:11:46,20 --> 00:11:49,10
So, in this video we have looked at

224
00:11:49,10 --> 00:11:51,80
the main differences between deploying

225
00:11:51,80 --> 00:11:53,90
static asset and custom server deployments.

226
00:11:53,90 --> 00:11:56,30
We have also looked at a few of the most

227
00:11:56,30 --> 00:11:59,80
common providers for hosting both static assets

228
00:11:59,80 --> 00:12:02,90
and server-based applications, then we have

229
00:12:02,90 --> 00:12:07,20
updated our webpacks with some pre-deployed configs,

230
00:12:07,20 --> 00:12:09,50
and we have also updated our build script.

231
00:12:09,50 --> 00:12:11,80
Now that our app has been built for production

232
00:12:11,80 --> 00:12:14,50
we're ready to start deploying it.

233
00:12:14,50 --> 00:12:15,70
So, in the next video we will start

234
00:12:15,70 --> 00:12:20,00
with deployments to GitHub Pages.

