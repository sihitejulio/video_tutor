1
00:00:01,70 --> 00:00:02,90
- [Instructor] In the previous video,

2
00:00:02,90 --> 00:00:06,30
we have looked at how to integrate our Webpack build process

3
00:00:06,30 --> 00:00:10,60
and bundling process with a node js server.

4
00:00:10,60 --> 00:00:12,30
In this video, let's have a look at how

5
00:00:12,30 --> 00:00:15,40
to deploy this node server together

6
00:00:15,40 --> 00:00:24,10
with our built app to AWS Elastic Beanstalk.

7
00:00:24,10 --> 00:00:26,50
First of all, we are going to talk

8
00:00:26,50 --> 00:00:29,20
about what AWS Elastic Beanstalk is

9
00:00:29,20 --> 00:00:33,40
and why it's such a great tool, and also have a look

10
00:00:33,40 --> 00:00:35,20
at some of the features that are available.

11
00:00:35,20 --> 00:00:39,00
Then we will install some of the command line interfaces

12
00:00:39,00 --> 00:00:41,10
that Amazon provides that work very well

13
00:00:41,10 --> 00:00:43,00
with Elastic Beanstalk, and then,

14
00:00:43,00 --> 00:00:44,10
towards the end of the video,

15
00:00:44,10 --> 00:00:46,00
we will be deploying our server

16
00:00:46,00 --> 00:00:50,30
and our app to AWS Elastic Beanstalk.

17
00:00:50,30 --> 00:00:52,30
So we have our server here

18
00:00:52,30 --> 00:00:54,00
which is configured to run with nug.

19
00:00:54,00 --> 00:00:58,00
Now, let's talk a bit about what is Elastic Beanstalk

20
00:00:58,00 --> 00:01:00,20
and why it's such a powerful tool.

21
00:01:00,20 --> 00:01:03,40
It's basically comprised of dozens of services

22
00:01:03,40 --> 00:01:07,80
which each expose a specific area of functionality.

23
00:01:07,80 --> 00:01:09,50
There is a lot other IPO services

24
00:01:09,50 --> 00:01:12,00
that offer you a lot of flexibility

25
00:01:12,00 --> 00:01:16,40
on how you want to manage your AWS infrastructure.

26
00:01:16,40 --> 00:01:19,80
So basically, you can quickly manage and deploy application

27
00:01:19,80 --> 00:01:21,40
into the cloud without worrying

28
00:01:21,40 --> 00:01:23,30
about the infrastructure layer

29
00:01:23,30 --> 00:01:25,60
that runs those application also.

30
00:01:25,60 --> 00:01:27,50
You have a free tier, so whenever,

31
00:01:27,50 --> 00:01:29,80
if your application is below a specific usage,

32
00:01:29,80 --> 00:01:32,60
you will have to pay nothing,

33
00:01:32,60 --> 00:01:35,60
and when your application grows, it becomes more popular,

34
00:01:35,60 --> 00:01:40,50
then you will have to probably start paying for the service.

35
00:01:40,50 --> 00:01:42,60
However, for the beginning, it's a great place

36
00:01:42,60 --> 00:01:45,30
to deploy your application.

37
00:01:45,30 --> 00:01:48,50
It also automatically scales based on the usage,

38
00:01:48,50 --> 00:01:51,90
so you will always pay for only what you need,

39
00:01:51,90 --> 00:01:54,90
and it also supports applications developed

40
00:01:54,90 --> 00:01:59,90
in Java, PHP, Nodes, Python, Ruby, and so on,

41
00:01:59,90 --> 00:02:01,70
making it very easy to create,

42
00:02:01,70 --> 00:02:03,00
for you to create your server

43
00:02:03,00 --> 00:02:04,90
in whatever you're comfortable with,

44
00:02:04,90 --> 00:02:06,90
and then deploy it together with a front end

45
00:02:06,90 --> 00:02:10,60
that you bundled with Webpack.

46
00:02:10,60 --> 00:02:13,20
Now if, in order to continue,

47
00:02:13,20 --> 00:02:15,20
you will need to install, as I mentioned,

48
00:02:15,20 --> 00:02:19,30
the command line interface tools that AWS provides.

49
00:02:19,30 --> 00:02:21,90
All you need to do is Google search for them.

50
00:02:21,90 --> 00:02:24,80
So AWS CLI and then the platform you want

51
00:02:24,80 --> 00:02:25,90
to install them on.

52
00:02:25,90 --> 00:02:29,20
In this case, if you are on a Mac,

53
00:02:29,20 --> 00:02:31,20
it will be extremely easy to install them.

54
00:02:31,20 --> 00:02:33,90
So just go to the link you see on the screen,

55
00:02:33,90 --> 00:02:41,30
and then if you use brew, just run brew install AWS EBCLI

56
00:02:41,30 --> 00:02:43,60
and once that's done, you will be able

57
00:02:43,60 --> 00:02:48,90
to run the command eb version in your terminal.

58
00:02:48,90 --> 00:02:53,10
So if you run eb dash dash version in your terminal,

59
00:02:53,10 --> 00:02:57,50
you'll be able to see that version of deploy is installed.

60
00:02:57,50 --> 00:02:59,00
If it's not exactly the same version

61
00:02:59,00 --> 00:03:00,20
that I have, do not worry.

62
00:03:00,20 --> 00:03:03,30
That should work the same across all versions.

63
00:03:03,30 --> 00:03:04,70
So make sure you install this.

64
00:03:04,70 --> 00:03:05,80
If you need to pause the video

65
00:03:05,80 --> 00:03:07,80
in order to install it, do that.

66
00:03:07,80 --> 00:03:09,70
I will assume you have it installed

67
00:03:09,70 --> 00:03:12,30
going forward in this video.

68
00:03:12,30 --> 00:03:15,40
So now, the first thing we need to do is once we are

69
00:03:15,40 --> 00:03:24,90
in our app folder, we will need to run eb init.

70
00:03:24,90 --> 00:03:30,10
You will see that immediately, at this point,

71
00:03:30,10 --> 00:03:31,70
the tools will ask you for a region

72
00:03:31,70 --> 00:03:33,90
you want your app to be hosted in.

73
00:03:33,90 --> 00:03:37,60
Make sure you choose a correct region

74
00:03:37,60 --> 00:03:40,70
for where you expect most of your users to be.

75
00:03:40,70 --> 00:03:43,60
In the future, you can deploy, once your app is big enough,

76
00:03:43,60 --> 00:03:46,80
you can deploy it across multiple data centers,

77
00:03:46,80 --> 00:03:50,80
but for now, let's just go with eu west one in this case,

78
00:03:50,80 --> 00:03:53,50
so we will do four, and then,

79
00:03:53,50 --> 00:03:55,20
it will ask for your application name.

80
00:03:55,20 --> 00:04:02,80
Again, here, I will just call it basic webpack app.

81
00:04:02,80 --> 00:04:04,70
Seems we are not authorized, so we'll need

82
00:04:04,70 --> 00:04:07,50
to set these authentication again.

83
00:04:07,50 --> 00:04:09,90
This will be from our end file that we've created

84
00:04:09,90 --> 00:04:16,20
when doing the buckets, so let's do that quickly.

85
00:04:16,20 --> 00:04:21,40
So again, we will go into our AWS console.

86
00:04:21,40 --> 00:04:23,20
Click on your account.

87
00:04:23,20 --> 00:04:24,60
Security credential, we will create

88
00:04:24,60 --> 00:04:28,50
a new access key in here.

89
00:04:28,50 --> 00:04:31,50
Let's create a new access key, and then show access key,

90
00:04:31,50 --> 00:04:39,20
and we will use these again in order to enable our CLIs

91
00:04:39,20 --> 00:04:43,30
to work with the Elastic Beanstalk.

92
00:04:43,30 --> 00:04:45,30
Again, make sure you do not put those into git anywhere.

93
00:04:45,30 --> 00:04:49,00
Make sure you always keep them secure

94
00:04:49,00 --> 00:04:52,50
and not make them public.

95
00:04:52,50 --> 00:04:55,30
So once you have your keys, just make sure

96
00:04:55,30 --> 00:05:01,80
to use the AWS CLI tools to have them set up,

97
00:05:01,80 --> 00:05:05,50
and then run eb init again,

98
00:05:05,50 --> 00:05:09,30
and then we will choose the region again.

99
00:05:09,30 --> 00:05:11,50
We will create a new,

100
00:05:11,50 --> 00:05:13,50
the app has been detected as basic app,

101
00:05:13,50 --> 00:05:18,10
and we will create a new application,

102
00:05:18,10 --> 00:05:21,60
and AWS CLI will try to automatically detect

103
00:05:21,60 --> 00:05:22,40
what we are using.

104
00:05:22,40 --> 00:05:23,90
In this case, it has detected correctly

105
00:05:23,90 --> 00:05:27,40
that we're using node js, so you'll press yes,

106
00:05:27,40 --> 00:05:31,30
and then do you wish to continue with code commit?

107
00:05:31,30 --> 00:05:32,50
You don't need to worry about this.

108
00:05:32,50 --> 00:05:40,50
Just press enter from now on.

109
00:05:40,50 --> 00:05:45,40
Again, just continue using the default in here.

110
00:05:45,40 --> 00:05:47,40
No passphrase.

111
00:05:47,40 --> 00:05:49,50
Again, don't need to worry about this.

112
00:05:49,50 --> 00:05:52,90
So now we have created our application into EBS.

113
00:05:52,90 --> 00:05:54,60
Let's look at how to actually

114
00:05:54,60 --> 00:05:57,30
deploy our node server in there.

115
00:05:57,30 --> 00:06:00,30
So now, let's run eb create now

116
00:06:00,30 --> 00:06:05,00
that we've initialized our account.

117
00:06:05,00 --> 00:06:08,70
Just defaults, DNS name default,

118
00:06:08,70 --> 00:06:14,50
and then we will also ask for a basic load balancer type.

119
00:06:14,50 --> 00:06:17,40
Now we just want a classic load balancer.

120
00:06:17,40 --> 00:06:20,80
Again, if you want to go into more details on this,

121
00:06:20,80 --> 00:06:27,20
this is a bit more advanced configuration for AWS Beanstalk.

122
00:06:27,20 --> 00:06:30,40
You can go into Amazon's documentation for this,

123
00:06:30,40 --> 00:06:32,00
but for now, we will just go

124
00:06:32,00 --> 00:06:37,90
with the default, and just press enter,

125
00:06:37,90 --> 00:06:41,20
and this will create our application as you can see,

126
00:06:41,20 --> 00:06:43,30
and then also deploy our code

127
00:06:43,30 --> 00:06:45,10
that we have into our application.

128
00:06:45,10 --> 00:06:51,20
So let's wait for this to finish.

129
00:06:51,20 --> 00:06:54,50
It might take a while, so I'm gonna pause the video now.

130
00:06:54,50 --> 00:06:57,20
Keep in mind that this deployment process will take a while,

131
00:06:57,20 --> 00:06:58,80
especially the first time you do it,

132
00:06:58,80 --> 00:07:03,00
so just be patient with it.

133
00:07:03,00 --> 00:07:09,00
We also need to change some little things after this,

134
00:07:09,00 --> 00:07:14,40
so I will see you once the deployment process is finished.

135
00:07:14,40 --> 00:07:16,50
In the meantime while the application is deploying,

136
00:07:16,50 --> 00:07:22,00
let's flick back to our server js and make a few changes

137
00:07:22,00 --> 00:07:24,60
to our server just to get it ready

138
00:07:24,60 --> 00:07:26,70
for deployment with Elastic Beanstalk.

139
00:07:26,70 --> 00:07:30,80
So let's, if you remember, we talked about that node env,

140
00:07:30,80 --> 00:07:32,60
about production, so let's get

141
00:07:32,60 --> 00:07:35,30
that included in here as well.

142
00:07:35,30 --> 00:07:38,80
So we will do an if else state and make sure that

143
00:07:38,80 --> 00:07:44,30
whenever the node is run in production,

144
00:07:44,30 --> 00:07:48,60
we run our webpack from a static directory

145
00:07:48,60 --> 00:07:51,40
and not build it every time the page is accessed,

146
00:07:51,40 --> 00:07:53,60
so let's quickly take care of that,

147
00:07:53,60 --> 00:08:01,80
so if process dot env dot node env if you remember.

148
00:08:01,80 --> 00:08:05,70
If this is not production,

149
00:08:05,70 --> 00:08:08,50
so if we're running this in development,

150
00:08:08,50 --> 00:08:10,70
then what we will want to do, we will want

151
00:08:10,70 --> 00:08:13,00
to move all of those in here,

152
00:08:13,00 --> 00:08:19,80
so we will want to build our application.

153
00:08:19,80 --> 00:08:21,10
We'll move this in here.

154
00:08:21,10 --> 00:08:23,90
We'll make sure the middleware is included,

155
00:08:23,90 --> 00:08:25,00
that webpack is included

156
00:08:25,00 --> 00:08:26,90
and webpack config is included as well,

157
00:08:26,90 --> 00:08:31,30
and then we will ask our app to use the middleware webpack,

158
00:08:31,30 --> 00:08:34,30
the webpack middleware, so let's add that in here,

159
00:08:34,30 --> 00:08:37,70
and in case we're not running this in development mode

160
00:08:37,70 --> 00:08:39,10
and we're running it in production,

161
00:08:39,10 --> 00:08:42,20
we will want to serve the files that are available

162
00:08:42,20 --> 00:08:46,80
into the dist directory.

163
00:08:46,80 --> 00:08:51,50
So in this case, we will do just app dot use,

164
00:08:51,50 --> 00:08:57,20
and then we will do an express dot static

165
00:08:57,20 --> 00:09:04,90
and the folder dist, and then we will also make sure

166
00:09:04,90 --> 00:09:07,40
that the app, whenever it receives

167
00:09:07,40 --> 00:09:14,50
a get request on any route,

168
00:09:14,50 --> 00:09:20,20
parameters requires a result,

169
00:09:20,20 --> 00:09:22,90
and we will make sure it will send the file

170
00:09:22,90 --> 00:09:28,60
from dist which is in dex dot html.

171
00:09:28,60 --> 00:09:33,40
We'll also need to import path at the end.

172
00:09:33,40 --> 00:09:39,60
Path equal require path,

173
00:09:39,60 --> 00:09:45,20
and then in here, we can do res dot send file.

174
00:09:45,20 --> 00:09:48,00
This will make sure that we always send our file

175
00:09:48,00 --> 00:09:50,20
whenever it's being requested.

176
00:09:50,20 --> 00:09:58,50
So we'll do path dot join, and then dirname

177
00:09:58,50 --> 00:10:04,70
and we will do dist slash index dot html

178
00:10:04,70 --> 00:10:08,70
to always be served from our application,

179
00:10:08,70 --> 00:10:12,70
and then at the end, let's change this port a little bit.

180
00:10:12,70 --> 00:10:18,70
So if we set a process dot env dot port,

181
00:10:18,70 --> 00:10:21,80
so if we wanna run our application on a specific port,

182
00:10:21,80 --> 00:10:24,50
we can do that as well as with an environment variable.

183
00:10:24,50 --> 00:10:27,70
If not, we'll run it on ATAT,

184
00:10:27,70 --> 00:10:33,20
and we'll just do console listening.

185
00:10:33,20 --> 00:10:34,40
Now let's flick back to our terminal.

186
00:10:34,40 --> 00:10:39,00
By now, our Elastic Beanstalk should have been deployed.

187
00:10:39,00 --> 00:10:42,00
As you can see, everything's been deployed,

188
00:10:42,00 --> 00:10:45,80
and let's do a quick thing in here.

189
00:10:45,80 --> 00:10:49,70
Let's set the environment variable node env to production.

190
00:10:49,70 --> 00:10:55,50
So in this case, we want to do eb set env

191
00:10:55,50 --> 00:11:01,50
and then node underscore E-N-V to be equal to production.

192
00:11:01,50 --> 00:11:04,70
So this way, we'll always run the code

193
00:11:04,70 --> 00:11:10,00
with the production from the static files,

194
00:11:10,00 --> 00:11:13,50
and we will need to wait for it to deploy again,

195
00:11:13,50 --> 00:11:15,10
so let's go into our browser

196
00:11:15,10 --> 00:11:17,60
and have a look at the Elastic Beanstalk.

197
00:11:17,60 --> 00:11:20,20
So if we go into our AWS console,

198
00:11:20,20 --> 00:11:24,40
we go to Elastic Beanstalk, and then we can see the process.

199
00:11:24,40 --> 00:11:25,80
So you can see that at the moment,

200
00:11:25,80 --> 00:11:27,40
the application is not live yet.

201
00:11:27,40 --> 00:11:33,80
It is just being deployed, so we'll wait for that.

202
00:11:33,80 --> 00:11:39,70
If you just click in it, it will show you what the,

203
00:11:39,70 --> 00:11:41,40
what the status of the application is,

204
00:11:41,40 --> 00:11:44,30
so as you can see at the moment, it's just being updated.

205
00:11:44,30 --> 00:11:48,20
I will pause the video again until it gets updated.

206
00:11:48,20 --> 00:11:50,90
So now, let's, as you can see,

207
00:11:50,90 --> 00:11:53,50
the application had been successfully redeployed,

208
00:11:53,50 --> 00:11:59,30
and let's go back into our terminal and run eb open,

209
00:11:59,30 --> 00:12:01,20
and this will open your browser app

210
00:12:01,20 --> 00:12:04,80
with your application booted up.

211
00:12:04,80 --> 00:12:08,50
So that's it about Elastic Beanstalk deployments.

212
00:12:08,50 --> 00:12:11,30
Just make sure you use the CLI and go through documentation

213
00:12:11,30 --> 00:12:15,20
for more advanced functionality.

214
00:12:15,20 --> 00:12:18,50
So that's AWS Elastic Beanstalk deployments

215
00:12:18,50 --> 00:12:20,80
finished as well.

216
00:12:20,80 --> 00:12:25,20
Again, if you've made sure you never submit your AWS keys

217
00:12:25,20 --> 00:12:27,40
into Git or anything public.

218
00:12:27,40 --> 00:12:32,90
So that, with that, we finished the section

219
00:12:32,90 --> 00:12:35,20
about deployments with Webpack.

220
00:12:35,20 --> 00:12:37,30
So let's just do a quick recap

221
00:12:37,30 --> 00:12:39,00
of what we've learned in this section.

222
00:12:39,00 --> 00:12:40,00
First of all, we've looked

223
00:12:40,00 --> 00:12:41,60
at getting our app production ready

224
00:12:41,60 --> 00:12:45,40
by setting the node env to production.

225
00:12:45,40 --> 00:12:47,60
We have also made sure that we run Webpack

226
00:12:47,60 --> 00:12:52,30
with the dash b flag instead of the dash d so that this way,

227
00:12:52,30 --> 00:12:55,90
Webpack will automatically minify our javascript

228
00:12:55,90 --> 00:13:00,30
and all our assets so that the app gets served.

229
00:13:00,30 --> 00:13:03,60
The bundle generated by Webpack is lots more

230
00:13:03,60 --> 00:13:06,10
than the one we have in development.

231
00:13:06,10 --> 00:13:09,40
Then we have looked at how to deploy the build application

232
00:13:09,40 --> 00:13:14,40
to GitHub pages using the gh pages branch

233
00:13:14,40 --> 00:13:22,70
as well as to AWS S3 bucket using the S3 website's utility.

234
00:13:22,70 --> 00:13:24,40
Then we have looked at the main differences

235
00:13:24,40 --> 00:13:27,00
between deploying a static,

236
00:13:27,00 --> 00:13:32,20
front end only application and a server application.

237
00:13:32,20 --> 00:13:33,10
The server-based application

238
00:13:33,10 --> 00:13:35,90
that also contains our front end app,

239
00:13:35,90 --> 00:13:40,00
and we have created a small server with Node and Express

240
00:13:40,00 --> 00:13:42,20
and configured it to run with Webpack

241
00:13:42,20 --> 00:13:48,90
using the webpack dev middleware utility,

242
00:13:48,90 --> 00:13:53,00
and then we have deployed this more complex application

243
00:13:53,00 --> 00:13:56,50
to Elastic Beanstalk, to AWS Elastic Beanstalk,

244
00:13:56,50 --> 00:14:08,00
and got it running with the service.

245
00:14:08,00 --> 00:14:12,70
With this, we end this section and this title

246
00:14:12,70 --> 00:14:17,10
about how to boost your app productivity with Webpack.

247
00:14:17,10 --> 00:14:21,30
We've covered a lot of topics from the basic configuration

248
00:14:21,30 --> 00:14:23,90
and the basic installation of Webpack

249
00:14:23,90 --> 00:14:27,40
to configurations of the project

250
00:14:27,40 --> 00:14:32,30
to importing the real react project

251
00:14:32,30 --> 00:14:34,30
into our application,

252
00:14:34,30 --> 00:14:37,40
as well as optimizing that project

253
00:14:37,40 --> 00:14:42,10
and that code using Webpack's functionalities and utilities.

254
00:14:42,10 --> 00:14:44,90
We have also looked at how to improve performance

255
00:14:44,90 --> 00:14:47,40
for the plans by setting up code splitting,

256
00:14:47,40 --> 00:14:51,10
vendor splitting, and cash busting,

257
00:14:51,10 --> 00:14:52,60
and then we have also looked at how

258
00:14:52,60 --> 00:14:55,30
to get all this application deployed,

259
00:14:55,30 --> 00:14:57,50
both as a simple static application

260
00:14:57,50 --> 00:15:01,70
as well as a part of more complex system.

261
00:15:01,70 --> 00:15:04,90
For example, as part as a Node JS server

262
00:15:04,90 --> 00:15:09,70
and deployed to AWS Elastic Beanstalk.

263
00:15:09,70 --> 00:15:13,30
Hopefully, these lessons have been useful for you,

264
00:15:13,30 --> 00:15:15,30
and you've learned everything that you needed to know

265
00:15:15,30 --> 00:15:37,00
to get you started with Webpack in the context of react.

