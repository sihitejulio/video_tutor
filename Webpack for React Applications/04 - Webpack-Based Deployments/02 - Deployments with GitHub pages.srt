1
00:00:01,90 --> 00:00:02,80
- [Instructor] In the previous video,

2
00:00:02,80 --> 00:00:05,00
we have looked at everything, all the other configs

3
00:00:05,00 --> 00:00:07,80
that we needed to add into our web pack config file

4
00:00:07,80 --> 00:00:09,60
as well as all the scripts

5
00:00:09,60 --> 00:00:12,20
that needed updated into our package json

6
00:00:12,20 --> 00:00:14,20
to get our application production ready,

7
00:00:14,20 --> 00:00:16,90
and now, the time has come,

8
00:00:16,90 --> 00:00:19,40
start deploying this application,

9
00:00:19,40 --> 00:00:23,90
and we will begin by deploying it to GitHub pages.

10
00:00:23,90 --> 00:00:26,90
First of all, we will look at what is GitHub pages.

11
00:00:26,90 --> 00:00:29,50
Then, we will have a look at what git scripts we need

12
00:00:29,50 --> 00:00:33,30
to run in order to deploy our application,

13
00:00:33,30 --> 00:00:36,30
and then we'll look at exactly how we can upload the app

14
00:00:36,30 --> 00:00:38,90
into our GitHub account into GitHub pages

15
00:00:38,90 --> 00:00:42,80
and how we can view the app and where it is being hosted.

16
00:00:42,80 --> 00:00:44,00
First of all, keep in mind

17
00:00:44,00 --> 00:00:47,40
that you will need a GitHub account for this.

18
00:00:47,40 --> 00:00:50,00
If you do not have one, please make sure

19
00:00:50,00 --> 00:00:55,10
to go to github.com and create a new account,

20
00:00:55,10 --> 00:01:00,20
and once you create one of your new account into GitHub,

21
00:01:00,20 --> 00:01:04,10
make sure you also add an SSH key to your account

22
00:01:04,10 --> 00:01:08,40
so that you can easily push file into git from here.

23
00:01:08,40 --> 00:01:11,50
To do that, all you need to go is go into your settings

24
00:01:11,50 --> 00:01:14,80
and then go into your SSH keys

25
00:01:14,80 --> 00:01:20,10
and follow the instructions that git gives you.

26
00:01:20,10 --> 00:01:24,00
Now, let's have a look at how we can deploy this application

27
00:01:24,00 --> 00:01:26,30
and talk a bit more about GitHub pages.

28
00:01:26,30 --> 00:01:29,60
First of all, what is GitHub pages?

29
00:01:29,60 --> 00:01:34,80
Basically, it's just a service that GitHub provides

30
00:01:34,80 --> 00:01:37,80
and is designed to host your personal organization

31
00:01:37,80 --> 00:01:42,00
or project pages directly from a git repository.

32
00:01:42,00 --> 00:01:43,90
So basically, you can create a repository

33
00:01:43,90 --> 00:01:45,20
in which you can push your files,

34
00:01:45,20 --> 00:01:51,20
and then GitHub will serve those files as a static website.

35
00:01:51,20 --> 00:01:53,50
There are a few things that you need to keep in mind.

36
00:01:53,50 --> 00:01:57,30
First of all that GitHub pages, any GitHub page created

37
00:01:57,30 --> 00:02:02,00
after June 2016 will always be server over HTTPS,

38
00:02:02,00 --> 00:02:05,10
so if you need HTTPS for your application,

39
00:02:05,10 --> 00:02:08,70
you can use GitHub pages for it.

40
00:02:08,70 --> 00:02:11,90
However, you shouldn't use GitHub pages

41
00:02:11,90 --> 00:02:14,60
for storing any sensitive transactions

42
00:02:14,60 --> 00:02:17,80
like sending passwords or card numbers

43
00:02:17,80 --> 00:02:22,80
because git does not provide any security features for that.

44
00:02:22,80 --> 00:02:26,00
Now, let's look at how we can deploy

45
00:02:26,00 --> 00:02:31,70
our project to a GitHub page.

46
00:02:31,70 --> 00:02:34,40
First of all, we will need to create a new repository.

47
00:02:34,40 --> 00:02:37,20
Make sure that the repository is public.

48
00:02:37,20 --> 00:02:39,20
This is the most important thing,

49
00:02:39,20 --> 00:02:41,50
making sure that our repository is public.

50
00:02:41,50 --> 00:02:43,70
So go in GitHub, create the new repository.

51
00:02:43,70 --> 00:02:47,00
I will just call mine webpack sample deploy.

52
00:02:47,00 --> 00:02:48,30
Go create the repository.

53
00:02:48,30 --> 00:02:50,80
Now, if you've also set up your SSH key,

54
00:02:50,80 --> 00:02:55,40
then you can follow the steps on your screen on here,

55
00:02:55,40 --> 00:02:58,90
so let's add this repository to our project.

56
00:02:58,90 --> 00:03:02,30
Let's go back into the terminal.

57
00:03:02,30 --> 00:03:07,00
Let's run git init, and then

58
00:03:07,00 --> 00:03:10,50
let's just commit everything that we have.

59
00:03:10,50 --> 00:03:13,90
So before committing, we might wanna go back

60
00:03:13,90 --> 00:03:16,20
into our editor and create a git ignore file

61
00:03:16,20 --> 00:03:18,60
to make sure the node modules folder

62
00:03:18,60 --> 00:03:20,40
is not included every time.

63
00:03:20,40 --> 00:03:23,10
So let's do that as well.

64
00:03:23,10 --> 00:03:28,20
So we will create a git ignore file.

65
00:03:28,20 --> 00:03:35,20
Basically, this will be called dot git ignore

66
00:03:35,20 --> 00:03:37,60
and in here, we want to, at the moment,

67
00:03:37,60 --> 00:03:42,50
just exclude the node modules folder.

68
00:03:42,50 --> 00:03:49,10
So we will just type in node modules in here,

69
00:03:49,10 --> 00:03:51,10
and also, if you're using the different editor

70
00:03:51,10 --> 00:03:53,00
that has various config files,

71
00:03:53,00 --> 00:03:55,60
you can also add that in here.

72
00:03:55,60 --> 00:03:57,80
You can also exclude the yarn error log,

73
00:03:57,80 --> 00:04:01,60
for example, in case you have one.

74
00:04:01,60 --> 00:04:03,10
Error dot log.

75
00:04:03,10 --> 00:04:04,60
Just make sure these files do not get added

76
00:04:04,60 --> 00:04:09,30
into our git repositories and be made public.

77
00:04:09,30 --> 00:04:12,90
Now, we can add all the files that we have into git

78
00:04:12,90 --> 00:04:16,80
by doing git add, and then we can also commit this

79
00:04:16,80 --> 00:04:22,90
and let's just give it initial commit message.

80
00:04:22,90 --> 00:04:25,40
It's up to you what messages you put.

81
00:04:25,40 --> 00:04:29,20
So once we've done this, now we can also push to,

82
00:04:29,20 --> 00:04:32,00
we need to set up our master repository,

83
00:04:32,00 --> 00:04:34,30
so we go back into our GitHub,

84
00:04:34,30 --> 00:04:38,10
and then we'll add this git remote add.

85
00:04:38,10 --> 00:04:40,60
As you can see, this will be based on the name

86
00:04:40,60 --> 00:04:43,10
of the repository you've created,

87
00:04:43,10 --> 00:04:47,30
so let's paste that in here, and now,

88
00:04:47,30 --> 00:04:52,00
we can also push our code with git push u origin master.

89
00:04:52,00 --> 00:04:56,90
Once we've done that, make sure

90
00:04:56,90 --> 00:04:58,80
you have the correct access rights.

91
00:04:58,80 --> 00:05:01,50
Just let me sort this out real quick.

92
00:05:01,50 --> 00:05:06,60
So now that we've added our SSH key into our git account,

93
00:05:06,60 --> 00:05:10,70
what we can do is just do a git push to origin master,

94
00:05:10,70 --> 00:05:15,90
and our project should show up into our GitHub account,

95
00:05:15,90 --> 00:05:18,70
so if we refresh this page, we'll be able

96
00:05:18,70 --> 00:05:20,20
to have access to our folder.

97
00:05:20,20 --> 00:05:22,00
As you can see, we have our dist folder

98
00:05:22,00 --> 00:05:26,20
which contains everything, all the files

99
00:05:26,20 --> 00:05:27,90
that have been created by webpack.

100
00:05:27,90 --> 00:05:31,80
Now, let's have a look at serving those.

101
00:05:31,80 --> 00:05:34,40
So in order to serve this from GitHub pages,

102
00:05:34,40 --> 00:05:36,00
we will need to push the dist folder

103
00:05:36,00 --> 00:05:39,50
into a very specific branch called gh pages.

104
00:05:39,50 --> 00:05:42,80
So let's get that set up as well.

105
00:05:42,80 --> 00:05:45,90
So to do that, we will go back into our terminal.

106
00:05:45,90 --> 00:05:50,80
We will create the new branch using git checkout,

107
00:05:50,80 --> 00:05:52,90
and we will create a page,

108
00:05:52,90 --> 00:05:57,90
and the branch name needs to be gh dash pages.

109
00:05:57,90 --> 00:06:00,60
Once we've done that, you see we switched into this,

110
00:06:00,60 --> 00:06:02,20
and we need to do one more command

111
00:06:02,20 --> 00:06:06,40
before we're ready to deploy with GitHub pages.

112
00:06:06,40 --> 00:06:16,20
That command will be git subtree push,

113
00:06:16,20 --> 00:06:20,20
and then dash dash prefix, and we want

114
00:06:20,20 --> 00:06:30,70
to push our dist folder into origin gh pages,

115
00:06:30,70 --> 00:06:34,10
and just press enter now, and you will see,

116
00:06:34,10 --> 00:06:40,90
you will push our dist folder into a gh pages branch.

117
00:06:40,90 --> 00:06:45,20
Now if we go back into our browser into GitHub,

118
00:06:45,20 --> 00:06:49,30
we will see a new branch has been created called gh pages

119
00:06:49,30 --> 00:06:54,90
which only contains the files from our dist folder.

120
00:06:54,90 --> 00:06:56,60
So to make this process even easier,

121
00:06:56,60 --> 00:07:00,50
let's go back into our package json file

122
00:07:00,50 --> 00:07:04,30
and create a deploy script for GitHub pages,

123
00:07:04,30 --> 00:07:09,30
and we will call that deploy gh,

124
00:07:09,30 --> 00:07:14,10
and in here, we will run our build script,

125
00:07:14,10 --> 00:07:20,40
so we will yarn build which will execute this command above,

126
00:07:20,40 --> 00:07:24,50
and then we will also push into git,

127
00:07:24,50 --> 00:07:31,70
so we'll do and and, and git subtree push,

128
00:07:31,70 --> 00:07:41,30
and then dash dash prefix dist origin gh pages.

129
00:07:41,30 --> 00:07:44,10
So basically, now at this point we will be able

130
00:07:44,10 --> 00:07:49,30
to go back into our terminal and then run yarn deploy gh,

131
00:07:49,30 --> 00:07:54,70
so yarn deploy gh, and this will build our project

132
00:07:54,70 --> 00:08:01,20
and then also handle pushing everything into git,

133
00:08:01,20 --> 00:08:03,60
and that concludes our deployment to GitHub pages.

134
00:08:03,60 --> 00:08:08,30
Now if you go into the browser into your GitHub pages,

135
00:08:08,30 --> 00:08:12,30
if you type in your name of account dot GitHub dot io

136
00:08:12,30 --> 00:08:14,40
slash the name of the repo, you will be able

137
00:08:14,40 --> 00:08:17,60
to access your application in there.

138
00:08:17,60 --> 00:08:19,10
So in this video, we have looked at

139
00:08:19,10 --> 00:08:22,00
how to deploy your static application with GitHub pages.

140
00:08:22,00 --> 00:08:23,60
In the next video, we'll have a look

141
00:08:23,60 --> 00:08:26,20
at how to deploy the same application

142
00:08:26,20 --> 00:08:29,00
to AWS into an S-tree bucket.

