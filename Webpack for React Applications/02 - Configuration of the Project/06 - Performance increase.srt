1
00:00:01,50 --> 00:00:03,20
- [Instructor] In the previous set of videos,

2
00:00:03,20 --> 00:00:04,20
we have added the code

3
00:00:04,20 --> 00:00:07,80
for our MyReads React application into Webpack,

4
00:00:07,80 --> 00:00:12,90
as well as written the basic configuration for Webpack

5
00:00:12,90 --> 00:00:16,50
in order to run and bundle our app together.

6
00:00:16,50 --> 00:00:18,70
Keep in mind, the configuration files

7
00:00:18,70 --> 00:00:20,90
that we have written in the previous videos,

8
00:00:20,90 --> 00:00:23,30
you can port them around and use them across

9
00:00:23,30 --> 00:00:27,50
any React project that you might have going on.

10
00:00:27,50 --> 00:00:28,30
In this video,

11
00:00:28,30 --> 00:00:31,30
we will talk a bit about performance increases,

12
00:00:31,30 --> 00:00:34,60
and we will specifically look at

13
00:00:34,60 --> 00:00:36,80
Webpack-specific performance increases

14
00:00:36,80 --> 00:00:39,80
that we can apply to our project.

15
00:00:39,80 --> 00:00:42,10
First of all, we will have a quick introduction

16
00:00:42,10 --> 00:00:45,80
to browser caching, and have a bit more understanding

17
00:00:45,80 --> 00:00:48,50
on how browsers load files.

18
00:00:48,50 --> 00:00:52,50
Then we are going to leverage this browser caching

19
00:00:52,50 --> 00:00:54,10
with the aid of Webpack,

20
00:00:54,10 --> 00:00:57,70
by splitting our applications into our code bundle,

21
00:00:57,70 --> 00:01:00,10
and our vendor libs.

22
00:01:00,10 --> 00:01:03,30
And then we will see how we can configure,

23
00:01:03,30 --> 00:01:06,50
how we can force the browser to cache large files,

24
00:01:06,50 --> 00:01:10,70
and configure Webpack to help with decreasing load times

25
00:01:10,70 --> 00:01:13,40
for our application when it is deployed.

26
00:01:13,40 --> 00:01:14,80
So let's have a quick look

27
00:01:14,80 --> 00:01:18,10
at why we might need this type of performance increase,

28
00:01:18,10 --> 00:01:20,70
why we would need to split the vendor libraries

29
00:01:20,70 --> 00:01:22,20
from our application itself,

30
00:01:22,20 --> 00:01:25,00
and let's also have a look at how we do it.

31
00:01:25,00 --> 00:01:27,50
At the moment, we have our Webpack project,

32
00:01:27,50 --> 00:01:31,30
which has been bundled together into our bundle.js

33
00:01:31,30 --> 00:01:35,70
and index.html, and we have it running on our http-server

34
00:01:35,70 --> 00:01:38,20
on port 8080.

35
00:01:38,20 --> 00:01:39,50
If we go into browser,

36
00:01:39,50 --> 00:01:42,70
if you open the developer tools in Chrome,

37
00:01:42,70 --> 00:01:44,60
and then go under network,

38
00:01:44,60 --> 00:01:46,70
make sure you select your JS in here,

39
00:01:46,70 --> 00:01:48,00
and refresh the page.

40
00:01:48,00 --> 00:01:49,60
Once you do that,

41
00:01:49,60 --> 00:01:52,80
you'll see this big, large bundle.js file,

42
00:01:52,80 --> 00:01:56,50
which is 2.5 MB large.

43
00:01:56,50 --> 00:01:58,80
Now, while this is acceptable in development,

44
00:01:58,80 --> 00:02:01,80
I usually do not recommend having your users

45
00:02:01,80 --> 00:02:05,60
download a 2.5 MB file every time they load your page.

46
00:02:05,60 --> 00:02:09,30
So let's see what we can do about this.

47
00:02:09,30 --> 00:02:11,80
The best way of dealing with these large files

48
00:02:11,80 --> 00:02:14,70
is leveraging the caching systems

49
00:02:14,70 --> 00:02:16,90
that exist in all modern browsers.

50
00:02:16,90 --> 00:02:19,40
Every browser, from Internet Explorer 8,

51
00:02:19,40 --> 00:02:21,60
if I remember correctly, has implemented the system

52
00:02:21,60 --> 00:02:22,40
of caching files to the local disc,

53
00:02:22,40 --> 00:02:27,20
so that, if they do not need to be reloaded,

54
00:02:27,20 --> 00:02:28,90
the browser just stores them on the disc,

55
00:02:28,90 --> 00:02:31,70
loads them from disc, reducing the network traffic,

56
00:02:31,70 --> 00:02:33,10
and obviously,

57
00:02:33,10 --> 00:02:37,60
increasing the speed with which the application loads.

58
00:02:37,60 --> 00:02:38,90
So let's have a quick look

59
00:02:38,90 --> 00:02:41,20
at a diagram explaining browser caching.

60
00:02:41,20 --> 00:02:42,70
And let's see what we can do

61
00:02:42,70 --> 00:02:46,20
in order to improve our application's performance.

62
00:02:46,20 --> 00:02:49,60
So as I mentioned in the previous videos,

63
00:02:49,60 --> 00:02:52,80
this is the basic Webpack bundling process.

64
00:02:52,80 --> 00:02:55,90
So Webpack will take all your existing libraries,

65
00:02:55,90 --> 00:03:00,10
such as React Router, React.js, Redux, TypeScript,

66
00:03:00,10 --> 00:03:02,10
anything else that you use,

67
00:03:02,10 --> 00:03:06,00
together with your own React components, and your app.js,

68
00:03:06,00 --> 00:03:10,20
and merge them all together into one large bundle.js file.

69
00:03:10,20 --> 00:03:13,30
In our case, this file is 2.5 MB large.

70
00:03:13,30 --> 00:03:17,10
And then, this bundle.js gets served to the user browsers

71
00:03:17,10 --> 00:03:19,50
in one big chunk.

72
00:03:19,50 --> 00:03:21,30
Ideally, what we would want to do

73
00:03:21,30 --> 00:03:25,40
is apply a technique called vendor code splitting.

74
00:03:25,40 --> 00:03:26,70
What this allows us to do,

75
00:03:26,70 --> 00:03:29,70
it allows us to isolate all the vendor libraries,

76
00:03:29,70 --> 00:03:32,80
also known as all the external libraries to our project,

77
00:03:32,80 --> 00:03:35,60
for example, React Router, React.js, and so on,

78
00:03:35,60 --> 00:03:39,10
and bundle them separately from our own components

79
00:03:39,10 --> 00:03:40,90
and our own app.js.

80
00:03:40,90 --> 00:03:43,80
Chances are that your components and app.js

81
00:03:43,80 --> 00:03:47,30
only account for 5 to 10%

82
00:03:47,30 --> 00:03:49,90
of the total size of your application.

83
00:03:49,90 --> 00:03:51,60
While React Router, React,

84
00:03:51,60 --> 00:03:53,20
and all the other vendor libraries,

85
00:03:53,20 --> 00:03:54,70
they usually stay the same.

86
00:03:54,70 --> 00:03:58,60
It's really rare that you update some, the vendor libs,

87
00:03:58,60 --> 00:03:59,60
on a production app.

88
00:03:59,60 --> 00:04:04,40
So ideally, we'd want those to be cached on the user's disc,

89
00:04:04,40 --> 00:04:06,40
and the user will never have to load them

90
00:04:06,40 --> 00:04:10,50
from your server again, as long as they have them cached.

91
00:04:10,50 --> 00:04:11,40
In order to do so,

92
00:04:11,40 --> 00:04:14,20
we will need to bundle all the vendor libs together

93
00:04:14,20 --> 00:04:18,80
into a file that is usually called vendor.js,

94
00:04:18,80 --> 00:04:22,50
and bundle all your components and your app

95
00:04:22,50 --> 00:04:26,40
into a much smaller size bundle.js file,

96
00:04:26,40 --> 00:04:29,20
and then serve them to the user browser.

97
00:04:29,20 --> 00:04:30,20
This way we ensure

98
00:04:30,20 --> 00:04:34,50
that the user has their vendors.js files cached,

99
00:04:34,50 --> 00:04:37,90
while bundle.js, which is the code that you actually update

100
00:04:37,90 --> 00:04:41,10
and maintain regularly, is a much smaller size,

101
00:04:41,10 --> 00:04:42,50
because it doesn't need to include

102
00:04:42,50 --> 00:04:44,60
all the other vendor libraries.

103
00:04:44,60 --> 00:04:49,20
So let's have a look at how we can do that using Webpack.

104
00:04:49,20 --> 00:04:50,00
As you can imagine,

105
00:04:50,00 --> 00:04:52,90
everything is done in the same webpackconfig.js file

106
00:04:52,90 --> 00:04:57,30
that we've been working with so far, so let's go there.

107
00:04:57,30 --> 00:04:58,90
Let's open our project.

108
00:04:58,90 --> 00:05:01,70
Let's open our config.js file,

109
00:05:01,70 --> 00:05:05,40
and let us write a few lines of code,

110
00:05:05,40 --> 00:05:09,30
and do not worry, I will explain what those lines do,

111
00:05:09,30 --> 00:05:10,80
just after I write them.

112
00:05:10,80 --> 00:05:14,40
First of all, we will create a new constant

113
00:05:14,40 --> 00:05:18,30
called VENDOR_LIBS.

114
00:05:18,30 --> 00:05:20,50
And this constant will be an array

115
00:05:20,50 --> 00:05:25,10
in which we will include all the vendor libraries

116
00:05:25,10 --> 00:05:27,80
that we want to put in a separate bundle.

117
00:05:27,80 --> 00:05:31,10
So we will start with react.

118
00:05:31,10 --> 00:05:36,40
We'll also do react-dom, which is also quite large.

119
00:05:36,40 --> 00:05:38,00
Then we'll also include,

120
00:05:38,00 --> 00:05:40,10
for the purpose of this demonstration,

121
00:05:40,10 --> 00:05:43,10
the router that we are using for this project,

122
00:05:43,10 --> 00:05:45,10
which is react-router-dom,

123
00:05:45,10 --> 00:05:49,00
react-router-dom.

124
00:05:49,00 --> 00:05:54,40
And now we will need to use this vendor libs,

125
00:05:54,40 --> 00:05:57,40
this vendor libs array, and tell Webpack,

126
00:05:57,40 --> 00:05:59,60
Webpack, you need to bundle these separately

127
00:05:59,60 --> 00:06:01,70
into a vendor.js bundle,

128
00:06:01,70 --> 00:06:04,30
and then serve it separately to the user.

129
00:06:04,30 --> 00:06:07,20
So let's quickly comment our entry,

130
00:06:07,20 --> 00:06:09,10
because now we have multiple entries

131
00:06:09,10 --> 00:06:10,90
that we wanna take count for.

132
00:06:10,90 --> 00:06:14,50
So let's take this line out, comment it out.

133
00:06:14,50 --> 00:06:18,90
And then, we will create an entry object

134
00:06:18,90 --> 00:06:24,20
that will contain our bundle and our vendor libs.

135
00:06:24,20 --> 00:06:27,40
So again, entry.

136
00:06:27,40 --> 00:06:32,20
We will have the same path we had before

137
00:06:32,20 --> 00:06:36,00
for our index.js.

138
00:06:36,00 --> 00:06:39,00
So we will just say, path, index.js.

139
00:06:39,00 --> 00:06:41,40
Don't forget to put the comma here at the end.

140
00:06:41,40 --> 00:06:44,40
And the new thing we are going to include in here

141
00:06:44,40 --> 00:06:47,70
is going to be our vendor libs.

142
00:06:47,70 --> 00:06:50,20
And for this, we have conveniently defined

143
00:06:50,20 --> 00:06:54,70
our vendor libs constant, just above.

144
00:06:54,70 --> 00:06:58,40
And once we do this, we will also need to tell Webpack

145
00:06:58,40 --> 00:07:01,40
that it needs to do some changes to the output files.

146
00:07:01,40 --> 00:07:04,10
Remember, we're trying to export the vendor.js

147
00:07:04,10 --> 00:07:06,90
and the bundle.js, not just the bundle.js,

148
00:07:06,90 --> 00:07:08,90
and the change for that is really simple,

149
00:07:08,90 --> 00:07:12,90
we will tell Webpack that the output should be [name].js.

150
00:07:12,90 --> 00:07:15,70
This way, we tell Webpack it is looking for the path,

151
00:07:15,70 --> 00:07:18,30
which it will transform into bundle.js

152
00:07:18,30 --> 00:07:21,00
and then the vendor libs,

153
00:07:21,00 --> 00:07:25,20
which it will transform automatically into vendor.js.

154
00:07:25,20 --> 00:07:27,90
So let's save our config file, like this.

155
00:07:27,90 --> 00:07:29,50
Hopefully there are no errors.

156
00:07:29,50 --> 00:07:34,40
Go back to our terminal, close the development server,

157
00:07:34,40 --> 00:07:36,20
and run our dev script.

158
00:07:36,20 --> 00:07:38,00
Now this might take a little bit,

159
00:07:38,00 --> 00:07:40,70
because when introducing code splitting,

160
00:07:40,70 --> 00:07:42,30
Webpack will take a bit longer.

161
00:07:42,30 --> 00:07:44,20
So if we look at the output now,

162
00:07:44,20 --> 00:07:49,90
we will see we have a path.js

163
00:07:49,90 --> 00:07:52,50
and a vendor.js file.

164
00:07:52,50 --> 00:07:54,70
That is mainly because I made a mistake

165
00:07:54,70 --> 00:08:00,20
and this is not path, this is supposed to be bundle in here,

166
00:08:00,20 --> 00:08:02,90
because we want the bundle.js, obviously.

167
00:08:02,90 --> 00:08:05,00
Let's run Webpack again.

168
00:08:05,00 --> 00:08:06,10
And once we run it,

169
00:08:06,10 --> 00:08:08,40
you'll see pretty much the same result,

170
00:08:08,40 --> 00:08:09,90
but with the correct file names.

171
00:08:09,90 --> 00:08:13,40
So you'll see a bundle.js and a vendor.js.

172
00:08:13,40 --> 00:08:15,10
Now for those of you who have already

173
00:08:15,10 --> 00:08:16,80
looked at the file sizes, you'll be,

174
00:08:16,80 --> 00:08:18,20
hold on, wait a second.

175
00:08:18,20 --> 00:08:22,10
Now we have two files that are 2.5 MB large.

176
00:08:22,10 --> 00:08:23,00
And that is correct.

177
00:08:23,00 --> 00:08:26,10
In order to finalize our vendor splitting process,

178
00:08:26,10 --> 00:08:28,40
we need to add one more line of configuration

179
00:08:28,40 --> 00:08:30,40
to our Webpack config file.

180
00:08:30,40 --> 00:08:32,40
So let's go back in there and do that.

181
00:08:32,40 --> 00:08:36,50
For this, we'll need to use one of Webpack's plugins.

182
00:08:36,50 --> 00:08:38,50
So if you remember from the previous section,

183
00:08:38,50 --> 00:08:41,10
we will need to go all the way to the bottom,

184
00:08:41,10 --> 00:08:43,50
where we have declared our plugins array,

185
00:08:43,50 --> 00:08:46,40
add a comma after, and then we will add

186
00:08:46,40 --> 00:08:55,30
a new webpack.optimize.CommonsChunkPlugin.

187
00:08:55,30 --> 00:08:57,80
Keep in mind it's called CommonsChunkPlugin,

188
00:08:57,80 --> 00:08:59,30
not CommonChunkPlugins.

189
00:08:59,30 --> 00:09:01,20
I don't know why they went with this name,

190
00:09:01,20 --> 00:09:02,40
but this is how we'll call.

191
00:09:02,40 --> 00:09:05,90
And we will pass a configuration object into this,

192
00:09:05,90 --> 00:09:08,70
which will include the names of the files,

193
00:09:08,70 --> 00:09:14,30
and this will be vendor, and manifest.

194
00:09:14,30 --> 00:09:16,70
The manifest is not strictly necessary at this point.

195
00:09:16,70 --> 00:09:19,70
However, I would always recommend including it in here.

196
00:09:19,70 --> 00:09:21,30
We will see more about that

197
00:09:21,30 --> 00:09:23,90
when we do cache busting in the next video.

198
00:09:23,90 --> 00:09:26,60
So we will save our webpackconfig.js,

199
00:09:26,60 --> 00:09:31,90
go back into our terminal, run the Webpack, build again,

200
00:09:31,90 --> 00:09:34,70
and what this CommonsChunkPlugin will do,

201
00:09:34,70 --> 00:09:36,60
it will remove all the duplicate code

202
00:09:36,60 --> 00:09:38,70
between the two bundles.

203
00:09:38,70 --> 00:09:39,90
So now, as you can see,

204
00:09:39,90 --> 00:09:42,80
we have our vendor.js and our bundle.js,

205
00:09:42,80 --> 00:09:45,00
which contains all the vendor libraries,

206
00:09:45,00 --> 00:09:50,50
such as React, React dom, and whatnot, is 2.46 MB,

207
00:09:50,50 --> 00:09:53,50
while our bundle.js, which is just our code,

208
00:09:53,50 --> 00:09:56,50
is a much smaller file, which is 114 KB.

209
00:09:56,50 --> 00:09:57,50
So this is basically

210
00:09:57,50 --> 00:09:59,40
what you want to keep sending your user,

211
00:09:59,40 --> 00:10:02,90
not the big vendor.js file, every time.

212
00:10:02,90 --> 00:10:06,20
Now let's run our http-server,

213
00:10:06,20 --> 00:10:07,80
make sure everything is working.

214
00:10:07,80 --> 00:10:11,80
And if we go back into our browser, refresh our page,

215
00:10:11,80 --> 00:10:15,70
as we can see, everything has worked correctly,

216
00:10:15,70 --> 00:10:18,20
and we have our manifest.js file,

217
00:10:18,20 --> 00:10:19,70
we have our vendor.js file,

218
00:10:19,70 --> 00:10:21,90
which hopefully will get cached by the browser,

219
00:10:21,90 --> 00:10:24,90
and we will have our small bundle.js file,

220
00:10:24,90 --> 00:10:29,80
which is just 112 KB large at this point.

221
00:10:29,80 --> 00:10:31,50
So hopefully everything is clear

222
00:10:31,50 --> 00:10:33,30
on how browser caching works,

223
00:10:33,30 --> 00:10:35,80
and how you can split your vendor libs

224
00:10:35,80 --> 00:10:39,50
from your main code, from your project's code.

225
00:10:39,50 --> 00:10:42,10
Next, we will have a look at cache busting,

226
00:10:42,10 --> 00:10:43,70
which is, again, very important,

227
00:10:43,70 --> 00:10:46,40
because at the moment, the way we left it is,

228
00:10:46,40 --> 00:10:47,70
the browser does not know

229
00:10:47,70 --> 00:10:50,40
if it needs to refresh our bundle.js,

230
00:10:50,40 --> 00:10:54,80
so it will keep caching bundle.js together with vendor.js

231
00:10:54,80 --> 00:10:57,80
without knowing when the browser needs to reload it.

232
00:10:57,80 --> 00:11:01,90
We will cover all this in the cache busting video,

233
00:11:01,90 --> 00:11:05,00
which is the next video for this section.

