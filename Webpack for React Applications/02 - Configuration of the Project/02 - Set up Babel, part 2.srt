1
00:00:01,50 --> 00:00:03,50
- [Instructor] Into our terminal, now that we've created

2
00:00:03,50 --> 00:00:05,30
this script into our package with json,

3
00:00:05,30 --> 00:00:08,40
what we need to do is just type in yarn, run,

4
00:00:08,40 --> 00:00:09,80
and then the name of the script.

5
00:00:09,80 --> 00:00:11,70
We will be creating a lot of scripts down the line,

6
00:00:11,70 --> 00:00:14,20
so that's how you call them with yarn.

7
00:00:14,20 --> 00:00:16,60
We've called our script dev.

8
00:00:16,60 --> 00:00:18,70
Press enter, as you see this is the same thing

9
00:00:18,70 --> 00:00:21,70
as running webpack-d in the terminal.

10
00:00:21,70 --> 00:00:25,10
It's built the bundle that we have it's created it

11
00:00:25,10 --> 00:00:31,50
and it's added it into -d, this directory of our project.

12
00:00:31,50 --> 00:00:37,00
Let's go on and install Babel and React

13
00:00:37,00 --> 00:00:40,30
into our application.

14
00:00:40,30 --> 00:00:42,50
We will need to install a few packages.

15
00:00:42,50 --> 00:00:45,00
Just feel free to pause the video

16
00:00:45,00 --> 00:00:48,00
and check the name of the packages we need to install.

17
00:00:48,00 --> 00:00:50,40
To install them, we will do yarn, add,

18
00:00:50,40 --> 00:00:55,10
and then we will need babel-loader.

19
00:00:55,10 --> 00:01:00,70
We'll also need babel-preset-es2015,

20
00:01:00,70 --> 00:01:03,90
which is the syntax that we're going to be using

21
00:01:03,90 --> 00:01:08,40
for React and then we'll also do babel-preset,

22
00:01:08,40 --> 00:01:12,30
and then -react and we will install all of those

23
00:01:12,30 --> 00:01:16,00
as development dependencies.

24
00:01:16,00 --> 00:01:17,80
Yarn will take care of all of the installation.

25
00:01:17,80 --> 00:01:21,60
It will also install any additional dependencies

26
00:01:21,60 --> 00:01:22,70
that are in here.

27
00:01:22,70 --> 00:01:24,70
If we look at the top, we will see that

28
00:01:24,70 --> 00:01:27,30
we also need to install babel-core.

29
00:01:27,30 --> 00:01:29,10
Let's do that as well.

30
00:01:29,10 --> 00:01:33,50
Yarn, add, and then we'll do babel-core,

31
00:01:33,50 --> 00:01:34,60
and we will install this

32
00:01:34,60 --> 00:01:37,90
as a development dependency as well.

33
00:01:37,90 --> 00:01:41,00
Hopefully once this gets done, everything is installed,

34
00:01:41,00 --> 00:01:45,80
and everything is also showing up into our package.json.

35
00:01:45,80 --> 00:01:49,40
Now let's look at configuring Babel to work with React.

36
00:01:49,40 --> 00:01:52,50
To do that, we will need to create a new file

37
00:01:52,50 --> 00:01:56,30
called .babelrc, again this file is a standard file.

38
00:01:56,30 --> 00:01:58,10
Babel will always look for it.

39
00:01:58,10 --> 00:02:02,10
Let's do touch .babelrc, and now that its been created,

40
00:02:02,10 --> 00:02:05,30
we will have it into our code directory.

41
00:02:05,30 --> 00:02:08,80
In here we will need to create the configuration.

42
00:02:08,80 --> 00:02:12,00
We will just create an object in which we will tell Babel

43
00:02:12,00 --> 00:02:13,90
to use the following presets.

44
00:02:13,90 --> 00:02:15,00
Presets.

45
00:02:15,00 --> 00:02:19,00
The presets will be the es2015

46
00:02:19,00 --> 00:02:22,00
which is the JSX syntax,

47
00:02:22,00 --> 00:02:26,70
and react, which allows Babel to use React,

48
00:02:26,70 --> 00:02:29,20
and to understand React and compile it.

49
00:02:29,20 --> 00:02:31,00
Now we can save this and we can close this file.

50
00:02:31,00 --> 00:02:32,80
We will not be touching this file again.

51
00:02:32,80 --> 00:02:37,20
Let's go back into our webpack.config.js

52
00:02:37,20 --> 00:02:40,10
and talk a bit about loaders.

53
00:02:40,10 --> 00:02:42,40
What loaders are, one functionality of Webpack

54
00:02:42,40 --> 00:02:44,70
which makes sure that Webpack knows

55
00:02:44,70 --> 00:02:47,30
how to process specific type of files.

56
00:02:47,30 --> 00:02:51,00
If at this moment we try to include or require

57
00:02:51,00 --> 00:02:53,20
any type of js file,

58
00:02:53,20 --> 00:02:55,10
Webpack will not know what to do with it.

59
00:02:55,10 --> 00:02:57,20
When it's a simple file and it's run from the terminal

60
00:02:57,20 --> 00:03:00,40
and it's just one file, it will know how to handle it.

61
00:03:00,40 --> 00:03:03,60
However, if we go to our app.js for example,

62
00:03:03,60 --> 00:03:05,60
and if we try to include something in here,

63
00:03:05,60 --> 00:03:07,50
the same way you'd include it in React,

64
00:03:07,50 --> 00:03:09,40
Webpack will just throw an error saying

65
00:03:09,40 --> 00:03:11,40
"I don't know what to do with this file."

66
00:03:11,40 --> 00:03:15,00
Let's go back into our webpack.config

67
00:03:15,00 --> 00:03:17,90
and let's set what we need to begin working

68
00:03:17,90 --> 00:03:20,10
with loaders in Webpack.

69
00:03:20,10 --> 00:03:22,80
In our configuration file, let's just add a trailing comma

70
00:03:22,80 --> 00:03:25,90
and add a new object called module.

71
00:03:25,90 --> 00:03:28,70
In this module, we will make sure we include

72
00:03:28,70 --> 00:03:31,80
all the loaders that we need for our project.

73
00:03:31,80 --> 00:03:33,70
We'll create this object called module.

74
00:03:33,70 --> 00:03:36,80
In it, we will create an array called loaders,

75
00:03:36,80 --> 00:03:38,90
a property called loaders,

76
00:03:38,90 --> 00:03:42,30
which is an array of loaders that we will be using.

77
00:03:42,30 --> 00:03:46,10
The first loader that we will use, we will test.

78
00:03:46,10 --> 00:03:51,30
We will try and load javascript files and process them

79
00:03:51,30 --> 00:03:54,90
with Babel, which was just installed.

80
00:03:54,90 --> 00:04:00,30
In the loaders, we will test for jsx files,

81
00:04:00,30 --> 00:04:06,40
and we'll make sure to only include our app directory.

82
00:04:06,40 --> 00:04:08,90
We do not want Webpack to compile everything

83
00:04:08,90 --> 00:04:10,40
that's already in those modules

84
00:04:10,40 --> 00:04:11,80
because those are already precompiled

85
00:04:11,80 --> 00:04:13,10
and we do not need to worry about it,

86
00:04:13,10 --> 00:04:16,30
and since javascript dependencies, javascript files,

87
00:04:16,30 --> 00:04:18,60
and projects depend on a lot of files,

88
00:04:18,60 --> 00:04:20,10
you don't want to compile those every time

89
00:04:20,10 --> 00:04:21,40
if they're already compiled,

90
00:04:21,40 --> 00:04:23,60
because that will just add a lot of time

91
00:04:23,60 --> 00:04:25,70
to your build process and development process

92
00:04:25,70 --> 00:04:29,40
and it will be wasting your time.

93
00:04:29,40 --> 00:04:35,00
For the loader, we will be using babel-loader.

94
00:04:35,00 --> 00:04:42,10
We will do, use and we will use babel-loader,

95
00:04:42,10 --> 00:04:44,60
which we have previously installed into our project

96
00:04:44,60 --> 00:04:46,60
as a development dependency.

97
00:04:46,60 --> 00:04:49,60
So as you can see we've added it in here.

98
00:04:49,60 --> 00:04:52,70
One thing that I will also do is, apart from this,

99
00:04:52,70 --> 00:04:55,00
include another thing that you can do is basically

100
00:04:55,00 --> 00:04:57,60
just exclude the node modules.

101
00:04:57,60 --> 00:05:00,00
It is up to you which way you want to use it.

102
00:05:00,00 --> 00:05:01,50
I prefer using exclude just because

103
00:05:01,50 --> 00:05:05,70
I want everything else to be bundled in.

104
00:05:05,70 --> 00:05:09,80
So let's exclude node modules, the node_modules folder,

105
00:05:09,80 --> 00:05:12,20
and let's comment the include: APP_DIR.

106
00:05:12,20 --> 00:05:15,50
It's up to you, whichever way you want to do it.

107
00:05:15,50 --> 00:05:17,30
One thing we will also have into our project

108
00:05:17,30 --> 00:05:20,80
we'll be using both js and jsx files.

109
00:05:20,80 --> 00:05:25,60
Let's make sure we do that for both of them.

110
00:05:25,60 --> 00:05:31,60
We will do js files or jsx files.

111
00:05:31,60 --> 00:05:33,30
I think I have a typo here.

112
00:05:33,30 --> 00:05:35,40
There we go.

113
00:05:35,40 --> 00:05:39,10
We will be processing all js and jsx files

114
00:05:39,10 --> 00:05:41,80
excluding the node modules,

115
00:05:41,80 --> 00:05:49,20
and we will be processing that with our babel-loader.

116
00:05:49,20 --> 00:05:51,60
Let's move on to the next step

117
00:05:51,60 --> 00:05:55,60
to actually installing React into our application.

118
00:05:55,60 --> 00:05:58,00
We will save our webpack.config.js.

119
00:05:58,00 --> 00:06:01,10
We will just quickly run yarn, run dev,

120
00:06:01,10 --> 00:06:04,00
just to make sure everything still works.

121
00:06:04,00 --> 00:06:07,00
As you can see everything still works.

122
00:06:07,00 --> 00:06:09,00
Let's install React into our project.

123
00:06:09,00 --> 00:06:13,50
At this point we will do yarn, add react,

124
00:06:13,50 --> 00:06:17,20
and we will also add in react-dom.

125
00:06:17,20 --> 00:06:20,90
Once that installs, we can go back into our project

126
00:06:20,90 --> 00:06:26,20
and actually import React in our application.

127
00:06:26,20 --> 00:06:29,70
We will be replacing our poor console.log

128
00:06:29,70 --> 00:06:31,60
with some actual React code.

129
00:06:31,60 --> 00:06:33,80
Just feel free to delete this.

130
00:06:33,80 --> 00:06:37,50
Then we will go import React from react,

131
00:06:37,50 --> 00:06:43,40
and then I will just paste in some boilerplate

132
00:06:43,40 --> 00:06:45,60
React applications just to get us started quicker.

133
00:06:45,60 --> 00:06:47,20
Feel free to pause the video.

134
00:06:47,20 --> 00:06:49,40
As you can see we've added some code.

135
00:06:49,40 --> 00:06:51,60
We've added a render method and everything else.

136
00:06:51,60 --> 00:06:54,90
Now if we go into our terminal and build the app again,

137
00:06:54,90 --> 00:06:56,10
when we are building it,

138
00:06:56,10 --> 00:06:58,60
we should see this Hello React.

139
00:06:58,60 --> 00:07:03,00
Let's go in here, let's run yarn, run dev again.

140
00:07:03,00 --> 00:07:07,10
This will build the app.bundle.js as you can see now,

141
00:07:07,10 --> 00:07:09,80
the size of the bundle has increased a lot

142
00:07:09,80 --> 00:07:11,70
because now we also include React.

143
00:07:11,70 --> 00:07:13,90
Let's go into our browser.

144
00:07:13,90 --> 00:07:16,60
Oh, seems to be a problem.

145
00:07:16,60 --> 00:07:18,80
Target container is not a DOM element.

146
00:07:18,80 --> 00:07:22,50
This means that we also need to update our template file

147
00:07:22,50 --> 00:07:26,60
and make sure we have this app div in it.

148
00:07:26,60 --> 00:07:29,40
We'll go back to index.html

149
00:07:29,40 --> 00:07:32,40
and we will create a div

150
00:07:32,40 --> 00:07:35,20
and we will set the id for this div.

151
00:07:35,20 --> 00:07:37,80
I usually do roots, so we'll do root instead of app.

152
00:07:37,80 --> 00:07:42,10
Let's go into app.js as well and then build this root

153
00:07:42,10 --> 00:07:45,30
and then if we build our project again,

154
00:07:45,30 --> 00:07:46,90
we should be able to go into the browser

155
00:07:46,90 --> 00:07:50,00
and see the React application working.

156
00:07:50,00 --> 00:07:52,10
There we go, we have Hello from React.

157
00:07:52,10 --> 00:07:54,00
Our app has compiled successfully

158
00:07:54,00 --> 00:07:58,70
and now we have access to React into our project.

159
00:07:58,70 --> 00:08:01,40
Now that we have access to both Babel and React,

160
00:08:01,40 --> 00:08:03,30
we can move on with the project and we can look

161
00:08:03,30 --> 00:08:06,80
at the rest of the Webpack configuration,

162
00:08:06,80 --> 00:08:10,50
as well as at the my roots application inside our project

163
00:08:10,50 --> 00:08:14,00
and see if there are any issues and how to solve them.

