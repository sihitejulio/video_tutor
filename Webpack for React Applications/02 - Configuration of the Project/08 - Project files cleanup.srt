1
00:00:01,40 --> 00:00:02,70
- [Instructor] In our previous video,

2
00:00:02,70 --> 00:00:05,30
we have had to look at how to deal with cache busting

3
00:00:05,30 --> 00:00:08,90
and how to make sure that whenever a user revisits our site,

4
00:00:08,90 --> 00:00:11,80
it only loads the code that has been changed

5
00:00:11,80 --> 00:00:14,30
in the meantime since his last visit.

6
00:00:14,30 --> 00:00:15,80
In the last video of this section,

7
00:00:15,80 --> 00:00:20,60
we will have a quick view at how to clean our project files.

8
00:00:20,60 --> 00:00:22,00
If you remember in the last video

9
00:00:22,00 --> 00:00:24,10
when doing the cache busting,

10
00:00:24,10 --> 00:00:27,10
and making sure that we generate the new file

11
00:00:27,10 --> 00:00:28,90
every time that something is changed,

12
00:00:28,90 --> 00:00:32,10
we have ended up with a really bloated disk directory,

13
00:00:32,10 --> 00:00:34,80
which is not a problem when your project is really small,

14
00:00:34,80 --> 00:00:36,70
but the more time you save your project,

15
00:00:36,70 --> 00:00:38,80
and update your project, that file,

16
00:00:38,80 --> 00:00:41,10
that distribution directory will get bigger and bigger

17
00:00:41,10 --> 00:00:44,10
and then you might not be able to handle deployments

18
00:00:44,10 --> 00:00:47,40
without cleaning it every time you build your project.

19
00:00:47,40 --> 00:00:50,20
In order to do that, we will see exactly how we're dealing

20
00:00:50,20 --> 00:00:54,10
with all those duplicated files, or those bundled .js.

21
00:00:54,10 --> 00:00:58,30
To do so we will have a look at a node package called rimraf

22
00:00:58,30 --> 00:01:01,70
and we'll also create a script that will clean our directory

23
00:01:01,70 --> 00:01:04,20
and we will add it to our application.

24
00:01:04,20 --> 00:01:08,00
In order to do that we will need to install this package,

25
00:01:08,00 --> 00:01:10,30
and I will explain what this package does as well.

26
00:01:10,30 --> 00:01:14,20
So we will install yarn add rimraf,

27
00:01:14,20 --> 00:01:15,60
this is how it is called.

28
00:01:15,60 --> 00:01:17,40
Once it has installed, as you can see,

29
00:01:17,40 --> 00:01:19,60
I already have it installed so it's going to skip it,

30
00:01:19,60 --> 00:01:21,60
but if you've never used rimraf before,

31
00:01:21,60 --> 00:01:24,00
it will just install it into your project.

32
00:01:24,00 --> 00:01:26,90
What rimraf does is a small node package

33
00:01:26,90 --> 00:01:30,30
that gives you access to a script

34
00:01:30,30 --> 00:01:32,80
that removes a specific directory

35
00:01:32,80 --> 00:01:35,20
or specific file from a particular directory.

36
00:01:35,20 --> 00:01:37,50
If you're using Linux or macOS,

37
00:01:37,50 --> 00:01:40,30
you're probably familiar with the rm command.

38
00:01:40,30 --> 00:01:43,60
However, this command is not available for Windows users

39
00:01:43,60 --> 00:01:47,40
and that is why rimraf basically just helps us add

40
00:01:47,40 --> 00:01:51,90
this command for Windows users in case you're working,

41
00:01:51,90 --> 00:01:55,30
you're studying this project on a Windows machine,

42
00:01:55,30 --> 00:01:58,20
you will be able to clean your project files as well.

43
00:01:58,20 --> 00:02:01,60
Now, in order to take advantage of rimraf,

44
00:02:01,60 --> 00:02:05,00
we will go back into our package.json file

45
00:02:05,00 --> 00:02:08,10
and in here we will create a new script,

46
00:02:08,10 --> 00:02:10,40
which will be called clean.

47
00:02:10,40 --> 00:02:15,50
So let's do a clean script, and this script will basically

48
00:02:15,50 --> 00:02:18,90
just run rimraf on this directory.

49
00:02:18,90 --> 00:02:20,40
Make sure you add the comma at the end.

50
00:02:20,40 --> 00:02:24,70
Now, whenever we run yarn run clean,

51
00:02:24,70 --> 00:02:28,60
what it will do, it will delete this directory.

52
00:02:28,60 --> 00:02:33,10
Let's quickly test that in here, so as you can see now,

53
00:02:33,10 --> 00:02:36,30
we have our disk directory in the project.

54
00:02:36,30 --> 00:02:42,00
If we run yarn run clean, it will remove the disk directory,

55
00:02:42,00 --> 00:02:44,40
so now we do not have it anymore.

56
00:02:44,40 --> 00:02:46,40
What we want to do is we need to make sure

57
00:02:46,40 --> 00:02:48,30
that this script runs every time

58
00:02:48,30 --> 00:02:51,40
Webpack rebuilds our bundles, so to do that,

59
00:02:51,40 --> 00:02:54,80
I will do a quick update to our dev script.

60
00:02:54,80 --> 00:02:56,30
I will remove the watch,

61
00:02:56,30 --> 00:03:01,60
and then I will add the clean script before that.

62
00:03:01,60 --> 00:03:03,90
What we want to do is, for the dev command,

63
00:03:03,90 --> 00:03:08,40
we want to draw an npm run clean.

64
00:03:08,40 --> 00:03:14,10
And we also want to run our Webpack by doing && webpack -d.

65
00:03:14,10 --> 00:03:15,80
So this time, every time we want

66
00:03:15,80 --> 00:03:19,20
to build our project, it will clean the disk folder

67
00:03:19,20 --> 00:03:23,10
and then run the build script from Webpack.

68
00:03:23,10 --> 00:03:27,80
Let's test that again, let's just build the project first.

69
00:03:27,80 --> 00:03:30,80
And as you can see, it's built the project.

70
00:03:30,80 --> 00:03:33,60
Now, if we look at our files, we have the disk folder.

71
00:03:33,60 --> 00:03:40,00
So if we do yarn run dev now, it will initially clean.

72
00:03:40,00 --> 00:03:43,00
As you can see, it's running rimraf on the disk folder

73
00:03:43,00 --> 00:03:46,20
and then building a new disk folder

74
00:03:46,20 --> 00:03:47,40
with the new bundles in it.

75
00:03:47,40 --> 00:03:51,70
So now, if we change something into our React application,

76
00:03:51,70 --> 00:03:53,80
so let's just go into the home screen

77
00:03:53,80 --> 00:03:58,00
and change the title again, let's go MyReads.

78
00:03:58,00 --> 00:04:03,30
If you have a look at this bundle .js, it's d1f77.

79
00:04:03,30 --> 00:04:06,80
If we run our dev command again, our build command again,

80
00:04:06,80 --> 00:04:08,50
what it will do, it will remove it,

81
00:04:08,50 --> 00:04:10,50
it will create a new bundle .js.

82
00:04:10,50 --> 00:04:13,70
And if we look into our code editor in dist,

83
00:04:13,70 --> 00:04:18,10
you will see the old bundle has been removed using rimraf.

84
00:04:18,10 --> 00:04:21,10
Again, this is not necessarily a Webpack-specific topic.

85
00:04:21,10 --> 00:04:23,10
However, it's a really good quality of life

86
00:04:23,10 --> 00:04:26,00
and Webpack benefits a lot from running this script.

87
00:04:26,00 --> 00:04:28,30
So make sure you add this into your projects

88
00:04:28,30 --> 00:04:30,80
whenever you decide to build your project with Webpack

89
00:04:30,80 --> 00:04:34,80
and use browser caching with the chunk's name.

90
00:04:34,80 --> 00:04:36,60
With that out of the way, we're ready

91
00:04:36,60 --> 00:04:40,40
to close this section of the title.

92
00:04:40,40 --> 00:04:42,00
We'll do a quick recap of everything

93
00:04:42,00 --> 00:04:44,40
that we've covered in the past five videos.

94
00:04:44,40 --> 00:04:47,70
First of all, we've looked at how to set up Babel and React

95
00:04:47,70 --> 00:04:50,10
and how to get a basic React application running.

96
00:04:50,10 --> 00:04:52,20
Then we have developed and written

97
00:04:52,20 --> 00:04:54,70
a Webpack 2 configuration file

98
00:04:54,70 --> 00:04:56,80
that we can use across all React projects,

99
00:04:56,80 --> 00:05:00,50
so feel free to take the Webpack config.js

100
00:05:00,50 --> 00:05:02,80
and drop it into any new React project

101
00:05:02,80 --> 00:05:05,30
or any existing React projects that you're working on.

102
00:05:05,30 --> 00:05:07,80
It should always behave the same,

103
00:05:07,80 --> 00:05:11,20
it should have your cache busting enabled,

104
00:05:11,20 --> 00:05:13,20
it should have your vendor splitting enabled

105
00:05:13,20 --> 00:05:14,30
and everything else.

106
00:05:14,30 --> 00:05:16,90
Then we have added our application,

107
00:05:16,90 --> 00:05:19,30
the MyReads application on top of our basic app,

108
00:05:19,30 --> 00:05:22,10
so we have successfully imported an existing React app

109
00:05:22,10 --> 00:05:24,60
and bundled it with Webpack.

110
00:05:24,60 --> 00:05:27,60
And then we have also looked at increasing performance

111
00:05:27,60 --> 00:05:31,60
by reducing load times, as well as cache busting

112
00:05:31,60 --> 00:05:35,80
and how browsers load files, and what is the best way

113
00:05:35,80 --> 00:05:39,90
to leverage the caching in modern browsers.

114
00:05:39,90 --> 00:05:42,30
Also towards the end, just in this last video,

115
00:05:42,30 --> 00:05:45,60
we have also looked at a bit of quality of life improvement.

116
00:05:45,60 --> 00:05:48,60
We have created a small script that will always run

117
00:05:48,60 --> 00:05:51,20
whenever we run our dev command.

118
00:05:51,20 --> 00:05:52,90
And this script will just make sure

119
00:05:52,90 --> 00:05:54,90
that all the duplicated bundle JSes

120
00:05:54,90 --> 00:05:58,90
and all the other files are always cleaned and removed

121
00:05:58,90 --> 00:06:03,20
from the disk directory whenever we're building the project.

122
00:06:03,20 --> 00:06:05,60
In the next section, we will cover a few

123
00:06:05,60 --> 00:06:09,20
React-specific topics, we will mostly have a look

124
00:06:09,20 --> 00:06:12,90
at the webpack-dev-server and how that can interact

125
00:06:12,90 --> 00:06:14,70
with your React application.

126
00:06:14,70 --> 00:06:16,30
We will also look at the best practices

127
00:06:16,30 --> 00:06:18,20
of using the webpack-dev-server,

128
00:06:18,20 --> 00:06:21,20
as well as a few words on react-router

129
00:06:21,20 --> 00:06:23,90
and how to use react-router with plain component

130
00:06:23,90 --> 00:06:27,00
in order to increase performance of your application.

