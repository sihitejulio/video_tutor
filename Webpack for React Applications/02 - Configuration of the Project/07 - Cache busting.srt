1
00:00:01,40 --> 00:00:02,60
- [Instructor] In the previous videos

2
00:00:02,60 --> 00:00:04,90
we've had a look at how to

3
00:00:04,90 --> 00:00:07,20
deal with large files and browsers

4
00:00:07,20 --> 00:00:09,60
and how to split our vendor libraries

5
00:00:09,60 --> 00:00:11,50
from our application files

6
00:00:11,50 --> 00:00:15,20
in order to increase performance and decrease load times.

7
00:00:15,20 --> 00:00:17,40
In this video, we will have a quick look at

8
00:00:17,40 --> 00:00:19,40
the method called cache busting

9
00:00:19,40 --> 00:00:21,80
and how we an use this to our advantage

10
00:00:21,80 --> 00:00:25,30
whenever dealing with webpack and React projects.

11
00:00:25,30 --> 00:00:27,90
We will have a look again at the browser caching

12
00:00:27,90 --> 00:00:31,10
and how it works with our MyReads application.

13
00:00:31,10 --> 00:00:33,30
Then we will have a look at how to only

14
00:00:33,30 --> 00:00:36,20
try and cache the files from our vendor libs,

15
00:00:36,20 --> 00:00:39,20
which are files that are updated very rarely.

16
00:00:39,20 --> 00:00:42,30
And then we will also have a look at how to bust a cache

17
00:00:42,30 --> 00:00:44,70
in case we need to update anything

18
00:00:44,70 --> 00:00:47,10
and make sure our files are reloaded

19
00:00:47,10 --> 00:00:48,40
only when they get changed

20
00:00:48,40 --> 00:00:51,40
so that whenever a user visits our application again,

21
00:00:51,40 --> 00:00:54,20
or our page again, they only load the differences

22
00:00:54,20 --> 00:00:56,30
between what they loaded before.

23
00:00:56,30 --> 00:00:59,80
So let's go back to the browser to our project

24
00:00:59,80 --> 00:01:01,40
where our application is running

25
00:01:01,40 --> 00:01:05,20
and see exactly how we can achieve this.

26
00:01:05,20 --> 00:01:07,80
So for the moment, I have opened Chrome inspector

27
00:01:07,80 --> 00:01:09,30
and disabled the cache again.

28
00:01:09,30 --> 00:01:10,80
Make sure you do that as well.

29
00:01:10,80 --> 00:01:12,10
Let's refresh our page

30
00:01:12,10 --> 00:01:14,80
and as you can see we're loading our vendor.js,

31
00:01:14,80 --> 00:01:19,20
manifest.js, and bundle.js every time.

32
00:01:19,20 --> 00:01:21,90
Now if we untick this Disable cache

33
00:01:21,90 --> 00:01:24,70
which is what most standard users

34
00:01:24,70 --> 00:01:26,20
will have in their browsers.

35
00:01:26,20 --> 00:01:29,20
All browsers by default enable cache.

36
00:01:29,20 --> 00:01:31,50
And when we refresh the page,

37
00:01:31,50 --> 00:01:34,30
you will see that our manifest.js, vendor.js,

38
00:01:34,30 --> 00:01:39,00
and everything else will be loaded from our disk.

39
00:01:39,00 --> 00:01:40,90
So let's visit the page again.

40
00:01:40,90 --> 00:01:43,10
As you can see, it's been cached by the browser

41
00:01:43,10 --> 00:01:46,20
and it's been loaded from disk, which is great.

42
00:01:46,20 --> 00:01:47,90
The only problem with this is that,

43
00:01:47,90 --> 00:01:51,30
for example, if we go back into the code for the application

44
00:01:51,30 --> 00:01:54,20
and we try and change something,

45
00:01:54,20 --> 00:01:58,80
so let's try and go and change our app.js.

46
00:01:58,80 --> 00:02:04,20
So let's just say instead of showing the MyReads title,

47
00:02:04,20 --> 00:02:06,00
we want to change the title,

48
00:02:06,00 --> 00:02:09,10
which is found into the home.js.

49
00:02:09,10 --> 00:02:11,50
So if we want to change the title from MyReads

50
00:02:11,50 --> 00:02:13,40
to MyReads project for example,

51
00:02:13,40 --> 00:02:15,00
once we save this,

52
00:02:15,00 --> 00:02:17,30
we'll see webpack compiles everything again.

53
00:02:17,30 --> 00:02:19,70
However, if we refresh the browser,

54
00:02:19,70 --> 00:02:21,10
it will be smart enough to know

55
00:02:21,10 --> 00:02:24,00
that the files have been updated.

56
00:02:24,00 --> 00:02:25,90
However, sometimes the browser will not know

57
00:02:25,90 --> 00:02:28,40
that this has been changed,

58
00:02:28,40 --> 00:02:30,20
and so we need to make sure that

59
00:02:30,20 --> 00:02:33,90
webpack knows how to only edit the files

60
00:02:33,90 --> 00:02:36,30
that have been changed in our application.

61
00:02:36,30 --> 00:02:38,50
So let's have a quick look at this diagram

62
00:02:38,50 --> 00:02:40,90
and see exactly what we want to achieve.

63
00:02:40,90 --> 00:02:42,40
If your remember in the previous video,

64
00:02:42,40 --> 00:02:45,90
we spoke about code splitting and how we do it.

65
00:02:45,90 --> 00:02:48,70
And we've said, we'll pull all the libraries

66
00:02:48,70 --> 00:02:52,40
into our vendor.js and all the code into bundle.js

67
00:02:52,40 --> 00:02:54,00
and send those to the user browsers.

68
00:02:54,00 --> 00:02:56,30
However, whenever the browser is caching files,

69
00:02:56,30 --> 00:02:58,90
the only way it will know that the file changed

70
00:02:58,90 --> 00:03:03,60
is if the file name changes as well with the browser.

71
00:03:03,60 --> 00:03:05,20
So what we would want to do is,

72
00:03:05,20 --> 00:03:09,30
instead of sending just the vendor.js and a bundle.js

73
00:03:09,30 --> 00:03:11,60
we need to somehow tell webpack

74
00:03:11,60 --> 00:03:13,60
that whenever a file changes,

75
00:03:13,60 --> 00:03:15,90
change the name of the file as well.

76
00:03:15,90 --> 00:03:17,70
As you can imagine, you could that manually,

77
00:03:17,70 --> 00:03:19,70
so whenever you build the application,

78
00:03:19,70 --> 00:03:22,90
you can just go in there and change the name

79
00:03:22,90 --> 00:03:24,80
from bundle to something else.

80
00:03:24,80 --> 00:03:26,80
So whenever you would want to change it,

81
00:03:26,80 --> 00:03:28,70
you'll basically need to change your webpack.config

82
00:03:28,70 --> 00:03:30,20
and you will need to make sure

83
00:03:30,20 --> 00:03:31,40
that it creates a new bundle,

84
00:03:31,40 --> 00:03:34,10
which is quite complicated to do

85
00:03:34,10 --> 00:03:36,10
every time you update the file.

86
00:03:36,10 --> 00:03:38,80
So what we can do and what webpack knows how to do

87
00:03:38,80 --> 00:03:42,50
is to create a file checksum

88
00:03:42,50 --> 00:03:44,00
and append it at the end of the file

89
00:03:44,00 --> 00:03:45,80
whenever it is building it,

90
00:03:45,80 --> 00:03:47,40
is building the specific file.

91
00:03:47,40 --> 00:03:50,70
So in order to do that, we will go into the config

92
00:03:50,70 --> 00:03:53,40
and then where we have our output,

93
00:03:53,40 --> 00:03:56,40
we will add a small modification

94
00:03:56,40 --> 00:03:59,00
to this particular property

95
00:03:59,00 --> 00:04:03,20
and we will add something that is called chunkhash.

96
00:04:03,20 --> 00:04:05,10
What this basically does, it tells webpack

97
00:04:05,10 --> 00:04:07,50
whenever you're creating a new file

98
00:04:07,50 --> 00:04:10,40
from what you're compiling, make sure that

99
00:04:10,40 --> 00:04:13,90
you are also adding the checksum of the file

100
00:04:13,90 --> 00:04:15,90
which is just a random string of characters

101
00:04:15,90 --> 00:04:18,00
based on the contents of the file.

102
00:04:18,00 --> 00:04:21,70
What will happen now is whenever we update our app.js,

103
00:04:21,70 --> 00:04:24,30
the bundle.js will update,

104
00:04:24,30 --> 00:04:27,30
however the vendor.js will stay at the same version

105
00:04:27,30 --> 00:04:28,70
as it was previously.

106
00:04:28,70 --> 00:04:29,90
So let's save this.

107
00:04:29,90 --> 00:04:31,60
Let's build our app.

108
00:04:31,60 --> 00:04:34,30
And it seems I made a mistake in here.

109
00:04:34,30 --> 00:04:36,40
Let's just fix that quickly.

110
00:04:36,40 --> 00:04:39,80
Seems I've accidentally removed my node module,

111
00:04:39,80 --> 00:04:41,90
so I'm just going to reinstall them again

112
00:04:41,90 --> 00:04:44,50
and then have the app compile.

113
00:04:44,50 --> 00:04:47,00
So if we go in here, we create the chunkhash.

114
00:04:47,00 --> 00:04:50,40
We add the chunkhash into our output

115
00:04:50,40 --> 00:04:52,50
and we run webpack.

116
00:04:52,50 --> 00:04:55,80
And you'll see once the files get built,

117
00:04:55,80 --> 00:04:57,90
the output will be a bit different

118
00:04:57,90 --> 00:04:59,10
from what we had before.

119
00:04:59,10 --> 00:05:00,90
So as you can see now,

120
00:05:00,90 --> 00:05:03,60
we're loading a vendor dot,

121
00:05:03,60 --> 00:05:05,80
a seemingly random string of characters,

122
00:05:05,80 --> 00:05:07,80
which is the checksum of the file

123
00:05:07,80 --> 00:05:09,50
and then we also have a bundle

124
00:05:09,50 --> 00:05:12,30
and then this bundle has a

125
00:05:12,30 --> 00:05:15,50
different string of characters at the end.

126
00:05:15,50 --> 00:05:18,80
If we go back to our browser now and refresh,

127
00:05:18,80 --> 00:05:21,80
as you can see, Chrome will load those files

128
00:05:21,80 --> 00:05:24,20
because we're using the HTML webpack plugins,

129
00:05:24,20 --> 00:05:27,70
which automatically adds them to our page.

130
00:05:27,70 --> 00:05:29,60
Now if we refresh a few times,

131
00:05:29,60 --> 00:05:32,30
as you can see, those files become cached.

132
00:05:32,30 --> 00:05:36,80
Now if we want to update something in our application,

133
00:05:36,80 --> 00:05:40,70
chances are we do not need to load the vendor.js anymore.

134
00:05:40,70 --> 00:05:42,50
So let's make that change.

135
00:05:42,50 --> 00:05:44,20
Go back into home.

136
00:05:44,20 --> 00:05:46,60
Let's move this back to MyReads.

137
00:05:46,60 --> 00:05:48,10
We'll save this

138
00:05:48,10 --> 00:05:50,80
and webpack will bundle everything again,

139
00:05:50,80 --> 00:05:53,20
and if we go into the browser and refresh,

140
00:05:53,20 --> 00:05:55,10
you will see a few files have been loaded.

141
00:05:55,10 --> 00:05:57,60
First of all we have the vendor file

142
00:05:57,60 --> 00:06:01,10
and then we have the bundle.js that has been created.

143
00:06:01,10 --> 00:06:04,00
If we look at the webpack output,

144
00:06:04,00 --> 00:06:07,30
you'll see that the only file that has been updated

145
00:06:07,30 --> 00:06:09,20
is our bundle.

146
00:06:09,20 --> 00:06:10,20
So as you can see,

147
00:06:10,20 --> 00:06:12,80
the string characters at the end of bundle

148
00:06:12,80 --> 00:06:15,80
is different from the one that we had previously.

149
00:06:15,80 --> 00:06:19,70
And in this case, the vendor file as remained unchanged,

150
00:06:19,70 --> 00:06:22,40
which means that if we refresh a few times,

151
00:06:22,40 --> 00:06:25,00
once Chrome starts caching,

152
00:06:25,00 --> 00:06:28,30
it will not load the vendor any more.

153
00:06:28,30 --> 00:06:31,00
So as you can see, you have the aade9

154
00:06:31,00 --> 00:06:33,00
and if we change this again,

155
00:06:33,00 --> 00:06:34,40
webpack will rebuild that file,

156
00:06:34,40 --> 00:06:39,90
will rebuild a new bundle which will be different

157
00:06:39,90 --> 00:06:42,10
from the one that's currently in the browser.

158
00:06:42,10 --> 00:06:48,30
So the new bundle is called bundle.d1f77 something,

159
00:06:48,30 --> 00:06:52,50
and the only one is 9595 etc.

160
00:06:52,50 --> 00:06:54,30
And so if we refresh this as you can see,

161
00:06:54,30 --> 00:06:56,90
we're loading in the new bundle

162
00:06:56,90 --> 00:07:02,50
and we're loading the vendor.js from cache.

163
00:07:02,50 --> 00:07:04,70
Just re-enable disable caching for development,

164
00:07:04,70 --> 00:07:07,30
but that's basically how you ensure

165
00:07:07,30 --> 00:07:08,70
that whenever you're changing a file,

166
00:07:08,70 --> 00:07:10,80
it will only update the correct files.

167
00:07:10,80 --> 00:07:13,60
So whenever somebody visits your site again,

168
00:07:13,60 --> 00:07:17,90
they will not need to download the large vendor.js file,

169
00:07:17,90 --> 00:07:19,50
they will only use that once.

170
00:07:19,50 --> 00:07:21,40
And then every time they join,

171
00:07:21,40 --> 00:07:23,00
they will only download the bundle

172
00:07:23,00 --> 00:07:25,40
which is what you have changed.

173
00:07:25,40 --> 00:07:26,50
One thing to keep in mind,

174
00:07:26,50 --> 00:07:28,00
one of the problem with this technique

175
00:07:28,00 --> 00:07:30,60
is that if we look into our dist folder,

176
00:07:30,60 --> 00:07:34,70
you'll see we have a lot of bundles that we have in here.

177
00:07:34,70 --> 00:07:37,60
So we would like to probably clean this folder

178
00:07:37,60 --> 00:07:39,40
every time we build our app.

179
00:07:39,40 --> 00:07:41,70
Also, it might not seem as a big deal now,

180
00:07:41,70 --> 00:07:44,30
but when you rebuild your project about 100 times

181
00:07:44,30 --> 00:07:47,60
you don't want to end up with 200 bundle.js and everything.

182
00:07:47,60 --> 00:07:49,80
And this is the reason why in the previous video

183
00:07:49,80 --> 00:07:53,00
we have also included our manifest

184
00:07:53,00 --> 00:07:55,00
into the optimize basically.

185
00:07:55,00 --> 00:07:57,40
Manifest is a JavaScript file

186
00:07:57,40 --> 00:07:58,90
that will be created every time

187
00:07:58,90 --> 00:08:01,40
and that will make sure that the browser

188
00:08:01,40 --> 00:08:04,80
knows exactly which versions of the vendor

189
00:08:04,80 --> 00:08:08,60
and bundle.js to load when the page is being loaded.

190
00:08:08,60 --> 00:08:10,70
So let's have a quick look at

191
00:08:10,70 --> 00:08:13,50
those files as well.

192
00:08:13,50 --> 00:08:15,10
They are very similar as you can see.

193
00:08:15,10 --> 00:08:18,30
They are the bundled versions of our applications.

194
00:08:18,30 --> 00:08:20,70
And as we can see, we only have one vendor,

195
00:08:20,70 --> 00:08:22,30
which has been cached by browser

196
00:08:22,30 --> 00:08:25,00
and then a lot of small bundle.js files

197
00:08:25,00 --> 00:08:26,30
which are not being cached

198
00:08:26,30 --> 00:08:28,40
and they're being updated every time

199
00:08:28,40 --> 00:08:31,20
the user refreshes the page.

200
00:08:31,20 --> 00:08:33,40
So now that we have looked at to bust a cache

201
00:08:33,40 --> 00:08:35,70
and how to make sure we only load what we need,

202
00:08:35,70 --> 00:08:38,20
let's have a quick look at how to clean up our project

203
00:08:38,20 --> 00:08:40,70
and make sure that those 20 plus files

204
00:08:40,70 --> 00:08:42,60
are automatically cleaned

205
00:08:42,60 --> 00:08:46,00
every time you build your application.

