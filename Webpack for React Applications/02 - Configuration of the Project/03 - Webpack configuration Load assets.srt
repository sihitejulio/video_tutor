1
00:00:00,80 --> 00:00:02,90
- [Instructor] In the previous video we have looked at

2
00:00:02,90 --> 00:00:05,00
how to install and configure Babel,

3
00:00:05,00 --> 00:00:07,70
as well as how to get React into our application,

4
00:00:07,70 --> 00:00:09,80
into our basic application.

5
00:00:09,80 --> 00:00:12,70
Now let's have a quick look at some other functionalities

6
00:00:12,70 --> 00:00:14,60
of Webpack, and what we will need

7
00:00:14,60 --> 00:00:17,20
in order to be able to build and run our

8
00:00:17,20 --> 00:00:20,20
MyReads bookshelf project.

9
00:00:20,20 --> 00:00:21,90
In this video, we'll be talking about

10
00:00:21,90 --> 00:00:24,10
the general Webpack configuration,

11
00:00:24,10 --> 00:00:26,40
and we will cover the following topics.

12
00:00:26,40 --> 00:00:28,50
First of all, we will have a look at

13
00:00:28,50 --> 00:00:31,10
how to load different other assets with loaders.

14
00:00:31,10 --> 00:00:33,10
We'll check out what loaders we need,

15
00:00:33,10 --> 00:00:36,10
as well as how to set up a loader in Webpack.

16
00:00:36,10 --> 00:00:39,70
So we will talk a bit about loaders a bit more generically,

17
00:00:39,70 --> 00:00:42,60
so that you can use any loader you might need in the future.

18
00:00:42,60 --> 00:00:45,70
We'll also talk about managing our project's assets.

19
00:00:45,70 --> 00:00:48,40
We'll talk about the file loader, the CSS loader,

20
00:00:48,40 --> 00:00:51,20
as well as the S-A-S-S loader, or SASS loader,

21
00:00:51,20 --> 00:00:53,80
if you like using S-A-S-S.

22
00:00:53,80 --> 00:00:55,40
And then towards the end of the video,

23
00:00:55,40 --> 00:00:59,20
we will then be adding the MyReads app code

24
00:00:59,20 --> 00:01:00,60
into our basic project,

25
00:01:00,60 --> 00:01:05,50
and trying to run that app into the browser.

26
00:01:05,50 --> 00:01:07,20
So going back into our project,

27
00:01:07,20 --> 00:01:08,00
we have created our app.js,

28
00:01:08,00 --> 00:01:11,20
we have our React installed,

29
00:01:11,20 --> 00:01:15,30
and we need to continue configuring our Webpack

30
00:01:15,30 --> 00:01:18,00
in order to load various other files.

31
00:01:18,00 --> 00:01:19,40
If you remember initially,

32
00:01:19,40 --> 00:01:22,70
when we've created the config variable,

33
00:01:22,70 --> 00:01:24,90
we have created the loaders array,

34
00:01:24,90 --> 00:01:27,10
which tells Webpack what it needs to load,

35
00:01:27,10 --> 00:01:30,80
and with which files it needs to load them.

36
00:01:30,80 --> 00:01:31,70
Let's have a look at

37
00:01:31,70 --> 00:01:33,50
what else we might need for our project.

38
00:01:33,50 --> 00:01:36,90
So first of all, we have our JavaScript files,

39
00:01:36,90 --> 00:01:38,70
which contain the bulk of our project,

40
00:01:38,70 --> 00:01:40,60
but in order to style any project,

41
00:01:40,60 --> 00:01:45,60
we also need access to CSS files and images.

42
00:01:45,60 --> 00:01:46,70
Those, in Webpack,

43
00:01:46,70 --> 00:01:49,40
are loaded with two very important loaders,

44
00:01:49,40 --> 00:01:53,20
one of them being called the CSS loader,

45
00:01:53,20 --> 00:01:55,60
and one of them being called the file loader.

46
00:01:55,60 --> 00:01:58,20
So let's get those installed and see how they work

47
00:01:58,20 --> 00:01:59,70
in our project.

48
00:01:59,70 --> 00:02:01,70
First of all, we will need to install them.

49
00:02:01,70 --> 00:02:05,20
So if we go back into our terminal,

50
00:02:05,20 --> 00:02:08,50
we will need install a few packages.

51
00:02:08,50 --> 00:02:11,20
We will install the CSS loader,

52
00:02:11,20 --> 00:02:15,90
so yarn add css-loader,

53
00:02:15,90 --> 00:02:18,50
and we will install this as a development dependency,

54
00:02:18,50 --> 00:02:22,90
because it only needs to work while developing.

55
00:02:22,90 --> 00:02:25,90
While Yarn is doing everything here, let's make sure we have

56
00:02:25,90 --> 00:02:28,10
all the other dependencies for this installed.

57
00:02:28,10 --> 00:02:30,40
So as you can see, no word means no errors.

58
00:02:30,40 --> 00:02:35,30
We will also need to install our file loader.

59
00:02:35,30 --> 00:02:40,40
So we will do, again, yarn add file-loader,

60
00:02:40,40 --> 00:02:42,20
again as a development dependency.

61
00:02:42,20 --> 00:02:43,50
We will get this installed as well.

62
00:02:43,50 --> 00:02:46,90
This will allow us to handle any type of images

63
00:02:46,90 --> 00:02:48,50
that we have in our project.

64
00:02:48,50 --> 00:02:52,10
And while we're at it, we'll also install a

65
00:02:52,10 --> 00:02:54,60
image loader, just to have it available

66
00:02:54,60 --> 00:02:57,10
in case we decide we want to compress any images

67
00:02:57,10 --> 00:02:59,30
at a later date, and bundle them together.

68
00:02:59,30 --> 00:03:03,30
So we will install the image-webpack-loader,

69
00:03:03,30 --> 00:03:06,10
that's how it's called, image-webpack-loader,

70
00:03:06,10 --> 00:03:08,70
again install as dependency,

71
00:03:08,70 --> 00:03:10,00
and once this is done,

72
00:03:10,00 --> 00:03:12,00
we can go back into our Webpack config

73
00:03:12,00 --> 00:03:15,10
and start building our configuration files.

74
00:03:15,10 --> 00:03:19,90
Let's just wait for this to finish, and, anytime soon.

75
00:03:19,90 --> 00:03:21,00
Just takes a bit of time,

76
00:03:21,00 --> 00:03:24,50
depending on your internet connection.

77
00:03:24,50 --> 00:03:26,20
And if we, we'll just check again,

78
00:03:26,20 --> 00:03:28,10
make sure all the packages are fine.

79
00:03:28,10 --> 00:03:29,30
There were no errors.

80
00:03:29,30 --> 00:03:30,10
Excellent.

81
00:03:30,10 --> 00:03:32,80
So let's go back into our webpackconfig.js

82
00:03:32,80 --> 00:03:36,80
and start writing our real Webpack configuration file.

83
00:03:36,80 --> 00:03:38,50
One thing that we will do,

84
00:03:38,50 --> 00:03:43,10
in order to keep up with the way Webpack is evolving,

85
00:03:43,10 --> 00:03:45,60
we will need to do some small tweaks

86
00:03:45,60 --> 00:03:48,90
to our initial webpackconfig.js,

87
00:03:48,90 --> 00:03:51,30
just to keep everything consistent

88
00:03:51,30 --> 00:03:53,50
and future-proof it a little bit more.

89
00:03:53,50 --> 00:03:58,00
So Webpack has changed the loaders array to be called rules.

90
00:03:58,00 --> 00:04:01,00
And then everything else pretty much will be the same.

91
00:04:01,00 --> 00:04:03,10
So make sure you just update this,

92
00:04:03,10 --> 00:04:05,70
in case you try and run the project

93
00:04:05,70 --> 00:04:07,30
with a different version of Webpack,

94
00:04:07,30 --> 00:04:11,30
just to make sure the project still runs as intended.

95
00:04:11,30 --> 00:04:14,40
So we have created a loader configuration

96
00:04:14,40 --> 00:04:16,30
for our JS and JSX files,

97
00:04:16,30 --> 00:04:18,40
which are going to be the react.

98
00:04:18,40 --> 00:04:21,60
And we have made sure that those are being loaded

99
00:04:21,60 --> 00:04:23,50
with babel-loader.

100
00:04:23,50 --> 00:04:26,20
Now let us include some CSS in our project.

101
00:04:26,20 --> 00:04:28,80
So let us test for CSS files.

102
00:04:28,80 --> 00:04:31,40
As you can see, we've installed the CSS loader,

103
00:04:31,40 --> 00:04:35,80
so we will need to test for .css files,

104
00:04:35,80 --> 00:04:40,60
and we will need to use the CSS loader for this.

105
00:04:40,60 --> 00:04:45,80
So we'll do use, and then, in here we will add css-loader.

106
00:04:45,80 --> 00:04:48,40
We'll save our Webpack config.

107
00:04:48,40 --> 00:04:51,10
We'll just run dev quickly, just to make sure

108
00:04:51,10 --> 00:04:53,50
we didn't make any mistakes in the config file.

109
00:04:53,50 --> 00:04:54,90
That's a really good way of checking,

110
00:04:54,90 --> 00:04:56,60
always just run the dev script.

111
00:04:56,60 --> 00:05:00,30
And let's actually add some CSS into our project.

112
00:05:00,30 --> 00:05:04,30
So into the source, I will just create a quick CSS file.

113
00:05:04,30 --> 00:05:07,10
I will just call this app.css for now.

114
00:05:07,10 --> 00:05:09,20
We will change this later.

115
00:05:09,20 --> 00:05:12,20
And let's do some styles that we want to add,

116
00:05:12,20 --> 00:05:16,20
so let's do body and let's just do a background

117
00:05:16,20 --> 00:05:18,80
does babyblue work?

118
00:05:18,80 --> 00:05:21,90
Let's save this, let's build our project again,

119
00:05:21,90 --> 00:05:24,40
and run it in the browser.

120
00:05:24,40 --> 00:05:26,80
So we will build our project.

121
00:05:26,80 --> 00:05:29,50
As you can see, everything has been built.

122
00:05:29,50 --> 00:05:31,20
One thing we have forgot to do

123
00:05:31,20 --> 00:05:36,10
is to actually include the CSS file into our project,

124
00:05:36,10 --> 00:05:38,40
because at the moment, we're not including it anywhere.

125
00:05:38,40 --> 00:05:39,60
So let's do that as well.

126
00:05:39,60 --> 00:05:41,40
We'll go back into our app.js,

127
00:05:41,40 --> 00:05:46,10
and into our app.js, we will just import the CSS.

128
00:05:46,10 --> 00:05:48,10
Keep in mind, whenever you're doing this,

129
00:05:48,10 --> 00:05:51,90
Webpack will automatically detect that it's a CSS file,

130
00:05:51,90 --> 00:05:53,60
and process it accordingly.

131
00:05:53,60 --> 00:05:54,90
So let's save.

132
00:05:54,90 --> 00:05:58,10
Let's run the build script again.

133
00:05:58,10 --> 00:06:02,20
And once this is done, we can go into our browser

134
00:06:02,20 --> 00:06:05,60
and refresh the page, and nothing happened at this moment.

135
00:06:05,60 --> 00:06:09,50
It is expected, because Webpack is only used to dealing

136
00:06:09,50 --> 00:06:10,70
with JavaScript files.

137
00:06:10,70 --> 00:06:14,10
So it doesn't know exactly what to do with CSS

138
00:06:14,10 --> 00:06:14,90
at this point.

139
00:06:14,90 --> 00:06:18,10
If we inspect the page, at this point, as you can see,

140
00:06:18,10 --> 00:06:20,80
and we find our app.bundle.js,

141
00:06:20,80 --> 00:06:24,00
so if we open this in a new tab,

142
00:06:24,00 --> 00:06:26,20
you'll be able to see

143
00:06:26,20 --> 00:06:28,40
that going all the way to the bottom,

144
00:06:28,40 --> 00:06:29,80
but let's find our body.

145
00:06:29,80 --> 00:06:30,80
So as you can see,

146
00:06:30,80 --> 00:06:34,90
our CSS code has been added into the bundle.

147
00:06:34,90 --> 00:06:37,00
However, the browser doesn't know what to do with it,

148
00:06:37,00 --> 00:06:40,50
because CSS is not supposed to be in JS files.

149
00:06:40,50 --> 00:06:42,90
So in order to deal with this situation,

150
00:06:42,90 --> 00:06:46,70
we will need to install another loader for Webpack,

151
00:06:46,70 --> 00:06:49,10
which is called style loader.

152
00:06:49,10 --> 00:06:50,80
And what the style loader does,

153
00:06:50,80 --> 00:06:54,30
it basically takes any CSS from your JavaScript,

154
00:06:54,30 --> 00:06:58,50
and then adds it inside the page as CSS text,

155
00:06:58,50 --> 00:07:01,80
which is how the browser knows to handle that.

156
00:07:01,80 --> 00:07:05,40
So let's install our style loader quickly.

157
00:07:05,40 --> 00:07:10,40
So we'll do again, yarn add, we'll do the style-loader.

158
00:07:10,40 --> 00:07:14,10
This will be installed as dev dependency.

159
00:07:14,10 --> 00:07:16,00
And once we have this installed,

160
00:07:16,00 --> 00:07:18,00
we will go back into our Webpack config,

161
00:07:18,00 --> 00:07:21,70
and we'll do a small tweak to our rules.

162
00:07:21,70 --> 00:07:24,30
One thing that is good about Webpack and loaders

163
00:07:24,30 --> 00:07:26,10
is that you can chain them together.

164
00:07:26,10 --> 00:07:29,00
So if you want to, first of all,

165
00:07:29,00 --> 00:07:30,80
load the style into your JavaScript,

166
00:07:30,80 --> 00:07:33,80
and then put it into the page itself,

167
00:07:33,80 --> 00:07:35,20
into the hot page as the CSS,

168
00:07:35,20 --> 00:07:39,30
you can chain the CSS loader and the style loader together,

169
00:07:39,30 --> 00:07:43,00
and that will create the output that we're looking for.

170
00:07:43,00 --> 00:07:45,20
So in order to chain two loaders together,

171
00:07:45,20 --> 00:07:47,80
we will go into our modules objects, into the rules,

172
00:07:47,80 --> 00:07:49,40
and then just create an array.

173
00:07:49,40 --> 00:07:52,10
So instead of having just one loader,

174
00:07:52,10 --> 00:07:54,20
we'll create an array of loaders

175
00:07:54,20 --> 00:07:56,90
that we tell Webpack to use.

176
00:07:56,90 --> 00:07:59,40
One thing that is very important is to keep in mind

177
00:07:59,40 --> 00:08:04,00
that you will always want to add the

178
00:08:04,00 --> 00:08:05,90
most-recently used loaders in front.

179
00:08:05,90 --> 00:08:09,40
So basically what Webpack understands from here is that

180
00:08:09,40 --> 00:08:11,50
it will initially use css-loader

181
00:08:11,50 --> 00:08:14,10
to load it as inside the JavaScript,

182
00:08:14,10 --> 00:08:17,30
and only after that it will use the style-loader.

183
00:08:17,30 --> 00:08:18,80
So the order is a bit reversed,

184
00:08:18,80 --> 00:08:21,10
but hopefully, once you do this a few times,

185
00:08:21,10 --> 00:08:22,00
you'll get used to it,

186
00:08:22,00 --> 00:08:26,10
and it shouldn't be any issues at this point.

187
00:08:26,10 --> 00:08:32,00
So let's save, and let us build the project again.

188
00:08:32,00 --> 00:08:35,50
And hopefully, when we refresh the page,

189
00:08:35,50 --> 00:08:39,50
we should see the background of the application change,

190
00:08:39,50 --> 00:08:41,10
unless I made a mistake,

191
00:08:41,10 --> 00:08:43,40
and babyblue is not actually a color.

192
00:08:43,40 --> 00:08:45,00
So let's try and do that.

193
00:08:45,00 --> 00:08:49,10
Let's fix that issue quickly.

194
00:08:49,10 --> 00:08:54,90
Let's go into our body, and background.

195
00:08:54,90 --> 00:08:56,80
Let's do blue in this case.

196
00:08:56,80 --> 00:09:00,40
I'm really sorry, for this color is not ideal,

197
00:09:00,40 --> 00:09:01,50
but it proves the point.

198
00:09:01,50 --> 00:09:05,10
So let's do salmon actually, a bit more easier to look at.

199
00:09:05,10 --> 00:09:08,70
So we'll run the dev build again,

200
00:09:08,70 --> 00:09:10,20
and we will refresh the page,

201
00:09:10,20 --> 00:09:11,40
and as you can see,

202
00:09:11,40 --> 00:09:14,40
we have managed to get our styles loaded in.

203
00:09:14,40 --> 00:09:16,10
The same way you do this, now you can include

204
00:09:16,10 --> 00:09:18,20
as many style sheets as you need

205
00:09:18,20 --> 00:09:19,20
into your project,

206
00:09:19,20 --> 00:09:22,50
by just importing them into the components

207
00:09:22,50 --> 00:09:25,40
or the JavaScript files that require them.

208
00:09:25,40 --> 00:09:29,10
One thing that I want to mention at this point is that

209
00:09:29,10 --> 00:09:33,00
having to rebuild every file every time is not ideal.

210
00:09:33,00 --> 00:09:35,60
It's not exactly efficient.

211
00:09:35,60 --> 00:09:37,90
Running the command every time we change a file

212
00:09:37,90 --> 00:09:39,20
is not a productive workflow.

213
00:09:39,20 --> 00:09:42,80
So we can quickly tweak our built script

214
00:09:42,80 --> 00:09:47,70
into our package.json to help us a bit with this.

215
00:09:47,70 --> 00:09:51,40
So we will be running the Webpack in development mode,

216
00:09:51,40 --> 00:09:54,50
and we will also add a flag called watch.

217
00:09:54,50 --> 00:09:56,00
Basically, what this will do is

218
00:09:56,00 --> 00:09:59,20
whenever we run our script, it will keep watching for files.

219
00:09:59,20 --> 00:10:02,10
As you can see, Webpack is watching for files.

220
00:10:02,10 --> 00:10:03,90
So whenever we change one of the files

221
00:10:03,90 --> 00:10:05,30
that's inside our project,

222
00:10:05,30 --> 00:10:07,60
it will automatically rebuild the project

223
00:10:07,60 --> 00:10:09,40
and regenerate the bundle.

224
00:10:09,40 --> 00:10:12,80
So for example, let's add some space in here,

225
00:10:12,80 --> 00:10:13,90
just so you see how it works.

226
00:10:13,90 --> 00:10:18,00
If we change our, this to beige for example,

227
00:10:18,00 --> 00:10:21,10
when we go back in here, as you can see,

228
00:10:21,10 --> 00:10:23,90
Webpack has rebuilt our bundle,

229
00:10:23,90 --> 00:10:26,20
and now if we just refresh the page,

230
00:10:26,20 --> 00:10:29,20
the page will be updated.

231
00:10:29,20 --> 00:10:30,80
We will take a quick break in this video,

232
00:10:30,80 --> 00:10:32,50
and then we will continue in the next video

233
00:10:32,50 --> 00:10:33,80
with the file loader

234
00:10:33,80 --> 00:10:38,00
and the best way of loading images into our projects.

