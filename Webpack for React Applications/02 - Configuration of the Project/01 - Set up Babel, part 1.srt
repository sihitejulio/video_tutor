1
00:00:01,30 --> 00:00:03,50
- [Instructor] Hi, and welcome to the second section

2
00:00:03,50 --> 00:00:07,50
of this cycle, the single React productivity with Webpack.

3
00:00:07,50 --> 00:00:09,50
In the previous sections, we have had a look

4
00:00:09,50 --> 00:00:12,40
at why Webpack has become such a popular tool

5
00:00:12,40 --> 00:00:17,20
and why single page applications are being more and more

6
00:00:17,20 --> 00:00:21,10
developed by big companies and small companies alike.

7
00:00:21,10 --> 00:00:24,30
In this section, we will have a look at how to configure

8
00:00:24,30 --> 00:00:26,60
our React projects to work with Webpack,

9
00:00:26,60 --> 00:00:29,80
and then we will also include our MyReads application

10
00:00:29,80 --> 00:00:32,90
that I presented previously into our code

11
00:00:32,90 --> 00:00:35,10
and deal with all the issues that will arise

12
00:00:35,10 --> 00:00:38,80
from including the React, the React application

13
00:00:38,80 --> 00:00:42,00
into our project.

14
00:00:42,00 --> 00:00:45,10
What we will learn in this section, we'll cover a few

15
00:00:45,10 --> 00:00:48,00
very important topics whenever working in React projects.

16
00:00:48,00 --> 00:00:52,70
First of all, we will look at installing Babel and React.

17
00:00:52,70 --> 00:00:54,30
We will go over what Babel is

18
00:00:54,30 --> 00:00:56,50
in the first video of this section.

19
00:00:56,50 --> 00:00:59,40
Then we will have a look at how to configure Webpack

20
00:00:59,40 --> 00:01:01,30
to handle the assets in our project.

21
00:01:01,30 --> 00:01:06,30
This includes JavaScript, CSS files, SES files,

22
00:01:06,30 --> 00:01:07,60
and everything like that

23
00:01:07,60 --> 00:01:09,80
and also images and any other assets

24
00:01:09,80 --> 00:01:11,60
that you might need in your project.

25
00:01:11,60 --> 00:01:13,90
Then after that, we will look at adding

26
00:01:13,90 --> 00:01:18,70
the MyReads application code into our project

27
00:01:18,70 --> 00:01:23,50
and how to get it up and running into a browser.

28
00:01:23,50 --> 00:01:25,60
Then after that, we will discuss a bit

29
00:01:25,60 --> 00:01:27,60
about performance boost with Webpack

30
00:01:27,60 --> 00:01:29,30
and as I mentioned, if you remembered

31
00:01:29,30 --> 00:01:32,40
when I mentioned loading few files,

32
00:01:32,40 --> 00:01:34,70
it's not always best to just load one file,

33
00:01:34,70 --> 00:01:36,50
so we will talk about code splitting

34
00:01:36,50 --> 00:01:39,30
and why we like to do that.

35
00:01:39,30 --> 00:01:41,80
And after that, we will also talk about cache busting

36
00:01:41,80 --> 00:01:44,20
in perfect performance using that.

37
00:01:44,20 --> 00:01:46,00
And towards the end, we'll have a look

38
00:01:46,00 --> 00:01:48,60
at creating subscripts that clean up your art projects

39
00:01:48,60 --> 00:01:51,50
and prepare it for deployment.

40
00:01:51,50 --> 00:01:52,90
In the first video of this section,

41
00:01:52,90 --> 00:01:56,20
we will be setting up Babel and React for our projects

42
00:01:56,20 --> 00:02:00,90
so that we can get React code into our application.

43
00:02:00,90 --> 00:02:02,90
In this, we will cover the following topics.

44
00:02:02,90 --> 00:02:05,30
We will install Babel, we will configure Babel,

45
00:02:05,30 --> 00:02:07,40
we will add React to our project,

46
00:02:07,40 --> 00:02:09,80
and then we will expand the basic application

47
00:02:09,80 --> 00:02:12,40
that we've built in the last video

48
00:02:12,40 --> 00:02:16,10
to include React and let us output something

49
00:02:16,10 --> 00:02:18,80
to the screen using React.

50
00:02:18,80 --> 00:02:20,40
If you remember, in the previous video,

51
00:02:20,40 --> 00:02:23,40
we've created this very, very simple (mumbles) app,

52
00:02:23,40 --> 00:02:26,30
which does nothing, just outputs something in the console

53
00:02:26,30 --> 00:02:27,50
whenever we run it.

54
00:02:27,50 --> 00:02:30,00
So let's get to work on this.

55
00:02:30,00 --> 00:02:33,00
Let's create an actual application for us to use.

56
00:02:33,00 --> 00:02:36,70
To do that, we will go back into our code editor.

57
00:02:36,70 --> 00:02:40,40
We'll go into our webpack.config.js,

58
00:02:40,40 --> 00:02:42,40
and we will make a few tweaks here and there

59
00:02:42,40 --> 00:02:45,40
just to make everything look a bit nicer

60
00:02:45,40 --> 00:02:47,10
and run a bit smoother.

61
00:02:47,10 --> 00:02:51,20
So let's comment the existing lines,

62
00:02:51,20 --> 00:02:53,80
and let us create a new one,

63
00:02:53,80 --> 00:02:54,90
so a new conflict file, (keyboard clicks)

64
00:02:54,90 --> 00:02:58,50
so we'll do var, we'll call webpack,

65
00:02:58,50 --> 00:03:01,70
and we will require Webpack

66
00:03:01,70 --> 00:03:03,50
that we have installed last time,

67
00:03:03,50 --> 00:03:09,10
webpack, we will also require the path,

68
00:03:09,10 --> 00:03:12,60
which we will use in order to define where every,

69
00:03:12,60 --> 00:03:14,30
where the build directories are going,

70
00:03:14,30 --> 00:03:19,50
so we will require path in this case.

71
00:03:19,50 --> 00:03:22,50
And we will also set some variables for our build directory

72
00:03:22,50 --> 00:03:24,00
and app directory, so this way,

73
00:03:24,00 --> 00:03:26,70
whenever you want to update your project

74
00:03:26,70 --> 00:03:28,10
or restructure your project,

75
00:03:28,10 --> 00:03:29,50
you don't have to do everywhere,

76
00:03:29,50 --> 00:03:31,60
you can just do it in those variables at the top

77
00:03:31,60 --> 00:03:33,50
and then just reuse them after.

78
00:03:33,50 --> 00:03:36,20
So we will call this our build directory,

79
00:03:36,20 --> 00:03:40,90
and this one will be path and then .resolve,

80
00:03:40,90 --> 00:03:45,30
and we will use their name, which is the current directory,

81
00:03:45,30 --> 00:03:50,10
and then we will also add the build directory,

82
00:03:50,10 --> 00:03:53,70
which we mentioned will be our, dist folder.

83
00:03:53,70 --> 00:03:58,70
So this build directory will be in our dir.

84
00:03:58,70 --> 00:04:02,60
Also, we will like to path join instead of resolve,

85
00:04:02,60 --> 00:04:05,70
so we will use join here.

86
00:04:05,70 --> 00:04:08,50
And then, we will also set another variable

87
00:04:08,50 --> 00:04:12,90
that we will use for the actual code that we're writing.

88
00:04:12,90 --> 00:04:16,90
And this is going to be called app dir.

89
00:04:16,90 --> 00:04:22,00
So do var, and this is going to be our code directory,

90
00:04:22,00 --> 00:04:27,30
and this will be path.join, and in this case,

91
00:04:27,30 --> 00:04:31,20
we will use dirname again.

92
00:04:31,20 --> 00:04:33,60
This is dist here, okay.

93
00:04:33,60 --> 00:04:41,60
And this will be the src folder that we have, src.

94
00:04:41,60 --> 00:04:45,20
Once we're done with this, let's create a conflict variable.

95
00:04:45,20 --> 00:04:46,50
Again, this is not necessary.

96
00:04:46,50 --> 00:04:48,60
You can just export everything by default,

97
00:04:48,60 --> 00:04:51,70
however, I do like creating those variables

98
00:04:51,70 --> 00:04:54,70
so that we can tweak them easily.

99
00:04:54,70 --> 00:04:57,00
We'll do a config and in here,

100
00:04:57,00 --> 00:05:00,50
we will basically just transcribe everything we have about,

101
00:05:00,50 --> 00:05:04,50
so basically, our entry point, and then our project output.

102
00:05:04,50 --> 00:05:08,10
So in this case, let's do again entry,

103
00:05:08,10 --> 00:05:12,90
and our entry will be the app directory,

104
00:05:12,90 --> 00:05:16,70
plus we called our application in source,

105
00:05:16,70 --> 00:05:22,30
we called it app.js, app.js,

106
00:05:22,30 --> 00:05:26,10
and then let's also create an output for,

107
00:05:26,10 --> 00:05:27,30
don't forget the slash here,

108
00:05:27,30 --> 00:05:30,50
let's also create the output,

109
00:05:30,50 --> 00:05:34,30
which will be the build directory as you can see,

110
00:05:34,30 --> 00:05:39,50
and then, in here, we need to set an object actually.

111
00:05:39,50 --> 00:05:42,00
We'll set the object, and then,

112
00:05:42,00 --> 00:05:47,50
we will do the path here, which is the build directory,

113
00:05:47,50 --> 00:05:56,00
and then the file name, which will be the app.bundle.js.

114
00:05:56,00 --> 00:05:58,50
Now, we will save this new Webpack configuration,

115
00:05:58,50 --> 00:06:00,50
also export the conflict file of course,

116
00:06:00,50 --> 00:06:02,10
so we will export it as well.

117
00:06:02,10 --> 00:06:06,60
So we will do module.exports.

118
00:06:06,60 --> 00:06:12,10
And we will make sure we export our config variable

119
00:06:12,10 --> 00:06:13,60
that we've declared.

120
00:06:13,60 --> 00:06:16,90
Now let's run this into the terminal and see what happens.

121
00:06:16,90 --> 00:06:20,00
If you remember last time, we added Webpack globally,

122
00:06:20,00 --> 00:06:21,50
however, in this case,

123
00:06:21,50 --> 00:06:24,70
we are specifically requiring Webpack into this project,

124
00:06:24,70 --> 00:06:27,60
so we will need to install Webpack locally as well.

125
00:06:27,60 --> 00:06:31,10
So to do that, we will just do yarn add,

126
00:06:31,10 --> 00:06:33,30
and then we will do Webpack,

127
00:06:33,30 --> 00:06:37,00
and make sure you do the version 2.6.1

128
00:06:37,00 --> 00:06:41,10
and then the flag -D, so that it installs it

129
00:06:41,10 --> 00:06:44,70
as a development dependency.

130
00:06:44,70 --> 00:06:47,80
And hopefully, this will install everything

131
00:06:47,80 --> 00:06:50,60
that you need to run it.

132
00:06:50,60 --> 00:06:55,50
And now, we will do Webpack again, run it.

133
00:06:55,50 --> 00:06:58,80
As you can see, it says invalid configuration object.

134
00:06:58,80 --> 00:07:01,30
The provided value is an absolute path.

135
00:07:01,30 --> 00:07:04,90
We need to use the output path to specifically,

136
00:07:04,90 --> 00:07:06,80
to specify absolute pathways.

137
00:07:06,80 --> 00:07:08,60
So we'll have to go back into our config

138
00:07:08,60 --> 00:07:09,80
and fix this issue.

139
00:07:09,80 --> 00:07:13,90
Make sure whenever you are creating Webpack configurations,

140
00:07:13,90 --> 00:07:18,50
make sure to run relative files and not absolute files.

141
00:07:18,50 --> 00:07:20,40
So in order to do that, we'll just go back

142
00:07:20,40 --> 00:07:24,30
into our Webpack config and remove the slash

143
00:07:24,30 --> 00:07:25,90
at the beginning of app.bundle.js

144
00:07:25,90 --> 00:07:27,50
so it's no longer an absolute path.

145
00:07:27,50 --> 00:07:30,10
And then when we go back into our terminal,

146
00:07:30,10 --> 00:07:33,00
we will run Webpack again, and as you can see,

147
00:07:33,00 --> 00:07:35,40
it has built our app.bundle.js,

148
00:07:35,40 --> 00:07:38,20
and it has added it in here.

149
00:07:38,20 --> 00:07:41,90
So now that we've installed Webpack into our project,

150
00:07:41,90 --> 00:07:45,70
it will also show up into our package json.

151
00:07:45,70 --> 00:07:48,30
So as you can see, we have a new object here

152
00:07:48,30 --> 00:07:53,20
called devDependencies, which is Webpack 2.6.1.

153
00:07:53,20 --> 00:07:55,20
One thing that I would recommend doing at this point

154
00:07:55,20 --> 00:07:59,00
is editing our package json and creating a scripts object

155
00:07:59,00 --> 00:08:01,20
that will allow us to run Webpack much easier

156
00:08:01,20 --> 00:08:03,80
than running it manually every time in the terminal,

157
00:08:03,80 --> 00:08:06,20
and we will expand upon those scripts

158
00:08:06,20 --> 00:08:08,00
further on during this title.

159
00:08:08,00 --> 00:08:10,90
Let's add the script files in now.

160
00:08:10,90 --> 00:08:15,80
We'll create a scripts, and this one will be an object.

161
00:08:15,80 --> 00:08:18,60
Don't forget to put the training comma at the end.

162
00:08:18,60 --> 00:08:21,40
And we will call it, let's just call it dev,

163
00:08:21,40 --> 00:08:23,00
'cause we are developing,

164
00:08:23,00 --> 00:08:27,50
and this dev script will run Webpack.

165
00:08:27,50 --> 00:08:30,70
And then you can also add the flag -D,

166
00:08:30,70 --> 00:08:33,10
so Webpack runs in development mode

167
00:08:33,10 --> 00:08:34,50
and doesn't spend a lot of time

168
00:08:34,50 --> 00:08:36,30
trying to compile everything down.

169
00:08:36,30 --> 00:08:41,30
So at least as long as we're developing our project.

170
00:08:41,30 --> 00:08:43,30
So let's save this.

171
00:08:43,30 --> 00:08:45,60
And let's go back into our terminal and run this script

172
00:08:45,60 --> 00:08:48,00
and see what happens.

