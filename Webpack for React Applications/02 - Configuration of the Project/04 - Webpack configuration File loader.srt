1
00:00:01,40 --> 00:00:02,80
- [Instructor] In the first part of this video,

2
00:00:02,80 --> 00:00:07,10
we have looked at installing and adding the css-loader,

3
00:00:07,10 --> 00:00:09,50
as well as the style-loader into our webpack config,

4
00:00:09,50 --> 00:00:12,80
so now, we can include CSS into our project.

5
00:00:12,80 --> 00:00:14,60
The next thing that is very important

6
00:00:14,60 --> 00:00:16,40
for all webpack applications

7
00:00:16,40 --> 00:00:19,80
is our images and other type of files,

8
00:00:19,80 --> 00:00:22,70
so let's have a look at how we can get images

9
00:00:22,70 --> 00:00:25,10
into our project.

10
00:00:25,10 --> 00:00:27,60
So, we'll just recommend downloading

11
00:00:27,60 --> 00:00:29,10
any type of image off the internet.

12
00:00:29,10 --> 00:00:32,40
So, whatever you want, and adding it into our project.

13
00:00:32,40 --> 00:00:36,50
I happen to have a React logo on my desktop,

14
00:00:36,50 --> 00:00:42,40
so let's just copy that React logo into our project

15
00:00:42,40 --> 00:00:46,40
and let's try and include it into our application.

16
00:00:46,40 --> 00:00:49,40
So, as you can see, I have added the React logo in here.

17
00:00:49,40 --> 00:00:53,40
Let's say I want the background not to be a color

18
00:00:53,40 --> 00:00:56,00
but I want the background to be an image,

19
00:00:56,00 --> 00:00:58,80
so let's add that into our CSS.

20
00:00:58,80 --> 00:01:01,90
I will do background-image

21
00:01:01,90 --> 00:01:07,60
and in here, we will just include our React logo,

22
00:01:07,60 --> 00:01:10,70
react-logo.png.

23
00:01:10,70 --> 00:01:14,20
Once we save this, we will go back into our terminal

24
00:01:14,20 --> 00:01:18,30
and oops, as you can see, there has been an error.

25
00:01:18,30 --> 00:01:21,00
Webpack is telling me that the parse failed

26
00:01:21,00 --> 00:01:22,40
and that it does not know

27
00:01:22,40 --> 00:01:25,40
what to do with our react-logo.png.

28
00:01:25,40 --> 00:01:27,70
It says it might need an appropriate loader

29
00:01:27,70 --> 00:01:29,00
to handle this file type.

30
00:01:29,00 --> 00:01:31,90
So yes, as you can guess, there is a loader for images

31
00:01:31,90 --> 00:01:33,80
and there is a loader for other types of files

32
00:01:33,80 --> 00:01:35,00
and we need to make sure

33
00:01:35,00 --> 00:01:37,80
that is included into our webpack config

34
00:01:37,80 --> 00:01:40,50
because otherwise, webpack will not know

35
00:01:40,50 --> 00:01:43,90
what to do with those images, so it will just ignore them.

36
00:01:43,90 --> 00:01:49,00
So, let's cancel the build and let us install the,

37
00:01:49,00 --> 00:01:50,70
if you remember, we installed the file loader,

38
00:01:50,70 --> 00:01:54,70
so let's add it into our webpack_config.js.

39
00:01:54,70 --> 00:01:58,30
So, going back into webpack_config.js,

40
00:01:58,30 --> 00:02:00,10
we will create another rule.

41
00:02:00,10 --> 00:02:02,00
So, I think you're getting the picture of it.

42
00:02:02,00 --> 00:02:03,50
Every time we need to create another rule,

43
00:02:03,50 --> 00:02:05,80
you just add it into this array.

44
00:02:05,80 --> 00:02:09,30
And in this case, we will be using the file loader

45
00:02:09,30 --> 00:02:12,70
in order to test for any type of images.

46
00:02:12,70 --> 00:02:15,80
So, the test will be a bit more complicated in this case

47
00:02:15,80 --> 00:02:18,70
because we want to make sure we cover all the images.

48
00:02:18,70 --> 00:02:22,70
What I recommend you do is just use this code.

49
00:02:22,70 --> 00:02:28,20
So, we want to do any JPEG images

50
00:02:28,20 --> 00:02:30,90
or we want to do any PNG images

51
00:02:30,90 --> 00:02:33,20
or we want to do any GIFs.

52
00:02:33,20 --> 00:02:34,50
Who doesn't like GIFs?

53
00:02:34,50 --> 00:02:36,90
And we'll also make sure

54
00:02:36,90 --> 00:02:41,00
that we're also adding the SVG files,

55
00:02:41,00 --> 00:02:42,60
which are vector files.

56
00:02:42,60 --> 00:02:44,70
We will close the type.

57
00:02:44,70 --> 00:02:50,00
And now, we will use the following loader

58
00:02:50,00 --> 00:02:53,70
which is the file-loader.

59
00:02:53,70 --> 00:02:55,80
This is supposed to be a comma here.

60
00:02:55,80 --> 00:02:58,50
And now, let us go back into our terminal

61
00:02:58,50 --> 00:03:02,00
and run our build script.

62
00:03:02,00 --> 00:03:03,70
Keep in mind, whenever you're changing

63
00:03:03,70 --> 00:03:08,20
the webpack config or whenever you're installing new things,

64
00:03:08,20 --> 00:03:12,00
you need to make sure that you are closing the build script

65
00:03:12,00 --> 00:03:13,40
and then running it again.

66
00:03:13,40 --> 00:03:15,10
So, let's refresh our browser

67
00:03:15,10 --> 00:03:18,50
and of course, that was exactly what I expected to happen,

68
00:03:18,50 --> 00:03:20,90
but at least at this point, as you can see,

69
00:03:20,90 --> 00:03:25,10
the images are being loaded into your project.

70
00:03:25,10 --> 00:03:27,70
So, keep in mind, if you want images in the project,

71
00:03:27,70 --> 00:03:30,50
you will need to use the file-loader.

72
00:03:30,50 --> 00:03:34,50
Let's set the background to be no-repeat

73
00:03:34,50 --> 00:03:36,20
just so we can see it better.

74
00:03:36,20 --> 00:03:38,80
We'll refresh the page, and there we go,

75
00:03:38,80 --> 00:03:44,30
we have our image loaded inside our project.

76
00:03:44,30 --> 00:03:46,70
One thing that you might also want to do

77
00:03:46,70 --> 00:03:49,50
is instead of working with plain CSS files,

78
00:03:49,50 --> 00:03:51,50
working with SCSS.

79
00:03:51,50 --> 00:03:52,90
Of course, as you can imagine,

80
00:03:52,90 --> 00:03:57,00
there is a loader for that, which is called sass-loader.

81
00:03:57,00 --> 00:04:00,50
So, we will install the sass-loader,

82
00:04:00,50 --> 00:04:03,70
which will allow us to use SCSS in our projects.

83
00:04:03,70 --> 00:04:07,50
So, let's go into our terminal, close the build process,

84
00:04:07,50 --> 00:04:11,00
and then install our sass-loader.

85
00:04:11,00 --> 00:04:13,00
So, we will do yarn add

86
00:04:13,00 --> 00:04:16,60
and then we will do sass-loader.

87
00:04:16,60 --> 00:04:22,60
And we will also need to install node-sass.

88
00:04:22,60 --> 00:04:24,40
This is a dependency of the sass-loader

89
00:04:24,40 --> 00:04:27,00
and without this, it will not work.

90
00:04:27,00 --> 00:04:27,80
And then again,

91
00:04:27,80 --> 00:04:33,00
make sure you install this as a dev dependency.

92
00:04:33,00 --> 00:04:34,40
So, once we've done that,

93
00:04:34,40 --> 00:04:38,40
we will also need to add it into our webpack_config.js.

94
00:04:38,40 --> 00:04:41,00
It just might take a little bit of time

95
00:04:41,00 --> 00:04:43,30
to install everything that's needed.

96
00:04:43,30 --> 00:04:45,60
Usually, my internet connection is a bit better,

97
00:04:45,60 --> 00:04:49,10
but today, it's being characteristically slow.

98
00:04:49,10 --> 00:04:52,00
So, just waiting for everything to install.

99
00:04:52,00 --> 00:04:54,00
And in the meantime, while it's installing,

100
00:04:54,00 --> 00:04:56,10
let's go into our webpack config

101
00:04:56,10 --> 00:05:01,60
and make sure webpack knows what to do with Sass files.

102
00:05:01,60 --> 00:05:05,50
So, we will create a new test, of course.

103
00:05:05,50 --> 00:05:09,80
I would recommend you do this right after the CSS.

104
00:05:09,80 --> 00:05:11,40
That is just so you have everything

105
00:05:11,40 --> 00:05:14,20
that's related to styles in the same place.

106
00:05:14,20 --> 00:05:17,20
So, we will be testing for SCSS

107
00:05:17,20 --> 00:05:20,60
and then again, we want to use the sass-loader

108
00:05:20,60 --> 00:05:22,30
in order to compile the CSS.

109
00:05:22,30 --> 00:05:24,30
Then we want to use the css-loader

110
00:05:24,30 --> 00:05:27,60
in order to add the CSS into our bundle.

111
00:05:27,60 --> 00:05:30,50
And then we also want to use the style-loader

112
00:05:30,50 --> 00:05:35,60
to output that CSS into the head of our index file.

113
00:05:35,60 --> 00:05:38,90
Keep in mind that whenever a loader

114
00:05:38,90 --> 00:05:40,70
is placed in the array, the last loader,

115
00:05:40,70 --> 00:05:43,60
the last element in the array, will be processed first.

116
00:05:43,60 --> 00:05:46,50
So, in this case, we want to do the sass-loader

117
00:05:46,50 --> 00:05:48,60
and then pipe it through the CSS

118
00:05:48,60 --> 00:05:50,20
and then the style-loader.

119
00:05:50,20 --> 00:05:54,00
So, as you can see, it's still adding the packages.

120
00:05:54,00 --> 00:05:55,90
Once that is done, we will have a look

121
00:05:55,90 --> 00:05:58,90
at how to create the SCSS files and add them to our project.

122
00:05:58,90 --> 00:06:01,60
Okay, it took a bit of time to finish downloading.

123
00:06:01,60 --> 00:06:04,50
Hopefully, in your case, it will not take this long

124
00:06:04,50 --> 00:06:07,50
and you'll just be able to download them and install them.

125
00:06:07,50 --> 00:06:09,50
So as you can see, we now have node-sass

126
00:06:09,50 --> 00:06:12,20
and we also have our sass-loader.

127
00:06:12,20 --> 00:06:14,30
So at this point, we should be good to go

128
00:06:14,30 --> 00:06:16,50
to include SCSS files.

129
00:06:16,50 --> 00:06:18,90
So in order to do that, I will just update our app.css.

130
00:06:18,90 --> 00:06:21,60
And instead of having it as app.cs,

131
00:06:21,60 --> 00:06:24,70
I will rename it to app.scss.

132
00:06:24,70 --> 00:06:29,20
So, let's do that, rename, and app.scss.

133
00:06:29,20 --> 00:06:33,50
Let's run the build script again.

134
00:06:33,50 --> 00:06:35,60
And hopefully it will say,

135
00:06:35,60 --> 00:06:37,20
it cannot resolve app.scc.

136
00:06:37,20 --> 00:06:39,10
Of course we need to go into our app.js

137
00:06:39,10 --> 00:06:40,60
and import the SCSS file

138
00:06:40,60 --> 00:06:43,10
because the CSS file does not exist anymore.

139
00:06:43,10 --> 00:06:44,70
And once this is done,

140
00:06:44,70 --> 00:06:46,80
webpack will bundle everything together.

141
00:06:46,80 --> 00:06:49,00
If we refresh our page, as you can see,

142
00:06:49,00 --> 00:06:51,40
this is running off our SCSS.

143
00:06:51,40 --> 00:06:54,50
Just to show you and make sure everything is clear,

144
00:06:54,50 --> 00:06:57,40
we will just add a new rule in here

145
00:06:57,40 --> 00:07:00,40
which is going to be paragraphs.

146
00:07:00,40 --> 00:07:03,60
And then we will do a font-size for this.

147
00:07:03,60 --> 00:07:04,70
Just say 50 pixels.

148
00:07:04,70 --> 00:07:07,80
We go back in here, when we refresh, there we go.

149
00:07:07,80 --> 00:07:09,70
So now, we are running our projects

150
00:07:09,70 --> 00:07:12,90
using SCSS and adding all the images

151
00:07:12,90 --> 00:07:16,70
and all the other assets that we need into them.

152
00:07:16,70 --> 00:07:20,00
So, we have everything we need in our webpack_config.js.

153
00:07:20,00 --> 00:07:22,90
There are just a few more things that we need to talk about

154
00:07:22,90 --> 00:07:27,40
and mainly, we will talk about how we can improve

155
00:07:27,40 --> 00:07:29,40
the process of building our files

156
00:07:29,40 --> 00:07:33,30
and how we can make it work a bit better.

157
00:07:33,30 --> 00:07:37,60
And for that, we'll start adding in the MyReads code

158
00:07:37,60 --> 00:07:40,40
and then dealing with problems as they come along.

159
00:07:40,40 --> 00:07:41,40
So, I will see you

160
00:07:41,40 --> 00:07:46,00
in the next part of this video with that process.

