1
00:00:00,90 --> 00:00:02,60
- [Instructor] So, in the previous two parts,

2
00:00:02,60 --> 00:00:04,60
we had to look at all the loaders

3
00:00:04,60 --> 00:00:07,20
that we need to add into our projects

4
00:00:07,20 --> 00:00:09,40
in order to be able to add in the code

5
00:00:09,40 --> 00:00:11,70
from our MyReads application.

6
00:00:11,70 --> 00:00:13,10
I will include the link to a zip

7
00:00:13,10 --> 00:00:15,60
containing the code for MyReads,

8
00:00:15,60 --> 00:00:18,80
and we do the code additions together,

9
00:00:18,80 --> 00:00:21,20
and move the code from the MyReads app

10
00:00:21,20 --> 00:00:23,60
into our basic application folder

11
00:00:23,60 --> 00:00:27,50
and overwrite everything that needs to be overwritten.

12
00:00:27,50 --> 00:00:30,60
Once you unzip the MyReads location,

13
00:00:30,60 --> 00:00:32,10
you will see a folder which has

14
00:00:32,10 --> 00:00:36,00
a very similar structure to our basic app folder.

15
00:00:36,00 --> 00:00:39,40
So again, you will see, in both of them,

16
00:00:39,40 --> 00:00:41,80
the dist folders, the note modules folder.

17
00:00:41,80 --> 00:00:45,00
We'll also get rid of the package json

18
00:00:45,00 --> 00:00:48,00
and a webpack config js file.

19
00:00:48,00 --> 00:00:49,80
As well as, I'll be removing

20
00:00:49,80 --> 00:00:53,40
the webpack config js from the MyReads,

21
00:00:53,40 --> 00:00:55,50
because that's what we're trying to build together.

22
00:00:55,50 --> 00:00:58,50
So you'll not have access to this one.

23
00:00:58,50 --> 00:01:01,20
We'll just need to do this during the title.

24
00:01:01,20 --> 00:01:03,70
And then, of course, once the video is over,

25
00:01:03,70 --> 00:01:05,30
you'll get access to the fully completed

26
00:01:05,30 --> 00:01:09,30
webpack config json film if you want to reference it.

27
00:01:09,30 --> 00:01:12,20
Now in the meantime, let's start by copying over

28
00:01:12,20 --> 00:01:16,10
our source directory from MyReads to our basic app.

29
00:01:16,10 --> 00:01:17,60
So let's copy that.

30
00:01:17,60 --> 00:01:21,50
We will paste it on top and make sure the merge the files.

31
00:01:21,50 --> 00:01:23,80
And now, into our source in the basic app,

32
00:01:23,80 --> 00:01:25,80
we will have a few folders.

33
00:01:25,80 --> 00:01:27,40
One of them is called components,

34
00:01:27,40 --> 00:01:29,40
in which we keep all of our

35
00:01:29,40 --> 00:01:31,80
React components that we're using.

36
00:01:31,80 --> 00:01:34,00
So if you remember back from the time

37
00:01:34,00 --> 00:01:36,30
when we looked at the MyReads application,

38
00:01:36,30 --> 00:01:38,90
those are all the components that are being used.

39
00:01:38,90 --> 00:01:40,80
They're all React components.

40
00:01:40,80 --> 00:01:44,60
And you can also see we have some extra files in there

41
00:01:44,60 --> 00:01:47,50
that we can use for developing the application further.

42
00:01:47,50 --> 00:01:50,60
Again, this is just a template for our app.

43
00:01:50,60 --> 00:01:53,80
You can develop it and change and make it more advanced

44
00:01:53,80 --> 00:01:57,30
whenever you want by using React.

45
00:01:57,30 --> 00:01:59,60
However, this title is more focused

46
00:01:59,60 --> 00:02:01,90
around how to use webpack with it,

47
00:02:01,90 --> 00:02:05,40
and we will not focus on the React part too much.

48
00:02:05,40 --> 00:02:07,50
So as you can see, we have an index js,

49
00:02:07,50 --> 00:02:09,20
which contains our app.

50
00:02:09,20 --> 00:02:11,80
Then we also have our apps in here,

51
00:02:11,80 --> 00:02:14,00
and all the components that we need to use.

52
00:02:14,00 --> 00:02:16,50
So let's start by, first of all,

53
00:02:16,50 --> 00:02:19,40
going into the MyReads package dot json

54
00:02:19,40 --> 00:02:22,70
and making sure we have all the dependencies installed.

55
00:02:22,70 --> 00:02:24,90
When you're migrating between apps,

56
00:02:24,90 --> 00:02:27,90
what you can do is basically just go into

57
00:02:27,90 --> 00:02:30,00
the all package json that you need to migrate from,

58
00:02:30,00 --> 00:02:33,10
make sure you copy all the dependencies in here,

59
00:02:33,10 --> 00:02:35,40
so we will copy both the regular dependencies

60
00:02:35,40 --> 00:02:37,40
and the development dependencies,

61
00:02:37,40 --> 00:02:42,00
and we will add them to our basic app package dot json.

62
00:02:42,00 --> 00:02:44,70
So we will be replacing our current dependencies

63
00:02:44,70 --> 00:02:47,90
with the dependencies from the MyReads app.

64
00:02:47,90 --> 00:02:50,50
We'll save this and we're going to return to the terminal,

65
00:02:50,50 --> 00:02:52,50
cancel the current build,

66
00:02:52,50 --> 00:02:54,80
and then just run yarn in the folder.

67
00:02:54,80 --> 00:02:56,40
This will install everything that's

68
00:02:56,40 --> 00:02:58,40
needed for the app to run.

69
00:02:58,40 --> 00:03:01,10
And then let's just run our dev script as well.

70
00:03:01,10 --> 00:03:03,50
And see, there are a few issues with this.

71
00:03:03,50 --> 00:03:05,80
It says it could not find some presets,

72
00:03:05,80 --> 00:03:09,40
so let's work on fixing those now.

73
00:03:09,40 --> 00:03:11,40
As you can see, this error is mostly caused

74
00:03:11,40 --> 00:03:14,50
by the presents we were using for Babel.

75
00:03:14,50 --> 00:03:18,60
And so, one of the things that I did with the basic app,

76
00:03:18,60 --> 00:03:22,00
I changed the Babel RC dependency.

77
00:03:22,00 --> 00:03:23,80
So let's have a look into the Babel RC

78
00:03:23,80 --> 00:03:25,90
and see what we're using.

79
00:03:25,90 --> 00:03:29,10
So, as you can see, it was using Babel and React stage two

80
00:03:29,10 --> 00:03:33,10
instead of just the Babel and the preset ES2015

81
00:03:33,10 --> 00:03:35,70
and React that we were using before.

82
00:03:35,70 --> 00:03:38,50
So let's make sure we copy those as well

83
00:03:38,50 --> 00:03:41,90
from our MyReads to our Babel RC here.

84
00:03:41,90 --> 00:03:44,00
So we'll make those changes

85
00:03:44,00 --> 00:03:47,00
and update them to the correct configuration again.

86
00:03:47,00 --> 00:03:47,90
And that's run.

87
00:03:47,90 --> 00:03:50,50
As you can see, another issue that shows up

88
00:03:50,50 --> 00:03:52,80
is it cannot resolve the sass loader.

89
00:03:52,80 --> 00:03:57,10
So we will need to reinstall the sass loader as well.

90
00:03:57,10 --> 00:03:59,20
I will do node sass first,

91
00:03:59,20 --> 00:04:02,40
and then we will install the sass loader as well.

92
00:04:02,40 --> 00:04:04,90
Hopefully it doesn't take that long this time.

93
00:04:04,90 --> 00:04:08,70
And let's just do yarn, sass loader.

94
00:04:08,70 --> 00:04:10,50
Hopefully at this point, the app should be

95
00:04:10,50 --> 00:04:12,70
able to compile correctly and bundle everything

96
00:04:12,70 --> 00:04:17,70
to get the bundle correctly.

97
00:04:17,70 --> 00:04:20,40
So, as you can see, we have created our bundle.

98
00:04:20,40 --> 00:04:22,30
If you look at the webpack output,

99
00:04:22,30 --> 00:04:25,30
you see we have our app dot bundle dot js,

100
00:04:25,30 --> 00:04:29,10
which is 2.9 megabytes, which is quite a large file.

101
00:04:29,10 --> 00:04:31,40
But keep in mind, this is a large application.

102
00:04:31,40 --> 00:04:32,40
It includes React.

103
00:04:32,40 --> 00:04:34,50
It also includes a few other dependencies,

104
00:04:34,50 --> 00:04:36,20
such as the React router,

105
00:04:36,20 --> 00:04:39,50
which are very large files that need to be included.

106
00:04:39,50 --> 00:04:41,90
One thing that, if we go into our app

107
00:04:41,90 --> 00:04:45,50
and refresh, we will not see any changes in the browser,

108
00:04:45,50 --> 00:04:48,60
because we need to include this new bundle into our index

109
00:04:48,60 --> 00:04:50,80
and make sure it runs in the browser

110
00:04:50,80 --> 00:04:54,30
before we can go on and talk about performance.

111
00:04:54,30 --> 00:04:56,70
So let's get the app added.

112
00:04:56,70 --> 00:05:00,30
We will need to change our webpack config a little bit,

113
00:05:00,30 --> 00:05:02,60
just to make sure we're including the correct files.

114
00:05:02,60 --> 00:05:06,30
So, at this point, before we were using

115
00:05:06,30 --> 00:05:10,50
the app dot js as an input with the MyReads,

116
00:05:10,50 --> 00:05:13,00
I have used the index dot js.

117
00:05:13,00 --> 00:05:16,50
So we will make sure we change this to index dot js.

118
00:05:16,50 --> 00:05:19,70
We want the output to be the same as before,

119
00:05:19,70 --> 00:05:23,40
so that will go into app bundle js.

120
00:05:23,40 --> 00:05:26,50
And hopefully, now, if we run the build script again,

121
00:05:26,50 --> 00:05:28,00
it should bundle the app together

122
00:05:28,00 --> 00:05:30,80
and we should be able to preview it in a browser.

123
00:05:30,80 --> 00:05:33,20
So let's go to the browser, refresh,

124
00:05:33,20 --> 00:05:35,70
and as you can see, the bench has changed.

125
00:05:35,70 --> 00:05:38,30
However, we do not have a lot of things happening.

126
00:05:38,30 --> 00:05:41,60
This is because we're using webpack router,

127
00:05:41,60 --> 00:05:43,90
which needs an http server to work on.

128
00:05:43,90 --> 00:05:47,20
Because otherwise, it does not know what routes to render.

129
00:05:47,20 --> 00:05:51,10
So let's make sure we install an http server as well,

130
00:05:51,10 --> 00:05:53,70
and try to run the application from there.

131
00:05:53,70 --> 00:05:56,20
So let us go back to our terminal

132
00:05:56,20 --> 00:05:59,30
and install a basic node http server.

133
00:05:59,30 --> 00:06:01,90
We will install this as a dev dependency again.

134
00:06:01,90 --> 00:06:04,80
And, for your information, the server

135
00:06:04,80 --> 00:06:08,20
is simply called http server.

136
00:06:08,20 --> 00:06:11,00
So we'll do yarn, add http server,

137
00:06:11,00 --> 00:06:14,30
and we will also add this as a dev dependency.

138
00:06:14,30 --> 00:06:16,10
So let's get this in.

139
00:06:16,10 --> 00:06:20,10
And we're also going to our package dot json

140
00:06:20,10 --> 00:06:23,00
and create the script that runs our http server

141
00:06:23,00 --> 00:06:25,60
every time we want it.

142
00:06:25,60 --> 00:06:28,90
Let's go into our package, into the basic app.

143
00:06:28,90 --> 00:06:32,40
And in there, where we have our scripts,

144
00:06:32,40 --> 00:06:36,10
let's create a new script called http server,

145
00:06:36,10 --> 00:06:38,80
which basically just runs the http server

146
00:06:38,80 --> 00:06:40,60
from the disc directory

147
00:06:40,60 --> 00:06:45,20
where our app bundle js and our index dot js are.

148
00:06:45,20 --> 00:06:48,70
So let's do that, let's run our server from the terminal.

149
00:06:48,70 --> 00:06:53,50
I will do yarn run and our http server.

150
00:06:53,50 --> 00:06:56,70
As you can see, the server has been started,

151
00:06:56,70 --> 00:07:01,20
and it will be watching, going on,

152
00:07:01,20 --> 00:07:03,70
everyone going on this link.

153
00:07:03,70 --> 00:07:05,50
Just paste this in the browser.

154
00:07:05,50 --> 00:07:07,70
And there seems to be a small problem.

155
00:07:07,70 --> 00:07:11,30
It's not loading that dist file.

156
00:07:11,30 --> 00:07:12,70
Let me check quickly.

157
00:07:12,70 --> 00:07:14,20
Seems I forgot to save the file.

158
00:07:14,20 --> 00:07:16,40
So just make sure you save the file before.

159
00:07:16,40 --> 00:07:19,60
So we'll save the file now, and then let's

160
00:07:19,60 --> 00:07:22,00
try and run it in the terminal again.

161
00:07:22,00 --> 00:07:24,20
And as you can see, it has now started

162
00:07:24,20 --> 00:07:27,90
on port 8080 on your local host.

163
00:07:27,90 --> 00:07:30,40
So if we go in here, as you can see,

164
00:07:30,40 --> 00:07:33,90
we still have the hello world from the template.

165
00:07:33,90 --> 00:07:36,80
But as you can see, the MyRead application is loading

166
00:07:36,80 --> 00:07:39,40
and everything is working as it should.

167
00:07:39,40 --> 00:07:43,70
So let's go and clean our index html a bit.

168
00:07:43,70 --> 00:07:45,40
Let's remove this paragraph.

169
00:07:45,40 --> 00:07:48,30
We also have our route div,

170
00:07:48,30 --> 00:07:50,00
which is loading the React app.

171
00:07:50,00 --> 00:07:52,80
And then the app is running on the server.

172
00:07:52,80 --> 00:07:54,60
Now one thing that we have to do now

173
00:07:54,60 --> 00:07:57,40
is change our index dot html file.

174
00:07:57,40 --> 00:08:00,20
We don't want to do this every time we create a new bundle,

175
00:08:00,20 --> 00:08:02,00
so one thing that we can do,

176
00:08:02,00 --> 00:08:05,10
and what webpack allows you to do,

177
00:08:05,10 --> 00:08:10,00
is to add a plugin called html webpack plugin,

178
00:08:10,00 --> 00:08:11,90
which will automatically generate

179
00:08:11,90 --> 00:08:14,40
the index html file so that you can

180
00:08:14,40 --> 00:08:17,00
rely on it always being there.

181
00:08:17,00 --> 00:08:18,70
In order to do that, what we will do

182
00:08:18,70 --> 00:08:21,70
is we will remove the index dot html

183
00:08:21,70 --> 00:08:24,60
from the dist folder so we can delete it from here.

184
00:08:24,60 --> 00:08:28,40
And then we will go into our terminal

185
00:08:28,40 --> 00:08:31,00
and install the html webpack plugin.

186
00:08:31,00 --> 00:08:34,30
So we will install this plugin for webpack

187
00:08:34,30 --> 00:08:37,80
and make sure it's a dev dependency.

188
00:08:37,80 --> 00:08:41,10
Once this installs, we will go into

189
00:08:41,10 --> 00:08:45,60
our webpack config js and set this plugin up.

190
00:08:45,60 --> 00:08:49,20
As we go into our webpack config js,

191
00:08:49,20 --> 00:08:52,90
we can import it now, so we will import the html plugin

192
00:08:52,90 --> 00:08:55,60
the same way we imported webpack.

193
00:08:55,60 --> 00:09:01,40
So we will do the var and then html webpack plugin,

194
00:09:01,40 --> 00:09:04,30
and this will require the html webpack plugin

195
00:09:04,30 --> 00:09:05,90
that we've just installed.

196
00:09:05,90 --> 00:09:07,30
There we go.

197
00:09:07,30 --> 00:09:10,20
So we've added the plugin.

198
00:09:10,20 --> 00:09:12,40
Now we need to also go into our config file

199
00:09:12,40 --> 00:09:15,60
and make a few changes just below the modules object.

200
00:09:15,60 --> 00:09:19,60
We can also create another property called plugins.

201
00:09:19,60 --> 00:09:21,90
So let's add that in, plugins.

202
00:09:21,90 --> 00:09:25,00
And the plugins property will be an array

203
00:09:25,00 --> 00:09:27,40
of plugins that we can use with webpack.

204
00:09:27,40 --> 00:09:31,60
So let's add our webpack html plugin.

205
00:09:31,60 --> 00:09:35,60
So we'll create a new html webpack plugin.

206
00:09:35,60 --> 00:09:38,20
And then this will be a function

207
00:09:38,20 --> 00:09:40,20
to which we can pass some arguments.

208
00:09:40,20 --> 00:09:42,70
And we want to pass it a template

209
00:09:42,70 --> 00:09:47,20
which it will use for generating our index html.

210
00:09:47,20 --> 00:09:50,90
And we'll call this index dot html in this case.

211
00:09:50,90 --> 00:09:53,20
We'll save our webpack file.

212
00:09:53,20 --> 00:09:56,90
Now let's also create our index dot html

213
00:09:56,90 --> 00:09:58,60
in the source directory.

214
00:09:58,60 --> 00:10:02,00
So we'll create a new file called index dot html.

215
00:10:02,00 --> 00:10:05,60
And I'll provide this into the MyReads code,

216
00:10:05,60 --> 00:10:09,80
so you can just copy it from there for now.

217
00:10:09,80 --> 00:10:13,10
We'll copy it from here and add it

218
00:10:13,10 --> 00:10:15,80
into our template in here.

219
00:10:15,80 --> 00:10:18,10
And now we run webpack again,

220
00:10:18,10 --> 00:10:20,20
and we'll see what the script will do.

221
00:10:20,20 --> 00:10:24,50
It should automatically generate a template for us in here.

222
00:10:24,50 --> 00:10:29,90
Let's see, seems we have a problem with the html web plugin.

223
00:10:29,90 --> 00:10:32,70
It says it's not been found.

224
00:10:32,70 --> 00:10:36,30
So let's make sure we've created the index html

225
00:10:36,30 --> 00:10:37,50
in the correct folder.

226
00:10:37,50 --> 00:10:40,00
And yes, as you can see, I've created it

227
00:10:40,00 --> 00:10:41,60
inside the source directory.

228
00:10:41,60 --> 00:10:42,90
So I want to pull this out

229
00:10:42,90 --> 00:10:46,40
and just move it into the basic app folder.

230
00:10:46,40 --> 00:10:49,40
When we run the application, the build process,

231
00:10:49,40 --> 00:10:52,60
we'll just find it and add it into our dist folder.

232
00:10:52,60 --> 00:10:55,10
So as you can see, everything's been compiled,

233
00:10:55,10 --> 00:10:57,20
index html has been generated.

234
00:10:57,20 --> 00:11:00,20
Let's have a quick look at the generated index file.

235
00:11:00,20 --> 00:11:03,10
If we open the index, you'll see it will

236
00:11:03,10 --> 00:11:05,20
automatically add the javascript bundle

237
00:11:05,20 --> 00:11:07,20
that webpack creates to our template.

238
00:11:07,20 --> 00:11:10,10
So this way, whenever you change your file,

239
00:11:10,10 --> 00:11:12,30
if you ever change the name of the bundle,

240
00:11:12,30 --> 00:11:16,60
so for example, you don't want it to be called bundle,

241
00:11:16,60 --> 00:11:21,00
and let's say you wanna change it to just say bundle js

242
00:11:21,00 --> 00:11:24,30
instead of app dot bundle dot js.

243
00:11:24,30 --> 00:11:27,80
When we save this and run the build script again,

244
00:11:27,80 --> 00:11:30,20
it will generate a new index html,

245
00:11:30,20 --> 00:11:33,10
which will include our new bundle dot js.

246
00:11:33,10 --> 00:11:36,70
So as you can see, from the app

247
00:11:36,70 --> 00:11:39,60
dot bundle dot js to just bundle js.

248
00:11:39,60 --> 00:11:43,10
So this plugin is very useful for updating your configs,

249
00:11:43,10 --> 00:11:44,60
updating your file names,

250
00:11:44,60 --> 00:11:47,30
the name of your files or everything like that.

251
00:11:47,30 --> 00:11:49,40
So I always recommend having this

252
00:11:49,40 --> 00:11:53,00
and including it into your webpack configuration.

253
00:11:53,00 --> 00:11:54,40
So in this three part video,

254
00:11:54,40 --> 00:11:56,60
we have gone over the basic webpack configurations

255
00:11:56,60 --> 00:11:59,20
that we will need for our projects.

256
00:11:59,20 --> 00:12:01,30
We have also added all the code

257
00:12:01,30 --> 00:12:03,40
that is required for MyRead apps

258
00:12:03,40 --> 00:12:06,00
to run into our basic application.

259
00:12:06,00 --> 00:12:08,20
Keep in mind, sometimes when you're

260
00:12:08,20 --> 00:12:10,70
working on projects with larger teams,

261
00:12:10,70 --> 00:12:12,60
you might only get the React code,

262
00:12:12,60 --> 00:12:14,70
or you might only get some of the components.

263
00:12:14,70 --> 00:12:16,60
So this is a very good way of making sure

264
00:12:16,60 --> 00:12:19,10
you know how to put those components into

265
00:12:19,10 --> 00:12:22,20
your own project and make sure everything works correctly.

266
00:12:22,20 --> 00:12:23,20
It's been a bit of a longer video,

267
00:12:23,20 --> 00:12:25,30
but if everything in this webpack

268
00:12:25,30 --> 00:12:27,90
configuration is understood, it will be much easier

269
00:12:27,90 --> 00:12:30,80
for you to work with webpack in the future.

270
00:12:30,80 --> 00:12:33,60
In the next video of the section,

271
00:12:33,60 --> 00:12:35,60
we will talk a bit more about performance increase

272
00:12:35,60 --> 00:12:38,00
and how we can make sure that we're serving

273
00:12:38,00 --> 00:12:40,80
the users with good size files

274
00:12:40,80 --> 00:12:43,70
and not sending them two megabytes every time.

275
00:12:43,70 --> 00:12:45,70
If you can remember from the beginning,

276
00:12:45,70 --> 00:12:47,40
the bundle size was quite large

277
00:12:47,40 --> 00:12:51,00
because we're including all of those dependencies.

