1
00:00:00,00 --> 00:00:02,70
- [Instructor] Before we get started working with the

2
00:00:02,70 --> 00:00:05,30
latest browser APIs, let's take a moment

3
00:00:05,30 --> 00:00:08,00
and get our development environment set up.

4
00:00:08,00 --> 00:00:09,80
I'm going to be using visual studio code

5
00:00:09,80 --> 00:00:12,10
as my code editor, but as I said earlier,

6
00:00:12,10 --> 00:00:14,70
you can use whatever code editor you like.

7
00:00:14,70 --> 00:00:16,90
One of the main reasons I really like using VS Code

8
00:00:16,90 --> 00:00:19,40
is that there are some really nice extensions available

9
00:00:19,40 --> 00:00:23,30
for all kinds of development tasks, so if you want to get

10
00:00:23,30 --> 00:00:24,30
your environment set up

11
00:00:24,30 --> 00:00:26,10
like I'll be using it in this course,

12
00:00:26,10 --> 00:00:28,20
then follow along with me here.

13
00:00:28,20 --> 00:00:30,60
First, place your exercise files folder

14
00:00:30,60 --> 00:00:33,20
wherever it's most convenient for you to access.

15
00:00:33,20 --> 00:00:35,40
I've put it on my desktop to make recording easier,

16
00:00:35,40 --> 00:00:37,80
but you can start wherever you want.

17
00:00:37,80 --> 00:00:40,40
Then once you've installed VS Code,

18
00:00:40,40 --> 00:00:47,30
go to the extensions view and type in Live Server

19
00:00:47,30 --> 00:00:50,40
in the search box, and this should produce a list of results

20
00:00:50,40 --> 00:00:53,70
for various extensions that let you preview web pages

21
00:00:53,70 --> 00:00:55,80
in a local development server

22
00:00:55,80 --> 00:00:58,10
which we'll need for some of these APIs.

23
00:00:58,10 --> 00:01:00,40
I've already installed this one called Live Server

24
00:01:00,40 --> 00:01:03,30
which has a lot of installs and is free.

25
00:01:03,30 --> 00:01:05,40
So this extension will make it easier for us

26
00:01:05,40 --> 00:01:08,20
to see our results as we go through the course,

27
00:01:08,20 --> 00:01:10,40
so go ahead and install this.

28
00:01:10,40 --> 00:01:12,60
And I'm also going to be using an online resource

29
00:01:12,60 --> 00:01:16,40
called httpbin.org.

30
00:01:16,40 --> 00:01:20,40
And this is useful for viewing real time data

31
00:01:20,40 --> 00:01:21,80
from a remote server,

32
00:01:21,80 --> 00:01:24,20
and it doesn't require you to set anything up.

33
00:01:24,20 --> 00:01:25,90
But you should spend a few minutes checking it out

34
00:01:25,90 --> 00:01:27,60
and getting familiar with it.

35
00:01:27,60 --> 00:01:30,90
The reason I like httpbin is because it's a great resource

36
00:01:30,90 --> 00:01:32,40
for testing out web apps.

37
00:01:32,40 --> 00:01:35,20
For example, I can generate sample JSON data

38
00:01:35,20 --> 00:01:40,20
just by going to httpbin.org/JSON.

39
00:01:40,20 --> 00:01:43,10
And there you can see, if I choose the raw data,

40
00:01:43,10 --> 00:01:45,50
and there you can see the sample JSON data.

41
00:01:45,50 --> 00:01:47,70
So spend a few minutes touring this site

42
00:01:47,70 --> 00:01:49,10
and getting familiar with it,

43
00:01:49,10 --> 00:01:50,90
and perhaps try out some of the samples.

44
00:01:50,90 --> 00:01:53,40
OK, so once you've got everything here in place,

45
00:01:53,40 --> 00:01:55,00
you should be ready to go.

