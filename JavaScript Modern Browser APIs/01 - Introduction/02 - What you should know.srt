1
00:00:01,00 --> 00:00:02,00
- [Instructor] Before we get started,

2
00:00:02,00 --> 00:00:03,20
there are some things that you should

3
00:00:03,20 --> 00:00:05,20
already be familiar with.

4
00:00:05,20 --> 00:00:06,70
First, you should be familiar

5
00:00:06,70 --> 00:00:09,00
with programming in JavaScript, since that's the

6
00:00:09,00 --> 00:00:11,20
main language that web browsers use

7
00:00:11,20 --> 00:00:14,00
and it's what we will be using in this course.

8
00:00:14,00 --> 00:00:16,50
You don't need to be an expert, but I do expect

9
00:00:16,50 --> 00:00:18,30
that you already know how to do things like

10
00:00:18,30 --> 00:00:20,60
write functions and handle events,

11
00:00:20,60 --> 00:00:22,00
and you're reasonably up to speed

12
00:00:22,00 --> 00:00:24,00
on the most recent version of JavaScript.

13
00:00:24,00 --> 00:00:26,50
If you need to brush up on your JavaScript skills,

14
00:00:26,50 --> 00:00:29,80
I suggest you take a look at JavaScript Essential Training.

15
00:00:29,80 --> 00:00:31,90
You should also have some experience building webpages

16
00:00:31,90 --> 00:00:34,40
with HTML and CSS.

17
00:00:34,40 --> 00:00:36,50
There are plenty of great courses to check out

18
00:00:36,50 --> 00:00:38,90
if you need to shore up your skills in these areas,

19
00:00:38,90 --> 00:00:41,40
and I'd suggest looking at HTML Essential Training

20
00:00:41,40 --> 00:00:43,90
and Learning CSS.

21
00:00:43,90 --> 00:00:46,20
Since we're going to be working with text files,

22
00:00:46,20 --> 00:00:49,10
you can use whatever code editor you want for this course.

23
00:00:49,10 --> 00:00:51,50
I'm going to be using Visual Studio Code,

24
00:00:51,50 --> 00:00:54,90
but if you prefer to use Sublime Text or Emacs or vi,

25
00:00:54,90 --> 00:00:56,40
that's entirely up to you.

26
00:00:56,40 --> 00:00:58,60
VS Code is a free code editor

27
00:00:58,60 --> 00:01:00,80
that has a rich set of features and extensions

28
00:01:00,80 --> 00:01:03,20
that make it a great editor for almost any language.

29
00:01:03,20 --> 00:01:05,80
It's available on Mac, Windows, and Linux,

30
00:01:05,80 --> 00:01:08,60
so whatever operating system you happen to be using,

31
00:01:08,60 --> 00:01:10,00
it should work there.

32
00:01:10,00 --> 00:01:14,00
You can download it for free from code.visualstudio.com.

33
00:01:14,00 --> 00:01:15,50
If you'd like to learn more about VS Code

34
00:01:15,50 --> 00:01:17,70
before starting this course, I'd suggest you watch

35
00:01:17,70 --> 00:01:20,40
Visual Studio Code for Web Developers.

36
00:01:20,40 --> 00:01:22,40
Once you feel comfortable with the subjects,

37
00:01:22,40 --> 00:01:25,00
you're ready to move on with the rest of the course.

