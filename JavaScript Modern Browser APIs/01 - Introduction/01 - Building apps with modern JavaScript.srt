1
00:00:01,00 --> 00:00:04,20
- It's never been a better time to be a web developer.

2
00:00:04,20 --> 00:00:06,20
The kinds of applications that you can build

3
00:00:06,20 --> 00:00:09,00
on the web today using standard JavaScript,

4
00:00:09,00 --> 00:00:12,90
HTML, and CSS are highly capable, performant,

5
00:00:12,90 --> 00:00:15,80
and can do almost anything a native app can.

6
00:00:15,80 --> 00:00:17,40
It's hard to picture this today,

7
00:00:17,40 --> 00:00:18,80
but there was once a time when

8
00:00:18,80 --> 00:00:21,00
validating the content of a web form,

9
00:00:21,00 --> 00:00:23,60
or dynamically changing images on a page

10
00:00:23,60 --> 00:00:26,50
was about as advanced as JavaScript could get.

11
00:00:26,50 --> 00:00:28,80
We've come a long way since those days.

12
00:00:28,80 --> 00:00:31,20
Today we can build web apps that use features

13
00:00:31,20 --> 00:00:34,80
like native dialogs, integrated system notifications,

14
00:00:34,80 --> 00:00:38,70
and rich full-screen experiences just to name a few.

15
00:00:38,70 --> 00:00:40,90
What makes all of these features possible

16
00:00:40,90 --> 00:00:43,70
are the raft of new and modern APIs

17
00:00:43,70 --> 00:00:45,00
that have been added over the years

18
00:00:45,00 --> 00:00:46,90
by the main browser vendors.

19
00:00:46,90 --> 00:00:49,40
Browser capabilities have evolved significantly,

20
00:00:49,40 --> 00:00:51,90
and, in many cases, the kinds of features

21
00:00:51,90 --> 00:00:54,70
that used to require third party libraries,

22
00:00:54,70 --> 00:00:57,20
or resorting to native application building

23
00:00:57,20 --> 00:00:59,90
can now be done with standard JavaScript.

24
00:00:59,90 --> 00:01:01,00
Hi, I'm Joe Marini,

25
00:01:01,00 --> 00:01:02,80
and I've been building software professionally

26
00:01:02,80 --> 00:01:05,50
for over 30 years at companies like Microsoft,

27
00:01:05,50 --> 00:01:07,20
Google, and Adobe.

28
00:01:07,20 --> 00:01:08,50
Come join me in my course,

29
00:01:08,50 --> 00:01:10,60
and see how we can build great applications

30
00:01:10,60 --> 00:01:14,00
with JavaScript, and modern browser APIs.

