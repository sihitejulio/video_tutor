1
00:00:01,10 --> 00:00:02,30
- [Instructor] So far in this chapter,

2
00:00:02,30 --> 00:00:05,70
we've looked at the APIs available for storing information.

3
00:00:05,70 --> 00:00:08,70
But how does your app know about the memory

4
00:00:08,70 --> 00:00:11,50
capabilities of a particular device?

5
00:00:11,50 --> 00:00:13,60
How do you know how much storage space is available

6
00:00:13,60 --> 00:00:15,10
and how much has already been used?

7
00:00:15,10 --> 00:00:17,80
How do you know how much RAM a device has?

8
00:00:17,80 --> 00:00:19,40
Well up until the storage manager

9
00:00:19,40 --> 00:00:21,90
and device memory APIs came along,

10
00:00:21,90 --> 00:00:25,30
there wasn't a good way to find out any of this information.

11
00:00:25,30 --> 00:00:29,40
But now, we can do so in a standards compliant way.

12
00:00:29,40 --> 00:00:31,50
So we'll start with the Storage Manager API

13
00:00:31,50 --> 00:00:34,20
which you can find at this link here

14
00:00:34,20 --> 00:00:36,40
and I'll let you catch up on the content

15
00:00:36,40 --> 00:00:37,60
in your own time.

16
00:00:37,60 --> 00:00:40,50
For now, we're going to exercise these features

17
00:00:40,50 --> 00:00:42,30
in our example file.

18
00:00:42,30 --> 00:00:48,60
So in VS Code, I'm going to open up the storage_start file

19
00:00:48,60 --> 00:00:52,00
and let's just scroll up, you can see that there's

20
00:00:52,00 --> 00:00:55,20
some controls in here for finding out

21
00:00:55,20 --> 00:00:57,30
about storage space and so on.

22
00:00:57,30 --> 00:00:59,50
Let's just go ahead and bring this up in the browser.

23
00:00:59,50 --> 00:01:03,10
So I'll bring this up with my Live Server.

24
00:01:03,10 --> 00:01:05,60
And here in the page, I have some fields

25
00:01:05,60 --> 00:01:08,90
where I'll fill out the quota and usage stats

26
00:01:08,90 --> 00:01:11,70
and there's a button that will let me request what's called

27
00:01:11,70 --> 00:01:14,20
Storage Persistence and we'll get into that in a moment.

28
00:01:14,20 --> 00:01:20,20
I also have a field for the device's RAM configuration.

29
00:01:20,20 --> 00:01:22,90
So storage space is allocated on the browser

30
00:01:22,90 --> 00:01:25,40
on an origin by origin basis.

31
00:01:25,40 --> 00:01:29,60
So, if we go back to the code,

32
00:01:29,60 --> 00:01:31,00
I already have some code here

33
00:01:31,00 --> 00:01:34,50
that does Feature Detection to see if the Storage API

34
00:01:34,50 --> 00:01:39,60
is available, and if it is, I'll call the Estimate Function,

35
00:01:39,60 --> 00:01:42,10
which just like many new APIs these days,

36
00:01:42,10 --> 00:01:44,40
revolves as a Javascript promise.

37
00:01:44,40 --> 00:01:45,20
So I'll write:

38
00:01:45,20 --> 00:01:53,10
navigator.storage.estimate

39
00:01:53,10 --> 00:02:02,30
and then, use my Then Function to get a callback.

40
00:02:02,30 --> 00:02:03,70
Let's get some more room here.

41
00:02:03,70 --> 00:02:04,60
Here we go.

42
00:02:04,60 --> 00:02:07,80
And then inside my callback, I'll write:

43
00:02:07,80 --> 00:02:11,60
document.getElementById

44
00:02:11,60 --> 00:02:17,80
and that is the estimate.

45
00:02:17,80 --> 00:02:23,60
And that's going to be my estimate.quota

46
00:02:23,60 --> 00:02:31,30
and I'll have a similar line for the usage

47
00:02:31,30 --> 00:02:36,00
and that's going to be estimate.usage.

48
00:02:36,00 --> 00:02:39,40
Alright, so I've got two properties in my estimate object

49
00:02:39,40 --> 00:02:42,10
that comes back to my callback to see

50
00:02:42,10 --> 00:02:43,60
how much storage I have available.

51
00:02:43,60 --> 00:02:45,50
So let's go ahead and try that out.

52
00:02:45,50 --> 00:02:47,70
And you can see that when I refresh the page,

53
00:02:47,70 --> 00:02:51,60
I've got a quota of a whole lot of bytes

54
00:02:51,60 --> 00:02:55,00
and I've only used a small amount.

55
00:02:55,00 --> 00:02:58,00
Alright, so I've got quite a lot of storage available

56
00:02:58,00 --> 00:03:00,30
and my app can then use this information to make

57
00:03:00,30 --> 00:03:02,80
intelligent choices about how much data

58
00:03:02,80 --> 00:03:04,60
to download and cache.

59
00:03:04,60 --> 00:03:07,60
I can also use this API to ask the browser

60
00:03:07,60 --> 00:03:09,30
to make the data that I store locally,

61
00:03:09,30 --> 00:03:11,00
what's called Persistent.

62
00:03:11,00 --> 00:03:13,50
In other words, I want the browser to do its best

63
00:03:13,50 --> 00:03:16,80
to prioritize maintaining my site's data.

64
00:03:16,80 --> 00:03:18,80
So when a device is under memory pressure

65
00:03:18,80 --> 00:03:21,80
and it needs to clear out storage in order to make room,

66
00:03:21,80 --> 00:03:25,80
browsers will automatically attempt to clear storage space.

67
00:03:25,80 --> 00:03:28,60
For offline web apps, that's a problem

68
00:03:28,60 --> 00:03:31,60
because the browser might clear storage that contains

69
00:03:31,60 --> 00:03:33,50
really important information.

70
00:03:33,50 --> 00:03:36,40
So storage that is marked as persistent will not

71
00:03:36,40 --> 00:03:40,80
be cleared without first asking the user for confirmation.

72
00:03:40,80 --> 00:03:44,00
So the way that we do this is by requesting

73
00:03:44,00 --> 00:03:46,80
the persistent storage permission from the browser.

74
00:03:46,80 --> 00:03:49,80
And so once again, I've got some feature detection code,

75
00:03:49,80 --> 00:03:53,70
and if this feature is available, I can write:

76
00:03:53,70 --> 00:04:01,00
navigator.storage.persisted

77
00:04:01,00 --> 00:04:07,80
and then got my callback function, which will get a result

78
00:04:07,80 --> 00:04:14,50
and I can write: persistEl

79
00:04:14,50 --> 00:04:19,50
and I'll get that from my document.getElementById

80
00:04:19,50 --> 00:04:28,30
which is dtPersisted

81
00:04:28,30 --> 00:04:29,20
and then I'll write:

82
00:04:29,20 --> 00:04:33,50
persistEl.textContent

83
00:04:33,50 --> 00:04:37,20
is equal to this result right here.

84
00:04:37,20 --> 00:04:40,20
So that's persisted

85
00:04:40,20 --> 00:04:44,10
and that's either going to be true

86
00:04:44,10 --> 00:04:47,50
or false.

87
00:04:47,50 --> 00:04:51,30
So now I can request this ability from the browser,

88
00:04:51,30 --> 00:04:54,70
which I'll do here in the Event Handler for

89
00:04:54,70 --> 00:04:58,00
the Request Persisted Storage button.

90
00:04:58,00 --> 00:05:01,00
So I'll write:

91
00:05:01,00 --> 00:05:09,70
navigator.storage.persist

92
00:05:09,70 --> 00:05:15,50
and then once again that resolves as promise

93
00:05:15,50 --> 00:05:19,60
and if the result is true,

94
00:05:19,60 --> 00:05:26,10
I'll console.log

95
00:05:26,10 --> 00:05:32,80
"Storage is persistent"

96
00:05:32,80 --> 00:05:39,00
I'll write out something else,

97
00:05:39,00 --> 00:05:45,10
"Unable to make storage persistent."

98
00:05:45,10 --> 00:05:47,40
Alright, so let's give this a try.

99
00:05:47,40 --> 00:05:50,60
Let's go back to the browser

100
00:05:50,60 --> 00:05:52,50
and we'll refresh and you can see that the

101
00:05:52,50 --> 00:05:55,40
Storage Persistence is false

102
00:05:55,40 --> 00:05:59,00
so when I bring up the Developer Tools,

103
00:05:59,00 --> 00:06:00,30
here we are in the console,

104
00:06:00,30 --> 00:06:03,50
I'm going to Request Storage Persistence

105
00:06:03,50 --> 00:06:06,80
and now you can see that the storage is persistent

106
00:06:06,80 --> 00:06:09,00
so when I refresh, you can see now

107
00:06:09,00 --> 00:06:13,40
that the Storage Persistence is true.

108
00:06:13,40 --> 00:06:17,00
So, what this means is that now,

109
00:06:17,00 --> 00:06:19,90
data that is stored locally by my app

110
00:06:19,90 --> 00:06:22,90
from this origin, will be given priority

111
00:06:22,90 --> 00:06:25,50
when other data needs to be cleared out.

112
00:06:25,50 --> 00:06:27,80
Now my app can't prevent that from happening,

113
00:06:27,80 --> 00:06:31,60
but at least I've asked the browser to prioritize

114
00:06:31,60 --> 00:06:36,00
my data above other more transient web apps.

