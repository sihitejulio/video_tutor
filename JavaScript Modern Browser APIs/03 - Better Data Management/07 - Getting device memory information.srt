1
00:00:01,10 --> 00:00:04,30
- Okay, so now we're going to use the Device Memory API

2
00:00:04,30 --> 00:00:07,40
to see how much RAM our device has.

3
00:00:07,40 --> 00:00:09,40
And this API's pretty simple.

4
00:00:09,40 --> 00:00:12,30
It's basically just a property available

5
00:00:12,30 --> 00:00:15,80
on the global navigator object called Device Memory.

6
00:00:15,80 --> 00:00:17,50
So I've brought the spec up here in a browser

7
00:00:17,50 --> 00:00:19,00
and you can learn more about it by

8
00:00:19,00 --> 00:00:20,60
following this link.

9
00:00:20,60 --> 00:00:21,50
In the mean time,

10
00:00:21,50 --> 00:00:23,00
let's jump over to the code

11
00:00:23,00 --> 00:00:25,70
and fill out the rest of our storage example

12
00:00:25,70 --> 00:00:28,10
to use the device memory API.

13
00:00:28,10 --> 00:00:30,30
So I'm going to open up storage_start

14
00:00:30,30 --> 00:00:33,10
and I'll close the files list,

15
00:00:33,10 --> 00:00:34,30
so we have some space.

16
00:00:34,30 --> 00:00:36,30
So this is essentially where

17
00:00:36,30 --> 00:00:38,90
we left off in the storage example.

18
00:00:38,90 --> 00:00:39,90
And if I scroll down,

19
00:00:39,90 --> 00:00:42,40
you can see that the code that we filled in

20
00:00:42,40 --> 00:00:45,60
for the storage persistence is still here.

21
00:00:45,60 --> 00:00:48,00
And we're going to fill in this section here.

22
00:00:48,00 --> 00:00:52,30
So as I stated, this API is pretty simple.

23
00:00:52,30 --> 00:00:58,70
I'm going to write if navigator.deviceMemory.

24
00:00:58,70 --> 00:01:02,20
So I'll feature detect to see if this property is here,

25
00:01:02,20 --> 00:01:03,10
and if it's there,

26
00:01:03,10 --> 00:01:08,80
I'm going to write Document.getElementById

27
00:01:08,80 --> 00:01:11,30
and the I.D. of the field that we want

28
00:01:11,30 --> 00:01:14,90
is this span right here called devMem,

29
00:01:14,90 --> 00:01:17,10
and we're going to fill in this value with

30
00:01:17,10 --> 00:01:19,50
how much RAM our device has.

31
00:01:19,50 --> 00:01:24,20
So, it's called devMem,

32
00:01:24,20 --> 00:01:30,10
and I'll set the text content equal to navigator.devMemory.

33
00:01:30,10 --> 00:01:32,30
Now for security purposes,

34
00:01:32,30 --> 00:01:35,60
this API is not exact.

35
00:01:35,60 --> 00:01:40,00
It doesn't tell you exactly how much RAM a device has,

36
00:01:40,00 --> 00:01:42,80
and that's to prevent device fingerprinting

37
00:01:42,80 --> 00:01:44,20
by malicious code.

38
00:01:44,20 --> 00:01:45,80
It just basically tells you what

39
00:01:45,80 --> 00:01:48,10
memory class this device is,

40
00:01:48,10 --> 00:01:50,30
so as of this recording,

41
00:01:50,30 --> 00:01:52,90
this API will only report the values of

42
00:01:52,90 --> 00:02:00,20
.25, .5, one, two, four, and eight gigabytes of RAM.

43
00:02:00,20 --> 00:02:03,50
So the API basically rounds whatever the value

44
00:02:03,50 --> 00:02:06,70
of the RAM is to the most significant bit.

45
00:02:06,70 --> 00:02:08,90
So with this information,

46
00:02:08,90 --> 00:02:11,70
we can decide on the fly what resources

47
00:02:11,70 --> 00:02:13,90
to download in order to provide

48
00:02:13,90 --> 00:02:15,70
the richest possible experience

49
00:02:15,70 --> 00:02:17,60
without degrading performance.

50
00:02:17,60 --> 00:02:18,80
So let's go ahead and save,

51
00:02:18,80 --> 00:02:20,60
and let's bring this up in a browser,

52
00:02:20,60 --> 00:02:24,60
and you can see from the previous storage example,

53
00:02:24,60 --> 00:02:26,70
we still have our values from that.

54
00:02:26,70 --> 00:02:29,20
So here's our quota, it has how much is used,

55
00:02:29,20 --> 00:02:30,80
here's whether it's persistent.

56
00:02:30,80 --> 00:02:34,00
And now down here in the device memory section,

57
00:02:34,00 --> 00:02:36,80
I've got my value of eight gigabytes,

58
00:02:36,80 --> 00:02:37,90
so we can see that I'm on

59
00:02:37,90 --> 00:02:40,00
a pretty serious class machine here,

60
00:02:40,00 --> 00:02:42,60
but if I was on a lower end mobile device,

61
00:02:42,60 --> 00:02:47,80
this might only be one gigabyte, or maybe even .5 gigabytes.

62
00:02:47,80 --> 00:02:50,00
So by using these different values

63
00:02:50,00 --> 00:02:52,80
for device memory and the storage manager,

64
00:02:52,80 --> 00:02:55,50
you can figure out what the storage capacity

65
00:02:55,50 --> 00:02:57,30
of a user's device is,

66
00:02:57,30 --> 00:02:59,40
and tailor the experience accordingly

67
00:02:59,40 --> 00:03:02,00
to make the most of the resources that the user has.

