1
00:00:01,00 --> 00:00:02,40
- Alright, let's pick up where we left off

2
00:00:02,40 --> 00:00:04,00
in the previous example

3
00:00:04,00 --> 00:00:06,60
and let's try using LocalForage

4
00:00:06,60 --> 00:00:09,50
with multiple database instances.

5
00:00:09,50 --> 00:00:11,80
So I'm going to go ahead and open up my

6
00:00:11,80 --> 00:00:14,40
indexed_start in the

7
00:00:14,40 --> 00:00:17,70
example four of chapter two.

8
00:00:17,70 --> 00:00:21,70
And let's scroll down.

9
00:00:21,70 --> 00:00:24,20
And here's the code from where we left off

10
00:00:24,20 --> 00:00:27,60
in the previous video.

11
00:00:27,60 --> 00:00:29,50
Alright, so

12
00:00:29,50 --> 00:00:31,60
here in the event handler for the

13
00:00:31,60 --> 00:00:33,70
create instances button

14
00:00:33,70 --> 00:00:35,30
which if we scroll back up

15
00:00:35,30 --> 00:00:38,10
you can see that there is a button here

16
00:00:38,10 --> 00:00:40,30
called btnMulti

17
00:00:40,30 --> 00:00:42,60
which is for Create Instances

18
00:00:42,60 --> 00:00:44,90
and then we've got buttons for storing

19
00:00:44,90 --> 00:00:46,30
and getting data.

20
00:00:46,30 --> 00:00:49,70
So back down here

21
00:00:49,70 --> 00:00:50,90
in the event handler

22
00:00:50,90 --> 00:00:53,70
for the create instances button

23
00:00:53,70 --> 00:00:56,50
I'm going to create two different databases.

24
00:00:56,50 --> 00:01:00,70
So I'll have instance1

25
00:01:00,70 --> 00:01:03,40
which will be

26
00:01:03,40 --> 00:01:04,40
localforage

27
00:01:04,40 --> 00:01:08,40
and I'll use the createInstance function

28
00:01:08,40 --> 00:01:11,20
and I have to pass in some options.

29
00:01:11,20 --> 00:01:15,80
And so I'll give it a name.

30
00:01:15,80 --> 00:01:18,60
And the name's going to be instance1

31
00:01:18,60 --> 00:01:22,40
and then I'll create instance2

32
00:01:22,40 --> 00:01:24,40
and then pretty much the same code.

33
00:01:24,40 --> 00:01:26,30
So I'll just copy and paste it.

34
00:01:26,30 --> 00:01:29,50
And that's going to be instance2.

35
00:01:29,50 --> 00:01:32,10
And just to show how this works,

36
00:01:32,10 --> 00:01:35,20
let's store two different values

37
00:01:35,20 --> 00:01:37,60
with the same key name,

38
00:01:37,60 --> 00:01:40,00
but in separate instances.

39
00:01:40,00 --> 00:01:42,50
So when the user clicks the star button

40
00:01:42,50 --> 00:01:45,10
I'm going to say instance1 and I'm going to call

41
00:01:45,10 --> 00:01:46,50
setItem()

42
00:01:46,50 --> 00:01:50,70
and I'm going to store key1

43
00:01:50,70 --> 00:01:55,20
with value1.

44
00:01:55,20 --> 00:02:00,80
And then I'll do the same thing for instance2.

45
00:02:00,80 --> 00:02:04,90
Same key name but a different value.

46
00:02:04,90 --> 00:02:07,40
And then let's retrieve the data.

47
00:02:07,40 --> 00:02:11,10
So I'll have instance1

48
00:02:11,10 --> 00:02:14,10
and I'll use the getItem() function

49
00:02:14,10 --> 00:02:16,30
and I'll get the value of

50
00:02:16,30 --> 00:02:19,20
key1

51
00:02:19,20 --> 00:02:23,30
and of course, have to have my promise function.

52
00:02:23,30 --> 00:02:27,80
So that's going to give me a value back

53
00:02:27,80 --> 00:02:31,80
and I'll log out to the console,

54
00:02:31,80 --> 00:02:33,80
that instance1

55
00:02:33,80 --> 00:02:35,80
returned.

56
00:02:35,80 --> 00:02:38,00
And that's going to be the value.

57
00:02:38,00 --> 00:02:41,80
And then I'll do the same thing for instance2.

58
00:02:41,80 --> 00:02:44,60
So there's instance2

59
00:02:44,60 --> 00:02:48,50
and it's going to retrieve the same key name.

60
00:02:48,50 --> 00:02:50,10
Alright so let's go ahead and save

61
00:02:50,10 --> 00:02:51,80
and let's go ahead and bring this back up

62
00:02:51,80 --> 00:02:54,00
in our browser.

63
00:02:54,00 --> 00:02:56,10
Alright. And I'll

64
00:02:56,10 --> 00:02:58,20
show the developer tools.

65
00:02:58,20 --> 00:02:59,70
Alright, there's the console.

66
00:02:59,70 --> 00:03:03,20
So I'm going to create the instances.

67
00:03:03,20 --> 00:03:04,00
Alright.

68
00:03:04,00 --> 00:03:10,00
And then I'm going to store the data.

69
00:03:10,00 --> 00:03:14,20
And you can see that when I click the Get Data button

70
00:03:14,20 --> 00:03:17,20
that instance1 returned value1

71
00:03:17,20 --> 00:03:19,70
and instance2 returned value2

72
00:03:19,70 --> 00:03:22,90
so even though the keys have the same names

73
00:03:22,90 --> 00:03:25,20
the data returned is different.

74
00:03:25,20 --> 00:03:27,40
Local forages are a really useful library

75
00:03:27,40 --> 00:03:31,40
for simplifying the IndexDB API for storing

76
00:03:31,40 --> 00:03:34,90
key value pairs while keeping the advantages

77
00:03:34,90 --> 00:03:37,40
of using this modern storage mechanism.

78
00:03:37,40 --> 00:03:39,30
You get Javascript promises.

79
00:03:39,30 --> 00:03:41,90
You get asynchronous code execution.

80
00:03:41,90 --> 00:03:43,80
And overall it's just much better

81
00:03:43,80 --> 00:03:47,00
than using local storage or session storage.

