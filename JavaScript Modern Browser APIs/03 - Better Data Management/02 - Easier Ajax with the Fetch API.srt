1
00:00:01,00 --> 00:00:02,80
- [Speaker] Back when it was first introduced,

2
00:00:02,80 --> 00:00:06,70
the XML HTTP request API, sometimes called xHR,

3
00:00:06,70 --> 00:00:08,90
or even Ajax for short,

4
00:00:08,90 --> 00:00:12,50
had a huge impact on the quality of web applications.

5
00:00:12,50 --> 00:00:15,70
The modern API equivalent of xHR is called fetch.

6
00:00:15,70 --> 00:00:18,10
And it dramatically simplifies writing code

7
00:00:18,10 --> 00:00:20,90
that requests data from remote servers.

8
00:00:20,90 --> 00:00:23,30
Let's take a look at how to use this API to perform

9
00:00:23,30 --> 00:00:25,20
a network operation.

10
00:00:25,20 --> 00:00:29,60
So here in chapter two, I'll open up fetch underscore_start.

11
00:00:29,60 --> 00:00:35,10
And let's begin by implementing a simple HTTP GET operation.

12
00:00:35,10 --> 00:00:39,30
And I'm going to be using the HTTP been.org site here

13
00:00:39,30 --> 00:00:42,50
to retrieve some sample JSON code.

14
00:00:42,50 --> 00:00:44,80
So let's start off with the code to perform,

15
00:00:44,80 --> 00:00:50,30
a get so all right fetch

16
00:00:50,30 --> 00:00:54,50
and the URL is

17
00:00:54,50 --> 00:00:59,80
HTTP been.org/JSON.

18
00:00:59,80 --> 00:01:04,00
Now because the fetch API uses JavaScript promises,

19
00:01:04,00 --> 00:01:07,20
I can use the then function

20
00:01:07,20 --> 00:01:10,10
to chain onto this call and define the code

21
00:01:10,10 --> 00:01:11,70
for what happens next.

22
00:01:11,70 --> 00:01:14,70
So all right then, and that takes a callback function

23
00:01:14,70 --> 00:01:19,00
with the response.

24
00:01:19,00 --> 00:01:22,10
And inside that callback, I'm going to log out

25
00:01:22,10 --> 00:01:24,10
some information to the console.

26
00:01:24,10 --> 00:01:29,30
So I'll console log the content-type.

27
00:01:29,30 --> 00:01:32,90
And that's going to be the,

28
00:01:32,90 --> 00:01:39,40
response.headers.get.

29
00:01:39,40 --> 00:01:47,40
And that's going to be content_type.

30
00:01:47,40 --> 00:01:51,60
And we close the files, we can see some more.

31
00:01:51,60 --> 00:01:54,50
Alright, and I'll just copy this line

32
00:01:54,50 --> 00:01:56,30
and get some other information,

33
00:01:56,30 --> 00:02:01,50
so we'll get the redirected header.

34
00:02:01,50 --> 00:02:04,60
And that's going to be the

35
00:02:04,60 --> 00:02:10,20
response.redirected

36
00:02:10,20 --> 00:02:13,90
and then we'll get the status.

37
00:02:13,90 --> 00:02:17,60
And that's going to be response status.

38
00:02:17,60 --> 00:02:21,80
And we might as well get the status-text while we're at it.

39
00:02:21,80 --> 00:02:25,10
And that's going to be status-text.

40
00:02:25,10 --> 00:02:29,80
And then we'll get the response type.

41
00:02:29,80 --> 00:02:32,80
And that's going to be

42
00:02:32,80 --> 00:02:35,30
response.type.

43
00:02:35,30 --> 00:02:37,70
And then finally,

44
00:02:37,70 --> 00:02:41,40
let's get the response URL

45
00:02:41,40 --> 00:02:47,80
and that's going to be the respons.url property.

46
00:02:47,80 --> 00:02:51,60
Okay, so that gives us a nice set of information

47
00:02:51,60 --> 00:02:54,30
that we can log out when we do the fetch.

48
00:02:54,30 --> 00:02:57,30
So let's go ahead and run what we have.

49
00:02:57,30 --> 00:03:00,70
So I'll bring this up in the live server.

50
00:03:00,70 --> 00:03:03,60
To see the output, we can look at the developer tools

51
00:03:03,60 --> 00:03:05,10
in the console.

52
00:03:05,10 --> 00:03:07,70
And you can see that the various fields

53
00:03:07,70 --> 00:03:10,10
have been logged out here, right?

54
00:03:10,10 --> 00:03:12,60
So the content type is application JSON,

55
00:03:12,60 --> 00:03:15,40
we were redirected, the status code is 200,

56
00:03:15,40 --> 00:03:18,20
which is okay, and so on.

57
00:03:18,20 --> 00:03:20,80
If you're not familiar with JavaScript promises, by the way,

58
00:03:20,80 --> 00:03:24,50
make a note to watch the JavaScript promises videos

59
00:03:24,50 --> 00:03:28,40
in the learning ECMO script six course.

60
00:03:28,40 --> 00:03:31,50
Alright, so let's add some code to get the response data.

61
00:03:31,50 --> 00:03:34,10
So let's go back to the code and in here,

62
00:03:34,10 --> 00:03:40,30
what I'm going to write is if response.status

63
00:03:40,30 --> 00:03:42,80
is equal to 200,

64
00:03:42,80 --> 00:03:47,30
then I'm going to return response.text

65
00:03:47,30 --> 00:03:52,50
which itself is a promise so now we can further chain,

66
00:03:52,50 --> 00:03:56,20
on to this function by writing .then

67
00:03:56,20 --> 00:04:00,90
and then I'm going to have a function called returned data.

68
00:04:00,90 --> 00:04:02,50
And return data...

69
00:04:02,50 --> 00:04:05,20
We actually

70
00:04:05,20 --> 00:04:08,80
return data will be the argument right?

71
00:04:08,80 --> 00:04:13,30
There we go, and inside that function,

72
00:04:13,30 --> 00:04:17,90
I'm going to write out to the console

73
00:04:17,90 --> 00:04:27,20
that the returned data was the return data argument.

74
00:04:27,20 --> 00:04:30,40
And I can also catch any errors.

75
00:04:30,40 --> 00:04:38,70
(typing)

76
00:04:38,70 --> 00:04:40,20
And if any errors happen,

77
00:04:40,20 --> 00:04:45,20
I'll just simply log the error out to the field.

78
00:04:45,20 --> 00:04:49,30
And that's going to be this argument there.

79
00:04:49,30 --> 00:04:53,80
Alright, so now I'm actually going to get the response text

80
00:04:53,80 --> 00:04:56,90
back from the response that comes back

81
00:04:56,90 --> 00:04:58,40
and log it out to the console,

82
00:04:58,40 --> 00:05:01,90
so let's save and let's go back to the code

83
00:05:01,90 --> 00:05:03,90
and I'll refresh.

84
00:05:03,90 --> 00:05:08,00
And you can see that now the return data is a bunch of JSON.

85
00:05:08,00 --> 00:05:12,10
And that's being written out to the console here.

86
00:05:12,10 --> 00:05:14,90
Alright, so of course, you're not limited to get requests,

87
00:05:14,90 --> 00:05:17,80
you can also easily make a POST request

88
00:05:17,80 --> 00:05:20,90
by using the right arguments to the fetch function.

89
00:05:20,90 --> 00:05:24,80
So in this case, will post a simple set of data

90
00:05:24,80 --> 00:05:26,30
and a custom header,

91
00:05:26,30 --> 00:05:29,50
so let's go back to the code and here in the post example,

92
00:05:29,50 --> 00:05:35,00
I'm going to write fetch and I'm going to use,

93
00:05:35,00 --> 00:05:37,70
the same address

94
00:05:37,70 --> 00:05:41,50
only this time, I'm going to call the post endpoint

95
00:05:41,50 --> 00:05:44,70
and I'm going to call the post endpoint with an object

96
00:05:44,70 --> 00:05:46,70
that contains my data.

97
00:05:46,70 --> 00:05:51,30
So all right method and that's going to be post

98
00:05:51,30 --> 00:05:53,60
and the body

99
00:05:53,60 --> 00:05:57,30
is going to be a string and I'll just pass in some simple

100
00:05:57,30 --> 00:06:00,60
arguments like full equals bar.

101
00:06:00,60 --> 00:06:02,70
And then I'll pass in some custom headers,

102
00:06:02,70 --> 00:06:06,00
which is itself an object.

103
00:06:06,00 --> 00:06:12,10
And I'll pass in, you know, x custom header.

104
00:06:12,10 --> 00:06:19,30
And I'll give it my custom value.

105
00:06:19,30 --> 00:06:22,70
All right, so that's all there is to that and then,

106
00:06:22,70 --> 00:06:24,80
I'll write then, and then once again,

107
00:06:24,80 --> 00:06:28,40
I have a function to handle the response.

108
00:06:28,40 --> 00:06:34,70
And then inside that, I'm going to return the response.text,

109
00:06:34,70 --> 00:06:38,40
which means I'll write .then,

110
00:06:38,40 --> 00:06:44,50
and that's going to be a function for the return data.

111
00:06:44,50 --> 00:06:46,90
And then I'll write out

112
00:06:46,90 --> 00:06:50,20
the return data,

113
00:06:50,20 --> 00:06:52,50
host data

114
00:06:52,50 --> 00:06:57,80
and that's going to be the return data.

115
00:06:57,80 --> 00:07:00,70
Alright, so let's go ahead and run this one more time.

116
00:07:00,70 --> 00:07:03,60
And then back here in the developer tools,

117
00:07:03,60 --> 00:07:06,80
you can see that here's the get operation

118
00:07:06,80 --> 00:07:08,20
and the post operation.

119
00:07:08,20 --> 00:07:11,00
You can see that that returns the posted data

120
00:07:11,00 --> 00:07:14,50
and right here is my full equals bar argument

121
00:07:14,50 --> 00:07:17,20
in the data property.

122
00:07:17,20 --> 00:07:19,10
And then if we look down here, yep,

123
00:07:19,10 --> 00:07:20,10
there's my custom header,

124
00:07:20,10 --> 00:07:24,60
so here's x custom header with my custom value.

125
00:07:24,60 --> 00:07:27,90
So as you can see, using the fetch API,

126
00:07:27,90 --> 00:07:30,00
along with JavaScript promises,

127
00:07:30,00 --> 00:07:34,00
results in code that is much easier to write and maintain,

128
00:07:34,00 --> 00:07:36,70
then using the old style Ajax.

129
00:07:36,70 --> 00:07:39,50
You can avoid having to handle multiple callbacks

130
00:07:39,50 --> 00:07:42,80
and different response states and just focus on sending

131
00:07:42,80 --> 00:07:44,00
and retrieving data.

