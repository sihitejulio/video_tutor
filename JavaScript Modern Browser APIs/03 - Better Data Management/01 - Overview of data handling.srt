1
00:00:00,90 --> 00:00:03,20
- [Instructor] Data is one of the most essential components

2
00:00:03,20 --> 00:00:05,10
of any real web app.

3
00:00:05,10 --> 00:00:08,30
Most apps have a need to retrieve, manipulate,

4
00:00:08,30 --> 00:00:11,40
store, and process various kinds of data.

5
00:00:11,40 --> 00:00:13,60
In this chapter, we're going to spend some time

6
00:00:13,60 --> 00:00:16,20
looking at the modern APIs available

7
00:00:16,20 --> 00:00:18,00
to make working with data easier.

8
00:00:18,00 --> 00:00:20,60
Over the years, various web standards have emerged

9
00:00:20,60 --> 00:00:23,00
to give developers the tools needed

10
00:00:23,00 --> 00:00:25,70
to perform basic data-related tasks.

11
00:00:25,70 --> 00:00:28,30
And this has resulted in a fairly complex set

12
00:00:28,30 --> 00:00:31,00
of APIs for working with data.

13
00:00:31,00 --> 00:00:33,80
So just to name some of them, there's Cookies,

14
00:00:33,80 --> 00:00:38,00
Application cache, localStorage, sessionStorage,

15
00:00:38,00 --> 00:00:43,30
XmlHttpRequest, IndexedDB, and WebSQL.

16
00:00:43,30 --> 00:00:44,90
Now some of these are still in use

17
00:00:44,90 --> 00:00:48,50
while others are obsolete and the ones that are still in use

18
00:00:48,50 --> 00:00:50,00
have drawbacks.

19
00:00:50,00 --> 00:00:53,70
LocalStorage, for example, is a synchronous API.

20
00:00:53,70 --> 00:00:55,40
So it blocks user interaction

21
00:00:55,40 --> 00:00:57,10
while your code is accessing it.

22
00:00:57,10 --> 00:00:59,80
Application cache never really fully solved

23
00:00:59,80 --> 00:01:03,60
all the scenarios of building true offline capable web apps,

24
00:01:03,60 --> 00:01:06,90
and WebSQL was never universally adopted.

25
00:01:06,90 --> 00:01:08,60
The APIs that we're going to be working with

26
00:01:08,60 --> 00:01:11,00
in this chapter provide the functionality you need

27
00:01:11,00 --> 00:01:14,10
to work with data using the latest JavaScript standards,

28
00:01:14,10 --> 00:01:15,30
like Promises.

29
00:01:15,30 --> 00:01:16,40
So we're going to start off learning

30
00:01:16,40 --> 00:01:19,50
about the fetch() API, which is a much nicer way

31
00:01:19,50 --> 00:01:24,00
of getting and posting data than XMLHttpRequest.

32
00:01:24,00 --> 00:01:26,10
Then we'll take a look at an API provided

33
00:01:26,10 --> 00:01:29,50
by Mozilla Foundation, called localForage,

34
00:01:29,50 --> 00:01:33,20
which gives you a nice wrapper around the IndexedDB API,

35
00:01:33,20 --> 00:01:36,50
and simplifies some common data storage scenarios.

36
00:01:36,50 --> 00:01:39,50
After that, we'll combine what we've learned about fetch()

37
00:01:39,50 --> 00:01:42,70
with the newer Cache API to see how to easily

38
00:01:42,70 --> 00:01:46,60
cache http request and response pairs locally.

39
00:01:46,60 --> 00:01:48,80
Then we'll wrap up with the StorageManager

40
00:01:48,80 --> 00:01:51,80
and DeviceMemory APIs, which give you new ways

41
00:01:51,80 --> 00:01:54,10
of determining how much storage space

42
00:01:54,10 --> 00:01:56,10
and memory capacity a device has,

43
00:01:56,10 --> 00:02:00,00
and how to make it persistent on the user's local machine.

