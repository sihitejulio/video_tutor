1
00:00:01,00 --> 00:00:03,00
- [Narrator] As I mentioned at the start of the chapter,

2
00:00:03,00 --> 00:00:05,70
most applications have a need to store data

3
00:00:05,70 --> 00:00:07,50
locally on the user's machine.

4
00:00:07,50 --> 00:00:10,80
And you probably already know by now that IndexedDB

5
00:00:10,80 --> 00:00:13,40
is the preferable way to do so.

6
00:00:13,40 --> 00:00:14,70
First, it's asynchronous.

7
00:00:14,70 --> 00:00:17,60
So it helps to keep your app responsive.

8
00:00:17,60 --> 00:00:20,10
It also lets you store multiple kinds of data,

9
00:00:20,10 --> 00:00:23,10
so you're not limited to strings of text.

10
00:00:23,10 --> 00:00:25,60
You can also define more than one database,

11
00:00:25,60 --> 00:00:28,00
so you can separate unrelated data

12
00:00:28,00 --> 00:00:31,10
and only operate on the collection that you need to.

13
00:00:31,10 --> 00:00:33,30
The downside of IndexedDB however,

14
00:00:33,30 --> 00:00:36,20
is that the API for using it is considerably

15
00:00:36,20 --> 00:00:39,40
more complex then the simple key value system

16
00:00:39,40 --> 00:00:41,70
used by something like local storage.

17
00:00:41,70 --> 00:00:43,60
The good folks at Mozilla, however,

18
00:00:43,60 --> 00:00:46,40
have created a good, third party library

19
00:00:46,40 --> 00:00:49,70
called localForage that helps with this.

20
00:00:49,70 --> 00:00:51,60
You can download it from this sight,

21
00:00:51,60 --> 00:00:54,90
which also contains the detailed docs for it.

22
00:00:54,90 --> 00:00:57,70
Using localForage you can treat IndexedDB

23
00:00:57,70 --> 00:01:01,20
databases as simple key value storage.

24
00:01:01,20 --> 00:01:03,00
So let's fire it up in our editor

25
00:01:03,00 --> 00:01:04,60
and take it for a spin.

26
00:01:04,60 --> 00:01:06,60
So here in our editor I'm going to open up

27
00:01:06,60 --> 00:01:11,20
indexed_start.html

28
00:01:11,20 --> 00:01:12,80
and I've already downloaded

29
00:01:12,80 --> 00:01:18,40
and stored the minimized version of localForage.js

30
00:01:18,40 --> 00:01:21,80
in my folder here, so I've already got that in place.

31
00:01:21,80 --> 00:01:24,80
And you can see I'm accessing it here

32
00:01:24,80 --> 00:01:26,80
in this script tag.

33
00:01:26,80 --> 00:01:30,30
So, let's take a quick look at our page in the browser,

34
00:01:30,30 --> 00:01:34,70
and you can see that this is a page that has some

35
00:01:34,70 --> 00:01:38,90
controls in it for storing and retrieving values

36
00:01:38,90 --> 00:01:41,30
and deleting items and listing data.

37
00:01:41,30 --> 00:01:44,80
So let's go fill in the code to make these controls work.

38
00:01:44,80 --> 00:01:47,40
So I've already got some code in place to handle

39
00:01:47,40 --> 00:01:48,90
the button click events.

40
00:01:48,90 --> 00:01:51,70
So let's add the code to store data using

41
00:01:51,70 --> 00:01:56,30
the set item function which takes a key and a value.

42
00:01:56,30 --> 00:02:00,10
So when the user clicks the set button, I'm going to write

43
00:02:00,10 --> 00:02:05,50
localForage.setitem

44
00:02:05,50 --> 00:02:08,60
and that takes a key and a value,

45
00:02:08,60 --> 00:02:10,70
which we've already retrieved from the forum

46
00:02:10,70 --> 00:02:12,80
using these statements.

47
00:02:12,80 --> 00:02:16,90
And what's nice is that localForage uses javascript

48
00:02:16,90 --> 00:02:19,90
promises, so I can just write then,

49
00:02:19,90 --> 00:02:22,20
and then I have my callback function,

50
00:02:22,20 --> 00:02:27,00
which will give me a value and I can write to the console

51
00:02:27,00 --> 00:02:28,80
that

52
00:02:28,80 --> 00:02:30,00
set item

53
00:02:30,00 --> 00:02:33,70
stored this value.

54
00:02:33,70 --> 00:02:37,40
And then let's add the code to retrieve the data.

55
00:02:37,40 --> 00:02:41,60
So that's going to be localForage.getitem

56
00:02:41,60 --> 00:02:45,10
and we're going to give it a key,

57
00:02:45,10 --> 00:02:49,10
and then once again I'll have a function

58
00:02:49,10 --> 00:02:52,30
as my callback

59
00:02:52,30 --> 00:02:57,20
and in this case I'll just log that

60
00:02:57,20 --> 00:03:02,10
get item retrieved that value.

61
00:03:02,10 --> 00:03:05,10
We could also remove data from the database using

62
00:03:05,10 --> 00:03:07,20
the remove item function.

63
00:03:07,20 --> 00:03:11,60
So I'll write localForage.removeitem

64
00:03:11,60 --> 00:03:16,20
and that takes a key

65
00:03:16,20 --> 00:03:20,30
and then once again we'll have our callback

66
00:03:20,30 --> 00:03:25,30
and the callback will just console log

67
00:03:25,30 --> 00:03:29,90
that remove item deleted

68
00:03:29,90 --> 00:03:32,90
that given value.

69
00:03:32,90 --> 00:03:34,00
And then finally,

70
00:03:34,00 --> 00:03:36,70
let's add some code to iterate over all

71
00:03:36,70 --> 00:03:38,10
the stored data.

72
00:03:38,10 --> 00:03:43,70
So I'll use localForage.iterate

73
00:03:43,70 --> 00:03:46,50
and the iterate function

74
00:03:46,50 --> 00:03:52,30
takes a callback and that callback will be given a value,

75
00:03:52,30 --> 00:03:57,40
a key, and the iteration number.

76
00:03:57,40 --> 00:04:03,30
And once again we can just console.log,

77
00:04:03,30 --> 00:04:06,60
the iteration number,

78
00:04:06,60 --> 00:04:11,80
and the key and the value.

79
00:04:11,80 --> 00:04:16,70
And of course that uses a promise so we can then say

80
00:04:16,70 --> 00:04:19,50
then, when it's done,

81
00:04:19,50 --> 00:04:25,90
we'll log out that iteration complete.

82
00:04:25,90 --> 00:04:29,10
Okay, so now let's go back and refresh our page

83
00:04:29,10 --> 00:04:32,20
and let's bring up the developer tools

84
00:04:32,20 --> 00:04:34,80
so we can see the console.

85
00:04:34,80 --> 00:04:35,70
Alright.

86
00:04:35,70 --> 00:04:38,10
So I'll go ahead and store some values.

87
00:04:38,10 --> 00:04:41,20
So here's key one and value one.

88
00:04:41,20 --> 00:04:43,40
So we'll store that.

89
00:04:43,40 --> 00:04:48,00
And then we'll store key two and value two.

90
00:04:48,00 --> 00:04:53,20
And let's store key three and value three.

91
00:04:53,20 --> 00:04:56,20
And you can see that those values are being stored.

92
00:04:56,20 --> 00:04:59,20
And let's try retrieving a value.

93
00:04:59,20 --> 00:05:02,70
So we'll retrieve value three using key three.

94
00:05:02,70 --> 00:05:04,30
You can see that that worked.

95
00:05:04,30 --> 00:05:08,10
Let's go ahead and delete key two.

96
00:05:08,10 --> 00:05:10,70
And so now when we try to get key two

97
00:05:10,70 --> 00:05:13,50
you can see that that's not there anymore.

98
00:05:13,50 --> 00:05:16,30
And now let's go ahead and list all the values.

99
00:05:16,30 --> 00:05:19,60
And you can see the key one and key three are left,

100
00:05:19,60 --> 00:05:22,90
and the iteration is complete.

101
00:05:22,90 --> 00:05:26,70
Alright, so that's a quick introduction to localForage.

102
00:05:26,70 --> 00:05:28,70
So in the next example,

103
00:05:28,70 --> 00:05:30,50
we'll take a look at how we can work with

104
00:05:30,50 --> 00:05:32,90
multiple different instances

105
00:05:32,90 --> 00:05:35,00
of localForage databases.

