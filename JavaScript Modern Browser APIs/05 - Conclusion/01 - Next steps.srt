1
00:00:01,00 --> 00:00:03,40
- [Instructor] Well congratulations on finishing the course.

2
00:00:03,40 --> 00:00:05,60
I hope you enjoyed learning about the modern features

3
00:00:05,60 --> 00:00:07,50
available to JavaScript developers

4
00:00:07,50 --> 00:00:09,30
in today's modern browsers.

5
00:00:09,30 --> 00:00:11,40
And that hopefully you're inspired to try some of these

6
00:00:11,40 --> 00:00:13,80
APIs out in your projects.

7
00:00:13,80 --> 00:00:15,60
Of course, there's a lot more to building great

8
00:00:15,60 --> 00:00:18,70
modern web apps than just using APIs,

9
00:00:18,70 --> 00:00:20,60
and I have a few suggestions on where you might

10
00:00:20,60 --> 00:00:22,50
want to go next.

11
00:00:22,50 --> 00:00:24,00
If you want to learn more about building

12
00:00:24,00 --> 00:00:26,70
modern, progressive web apps, then there are

13
00:00:26,70 --> 00:00:29,20
a couple of courses to consider watching:

14
00:00:29,20 --> 00:00:31,20
Building a Progressive Web App

15
00:00:31,20 --> 00:00:34,10
and Vanilla JavaScript: Service Workers.

16
00:00:34,10 --> 00:00:36,30
Each of which cover important topics

17
00:00:36,30 --> 00:00:39,20
that are key to building great progressive web apps.

18
00:00:39,20 --> 00:00:41,60
Of course, a web app can't be considered great

19
00:00:41,60 --> 00:00:43,60
unless it also has great performance,

20
00:00:43,60 --> 00:00:45,30
so I'd also suggest watching

21
00:00:45,30 --> 00:00:48,40
Learning Enterprise Web Application Performance

22
00:00:48,40 --> 00:00:50,40
which discusses the tools and techniques

23
00:00:50,40 --> 00:00:53,90
you need to know to get the best performance from your apps.

24
00:00:53,90 --> 00:00:56,10
Finally, if you liked what you saw in

25
00:00:56,10 --> 00:00:59,50
Using Visual Studio Code and want to learn more about it,

26
00:00:59,50 --> 00:01:00,90
then I suggest watching

27
00:01:00,90 --> 00:01:03,20
Visual Studio Code for Web Developers.

28
00:01:03,20 --> 00:01:06,20
And of course, remember to stay on top of new developments

29
00:01:06,20 --> 00:01:08,30
by checking out the emerging web standards

30
00:01:08,30 --> 00:01:11,80
at the W3C and WHATWG working groups.

31
00:01:11,80 --> 00:01:14,80
I hope to see you again in another one of my courses soon.

32
00:01:14,80 --> 00:01:17,00
Until then, happy coding!

