1
00:00:01,00 --> 00:00:01,90
- [Instructor] Alright, now let's learn

2
00:00:01,90 --> 00:00:03,50
about the Beacon API.

3
00:00:03,50 --> 00:00:05,90
So chances are, you've probably written code

4
00:00:05,90 --> 00:00:08,50
that makes use of Ajax to send data

5
00:00:08,50 --> 00:00:12,90
to a server using the XML http request object.

6
00:00:12,90 --> 00:00:14,50
This capability makes it possible

7
00:00:14,50 --> 00:00:16,50
to send all kinds of data to the server

8
00:00:16,50 --> 00:00:18,70
from your web app, including analytics

9
00:00:18,70 --> 00:00:21,20
and other kinds of diagnostic data.

10
00:00:21,20 --> 00:00:23,30
There are; however, some challenges involved

11
00:00:23,30 --> 00:00:25,80
when relying on Ajax for this.

12
00:00:25,80 --> 00:00:27,60
For example, it's not an easy task

13
00:00:27,60 --> 00:00:30,20
to send data during certain kinds of events,

14
00:00:30,20 --> 00:00:32,20
such as when the page is being unloaded

15
00:00:32,20 --> 00:00:35,20
because the browser will ignore those requests.

16
00:00:35,20 --> 00:00:37,90
You also want to make sure that your server communication

17
00:00:37,90 --> 00:00:41,10
doesn't interfere with the performance of your app.

18
00:00:41,10 --> 00:00:43,70
And this is where the Beacon API is really useful

19
00:00:43,70 --> 00:00:47,10
and can be a better alternative to using plain Ajax

20
00:00:47,10 --> 00:00:50,10
for data like analytics and diagnostics.

21
00:00:50,10 --> 00:00:52,90
It provides a way to send data to the server

22
00:00:52,90 --> 00:00:56,30
in situations where you don't expect to get a response.

23
00:00:56,30 --> 00:00:58,50
And it's done asynchronously,

24
00:00:58,50 --> 00:01:01,00
when the browser gets a chance to send the data

25
00:01:01,00 --> 00:01:04,10
during an idle moment during regular processing.

26
00:01:04,10 --> 00:01:07,00
The Beacon API is supported in all the latest versions

27
00:01:07,00 --> 00:01:09,40
of the major modern browsers.

28
00:01:09,40 --> 00:01:12,40
It consists of a single function named sendBeacon,

29
00:01:12,40 --> 00:01:15,20
which is found on the top level navigator object.

30
00:01:15,20 --> 00:01:18,20
The first argument is the URL to send the data to

31
00:01:18,20 --> 00:01:20,60
and the second argument is the data to send.

32
00:01:20,60 --> 00:01:23,20
And this data can either be an array buffer,

33
00:01:23,20 --> 00:01:26,70
a string, a blob object, or a form data.

34
00:01:26,70 --> 00:01:29,40
The function returns true, if the browser is able

35
00:01:29,40 --> 00:01:33,00
to schedule the data to be sent and false otherwise.

36
00:01:33,00 --> 00:01:34,90
You can learn about the Beacon API

37
00:01:34,90 --> 00:01:38,10
at this link on the W3C's website.

38
00:01:38,10 --> 00:01:40,60
So let's go over to the code and try this out.

39
00:01:40,60 --> 00:01:41,70
Alright, so here in my editor,

40
00:01:41,70 --> 00:01:45,30
I'm going to open up the Beacon start file.

41
00:01:45,30 --> 00:01:47,00
And in order to test this function,

42
00:01:47,00 --> 00:01:50,50
I'm going to use an online tool called PutsReq.

43
00:01:50,50 --> 00:01:52,90
So I'll bring that up here in my browser.

44
00:01:52,90 --> 00:01:55,70
And PutsReq is a tool that basically makes it easy for me

45
00:01:55,70 --> 00:01:58,50
to create a temporary end point

46
00:01:58,50 --> 00:02:01,00
that I can send data to, to make sure it's working.

47
00:02:01,00 --> 00:02:02,50
So if you want to follow along with me,

48
00:02:02,50 --> 00:02:05,20
then you can use this tool to create your own end point

49
00:02:05,20 --> 00:02:07,90
and use that URL in place of mine.

50
00:02:07,90 --> 00:02:11,30
So I'm going to create a PutsReq

51
00:02:11,30 --> 00:02:15,00
and I'm going to copy this link, okay?

52
00:02:15,00 --> 00:02:16,30
And I'll scroll down a little bit.

53
00:02:16,30 --> 00:02:19,10
And this is where the requests will show up, down here.

54
00:02:19,10 --> 00:02:22,60
So, back over in my code, what I'm going to do

55
00:02:22,60 --> 00:02:25,30
is paste that URL in here,

56
00:02:25,30 --> 00:02:28,20
which is where the end point's going to be.

57
00:02:28,20 --> 00:02:31,80
And I'll close that, so we have more room.

58
00:02:31,80 --> 00:02:34,80
Alright, so I'll save.

59
00:02:34,80 --> 00:02:37,40
Okay, so now that I've pasted the URL in,

60
00:02:37,40 --> 00:02:39,50
so you can see here in the code

61
00:02:39,50 --> 00:02:41,90
that I have several event handlers

62
00:02:41,90 --> 00:02:45,30
for the loading and the unloading of the page,

63
00:02:45,30 --> 00:02:48,50
along with two click handlers for buttons.

64
00:02:48,50 --> 00:02:52,20
And those buttons are right here, up in the mark up.

65
00:02:52,20 --> 00:02:55,10
So, let's fill out the function

66
00:02:55,10 --> 00:02:58,40
that will send data to our end point.

67
00:02:58,40 --> 00:03:01,40
So right here, in our send Beacon function,

68
00:03:01,40 --> 00:03:03,50
I'm using feature detection

69
00:03:03,50 --> 00:03:06,20
to make sure that the send Beacon API exists

70
00:03:06,20 --> 00:03:11,20
and if it does I will call navigator.sendBeacon

71
00:03:11,20 --> 00:03:15,90
and I will use the Beacon URL,

72
00:03:15,90 --> 00:03:20,00
which is defined right here, that's my PutsReq end point.

73
00:03:20,00 --> 00:03:21,80
And then the data I'm going to send

74
00:03:21,80 --> 00:03:25,20
is whatever the value of this strEvent argument is.

75
00:03:25,20 --> 00:03:29,70
So I'll send strEvent and let's save.

76
00:03:29,70 --> 00:03:32,20
So, in my various even handlers,

77
00:03:32,20 --> 00:03:34,90
I'll send strings like load and unload events

78
00:03:34,90 --> 00:03:37,80
and I have button one click and button two click.

79
00:03:37,80 --> 00:03:39,00
Alright, so let's try this out.

80
00:03:39,00 --> 00:03:43,80
I'm going to right click and choose open with live server.

81
00:03:43,80 --> 00:03:49,80
And I'll give the browser a moment to send the data

82
00:03:49,80 --> 00:03:54,40
and then let's go over to our PutsReq page.

83
00:03:54,40 --> 00:03:57,70
Alright, and here you can see our request.

84
00:03:57,70 --> 00:04:02,40
So, right there, you can see that the post to this URL

85
00:04:02,40 --> 00:04:06,10
was the load event and I'm sending back a nonsense response,

86
00:04:06,10 --> 00:04:07,40
but you can customize the response

87
00:04:07,40 --> 00:04:09,00
up here in the Java Script.

88
00:04:09,00 --> 00:04:12,30
And you can see that we've got our first event being sent.

89
00:04:12,30 --> 00:04:15,00
So now we're using the Beacon API

90
00:04:15,00 --> 00:04:18,80
to send data to, in this case, a temporary end point.

91
00:04:18,80 --> 00:04:20,40
But if this were your server,

92
00:04:20,40 --> 00:04:23,00
that's where you would send the data, alright?

93
00:04:23,00 --> 00:04:25,10
So here's the headers that were sent

94
00:04:25,10 --> 00:04:27,70
and you can see that the referrer

95
00:04:27,70 --> 00:04:31,20
is our local server right here.

96
00:04:31,20 --> 00:04:33,70
And the payload area shows our string.

97
00:04:33,70 --> 00:04:35,90
So let's go back to the page and now,

98
00:04:35,90 --> 00:04:39,50
let's go ahead and click on button one

99
00:04:39,50 --> 00:04:43,10
and then let's click on button two.

100
00:04:43,10 --> 00:04:47,10
And then we'll go back to the PutsReq page, alright.

101
00:04:47,10 --> 00:04:51,10
And now you can see that there's another event.

102
00:04:51,10 --> 00:04:53,60
So here's the button one click event

103
00:04:53,60 --> 00:04:56,50
and then I'll click on next again, alright.

104
00:04:56,50 --> 00:05:00,30
And there's the load event and if I go back,

105
00:05:00,30 --> 00:05:02,20
alright, there's button one click.

106
00:05:02,20 --> 00:05:04,10
And I'll go back again,

107
00:05:04,10 --> 00:05:08,50
and there is the button two click event, okay?

108
00:05:08,50 --> 00:05:11,10
So, you can see that this is a much simpler way

109
00:05:11,10 --> 00:05:13,10
of sending data to the server,

110
00:05:13,10 --> 00:05:15,60
than using full-blown Ajax.

111
00:05:15,60 --> 00:05:18,50
And it works in situations like page unload,

112
00:05:18,50 --> 00:05:19,70
where Ajax doesn't.

113
00:05:19,70 --> 00:05:22,50
So, just to show that, let's go back to the browser.

114
00:05:22,50 --> 00:05:26,00
So when I close this page, alright,

115
00:05:26,00 --> 00:05:29,50
let's go back here to the PutsReq

116
00:05:29,50 --> 00:05:34,00
and we'll give it a moment to record the event.

117
00:05:34,00 --> 00:05:37,10
So there's button two click, alright.

118
00:05:37,10 --> 00:05:38,00
And there you can see

119
00:05:38,00 --> 00:05:41,50
that now the unload event is being passed in.

120
00:05:41,50 --> 00:05:44,90
So, again, much simpler way of sending data to the server

121
00:05:44,90 --> 00:05:48,20
than using regular Ajax and unlike Ajax,

122
00:05:48,20 --> 00:05:51,20
it works in situations like page unload.

123
00:05:51,20 --> 00:05:53,00
Browsers ignore that even, because hey,

124
00:05:53,00 --> 00:05:56,70
why isn't it Ajax data if the page is about to be unloaded?

125
00:05:56,70 --> 00:06:00,10
So if you find a need to be sending analytics

126
00:06:00,10 --> 00:06:02,10
or diagnostics data to your server,

127
00:06:02,10 --> 00:06:05,00
consider using the Beacon API instead.

