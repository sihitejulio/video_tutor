1
00:00:01,10 --> 00:00:02,60
- [Instructor] In this example we're going to learn

2
00:00:02,60 --> 00:00:05,20
about a technique called link prefetching,

3
00:00:05,20 --> 00:00:07,70
which is a mechanism that the browser can use

4
00:00:07,70 --> 00:00:09,40
to start downloading pages

5
00:00:09,40 --> 00:00:12,10
that the user is likely to visit next.

6
00:00:12,10 --> 00:00:13,90
And there are a variety of scenarios

7
00:00:13,90 --> 00:00:16,00
where this ability is useful.

8
00:00:16,00 --> 00:00:18,70
So for example, if you display a list of search results

9
00:00:18,70 --> 00:00:21,70
to the user with links to more results,

10
00:00:21,70 --> 00:00:23,70
it's reasonable to assume that the user

11
00:00:23,70 --> 00:00:26,40
is likely to click on the next page link.

12
00:00:26,40 --> 00:00:29,60
Or if the user is filling out a purchase form on your site,

13
00:00:29,60 --> 00:00:31,40
then you can reasonably expect them

14
00:00:31,40 --> 00:00:33,20
to progress through the form.

15
00:00:33,20 --> 00:00:34,80
And you can indicate to the browser

16
00:00:34,80 --> 00:00:37,80
which resources are likely to be needed

17
00:00:37,80 --> 00:00:41,40
on the next navigation by using the standard link tag

18
00:00:41,40 --> 00:00:45,30
with a value of prefetch for the rel attribute.

19
00:00:45,30 --> 00:00:46,90
So let's see how this works.

20
00:00:46,90 --> 00:00:51,60
In my editor, I'm going to open prefetch_start

21
00:00:51,60 --> 00:00:54,00
and I'm going to bring this up in the browser

22
00:00:54,00 --> 00:00:55,30
so we can see the content,

23
00:00:55,30 --> 00:00:59,20
so I'll right click and choose the open with live server.

24
00:00:59,20 --> 00:01:00,70
So here's the page with my content

25
00:01:00,70 --> 00:01:03,00
and in this page I have a link

26
00:01:03,00 --> 00:01:05,00
to a destination page.

27
00:01:05,00 --> 00:01:10,50
So if I open the developer tools here in Chrome,

28
00:01:10,50 --> 00:01:11,80
and I'll choose the Network tab

29
00:01:11,80 --> 00:01:13,80
and let me reload this one more time,

30
00:01:13,80 --> 00:01:17,90
so you can see that the prefetch_start page is being loaded,

31
00:01:17,90 --> 00:01:21,80
but the destination page link is not being loaded, right?

32
00:01:21,80 --> 00:01:25,00
So just the, just the page that we're currently looking at.

33
00:01:25,00 --> 00:01:27,00
So when I click on the link,

34
00:01:27,00 --> 00:01:31,00
right, the destination page loads and everything is fine

35
00:01:31,00 --> 00:01:32,10
and over here in the network panel

36
00:01:32,10 --> 00:01:34,10
you can see that the prefetch page destination

37
00:01:34,10 --> 00:01:36,10
is now loaded.

38
00:01:36,10 --> 00:01:37,40
All right, so let's go back to the code.

39
00:01:37,40 --> 00:01:41,90
So what I'm going to do is add a link tag.

40
00:01:41,90 --> 00:01:47,70
I'm going to say link rel is equal to prefetch

41
00:01:47,70 --> 00:01:50,60
and then I'm going to specify the destination as

42
00:01:50,60 --> 00:01:52,30
that destination page.

43
00:01:52,30 --> 00:01:58,90
That's going to be prefetch page underscore page dot html.

44
00:01:58,90 --> 00:02:01,60
And I'll close the link.

45
00:02:01,60 --> 00:02:03,10
So now let's go back to the browser.

46
00:02:03,10 --> 00:02:06,30
And let's go back to our original page and I'll refresh.

47
00:02:06,30 --> 00:02:08,10
And now when I refresh the page,

48
00:02:08,10 --> 00:02:12,00
you can see that in the list of pages that were loaded,

49
00:02:12,00 --> 00:02:14,20
here's the start page right here,

50
00:02:14,20 --> 00:02:15,90
and here's the destination page,

51
00:02:15,90 --> 00:02:19,10
that's prefetch_page.html.

52
00:02:19,10 --> 00:02:21,60
So the browser is making a request

53
00:02:21,60 --> 00:02:23,90
for the prefetch page resource

54
00:02:23,90 --> 00:02:26,20
even though I haven't yet clicked on it.

55
00:02:26,20 --> 00:02:29,30
So what I've done is I've told the browser

56
00:02:29,30 --> 00:02:33,10
that this page is very likely to be visited

57
00:02:33,10 --> 00:02:36,50
so to go ahead and fetch that page while the user

58
00:02:36,50 --> 00:02:38,50
is still on this one.

59
00:02:38,50 --> 00:02:41,10
And this feature works with more than just pages.

60
00:02:41,10 --> 00:02:43,50
You can prefetch images, or scripts,

61
00:02:43,50 --> 00:02:45,70
or other resources as well.

62
00:02:45,70 --> 00:02:48,70
Now before you run off and start adding prefetch links

63
00:02:48,70 --> 00:02:50,30
to everything on your site,

64
00:02:50,30 --> 00:02:52,30
it's important to understand that this feature

65
00:02:52,30 --> 00:02:55,30
will only be a benefit to you if you take the time

66
00:02:55,30 --> 00:02:59,90
to understand which resources are in fact highly used.

67
00:02:59,90 --> 00:03:03,80
And that's why it's a good idea to profile your web app

68
00:03:03,80 --> 00:03:06,50
with analytics and other tools to make sure

69
00:03:06,50 --> 00:03:09,20
you're applying this technique to the right things.

70
00:03:09,20 --> 00:03:11,40
So you would use analytics to figure out,

71
00:03:11,40 --> 00:03:14,00
oh, well the user is likely to click on this link,

72
00:03:14,00 --> 00:03:17,30
so let me tell the browser to go ahead and prefetch it.

73
00:03:17,30 --> 00:03:19,40
So if you want to learn more about this,

74
00:03:19,40 --> 00:03:22,40
you can go over to the w3.org website

75
00:03:22,40 --> 00:03:24,60
and I'll going to pull that up here.

76
00:03:24,60 --> 00:03:26,90
You can see that the Resource Hints page is here.

77
00:03:26,90 --> 00:03:28,30
This feature is specified

78
00:03:28,30 --> 00:03:30,40
in the resource hints working draft

79
00:03:30,40 --> 00:03:33,00
and you can read more about it at this link.

