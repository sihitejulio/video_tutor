1
00:00:01,10 --> 00:00:03,80
- A close relative of prefetching data,

2
00:00:03,80 --> 00:00:05,70
which we saw in the previous example,

3
00:00:05,70 --> 00:00:07,80
is preloading data.

4
00:00:07,80 --> 00:00:09,60
Now, they may sound like similar operations,

5
00:00:09,60 --> 00:00:12,30
and they are, but there is an important difference.

6
00:00:12,30 --> 00:00:14,80
The prefetch directive we saw earlier

7
00:00:14,80 --> 00:00:17,60
gives a hint to the browser

8
00:00:17,60 --> 00:00:20,50
to fetch a resource that will likely be needed

9
00:00:20,50 --> 00:00:23,20
for the next navigation event.

10
00:00:23,20 --> 00:00:25,00
So for example, if we know that a user

11
00:00:25,00 --> 00:00:27,40
is very likely to visit a specific page

12
00:00:27,40 --> 00:00:29,50
after the one that they are currently on,

13
00:00:29,50 --> 00:00:31,80
because it's the next page of search results

14
00:00:31,80 --> 00:00:33,40
or the next page of a form,

15
00:00:33,40 --> 00:00:36,40
then we can ask the browser to get it in advance.

16
00:00:36,40 --> 00:00:39,70
Preload, on the other hand, has a similar syntax

17
00:00:39,70 --> 00:00:41,80
in that it also uses the link tag.

18
00:00:41,80 --> 00:00:44,20
However, you also need to specify

19
00:00:44,20 --> 00:00:46,60
what kind of resource it is.

20
00:00:46,60 --> 00:00:50,00
CSS, Script, Image, and so on,

21
00:00:50,00 --> 00:00:53,00
and this resource is very likely to be needed

22
00:00:53,00 --> 00:00:54,70
in the current page.

23
00:00:54,70 --> 00:00:58,10
So, let's take a look at a very simplified example.

24
00:00:58,10 --> 00:00:59,10
So, here are my code.

25
00:00:59,10 --> 00:01:03,70
I'm going to open up my preload start file.

26
00:01:03,70 --> 00:01:06,00
And, let's take a look at this in the browser first,

27
00:01:06,00 --> 00:01:08,10
so I'll bring this up in the live server.

28
00:01:08,10 --> 00:01:09,70
You can see, when I load the page,

29
00:01:09,70 --> 00:01:11,40
that there's an image here

30
00:01:11,40 --> 00:01:14,10
and back in the code you can see

31
00:01:14,10 --> 00:01:17,20
that it's actually a div element,

32
00:01:17,20 --> 00:01:19,70
which is this div element right here,

33
00:01:19,70 --> 00:01:23,10
and the div element has a style applied

34
00:01:23,10 --> 00:01:26,90
and the style definition has a background image.

35
00:01:26,90 --> 00:01:28,40
So, let's go back to the page

36
00:01:28,40 --> 00:01:30,70
and look at the network panel.

37
00:01:30,70 --> 00:01:35,50
So I'll bring up the Tools,

38
00:01:35,50 --> 00:01:36,70
and here's the network panel,

39
00:01:36,70 --> 00:01:39,10
and let me refresh the page.

40
00:01:39,10 --> 00:01:40,30
Okay.

41
00:01:40,30 --> 00:01:42,50
So, you can see that the page,

42
00:01:42,50 --> 00:01:46,60
when the page loads there's some processing that happens,

43
00:01:46,60 --> 00:01:50,90
and then the image loads after the CSS is parsed.

44
00:01:50,90 --> 00:01:53,90
Now, again, this is a fairly simple page,

45
00:01:53,90 --> 00:01:55,10
but you can imagine that

46
00:01:55,10 --> 00:01:57,20
if this were a complicated CSS style

47
00:01:57,20 --> 00:01:59,60
that wasn't loaded right away,

48
00:01:59,60 --> 00:02:03,70
there could be a significant delay in the image loading.

49
00:02:03,70 --> 00:02:05,70
And remember, this issue isn't limited to images,

50
00:02:05,70 --> 00:02:08,00
this can happen to script code, or style sheets,

51
00:02:08,00 --> 00:02:11,80
or other media resources that load after some point

52
00:02:11,80 --> 00:02:14,50
when the initial page gets brought up.

53
00:02:14,50 --> 00:02:15,80
So, to help with this,

54
00:02:15,80 --> 00:02:18,60
we can use the preload attribute,

55
00:02:18,60 --> 00:02:20,30
which essentially allows us

56
00:02:20,30 --> 00:02:22,60
to declare high-priority resources

57
00:02:22,60 --> 00:02:25,60
that the browser should load first.

58
00:02:25,60 --> 00:02:28,00
So I'm going to add my preload directive

59
00:02:28,00 --> 00:02:29,40
to the top of the page.

60
00:02:29,40 --> 00:02:31,30
So back in the code,

61
00:02:31,30 --> 00:02:33,80
and I'm going to write my link tag

62
00:02:33,80 --> 00:02:36,00
and, in this case the rel attribute

63
00:02:36,00 --> 00:02:38,40
is going to be preload,

64
00:02:38,40 --> 00:02:40,30
and then the href is going to be

65
00:02:40,30 --> 00:02:44,70
a link to my image.

66
00:02:44,70 --> 00:02:50,60
So that's going to be...oops...images...

67
00:02:50,60 --> 00:02:53,10
And that's going to be my tulips image.

68
00:02:53,10 --> 00:02:56,90
And then I have to say as=image,

69
00:02:56,90 --> 00:02:58,00
so I've got to tell the browser

70
00:02:58,00 --> 00:03:01,30
I'm loading this resource as an image.

71
00:03:01,30 --> 00:03:03,80
All right, so I'll save,

72
00:03:03,80 --> 00:03:07,30
and now let's go back and refresh the page again.

73
00:03:07,30 --> 00:03:08,60
So, when I refresh,

74
00:03:08,60 --> 00:03:10,40
you can see now that the image has loaded

75
00:03:10,40 --> 00:03:13,20
much, much sooner in the process.

76
00:03:13,20 --> 00:03:15,50
So here, the style sheet got loaded and parsed,

77
00:03:15,50 --> 00:03:19,30
and the image got loaded almost immediately.

78
00:03:19,30 --> 00:03:21,50
So, when the CSS is being parsed,

79
00:03:21,50 --> 00:03:22,90
and the image is discovered,

80
00:03:22,90 --> 00:03:24,90
my link tag has told the browser

81
00:03:24,90 --> 00:03:26,00
to already go out and get it

82
00:03:26,00 --> 00:03:29,00
because I know it's going to be used in the page.

83
00:03:29,00 --> 00:03:31,70
So, that dramatically speeds up the loading process

84
00:03:31,70 --> 00:03:33,80
for that particular resource.

85
00:03:33,80 --> 00:03:35,40
Now, does that mean that you should

86
00:03:35,40 --> 00:03:37,00
just preload all the things,

87
00:03:37,00 --> 00:03:40,10
and then you don't have to worry about loading performance?

88
00:03:40,10 --> 00:03:41,20
No, of course not.

89
00:03:41,20 --> 00:03:43,90
You're web app will be different than everyone else's,

90
00:03:43,90 --> 00:03:46,50
and that's why it's important to use the browser tools,

91
00:03:46,50 --> 00:03:47,80
like this network panel,

92
00:03:47,80 --> 00:03:50,90
to figure out what makes sense for your app.

93
00:03:50,90 --> 00:03:52,40
Preload can be a great way

94
00:03:52,40 --> 00:03:54,30
to increase the performance of your app,

95
00:03:54,30 --> 00:03:56,50
but you need to use it properly.

96
00:03:56,50 --> 00:03:59,40
It turns out that there is a W3C spec for this.

97
00:03:59,40 --> 00:04:02,30
So, if you want to learn more about this capability,

98
00:04:02,30 --> 00:04:04,00
you can check out the spec at this link.

