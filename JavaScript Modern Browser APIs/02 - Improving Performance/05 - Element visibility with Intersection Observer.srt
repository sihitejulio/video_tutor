1
00:00:01,10 --> 00:00:02,70
- [Instructor] It's a fairly common design pattern

2
00:00:02,70 --> 00:00:06,10
in modern web apps to detect when an element on the page

3
00:00:06,10 --> 00:00:09,00
is actually visible to the user.

4
00:00:09,00 --> 00:00:10,50
You can use this kind of detection

5
00:00:10,50 --> 00:00:13,20
to delay the loading of images until they're visible,

6
00:00:13,20 --> 00:00:17,20
or to start and stop a video, or to see if an ad

7
00:00:17,20 --> 00:00:19,50
has actually been viewed by the user, and so on.

8
00:00:19,50 --> 00:00:22,30
There's a variety of use cases for this.

9
00:00:22,30 --> 00:00:24,80
The IntersectionObserver API makes this kind of scenario

10
00:00:24,80 --> 00:00:27,60
much easier to accomplish in a way

11
00:00:27,60 --> 00:00:30,50
that doesn't degrade the browser's performance.

12
00:00:30,50 --> 00:00:32,60
So consider an example where we have a webpage

13
00:00:32,60 --> 00:00:35,50
with some content in it, and an element,

14
00:00:35,50 --> 00:00:39,10
let's call it Element A, that's down in the page somewhere.

15
00:00:39,10 --> 00:00:41,50
So in this example, the element is not visible

16
00:00:41,50 --> 00:00:44,50
when the page first loads, but when the user

17
00:00:44,50 --> 00:00:47,40
scrolls the page content and reveals Element A,

18
00:00:47,40 --> 00:00:50,00
we want to be able to detect that.

19
00:00:50,00 --> 00:00:53,00
This is what the IntersectionObserver API does.

20
00:00:53,00 --> 00:00:55,10
So let's take it for a spin in our code editor

21
00:00:55,10 --> 00:00:57,50
and exercise it a little bit.

22
00:00:57,50 --> 00:00:59,50
So here in my editor, I'm going to open up

23
00:00:59,50 --> 00:01:04,90
intersection_start.html, and I'll hide the files

24
00:01:04,90 --> 00:01:06,70
so we have some more room.

25
00:01:06,70 --> 00:01:10,30
And you can see that it contains a bunch of placeholder text

26
00:01:10,30 --> 00:01:14,60
along with an element that's down here in the content,

27
00:01:14,60 --> 00:01:16,80
and that's this element right here.

28
00:01:16,80 --> 00:01:19,30
So we want to write some code that detects

29
00:01:19,30 --> 00:01:23,30
when this div element is visible to the user.

30
00:01:23,30 --> 00:01:27,30
So let's start by implementing a basic observer.

31
00:01:27,30 --> 00:01:29,60
And this will tell me when the element becomes visible

32
00:01:29,60 --> 00:01:31,50
or is no longer visible.

33
00:01:31,50 --> 00:01:33,10
So to do that, I need to create

34
00:01:33,10 --> 00:01:35,20
an IntersectionObserver object,

35
00:01:35,20 --> 00:01:40,00
so I'll write const observer and I'll create

36
00:01:40,00 --> 00:01:42,80
a new IntersectionObserver.

37
00:01:42,80 --> 00:01:44,40
And it takes two arguments.

38
00:01:44,40 --> 00:01:47,30
The first argument is a callback function,

39
00:01:47,30 --> 00:01:51,50
and that takes one argument.

40
00:01:51,50 --> 00:01:53,70
And inside my callback, I'm just, for now,

41
00:01:53,70 --> 00:01:59,40
going to log out to the console the entries argument.

42
00:01:59,40 --> 00:02:02,80
So the callback function is going to be invoked

43
00:02:02,80 --> 00:02:06,10
when any of our observed elements enters

44
00:02:06,10 --> 00:02:09,70
or leaves the viewport, and this entries element contains

45
00:02:09,70 --> 00:02:13,00
information about the elements that we're observing.

46
00:02:13,00 --> 00:02:14,90
The second argument is a set of options,

47
00:02:14,90 --> 00:02:16,20
and I'm going to leave that out for now

48
00:02:16,20 --> 00:02:18,80
and just use the default values.

49
00:02:18,80 --> 00:02:20,60
So once I've created the observer,

50
00:02:20,60 --> 00:02:24,30
I can then ask it to start observing elements on the page.

51
00:02:24,30 --> 00:02:28,60
So I'm going to write observer.observe,

52
00:02:28,60 --> 00:02:31,90
and I want it to observe this element,

53
00:02:31,90 --> 00:02:39,00
so it has an ID, so I'll write document.getElementById,

54
00:02:39,00 --> 00:02:42,40
and that's going to be targetElem.

55
00:02:42,40 --> 00:02:43,80
Okay, so we're all set.

56
00:02:43,80 --> 00:02:45,70
Let's run this and see what happens.

57
00:02:45,70 --> 00:02:48,30
So I'll bring this up in our live server,

58
00:02:48,30 --> 00:02:50,80
and I'll bring up the developer tools

59
00:02:50,80 --> 00:02:53,10
so we can see the console.

60
00:02:53,10 --> 00:02:55,90
And you can see that there's already one entry

61
00:02:55,90 --> 00:02:59,10
in the console, and that's because when the page loaded,

62
00:02:59,10 --> 00:03:02,90
the element that I'm viewing is not visible.

63
00:03:02,90 --> 00:03:04,00
So if we look at the data,

64
00:03:04,00 --> 00:03:06,30
we can see isIntersecting is false.

65
00:03:06,30 --> 00:03:09,20
So it's not visible right now, okay?

66
00:03:09,20 --> 00:03:12,00
So I'll start scrolling the page,

67
00:03:12,00 --> 00:03:13,70
and when I scroll the page, you can see that

68
00:03:13,70 --> 00:03:16,10
when the element becomes visible I get

69
00:03:16,10 --> 00:03:19,50
another entry here in the console,

70
00:03:19,50 --> 00:03:23,00
and we look at the data, we can see the results,

71
00:03:23,00 --> 00:03:24,40
and so right here we can see

72
00:03:24,40 --> 00:03:27,90
that isIntersecting is now true, right,

73
00:03:27,90 --> 00:03:30,90
so we can see that there is the true value

74
00:03:30,90 --> 00:03:33,70
means that it's now visible in the viewport,

75
00:03:33,70 --> 00:03:36,70
and we can see how much of the element is visible

76
00:03:36,70 --> 00:03:39,10
by looking at the intersectionRect

77
00:03:39,10 --> 00:03:41,90
or the intersectionRatio.

78
00:03:41,90 --> 00:03:43,50
And we can see that the intersectionRatio,

79
00:03:43,50 --> 00:03:47,50
that's about, hm, maybe 3% or so of the element

80
00:03:47,50 --> 00:03:52,60
was visible when the IntersectionObserver fired.

81
00:03:52,60 --> 00:03:54,90
So this is interesting, but we can actually

82
00:03:54,90 --> 00:03:58,40
get more fine-grained control than this.

83
00:03:58,40 --> 00:04:03,10
We can give the observer a set of threshold ratios

84
00:04:03,10 --> 00:04:04,90
that we want to serve as trigger points

85
00:04:04,90 --> 00:04:08,70
as the element comes into and goes out of view.

86
00:04:08,70 --> 00:04:10,50
So in other words, we can ask the observer

87
00:04:10,50 --> 00:04:13,00
to trigger when different portion sizes

88
00:04:13,00 --> 00:04:15,40
of the element are visible.

89
00:04:15,40 --> 00:04:18,80
So to do this, we give the observer an array of numbers

90
00:04:18,80 --> 00:04:22,30
as the second option in the constructor.

91
00:04:22,30 --> 00:04:23,70
So let's go back to the code,

92
00:04:23,70 --> 00:04:27,60
and for the second argument to the creation function,

93
00:04:27,60 --> 00:04:30,80
I'm going to give it an options object,

94
00:04:30,80 --> 00:04:34,30
and that's going to be the threshold property,

95
00:04:34,30 --> 00:04:37,10
and I'll go in and specify an array of values

96
00:04:37,10 --> 00:04:44,50
of zero, 0.5, and 1.0.

97
00:04:44,50 --> 00:04:47,20
So now I want to trigger the IntersectionObserver

98
00:04:47,20 --> 00:04:54,30
when zero, 50%, and 100% of the element are visible.

99
00:04:54,30 --> 00:04:55,90
And you might have noticed that

100
00:04:55,90 --> 00:04:58,60
I had some CSS styles declared back up

101
00:04:58,60 --> 00:05:01,40
in the top of my document labeled, you know,

102
00:05:01,40 --> 00:05:04,20
some background, and then some number,

103
00:05:04,20 --> 00:05:06,40
so what I'm going to do is apply a different

104
00:05:06,40 --> 00:05:08,30
background color to the element

105
00:05:08,30 --> 00:05:11,10
as more or less of it is visible.

106
00:05:11,10 --> 00:05:15,90
So let's go back down to our observer code.

107
00:05:15,90 --> 00:05:18,30
And in addition to running out to the console,

108
00:05:18,30 --> 00:05:27,80
I'm going to write if (entries[0].interesctionRatio,

109
00:05:27,80 --> 00:05:30,20
so for the case where it's less than .5,

110
00:05:30,20 --> 00:05:37,90
I'm going to apply a background color to entries[0],

111
00:05:37,90 --> 00:05:39,70
and I've only got one entry, right,

112
00:05:39,70 --> 00:05:42,70
that's why I'm using this zero suffix.

113
00:05:42,70 --> 00:05:50,80
So entries[0].target.className is going to be background0,

114
00:05:50,80 --> 00:05:55,70
and then I'll copy and paste this a couple times.

115
00:05:55,70 --> 00:05:59,60
So for the case where it's greater than or equal to one,

116
00:05:59,60 --> 00:06:06,40
that would be background 100, and if it's greater than

117
00:06:06,40 --> 00:06:20,70
or equal to 0.5 and if it's going to be less than one,

118
00:06:20,70 --> 00:06:23,80
I'll apply background 50.

119
00:06:23,80 --> 00:06:25,50
So now I've got backgrounds for 0%,

120
00:06:25,50 --> 00:06:29,80
50%, and 100% visibility, all right?

121
00:06:29,80 --> 00:06:31,30
So let's go back to the browser,

122
00:06:31,30 --> 00:06:34,00
and scroll back up, and let's reload.

123
00:06:34,00 --> 00:06:39,00
All right, and now as I scroll the element into view,

124
00:06:39,00 --> 00:06:42,40
you can see that as it barely becomes visible

125
00:06:42,40 --> 00:06:44,80
it's got a background of zero,

126
00:06:44,80 --> 00:06:46,70
and then when half of it becomes visible,

127
00:06:46,70 --> 00:06:49,30
the background changes to light blue,

128
00:06:49,30 --> 00:06:52,60
and then when it's fully visible it becomes dark blue.

129
00:06:52,60 --> 00:06:56,50
And then vice versa, as I scroll it back out of sight again.

130
00:06:56,50 --> 00:06:58,80
Now, it's important to point out here

131
00:06:58,80 --> 00:07:01,60
that IntersectionObserver is not intended

132
00:07:01,60 --> 00:07:05,90
to be low-latency or pixel-perfect,

133
00:07:05,90 --> 00:07:08,10
and that means that you can't expect it to fire immediately

134
00:07:08,10 --> 00:07:11,10
when the element visibility changes.

135
00:07:11,10 --> 00:07:12,90
So you shouldn't do things like base

136
00:07:12,90 --> 00:07:16,30
scroll value animations on it or anything

137
00:07:16,30 --> 00:07:19,00
that needs to be super low latency.

138
00:07:19,00 --> 00:07:21,50
You can learn more about this API by visiting

139
00:07:21,50 --> 00:07:26,00
the W3C specification for it, located at this link.

