1
00:00:01,00 --> 00:00:02,80
- In this chapter we're going to be looking at

2
00:00:02,80 --> 00:00:06,50
APIs that help you build web apps with better performance.

3
00:00:06,50 --> 00:00:07,30
And we are going to start off

4
00:00:07,30 --> 00:00:08,80
with a relatively simple API

5
00:00:08,80 --> 00:00:12,70
that improves the quality of animations in a webpage.

6
00:00:12,70 --> 00:00:14,90
So I'm going to open the finished example here

7
00:00:14,90 --> 00:00:17,40
using my live server extension.

8
00:00:17,40 --> 00:00:21,00
And you can see that it's just a simple animation

9
00:00:21,00 --> 00:00:23,50
in a canvas element on the webpage.

10
00:00:23,50 --> 00:00:25,40
So the API we're going to use is called

11
00:00:25,40 --> 00:00:26,90
request animation frame.

12
00:00:26,90 --> 00:00:30,60
And it's used to sync up the display refresh cycle

13
00:00:30,60 --> 00:00:33,50
with animation code in the browser.

14
00:00:33,50 --> 00:00:35,30
And before this API was available,

15
00:00:35,30 --> 00:00:37,80
developers had to use either set timeout

16
00:00:37,80 --> 00:00:41,40
or set interval in order to animate page content.

17
00:00:41,40 --> 00:00:43,70
The downside of that approach is that these functions

18
00:00:43,70 --> 00:00:45,30
are not very accurate

19
00:00:45,30 --> 00:00:48,60
and are not synchronized with the display refresh rate.

20
00:00:48,60 --> 00:00:50,80
So the result of this is that calls to set timeout

21
00:00:50,80 --> 00:00:54,10
or set interval might either be duplicated or dropped

22
00:00:54,10 --> 00:00:55,70
during screen paint cycles,

23
00:00:55,70 --> 00:00:58,80
which will produce a stuttering effect.

24
00:00:58,80 --> 00:01:00,80
Request animation frame fixes that problem

25
00:01:00,80 --> 00:01:02,40
by ensuring that it is only called once

26
00:01:02,40 --> 00:01:04,60
per display refresh cycle.

27
00:01:04,60 --> 00:01:07,70
So let's open the editor and build this example.

28
00:01:07,70 --> 00:01:11,60
Alright, so here I'm going to open up animation start.html.

29
00:01:11,60 --> 00:01:14,70
And you can see my page here has a canvas element

30
00:01:14,70 --> 00:01:16,80
which is right down here.

31
00:01:16,80 --> 00:01:18,70
Okay, here's my canvas.

32
00:01:18,70 --> 00:01:20,90
And it has some code that sets up

33
00:01:20,90 --> 00:01:24,70
the drawing environment, gets a reference to the canvas,

34
00:01:24,70 --> 00:01:26,90
initializes some content.

35
00:01:26,90 --> 00:01:30,30
And if you scroll down a little bit further,

36
00:01:30,30 --> 00:01:32,30
here's my initialization function.

37
00:01:32,30 --> 00:01:33,60
And if we scroll down

38
00:01:33,60 --> 00:01:37,50
there's an anim function and this anim function

39
00:01:37,50 --> 00:01:41,60
is called repeatedly to create the animation.

40
00:01:41,60 --> 00:01:45,40
So first, in the init function what I'm going to do is

41
00:01:45,40 --> 00:01:49,30
call the request animation frame function

42
00:01:49,30 --> 00:01:54,00
and assign the result to my animationRef variable here.

43
00:01:54,00 --> 00:01:56,40
And that will become clear a little bit later.

44
00:01:56,40 --> 00:02:00,30
So for now I'm going to say animationRef is equal to

45
00:02:00,30 --> 00:02:04,80
and I'm going to call request animation frame.

46
00:02:04,80 --> 00:02:06,80
And then I'm going to call the anim function.

47
00:02:06,80 --> 00:02:08,80
So it's kind of like set interval

48
00:02:08,80 --> 00:02:12,10
except that you don't specify a timing interval.

49
00:02:12,10 --> 00:02:13,90
The request animation frame just syncs up

50
00:02:13,90 --> 00:02:15,60
with whatever the display cycle is.

51
00:02:15,60 --> 00:02:18,20
Alright, so this starts the process.

52
00:02:18,20 --> 00:02:20,30
And we can use the reference value

53
00:02:20,30 --> 00:02:23,60
for this function to stop the animation later.

54
00:02:23,60 --> 00:02:25,90
So what we're going to do now is

55
00:02:25,90 --> 00:02:29,00
I'm going to have to call request animation frame again

56
00:02:29,00 --> 00:02:32,20
from within the animation function.

57
00:02:32,20 --> 00:02:33,90
And I'm not going to go deep into the animation code.

58
00:02:33,90 --> 00:02:36,10
You can follow it later on in your, or debunker it

59
00:02:36,10 --> 00:02:37,20
if you want to.

60
00:02:37,20 --> 00:02:40,00
What I'm going to do is, once again,

61
00:02:40,00 --> 00:02:44,60
animationRef is equal to request animation frame.

62
00:02:44,60 --> 00:02:46,50
And this time I'm going to have my animation function

63
00:02:46,50 --> 00:02:48,80
call itself.

64
00:02:48,80 --> 00:02:52,60
Now to stop the animation I call cancel animation frame

65
00:02:52,60 --> 00:02:54,70
with this reference value.

66
00:02:54,70 --> 00:02:57,20
So let's go ahead and scroll up

67
00:02:57,20 --> 00:03:00,40
and I have a button here that says stop animation

68
00:03:00,40 --> 00:03:02,30
and here's my event handler.

69
00:03:02,30 --> 00:03:05,70
So to stop the animation what I'm going to do is call

70
00:03:05,70 --> 00:03:08,60
cancel animation frame

71
00:03:08,60 --> 00:03:13,80
and I have my animationRef variable that I pass in.

72
00:03:13,80 --> 00:03:15,80
That's pretty much all there is to it.

73
00:03:15,80 --> 00:03:20,30
So let's go ahead and run this.

74
00:03:20,30 --> 00:03:23,20
And there you can see the animation is running

75
00:03:23,20 --> 00:03:24,50
and I can click stop

76
00:03:24,50 --> 00:03:25,90
and the animation stops.

77
00:03:25,90 --> 00:03:30,00
So if I refresh animation starts all over again, right?

78
00:03:30,00 --> 00:03:33,30
So using request animation frame is much more efficient

79
00:03:33,30 --> 00:03:34,70
that using set interval

80
00:03:34,70 --> 00:03:37,50
and it's supported in all of the major browsers

81
00:03:37,50 --> 00:03:39,40
so there's not reason not to use it.

82
00:03:39,40 --> 00:03:41,90
If you're building applications that need to use animation,

83
00:03:41,90 --> 00:03:45,00
you should be using request animation frame from now on.

