1
00:00:01,00 --> 00:00:02,40
- [Instructor] Okay, let's pick up where we left off

2
00:00:02,40 --> 00:00:03,70
on our last example

3
00:00:03,70 --> 00:00:06,30
by adding the ability to customize

4
00:00:06,30 --> 00:00:08,30
the image that gets drawn

5
00:00:08,30 --> 00:00:10,90
by using CSS custom properties.

6
00:00:10,90 --> 00:00:12,20
So, here in my editor,

7
00:00:12,20 --> 00:00:13,50
I'm going to open up the

8
00:00:13,50 --> 00:00:18,60
CSS start html and CSS paint start JS files,

9
00:00:18,60 --> 00:00:21,10
and you can see this is where we left off.

10
00:00:21,10 --> 00:00:23,40
So, what I need to do first

11
00:00:23,40 --> 00:00:26,60
is define two custom properties in my style sheet

12
00:00:26,60 --> 00:00:28,10
that are going to represent the thickness

13
00:00:28,10 --> 00:00:29,90
and color of the lines.

14
00:00:29,90 --> 00:00:36,30
So, I'm going to use dash dash cross thickness,

15
00:00:36,30 --> 00:00:38,50
and I'll give that a value of two,

16
00:00:38,50 --> 00:00:43,60
and then I'll define cross color, which will be red.

17
00:00:43,60 --> 00:00:46,70
Then in my painter class definition,

18
00:00:46,70 --> 00:00:49,10
I have to define a static method

19
00:00:49,10 --> 00:00:53,20
that declares what properties my class uses.

20
00:00:53,20 --> 00:00:57,50
So, I'll write a static getter,

21
00:00:57,50 --> 00:01:04,40
and it's called input properties.

22
00:01:04,40 --> 00:01:08,80
This method is going to return an array

23
00:01:08,80 --> 00:01:10,70
that identifies the name of the properties

24
00:01:10,70 --> 00:01:12,90
that I want to access from CSS.

25
00:01:12,90 --> 00:01:18,00
So, the first one is dash dash cross thickness,

26
00:01:18,00 --> 00:01:21,50
and the second one is, dash dash cross color.

27
00:01:21,50 --> 00:01:23,70
Then, I just need to retrieve those values

28
00:01:23,70 --> 00:01:25,40
in my paint function,

29
00:01:25,40 --> 00:01:28,70
and here's where this props argument comes in.

30
00:01:28,70 --> 00:01:30,00
So, what I'm going to do

31
00:01:30,00 --> 00:01:35,80
is write that width equals

32
00:01:35,80 --> 00:01:38,40
props dot get,

33
00:01:38,40 --> 00:01:44,10
and I'm going to write dash dash cross thickness,

34
00:01:44,10 --> 00:01:48,40
and then I'm going to write let color equal props dot get,

35
00:01:48,40 --> 00:01:51,60
and in this case, I'm going to use the

36
00:01:51,60 --> 00:01:56,70
cross-color property,

37
00:01:56,70 --> 00:02:00,70
and in the case of the color, I have to write to string.

38
00:02:00,70 --> 00:02:03,00
So, the way that these properties are specified,

39
00:02:03,00 --> 00:02:04,60
along with units and all that,

40
00:02:04,60 --> 00:02:08,80
that's still in the process of being specified.

41
00:02:08,80 --> 00:02:09,60
For the most part,

42
00:02:09,60 --> 00:02:12,40
you can assume that they're going to be strings,

43
00:02:12,40 --> 00:02:15,70
and I'm not going to get too deeply into how the

44
00:02:15,70 --> 00:02:18,00
CSS typed object model works just yet

45
00:02:18,00 --> 00:02:20,10
because that's another Houdini spec

46
00:02:20,10 --> 00:02:21,80
that's still being written,

47
00:02:21,80 --> 00:02:23,50
but for the moment, you can get these properties

48
00:02:23,50 --> 00:02:26,10
just using this getter function right here.

49
00:02:26,10 --> 00:02:29,10
So now that I've got my width and color,

50
00:02:29,10 --> 00:02:31,50
I can parametrize that

51
00:02:31,50 --> 00:02:38,80
and set my local variables to be width and color.

52
00:02:38,80 --> 00:02:40,70
Okay, so now I have the ability

53
00:02:40,70 --> 00:02:42,70
to specify values in the style sheet,

54
00:02:42,70 --> 00:02:45,40
so let's go ahead and back to the browser,

55
00:02:45,40 --> 00:02:52,20
and I'm going to open this file in my live server,

56
00:02:52,20 --> 00:02:55,30
and now you can see that the colors are red,

57
00:02:55,30 --> 00:02:57,30
and the thickness is still two,

58
00:02:57,30 --> 00:03:00,10
but now because I've got these properties,

59
00:03:00,10 --> 00:03:02,00
let's go ahead and open the developer tools,

60
00:03:02,00 --> 00:03:03,90
and go to the elements panel,

61
00:03:03,90 --> 00:03:05,70
and then here in the style sheet,

62
00:03:05,70 --> 00:03:09,10
what I can do is select this element,

63
00:03:09,10 --> 00:03:11,50
and you can see now that I've got my properties here

64
00:03:11,50 --> 00:03:12,80
in the inspector,

65
00:03:12,80 --> 00:03:14,90
so I can just modify these on the fly.

66
00:03:14,90 --> 00:03:16,20
So, I can change this to five,

67
00:03:16,20 --> 00:03:17,90
and you can see the thickness changes.

68
00:03:17,90 --> 00:03:20,40
I can make this purple, oops, let me, hang on a second,

69
00:03:20,40 --> 00:03:22,00
double click that, there we go.

70
00:03:22,00 --> 00:03:23,90
I'll change that to purple,

71
00:03:23,90 --> 00:03:25,70
and you can see the color is changing.

72
00:03:25,70 --> 00:03:29,50
So, I can use the built in browser tools

73
00:03:29,50 --> 00:03:31,80
to change the values in the style sheet,

74
00:03:31,80 --> 00:03:33,60
and you can see that my JavaScript class

75
00:03:33,60 --> 00:03:35,40
is picking up those values

76
00:03:35,40 --> 00:03:37,10
and using them on the fly.

77
00:03:37,10 --> 00:03:40,20
So, this is a really exciting new area for developers,

78
00:03:40,20 --> 00:03:41,70
and I encourage you to check out

79
00:03:41,70 --> 00:03:45,00
and follow the Houdini project as it progresses.

