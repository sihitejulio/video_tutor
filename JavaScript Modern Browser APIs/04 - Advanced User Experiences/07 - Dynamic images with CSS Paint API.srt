1
00:00:01,00 --> 00:00:01,90
- [Instructor] What I'm about to say

2
00:00:01,90 --> 00:00:03,90
might surprise you, but there's a very important

3
00:00:03,90 --> 00:00:06,30
area of web development that hasn't really

4
00:00:06,30 --> 00:00:07,60
seen a whole lot of innovation

5
00:00:07,60 --> 00:00:11,20
for JavaScript programmers, and that's CSS.

6
00:00:11,20 --> 00:00:14,20
Now of course, CSS has added a ton of features

7
00:00:14,20 --> 00:00:16,90
for designers over the years, and most of those

8
00:00:16,90 --> 00:00:19,20
are accessible to developers.

9
00:00:19,20 --> 00:00:21,10
But JavaScript coders haven't had access

10
00:00:21,10 --> 00:00:24,00
to the underlying CSS engine itself

11
00:00:24,00 --> 00:00:26,70
in order to customize how things work.

12
00:00:26,70 --> 00:00:29,40
A new effort called CSS Houdini

13
00:00:29,40 --> 00:00:31,20
aims to change all of that.

14
00:00:31,20 --> 00:00:33,00
And some of the first efforts to bear fruit

15
00:00:33,00 --> 00:00:35,80
are showing some real promise and excitement.

16
00:00:35,80 --> 00:00:37,00
At the time of this recording,

17
00:00:37,00 --> 00:00:39,50
it's still pretty new and under development,

18
00:00:39,50 --> 00:00:43,50
and if you go to the css-houdini.org page,

19
00:00:43,50 --> 00:00:47,20
you'll be taken to the Houdini homepage here

20
00:00:47,20 --> 00:00:48,90
where you can learn more about the features

21
00:00:48,90 --> 00:00:51,10
and ideas that are being explored.

22
00:00:51,10 --> 00:00:53,40
So I suggest you take a few minutes

23
00:00:53,40 --> 00:00:57,50
and look through the ideas and the specs area on the site.

24
00:00:57,50 --> 00:00:59,50
And there's also a handy chart

25
00:00:59,50 --> 00:01:02,80
of Houdini features at this link,

26
00:01:02,80 --> 00:01:04,90
which tracks the progress that's being made

27
00:01:04,90 --> 00:01:09,10
in each of the features across the various browsers.

28
00:01:09,10 --> 00:01:10,60
The first of these to reach candidate

29
00:01:10,60 --> 00:01:13,10
recommendation status and actually ship

30
00:01:13,10 --> 00:01:16,50
is the CSS Paint API, which we'll take a look at

31
00:01:16,50 --> 00:01:17,80
in this example.

32
00:01:17,80 --> 00:01:20,60
Using CSS Paint, you can programmatically

33
00:01:20,60 --> 00:01:23,70
create an image anywhere that a CSS property

34
00:01:23,70 --> 00:01:27,00
expects an image, such as the background image property.

35
00:01:27,00 --> 00:01:30,40
And this opens up a large number of possibilities.

36
00:01:30,40 --> 00:01:32,60
So let's go over to the code and try this out.

37
00:01:32,60 --> 00:01:34,30
So here in the editor, I'm going to open up

38
00:01:34,30 --> 00:01:39,20
the csspaint_start and csspaint JS file.

39
00:01:39,20 --> 00:01:40,40
So what we're going to do in this example

40
00:01:40,40 --> 00:01:43,40
is create a JavaScript class that dynamically

41
00:01:43,40 --> 00:01:47,10
generates an image for use in a CSS style sheet.

42
00:01:47,10 --> 00:01:49,00
And the way that this works is we're going to

43
00:01:49,00 --> 00:01:51,10
create something called a paint worklet

44
00:01:51,10 --> 00:01:53,00
that will do the drawing.

45
00:01:53,00 --> 00:01:56,00
And by the way, this uses the HTML Canvas API,

46
00:01:56,00 --> 00:01:58,30
so if you're not familiar with Canvas, I suggest you go

47
00:01:58,30 --> 00:02:02,30
take a look at the Learning HTML Canvas course.

48
00:02:02,30 --> 00:02:04,50
Alright, so step number one is to see

49
00:02:04,50 --> 00:02:06,50
if paint worklets are supported,

50
00:02:06,50 --> 00:02:08,90
and then register our painting module.

51
00:02:08,90 --> 00:02:12,10
So here in the HTML file, in the code

52
00:02:12,10 --> 00:02:15,30
for the window event listener, I already have

53
00:02:15,30 --> 00:02:18,30
some code to check to see if the paintWorklet

54
00:02:18,30 --> 00:02:21,40
is in the CSS object.

55
00:02:21,40 --> 00:02:26,30
And if it is, I'm going to call a addModule function

56
00:02:26,30 --> 00:02:29,00
to add our painting module, and the way I do that

57
00:02:29,00 --> 00:02:35,70
is by saying CSS.paintWorklet.addModule,

58
00:02:35,70 --> 00:02:37,40
and then I have to provide the name

59
00:02:37,40 --> 00:02:40,10
of our JavaScript file that implements the module,

60
00:02:40,10 --> 00:02:46,50
and that is going to be csspaint_start.js.

61
00:02:46,50 --> 00:02:48,40
Alright, so I'll save that.

62
00:02:48,40 --> 00:02:50,30
And otherwise, I'll just log out to the console

63
00:02:50,30 --> 00:02:53,50
that paintworklet is not supported here.

64
00:02:53,50 --> 00:02:56,00
So here, I've given as the name of the module

65
00:02:56,00 --> 00:03:00,90
the csspaint_start.js, so let's fill that out next.

66
00:03:00,90 --> 00:03:04,30
So I'll go over to the JavaScript code for the module.

67
00:03:04,30 --> 00:03:05,90
So here in this module, I need to see

68
00:03:05,90 --> 00:03:08,40
if the registerPaint function exists,

69
00:03:08,40 --> 00:03:11,30
and if it does, then I'm going to create a class

70
00:03:11,30 --> 00:03:13,50
that does the actual painting.

71
00:03:13,50 --> 00:03:16,70
So my class named SampleCSSPaint

72
00:03:16,70 --> 00:03:20,50
has a method named paint that does all the work.

73
00:03:20,50 --> 00:03:23,10
So this function will be given a drawing service

74
00:03:23,10 --> 00:03:27,10
that is essentially an HTML Canvas object.

75
00:03:27,10 --> 00:03:29,80
So this function will be Canvas context,

76
00:03:29,80 --> 00:03:32,30
its size, and some properties,

77
00:03:32,30 --> 00:03:34,20
and we'll come back to the properties later.

78
00:03:34,20 --> 00:03:36,10
For now, I'm just going to have my code

79
00:03:36,10 --> 00:03:39,90
draw a blue X as the background image for my element.

80
00:03:39,90 --> 00:03:42,00
So using the context, and again,

81
00:03:42,00 --> 00:03:44,80
you should check out the Canvas course

82
00:03:44,80 --> 00:03:46,10
if you're not familiar with any of this,

83
00:03:46,10 --> 00:03:47,90
but for now, just follow along.

84
00:03:47,90 --> 00:03:50,30
So I'll give the lineWidth as 3,

85
00:03:50,30 --> 00:03:58,80
and I'll give it a strokeStyle of blue,

86
00:03:58,80 --> 00:04:00,70
and I'm going to draw my path,

87
00:04:00,70 --> 00:04:04,40
so I'm going to begin my path,

88
00:04:04,40 --> 00:04:11,10
and that has to be counterbalanced by a stroke.

89
00:04:11,10 --> 00:04:14,10
And then, inside the, whoops.

90
00:04:14,10 --> 00:04:22,70
And then inside that, I'm going to move the pen

91
00:04:22,70 --> 00:04:28,90
to the top of the box, and then draw a line.

92
00:04:28,90 --> 00:04:33,70
And I'm going to draw a line to the size.width

93
00:04:33,70 --> 00:04:37,20
and size.height.

94
00:04:37,20 --> 00:04:40,50
Alright, so that's line number one.

95
00:04:40,50 --> 00:04:45,10
And then I'll just copy this and modify it.

96
00:04:45,10 --> 00:04:51,70
So now, I'm going to moveTo size.width,

97
00:04:51,70 --> 00:04:57,40
and I'm going to lineTo 0 and size.height.

98
00:04:57,40 --> 00:05:02,00
Okay, then, I have to call my registerPaint function

99
00:05:02,00 --> 00:05:05,10
to register my painting class with the browser's

100
00:05:05,10 --> 00:05:09,00
CSS engine, so I'll call registerPaint,

101
00:05:09,00 --> 00:05:14,60
and I'm going to call it samplepainter,

102
00:05:14,60 --> 00:05:17,50
that's I'm going to use inside the CSS,

103
00:05:17,50 --> 00:05:22,80
and then I just give it my class name, SampleCSSPaint.

104
00:05:22,80 --> 00:05:25,70
Alright, so back in the HTML, let's add

105
00:05:25,70 --> 00:05:28,00
the paint function to the style

106
00:05:28,00 --> 00:05:30,50
that's applied to my div element.

107
00:05:30,50 --> 00:05:35,80
So here in the code, I have this target div, okay,

108
00:05:35,80 --> 00:05:39,00
and then what I'm going to do is

109
00:05:39,00 --> 00:05:42,10
modify the CSS style that's applied to that box.

110
00:05:42,10 --> 00:05:44,50
So the first thing I'm going to do is

111
00:05:44,50 --> 00:05:49,80
add a background image, so I'll say background-image,

112
00:05:49,80 --> 00:05:52,80
and now instead of specifying a background image,

113
00:05:52,80 --> 00:05:55,20
I'm going to use the paint function,

114
00:05:55,20 --> 00:06:00,00
and I'm going to use samplepainter as the background.

115
00:06:00,00 --> 00:06:02,40
Alright so now, I'm at a point where I can run this,

116
00:06:02,40 --> 00:06:04,30
so let's bring this up in the browser.

117
00:06:04,30 --> 00:06:06,90
And for the moment, this only works in Chrome,

118
00:06:06,90 --> 00:06:09,60
but it should work in other browsers soon.

119
00:06:09,60 --> 00:06:10,90
So let's go ahead and open this

120
00:06:10,90 --> 00:06:13,10
with our live server, and you can see

121
00:06:13,10 --> 00:06:15,70
that my blue X's are being drawn

122
00:06:15,70 --> 00:06:19,70
programmatically in the background of the box.

123
00:06:19,70 --> 00:06:21,50
But here's the thing, I don't really want

124
00:06:21,50 --> 00:06:23,50
to hard code these values, right?

125
00:06:23,50 --> 00:06:25,60
I don't want to hard code the line width and the color.

126
00:06:25,60 --> 00:06:28,00
It would be better if I could specify

127
00:06:28,00 --> 00:06:31,40
the line thickness and the color in the style sheet.

128
00:06:31,40 --> 00:06:32,90
So in the next part of the example,

129
00:06:32,90 --> 00:06:35,00
we're going to add that ability.

