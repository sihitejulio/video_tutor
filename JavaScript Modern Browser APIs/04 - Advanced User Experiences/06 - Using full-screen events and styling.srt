1
00:00:01,10 --> 00:00:02,10
- [Instructor] Alright in the previous example

2
00:00:02,10 --> 00:00:05,00
we saw how to enter and exit full screen mode

3
00:00:05,00 --> 00:00:08,30
for both the document and an individual element.

4
00:00:08,30 --> 00:00:10,00
We're going to pick up where we left off.

5
00:00:10,00 --> 00:00:12,50
And try out a couple more things.

6
00:00:12,50 --> 00:00:14,80
So here in chapter 6 I'm going

7
00:00:14,80 --> 00:00:18,50
to open up my full screen start.

8
00:00:18,50 --> 00:00:21,60
And I'm going to scroll down and heres the code

9
00:00:21,60 --> 00:00:23,30
we've already entered.

10
00:00:23,30 --> 00:00:24,10
Right.

11
00:00:24,10 --> 00:00:29,30
So first there are events that your code can listen to.

12
00:00:29,30 --> 00:00:34,10
In order to detect entering and exiting from fullscreen.

13
00:00:34,10 --> 00:00:37,20
You can see that those event handlers are right here.

14
00:00:37,20 --> 00:00:40,80
The standard event name is fullscreen change.

15
00:00:40,80 --> 00:00:43,20
But you can see that there are prefixed versions

16
00:00:43,20 --> 00:00:45,60
of this API as well.

17
00:00:45,60 --> 00:00:48,10
And each of these event handlers call a function

18
00:00:48,10 --> 00:00:50,30
that I've written here.

19
00:00:50,30 --> 00:00:53,90
That looks at the current fullscreen element

20
00:00:53,90 --> 00:00:57,10
and writes it out to the debug console.

21
00:00:57,10 --> 00:00:59,00
So I've already filled that function in

22
00:00:59,00 --> 00:01:00,80
and we'll take a look at that in a moment.

23
00:01:00,80 --> 00:01:03,70
But there's one other thing I want to demonstrate.

24
00:01:03,70 --> 00:01:07,50
You could also define CSS styles that are applied

25
00:01:07,50 --> 00:01:10,50
when fullscreen mode is active.

26
00:01:10,50 --> 00:01:13,00
So to do that you define a style

27
00:01:13,00 --> 00:01:15,50
using a fullscreen pseudo class.

28
00:01:15,50 --> 00:01:18,10
So lets go back up to the top of the doc.

29
00:01:18,10 --> 00:01:21,20
And in my style sheet here I'm going to create a

30
00:01:21,20 --> 00:01:28,40
fullscreen style and I'm going to apply it to

31
00:01:28,40 --> 00:01:31,20
the buttons in the page.

32
00:01:31,20 --> 00:01:36,10
So when the document is in fullscreen mode.

33
00:01:36,10 --> 00:01:40,50
I'm going to set the background color

34
00:01:40,50 --> 00:01:46,00
of the button to be green.

35
00:01:46,00 --> 00:01:52,00
And I'm going to set the foreground color to be white.

36
00:01:52,00 --> 00:01:54,50
And again because this is a new API

37
00:01:54,50 --> 00:01:58,20
there a prefixed versions of this style as well.

38
00:01:58,20 --> 00:02:00,50
So I'm going to copy this and I'm going

39
00:02:00,50 --> 00:02:02,00
to paste it a couple times.

40
00:02:02,00 --> 00:02:09,40
So I'm going to use dash webkit dash fullscreen

41
00:02:09,40 --> 00:02:17,20
and then there's one for which is the Moz version.

42
00:02:17,20 --> 00:02:22,80
Which is dash moz

43
00:02:22,80 --> 00:02:25,80
dash fullscreen.

44
00:02:25,80 --> 00:02:27,90
And then there's another one and in this case

45
00:02:27,90 --> 00:02:29,60
there's the Microsoft version

46
00:02:29,60 --> 00:02:34,30
which is dash ms dash fullscreen.

47
00:02:34,30 --> 00:02:35,70
That's all one word.

48
00:02:35,70 --> 00:02:39,70
Then finally an older version of the API

49
00:02:39,70 --> 00:02:45,50
which is full dash screen button.

50
00:02:45,50 --> 00:02:46,90
Okay.

51
00:02:46,90 --> 00:02:50,20
And lets also define a style just for the case

52
00:02:50,20 --> 00:02:54,00
when the image element is in fullscreen mode.

53
00:02:54,00 --> 00:02:57,20
I'm not going to do every single one of the prefixes.

54
00:02:57,20 --> 00:02:58,70
I'm just going to know in advance

55
00:02:58,70 --> 00:03:00,30
I'm going to use the webkit one.

56
00:03:00,30 --> 00:03:02,80
You're free to try the other on your own.

57
00:03:02,80 --> 00:03:08,30
So I'm going to use image, webkit full screen.

58
00:03:08,30 --> 00:03:10,10
And when the image is in fullscreen mode

59
00:03:10,10 --> 00:03:17,10
I'm going to give it a border of a 5 pix solid blue.

60
00:03:17,10 --> 00:03:25,40
I'm going to give it a background color of green.

61
00:03:25,40 --> 00:03:28,80
Okay so now lets take a look at our updated example.

62
00:03:28,80 --> 00:03:33,00
So I'll go ahead and open this in the browser.

63
00:03:33,00 --> 00:03:42,50
Alright so lets go ahead and open the developer tools.

64
00:03:42,50 --> 00:03:44,70
And we'll go to the console.

65
00:03:44,70 --> 00:03:48,10
Ill go ahead and enter fullscreen mode.

66
00:03:48,10 --> 00:03:52,60
You can see here that the fullscreen event fired.

67
00:03:52,60 --> 00:03:55,40
Heres the information about that event.

68
00:03:55,40 --> 00:03:56,40
Alright.

69
00:03:56,40 --> 00:03:58,50
And you can see that the current fullscreen element

70
00:03:58,50 --> 00:04:01,70
is the document, its the entire document element.

71
00:04:01,70 --> 00:04:04,00
And you can also see that the styling is now

72
00:04:04,00 --> 00:04:05,40
being applied to my buttons.

73
00:04:05,40 --> 00:04:06,30
Right?

74
00:04:06,30 --> 00:04:08,00
So there's the background color of green

75
00:04:08,00 --> 00:04:10,00
and the foreground color of white.

76
00:04:10,00 --> 00:04:12,40
And you can that when I exit the fullscreen mode

77
00:04:12,40 --> 00:04:14,00
that styling goes away.

78
00:04:14,00 --> 00:04:17,10
And then the fullscreen event fires again

79
00:04:17,10 --> 00:04:19,80
and now there is no fullscreen element.

80
00:04:19,80 --> 00:04:22,00
So lets go ahead now just do the image.

81
00:04:22,00 --> 00:04:25,20
And now you can see that the image has this

82
00:04:25,20 --> 00:04:26,50
background color of green,

83
00:04:26,50 --> 00:04:30,90
and heres the 5 pixel blue border.

84
00:04:30,90 --> 00:04:34,20
So if you have an application that would benefit

85
00:04:34,20 --> 00:04:35,90
from having a fullscreen view.

86
00:04:35,90 --> 00:04:38,50
You could easily accomplish that with the modern

87
00:04:38,50 --> 00:04:40,00
fullscreen API.

