1
00:00:01,00 --> 00:00:03,90
- Modern web apps have a great number of capabilities

2
00:00:03,90 --> 00:00:06,90
for creating rich, immersive experiences

3
00:00:06,90 --> 00:00:10,40
that wouldn't have been possible even just a few years ago.

4
00:00:10,40 --> 00:00:13,30
One of these is the ability to have your apps content

5
00:00:13,30 --> 00:00:15,60
fill the user's entire screen.

6
00:00:15,60 --> 00:00:17,40
And this of course has obvious benefits

7
00:00:17,40 --> 00:00:21,10
for media heavy apps like games or video players,

8
00:00:21,10 --> 00:00:23,70
but it's also useful for other kinds of apps.

9
00:00:23,70 --> 00:00:25,00
Productivity apps, for example,

10
00:00:25,00 --> 00:00:27,40
can maximize the user's workspace,

11
00:00:27,40 --> 00:00:30,90
or even an app specialized for something like real estate

12
00:00:30,90 --> 00:00:34,70
can show photos of houses at fullscreen size.

13
00:00:34,70 --> 00:00:36,90
So let's see the finished example in action.

14
00:00:36,90 --> 00:00:38,60
Here in my browser I've opened up

15
00:00:38,60 --> 00:00:41,50
the fullscreen finished example file.

16
00:00:41,50 --> 00:00:42,90
So there's two buttons to start

17
00:00:42,90 --> 00:00:46,10
fullscreen mode and one to exit.

18
00:00:46,10 --> 00:00:48,50
So notice that I can have the entire document

19
00:00:48,50 --> 00:00:50,90
enter fullscreen, which I can then

20
00:00:50,90 --> 00:00:54,10
programmatically exit by pressing this button.

21
00:00:54,10 --> 00:00:57,70
Or I can enter fullscreen for just the image

22
00:00:57,70 --> 00:01:01,40
and then exit it by using the escape key.

23
00:01:01,40 --> 00:01:02,70
Alright so let's open the fullscreen

24
00:01:02,70 --> 00:01:09,10
start file in the code editor and get to work on this.

25
00:01:09,10 --> 00:01:12,20
So in browsers that implement this API,

26
00:01:12,20 --> 00:01:15,40
both the root document and elements on the page

27
00:01:15,40 --> 00:01:18,10
have the ability to request fullscreen mode.

28
00:01:18,10 --> 00:01:20,70
So let's start with the document.

29
00:01:20,70 --> 00:01:23,10
Now as of this recording the fullscreen API

30
00:01:23,10 --> 00:01:25,80
is still relatively new, so some browsers

31
00:01:25,80 --> 00:01:30,70
still use prefixed APIs such as WebKit and Moz and so on.

32
00:01:30,70 --> 00:01:33,30
So what that means, that for now I have to write

33
00:01:33,30 --> 00:01:36,20
a wrapper function that does the feature detection

34
00:01:36,20 --> 00:01:39,00
for the presence of the correct API.

35
00:01:39,00 --> 00:01:45,00
So when the user clicks on the enter fullscreen button,

36
00:01:45,00 --> 00:01:49,50
which is, let's see, if we look at the event handler

37
00:01:49,50 --> 00:01:52,70
here's the enter fullscreen mode for the document

38
00:01:52,70 --> 00:01:57,40
and enter screen mode for the image.

39
00:01:57,40 --> 00:02:00,90
If I scroll down, you'll see that there are

40
00:02:00,90 --> 00:02:03,20
event listeners for those buttons.

41
00:02:03,20 --> 00:02:05,30
So here's the doc, here's the image.

42
00:02:05,30 --> 00:02:08,40
And they call this function called enter fullscreen,

43
00:02:08,40 --> 00:02:10,60
and the exit button calls exit fullscreen.

44
00:02:10,60 --> 00:02:12,80
So I need to fill those functions out.

45
00:02:12,80 --> 00:02:14,60
And for enter fullscreen, it passes in

46
00:02:14,60 --> 00:02:19,30
either the document element or in this case the image

47
00:02:19,30 --> 00:02:22,60
for the element that is requesting fullscreen mode.

48
00:02:22,60 --> 00:02:25,70
So in my enter fullscreen function, so what I'm going to do

49
00:02:25,70 --> 00:02:29,50
is check if the standard API is supported.

50
00:02:29,50 --> 00:02:38,60
So if element dot request fullscreen is there,

51
00:02:38,60 --> 00:02:42,10
then I'm just going to call that element

52
00:02:42,10 --> 00:02:44,20
request fullscreen function.

53
00:02:44,20 --> 00:02:45,80
Otherwise, we need to use one

54
00:02:45,80 --> 00:02:49,30
of the prefixed versions and there's a few of them.

55
00:02:49,30 --> 00:02:54,90
So there's one for WebKit,

56
00:02:54,90 --> 00:02:58,80
and there's one for Mozilla, which is M-O-Z,

57
00:02:58,80 --> 00:03:02,80
and that's request fullscreen,

58
00:03:02,80 --> 00:03:05,70
that's a capital s in their case.

59
00:03:05,70 --> 00:03:10,20
And then there's one for Microsoft.

60
00:03:10,20 --> 00:03:17,80
And then I'll just simply copy and paste each of these.

61
00:03:17,80 --> 00:03:20,00
Alright.

62
00:03:20,00 --> 00:03:22,40
And for exiting fullscreen mode,

63
00:03:22,40 --> 00:03:28,80
there's a similar standard API called,

64
00:03:28,80 --> 00:03:31,60
and it's on the document object.

65
00:03:31,60 --> 00:03:35,40
and it's called exit fullscreen.

66
00:03:35,40 --> 00:03:37,30
So if that's present then I'm just going to

67
00:03:37,30 --> 00:03:40,20
simply call that function.

68
00:03:40,20 --> 00:03:45,00
Otherwise, I need to check for the prefixed versions

69
00:03:45,00 --> 00:03:48,80
and there's a WebKit one,

70
00:03:48,80 --> 00:03:51,70
and in the Moz case it's called M-O-Z,

71
00:03:51,70 --> 00:03:56,30
and it's called cancel fullscreen.

72
00:03:56,30 --> 00:03:59,30
And then there's the MS version.

73
00:03:59,30 --> 00:04:06,40
And then once again I'll just simply copy and paste these.

74
00:04:06,40 --> 00:04:08,70
Okay.

75
00:04:08,70 --> 00:04:10,70
Alright, so let's try out what we have so far.

76
00:04:10,70 --> 00:04:14,50
So, I'm going to bring this up in my live server

77
00:04:14,50 --> 00:04:16,30
and here's my example, so let's click

78
00:04:16,30 --> 00:04:19,00
on enter document fullscreen mode and you can see

79
00:04:19,00 --> 00:04:21,40
that that works and I can exit

80
00:04:21,40 --> 00:04:23,40
and I can do the same thing with the image

81
00:04:23,40 --> 00:04:25,90
and once again I can exit.

82
00:04:25,90 --> 00:04:29,60
Alright, so that's how we enter and exit fullscreen mode.

83
00:04:29,60 --> 00:04:31,80
In the next example, we'll see how we can use

84
00:04:31,80 --> 00:04:35,70
the fullscreen events as well as apply styling

85
00:04:35,70 --> 00:04:39,00
to elements when they are in fullscreen mode.

