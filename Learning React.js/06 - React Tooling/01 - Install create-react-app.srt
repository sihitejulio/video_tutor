1
00:00:01,00 --> 00:00:01,90
- [Instructor] Currently, we're using

2
00:00:01,90 --> 00:00:05,40
an html file for our entire React project.

3
00:00:05,40 --> 00:00:07,30
Now, this is really good for testing

4
00:00:07,30 --> 00:00:09,00
and getting comfortable with React,

5
00:00:09,00 --> 00:00:10,90
but here may be a point at which you want

6
00:00:10,90 --> 00:00:13,90
to really setup a full-fledged project,

7
00:00:13,90 --> 00:00:15,70
and the way I would recommend you get started

8
00:00:15,70 --> 00:00:18,70
with that is with the tool Create React App.

9
00:00:18,70 --> 00:00:22,20
Create React App will let you setup a React project

10
00:00:22,20 --> 00:00:24,50
with all of the tooling setup and everything

11
00:00:24,50 --> 00:00:26,50
you need to get started, without having

12
00:00:26,50 --> 00:00:28,40
to do to a bunch of setup.

13
00:00:28,40 --> 00:00:30,90
Create React App is a tool that will generate

14
00:00:30,90 --> 00:00:33,70
a React project for you, so you don't have to set up

15
00:00:33,70 --> 00:00:35,40
any of the tooling.

16
00:00:35,40 --> 00:00:37,70
All we need to get started with Create React App

17
00:00:37,70 --> 00:00:42,00
is we need to install Create React App using NPM.

18
00:00:42,00 --> 00:00:45,30
So over in my terminal, I'm going run npm -v.

19
00:00:45,30 --> 00:00:47,10
This is going to let me know that I'm running

20
00:00:47,10 --> 00:00:50,90
6.5 and above, I also am going to check node -v,

21
00:00:50,90 --> 00:00:53,70
and this will tell me I'm running 11.6.

22
00:00:53,70 --> 00:00:57,20
Now, if you have anything above 6.2 or higher,

23
00:00:57,20 --> 00:00:58,50
that should be fine.

24
00:00:58,50 --> 00:01:00,30
If you're getting warnings, or errors,

25
00:01:00,30 --> 00:01:02,50
or anything popping up that doesn't give you

26
00:01:02,50 --> 00:01:04,40
a version number, that likely means

27
00:01:04,40 --> 00:01:06,20
that you need to install Node.

28
00:01:06,20 --> 00:01:11,50
You can go to nodejs.org and install it from the homepage.

29
00:01:11,50 --> 00:01:13,20
Now, the next thing I want to do here is

30
00:01:13,20 --> 00:01:19,20
let's go ahead and run sudo npm install -g create react app.

31
00:01:19,20 --> 00:01:22,20
This is going to run this as an administrator

32
00:01:22,20 --> 00:01:24,50
on my computer with the sudo.

33
00:01:24,50 --> 00:01:27,40
If you're using a pc you can just run your command prompt

34
00:01:27,40 --> 00:01:30,90
as an administrator, this will ask for my password,

35
00:01:30,90 --> 00:01:34,00
and then this is going to install this.

36
00:01:34,00 --> 00:01:34,80
Perfect.

37
00:01:34,80 --> 00:01:37,50
So the next thing I want to do here, after hitting clear,

38
00:01:37,50 --> 00:01:39,20
is I want to make sure that I've navigated

39
00:01:39,20 --> 00:01:43,00
into the right folder where I want to create this project.

40
00:01:43,00 --> 00:01:45,90
So, what I want to do I type cd.

41
00:01:45,90 --> 00:01:47,90
I'm going to go to the chapter five lesson one.

42
00:01:47,90 --> 00:01:52,10
I'm going to drag the start folder here into the terminal.

43
00:01:52,10 --> 00:01:55,90
So again, cd, and just drag that folder over,

44
00:01:55,90 --> 00:01:57,80
and now you're inside of that folder.

45
00:01:57,80 --> 00:02:01,00
That's your context for installing a folder

46
00:02:01,00 --> 00:02:02,30
with Create React App.

47
00:02:02,30 --> 00:02:03,90
How cool is that?

48
00:02:03,90 --> 00:02:08,30
So, the next thing I'll type is create-react-app

49
00:02:08,30 --> 00:02:11,00
and then whatever the name of the project is.

50
00:02:11,00 --> 00:02:15,50
So, I'm just going to call this lake-app,

51
00:02:15,50 --> 00:02:19,30
and this is going to generate an entire React app project

52
00:02:19,30 --> 00:02:22,30
inside of this folder.

53
00:02:22,30 --> 00:02:24,90
Great, so once Create React App has installed this,

54
00:02:24,90 --> 00:02:28,30
we should be able to NVS code.

55
00:02:28,30 --> 00:02:30,00
Scroll over.

56
00:02:30,00 --> 00:02:31,60
We should see that the lake app is now

57
00:02:31,60 --> 00:02:32,80
inside of this folder.

58
00:02:32,80 --> 00:02:34,50
A couple of things are going on here,

59
00:02:34,50 --> 00:02:37,80
we have our package json, and inside the package json

60
00:02:37,80 --> 00:02:39,80
we have react as a dependency,

61
00:02:39,80 --> 00:02:42,70
we have react-dom, and we have react-scripts.

62
00:02:42,70 --> 00:02:44,40
Now, react-scripts is just a wrapper

63
00:02:44,40 --> 00:02:47,10
around all of the tooling packages needed,

64
00:02:47,10 --> 00:02:49,90
like babel and webpack.

65
00:02:49,90 --> 00:02:52,60
Great, so now that we have this folder installed,

66
00:02:52,60 --> 00:02:56,60
we are going to be able to break down our current application

67
00:02:56,60 --> 00:02:58,00
into the structure.

