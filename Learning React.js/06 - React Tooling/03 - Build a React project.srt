1
00:00:00,00 --> 00:00:02,20
- [Instructor] Currently our Lake app

2
00:00:02,20 --> 00:00:04,30
is running on local host 3000

3
00:00:04,30 --> 00:00:08,10
because we ran NPM Start, and it's running it there.

4
00:00:08,10 --> 00:00:11,90
Now, let's say our app is ready to go.

5
00:00:11,90 --> 00:00:14,00
We want to take it out of development

6
00:00:14,00 --> 00:00:16,80
and we want to push it to production.

7
00:00:16,80 --> 00:00:19,10
A quick thing we can do with Create React app

8
00:00:19,10 --> 00:00:23,40
to make that possible, is we can build a production build.

9
00:00:23,40 --> 00:00:24,90
So, it says here to create

10
00:00:24,90 --> 00:00:28,00
a production build, use npm run build.

11
00:00:28,00 --> 00:00:29,40
Let's do just that.

12
00:00:29,40 --> 00:00:32,10
So, let me stop my server real quick,

13
00:00:32,10 --> 00:00:35,70
and let's run npm run build,

14
00:00:35,70 --> 00:00:37,70
and then this is going to create

15
00:00:37,70 --> 00:00:39,90
an optimized production build,

16
00:00:39,90 --> 00:00:42,20
so that we can put it up online.

17
00:00:42,20 --> 00:00:45,20
So, we could put it on Netlify, or next,

18
00:00:45,20 --> 00:00:47,50
or any sort of hosting option.

19
00:00:47,50 --> 00:00:50,30
Now, if we go back to our browser

20
00:00:50,30 --> 00:00:53,20
and we open up the folder,

21
00:00:53,20 --> 00:00:55,90
we're going to see that now there's a build folder here.

22
00:00:55,90 --> 00:00:58,60
And there's all this cool stuff in here,

23
00:00:58,60 --> 00:01:01,10
there's a manifest, there's all sorts of things.

24
00:01:01,10 --> 00:01:03,00
There's also this Static folder,

25
00:01:03,00 --> 00:01:04,90
and in the Static folder we have

26
00:01:04,90 --> 00:01:07,80
a bunch of chunks of data.

27
00:01:07,80 --> 00:01:09,90
Basically, all this has been done for us

28
00:01:09,90 --> 00:01:12,40
because we're running Web Pack behind the scenes

29
00:01:12,40 --> 00:01:14,20
as part of Create React app.

30
00:01:14,20 --> 00:01:17,60
This is creating a build that will be really fast

31
00:01:17,60 --> 00:01:19,30
in most modern browsers.

32
00:01:19,30 --> 00:01:21,10
So, this is pretty cool.

33
00:01:21,10 --> 00:01:24,70
Now, a quick way that we can take a look

34
00:01:24,70 --> 00:01:27,00
at what this looks like on our own computer,

35
00:01:27,00 --> 00:01:30,20
it looks like this is giving us this command also,

36
00:01:30,20 --> 00:01:36,60
we would need to sudo npm install serve -g.

37
00:01:36,60 --> 00:01:40,30
So, serve is a package that will serve this locally

38
00:01:40,30 --> 00:01:45,20
with a static server, so we can quickly install this.

39
00:01:45,20 --> 00:01:48,40
Once we do that, we can type the following command.

40
00:01:48,40 --> 00:01:52,80
We're going to serve up the Build folder on a particular port.

41
00:01:52,80 --> 00:01:56,00
So, serve -s build.

42
00:01:56,00 --> 00:02:00,10
Now, it looks like this is running on local host 5000.

43
00:02:00,10 --> 00:02:05,50
So, I can just Copy+Paste this into my browser.

44
00:02:05,50 --> 00:02:07,80
And not a whole lot of changes,

45
00:02:07,80 --> 00:02:10,60
but this is actually the optimize production build

46
00:02:10,60 --> 00:02:13,50
that is being served from that folder.

47
00:02:13,50 --> 00:02:14,90
So, this is a really nice thing.

48
00:02:14,90 --> 00:02:17,00
We can take this and deploy it

49
00:02:17,00 --> 00:02:19,40
anywhere that we want to host our content.

50
00:02:19,40 --> 00:02:21,50
And all of this is built directly

51
00:02:21,50 --> 00:02:26,00
into Create React app with no configuration necessary.

