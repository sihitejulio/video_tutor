1
00:00:01,00 --> 00:00:01,90
- [Instructor] Now that we have

2
00:00:01,90 --> 00:00:03,30
our React application set up,

3
00:00:03,30 --> 00:00:06,80
we can break down on our current index.html file

4
00:00:06,80 --> 00:00:09,40
into that project structure.

5
00:00:09,40 --> 00:00:10,90
The first thing I want to do is,

6
00:00:10,90 --> 00:00:14,70
we're going to open up our source folder.

7
00:00:14,70 --> 00:00:17,10
The source folder is where all of our components live.

8
00:00:17,10 --> 00:00:21,10
For example, the index file has reactDOM.render,

9
00:00:21,10 --> 00:00:23,50
and it's rendering that item to the root.

10
00:00:23,50 --> 00:00:27,20
What I can do is make sure that our react.DOM is the same.

11
00:00:27,20 --> 00:00:32,90
Let's go ahead and cut and paste this into that file,

12
00:00:32,90 --> 00:00:36,80
and we can paste this here on line 7,

13
00:00:36,80 --> 00:00:38,50
and we'll give that a save.

14
00:00:38,50 --> 00:00:40,80
Looks like our Prettier is being picked up here,

15
00:00:40,80 --> 00:00:44,70
and that will just format that for me.

16
00:00:44,70 --> 00:00:49,10
The next thing I want to do is, it looks like we're sending

17
00:00:49,10 --> 00:00:52,30
the lakes list to this as a property,

18
00:00:52,30 --> 00:00:55,80
so let's make sure our data also lives in that file.

19
00:00:55,80 --> 00:00:58,30
Back to the index.html.

20
00:00:58,30 --> 00:01:01,20
We'll grab the lakeList, we'll hit cut,

21
00:01:01,20 --> 00:01:04,60
and we'll paste it into the index.js.

22
00:01:04,60 --> 00:01:07,50
Let's just place this here on line 7.

23
00:01:07,50 --> 00:01:12,30
The next thing I want to do is in the App.js file.

24
00:01:12,30 --> 00:01:14,70
This is where our component is going to go, so we're going to

25
00:01:14,70 --> 00:01:19,30
replace all of this app code with our own app.

26
00:01:19,30 --> 00:01:24,00
We can find this here so let's go ahead and grab it.

27
00:01:24,00 --> 00:01:30,10
We're going to paste it into the app, of course.

28
00:01:30,10 --> 00:01:33,60
We get some nice formatting from VS Code.

29
00:01:33,60 --> 00:01:37,90
We can get rid of the Component import.

30
00:01:37,90 --> 00:01:42,10
We can rid rid of the App.css if we want to.

31
00:01:42,10 --> 00:01:43,60
Yes, this looks pretty good.

32
00:01:43,60 --> 00:01:48,70
We have our import and we're exporting the default app.

33
00:01:48,70 --> 00:01:49,60
How do we run this?

34
00:01:49,60 --> 00:01:52,20
How do we check to make sure that this is working?

35
00:01:52,20 --> 00:01:54,50
We're going to go to the Terminal.

36
00:01:54,50 --> 00:01:56,30
Right now I'm in the start directory

37
00:01:56,30 --> 00:01:58,90
but we want to make sure that we're in the project.

38
00:01:58,90 --> 00:02:03,70
It's called lake-app so we'll say cd lake-app.

39
00:02:03,70 --> 00:02:06,20
We're in the context of that folder.

40
00:02:06,20 --> 00:02:10,30
We're going to also make sure npm install or npm i

41
00:02:10,30 --> 00:02:14,30
to install all of these dependencies that we'll need,

42
00:02:14,30 --> 00:02:15,60
and we're just going to take this step

43
00:02:15,60 --> 00:02:18,50
if we haven't done so already,

44
00:02:18,50 --> 00:02:23,30
Once this is complete, we can run npm start.

45
00:02:23,30 --> 00:02:27,30
This is going to run our project on localhost:3000.

46
00:02:27,30 --> 00:02:30,20
Alright, so let me pull this over.

47
00:02:30,20 --> 00:02:33,60
It looks like it is running on localhost:3000, that's great.

48
00:02:33,60 --> 00:02:35,90
I can pull it up here and we should see

49
00:02:35,90 --> 00:02:38,30
Echo Lake | Trailhead: Echo Lake.

50
00:02:38,30 --> 00:02:44,40
Everything is being nicely formatted also by this index.css

51
00:02:44,40 --> 00:02:46,10
that's being imported, so this is kind of

52
00:02:46,10 --> 00:02:49,80
the default styles that create-react-app provides.

53
00:02:49,80 --> 00:02:51,90
This is pretty cool we've been able

54
00:02:51,90 --> 00:02:56,50
to use create-react-app as a structure for our project.

55
00:02:56,50 --> 00:03:00,40
We're rendering our app, we're using our lakeList,

56
00:03:00,40 --> 00:03:02,70
and then in the App.js file,

57
00:03:02,70 --> 00:03:05,90
we're going to create our App component.

58
00:03:05,90 --> 00:03:08,40
There are many, many, many different ways

59
00:03:08,40 --> 00:03:11,50
to break down a project but more often than not,

60
00:03:11,50 --> 00:03:14,80
you'll have a file for each component

61
00:03:14,80 --> 00:03:16,90
or a small set of components,

62
00:03:16,90 --> 00:03:19,00
so that you can keep things more organized.

