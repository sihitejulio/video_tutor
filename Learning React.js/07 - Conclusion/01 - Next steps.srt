1
00:00:01,10 --> 00:00:03,80
- [Eve] There you have it, we've learned the basics of React

2
00:00:03,80 --> 00:00:05,10
and we've gotten an understanding

3
00:00:05,10 --> 00:00:07,90
of how to build and launch an application.

4
00:00:07,90 --> 00:00:09,60
From here, there are many directions

5
00:00:09,60 --> 00:00:11,20
you can take your study of React

6
00:00:11,20 --> 00:00:14,90
depending on the types of projects that you're working on.

7
00:00:14,90 --> 00:00:16,70
There are several other React courses

8
00:00:16,70 --> 00:00:18,50
that you should investigate.

9
00:00:18,50 --> 00:00:21,00
To go a little more in-depth beyond this course,

10
00:00:21,00 --> 00:00:24,10
check out my course React Essential Training,

11
00:00:24,10 --> 00:00:27,50
or to learn how to use React to create native applications,

12
00:00:27,50 --> 00:00:31,00
I'd recommend Learning React Native, with Alex Banks.

13
00:00:31,00 --> 00:00:34,50
And then to see where the future of React is headed,

14
00:00:34,50 --> 00:00:39,10
I'd also watched Learning React VR, with Manny Henry.

15
00:00:39,10 --> 00:00:40,50
I'd also recommend that you check out

16
00:00:40,50 --> 00:00:44,10
React's documentation website at reactjs.org,

17
00:00:44,10 --> 00:00:47,00
it has a ton of good examples, and there's a blog here

18
00:00:47,00 --> 00:00:50,80
where you can check out the latest in React News.

19
00:00:50,80 --> 00:00:53,30
I hope you've enjoyed learning React with me.

20
00:00:53,30 --> 00:00:56,00
I can't wait to see what you'll build.

