1
00:00:01,00 --> 00:00:02,30
- [Instructor] The power of React starts

2
00:00:02,30 --> 00:00:03,90
to become more obvious when you start

3
00:00:03,90 --> 00:00:05,80
putting components together to create

4
00:00:05,80 --> 00:00:07,90
a larger user interface.

5
00:00:07,90 --> 00:00:09,60
The process of composing components

6
00:00:09,60 --> 00:00:11,80
is what React is all about.

7
00:00:11,80 --> 00:00:15,30
So, let's go ahead and replace our hello component.

8
00:00:15,30 --> 00:00:18,90
We're going to replace this with a component called lake.

9
00:00:18,90 --> 00:00:22,40
Lake is going to take in the name property,

10
00:00:22,40 --> 00:00:28,60
and it's going to return an h1 that displays that name.

11
00:00:28,60 --> 00:00:31,40
Okay, the next step is we're going to create

12
00:00:31,40 --> 00:00:33,60
a component called app.

13
00:00:33,60 --> 00:00:38,00
Now, the app component is going to be this wrapper

14
00:00:38,00 --> 00:00:40,90
around a div

15
00:00:40,90 --> 00:00:43,10
and inside of the div we're going to have

16
00:00:43,10 --> 00:00:45,00
a few different lakes.

17
00:00:45,00 --> 00:00:47,90
So lake, and then where we render this

18
00:00:47,90 --> 00:00:49,20
we'll pass it a name.

19
00:00:49,20 --> 00:00:52,80
So, Lake Tahoe.

20
00:00:52,80 --> 00:00:56,40
And then we can copy and paste these for the other two.

21
00:00:56,40 --> 00:00:59,70
We'll use Angora Lake.

22
00:00:59,70 --> 00:01:03,10
And Shirley Lake.

23
00:01:03,10 --> 00:01:04,40
Alright.

24
00:01:04,40 --> 00:01:06,60
So now, I have two components.

25
00:01:06,60 --> 00:01:09,70
The app component is rendering our lake component.

26
00:01:09,70 --> 00:01:12,50
And then we're passing down name as a property.

27
00:01:12,50 --> 00:01:15,80
So now I can render the app component,

28
00:01:15,80 --> 00:01:17,20
and if I give that a save

29
00:01:17,20 --> 00:01:20,20
we should see Lake Tahoe, Angora Lake, Shirley Lake

30
00:01:20,20 --> 00:01:21,70
all being added.

31
00:01:21,70 --> 00:01:26,40
So, I can create as many of these components as I'd like to.

32
00:01:26,40 --> 00:01:30,20
And they're always going to be displayed within this app.

33
00:01:30,20 --> 00:01:32,50
So, this gives us a lot of flexibility.

34
00:01:32,50 --> 00:01:35,40
Think about data coming from external sources,

35
00:01:35,40 --> 00:01:39,50
we can use this component hierarchy to display

36
00:01:39,50 --> 00:01:41,90
a certain number of components.

37
00:01:41,90 --> 00:01:44,00
So, let me change this lake name.

38
00:01:44,00 --> 00:01:47,10
We'll call it Cathedral Lake, and we should see

39
00:01:47,10 --> 00:01:51,00
another lake being added to our page.

