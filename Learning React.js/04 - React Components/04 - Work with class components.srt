1
00:00:01,00 --> 00:00:02,60
- [Instructor] Another way to create a component

2
00:00:02,60 --> 00:00:04,80
is to use a JavaScript class.

3
00:00:04,80 --> 00:00:08,70
Let's refactor our small app to use a class component.

4
00:00:08,70 --> 00:00:11,90
So, here on line 21,

5
00:00:11,90 --> 00:00:17,40
I'm going to say class App extends React.Component

6
00:00:17,40 --> 00:00:19,00
and within this React.Component,

7
00:00:19,00 --> 00:00:22,10
we're going to add a required render method.

8
00:00:22,10 --> 00:00:23,60
Render is going to describe

9
00:00:23,60 --> 00:00:25,80
what should be rendered to the page,

10
00:00:25,80 --> 00:00:27,70
so it's really just this,

11
00:00:27,70 --> 00:00:29,20
so we'll say return

12
00:00:29,20 --> 00:00:33,20
and we'll cut and paste everything from our app here

13
00:00:33,20 --> 00:00:35,70
on line 23.

14
00:00:35,70 --> 00:00:39,20
And let's clean that up a little bit.

15
00:00:39,20 --> 00:00:40,60
All right.

16
00:00:40,60 --> 00:00:43,50
Cool, so now I can get rid of the function component

17
00:00:43,50 --> 00:00:45,60
and instead just render the app,

18
00:00:45,60 --> 00:00:47,20
so if I save that and hit Refresh,

19
00:00:47,20 --> 00:00:48,50
we should see that everything

20
00:00:48,50 --> 00:00:50,80
is still rendering appropriately.

21
00:00:50,80 --> 00:00:52,30
You have your JavaScript class

22
00:00:52,30 --> 00:00:55,50
and then this is returning some UI elements

23
00:00:55,50 --> 00:00:58,10
and remember, we always need to have the render method

24
00:00:58,10 --> 00:01:00,00
inside of the component class.

