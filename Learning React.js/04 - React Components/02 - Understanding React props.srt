1
00:00:01,00 --> 00:00:02,50
- [Instructor] Props is an object in React

2
00:00:02,50 --> 00:00:05,00
that contains properties about the component.

3
00:00:05,00 --> 00:00:08,60
With props, we can display dynamic data within a component.

4
00:00:08,60 --> 00:00:11,40
To begin, let's take a look at the props object.

5
00:00:11,40 --> 00:00:13,30
So the props are always going to be sent

6
00:00:13,30 --> 00:00:15,50
into these component functions,

7
00:00:15,50 --> 00:00:18,10
I'm going to use props and then let's go ahead

8
00:00:18,10 --> 00:00:22,10
and above the return, console log props.

9
00:00:22,10 --> 00:00:24,70
So I'll give that a save, I'll hit refresh,

10
00:00:24,70 --> 00:00:26,30
and I'll open up my console

11
00:00:26,30 --> 00:00:29,30
and we should see that props is this object.

12
00:00:29,30 --> 00:00:31,90
And there's a few values on it

13
00:00:31,90 --> 00:00:34,40
but there's really nothing in this object yet.

14
00:00:34,40 --> 00:00:36,60
So let's go ahead and add on to this.

15
00:00:36,60 --> 00:00:39,00
So now instead of using welcome to React,

16
00:00:39,00 --> 00:00:43,70
I'm going to say props.library,

17
00:00:43,70 --> 00:00:44,60
and for good measure,

18
00:00:44,60 --> 00:00:47,60
let's go ahead and add a paragraph here.

19
00:00:47,60 --> 00:00:50,10
Let's make that go forward as well.

20
00:00:50,10 --> 00:00:55,30
And the paragraph should have props.message here.

21
00:00:55,30 --> 00:00:58,30
Okay, so now I can pass properties

22
00:00:58,30 --> 00:01:01,00
to the hello component here in the render function.

23
00:01:01,00 --> 00:01:03,20
So wherever I'm rendering this hello,

24
00:01:03,20 --> 00:01:06,50
I can pass in some property values.

25
00:01:06,50 --> 00:01:08,40
So the first thing I'll do is I'll add

26
00:01:08,40 --> 00:01:12,30
equals react.

27
00:01:12,30 --> 00:01:15,20
And the second thing I'll add is our message

28
00:01:15,20 --> 00:01:17,70
and I'll just say enjoy!

29
00:01:17,70 --> 00:01:21,10
Alright, so, let's go ahead and hit refresh again,

30
00:01:21,10 --> 00:01:22,70
welcome to React, enjoy!

31
00:01:22,70 --> 00:01:26,00
All of this is being passed via that props object

32
00:01:26,00 --> 00:01:28,20
and we're still logging it so let's take a look.

33
00:01:28,20 --> 00:01:31,20
We have library and message on that object.

34
00:01:31,20 --> 00:01:33,60
So you can create as many properties as you'd like to

35
00:01:33,60 --> 00:01:36,40
and this is going to allow you to display

36
00:01:36,40 --> 00:01:38,80
all sorts of dynamic data.

37
00:01:38,80 --> 00:01:41,30
Another thing that's really common to see is that

38
00:01:41,30 --> 00:01:43,90
sometimes these props are de-structured

39
00:01:43,90 --> 00:01:46,40
in the function for brevity.

40
00:01:46,40 --> 00:01:49,40
So I'm going to add the curly braces,

41
00:01:49,40 --> 00:01:52,40
I'll say library and message

42
00:01:52,40 --> 00:01:55,60
and I'll get rid of our console log

43
00:01:55,60 --> 00:01:59,80
and let's go ahead and display library

44
00:01:59,80 --> 00:02:01,10
and message.

45
00:02:01,10 --> 00:02:03,10
So that's a personal preference,

46
00:02:03,10 --> 00:02:05,20
you don't have to take that step, certainly.

47
00:02:05,20 --> 00:02:07,10
But this is going to display

48
00:02:07,10 --> 00:02:09,00
whatever the value of library is

49
00:02:09,00 --> 00:02:12,10
and whatever the value of message is.

50
00:02:12,10 --> 00:02:13,80
So to recap, the props object

51
00:02:13,80 --> 00:02:16,80
provides data to a component to be displayed.

52
00:02:16,80 --> 00:02:18,90
Really you can just think of a React component

53
00:02:18,90 --> 00:02:22,20
as a function that takes in some data as an argument

54
00:02:22,20 --> 00:02:26,00
and returns React elements or a user interface.

