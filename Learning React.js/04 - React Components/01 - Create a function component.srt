1
00:00:01,00 --> 00:00:02,60
- The way that a react application

2
00:00:02,60 --> 00:00:06,20
represents a UI element is with a component.

3
00:00:06,20 --> 00:00:07,70
A component lets you put together a

4
00:00:07,70 --> 00:00:11,20
user interface with independent reusable pieces.

5
00:00:11,20 --> 00:00:12,60
What's great, a component that is a

6
00:00:12,60 --> 00:00:14,50
function that returns some UI.

7
00:00:14,50 --> 00:00:17,10
So we'll replace let city = "Madrid"

8
00:00:17,10 --> 00:00:20,60
with a new function, now this function

9
00:00:20,60 --> 00:00:24,80
should return in H1, and within

10
00:00:24,80 --> 00:00:28,60
the H1 we'll just say "Welcome to react."

11
00:00:28,60 --> 00:00:30,80
Now, instead of rendering this H1

12
00:00:30,80 --> 00:00:32,90
in the reactdom.render function, we're

13
00:00:32,90 --> 00:00:36,70
going to replace this with the Hello component.

14
00:00:36,70 --> 00:00:38,40
What we'll do to render it is we'll

15
00:00:38,40 --> 00:00:41,70
use the name of that component as a tag.

16
00:00:41,70 --> 00:00:44,10
So this will match exactly.

17
00:00:44,10 --> 00:00:46,30
So I'll give that a save, I'll hit refresh,

18
00:00:46,30 --> 00:00:50,00
and we should see "Welcome to React" being rendered.

19
00:00:50,00 --> 00:00:53,10
Right now, we're just returning one element, the H1.

20
00:00:53,10 --> 00:00:55,00
Let's return more than one element.

21
00:00:55,00 --> 00:00:59,20
So I'm going to go ahead and create a div as a

22
00:00:59,20 --> 00:01:05,50
wrapper to this, and we'll add a closing div.

23
00:01:05,50 --> 00:01:11,40
And now if I add that class name,

24
00:01:11,40 --> 00:01:17,90
so we'll say div className="heading"

25
00:01:17,90 --> 00:01:19,60
and I hit refresh, we should see

26
00:01:19,60 --> 00:01:21,40
that that is being applied.

27
00:01:21,40 --> 00:01:22,70
Now, there's a lot of different ways

28
00:01:22,70 --> 00:01:24,90
that you can return these UI elements.

29
00:01:24,90 --> 00:01:29,50
You could, for example, wrap this in parentheses.

30
00:01:29,50 --> 00:01:31,40
That's one way to do it, there's not

31
00:01:31,40 --> 00:01:33,30
going to be any problem with that.

32
00:01:33,30 --> 00:01:35,60
You also can do this, so I can keep these in

33
00:01:35,60 --> 00:01:43,70
place, and I'll wrap the component like so.

34
00:01:43,70 --> 00:01:46,00
Now if I am using curly braces,

35
00:01:46,00 --> 00:01:48,40
I need to use a return, so that's

36
00:01:48,40 --> 00:01:50,40
the only little gotcha there.

37
00:01:50,40 --> 00:01:55,40
So we'll use this return, let's hit a tab here,

38
00:01:55,40 --> 00:01:57,60
and again, we'll still see the same

39
00:01:57,60 --> 00:02:00,50
output being added to the dom.

40
00:02:00,50 --> 00:02:01,80
So these are a couple different ways

41
00:02:01,80 --> 00:02:04,00
that you'll see these components represented.

42
00:02:04,00 --> 00:02:06,90
One quick tip: that you have to capitalize the

43
00:02:06,90 --> 00:02:09,50
name of the component in order for this to work.

44
00:02:09,50 --> 00:02:11,40
So for example, if I made "hello" lower case,

45
00:02:11,40 --> 00:02:14,40
and then I tried to render that as a component,

46
00:02:14,40 --> 00:02:17,10
we're going to see a warning here

47
00:02:17,10 --> 00:02:21,00
that says "the react tag hello is unrecognized

48
00:02:21,00 --> 00:02:22,70
in the browser, if you meant to render

49
00:02:22,70 --> 00:02:24,60
a react component, start its name

50
00:02:24,60 --> 00:02:26,00
with an uppercase letter."

51
00:02:26,00 --> 00:02:29,10
So this is just a requirement, it will now

52
00:02:29,10 --> 00:02:31,70
throw an error, so I need to replace

53
00:02:31,70 --> 00:02:34,30
the h's with capital H's, and we

54
00:02:34,30 --> 00:02:37,00
should see our code is back.

55
00:02:37,00 --> 00:02:39,50
So we'll add our semi-colon there at the end,

56
00:02:39,50 --> 00:02:41,70
and we'll just understand that the

57
00:02:41,70 --> 00:02:44,00
hello component is our first of our

58
00:02:44,00 --> 00:02:46,50
components we're going to render that to

59
00:02:46,50 --> 00:02:48,60
the dom with reactdom.render,

60
00:02:48,60 --> 00:02:50,00
and add it to the root.

