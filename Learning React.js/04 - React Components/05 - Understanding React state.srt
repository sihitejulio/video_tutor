1
00:00:01,00 --> 00:00:03,30
- [Instructor] One of the most important concepts in React

2
00:00:03,30 --> 00:00:04,50
is State.

3
00:00:04,50 --> 00:00:07,30
When a component's State data changes, the render function

4
00:00:07,30 --> 00:00:10,00
will be called again to re-render the state change.

5
00:00:10,00 --> 00:00:14,50
Let's go ahead and add some State to our component.

6
00:00:14,50 --> 00:00:17,40
We're going to add a value called State and this is going

7
00:00:17,40 --> 00:00:20,90
to be set to an object with the key of loggedIn

8
00:00:20,90 --> 00:00:23,60
and we're going to set the initial state to true.

9
00:00:23,60 --> 00:00:27,10
Now within our render method we're going to add a little

10
00:00:27,10 --> 00:00:31,50
div here and inside of this div we're going to display

11
00:00:31,50 --> 00:00:35,30
the user is.

12
00:00:35,30 --> 00:00:37,40
So what I'm going to do here in this div is I want to add

13
00:00:37,40 --> 00:00:39,10
a JSX expression.

14
00:00:39,10 --> 00:00:42,70
Now I'm going to check to see if logged in is true.

15
00:00:42,70 --> 00:00:48,90
I'm going to say if this.state.loggedIn so if that

16
00:00:48,90 --> 00:00:55,20
is (mumbles) we're going to return logged in, otherwise

17
00:00:55,20 --> 00:00:58,40
we're going to return not logged in.

18
00:00:58,40 --> 00:01:01,20
Now if I hit Save on that and I hit Refresh I should see

19
00:01:01,20 --> 00:01:05,50
the user is logged in because this value is true,

20
00:01:05,50 --> 00:01:07,20
so we'll return logged in.

21
00:01:07,20 --> 00:01:10,60
If for some reason logged in was set to false,

22
00:01:10,60 --> 00:01:15,00
then it's going to change to not logged in.

23
00:01:15,00 --> 00:01:18,90
So this is a simple reflection of the state value as UI

24
00:01:18,90 --> 00:01:23,00
but we're going to use the state with events in order

25
00:01:23,00 --> 00:01:26,00
to change the state of our entire application.

