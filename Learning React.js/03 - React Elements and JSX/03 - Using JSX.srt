1
00:00:01,00 --> 00:00:01,90
- [Instructor] In the previous video

2
00:00:01,90 --> 00:00:05,80
we rendered a div and then an h1 to the DOM using React.

3
00:00:05,80 --> 00:00:08,60
This only a tiny, little part of a UI,

4
00:00:08,60 --> 00:00:11,60
so using a couple of create element calls

5
00:00:11,60 --> 00:00:13,30
wasn't that big of a deal.

6
00:00:13,30 --> 00:00:16,20
Consider though that I wanted to render an unordered list

7
00:00:16,20 --> 00:00:19,00
and that the unordered list had 10 items.

8
00:00:19,00 --> 00:00:21,50
That would be a whole lot of create element calls.

9
00:00:21,50 --> 00:00:23,00
And that would be a lot of nesting

10
00:00:23,00 --> 00:00:25,00
that would be prone to errors.

11
00:00:25,00 --> 00:00:27,80
Now is a good time to incorporate JSX.

12
00:00:27,80 --> 00:00:31,50
JSX or JavaScript as XML is a language extension

13
00:00:31,50 --> 00:00:35,00
that allows you to write tags directly in the JavaScript.

14
00:00:35,00 --> 00:00:37,40
So let's go ahead and replace all of these

15
00:00:37,40 --> 00:00:41,00
create element calls that are inside of ReactDOM.render

16
00:00:41,00 --> 00:00:43,70
and instead I'm going to replace this, let me get rid

17
00:00:43,70 --> 00:00:46,60
of that parenthesis too.

18
00:00:46,60 --> 00:00:50,40
We'll add an h1

19
00:00:50,40 --> 00:00:52,90
and this h1 should include a bit of text

20
00:00:52,90 --> 00:00:54,30
so we'll add Hello World.

21
00:00:54,30 --> 00:00:57,10
We'll get that to save, we'll refresh our page.

22
00:00:57,10 --> 00:00:59,30
And we see that this isn't working.

23
00:00:59,30 --> 00:01:03,70
We see Uncaught SyntaxError, Unexpected token bracket.

24
00:01:03,70 --> 00:01:08,30
So we're adding this JSX, this JavaScript as XML,

25
00:01:08,30 --> 00:01:11,90
but the browser can't understand it, it can't interpret it.

26
00:01:11,90 --> 00:01:14,40
So what we need to do is add one last thing.

27
00:01:14,40 --> 00:01:17,60
So we need to add another script for Babel

28
00:01:17,60 --> 00:01:21,70
ad this is going to transform this JSX into code

29
00:01:21,70 --> 00:01:23,40
that will work in the browser.

30
00:01:23,40 --> 00:01:27,20
So let's go ahead and add that to our scripts here.

31
00:01:27,20 --> 00:01:29,10
So we're going to add a script.

32
00:01:29,10 --> 00:01:31,50
We'll give it a source attribute.

33
00:01:31,50 --> 00:01:36,00
And the source should link to https

34
00:01:36,00 --> 00:01:41,60
unpackage.com/@babel/standalone

35
00:01:41,60 --> 00:01:45,20
/babel.min.js.

36
00:01:45,20 --> 00:01:46,00
Perfect.

37
00:01:46,00 --> 00:01:49,60
So now if I try to run this again,

38
00:01:49,60 --> 00:01:52,50
I'm still getting this unexpected token as soon as it hits

39
00:01:52,50 --> 00:01:55,90
this opening bracket it doesn't know what to render.

40
00:01:55,90 --> 00:01:58,60
So the final step I need to take is I need to change

41
00:01:58,60 --> 00:02:03,70
this type attribute from text/javascript to text/babel.

42
00:02:03,70 --> 00:02:06,60
Now if I save that and I refresh that

43
00:02:06,60 --> 00:02:10,20
we should see Hello World being rendered to the DOM.

44
00:02:10,20 --> 00:02:14,30
So if I open up my Elements tab you'll notice that the div

45
00:02:14,30 --> 00:02:19,30
contains the h1 we're also seeing this script tag here.

46
00:02:19,30 --> 00:02:22,90
So if you expand the script you'll see all of the React code

47
00:02:22,90 --> 00:02:24,10
being located here.

48
00:02:24,10 --> 00:02:27,30
So JSX will allow us to use these tags directly

49
00:02:27,30 --> 00:02:29,10
in our React code.

50
00:02:29,10 --> 00:02:30,70
This will make it so that we don't have to use

51
00:02:30,70 --> 00:02:33,90
so many create elements which can lead to a lot of bugs

52
00:02:33,90 --> 00:02:37,00
because of punctuation and all sorts of nesting.

