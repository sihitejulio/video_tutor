1
00:00:01,00 --> 00:00:02,90
- [Instructor] We have our HTML file set up

2
00:00:02,90 --> 00:00:06,30
for React development, for importing these two links.

3
00:00:06,30 --> 00:00:08,50
We also have our React developer tools

4
00:00:08,50 --> 00:00:10,00
set up in our browser.

5
00:00:10,00 --> 00:00:12,70
Now, I can actually create a react element

6
00:00:12,70 --> 00:00:14,60
and render it to the page.

7
00:00:14,60 --> 00:00:16,60
So I'm going to zoom in a bunch here

8
00:00:16,60 --> 00:00:20,30
and we're going to add a script tag.

9
00:00:20,30 --> 00:00:23,10
This script tag is going to have a type attribute

10
00:00:23,10 --> 00:00:25,00
of text/javascript

11
00:00:25,00 --> 00:00:27,50
and then I can add my React

12
00:00:27,50 --> 00:00:29,70
directly here to this script tag.

13
00:00:29,70 --> 00:00:34,20
So, first I'm going to use ReactDOM.render

14
00:00:34,20 --> 00:00:37,80
and we're going to pass ReactDOM.render a couple of arguments.

15
00:00:37,80 --> 00:00:42,10
The first of which is React.createElement

16
00:00:42,10 --> 00:00:43,30
so in other words, I'm saying

17
00:00:43,30 --> 00:00:44,30
what do I want to create?

18
00:00:44,30 --> 00:00:47,30
What type of react element do I want to create?

19
00:00:47,30 --> 00:00:49,30
That's the first argument here.

20
00:00:49,30 --> 00:00:52,00
Create element is going to take in a few things,

21
00:00:52,00 --> 00:00:53,10
the first of which is

22
00:00:53,10 --> 00:00:56,40
what type of element we want to create in h1.

23
00:00:56,40 --> 00:00:58,00
We'll pass it any properties,

24
00:00:58,00 --> 00:01:00,40
in this case I don't want to pass anything yet.

25
00:01:00,40 --> 00:01:01,60
We'll pass null

26
00:01:01,60 --> 00:01:03,70
and then I want to pass the child element,

27
00:01:03,70 --> 00:01:08,80
so what do I want to be displayed inside of this h1?

28
00:01:08,80 --> 00:01:10,40
Now, the second thing I want to pass here

29
00:01:10,40 --> 00:01:12,60
is where do I want to render this element?

30
00:01:12,60 --> 00:01:16,60
So, where do I want to inject this element into the DOM?

31
00:01:16,60 --> 00:01:18,10
Well, I want to add it here

32
00:01:18,10 --> 00:01:20,80
to this div with an Id of root.

33
00:01:20,80 --> 00:01:25,90
So we'll say, document.getElementbyId

34
00:01:25,90 --> 00:01:29,10
and we'll select the root element.

35
00:01:29,10 --> 00:01:32,40
Perfect, so now, I can refresh my page

36
00:01:32,40 --> 00:01:36,40
and I should see Hello World being rendered to the DOM.

37
00:01:36,40 --> 00:01:39,00
Then, if I open up my developer tools

38
00:01:39,00 --> 00:01:42,10
and I open up the react tab,

39
00:01:42,10 --> 00:01:45,30
we're going to see that the h1 is being rendered

40
00:01:45,30 --> 00:01:49,70
and then any children are going to be Hello Word here.

41
00:01:49,70 --> 00:01:52,20
I can also open the elements tab

42
00:01:52,20 --> 00:01:56,00
and we'll be able to find the div with the Id of root

43
00:01:56,00 --> 00:01:58,80
right here and you'll notice that the h1

44
00:01:58,80 --> 00:02:01,50
is directly inside of that.

45
00:02:01,50 --> 00:02:04,70
So let's add one other thing here.

46
00:02:04,70 --> 00:02:08,50
So, we talked about null being a stand-in

47
00:02:08,50 --> 00:02:10,70
for any properties that we want to add.

48
00:02:10,70 --> 00:02:12,10
Let's actually add some.

49
00:02:12,10 --> 00:02:14,40
So I'm going to replace null with an object,

50
00:02:14,40 --> 00:02:17,30
where style is going to be the key.

51
00:02:17,30 --> 00:02:21,80
So style should have a value for color

52
00:02:21,80 --> 00:02:25,00
and we'll set the color to red.

53
00:02:25,00 --> 00:02:28,40
So as soon as I hit save on that,

54
00:02:28,40 --> 00:02:32,70
and let's hit enter so we can actually see this.

55
00:02:32,70 --> 00:02:35,20
So again, the element I want to create,

56
00:02:35,20 --> 00:02:38,20
the h1, is first, any properties,

57
00:02:38,20 --> 00:02:41,70
and then the child element Hello World here.

58
00:02:41,70 --> 00:02:43,80
If I give that a save and a refresh,

59
00:02:43,80 --> 00:02:46,70
we should see the text being changed.

60
00:02:46,70 --> 00:02:49,80
So this is our first react element that we're creating,

61
00:02:49,80 --> 00:02:52,70
we're going to create it with the create element function

62
00:02:52,70 --> 00:02:54,30
and then we're going to add it with

63
00:02:54,30 --> 00:02:58,00
document.getElementById to the root.

