1
00:00:01,00 --> 00:00:03,80
- A solid understanding of JSX syntax is important

2
00:00:03,80 --> 00:00:07,10
for getting the most our of REACT and while JSX shares

3
00:00:07,10 --> 00:00:10,80
a lot of similarities with HTML, there are some differences.

4
00:00:10,80 --> 00:00:12,40
Let's take a look at some of the cool things

5
00:00:12,40 --> 00:00:16,60
you can do with JSX, it's a pretty dynamic syntax.

6
00:00:16,60 --> 00:00:19,50
So the first thing I want to do here, on line 11,

7
00:00:19,50 --> 00:00:22,70
is hit a couple of enters and I'm going to create

8
00:00:22,70 --> 00:00:26,80
a variable called city and I'm going to set that to Madrid.

9
00:00:26,80 --> 00:00:28,20
Now the next step I want to take is,

10
00:00:28,20 --> 00:00:33,30
let's replace our Hello World here with a JSX expression.

11
00:00:33,30 --> 00:00:35,40
So I'm going to use curly braces and then

12
00:00:35,40 --> 00:00:37,30
I'm going to pass the name of this variable

13
00:00:37,30 --> 00:00:40,40
into this set of curly braces.

14
00:00:40,40 --> 00:00:42,70
And I'm going to say Hello {city}

15
00:00:42,70 --> 00:00:46,50
So, if I give that a save and refresh.

16
00:00:46,50 --> 00:00:50,30
We should see Hello Madrid. So, we're able to use

17
00:00:50,30 --> 00:00:52,20
the variable name, we're able to use

18
00:00:52,20 --> 00:00:57,20
this JSX expression to pass a value into the JSX.

19
00:00:57,20 --> 00:00:59,50
When adding attributes to your JSX elements,

20
00:00:59,50 --> 00:01:02,00
you can add them directly to the tags.

21
00:01:02,00 --> 00:01:07,80
For example, if I added an id and it was called heading.

22
00:01:07,80 --> 00:01:11,30
And then I added some CSS, so we're just going adjust to

23
00:01:11,30 --> 00:01:15,20
the head tag, add a quick little style tag

24
00:01:15,20 --> 00:01:18,00
for some low budget styling and

25
00:01:18,00 --> 00:01:21,60
we're going to go ahead and use the #heading

26
00:01:21,60 --> 00:01:26,60
and, say, color and we'll set this to pink.

27
00:01:26,60 --> 00:01:28,70
So, as soon as I hit a refresh,

28
00:01:28,70 --> 00:01:30,90
we should see that this is now pink.

29
00:01:30,90 --> 00:01:34,40
We've used just simple attribute syntax

30
00:01:34,40 --> 00:01:38,50
to add an id to select this h1.

31
00:01:38,50 --> 00:01:40,60
You can also add a class of course.

32
00:01:40,60 --> 00:01:45,90
So let's go ahead and change this selector to the class.

33
00:01:45,90 --> 00:01:49,00
So we'll say .heading and I'll change this

34
00:01:49,00 --> 00:01:51,90
to className heading, so that's

35
00:01:51,90 --> 00:01:54,70
sort of a little "gotcha there".

36
00:01:54,70 --> 00:01:55,70
I'm going to change the color

37
00:01:55,70 --> 00:01:57,20
just so that we see it change.

38
00:01:57,20 --> 00:02:01,50
The className attribute is what we want to use in React.

39
00:02:01,50 --> 00:02:05,60
Not "class", because class is a reserved word in JavaScript.

40
00:02:05,60 --> 00:02:07,70
So, let's go ahead and hit refresh and

41
00:02:07,70 --> 00:02:11,80
we should see that that class is being applied to the h1.

42
00:02:11,80 --> 00:02:14,10
So, we'll go through a lot more techniques

43
00:02:14,10 --> 00:02:15,90
that we'll use use with JSX throughout

44
00:02:15,90 --> 00:02:17,60
the rest of the course but these are

45
00:02:17,60 --> 00:02:20,00
a couple of things to know before we get started.

