1
00:00:00,00 --> 00:00:02,70
- [Instructor] Now that we have this one h one

2
00:00:02,70 --> 00:00:04,70
being rendered to the page, let's take a look

3
00:00:04,70 --> 00:00:06,40
at what it might look like to render more

4
00:00:06,40 --> 00:00:08,90
than one element with react.

5
00:00:08,90 --> 00:00:10,10
So the first thing I want to do is

6
00:00:10,10 --> 00:00:13,60
let's change this h one to a div.

7
00:00:13,60 --> 00:00:17,20
The second argument here I will replace with null

8
00:00:17,20 --> 00:00:19,60
from before 'cause we don't want to add any properties.

9
00:00:19,60 --> 00:00:21,80
Again the third argument is always going

10
00:00:21,80 --> 00:00:25,30
to refer to any children that that element has.

11
00:00:25,30 --> 00:00:28,20
The div should have a child of an h one.

12
00:00:28,20 --> 00:00:32,50
So we're going to add another nested create element call here.

13
00:00:32,50 --> 00:00:34,20
And create element, once again,

14
00:00:34,20 --> 00:00:37,50
will have the arguments we're accustomed to.

15
00:00:37,50 --> 00:00:40,70
It'll have what element we want to create, the h one.

16
00:00:40,70 --> 00:00:43,00
We'll use null for any properties,

17
00:00:43,00 --> 00:00:47,60
and then we'll say oh hello, perfect.

18
00:00:47,60 --> 00:00:50,80
So now if I hit refresh we should see oh hello.

19
00:00:50,80 --> 00:00:53,60
I can expand our developer tools,

20
00:00:53,60 --> 00:00:57,00
and you'll notice that we have a little tree going on.

21
00:00:57,00 --> 00:01:00,80
We have a div that wraps around an h one.

22
00:01:00,80 --> 00:01:02,70
And if we go to the elements tab,

23
00:01:02,70 --> 00:01:04,80
we'll notice that our div is here.

24
00:01:04,80 --> 00:01:06,60
It has a div inside of it and then

25
00:01:06,60 --> 00:01:08,90
inside of that is an h one.

26
00:01:08,90 --> 00:01:10,70
So this is how we would use multiple

27
00:01:10,70 --> 00:01:13,20
react create element calls to create

28
00:01:13,20 --> 00:01:16,00
nested elements on the dom.

