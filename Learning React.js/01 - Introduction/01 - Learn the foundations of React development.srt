1
00:00:01,00 --> 00:00:02,70
- [Eve] React is a Java Script library

2
00:00:02,70 --> 00:00:06,60
for building user interfaces and it is everywhere.

3
00:00:06,60 --> 00:00:10,00
React can be found on huge websites like Airbnb,

4
00:00:10,00 --> 00:00:12,70
Twitter, and of course, Facebook.

5
00:00:12,70 --> 00:00:14,30
In this course we're going to get a handle

6
00:00:14,30 --> 00:00:17,50
on the fundamentals by looking at the most popular features

7
00:00:17,50 --> 00:00:19,20
of the library.

8
00:00:19,20 --> 00:00:20,30
I'm Eve Porcello.

9
00:00:20,30 --> 00:00:22,50
I'm a software engineer and a teacher.

10
00:00:22,50 --> 00:00:24,00
I've built apps with React.

11
00:00:24,00 --> 00:00:26,00
I've created courses about React.

12
00:00:26,00 --> 00:00:28,00
I even wrote a book about React.

13
00:00:28,00 --> 00:00:30,20
What can I say, React is really awesome.

14
00:00:30,20 --> 00:00:33,60
And React isn't going anywhere so now is a great time

15
00:00:33,60 --> 00:00:35,00
to start your journey.

