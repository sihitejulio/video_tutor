1
00:00:01,00 --> 00:00:03,40
- [Instructor] Learning React is an intro to React,

2
00:00:03,40 --> 00:00:05,50
but I'm going to assume that you're taking this course

3
00:00:05,50 --> 00:00:08,30
with a few front-end skills in place.

4
00:00:08,30 --> 00:00:12,20
Number one, you should be familiar with HTML and CSS.

5
00:00:12,20 --> 00:00:14,50
If you haven't worked with these languages before,

6
00:00:14,50 --> 00:00:18,10
you may want to check out HTML5: Structure, Syntax,

7
00:00:18,10 --> 00:00:21,20
and Semantics with James Williamson.

8
00:00:21,20 --> 00:00:23,70
Also, React is built out of JavaScript,

9
00:00:23,70 --> 00:00:26,30
so you do need to know JavaScript.

10
00:00:26,30 --> 00:00:28,10
If you aren't familiar with JavaScript,

11
00:00:28,10 --> 00:00:30,50
I'd recommend JavaScript Essential Training

12
00:00:30,50 --> 00:00:32,70
with Morten Ran-Hendrickson.

13
00:00:32,70 --> 00:00:35,60
The course will also use some of the most modern

14
00:00:35,60 --> 00:00:38,30
JavaScript syntax, so if you're not familiar

15
00:00:38,30 --> 00:00:40,90
with the latest ECMAScript features, my class,

16
00:00:40,90 --> 00:00:44,10
Learning ES6, could be a good supplement.

17
00:00:44,10 --> 00:00:46,10
This course will build our knowledge of React

18
00:00:46,10 --> 00:00:49,60
from the ground up, so your background with HTML, CSS,

19
00:00:49,60 --> 00:00:53,00
and JavaScript will prepare you well for learning React.

