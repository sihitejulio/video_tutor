1
00:00:01,00 --> 00:00:02,80
- [Instructor] To get started with a React project,

2
00:00:02,80 --> 00:00:04,30
you don't need to set up any sort

3
00:00:04,30 --> 00:00:06,30
of complicated project folder.

4
00:00:06,30 --> 00:00:08,70
You can just start with an HTML file.

5
00:00:08,70 --> 00:00:11,40
So in my start folder, I'm going to create a new file

6
00:00:11,40 --> 00:00:14,30
called index.html, and in this file,

7
00:00:14,30 --> 00:00:18,00
we're going to create our first React file.

8
00:00:18,00 --> 00:00:20,10
So first let's set up our HTML page.

9
00:00:20,10 --> 00:00:22,70
We're going to add a doc type.

10
00:00:22,70 --> 00:00:25,00
We'll add the HTML tag.

11
00:00:25,00 --> 00:00:27,00
We'll add a head tag.

12
00:00:27,00 --> 00:00:30,20
This head tag is just going to have a title,

13
00:00:30,20 --> 00:00:34,50
and we'll call this React Project.

14
00:00:34,50 --> 00:00:39,00
Within the body of this document, we're going to add a div.

15
00:00:39,00 --> 00:00:43,70
This div should have an ID of root,

16
00:00:43,70 --> 00:00:45,70
and I'll give that a save.

17
00:00:45,70 --> 00:00:48,80
The next thing I want to do is incorporate the React library,

18
00:00:48,80 --> 00:00:50,40
so let's add a script tag.

19
00:00:50,40 --> 00:00:54,10
This script tag should have a source,

20
00:00:54,10 --> 00:01:05,50
and the source should be https://unpkg.com/react@16.7.0,

21
00:01:05,50 --> 00:01:13,00
and then we'll use /umd/react.development.js.

22
00:01:13,00 --> 00:01:18,30
So this is pulling React from the unpkg CDN,

23
00:01:18,30 --> 00:01:20,60
and this is going to help us work with React.

24
00:01:20,60 --> 00:01:22,40
We need one other script.

25
00:01:22,40 --> 00:01:23,70
I'm going to copy and paste this

26
00:01:23,70 --> 00:01:26,20
because the change is minimal here.

27
00:01:26,20 --> 00:01:30,80
All we need to incorporate is react-dom

28
00:01:30,80 --> 00:01:34,70
and similarly react-dom.

29
00:01:34,70 --> 00:01:38,00
Cool, so this is really all we need to get started.

