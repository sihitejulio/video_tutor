1
00:00:01,00 --> 00:00:02,60
- [Instructor] React is a JavaScript library

2
00:00:02,60 --> 00:00:05,20
that's used for building user interfaces.

3
00:00:05,20 --> 00:00:07,60
It's an opensource project that started at Facebook

4
00:00:07,60 --> 00:00:09,90
and is maintained by developers at Facebook

5
00:00:09,90 --> 00:00:12,90
and also by a large community if contributors.

6
00:00:12,90 --> 00:00:14,90
React aims to make developing large scale,

7
00:00:14,90 --> 00:00:17,60
single-page applications easier.

8
00:00:17,60 --> 00:00:19,50
React is also part of a larger movement

9
00:00:19,50 --> 00:00:21,00
in the world of JavaScript.

10
00:00:21,00 --> 00:00:23,50
You may have heard about functional JavaScript,

11
00:00:23,50 --> 00:00:27,00
a programming paradigm that emphasizes function composition

12
00:00:27,00 --> 00:00:29,50
over object orientation.

13
00:00:29,50 --> 00:00:32,30
Since being opensourced in 2013,

14
00:00:32,30 --> 00:00:34,90
React has exploded in popularity.

15
00:00:34,90 --> 00:00:37,30
It's commonly used in enterprise applications

16
00:00:37,30 --> 00:00:41,00
by companies including PayPal, Airbnb, Apple,

17
00:00:41,00 --> 00:00:44,00
Microsoft and, of course, Facebook.

18
00:00:44,00 --> 00:00:45,60
React is one of the most popular

19
00:00:45,60 --> 00:00:47,50
JavaScript libraries available,

20
00:00:47,50 --> 00:00:49,20
but I'm not here just to tell you to use it

21
00:00:49,20 --> 00:00:50,50
because it's popular.

22
00:00:50,50 --> 00:00:52,60
In this course, we'll talk about why React

23
00:00:52,60 --> 00:00:54,10
and how to get busy creating

24
00:00:54,10 --> 00:00:57,00
fantastic projects really quickly.

