1
00:00:00,00 --> 00:00:01,90
- [Instructor] To get ready to work

2
00:00:01,90 --> 00:00:04,40
on the React project, a great place to get started

3
00:00:04,40 --> 00:00:06,80
is to install React Developer tools.

4
00:00:06,80 --> 00:00:08,60
Let's look at how we would install

5
00:00:08,60 --> 00:00:11,20
the Developer tools using Chrome.

6
00:00:11,20 --> 00:00:13,60
So, I'm here at the Chrome web store,

7
00:00:13,60 --> 00:00:17,90
and I'm going to type React Developer tools

8
00:00:17,90 --> 00:00:19,60
into the search box.

9
00:00:19,60 --> 00:00:21,50
And now, I can click Add to Chrome,

10
00:00:21,50 --> 00:00:24,00
which will add this as an extension.

11
00:00:24,00 --> 00:00:27,50
Now, the next that I want to do is go to a site

12
00:00:27,50 --> 00:00:30,60
that uses React, so I'm going to go to moonhighway.com.

13
00:00:30,60 --> 00:00:33,90
And you'll notice that the React Developer tools

14
00:00:33,90 --> 00:00:36,70
are lit up over here, and this looks like

15
00:00:36,70 --> 00:00:39,50
we are using React on this page.

16
00:00:39,50 --> 00:00:41,90
Now, if I hit Cmd+Opt+J on my Mac,

17
00:00:41,90 --> 00:00:45,10
or Ctrl+Shift+J on a PC, I'm going to be able

18
00:00:45,10 --> 00:00:47,40
to open up my Developer tools.

19
00:00:47,40 --> 00:00:49,40
Then, if I go to the React tab,

20
00:00:49,40 --> 00:00:51,10
this is going to show me all of the different

21
00:00:51,10 --> 00:00:53,00
elements that make up this page.

22
00:00:53,00 --> 00:00:54,90
So, this is pretty cool.

23
00:00:54,90 --> 00:00:56,10
Now, the next thing I want to do

24
00:00:56,10 --> 00:00:58,40
is let's open up the React project,

25
00:00:58,40 --> 00:01:03,40
so the index file in the 01_03 start folder.

26
00:01:03,40 --> 00:01:05,90
Even though, I do have React added to the page,

27
00:01:05,90 --> 00:01:09,40
it looks like the React Developer tools aren't lit up.

28
00:01:09,40 --> 00:01:11,10
The reason for this is I'm opening

29
00:01:11,10 --> 00:01:13,50
this file from my file system.

30
00:01:13,50 --> 00:01:18,70
So, if you go to this menu here to Customize Chrome,

31
00:01:18,70 --> 00:01:21,70
and you open up More Tools,

32
00:01:21,70 --> 00:01:23,70
there's an option here called Extensions.

33
00:01:23,70 --> 00:01:25,90
This is going to open up all the extensions

34
00:01:25,90 --> 00:01:28,10
that are currently added to my browser.

35
00:01:28,10 --> 00:01:30,10
Now, if I click on Details here,

36
00:01:30,10 --> 00:01:33,10
this is going to give us all of the different

37
00:01:33,10 --> 00:01:35,40
preferences we can use to set this up.

38
00:01:35,40 --> 00:01:37,70
Now, there's an option here at the bottom,

39
00:01:37,70 --> 00:01:40,40
Allow Access to File URLs.

40
00:01:40,40 --> 00:01:42,40
We want to make sure to enable that.

41
00:01:42,40 --> 00:01:44,70
Now, what this is going to do is it's going to make it

42
00:01:44,70 --> 00:01:46,70
so that if I reload this page,

43
00:01:46,70 --> 00:01:49,70
we have the React Developer tools setup.

44
00:01:49,70 --> 00:01:51,40
Now, this is letting me know that I'm using

45
00:01:51,40 --> 00:01:53,20
the development build of React.

46
00:01:53,20 --> 00:01:55,20
It looks a little different, it's bright red,

47
00:01:55,20 --> 00:01:59,30
versus the production website which has the black and blue.

48
00:01:59,30 --> 00:02:00,50
This is just because I'm using

49
00:02:00,50 --> 00:02:03,50
the Development version of React on this page.

50
00:02:03,50 --> 00:02:06,30
When I move this application into production,

51
00:02:06,30 --> 00:02:09,10
we'll see that go away.

52
00:02:09,10 --> 00:02:10,80
Okay, so there we go.

53
00:02:10,80 --> 00:02:13,10
Our Chrome browser is now ready to develop

54
00:02:13,10 --> 00:02:16,00
some real projects using React.

