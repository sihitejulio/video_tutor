1
00:00:01,00 --> 00:00:02,60
- [Instructor] Right now, we have multiple components

2
00:00:02,60 --> 00:00:05,50
being rendered to our page, but there may be cases

3
00:00:05,50 --> 00:00:07,80
where you want to conditionally render

4
00:00:07,80 --> 00:00:10,80
a component based on some sort of a condition.

5
00:00:10,80 --> 00:00:14,20
So we're going to make some changes to our app here

6
00:00:14,20 --> 00:00:16,00
so that we can make that happen.

7
00:00:16,00 --> 00:00:17,50
Now, the first thing I want to do,

8
00:00:17,50 --> 00:00:19,30
aside from zooming in a little bit, is

9
00:00:19,30 --> 00:00:21,60
let's go ahead and take pretty much everything

10
00:00:21,60 --> 00:00:25,60
that is in our app component and we're going to just delete it.

11
00:00:25,60 --> 00:00:27,70
So we're going to make this a little bit smaller,

12
00:00:27,70 --> 00:00:29,50
a little bit more manageable.

13
00:00:29,50 --> 00:00:31,30
Now, the second thing I want to do is,

14
00:00:31,30 --> 00:00:33,30
right now, we have a component called lake,

15
00:00:33,30 --> 00:00:36,40
I'm going to add another one called ski resort.

16
00:00:36,40 --> 00:00:38,50
So, these are two activities,

17
00:00:38,50 --> 00:00:41,30
the lake is something that happens in the summer

18
00:00:41,30 --> 00:00:43,80
and then the ski resort, we are going

19
00:00:43,80 --> 00:00:46,40
to take advantage of in the winter.

20
00:00:46,40 --> 00:00:49,00
So, let's create an h1 for this,

21
00:00:49,00 --> 00:00:52,10
the ski resort will also have a name and

22
00:00:52,10 --> 00:00:55,30
within the app, we can conditionally render this component.

23
00:00:55,30 --> 00:00:58,70
So, actually let's go ahead and get rid of the entire app.

24
00:00:58,70 --> 00:01:02,00
We're going to replace this with a function component

25
00:01:02,00 --> 00:01:07,40
and we're going to just go ahead and add a div here.

26
00:01:07,40 --> 00:01:13,70
Now, the div is going to wrap around a JSX expression.

27
00:01:13,70 --> 00:01:16,80
Now, in this expression, we're going to check to see

28
00:01:16,80 --> 00:01:19,40
if there is a value for summer.

29
00:01:19,40 --> 00:01:23,30
So we're going to first pass this down as a prop,

30
00:01:23,30 --> 00:01:26,00
so we'll say summer false,

31
00:01:26,00 --> 00:01:29,60
we then are going to pass this down to the app component.

32
00:01:29,60 --> 00:01:33,40
So, we're going to make sure that this is displayed here.

33
00:01:33,40 --> 00:01:34,60
Let's go ahead and add summer,

34
00:01:34,60 --> 00:01:37,20
we'll destructure that from the props objects

35
00:01:37,20 --> 00:01:39,90
and then we're going to go ahead and say alright,

36
00:01:39,90 --> 00:01:46,50
if summer is true, let's go ahead and display the lake

37
00:01:46,50 --> 00:01:48,30
and then if summer is false,

38
00:01:48,30 --> 00:01:51,40
let's go ahead and display the ski resort.

39
00:01:51,40 --> 00:01:54,80
So, this is what that inline if else statement looks like.

40
00:01:54,80 --> 00:01:57,70
This is going to check to see if this is true.

41
00:01:57,70 --> 00:02:00,60
If so, we'll display this lake component,

42
00:02:00,60 --> 00:02:03,20
if not, we'll display the ski resort.

43
00:02:03,20 --> 00:02:05,40
Now, there's one other thing that we're missing here.

44
00:02:05,40 --> 00:02:07,20
So, our app component is being rendered,

45
00:02:07,20 --> 00:02:10,10
we have the summer property being passed to it,

46
00:02:10,10 --> 00:02:12,10
we also need to do the same thing for lake

47
00:02:12,10 --> 00:02:15,70
because lake needs to display something inside of the h1

48
00:02:15,70 --> 00:02:19,30
and ski resort needs to display a name inside of the h1 too.

49
00:02:19,30 --> 00:02:21,30
So, let's do this, I'm going to hit Enter

50
00:02:21,30 --> 00:02:23,80
and this'll give us a little more real estate

51
00:02:23,80 --> 00:02:26,30
to add a name to our lake.

52
00:02:26,30 --> 00:02:32,60
We'll say equals, and we'll use a string here, Lake Tahoe.

53
00:02:32,60 --> 00:02:38,00
And then we're going to add a name for Alpine Meadows.

54
00:02:38,00 --> 00:02:39,10
Perfect.

55
00:02:39,10 --> 00:02:43,10
Now, what I can do is hit a refresh

56
00:02:43,10 --> 00:02:46,10
and I should see that the ski resort is being displayed

57
00:02:46,10 --> 00:02:49,40
and that makes sense because summer is false,

58
00:02:49,40 --> 00:02:52,40
so this doesn't evaluate as true,

59
00:02:52,40 --> 00:02:54,10
we skip over this condition

60
00:02:54,10 --> 00:02:56,50
and instead go straight to the ski resort.

61
00:02:56,50 --> 00:02:58,50
Now, what happens if that's true?

62
00:02:58,50 --> 00:03:00,50
We'll give this a refresh,

63
00:03:00,50 --> 00:03:03,30
and there we go, we have summer evaluating as true

64
00:03:03,30 --> 00:03:05,90
and then we can display the lake.

65
00:03:05,90 --> 00:03:09,00
So, this is how we might conditionally render components

66
00:03:09,00 --> 00:03:12,80
based on some sort of truthiness, so we're going to say

67
00:03:12,80 --> 00:03:15,90
that summer is true, we can display the lake,

68
00:03:15,90 --> 00:03:20,00
when false, we'll display the ski resort.

