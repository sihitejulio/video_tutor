1
00:00:01,10 --> 00:00:02,20
- [Instructor] In the last video we looked

2
00:00:02,20 --> 00:00:04,50
at how to iterate over a list

3
00:00:04,50 --> 00:00:06,70
and display different elements based

4
00:00:06,70 --> 00:00:08,90
on the data in that list.

5
00:00:08,90 --> 00:00:11,40
Lets go ahead and adjust our lake list a little bit

6
00:00:11,40 --> 00:00:14,40
to include objects instead of strings.

7
00:00:14,40 --> 00:00:16,40
So lets first replace Echo Lake

8
00:00:16,40 --> 00:00:19,10
we're going to give it an ID.

9
00:00:19,10 --> 00:00:21,30
And the ID will be 1.

10
00:00:21,30 --> 00:00:27,90
Lets give it a name, and we'll use Echo Lake.

11
00:00:27,90 --> 00:00:31,00
And then finally lets give this a value for trailhead.

12
00:00:31,00 --> 00:00:34,40
And we'll say Echo Lake.

13
00:00:34,40 --> 00:00:37,10
Cut and paste that as you see fit.

14
00:00:37,10 --> 00:00:38,30
Now the next thing we're going to do

15
00:00:38,30 --> 00:00:44,30
is copy, and paste this to the next line.

16
00:00:44,30 --> 00:00:45,80
And just replace the data.

17
00:00:45,80 --> 00:00:51,30
We will say this one is Maud, and this one is Wright's Lake.

18
00:00:51,30 --> 00:00:55,40
And finally we'll replace this ID with 3.

19
00:00:55,40 --> 00:01:00,00
And we'll replace this one with Cascade Lake.

20
00:01:00,00 --> 00:01:04,00
And the trail head is Bayview.

21
00:01:04,00 --> 00:01:05,20
Alright cool.

22
00:01:05,20 --> 00:01:07,80
So now if I hit refresh we're going to se that nothing

23
00:01:07,80 --> 00:01:10,90
is rendering cause we can't find the value

24
00:01:10,90 --> 00:01:13,20
for this lake.

25
00:01:13,20 --> 00:01:14,80
And what we're going to have to do instead,

26
00:01:14,80 --> 00:01:16,80
is we're going to have to pull

27
00:01:16,80 --> 00:01:19,40
the data off of each of these keys.

28
00:01:19,40 --> 00:01:20,50
So lets do that.

29
00:01:20,50 --> 00:01:22,40
The first thing that we are going to do

30
00:01:22,40 --> 00:01:26,30
is we're going to use lake dot name.

31
00:01:26,30 --> 00:01:27,70
We'll give that a save.

32
00:01:27,70 --> 00:01:30,80
We should see that those lakes are now appearing.

33
00:01:30,80 --> 00:01:34,70
Then we could say for example

34
00:01:34,70 --> 00:01:38,50
Trailhead and then we'll say

35
00:01:38,50 --> 00:01:42,50
lake dot trailhead and zoom that out

36
00:01:42,50 --> 00:01:43,90
and I'll give it a save.

37
00:01:43,90 --> 00:01:49,70
We should see that Echo Lake has the Trailhead: Echo Lake.

38
00:01:49,70 --> 00:01:52,40
Maude Lake, Wright's Lake et cetera.

39
00:01:52,40 --> 00:01:54,10
Maybe we should put a little pipe in between

40
00:01:54,10 --> 00:01:57,50
that so it looks a little more clear.

41
00:01:57,50 --> 00:01:59,90
Ou nice, that looks great.

42
00:01:59,90 --> 00:02:03,20
So we have some very very small data here

43
00:02:03,20 --> 00:02:06,50
but we're seeing that we can pull the values we need

44
00:02:06,50 --> 00:02:09,40
off of each of those keys and the object.

45
00:02:09,40 --> 00:02:10,90
The final thing I wanted to point out,

46
00:02:10,90 --> 00:02:13,10
is lets say you didn't want to use this

47
00:02:13,10 --> 00:02:17,10
iterating I each time. You could just use the ID.

48
00:02:17,10 --> 00:02:20,10
The way we do that is we get rid of lake and ID.

49
00:02:20,10 --> 00:02:23,70
Then just use the one parameter here.

50
00:02:23,70 --> 00:02:30,40
Then we can replace I with lets see lake dot ID. Perfect.

51
00:02:30,40 --> 00:02:33,50
So now we are not in violation at all of

52
00:02:33,50 --> 00:02:37,00
the key roles but we've given that a unique key.

53
00:02:37,00 --> 00:02:41,00
Again if I open up my console with command option J

54
00:02:41,00 --> 00:02:44,30
and I open the react panel, we're going to see

55
00:02:44,30 --> 00:02:45,80
that there's an app here.

56
00:02:45,80 --> 00:02:50,30
We have a list item with a key of 1, 2 and 3.

57
00:02:50,30 --> 00:02:54,90
So that corresponds to the IDs 1, 2, and 3.

58
00:02:54,90 --> 00:02:56,80
So that's a quick example of how we might

59
00:02:56,80 --> 00:03:00,90
iterate over an array of objects to display data.

60
00:03:00,90 --> 00:03:04,00
Unique data for each of these list items.

