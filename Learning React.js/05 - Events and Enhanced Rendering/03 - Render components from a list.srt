1
00:00:01,00 --> 00:00:01,90
- [Instructor] Currently,

2
00:00:01,90 --> 00:00:03,20
we're just rendering two components.

3
00:00:03,20 --> 00:00:04,90
There's one component, our App,

4
00:00:04,90 --> 00:00:07,10
and that's conditionally rendering either

5
00:00:07,10 --> 00:00:10,00
the lake or the ski resort component.

6
00:00:10,00 --> 00:00:12,50
Now one of the really cool things about React is

7
00:00:12,50 --> 00:00:15,90
as your data changes, as it becomes more dynamic,

8
00:00:15,90 --> 00:00:20,50
you can use React to render the correct number of elements

9
00:00:20,50 --> 00:00:22,80
based on whatever your data is.

10
00:00:22,80 --> 00:00:24,10
Now if that doesn't make sense,

11
00:00:24,10 --> 00:00:26,10
let's take a look at a quick example.

12
00:00:26,10 --> 00:00:27,40
So the first thing I want to do is

13
00:00:27,40 --> 00:00:30,20
let's get rid of our lake and our ski resort component

14
00:00:30,20 --> 00:00:32,70
and we're going to replace this with a

15
00:00:32,70 --> 00:00:35,50
constant called lake list

16
00:00:35,50 --> 00:00:38,00
and this is just going to be an array

17
00:00:38,00 --> 00:00:38,80
of a few lakes.

18
00:00:38,80 --> 00:00:41,80
So we'll have Echo Lake, we'll have Maud Lake,

19
00:00:41,80 --> 00:00:47,80
and we'll have Cascade Lake.

20
00:00:47,80 --> 00:00:48,70
Cool.

21
00:00:48,70 --> 00:00:53,50
And I'm going to hit Enter on these so that we can read them.

22
00:00:53,50 --> 00:00:54,70
And now once we have them,

23
00:00:54,70 --> 00:00:57,30
I'm going to adjust our div a little bit here.

24
00:00:57,30 --> 00:00:59,10
So first things first,

25
00:00:59,10 --> 00:01:02,30
I can take our lake list,

26
00:01:02,30 --> 00:01:03,70
and we're going to pass this

27
00:01:03,70 --> 00:01:05,80
as a property to the App component.

28
00:01:05,80 --> 00:01:09,20
So let's add a property called lakes.

29
00:01:09,20 --> 00:01:12,80
And we're going to set this to lake list.

30
00:01:12,80 --> 00:01:16,20
So this will pump all of our lakes from our array

31
00:01:16,20 --> 00:01:20,20
right here into this property.

32
00:01:20,20 --> 00:01:22,70
Cool, so the next thing we can do is

33
00:01:22,70 --> 00:01:26,10
pass lakes into our App component.

34
00:01:26,10 --> 00:01:28,20
And from here what we'll do is

35
00:01:28,20 --> 00:01:33,00
we'll replace this div with an unordered list.

36
00:01:33,00 --> 00:01:36,50
So based on the items that are in our array,

37
00:01:36,50 --> 00:01:38,80
Echo Lake, Maud Lake, and Cascade Lake,

38
00:01:38,80 --> 00:01:41,90
I want to create list items for each of these.

39
00:01:41,90 --> 00:01:42,80
So what I can do is

40
00:01:42,80 --> 00:01:46,40
I'll add a JSX expression here to line 27.

41
00:01:46,40 --> 00:01:49,40
And then we're going to map over these lakes.

42
00:01:49,40 --> 00:01:51,00
So when we map over them,

43
00:01:51,00 --> 00:01:52,70
we're going to say okay,

44
00:01:52,70 --> 00:01:55,70
for each of these lakes we want to display

45
00:01:55,70 --> 00:01:57,90
each of their names in an li.

46
00:01:57,90 --> 00:02:01,90
So we'll say for each lake,

47
00:02:01,90 --> 00:02:05,40
let's go ahead and return a list item,

48
00:02:05,40 --> 00:02:08,30
and inside of that list item should be the lake,

49
00:02:08,30 --> 00:02:11,10
so the name of the lake should be displayed.

50
00:02:11,10 --> 00:02:12,50
Let's take a look.

51
00:02:12,50 --> 00:02:13,40
I'm going to refresh,

52
00:02:13,40 --> 00:02:15,20
and we should see that this is working.

53
00:02:15,20 --> 00:02:18,60
Awesome, Echo Lake, Maud Lake, and Cascade Lake.

54
00:02:18,60 --> 00:02:20,80
Now there's one thing that's hiding out here.

55
00:02:20,80 --> 00:02:22,20
If I open up our console,

56
00:02:22,20 --> 00:02:23,50
we see a warning here.

57
00:02:23,50 --> 00:02:26,10
And it says each child in an array or iterator

58
00:02:26,10 --> 00:02:29,00
should have a unique key property.

59
00:02:29,00 --> 00:02:31,80
So whenever we're rendering some sort of a list,

60
00:02:31,80 --> 00:02:33,60
with dynamic values,

61
00:02:33,60 --> 00:02:35,50
we're going to need to give each a key

62
00:02:35,50 --> 00:02:39,20
so that React can re-render things appropriately

63
00:02:39,20 --> 00:02:41,30
according to the rendering rules.

64
00:02:41,30 --> 00:02:43,10
So, let's do this.

65
00:02:43,10 --> 00:02:46,20
We're going to add to our lake,

66
00:02:46,20 --> 00:02:49,40
we're going to add a set of parenthesis here,

67
00:02:49,40 --> 00:02:51,80
and I'm going to say lake and then i.

68
00:02:51,80 --> 00:02:55,30
So for each time I loop through these lakes,

69
00:02:55,30 --> 00:02:58,40
we're going to get a value for i,

70
00:02:58,40 --> 00:03:00,80
and we're going to set that value to the key.

71
00:03:00,80 --> 00:03:02,90
So now I can hit refresh,

72
00:03:02,90 --> 00:03:05,10
I've cleared this warning,

73
00:03:05,10 --> 00:03:09,40
and if I look at my React panel,

74
00:03:09,40 --> 00:03:12,50
I'm going to be able to see that my properties are these lakes,

75
00:03:12,50 --> 00:03:14,80
Echo, Maud, and Cascade.

76
00:03:14,80 --> 00:03:16,60
If I expand our App,

77
00:03:16,60 --> 00:03:18,30
we see that each of these has a key,

78
00:03:18,30 --> 00:03:20,40
zero, one, and two.

79
00:03:20,40 --> 00:03:22,70
So this way if any updates happen,

80
00:03:22,70 --> 00:03:25,60
React will know exactly which element to target

81
00:03:25,60 --> 00:03:27,40
based on that key.

82
00:03:27,40 --> 00:03:28,70
So this is a quick example

83
00:03:28,70 --> 00:03:30,80
of how you might iterate over a list

84
00:03:30,80 --> 00:03:34,00
and display according to the data that you have.

