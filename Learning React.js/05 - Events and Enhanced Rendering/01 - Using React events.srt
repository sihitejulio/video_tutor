1
00:00:01,00 --> 00:00:02,30
- [Instructor] In a React application,

2
00:00:02,30 --> 00:00:05,80
like any application, we'll want to be able to track events.

3
00:00:05,80 --> 00:00:07,60
In this video, we'll take a closer look

4
00:00:07,60 --> 00:00:11,20
at how to bind event functions to UI elements.

5
00:00:11,20 --> 00:00:14,90
So right now we have our logged in value in the state.

6
00:00:14,90 --> 00:00:18,70
We're going to go ahead and hit enter on these lines

7
00:00:18,70 --> 00:00:21,10
just so that we can see what's going on here

8
00:00:21,10 --> 00:00:23,40
a little bit more clearly.

9
00:00:23,40 --> 00:00:30,10
We have a bit of text and a JSX expression her in our div.

10
00:00:30,10 --> 00:00:31,60
Now the next step I want to take is

11
00:00:31,60 --> 00:00:35,10
let's add a function called login.

12
00:00:35,10 --> 00:00:39,60
And this is going to be a local method to this component.

13
00:00:39,60 --> 00:00:43,00
And we're going to use this to trigger a change in state.

14
00:00:43,00 --> 00:00:45,80
So we're going to use this.setState

15
00:00:45,80 --> 00:00:47,90
and we're going to pass in a new state object

16
00:00:47,90 --> 00:00:49,90
that reflects a new state.

17
00:00:49,90 --> 00:00:55,70
So let's say loggedIn is true, perfect.

18
00:00:55,70 --> 00:00:58,60
So now every time I call this log in function,

19
00:00:58,60 --> 00:01:03,50
I want to logged in state value to be changed to true.

20
00:01:03,50 --> 00:01:06,10
Now the next thing I can do to make this happen

21
00:01:06,10 --> 00:01:10,50
is let's add a button here next to this div.

22
00:01:10,50 --> 00:01:16,10
Now the button is going to say Log In

23
00:01:16,10 --> 00:01:18,00
and let me hit save on that and refresh

24
00:01:18,00 --> 00:01:20,10
just to see the button.

25
00:01:20,10 --> 00:01:21,60
Right now it doesn't do anything,

26
00:01:21,60 --> 00:01:24,10
but we can an add an on click handler to this.

27
00:01:24,10 --> 00:01:29,10
So onClick=(this.logIn).

28
00:01:29,10 --> 00:01:34,50
So now if I hit another refresh and notice it says

29
00:01:34,50 --> 00:01:36,40
this user is not logged in,

30
00:01:36,40 --> 00:01:40,60
if I click Log In, it says the user is logged in, awesome.

31
00:01:40,60 --> 00:01:44,10
So let's go ahead and add another one of these functions

32
00:01:44,10 --> 00:01:46,20
for log out.

33
00:01:46,20 --> 00:01:49,60
And we'll say this.setState and we'll pass in

34
00:01:49,60 --> 00:01:52,60
loggedIn is false.

35
00:01:52,60 --> 00:01:54,80
So this will allow me to log out

36
00:01:54,80 --> 00:01:56,80
every time I click that button.

37
00:01:56,80 --> 00:01:59,10
I'm going to copy and paste this button

38
00:01:59,10 --> 00:02:01,00
'cause it's going to look much the same

39
00:02:01,00 --> 00:02:03,80
instead of log in it'll say log out.

40
00:02:03,80 --> 00:02:07,90
Instead of log in it'll say log out, perfect.

41
00:02:07,90 --> 00:02:11,90
So now I can see both of these buttons are appearing here.

42
00:02:11,90 --> 00:02:14,70
Our initial state is false.

43
00:02:14,70 --> 00:02:17,10
So it says the user is not logged in.

44
00:02:17,10 --> 00:02:20,30
As soon as I click log in, the user is logged in.

45
00:02:20,30 --> 00:02:24,00
As soon as I click log out, the user is not logged in.

46
00:02:24,00 --> 00:02:27,20
Cool, so this is our first example of binding

47
00:02:27,20 --> 00:02:29,80
an event function to a UI element.

48
00:02:29,80 --> 00:02:31,80
We have an initial state variable

49
00:02:31,80 --> 00:02:35,00
and then we can change it using these functions.

