1
00:00:00,50 --> 00:00:02,30
- [Manny] Hi, I'm Manny Henri,

2
00:00:02,30 --> 00:00:05,00
and I've been working with React since it's release.

3
00:00:05,00 --> 00:00:07,50
Building animations and complex interactions

4
00:00:07,50 --> 00:00:10,80
is a great next step after learning React Basics.

5
00:00:10,80 --> 00:00:12,80
In this course, take the next step

6
00:00:12,80 --> 00:00:15,40
in your evolution as a React Developer.

7
00:00:15,40 --> 00:00:17,20
We will explore the tools and techniques

8
00:00:17,20 --> 00:00:19,20
you will need to build complex and fun

9
00:00:19,20 --> 00:00:21,80
interactions in your React applications.

10
00:00:21,80 --> 00:00:23,60
I use these interaction techniques

11
00:00:23,60 --> 00:00:25,80
often in my applications.

12
00:00:25,80 --> 00:00:28,20
First, we will create a base project

13
00:00:28,20 --> 00:00:30,70
with a few tools and libraries.

14
00:00:30,70 --> 00:00:33,30
Then, we will look at common interactions

15
00:00:33,30 --> 00:00:36,80
and how to achieve them using libraries and states.

16
00:00:36,80 --> 00:00:38,60
We'll also combine several techniques

17
00:00:38,60 --> 00:00:40,30
to build a complex interaction,

18
00:00:40,30 --> 00:00:42,20
and then, go through UX concepts

19
00:00:42,20 --> 00:00:45,30
to implement them with the new techniques.

20
00:00:45,30 --> 00:00:46,80
If you're ready to learn how to

21
00:00:46,80 --> 00:00:48,80
create interactions with React,

22
00:00:48,80 --> 00:00:52,00
fire up your favorite editor and let's get started.

