1
00:00:01,00 --> 00:00:03,20
- [Narrator] If you have access to the exercise files

2
00:00:03,20 --> 00:00:06,30
that accompany this course, you can download them now

3
00:00:06,30 --> 00:00:08,50
and extract them to your desktop.

4
00:00:08,50 --> 00:00:10,10
Once you have them on your desktop,

5
00:00:10,10 --> 00:00:12,80
click on the Exercise Files, and let's here,

6
00:00:12,80 --> 00:00:15,70
for example, we're in chapter two of the course,

7
00:00:15,70 --> 00:00:20,20
and then, video 02_04, and then, we are at the end state,

8
00:00:20,20 --> 00:00:22,50
then you can get the files from here.

9
00:00:22,50 --> 00:00:25,20
And that goes for any other videos

10
00:00:25,20 --> 00:00:27,50
or chapters in the course.

11
00:00:27,50 --> 00:00:28,90
And once you have these files,

12
00:00:28,90 --> 00:00:31,20
you can open them in your favorite editor.

13
00:00:31,20 --> 00:00:33,00
So let's say, for example, in this case,

14
00:00:33,00 --> 00:00:35,10
we are using VS Code.

15
00:00:35,10 --> 00:00:37,50
We can literally drag and drop these files

16
00:00:37,50 --> 00:00:43,00
inside of VS Code, or simply open VS Code, like so.

17
00:00:43,00 --> 00:00:44,60
And once you have VS Code open,

18
00:00:44,60 --> 00:00:50,00
you can open a folder, go to the actual desktop,

19
00:00:50,00 --> 00:00:54,40
Exercise Files, chapter two, 02_04,

20
00:00:54,40 --> 00:00:58,50
end state, and then, open those files.

21
00:00:58,50 --> 00:01:01,50
Let me just make that bigger.

22
00:01:01,50 --> 00:01:03,90
And then, you can open the integrated terminal.

23
00:01:03,90 --> 00:01:06,00
So click on View,

24
00:01:06,00 --> 00:01:09,20
Integrated Terminal,

25
00:01:09,20 --> 00:01:21,80
then do an npm install to install all the dependencies.

26
00:01:21,80 --> 00:01:24,10
And once you have the dependencies installed,

27
00:01:24,10 --> 00:01:27,40
you can just do npm start, like so,

28
00:01:27,40 --> 00:01:29,40
and the application will start for you.

29
00:01:29,40 --> 00:01:30,00
And, off you go.

