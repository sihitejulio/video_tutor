1
00:00:01,00 --> 00:00:02,50
- [Narrator] This course is focused on building

2
00:00:02,50 --> 00:00:04,60
complex interactions with React.

3
00:00:04,60 --> 00:00:07,20
So, some experience with React is a must.

4
00:00:07,20 --> 00:00:09,90
If you aren't familiar with creating React components,

5
00:00:09,90 --> 00:00:12,50
using props, and importing modules,

6
00:00:12,50 --> 00:00:14,80
I'd suggest that you take a basic React course,

7
00:00:14,80 --> 00:00:17,90
such as, React Essential Training, in the library.

8
00:00:17,90 --> 00:00:20,00
Also, experience with JavaScript,

9
00:00:20,00 --> 00:00:23,70
especially ES6 syntax, is a must for this course,

10
00:00:23,70 --> 00:00:26,90
as this is the basis of a React Programmer.

11
00:00:26,90 --> 00:00:31,40
In this course, I use the ES 2015 version of the syntax.

12
00:00:31,40 --> 00:00:33,60
Next, being familiar with terminal commands

13
00:00:33,60 --> 00:00:36,30
and NPM isn't a must, but will definitely

14
00:00:36,30 --> 00:00:38,80
help you when we use these tools.

15
00:00:38,80 --> 00:00:41,20
As you see, I'm using a Mac, but you can

16
00:00:41,20 --> 00:00:43,60
follow along on a PC, because the tools

17
00:00:43,60 --> 00:00:46,50
work exactly the same on both platforms.

18
00:00:46,50 --> 00:00:48,60
I'm using VS Code as my code editor,

19
00:00:48,60 --> 00:00:51,40
but feel free to use whichever editor you prefer,

20
00:00:51,40 --> 00:00:53,30
although I recommend using an editor

21
00:00:53,30 --> 00:00:55,00
with a built-in terminal.

