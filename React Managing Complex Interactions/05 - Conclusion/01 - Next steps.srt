1
00:00:01,00 --> 00:00:02,60
- [Narrator] This course was a in depth look

2
00:00:02,60 --> 00:00:05,40
into interactions and animations with React,

3
00:00:05,40 --> 00:00:07,00
and the related libraries to achieve

4
00:00:07,00 --> 00:00:09,20
these complex interactions.

5
00:00:09,20 --> 00:00:11,10
With this knowledge, you should be able to build

6
00:00:11,10 --> 00:00:13,60
a React application and apply these techniques

7
00:00:13,60 --> 00:00:17,20
to create amazing UX experience for users.

8
00:00:17,20 --> 00:00:19,60
Building intuitive applications and what are

9
00:00:19,60 --> 00:00:21,70
the latest user experience trends

10
00:00:21,70 --> 00:00:24,00
is always in constant flux.

11
00:00:24,00 --> 00:00:26,10
And you, as a Developer, need to keep

12
00:00:26,10 --> 00:00:27,90
abreast of these trends.

13
00:00:27,90 --> 00:00:30,00
So, this kind of knowledge is really important

14
00:00:30,00 --> 00:00:32,20
and not React-related specifically,

15
00:00:32,20 --> 00:00:36,00
but nonetheless, important skills to grow as a Developer.

16
00:00:36,00 --> 00:00:38,10
For example, there are tons of groups

17
00:00:38,10 --> 00:00:39,90
on Facebook related to UX,

18
00:00:39,90 --> 00:00:41,80
and this is a great place to start,

19
00:00:41,80 --> 00:00:43,40
and see the latest trends,

20
00:00:43,40 --> 00:00:45,80
and learn from others and experts.

21
00:00:45,80 --> 00:00:47,80
There are also a few sites,

22
00:00:47,80 --> 00:00:50,90
such as the Usability and User Experience section

23
00:00:50,90 --> 00:00:55,50
on Smashing Magazine, or UX Feeds on Feedly or Flipboard.

24
00:00:55,50 --> 00:00:57,30
Make sure you follow these feeds,

25
00:00:57,30 --> 00:00:59,70
an important source of reference.

26
00:00:59,70 --> 00:01:01,80
Finally, there are several courses

27
00:01:01,80 --> 00:01:03,60
related to UX in our library.

28
00:01:03,60 --> 00:01:06,20
Now, I strongly recommend you take a few of them.

29
00:01:06,20 --> 00:01:08,80
Knowing why you should place a specific button

30
00:01:08,80 --> 00:01:10,90
in your application is as important

31
00:01:10,90 --> 00:01:13,40
as knowing how to code it.

32
00:01:13,40 --> 00:01:15,40
Thanks very much for taking my course,

33
00:01:15,40 --> 00:01:17,00
and I'll see you in a bit.

