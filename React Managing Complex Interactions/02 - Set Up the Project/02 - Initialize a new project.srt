1
00:00:01,00 --> 00:00:02,50
- [Instructor] Let's get our project initialized

2
00:00:02,50 --> 00:00:04,70
and add the proper libraries.

3
00:00:04,70 --> 00:00:06,00
So I'm uisng VS Code

4
00:00:06,00 --> 00:00:09,10
because VS Code has a built-in terminal,

5
00:00:09,10 --> 00:00:11,70
but if you're not using VS Code or using something else

6
00:00:11,70 --> 00:00:14,90
at least make sure that you have a terminal close by

7
00:00:14,90 --> 00:00:17,90
so you can do the commands that we're going to do now.

8
00:00:17,90 --> 00:00:20,80
So let's go ahead and let's close the Welcome,

9
00:00:20,80 --> 00:00:22,50
so if you don't have VS Code open

10
00:00:22,50 --> 00:00:25,40
and you're using VS Code, just click on VS Code.

11
00:00:25,40 --> 00:00:31,70
And then go to View, Integrated Terminal.

12
00:00:31,70 --> 00:00:35,90
And we should be on the main directory of the computer.

13
00:00:35,90 --> 00:00:40,70
So we want to move to the desktop, so cd Desktop/ like so,

14
00:00:40,70 --> 00:00:43,90
and we're on the Desktop, perfect.

15
00:00:43,90 --> 00:00:45,40
So what we're going to do,

16
00:00:45,40 --> 00:00:48,90
so first if you don't have npm or Node installed,

17
00:00:48,90 --> 00:00:50,20
please install it.

18
00:00:50,20 --> 00:00:53,40
You should have Node installed if you're taking this course,

19
00:00:53,40 --> 00:00:56,20
'cause this is not an introduction course to React,

20
00:00:56,20 --> 00:00:59,40
so you should have at least installed npm in the past.

21
00:00:59,40 --> 00:01:01,90
If you don't, go to nodejs,

22
00:01:01,90 --> 00:01:08,00
I believe the link is nodejs.org, like so

23
00:01:08,00 --> 00:01:11,10
and install npm or the latest version

24
00:01:11,10 --> 00:01:13,00
and then get back to VS Code.

25
00:01:13,00 --> 00:01:14,60
Once you're back to VS Code,

26
00:01:14,60 --> 00:01:19,00
the first thing we'll do is to update a create React app,

27
00:01:19,00 --> 00:01:24,60
by simply doing an npm install -g, for global,

28
00:01:24,60 --> 00:01:28,50
create-react-app.

29
00:01:28,50 --> 00:01:30,70
I already have it installed and most of the time

30
00:01:30,70 --> 00:01:32,60
I update it quite often,

31
00:01:32,60 --> 00:01:35,30
but I want to go through this exercise with you,

32
00:01:35,30 --> 00:01:37,40
just to make sure you do as well.

33
00:01:37,40 --> 00:01:40,60
And if you don't have root access to your computer,

34
00:01:40,60 --> 00:01:43,00
it may actually ask you to do a pseudo.

35
00:01:43,00 --> 00:01:45,60
So basically you do the exact same command

36
00:01:45,60 --> 00:01:49,20
with pseudo before, like so.

37
00:01:49,20 --> 00:01:51,20
And I'll remove that command after.

38
00:01:51,20 --> 00:01:57,60
And then it'll as for your system password.

39
00:01:57,60 --> 00:02:02,90
Okay, so once you have create React updated or installed,

40
00:02:02,90 --> 00:02:05,20
let's go ahead and create our first project.

41
00:02:05,20 --> 00:02:09,70
So again, if you do ls, you know exactly where you are.

42
00:02:09,70 --> 00:02:13,40
I want to create basically the project on my Desktop here,

43
00:02:13,40 --> 00:02:15,20
so let me minimize that for a second.

44
00:02:15,20 --> 00:02:17,60
So I want to create the project right here.

45
00:02:17,60 --> 00:02:20,20
Feel free to create it anywhere you want.

46
00:02:20,20 --> 00:02:24,10
This is part of the experience of being a developer.

47
00:02:24,10 --> 00:02:27,20
So let's go ahead and create a new application,

48
00:02:27,20 --> 00:02:30,00
create-react-app.

49
00:02:30,00 --> 00:02:33,60
And then I'm going to call this one vitaminstore.

50
00:02:33,60 --> 00:02:35,80
And just a quick note for yourself,

51
00:02:35,80 --> 00:02:38,30
you cannot do vitaminStore like this.

52
00:02:38,30 --> 00:02:41,70
It no longer accepts doing capital letters

53
00:02:41,70 --> 00:02:45,10
inside of creation of an application and most likely

54
00:02:45,10 --> 00:02:49,70
because it uses that for the package.json naming

55
00:02:49,70 --> 00:02:53,80
and package.json doesn't accept capital letters

56
00:02:53,80 --> 00:02:54,60
in a name like that.

57
00:02:54,60 --> 00:02:56,60
So make sure you do it that way.

58
00:02:56,60 --> 00:03:03,60
Let's go ahead and create a new project.

59
00:03:03,60 --> 00:03:06,10
Okay, so now we got our application created

60
00:03:06,10 --> 00:03:08,10
on the Desktop, so let's take a look at this.

61
00:03:08,10 --> 00:03:10,60
We got it here, so we're good to go.

62
00:03:10,60 --> 00:03:12,80
Let's go back inside of our console.

63
00:03:12,80 --> 00:03:18,50
All right, so let's get inside of the

64
00:03:18,50 --> 00:03:22,90
project vitaminstore.

65
00:03:22,90 --> 00:03:25,00
And we are inside, perfect.

66
00:03:25,00 --> 00:03:27,10
So now we're going to install a dependency

67
00:03:27,10 --> 00:03:30,70
that we'll use later on and it's called react-lazyload

68
00:03:30,70 --> 00:03:33,70
So if you want to take a look at what react-lazyload is,

69
00:03:33,70 --> 00:03:35,80
that's basically a dependency that we'll install

70
00:03:35,80 --> 00:03:38,00
that will allow us to do lazy loading

71
00:03:38,00 --> 00:03:39,70
inside of our application later on.

72
00:03:39,70 --> 00:03:42,60
So I just want to install all our dependencies right now.

73
00:03:42,60 --> 00:03:44,00
So if you want to take a look at it,

74
00:03:44,00 --> 00:03:51,30
it's github.com/jasonslyvia/react-lazyload.

75
00:03:51,30 --> 00:03:53,70
All right, so let's go ahead and install that.

76
00:03:53,70 --> 00:04:05,80
So let's do an npm install --save react-lazyload.

77
00:04:05,80 --> 00:04:07,60
Now we got our dependencies installed,

78
00:04:07,60 --> 00:04:11,50
let's load the application inside of VS Code like so.

79
00:04:11,50 --> 00:04:15,40
So click on explorer, then click Open Folder.

80
00:04:15,40 --> 00:04:22,70
Then go on the Desktop, vitaminstore, Open,

81
00:04:22,70 --> 00:04:24,30
and you have your application here.

82
00:04:24,30 --> 00:04:26,40
Okay, the last thing I want to explore

83
00:04:26,40 --> 00:04:29,00
before we actually end this video

84
00:04:29,00 --> 00:04:31,60
is the resources that we'll use in this course.

85
00:04:31,60 --> 00:04:36,00
So click on Exercise Files, click on Resources,

86
00:04:36,00 --> 00:04:38,50
and these are the background,

87
00:04:38,50 --> 00:04:40,70
we're going to use some data later on,

88
00:04:40,70 --> 00:04:44,00
and we got also this juice image

89
00:04:44,00 --> 00:04:45,70
that we'll use for our cards,

90
00:04:45,70 --> 00:04:48,00
and finally the logo of the company.

91
00:04:48,00 --> 00:04:50,60
All right so now that we have our initial project ready

92
00:04:50,60 --> 00:04:53,00
and our resources, we can get started.

