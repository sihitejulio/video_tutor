1
00:00:01,00 --> 00:00:03,00
- [Instructor] For any type of web development,

2
00:00:03,00 --> 00:00:05,10
our first tool of choice is the dev tools

3
00:00:05,10 --> 00:00:07,00
that come with your own browser.

4
00:00:07,00 --> 00:00:10,30
Whether you use Chrome, Safari, Firefox, or Edge,

5
00:00:10,30 --> 00:00:12,60
you have the tools needed to inspect your code,

6
00:00:12,60 --> 00:00:15,10
do performance checks, and more.

7
00:00:15,10 --> 00:00:17,20
In this course, we'll specifically use

8
00:00:17,20 --> 00:00:19,30
Chrome's internal tools.

9
00:00:19,30 --> 00:00:23,10
I highly recommend that you use either Chrome or Firefox

10
00:00:23,10 --> 00:00:24,70
as they offer solid performance

11
00:00:24,70 --> 00:00:27,40
and work with the extensions we'll use.

12
00:00:27,40 --> 00:00:29,50
As a developer, you've probably used

13
00:00:29,50 --> 00:00:31,30
Chrome's developer tools, but if not,

14
00:00:31,30 --> 00:00:35,40
you can access them on Mac with Option + Command + I,

15
00:00:35,40 --> 00:00:39,00
and on Windows, Control + Shift + I, like so.

16
00:00:39,00 --> 00:00:42,20
You'll see the developer tools and you'll see

17
00:00:42,20 --> 00:00:44,70
that there's a lot of errors on the GitHub website

18
00:00:44,70 --> 00:00:46,30
for some reason.

19
00:00:46,30 --> 00:00:49,00
Let's go to elements so it's not distracting.

20
00:00:49,00 --> 00:00:51,60
Now let's install the React developer tools

21
00:00:51,60 --> 00:00:54,10
which you can find here on this link,

22
00:00:54,10 --> 00:01:00,90
so github.com/facebook/react-devtools.

23
00:01:00,90 --> 00:01:03,20
Let's go and close that.

24
00:01:03,20 --> 00:01:05,90
Once you get to this site, you scroll down

25
00:01:05,90 --> 00:01:08,20
and then you'll see the actual links

26
00:01:08,20 --> 00:01:10,00
to the extensions you can install.

27
00:01:10,00 --> 00:01:13,20
There's your install if you don't have Firefox or Chrome,

28
00:01:13,20 --> 00:01:16,20
but again, you'll see that it's much more useful

29
00:01:16,20 --> 00:01:19,50
when you have it inside of the actual browser.

30
00:01:19,50 --> 00:01:24,60
In our case, we're going to go to Chrome

31
00:01:24,60 --> 00:01:26,80
and then click on add.

32
00:01:26,80 --> 00:01:28,50
I already have it on my browser,

33
00:01:28,50 --> 00:01:30,30
so that's what I'm seeing it green.

34
00:01:30,30 --> 00:01:33,10
But for you, it should be blue, and then add it.

35
00:01:33,10 --> 00:01:37,20
Once you are done, you should see a little

36
00:01:37,20 --> 00:01:41,10
react developer tool here, you can actually access it

37
00:01:41,10 --> 00:01:44,20
on the dev tools, so let's get back to dev tools.

38
00:01:44,20 --> 00:01:46,60
Again, Option + Command + I for Mac,

39
00:01:46,60 --> 00:01:48,80
Control + Shift + I for Windows,

40
00:01:48,80 --> 00:01:52,60
and then if you have a website that actually has React,

41
00:01:52,60 --> 00:01:54,10
you'll see a React tab.

42
00:01:54,10 --> 00:02:00,10
Let's go to a website that I know for sure that has React.

43
00:02:00,10 --> 00:02:04,20
As you can see, Netflix is completely built on React.

44
00:02:04,20 --> 00:02:09,30
We can click on their React tab and see the components,

45
00:02:09,30 --> 00:02:13,30
see the props, and the state when you have a state.

46
00:02:13,30 --> 00:02:15,30
I believe there's one somewhere in here.

47
00:02:15,30 --> 00:02:17,20
There's an empty state here.

48
00:02:17,20 --> 00:02:19,50
But anyways, you can see the state and the props

49
00:02:19,50 --> 00:02:21,40
as you click on the components.

50
00:02:21,40 --> 00:02:25,00
Now we have all the tools we need, so let's move on.

