1
00:00:01,00 --> 00:00:03,20
- [Narrator] When we talk about hero interactions,

2
00:00:03,20 --> 00:00:06,40
we talk about what the user should pay his attention to.

3
00:00:06,40 --> 00:00:09,40
As the elements of your UI shows up on the page,

4
00:00:09,40 --> 00:00:11,20
it's really important to make sure that whatever

5
00:00:11,20 --> 00:00:13,20
actions you want your user to do

6
00:00:13,20 --> 00:00:15,70
is what attracts their attention.

7
00:00:15,70 --> 00:00:17,40
Right now, the application shows us

8
00:00:17,40 --> 00:00:19,10
there is a logo that wobbles,

9
00:00:19,10 --> 00:00:22,50
but what exactly is its purpose?

10
00:00:22,50 --> 00:00:24,40
Let's add in a new label that shows

11
00:00:24,40 --> 00:00:29,10
when we hover over the logo, the user knows it's the menu.

12
00:00:29,10 --> 00:00:33,30
So let's get to VS Code.

13
00:00:33,30 --> 00:00:37,50
Let's go inside of app.js and start coding h1

14
00:00:37,50 --> 00:00:41,30
just below the image here, on Line 78.

15
00:00:41,30 --> 00:00:43,90
So, do an h1,

16
00:00:43,90 --> 00:00:52,50
and then let's just return here and close the tag,

17
00:00:52,50 --> 00:00:54,80
like so,

18
00:00:54,80 --> 00:00:59,60
and return on this line here, and start adding some stuff.

19
00:00:59,60 --> 00:01:04,80
So, className= and let's enter some Javascript.

20
00:01:04,80 --> 00:01:08,20
We're going to do the ES6 version of an if statement,

21
00:01:08,20 --> 00:01:14,50
so, if that state.toggleLogo is true,

22
00:01:14,50 --> 00:01:22,90
then add a class 'menu-hidden',

23
00:01:22,90 --> 00:01:33,90
otherwise, do 'menu animated bounceInDown'.

24
00:01:33,90 --> 00:01:37,40
Let's also add the onClick to open the nav here,

25
00:01:37,40 --> 00:01:39,70
so copy the onClick on Line 77

26
00:01:39,70 --> 00:01:43,90
and then paste it on Line 71, like so.

27
00:01:43,90 --> 00:01:48,60
And then, here, just add Menu,

28
00:01:48,60 --> 00:01:53,20
and we can just bring it up, here, like so,

29
00:01:53,20 --> 00:01:54,70
we should be good.

30
00:01:54,70 --> 00:01:58,30
Okay, so this is pretty much what we need to do here,

31
00:01:58,30 --> 00:02:01,70
on the App.js, and then we need to do

32
00:02:01,70 --> 00:02:05,70
a few changes here, on the App.CSS.

33
00:02:05,70 --> 00:02:07,80
So, the first thing, we don't need

34
00:02:07,80 --> 00:02:12,10
the app intro or the app title anymore,

35
00:02:12,10 --> 00:02:15,80
so we can remove all this.

36
00:02:15,80 --> 00:02:18,50
And then, we can also remove the animated logo

37
00:02:18,50 --> 00:02:22,50
we don't need that anymore so we are not using it.

38
00:02:22,50 --> 00:02:24,40
So we're good there.

39
00:02:24,40 --> 00:02:29,90
What I'd like to do as well is increase the logo size

40
00:02:29,90 --> 00:02:33,90
let's make it 150 pixel

41
00:02:33,90 --> 00:02:36,50
and then let's add the new classes

42
00:02:36,50 --> 00:02:38,50
that we just inserted here.

43
00:02:38,50 --> 00:02:41,20
So we need to create a 'menu-hidden' and then

44
00:02:41,20 --> 00:02:44,20
a menu class inside of our CSS.

45
00:02:44,20 --> 00:02:46,20
So let's start with the menu,

46
00:02:46,20 --> 00:02:53,10
so menu position absolute

47
00:02:53,10 --> 00:02:59,20
margin-left: auto

48
00:02:59,20 --> 00:03:02,40
and this is basically to make sure that it is

49
00:03:02,40 --> 00:03:07,80
positioned properly, right, auto as well,

50
00:03:07,80 --> 00:03:12,50
top: 145 pixels,

51
00:03:12,50 --> 00:03:18,40
left zero, right zero,

52
00:03:18,40 --> 00:03:26,80
font-weight: 800,

53
00:03:26,80 --> 00:03:32,80
font-size: 35 pixels, text shadow

54
00:03:32,80 --> 00:03:35,50
I'm basically reproducing a lot of the same things

55
00:03:35,50 --> 00:03:42,50
that I've done for the pricing, except the color.

56
00:03:42,50 --> 00:03:46,10
We'll have a menu that will slightly look like

57
00:03:46,10 --> 00:03:51,60
it's above the content or the picture, like the pricing was.

58
00:03:51,60 --> 00:03:58,20
Zero pixel, five pixel, rgba,

59
00:03:58,20 --> 00:04:05,50
zero, zero, zero, zero point 42

60
00:04:05,50 --> 00:04:07,20
and I want to cursor, just to make sure

61
00:04:07,20 --> 00:04:09,40
that we actually see it.

62
00:04:09,40 --> 00:04:14,10
Like so, perfect.

63
00:04:14,10 --> 00:04:16,30
Now we need to do the menu-hidden,

64
00:04:16,30 --> 00:04:21,40
so let's go ahead and do that.

65
00:04:21,40 --> 00:04:25,50
We're going to do a visibility: hidden.

66
00:04:25,50 --> 00:04:29,30
Okay, so I'm done here.

67
00:04:29,30 --> 00:04:31,60
Okay, so let's make sure everything is good here,

68
00:04:31,60 --> 00:04:38,10
so menu, menu animated, oh, bounceInDown, save,

69
00:04:38,10 --> 00:04:40,20
and now we should be good to go.

70
00:04:40,20 --> 00:04:43,20
So let's go and take a look at our application.

71
00:04:43,20 --> 00:04:47,60
If we do this our menu is going to bounce in,

72
00:04:47,60 --> 00:04:51,70
if we do that it disappears, and so on and so forth.

73
00:04:51,70 --> 00:04:54,40
Although this is a simple change it does wonders

74
00:04:54,40 --> 00:04:58,80
to educate your user: 'hey, this is the menu'.

75
00:04:58,80 --> 00:05:00,00
Alright so let's move on.

