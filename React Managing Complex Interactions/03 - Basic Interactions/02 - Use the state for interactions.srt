1
00:00:01,00 --> 00:00:02,70
- [Narrator] One of the best method in React

2
00:00:02,70 --> 00:00:04,80
to introduce interactions is to leverage

3
00:00:04,80 --> 00:00:07,40
the state to change behaviors in your application.

4
00:00:07,40 --> 00:00:08,80
Let's work on this.

5
00:00:08,80 --> 00:00:12,50
So, go inside the Xcode and open App.js,

6
00:00:12,50 --> 00:00:14,10
and what we're going to do first

7
00:00:14,10 --> 00:00:17,40
is add a constructor so we can initialize our state.

8
00:00:17,40 --> 00:00:20,10
So, let's go ahead and do that.

9
00:00:20,10 --> 00:00:24,40
Constructor, and constructor is a function,

10
00:00:24,40 --> 00:00:26,90
and we'll pass props to it.

11
00:00:26,90 --> 00:00:31,20
And then, do a super with props,

12
00:00:31,20 --> 00:00:35,20
so we can probably pass the props inside of our class.

13
00:00:35,20 --> 00:00:40,00
And then, let's initialize our state,

14
00:00:40,00 --> 00:00:41,40
and we'll initialize our state

15
00:00:41,40 --> 00:00:46,00
with a value called toggleLogo,

16
00:00:46,00 --> 00:00:48,90
and we'll set it as true first.

17
00:00:48,90 --> 00:00:52,80
So, the idea here is when we click on the logo,

18
00:00:52,80 --> 00:00:56,50
we change the state to false or true,

19
00:00:56,50 --> 00:00:59,50
and then, we use and leverage that value to actually

20
00:00:59,50 --> 00:01:02,70
change the way the logo is actually behaving.

21
00:01:02,70 --> 00:01:04,30
And that's the idea here.

22
00:01:04,30 --> 00:01:07,00
So, let's go and do that.

23
00:01:07,00 --> 00:01:10,20
Next thing we need to do is create a function

24
00:01:10,20 --> 00:01:15,00
that will be called toggleLogo,

25
00:01:15,00 --> 00:01:19,60
with an event that we'll pass.

26
00:01:19,60 --> 00:01:22,70
And basically, this function will change the state.

27
00:01:22,70 --> 00:01:25,50
So this, set state.

28
00:01:25,50 --> 00:01:28,70
And what we'll do is pass the previous state,

29
00:01:28,70 --> 00:01:32,10
so we can basically change from a true

30
00:01:32,10 --> 00:01:34,50
to a false value whenever we click.

31
00:01:34,50 --> 00:01:36,60
So whenever we click on that,

32
00:01:36,60 --> 00:01:39,80
it will toggle the value that we pass to it.

33
00:01:39,80 --> 00:01:41,50
Simple as that.

34
00:01:41,50 --> 00:01:44,90
Okay, so we'll do that here, like so.

35
00:01:44,90 --> 00:01:47,80
And what we'll do is basically change

36
00:01:47,80 --> 00:01:52,50
the toggleLogo value inside of our state,

37
00:01:52,50 --> 00:01:57,80
passing the previous value and just switching it to

38
00:01:57,80 --> 00:02:01,00
a new value, from true to false.

39
00:02:01,00 --> 00:02:03,00
So, basically what I've done here is grab

40
00:02:03,00 --> 00:02:05,30
the previous state, and then, in this function,

41
00:02:05,30 --> 00:02:07,50
changed it to the reverse values.

42
00:02:07,50 --> 00:02:09,80
So if it's true, then it's going to be false.

43
00:02:09,80 --> 00:02:13,80
If it's false, it's going to be true, like so.

44
00:02:13,80 --> 00:02:16,00
Okay, the next thing we need to do,

45
00:02:16,00 --> 00:02:18,50
because we've just created a function that we'll use,

46
00:02:18,50 --> 00:02:20,90
we need to bind it, and I usually do this

47
00:02:20,90 --> 00:02:24,10
in the constructors, so I don't have to

48
00:02:24,10 --> 00:02:26,40
think about that later on.

49
00:02:26,40 --> 00:02:29,70
ToggleLogo,

50
00:02:29,70 --> 00:02:35,60
this.toggleLogo.bind, and this.

51
00:02:35,60 --> 00:02:39,10
And binding is something we need to do when we use ES6.

52
00:02:39,10 --> 00:02:40,80
If you want to have more detail about it,

53
00:02:40,80 --> 00:02:44,60
just look for a ES6 bind, and you'll find

54
00:02:44,60 --> 00:02:47,40
more details as to why we need to do this.

55
00:02:47,40 --> 00:02:48,90
Okay.

56
00:02:48,90 --> 00:02:50,90
So now that we've done the function,

57
00:02:50,90 --> 00:02:55,00
and then, we bind and we have our state initialized,

58
00:02:55,00 --> 00:02:58,00
now, we can use this inside of our logo here,

59
00:02:58,00 --> 00:03:00,20
and basically change the class of the logo

60
00:03:00,20 --> 00:03:02,40
based on what is click.

61
00:03:02,40 --> 00:03:05,00
So, let me do something and put className

62
00:03:05,00 --> 00:03:08,70
on the second line, and then, the out on the third.

63
00:03:08,70 --> 00:03:13,10
And let's just put the closing of the element

64
00:03:13,10 --> 00:03:14,40
a line further.

65
00:03:14,40 --> 00:03:17,10
And what I'm going to do here is change this to JavaScript.

66
00:03:17,10 --> 00:03:19,60
So when you want to add JavaScript to an element,

67
00:03:19,60 --> 00:03:21,30
all you have to do is curly braces

68
00:03:21,30 --> 00:03:23,80
inside of a React application.

69
00:03:23,80 --> 00:03:26,90
So, what we're going to do here is if this does that,

70
00:03:26,90 --> 00:03:28,40
and we're going to do a short version

71
00:03:28,40 --> 00:03:31,60
of the if statement using ES6.

72
00:03:31,60 --> 00:03:37,60
So if the state.toggleLogo is true,

73
00:03:37,60 --> 00:03:42,00
then use 'static-logo' class.

74
00:03:42,00 --> 00:03:44,10
So basically, we're going to take a look

75
00:03:44,10 --> 00:03:47,00
at our CSS to make sure we have that.

76
00:03:47,00 --> 00:03:53,20
Otherwise, do the animated-logo.

77
00:03:53,20 --> 00:03:56,60
And what we'll do next is add an unclick,

78
00:03:56,60 --> 00:04:01,50
so we can click and change the value of the state.

79
00:04:01,50 --> 00:04:06,20
So with an unclick, do this toggleLogo.

80
00:04:06,20 --> 00:04:07,80
So, let me explain what's happening here.

81
00:04:07,80 --> 00:04:09,90
So whenever we click on the logo,

82
00:04:09,90 --> 00:04:11,70
we're going to do this event here,

83
00:04:11,70 --> 00:04:14,80
which will basically act on this

84
00:04:14,80 --> 00:04:16,00
particular function here.

85
00:04:16,00 --> 00:04:17,80
So, we're going to change the state to

86
00:04:17,80 --> 00:04:19,60
either a true or false.

87
00:04:19,60 --> 00:04:22,90
And when the state is true, then this is the class

88
00:04:22,90 --> 00:04:26,40
that's going to be running on this particular image.

89
00:04:26,40 --> 00:04:30,40
If not, then do the animated-logo, and so on, so forth.

90
00:04:30,40 --> 00:04:34,50
So, let's go and change the actual class itself.

91
00:04:34,50 --> 00:04:36,80
So, let's save that,

92
00:04:36,80 --> 00:04:39,60
then go inside of the App.css.

93
00:04:39,60 --> 00:04:41,80
And what we're going to do now is

94
00:04:41,80 --> 00:04:46,80
basically change the App-logo to a static logo.

95
00:04:46,80 --> 00:04:48,60
So, let's change that class here.

96
00:04:48,60 --> 00:04:52,00
So, .static-logo.

97
00:04:52,00 --> 00:04:53,70
We're also going to change the height.

98
00:04:53,70 --> 00:04:57,30
I like to make it 100 pixels.

99
00:04:57,30 --> 00:04:59,30
This is going to be the animated-logo.

100
00:04:59,30 --> 00:05:02,10
Sorry about that, because the animation is right here.

101
00:05:02,10 --> 00:05:04,50
So, this is going to be the .animated-logo,

102
00:05:04,50 --> 00:05:07,70
and for the static-logo, let's just copy

103
00:05:07,70 --> 00:05:12,00
and paste this here, and just do .static,

104
00:05:12,00 --> 00:05:15,50
and then, just remove the animation here, like so.

105
00:05:15,50 --> 00:05:18,10
So, this is going to be the static-logo.

106
00:05:18,10 --> 00:05:19,70
If we are clicking on it, then it's

107
00:05:19,70 --> 00:05:20,90
going to run this class here.

108
00:05:20,90 --> 00:05:22,40
So, let's save that.

109
00:05:22,40 --> 00:05:24,70
Let's make sure that our application

110
00:05:24,70 --> 00:05:26,60
is running so we can see the result.

111
00:05:26,60 --> 00:05:28,30
So click on View,

112
00:05:28,30 --> 00:05:29,80
Integrated Terminal,

113
00:05:29,80 --> 00:05:33,20
do an npm start.

114
00:05:33,20 --> 00:05:36,30
Okay, so let's go here.

115
00:05:36,30 --> 00:05:38,90
Okay, so now our application is running.

116
00:05:38,90 --> 00:05:40,60
So right now, our logo is not doing anything,

117
00:05:40,60 --> 00:05:42,50
so that means it's static.

118
00:05:42,50 --> 00:05:46,10
And then if we click on it, it's going to start running.

119
00:05:46,10 --> 00:05:48,60
If we click on it again, it's going to stop,

120
00:05:48,60 --> 00:05:51,00
and so on, so forth.

121
00:05:51,00 --> 00:05:53,60
Okay, so now, we have leveraged the state

122
00:05:53,60 --> 00:05:55,40
to introduce an animation.

123
00:05:55,40 --> 00:05:57,00
Let's move on.

