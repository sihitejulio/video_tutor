1
00:00:00,90 --> 00:00:02,60
- [Instructor] For all the interactions we'll explore,

2
00:00:02,60 --> 00:00:05,60
we'll create a card component, so let's get to it.

3
00:00:05,60 --> 00:00:07,40
The beauty of this particular course

4
00:00:07,40 --> 00:00:11,00
is that everything that we're going to do is pure CSS.

5
00:00:11,00 --> 00:00:15,00
The only exception will be the React Lazy Load,

6
00:00:15,00 --> 00:00:16,90
but everything else we'll build custom,

7
00:00:16,90 --> 00:00:20,30
so you can grab that code and use it in any other projects.

8
00:00:20,30 --> 00:00:22,70
You're going to be good to go with that.

9
00:00:22,70 --> 00:00:24,60
So the very first thing I'm going to do

10
00:00:24,60 --> 00:00:28,20
is just minimize that so I can get some screen estate here.

11
00:00:28,20 --> 00:00:31,70
I'm going to create a new folder in the source folder.

12
00:00:31,70 --> 00:00:34,30
So right click, new folder.

13
00:00:34,30 --> 00:00:37,20
And I'm going to call this one component.

14
00:00:37,20 --> 00:00:39,20
To follow the React standards,

15
00:00:39,20 --> 00:00:41,00
we're going to put the components in here.

16
00:00:41,00 --> 00:00:43,90
The only exception might be the app.js,

17
00:00:43,90 --> 00:00:45,80
but for this course, I'm going to leave

18
00:00:45,80 --> 00:00:48,00
the app.js outside of components

19
00:00:48,00 --> 00:00:51,20
and everything else will be in components.

20
00:00:51,20 --> 00:00:54,60
Create a new file, so right click, new file.

21
00:00:54,60 --> 00:01:00,50
Call this one card.js and then create a new file again.

22
00:01:00,50 --> 00:01:04,90
Card.css.

23
00:01:04,90 --> 00:01:07,90
Perfect, and then let's go in the app.js

24
00:01:07,90 --> 00:01:12,10
and we'll create another item for the state here.

25
00:01:12,10 --> 00:01:14,30
We're going to create a list of cards

26
00:01:14,30 --> 00:01:17,30
so we can use some of the stuff in here.

27
00:01:17,30 --> 00:01:19,70
This is going to be an array, and basically

28
00:01:19,70 --> 00:01:22,80
we're going to do IDs, only IDs for now.

29
00:01:22,80 --> 00:01:29,60
We're going to do ID one and then simply repeat that

30
00:01:29,60 --> 00:01:37,10
a couple times, so copy, paste, and then do this one too.

31
00:01:37,10 --> 00:01:39,70
Don't pay attention to your syntax on this one

32
00:01:39,70 --> 00:01:43,10
because we're going to soon use a data file

33
00:01:43,10 --> 00:01:45,80
that will import as opposed to use this.

34
00:01:45,80 --> 00:01:49,30
This is just for this particular exercise.

35
00:01:49,30 --> 00:01:53,60
Let's just create five and then a six,

36
00:01:53,60 --> 00:01:56,50
so we have two rows of three cards.

37
00:01:56,50 --> 00:02:01,30
Okay, and then put a comma at the end, perfect.

38
00:02:01,30 --> 00:02:04,10
So the next thing we need to do is, in here we're going

39
00:02:04,10 --> 00:02:08,20
to create a component very soon in the card.js file.

40
00:02:08,20 --> 00:02:10,90
So let's go and import it regardless

41
00:02:10,90 --> 00:02:13,60
that we don't have it yet.

42
00:02:13,60 --> 00:02:15,80
So we are done with this particular file here,

43
00:02:15,80 --> 00:02:17,90
but we'll create that in a second.

44
00:02:17,90 --> 00:02:28,60
So import Card from components and then card; perfect.

45
00:02:28,60 --> 00:02:31,20
Once you save this file, because we're going to save it

46
00:02:31,20 --> 00:02:34,70
very soon, it's going to throw off your server

47
00:02:34,70 --> 00:02:37,50
because we haven't created that component yet.

48
00:02:37,50 --> 00:02:40,20
So that's completely fine.

49
00:02:40,20 --> 00:02:44,70
So create a div with a className of Grid.

50
00:02:44,70 --> 00:02:48,40
And we'll create the Grid class very soon as well.

51
00:02:48,40 --> 00:02:51,20
Alright, so let's do that, and then inside of that

52
00:02:51,20 --> 00:02:54,90
we're going to basically run some Javascript.

53
00:02:54,90 --> 00:03:00,90
And this.state.cards and we'll map so iterate

54
00:03:00,90 --> 00:03:06,10
though the list of IDs that we've basically initialized

55
00:03:06,10 --> 00:03:13,00
in the state and then basically return what is next.

56
00:03:13,00 --> 00:03:16,80
So for every cards ID in the state,

57
00:03:16,80 --> 00:03:20,10
you're going to iterate and render this card.

58
00:03:20,10 --> 00:03:24,80
And I'm going to add this duration thingy here for now.

59
00:03:24,80 --> 00:03:26,70
Don't pay attention to it; I'm actually being

60
00:03:26,70 --> 00:03:28,80
a little bit proactive here.

61
00:03:28,80 --> 00:03:32,10
We'll actually use that very soon.

62
00:03:32,10 --> 00:03:34,90
Just type it for now since we are going

63
00:03:34,90 --> 00:03:37,90
to create that very soon.

64
00:03:37,90 --> 00:03:41,30
Okay, we're going to iterate through the card component

65
00:03:41,30 --> 00:03:45,20
as many times as we have IDs in our state.

66
00:03:45,20 --> 00:03:48,80
So we are good to go here and let's save that.

67
00:03:48,80 --> 00:03:51,70
Obviously your server is going to throw an error

68
00:03:51,70 --> 00:03:56,20
right now so that's completely fine.

69
00:03:56,20 --> 00:03:58,10
Next thing, I want to create the Grid class.

70
00:03:58,10 --> 00:04:01,70
And we're going to do that around line 12.

71
00:04:01,70 --> 00:04:05,20
So go at the top of the file on the f.css file

72
00:04:05,20 --> 00:04:09,80
and create the Grid class here and that will basically

73
00:04:09,80 --> 00:04:15,30
be a margin of 5% to give some space.

74
00:04:15,30 --> 00:04:19,60
And we'll use flexbox to create this grid;

75
00:04:19,60 --> 00:04:22,00
very cool feature of CSS.

76
00:04:22,00 --> 00:04:24,80
And we're going to do flex-flow.

77
00:04:24,80 --> 00:04:28,40
I'm not going to talk about the details of flex

78
00:04:28,40 --> 00:04:31,00
in this course but if you're curious about flex,

79
00:04:31,00 --> 00:04:35,00
take a look at the library; we got a course on this subject.

80
00:04:35,00 --> 00:04:37,20
And you should be able to get

81
00:04:37,20 --> 00:04:38,70
a little bit more knowledge about flex.

82
00:04:38,70 --> 00:04:40,70
But this is a very cool feature of CSS

83
00:04:40,70 --> 00:04:43,50
that I'm using in this course, as opposed to use

84
00:04:43,50 --> 00:04:46,20
a pre-made library so you guys can

85
00:04:46,20 --> 00:04:48,20
learn how to build those things.

86
00:04:48,20 --> 00:04:51,80
Alright, so the next thing, let's go and card that js.

87
00:04:51,80 --> 00:04:55,20
And we're going to start building our component

88
00:04:55,20 --> 00:05:01,10
here from react and then we'll import

89
00:05:01,10 --> 00:05:05,10
the CSS file that we have here.

90
00:05:05,10 --> 00:05:10,00
And we'll implement some styles very shortly.

91
00:05:10,00 --> 00:05:12,10
Then we'll create a stateless component.

92
00:05:12,10 --> 00:05:14,10
If you're not familiar with a stateless component,

93
00:05:14,10 --> 00:05:16,90
it's basically means that it's not a class-based component

94
00:05:16,90 --> 00:05:19,80
and it doesn't have all the lifecycle methods

95
00:05:19,80 --> 00:05:24,10
that React offers, so this is basically a dumb component

96
00:05:24,10 --> 00:05:26,50
that takes props from its parent

97
00:05:26,50 --> 00:05:36,40
and renders the component last name and then card.

98
00:05:36,40 --> 00:05:43,50
And then we'll do an image with source and we'll have to add

99
00:05:43,50 --> 00:05:46,90
that image very soon in our public folder

100
00:05:46,90 --> 00:05:48,90
so we'll do that in a second.

101
00:05:48,90 --> 00:05:51,60
Choose that jpeg, alt.

102
00:05:51,60 --> 00:05:54,60
And I'm doing a lot of manual typing in this course

103
00:05:54,60 --> 00:05:58,70
because I want you to get really familiar with typing

104
00:05:58,70 --> 00:06:03,10
components like this and really create your own components,

105
00:06:03,10 --> 00:06:06,80
create your own classes, use flexbox to really build

106
00:06:06,80 --> 00:06:10,70
interactive components manually so you can learn

107
00:06:10,70 --> 00:06:13,50
all these things as opposed to always leverage libraries

108
00:06:13,50 --> 00:06:17,20
like we do quite often in my courses usually.

109
00:06:17,20 --> 00:06:21,60
Okay, so the alt, let's just say Vitamin Juice.

110
00:06:21,60 --> 00:06:24,00
And you could almost pass a description

111
00:06:24,00 --> 00:06:27,50
in here if you had one as a prop.

112
00:06:27,50 --> 00:06:34,40
Okay so this one will be card-image; perfect.

113
00:06:34,40 --> 00:06:37,60
Then let's do a div and before I actually forget,

114
00:06:37,60 --> 00:06:41,00
I'm going to move the juice.jpeg file directly

115
00:06:41,00 --> 00:06:42,90
in the public folder right away,

116
00:06:42,90 --> 00:06:46,00
so I do not forget this particular one.

117
00:06:46,00 --> 00:06:51,00
So let's go and get out of VSCode, and again this is normal.

118
00:06:51,00 --> 00:06:55,50
Go in the exercise files, go in the resources,

119
00:06:55,50 --> 00:06:59,10
click on juice, so command C for juice,

120
00:06:59,10 --> 00:07:02,00
and then go in vitaminstore, and then

121
00:07:02,00 --> 00:07:05,10
in the public folder command V to paste.

122
00:07:05,10 --> 00:07:07,00
So at least we didn't forget about that one

123
00:07:07,00 --> 00:07:08,10
and we're not going to get an error

124
00:07:08,10 --> 00:07:10,10
that there's no image called juice.

125
00:07:10,10 --> 00:07:13,40
Okay, so we got that part done; we can continue.

126
00:07:13,40 --> 00:07:17,00
The next one will be a className of container.

127
00:07:17,00 --> 00:07:21,20
And we'll create that class momentarily.

128
00:07:21,20 --> 00:07:28,10
And inside that container we'll have an h3 Vitamin Juice,

129
00:07:28,10 --> 00:07:35,50
the title, and we'll have also a span with className price.

130
00:07:35,50 --> 00:07:38,90
We'll create all these styles shortly.

131
00:07:38,90 --> 00:07:44,00
And we'll insert price here, 24.99.

132
00:07:44,00 --> 00:07:46,20
That's an expensive vitamin juice.

133
00:07:46,20 --> 00:07:49,80
I wonder who set up the price for that.

134
00:07:49,80 --> 00:07:52,30
Okay, so let me just quickly do something.

135
00:07:52,30 --> 00:08:04,20
Need a jump on your vitamins while drinking?

136
00:08:04,20 --> 00:08:06,10
I'm just doing a description, and yes,

137
00:08:06,10 --> 00:08:07,50
we're typing the whole description

138
00:08:07,50 --> 00:08:09,80
because I want to see what it's going to look like

139
00:08:09,80 --> 00:08:12,20
once you have a good description.

140
00:08:12,20 --> 00:08:14,50
Tired of popping pills?

141
00:08:14,50 --> 00:08:20,20
Drink our vitamin enhanced juice.

142
00:08:20,20 --> 00:08:24,10
And let me just return here.

143
00:08:24,10 --> 00:08:28,00
The spaces inside of an element doesn't really matter.

144
00:08:28,00 --> 00:08:32,20
So you can basically do this for your own visual goods.

145
00:08:32,20 --> 00:08:35,70
Available in several flavors.

146
00:08:35,70 --> 00:08:37,30
I truly believe in this company

147
00:08:37,30 --> 00:08:39,90
because I'm doing a full description.

148
00:08:39,90 --> 00:08:42,50
Okay, so we got all this done.

149
00:08:42,50 --> 00:08:47,60
So let's put a semicolon after the parentheses on line 13.

150
00:08:47,60 --> 00:08:51,40
And then export our component.

151
00:08:51,40 --> 00:08:53,60
If we don't export it, it's not going to be available

152
00:08:53,60 --> 00:08:57,10
outside of this particular code here.

153
00:08:57,10 --> 00:08:59,70
Alright, so we are done with this.

154
00:08:59,70 --> 00:09:02,10
The only typing that we have left now

155
00:09:02,10 --> 00:09:05,80
is the CSS, so let's get to that.

156
00:09:05,80 --> 00:09:08,40
And I'm going to go ahead and type very quickly

157
00:09:08,40 --> 00:09:12,50
because there's a lot of stuff here and this video is going to

158
00:09:12,50 --> 00:09:15,80
run a little bit long if I don't do this very quickly.

159
00:09:15,80 --> 00:09:21,30
So zero, four pixel, eight pixel, zero again.

160
00:09:21,30 --> 00:09:28,10
And rgba, zero, zero, zero, 0.2.

161
00:09:28,10 --> 00:09:36,30
Okay, then transition, 0.3 seconds then width.

162
00:09:36,30 --> 00:09:40,70
So basically I'm just doing all the styles for the cards.

163
00:09:40,70 --> 00:09:43,00
They're going to be 30% wide,

164
00:09:43,00 --> 00:09:47,70
so basically we'll make three per row.

165
00:09:47,70 --> 00:09:51,90
And we'll do a white on this one.

166
00:09:51,90 --> 00:09:57,30
Then margin, five pixels, and we could do five pixels,

167
00:09:57,30 --> 00:10:00,50
five pixels, five pixels, I'm doing the pixels

168
00:10:00,50 --> 00:10:02,60
because we may actually change that.

169
00:10:02,60 --> 00:10:05,50
Or if you want to change that on your own,

170
00:10:05,50 --> 00:10:09,20
then you can do that without doing the five five five

171
00:10:09,20 --> 00:10:13,60
five five as the short version, ten pixels, okay.

172
00:10:13,60 --> 00:10:17,10
So now we're going to do the price.

173
00:10:17,10 --> 00:10:19,70
And the price we're going to do in red

174
00:10:19,70 --> 00:10:23,60
so people can actually see it.

175
00:10:23,60 --> 00:10:28,10
Font-weight is 800.

176
00:10:28,10 --> 00:10:32,30
Again, as any courses where I actually do tons of styles,

177
00:10:32,30 --> 00:10:36,80
these are my own, I guess, styles that I like.

178
00:10:36,80 --> 00:10:40,30
If you don't like them, feel free to change that.

179
00:10:40,30 --> 00:10:42,40
Styles in an application,

180
00:10:42,40 --> 00:10:46,00
unless you actually change the flexbox stuff,

181
00:10:46,00 --> 00:10:50,80
won't have any large impact on the actual application.

182
00:10:50,80 --> 00:10:53,40
But everything else, you can basically change

183
00:10:53,40 --> 00:10:55,90
if you want to make it look the way you want.

184
00:10:55,90 --> 00:10:59,50
Let me just make sure I didn't make any typo here,

185
00:10:59,50 --> 00:11:03,70
because while I'm talking sometimes I do make typos.

186
00:11:03,70 --> 00:11:05,60
We're all good, okay.

187
00:11:05,60 --> 00:11:11,20
Container.

188
00:11:11,20 --> 00:11:13,20
Again, on this particular course

189
00:11:13,20 --> 00:11:16,00
I didn't want to leverage materialize

190
00:11:16,00 --> 00:11:19,70
which I usually use in all my courses.

191
00:11:19,70 --> 00:11:22,60
I really wanted to get into the weeds

192
00:11:22,60 --> 00:11:26,60
of doing the styles ourselves.

193
00:11:26,60 --> 00:11:28,60
So you guys can see how to build these things

194
00:11:28,60 --> 00:11:33,80
and possibly learn more about CSS in animations

195
00:11:33,80 --> 00:11:37,30
and interactions and also build your own after that.

196
00:11:37,30 --> 00:11:41,10
That's the idea: I give you a platform to learn

197
00:11:41,10 --> 00:11:43,50
and then you guys can fly on your own.

198
00:11:43,50 --> 00:11:47,00
And I'm talking and I'm making mistakes here.

199
00:11:47,00 --> 00:11:54,00
So width, perfect, and then last border.

200
00:11:54,00 --> 00:11:57,80
This is the last line of this particular video.

201
00:11:57,80 --> 00:12:01,90
We are almost there; thanks for sticking with me.

202
00:12:01,90 --> 00:12:10,00
Okay, so now there's another typo here, border-radius.

203
00:12:10,00 --> 00:12:13,10
Oh, there you go, it was above.

204
00:12:13,10 --> 00:12:17,20
Okay, so we are good to go; let's save this file

205
00:12:17,20 --> 00:12:21,20
now let's go back to our application and there we go.

206
00:12:21,20 --> 00:12:24,00
We got our cards, we got our vitamin juice

207
00:12:24,00 --> 00:12:28,10
that is really apparent, very expensive vitamin juice.

208
00:12:28,10 --> 00:12:31,80
By the way, this is a box of juice and not just one.

209
00:12:31,80 --> 00:12:36,30
And here are our card; cool!

210
00:12:36,30 --> 00:12:38,50
Now we've got a card component we'll use

211
00:12:38,50 --> 00:12:41,00
to add some interactions so let's move on.

