1
00:00:01,00 --> 00:00:03,30
- Okay, so now we've got half the work done

2
00:00:03,30 --> 00:00:06,00
on our navigation, let's continue.

3
00:00:06,00 --> 00:00:10,10
So now you're going to go inside of the Navigation.css

4
00:00:10,10 --> 00:00:12,30
and we're going to be typing a lot of classes

5
00:00:12,30 --> 00:00:13,90
so let's get to it.

6
00:00:13,90 --> 00:00:17,70
So, overlay.

7
00:00:17,70 --> 00:00:23,10
And we're going to do a height of 100%.

8
00:00:23,10 --> 00:00:26,30
We're going to do a width of zero.

9
00:00:26,30 --> 00:00:28,40
Remember we're actually going to

10
00:00:28,40 --> 00:00:32,00
specify the width based on a clicking event.

11
00:00:32,00 --> 00:00:35,40
So position, fixed.

12
00:00:35,40 --> 00:00:40,60
Then a Z index of one.

13
00:00:40,60 --> 00:00:43,60
Left, zero.

14
00:00:43,60 --> 00:00:46,50
Top, zero.

15
00:00:46,50 --> 00:00:51,70
Then background color.

16
00:00:51,70 --> 00:00:54,60
And we'll do black.

17
00:00:54,60 --> 00:00:58,40
So let's select black.

18
00:00:58,40 --> 00:01:03,20
Then overflow, X.

19
00:01:03,20 --> 00:01:07,00
We'll do hidden.

20
00:01:07,00 --> 00:01:13,80
And then transition, we'll do 0.5 seconds.

21
00:01:13,80 --> 00:01:16,60
Like so, okay.

22
00:01:16,60 --> 00:01:22,00
Then we'll do overlay-content.

23
00:01:22,00 --> 00:01:24,70
So basically the content that will be on the overlay.

24
00:01:24,70 --> 00:01:30,70
So position relative.

25
00:01:30,70 --> 00:01:37,10
Top 25%.

26
00:01:37,10 --> 00:01:41,10
Width 100%.

27
00:01:41,10 --> 00:01:46,00
Text-align, center.

28
00:01:46,00 --> 00:01:50,10
Margin-top, we'll do 30 pixels.

29
00:01:50,10 --> 00:01:53,80
And these again are all the styles that I've set,

30
00:01:53,80 --> 00:01:57,50
feel free to change that at your own will.

31
00:01:57,50 --> 00:01:59,80
Overlay a.

32
00:01:59,80 --> 00:02:01,70
Basically all the links.

33
00:02:01,70 --> 00:02:05,40
So padding will be eight pixels,

34
00:02:05,40 --> 00:02:12,70
then we'll do a text decoration of none.

35
00:02:12,70 --> 00:02:17,00
Then a font size of 30 pixels.

36
00:02:17,00 --> 00:02:20,80
Yes, you can hear me almost laughing in the back.

37
00:02:20,80 --> 00:02:26,80
And then for that we'll do 81, 81, 81, like so.

38
00:02:26,80 --> 00:02:29,80
Display block.

39
00:02:29,80 --> 00:02:37,00
And transition, 0.3 seconds.

40
00:02:37,00 --> 00:02:39,20
We're halfway done.

41
00:02:39,20 --> 00:02:41,50
Let's scroll a little bit.

42
00:02:41,50 --> 00:02:47,60
Okay, so then we'll do an overlay, hover state.

43
00:02:47,60 --> 00:02:53,90
And also an overlay a focused.

44
00:02:53,90 --> 00:02:59,90
And color will be doing white.

45
00:02:59,90 --> 00:03:03,10
And let me make sure there's no syntax issues here.

46
00:03:03,10 --> 00:03:06,00
Color white, we're all good.

47
00:03:06,00 --> 00:03:08,70
Because the more you type,

48
00:03:08,70 --> 00:03:10,50
the more you can make typos

49
00:03:10,50 --> 00:03:14,80
and you always need to check your code when you are,

50
00:03:14,80 --> 00:03:17,40
otherwise the effect that you're looking for

51
00:03:17,40 --> 00:03:19,50
may not be the same.

52
00:03:19,50 --> 00:03:23,10
So closebtn, let's make sure it's the same here.

53
00:03:23,10 --> 00:03:26,00
Closebtn, yep, so we're good there.

54
00:03:26,00 --> 00:03:32,60
Overlay, let's go ahead and do a position.

55
00:03:32,60 --> 00:03:34,10
Absolute.

56
00:03:34,10 --> 00:03:38,10
Top, 20 pixels.

57
00:03:38,10 --> 00:03:42,20
Right, 45 pixels.

58
00:03:42,20 --> 00:03:47,50
Font size, 60 pixels.

59
00:03:47,50 --> 00:03:49,50
And the cursor, we need to see the cursor,

60
00:03:49,50 --> 00:03:55,30
and we'll do that for a few of our items afterwards as well.

61
00:03:55,30 --> 00:03:57,30
And you know what while I'm thinking about it

62
00:03:57,30 --> 00:04:00,10
let's go ahead and do them right away.

63
00:04:00,10 --> 00:04:03,60
So basically on our card I want to be able

64
00:04:03,60 --> 00:04:05,50
to see a cursor here as well.

65
00:04:05,50 --> 00:04:10,50
So let's do a cursor pointer there too, let's save that.

66
00:04:10,50 --> 00:04:12,30
And on the logo as well,

67
00:04:12,30 --> 00:04:16,40
so the logo would be in the App.css.

68
00:04:16,40 --> 00:04:19,90
So let's go in the static logo

69
00:04:19,90 --> 00:04:21,70
and do a cursor pointer as well

70
00:04:21,70 --> 00:04:25,00
so we can see the pointer whenever we use these things.

71
00:04:25,00 --> 00:04:29,90
So now you can close the App.css, the Card.css.

72
00:04:29,90 --> 00:04:33,70
And now we're almost done with the style here,

73
00:04:33,70 --> 00:04:36,70
so let's return on line 40

74
00:04:36,70 --> 00:04:39,90
and do some resizing here.

75
00:04:39,90 --> 00:04:50,10
So media screen and max height of 450 pixels.

76
00:04:50,10 --> 00:04:56,50
Then do this, overlay a.

77
00:04:56,50 --> 00:05:00,30
Font size.

78
00:05:00,30 --> 00:05:02,60
20 pixels.

79
00:05:02,60 --> 00:05:10,40
And then let's do a .overlay .closebtn.

80
00:05:10,40 --> 00:05:17,30
Let's do a font size of 40 pixels.

81
00:05:17,30 --> 00:05:22,10
A top of 15 pixels.

82
00:05:22,10 --> 00:05:24,10
Let me add some space here.

83
00:05:24,10 --> 00:05:29,50
And then finally, right of 35 pixels.

84
00:05:29,50 --> 00:05:32,20
Okay, so let me quickly check my stuff here

85
00:05:32,20 --> 00:05:34,00
just to make sure everything makes sense.

86
00:05:34,00 --> 00:05:37,20
Overlay, close button, overlay a.

87
00:05:37,20 --> 00:05:41,10
Overlay close button, overlay hover.

88
00:05:41,10 --> 00:05:43,10
Okay.

89
00:05:43,10 --> 00:05:45,50
Overlay a focused.

90
00:05:45,50 --> 00:05:48,50
Okay, we're all good there.

91
00:05:48,50 --> 00:05:51,00
Overlay a, we're all good,

92
00:05:51,00 --> 00:05:54,20
and overlay content, and overlay.

93
00:05:54,20 --> 00:05:55,10
Okay.

94
00:05:55,10 --> 00:05:58,30
So let's save that.

95
00:05:58,30 --> 00:06:02,00
Let's go back to our application.

96
00:06:02,00 --> 00:06:03,90
And let's refresh because sometimes

97
00:06:03,90 --> 00:06:07,30
these CSS styles in React

98
00:06:07,30 --> 00:06:10,00
don't automatically refresh the application.

99
00:06:10,00 --> 00:06:12,80
So by hovering over, and by the way you can

100
00:06:12,80 --> 00:06:16,00
see now the cursor appearing, and also here.

101
00:06:16,00 --> 00:06:18,90
So if I'm hovering over it the animation is still there.

102
00:06:18,90 --> 00:06:25,90
If I click on it I get a really nice overlay menu, like so.

103
00:06:25,90 --> 00:06:30,00
If I click on the X, it's going away.

104
00:06:30,00 --> 00:06:32,30
And there you go.

105
00:06:32,30 --> 00:06:36,00
So now we've got our menu coded, let's move on.

