1
00:00:01,30 --> 00:00:02,30
- [Instructor] Handling hover states

2
00:00:02,30 --> 00:00:05,60
in any applications is quite easy using CSS.

3
00:00:05,60 --> 00:00:07,80
When you hover over an element

4
00:00:07,80 --> 00:00:09,70
you can add specific CSS styles

5
00:00:09,70 --> 00:00:11,20
to change the look.

6
00:00:11,20 --> 00:00:13,40
But what if you'd like to add classes

7
00:00:13,40 --> 00:00:15,50
from libraries such as Animate.CSS

8
00:00:15,50 --> 00:00:18,30
to an element when you hover over it.

9
00:00:18,30 --> 00:00:20,50
Unless you are using SAS RLS,

10
00:00:20,50 --> 00:00:22,30
you can't do it in CSS.

11
00:00:22,30 --> 00:00:24,00
You have to use either jQuery

12
00:00:24,00 --> 00:00:25,50
or some other form of scripting

13
00:00:25,50 --> 00:00:27,20
to add the classes.

14
00:00:27,20 --> 00:00:30,20
Let me demonstrate how I do it in React.

15
00:00:30,20 --> 00:00:32,60
So the first thing I'd like to show you

16
00:00:32,60 --> 00:00:36,40
is the Animate.CSS library or basically

17
00:00:36,40 --> 00:00:40,10
their site, it's very cool.

18
00:00:40,10 --> 00:00:48,30
So if you go to DaneDen.github.io/Animate.CSS

19
00:00:48,30 --> 00:00:51,10
basically you'll see this little animation here.

20
00:00:51,10 --> 00:00:52,80
So these are all the animations

21
00:00:52,80 --> 00:00:56,80
that are available to us when you use Animate.CSS.

22
00:00:56,80 --> 00:01:00,30
And if you are curious how they're doing this,

23
00:01:00,30 --> 00:01:02,70
you can basically take a look on GitHub,

24
00:01:02,70 --> 00:01:05,30
and take a look at all the key frames

25
00:01:05,30 --> 00:01:07,30
and the CSS styles that they've done

26
00:01:07,30 --> 00:01:09,30
to do those animations.

27
00:01:09,30 --> 00:01:10,90
So we want to leverage the good work

28
00:01:10,90 --> 00:01:13,40
that this guy has done with this library

29
00:01:13,40 --> 00:01:17,10
of CSS styles, and then when we hover over

30
00:01:17,10 --> 00:01:21,80
an element on our page, apply that specific CSS style

31
00:01:21,80 --> 00:01:24,60
to that particular element.

32
00:01:24,60 --> 00:01:28,30
So let's go to GitHub,

33
00:01:28,30 --> 00:01:30,10
and you can do this two ways.

34
00:01:30,10 --> 00:01:33,30
You can actually go to the source

35
00:01:33,30 --> 00:01:36,70
and take a look at any of the animations in here

36
00:01:36,70 --> 00:01:39,70
and take a look at, so for example, flippers

37
00:01:39,70 --> 00:01:43,50
and take a look at Flip.CSS.

38
00:01:43,50 --> 00:01:44,80
And then you'll see the styles

39
00:01:44,80 --> 00:01:46,50
that this guy actually wrote

40
00:01:46,50 --> 00:01:48,70
to make the animation that you've seen

41
00:01:48,70 --> 00:01:50,40
on the previous page.

42
00:01:50,40 --> 00:01:53,60
But what if you wanted to leverage those styles

43
00:01:53,60 --> 00:01:56,60
without manually typing all the CSS

44
00:01:56,60 --> 00:01:59,10
that's in here, and I know I've said

45
00:01:59,10 --> 00:02:00,70
in previous videos that we're going

46
00:02:00,70 --> 00:02:02,80
to build our own stuff here,

47
00:02:02,80 --> 00:02:05,30
but I want to show you in this particular video

48
00:02:05,30 --> 00:02:07,30
how you can leverage other classes

49
00:02:07,30 --> 00:02:09,80
when you hover over your elements

50
00:02:09,80 --> 00:02:13,90
without having to retype the entire CSS.

51
00:02:13,90 --> 00:02:16,20
So basically, something you'd be able

52
00:02:16,20 --> 00:02:18,90
to easily apply with SAS RLS,

53
00:02:18,90 --> 00:02:21,30
but in CSS it's not available yet.

54
00:02:21,30 --> 00:02:23,20
So if you do something like

55
00:02:23,20 --> 00:02:28,70
in SAS, extend a class to this particular class here,

56
00:02:28,70 --> 00:02:31,10
then you would be able to do that in SAS RLS,

57
00:02:31,10 --> 00:02:34,00
but not regular CSS, so let me show you

58
00:02:34,00 --> 00:02:35,30
how I would do it.

59
00:02:35,30 --> 00:02:37,20
So the first thing we'll do is go

60
00:02:37,20 --> 00:02:40,70
inside the index.html, so we need

61
00:02:40,70 --> 00:02:47,20
to grab the CDN for the Animate CSS here.

62
00:02:47,20 --> 00:02:51,40
Let me grab the JS deliver here,

63
00:02:51,40 --> 00:02:53,40
so let's grab this line here.

64
00:02:53,40 --> 00:02:55,70
If you go in the basic usage

65
00:02:55,70 --> 00:03:01,50
on the site GitHub.com/DaneDen/Animate.CSS,

66
00:03:01,50 --> 00:03:03,00
you'll go to basic usage

67
00:03:03,00 --> 00:03:05,70
and then you can click on the link here

68
00:03:05,70 --> 00:03:08,60
and then paste that link inside of our index

69
00:03:08,60 --> 00:03:10,80
that HTML in the public folder.

70
00:03:10,80 --> 00:03:13,30
So I want you to basically put it

71
00:03:13,30 --> 00:03:18,30
right below line 12 here, like so,

72
00:03:18,30 --> 00:03:21,00
now we're able to leverage the CSS styles

73
00:03:21,00 --> 00:03:22,80
inside of our application.

74
00:03:22,80 --> 00:03:25,00
So what I would do next is go inside

75
00:03:25,00 --> 00:03:28,10
of, App.js and let me close everything else

76
00:03:28,10 --> 00:03:30,50
because we don't need anything else for now

77
00:03:30,50 --> 00:03:33,10
Card, we don't need Card.

78
00:03:33,10 --> 00:03:36,30
We only need the App.js here so,

79
00:03:36,30 --> 00:03:39,50
let me just remove the explorer.

80
00:03:39,50 --> 00:03:41,60
And now the way I would do it,

81
00:03:41,60 --> 00:03:44,00
and this is my own approach,

82
00:03:44,00 --> 00:03:47,30
the cool thing is in interactions

83
00:03:47,30 --> 00:03:50,80
in CSS and HTML and React,

84
00:03:50,80 --> 00:03:54,00
there's multiple ways to achieve the same thing,

85
00:03:54,00 --> 00:03:56,60
and as a developer you'll find your own styles,

86
00:03:56,60 --> 00:03:58,20
you'll get a little bit from me,

87
00:03:58,20 --> 00:04:00,30
you'll get a little bit from where you work,

88
00:04:00,30 --> 00:04:02,60
you'll get a little bit from peers,

89
00:04:02,60 --> 00:04:05,20
and then you'll build your own style,

90
00:04:05,20 --> 00:04:07,30
and there might be a different way

91
00:04:07,30 --> 00:04:08,90
that you like to do this.

92
00:04:08,90 --> 00:04:12,00
So this is how I would handle hover states

93
00:04:12,00 --> 00:04:14,70
inside of my applications, so I'll remove

94
00:04:14,70 --> 00:04:18,40
the onClick here, on line 29,

95
00:04:18,40 --> 00:04:19,50
and then I'll do an onMouseEnter,

96
00:04:19,50 --> 00:04:28,20
then do this.toggleLogo.

97
00:04:28,20 --> 00:04:32,20
So as opposed to use the click on the logo,

98
00:04:32,20 --> 00:04:36,90
what I'm going to use is when I enter the logo,

99
00:04:36,90 --> 00:04:40,60
onMouseEnter, I will initiate this function here,

100
00:04:40,60 --> 00:04:50,30
and then I'm going to do the same thing onMouseLeave.

101
00:04:50,30 --> 00:04:53,50
Like so, so basically whenever I enter the logo

102
00:04:53,50 --> 00:04:56,60
I'm going to initiate this function,

103
00:04:56,60 --> 00:04:58,60
therefore, change the class,

104
00:04:58,60 --> 00:05:01,20
and when I'm leaving, I will change it again.

105
00:05:01,20 --> 00:05:03,40
So let's save that, let's go back

106
00:05:03,40 --> 00:05:06,10
to our application, and let's close that here,

107
00:05:06,10 --> 00:05:09,50
and as you can see, as soon as I enter the logo

108
00:05:09,50 --> 00:05:14,10
it's going to change the style, like so.

109
00:05:14,10 --> 00:05:15,40
Now the next thing I want to do

110
00:05:15,40 --> 00:05:17,70
I want to leverage the styles

111
00:05:17,70 --> 00:05:19,80
from the Animate.CSS.

112
00:05:19,80 --> 00:05:22,40
So as opposed to do a static logo

113
00:05:22,40 --> 00:05:26,30
and animate logo, let's do a style

114
00:05:26,30 --> 00:05:29,30
from Animate.CSS, and the first thing

115
00:05:29,30 --> 00:05:32,20
that I'm going to do is add the class static logo

116
00:05:32,20 --> 00:05:36,00
so I can have the actual size from the static logo.

117
00:05:36,00 --> 00:05:38,90
And then add animated, which is the way

118
00:05:38,90 --> 00:05:43,70
you use Animate.CSS, and then add the jello animation.

119
00:05:43,70 --> 00:05:47,00
So if we go back to Animate.CSS here,

120
00:05:47,00 --> 00:05:50,20
let me just go back here,

121
00:05:50,20 --> 00:05:53,40
and take a look at jello,

122
00:05:53,40 --> 00:05:54,80
this is what jello is.

123
00:05:54,80 --> 00:05:57,60
So, let's go back to our code,

124
00:05:57,60 --> 00:06:01,60
and save our file, like so,

125
00:06:01,60 --> 00:06:03,20
and then go back to our application

126
00:06:03,20 --> 00:06:06,70
and then enter again our logo,

127
00:06:06,70 --> 00:06:12,50
and you see the logo class being applied.

128
00:06:12,50 --> 00:06:14,20
So as you can see, it's pretty simple

129
00:06:14,20 --> 00:06:16,60
to add a hover class from an external library

130
00:06:16,60 --> 00:06:19,30
to our component, leveraging the state

131
00:06:19,30 --> 00:06:23,00
in onMouseEnter and onMouseLeave.

132
00:06:23,00 --> 00:06:24,00
Let's move on.

