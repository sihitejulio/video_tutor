1
00:00:01,00 --> 00:00:02,40
- [Instructor] In some cases your application

2
00:00:02,40 --> 00:00:05,90
will consist of a grid of items or a gallery of images,

3
00:00:05,90 --> 00:00:07,90
pretty much like our application.

4
00:00:07,90 --> 00:00:10,30
As we speak, we have some type of interactions

5
00:00:10,30 --> 00:00:13,00
where we click on the card and it disappears

6
00:00:13,00 --> 00:00:14,20
in the background.

7
00:00:14,20 --> 00:00:16,70
Not really what most applications require.

8
00:00:16,70 --> 00:00:19,40
In these type of grid light applications,

9
00:00:19,40 --> 00:00:21,30
we often need to reveal more information

10
00:00:21,30 --> 00:00:22,90
for the item in question,

11
00:00:22,90 --> 00:00:25,20
so let's work on something like this.

12
00:00:25,20 --> 00:00:26,50
The first thing you need to do

13
00:00:26,50 --> 00:00:29,60
is get inside of the app.js file.

14
00:00:29,60 --> 00:00:32,50
What we'll do first is remove all the manual input

15
00:00:32,50 --> 00:00:34,10
inside of this state,

16
00:00:34,10 --> 00:00:37,50
and instead free that and load something

17
00:00:37,50 --> 00:00:38,80
from an external file.

18
00:00:38,80 --> 00:00:42,20
So let's go ahead and remove that.

19
00:00:42,20 --> 00:00:44,60
We still need to initialize the array inside of the state,

20
00:00:44,60 --> 00:00:46,60
so we'll leave that as is.

21
00:00:46,60 --> 00:00:49,80
And then, let's get out of VS Code

22
00:00:49,80 --> 00:00:53,00
and go in the exercise files,

23
00:00:53,00 --> 00:00:56,70
click on resources, and then the data.json file,

24
00:00:56,70 --> 00:00:58,60
copy that file,

25
00:00:58,60 --> 00:01:02,50
and then go inside of Vitamin Store, our application.

26
00:01:02,50 --> 00:01:07,60
In the source folder, create a new folder called beta,

27
00:01:07,60 --> 00:01:11,40
and then paste by doing Command V,

28
00:01:11,40 --> 00:01:14,80
or right clicking and pasting.

29
00:01:14,80 --> 00:01:17,50
So we go our data.json file here,

30
00:01:17,50 --> 00:01:19,50
so let's go back to VS Code.

31
00:01:19,50 --> 00:01:22,50
Let's import that data here.

32
00:01:22,50 --> 00:01:32,10
Import data from data, data.json, like so,

33
00:01:32,10 --> 00:01:35,00
and then we'll use one of the life cycle methods

34
00:01:35,00 --> 00:01:38,70
which is the componentWillMount,

35
00:01:38,70 --> 00:01:43,00
so we can actually set our cards array

36
00:01:43,00 --> 00:01:46,10
with the data that we just loaded here,

37
00:01:46,10 --> 00:01:48,30
just before the app actually renders.

38
00:01:48,30 --> 00:01:50,40
So let's go ahead and do that.

39
00:01:50,40 --> 00:01:53,50
This.setState,

40
00:01:53,50 --> 00:01:54,70
and then

41
00:01:54,70 --> 00:01:58,00
cards with the data,

42
00:01:58,00 --> 00:01:59,70
like so.

43
00:01:59,70 --> 00:02:00,60
Okay, perfect.

44
00:02:00,60 --> 00:02:04,90
So let's put semicolon here,

45
00:02:04,90 --> 00:02:07,00
and we're good to go here.

46
00:02:07,00 --> 00:02:10,50
Now, what we need to do is create two functions

47
00:02:10,50 --> 00:02:12,10
that we'll use on the card.

48
00:02:12,10 --> 00:02:14,10
So if we go back here,

49
00:02:14,10 --> 00:02:16,20
when we click on this specific card,

50
00:02:16,20 --> 00:02:18,70
we want it to turn, to flip over,

51
00:02:18,70 --> 00:02:20,20
and show us more information.

52
00:02:20,20 --> 00:02:23,10
So this is the purpose of this particular video

53
00:02:23,10 --> 00:02:24,40
and what we want to do here.

54
00:02:24,40 --> 00:02:27,10
So what we need to do is create two functions,

55
00:02:27,10 --> 00:02:29,00
one that says showBack,

56
00:02:29,00 --> 00:02:31,50
and the other one that says showFront.

57
00:02:31,50 --> 00:02:33,50
And what we'll do is leverage some of the stuff

58
00:02:33,50 --> 00:02:36,50
that we've actually done here on the clickCard,

59
00:02:36,50 --> 00:02:38,30
and create those two functions.

60
00:02:38,30 --> 00:02:41,50
And in fact, it will replace the clickCard here.

61
00:02:41,50 --> 00:02:46,30
So let's first create the function showBack,

62
00:02:46,30 --> 00:02:50,50
and it takes a card like it does for the clickCard,

63
00:02:50,50 --> 00:02:55,10
and then let's create another one, showFront,

64
00:02:55,10 --> 00:02:58,10
same argument.

65
00:02:58,10 --> 00:03:00,40
And then what we'll do, basically,

66
00:03:00,40 --> 00:03:05,30
is get all the code that we got in this particular code here

67
00:03:05,30 --> 00:03:09,60
copy, and then paste, and the paste again

68
00:03:09,60 --> 00:03:11,60
in the showFront,

69
00:03:11,60 --> 00:03:17,00
and what we'll do then is remove clickCard,

70
00:03:17,00 --> 00:03:19,80
and then change some other things here.

71
00:03:19,80 --> 00:03:20,90
So basically the only thing that we're going to do

72
00:03:20,90 --> 00:03:24,30
differently here is change what kind of

73
00:03:24,30 --> 00:03:28,20
animation we are passing for our card,

74
00:03:28,20 --> 00:03:30,00
so what is that class that we're passing.

75
00:03:30,00 --> 00:03:33,10
So here what we'll do is do a card-flip

76
00:03:33,10 --> 00:03:37,50
for the showBack,

77
00:03:37,50 --> 00:03:38,80
and for the showFront,

78
00:03:38,80 --> 00:03:43,10
we only need to do the card.

79
00:03:43,10 --> 00:03:45,50
And then what we'll need to do again

80
00:03:45,50 --> 00:03:48,20
is make sure those two functions are bound,

81
00:03:48,20 --> 00:03:49,60
so let's go here,

82
00:03:49,60 --> 00:03:54,60
and then we'll need to remove the clickCard, like so,

83
00:03:54,60 --> 00:03:57,80
and let's copy line 19,

84
00:03:57,80 --> 00:04:00,90
paste it twice,

85
00:04:00,90 --> 00:04:05,00
and basically use Option click

86
00:04:05,00 --> 00:04:07,80
to select those two and do showBack first,

87
00:04:07,80 --> 00:04:11,40
let's go in order.

88
00:04:11,40 --> 00:04:14,20
And then Option, Option click,

89
00:04:14,20 --> 00:04:18,40
and then showFront, like so.

90
00:04:18,40 --> 00:04:22,50
Okay, so we are all good to go.

91
00:04:22,50 --> 00:04:25,40
The next thing we need to do is basically pass

92
00:04:25,40 --> 00:04:28,10
those two functions to the card.

93
00:04:28,10 --> 00:04:33,50
So let's scroll down to our actual component here,

94
00:04:33,50 --> 00:04:37,80
and let's start bringing this into multiple lines,

95
00:04:37,80 --> 00:04:40,30
like so, so at least we get some order here,

96
00:04:40,30 --> 00:04:43,40
so let's do this.

97
00:04:43,40 --> 00:04:45,40
Perfect.

98
00:04:45,40 --> 00:04:47,50
And now as opposed to pass clickCard,

99
00:04:47,50 --> 00:04:52,70
we're passing, let's do showBack first,

100
00:04:52,70 --> 00:04:57,00
and let's copy that,

101
00:04:57,00 --> 00:04:57,80
paste it again,

102
00:04:57,80 --> 00:05:00,40
and then let's do click,

103
00:05:00,40 --> 00:05:02,50
and actually,

104
00:05:02,50 --> 00:05:07,60
at the end, click, and then Option click here,

105
00:05:07,60 --> 00:05:12,60
and let's just do showFront like so,

106
00:05:12,60 --> 00:05:14,40
and we are good to go.

107
00:05:14,40 --> 00:05:16,50
We have our card, our key,

108
00:05:16,50 --> 00:05:17,80
everything is good here,

109
00:05:17,80 --> 00:05:20,30
so let's save this file,

110
00:05:20,30 --> 00:05:22,60
and now we need to go inside of the card

111
00:05:22,60 --> 00:05:25,30
and make a little bit of a change here.

112
00:05:25,30 --> 00:05:28,50
So right now we don't need the unclick anymore,

113
00:05:28,50 --> 00:05:30,70
on the particular card,

114
00:05:30,70 --> 00:05:32,70
we'll actually do this somewhere else,

115
00:05:32,70 --> 00:05:37,80
so let's go and just clean that up a little bit, like so,

116
00:05:37,80 --> 00:05:40,30
and let's just put that higher.

117
00:05:40,30 --> 00:05:42,20
We still have to pass the animation,

118
00:05:42,20 --> 00:05:44,40
because this is what will determine

119
00:05:44,40 --> 00:05:47,00
the actual class of our card.

120
00:05:47,00 --> 00:05:53,00
And now we need to do a class name of front,

121
00:05:53,00 --> 00:05:55,30
so we have our front of our card,

122
00:05:55,30 --> 00:06:00,50
and we'll do an unclick event here,

123
00:06:00,50 --> 00:06:04,20
and again, we do a function inside,

124
00:06:04,20 --> 00:06:08,30
and then pass the props.showBack,

125
00:06:08,30 --> 00:06:11,80
so the front will show the back,

126
00:06:11,80 --> 00:06:18,70
and we need to pass the props.card inside of it, like so.

127
00:06:18,70 --> 00:06:22,30
And let's close that out.

128
00:06:22,30 --> 00:06:26,10
Perfect.

129
00:06:26,10 --> 00:06:28,30
So basically, we'll have the two,

130
00:06:28,30 --> 00:06:31,90
the front element and the back element here.

131
00:06:31,90 --> 00:06:34,20
So we'll have a full card here,

132
00:06:34,20 --> 00:06:36,40
and then the back of the card after.

133
00:06:36,40 --> 00:06:37,80
So let me do the back,

134
00:06:37,80 --> 00:06:39,40
and then we'll move a few of the things here.

135
00:06:39,40 --> 00:06:50,50
So let's do className in container-back and the back,

136
00:06:50,50 --> 00:06:52,80
and we'll do a class for that very soon,

137
00:06:52,80 --> 00:06:56,00
and do an unclick,

138
00:06:56,00 --> 00:07:01,30
and again let's just copy that guy here,

139
00:07:01,30 --> 00:07:02,40
and paste it,

140
00:07:02,40 --> 00:07:07,30
and then do the showFront,

141
00:07:07,30 --> 00:07:10,20
showFront, like so,

142
00:07:10,20 --> 00:07:14,70
close that div.

143
00:07:14,70 --> 00:07:17,40
Let's do the closing tag for that element.

144
00:07:17,40 --> 00:07:20,20
Alright, so, we have our front, we have our back,

145
00:07:20,20 --> 00:07:22,40
now let's start moving some stuff.

146
00:07:22,40 --> 00:07:24,10
So on our front,

147
00:07:24,10 --> 00:07:28,70
we need to basically have all the stuff that we had in here.

148
00:07:28,70 --> 00:07:31,60
So let's copy all this,

149
00:07:31,60 --> 00:07:33,00
and then put it on the front,

150
00:07:33,00 --> 00:07:36,40
so the front doesn't change,

151
00:07:36,40 --> 00:07:39,30
and then let's indent this.

152
00:07:39,30 --> 00:07:41,70
So we're basically reusing all of the code

153
00:07:41,70 --> 00:07:43,80
that we already did for the front.

154
00:07:43,80 --> 00:07:45,50
The front is all good.

155
00:07:45,50 --> 00:07:48,10
For the back, I'm going to change just a bit.

156
00:07:48,10 --> 00:07:54,10
So let's first remove that extra picture here.

157
00:07:54,10 --> 00:07:57,70
So we remove the container here,

158
00:07:57,70 --> 00:08:02,30
and let's remove the closing tag,

159
00:08:02,30 --> 00:08:07,40
then what we'll do is cut all this,

160
00:08:07,40 --> 00:08:10,10
and then paste it here,

161
00:08:10,10 --> 00:08:13,70
and we'll make a last change after all this.

162
00:08:13,70 --> 00:08:16,40
So paste,

163
00:08:16,40 --> 00:08:20,80
and then indent it so it's properly in there,

164
00:08:20,80 --> 00:08:23,80
and then, because if you take a look at the data,

165
00:08:23,80 --> 00:08:27,30
so let's quickly take a look here,

166
00:08:27,30 --> 00:08:30,00
we have the description for the back,

167
00:08:30,00 --> 00:08:31,40
but for the front, we don't.

168
00:08:31,40 --> 00:08:34,90
So let's basically still keep that here,

169
00:08:34,90 --> 00:08:38,00
but I wanted a longer description here,

170
00:08:38,00 --> 00:08:40,10
so I did that on the data file,

171
00:08:40,10 --> 00:08:41,80
so we can remove that,

172
00:08:41,80 --> 00:08:46,50
and then basically pass the props,

173
00:08:46,50 --> 00:08:51,90
that card, that description, like so.

174
00:08:51,90 --> 00:08:55,50
And then everything else stays the same.

175
00:08:55,50 --> 00:08:58,30
Okay, so we're good for the card.js.

176
00:08:58,30 --> 00:09:02,50
Now we need to do a few more lines on the CSS.

177
00:09:02,50 --> 00:09:05,20
So let's go to the CSS file.

178
00:09:05,20 --> 00:09:06,60
First thing I'm going to do is

179
00:09:06,60 --> 00:09:10,00
change the transition here to a six,

180
00:09:10,00 --> 00:09:15,00
and then what I'm going to do is add a transform style,

181
00:09:15,00 --> 00:09:18,50
and do preserve-3d,

182
00:09:18,50 --> 00:09:20,80
and then I'm going to add a few more styles.

183
00:09:20,80 --> 00:09:22,60
So let's go and do that.

184
00:09:22,60 --> 00:09:31,90
So card-flip, transform, rotateY,

185
00:09:31,90 --> 00:09:38,50
and then the angle will be a 180deg, for degrees.

186
00:09:38,50 --> 00:09:40,00
Then we'll do another style,

187
00:09:40,00 --> 00:09:45,20
which is the front and the back,

188
00:09:45,20 --> 00:09:47,00
let's put a comma in between the two,

189
00:09:47,00 --> 00:09:49,90
front and back, like so.

190
00:09:49,90 --> 00:09:54,30
Backface-visibility, hidden,

191
00:09:54,30 --> 00:10:03,40
position absolute, top the zero, left zero,

192
00:10:03,40 --> 00:10:09,00
height 480 pixels.

193
00:10:09,00 --> 00:10:14,70
Then we'll do just the front,

194
00:10:14,70 --> 00:10:16,90
z-index,

195
00:10:16,90 --> 00:10:21,20
two, and then transform,

196
00:10:21,20 --> 00:10:24,80
rotateY,

197
00:10:24,80 --> 00:10:27,60
180 degrees.

198
00:10:27,60 --> 00:10:29,60
So basically when we are in the front,

199
00:10:29,60 --> 00:10:34,90
we are able to transform the Y axis to 180.

200
00:10:34,90 --> 00:10:36,40
While we are doing that,

201
00:10:36,40 --> 00:10:40,10
the back will be hidden, and these are the positions,

202
00:10:40,10 --> 00:10:43,80
and we preserve the 3D as we do the card flipping,

203
00:10:43,80 --> 00:10:47,70
and now we only need to do a few more styles,

204
00:10:47,70 --> 00:10:50,80
so we need to do the back as well,

205
00:10:50,80 --> 00:10:55,70
so let's copy this guy here,

206
00:10:55,70 --> 00:11:05,30
so the back here, only remove the z-index, like so,

207
00:11:05,30 --> 00:11:08,60
and then we need to do the container for the back.

208
00:11:08,60 --> 00:11:10,50
So let's do container here,

209
00:11:10,50 --> 00:11:14,10
and just below the container, we'll do a container back.

210
00:11:14,10 --> 00:11:19,00
Container-back,

211
00:11:19,00 --> 00:11:22,40
and let's do padding,

212
00:11:22,40 --> 00:11:26,80
2 pixels, 16 pixels,

213
00:11:26,80 --> 00:11:32,20
background color, white,

214
00:11:32,20 --> 00:11:36,80
and then border radius,

215
00:11:36,80 --> 00:11:40,30
10 pixel, 10 pixel,

216
00:11:40,30 --> 00:11:42,80
10 pixel and 10 pixel.

217
00:11:42,80 --> 00:11:44,40
Wait a minute, I just realized one thing.

218
00:11:44,40 --> 00:11:46,60
This is the transform from front to back,

219
00:11:46,60 --> 00:11:50,40
is actually 0 here, not 180,

220
00:11:50,40 --> 00:11:54,10
because you need to start from 0 and then go to 180.

221
00:11:54,10 --> 00:11:59,50
And one more thing that I forgot.

222
00:11:59,50 --> 00:12:03,70
I need to add height, like this guy here,

223
00:12:03,70 --> 00:12:06,20
to the actual card itself.

224
00:12:06,20 --> 00:12:10,70
So let's do that, just below the width,

225
00:12:10,70 --> 00:12:16,60
480 pixels, perfect.

226
00:12:16,60 --> 00:12:19,80
So one more class that I need to add to our styles here,

227
00:12:19,80 --> 00:12:24,60
I like to kind of bring the grid in a bouncy way

228
00:12:24,60 --> 00:12:25,40
into a screen,

229
00:12:25,40 --> 00:12:33,70
so let's do an animated bounce in up, like so,

230
00:12:33,70 --> 00:12:37,90
for this guy here, save.

231
00:12:37,90 --> 00:12:38,80
Kay.

232
00:12:38,80 --> 00:12:41,10
So let's go to our application,

233
00:12:41,10 --> 00:12:42,70
and right now it's already loaded,

234
00:12:42,70 --> 00:12:46,70
so just refresh that,

235
00:12:46,70 --> 00:12:49,70
and we got the bounce in here, so we're good,

236
00:12:49,70 --> 00:12:52,90
and now if we click on any of the cards,

237
00:12:52,90 --> 00:12:54,20
we get a nice flip,

238
00:12:54,20 --> 00:12:58,40
and we do that, we get the back of the card

239
00:12:58,40 --> 00:13:00,50
with the description that we inserted.

240
00:13:00,50 --> 00:13:01,60
So we are good to go.

241
00:13:01,60 --> 00:13:05,10
If you click on it again, it will bring back the front,

242
00:13:05,10 --> 00:13:07,10
and so on and so forth.

243
00:13:07,10 --> 00:13:12,30
Sometimes, certain browsers may give you some artifacts,

244
00:13:12,30 --> 00:13:14,40
so when you do the flip,

245
00:13:14,40 --> 00:13:16,60
it can actually white out or something like that.

246
00:13:16,60 --> 00:13:20,40
If that's the case, just look at it from another browser

247
00:13:20,40 --> 00:13:22,70
such as Firefox or anything like that.

248
00:13:22,70 --> 00:13:25,00
It does occur from time to time,

249
00:13:25,00 --> 00:13:27,10
and it's one of those unexplained things

250
00:13:27,10 --> 00:13:29,70
that I've seen and experienced.

251
00:13:29,70 --> 00:13:32,00
And I check the code over and over,

252
00:13:32,00 --> 00:13:35,50
when those things happen, and they went unexplained.

253
00:13:35,50 --> 00:13:38,30
So if you see an artifact like that,

254
00:13:38,30 --> 00:13:41,70
just switch to another browser, and off you go.

255
00:13:41,70 --> 00:13:42,50
Okay.

256
00:13:42,50 --> 00:13:43,50
So there you have it.

257
00:13:43,50 --> 00:13:48,00
We added more information to our card, let's move on.

