1
00:00:01,00 --> 00:00:02,90
- [Narrator] Most of today's applications require

2
00:00:02,90 --> 00:00:05,00
a navigation or menu of some sort.

3
00:00:05,00 --> 00:00:07,00
And we'll work on this particular item right now.

4
00:00:07,00 --> 00:00:08,60
So let's get to it.

5
00:00:08,60 --> 00:00:12,30
So the first thing you want to do is go in the components

6
00:00:12,30 --> 00:00:14,20
and create two new files.

7
00:00:14,20 --> 00:00:20,30
So right click, new file, call navigation dot JS

8
00:00:20,30 --> 00:00:23,40
and then create a new file again,

9
00:00:23,40 --> 00:00:28,10
and you guessed it, probably, navigation CSS.

10
00:00:28,10 --> 00:00:29,10
Okay.

11
00:00:29,10 --> 00:00:31,50
So first we'll create the navigation

12
00:00:31,50 --> 00:00:34,70
and just to make sure that this video is short enough,

13
00:00:34,70 --> 00:00:38,00
I'm going to create the navigation and then the functions

14
00:00:38,00 --> 00:00:40,50
to click on the app, that JS,

15
00:00:40,50 --> 00:00:44,40
and then we'll finish with the styles in the next video.

16
00:00:44,40 --> 00:00:47,10
So let's go ahead and create the component first.

17
00:00:47,10 --> 00:00:51,50
So, react from react.

18
00:00:51,50 --> 00:00:57,40
Then import as usual the CSS style.

19
00:00:57,40 --> 00:01:01,60
Navigation dot CSS,

20
00:01:01,60 --> 00:01:06,30
and let's create a stateless component.

21
00:01:06,30 --> 00:01:09,30
Navigation.

22
00:01:09,30 --> 00:01:12,80
And we'll need props on this one because we'll pass

23
00:01:12,80 --> 00:01:18,30
a function from apiDoc JS shortly,

24
00:01:18,30 --> 00:01:20,10
so let's pass props,

25
00:01:20,10 --> 00:01:22,70
and then let's just create a div

26
00:01:22,70 --> 00:01:28,50
with an ID of my nav,

27
00:01:28,50 --> 00:01:31,10
with a class name of overlay,

28
00:01:31,10 --> 00:01:35,80
and we'll create those classes in the next video.

29
00:01:35,80 --> 00:01:39,60
And inside of that particular div we're going to have

30
00:01:39,60 --> 00:01:47,60
an A element with a class name of close VTN,

31
00:01:47,60 --> 00:01:53,20
for close button, and unclick

32
00:01:53,20 --> 00:01:56,10
we'll run a function.

33
00:01:56,10 --> 00:01:58,30
And by the way, you may be wondering one thing.

34
00:01:58,30 --> 00:02:00,70
And let me just finish that line and I'll explain

35
00:02:00,70 --> 00:02:03,60
where I'm going with this.

36
00:02:03,60 --> 00:02:07,40
Close nav like so.

37
00:02:07,40 --> 00:02:13,20
So here, on the unclick, I'm going to run a function

38
00:02:13,20 --> 00:02:16,10
with the function close nav.

39
00:02:16,10 --> 00:02:20,40
But why am I not doing just this?

40
00:02:20,40 --> 00:02:25,90
And if you do this, as soon as your component is rendered,

41
00:02:25,90 --> 00:02:29,60
it's going to run this particular function.

42
00:02:29,60 --> 00:02:33,30
If you don't do the function this way,

43
00:02:33,30 --> 00:02:35,40
it's going to run automatically.

44
00:02:35,40 --> 00:02:38,80
So what we want to do is the unclick.

45
00:02:38,80 --> 00:02:41,50
So unclicking that specific element

46
00:02:41,50 --> 00:02:43,40
we want to run this function.

47
00:02:43,40 --> 00:02:44,70
So that's why I did it this way.

48
00:02:44,70 --> 00:02:49,30
So if you're placing a function inside of an event

49
00:02:49,30 --> 00:02:52,60
and it runs automatically, it's probably because you haven't

50
00:02:52,60 --> 00:02:53,90
done it this way.

51
00:02:53,90 --> 00:02:56,30
So this is the proper syntax to run a function

52
00:02:56,30 --> 00:02:59,50
on the click event and not right away.

53
00:02:59,50 --> 00:03:04,50
Let me just finish that line, here.

54
00:03:04,50 --> 00:03:06,80
This is basically an X icon

55
00:03:06,80 --> 00:03:10,40
as opposed to just some regular text.

56
00:03:10,40 --> 00:03:16,00
Okay, so we'll need another div with a class name,

57
00:03:16,00 --> 00:03:23,10
overlay content, and then let's do our menu.

58
00:03:23,10 --> 00:03:31,30
So, I'm just going to do one and then do the rest.

59
00:03:31,30 --> 00:03:35,10
Kay, so this one will be vitamin juice,

60
00:03:35,10 --> 00:03:37,10
and then let's copy that line

61
00:03:37,10 --> 00:03:40,20
and paste it a couple times.

62
00:03:40,20 --> 00:03:44,90
One, two, let's do four.

63
00:03:44,90 --> 00:03:47,90
And the second one we'll just call clothing,

64
00:03:47,90 --> 00:03:50,80
'cause our company's also selling clothing,

65
00:03:50,80 --> 00:03:53,20
our company is also selling supplements.

66
00:03:53,20 --> 00:03:56,50
Although you may be thinking Vitamin Juice must be

67
00:03:56,50 --> 00:03:58,70
a supplement or whatever,

68
00:03:58,70 --> 00:04:01,90
I'm just doing a fictive company here.

69
00:04:01,90 --> 00:04:04,80
Okay?

70
00:04:04,80 --> 00:04:08,60
And then semi-colon here, and then export,

71
00:04:08,60 --> 00:04:14,20
mark in navigation, component, and save.

72
00:04:14,20 --> 00:04:16,70
And let's go in the app dot JS now to create

73
00:04:16,70 --> 00:04:20,20
what we need to basically be able to open

74
00:04:20,20 --> 00:04:23,40
and click our menu.

75
00:04:23,40 --> 00:04:27,00
And let's go and create the two functions that we need.

76
00:04:27,00 --> 00:04:29,30
So let's just scroll all the way down after

77
00:04:29,30 --> 00:04:32,80
the click card on line 55, return.

78
00:04:32,80 --> 00:04:38,00
And then let's do an open nav function,

79
00:04:38,00 --> 00:04:42,70
and this is going to be some pure HTML stuff,

80
00:04:42,70 --> 00:04:47,30
so document dot get, element by ID,

81
00:04:47,30 --> 00:04:51,70
and we're getting the element my nav,

82
00:04:51,70 --> 00:04:54,40
let me just make sure the syntax is the same,

83
00:04:54,40 --> 00:04:56,40
so let's go here.

84
00:04:56,40 --> 00:05:00,40
My nav, so I'm going to copy the actual syntax

85
00:05:00,40 --> 00:05:02,90
from line five here and paste it here,

86
00:05:02,90 --> 00:05:06,00
just to make sure that I'm getting the right stuff.

87
00:05:06,00 --> 00:05:06,90
Okay.

88
00:05:06,90 --> 00:05:16,50
And then style width equals 100%.

89
00:05:16,50 --> 00:05:18,80
So basically when I'm clicking open nav,

90
00:05:18,80 --> 00:05:23,80
I'm going to make this particular ID 100%

91
00:05:23,80 --> 00:05:28,90
and I'm going to do the exact same thing here

92
00:05:28,90 --> 00:05:34,30
and call this one close nav,

93
00:05:34,30 --> 00:05:39,50
and basically copy line 58, so copy that,

94
00:05:39,50 --> 00:05:47,30
paste it on 62, and make the same thing at 0%, like so.

95
00:05:47,30 --> 00:05:49,90
Okay.

96
00:05:49,90 --> 00:05:54,10
Now we absolutely need to bind this again

97
00:05:54,10 --> 00:05:57,00
if we want to pass it to our props

98
00:05:57,00 --> 00:05:59,60
we want to make sure that we bind them first.

99
00:05:59,60 --> 00:06:02,10
Let's do two.

100
00:06:02,10 --> 00:06:05,50
Click here and option click here, and

101
00:06:05,50 --> 00:06:10,20
call this one, the first one we'll do open nav

102
00:06:10,20 --> 00:06:13,90
and then click here, and then option click here,

103
00:06:13,90 --> 00:06:21,30
and then do the close nav one.

104
00:06:21,30 --> 00:06:27,90
So let's import our navigation component.

105
00:06:27,90 --> 00:06:35,10
From components, navigation,

106
00:06:35,10 --> 00:06:38,50
and then let's scroll all the way down

107
00:06:38,50 --> 00:06:45,30
and just below the image we'll remove this H1 line here,

108
00:06:45,30 --> 00:06:47,30
so let's remove that,

109
00:06:47,30 --> 00:06:49,40
and we'll do the navigation this time.

110
00:06:49,40 --> 00:06:54,20
So navigation, and I'm going to pass only the close nav.

111
00:06:54,20 --> 00:06:57,00
Because we'll open the menu from the logo,

112
00:06:57,00 --> 00:06:59,80
and we'll close it inside of the navigation.

113
00:06:59,80 --> 00:07:04,30
So we only need to pass the close nav to our prop.

114
00:07:04,30 --> 00:07:07,70
So let's do this.

115
00:07:07,70 --> 00:07:12,60
Close nav, close our component.

116
00:07:12,60 --> 00:07:23,10
And then here we'll add and unclick this open nav.

117
00:07:23,10 --> 00:07:28,10
Okay, so let's save that and now we got half the work done,

118
00:07:28,10 --> 00:07:30,00
and let's continue.

