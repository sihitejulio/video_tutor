1
00:00:01,00 --> 00:00:02,10
- [Instructor] In many applications

2
00:00:02,10 --> 00:00:04,10
you see a lazy loading feature where

3
00:00:04,10 --> 00:00:06,70
the components are loaded as you scroll through,

4
00:00:06,70 --> 00:00:08,10
and in some cases that might be

5
00:00:08,10 --> 00:00:10,00
helpful with improving performance,

6
00:00:10,00 --> 00:00:13,00
so lets get this coded.

7
00:00:13,00 --> 00:00:14,60
The first thing I want to show you

8
00:00:14,60 --> 00:00:17,50
is the actual page of the package so,

9
00:00:17,50 --> 00:00:23,80
go to github.com/jasonslyvia/react-lazyload

10
00:00:23,80 --> 00:00:27,00
and then just scroll all the way down to Usage

11
00:00:27,00 --> 00:00:28,30
and you can take a look.

12
00:00:28,30 --> 00:00:30,10
It's fairly easy to put together.

13
00:00:30,10 --> 00:00:33,70
So, basically, you only need to put the component

14
00:00:33,70 --> 00:00:36,70
as a parent to the images

15
00:00:36,70 --> 00:00:39,10
or whatever it is that you want to lazy load.

16
00:00:39,10 --> 00:00:41,90
And then, the other thing that you need to take a look at

17
00:00:41,90 --> 00:00:43,50
is the props that it takes.

18
00:00:43,50 --> 00:00:47,50
So, the height is something that you may have to play with

19
00:00:47,50 --> 00:00:49,50
depending on the size of your window.

20
00:00:49,50 --> 00:00:53,40
But usually I put it around 650 to 800.

21
00:00:53,40 --> 00:00:55,30
So if you put anything lower

22
00:00:55,30 --> 00:00:56,80
when you're going to start scrolling

23
00:00:56,80 --> 00:00:59,70
you're not going to see your elements

24
00:00:59,70 --> 00:01:01,40
lazy loading on the screen.

25
00:01:01,40 --> 00:01:04,00
So, this is a note to self.

26
00:01:04,00 --> 00:01:06,90
The other props we're not really going to use.

27
00:01:06,90 --> 00:01:10,50
So, once, basically it's going to lazy load once.

28
00:01:10,50 --> 00:01:13,60
The scroll, you can set it to false,

29
00:01:13,60 --> 00:01:15,60
so if you scroll then its not going to,

30
00:01:15,60 --> 00:01:18,50
basically, react if you set it to false.

31
00:01:18,50 --> 00:01:22,60
Basically, the other ones you can read and take a look at.

32
00:01:22,60 --> 00:01:26,00
But the one that we'll actually use is the offset.

33
00:01:26,00 --> 00:01:28,50
So, offset is a good way to show off

34
00:01:28,50 --> 00:01:29,80
that you're using lazy load.

35
00:01:29,80 --> 00:01:32,20
If you're not doing any offset,

36
00:01:32,20 --> 00:01:34,90
your components are going to appear on screen

37
00:01:34,90 --> 00:01:37,30
as soon as they are on screen

38
00:01:37,30 --> 00:01:40,20
and you're not going to see that they're lazy loaded.

39
00:01:40,20 --> 00:01:43,70
So, that's basically if you don't want to show that you're

40
00:01:43,70 --> 00:01:47,80
lazy loading and you want it to be done in the back.

41
00:01:47,80 --> 00:01:49,20
But, in our case here,

42
00:01:49,20 --> 00:01:51,60
I'm going to set an offset so you can see it

43
00:01:51,60 --> 00:01:55,00
and be able to see that what we're doing

44
00:01:55,00 --> 00:01:56,60
in the code is actually working.

45
00:01:56,60 --> 00:01:58,00
So let's get to that.

46
00:01:58,00 --> 00:02:00,10
Let's go back to VS code.

47
00:02:00,10 --> 00:02:02,20
So, the first thing we're going to do

48
00:02:02,20 --> 00:02:05,40
is verify that it is installed, and yes it is.

49
00:02:05,40 --> 00:02:08,60
So lazy load is indeed in the package.jason file.

50
00:02:08,60 --> 00:02:10,10
So we're good to go.

51
00:02:10,10 --> 00:02:13,10
If you don't have it, basically all you have to do

52
00:02:13,10 --> 00:02:16,30
is do an NPM install and then dash dash save

53
00:02:16,30 --> 00:02:20,40
and react-lazyload and get your project started,

54
00:02:20,40 --> 00:02:22,70
and then get back here.

55
00:02:22,70 --> 00:02:24,10
So the only thing we need to do

56
00:02:24,10 --> 00:02:26,50
before we actually use it is import it.

57
00:02:26,50 --> 00:02:34,70
LazyLoad from react-lazyload, like so.

58
00:02:34,70 --> 00:02:38,00
And then, all we have to do to use it is

59
00:02:38,00 --> 00:02:42,40
to set it as the parent of our entire card component.

60
00:02:42,40 --> 00:02:44,90
So, basically, wrap all our code

61
00:02:44,90 --> 00:02:47,20
inside of a lazy load component.

62
00:02:47,20 --> 00:02:49,10
So, lets do the LazyLoad.

63
00:02:49,10 --> 00:02:53,50
We're going to paste the props that we are going to need.

64
00:02:53,50 --> 00:02:56,20
The first one is going to be

65
00:02:56,20 --> 00:02:58,80
the actual height of your lazy load.

66
00:02:58,80 --> 00:03:01,80
So I'm going to set it at 650, that should work.

67
00:03:01,80 --> 00:03:05,70
So if I'm scrolling and I'm not seeing my components

68
00:03:05,70 --> 00:03:09,00
showing up then I just need to set this higher.

69
00:03:09,00 --> 00:03:10,60
And the same for you.

70
00:03:10,60 --> 00:03:12,90
Sometimes you need to play a little bit with that

71
00:03:12,90 --> 00:03:14,50
value to make sure that it works,

72
00:03:14,50 --> 00:03:18,20
but 650 to 800 is usually a good value.

73
00:03:18,20 --> 00:03:21,40
And then we're going to set an offset,

74
00:03:21,40 --> 00:03:23,90
and do it at a minus 100,

75
00:03:23,90 --> 00:03:28,10
so we can actually see our lazy loading if it works or not.

76
00:03:28,10 --> 00:03:31,50
And then, we're going to grab the closing tag,

77
00:03:31,50 --> 00:03:33,90
and cut it, so Command+X,

78
00:03:33,90 --> 00:03:36,80
and then paste it at the end.

79
00:03:36,80 --> 00:03:40,00
So, return line 20, paste it,

80
00:03:40,00 --> 00:03:43,90
and then lets just indent this so it looks cleaner.

81
00:03:43,90 --> 00:03:47,20
And all we have to do at this point is save.

82
00:03:47,20 --> 00:03:51,40
Okay, so let's go back to our application

83
00:03:51,40 --> 00:03:52,70
and right now there's nothing

84
00:03:52,70 --> 00:03:54,90
and if we scroll then our stuff

85
00:03:54,90 --> 00:03:57,00
are going to start showing up.

86
00:03:57,00 --> 00:03:58,50
And, as you can see,

87
00:03:58,50 --> 00:04:01,10
as we scroll we have a little bit of a pixel,

88
00:04:01,10 --> 00:04:04,20
basically offset before the cards

89
00:04:04,20 --> 00:04:08,10
are showing up here so were good.

90
00:04:08,10 --> 00:04:14,90
And, if you want to see it one more time just reload.

91
00:04:14,90 --> 00:04:20,00
Start scrolling, and our stuff are going to show up.

92
00:04:20,00 --> 00:04:22,60
So, here's the UX issue here.

93
00:04:22,60 --> 00:04:27,60
So when we actually refresh the page,

94
00:04:27,60 --> 00:04:29,00
we don't see anything.

95
00:04:29,00 --> 00:04:31,50
And that's a big UX no no.

96
00:04:31,50 --> 00:04:34,20
So, what we're going to do in the future video

97
00:04:34,20 --> 00:04:37,50
is revise our UX of this application

98
00:04:37,50 --> 00:04:39,80
and then make some minor adjustments

99
00:04:39,80 --> 00:04:42,40
to make sure that we are good to go.

100
00:04:42,40 --> 00:04:43,90
So, this is one of the things that we're

101
00:04:43,90 --> 00:04:47,10
going to fix as part of our UX validation.

102
00:04:47,10 --> 00:04:49,80
Alright, so now we've got a simple solution

103
00:04:49,80 --> 00:04:53,00
to do lazy load in our application, lets move on.

