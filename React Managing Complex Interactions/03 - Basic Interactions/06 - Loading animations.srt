1
00:00:01,00 --> 00:00:02,30
- [Instructor] When your application loads

2
00:00:02,30 --> 00:00:04,10
there is sometimes a wait period

3
00:00:04,10 --> 00:00:06,90
while your components render and loads its data.

4
00:00:06,90 --> 00:00:08,80
Could be waiting for APIs to respond

5
00:00:08,80 --> 00:00:11,00
or any other process.

6
00:00:11,00 --> 00:00:13,70
Making a loading state is always a good idea

7
00:00:13,70 --> 00:00:15,50
and great UX practice.

8
00:00:15,50 --> 00:00:18,20
So the user knows something is happening in the background

9
00:00:18,20 --> 00:00:20,30
a loading animation is best for this.

10
00:00:20,30 --> 00:00:21,70
So let me demonstrate.

11
00:00:21,70 --> 00:00:24,30
So the first thing you need to do is create

12
00:00:24,30 --> 00:00:28,40
in the components folder, so go in the components folder

13
00:00:28,40 --> 00:00:30,10
and create two new files.

14
00:00:30,10 --> 00:00:34,80
So create a new file called loading.js,

15
00:00:34,80 --> 00:00:40,60
and let's create another new file, loading.css.

16
00:00:40,60 --> 00:00:45,60
And now we're going to also go in the app.js

17
00:00:45,60 --> 00:00:48,10
and set a few things.

18
00:00:48,10 --> 00:00:50,40
So right now if you load the application

19
00:00:50,40 --> 00:00:52,90
there's not going to be any wait, so all the components

20
00:00:52,90 --> 00:00:55,60
will render immediately because the data is available

21
00:00:55,60 --> 00:00:57,40
and there is now wait period.

22
00:00:57,40 --> 00:00:58,70
So let's simulate that.

23
00:00:58,70 --> 00:01:01,60
So let's go inside of the state

24
00:01:01,60 --> 00:01:05,80
and set a new property called loading

25
00:01:05,80 --> 00:01:08,60
and make it true and then what we'll do

26
00:01:08,60 --> 00:01:13,30
is leverage the component did mount lifecycle method,

27
00:01:13,30 --> 00:01:16,80
so this will execute once the component

28
00:01:16,80 --> 00:01:20,50
is fully loaded, so did mount.

29
00:01:20,50 --> 00:01:22,50
And what we'll do, we'll set a timeout.

30
00:01:22,50 --> 00:01:28,30
So basically we'll do a set timeout

31
00:01:28,30 --> 00:01:35,80
which will be this set state and we'll change

32
00:01:35,80 --> 00:01:43,10
the loading to false after three seconds.

33
00:01:43,10 --> 00:01:45,20
So once the component is mounted

34
00:01:45,20 --> 00:01:49,00
it's going to set a timeout and basically for three seconds

35
00:01:49,00 --> 00:01:52,10
our application is going to be in a loading state,

36
00:01:52,10 --> 00:01:55,20
so loading will be true and after those three seconds

37
00:01:55,20 --> 00:01:57,80
it's going to change it to false and therefore

38
00:01:57,80 --> 00:02:02,10
bring the rendering of the information.

39
00:02:02,10 --> 00:02:04,90
So what we need to do right now, so this is going to happen

40
00:02:04,90 --> 00:02:07,80
in the background, so it's not having any impact

41
00:02:07,80 --> 00:02:10,10
on our application yet.

42
00:02:10,10 --> 00:02:13,60
So what we'll do is create a component called loading

43
00:02:13,60 --> 00:02:19,10
and import it here and once it's imported what we'll do

44
00:02:19,10 --> 00:02:22,30
is create an if statement here.

45
00:02:22,30 --> 00:02:25,80
So basically after the header just return

46
00:02:25,80 --> 00:02:28,00
and we'll do some Java Script here.

47
00:02:28,00 --> 00:02:30,70
So do curly braces and then what we'll do

48
00:02:30,70 --> 00:02:37,00
is basically cut and paste line 71 through 77,

49
00:02:37,00 --> 00:02:40,30
cut that and then place it inside

50
00:02:40,30 --> 00:02:42,30
of our curly braces.

51
00:02:42,30 --> 00:02:46,30
And what we'll do here is basically return a line

52
00:02:46,30 --> 00:02:53,10
this dot state dot loading and we'll do an ES six

53
00:02:53,10 --> 00:02:58,60
if statement by doing a question mark.

54
00:02:58,60 --> 00:03:01,60
So basically if this dot state dot loading

55
00:03:01,60 --> 00:03:05,80
is true, then load this component,

56
00:03:05,80 --> 00:03:09,70
otherwise load the grid.

57
00:03:09,70 --> 00:03:11,30
So that's basically what we've just done,

58
00:03:11,30 --> 00:03:15,50
we've done an if statement right here.

59
00:03:15,50 --> 00:03:19,00
So let's make sure we load our loading state.

60
00:03:19,00 --> 00:03:21,30
So we import it here.

61
00:03:21,30 --> 00:03:23,90
Right now it's not created so that fine,

62
00:03:23,90 --> 00:03:26,00
we'll just do that in a second.

63
00:03:26,00 --> 00:03:31,50
Components forward slash loading, save that,

64
00:03:31,50 --> 00:03:34,00
now our application's going to break.

65
00:03:34,00 --> 00:03:36,50
Now let's get to loading.

66
00:03:36,50 --> 00:03:40,70
And create our new component.

67
00:03:40,70 --> 00:03:42,60
By the way, if you have shortcuts,

68
00:03:42,60 --> 00:03:45,70
I'm using Dash whenever I'm not recording

69
00:03:45,70 --> 00:03:50,20
to import all these new stateless or stateful components,

70
00:03:50,20 --> 00:03:53,10
I have a template basically created in Dash.

71
00:03:53,10 --> 00:03:57,00
So that's a little trick if you have Dash

72
00:03:57,00 --> 00:03:59,60
that you can create code snippets

73
00:03:59,60 --> 00:04:02,30
and then import them into your application

74
00:04:02,30 --> 00:04:06,80
or here and create your applications much faster.

75
00:04:06,80 --> 00:04:09,50
So I have that, but when I'm actually teaching

76
00:04:09,50 --> 00:04:12,70
I'm basically typing everything so you guys learn.

77
00:04:12,70 --> 00:04:21,00
So let's load our classes from loading.css.

78
00:04:21,00 --> 00:04:24,10
And then let's create a stateless component,

79
00:04:24,10 --> 00:04:29,00
loading, we won't pass any props to this guy,

80
00:04:29,00 --> 00:04:34,20
this is a simple, dumb component that will basically have

81
00:04:34,20 --> 00:04:37,50
classes applied to it.

82
00:04:37,50 --> 00:04:39,30
And I have a good idea of what I want here,

83
00:04:39,30 --> 00:04:42,80
so I'm actually applying classes that I know

84
00:04:42,80 --> 00:04:45,50
we'll create in a second.

85
00:04:45,50 --> 00:04:51,00
And these specific classes or this specific approach

86
00:04:51,00 --> 00:04:55,00
to a loading is borrowed from a guy,

87
00:04:55,00 --> 00:04:57,50
forger his name, but I'll show you in a second.

88
00:04:57,50 --> 00:05:01,50
This guy has built a bunch of loading templates

89
00:05:01,50 --> 00:05:05,00
and they're really good, so I usually use those.

90
00:05:05,00 --> 00:05:10,30
And you'll see and recognize a lot of those from the web,

91
00:05:10,30 --> 00:05:15,80
it's been used a lot if you ask me.

92
00:05:15,80 --> 00:05:25,30
So bounce two, and let's export a default loading,

93
00:05:25,30 --> 00:05:28,90
like so, so this is done.

94
00:05:28,90 --> 00:05:32,50
So let's get out of VS Code and let me show you

95
00:05:32,50 --> 00:05:34,30
what I've just mentioned.

96
00:05:34,30 --> 00:05:36,10
So it's called Spin Kit.

97
00:05:36,10 --> 00:05:43,20
So if you go to tobiasahlin.com forward slash spinkit

98
00:05:43,20 --> 00:05:45,60
forward slash, you'll see a whole bunch

99
00:05:45,60 --> 00:05:49,20
of spin kits, so you can actually use the one that you want,

100
00:05:49,20 --> 00:05:51,60
you don't have to use the one that I'm using,

101
00:05:51,60 --> 00:05:54,40
I'm actually using the one that I've used

102
00:05:54,40 --> 00:06:01,00
is the second one, so this one here, so the double down.

103
00:06:01,00 --> 00:06:02,30
You can use any one you want here

104
00:06:02,30 --> 00:06:07,20
because most of it is applicable for whatever case you want.

105
00:06:07,20 --> 00:06:10,40
And as I scroll through these you probably recognize

106
00:06:10,40 --> 00:06:13,00
these from a lot of the popular applications

107
00:06:13,00 --> 00:06:14,20
that you're using.

108
00:06:14,20 --> 00:06:15,70
So I'm going to use this one.

109
00:06:15,70 --> 00:06:19,80
So I'm going to click on source and then I can see the CSS.

110
00:06:19,80 --> 00:06:21,90
You probably recognize what we just typed.

111
00:06:21,90 --> 00:06:24,00
So basically what I'm going to do is just

112
00:06:24,00 --> 00:06:26,60
copy and paste all this.

113
00:06:26,60 --> 00:06:33,30
And copy, so command, C and then go into my loading CSS

114
00:06:33,30 --> 00:06:35,40
and paste those styles.

115
00:06:35,40 --> 00:06:37,60
And basically I have my spinner.

116
00:06:37,60 --> 00:06:44,00
So if I save this and let's go back to our application.

117
00:06:44,00 --> 00:06:49,70
And let's refresh, you'll see that now we have a spinner

118
00:06:49,70 --> 00:06:54,40
for three seconds and now after that our application loads.

119
00:06:54,40 --> 00:06:56,70
The only thing that I'm going to do differently

120
00:06:56,70 --> 00:07:00,00
from the current spinner, I'm going to make it bigger

121
00:07:00,00 --> 00:07:03,00
because I think it's a little bit small to my taste.

122
00:07:03,00 --> 00:07:06,00
So let's go inside of the CSS and as opposed

123
00:07:06,00 --> 00:07:10,60
to make it 40 pixels, I'm going to make it 100 pixels.

124
00:07:10,60 --> 00:07:14,60
So basically change the width and the height

125
00:07:14,60 --> 00:07:18,30
to 100 pixels like so, but again,

126
00:07:18,30 --> 00:07:20,30
these are all my personal taste,

127
00:07:20,30 --> 00:07:21,60
feel free to change those.

128
00:07:21,60 --> 00:07:23,70
So as we go inside of the application

129
00:07:23,70 --> 00:07:26,70
let's reload the app and you can see my double spinner

130
00:07:26,70 --> 00:07:28,20
is much bigger.

131
00:07:28,20 --> 00:07:30,80
Okay, so you've got a loading state

132
00:07:30,80 --> 00:07:34,90
when your app needs to load resources in the background.

133
00:07:34,90 --> 00:07:36,00
Let's move on.

