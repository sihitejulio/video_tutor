1
00:00:01,00 --> 00:00:02,00
- [Instructor] Before we get started

2
00:00:02,00 --> 00:00:03,50
with animating our project,

3
00:00:03,50 --> 00:00:06,30
let's update the current version with a few modifications.

4
00:00:06,30 --> 00:00:07,70
So the first thing I want you to do

5
00:00:07,70 --> 00:00:11,30
is go inside of Vitaminstore

6
00:00:11,30 --> 00:00:14,80
and also open the exercise files

7
00:00:14,80 --> 00:00:18,30
and in the exercise files grab the resources

8
00:00:18,30 --> 00:00:19,90
and grab the background first

9
00:00:19,90 --> 00:00:22,90
so let's copy that so Command + C

10
00:00:22,90 --> 00:00:27,20
and then drop it in the public folder so Command + V

11
00:00:27,20 --> 00:00:32,40
in Vitaminstore so in the current project, paste it there,

12
00:00:32,40 --> 00:00:38,50
and then grab the logo.png so copy that

13
00:00:38,50 --> 00:00:42,70
and drop it in the source folder because right now,

14
00:00:42,70 --> 00:00:45,80
we have a logo.svg that we'll remove

15
00:00:45,80 --> 00:00:48,70
and we'll add a logo.png

16
00:00:48,70 --> 00:00:54,90
and then we'll link the current call to logo.svg to logo.png

17
00:00:54,90 --> 00:01:03,40
so let's remove that and then paste with Command + V.

18
00:01:03,40 --> 00:01:05,80
So we should be good to go there.

19
00:01:05,80 --> 00:01:09,00
And now what you need to do is get back into Visual Code,

20
00:01:09,00 --> 00:01:13,90
then change the import logo from logo.svg to png

21
00:01:13,90 --> 00:01:19,20
so on line two remove svg, add png,

22
00:01:19,20 --> 00:01:22,30
and then let's also remove the p element here.

23
00:01:22,30 --> 00:01:24,80
We don't need it.

24
00:01:24,80 --> 00:01:26,80
Save that and once you save that,

25
00:01:26,80 --> 00:01:28,10
let's go back to our application

26
00:01:28,10 --> 00:01:30,90
and make sure that we have our new logo turning

27
00:01:30,90 --> 00:01:32,10
so we're good to go.

28
00:01:32,10 --> 00:01:36,20
Now let's go back to our code and go inside of the app.css.

29
00:01:36,20 --> 00:01:38,60
So what we're going to do now is basically do

30
00:01:38,60 --> 00:01:43,20
a full width background of the background file

31
00:01:43,20 --> 00:01:45,00
that we just added to our public

32
00:01:45,00 --> 00:01:47,00
so basically this picture

33
00:01:47,00 --> 00:01:49,60
should be the background of our application

34
00:01:49,60 --> 00:01:52,20
and it should automatically resize and cover

35
00:01:52,20 --> 00:01:55,40
the background of our application so let's go and do that.

36
00:01:55,40 --> 00:02:01,10
So what I'm going to do first is remove the app here

37
00:02:01,10 --> 00:02:05,80
and then do an html, body

38
00:02:05,80 --> 00:02:08,20
and then add a few styles here

39
00:02:08,20 --> 00:02:13,50
so we'll do a text-align, center.

40
00:02:13,50 --> 00:02:16,10
We'll do a background

41
00:02:16,10 --> 00:02:19,30
and then let's do that on the second line.

42
00:02:19,30 --> 00:02:23,80
We'll do a linear-gradient like so and then enter

43
00:02:23,80 --> 00:02:31,00
and then we'll do our rgba and we'll do 0, 0, 0

44
00:02:31,00 --> 00:02:32,90
and basically we're doing a gradient

45
00:02:32,90 --> 00:02:36,70
just in case the picture doesn't load.

46
00:02:36,70 --> 00:02:40,80
And then let's do it again so I'll copy that line

47
00:02:40,80 --> 00:02:45,00
so basically we're doing a gradient for the same color

48
00:02:45,00 --> 00:02:49,20
like so and then do a comma on line seven

49
00:02:49,20 --> 00:02:52,30
and then this is where we're going to add the background

50
00:02:52,30 --> 00:02:56,40
so the url so it's coming from our public folder

51
00:02:56,40 --> 00:03:02,30
so basically /background.jpg

52
00:03:02,30 --> 00:03:03,70
and then we'll do a few things,

53
00:03:03,70 --> 00:03:12,80
no-repeat center center fixed

54
00:03:12,80 --> 00:03:19,40
background and select size

55
00:03:19,40 --> 00:03:23,10
to cover our screen like so.

56
00:03:23,10 --> 00:03:24,80
All right, so we'll save this.

57
00:03:24,80 --> 00:03:26,60
Go back to the application

58
00:03:26,60 --> 00:03:32,10
and now we have our background covering our full picture.

59
00:03:32,10 --> 00:03:33,80
The only thing that we need to do now is

60
00:03:33,80 --> 00:03:36,30
remove the background color for the app header

61
00:03:36,30 --> 00:03:40,60
so we can see our background so let's remove that, save it,

62
00:03:40,60 --> 00:03:43,30
then go back to our app and now we're good to go.

63
00:03:43,30 --> 00:03:45,10
So now we have the surfer

64
00:03:45,10 --> 00:03:47,10
as the background of our application.

65
00:03:47,10 --> 00:03:49,30
We have the logo that's turning

66
00:03:49,30 --> 00:03:51,10
and we are good to go.

67
00:03:51,10 --> 00:03:55,00
So now we've got our canvas to work with so let's move on.

