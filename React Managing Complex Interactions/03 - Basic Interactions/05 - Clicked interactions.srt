1
00:00:01,00 --> 00:00:02,10
- [Instructor] By far the one action

2
00:00:02,10 --> 00:00:03,70
we do quite often when browsing

3
00:00:03,70 --> 00:00:07,00
is clicking on items, links, etc.

4
00:00:07,00 --> 00:00:08,40
Needless to say this is one of

5
00:00:08,40 --> 00:00:10,80
the most important interaction to master.

6
00:00:10,80 --> 00:00:14,00
Once again, I leveraged the state to update a UI state

7
00:00:14,00 --> 00:00:16,40
and interact with the application.

8
00:00:16,40 --> 00:00:17,60
Let me demonstrate.

9
00:00:17,60 --> 00:00:19,50
So go back to the S code.

10
00:00:19,50 --> 00:00:21,80
And the first thing we'll do is basically add

11
00:00:21,80 --> 00:00:23,80
some information here on the ID.

12
00:00:23,80 --> 00:00:27,10
So, let's start making this a little bit bigger.

13
00:00:27,10 --> 00:00:30,80
And very, very soon we're actually going to load data

14
00:00:30,80 --> 00:00:34,10
as opposed to enter manual here in the files.

15
00:00:34,10 --> 00:00:36,00
So, no worries.

16
00:00:36,00 --> 00:00:39,30
'Kay, so let's do that.

17
00:00:39,30 --> 00:00:43,40
Let's return.

18
00:00:43,40 --> 00:00:46,00
And let me do something very, very quickly

19
00:00:46,00 --> 00:00:48,50
that's going to help a lot.

20
00:00:48,50 --> 00:00:50,50
Basically you do the option click

21
00:00:50,50 --> 00:00:52,30
whenever you want to do something.

22
00:00:52,30 --> 00:00:55,10
So what I'm going to do now is basically

23
00:00:55,10 --> 00:00:56,80
click everywhere I want to return.

24
00:00:56,80 --> 00:01:01,30
So, click before the first curly braces.

25
00:01:01,30 --> 00:01:06,00
And then option click everywhere you want to add something.

26
00:01:06,00 --> 00:01:08,40
So there's going to be a click here.

27
00:01:08,40 --> 00:01:09,90
One here.

28
00:01:09,90 --> 00:01:11,20
One at the end here.

29
00:01:11,20 --> 00:01:12,80
One before.

30
00:01:12,80 --> 00:01:14,70
One after the number.

31
00:01:14,70 --> 00:01:15,90
One before.

32
00:01:15,90 --> 00:01:17,30
And one at the end.

33
00:01:17,30 --> 00:01:18,70
So once you hit enter,

34
00:01:18,70 --> 00:01:21,20
then you're going to get something similar to this.

35
00:01:21,20 --> 00:01:22,60
And then we can add the comma.

36
00:01:22,60 --> 00:01:26,80
So option click at the end as well.

37
00:01:26,80 --> 00:01:30,60
Yeah, quite strangely, this is not three but six.

38
00:01:30,60 --> 00:01:32,40
So let's fix that.

39
00:01:32,40 --> 00:01:34,80
And now that we have that,

40
00:01:34,80 --> 00:01:36,60
let's put our cursor,

41
00:01:36,60 --> 00:01:41,30
so option click at the end after the comma on every line.

42
00:01:41,30 --> 00:01:42,90
And then return.

43
00:01:42,90 --> 00:01:47,00
And then animation.

44
00:01:47,00 --> 00:01:49,20
And then add card.

45
00:01:49,20 --> 00:01:53,00
So basically what we're doing is,

46
00:01:53,00 --> 00:01:54,70
we're creating in the state,

47
00:01:54,70 --> 00:01:58,20
a prop that's called animation that will pass card.

48
00:01:58,20 --> 00:02:03,40
And then we'll use that as the class of the old card.

49
00:02:03,40 --> 00:02:06,90
But the thing is, once we start clicking on the card itself,

50
00:02:06,90 --> 00:02:10,90
we'll change that class to whatever else.

51
00:02:10,90 --> 00:02:12,00
We'll change that class actually

52
00:02:12,00 --> 00:02:15,50
to something called card animated zoom out.

53
00:02:15,50 --> 00:02:17,80
And once we click on a specific card,

54
00:02:17,80 --> 00:02:21,40
it will zoom out the card.

55
00:02:21,40 --> 00:02:23,30
Now that we've created those properties

56
00:02:23,30 --> 00:02:25,40
for each item in the state,

57
00:02:25,40 --> 00:02:28,20
let's create another function called click card.

58
00:02:28,20 --> 00:02:31,50
So hit return on line 38.

59
00:02:31,50 --> 00:02:36,20
And let's create click card.

60
00:02:36,20 --> 00:02:40,10
And that will take an argument of card.

61
00:02:40,10 --> 00:02:43,90
And then, what I'm going to do is type all this

62
00:02:43,90 --> 00:02:46,20
and then explain what I just typed.

63
00:02:46,20 --> 00:02:49,10
So bare with me for a second.

64
00:02:49,10 --> 00:02:53,50
So this dot state dot cards.

65
00:02:53,50 --> 00:03:04,00
Then cards and whatever is the card ID dot animation

66
00:03:04,00 --> 00:03:13,30
equals card animated zoom out.

67
00:03:13,30 --> 00:03:15,70
And then we'll console log the cards

68
00:03:15,70 --> 00:03:18,20
just to make sure that we are seeing everything.

69
00:03:18,20 --> 00:03:20,70
So you can take a look at what you are getting

70
00:03:20,70 --> 00:03:25,20
from the cards at this stage.

71
00:03:25,20 --> 00:03:31,70
And then this dot set state.

72
00:03:31,70 --> 00:03:34,20
Again, I'm going to explain this in a second.

73
00:03:34,20 --> 00:03:37,10
What I'm doing and while I'm thinking about it.

74
00:03:37,10 --> 00:03:40,40
Because this is going to use an array position,

75
00:03:40,40 --> 00:03:42,40
we need to have the numbers change.

76
00:03:42,40 --> 00:03:45,70
We need to start at zero 'cause arrays start at zero.

77
00:03:45,70 --> 00:03:48,10
So let's change that here very quickly.

78
00:03:48,10 --> 00:03:52,70
So two, three, four, five.

79
00:03:52,70 --> 00:03:54,40
Okay, so we're good there.

80
00:03:54,40 --> 00:03:56,50
So, let me explain this.

81
00:03:56,50 --> 00:03:59,00
So we'll pass a card.

82
00:03:59,00 --> 00:04:03,20
So basically whichever card we click on, we'll pass.

83
00:04:03,20 --> 00:04:05,60
Then we'll use the state,

84
00:04:05,60 --> 00:04:09,00
so all the cards in the state to create a new object.

85
00:04:09,00 --> 00:04:10,60
And then within that object,

86
00:04:10,60 --> 00:04:12,20
we'll pass the card ID.

87
00:04:12,20 --> 00:04:15,20
So basically, which position in the array

88
00:04:15,20 --> 00:04:17,40
we have this card from.

89
00:04:17,40 --> 00:04:21,90
And then, in the animation property for that specific card,

90
00:04:21,90 --> 00:04:24,30
we'll change card, basically the

91
00:04:24,30 --> 00:04:27,50
value to card animated zoom out.

92
00:04:27,50 --> 00:04:29,90
And then we'll basically set the state.

93
00:04:29,90 --> 00:04:31,80
So basically we're doing some form

94
00:04:31,80 --> 00:04:33,30
of functional programming here.

95
00:04:33,30 --> 00:04:39,90
So we're recreating the cards object by changing a value

96
00:04:39,90 --> 00:04:43,50
of one based off their position in the array.

97
00:04:43,50 --> 00:04:45,30
And then we're changing the state.

98
00:04:45,30 --> 00:04:50,30
Or basically updating the state with this new card object.

99
00:04:50,30 --> 00:04:53,90
And then whatever card as the new class

100
00:04:53,90 --> 00:04:57,10
will be rendered with this.

101
00:04:57,10 --> 00:04:59,50
Let's put assignment colon here.

102
00:04:59,50 --> 00:05:01,80
But if we do this now,

103
00:05:01,80 --> 00:05:05,20
none of the cards will actually change

104
00:05:05,20 --> 00:05:07,80
because we haven't passed that function to the card.

105
00:05:07,80 --> 00:05:13,20
And we haven't set the class to change based off the state.

106
00:05:13,20 --> 00:05:16,00
So let's do that and I'll explain as well what I just said

107
00:05:16,00 --> 00:05:18,60
because it may not make sense right now.

108
00:05:18,60 --> 00:05:22,20
The one thing I'm going to do before I leave app.js file,

109
00:05:22,20 --> 00:05:24,40
I'm going to make sure that I bind

110
00:05:24,40 --> 00:05:26,80
this new function that I just created.

111
00:05:26,80 --> 00:05:28,60
So let's paste that line.

112
00:05:28,60 --> 00:05:30,50
So copy 31, paste it.

113
00:05:30,50 --> 00:05:32,90
And then just click on toggle logo.

114
00:05:32,90 --> 00:05:35,20
Option click on the second one.

115
00:05:35,20 --> 00:05:38,50
And just change this to click card, like so.

116
00:05:38,50 --> 00:05:39,40
Alright.

117
00:05:39,40 --> 00:05:42,20
So let's make sure also we passed

118
00:05:42,20 --> 00:05:43,70
that function just before we leave.

119
00:05:43,70 --> 00:05:44,90
We want to make sure we passed

120
00:05:44,90 --> 00:05:48,20
that function to our card also here.

121
00:05:48,20 --> 00:05:49,80
So we can pass that.

122
00:05:49,80 --> 00:05:54,00
We'll also pass the card object now because we'll need it.

123
00:05:54,00 --> 00:05:56,50
So we need to pass the card object.

124
00:05:56,50 --> 00:05:58,60
So, there's an object with properties that

125
00:05:58,60 --> 00:06:02,80
are going to come in from the iteration of the state.

126
00:06:02,80 --> 00:06:04,50
And now we can pass the whole card,

127
00:06:04,50 --> 00:06:06,50
so we'll have access to the ID

128
00:06:06,50 --> 00:06:08,70
and the animation property

129
00:06:08,70 --> 00:06:10,70
inside of that component.

130
00:06:10,70 --> 00:06:16,60
And also let's pass the clicked card as a prop.

131
00:06:16,60 --> 00:06:22,20
This dot click card, perfect.

132
00:06:22,20 --> 00:06:24,50
I don't have to bind click card here because

133
00:06:24,50 --> 00:06:26,60
I've already done it in the state.

134
00:06:26,60 --> 00:06:30,40
Okay, so let's save the app.js file now.

135
00:06:30,40 --> 00:06:32,90
Let's go in card.

136
00:06:32,90 --> 00:06:34,20
Here.

137
00:06:34,20 --> 00:06:36,60
And let's change a few things.

138
00:06:36,60 --> 00:06:40,30
So inside of the first div here.

139
00:06:40,30 --> 00:06:43,40
Let's return on that.

140
00:06:43,40 --> 00:06:48,60
And let's also put the end of that tag here.

141
00:06:48,60 --> 00:06:51,50
And what we're going to do is basically similar

142
00:06:51,50 --> 00:06:54,30
to what we've done in the app.js here.

143
00:06:54,30 --> 00:06:58,90
So if you want, you can copy basically all this code here

144
00:06:58,90 --> 00:07:03,30
and we'll just change what we're using.

145
00:07:03,30 --> 00:07:05,40
You can copy that on line 56.

146
00:07:05,40 --> 00:07:07,60
You don't have to copy class name.

147
00:07:07,60 --> 00:07:11,00
And then paste it right here, like so.

148
00:07:11,00 --> 00:07:15,80
And what we're going to do is basically pass props, as well.

149
00:07:15,80 --> 00:07:19,10
So let's make sure we are using props here

150
00:07:19,10 --> 00:07:22,20
because we're passing props from app.

151
00:07:22,20 --> 00:07:23,60
But right now we want to have access

152
00:07:23,60 --> 00:07:24,80
to it if we didn't do that.

153
00:07:24,80 --> 00:07:27,70
So now we have access to props.

154
00:07:27,70 --> 00:07:31,90
And what we're going to do now is basically remove all

155
00:07:31,90 --> 00:07:39,20
this here and do props dot card dot animation.

156
00:07:39,20 --> 00:07:43,00
So basically we're passing the card animation class.

157
00:07:43,00 --> 00:07:45,20
Which is usually card.

158
00:07:45,20 --> 00:07:48,70
So right now it would be card.

159
00:07:48,70 --> 00:07:52,60
So this class will be applied to our card component.

160
00:07:52,60 --> 00:07:58,60
So basically the styles from this.

161
00:07:58,60 --> 00:08:03,50
Class here will be passed into this.

162
00:08:03,50 --> 00:08:07,70
If we click on it, it will change to this class

163
00:08:07,70 --> 00:08:11,20
plus the animated css.

164
00:08:11,20 --> 00:08:15,50
And I forget, this is animated zoom out.

165
00:08:15,50 --> 00:08:17,90
So this will be applied here.

166
00:08:17,90 --> 00:08:20,10
So card, animated, zoom out.

167
00:08:20,10 --> 00:08:22,50
So it will zoom out the card as well.

168
00:08:22,50 --> 00:08:27,20
The only thing we need to do now is add the on click event.

169
00:08:27,20 --> 00:08:28,70
Because if we click on it right now,

170
00:08:28,70 --> 00:08:31,60
it's still not going to do anything.

171
00:08:31,60 --> 00:08:32,60
Okay.

172
00:08:32,60 --> 00:08:35,40
And then you do this by doing a function.

173
00:08:35,40 --> 00:08:37,20
Like so.

174
00:08:37,20 --> 00:08:42,00
And pass the props, click card.

175
00:08:42,00 --> 00:08:46,00
And make sure you pass the props dot card.

176
00:08:46,00 --> 00:08:49,80
Okay, so let's briefly recap what we just did.

177
00:08:49,80 --> 00:08:52,90
And save that.

178
00:08:52,90 --> 00:08:56,30
And now we'll take a look at what's happening.

179
00:08:56,30 --> 00:08:58,40
Now we have a prop called animation,

180
00:08:58,40 --> 00:09:01,20
and the value is card.

181
00:09:01,20 --> 00:09:06,70
So once we are passing that to our prop here

182
00:09:06,70 --> 00:09:08,90
on the card component,

183
00:09:08,90 --> 00:09:16,20
basically all these cards have a class of card here.

184
00:09:16,20 --> 00:09:18,20
Once we click on the card then

185
00:09:18,20 --> 00:09:20,50
it will initiate this function.

186
00:09:20,50 --> 00:09:22,90
Click card, so let's go back here.

187
00:09:22,90 --> 00:09:26,70
Click card, and then through a change in the state,

188
00:09:26,70 --> 00:09:30,80
this particular card will be now card animated zoom out,

189
00:09:30,80 --> 00:09:33,70
and then this card will zoom out.

190
00:09:33,70 --> 00:09:36,90
And this is how I would handle a click event.

191
00:09:36,90 --> 00:09:38,50
So now this is all saved.

192
00:09:38,50 --> 00:09:41,10
Let's go back to our actual application.

193
00:09:41,10 --> 00:09:44,40
If I click on any of those, bye bye.

194
00:09:44,40 --> 00:09:47,70
The card is going away.

195
00:09:47,70 --> 00:09:49,80
Okay, there you go.

196
00:09:49,80 --> 00:09:53,70
Now you can make your cards disappear by clicking on them.

197
00:09:53,70 --> 00:09:55,10
Nice game, ain't it.

198
00:09:55,10 --> 00:09:56,00
Let's move on.

