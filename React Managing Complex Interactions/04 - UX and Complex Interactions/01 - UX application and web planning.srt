1
00:00:00,50 --> 00:00:02,70
- [Instructor] It's a good idea to discuss user experience

2
00:00:02,70 --> 00:00:04,80
as you're building your application

3
00:00:04,80 --> 00:00:08,10
along with a good community of test users to evaluate

4
00:00:08,10 --> 00:00:10,80
how well you have achieved your objectives.

5
00:00:10,80 --> 00:00:13,80
In the chapter three folder of the exercise files,

6
00:00:13,80 --> 00:00:16,10
I've created a handout with a few questions

7
00:00:16,10 --> 00:00:18,10
that will help you improve your own eye

8
00:00:18,10 --> 00:00:20,40
in reasoning related to UX.

9
00:00:20,40 --> 00:00:22,80
For each question, write your response

10
00:00:22,80 --> 00:00:25,00
and then I will answer how we've achieved

11
00:00:25,00 --> 00:00:28,80
some of these goals with our application in this course.

12
00:00:28,80 --> 00:00:33,10
First, evaluate your main objective of your application.

13
00:00:33,10 --> 00:00:35,20
Does the application or website meet

14
00:00:35,20 --> 00:00:38,00
what you intended to provide to the user?

15
00:00:38,00 --> 00:00:40,60
If not, what is missing?

16
00:00:40,60 --> 00:00:43,50
In our case, we are selling vitamin juice

17
00:00:43,50 --> 00:00:45,70
and other items related to health.

18
00:00:45,70 --> 00:00:47,70
So as a user or developer,

19
00:00:47,70 --> 00:00:50,20
do you feel like we've achieved this goal?

20
00:00:50,20 --> 00:00:53,70
Please pause the video and answer this.

21
00:00:53,70 --> 00:00:56,60
As for me, we might achieve a quick and easy way

22
00:00:56,60 --> 00:00:58,60
to get the information for the products

23
00:00:58,60 --> 00:01:00,40
and they are front and center

24
00:01:00,40 --> 00:01:02,60
when we first arrive on the site,

25
00:01:02,60 --> 00:01:04,70
but where is the buy button?

26
00:01:04,70 --> 00:01:06,70
There are currently no action on our application

27
00:01:06,70 --> 00:01:09,20
that would facilitate this.

28
00:01:09,20 --> 00:01:11,80
Next, is the entire experience support

29
00:01:11,80 --> 00:01:13,70
getting informed about the products

30
00:01:13,70 --> 00:01:16,60
and also be able to buy what we came to this site

31
00:01:16,60 --> 00:01:19,10
or application in less than two steps?

32
00:01:19,10 --> 00:01:22,10
If not, what is preventing this?

33
00:01:22,10 --> 00:01:24,30
I'll pause again.

34
00:01:24,30 --> 00:01:26,80
So again, you can easily see the information

35
00:01:26,80 --> 00:01:28,30
in less than two steps,

36
00:01:28,30 --> 00:01:31,70
but buying any items is nowhere to be found.

37
00:01:31,70 --> 00:01:35,00
Also, are there any technical or UI pains

38
00:01:35,00 --> 00:01:36,60
that could be improved,

39
00:01:36,60 --> 00:01:39,30
areas that would be frustrating to the user

40
00:01:39,30 --> 00:01:41,60
as they browse our application?

41
00:01:41,60 --> 00:01:44,20
I'll wait for your answer.

42
00:01:44,20 --> 00:01:46,80
If we analyze the application from first load

43
00:01:46,80 --> 00:01:50,00
to all the other items that occur on the application,

44
00:01:50,00 --> 00:01:53,10
there are a few areas that will need improvement.

45
00:01:53,10 --> 00:01:55,30
First, when the application loads,

46
00:01:55,30 --> 00:01:57,90
the lazy loading of our items doesn't get started

47
00:01:57,90 --> 00:02:00,10
until we actually scroll.

48
00:02:00,10 --> 00:02:02,90
This might be a big issue and will need to be addressed

49
00:02:02,90 --> 00:02:06,10
so the user is aware it needs to scroll.

50
00:02:06,10 --> 00:02:08,60
Also, as we scroll we lose access

51
00:02:08,60 --> 00:02:10,80
to the menu logo at the top.

52
00:02:10,80 --> 00:02:13,70
This might be an issue if someone wants to quickly go

53
00:02:13,70 --> 00:02:17,30
to another section of the site without scrolling.

54
00:02:17,30 --> 00:02:19,60
Since this isn't a full application

55
00:02:19,60 --> 00:02:21,40
but a simple course sample

56
00:02:21,40 --> 00:02:23,80
we've built to go through React interactions,

57
00:02:23,80 --> 00:02:26,90
I'll stop there, but you should always be asking

58
00:02:26,90 --> 00:02:29,20
these three questions on any new features

59
00:02:29,20 --> 00:02:32,60
you want to introduce on your site or application.

60
00:02:32,60 --> 00:02:34,30
So let me recap.

61
00:02:34,30 --> 00:02:35,60
Number one,

62
00:02:35,60 --> 00:02:39,40
what is the value of this feature, site, application?

63
00:02:39,40 --> 00:02:40,60
Number two,

64
00:02:40,60 --> 00:02:44,10
is the experience of the site, feature, application

65
00:02:44,10 --> 00:02:47,50
fully supporting the value we want to give our users?

66
00:02:47,50 --> 00:02:49,50
And number three,

67
00:02:49,50 --> 00:02:53,30
are there UI technical issues hindering this value?

68
00:02:53,30 --> 00:02:56,00
And again, once you have a yes or no answer

69
00:02:56,00 --> 00:02:59,90
to these questions, start jotting down some other questions,

70
00:02:59,90 --> 00:03:02,10
so what is hindering this value

71
00:03:02,10 --> 00:03:06,70
or what is really the value of this feature or application?

72
00:03:06,70 --> 00:03:09,80
So ask these questions whenever you introduce something new

73
00:03:09,80 --> 00:03:12,00
and you'll be ahead of the curve.

