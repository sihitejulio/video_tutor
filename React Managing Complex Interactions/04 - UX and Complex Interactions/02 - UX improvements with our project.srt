1
00:00:00,00 --> 00:00:02,00
- [Instructor] Now we've identified some areas

2
00:00:02,00 --> 00:00:04,20
we can improve in our application,

3
00:00:04,20 --> 00:00:07,20
so lets take a look at these and improve them.

4
00:00:07,20 --> 00:00:09,60
Although this is a simple sample of interactions

5
00:00:09,60 --> 00:00:11,10
and not a full application,

6
00:00:11,10 --> 00:00:14,10
we'll fix the simple areas we've identified.

7
00:00:14,10 --> 00:00:16,90
However, when you go through this Q and A exercise

8
00:00:16,90 --> 00:00:19,30
we've done previously, it's always a good idea

9
00:00:19,30 --> 00:00:22,90
to improve all these areas immediately, like we'll do.

10
00:00:22,90 --> 00:00:24,60
Let's get to it.

11
00:00:24,60 --> 00:00:27,60
So the first thing I want you to do is go to vs code

12
00:00:27,60 --> 00:00:33,50
and then open up the app.css on the main directory.

13
00:00:33,50 --> 00:00:36,30
And what we'll do is add a few lines to the static logo.

14
00:00:36,30 --> 00:00:38,70
So the first thing we'll fix is to make sure the logo

15
00:00:38,70 --> 00:00:41,80
doesn't move when we actually start scrolling.

16
00:00:41,80 --> 00:00:45,10
So let's go in to make those changes.

17
00:00:45,10 --> 00:00:48,10
We'll do position,

18
00:00:48,10 --> 00:00:50,10
fixed,

19
00:00:50,10 --> 00:00:53,10
margin left,

20
00:00:53,10 --> 00:00:55,30
margin right,

21
00:00:55,30 --> 00:00:58,70
auto,

22
00:00:58,70 --> 00:01:03,70
top 20 pixels,

23
00:01:03,70 --> 00:01:09,00
left zero,

24
00:01:09,00 --> 00:01:13,30
right zero,

25
00:01:13,30 --> 00:01:16,50
and z index,

26
00:01:16,50 --> 00:01:19,80
we'll put it at 99 just to make sure.

27
00:01:19,80 --> 00:01:23,20
OK, now the next change is the menu.

28
00:01:23,20 --> 00:01:26,00
So we also need to make a z index for the menu

29
00:01:26,00 --> 00:01:28,80
just to make sure that it shows as well.

30
00:01:28,80 --> 00:01:31,80
So I'm going to put a z index right there,

31
00:01:31,80 --> 00:01:34,80
so z index,

32
00:01:34,80 --> 00:01:37,00
99,

33
00:01:37,00 --> 00:01:38,00
perfect.

34
00:01:38,00 --> 00:01:40,70
And the last thing I'm going to remove the key frames

35
00:01:40,70 --> 00:01:43,50
because we're no longer using that.

36
00:01:43,50 --> 00:01:46,80
So let's remove that just to clean up our file.

37
00:01:46,80 --> 00:01:49,90
And save this particular one,

38
00:01:49,90 --> 00:01:52,70
then what we'll do is go inside of loading.

39
00:01:52,70 --> 00:01:54,80
So the second thing we're going to do is to make sure

40
00:01:54,80 --> 00:01:57,90
that the user knows, once the loading is done

41
00:01:57,90 --> 00:01:58,90
that they have to scroll.

42
00:01:58,90 --> 00:02:02,20
So lets go inside of the loading.js file,

43
00:02:02,20 --> 00:02:04,80
inside of the components folder

44
00:02:04,80 --> 00:02:12,00
and then simply wrap this particular guy here,

45
00:02:12,00 --> 00:02:16,10
inside of a div,

46
00:02:16,10 --> 00:02:19,20
and put these lines, so seven to 10,

47
00:02:19,20 --> 00:02:28,20
cut these, put them inside here,

48
00:02:28,20 --> 00:02:31,30
like so.

49
00:02:31,30 --> 00:02:36,10
And then we'll add an h1.

50
00:02:36,10 --> 00:02:39,60
And the reason why I wrapped them inside of a div here

51
00:02:39,60 --> 00:02:44,90
is because you can't simply add and h1 below the spinner,

52
00:02:44,90 --> 00:02:50,20
because you need to have only one element first,

53
00:02:50,20 --> 00:02:52,70
inside of a component and then everything else should

54
00:02:52,70 --> 00:02:55,50
be a child of that parent.

55
00:02:55,50 --> 00:02:58,10
So if I did something without those div here,

56
00:02:58,10 --> 00:03:02,10
so lets say I would remove those two here,

57
00:03:02,10 --> 00:03:05,10
then this wouldn't be working inside of react

58
00:03:05,10 --> 00:03:07,50
it would actually give you an error.

59
00:03:07,50 --> 00:03:09,40
So that's why I wrapped it this way.

60
00:03:09,40 --> 00:03:14,50
So, now, we're going to do some little basically warning.

61
00:03:14,50 --> 00:03:15,90
So you can type something different here,

62
00:03:15,90 --> 00:03:18,30
if you selected a different spinner

63
00:03:18,30 --> 00:03:20,10
when we were on the website,

64
00:03:20,10 --> 00:03:26,70
but for me it's circles disappear, begin scrolling.

65
00:03:26,70 --> 00:03:31,50
So this is a simple fix, again we're educating the user

66
00:03:31,50 --> 00:03:33,80
that they need to do something while they're seeing

67
00:03:33,80 --> 00:03:36,80
the a little circles and once they are gone

68
00:03:36,80 --> 00:03:39,50
then they need to start scrolling.

69
00:03:39,50 --> 00:03:42,20
So Let's do that, let's save that

70
00:03:42,20 --> 00:03:46,20
and then let's go inside of the loading.css

71
00:03:46,20 --> 00:03:48,90
in the components folder

72
00:03:48,90 --> 00:03:55,90
and then we'll simply add some styles to h1,

73
00:03:55,90 --> 00:04:00,90
by doing a color, white, like so.

74
00:04:00,90 --> 00:04:03,60
So if you're not familiar with css and you see that

75
00:04:03,60 --> 00:04:05,90
I'm always doing classes for the other ones,

76
00:04:05,90 --> 00:04:09,10
this is in elements, so we can actually impact that element

77
00:04:09,10 --> 00:04:13,70
directly and the css in this file is actually,

78
00:04:13,70 --> 00:04:16,80
loaded inside of that particular component.

79
00:04:16,80 --> 00:04:21,00
So there's no problem in just doing an h1 for this.

80
00:04:21,00 --> 00:04:23,10
So this is going to be white and we're going to see it.

81
00:04:23,10 --> 00:04:25,90
OK, so now that we've fixed those things,

82
00:04:25,90 --> 00:04:28,30
lets go take a look at the application,

83
00:04:28,30 --> 00:04:32,90
the final product, and let's reload.

84
00:04:32,90 --> 00:04:35,10
And as you can see once these circles disappear

85
00:04:35,10 --> 00:04:38,50
begins scrolling and then we begin scrolling

86
00:04:38,50 --> 00:04:40,90
and our logo stays fixed on top,

87
00:04:40,90 --> 00:04:44,50
so we can still use that little thing here.

88
00:04:44,50 --> 00:04:46,10
And we need to one more fix

89
00:04:46,10 --> 00:04:47,60
because we are not seeing the menu,

90
00:04:47,60 --> 00:04:50,70
so we need to do a z index on that guy as well.

91
00:04:50,70 --> 00:04:54,40
So lets take a quick look at the menu itself.

92
00:04:54,40 --> 00:04:57,90
Let's just make sure, so go back to app.css,

93
00:04:57,90 --> 00:05:01,70
and let's make sure that our menu is all good.

94
00:05:01,70 --> 00:05:04,80
So we didn't do the z index here so we need do the,

95
00:05:04,80 --> 00:05:09,80
opp, yeah we did, so this is,

96
00:05:09,80 --> 00:05:12,40
should be working fine.

97
00:05:12,40 --> 00:05:15,80
And we just need to do a position fixed here

98
00:05:15,80 --> 00:05:19,90
so lets do the position fixed and that should fix it.

99
00:05:19,90 --> 00:05:23,90
Alright, let's go back here and there you go, perfect.

100
00:05:23,90 --> 00:05:27,40
So, our menu is now showing up wherever we are

101
00:05:27,40 --> 00:05:30,00
in the application, if we click on it,

102
00:05:30,00 --> 00:05:33,50
then we still see the menu on top, which is cool.

103
00:05:33,50 --> 00:05:36,80
And we now have the menu and then we can close the menu

104
00:05:36,80 --> 00:05:39,40
and the logo is still there.

105
00:05:39,40 --> 00:05:42,60
So there you have it, all those simple fixes,

106
00:05:42,60 --> 00:05:45,30
considering this is a simple application,

107
00:05:45,30 --> 00:05:47,00
they will do the trick.

108
00:05:47,00 --> 00:05:48,90
Keep in mind that in more complex

109
00:05:48,90 --> 00:05:50,60
and production ready application,

110
00:05:50,60 --> 00:05:53,00
it might be better to look at fixing the lazy loading

111
00:05:53,00 --> 00:05:56,30
to load the initial components without scrolling

112
00:05:56,30 --> 00:05:59,00
but this is beyond the scope of this course.

