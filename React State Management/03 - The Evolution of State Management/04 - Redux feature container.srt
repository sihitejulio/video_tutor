1
00:00:00,50 --> 00:00:02,60
- [Instructor] Now that we've implemented out boilerplate,

2
00:00:02,60 --> 00:00:05,50
it's time to integrate our existing boilerplate

3
00:00:05,50 --> 00:00:08,20
into our container and our component.

4
00:00:08,20 --> 00:00:11,20
First let's take a look at MainSection.js

5
00:00:11,20 --> 00:00:13,30
in our containers directory.

6
00:00:13,30 --> 00:00:17,10
You can see that we have an empty mapStateToProps function.

7
00:00:17,10 --> 00:00:20,90
What mapStateToProps does is it maps a redux state

8
00:00:20,90 --> 00:00:24,30
to props that get passed down to the component

9
00:00:24,30 --> 00:00:25,80
that it's connected to.

10
00:00:25,80 --> 00:00:28,30
Here, what we're going to want to do is actually

11
00:00:28,30 --> 00:00:31,60
just return the keys that we care about.

12
00:00:31,60 --> 00:00:33,40
Here, that's going to be books,

13
00:00:33,40 --> 00:00:36,40
which is state.books.books

14
00:00:36,40 --> 00:00:38,90
and then completedBooks,

15
00:00:38,90 --> 00:00:41,70
which is state.books.completedBooks.

16
00:00:41,70 --> 00:00:45,20
Now, if you're wondering why were using state.books.books,

17
00:00:45,20 --> 00:00:47,20
remember that in index.js

18
00:00:47,20 --> 00:00:49,20
we have a key here called books

19
00:00:49,20 --> 00:00:51,30
from combineReducers,

20
00:00:51,30 --> 00:00:52,60
and then in addition to that,

21
00:00:52,60 --> 00:00:54,50
we have another key called books here,

22
00:00:54,50 --> 00:00:56,90
from our books reducer.

23
00:00:56,90 --> 00:00:59,40
We only care about this classicBooks array,

24
00:00:59,40 --> 00:01:02,50
which is why we're doing state.books.books.

25
00:01:02,50 --> 00:01:04,30
So now that we're all set here,

26
00:01:04,30 --> 00:01:06,80
it's time to take a look at our component.

27
00:01:06,80 --> 00:01:11,50
So earlier, what we had was everything in local react state.

28
00:01:11,50 --> 00:01:13,80
Now that we're using books and completedBooks

29
00:01:13,80 --> 00:01:15,00
from redux state,

30
00:01:15,00 --> 00:01:17,10
we can get rid of this.

31
00:01:17,10 --> 00:01:18,60
Also keep in mind that we have

32
00:01:18,60 --> 00:01:21,30
this existing updateBookStatus method.

33
00:01:21,30 --> 00:01:23,50
We won't need to use this in redux,

34
00:01:23,50 --> 00:01:25,00
but instead of deleting it,

35
00:01:25,00 --> 00:01:26,20
I'll just go ahead and copy it

36
00:01:26,20 --> 00:01:31,10
and instead name this updateBookStatusRedux.

37
00:01:31,10 --> 00:01:35,10
So let's go ahead and implement updateBookStatusRedux.

38
00:01:35,10 --> 00:01:37,00
It's going to take a book ID,

39
00:01:37,00 --> 00:01:39,10
and what we're going to do is actually just

40
00:01:39,10 --> 00:01:44,60
recall this.props.actions.updateBookStatus

41
00:01:44,60 --> 00:01:46,20
with the book ID.

42
00:01:46,20 --> 00:01:48,10
So, remember that this was defined

43
00:01:48,10 --> 00:01:49,60
in our actions from before,

44
00:01:49,60 --> 00:01:52,00
when we defined updateBookStatus.

45
00:01:52,00 --> 00:01:54,00
The other thing we want to do now is,

46
00:01:54,00 --> 00:01:56,90
because we've eliminated a lot of the redux state

47
00:01:56,90 --> 00:01:59,70
with books and completedBooks,

48
00:01:59,70 --> 00:02:01,20
what we're going to need to do

49
00:02:01,20 --> 00:02:05,80
is replace this.state with this.props.

50
00:02:05,80 --> 00:02:08,00
And what this does is makes sure

51
00:02:08,00 --> 00:02:11,40
that we get the data from the right place.

52
00:02:11,40 --> 00:02:13,90
So let's make sure that we do this everywhere,

53
00:02:13,90 --> 00:02:16,80
except for this.state.showAuthors,

54
00:02:16,80 --> 00:02:20,30
because that is going to stay in local react state.

55
00:02:20,30 --> 00:02:21,60
So now that we've done this,

56
00:02:21,60 --> 00:02:23,50
we have one last thing to do,

57
00:02:23,50 --> 00:02:25,80
and it's looking at BookItem.

58
00:02:25,80 --> 00:02:27,00
The way that we've defined

59
00:02:27,00 --> 00:02:29,00
whether a book is completed or not

60
00:02:29,00 --> 00:02:32,30
is actually just by using the book.completed key

61
00:02:32,30 --> 00:02:34,10
instead of the book.status key.

62
00:02:34,10 --> 00:02:36,30
So let's just go ahead and change that.

63
00:02:36,30 --> 00:02:37,90
Book.completed is a bullion,

64
00:02:37,90 --> 00:02:40,20
so it'll evaluate to true or false

65
00:02:40,20 --> 00:02:42,10
and update accordingly.

66
00:02:42,10 --> 00:02:44,50
Now it's time to take a look at our application.

67
00:02:44,50 --> 00:02:46,00
We see that we're able to add

68
00:02:46,00 --> 00:02:48,90
and remove a book as completed from our list

69
00:02:48,90 --> 00:02:50,70
only using redux.

70
00:02:50,70 --> 00:02:54,30
This shows that we're able to add a feature predictably.

71
00:02:54,30 --> 00:02:56,50
What can be seen as restrictive in design

72
00:02:56,50 --> 00:02:58,30
can also be a good thing.

73
00:02:58,30 --> 00:03:00,20
I know exactly what I need to do

74
00:03:00,20 --> 00:03:02,60
when implementing any feature.

75
00:03:02,60 --> 00:03:03,90
I don't have to think about

76
00:03:03,90 --> 00:03:05,30
where the parent state should live,

77
00:03:05,30 --> 00:03:08,80
whether I need to introduce context or hooks.

78
00:03:08,80 --> 00:03:11,60
I can simply plug into the existing redux architecture

79
00:03:11,60 --> 00:03:13,00
and go.

