1
00:00:00,60 --> 00:00:02,10
- [Instructor] No matter what, you should know

2
00:00:02,10 --> 00:00:05,60
the basics of Redux and why you'd want to use it.

3
00:00:05,60 --> 00:00:08,90
The fact is it'll be around for a long time

4
00:00:08,90 --> 00:00:11,90
and as you work on more complex applications,

5
00:00:11,90 --> 00:00:13,60
it'll become harder to work

6
00:00:13,60 --> 00:00:15,90
with just React State Management.

7
00:00:15,90 --> 00:00:19,00
To this date, I've never worked in a production code base

8
00:00:19,00 --> 00:00:21,40
that doesn't use Redux.

9
00:00:21,40 --> 00:00:24,20
One of the reasons why Redux became so popular

10
00:00:24,20 --> 00:00:28,00
is because there's a standardized way of managing state.

11
00:00:28,00 --> 00:00:30,90
State is always organized in the same pattern,

12
00:00:30,90 --> 00:00:33,00
which means that if you know Redux

13
00:00:33,00 --> 00:00:36,00
you can jump into an entirely new code base

14
00:00:36,00 --> 00:00:38,30
and know how the data's manipulated,

15
00:00:38,30 --> 00:00:41,10
which makes it easier to get started as a new engineer

16
00:00:41,10 --> 00:00:44,00
and makes your code easier to maintain.

17
00:00:44,00 --> 00:00:47,90
Redux is also popular because every time you call an action

18
00:00:47,90 --> 00:00:50,40
you know what the resulting state will be.

19
00:00:50,40 --> 00:00:54,90
It's very predictable, which makes it easier to understand.

20
00:00:54,90 --> 00:00:57,00
Now, let's go through out book application

21
00:00:57,00 --> 00:01:00,30
and use Redux to understand its predictability.

22
00:01:00,30 --> 00:01:02,40
We'll start with the general boilerplate

23
00:01:02,40 --> 00:01:06,00
and implement updating our books status through Redux.

24
00:01:06,00 --> 00:01:07,80
Our boilerplate that we're using now

25
00:01:07,80 --> 00:01:10,30
is different from our last saved state.

26
00:01:10,30 --> 00:01:12,80
Let me show you a couple of things that are different.

27
00:01:12,80 --> 00:01:14,30
First of all, in our reducer,

28
00:01:14,30 --> 00:01:16,40
we now have a books reducer here,

29
00:01:16,40 --> 00:01:19,10
which is different from the default to do reducer

30
00:01:19,10 --> 00:01:21,10
that we were using before.

31
00:01:21,10 --> 00:01:24,30
In addition, I've included this file called books.js,

32
00:01:24,30 --> 00:01:26,40
which has a boilerplate producer

33
00:01:26,40 --> 00:01:29,80
to have us be able to add and edit based on

34
00:01:29,80 --> 00:01:32,00
whatever state that we choose.

35
00:01:32,00 --> 00:01:33,70
Now that we have that in place,

36
00:01:33,70 --> 00:01:36,20
let's go ahead and take a look at our actions.

37
00:01:36,20 --> 00:01:39,60
So, if we take a look at action/index.js,

38
00:01:39,60 --> 00:01:41,80
as you can see here, there are

39
00:01:41,80 --> 00:01:44,80
a bunch of different, existing actions that we can use

40
00:01:44,80 --> 00:01:47,20
to model our own action after.

41
00:01:47,20 --> 00:01:49,40
This is part of the existing boilerplate,

42
00:01:49,40 --> 00:01:51,50
but what we'd like to do is go ahead

43
00:01:51,50 --> 00:01:53,50
and update our book status.

44
00:01:53,50 --> 00:01:56,50
Let's define updateBookStatus.

45
00:01:56,50 --> 00:01:59,40
This is going to take one argument, which is called id

46
00:01:59,40 --> 00:02:01,20
and then what we're going to do is say,

47
00:02:01,20 --> 00:02:05,00
type gets types.update_book.

48
00:02:05,00 --> 00:02:08,00
Now, remember that we haven't actually defined this yet,

49
00:02:08,00 --> 00:02:10,60
we're going to go ahead and do that now.

50
00:02:10,60 --> 00:02:13,10
Let's go ahead and go to ActionTypes.

51
00:02:13,10 --> 00:02:15,50
Like the other action constants, we're going to want

52
00:02:15,50 --> 00:02:17,90
to export one for our actions.

53
00:02:17,90 --> 00:02:22,10
Great, so now types to update book is defined.

54
00:02:22,10 --> 00:02:24,10
Now, what we'd like to do is go ahead

55
00:02:24,10 --> 00:02:25,60
and update our reducer.

56
00:02:25,60 --> 00:02:28,30
Remember that book reducer I showed you earlier?

57
00:02:28,30 --> 00:02:30,00
We have initial state here,

58
00:02:30,00 --> 00:02:31,80
which is pulling from classic books

59
00:02:31,80 --> 00:02:34,50
to populate our initial book status.

60
00:02:34,50 --> 00:02:38,10
We're also passing in an argument called completed books,

61
00:02:38,10 --> 00:02:40,50
which we'll say is zero for now.

62
00:02:40,50 --> 00:02:43,00
You can see here that in the boiler plate,

63
00:02:43,00 --> 00:02:45,80
we have a bunch of different action types that are imported.

64
00:02:45,80 --> 00:02:48,10
In addition to these, we'd like to go ahead

65
00:02:48,10 --> 00:02:50,20
and import update book.

66
00:02:50,20 --> 00:02:51,80
Now that we have our initial state

67
00:02:51,80 --> 00:02:55,90
and action types defined, it's time to define our reducer.

68
00:02:55,90 --> 00:02:59,20
Every time we receive an action of type update book,

69
00:02:59,20 --> 00:03:01,50
we want to take some sort of action.

70
00:03:01,50 --> 00:03:05,30
Now first, let's make sure to return a copy of state,

71
00:03:05,30 --> 00:03:07,80
this make sure that we preserve all of the keys

72
00:03:07,80 --> 00:03:10,60
from initial state and we only modify the keys

73
00:03:10,60 --> 00:03:13,70
that we would like to in the copy of state.

74
00:03:13,70 --> 00:03:16,50
We're not modifying the actual state itself.

75
00:03:16,50 --> 00:03:19,50
The key that we would like to modify is called books.

76
00:03:19,50 --> 00:03:23,70
And so, what we're going to do here is do state.books.map

77
00:03:23,70 --> 00:03:26,90
and remember that map actually returns a copy of the array,

78
00:03:26,90 --> 00:03:28,20
so we don't have to worry

79
00:03:28,20 --> 00:03:30,90
about modifying anything specifically.

80
00:03:30,90 --> 00:03:33,60
Now, let's make sure that we are checking to see

81
00:03:33,60 --> 00:03:35,60
whether the book that we're looking at is the book

82
00:03:35,60 --> 00:03:37,20
that we're looking for.

83
00:03:37,20 --> 00:03:41,20
And if it is, let's go ahead and return a copy of the book

84
00:03:41,20 --> 00:03:43,50
and make sure that the completed property

85
00:03:43,50 --> 00:03:45,70
is the opposite of what it was before.

86
00:03:45,70 --> 00:03:46,80
'Cause this is just a toggle,

87
00:03:46,80 --> 00:03:49,50
so it's pretty easy to manipulate.

88
00:03:49,50 --> 00:03:51,20
If it's not the book we're looking for,

89
00:03:51,20 --> 00:03:53,60
let's just return a copy of that book.

90
00:03:53,60 --> 00:03:55,60
Great, so now we're all set.

91
00:03:55,60 --> 00:03:57,80
Now that we have our action and producer,

92
00:03:57,80 --> 00:03:59,70
in the next video what we're going to do

93
00:03:59,70 --> 00:04:03,00
is implement this in our container and our components.

