1
00:00:00,60 --> 00:00:02,20
- [Instructor] Back on 2015,

2
00:00:02,20 --> 00:00:05,70
when the Flux architecture and Redux was still new,

3
00:00:05,70 --> 00:00:09,60
Dan Abramov, the creator of Redux wrote this.

4
00:00:09,60 --> 00:00:13,20
"It is in React's spirit to constantly question the dogma,

5
00:00:13,20 --> 00:00:17,20
"and we must be careful not to introduce a dogma of our own.

6
00:00:17,20 --> 00:00:20,20
"As a community, we need to teach the beginners

7
00:00:20,20 --> 00:00:22,10
"to keep their eyes wide open

8
00:00:22,10 --> 00:00:23,60
"so that they can recognize it

9
00:00:23,60 --> 00:00:26,70
"when something better than Flux comes along."

10
00:00:26,70 --> 00:00:30,00
At the time, Flux and eventually Redux,

11
00:00:30,00 --> 00:00:33,40
was seen as the way to deal with state management,

12
00:00:33,40 --> 00:00:37,20
and that attitude hasn't really gone away since then.

13
00:00:37,20 --> 00:00:39,30
This idea that there's only one way

14
00:00:39,30 --> 00:00:41,40
to do things is dangerous.

15
00:00:41,40 --> 00:00:43,20
It means that you're not evaluating

16
00:00:43,20 --> 00:00:45,90
the trade-offs of using an alternative system,

17
00:00:45,90 --> 00:00:49,00
and worse, you might be introducing extra complexity

18
00:00:49,00 --> 00:00:51,80
into your app when you don't need it.

19
00:00:51,80 --> 00:00:54,70
Let's take a look at our book application.

20
00:00:54,70 --> 00:00:57,30
It's possible to handle all of our state changes

21
00:00:57,30 --> 00:00:59,30
using React alone.

22
00:00:59,30 --> 00:01:01,60
We don't actually need Redux.

23
00:01:01,60 --> 00:01:03,30
It's tempting to think about adding it

24
00:01:03,30 --> 00:01:06,80
for things like data fetching, like in line 22,

25
00:01:06,80 --> 00:01:09,60
or reusing functions that involve state,

26
00:01:09,60 --> 00:01:10,90
but as it turns out,

27
00:01:10,90 --> 00:01:13,80
we can use React for all of those things.

28
00:01:13,80 --> 00:01:15,80
We're not using local storage

29
00:01:15,80 --> 00:01:17,40
nor do we need to record the state

30
00:01:17,40 --> 00:01:19,60
of our application over time.

31
00:01:19,60 --> 00:01:23,40
We also don't have complex actions like chaining API calls,

32
00:01:23,40 --> 00:01:25,60
which might mean that we would add a library

33
00:01:25,60 --> 00:01:27,20
like Redux Saga.

34
00:01:27,20 --> 00:01:30,30
We don't even have an undo or redo button.

35
00:01:30,30 --> 00:01:33,10
These are all things that Redux can help with greatly

36
00:01:33,10 --> 00:01:35,50
if we wanted those features down the road,

37
00:01:35,50 --> 00:01:37,50
but it isn't worth changing the structure

38
00:01:37,50 --> 00:01:41,60
of our React application for just one of those features.

39
00:01:41,60 --> 00:01:44,80
More so, we're introducing a lot of complexity

40
00:01:44,80 --> 00:01:46,60
by bringing in Redux.

41
00:01:46,60 --> 00:01:49,40
Redux introduces significant boiler plate

42
00:01:49,40 --> 00:01:51,40
because now you have to add actions

43
00:01:51,40 --> 00:01:53,90
and reducers and a store.

44
00:01:53,90 --> 00:01:55,80
You don't have easy access to manipulate state

45
00:01:55,80 --> 00:01:57,70
from your front end.

46
00:01:57,70 --> 00:01:58,90
There's a learning curve.

47
00:01:58,90 --> 00:02:02,40
So if a new engineer joins your team and doesn't know Redux,

48
00:02:02,40 --> 00:02:04,90
that's just one more thing they have to learn

49
00:02:04,90 --> 00:02:07,30
in addition to using React.

50
00:02:07,30 --> 00:02:10,90
I mention these drawbacks not to deter you from using Redux.

51
00:02:10,90 --> 00:02:14,30
It's a great library to use when the need arises.

52
00:02:14,30 --> 00:02:17,50
It's far simpler than state management solutions of the past

53
00:02:17,50 --> 00:02:19,40
and gives you a range of functionality

54
00:02:19,40 --> 00:02:21,00
you wouldn't have without it.

55
00:02:21,00 --> 00:02:23,00
My broader point is this.

56
00:02:23,00 --> 00:02:25,60
Programming is about always using the right tools

57
00:02:25,60 --> 00:02:28,20
for the job and not about using the tools

58
00:02:28,20 --> 00:02:30,10
that are the most familiar.

59
00:02:30,10 --> 00:02:32,40
Think back to the quote from the beginning.

60
00:02:32,40 --> 00:02:34,90
By always using Redux, we shut ourselves

61
00:02:34,90 --> 00:02:38,00
out of choosing the best tool for the job.

