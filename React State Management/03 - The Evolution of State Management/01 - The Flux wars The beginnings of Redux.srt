1
00:00:00,50 --> 00:00:02,90
- [Narrator] What options existed for state management

2
00:00:02,90 --> 00:00:04,50
before Redux?

3
00:00:04,50 --> 00:00:07,10
You might be wondering why asking this question

4
00:00:07,10 --> 00:00:09,50
is important in the first place.

5
00:00:09,50 --> 00:00:12,30
Understanding the history behind why Redux

6
00:00:12,30 --> 00:00:16,00
became so popular, is useful for a couple of reasons.

7
00:00:16,00 --> 00:00:18,20
It helps you understand the pain points

8
00:00:18,20 --> 00:00:21,20
with previous solutions that Redux solved for.

9
00:00:21,20 --> 00:00:23,90
You'll also gain an appreciation for the stability

10
00:00:23,90 --> 00:00:27,40
that Redux provided to end the Flux wars.

11
00:00:27,40 --> 00:00:29,50
Before we get into the Flux wars,

12
00:00:29,50 --> 00:00:32,90
it's important to understand what Flux is.

13
00:00:32,90 --> 00:00:35,20
Developed at Facebook, Flux introduced

14
00:00:35,20 --> 00:00:36,90
a pattern for state management

15
00:00:36,90 --> 00:00:39,40
that wasn't used widely before.

16
00:00:39,40 --> 00:00:41,80
The most popular alternative at the time

17
00:00:41,80 --> 00:00:46,00
was angular js, which used by directional data binding.

18
00:00:46,00 --> 00:00:49,60
While this was great to keep the view and model in sync,

19
00:00:49,60 --> 00:00:51,80
it often slowed down applications,

20
00:00:51,80 --> 00:00:54,80
and was trickier to understand conceptually.

21
00:00:54,80 --> 00:00:58,20
Flux specifies one-directional data binding,

22
00:00:58,20 --> 00:01:00,40
that means that data always flows

23
00:01:00,40 --> 00:01:02,70
from the view to the model.

24
00:01:02,70 --> 00:01:05,30
This is easier to grasp if you're starting out

25
00:01:05,30 --> 00:01:09,20
as you never have to guess which way the data is flowing.

26
00:01:09,20 --> 00:01:11,70
The downside is that there are situations

27
00:01:11,70 --> 00:01:15,50
where the model and view might not be in sync.

28
00:01:15,50 --> 00:01:18,30
Flux came into existence because state management

29
00:01:18,30 --> 00:01:22,20
was too difficult to scale in complex react applications

30
00:01:22,20 --> 00:01:24,50
with the react library at the time.

31
00:01:24,50 --> 00:01:27,20
This was before context and hooks.

32
00:01:27,20 --> 00:01:28,80
If you're familiar with Redux,

33
00:01:28,80 --> 00:01:32,60
the specifications of Flux will sound pretty familiar.

34
00:01:32,60 --> 00:01:35,30
In Flux, there's a dispatcher in stores,

35
00:01:35,30 --> 00:01:37,10
and actions which indicate

36
00:01:37,10 --> 00:01:39,50
that the store needs to be changed.

37
00:01:39,50 --> 00:01:40,90
Note that unlike Redux,

38
00:01:40,90 --> 00:01:44,20
you can have multiple stores in Flux.

39
00:01:44,20 --> 00:01:47,60
More than a library, Flux is a pattern specification

40
00:01:47,60 --> 00:01:49,20
for state management.

41
00:01:49,20 --> 00:01:51,70
It's not a library like Redux,

42
00:01:51,70 --> 00:01:54,10
rather you can create a library

43
00:01:54,10 --> 00:01:56,90
that implements what Flux specifies.

44
00:01:56,90 --> 00:02:00,00
Think of it like an interface in a programming language.

45
00:02:00,00 --> 00:02:02,50
You can program a class that implements the methods

46
00:02:02,50 --> 00:02:06,90
in your interface but you can't use the interface by itself.

47
00:02:06,90 --> 00:02:10,30
Now that we understand a little bit more about Flux,

48
00:02:10,30 --> 00:02:13,00
let's dive into why the state management landscape

49
00:02:13,00 --> 00:02:14,40
became messy.

50
00:02:14,40 --> 00:02:16,60
Remember that we said that Flux is a pattern

51
00:02:16,60 --> 00:02:18,60
not a library?

52
00:02:18,60 --> 00:02:20,20
This meant that all of a sudden,

53
00:02:20,20 --> 00:02:23,80
we had many implementations of Flux to choose between.

54
00:02:23,80 --> 00:02:25,80
They all had slight differences,

55
00:02:25,80 --> 00:02:28,70
like the APIs used to communicate between the views

56
00:02:28,70 --> 00:02:33,20
and the state, support for non-react views, etc.

57
00:02:33,20 --> 00:02:36,10
If you're a developer in 2015 trying to choose

58
00:02:36,10 --> 00:02:38,90
between these libraries, it's difficult.

59
00:02:38,90 --> 00:02:41,10
You're choosing between minor differences,

60
00:02:41,10 --> 00:02:44,80
trying to choose a library that's well supported, etc.

61
00:02:44,80 --> 00:02:47,50
Not to mention that there were new implementations

62
00:02:47,50 --> 00:02:50,80
that were released almost every week.

63
00:02:50,80 --> 00:02:53,70
Dan Abramov's implementation of Flux,

64
00:02:53,70 --> 00:02:57,70
which is called Redux, effectively ended the Flux wars.

65
00:02:57,70 --> 00:03:00,00
If you're interested in reading about his thoughts

66
00:03:00,00 --> 00:03:03,40
on Flux back when Redux was still an experiment,

67
00:03:03,40 --> 00:03:06,80
I highly recommend checking out this media article.

68
00:03:06,80 --> 00:03:08,80
What separated Redux from the pack

69
00:03:08,80 --> 00:03:11,40
were a few different things.

70
00:03:11,40 --> 00:03:14,30
One, Redux's store is immutable,

71
00:03:14,30 --> 00:03:17,00
meaning that any time we change the store,

72
00:03:17,00 --> 00:03:21,70
we create a new copy of the store instead of modifying it.

73
00:03:21,70 --> 00:03:23,80
This unlocks features like replaying

74
00:03:23,80 --> 00:03:25,50
what our application did,

75
00:03:25,50 --> 00:03:28,50
and undo and redo functionality.

76
00:03:28,50 --> 00:03:31,60
Two, stores and actions are pure functions,

77
00:03:31,60 --> 00:03:35,20
meaning that they're easier to test and reason about.

78
00:03:35,20 --> 00:03:37,90
There aren't any side effects when you dispatch an action,

79
00:03:37,90 --> 00:03:40,40
so your application becomes more predictable

80
00:03:40,40 --> 00:03:42,30
and easier to work with.

81
00:03:42,30 --> 00:03:45,70
Lastly, there's only one store in Redux applications,

82
00:03:45,70 --> 00:03:48,20
and therefore just one source of truth,

83
00:03:48,20 --> 00:03:51,30
further simplifying state management.

84
00:03:51,30 --> 00:03:54,90
Redux provided a simple answer for state management,

85
00:03:54,90 --> 00:03:57,60
although it quickly became the de facto solution

86
00:03:57,60 --> 00:04:01,00
without considering alternatives, like using react.

