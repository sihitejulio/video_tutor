1
00:00:00,60 --> 00:00:02,10
- [Instructor] While the focus of this course

2
00:00:02,10 --> 00:00:06,00
is on React and Redux as solutions for state management,

3
00:00:06,00 --> 00:00:08,10
we wouldn't be able to complete a survey

4
00:00:08,10 --> 00:00:11,80
on state management solutions without looking at MobX.

5
00:00:11,80 --> 00:00:14,70
It takes a fundamentally different approach from Redux,

6
00:00:14,70 --> 00:00:18,60
and, as always, comes with its own set of trade offs.

7
00:00:18,60 --> 00:00:20,60
It's worth clarifying from the get go

8
00:00:20,60 --> 00:00:24,20
that MobX is not a full alternative to Redux.

9
00:00:24,20 --> 00:00:26,50
It gives you the ability to manage state,

10
00:00:26,50 --> 00:00:28,60
but you should still organize your code

11
00:00:28,60 --> 00:00:31,60
into some form of stores and controllers.

12
00:00:31,60 --> 00:00:34,40
Benefit to using MobX is that you're not bound

13
00:00:34,40 --> 00:00:36,70
to the exact organizational structure

14
00:00:36,70 --> 00:00:40,10
of using actions and producers, which gives you flexibility

15
00:00:40,10 --> 00:00:43,00
to make different architectural choices.

16
00:00:43,00 --> 00:00:45,80
MobX has similar terms to Redux.

17
00:00:45,80 --> 00:00:48,80
You can see here we still have one central store,

18
00:00:48,80 --> 00:00:52,10
and there are still actions which can mutate state.

19
00:00:52,10 --> 00:00:54,50
Notice here that I said mutate.

20
00:00:54,50 --> 00:00:58,60
Immutability isn't the preferred practice for using MobX.

21
00:00:58,60 --> 00:01:00,80
We're used to thinking of application state

22
00:01:00,80 --> 00:01:02,50
as a json object.

23
00:01:02,50 --> 00:01:05,20
In the case of MobX, it's also really useful

24
00:01:05,20 --> 00:01:08,40
to think of state as a spreadsheet with derivations

25
00:01:08,40 --> 00:01:11,10
and reactions which can go ahead and update

26
00:01:11,10 --> 00:01:13,20
different cell values.

27
00:01:13,20 --> 00:01:15,60
First let's talk about the concept of derivation,

28
00:01:15,60 --> 00:01:18,40
which is any value that can be computed automatically

29
00:01:18,40 --> 00:01:19,80
from your state.

30
00:01:19,80 --> 00:01:22,60
In a pure React and Redux implementation,

31
00:01:22,60 --> 00:01:25,80
you could imagine writing a small function which recomputes

32
00:01:25,80 --> 00:01:28,80
maybe the number of books that we have left in our album,

33
00:01:28,80 --> 00:01:30,50
or in the example here,

34
00:01:30,50 --> 00:01:33,50
perhaps how many to do's are completed.

35
00:01:33,50 --> 00:01:36,20
In MobX these derivations are applied

36
00:01:36,20 --> 00:01:38,70
to the quote unquote cell automatically

37
00:01:38,70 --> 00:01:41,50
so that you don't have to rewrite these small functions

38
00:01:41,50 --> 00:01:43,90
in our React components.

39
00:01:43,90 --> 00:01:45,90
How do you call a derivation?

40
00:01:45,90 --> 00:01:49,40
You use something called a decorator.

41
00:01:49,40 --> 00:01:51,10
This is likely familiar to you

42
00:01:51,10 --> 00:01:54,70
if you're coming from Java, Python, or Ruby.

43
00:01:54,70 --> 00:01:58,20
Decorators allow behavior to be added on to an object

44
00:01:58,20 --> 00:02:00,60
without effecting other objects.

45
00:02:00,60 --> 00:02:03,70
Here we'll want to use the computed decorator

46
00:02:03,70 --> 00:02:06,60
to indicate that we can automatically compute things,

47
00:02:06,60 --> 00:02:09,40
like getting the number of books in our book collection,

48
00:02:09,40 --> 00:02:11,40
or the number of to do's.

49
00:02:11,40 --> 00:02:15,50
We'll use the observer decorator down here

50
00:02:15,50 --> 00:02:18,70
for things like the observable properties in our store.

51
00:02:18,70 --> 00:02:21,20
This indicates to MobX that these values

52
00:02:21,20 --> 00:02:22,80
will change over time,

53
00:02:22,80 --> 00:02:26,60
compared to Redux providing a new state every time.

54
00:02:26,60 --> 00:02:29,70
To me, MobX feels a little like magic

55
00:02:29,70 --> 00:02:32,80
even though I know what's going on under the hood.

56
00:02:32,80 --> 00:02:35,60
When you use the solution, you don't need to call things

57
00:02:35,60 --> 00:02:37,70
like the stop set state anymore,

58
00:02:37,70 --> 00:02:40,60
and you don't have a need for higher order components

59
00:02:40,60 --> 00:02:43,90
to connect the right parts of the Redux storage react.

60
00:02:43,90 --> 00:02:46,60
It's a pretty simplified solution.

61
00:02:46,60 --> 00:02:48,70
The trade off here though is that there are

62
00:02:48,70 --> 00:02:51,20
reported difficulties getting MobX to work

63
00:02:51,20 --> 00:02:53,20
in complex applications.

64
00:02:53,20 --> 00:02:55,90
Although admittedly, those reports come from people

65
00:02:55,90 --> 00:02:59,10
who have used only Redux for a long time.

66
00:02:59,10 --> 00:03:01,90
If you're comfortable with object-oriented programming,

67
00:03:01,90 --> 00:03:04,20
MobX will feel pretty familiar to you,

68
00:03:04,20 --> 00:03:06,90
and will feel like the right way of managing state.

69
00:03:06,90 --> 00:03:08,60
It's not as popular.

70
00:03:08,60 --> 00:03:11,50
To date I haven't worked in any code bases with it,

71
00:03:11,50 --> 00:03:13,90
but I have heard of other developers using it

72
00:03:13,90 --> 00:03:16,20
in their production code bases.

73
00:03:16,20 --> 00:03:17,50
But you don't have to deal with

74
00:03:17,50 --> 00:03:20,00
the functional programming curve that Redux has,

75
00:03:20,00 --> 00:03:22,50
like immutability, impure functions,

76
00:03:22,50 --> 00:03:24,90
and there's much less boilerplate.

77
00:03:24,90 --> 00:03:27,20
With that said, I'd urge some caution

78
00:03:27,20 --> 00:03:29,10
before going the MobX route.

79
00:03:29,10 --> 00:03:32,30
Because Redux has a strict way of organizing code,

80
00:03:32,30 --> 00:03:34,80
it's easier to architecture applications.

81
00:03:34,80 --> 00:03:36,50
If you're looking to start a project

82
00:03:36,50 --> 00:03:40,90
or a small to mid-size project, MobX is a great solution.

83
00:03:40,90 --> 00:03:42,60
Once your application starts to grow

84
00:03:42,60 --> 00:03:44,80
in size and complexity however,

85
00:03:44,80 --> 00:03:47,60
make sure you have an architectural plan in place

86
00:03:47,60 --> 00:03:48,80
to make sure you're using

87
00:03:48,80 --> 00:03:51,00
software engineering best practices.

