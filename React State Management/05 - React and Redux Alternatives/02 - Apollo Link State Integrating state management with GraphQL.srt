1
00:00:00,50 --> 00:00:02,30
- [Narrator] A great alternative to Redux

2
00:00:02,30 --> 00:00:03,90
or React State Management

3
00:00:03,90 --> 00:00:08,20
if your code base is using GraphQL is to use Apollo Client.

4
00:00:08,20 --> 00:00:10,70
The nice thing about using GraphQL is that

5
00:00:10,70 --> 00:00:13,70
when you're making a call to the server to get your data,

6
00:00:13,70 --> 00:00:15,60
you can call it in exactly the shape

7
00:00:15,60 --> 00:00:17,10
you'd like to receive it.

8
00:00:17,10 --> 00:00:21,00
As an example, let's take a look at the Google Books API.

9
00:00:21,00 --> 00:00:23,70
If we call the API directly, we can see here

10
00:00:23,70 --> 00:00:26,30
that we get a ton of information.

11
00:00:26,30 --> 00:00:28,60
We don't need all of this information,

12
00:00:28,60 --> 00:00:31,30
nor do we need it in exactly this shape.

13
00:00:31,30 --> 00:00:35,80
Let's take a look now at an Apollo GraphQL example response.

14
00:00:35,80 --> 00:00:38,80
You can see here that we can get exactly the items

15
00:00:38,80 --> 00:00:42,00
that we need in exactly the shape that we'd like it.

16
00:00:42,00 --> 00:00:45,30
If you think about it, a lot of what state management does

17
00:00:45,30 --> 00:00:47,80
is call various endpoints to get data

18
00:00:47,80 --> 00:00:50,80
and then manipulate that data to fit a certain shape

19
00:00:50,80 --> 00:00:53,00
to provide to React components.

20
00:00:53,00 --> 00:00:55,70
Other things like color changes on hovering,

21
00:00:55,70 --> 00:00:57,60
modals opening and closing,

22
00:00:57,60 --> 00:01:01,40
are all well-suited for React state.

23
00:01:01,40 --> 00:01:03,00
What does this mean?

24
00:01:03,00 --> 00:01:06,80
If we're requesting data in exactly the shape we want,

25
00:01:06,80 --> 00:01:08,90
we don't really have much of a need

26
00:01:08,90 --> 00:01:11,90
for a state management library, do we?

27
00:01:11,90 --> 00:01:14,90
Maybe you do, especially if you're handling things

28
00:01:14,90 --> 00:01:17,30
like persisting state with local storage,

29
00:01:17,30 --> 00:01:19,70
offline synching, et cetera.

30
00:01:19,70 --> 00:01:21,70
But if your application doesn't have a need

31
00:01:21,70 --> 00:01:24,10
for those things, GraphQL can produce

32
00:01:24,10 --> 00:01:27,80
a lot of state management pins.

33
00:01:27,80 --> 00:01:29,60
Of course, this doesn't apply

34
00:01:29,60 --> 00:01:32,10
if you're working with a REST API.

35
00:01:32,10 --> 00:01:34,50
You'd need to be building a GraphQL API

36
00:01:34,50 --> 00:01:38,40
on the backend as well in order to be able to use this.

37
00:01:38,40 --> 00:01:40,80
But if using GraphQL is an option,

38
00:01:40,80 --> 00:01:43,30
I'd strongly consider going this route.

39
00:01:43,30 --> 00:01:45,40
I've seen teams adopt this approach,

40
00:01:45,40 --> 00:01:47,50
and as a result were able to remove

41
00:01:47,50 --> 00:01:50,60
a ton of Redux code from their application.

42
00:01:50,60 --> 00:01:53,40
We're not going to set up GraphQL for our particular

43
00:01:53,40 --> 00:01:56,60
book application, just because the Google APIs

44
00:01:56,60 --> 00:01:59,80
we're working with are REST APIs.

45
00:01:59,80 --> 00:02:02,00
Even if we can't implement it though,

46
00:02:02,00 --> 00:02:05,30
I hope this implementation of state management helps you

47
00:02:05,30 --> 00:02:09,10
think about what a lot of state management libraries do,

48
00:02:09,10 --> 00:02:12,80
and where the bottlenecks are for React applications.

49
00:02:12,80 --> 00:02:16,00
What GraphQL made clear was that there's a lot of code

50
00:02:16,00 --> 00:02:20,30
dedicated to problems that the backend can solve.

51
00:02:20,30 --> 00:02:22,00
If you have the chance to advocate

52
00:02:22,00 --> 00:02:23,90
for a state management library,

53
00:02:23,90 --> 00:02:26,40
bring up these trade-offs of using different kinds

54
00:02:26,40 --> 00:02:29,60
of specifications in the backend API.

55
00:02:29,60 --> 00:02:31,20
It may or may not make a difference

56
00:02:31,20 --> 00:02:34,20
to the backend engineers implementing the API,

57
00:02:34,20 --> 00:02:36,10
but it could potentially save you

58
00:02:36,10 --> 00:02:38,40
a lot of headache down the road,

59
00:02:38,40 --> 00:02:40,40
and for any other state management needs

60
00:02:40,40 --> 00:02:42,40
you might have, you can always use

61
00:02:42,40 --> 00:02:45,00
the Robust React state management techniques.

