1
00:00:00,50 --> 00:00:02,50
- [Instructor] Immutability is one of the greatest

2
00:00:02,50 --> 00:00:03,80
draws of Redux.

3
00:00:03,80 --> 00:00:06,10
It's what allows us to easily implement

4
00:00:06,10 --> 00:00:07,70
undo and redo functionality

5
00:00:07,70 --> 00:00:10,20
and what allows us to replay our app

6
00:00:10,20 --> 00:00:12,50
through the course of a user experience.

7
00:00:12,50 --> 00:00:15,00
Immutability in programming means that

8
00:00:15,00 --> 00:00:17,70
we don't change something after it's created.

9
00:00:17,70 --> 00:00:19,50
With regards to Redux,

10
00:00:19,50 --> 00:00:22,30
it means we never mutate state directly.

11
00:00:22,30 --> 00:00:25,00
For example, if we wanted to change state

12
00:00:25,00 --> 00:00:27,80
we wouldn't manipulate a property directly.

13
00:00:27,80 --> 00:00:32,20
We'd instead return a new state with the modified property.

14
00:00:32,20 --> 00:00:34,60
This also means that you can't use functions

15
00:00:34,60 --> 00:00:36,80
like pop or push directly

16
00:00:36,80 --> 00:00:39,40
when updating an array, for example.

17
00:00:39,40 --> 00:00:42,20
You always need to extend the current object.

18
00:00:42,20 --> 00:00:45,50
For example, using the ES6 spread operator.

19
00:00:45,50 --> 00:00:49,80
And remember, this applies to both React and Redux state.

20
00:00:49,80 --> 00:00:53,80
Immutability is what allows us to undo and redo.

21
00:00:53,80 --> 00:00:56,20
If we ever want to undo an action,

22
00:00:56,20 --> 00:00:57,90
store all versions of the state

23
00:00:57,90 --> 00:01:00,60
and return the previous version of the state

24
00:01:00,60 --> 00:01:02,10
before the current one.

25
00:01:02,10 --> 00:01:05,00
It's also what makes Redux predictable.

26
00:01:05,00 --> 00:01:08,60
Using immutability ensures that there are no side effects.

27
00:01:08,60 --> 00:01:10,10
By never mutating the object,

28
00:01:10,10 --> 00:01:12,90
we won't see any unintended consequences

29
00:01:12,90 --> 00:01:14,50
when we change state.

30
00:01:14,50 --> 00:01:17,10
Keep in mind that React does this too.

31
00:01:17,10 --> 00:01:21,10
This is why we never modify state directly in React.

32
00:01:21,10 --> 00:01:22,90
We always call setState,

33
00:01:22,90 --> 00:01:26,30
which returns a new state object under the hood.

34
00:01:26,30 --> 00:01:30,00
Returning a new state every time can be costly though

35
00:01:30,00 --> 00:01:33,00
and it's one of the drawbacks of using Redux.

36
00:01:33,00 --> 00:01:34,40
There's a lot of overhead

37
00:01:34,40 --> 00:01:37,60
because every time we create a new state object,

38
00:01:37,60 --> 00:01:40,50
we're copying over information we already have

39
00:01:40,50 --> 00:01:42,60
which can be redundant.

40
00:01:42,60 --> 00:01:45,10
Redux uses shallow equality checking

41
00:01:45,10 --> 00:01:46,70
to be able to check quickly

42
00:01:46,70 --> 00:01:49,70
whether a new state object needs to be returned or not,

43
00:01:49,70 --> 00:01:51,80
but there are other libraries that can help

44
00:01:51,80 --> 00:01:54,20
with performance, too.

45
00:01:54,20 --> 00:01:56,80
Immutable.JS is a popular library

46
00:01:56,80 --> 00:01:59,00
that helps improve performance.

47
00:01:59,00 --> 00:02:01,10
As always, there are trade offs.

48
00:02:01,10 --> 00:02:02,80
Let's examine further.

49
00:02:02,80 --> 00:02:05,90
Immutable.JS guarantees immutability.

50
00:02:05,90 --> 00:02:07,80
This is a huge benefit.

51
00:02:07,80 --> 00:02:11,00
You never have to wonder whether you accidentally

52
00:02:11,00 --> 00:02:12,20
tried to modify state

53
00:02:12,20 --> 00:02:15,40
or if there's an unintended side effect.

54
00:02:15,40 --> 00:02:18,30
This is especially common with arrays in JavaScript

55
00:02:18,30 --> 00:02:22,40
since some methods can return a new array and others don't.

56
00:02:22,40 --> 00:02:24,60
There are significant drawbacks though

57
00:02:24,60 --> 00:02:26,20
and the biggest one is that

58
00:02:26,20 --> 00:02:28,50
it's hard to interact with Immutable.

59
00:02:28,50 --> 00:02:31,20
What I mean by that is that you can't really mix

60
00:02:31,20 --> 00:02:33,30
immutable objects like this

61
00:02:33,30 --> 00:02:36,50
with regular JavaScript objects, like this.

62
00:02:36,50 --> 00:02:39,00
You're either all in or you're all out.

63
00:02:39,00 --> 00:02:42,20
There is a Two.js method which provides a way to interrupt,

64
00:02:42,20 --> 00:02:45,00
but at the cost of performance.

65
00:02:45,00 --> 00:02:48,40
With all that said, the Redux documentation does say

66
00:02:48,40 --> 00:02:51,50
that Immutable.JS is worth the effort.

67
00:02:51,50 --> 00:02:53,40
The documentation mentions

68
00:02:53,40 --> 00:02:55,20
the various trade offs to consider,

69
00:02:55,20 --> 00:02:58,70
particularly around the difficulty of debugging issues

70
00:02:58,70 --> 00:03:01,20
in your app related to immutability.

71
00:03:01,20 --> 00:03:04,50
Often the issue is that we accidentally mutate properties

72
00:03:04,50 --> 00:03:07,90
which is avoided by using Immutable.JS.

73
00:03:07,90 --> 00:03:10,00
This, in addition to better performance,

74
00:03:10,00 --> 00:03:12,50
is why the Redux team believes using it

75
00:03:12,50 --> 00:03:14,60
is worth the effort.

76
00:03:14,60 --> 00:03:19,00
My personal opinion is derived from my anecdotal experience.

77
00:03:19,00 --> 00:03:20,80
I haven't worked in any code base

78
00:03:20,80 --> 00:03:22,70
which uses Immutable.JS.

79
00:03:22,70 --> 00:03:25,80
I'm sure the trade off of immutability is worth it.

80
00:03:25,80 --> 00:03:29,50
However, the fact that I'd have to be all in on using it

81
00:03:29,50 --> 00:03:31,40
makes it a draw back for me

82
00:03:31,40 --> 00:03:34,30
as I'd have to refactor the whole code base I'm working in

83
00:03:34,30 --> 00:03:36,90
and I don't often get to build code bases

84
00:03:36,90 --> 00:03:38,80
from scratch at work.

85
00:03:38,80 --> 00:03:40,40
With that said, it's worth considering if

86
00:03:40,40 --> 00:03:42,40
you are building something from scratch

87
00:03:42,40 --> 00:03:46,00
to get all of the right benefits from Redux.

