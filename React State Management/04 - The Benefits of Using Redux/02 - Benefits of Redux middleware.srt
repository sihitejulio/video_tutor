1
00:00:00,50 --> 00:00:02,20
- [Instructor] When you first introduce Redux

2
00:00:02,20 --> 00:00:05,30
into your app, it makes sense to think of only actions

3
00:00:05,30 --> 00:00:07,30
and reducers to start with.

4
00:00:07,30 --> 00:00:09,90
However, there's a powerful concept of middleware

5
00:00:09,90 --> 00:00:12,30
which is very well suited to Redux

6
00:00:12,30 --> 00:00:15,30
and which makes Redux a good state management solution

7
00:00:15,30 --> 00:00:18,10
if you plan to utilize a lot of middleware.

8
00:00:18,10 --> 00:00:20,20
So what is middleware?

9
00:00:20,20 --> 00:00:23,00
Middleware is a way to call third-party endpoints

10
00:00:23,00 --> 00:00:25,60
that happens after you dispatch an action

11
00:00:25,60 --> 00:00:28,40
and before the action reaches the reducer.

12
00:00:28,40 --> 00:00:31,80
It's mostly commonly used for use cases like logging,

13
00:00:31,80 --> 00:00:34,50
calling an API, et cetera.

14
00:00:34,50 --> 00:00:36,20
Let's take the use case of logging

15
00:00:36,20 --> 00:00:38,90
since it can be really helpful when debugging.

16
00:00:38,90 --> 00:00:41,40
My first instinct when debugging Redux is

17
00:00:41,40 --> 00:00:45,40
to console.log actions, doing something like this.

18
00:00:45,40 --> 00:00:47,40
So I would console.log to say like action fired

19
00:00:47,40 --> 00:00:50,70
and then I would say console.log like id

20
00:00:50,70 --> 00:00:52,70
and so I would do stuff like this

21
00:00:52,70 --> 00:00:56,80
and it kind of works but then the other issue

22
00:00:56,80 --> 00:01:00,30
is that I'd also have to console.log in my reducer

23
00:01:00,30 --> 00:01:02,00
so let's take a look here.

24
00:01:02,00 --> 00:01:05,80
I would have to console.log reached reducer,

25
00:01:05,80 --> 00:01:09,70
maybe the state or maybe the action

26
00:01:09,70 --> 00:01:12,00
so I would be doing all kinds of things here

27
00:01:12,00 --> 00:01:13,70
which kind of gets cumbersome

28
00:01:13,70 --> 00:01:16,00
if you're doing it from multiple actions.

29
00:01:16,00 --> 00:01:19,00
This is where middleware can be really helpful.

30
00:01:19,00 --> 00:01:22,80
So, to write middleware, realize that it's just a function.

31
00:01:22,80 --> 00:01:25,00
It's often written as a curried function

32
00:01:25,00 --> 00:01:28,10
which is another way of saying it takes multiple arguments,

33
00:01:28,10 --> 00:01:30,30
one argument at a time.

34
00:01:30,30 --> 00:01:33,50
The first argument is the store, the second argument

35
00:01:33,50 --> 00:01:37,60
is the next function and the third argument is the action.

36
00:01:37,60 --> 00:01:40,80
The first argument is simply the redux store,

37
00:01:40,80 --> 00:01:42,70
the next function tells redux

38
00:01:42,70 --> 00:01:45,20
to continue processing the next middleware

39
00:01:45,20 --> 00:01:47,90
and that we're done doing whatever it was that we planned

40
00:01:47,90 --> 00:01:49,60
to do with the middleware.

41
00:01:49,60 --> 00:01:52,70
The last argument is the action which will then be sent

42
00:01:52,70 --> 00:01:54,30
onto the reducer.

43
00:01:54,30 --> 00:01:56,90
Once you're doing executing your middleware task,

44
00:01:56,90 --> 00:01:59,70
you call next on the action which determines

45
00:01:59,70 --> 00:02:01,70
whether more middleware is run

46
00:02:01,70 --> 00:02:04,90
or whether the application will move on to the reducer.

47
00:02:04,90 --> 00:02:06,90
Let's try putting it into action.

48
00:02:06,90 --> 00:02:08,90
Similar to how we did for context,

49
00:02:08,90 --> 00:02:10,90
we're going to want to create a new folder

50
00:02:10,90 --> 00:02:12,80
here called middleware

51
00:02:12,80 --> 00:02:14,50
and in this middleware folder

52
00:02:14,50 --> 00:02:17,40
let's create a loggerMiddleware file.

53
00:02:17,40 --> 00:02:19,60
Remember that our middleware is just a function

54
00:02:19,60 --> 00:02:22,60
so let's go ahead and get started by writing a function

55
00:02:22,60 --> 00:02:25,60
that's similar to the one that we saw before,

56
00:02:25,60 --> 00:02:28,60
const logger gets store okay then we're going

57
00:02:28,60 --> 00:02:31,20
to have a next argument and then we're going

58
00:02:31,20 --> 00:02:32,90
to have an action argument

59
00:02:32,90 --> 00:02:34,90
and what we want to do first is we want

60
00:02:34,90 --> 00:02:37,40
to console.log dispatching

61
00:02:37,40 --> 00:02:39,40
and remember that we have access to the action

62
00:02:39,40 --> 00:02:41,50
so we can say dispatching action.

63
00:02:41,50 --> 00:02:42,40
Great.

64
00:02:42,40 --> 00:02:45,40
We'd also like to store our state

65
00:02:45,40 --> 00:02:47,50
after the action is executed.

66
00:02:47,50 --> 00:02:51,20
In order to do that, instead of just calling next on action

67
00:02:51,20 --> 00:02:53,10
we're going to store that in a variable.

68
00:02:53,10 --> 00:02:56,30
So, let result get next on action.

69
00:02:56,30 --> 00:02:57,30
Perfect.

70
00:02:57,30 --> 00:02:58,50
Now that we have this here,

71
00:02:58,50 --> 00:03:02,50
we can now console.log the next state.

72
00:03:02,50 --> 00:03:06,80
Perfect so store.getState and then now that we have this

73
00:03:06,80 --> 00:03:08,60
we can return result which is really just

74
00:03:08,60 --> 00:03:11,40
next calling action.

75
00:03:11,40 --> 00:03:14,60
Now that we have state before and after calling the action

76
00:03:14,60 --> 00:03:17,20
we can return result which is really next

77
00:03:17,20 --> 00:03:19,70
with the argument of action.

78
00:03:19,70 --> 00:03:23,00
Now every time we execute an action we get a log

79
00:03:23,00 --> 00:03:24,80
of how the state has changed,

80
00:03:24,80 --> 00:03:28,10
pretty useful and not that difficult to do.

81
00:03:28,10 --> 00:03:30,70
Another use case which doesn't quite make sense

82
00:03:30,70 --> 00:03:33,70
for this app but is used all the time in production

83
00:03:33,70 --> 00:03:36,80
is calling an external analytics endpoint.

84
00:03:36,80 --> 00:03:39,80
For example, at a former company I used to work at

85
00:03:39,80 --> 00:03:42,80
we needed to track user clicks for every button.

86
00:03:42,80 --> 00:03:45,10
We implemented this in our react components

87
00:03:45,10 --> 00:03:47,30
by simply calling an API endpoint

88
00:03:47,30 --> 00:03:49,30
so something like this

89
00:03:49,30 --> 00:03:53,00
and this would happen every time we fired an on click event.

90
00:03:53,00 --> 00:03:55,70
Now let's think about how middleware can make this problem

91
00:03:55,70 --> 00:03:57,50
a lot easier.

92
00:03:57,50 --> 00:03:59,70
What if for every action we were able

93
00:03:59,70 --> 00:04:01,90
to call our analytics API?

94
00:04:01,90 --> 00:04:03,50
It would save us a lot of code

95
00:04:03,50 --> 00:04:06,00
and we wouldn't be co mingling presentation

96
00:04:06,00 --> 00:04:07,50
and business logic.

97
00:04:07,50 --> 00:04:10,30
Now our logger example here we can just replace

98
00:04:10,30 --> 00:04:13,80
these console logs with calling an analytics endpoint

99
00:04:13,80 --> 00:04:16,50
and we can even use the action name as an argument

100
00:04:16,50 --> 00:04:18,80
to identify which button was pressed.

101
00:04:18,80 --> 00:04:22,10
After that, we can call next and then just move on

102
00:04:22,10 --> 00:04:24,20
and we have an analytics tracker

103
00:04:24,20 --> 00:04:25,60
so as you can see here,

104
00:04:25,60 --> 00:04:28,30
middleware is a powerful tool for state management,

105
00:04:28,30 --> 00:04:30,60
to clean up your code and separate your concerns

106
00:04:30,60 --> 00:04:32,00
a lot more easily.

