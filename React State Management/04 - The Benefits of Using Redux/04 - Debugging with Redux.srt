1
00:00:01,00 --> 00:00:02,80
- [Instructor] Redux's immutability and one-way

2
00:00:02,80 --> 00:00:05,10
data flow approach to state management

3
00:00:05,10 --> 00:00:07,90
makes it relatively nice to debug.

4
00:00:07,90 --> 00:00:10,50
In applications with two-way data binding,

5
00:00:10,50 --> 00:00:13,00
it can be hard to find the source of an issue

6
00:00:13,00 --> 00:00:16,00
where your state isn't what you think it should be.

7
00:00:16,00 --> 00:00:17,80
Because of Redux's structure,

8
00:00:17,80 --> 00:00:20,70
there are powerful tools that we can use to debug

9
00:00:20,70 --> 00:00:23,40
that go far beyond console logging.

10
00:00:23,40 --> 00:00:25,30
First, let's make sure that we have

11
00:00:25,30 --> 00:00:27,50
Redux DevTools installed.

12
00:00:27,50 --> 00:00:28,90
So, let's go here,

13
00:00:28,90 --> 00:00:31,40
and let's search for Redux DevTools.

14
00:00:31,40 --> 00:00:33,60
You can see that I already have it installed,

15
00:00:33,60 --> 00:00:35,10
but what you're going to want to do

16
00:00:35,10 --> 00:00:38,00
is add it to your Chrome browser.

17
00:00:38,00 --> 00:00:40,30
Next what we're going to do is we're going to

18
00:00:40,30 --> 00:00:42,10
take a look at the Redux DevTools

19
00:00:42,10 --> 00:00:44,70
extension repository on GitHub.

20
00:00:44,70 --> 00:00:46,80
You can see here that there are instructions

21
00:00:46,80 --> 00:00:49,40
for every browser if you're not using Chrome.

22
00:00:49,40 --> 00:00:51,60
But since we are, let's go ahead and take a look

23
00:00:51,60 --> 00:00:53,90
at the installation instructions.

24
00:00:53,90 --> 00:00:56,80
First, we went ahead and installed the extension

25
00:00:56,80 --> 00:00:58,70
from the Chrome web store.

26
00:00:58,70 --> 00:01:00,50
Now what we'll need to do is,

27
00:01:00,50 --> 00:01:03,30
because we have a basic Redux store,

28
00:01:03,30 --> 00:01:05,50
we're going to go ahead and modify our

29
00:01:05,50 --> 00:01:09,20
create store method to take this extra parameter.

30
00:01:09,20 --> 00:01:12,50
Let's copy and paste this and go into our code.

31
00:01:12,50 --> 00:01:14,20
You can see here that I have open

32
00:01:14,20 --> 00:01:16,80
source slash index dot js.

33
00:01:16,80 --> 00:01:20,00
We have here this create store with a reducer argument.

34
00:01:20,00 --> 00:01:21,90
Now we're going to go ahead and go into

35
00:01:21,90 --> 00:01:24,80
our create store method to add our line.

36
00:01:24,80 --> 00:01:27,90
Let's go ahead and put reducer on a separate line here

37
00:01:27,90 --> 00:01:32,70
and then create an extra line for our pasted line here.

38
00:01:32,70 --> 00:01:33,60
Perfect.

39
00:01:33,60 --> 00:01:36,30
Now that we have the Redux DevTools extension,

40
00:01:36,30 --> 00:01:40,20
our browser knows to load up the store when it reloads.

41
00:01:40,20 --> 00:01:42,00
Now let's go to our app.

42
00:01:42,00 --> 00:01:44,80
Now let's take a look at our book application.

43
00:01:44,80 --> 00:01:47,80
We're going to go ahead and open up Developer Tools.

44
00:01:47,80 --> 00:01:49,10
You have two options.

45
00:01:49,10 --> 00:01:51,50
You can go into the View, Developer,

46
00:01:51,50 --> 00:01:53,20
and then Inspect Elements tab,

47
00:01:53,20 --> 00:01:55,10
or Developer Tools tab,

48
00:01:55,10 --> 00:01:57,70
or you can hit command + option I.

49
00:01:57,70 --> 00:02:00,20
Now that we have our developer tab open,

50
00:02:00,20 --> 00:02:02,70
let's go ahead and click on the Redux tab.

51
00:02:02,70 --> 00:02:04,80
Remember, if you don't have this open,

52
00:02:04,80 --> 00:02:07,50
you will probably have to restart Chrome.

53
00:02:07,50 --> 00:02:09,30
Now that we have this up and running,

54
00:02:09,30 --> 00:02:11,00
let's take a look and see what are

55
00:02:11,00 --> 00:02:12,40
our different options.

56
00:02:12,40 --> 00:02:14,70
We're able to see our log of the actions

57
00:02:14,70 --> 00:02:16,60
that we've dispatched here.

58
00:02:16,60 --> 00:02:18,70
We're able to see how our state has changed

59
00:02:18,70 --> 00:02:20,40
here in this Diff tab.

60
00:02:20,40 --> 00:02:22,10
And here, we'll get to this part,

61
00:02:22,10 --> 00:02:24,30
but you have the option to play through your

62
00:02:24,30 --> 00:02:27,20
actions as you execute them in the app.

63
00:02:27,20 --> 00:02:29,50
So let's go ahead and see what this looks like.

64
00:02:29,50 --> 00:02:32,00
Let's click on "A Tale of Two Cities"

65
00:02:32,00 --> 00:02:33,50
and "War and Peace."

66
00:02:33,50 --> 00:02:36,40
You can see here that we update a book twice.

67
00:02:36,40 --> 00:02:38,70
And each time, you can see what the state

68
00:02:38,70 --> 00:02:40,40
has changed to.

69
00:02:40,40 --> 00:02:42,10
Every time we click on a book,

70
00:02:42,10 --> 00:02:44,00
we're marking it as completed.

71
00:02:44,00 --> 00:02:46,80
You can see that here because the state has changed

72
00:02:46,80 --> 00:02:49,20
from false to true.

73
00:02:49,20 --> 00:02:51,20
So let's suppose we click on this,

74
00:02:51,20 --> 00:02:53,90
and maybe we unclick on "Frankenstein."

75
00:02:53,90 --> 00:02:57,40
Now we want to see how our state has changed over time.

76
00:02:57,40 --> 00:03:00,40
You have the option now to play your app

77
00:03:00,40 --> 00:03:03,50
from the beginning when you loaded the page.

78
00:03:03,50 --> 00:03:05,10
Let's see what that looks like.

79
00:03:05,10 --> 00:03:08,30
You can see here that you can go into each action,

80
00:03:08,30 --> 00:03:11,90
one by one, and see how the state has changed over time.

81
00:03:11,90 --> 00:03:13,70
This is great for debugging.

82
00:03:13,70 --> 00:03:16,20
You're able to see exactly where the state

83
00:03:16,20 --> 00:03:19,30
isn't changing as you would expect.

84
00:03:19,30 --> 00:03:22,50
This concept of time travel debugging is really cool.

85
00:03:22,50 --> 00:03:25,30
It means that you can replay your application

86
00:03:25,30 --> 00:03:27,40
and you don't have to change your code,

87
00:03:27,40 --> 00:03:29,40
or even reload the page.

88
00:03:29,40 --> 00:03:32,20
Using the Redux DevTools is an industry standard,

89
00:03:32,20 --> 00:03:34,30
and I highly recommend that you use it

90
00:03:34,30 --> 00:03:37,00
when you're debugging your Redux application.

