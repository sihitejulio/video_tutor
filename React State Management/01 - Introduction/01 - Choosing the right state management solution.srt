1
00:00:01,00 --> 00:00:03,50
- When writing code, it's our job as developers

2
00:00:03,50 --> 00:00:05,70
to think in terms of trade-offs.

3
00:00:05,70 --> 00:00:08,00
With React and Redux, it's often hard

4
00:00:08,00 --> 00:00:09,70
to distill what those trade-offs will

5
00:00:09,70 --> 00:00:13,00
be when introducing a state management package.

6
00:00:13,00 --> 00:00:14,80
In any application, you'll need

7
00:00:14,80 --> 00:00:16,60
to introduce state management.

8
00:00:16,60 --> 00:00:18,90
Choosing the right approach is the difference between

9
00:00:18,90 --> 00:00:22,20
having unmaintainable, difficult-to-comprehend code

10
00:00:22,20 --> 00:00:25,40
versus code that is easy to navigate and update.

11
00:00:25,40 --> 00:00:27,70
In this course, we'll delve into the trade-offs

12
00:00:27,70 --> 00:00:30,20
of using various state management solutions,

13
00:00:30,20 --> 00:00:31,90
and common issues that occur

14
00:00:31,90 --> 00:00:34,80
as your React application grows.

15
00:00:34,80 --> 00:00:37,00
We'll start with a simple book application

16
00:00:37,00 --> 00:00:39,50
and modify it to use different techniques.

17
00:00:39,50 --> 00:00:41,40
I'll also challenge you to think beyond

18
00:00:41,40 --> 00:00:44,80
the necessity of keeping a single, global application state.

19
00:00:44,80 --> 00:00:48,50
And to use React APIs, you might not be as familiar with,

20
00:00:48,50 --> 00:00:51,40
such as the context API and hooks.

21
00:00:51,40 --> 00:00:54,50
I'll also cover concepts like immutability and middleware

22
00:00:54,50 --> 00:00:56,30
and provide helpful debugging techniques

23
00:00:56,30 --> 00:00:58,00
for Redux applications.

24
00:00:58,00 --> 00:01:00,40
Hi, I'm Sravanti Tekumalla.

25
00:01:00,40 --> 00:01:01,90
I'm a front-end software engineer

26
00:01:01,90 --> 00:01:03,60
who specializes in React.

27
00:01:03,60 --> 00:01:05,70
Join me in my LinkedIn Learning course

28
00:01:05,70 --> 00:01:09,00
about state management for React applications.

