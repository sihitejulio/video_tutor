1
00:00:01,00 --> 00:00:03,30
- One of the most important decisions you'll make

2
00:00:03,30 --> 00:00:06,10
when deciding how to architect your React application

3
00:00:06,10 --> 00:00:09,60
is figuring out which state management solution to use.

4
00:00:09,60 --> 00:00:12,10
As with other aspects of the JavaScript communities,

5
00:00:12,10 --> 00:00:15,80
there are constantly new libraries and solutions popping up.

6
00:00:15,80 --> 00:00:19,70
But the focus here is to discuss the most popular options,

7
00:00:19,70 --> 00:00:25,40
React, Redux, MobX and Apollo GraphQL.

8
00:00:25,40 --> 00:00:27,40
Redux is the most popular option

9
00:00:27,40 --> 00:00:30,70
and emphasizes one way data flow and immutability.

10
00:00:30,70 --> 00:00:33,70
What that means is that every time you change state,

11
00:00:33,70 --> 00:00:35,60
for example, you change some information

12
00:00:35,60 --> 00:00:39,00
in your user profile, what's returned to React is a copy

13
00:00:39,00 --> 00:00:41,50
of the old state with just the new thing changed,

14
00:00:41,50 --> 00:00:43,60
not simply the modification.

15
00:00:43,60 --> 00:00:46,20
This might sound redundant or wasteful,

16
00:00:46,20 --> 00:00:48,60
but there are some great benefits that include undo

17
00:00:48,60 --> 00:00:51,60
and redo functionality and better debuggability

18
00:00:51,60 --> 00:00:54,70
which we'll cover more in depth later in the course.

19
00:00:54,70 --> 00:00:57,60
MobX appeals to those with knowledge and background

20
00:00:57,60 --> 00:00:59,60
in object oriented programming.

21
00:00:59,60 --> 00:01:02,40
It uses the observer pattern which allows the system

22
00:01:02,40 --> 00:01:05,50
to build a dependency tree among different parts of state

23
00:01:05,50 --> 00:01:08,00
and produces free rendering of components

24
00:01:08,00 --> 00:01:11,00
by just keeping track of what needs to be mutated.

25
00:01:11,00 --> 00:01:13,20
Lastly, the React team has invested

26
00:01:13,20 --> 00:01:15,80
in state management solutions which means you don't have

27
00:01:15,80 --> 00:01:18,50
to turn to another state management library.

28
00:01:18,50 --> 00:01:21,00
We're going to focus on React and Redux.

29
00:01:21,00 --> 00:01:22,90
There's a lot that you can do with React

30
00:01:22,90 --> 00:01:24,60
and Redux state management

31
00:01:24,60 --> 00:01:27,60
that will help you reduce the complexity of your app.

32
00:01:27,60 --> 00:01:29,90
As you can see, there is lots of options

33
00:01:29,90 --> 00:01:31,50
when it comes to state management.

34
00:01:31,50 --> 00:01:34,00
They all have different ideologies

35
00:01:34,00 --> 00:01:35,70
but it's important to keep in mind

36
00:01:35,70 --> 00:01:37,40
that you can create a maintainable,

37
00:01:37,40 --> 00:01:42,00
readable React application with any of the above approaches.

