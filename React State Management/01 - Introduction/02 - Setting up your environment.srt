1
00:00:00,50 --> 00:00:02,40
- [Instructor] To get the most out of this course,

2
00:00:02,40 --> 00:00:04,50
you should be familiar with the fundamentals

3
00:00:04,50 --> 00:00:06,30
of React and Redux.

4
00:00:06,30 --> 00:00:09,10
I'll be setting up my Redux environment on a Mac,

5
00:00:09,10 --> 00:00:10,30
but everything I show you

6
00:00:10,30 --> 00:00:13,50
will also work on a PC or on Linux.

7
00:00:13,50 --> 00:00:15,80
As a prerequisite, make sure that you have

8
00:00:15,80 --> 00:00:18,10
at least npm 8 installed.

9
00:00:18,10 --> 00:00:19,70
If you don't have it installed,

10
00:00:19,70 --> 00:00:22,40
you can install it on the npm website.

11
00:00:22,40 --> 00:00:25,20
Let's get started by setting up our environment.

12
00:00:25,20 --> 00:00:28,60
I've provided exercise files for you to follow along with.

13
00:00:28,60 --> 00:00:30,50
I've downloaded them to the desktop,

14
00:00:30,50 --> 00:00:33,40
but you can download them to wherever you'd like.

15
00:00:33,40 --> 00:00:35,80
We'll be working with the simple book application

16
00:00:35,80 --> 00:00:38,10
that allows you to mark books as red.

17
00:00:38,10 --> 00:00:42,80
This is the modification of the Redux To-Do MVC example.

18
00:00:42,80 --> 00:00:44,30
First, from the command line,

19
00:00:44,30 --> 00:00:51,30
navigate to the first exercise file to do MVC 01-03 Start.

20
00:00:51,30 --> 00:00:52,10
Perfect.

21
00:00:52,10 --> 00:00:55,60
Now let's run npm install or npm i.

22
00:00:55,60 --> 00:00:58,70
This will install all of your dependencies.

23
00:00:58,70 --> 00:01:01,40
This may take a few minutes, so definitely be patient.

24
00:01:01,40 --> 00:01:02,30
Great.

25
00:01:02,30 --> 00:01:03,80
You might see this error saying

26
00:01:03,80 --> 00:01:06,20
that there's low security vulnerabilities.

27
00:01:06,20 --> 00:01:07,50
Don't worry about that.

28
00:01:07,50 --> 00:01:10,00
Let's go ahead and run npm start.

29
00:01:10,00 --> 00:01:12,30
This will start up our application

30
00:01:12,30 --> 00:01:15,70
and now it will navigate to local host 3000.

31
00:01:15,70 --> 00:01:17,40
If you set it up successfully,

32
00:01:17,40 --> 00:01:20,00
you should see a list of books like this.

33
00:01:20,00 --> 00:01:20,80
Awesome.

34
00:01:20,80 --> 00:01:22,60
You're all set and ready to go.

35
00:01:22,60 --> 00:01:25,40
Next, let's install a couple of browser extensions

36
00:01:25,40 --> 00:01:26,80
that will help with debugging

37
00:01:26,80 --> 00:01:29,10
and understanding our code better.

38
00:01:29,10 --> 00:01:31,90
First, let's install React Development Tools.

39
00:01:31,90 --> 00:01:34,30
Using the search for React Developer Tools

40
00:01:34,30 --> 00:01:36,20
in the Chrome web store.

41
00:01:36,20 --> 00:01:38,80
This allows you to click into React components

42
00:01:38,80 --> 00:01:42,70
and see which props and state belong to which components.

43
00:01:42,70 --> 00:01:45,30
We'll be working with Redux later in the course.

44
00:01:45,30 --> 00:01:47,20
To understand how state changes

45
00:01:47,20 --> 00:01:48,90
throughout the use of our application,

46
00:01:48,90 --> 00:01:52,20
we want to be able to also install Redux Dev Tools,

47
00:01:52,20 --> 00:01:54,90
which is similar to React Dev Tools.

48
00:01:54,90 --> 00:01:57,20
For example, now we can see exactly

49
00:01:57,20 --> 00:01:59,10
which actions are triggered when we click

50
00:01:59,10 --> 00:02:01,80
on a button and what the new state is.

51
00:02:01,80 --> 00:02:04,80
This will come in super handy when debugging.

52
00:02:04,80 --> 00:02:05,90
We'll go more into depth

53
00:02:05,90 --> 00:02:08,30
using these tools later in the course.

54
00:02:08,30 --> 00:02:10,40
For now, installing them and understanding

55
00:02:10,40 --> 00:02:13,00
what they're used for is good enough.

