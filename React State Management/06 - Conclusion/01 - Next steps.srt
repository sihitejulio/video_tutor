1
00:00:00,60 --> 00:00:01,90
- Awesome.

2
00:00:01,90 --> 00:00:04,90
We've explored different approaches to state management.

3
00:00:04,90 --> 00:00:07,40
If you're looking for a more in-depth course on Redux,

4
00:00:07,40 --> 00:00:10,20
check out Learning Redux in our library.

5
00:00:10,20 --> 00:00:13,20
We also have courses on Apollo and GraphQL.

6
00:00:13,20 --> 00:00:16,70
Lastly, I'd recommend trying to extend the functionality

7
00:00:16,70 --> 00:00:18,00
of our book application

8
00:00:18,00 --> 00:00:19,50
to practice when to apply

9
00:00:19,50 --> 00:00:21,60
different kinds of state management.

10
00:00:21,60 --> 00:00:24,80
Try adding user profiles or filtering functionality.

11
00:00:24,80 --> 00:00:27,10
When would you use React state?

12
00:00:27,10 --> 00:00:28,70
When would you use Redux?

13
00:00:28,70 --> 00:00:30,90
In cases where you would use Redux,

14
00:00:30,90 --> 00:00:32,40
try implementing the feature

15
00:00:32,40 --> 00:00:35,80
using both Redux and React Context and Hooks.

16
00:00:35,80 --> 00:00:38,20
You can also keep up with me on Twitter,

17
00:00:38,20 --> 00:00:40,70
or on my blog, sravanti.dev

18
00:00:40,70 --> 00:00:42,40
where I talk about web development.

19
00:00:42,40 --> 00:00:44,00
Thanks so much for watching.

