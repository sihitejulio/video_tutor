1
00:00:00,60 --> 00:00:02,10
- [Instructor] One of the most common ways

2
00:00:02,10 --> 00:00:04,30
to introduce complexity into your app

3
00:00:04,30 --> 00:00:06,80
is by introducing an API call.

4
00:00:06,80 --> 00:00:08,80
Where should that API call live?

5
00:00:08,80 --> 00:00:11,80
What should we do with that data once we have it?

6
00:00:11,80 --> 00:00:14,50
It's easy to jump to a more complex solution

7
00:00:14,50 --> 00:00:18,30
that involves Redux, setting up Boilerplate, et cetera.

8
00:00:18,30 --> 00:00:19,80
In lots of causes though,

9
00:00:19,80 --> 00:00:23,70
using the built-in React methods suffice just fine.

10
00:00:23,70 --> 00:00:27,10
Let's take a look at our book collection example.

11
00:00:27,10 --> 00:00:30,40
Suppose we don't want to use a hard-coded set of books,

12
00:00:30,40 --> 00:00:33,90
and maybe we want to re-read Jane Austen books.

13
00:00:33,90 --> 00:00:35,00
In order to that,

14
00:00:35,00 --> 00:00:38,20
we're going to be using the Google Books API.

15
00:00:38,20 --> 00:00:39,90
Let's take a look specifically

16
00:00:39,90 --> 00:00:42,80
at the performing a search section.

17
00:00:42,80 --> 00:00:46,00
We can use the Google Books API to dynamically load

18
00:00:46,00 --> 00:00:47,80
the books for a given author

19
00:00:47,80 --> 00:00:50,40
using this inauthor query string.

20
00:00:50,40 --> 00:00:52,80
What we want is we want for these books

21
00:00:52,80 --> 00:00:54,90
to dynamically appear for the users

22
00:00:54,90 --> 00:00:57,70
when they first load the page.

23
00:00:57,70 --> 00:01:00,40
But which React life cycle should we look at?

24
00:01:00,40 --> 00:01:02,40
There are a couple of different options.

25
00:01:02,40 --> 00:01:04,60
The first one is componentWillMount,

26
00:01:04,60 --> 00:01:07,10
and the second one is componentDidMount.

27
00:01:07,10 --> 00:01:09,30
I recommend using componentWillMount

28
00:01:09,30 --> 00:01:11,60
since that avoids an extra render

29
00:01:11,60 --> 00:01:14,00
after you request your data.

30
00:01:14,00 --> 00:01:16,30
Once we have our data from the API,

31
00:01:16,30 --> 00:01:18,80
we only want to keep what's relevant.

32
00:01:18,80 --> 00:01:20,60
We'll need to map through the results

33
00:01:20,60 --> 00:01:23,10
and update our state accordingly.

34
00:01:23,10 --> 00:01:24,00
As a side note,

35
00:01:24,00 --> 00:01:26,40
it's important to know what your data will look like

36
00:01:26,40 --> 00:01:29,00
before you start building out your components.

37
00:01:29,00 --> 00:01:31,80
I recommend playing with the request and response

38
00:01:31,80 --> 00:01:35,00
so that you know what the expected response is.

39
00:01:35,00 --> 00:01:37,40
For example, you'll see here that we have

40
00:01:37,40 --> 00:01:41,60
this kind of books, total items, and then the actual items.

41
00:01:41,60 --> 00:01:44,50
Here we'll only need the items part

42
00:01:44,50 --> 00:01:46,80
because that's what contains all of the information

43
00:01:46,80 --> 00:01:48,10
about the books.

44
00:01:48,10 --> 00:01:51,70
We don't really care about the total items or the kind.

45
00:01:51,70 --> 00:01:53,90
Now that we know what our data will look like,

46
00:01:53,90 --> 00:01:56,40
let's go ahead and make our API call

47
00:01:56,40 --> 00:01:58,40
using componentWillMount.

48
00:01:58,40 --> 00:02:00,70
Before we fill this in it's important to know

49
00:02:00,70 --> 00:02:03,10
how we're going to make our API request.

50
00:02:03,10 --> 00:02:04,40
In order to that,

51
00:02:04,40 --> 00:02:07,00
we'll use this API called Fetch.

52
00:02:07,00 --> 00:02:09,20
Fetch is a simple API

53
00:02:09,20 --> 00:02:12,70
and all it does is it takes a query string

54
00:02:12,70 --> 00:02:15,50
and then it returns a promise.

55
00:02:15,50 --> 00:02:18,90
Remember that a promise doesn't return data immediately.

56
00:02:18,90 --> 00:02:21,80
We'll have to resolve that promise ourselves.

57
00:02:21,80 --> 00:02:23,70
Now going into the app,

58
00:02:23,70 --> 00:02:26,50
let's go ahead and implement Fetch.

59
00:02:26,50 --> 00:02:29,00
Let's use our query string from before

60
00:02:29,00 --> 00:02:31,30
and then using the documentation as a guide,

61
00:02:31,30 --> 00:02:35,00
we will go ahead and resolve the promise.

62
00:02:35,00 --> 00:02:36,70
Now once we have this,

63
00:02:36,70 --> 00:02:38,50
we're going to have myData

64
00:02:38,50 --> 00:02:40,60
and you can call this any variable you want,

65
00:02:40,60 --> 00:02:43,10
I just chose myData arbitrarily.

66
00:02:43,10 --> 00:02:46,10
Remember that we'll only want myData .items

67
00:02:46,10 --> 00:02:48,70
since we don't care about the other keys.

68
00:02:48,70 --> 00:02:51,30
And also remember that we're going to want to update

69
00:02:51,30 --> 00:02:53,80
this .state .books.

70
00:02:53,80 --> 00:02:56,50
Now, right now books has classicBooks,

71
00:02:56,50 --> 00:02:59,40
but we'll actually really want this to be an empty array

72
00:02:59,40 --> 00:03:02,50
that we then populate with books from the API.

73
00:03:02,50 --> 00:03:05,00
So instead if having classic books,

74
00:03:05,00 --> 00:03:08,20
what we'll do is we'll say this.setState

75
00:03:08,20 --> 00:03:11,30
and then books get myData.items.

76
00:03:11,30 --> 00:03:15,70
Now what happens is that this hard-coded array here

77
00:03:15,70 --> 00:03:19,40
is replaced with whatever our Google API returns.

78
00:03:19,40 --> 00:03:22,10
Let's make sure that this returns what we expect.

79
00:03:22,10 --> 00:03:22,90
Perfect!

80
00:03:22,90 --> 00:03:25,80
Now we have a set of different Jane Austen books.

81
00:03:25,80 --> 00:03:28,00
You'll notice that there's some might not belong

82
00:03:28,00 --> 00:03:30,10
but that's just due to the Google API,

83
00:03:30,10 --> 00:03:32,20
so don't worry about it too much.

84
00:03:32,20 --> 00:03:33,40
We're all set.

85
00:03:33,40 --> 00:03:36,40
You can see how doable it is to make API calls

86
00:03:36,40 --> 00:03:39,30
in meta state and React components alone.

87
00:03:39,30 --> 00:03:40,60
As always, however,

88
00:03:40,60 --> 00:03:43,30
I want us to think about the trade-offs here.

89
00:03:43,30 --> 00:03:46,40
This worked out great for a single API call.

90
00:03:46,40 --> 00:03:47,40
You could imagine though

91
00:03:47,40 --> 00:03:50,20
that if we wanted to chain multi API calls,

92
00:03:50,20 --> 00:03:52,10
that would get difficult to read.

93
00:03:52,10 --> 00:03:53,50
Think about testing,

94
00:03:53,50 --> 00:03:56,10
it's harder to test a component with an API call

95
00:03:56,10 --> 00:03:58,90
since you'll need to mock the API in your test,

96
00:03:58,90 --> 00:04:02,20
which adds another level of complexity to your unit test.

97
00:04:02,20 --> 00:04:04,30
If those complexities aren't worth keeping

98
00:04:04,30 --> 00:04:05,90
your API calls in React,

99
00:04:05,90 --> 00:04:07,80
then it's time to consider a package

100
00:04:07,80 --> 00:04:10,20
like Redux Thunk or Redux Saga's,

101
00:04:10,20 --> 00:04:13,00
which can help manage complex API calls

102
00:04:13,00 --> 00:04:16,00
and keep your applications state in one place.

