1
00:00:00,50 --> 00:00:03,50
- [Presenter] A new feature to manage state is React Hooks.

2
00:00:03,50 --> 00:00:07,20
It was recently added in React 16.8.

3
00:00:07,20 --> 00:00:08,60
To explore this more,

4
00:00:08,60 --> 00:00:10,70
let's take a look at our footer component

5
00:00:10,70 --> 00:00:12,80
in our application.

6
00:00:12,80 --> 00:00:15,40
Now, suppose in addition to the number

7
00:00:15,40 --> 00:00:16,90
of books that we've read,

8
00:00:16,90 --> 00:00:19,30
that we'd like to add a goal of the number

9
00:00:19,30 --> 00:00:21,30
of books we'd like to read.

10
00:00:21,30 --> 00:00:23,90
Let's go ahead and modify our footer

11
00:00:23,90 --> 00:00:25,90
with a couple of buttons that'll allow us

12
00:00:25,90 --> 00:00:28,60
to increment and decrement our goal number

13
00:00:28,60 --> 00:00:30,30
of books that we'd like to read.

14
00:00:30,30 --> 00:00:32,90
I'm going to go ahead an do that now.

15
00:00:32,90 --> 00:00:35,10
So instead of Toggle Authors,

16
00:00:35,10 --> 00:00:37,10
this is going to be Increment.

17
00:00:37,10 --> 00:00:39,20
And instead of Toggle Authors here,

18
00:00:39,20 --> 00:00:40,90
we're going to say Decrement.

19
00:00:40,90 --> 00:00:43,80
Now let's not worry about the setState for now.

20
00:00:43,80 --> 00:00:45,20
We'll come back to it.

21
00:00:45,20 --> 00:00:47,10
With our understanding of React,

22
00:00:47,10 --> 00:00:48,40
it would make sense that,

23
00:00:48,40 --> 00:00:51,20
in order to keep track of a counter state,

24
00:00:51,20 --> 00:00:52,70
that we would want to go ahead

25
00:00:52,70 --> 00:00:54,80
and refactor this component.

26
00:00:54,80 --> 00:00:56,10
If we were to do that,

27
00:00:56,10 --> 00:00:58,50
it would probably look something like this.

28
00:00:58,50 --> 00:01:01,00
Class footer extends component.

29
00:01:01,00 --> 00:01:03,60
And then we'd probably do something like

30
00:01:03,60 --> 00:01:05,50
have a constructor.

31
00:01:05,50 --> 00:01:07,10
And then in addition to that,

32
00:01:07,10 --> 00:01:09,10
we'd probably have a render method.

33
00:01:09,10 --> 00:01:11,80
And so instead of doing all of that,

34
00:01:11,80 --> 00:01:15,70
what React Hooks allows us to do is to use state

35
00:01:15,70 --> 00:01:18,40
without introducing a class.

36
00:01:18,40 --> 00:01:21,40
Typically, what you would like to do is

37
00:01:21,40 --> 00:01:23,20
refactor your component.

38
00:01:23,20 --> 00:01:25,20
Instead now you can use Hooks

39
00:01:25,20 --> 00:01:27,50
to manage this counter state.

40
00:01:27,50 --> 00:01:30,30
So let's go ahead and implement this.

41
00:01:30,30 --> 00:01:32,00
So when we import React,

42
00:01:32,00 --> 00:01:34,70
let's go ahead and also import useState.

43
00:01:34,70 --> 00:01:37,40
This is the hook that we're going to be using.

44
00:01:37,40 --> 00:01:40,40
Now here we're going to have our hook.

45
00:01:40,40 --> 00:01:42,80
So for this, let's go ahead and say

46
00:01:42,80 --> 00:01:46,80
const, goalBooks, updateGoalBooks

47
00:01:46,80 --> 00:01:48,90
equals useState zero.

48
00:01:48,90 --> 00:01:50,70
Let's go through what each of these mean.

49
00:01:50,70 --> 00:01:53,90
GoalBooks is our equivalent of state.

50
00:01:53,90 --> 00:01:56,40
So state.goalBooks would be the equivalent

51
00:01:56,40 --> 00:01:58,30
in a class component.

52
00:01:58,30 --> 00:02:01,30
UpdateGoalBooks is what updates our state.

53
00:02:01,30 --> 00:02:05,10
So that's the equivalent of calling this .setState.

54
00:02:05,10 --> 00:02:07,10
And here, when we're saying zero,

55
00:02:07,10 --> 00:02:08,80
we're just initializing the state.

56
00:02:08,80 --> 00:02:12,60
So let's just say we're starting off with zero goalBooks.

57
00:02:12,60 --> 00:02:14,70
Now we want to make sure to integrate this

58
00:02:14,70 --> 00:02:16,70
into the component that we have.

59
00:02:16,70 --> 00:02:20,30
So here we have number of books left to read,

60
00:02:20,30 --> 00:02:24,00
and let's just say out of goalBooks.

61
00:02:24,00 --> 00:02:26,70
So out of goalBooks books.

62
00:02:26,70 --> 00:02:28,80
So now in our button,

63
00:02:28,80 --> 00:02:31,60
in order to be able to change that number,

64
00:02:31,60 --> 00:02:34,20
we're going to replace this .setState.

65
00:02:34,20 --> 00:02:36,30
So instead of this .setState,

66
00:02:36,30 --> 00:02:38,70
we're going to say updateGoalBooks

67
00:02:38,70 --> 00:02:43,80
with the number of goalBooks plus one to Increment.

68
00:02:43,80 --> 00:02:46,40
Similarly we are going to do the same thing

69
00:02:46,40 --> 00:02:48,70
with this .setState here.

70
00:02:48,70 --> 00:02:52,60
And instead of incrementing, we're going to decrement.

71
00:02:52,60 --> 00:02:55,60
Now here what we see is that

72
00:02:55,60 --> 00:03:00,10
we're able to basically call the equivalent of setState.

73
00:03:00,10 --> 00:03:02,00
In addition to this, what I'm going to do

74
00:03:02,00 --> 00:03:04,40
is I'm just going to quickly move this footer up

75
00:03:04,40 --> 00:03:08,00
so that it's easier to see on the screen.

76
00:03:08,00 --> 00:03:08,80
Great!

77
00:03:08,80 --> 00:03:11,30
Now let's see what this looks like in our application.

78
00:03:11,30 --> 00:03:14,30
So we have ten books left to read out of zero books.

79
00:03:14,30 --> 00:03:17,10
We can increment it here, and decrement it.

80
00:03:17,10 --> 00:03:18,00
Perfect!

81
00:03:18,00 --> 00:03:20,00
This is exactly like using state

82
00:03:20,00 --> 00:03:23,00
without actually introducing state in our component.

