1
00:00:00,50 --> 00:00:02,50
- [Instructor] If you've used React before,

2
00:00:02,50 --> 00:00:05,10
it's pretty likely you've used state.

3
00:00:05,10 --> 00:00:06,80
It's what allows you to keep track

4
00:00:06,80 --> 00:00:09,40
of changing variables in your application

5
00:00:09,40 --> 00:00:12,90
that determine how your component renders behaves.

6
00:00:12,90 --> 00:00:15,30
That's a lot, so let's dig deeper

7
00:00:15,30 --> 00:00:17,60
into what React state can accomplish,

8
00:00:17,60 --> 00:00:21,20
and why you would want to use a pure React solution

9
00:00:21,20 --> 00:00:24,60
instead of introducing an external state management library

10
00:00:24,60 --> 00:00:26,60
such as Redux.

11
00:00:26,60 --> 00:00:28,90
What's confusing about the React world,

12
00:00:28,90 --> 00:00:31,50
is that we've overloaded the word state.

13
00:00:31,50 --> 00:00:36,10
We use it to mean both component and application state.

14
00:00:36,10 --> 00:00:39,10
I'll clarify which kind of state I'm talking about

15
00:00:39,10 --> 00:00:42,70
by using React state when talking about component state,

16
00:00:42,70 --> 00:00:44,30
and I'll say Redux state

17
00:00:44,30 --> 00:00:47,60
when talking about application state.

18
00:00:47,60 --> 00:00:50,80
Before we get started talking about React state,

19
00:00:50,80 --> 00:00:53,30
I recommend reading Dan Abramov's post

20
00:00:53,30 --> 00:00:56,00
on why you might not need Redux.

21
00:00:56,00 --> 00:00:58,50
It's not about Redux as much as it is

22
00:00:58,50 --> 00:01:01,00
about the power of React state.

23
00:01:01,00 --> 00:01:03,00
In particular, he points out

24
00:01:03,00 --> 00:01:06,50
that we already have a very powerful tool in React

25
00:01:06,50 --> 00:01:08,70
to do things like keep track of a counter

26
00:01:08,70 --> 00:01:10,90
or even make an API call.

27
00:01:10,90 --> 00:01:13,30
In fact, if that's the bulk of what you're doing,

28
00:01:13,30 --> 00:01:16,80
you can get away with using React state management solution

29
00:01:16,80 --> 00:01:19,60
without ever introducing Redux.

30
00:01:19,60 --> 00:01:23,50
Let's tale a look at our simple book list application.

31
00:01:23,50 --> 00:01:25,80
For an action such as allowing the user

32
00:01:25,80 --> 00:01:29,50
to mark books as read or unread in their collection,

33
00:01:29,50 --> 00:01:31,90
we actually don't have to use Redux.

34
00:01:31,90 --> 00:01:34,10
We can introduce methods that allow us

35
00:01:34,10 --> 00:01:37,70
to store a user's collection of books in local state.

36
00:01:37,70 --> 00:01:41,50
Now, let's take a look at our code in VS Code.

37
00:01:41,50 --> 00:01:44,00
Our methods to add and remove books,

38
00:01:44,00 --> 00:01:46,70
on line 18, manipulates local state

39
00:01:46,70 --> 00:01:50,60
with the updated collection causing the app to re-render.

40
00:01:50,60 --> 00:01:51,80
We don't need to worry

41
00:01:51,80 --> 00:01:54,10
about creating a Redux specific action,

42
00:01:54,10 --> 00:01:56,70
and keeping the books in a global Redux state

43
00:01:56,70 --> 00:01:58,60
since React is doing all of that work

44
00:01:58,60 --> 00:02:02,00
for us in local state here on line 11.

45
00:02:02,00 --> 00:02:04,10
This works great for our needs.

46
00:02:04,10 --> 00:02:07,00
We can render any manipulation to the user's collection

47
00:02:07,00 --> 00:02:09,20
of books, and even store those books

48
00:02:09,20 --> 00:02:12,70
in a database using an API call down the road.

49
00:02:12,70 --> 00:02:14,30
One important thing to think about,

50
00:02:14,30 --> 00:02:16,90
when implementing any state management solution,

51
00:02:16,90 --> 00:02:19,10
are the trade-offs of the solution.

52
00:02:19,10 --> 00:02:22,20
No solution is perfect, React state included.

53
00:02:22,20 --> 00:02:25,10
Let's take a look at the presentation business logic

54
00:02:25,10 --> 00:02:27,00
of this component.

55
00:02:27,00 --> 00:02:29,20
We separated our business logic,

56
00:02:29,20 --> 00:02:31,90
adding to and removing from a collection,

57
00:02:31,90 --> 00:02:33,60
but still this all lives

58
00:02:33,60 --> 00:02:37,00
in this one component called MainSection.

59
00:02:37,00 --> 00:02:39,20
Now depending on how often you plan

60
00:02:39,20 --> 00:02:40,80
on changing UI frameworks,

61
00:02:40,80 --> 00:02:43,70
this could be a positive or a negative.

62
00:02:43,70 --> 00:02:47,20
If you don't plan on using a different UI rendering library,

63
00:02:47,20 --> 00:02:49,50
such as Angular or Vue.js,

64
00:02:49,50 --> 00:02:52,60
then maybe this is a good solution because it allows you

65
00:02:52,60 --> 00:02:55,50
to keep all of your logic in one place.

66
00:02:55,50 --> 00:02:57,00
On the other hand, if you think you're going

67
00:02:57,00 --> 00:03:00,20
to change your UI frameworks frequently,

68
00:03:00,20 --> 00:03:03,50
it could be worth it to completely separate the two.

69
00:03:03,50 --> 00:03:05,50
Remember to keep in mind that no matter

70
00:03:05,50 --> 00:03:07,80
what state management solution you use,

71
00:03:07,80 --> 00:03:09,60
there are trade-offs involved.

72
00:03:09,60 --> 00:03:11,50
Think about the functionality you need,

73
00:03:11,50 --> 00:03:13,00
and how often do you expect

74
00:03:13,00 --> 00:03:16,10
to change your presentation or business logic.

75
00:03:16,10 --> 00:03:18,30
React provides powerful tools for us

76
00:03:18,30 --> 00:03:21,00
to accomplish complex business logic.

