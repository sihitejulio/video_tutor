1
00:00:00,50 --> 00:00:01,90
- [Narrator] The concept of context

2
00:00:01,90 --> 00:00:04,80
is most easily introduced with an example.

3
00:00:04,80 --> 00:00:08,20
To do this we're going to create a folder called context

4
00:00:08,20 --> 00:00:11,00
where we'll define our book context file.

5
00:00:11,00 --> 00:00:13,20
Lets go ahead and do that now.

6
00:00:13,20 --> 00:00:17,40
Cool, so lets go line by line and implement this.

7
00:00:17,40 --> 00:00:19,70
It's a pretty short file.

8
00:00:19,70 --> 00:00:21,20
First, we're going to import react,

9
00:00:21,20 --> 00:00:24,30
and then we're going to define our context.

10
00:00:24,30 --> 00:00:26,60
Now, notice that we're creating context

11
00:00:26,60 --> 00:00:29,40
with an empty object, that's totally okay,

12
00:00:29,40 --> 00:00:31,70
it's going to get populated with context

13
00:00:31,70 --> 00:00:34,90
from our provider component later on.

14
00:00:34,90 --> 00:00:37,00
The other thing I'm going to do here

15
00:00:37,00 --> 00:00:40,50
is define a provider,

16
00:00:40,50 --> 00:00:42,50
and a consumer.

17
00:00:42,50 --> 00:00:45,10
What this does is, that it's a nice substraction

18
00:00:45,10 --> 00:00:47,30
and it makes it easier to read code.

19
00:00:47,30 --> 00:00:50,30
It's not a necessity, it's just something nice to do.

20
00:00:50,30 --> 00:00:54,00
So, next we'll insert the provider and consumer.

21
00:00:54,00 --> 00:00:56,60
To show the power of context, we'll introduce

22
00:00:56,60 --> 00:00:58,70
a button called toggle authors,

23
00:00:58,70 --> 00:01:02,50
which decides whether BookItem displays the author or not.

24
00:01:02,50 --> 00:01:04,30
Notice that BookList won't need

25
00:01:04,30 --> 00:01:07,00
this information but BookItem will.

26
00:01:07,00 --> 00:01:09,40
Lets go ahead and copy paste this

27
00:01:09,40 --> 00:01:11,40
in our application and make sure

28
00:01:11,40 --> 00:01:13,40
that it appears as we expect.

29
00:01:13,40 --> 00:01:16,90
Cool, so now we have this toggle authors button.

30
00:01:16,90 --> 00:01:18,80
We don't need to change anything about

31
00:01:18,80 --> 00:01:21,70
how we define state, or any of our methods.

32
00:01:21,70 --> 00:01:24,00
All we need to do is enter render method,

33
00:01:24,00 --> 00:01:26,60
instead of returning our top level component,

34
00:01:26,60 --> 00:01:29,60
we need to wrap the component in our provider.

35
00:01:29,60 --> 00:01:32,00
First, we're going to import BookProvider.

36
00:01:32,00 --> 00:01:35,30
So, what we have here is, once we import this,

37
00:01:35,30 --> 00:01:37,00
we can go ahead and wrap our

38
00:01:37,00 --> 00:01:39,60
top level component in our BookProvider.

39
00:01:39,60 --> 00:01:44,50
So, here in our render, we're going to say BookProvider.

40
00:01:44,50 --> 00:01:50,00
Perfect. And then,

41
00:01:50,00 --> 00:01:51,70
nice. Now that we have our

42
00:01:51,70 --> 00:01:55,70
BookProvider component it's time to define our context.

43
00:01:55,70 --> 00:01:59,00
Remember that here we defined an empty object,

44
00:01:59,00 --> 00:02:00,90
and now we would like to go ahead

45
00:02:00,90 --> 00:02:04,40
and populate it with a value from state.

46
00:02:04,40 --> 00:02:07,90
Now, since we like to show authors and toggle that,

47
00:02:07,90 --> 00:02:09,70
we don't that defined in state and,

48
00:02:09,70 --> 00:02:13,60
we need to do that if we want to pass it on to a provider.

49
00:02:13,60 --> 00:02:16,10
So, lets define it as true.

50
00:02:16,10 --> 00:02:17,90
Now that we have showAuthors,

51
00:02:17,90 --> 00:02:23,90
lets go ahead and pass the value key, our context.

52
00:02:23,90 --> 00:02:26,50
So showAuthors is going to be our key.

53
00:02:26,50 --> 00:02:29,50
And, what that does is, it's going to just grab

54
00:02:29,50 --> 00:02:33,20
the value of showAuthors from desktop state.

55
00:02:33,20 --> 00:02:34,80
Now that we have our provider,

56
00:02:34,80 --> 00:02:37,70
we can go ahead and import our consumer

57
00:02:37,70 --> 00:02:39,60
which will use the value that we've

58
00:02:39,60 --> 00:02:42,80
defined in this BookProvider.

59
00:02:42,80 --> 00:02:46,90
Lets import. Okay, so,

60
00:02:46,90 --> 00:02:50,30
context, BookContext, all set, cool.

61
00:02:50,30 --> 00:02:52,90
So now that we have that, we want to go ahead

62
00:02:52,90 --> 00:02:55,20
and wrap our component that's going to be

63
00:02:55,20 --> 00:02:58,50
using the context in BookConsumer.

64
00:02:58,50 --> 00:03:00,70
Now that we've imported BookConsumer,

65
00:03:00,70 --> 00:03:02,60
we're going to want to wrap the element

66
00:03:02,60 --> 00:03:06,80
that will be accessing context with BookConsumer.

67
00:03:06,80 --> 00:03:09,40
In element here, lets go ahead and

68
00:03:09,40 --> 00:03:13,00
wrap this div with BookConsumer.

69
00:03:13,00 --> 00:03:15,60
So, now what we'll need to do is,

70
00:03:15,60 --> 00:03:20,60
we'll have to access the object that the provider gave us.

71
00:03:20,60 --> 00:03:22,30
Lets make sure to do that first

72
00:03:22,30 --> 00:03:26,80
before accessing any other aspect of the component.

73
00:03:26,80 --> 00:03:29,60
And then, to make sure that this compiles,

74
00:03:29,60 --> 00:03:31,70
lets go ahead and make sure our

75
00:03:31,70 --> 00:03:33,90
PrepCeasar closed appropriately.

76
00:03:33,90 --> 00:03:37,10
Cool, so lets make sure that that compiled correctly.

77
00:03:37,10 --> 00:03:39,80
Great. Now that we're all set there,

78
00:03:39,80 --> 00:03:41,90
we have access to the showAuthors

79
00:03:41,90 --> 00:03:44,00
variable from the context.

80
00:03:44,00 --> 00:03:46,20
In this case, what we'd like to do is

81
00:03:46,20 --> 00:03:49,30
display this element if show authors is true.

82
00:03:49,30 --> 00:03:53,20
But if it's not true, lets just not render anything.

83
00:03:53,20 --> 00:03:56,70
So here, we're going to use a ternary statement

84
00:03:56,70 --> 00:03:59,20
and say, if showAuthors is true,

85
00:03:59,20 --> 00:04:03,50
return the statement, otherwise, just show no.

86
00:04:03,50 --> 00:04:06,40
So, lets see if this works, going back to our app,

87
00:04:06,40 --> 00:04:08,40
we see that showAuthors is true.

88
00:04:08,40 --> 00:04:10,50
If we toggle it, we see that it's false,

89
00:04:10,50 --> 00:04:14,10
and then we can continue to toggle as we please.

90
00:04:14,10 --> 00:04:16,60
When implementing context, the component that

91
00:04:16,60 --> 00:04:18,30
you would like to receive state from

92
00:04:18,30 --> 00:04:21,00
has to be a direct parent of the child component,

93
00:04:21,00 --> 00:04:23,20
which is important to keep in mind.

94
00:04:23,20 --> 00:04:25,30
You can't use context to communicate

95
00:04:25,30 --> 00:04:27,70
state between components, although now,

96
00:04:27,70 --> 00:04:30,00
you can use react hooks to do so.

