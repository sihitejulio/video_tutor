1
00:00:00,60 --> 00:00:01,60
- [Instructor] Let's talk about

2
00:00:01,60 --> 00:00:03,70
why we need state management.

3
00:00:03,70 --> 00:00:06,60
As soon as you extend beyond basic functionality,

4
00:00:06,60 --> 00:00:10,40
there's typically some need for a state management solution.

5
00:00:10,40 --> 00:00:12,60
As you begin to manage large amounts of data,

6
00:00:12,60 --> 00:00:15,10
it can become pretty unwieldy.

7
00:00:15,10 --> 00:00:18,20
The main reason to use a state management solution

8
00:00:18,20 --> 00:00:20,10
is that it manages business logic.

9
00:00:20,10 --> 00:00:22,10
Well, what do I mean by that?

10
00:00:22,10 --> 00:00:23,50
Let's start with the distinction

11
00:00:23,50 --> 00:00:26,40
between presentation and business logic.

12
00:00:26,40 --> 00:00:28,50
Presentation logic refers to logic

13
00:00:28,50 --> 00:00:31,70
that's associated with how components appear on a page.

14
00:00:31,70 --> 00:00:35,00
For example, when should a modal dialog pop up?

15
00:00:35,00 --> 00:00:36,50
Does the color of a button change

16
00:00:36,50 --> 00:00:38,50
when a user hovers over it?

17
00:00:38,50 --> 00:00:40,20
Another way to think about it is,

18
00:00:40,20 --> 00:00:42,00
think of logic that's agnostic

19
00:00:42,00 --> 00:00:44,80
towards the application's purpose.

20
00:00:44,80 --> 00:00:47,20
On the other hand, we have business logic,

21
00:00:47,20 --> 00:00:49,80
which refers to handling, manipulating,

22
00:00:49,80 --> 00:00:52,00
and storing business objects.

23
00:00:52,00 --> 00:00:54,00
So for example, in a to-do app

24
00:00:54,00 --> 00:00:56,20
where we have a user that needs to log in,

25
00:00:56,20 --> 00:00:58,80
it could mean things like user accounts

26
00:00:58,80 --> 00:01:00,80
or items in a to-do list.

27
00:01:00,80 --> 00:01:02,30
Unlike presentation logic,

28
00:01:02,30 --> 00:01:05,80
this is logic that is application-specific.

29
00:01:05,80 --> 00:01:08,50
Handling business logic is the most common use case

30
00:01:08,50 --> 00:01:11,30
for introducing a state management library.

31
00:01:11,30 --> 00:01:13,60
Figuring out where to connect presentation

32
00:01:13,60 --> 00:01:16,20
and business logic can be pretty tricky.

33
00:01:16,20 --> 00:01:18,80
We run into all kinds of problems to solve.

34
00:01:18,80 --> 00:01:21,80
How do we persist state for users between sessions?

35
00:01:21,80 --> 00:01:24,20
How does React know that our data has changed

36
00:01:24,20 --> 00:01:26,80
and that we need to re-render the component?

37
00:01:26,80 --> 00:01:29,90
State management makes opinionated decisions for us

38
00:01:29,90 --> 00:01:33,20
that allow us to work within a consistent system.

39
00:01:33,20 --> 00:01:37,20
So what happens when we don't use a state management system?

40
00:01:37,20 --> 00:01:38,50
This would mean that we store

41
00:01:38,50 --> 00:01:41,80
every part of our business logic in a React state.

42
00:01:41,80 --> 00:01:43,50
This is not best practice,

43
00:01:43,50 --> 00:01:45,30
and I would not recommend doing this

44
00:01:45,30 --> 00:01:47,50
in a production application.

45
00:01:47,50 --> 00:01:49,90
So let's take the case that we'd actually decide

46
00:01:49,90 --> 00:01:51,50
to store our application state

47
00:01:51,50 --> 00:01:53,50
all in one component.

48
00:01:53,50 --> 00:01:55,70
Every time we would want to change the state

49
00:01:55,70 --> 00:01:57,10
from a child component,

50
00:01:57,10 --> 00:01:58,60
we would need to send that state

51
00:01:58,60 --> 00:02:01,20
back up to the parent, which can be pretty tedious

52
00:02:01,20 --> 00:02:04,60
if your component is several levels of nesting down.

53
00:02:04,60 --> 00:02:07,70
You'd also be re-rendering your entire application

54
00:02:07,70 --> 00:02:10,50
every time state changes in a component,

55
00:02:10,50 --> 00:02:14,10
which is unnecessary and costly performance-wise.

56
00:02:14,10 --> 00:02:17,80
It actually also defeats the purpose of using React.

57
00:02:17,80 --> 00:02:19,80
It's pretty difficult to work with.

58
00:02:19,80 --> 00:02:23,10
Handling one large JSON blob makes code difficult

59
00:02:23,10 --> 00:02:24,50
to reason about.

60
00:02:24,50 --> 00:02:27,10
So for example, how would you know which part

61
00:02:27,10 --> 00:02:30,20
of state belongs to which components?

62
00:02:30,20 --> 00:02:33,20
Breaking up state between components often means

63
00:02:33,20 --> 00:02:35,40
that you need a way to coordinate state

64
00:02:35,40 --> 00:02:37,60
among all of these components.

65
00:02:37,60 --> 00:02:40,70
Keep in mind though, that if you have a simple application,

66
00:02:40,70 --> 00:02:43,10
you might not need a more complex solution

67
00:02:43,10 --> 00:02:46,70
than keeping your business logic in React state.

68
00:02:46,70 --> 00:02:50,10
Ask yourself questions about your code readability.

69
00:02:50,10 --> 00:02:53,00
If a new developer begins working with your code base,

70
00:02:53,00 --> 00:02:54,70
they should be able to understand

71
00:02:54,70 --> 00:02:58,40
how each component is using business logic clearly.

72
00:02:58,40 --> 00:03:00,90
If not, it's worth reconsidering your approach

73
00:03:00,90 --> 00:03:02,00
to state management.

