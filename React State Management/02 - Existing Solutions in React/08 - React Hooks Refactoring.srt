1
00:00:00,60 --> 00:00:02,90
- [Instructor] The first question we should ask ourselves

2
00:00:02,90 --> 00:00:06,40
when using Hooks is, why not just refactor?

3
00:00:06,40 --> 00:00:08,70
Is this really so much better?

4
00:00:08,70 --> 00:00:10,20
It might not be obvious,

5
00:00:10,20 --> 00:00:12,60
but think of cases where you would want to share

6
00:00:12,60 --> 00:00:15,50
the same logic in multiple components.

7
00:00:15,50 --> 00:00:19,00
Suppose we have multiple counters or other components

8
00:00:19,00 --> 00:00:20,90
that increment and decrement.

9
00:00:20,90 --> 00:00:23,60
It would be great to define that logic in one place,

10
00:00:23,60 --> 00:00:25,60
and use it in multiple components

11
00:00:25,60 --> 00:00:29,30
instead of defining local state in several components.

12
00:00:29,30 --> 00:00:31,60
It's also much cleaner to write.

13
00:00:31,60 --> 00:00:34,40
If you've worked with React in a production code base,

14
00:00:34,40 --> 00:00:37,20
you've likely seen cases where components are wrapped

15
00:00:37,20 --> 00:00:40,10
in multiple layers of higher-order components,

16
00:00:40,10 --> 00:00:41,90
which makes it hard to understand

17
00:00:41,90 --> 00:00:44,30
what the component is supposed to be doing.

18
00:00:44,30 --> 00:00:47,30
If we abstract out the common stateful logic,

19
00:00:47,30 --> 00:00:50,50
we don't need to wrap components to give them state.

20
00:00:50,50 --> 00:00:52,20
A great example of this

21
00:00:52,20 --> 00:00:56,20
is one of React's built-in hooks, useContext.

22
00:00:56,20 --> 00:01:00,00
As a FYI, there are currently 10 built-in hooks.

23
00:01:00,00 --> 00:01:02,50
This hook leverages React context,

24
00:01:02,50 --> 00:01:04,70
which allows us to avoid prop drilling

25
00:01:04,70 --> 00:01:08,80
and broadcast state from a parent to a child component.

26
00:01:08,80 --> 00:01:10,60
However, one drawback is

27
00:01:10,60 --> 00:01:13,20
that we can potentially have child components

28
00:01:13,20 --> 00:01:15,70
that are consuming state from multiple parents,

29
00:01:15,70 --> 00:01:17,60
such as in this example.

30
00:01:17,60 --> 00:01:19,40
So the core component is wrapped

31
00:01:19,40 --> 00:01:22,70
in several context consumer components.

32
00:01:22,70 --> 00:01:24,50
With the useContext hook,

33
00:01:24,50 --> 00:01:27,10
we can get the context by using the hook

34
00:01:27,10 --> 00:01:30,40
and plug into the component directly.

35
00:01:30,40 --> 00:01:33,60
Stylistically, this looks a lot cleaner.

36
00:01:33,60 --> 00:01:34,90
If you look at the component,

37
00:01:34,90 --> 00:01:36,40
it's really easy to tell

38
00:01:36,40 --> 00:01:38,30
what it's supposed to be doing.

39
00:01:38,30 --> 00:01:40,60
There's similar examples for fetching data

40
00:01:40,60 --> 00:01:43,60
that completely replace React lifecycle methods

41
00:01:43,60 --> 00:01:46,60
and update your component when your data comes in.

42
00:01:46,60 --> 00:01:49,00
The React team has put a lot of effort

43
00:01:49,00 --> 00:01:51,90
into documenting the different use cases for Hooks.

44
00:01:51,90 --> 00:01:53,40
So I strongly encourage you

45
00:01:53,40 --> 00:01:56,70
to explore the documentation more.

46
00:01:56,70 --> 00:01:59,20
So should you go out and refactor

47
00:01:59,20 --> 00:02:01,80
all of your components to use Hooks?

48
00:02:01,80 --> 00:02:03,10
Not quite.

49
00:02:03,10 --> 00:02:05,70
It looks like there are more changes coming to React,

50
00:02:05,70 --> 00:02:09,10
particularly around data loading and suspense.

51
00:02:09,10 --> 00:02:12,00
In this case and in refactoring in general,

52
00:02:12,00 --> 00:02:13,90
I recommend taking the approach

53
00:02:13,90 --> 00:02:17,30
of trying this out with one new component you're writing

54
00:02:17,30 --> 00:02:20,40
and figuring out what the tradeoffs are for yourself.

55
00:02:20,40 --> 00:02:23,30
If you think your application would improve substantially

56
00:02:23,30 --> 00:02:26,00
with regards to code reuse and readability,

57
00:02:26,00 --> 00:02:27,60
it might be worth it.

58
00:02:27,60 --> 00:02:30,10
Otherwise, definitely keep this patterned in mind

59
00:02:30,10 --> 00:02:32,00
as your application grows.

