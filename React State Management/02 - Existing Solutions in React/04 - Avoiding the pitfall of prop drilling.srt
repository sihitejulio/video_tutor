1
00:00:00,50 --> 00:00:02,90
- [Instructor] In almost every app I've worked in,

2
00:00:02,90 --> 00:00:04,90
I've seen a pattern like this.

3
00:00:04,90 --> 00:00:06,60
And if you're wondering what this is,

4
00:00:06,60 --> 00:00:09,10
this is actually just a pared down version

5
00:00:09,10 --> 00:00:11,30
of our book app that we've been working with.

6
00:00:11,30 --> 00:00:14,60
And to be clear I don't expect you to code anything with me

7
00:00:14,60 --> 00:00:16,50
this is just for your reference.

8
00:00:16,50 --> 00:00:19,20
So as you can see here props are passed down

9
00:00:19,20 --> 00:00:22,40
from component to component to component.

10
00:00:22,40 --> 00:00:25,20
Take a look at this update books fun.

11
00:00:25,20 --> 00:00:28,60
In the example here we're passing this updateBook function

12
00:00:28,60 --> 00:00:31,80
from the parent component to the child component

13
00:00:31,80 --> 00:00:33,80
to the grandparent component.

14
00:00:33,80 --> 00:00:35,80
But notice that the child component

15
00:00:35,80 --> 00:00:38,90
has no use for this prop at all.

16
00:00:38,90 --> 00:00:42,10
This is called prop drilling or threading in React

17
00:00:42,10 --> 00:00:44,20
and it's actually an anti-pattern.

18
00:00:44,20 --> 00:00:46,30
Your component should only have the data

19
00:00:46,30 --> 00:00:49,60
it needs to render and nothing more.

20
00:00:49,60 --> 00:00:52,40
Let's take a step back and see why prop drilling

21
00:00:52,40 --> 00:00:55,30
gets introduced in an application.

22
00:00:55,30 --> 00:00:56,90
In these sample components here

23
00:00:56,90 --> 00:00:58,90
we can actually render everything

24
00:00:58,90 --> 00:01:01,20
in one component like this.

25
00:01:01,20 --> 00:01:05,30
It avoids prop drilling since we only have one component.

26
00:01:05,30 --> 00:01:06,50
But as you can guess,

27
00:01:06,50 --> 00:01:09,40
this gets pretty unwieldy after a while.

28
00:01:09,40 --> 00:01:13,50
Our solution can't be to keep everything in one component.

29
00:01:13,50 --> 00:01:15,50
So what we do is we break out

30
00:01:15,50 --> 00:01:18,40
things into multiple components.

31
00:01:18,40 --> 00:01:21,00
The issue here is that our prop dependencies

32
00:01:21,00 --> 00:01:24,00
aren't always a straight line between the top level

33
00:01:24,00 --> 00:01:26,50
and the more deeply nested components.

34
00:01:26,50 --> 00:01:29,10
In our example code we need a dependency

35
00:01:29,10 --> 00:01:32,30
on the updateBook function in the grandparent component

36
00:01:32,30 --> 00:01:35,70
that the child component doesn't need.

37
00:01:35,70 --> 00:01:38,30
Our instinct to make sense at first.

38
00:01:38,30 --> 00:01:40,10
Let's just pass down everything

39
00:01:40,10 --> 00:01:43,30
from the top level component and have the child components

40
00:01:43,30 --> 00:01:45,80
figure out which props go where.

41
00:01:45,80 --> 00:01:48,70
This approach probably works for a while.

42
00:01:48,70 --> 00:01:51,20
Then suppose one day you get a request

43
00:01:51,20 --> 00:01:53,20
to change the shape of your data.

44
00:01:53,20 --> 00:01:55,30
We no longer want update book,

45
00:01:55,30 --> 00:01:57,70
we want to change the names of these props

46
00:01:57,70 --> 00:02:01,70
to be update media to support music and movies.

47
00:02:01,70 --> 00:02:04,40
You start refactoring the top level component

48
00:02:04,40 --> 00:02:05,70
then realize you have to change

49
00:02:05,70 --> 00:02:07,70
not only the grandchild component

50
00:02:07,70 --> 00:02:09,70
and then the child component,

51
00:02:09,70 --> 00:02:12,70
all of a sudden refactoring just becomes a huge deal,

52
00:02:12,70 --> 00:02:15,80
a much bigger deal than you intially realized.

53
00:02:15,80 --> 00:02:19,60
We always want to make sure out code is readable too right?

54
00:02:19,60 --> 00:02:22,00
One issue I run into a lot of times

55
00:02:22,00 --> 00:02:24,10
when writing code that has prop drilling

56
00:02:24,10 --> 00:02:25,90
is that I name props different things

57
00:02:25,90 --> 00:02:27,40
and different components.

58
00:02:27,40 --> 00:02:31,60
Notice that this is update books here, update book fun here,

59
00:02:31,60 --> 00:02:33,30
how do I know that this is actually

60
00:02:33,30 --> 00:02:35,20
the same prop in my head?

61
00:02:35,20 --> 00:02:36,60
I can't really figure that out

62
00:02:36,60 --> 00:02:39,00
which can be super confusing.

63
00:02:39,00 --> 00:02:42,80
Clearly prop drilling is an issue but how do we fix it?

64
00:02:42,80 --> 00:02:44,60
The answer lies in keeping state

65
00:02:44,60 --> 00:02:47,80
as close to your components as possible.

66
00:02:47,80 --> 00:02:50,00
Similar to what we discussed before,

67
00:02:50,00 --> 00:02:54,00
one possible answer is to avoid breaking up your components.

68
00:02:54,00 --> 00:02:55,70
This doesn't mean keeping everything

69
00:02:55,70 --> 00:02:58,90
in one component like we're doing here.

70
00:02:58,90 --> 00:03:00,70
Sometimes we run into prop drilling

71
00:03:00,70 --> 00:03:04,10
by prematurely breaking up our components.

72
00:03:04,10 --> 00:03:07,40
If you're passing code between several different components

73
00:03:07,40 --> 00:03:10,10
it's also worth asking how much data needs

74
00:03:10,10 --> 00:03:12,30
to be shared at the child level.

75
00:03:12,30 --> 00:03:14,30
If you're only using one prop

76
00:03:14,30 --> 00:03:17,40
a few levels down for just one component

77
00:03:17,40 --> 00:03:21,10
consider passing down the whole component as a prop.

78
00:03:21,10 --> 00:03:23,60
Remember that components are data.

79
00:03:23,60 --> 00:03:25,80
Let's take a look at completed books.

80
00:03:25,80 --> 00:03:28,90
This is a component that only requires one prop,

81
00:03:28,90 --> 00:03:30,60
numBooksCompleted.

82
00:03:30,60 --> 00:03:33,50
What we can do here in our parent component

83
00:03:33,50 --> 00:03:35,20
is actually just pass down

84
00:03:35,20 --> 00:03:38,20
the whole rendered component to the grandchild.

85
00:03:38,20 --> 00:03:40,60
That way the child component doesn't really need

86
00:03:40,60 --> 00:03:44,60
to care about what numBooksCompleted is.

87
00:03:44,60 --> 00:03:48,60
Yet another solution is to use React's context API.

88
00:03:48,60 --> 00:03:52,00
This way you can keep state relevant to only the components

89
00:03:52,00 --> 00:03:55,80
that need it rather than managing state at the top level.

90
00:03:55,80 --> 00:03:57,30
This solution is ideal

91
00:03:57,30 --> 00:03:59,60
when you have more than just one or two props

92
00:03:59,60 --> 00:04:02,00
that need to be used in multiple components.

