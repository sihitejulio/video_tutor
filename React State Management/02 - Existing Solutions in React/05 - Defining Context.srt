1
00:00:00,90 --> 00:00:03,00
- [Instructor] React's context API is a great way

2
00:00:03,00 --> 00:00:06,60
to manage state without introducing a third-party library

3
00:00:06,60 --> 00:00:08,70
like Mobx or Redux.

4
00:00:08,70 --> 00:00:12,40
Before context, React developers didn't have an easy way

5
00:00:12,40 --> 00:00:15,40
to manage state in mid-sized applications.

6
00:00:15,40 --> 00:00:17,70
The options were to either use prop drilling,

7
00:00:17,70 --> 00:00:20,50
which introduces unnecessary confusion

8
00:00:20,50 --> 00:00:22,50
or bring in Redux, which introduces

9
00:00:22,50 --> 00:00:24,70
unnecessary complexity.

10
00:00:24,70 --> 00:00:27,90
Context exists for small to mid-sized applications

11
00:00:27,90 --> 00:00:30,00
that need some way to know about data

12
00:00:30,00 --> 00:00:31,90
without prop drilling.

13
00:00:31,90 --> 00:00:34,10
If you're wondering what the threshold is

14
00:00:34,10 --> 00:00:36,30
where prop drilling becomes a problem,

15
00:00:36,30 --> 00:00:38,60
my personal heuristic is asking myself

16
00:00:38,60 --> 00:00:40,40
if I'm annoyed passing down props

17
00:00:40,40 --> 00:00:42,20
from component to component,

18
00:00:42,20 --> 00:00:43,80
which typically happens

19
00:00:43,80 --> 00:00:47,00
if I'm prop drilling through three components.

20
00:00:47,00 --> 00:00:49,00
So let's take a look and see

21
00:00:49,00 --> 00:00:51,50
what exactly is context?

22
00:00:51,50 --> 00:00:55,10
It's a way to broadcast data to other components.

23
00:00:55,10 --> 00:00:58,20
React uses the terms providers and consumers

24
00:00:58,20 --> 00:01:01,60
for the two types of components involved in context.

25
00:01:01,60 --> 00:01:03,90
There's a component which provides state

26
00:01:03,90 --> 00:01:06,20
and components that consume state.

27
00:01:06,20 --> 00:01:09,00
In this case, you can see that there is a provider

28
00:01:09,00 --> 00:01:11,70
that has information about the collection of books

29
00:01:11,70 --> 00:01:13,30
that a user has.

30
00:01:13,30 --> 00:01:15,10
Now for this information,

31
00:01:15,10 --> 00:01:17,60
you can imagine that there are a few components

32
00:01:17,60 --> 00:01:18,80
that would want that.

33
00:01:18,80 --> 00:01:21,60
In this example, we have a provider component,

34
00:01:21,60 --> 00:01:23,00
which has information

35
00:01:23,00 --> 00:01:25,90
about the collection of books a user has.

36
00:01:25,90 --> 00:01:26,80
You could imagine

37
00:01:26,80 --> 00:01:29,40
that there are a few different potential consumers

38
00:01:29,40 --> 00:01:31,00
of this information.

39
00:01:31,00 --> 00:01:34,10
Maybe it's a component to edit the book collection,

40
00:01:34,10 --> 00:01:36,60
maybe it's a component to display the books,

41
00:01:36,60 --> 00:01:39,00
or it could even be a component that allows you

42
00:01:39,00 --> 00:01:41,10
to share books with your friends.

43
00:01:41,10 --> 00:01:42,40
Think about it like this.

44
00:01:42,40 --> 00:01:45,30
Suppose you wanted to pass a note to someone in class.

45
00:01:45,30 --> 00:01:47,30
You could pass the note to a friend,

46
00:01:47,30 --> 00:01:49,10
who passes it to another friend,

47
00:01:49,10 --> 00:01:50,40
who passes it to the person

48
00:01:50,40 --> 00:01:52,20
that you would like to receive it.

49
00:01:52,20 --> 00:01:54,00
Or you could crumple your note into a ball

50
00:01:54,00 --> 00:01:56,20
and directly toss it to your friend.

51
00:01:56,20 --> 00:01:59,20
The point here is that you can bypass child components

52
00:01:59,20 --> 00:02:02,00
to access the grandchildren component state.

