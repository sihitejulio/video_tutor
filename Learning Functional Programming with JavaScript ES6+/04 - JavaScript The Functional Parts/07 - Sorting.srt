1
00:00:00,50 --> 00:00:01,30
- [Instructor] Now that we've seen

2
00:00:01,30 --> 00:00:03,50
how to use Slice to prevent mutation,

3
00:00:03,50 --> 00:00:06,00
let's take a look at the function Sort.

4
00:00:06,00 --> 00:00:07,70
As you may have guessed by the name,

5
00:00:07,70 --> 00:00:09,50
this function is used when you want to change

6
00:00:09,50 --> 00:00:11,70
the order of elements in an array.

7
00:00:11,70 --> 00:00:13,30
This would be very useful, for example,

8
00:00:13,30 --> 00:00:14,70
if we had an array of names

9
00:00:14,70 --> 00:00:16,90
and wanted to sort it in alphabetical order.

10
00:00:16,90 --> 00:00:18,40
Or if we had an array of employees

11
00:00:18,40 --> 00:00:20,20
and wanted to sort them by the number of years

12
00:00:20,20 --> 00:00:22,80
they've been with the company, for example.

13
00:00:22,80 --> 00:00:24,30
Before we look at how to use sort,

14
00:00:24,30 --> 00:00:25,60
it's very important to remember

15
00:00:25,60 --> 00:00:27,60
that Sort is a mutating function.

16
00:00:27,60 --> 00:00:29,30
Which means that it actually modifies

17
00:00:29,30 --> 00:00:32,30
the original version of the array that it's called on.

18
00:00:32,30 --> 00:00:34,40
In a previous video we saw how JavaScript's

19
00:00:34,40 --> 00:00:36,10
built-in Slice function could be used

20
00:00:36,10 --> 00:00:38,70
to make a copy of an array, which would then allow us

21
00:00:38,70 --> 00:00:40,60
to use mutating functions such as Sort

22
00:00:40,60 --> 00:00:43,20
without modifying the original array.

23
00:00:43,20 --> 00:00:45,50
And that's exactly what we're going to do with Sort.

24
00:00:45,50 --> 00:00:48,40
So, whenever we use Sort instead of simply calling it

25
00:00:48,40 --> 00:00:53,80
using array.sort we're going to call it using array.slice.sort.

26
00:00:53,80 --> 00:00:58,00
Keep this in mind for all the examples we see using Sort.

27
00:00:58,00 --> 00:00:59,80
So then how do we use Sort?

28
00:00:59,80 --> 00:01:01,80
Sort like Map, Filter, Every

29
00:01:01,80 --> 00:01:05,90
and Sum takes a function as an argument.

30
00:01:05,90 --> 00:01:08,10
However, unlike the other functions,

31
00:01:08,10 --> 00:01:11,90
the function we pass to Sort called a Comparator function

32
00:01:11,90 --> 00:01:14,40
has two arguments instead of one.

33
00:01:14,40 --> 00:01:17,00
These two arguments are a little more abstract

34
00:01:17,00 --> 00:01:19,10
and stand for any two elements in our array

35
00:01:19,10 --> 00:01:21,20
that could be compared.

36
00:01:21,20 --> 00:01:24,40
The Return value of this function for any two elements

37
00:01:24,40 --> 00:01:26,50
determines the order in which these elements

38
00:01:26,50 --> 00:01:29,90
will appear in relation to each other in the final array.

39
00:01:29,90 --> 00:01:32,70
For example, if our Comparator function returns a value

40
00:01:32,70 --> 00:01:35,40
less than zero, then the first argument will come

41
00:01:35,40 --> 00:01:38,30
before the second argument in the final array.

42
00:01:38,30 --> 00:01:41,40
If the Comparator function returns a value greater than zero

43
00:01:41,40 --> 00:01:43,40
than the first argument will come after

44
00:01:43,40 --> 00:01:46,00
the second argument in the final array.

45
00:01:46,00 --> 00:01:49,00
And finally, if the return value is equal to zero,

46
00:01:49,00 --> 00:01:51,80
than that signifies to Sort that the numbers are the same

47
00:01:51,80 --> 00:01:53,70
and it will leave them in the same relative order

48
00:01:53,70 --> 00:01:55,80
that it finds them.

49
00:01:55,80 --> 00:01:58,10
This might sound a little complicated at first.

50
00:01:58,10 --> 00:01:59,50
So let's see what this will look like

51
00:01:59,50 --> 00:02:01,20
in a very simple example.

52
00:02:01,20 --> 00:02:02,70
Let's say that we have an array of numbers

53
00:02:02,70 --> 00:02:05,90
from one to 10 all mixed up like we have here.

54
00:02:05,90 --> 00:02:08,20
In order to get a sorted copy of this array,

55
00:02:08,20 --> 00:02:10,60
the first thing we'll do is define our Comparator function

56
00:02:10,60 --> 00:02:13,10
which will pass into Sort to tell it what order

57
00:02:13,10 --> 00:02:14,90
we want our numbers in.

58
00:02:14,90 --> 00:02:16,50
Let's say that we want to sort our numbers

59
00:02:16,50 --> 00:02:18,90
in ascending order from one to 10.

60
00:02:18,90 --> 00:02:21,80
So we'll create a function and call it Ascending,

61
00:02:21,80 --> 00:02:24,90
const ascending.

62
00:02:24,90 --> 00:02:27,10
And this function will have to arguments.

63
00:02:27,10 --> 00:02:29,40
We'll call them A and B.

64
00:02:29,40 --> 00:02:32,50
Now, using the logic we discussed a minute ago,

65
00:02:32,50 --> 00:02:34,90
if our first argument A is smaller than

66
00:02:34,90 --> 00:02:37,40
our second argument B, we'll want it to come

67
00:02:37,40 --> 00:02:39,60
before B in our final array.

68
00:02:39,60 --> 00:02:42,20
So we'll want to return a value less than zero.

69
00:02:42,20 --> 00:02:43,70
We'll use negative one.

70
00:02:43,70 --> 00:02:45,70
That'll look like this;

71
00:02:45,70 --> 00:02:52,30
if (a < b) return -1.

72
00:02:52,30 --> 00:02:54,50
Otherwise, if A is larger than B,

73
00:02:54,50 --> 00:02:57,30
we'll want it to come after B in our final array.

74
00:02:57,30 --> 00:02:59,80
So we'll want to return a value greater than zero.

75
00:02:59,80 --> 00:03:01,80
We'll use one;

76
00:03:01,80 --> 00:03:06,60
if (a > b) return 1.

77
00:03:06,60 --> 00:03:09,60
And then the only other case is if A and B are equal.

78
00:03:09,60 --> 00:03:11,90
So, if neither of these cases are true,

79
00:03:11,90 --> 00:03:13,90
we'll just return zero.

80
00:03:13,90 --> 00:03:15,90
Now that we have our Comparator function,

81
00:03:15,90 --> 00:03:19,40
all we have to do is call Sort on a copy of our array.

82
00:03:19,40 --> 00:03:22,50
Remember that we have to call Slice first to create a copy

83
00:03:22,50 --> 00:03:24,80
and that'll look like this,

84
00:03:24,80 --> 00:03:33,40
const sortedNUmbers = mixedUpNumbers.slice().sort()

85
00:03:33,40 --> 00:03:37,60
and then we'll pass our ascending function, ascending.

86
00:03:37,60 --> 00:03:40,30
Now, if we print this to the console,

87
00:03:40,30 --> 00:03:50,50
console.log(sortedNumbers) and run our code.

88
00:03:50,50 --> 00:03:53,00
We see that we get the desired result.

89
00:03:53,00 --> 00:03:56,30
So, so far we've sorted our numbers in ascending order.

90
00:03:56,30 --> 00:03:58,40
What if we wanted to sort them in descending order,

91
00:03:58,40 --> 00:04:00,40
from biggest to smallest?

92
00:04:00,40 --> 00:04:02,20
Well, to do this all we have to do

93
00:04:02,20 --> 00:04:04,30
is make two small changes.

94
00:04:04,30 --> 00:04:06,00
We just have to change the less than

95
00:04:06,00 --> 00:04:09,70
and greater than conditions around.

96
00:04:09,70 --> 00:04:14,80
Let's copy our ascending function.

97
00:04:14,80 --> 00:04:19,30
And rename this function, Descending.

98
00:04:19,30 --> 00:04:21,40
And then, as I said, all we have to do is switch

99
00:04:21,40 --> 00:04:25,80
the greater than and less than signs.

100
00:04:25,80 --> 00:04:31,80
Now, if we pass our descending function to Sort

101
00:04:31,80 --> 00:04:35,90
and run our code again, we see that again,

102
00:04:35,90 --> 00:04:38,40
it works perfectly.

103
00:04:38,40 --> 00:04:40,40
Note, that these Ascending and Descending functions

104
00:04:40,40 --> 00:04:44,40
will work equally well if we're sorting arrays of strings.

105
00:04:44,40 --> 00:04:46,20
For example, JavaScript would return True

106
00:04:46,20 --> 00:04:50,80
for this statement here.

107
00:04:50,80 --> 00:04:55,80
'Alabama' < 'Banana', for example.

108
00:04:55,80 --> 00:04:59,00
And again, as I mentioned before the exact implementation

109
00:04:59,00 --> 00:05:00,80
behind the scenes of the Sort function

110
00:05:00,80 --> 00:05:03,00
isn't important for us.

111
00:05:03,00 --> 00:05:05,40
And many browsers actually implement it differently

112
00:05:05,40 --> 00:05:08,60
depending on the array and several other factors.

113
00:05:08,60 --> 00:05:11,00
If you're curious about it feel free to look it up.

