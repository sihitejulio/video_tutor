1
00:00:00,50 --> 00:00:02,10
- [Instructor] Next, we're going to look at two built-in

2
00:00:02,10 --> 00:00:04,40
array functions that are pretty similar.

3
00:00:04,40 --> 00:00:07,30
These functions are called every and some.

4
00:00:07,30 --> 00:00:08,70
We saw in a previous video

5
00:00:08,70 --> 00:00:11,10
how to use JavaScript's filter function

6
00:00:11,10 --> 00:00:12,50
to find the elements in an array

7
00:00:12,50 --> 00:00:15,00
for which a given condition is true.

8
00:00:15,00 --> 00:00:16,50
Finding all the even numbers in an array

9
00:00:16,50 --> 00:00:18,60
of numbers, for example.

10
00:00:18,60 --> 00:00:21,10
JavaScript's built-in every and some functions

11
00:00:21,10 --> 00:00:23,60
are similar to this in that we pass them a function

12
00:00:23,60 --> 00:00:26,60
that returns either true or false for each element.

13
00:00:26,60 --> 00:00:29,40
But the difference is that instead of returning an array,

14
00:00:29,40 --> 00:00:33,60
every and some return a single Boolean, true or false.

15
00:00:33,60 --> 00:00:36,80
Every returns true if and only if a given condition is true

16
00:00:36,80 --> 00:00:39,30
for every single element in our array.

17
00:00:39,30 --> 00:00:41,50
Otherwise, it returns false.

18
00:00:41,50 --> 00:00:44,80
So, for example, if we have an array of all odd numbers

19
00:00:44,80 --> 00:00:46,40
and we call every with a function

20
00:00:46,40 --> 00:00:49,80
that checks if a number is odd, it will return true.

21
00:00:49,80 --> 00:00:52,00
But if there's a single even number in our array,

22
00:00:52,00 --> 00:00:53,90
it will return false.

23
00:00:53,90 --> 00:00:56,30
And, of course, if the array contains all even numbers,

24
00:00:56,30 --> 00:00:59,10
it will return false as well.

25
00:00:59,10 --> 00:01:01,50
Now some, on the other hand, returns true

26
00:01:01,50 --> 00:01:03,60
if the condition is true for any of the elements

27
00:01:03,60 --> 00:01:04,80
in our array.

28
00:01:04,80 --> 00:01:06,60
In other words, it returns false

29
00:01:06,60 --> 00:01:08,50
if and only if the condition is false

30
00:01:08,50 --> 00:01:11,00
for every element in our array.

31
00:01:11,00 --> 00:01:13,40
So taking the same example from before,

32
00:01:13,40 --> 00:01:16,40
if we have an array of all odd numbers and we call some

33
00:01:16,40 --> 00:01:18,60
with a function that checks if a number is odd,

34
00:01:18,60 --> 00:01:21,10
it will return true.

35
00:01:21,10 --> 00:01:23,50
And if we have a single even number in our array,

36
00:01:23,50 --> 00:01:26,30
it will still return true all the way down

37
00:01:26,30 --> 00:01:29,70
to if there's only one odd number in the array,

38
00:01:29,70 --> 00:01:32,20
where it will still return true.

39
00:01:32,20 --> 00:01:34,50
The only time it will return false is if we have an array

40
00:01:34,50 --> 00:01:37,00
of all even numbers.

41
00:01:37,00 --> 00:01:39,80
So then, comparing the output of every and some,

42
00:01:39,80 --> 00:01:42,60
we see that their return values will be exactly the same

43
00:01:42,60 --> 00:01:45,20
if we call them on an array where the condition is either

44
00:01:45,20 --> 00:01:49,30
true for all the elements or false for all the elements.

45
00:01:49,30 --> 00:01:51,90
Where their outputs disagree is when the condition is true

46
00:01:51,90 --> 00:01:54,50
for some intermediate number of elements.

47
00:01:54,50 --> 00:01:59,40
This is where some returns true and every returns false.

48
00:01:59,40 --> 00:02:00,50
It's worth noting that while

49
00:02:00,50 --> 00:02:03,10
there's no built in none function in JavaScript

50
00:02:03,10 --> 00:02:04,80
to explicitly test if a condition

51
00:02:04,80 --> 00:02:07,30
isn't true for any elements in the array,

52
00:02:07,30 --> 00:02:09,30
we can easily get this same functionality

53
00:02:09,30 --> 00:02:12,90
using the not operator along with the some function.

54
00:02:12,90 --> 00:02:14,10
This will return true

55
00:02:14,10 --> 00:02:18,00
only if a function is false for every element.

56
00:02:18,00 --> 00:02:19,20
Let's look at a quick example

57
00:02:19,20 --> 00:02:21,30
of how we might use these functions.

58
00:02:21,30 --> 00:02:23,60
Imagine that we're writing a payroll program,

59
00:02:23,60 --> 00:02:25,20
and we have an array of employee data

60
00:02:25,20 --> 00:02:27,40
with name and salary information

61
00:02:27,40 --> 00:02:29,40
and we want to find out if any of our employees

62
00:02:29,40 --> 00:02:31,70
makes more than a million dollars a year.

63
00:02:31,70 --> 00:02:34,10
With some, this is pretty trivial.

64
00:02:34,10 --> 00:02:37,10
We create a function called makesMoreThanOneMillion,

65
00:02:37,10 --> 00:02:41,30
const makesMoreThanOneMillion,

66
00:02:41,30 --> 00:02:45,10
that takes an employee as an argument,

67
00:02:45,10 --> 00:02:48,00
and returns whether or not that employee's salary attribute

68
00:02:48,00 --> 00:02:51,50
is greater than one million.

69
00:02:51,50 --> 00:02:57,30
employee.salary is greater than one million.

70
00:02:57,30 --> 00:03:02,00
Let's put a space in here for readability.

71
00:03:02,00 --> 00:03:04,40
Then, all we have to do is define a new constant.

72
00:03:04,40 --> 00:03:06,30
We'll call it result.

73
00:03:06,30 --> 00:03:12,60
And then, do employees, our array of employee data, .some,

74
00:03:12,60 --> 00:03:17,80
and then, we'll pass our makesMoreThanOneMillion function.

75
00:03:17,80 --> 00:03:23,50
And then, we'll log our result, console.log(result).

76
00:03:23,50 --> 00:03:25,00
If we run our function now,

77
00:03:25,00 --> 00:03:38,50
using npx babel-node Ch03/03_05/Start/examples.js,

78
00:03:38,50 --> 00:03:41,20
we see that our result is true because one of our employees

79
00:03:41,20 --> 00:03:45,60
does make more than a million, right here.

80
00:03:45,60 --> 00:03:48,30
Now, as an example of how every might be used,

81
00:03:48,30 --> 00:03:50,80
let's imagine that we're programming an online form

82
00:03:50,80 --> 00:03:52,10
for a website

83
00:03:52,10 --> 00:03:54,00
and we want to check if the user has filled out

84
00:03:54,00 --> 00:03:57,10
all the fields before letting them continue.

85
00:03:57,10 --> 00:03:59,00
Let's imagine that we have our form values

86
00:03:59,00 --> 00:04:03,90
in an array of strings, like this; const formValues.

87
00:04:03,90 --> 00:04:06,70
And then, some strings that a user might enter into a form;

88
00:04:06,70 --> 00:04:09,20
for example, their first name,

89
00:04:09,20 --> 00:04:16,50
their last name, their state of residence,

90
00:04:16,50 --> 00:04:21,20
and then, maybe they left one of the fields blank.

91
00:04:21,20 --> 00:04:23,20
So all we have to do to make sure that the user

92
00:04:23,20 --> 00:04:26,10
has filled out all the forms is create a function,

93
00:04:26,10 --> 00:04:30,90
we'll call it isNotEmpty, const isNotEmpty,

94
00:04:30,90 --> 00:04:32,40
and we'll simply return whether or not

95
00:04:32,40 --> 00:04:34,30
the string we pass it is empty.

96
00:04:34,30 --> 00:04:35,60
In this case, we can just use

97
00:04:35,60 --> 00:04:37,60
the double exclamation mark notation

98
00:04:37,60 --> 00:04:39,90
to convert our strings into Booleans.

99
00:04:39,90 --> 00:04:41,60
And this works because, in JavaScript,

100
00:04:41,60 --> 00:04:44,30
an empty string is a falsey value.

101
00:04:44,30 --> 00:04:47,40
Now, all we have to do is define a new constant.

102
00:04:47,40 --> 00:04:52,30
We'll call it allFieldsFilled, const allFieldsFilled.

103
00:04:52,30 --> 00:04:57,30
And that will be equal to formvalues.every()

104
00:04:57,30 --> 00:05:03,60
and we'll pass our isNotEmpty function to every.

105
00:05:03,60 --> 00:05:05,80
And finally, we'll log our result,

106
00:05:05,80 --> 00:05:10,70
console.log all fields filled.

107
00:05:10,70 --> 00:05:14,30
And if we run our program again,

108
00:05:14,30 --> 00:05:15,60
we see that it returns false

109
00:05:15,60 --> 00:05:18,10
because we left one of these fields unfilled.

110
00:05:18,10 --> 00:05:23,40
If we fill that in, let's say it's your job title,

111
00:05:23,40 --> 00:05:28,20
and run our program again, we see that it now returns true

112
00:05:28,20 --> 00:05:30,40
because all the fields are filled.

113
00:05:30,40 --> 00:05:32,10
Notice again how readable it is

114
00:05:32,10 --> 00:05:34,80
to use these built-in functions.

115
00:05:34,80 --> 00:05:36,40
In this case the, right-hand side

116
00:05:36,40 --> 00:05:38,20
reads almost like a function.

117
00:05:38,20 --> 00:05:42,60
We want to check that every form value isn't empty.

118
00:05:42,60 --> 00:05:44,20
Again, this is just one of the benefits

119
00:05:44,20 --> 00:05:46,00
that functional programming gives us.

