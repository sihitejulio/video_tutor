1
00:00:00,50 --> 00:00:01,70
- [Instructor] The first of JavaScript's

2
00:00:01,70 --> 00:00:02,70
built-in array functions

3
00:00:02,70 --> 00:00:05,40
we're going to be talking about is called map.

4
00:00:05,40 --> 00:00:08,00
Quite often it happens that we have an array of data

5
00:00:08,00 --> 00:00:10,40
and we want to convert each of the individual elements

6
00:00:10,40 --> 00:00:12,50
in the array to some other form.

7
00:00:12,50 --> 00:00:14,50
For example, we might have an array of numbers

8
00:00:14,50 --> 00:00:17,50
and we might want to double all the numbers in the array.

9
00:00:17,50 --> 00:00:19,80
Or we might want to convert an array of inch measurements

10
00:00:19,80 --> 00:00:22,50
to an array of centimeter measurements.

11
00:00:22,50 --> 00:00:24,50
Or we might have an array of person objects

12
00:00:24,50 --> 00:00:26,80
with name, age, and job attributes,

13
00:00:26,80 --> 00:00:28,90
and maybe a lot more data as well.

14
00:00:28,90 --> 00:00:30,70
And we want to convert this data into an array

15
00:00:30,70 --> 00:00:33,80
that contains only the people's names.

16
00:00:33,80 --> 00:00:35,10
The typical way of doing this,

17
00:00:35,10 --> 00:00:37,40
and the way that many of us first learned how to do it,

18
00:00:37,40 --> 00:00:39,60
was to use a for loop and an index variable

19
00:00:39,60 --> 00:00:41,90
to loop through all the elements in the array,

20
00:00:41,90 --> 00:00:45,30
and push modified elements onto a new array.

21
00:00:45,30 --> 00:00:47,60
Or worse, we just modify the elements in place,

22
00:00:47,60 --> 00:00:50,10
which destroys the original data.

23
00:00:50,10 --> 00:00:53,00
However, as many of us know, this way can very easily

24
00:00:53,00 --> 00:00:55,70
lead to bugs, especially as the body of the for loop

25
00:00:55,70 --> 00:00:58,20
gets bigger and more complex.

26
00:00:58,20 --> 00:01:01,20
Fortunately for us, JavaScript provides a much easier,

27
00:01:01,20 --> 00:01:03,60
cleaner, and more functional way of doing this using its

28
00:01:03,60 --> 00:01:07,50
built-in map function that we can call on any array.

29
00:01:07,50 --> 00:01:09,90
The way we use map is by calling it on an array

30
00:01:09,90 --> 00:01:12,50
and passing it some function we want to apply

31
00:01:12,50 --> 00:01:14,70
to each element in the array.

32
00:01:14,70 --> 00:01:16,90
Map then returns another array that contains the

33
00:01:16,90 --> 00:01:19,40
return values of the function for each element

34
00:01:19,40 --> 00:01:21,30
in our original array.

35
00:01:21,30 --> 00:01:24,00
So in other words, it takes each element and maps it

36
00:01:24,00 --> 00:01:27,30
to the return value of the function we give it.

37
00:01:27,30 --> 00:01:28,70
If this function does something like

38
00:01:28,70 --> 00:01:30,70
square a number, for example,

39
00:01:30,70 --> 00:01:32,50
map will return a new array where each of

40
00:01:32,50 --> 00:01:35,20
the numbers has been squared.

41
00:01:35,20 --> 00:01:37,80
Now, before we continue there's one important thing to note

42
00:01:37,80 --> 00:01:40,30
about the built-in array functions.

43
00:01:40,30 --> 00:01:43,20
Some functions like map don't actually change the original

44
00:01:43,20 --> 00:01:45,30
array we're calling the functions on.

45
00:01:45,30 --> 00:01:48,00
They just return a modified copy.

46
00:01:48,00 --> 00:01:50,10
So just calling map on an array, for example,

47
00:01:50,10 --> 00:01:52,80
without defining another constant to hold the result,

48
00:01:52,80 --> 00:01:55,10
essentially does nothing.

49
00:01:55,10 --> 00:01:57,20
But keep in mind also that this is not the case

50
00:01:57,20 --> 00:02:00,40
for many other built-in array functions in JavaScript.

51
00:02:00,40 --> 00:02:02,30
The reverse function, for example,

52
00:02:02,30 --> 00:02:04,60
if we call numbers dot reverse,

53
00:02:04,60 --> 00:02:09,30
actually does modify the original array that you call it on.

54
00:02:09,30 --> 00:02:11,00
So for example, if we log numbers now,

55
00:02:11,00 --> 00:02:14,00
console dot log, numbers,

56
00:02:14,00 --> 00:02:16,40
and run our program,

57
00:02:16,40 --> 00:02:21,00
npx babel dash node c h zero three,

58
00:02:21,00 --> 00:02:25,40
slash zero three, underscore zero three, slash start,

59
00:02:25,40 --> 00:02:29,90
slash examples dot js, and run it.

60
00:02:29,90 --> 00:02:32,30
We see that reverse has actually mutated our

61
00:02:32,30 --> 00:02:35,70
original array, which obviously we don't want.

62
00:02:35,70 --> 00:02:37,70
And if you're using ESLint to warn you of

63
00:02:37,70 --> 00:02:39,20
unwanted mutation in your code,

64
00:02:39,20 --> 00:02:41,70
as we talked about in a previous video,

65
00:02:41,70 --> 00:02:43,00
you should be able to guard yourself

66
00:02:43,00 --> 00:02:44,80
against this fairly easily.

67
00:02:44,80 --> 00:02:49,30
And if you're not using ESLint, well, be careful.

68
00:02:49,30 --> 00:02:51,60
So earlier in the video I mentioned a few examples

69
00:02:51,60 --> 00:02:54,20
of how map might be used in our programs.

70
00:02:54,20 --> 00:02:55,90
Let's go through an example to solidify

71
00:02:55,90 --> 00:02:59,60
our understanding of what map looks like in code.

72
00:02:59,60 --> 00:03:02,00
Again, the typical procedural way of doing this

73
00:03:02,00 --> 00:03:03,70
is something like this.

74
00:03:03,70 --> 00:03:05,70
We start off with our numbers array,

75
00:03:05,70 --> 00:03:11,20
we create another array, we'll call it doubled numbers

76
00:03:11,20 --> 00:03:13,20
and make that an empty array.

77
00:03:13,20 --> 00:03:14,90
And then we'll write a for loop that cycles

78
00:03:14,90 --> 00:03:17,80
through each element in the array, does something to it,

79
00:03:17,80 --> 00:03:21,00
and pushes it on to the new array, like this.

80
00:03:21,00 --> 00:03:24,70
For let i equals zero,

81
00:03:24,70 --> 00:03:28,60
i is less than numbers dot length,

82
00:03:28,60 --> 00:03:33,50
i plus plus, and then doubled numbers dot push,

83
00:03:33,50 --> 00:03:40,90
the new element, which will be numbers i, times two.

84
00:03:40,90 --> 00:03:43,20
Now this works and it's not necessarily a bad way

85
00:03:43,20 --> 00:03:45,50
of doing it but the main problem here is

86
00:03:45,50 --> 00:03:48,60
readable and conceptual purity.

87
00:03:48,60 --> 00:03:51,00
If we want to double all the numbers in an array,

88
00:03:51,00 --> 00:03:53,40
we shouldn't be forced to worry about indexing

89
00:03:53,40 --> 00:03:56,10
and array lengths and off by one errors.

90
00:03:56,10 --> 00:03:58,10
We should be able to program in a way that allows us

91
00:03:58,10 --> 00:04:00,60
to simply specify what it is we want.

92
00:04:00,60 --> 00:04:03,30
Instead of how to compute the result.

93
00:04:03,30 --> 00:04:05,10
And that's exactly what functional programming

94
00:04:05,10 --> 00:04:06,50
allows us to do.

95
00:04:06,50 --> 00:04:08,00
Let's look at how we can do the same thing

96
00:04:08,00 --> 00:04:10,00
in a functional way.

97
00:04:10,00 --> 00:04:11,50
We're going to re-write this functionality

98
00:04:11,50 --> 00:04:13,20
using the map function.

99
00:04:13,20 --> 00:04:16,60
First we're going to write a function called double.

100
00:04:16,60 --> 00:04:19,40
And this function is simply going to take one argument

101
00:04:19,40 --> 00:04:22,20
and double it.

102
00:04:22,20 --> 00:04:25,80
And all we have to do now is define a new constant.

103
00:04:25,80 --> 00:04:28,60
Const, doubled numbers,

104
00:04:28,60 --> 00:04:32,30
and call the map function on our numbers array.

105
00:04:32,30 --> 00:04:33,90
Numbers dot map.

106
00:04:33,90 --> 00:04:35,50
And finally we're going to pass our new

107
00:04:35,50 --> 00:04:37,30
double function to map.

108
00:04:37,30 --> 00:04:40,80
Because we want to double each element in our array.

109
00:04:40,80 --> 00:04:45,20
Let's log this to the console and run our code.

110
00:04:45,20 --> 00:04:51,90
Console dot log doubled numbers.

111
00:04:51,90 --> 00:04:54,20
And we see that it works beautifully with the added benefit

112
00:04:54,20 --> 00:04:56,90
that it's now much easier to read and maintain.

113
00:04:56,90 --> 00:04:58,80
We don't have to worry about indexing

114
00:04:58,80 --> 00:05:00,90
or exceeding the length of our array

115
00:05:00,90 --> 00:05:03,50
since map takes care of all that for us.

116
00:05:03,50 --> 00:05:05,90
All we have to think about now is how we want to

117
00:05:05,90 --> 00:05:09,10
transform each element in our array and express that

118
00:05:09,10 --> 00:05:11,00
in a function like we did with double.

