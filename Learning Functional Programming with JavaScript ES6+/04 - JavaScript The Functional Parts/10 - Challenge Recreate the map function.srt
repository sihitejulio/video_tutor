1
00:00:00,00 --> 00:00:05,60
(upbeat music)

2
00:00:05,60 --> 00:00:06,90
- [Instructor] Now that we've had a lot of practice

3
00:00:06,90 --> 00:00:08,40
with JavaScript's functional parts

4
00:00:08,40 --> 00:00:10,40
such as built in array functions,

5
00:00:10,40 --> 00:00:11,60
let's do a challenge.

6
00:00:11,60 --> 00:00:14,40
The challenge here, to make sure you really understand

7
00:00:14,40 --> 00:00:18,40
how the map function works, is to recreate the map function.

8
00:00:18,40 --> 00:00:20,60
Specifically, what we want to create is a function

9
00:00:20,60 --> 00:00:23,00
called map that takes two arguments.

10
00:00:23,00 --> 00:00:24,90
The first argument is an array

11
00:00:24,90 --> 00:00:26,40
and the second argument is a function

12
00:00:26,40 --> 00:00:29,40
that should be applied to every element in the array.

13
00:00:29,40 --> 00:00:32,00
Your task is to recreate the map function in this way,

14
00:00:32,00 --> 00:00:35,60
obviously without using JavaScript's built in map function.

15
00:00:35,60 --> 00:00:38,00
There are quite a few different solutions to this problem

16
00:00:38,00 --> 00:00:40,80
but one of the easier solutions is to use a for Loop

17
00:00:40,80 --> 00:00:43,40
and one of the harder versions is to use only JavaScript's

18
00:00:43,40 --> 00:00:45,30
built in reduce function.

19
00:00:45,30 --> 00:00:47,00
You'll want to write your code inside the body

20
00:00:47,00 --> 00:00:49,10
of this map function here

21
00:00:49,10 --> 00:00:50,80
and if you've implemented it correctly,

22
00:00:50,80 --> 00:00:52,50
when you run your code, these lines here

23
00:00:52,50 --> 00:00:55,20
should print the correct answers to the console.

24
00:00:55,20 --> 00:00:57,50
This will probably take you about 10 minutes.

25
00:00:57,50 --> 00:01:00,50
Once you've completed it, check out the solution video

26
00:01:00,50 --> 00:01:03,00
where I show you how I solve this challenge.

