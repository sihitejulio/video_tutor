1
00:00:00,50 --> 00:00:02,40
- [Instructor] The next important built-in array function

2
00:00:02,40 --> 00:00:03,90
is called Filter.

3
00:00:03,90 --> 00:00:05,60
Filter is used when you want to find

4
00:00:05,60 --> 00:00:07,40
all of the elements in an array

5
00:00:07,40 --> 00:00:09,60
that fit some kind of criteria.

6
00:00:09,60 --> 00:00:11,50
For example if we have an array of numbers

7
00:00:11,50 --> 00:00:14,60
and we want to get all the numbers from it that are even,

8
00:00:14,60 --> 00:00:16,30
or if we have an array of employee data,

9
00:00:16,30 --> 00:00:18,10
and want to find the employees from this array

10
00:00:18,10 --> 00:00:20,70
that make more than a certain amount per year.

11
00:00:20,70 --> 00:00:23,30
These are the things that Filter can help us with.

12
00:00:23,30 --> 00:00:25,60
The syntax to Filter is similar to Map.

13
00:00:25,60 --> 00:00:29,00
We can call it on any array and then we pass in a function.

14
00:00:29,00 --> 00:00:31,40
The main difference between Filter and Map is the type

15
00:00:31,40 --> 00:00:33,60
of function that we pass into it.

16
00:00:33,60 --> 00:00:35,70
In Map, we pass in a function that returns

17
00:00:35,70 --> 00:00:38,30
a value for each element in the array.

18
00:00:38,30 --> 00:00:40,50
And the return value of this function represents

19
00:00:40,50 --> 00:00:43,80
what the element becomes in our new array.

20
00:00:43,80 --> 00:00:46,60
On the other hand, for Filter, we pass it a function

21
00:00:46,60 --> 00:00:50,00
that returns either true or false for each element.

22
00:00:50,00 --> 00:00:52,20
If the function that we pass returns true

23
00:00:52,20 --> 00:00:54,40
for a given element, then that element's included

24
00:00:54,40 --> 00:00:56,30
in the final array.

25
00:00:56,30 --> 00:00:58,80
Otherwise, it's left out.

26
00:00:58,80 --> 00:01:00,40
Let's look at a few examples of what using

27
00:01:00,40 --> 00:01:01,80
Filter looks like.

28
00:01:01,80 --> 00:01:04,10
For our first example, let's say again that we have

29
00:01:04,10 --> 00:01:05,60
a simple array of numbers and we want

30
00:01:05,60 --> 00:01:08,20
only the even numbers from the array.

31
00:01:08,20 --> 00:01:10,30
Again, the typical procedural way to do this

32
00:01:10,30 --> 00:01:11,90
would be like this,

33
00:01:11,90 --> 00:01:14,40
we define a variable called even numbers,

34
00:01:14,40 --> 00:01:16,60
which is an empty array.

35
00:01:16,60 --> 00:01:18,30
And then we create a for loop to loop through

36
00:01:18,30 --> 00:01:19,50
all our elements.

37
00:01:19,50 --> 00:01:22,30
And if an if-statement is true for a certain element,

38
00:01:22,30 --> 00:01:25,50
we push that element onto our array, like this.

39
00:01:25,50 --> 00:01:29,80
First our for loop, for, let i equal zero,

40
00:01:29,80 --> 00:01:36,20
i is less than numbers, dot length, i plus plus.

41
00:01:36,20 --> 00:01:38,50
And since we want to know if our numbers are even,

42
00:01:38,50 --> 00:01:40,70
our if-statement is going to look like this,

43
00:01:40,70 --> 00:01:48,30
if numbers i, modulus two is equal to zero

44
00:01:48,30 --> 00:01:51,10
this is just a way of testing if a number is even

45
00:01:51,10 --> 00:01:53,50
and if it is even we want to push this number

46
00:01:53,50 --> 00:01:55,90
onto our even numbers array like this.

47
00:01:55,90 --> 00:01:59,90
Even numbers dot push

48
00:01:59,90 --> 00:02:03,50
numbers i and again this big for loop

49
00:02:03,50 --> 00:02:06,70
is pretty complicated for such a simple task.

50
00:02:06,70 --> 00:02:10,30
Let's see how to simplify it using Filter.

51
00:02:10,30 --> 00:02:12,60
So first of all we want to create a function,

52
00:02:12,60 --> 00:02:17,20
we'll call it is even and take this condition

53
00:02:17,20 --> 00:02:19,50
from the if statement.

54
00:02:19,50 --> 00:02:21,90
And then instead of numbers i we just want to say x

55
00:02:21,90 --> 00:02:24,90
for the argument we pass in.

56
00:02:24,90 --> 00:02:27,00
Now instead of this big for loop

57
00:02:27,00 --> 00:02:32,20
we can simply say const even numbers

58
00:02:32,20 --> 00:02:39,00
equals numbers dot filter is even.

59
00:02:39,00 --> 00:02:43,20
And if we print our even numbers array to the console,

60
00:02:43,20 --> 00:02:46,40
console dot log even numbers

61
00:02:46,40 --> 00:02:50,30
and run our code npx babel node

62
00:02:50,30 --> 00:02:56,10
Ch zero three zero three underscore zero four slash start

63
00:02:56,10 --> 00:02:59,30
slash examples dot js

64
00:02:59,30 --> 00:03:01,20
we see that it simply returns the even numbers

65
00:03:01,20 --> 00:03:04,70
from our array and note here that it's not necessary

66
00:03:04,70 --> 00:03:07,20
for us to define is even as a separate function

67
00:03:07,20 --> 00:03:09,20
before we pass it to Filter,

68
00:03:09,20 --> 00:03:11,70
we could just as easily write it as an anonymous function

69
00:03:11,70 --> 00:03:13,50
inside the parentheses of Filter

70
00:03:13,50 --> 00:03:16,70
and that would look like this

71
00:03:16,70 --> 00:03:20,40
we say x arrow and then the condition

72
00:03:20,40 --> 00:03:24,70
x modulus two is equal to zero.

73
00:03:24,70 --> 00:03:28,10
Again this is just checking if a number is even

74
00:03:28,10 --> 00:03:31,10
and if we run our code again

75
00:03:31,10 --> 00:03:33,50
we see that it works just the same.

76
00:03:33,50 --> 00:03:36,80
What if we have an array of words like this

77
00:03:36,80 --> 00:03:40,90
const words

78
00:03:40,90 --> 00:03:43,70
hello,

79
00:03:43,70 --> 00:03:45,30
goodbye,

80
00:03:45,30 --> 00:03:47,10
the,

81
00:03:47,10 --> 00:03:51,40
and some other word like Antarctica

82
00:03:51,40 --> 00:03:53,30
and let's say we want to find all the words

83
00:03:53,30 --> 00:03:55,90
from this array that are longer than five letters.

84
00:03:55,90 --> 00:03:58,00
Now I'm not going to show you the for loop way

85
00:03:58,00 --> 00:03:59,70
of doing this, but you're free to write it out

86
00:03:59,70 --> 00:04:01,50
for yourself if you find that it helps you think

87
00:04:01,50 --> 00:04:03,40
about it more clearly.

88
00:04:03,40 --> 00:04:06,20
So first what we'll do is define a function called

89
00:04:06,20 --> 00:04:10,80
is longer than five.

90
00:04:10,80 --> 00:04:14,30
Const is longer than five

91
00:04:14,30 --> 00:04:17,00
and the argument we'll call it word

92
00:04:17,00 --> 00:04:20,10
and we simply want a return if the word

93
00:04:20,10 --> 00:04:24,30
dot length is greater than five

94
00:04:24,30 --> 00:04:28,60
and then we can simply say const long words

95
00:04:28,60 --> 00:04:32,00
equals words dot filter

96
00:04:32,00 --> 00:04:36,40
is longer than five.

97
00:04:36,40 --> 00:04:38,50
And let's delete this console dot log statement

98
00:04:38,50 --> 00:04:43,00
so that it doesn't clutter up our console

99
00:04:43,00 --> 00:04:45,90
and then if we log this long words constant,

100
00:04:45,90 --> 00:04:50,20
console dot log long words

101
00:04:50,20 --> 00:04:53,50
and run our code, we see that we get the two words

102
00:04:53,50 --> 00:04:54,80
from our words array

103
00:04:54,80 --> 00:04:57,20
that are longer than five letters.

104
00:04:57,20 --> 00:04:59,20
Now those of you who watched the previous video

105
00:04:59,20 --> 00:05:00,70
about returning functions,

106
00:05:00,70 --> 00:05:03,30
may have noticed that our is longer than five function

107
00:05:03,30 --> 00:05:05,20
is strangely specific.

108
00:05:05,20 --> 00:05:06,00
Why five?

109
00:05:06,00 --> 00:05:08,30
Why not four or six or ten?

110
00:05:08,30 --> 00:05:10,70
It's conceivable that in a larger program

111
00:05:10,70 --> 00:05:11,80
there might be many places

112
00:05:11,80 --> 00:05:13,60
where we want to do this kind of filtering

113
00:05:13,60 --> 00:05:15,10
with different lengths.

114
00:05:15,10 --> 00:05:17,20
So let's write ourselves a higher order function

115
00:05:17,20 --> 00:05:20,00
that we can use to do this with minimal code.

116
00:05:20,00 --> 00:05:23,00
What we're going to do here is write a function.

117
00:05:23,00 --> 00:05:27,30
We'll call it create length test

118
00:05:27,30 --> 00:05:31,30
const create length test

119
00:05:31,30 --> 00:05:33,20
and this function is going to take an argument

120
00:05:33,20 --> 00:05:34,60
that specifies the length

121
00:05:34,60 --> 00:05:38,20
that we want the function we return to test for.

122
00:05:38,20 --> 00:05:40,10
For example if we want to create a function

123
00:05:40,10 --> 00:05:42,00
that tests if a string is longer than four

124
00:05:42,00 --> 00:05:45,50
or longer than five or longer than ten et cetera.

125
00:05:45,50 --> 00:05:50,00
We'll call that argument min length.

126
00:05:50,00 --> 00:05:51,60
And finally we're going to return a function

127
00:05:51,60 --> 00:05:53,10
that looks just like our original

128
00:05:53,10 --> 00:05:56,60
is longer than five function

129
00:05:56,60 --> 00:05:58,70
we'll copy and paste that

130
00:05:58,70 --> 00:06:01,00
except instead of testing if the string length

131
00:06:01,00 --> 00:06:04,00
is greater than a specific number, five,

132
00:06:04,00 --> 00:06:05,40
we're going to test if the string length

133
00:06:05,40 --> 00:06:09,70
is greater than the min length argument.

134
00:06:09,70 --> 00:06:12,50
Like this.

135
00:06:12,50 --> 00:06:17,00
And we'll add some spacing in here for readability.

136
00:06:17,00 --> 00:06:19,20
And now what we can do is instead of defining

137
00:06:19,20 --> 00:06:22,60
our is longer than five function like this,

138
00:06:22,60 --> 00:06:25,20
we can simply say that is longer than five

139
00:06:25,20 --> 00:06:29,50
is equal to create length test five

140
00:06:29,50 --> 00:06:34,60
and we can also simply cut out this middle step here

141
00:06:34,60 --> 00:06:37,30
and simply say words dot filter

142
00:06:37,30 --> 00:06:42,40
create length test five.

143
00:06:42,40 --> 00:06:46,80
And if we do console dot log long words

144
00:06:46,80 --> 00:06:52,00
and run our program again,

145
00:06:52,00 --> 00:06:53,20
we see that it still behaves

146
00:06:53,20 --> 00:06:56,10
in exactly the same way as before.

147
00:06:56,10 --> 00:06:59,00
And this here is a very nice functional piece of code.

