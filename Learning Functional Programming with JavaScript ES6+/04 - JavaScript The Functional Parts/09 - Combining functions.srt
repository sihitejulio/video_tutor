1
00:00:00,50 --> 00:00:01,90
- [Instructor] So far we've seen several of

2
00:00:01,90 --> 00:00:04,50
JavaScript's numerous built-in array functions,

3
00:00:04,50 --> 00:00:06,40
their syntax and some examples of how

4
00:00:06,40 --> 00:00:08,90
they might be used in a code base.

5
00:00:08,90 --> 00:00:11,60
These functions are certainly very powerful on their own,

6
00:00:11,60 --> 00:00:14,20
and each contributes a great deal towards simplifying

7
00:00:14,20 --> 00:00:16,50
and increasing readability in our code.

8
00:00:16,50 --> 00:00:19,30
The really exciting part, however, is that these functions

9
00:00:19,30 --> 00:00:21,00
can all be combined to give us

10
00:00:21,00 --> 00:00:23,10
an incredible amount of functionality.

11
00:00:23,10 --> 00:00:25,00
In this video we're going to walk through

12
00:00:25,00 --> 00:00:28,00
an example of how we can do this.

13
00:00:28,00 --> 00:00:30,20
The example we're going to look at is this.

14
00:00:30,20 --> 00:00:32,70
Let's say we have an array of employee data,

15
00:00:32,70 --> 00:00:36,60
each with name, age, job title and salary attributes.

16
00:00:36,60 --> 00:00:39,10
Now there's all sorts of interesting information that can be

17
00:00:39,10 --> 00:00:42,30
deduced from a data set like this and we're going to examine

18
00:00:42,30 --> 00:00:44,70
a few ways that we can discover this information

19
00:00:44,70 --> 00:00:47,70
using the built-in array functions we've learned.

20
00:00:47,70 --> 00:00:50,40
What we're going to do is calculate how the average salary

21
00:00:50,40 --> 00:00:53,00
of the developers in our data set compares to the

22
00:00:53,00 --> 00:00:56,30
average salary of the other people in our data set.

23
00:00:56,30 --> 00:01:00,10
First, let's collapse this so that it's more readable.

24
00:01:00,10 --> 00:01:01,80
The first array function we're going to use

25
00:01:01,80 --> 00:01:04,10
for this calculation is filter.

26
00:01:04,10 --> 00:01:05,80
We want to use filter to separate

27
00:01:05,80 --> 00:01:08,10
the developers from the non-developers.

28
00:01:08,10 --> 00:01:09,80
And the way we do this is by defining

29
00:01:09,80 --> 00:01:12,20
a new constant; we'll call it developers.

30
00:01:12,20 --> 00:01:14,80
Const developers.

31
00:01:14,80 --> 00:01:19,10
And we're going to call filter on our employees array.

32
00:01:19,10 --> 00:01:21,50
Employees dot filter.

33
00:01:21,50 --> 00:01:24,20
Since we want filter to give us an array containing only the

34
00:01:24,20 --> 00:01:27,50
employees from our array whose profession is developer,

35
00:01:27,50 --> 00:01:30,70
we'll simply define an anonymous function here

36
00:01:30,70 --> 00:01:33,10
which returns whether or not an employee's profession is

37
00:01:33,10 --> 00:01:36,00
equal to developer, and that'll look like this.

38
00:01:36,00 --> 00:01:39,20
The argument we use will be called employee,

39
00:01:39,20 --> 00:01:40,70
and we simply want to return whether

40
00:01:40,70 --> 00:01:47,30
employee dot job title is equal to developer.

41
00:01:47,30 --> 00:01:48,70
Let's print out this array to make sure

42
00:01:48,70 --> 00:01:50,50
we're on the right track so far.

43
00:01:50,50 --> 00:01:55,70
Console dot log, developers.

44
00:01:55,70 --> 00:02:00,00
Now if we run our program,

45
00:02:00,00 --> 00:02:01,80
we see that we have an array that contains all the

46
00:02:01,80 --> 00:02:07,30
developers from our original employees array.

47
00:02:07,30 --> 00:02:10,60
And this is exactly what we want, so let's move on.

48
00:02:10,60 --> 00:02:12,80
The next thing we're going to do is transform our array of

49
00:02:12,80 --> 00:02:15,20
developer objects into an array of only

50
00:02:15,20 --> 00:02:19,50
their salary attributes, and to do this we'll use map.

51
00:02:19,50 --> 00:02:22,80
So what we'll do again is define a new constant.

52
00:02:22,80 --> 00:02:25,30
We'll call it developer salaries.

53
00:02:25,30 --> 00:02:29,30
And we'll call map on our developers array.

54
00:02:29,30 --> 00:02:32,30
For the function we pass to map we just wanted to take

55
00:02:32,30 --> 00:02:35,90
each developer object and map it to their salary attribute.

56
00:02:35,90 --> 00:02:37,80
And that'll look like this.

57
00:02:37,80 --> 00:02:41,10
We'll call the argument of our function developer,

58
00:02:41,10 --> 00:02:45,80
and we'll simply return developer dot salary.

59
00:02:45,80 --> 00:02:47,70
And the final thing we have to do is calculate

60
00:02:47,70 --> 00:02:51,80
the average of the values in our developer salaries array.

61
00:02:51,80 --> 00:02:55,10
The first step of this is to calculate the sum of the array.

62
00:02:55,10 --> 00:02:56,80
We went over this in a previous video

63
00:02:56,80 --> 00:02:59,30
so I won't spend too much time discussing it,

64
00:02:59,30 --> 00:03:00,50
but in order to find the sum

65
00:03:00,50 --> 00:03:02,60
of all the values in this array,

66
00:03:02,60 --> 00:03:05,70
all we need to do is define a new constant.

67
00:03:05,70 --> 00:03:10,40
We'll call it total developer salaries.

68
00:03:10,40 --> 00:03:14,10
And then call reduce on our developer salaries array.

69
00:03:14,10 --> 00:03:17,20
Developer salaries dot reduce.

70
00:03:17,20 --> 00:03:19,30
And remember that the function that we pass to reduce

71
00:03:19,30 --> 00:03:23,80
is going to have two arguments: our accumulator argument and x,

72
00:03:23,80 --> 00:03:26,80
which stands for each of the elements in our array,

73
00:03:26,80 --> 00:03:30,50
and finally we're going to return our accumulator plus x.

74
00:03:30,50 --> 00:03:34,30
And let's not forget to set the starting value to zero.

75
00:03:34,30 --> 00:03:36,20
And the only thing left to do is divide our

76
00:03:36,20 --> 00:03:38,40
total developer salaries, the sum of all

77
00:03:38,40 --> 00:03:42,70
the developer salaries, by the total number of salaries.

78
00:03:42,70 --> 00:03:50,20
So we'll define a constant called average developer salary

79
00:03:50,20 --> 00:03:54,20
and that'll be equal to total developer salaries

80
00:03:54,20 --> 00:03:57,60
divided by the length of developer salaries.

81
00:03:57,60 --> 00:04:02,60
Developer salaries dot length.

82
00:04:02,60 --> 00:04:04,30
And this should give us our answer.

83
00:04:04,30 --> 00:04:06,40
Let's log it to the console.

84
00:04:06,40 --> 00:04:11,60
Console dot log, average Developer Salary.

85
00:04:11,60 --> 00:04:14,70
And run our code.

86
00:04:14,70 --> 00:04:17,30
And the result we get is the average salary of all

87
00:04:17,30 --> 00:04:21,00
the developers in our original employees array.

88
00:04:21,00 --> 00:04:23,80
Now that we've found the average salary of our developers,

89
00:04:23,80 --> 00:04:26,90
let's find the average salary of our non developers.

90
00:04:26,90 --> 00:04:28,80
So the first thing we're going to do

91
00:04:28,80 --> 00:04:33,70
is to find a constant called non-developers

92
00:04:33,70 --> 00:04:36,80
and call filter on employees.

93
00:04:36,80 --> 00:04:39,10
And we want to get an array that contains the employees

94
00:04:39,10 --> 00:04:41,70
whose job title is not developer.

95
00:04:41,70 --> 00:04:43,60
And that'll look like this.

96
00:04:43,60 --> 00:04:48,20
We have our employee argument and we want to return

97
00:04:48,20 --> 00:04:55,70
whether employee dot job title is not equal to developer.

98
00:04:55,70 --> 00:04:58,30
And the next step, as we did above, is to map this array

99
00:04:58,30 --> 00:05:02,40
of non-developers to simply an array of their salaries.

100
00:05:02,40 --> 00:05:07,00
So we'll define a new constant called non-developer salaries

101
00:05:07,00 --> 00:05:12,00
and we'll set it equal to non-developers dot map

102
00:05:12,00 --> 00:05:15,40
and for the argument we'll just say non dev

103
00:05:15,40 --> 00:05:18,30
and we want to map the non-developer

104
00:05:18,30 --> 00:05:19,90
to their salary attribute.

105
00:05:19,90 --> 00:05:23,90
So non dev dot salary.

106
00:05:23,90 --> 00:05:25,50
After that we want to find the sum

107
00:05:25,50 --> 00:05:27,60
of our non-developer salaries.

108
00:05:27,60 --> 00:05:32,60
So we'll call that total non-developer salaries

109
00:05:32,60 --> 00:05:36,90
and that'll be equal to non developer salaries

110
00:05:36,90 --> 00:05:41,60
dot reduce and this will be the same as we had above.

111
00:05:41,60 --> 00:05:47,90
Accumulator, x, and we want to return accumulator plus x.

112
00:05:47,90 --> 00:05:51,20
And don't forget the starting value.

113
00:05:51,20 --> 00:05:53,20
And then just as we did above,

114
00:05:53,20 --> 00:05:57,70
in order to get our average non-developer salary

115
00:05:57,70 --> 00:06:03,50
we simply divide the total non-developer salaries

116
00:06:03,50 --> 00:06:06,60
by the length of the non-developer salaries array.

117
00:06:06,60 --> 00:06:11,80
Non-developer salaries dot length,

118
00:06:11,80 --> 00:06:13,20
and that should give us our result.

119
00:06:13,20 --> 00:06:15,00
Let's log it to the console.

120
00:06:15,00 --> 00:06:22,00
Console dot log, average non-developer salary.

121
00:06:22,00 --> 00:06:26,90
And if we run our code we see that we get the two results.

122
00:06:26,90 --> 00:06:28,80
So we've seen in this video that there's quite a bit

123
00:06:28,80 --> 00:06:31,00
we can do by combining JavaScript's

124
00:06:31,00 --> 00:06:33,00
built-in array functions.

125
00:06:33,00 --> 00:06:34,90
Compare how simple and straightforward this was

126
00:06:34,90 --> 00:06:37,90
with doing the same task using for-loops.

127
00:06:37,90 --> 00:06:40,00
I'll leave that as an exercise for you.

