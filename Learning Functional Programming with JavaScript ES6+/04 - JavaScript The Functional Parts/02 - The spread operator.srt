1
00:00:00,00 --> 00:00:02,90
- [Instructor] ES6 offers many syntax improvements

2
00:00:02,90 --> 00:00:05,50
over its predecessor CommonJS.

3
00:00:05,50 --> 00:00:07,30
In addition to arrow function syntax,

4
00:00:07,30 --> 00:00:09,30
which we covered in a previous video,

5
00:00:09,30 --> 00:00:10,80
and import export statements,

6
00:00:10,80 --> 00:00:13,90
ES6 provides us with a new syntax that's really helpful

7
00:00:13,90 --> 00:00:18,00
for dealing with JavaScript objects and arrays.

8
00:00:18,00 --> 00:00:19,20
Something that we do a lot of

9
00:00:19,20 --> 00:00:21,30
when doing functional programming in JavaScript

10
00:00:21,30 --> 00:00:23,40
since these are the main ways that our data

11
00:00:23,40 --> 00:00:25,50
is represented in code.

12
00:00:25,50 --> 00:00:29,10
This new syntax is called spread operator syntax.

13
00:00:29,10 --> 00:00:31,70
To see what the spread operator syntax looks like

14
00:00:31,70 --> 00:00:35,20
and how it works, let's imagine that we have two objects.

15
00:00:35,20 --> 00:00:37,40
The first object represents a person.

16
00:00:37,40 --> 00:00:40,90
We have their name, their age and their hair and eye color.

17
00:00:40,90 --> 00:00:42,90
The second object represents career data

18
00:00:42,90 --> 00:00:44,70
that we might have about the person.

19
00:00:44,70 --> 00:00:48,00
Their job title and the company they work for.

20
00:00:48,00 --> 00:00:49,40
What if we wanted to combine these two objects

21
00:00:49,40 --> 00:00:52,20
into one object that had all the same properties

22
00:00:52,20 --> 00:00:54,60
and values as these two objects?

23
00:00:54,60 --> 00:00:57,50
Well, that's exactly what the spread operator helps us do.

24
00:00:57,50 --> 00:00:59,10
Instead of having to loop through each

25
00:00:59,10 --> 00:01:00,60
of the properties of the career data

26
00:01:00,60 --> 00:01:03,20
and set those properties on the person's object,

27
00:01:03,20 --> 00:01:05,50
which would involve a great deal of mutation,

28
00:01:05,50 --> 00:01:09,00
we can simply combine the two like this.

29
00:01:09,00 --> 00:01:11,60
This is what the spread operator syntax looks like.

30
00:01:11,60 --> 00:01:13,30
So, let's define a new object.

31
00:01:13,30 --> 00:01:17,40
We'll call it personWithCareerData.

32
00:01:17,40 --> 00:01:20,50
We'll have the brackets and this is where

33
00:01:20,50 --> 00:01:22,00
we use the spread operator.

34
00:01:22,00 --> 00:01:24,30
We simply put three dots

35
00:01:24,30 --> 00:01:27,80
and then our person object, and then three dots

36
00:01:27,80 --> 00:01:30,40
and then our career data object.

37
00:01:30,40 --> 00:01:33,60
What this does is it creates a new object

38
00:01:33,60 --> 00:01:37,10
with all the properties with both person and career.

39
00:01:37,10 --> 00:01:39,60
If we print this new object

40
00:01:39,60 --> 00:01:43,70
console.log(personWithCareerData)

41
00:01:43,70 --> 00:01:46,10
and then run our code,

42
00:01:46,10 --> 00:01:57,40
npx babel-node Ch03/03_02/Start/examples.js

43
00:01:57,40 --> 00:01:59,50
and run it.

44
00:01:59,50 --> 00:02:01,80
We see that this object has the properties

45
00:02:01,80 --> 00:02:04,10
and values from both of our other objects.

46
00:02:04,10 --> 00:02:06,00
In other words, the spread operator allows us

47
00:02:06,00 --> 00:02:07,80
to, sort of, spread the properties

48
00:02:07,80 --> 00:02:12,50
of one object inside another, as we've done in this example.

49
00:02:12,50 --> 00:02:14,00
Of course, we don't necessarily need

50
00:02:14,00 --> 00:02:17,10
to combine all the properties of both these objects.

51
00:02:17,10 --> 00:02:19,00
For a particular program we're writing

52
00:02:19,00 --> 00:02:21,20
it might be enough just to have the person's name,

53
00:02:21,20 --> 00:02:24,00
along with their career data, which would look like this,

54
00:02:24,00 --> 00:02:28,70
name: person.name.

55
00:02:28,70 --> 00:02:31,20
And if we run it again

56
00:02:31,20 --> 00:02:33,70
we see that our new object now only has

57
00:02:33,70 --> 00:02:36,40
the name, title and company properties.

58
00:02:36,40 --> 00:02:38,60
Note that when using the spread operator

59
00:02:38,60 --> 00:02:40,00
if the objects that we're spreading

60
00:02:40,00 --> 00:02:42,40
have property names in common, for example,

61
00:02:42,40 --> 00:02:44,40
if both the person data and careerData

62
00:02:44,40 --> 00:02:47,60
have a name property, whichever one comes last

63
00:02:47,60 --> 00:02:50,80
will overwrite the ones that come before it.

64
00:02:50,80 --> 00:02:52,40
For example, if we change this back

65
00:02:52,40 --> 00:02:56,90
to just be the spread operator, person.

66
00:02:56,90 --> 00:02:59,00
And we give the career data a different name

67
00:02:59,00 --> 00:03:06,40
than the person data. (computer keys clicking)

68
00:03:06,40 --> 00:03:10,00
Then we run our program again.

69
00:03:10,00 --> 00:03:13,20
We'll see that the name from the careerData overwrites

70
00:03:13,20 --> 00:03:16,30
the name property from the person data.

71
00:03:16,30 --> 00:03:19,10
This can be quite useful when we have a person object

72
00:03:19,10 --> 00:03:21,60
and want to update some of the properties.

73
00:03:21,60 --> 00:03:23,80
What we can do is create an updates object,

74
00:03:23,80 --> 00:03:27,80
const updates, which contains the fields

75
00:03:27,80 --> 00:03:30,80
that we want to update as well as their new values.

76
00:03:30,80 --> 00:03:33,80
For example, if we want to change a person's name,

77
00:03:33,80 --> 00:03:39,80
name: 'James Smith' (computer keys clicking)

78
00:03:39,80 --> 00:03:42,30
we can then create an updated person,

79
00:03:42,30 --> 00:03:45,80
const updatedPerson

80
00:03:45,80 --> 00:03:49,70
and set it equal to the spread operator person

81
00:03:49,70 --> 00:03:52,80
and then the spread operator updates.

82
00:03:52,80 --> 00:03:57,90
If we print this, updatedPerson,

83
00:03:57,90 --> 00:04:01,70
and run our code, we see that it now contains

84
00:04:01,70 --> 00:04:04,00
the updated name property instead of

85
00:04:04,00 --> 00:04:07,20
the original name property.

86
00:04:07,20 --> 00:04:09,50
So far, we've seen that the spread operator

87
00:04:09,50 --> 00:04:11,70
can be used for objects, but it can also

88
00:04:11,70 --> 00:04:13,50
be used with arrays as well.

89
00:04:13,50 --> 00:04:15,70
If we have an array, for example, and we want

90
00:04:15,70 --> 00:04:18,00
to add some elements to it.

91
00:04:18,00 --> 00:04:20,50
Let's create our array, we'll call it numbers,

92
00:04:20,50 --> 00:04:27,40
const numbers, and it'll just be 1, 2, 3, 4, 5.

93
00:04:27,40 --> 00:04:30,00
Instead of using the push method, which mutates

94
00:04:30,00 --> 00:04:33,30
the array it's called on, we can use the spread operator

95
00:04:33,30 --> 00:04:35,60
to combine the old array with the new elements

96
00:04:35,60 --> 00:04:37,20
we want to add to it.

97
00:04:37,20 --> 00:04:39,50
That looks like this.

98
00:04:39,50 --> 00:04:46,10
We'll say, const newNumbers, array and then three dots

99
00:04:46,10 --> 00:04:51,90
and the numbers, and whatever new element we want to add.

100
00:04:51,90 --> 00:04:56,20
If we then print this newNumbers array

101
00:04:56,20 --> 00:04:58,90
and run our code.

102
00:04:58,90 --> 00:05:00,70
We see that the new array now contains

103
00:05:00,70 --> 00:05:02,90
all of the numbers from our original array

104
00:05:02,90 --> 00:05:04,90
as well as whatever numbers we added after

105
00:05:04,90 --> 00:05:06,80
the spread syntax.

106
00:05:06,80 --> 00:05:08,50
Of course, we can also add new elements

107
00:05:08,50 --> 00:05:10,90
to the beginning of the array, just as easily,

108
00:05:10,90 --> 00:05:12,90
by placing the spread operator after the elements

109
00:05:12,90 --> 00:05:14,50
we want to add.

110
00:05:14,50 --> 00:05:19,40
For example, if we wanted to put zero before our array.

111
00:05:19,40 --> 00:05:22,60
And if we run our code again,

112
00:05:22,60 --> 00:05:24,60
we see that our new array now contains zero

113
00:05:24,60 --> 00:05:27,00
at the beginning and then all the numbers

114
00:05:27,00 --> 00:05:30,00
from our original array and then six at the end.

