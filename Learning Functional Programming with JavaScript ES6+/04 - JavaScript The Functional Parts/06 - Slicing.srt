1
00:00:00,50 --> 00:00:01,80
- [Narrator] The next built in array method

2
00:00:01,80 --> 00:00:04,30
we're going to be looking at is called slice.

3
00:00:04,30 --> 00:00:06,60
Unlike the last few built in methods we've seen,

4
00:00:06,60 --> 00:00:08,80
slice is not a higher order function.

5
00:00:08,80 --> 00:00:10,50
It doesn't take a function as an argument,

6
00:00:10,50 --> 00:00:12,80
like map or filter, but it does have some

7
00:00:12,80 --> 00:00:16,20
important uses when working with arrays in JavaScript.

8
00:00:16,20 --> 00:00:17,60
I mentioned earlier in the course

9
00:00:17,60 --> 00:00:18,80
that their are some array functions

10
00:00:18,80 --> 00:00:21,20
that mutant the arrays you call them on.

11
00:00:21,20 --> 00:00:23,60
Sort, which we'll look at in a later video,

12
00:00:23,60 --> 00:00:25,10
is one of those functions.

13
00:00:25,10 --> 00:00:26,60
And we also have push and pop

14
00:00:26,60 --> 00:00:29,30
which add and remove elements from the array respectively,

15
00:00:29,30 --> 00:00:32,10
but also mutate the array.

16
00:00:32,10 --> 00:00:33,50
So as I mentioned, these functions

17
00:00:33,50 --> 00:00:35,70
don't just return a copy of the original array,

18
00:00:35,70 --> 00:00:38,00
they actually mutate the original array.

19
00:00:38,00 --> 00:00:39,80
And this isn't always obvious.

20
00:00:39,80 --> 00:00:41,60
It's for this reason that I strongly recommend

21
00:00:41,60 --> 00:00:43,20
that you use ESLint, which I showed you

22
00:00:43,20 --> 00:00:45,00
how to set up in a previous video,

23
00:00:45,00 --> 00:00:48,20
to catch unintended mutations for you.

24
00:00:48,20 --> 00:00:49,90
So what does this mean for us?

25
00:00:49,90 --> 00:00:51,70
JavaScipt provides these very useful

26
00:00:51,70 --> 00:00:53,20
built in functions for us,

27
00:00:53,20 --> 00:00:56,10
but because functional programming prohibits mutation,

28
00:00:56,10 --> 00:00:58,60
and these functions mutate the arrays we call them on,

29
00:00:58,60 --> 00:01:00,40
we can't use them directly.

30
00:01:00,40 --> 00:01:03,50
So, are we content to just not use these helpful functions?

31
00:01:03,50 --> 00:01:04,90
Of course not.

32
00:01:04,90 --> 00:01:06,90
As it happens, there's a very simple workaround

33
00:01:06,90 --> 00:01:10,00
to turn mutating functions into non-mutating functions.

34
00:01:10,00 --> 00:01:11,20
And the key to this workaround

35
00:01:11,20 --> 00:01:15,00
is another built in function called slice.

36
00:01:15,00 --> 00:01:16,80
So let's take a look at slice.

37
00:01:16,80 --> 00:01:19,40
First of all, what does slice do exactly?

38
00:01:19,40 --> 00:01:21,40
Well, normally slice is used to get a section

39
00:01:21,40 --> 00:01:23,30
of the elements from an array.

40
00:01:23,30 --> 00:01:26,20
For example, if we have an array of numbers zero to 10,

41
00:01:26,20 --> 00:01:29,90
we can get a subset of these numbers by calling slice.

42
00:01:29,90 --> 00:01:33,10
So numbers dot slice.

43
00:01:33,10 --> 00:01:34,70
And the first argument tells slice

44
00:01:34,70 --> 00:01:37,70
which index of the array to start on.

45
00:01:37,70 --> 00:01:39,60
For example, three.

46
00:01:39,60 --> 00:01:41,10
And the second argument tells slice

47
00:01:41,10 --> 00:01:43,10
which index to stop before.

48
00:01:43,10 --> 00:01:44,60
For example, eight, which means

49
00:01:44,60 --> 00:01:47,40
that the last element in our array will be the number seven

50
00:01:47,40 --> 00:01:49,30
in this example.

51
00:01:49,30 --> 00:01:52,80
Now let's just log this to see what the result is.

52
00:01:52,80 --> 00:01:57,10
Console dot log.

53
00:01:57,10 --> 00:02:06,80
And then we'll run our code using babel node.

54
00:02:06,80 --> 00:02:08,40
And here we see that we have a sub set

55
00:02:08,40 --> 00:02:11,80
of our original array, starting at the index three

56
00:02:11,80 --> 00:02:14,90
and ending just before index eight.

57
00:02:14,90 --> 00:02:16,50
Now, fortunately for us,

58
00:02:16,50 --> 00:02:19,10
slice does not mutate the array it's called on.

59
00:02:19,10 --> 00:02:21,30
It returns a copy.

60
00:02:21,30 --> 00:02:23,30
So what if we used slice to simply create

61
00:02:23,30 --> 00:02:25,00
a copy of the array for us,

62
00:02:25,00 --> 00:02:26,70
and then called sort or one of those

63
00:02:26,70 --> 00:02:29,70
other mutating methods on that copy?

64
00:02:29,70 --> 00:02:31,60
All we have to do is take a slice

65
00:02:31,60 --> 00:02:33,70
which starts at the beginning of the array,

66
00:02:33,70 --> 00:02:38,60
at index zero, and includes the whole array.

67
00:02:38,60 --> 00:02:41,30
Well as it happens, the default arguments for slice,

68
00:02:41,30 --> 00:02:44,10
if we simply call slice without any arguments,

69
00:02:44,10 --> 00:02:46,10
do exactly that.

70
00:02:46,10 --> 00:02:50,80
So if we run our code again.

71
00:02:50,80 --> 00:02:54,00
We see that we simply have a copy of the original array.

72
00:02:54,00 --> 00:02:55,40
This is really helpful to use

73
00:02:55,40 --> 00:02:56,60
with built in array functions

74
00:02:56,60 --> 00:02:59,30
that mutate the array by default.

75
00:02:59,30 --> 00:03:01,00
All we have to do to stop JavaScript

76
00:03:01,00 --> 00:03:03,00
from mutating an array with these functions

77
00:03:03,00 --> 00:03:05,20
is to call slice with no arguments,

78
00:03:05,20 --> 00:03:07,50
and then call the mutating function after that.

79
00:03:07,50 --> 00:03:11,20
For example, reverse.

80
00:03:11,20 --> 00:03:16,40
And then if we run our code again.

81
00:03:16,40 --> 00:03:20,20
We see that we have a reversed copy of the array.

82
00:03:20,20 --> 00:03:22,10
In order to see that it's really not mutating

83
00:03:22,10 --> 00:03:27,70
our original array, let's log the original array as well.

84
00:03:27,70 --> 00:03:30,60
We run our code again.

85
00:03:30,60 --> 00:03:33,70
And we see that our original array remained intact.

86
00:03:33,70 --> 00:03:36,40
So slice is very useful for this kind of situation.

87
00:03:36,40 --> 00:03:38,00
You'll see it more in coming videos.

