1
00:00:00,10 --> 00:00:05,60
(upbeat music)

2
00:00:05,60 --> 00:00:06,60
- [Instructor] Okay, so let's take a look

3
00:00:06,60 --> 00:00:08,70
at two possible solutions to the challenge

4
00:00:08,70 --> 00:00:11,40
of recreating the map function in JavaScript.

5
00:00:11,40 --> 00:00:13,60
We'll start with one of the easier possible solutions

6
00:00:13,60 --> 00:00:15,40
of using a for loop.

7
00:00:15,40 --> 00:00:16,90
Inside our function body,

8
00:00:16,90 --> 00:00:19,20
the first thing we want to do is define a new array

9
00:00:19,20 --> 00:00:20,50
so that we don't mutate the array

10
00:00:20,50 --> 00:00:21,90
that we pass into our function.

11
00:00:21,90 --> 00:00:24,00
That would go against functional programming's

12
00:00:24,00 --> 00:00:26,00
core tenet of immutability.

13
00:00:26,00 --> 00:00:29,30
So, let's write let since we'll be modifying this variable

14
00:00:29,30 --> 00:00:31,90
and newArray and the initial value

15
00:00:31,90 --> 00:00:34,10
will just be an empty array.

16
00:00:34,10 --> 00:00:35,30
And what we want to do now

17
00:00:35,30 --> 00:00:37,60
is iterate through all the elements in our array

18
00:00:37,60 --> 00:00:41,30
using the old index method from procedural programming.

19
00:00:41,30 --> 00:00:47,60
So, we'll write for let i for index equal zero,

20
00:00:47,60 --> 00:00:55,30
i is less than array.length and i++.

21
00:00:55,30 --> 00:00:57,30
And for each element in our array,

22
00:00:57,30 --> 00:00:59,10
we want to get the value of the result

23
00:00:59,10 --> 00:01:01,50
that we get when we call our function argument

24
00:01:01,50 --> 00:01:02,50
on the element

25
00:01:02,50 --> 00:01:04,60
and that'll look like this.

26
00:01:04,60 --> 00:01:11,60
Const result equals func array index i.

27
00:01:11,60 --> 00:01:13,90
And then we want to push this result onto the new array

28
00:01:13,90 --> 00:01:15,50
that we defined.

29
00:01:15,50 --> 00:01:21,90
And that'll look like newArray.push result.

30
00:01:21,90 --> 00:01:24,70
And of course we want to make sure we return our new array

31
00:01:24,70 --> 00:01:28,90
once all the elements have been pushed onto it.

32
00:01:28,90 --> 00:01:33,00
Let's test this by running our code.

33
00:01:33,00 --> 00:01:35,40
And we see the results that get printed to the console

34
00:01:35,40 --> 00:01:38,00
are exactly the ones that we were looking for.

35
00:01:38,00 --> 00:01:40,80
So, great, that works.

36
00:01:40,80 --> 00:01:42,80
Now let's move on to one of the harder possible solutions

37
00:01:42,80 --> 00:01:44,30
to this challenge.

38
00:01:44,30 --> 00:01:46,40
Recreating map using only JavaScript's

39
00:01:46,40 --> 00:01:48,70
built-in reduce function.

40
00:01:48,70 --> 00:01:50,10
For the body of our function,

41
00:01:50,10 --> 00:01:51,60
the first and most obvious step

42
00:01:51,60 --> 00:01:54,20
is to call reduce on our array.

43
00:01:54,20 --> 00:01:56,70
Array.reduce.

44
00:01:56,70 --> 00:01:57,70
Let's start off by thinking

45
00:01:57,70 --> 00:02:00,70
about what we want our starting value to look like.

46
00:02:00,70 --> 00:02:03,10
In this case, since we're building up an array,

47
00:02:03,10 --> 00:02:04,30
we're going to want our starting value

48
00:02:04,30 --> 00:02:08,60
to just be an empty array.

49
00:02:08,60 --> 00:02:11,00
Now the accumulator function that we passed to reduce

50
00:02:11,00 --> 00:02:12,40
will look like this.

51
00:02:12,40 --> 00:02:14,00
It'll have two arguments,

52
00:02:14,00 --> 00:02:17,30
the accumulator and the current element in the array,

53
00:02:17,30 --> 00:02:19,50
we'll call it x

54
00:02:19,50 --> 00:02:21,40
and what do we want to return?

55
00:02:21,40 --> 00:02:22,70
Well, we want to return an array

56
00:02:22,70 --> 00:02:23,80
that contains all the elements

57
00:02:23,80 --> 00:02:26,00
that we've accumulated so far.

58
00:02:26,00 --> 00:02:28,90
We'll use the spread operator for that.

59
00:02:28,90 --> 00:02:31,80
Dot dot dot accumulator.

60
00:02:31,80 --> 00:02:33,80
And to this array we want to add the result

61
00:02:33,80 --> 00:02:36,00
that we get when we call our function argument

62
00:02:36,00 --> 00:02:38,80
on the current element like this.

63
00:02:38,80 --> 00:02:41,40
Func x.

64
00:02:41,40 --> 00:02:43,10
And make sure you either remove the brackets

65
00:02:43,10 --> 00:02:44,30
from the function body

66
00:02:44,30 --> 00:02:46,50
or use the return keyword.

67
00:02:46,50 --> 00:02:50,20
Otherwise your map function will always return undefined.

68
00:02:50,20 --> 00:02:51,00
And that's it.

69
00:02:51,00 --> 00:02:54,50
If we run our code,

70
00:02:54,50 --> 00:02:56,70
we see that again we get the correct results printed

71
00:02:56,70 --> 00:02:58,60
to the console.

72
00:02:58,60 --> 00:03:01,70
So, you can see that both of the possible solutions work

73
00:03:01,70 --> 00:03:03,20
and depending on your situation,

74
00:03:03,20 --> 00:03:06,00
either one of the solutions might be better for you.

