1
00:00:00,00 --> 00:00:01,90
- [Narrator] The final built-in array function

2
00:00:01,90 --> 00:00:04,50
that we're going to discuss is called reduce.

3
00:00:04,50 --> 00:00:05,50
The purpose of this function

4
00:00:05,50 --> 00:00:08,70
might not be quite as apparent as map, filtered or sort.

5
00:00:08,70 --> 00:00:11,00
But reduce is definitely just as useful as,

6
00:00:11,00 --> 00:00:14,30
if not more useful than the other array functions.

7
00:00:14,30 --> 00:00:16,90
What reduce allows us to do is take an array of data,

8
00:00:16,90 --> 00:00:19,40
and reduce it down to a single value.

9
00:00:19,40 --> 00:00:21,70
So for example, we could take an array of numbers

10
00:00:21,70 --> 00:00:25,10
and reduce it down to a sum or an average.

11
00:00:25,10 --> 00:00:26,90
Essentially, what reduce does

12
00:00:26,90 --> 00:00:30,20
is starts with an initial value, say zero,

13
00:00:30,20 --> 00:00:31,80
and for each element in the array,

14
00:00:31,80 --> 00:00:34,40
it modifies its initial value in some way

15
00:00:34,40 --> 00:00:36,90
until after we've processed all our elements.

16
00:00:36,90 --> 00:00:38,70
We end up with the result.

17
00:00:38,70 --> 00:00:40,00
In the case of finding the sum

18
00:00:40,00 --> 00:00:41,90
of an array of numbers for example,

19
00:00:41,90 --> 00:00:44,20
we add each element to the initial value

20
00:00:44,20 --> 00:00:47,70
over and over again until we find our final result.

21
00:00:47,70 --> 00:00:49,20
And if we were trying to find the product

22
00:00:49,20 --> 00:00:51,90
of an array of numbers, we'd multiply each element

23
00:00:51,90 --> 00:00:54,20
by the initial value, and so on.

24
00:00:54,20 --> 00:00:56,50
So what's the syntax of reduce?

25
00:00:56,50 --> 00:00:59,30
Reduce, like most of our other built-it array functions,

26
00:00:59,30 --> 00:01:01,20
takes a function as an argument.

27
00:01:01,20 --> 00:01:02,80
The form of this function however

28
00:01:02,80 --> 00:01:04,50
is a little different from what we've seen

29
00:01:04,50 --> 00:01:06,20
with map and filter.

30
00:01:06,20 --> 00:01:09,60
This function in it's simplest form takes two arguments.

31
00:01:09,60 --> 00:01:11,00
The first argument represents

32
00:01:11,00 --> 00:01:12,70
the current value that we've built up

33
00:01:12,70 --> 00:01:14,80
on top of the initial value.

34
00:01:14,80 --> 00:01:16,80
We call this argument the accumulator,

35
00:01:16,80 --> 00:01:18,70
and by convention, we just use

36
00:01:18,70 --> 00:01:22,10
the abbreviation acc in the function's arguments.

37
00:01:22,10 --> 00:01:23,60
The second argument represents

38
00:01:23,60 --> 00:01:26,10
the current element of the array that we're looking at,

39
00:01:26,10 --> 00:01:29,00
much like in the map or filter functions.

40
00:01:29,00 --> 00:01:31,70
And the purpose of this function that we pass to reduce

41
00:01:31,70 --> 00:01:33,40
is to tell reduce how to combine

42
00:01:33,40 --> 00:01:36,10
the current value of the accumulated variable,

43
00:01:36,10 --> 00:01:37,70
what we've got so far,

44
00:01:37,70 --> 00:01:40,70
with the current element that we're looking at.

45
00:01:40,70 --> 00:01:41,80
When we want to calculate

46
00:01:41,80 --> 00:01:44,10
the sum of an array of numbers for example,

47
00:01:44,10 --> 00:01:45,40
our function will simply return

48
00:01:45,40 --> 00:01:49,10
the accumulator argument, plus the current element.

49
00:01:49,10 --> 00:01:50,80
There's one last thing we have to remember

50
00:01:50,80 --> 00:01:52,30
about the reduce function.

51
00:01:52,30 --> 00:01:53,50
In addition to the function

52
00:01:53,50 --> 00:01:55,20
that we pass it as an argument,

53
00:01:55,20 --> 00:01:57,70
we have to remember to provide it with another argument-

54
00:01:57,70 --> 00:01:59,60
the starting value.

55
00:01:59,60 --> 00:02:01,70
This is the value that reduce will start with

56
00:02:01,70 --> 00:02:04,10
when looking at the elements in our array.

57
00:02:04,10 --> 00:02:06,40
In other words, when the function we pass to reduce

58
00:02:06,40 --> 00:02:08,60
is called for the very first time,

59
00:02:08,60 --> 00:02:09,80
our accumulator argument

60
00:02:09,80 --> 00:02:11,90
will be equal to the starting value,

61
00:02:11,90 --> 00:02:13,50
and the second argument will be equal

62
00:02:13,50 --> 00:02:16,70
to the first element in our array.

63
00:02:16,70 --> 00:02:18,30
Since people often find reduce

64
00:02:18,30 --> 00:02:21,30
the hardest array function to grasp conceptually,

65
00:02:21,30 --> 00:02:23,20
let's take a look at what's happening exactly

66
00:02:23,20 --> 00:02:25,40
when we call reduce on an array.

67
00:02:25,40 --> 00:02:26,80
We're going to use the simple example

68
00:02:26,80 --> 00:02:29,60
of finding the sum of an array of numbers.

69
00:02:29,60 --> 00:02:35,90
So first let's say, const sum = numbers.reduce

70
00:02:35,90 --> 00:02:38,10
and when we're using reduce to find the sum,

71
00:02:38,10 --> 00:02:42,00
we'll want the starting value to be zero.

72
00:02:42,00 --> 00:02:43,10
Now let's define our function

73
00:02:43,10 --> 00:02:45,70
that will combine our numbers to get the sum.

74
00:02:45,70 --> 00:02:47,00
As I mentioned before,

75
00:02:47,00 --> 00:02:50,00
we usually call the first element acc.

76
00:02:50,00 --> 00:02:56,00
And for the second element, we're just going to use x.

77
00:02:56,00 --> 00:02:58,00
Now what we're going to do inside this function

78
00:02:58,00 --> 00:03:01,20
is logout acc and x

79
00:03:01,20 --> 00:03:02,20
so that we can better see

80
00:03:02,20 --> 00:03:05,10
exactly what's happening when we call reduce.

81
00:03:05,10 --> 00:03:08,10
So let's say console that log,

82
00:03:08,10 --> 00:03:11,30
and then we'll use backticks instead of single quotes,

83
00:03:11,30 --> 00:03:12,10
and then we'll logout

84
00:03:12,10 --> 00:03:15,20
the value of our accumulative argument.

85
00:03:15,20 --> 00:03:21,40
Acc is, and then we'll say the value of acc.

86
00:03:21,40 --> 00:03:24,80
And then we'll do the same thing for x.

87
00:03:24,80 --> 00:03:30,50
Console.log, backticks, x is,

88
00:03:30,50 --> 00:03:33,90
and then the value of x.

89
00:03:33,90 --> 00:03:36,40
And finally, what we actually want to return

90
00:03:36,40 --> 00:03:39,10
is simply the accumulator,

91
00:03:39,10 --> 00:03:43,00
plus whatever element we're at, x.

92
00:03:43,00 --> 00:03:46,40
And if we run our code now,

93
00:03:46,40 --> 00:03:49,60
let's take a look at the output.

94
00:03:49,60 --> 00:03:52,70
So here we see that the first time our function is called,

95
00:03:52,70 --> 00:03:54,60
our accumulator argument will be zero,

96
00:03:54,60 --> 00:03:55,90
our starting value,

97
00:03:55,90 --> 00:03:57,70
and our second argument will be 5,

98
00:03:57,70 --> 00:04:00,00
the first number in our array.

99
00:04:00,00 --> 00:04:02,20
The next time our function is called,

100
00:04:02,20 --> 00:04:05,70
acc is the sum of our previous two numbers,

101
00:04:05,70 --> 00:04:10,10
and x is the second number in our array. So,

102
00:04:10,10 --> 00:04:12,20
and after that, acc is the sum

103
00:04:12,20 --> 00:04:14,10
of our previous two numbers again,

104
00:04:14,10 --> 00:04:17,40
and x is the third element in our array.

105
00:04:17,40 --> 00:04:18,70
And then the fourth time it's called,

106
00:04:18,70 --> 00:04:21,40
acc is the sum of our previous two numbers,

107
00:04:21,40 --> 00:04:22,80
12 + 2,

108
00:04:22,80 --> 00:04:26,50
and x is the fourth number in our array. Fourth.

109
00:04:26,50 --> 00:04:31,90
And so on and so forth, until we get our final result.

110
00:04:31,90 --> 00:04:33,10
And we can see our final result

111
00:04:33,10 --> 00:04:35,10
by logging sum afterward.

112
00:04:35,10 --> 00:04:40,50
So we'll say console.log(sum);

113
00:04:40,50 --> 00:04:43,10
and then run our code again.

114
00:04:43,10 --> 00:04:46,20
And we see that our final sum is 114.

115
00:04:46,20 --> 00:04:49,60
In other words, 103 + 11,

116
00:04:49,60 --> 00:04:57,00
which is the last number in our array.

117
00:04:57,00 --> 00:04:58,90
Now note that if we wanted to use reduce

118
00:04:58,90 --> 00:05:02,30
to calculate the product of all the numbers in our array,

119
00:05:02,30 --> 00:05:03,90
the starting value we'd have to provide

120
00:05:03,90 --> 00:05:08,40
wouldn't be 0, it would be 1.

121
00:05:08,40 --> 00:05:10,00
If we change the return value here

122
00:05:10,00 --> 00:05:12,20
to find the product of all our numbers,

123
00:05:12,20 --> 00:05:14,80
and keep 0 the same,

124
00:05:14,80 --> 00:05:21,30
we'll change this variable name to product as well,

125
00:05:21,30 --> 00:05:25,90
and then run our code,

126
00:05:25,90 --> 00:05:28,40
we see that the final result we get is 0,

127
00:05:28,40 --> 00:05:30,90
because our starting value is 0,

128
00:05:30,90 --> 00:05:32,90
and our first number is 5,

129
00:05:32,90 --> 00:05:35,10
and then the next time the function is called,

130
00:05:35,10 --> 00:05:39,10
the accumulator value is 5 times 0, which is 0.

131
00:05:39,10 --> 00:05:40,40
And then the next time it's called,

132
00:05:40,40 --> 00:05:43,00
it's 7 times 0 which is 0.

133
00:05:43,00 --> 00:05:44,30
And then the next time it's called

134
00:05:44,30 --> 00:05:49,30
it's 2 times 0, which is also 0, and so on.

135
00:05:49,30 --> 00:05:51,20
So if we're finding the product,

136
00:05:51,20 --> 00:05:56,10
we want to make sure that our starting value is 1.

137
00:05:56,10 --> 00:05:59,70
If we run our code again,

138
00:05:59,70 --> 00:06:01,80
We see that we get a very large number,

139
00:06:01,80 --> 00:06:04,10
but it's working correctly.

140
00:06:04,10 --> 00:06:05,70
The first time our function is called,

141
00:06:05,70 --> 00:06:09,10
the accumulator variable is 1, our starting value.

142
00:06:09,10 --> 00:06:10,40
And then the next time it's called,

143
00:06:10,40 --> 00:06:14,80
It's 5 times 1 which is 5, and so on and so forth.

144
00:06:14,80 --> 00:06:15,70
And this is just something

145
00:06:15,70 --> 00:06:17,60
to keep in mind when using reduce.

146
00:06:17,60 --> 00:06:19,30
The starting value is very important

147
00:06:19,30 --> 00:06:20,20
and can be the difference

148
00:06:20,20 --> 00:06:23,00
between your reduce function working, and not working.

