1
00:00:00,50 --> 00:00:01,50
- [Instructor] One of the nice things

2
00:00:01,50 --> 00:00:04,20
about learning functional programming with JavaScript

3
00:00:04,20 --> 00:00:06,00
is that many of the concepts we've learned

4
00:00:06,00 --> 00:00:07,60
have a great deal of native support

5
00:00:07,60 --> 00:00:09,30
in the language itself.

6
00:00:09,30 --> 00:00:12,40
In general, JavaScript, and in particular ES6,

7
00:00:12,40 --> 00:00:14,20
makes it much easier to apply many

8
00:00:14,20 --> 00:00:16,70
of the core functional programming concepts

9
00:00:16,70 --> 00:00:18,10
than if we were working in languages

10
00:00:18,10 --> 00:00:20,80
such as Java or C++.

11
00:00:20,80 --> 00:00:23,30
One of the areas where this fact is especially true

12
00:00:23,30 --> 00:00:25,90
is in working with objects and arrays.

13
00:00:25,90 --> 00:00:28,30
JavaScript provides a host of built-in functions

14
00:00:28,30 --> 00:00:31,20
and functionality that makes working with these constructs

15
00:00:31,20 --> 00:00:34,00
in a functional way incredibly easy.

16
00:00:34,00 --> 00:00:36,30
Built-in functions, such as map, filter, reduce,

17
00:00:36,30 --> 00:00:39,10
and sort make the formally complicated task

18
00:00:39,10 --> 00:00:42,40
of transforming array data almost trivial.

19
00:00:42,40 --> 00:00:43,80
As you're studying these functions

20
00:00:43,80 --> 00:00:45,50
and learning about what they can do

21
00:00:45,50 --> 00:00:47,80
you may start to wonder how these functions work

22
00:00:47,80 --> 00:00:49,40
under the hood.

23
00:00:49,40 --> 00:00:51,20
The sort function, for example.

24
00:00:51,20 --> 00:00:53,40
What sorting algorithm does it actually use

25
00:00:53,40 --> 00:00:54,60
to sort our array?

26
00:00:54,60 --> 00:00:57,30
Quick sort, bubble sort, merge sort?

27
00:00:57,30 --> 00:01:00,90
Well, the beautiful thing is it doesn't matter.

28
00:01:00,90 --> 00:01:03,80
Functional programming, as we saw in previous videos,

29
00:01:03,80 --> 00:01:06,00
is meant to free us from having to worry about

30
00:01:06,00 --> 00:01:08,70
how things are done, and instead just focuses

31
00:01:08,70 --> 00:01:10,70
on what things are.

32
00:01:10,70 --> 00:01:13,30
If you want an array of names sorted alphabetically

33
00:01:13,30 --> 00:01:16,10
you can just say so, you don't have to worry about

34
00:01:16,10 --> 00:01:19,10
all the mechanics behind what makes these functions work.

35
00:01:19,10 --> 00:01:21,90
Leave that to the browsers and node servers.

36
00:01:21,90 --> 00:01:24,40
In fact, many browsers actually implement

37
00:01:24,40 --> 00:01:26,30
these functions differently depending on

38
00:01:26,30 --> 00:01:28,00
what works better for them.

39
00:01:28,00 --> 00:01:29,70
Anyway, that's just something to keep in mind

40
00:01:29,70 --> 00:01:32,00
as you learn about JavaScript's many built-in functions

41
00:01:32,00 --> 00:01:33,90
to help you manipulate data.

42
00:01:33,90 --> 00:01:35,30
If you find yourself worrying about

43
00:01:35,30 --> 00:01:37,90
how something works behind the scenes,

44
00:01:37,90 --> 00:01:40,20
well if might be a fun mental exercise,

45
00:01:40,20 --> 00:01:41,70
but at the end of the day you can just sit back

46
00:01:41,70 --> 00:01:44,00
and relax because as a functional programmer

47
00:01:44,00 --> 00:01:45,50
that's not your job.

48
00:01:45,50 --> 00:01:47,10
Over the course of the next few videos

49
00:01:47,10 --> 00:01:49,70
we're going to examine ES6's built-in support

50
00:01:49,70 --> 00:01:50,90
for functional programming,

51
00:01:50,90 --> 00:01:53,00
and how we can use it to our advantage.

