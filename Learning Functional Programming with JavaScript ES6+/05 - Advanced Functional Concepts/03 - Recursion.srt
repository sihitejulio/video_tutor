1
00:00:00,50 --> 00:00:02,40
- [Instructor] The next advance concept we're going to look at

2
00:00:02,40 --> 00:00:04,10
is called Recursion.

3
00:00:04,10 --> 00:00:06,70
Recursion is a concept that you've probably heard of before

4
00:00:06,70 --> 00:00:09,30
but maybe you've never seen it in action.

5
00:00:09,30 --> 00:00:10,40
What recursion is

6
00:00:10,40 --> 00:00:13,60
is simply a case when a function calls itself.

7
00:00:13,60 --> 00:00:15,30
While doing this can very easily lead

8
00:00:15,30 --> 00:00:17,20
to an infinite loop if we're not careful

9
00:00:17,20 --> 00:00:19,20
it can also be used to solve certain problems

10
00:00:19,20 --> 00:00:22,60
that aren't easy to solve using other methods.

11
00:00:22,60 --> 00:00:26,30
Let's take a look at a very simple example using recursion.

12
00:00:26,30 --> 00:00:28,70
We're going to create a function the behaves like a for loop

13
00:00:28,70 --> 00:00:31,00
without actually using a for loop.

14
00:00:31,00 --> 00:00:33,50
We'll call this function countDown.

15
00:00:33,50 --> 00:00:37,10
So const countDown

16
00:00:37,10 --> 00:00:38,40
and what this function will do

17
00:00:38,40 --> 00:00:41,90
is start at whatever number we pass into it as an argument,

18
00:00:41,90 --> 00:00:45,50
we'll call that argument x

19
00:00:45,50 --> 00:00:46,80
and it's going to use recursion

20
00:00:46,80 --> 00:00:48,80
to countdown from that number to zero

21
00:00:48,80 --> 00:00:51,20
printing the results along the way.

22
00:00:51,20 --> 00:00:53,30
So all we have to do is print the number,

23
00:00:53,30 --> 00:00:55,40
console.log

24
00:00:55,40 --> 00:00:56,90
x

25
00:00:56,90 --> 00:01:00,80
and then call our function with our argument minus one.

26
00:01:00,80 --> 00:01:02,70
CountDown

27
00:01:02,70 --> 00:01:05,30
and we'll pass x minus one.

28
00:01:05,30 --> 00:01:08,60
If we were to run our code now it will recurs infinitely

29
00:01:08,60 --> 00:01:11,00
because we forgot one important thing,

30
00:01:11,00 --> 00:01:13,10
in recursion we always have to tell our function

31
00:01:13,10 --> 00:01:14,80
when to stop,

32
00:01:14,80 --> 00:01:16,10
without a stop condition

33
00:01:16,10 --> 00:01:19,00
any recursive function will go on infinitely.

34
00:01:19,00 --> 00:01:20,90
Our stop condition in this case

35
00:01:20,90 --> 00:01:23,30
will be when the argument that the function receives

36
00:01:23,30 --> 00:01:25,10
is less than zero.

37
00:01:25,10 --> 00:01:28,80
So up at the top of our function we want to say if

38
00:01:28,80 --> 00:01:31,00
x is less than zero

39
00:01:31,00 --> 00:01:33,30
and then we simply want to return.

40
00:01:33,30 --> 00:01:34,60
This prevents the function

41
00:01:34,60 --> 00:01:37,30
from calling itself again at this point.

42
00:01:37,30 --> 00:01:39,50
Now if we call our function

43
00:01:39,50 --> 00:01:40,80
countDown

44
00:01:40,80 --> 00:01:43,80
and will pass in the number 10

45
00:01:43,80 --> 00:01:47,00
and then we run our code

46
00:01:47,00 --> 00:01:50,00
we see that it counts from 10 all the way down to zero

47
00:01:50,00 --> 00:01:52,50
without using any kind of for loop.

48
00:01:52,50 --> 00:01:55,40
So what if we wanted to count up instead?

49
00:01:55,40 --> 00:01:57,80
Well this is going to be very similar to counting down

50
00:01:57,80 --> 00:01:59,70
with a small few changes.

51
00:01:59,70 --> 00:02:01,80
First of all instead of subtracting one

52
00:02:01,80 --> 00:02:06,20
from the number on each function call we're going to add one.

53
00:02:06,20 --> 00:02:07,80
Second of all instead of checking to see

54
00:02:07,80 --> 00:02:10,20
if the current number is less than zero

55
00:02:10,20 --> 00:02:11,50
we're going to check if the current number

56
00:02:11,50 --> 00:02:14,50
is greater than the number we want to count up to

57
00:02:14,50 --> 00:02:19,70
and this means that we need to add another argument.

58
00:02:19,70 --> 00:02:22,90
So now x will be the number the we start on

59
00:02:22,90 --> 00:02:25,00
and will define another argument called max

60
00:02:25,00 --> 00:02:27,20
that we want to count up to.

61
00:02:27,20 --> 00:02:28,70
Then inside this if statement

62
00:02:28,70 --> 00:02:32,30
we simply check if x is greater than max and if it is

63
00:02:32,30 --> 00:02:36,00
then we prevent our function from calling itself again.

64
00:02:36,00 --> 00:02:38,40
We also need to make sure to pass this max argument

65
00:02:38,40 --> 00:02:41,70
when out function calls itself.

66
00:02:41,70 --> 00:02:49,80
And finally let's rename this function to countUp.

67
00:02:49,80 --> 00:02:52,50
And when we call this function initially

68
00:02:52,50 --> 00:02:56,30
we have to define the number that we want it to start on, x

69
00:02:56,30 --> 00:02:59,00
as well as the number we want it to count up to,

70
00:02:59,00 --> 00:03:02,50
we'll do 10 again.

71
00:03:02,50 --> 00:03:05,50
And if we run our code

72
00:03:05,50 --> 00:03:06,70
we see that it prints the numbers

73
00:03:06,70 --> 00:03:09,20
from zero to 10 counting up

74
00:03:09,20 --> 00:03:11,20
and this is exactly what we wanted.

75
00:03:11,20 --> 00:03:12,80
We now have a function that count up

76
00:03:12,80 --> 00:03:15,00
without any help from a for loop.

