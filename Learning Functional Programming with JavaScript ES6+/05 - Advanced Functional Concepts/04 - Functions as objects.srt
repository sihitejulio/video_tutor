1
00:00:00,50 --> 00:00:02,40
- Throughout the other videos in this course,

2
00:00:02,40 --> 00:00:03,50
we've consistently seen

3
00:00:03,50 --> 00:00:06,40
that the way we treat functions in functional programming

4
00:00:06,40 --> 00:00:08,60
is quite different from what we might be used to,

5
00:00:08,60 --> 00:00:11,40
in say, object-oriented programming.

6
00:00:11,40 --> 00:00:13,30
We've seen that they can be treated like other types

7
00:00:13,30 --> 00:00:14,80
such as strings or numbers,

8
00:00:14,80 --> 00:00:16,70
and also that we can pass them as arguments

9
00:00:16,70 --> 00:00:17,70
into other functions,

10
00:00:17,70 --> 00:00:20,00
or return them from other functions.

11
00:00:20,00 --> 00:00:22,50
Well there's another aspect of functions in JavaScript

12
00:00:22,50 --> 00:00:24,70
that we're going to talk about today.

13
00:00:24,70 --> 00:00:26,10
In addition to the many other roles

14
00:00:26,10 --> 00:00:27,70
we've seen them play so far,

15
00:00:27,70 --> 00:00:31,50
the functions that we define in our code are also objects.

16
00:00:31,50 --> 00:00:33,80
So when we say that functions are objects,

17
00:00:33,80 --> 00:00:36,60
we mean that they have properties like objects do.

18
00:00:36,60 --> 00:00:38,40
They have a name property for example

19
00:00:38,40 --> 00:00:40,90
that returns whatever name we gave the function.

20
00:00:40,90 --> 00:00:44,60
So for instance if we define a function called sayHello

21
00:00:44,60 --> 00:00:51,00
that just prints hello to the console,

22
00:00:51,00 --> 00:00:55,30
we can say sayHello.name,

23
00:00:55,30 --> 00:00:58,50
and if we run our code,

24
00:00:58,50 --> 00:01:00,10
we see that that gives us a name

25
00:01:00,10 --> 00:01:02,10
that we initially gave the function.

26
00:01:02,10 --> 00:01:03,90
Now functions also have a length property

27
00:01:03,90 --> 00:01:05,30
which returns the number of arguments

28
00:01:05,30 --> 00:01:08,10
that a function expects.

29
00:01:08,10 --> 00:01:10,20
If we use our add function from previous videos

30
00:01:10,20 --> 00:01:13,70
that takes three arguments like this,

31
00:01:13,70 --> 00:01:16,80
const add equals x, y, z,

32
00:01:16,80 --> 00:01:23,40
and returns the sum of x, y, z,

33
00:01:23,40 --> 00:01:26,80
the length property of add

34
00:01:26,80 --> 00:01:31,00
is going to be three since it expects three arguments.

35
00:01:31,00 --> 00:01:32,70
Functions also have properties

36
00:01:32,70 --> 00:01:35,20
which themselves are functions.

37
00:01:35,20 --> 00:01:37,70
For example, every function has a two-string property

38
00:01:37,70 --> 00:01:39,40
which returns a string representation

39
00:01:39,40 --> 00:01:42,50
of the function's code.

40
00:01:42,50 --> 00:01:45,50
So if we say add.toString,

41
00:01:45,50 --> 00:01:47,20
and print it,

42
00:01:47,20 --> 00:01:49,50
and then run our code again,

43
00:01:49,50 --> 00:01:51,60
we see that it prints a string representation

44
00:01:51,60 --> 00:01:53,40
of our function.

45
00:01:53,40 --> 00:01:55,10
There are three other function properties

46
00:01:55,10 --> 00:01:58,10
which are of interest to us from a functional standpoint.

47
00:01:58,10 --> 00:02:02,00
These functions are called call, apply, and bind.

48
00:02:02,00 --> 00:02:04,10
Let's start by looking at call.

49
00:02:04,10 --> 00:02:08,10
What call does is basically just calls our function.

50
00:02:08,10 --> 00:02:11,90
So we can say add.call,

51
00:02:11,90 --> 00:02:14,70
and the first argument of this call function allows us

52
00:02:14,70 --> 00:02:17,10
to change the value of the this keyword

53
00:02:17,10 --> 00:02:19,60
if we used it in our function.

54
00:02:19,60 --> 00:02:20,90
Now in functional programming,

55
00:02:20,90 --> 00:02:22,40
we'll only use the this keyword

56
00:02:22,40 --> 00:02:24,50
inside functions very rarely,

57
00:02:24,50 --> 00:02:26,70
so we usually just end up passing null in

58
00:02:26,70 --> 00:02:28,60
for the first argument,

59
00:02:28,60 --> 00:02:31,10
and the rest of the arguments that we pass to call

60
00:02:31,10 --> 00:02:32,30
will be passed to our function

61
00:02:32,30 --> 00:02:34,40
and the result will be returned.

62
00:02:34,40 --> 00:02:38,00
So if we say one, two, and three,

63
00:02:38,00 --> 00:02:40,40
and run our code,

64
00:02:40,40 --> 00:02:42,90
we see that call simply calls our add function

65
00:02:42,90 --> 00:02:45,60
with the arguments one, two, and three.

66
00:02:45,60 --> 00:02:47,00
This is exactly the same result

67
00:02:47,00 --> 00:02:50,10
as if we had simply just called add

68
00:02:50,10 --> 00:02:53,00
and passed in our one, two, and three.

69
00:02:53,00 --> 00:02:56,00
The next function we're going to look at is apply.

70
00:02:56,00 --> 00:02:58,70
Apply is almost exactly the same as call,

71
00:02:58,70 --> 00:02:59,60
the only difference is

72
00:02:59,60 --> 00:03:01,50
that instead of passing the arguments we want

73
00:03:01,50 --> 00:03:04,30
to call our function with in the usual way,

74
00:03:04,30 --> 00:03:06,90
we pass apply an array of values

75
00:03:06,90 --> 00:03:09,60
which it passes to our function as arguments.

76
00:03:09,60 --> 00:03:13,20
So if want to do this example here using apply,

77
00:03:13,20 --> 00:03:14,80
that would look like this,

78
00:03:14,80 --> 00:03:16,90
add.apply,

79
00:03:16,90 --> 00:03:20,30
and the first argument that apply takes as well allows us

80
00:03:20,30 --> 00:03:22,20
to change the value of the this keyword

81
00:03:22,20 --> 00:03:23,70
if it's used in our function.

82
00:03:23,70 --> 00:03:26,00
Again this will just be null.

83
00:03:26,00 --> 00:03:29,40
And then we can pass in our arguments as an array.

84
00:03:29,40 --> 00:03:33,10
We'll pass in one, two, and three,

85
00:03:33,10 --> 00:03:36,20
and if we run our code,

86
00:03:36,20 --> 00:03:38,70
we see that we get the same result.

87
00:03:38,70 --> 00:03:41,20
Now in reality, we'll rarely use either

88
00:03:41,20 --> 00:03:43,10
of these functions in our code.

89
00:03:43,10 --> 00:03:44,80
First of all, as I said before,

90
00:03:44,80 --> 00:03:47,30
in functional programming we rarely have any desire

91
00:03:47,30 --> 00:03:50,90
to use the this keyword inside of our functions.

92
00:03:50,90 --> 00:03:52,60
This is something that's seen much more often

93
00:03:52,60 --> 00:03:54,30
in the world of object-oriented programming

94
00:03:54,30 --> 00:03:56,90
as a means to control internal state.

95
00:03:56,90 --> 00:03:59,40
But also, while prior to ES6,

96
00:03:59,40 --> 00:04:01,60
apply was the easiest way to call a function

97
00:04:01,60 --> 00:04:04,30
when we wanted to pass the arguments as an array,

98
00:04:04,30 --> 00:04:07,00
ES6 now provides us with a very neat syntax

99
00:04:07,00 --> 00:04:09,00
for doing this using the spread op,

100
00:04:09,00 --> 00:04:10,40
and that looks like this.

101
00:04:10,40 --> 00:04:13,20
If we define a constant called args

102
00:04:13,20 --> 00:04:14,50
and set it equal to an array

103
00:04:14,50 --> 00:04:17,20
with one, two, and three in it,

104
00:04:17,20 --> 00:04:20,10
if we want to call add with this array,

105
00:04:20,10 --> 00:04:24,60
we can simply do it like this,

106
00:04:24,60 --> 00:04:29,80
using the spread operator and args.

107
00:04:29,80 --> 00:04:32,40
If we run our code again,

108
00:04:32,40 --> 00:04:34,70
we get the same result.

109
00:04:34,70 --> 00:04:36,60
So even though the functions call and apply

110
00:04:36,60 --> 00:04:38,40
don't have a whole lot of practical use

111
00:04:38,40 --> 00:04:40,50
in everyday functional programming,

112
00:04:40,50 --> 00:04:42,30
in my experience call and apply

113
00:04:42,30 --> 00:04:44,20
have a habit of showing up in job interviews

114
00:04:44,20 --> 00:04:46,50
for JavaScript related jobs,

115
00:04:46,50 --> 00:04:50,10
so it's not a bad idea to at least know how they work.

116
00:04:50,10 --> 00:04:52,00
The last function we're going to look at,

117
00:04:52,00 --> 00:04:54,20
which does have some real practical application

118
00:04:54,20 --> 00:04:55,60
in functional programming,

119
00:04:55,60 --> 00:04:58,40
is a function called bind.

120
00:04:58,40 --> 00:05:00,20
Bind also let's us change the value

121
00:05:00,20 --> 00:05:02,90
of the this keyword when it's used inside a function.

122
00:05:02,90 --> 00:05:05,00
Again this is usually null,

123
00:05:05,00 --> 00:05:06,20
but the useful part comes

124
00:05:06,20 --> 00:05:08,60
in the next arguments that we pass.

125
00:05:08,60 --> 00:05:11,20
In a previous video we touched on partial application,

126
00:05:11,20 --> 00:05:13,10
and how it allows us to fix the value

127
00:05:13,10 --> 00:05:14,70
of certain arguments in a function

128
00:05:14,70 --> 00:05:16,00
and return another function

129
00:05:16,00 --> 00:05:18,20
that we can use to pass the rest of the arguments

130
00:05:18,20 --> 00:05:19,80
to get the result.

131
00:05:19,80 --> 00:05:22,60
Normally doing this requires somewhat complicated syntax

132
00:05:22,60 --> 00:05:24,10
as we saw before,

133
00:05:24,10 --> 00:05:27,60
but bind allows us to do this in a much cleaner way.

134
00:05:27,60 --> 00:05:30,40
If we want to fix one of the arguments in our add function,

135
00:05:30,40 --> 00:05:32,20
we simply have to call bind,

136
00:05:32,20 --> 00:05:35,90
and pass it the value we want to fix the first argument.

137
00:05:35,90 --> 00:05:37,90
So if we wanted to fix the first argument x,

138
00:05:37,90 --> 00:05:42,50
we could just pass one as the second argument to bind.

139
00:05:42,50 --> 00:05:44,00
This partially applies our function

140
00:05:44,00 --> 00:05:45,50
so that we can do stuff like this,

141
00:05:45,50 --> 00:05:50,10
const add1 equals add.bind,

142
00:05:50,10 --> 00:05:52,20
null for the first argument,

143
00:05:52,20 --> 00:05:56,10
and we'll fix the first argument of our add function to one,

144
00:05:56,10 --> 00:05:59,20
and then we can simply call add1

145
00:05:59,20 --> 00:06:02,80
with the remaining two arguments,

146
00:06:02,80 --> 00:06:05,60
and if we run our code,

147
00:06:05,60 --> 00:06:07,90
again we get the same result.

148
00:06:07,90 --> 00:06:09,00
This can be very useful

149
00:06:09,00 --> 00:06:12,00
to use in our functional programing in many ways.

