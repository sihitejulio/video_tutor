1
00:00:00,50 --> 00:00:01,60
- [Instructor] So far in this course

2
00:00:01,60 --> 00:00:04,30
we've covered a wide range of functional topics.

3
00:00:04,30 --> 00:00:05,90
We've learned how to think about our programs

4
00:00:05,90 --> 00:00:06,90
in a more functional way

5
00:00:06,90 --> 00:00:09,10
by covering the three main concepts.

6
00:00:09,10 --> 00:00:11,20
We've learned to work with first-class functions

7
00:00:11,20 --> 00:00:13,30
in various applications of doing so.

8
00:00:13,30 --> 00:00:15,80
And finally, we looked at how we can work with arrays

9
00:00:15,80 --> 00:00:17,60
in a functional way.

10
00:00:17,60 --> 00:00:20,30
The things we've learned up until now are extremely useful,

11
00:00:20,30 --> 00:00:21,30
and knowing these concepts

12
00:00:21,30 --> 00:00:22,60
and applying them in your code base

13
00:00:22,60 --> 00:00:26,50
will quickly show you the positive impacts they provide.

14
00:00:26,50 --> 00:00:28,10
But before we wrap up this course,

15
00:00:28,10 --> 00:00:31,50
there are two more concepts I'd like to cover with you.

16
00:00:31,50 --> 00:00:34,90
These concepts are recursion and partial application.

17
00:00:34,90 --> 00:00:36,90
Keep in mind that these concepts are more advanced,

18
00:00:36,90 --> 00:00:40,00
so don't worry if they don't really click with you at first.

19
00:00:40,00 --> 00:00:41,90
Furthermore, what I'll be covering in this course

20
00:00:41,90 --> 00:00:43,30
with regards to these concepts

21
00:00:43,30 --> 00:00:45,10
is only the tip of the iceberg,

22
00:00:45,10 --> 00:00:47,20
and a whole course could probably be made about each

23
00:00:47,20 --> 00:00:49,00
of these concepts alone.

24
00:00:49,00 --> 00:00:51,10
That being said, these concepts do serve

25
00:00:51,10 --> 00:00:52,30
as a great jumping off point

26
00:00:52,30 --> 00:00:55,50
to get into more complex functional programming.

27
00:00:55,50 --> 00:00:56,40
If you stick to it

28
00:00:56,40 --> 00:00:58,80
and gain a thorough understanding of these concepts,

29
00:00:58,80 --> 00:01:02,20
they can allow you to do some very cool and flexible things,

30
00:01:02,20 --> 00:01:03,80
as well as gain a better understanding

31
00:01:03,80 --> 00:01:06,00
of functional programing in general.

