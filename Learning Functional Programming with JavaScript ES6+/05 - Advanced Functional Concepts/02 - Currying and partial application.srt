1
00:00:00,50 --> 00:00:02,60
- The first advanced concept we're going to look at

2
00:00:02,60 --> 00:00:04,90
is called partial application.

3
00:00:04,90 --> 00:00:07,00
In simple terms, partial application is when

4
00:00:07,00 --> 00:00:09,60
we take a function that has some number of arguments,

5
00:00:09,60 --> 00:00:13,30
and we fix some of those arguments to a set value.

6
00:00:13,30 --> 00:00:15,20
This gives us a function with less arguments

7
00:00:15,20 --> 00:00:17,10
than we had before.

8
00:00:17,10 --> 00:00:18,90
This function with fixed arguments can then be called

9
00:00:18,90 --> 00:00:20,90
from anywhere else in code,

10
00:00:20,90 --> 00:00:23,20
and it will be as if we had called the original function

11
00:00:23,20 --> 00:00:25,30
with all of its arguments.

12
00:00:25,30 --> 00:00:27,80
Partial application is a very useful way to configure

13
00:00:27,80 --> 00:00:30,90
more general functions into more specific ones.

14
00:00:30,90 --> 00:00:33,20
In general, if there's a function that we use often

15
00:00:33,20 --> 00:00:36,00
in our code, where one or more of the arguments that we call

16
00:00:36,00 --> 00:00:38,50
it with is almost always the same,

17
00:00:38,50 --> 00:00:42,40
this is a good candidate for using partial application.

18
00:00:42,40 --> 00:00:44,90
So now that we know what partial application is,

19
00:00:44,90 --> 00:00:47,20
what does it look like in code?

20
00:00:47,20 --> 00:00:48,20
Well let's imagine that we have

21
00:00:48,20 --> 00:00:50,10
a simple add function.

22
00:00:50,10 --> 00:00:51,90
Const add,

23
00:00:51,90 --> 00:00:56,10
and it takes three arguments we'll call them X, Y, and Z,

24
00:00:56,10 --> 00:00:57,60
and adds them together.

25
00:00:57,60 --> 00:01:00,00
X plus Y plus Z.

26
00:01:00,00 --> 00:01:02,00
What if we wanted to fix one of these arguments

27
00:01:02,00 --> 00:01:04,20
to a certain number, so that we only had to pass

28
00:01:04,20 --> 00:01:06,50
in the other two arguments later on.

29
00:01:06,50 --> 00:01:08,60
Well that would look like this.

30
00:01:08,60 --> 00:01:09,90
We'll create another function.

31
00:01:09,90 --> 00:01:12,40
We'll call it add partial,

32
00:01:12,40 --> 00:01:14,50
and this function will take a single argument.

33
00:01:14,50 --> 00:01:16,30
We'll call that X.

34
00:01:16,30 --> 00:01:18,00
It will then return another function,

35
00:01:18,00 --> 00:01:21,80
which takes the remaining two arguments, Y and Z.

36
00:01:21,80 --> 00:01:23,90
And finally, this function that we return will call

37
00:01:23,90 --> 00:01:29,20
our original add function with all three arguments.

38
00:01:29,20 --> 00:01:32,20
And we can then call our function like this.

39
00:01:32,20 --> 00:01:38,30
We'll say const add five equals add partial

40
00:01:38,30 --> 00:01:40,00
with five.

41
00:01:40,00 --> 00:01:43,20
Now the five that we're passing to add partial becomes X,

42
00:01:43,20 --> 00:01:45,50
which is the first argument that we end up passing

43
00:01:45,50 --> 00:01:48,50
to our add function inside add partial.

44
00:01:48,50 --> 00:01:51,80
So basically, we're fixing X in our add function

45
00:01:51,80 --> 00:01:54,80
to a specific value, five.

46
00:01:54,80 --> 00:01:58,30
We can then call our add five function like this.

47
00:01:58,30 --> 00:02:02,50
We'll say const sum equals, add five,

48
00:02:02,50 --> 00:02:05,30
and then we pass in the remaining two arguments.

49
00:02:05,30 --> 00:02:08,10
We'll say six and seven.

50
00:02:08,10 --> 00:02:13,10
If we log the result console dot log sum

51
00:02:13,10 --> 00:02:16,20
and run our code,

52
00:02:16,20 --> 00:02:19,20
we can see that we get the sum of the three numbers.

53
00:02:19,20 --> 00:02:21,70
Just as if we had called add with all three numbers

54
00:02:21,70 --> 00:02:25,40
to begin with, like this.

55
00:02:25,40 --> 00:02:28,30
Add five, six, seven.

56
00:02:28,30 --> 00:02:30,40
If we run our code again,

57
00:02:30,40 --> 00:02:33,40
we see that we get the exact same result.

58
00:02:33,40 --> 00:02:36,30
So basically what we're doing with partial application then

59
00:02:36,30 --> 00:02:38,50
is instead of passing in all our arguments

60
00:02:38,50 --> 00:02:40,10
at the same time,

61
00:02:40,10 --> 00:02:42,20
we're simply passing them in at different times

62
00:02:42,20 --> 00:02:44,50
in different parts of the code.

63
00:02:44,50 --> 00:02:46,80
Now in this example we fixed the first argument

64
00:02:46,80 --> 00:02:48,40
and left the other two.

65
00:02:48,40 --> 00:02:50,60
In reality, we can really divide up our arguments

66
00:02:50,60 --> 00:02:51,90
however we want.

67
00:02:51,90 --> 00:02:54,90
For example, we can partially apply the first two arguments

68
00:02:54,90 --> 00:02:56,30
and leave the last one for later,

69
00:02:56,30 --> 00:02:58,60
and that'll look like this.

70
00:02:58,60 --> 00:03:02,60
We'll simply take our Y argument here,

71
00:03:02,60 --> 00:03:06,30
and move it up here with X.

72
00:03:06,30 --> 00:03:07,50
Then the function that we return

73
00:03:07,50 --> 00:03:11,30
will only take the remaining Z argument,

74
00:03:11,30 --> 00:03:13,50
and calling that will look like this.

75
00:03:13,50 --> 00:03:17,80
We'll say context, add five and six,

76
00:03:17,80 --> 00:03:21,10
and that'll be equal to add partial

77
00:03:21,10 --> 00:03:25,70
with the X and Y arguments as five and six.

78
00:03:25,70 --> 00:03:31,90
And then, we can say const sum equals add five and six,

79
00:03:31,90 --> 00:03:34,00
and passing the remaining Z argument

80
00:03:34,00 --> 00:03:35,70
that we haven't passed in yet.

81
00:03:35,70 --> 00:03:38,60
Seven.

82
00:03:38,60 --> 00:03:40,70
And if we run our code again,

83
00:03:40,70 --> 00:03:44,30
we see that again we get the exact same result.

84
00:03:44,30 --> 00:03:46,90
We can also go one level deeper and pass in

85
00:03:46,90 --> 00:03:49,90
each of our arguments one at a time.

86
00:03:49,90 --> 00:03:53,20
And what that will look like is this.

87
00:03:53,20 --> 00:03:56,00
First we'll pass in our X argument,

88
00:03:56,00 --> 00:03:59,80
and we'll return a function that takes the Y argument.

89
00:03:59,80 --> 00:04:02,40
And finally, we'll return one last function

90
00:04:02,40 --> 00:04:05,50
that takes the Z argument.

91
00:04:05,50 --> 00:04:09,30
And then calls, add with X, Y, and Z,

92
00:04:09,30 --> 00:04:13,90
and calling this function will look like this.

93
00:04:13,90 --> 00:04:16,10
First we pass in our X argument.

94
00:04:16,10 --> 00:04:20,40
We'll call this add five equals add partial.

95
00:04:20,40 --> 00:04:22,10
Five.

96
00:04:22,10 --> 00:04:24,80
And then we'll pass in our Y argument.

97
00:04:24,80 --> 00:04:28,00
So we'll say const add five

98
00:04:28,00 --> 00:04:30,30
and six

99
00:04:30,30 --> 00:04:32,70
equals, add five,

100
00:04:32,70 --> 00:04:34,40
and we're passing in our Y argument.

101
00:04:34,40 --> 00:04:37,10
Six.

102
00:04:37,10 --> 00:04:38,80
And this add five and six function

103
00:04:38,80 --> 00:04:42,80
that we have here, is this function here that we return,

104
00:04:42,80 --> 00:04:44,70
which takes a single argument,

105
00:04:44,70 --> 00:04:47,50
and then calls our original add function

106
00:04:47,50 --> 00:04:51,30
with all three arguments that we've passed in.

107
00:04:51,30 --> 00:04:56,30
So we can say const sum equals, add five and six,

108
00:04:56,30 --> 00:04:58,50
and we're passing and Z argument here.

109
00:04:58,50 --> 00:05:01,10
That'll be seven,

110
00:05:01,10 --> 00:05:03,30
and that should give us the result.

111
00:05:03,30 --> 00:05:05,80
If we run our code again,

112
00:05:05,80 --> 00:05:08,40
we see that we get the exact same thing.

113
00:05:08,40 --> 00:05:10,10
Now this sort of partial application

114
00:05:10,10 --> 00:05:12,50
where we pass in our arguments one at a time

115
00:05:12,50 --> 00:05:16,60
has a special name, and this is called currying.

116
00:05:16,60 --> 00:05:18,90
So when you hear talk of currying in the programming world,

117
00:05:18,90 --> 00:05:21,40
now you know what it means.

118
00:05:21,40 --> 00:05:23,20
Keep in mind also that we don't necessarily

119
00:05:23,20 --> 00:05:26,80
have to define all these intermediate functions.

120
00:05:26,80 --> 00:05:29,40
In the case of our add partial function here,

121
00:05:29,40 --> 00:05:31,80
we could simply call it like this.

122
00:05:31,80 --> 00:05:36,10
Const sum equals add partial,

123
00:05:36,10 --> 00:05:41,00
and then pass in our functions this way.

124
00:05:41,00 --> 00:05:42,40
Now this might look a little funny,

125
00:05:42,40 --> 00:05:45,80
but it works exactly the same way.

126
00:05:45,80 --> 00:05:47,40
If we run our code,

127
00:05:47,40 --> 00:05:50,00
we see that once again we get the exact same result.

