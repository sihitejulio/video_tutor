1
00:00:00,50 --> 00:00:01,70
- [Instructor] The third major concept

2
00:00:01,70 --> 00:00:03,10
of functional programming is something

3
00:00:03,10 --> 00:00:05,40
called first-class functions.

4
00:00:05,40 --> 00:00:07,70
This is where things really start to get interesting

5
00:00:07,70 --> 00:00:09,20
and where the power and flexibility

6
00:00:09,20 --> 00:00:12,80
of functional programming really starts to shine through.

7
00:00:12,80 --> 00:00:14,30
So what exactly do we mean

8
00:00:14,30 --> 00:00:16,50
when we talk about first-class functions?

9
00:00:16,50 --> 00:00:18,50
Well, in object-oriented programming,

10
00:00:18,50 --> 00:00:20,50
we generally treat data and functions

11
00:00:20,50 --> 00:00:23,30
as different types of entities.

12
00:00:23,30 --> 00:00:25,30
We would probably never think of, for example,

13
00:00:25,30 --> 00:00:26,90
creating an array of functions

14
00:00:26,90 --> 00:00:29,90
or passing functions as arguments to other functions,

15
00:00:29,90 --> 00:00:34,00
or even stranger, returning functions from other functions.

16
00:00:34,00 --> 00:00:35,70
However, in functional programming,

17
00:00:35,70 --> 00:00:37,90
it's not only possible to do these things,

18
00:00:37,90 --> 00:00:41,00
it's in fact a source of tremendous flexibility.

19
00:00:41,00 --> 00:00:42,40
This is exactly what we mean

20
00:00:42,40 --> 00:00:45,60
when we say that a language supports first-class functions.

21
00:00:45,60 --> 00:00:48,80
It allows us to treat functions in a very similar way

22
00:00:48,80 --> 00:00:50,70
to how we normally treat other values

23
00:00:50,70 --> 00:00:53,50
such as numbers, strings, and objects.

24
00:00:53,50 --> 00:00:54,50
But hold on a minute.

25
00:00:54,50 --> 00:00:56,90
Doesn't this go against what I said in the previous video

26
00:00:56,90 --> 00:00:58,70
about the separation of data and functions

27
00:00:58,70 --> 00:01:00,40
in functional programming?

28
00:01:00,40 --> 00:01:02,10
Well at first glance, it might seem like it,

29
00:01:02,10 --> 00:01:04,20
so let me clarify.

30
00:01:04,20 --> 00:01:05,90
In object-oriented programming,

31
00:01:05,90 --> 00:01:07,90
since functions and data are both defined

32
00:01:07,90 --> 00:01:09,40
within the same class,

33
00:01:09,40 --> 00:01:11,30
they tend to be tightly coupled.

34
00:01:11,30 --> 00:01:14,20
In other words, the functions inside a given class

35
00:01:14,20 --> 00:01:17,20
usually reference the data that they operate on directly,

36
00:01:17,20 --> 00:01:20,30
as is the case here with this.name.

37
00:01:20,30 --> 00:01:22,90
In other words, you generally can't take a given function

38
00:01:22,90 --> 00:01:24,50
and remove it from the class it's in

39
00:01:24,50 --> 00:01:27,30
and still have it work on its own.

40
00:01:27,30 --> 00:01:29,30
In functional programming on the other hand,

41
00:01:29,30 --> 00:01:31,30
we designed our functions specifically

42
00:01:31,30 --> 00:01:33,30
so that they work in isolation.

43
00:01:33,30 --> 00:01:36,10
All of the data that a function needs to operate on

44
00:01:36,10 --> 00:01:38,10
is passed in through the arguments,

45
00:01:38,10 --> 00:01:38,90
and the only thing

46
00:01:38,90 --> 00:01:40,70
that will change the output of the function

47
00:01:40,70 --> 00:01:43,30
is a change in the arguments.

48
00:01:43,30 --> 00:01:45,10
In other words, functions aren't influenced

49
00:01:45,10 --> 00:01:48,60
by any kind of internal or external state.

50
00:01:48,60 --> 00:01:50,60
There's a name for this kind of function.

51
00:01:50,60 --> 00:01:52,00
We call them pure functions,

52
00:01:52,00 --> 00:01:54,10
and in functional programming, we try our best

53
00:01:54,10 --> 00:01:56,80
to make every single one of our functions pure.

54
00:01:56,80 --> 00:01:58,60
And this is what we mean when we say that we want

55
00:01:58,60 --> 00:02:01,70
to keep data and functions separate.

56
00:02:01,70 --> 00:02:02,90
Now you might be wondering

57
00:02:02,90 --> 00:02:05,10
why we'd want to do stuff like this.

58
00:02:05,10 --> 00:02:06,40
What could we possibly gain

59
00:02:06,40 --> 00:02:07,50
from doing weird things

60
00:02:07,50 --> 00:02:09,00
like having arrays of functions

61
00:02:09,00 --> 00:02:11,30
or passing functions as arguments?

62
00:02:11,30 --> 00:02:13,60
In fact, this simple change in mindset

63
00:02:13,60 --> 00:02:16,70
toward functions opens up a world of possibilities

64
00:02:16,70 --> 00:02:18,80
in terms of software design.

65
00:02:18,80 --> 00:02:20,40
It gives a lot of flexibility

66
00:02:20,40 --> 00:02:22,10
and greatly increases our opportunities

67
00:02:22,10 --> 00:02:23,60
for code re-use.

68
00:02:23,60 --> 00:02:25,40
It also allows us to do useful things

69
00:02:25,40 --> 00:02:27,00
like combine existing functions

70
00:02:27,00 --> 00:02:28,90
to create new functions.

71
00:02:28,90 --> 00:02:30,10
We'll see in the next chapter

72
00:02:30,10 --> 00:02:32,10
how this is done and what its implications are

73
00:02:32,10 --> 00:02:34,00
for our programs.

