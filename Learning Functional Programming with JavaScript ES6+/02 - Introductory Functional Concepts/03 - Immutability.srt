1
00:00:00,50 --> 00:00:01,80
- [Instructor] The first major concept

2
00:00:01,80 --> 00:00:04,30
of functional programming is immutability,

3
00:00:04,30 --> 00:00:06,00
and it's a concept that may surprise

4
00:00:06,00 --> 00:00:07,80
a lot of people at first.

5
00:00:07,80 --> 00:00:09,20
Most programmers learned early on

6
00:00:09,20 --> 00:00:11,80
that you could assign a value to a variable.

7
00:00:11,80 --> 00:00:14,20
For example, we can define a variable called x

8
00:00:14,20 --> 00:00:16,20
and store the value five in it.

9
00:00:16,20 --> 00:00:17,90
And then, later on in the program,

10
00:00:17,90 --> 00:00:19,80
we can change the value of that variable

11
00:00:19,80 --> 00:00:21,80
to some completely different number.

12
00:00:21,80 --> 00:00:25,00
And later on, we can change this value again, and so on.

13
00:00:25,00 --> 00:00:28,10
However, in functional programming, this is not allowed.

14
00:00:28,10 --> 00:00:30,20
When we say that x is equal to five,

15
00:00:30,20 --> 00:00:32,30
we mean that, for the rest of the program,

16
00:00:32,30 --> 00:00:33,90
x will only ever be five.

17
00:00:33,90 --> 00:00:36,50
There's no way we can change it.

18
00:00:36,50 --> 00:00:38,30
In short, immutability means that

19
00:00:38,30 --> 00:00:40,80
most of the values in a program are constant,

20
00:00:40,80 --> 00:00:43,40
which, in JavaScript, means using the const keyword

21
00:00:43,40 --> 00:00:46,90
instead of the var or let keywords.

22
00:00:46,90 --> 00:00:49,40
Another way to think about it is this.

23
00:00:49,40 --> 00:00:51,90
In object-oriented and procedural programming,

24
00:00:51,90 --> 00:00:55,30
we treat variables as buckets that we can put values into.

25
00:00:55,30 --> 00:00:58,60
We call this assigning values to variables.

26
00:00:58,60 --> 00:01:01,70
So, at one point in time, x might hold the value three.

27
00:01:01,70 --> 00:01:04,00
At another time, it might hold the value 10.

28
00:01:04,00 --> 00:01:07,30
At another point, it might be negative four, and so on.

29
00:01:07,30 --> 00:01:09,40
On the other hand, in functional programming,

30
00:01:09,40 --> 00:01:11,70
we don't assign, we define.

31
00:01:11,70 --> 00:01:13,10
Instead of naming buckets

32
00:01:13,10 --> 00:01:15,40
that hold different values at different times,

33
00:01:15,40 --> 00:01:17,70
we name the values themselves.

34
00:01:17,70 --> 00:01:19,50
When we say that x equals three,

35
00:01:19,50 --> 00:01:21,20
we don't mean that x is a container

36
00:01:21,20 --> 00:01:23,40
that's currently holding the value three,

37
00:01:23,40 --> 00:01:25,70
we literally mean that x is three,

38
00:01:25,70 --> 00:01:30,20
in the same way that pi is 3.14159 et cetera.

39
00:01:30,20 --> 00:01:32,20
Could you imagine changing the value of pi?

40
00:01:32,20 --> 00:01:33,50
Of course not.

41
00:01:33,50 --> 00:01:35,50
Functional programming treats all values

42
00:01:35,50 --> 00:01:38,30
as if they were as concrete and unchanging as pi

43
00:01:38,30 --> 00:01:40,60
or any other mathematical constant.

44
00:01:40,60 --> 00:01:44,90
And so, once we've defined x, for the whole entire program,

45
00:01:44,90 --> 00:01:46,70
x can never be anything else.

46
00:01:46,70 --> 00:01:47,70
It's just another name

47
00:01:47,70 --> 00:01:50,60
for whatever value we've defined it as.

48
00:01:50,60 --> 00:01:53,70
At first glance, this may seem strange to most programmers.

49
00:01:53,70 --> 00:01:55,10
After all, what good is a program

50
00:01:55,10 --> 00:01:56,70
where you can't change anything?

51
00:01:56,70 --> 00:01:59,70
How can a program like that be useful at all?

52
00:01:59,70 --> 00:02:01,80
Let's look at a quick example.

53
00:02:01,80 --> 00:02:03,70
What if, in a certain theoretical program,

54
00:02:03,70 --> 00:02:07,40
we have an employee, and we want to raise their salary?

55
00:02:07,40 --> 00:02:09,90
Well, in object-oriented programming, of course,

56
00:02:09,90 --> 00:02:11,40
we can just change it directly,

57
00:02:11,40 --> 00:02:15,00
usually by calling a member function like this.

58
00:02:15,00 --> 00:02:17,20
In functional programming, on the other hand,

59
00:02:17,20 --> 00:02:19,10
we would instead define a new constant

60
00:02:19,10 --> 00:02:21,10
that represents the updated data

61
00:02:21,10 --> 00:02:24,80
and then use this constant in our future calculations.

62
00:02:24,80 --> 00:02:27,10
Notice here also that I'm not using the new keyword

63
00:02:27,10 --> 00:02:29,30
as we do in object-oriented programming.

64
00:02:29,30 --> 00:02:32,20
I'm just defining our data in a JavaScript object.

65
00:02:32,20 --> 00:02:35,30
We'll learn more about why this is later on.

66
00:02:35,30 --> 00:02:36,80
The advantage of immutability,

67
00:02:36,80 --> 00:02:38,30
and the reason that functional programming

68
00:02:38,30 --> 00:02:40,10
places such an emphasis on it,

69
00:02:40,10 --> 00:02:43,30
is that it frees us from having to deal with state change.

70
00:02:43,30 --> 00:02:45,30
When a program contains many variables

71
00:02:45,30 --> 00:02:47,90
that are all changing constantly at different times,

72
00:02:47,90 --> 00:02:50,70
it can be very hard to know what state a program is in

73
00:02:50,70 --> 00:02:53,00
at any given point in time.

74
00:02:53,00 --> 00:02:55,70
As programs increase in size to include thousands

75
00:02:55,70 --> 00:02:58,20
or even millions of individual variables,

76
00:02:58,20 --> 00:03:00,30
this can lead to extremely hard-to-find bugs

77
00:03:00,30 --> 00:03:01,90
and an overall fragile code base

78
00:03:01,90 --> 00:03:04,50
that programmers are afraid to make changes to.

79
00:03:04,50 --> 00:03:06,60
This is a problem that even test-driven development

80
00:03:06,60 --> 00:03:08,10
can't solve completely,

81
00:03:08,10 --> 00:03:10,70
since the task of testing all possible states

82
00:03:10,70 --> 00:03:12,60
that a program might get itself into

83
00:03:12,60 --> 00:03:16,70
is nearly impossible in programs of considerable size.

84
00:03:16,70 --> 00:03:18,70
In functional programming, on the other hand,

85
00:03:18,70 --> 00:03:21,00
we start off with an immutable set of data

86
00:03:21,00 --> 00:03:23,10
as a single source of truth,

87
00:03:23,10 --> 00:03:24,20
and then we use functions

88
00:03:24,20 --> 00:03:26,50
to combine this data piece by piece

89
00:03:26,50 --> 00:03:29,40
and transform it into useful information.

90
00:03:29,40 --> 00:03:31,60
This data is usually retrieved from a database

91
00:03:31,60 --> 00:03:33,80
or some other memory storage.

92
00:03:33,80 --> 00:03:36,60
There are two powerful advantages to this approach.

93
00:03:36,60 --> 00:03:39,00
The first is that the original data in a program

94
00:03:39,00 --> 00:03:40,70
will always remain intact,

95
00:03:40,70 --> 00:03:43,10
which makes bugs much easier to find.

96
00:03:43,10 --> 00:03:46,00
The second is that programs constructed in this way

97
00:03:46,00 --> 00:03:47,90
are much easier to keep track of,

98
00:03:47,90 --> 00:03:51,60
since you can focus on any given piece individually.

99
00:03:51,60 --> 00:03:54,70
The only thing that determines the output of a given piece

100
00:03:54,70 --> 00:03:56,20
is the input.

101
00:03:56,20 --> 00:04:00,40
We don't have to think about the entire system all the time.

102
00:04:00,40 --> 00:04:02,00
If the concept of immutability

103
00:04:02,00 --> 00:04:03,80
doesn't quite make sense to you yet,

104
00:04:03,80 --> 00:04:06,80
or if the advantages don't seem apparent, don't worry.

105
00:04:06,80 --> 00:04:09,30
Later on in the course, we'll take a look at many examples

106
00:04:09,30 --> 00:04:11,70
that incorporate immutability.

107
00:04:11,70 --> 00:04:14,30
For now, just remember that in functional programming,

108
00:04:14,30 --> 00:04:18,00
all data is immutable, so use const instead of let or var.

