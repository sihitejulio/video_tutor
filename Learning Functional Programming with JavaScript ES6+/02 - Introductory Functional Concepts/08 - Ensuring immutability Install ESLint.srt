1
00:00:00,50 --> 00:00:01,60
- [Instructor] The first thing we're going to do

2
00:00:01,60 --> 00:00:04,90
is open up a terminal inside our Exercise Files folder.

3
00:00:04,90 --> 00:00:08,20
I'm going to use the integrated terminal inside inside VS Code,

4
00:00:08,20 --> 00:00:09,10
but you're more than welcome

5
00:00:09,10 --> 00:00:11,50
to use Mac's built in terminal as well.

6
00:00:11,50 --> 00:00:14,00
And then, inside our Exercise Files folder,

7
00:00:14,00 --> 00:00:20,60
we're going to initialize an npm package with npm init -y.

8
00:00:20,60 --> 00:00:21,60
Once we've done that,

9
00:00:21,60 --> 00:00:24,10
we'll install ESLint as a dev dependency

10
00:00:24,10 --> 00:00:32,90
by typing npm install --save-dev eslint.

11
00:00:32,90 --> 00:00:35,10
Then we'll initialize ESLint for our project

12
00:00:35,10 --> 00:00:40,40
by running npx eslint --init,

13
00:00:40,40 --> 00:00:43,00
and then it's going to ask us a series of questions,

14
00:00:43,00 --> 00:00:46,70
so we'll answer those,

15
00:00:46,70 --> 00:00:48,50
and to make things easy on ourselves,

16
00:00:48,50 --> 00:00:52,00
we're going to select Use a popular style guide, when it asks,

17
00:00:52,00 --> 00:00:55,00
How would you like to define a style for your project?

18
00:00:55,00 --> 00:00:59,30
This enables us to use a predefined set of rules.

19
00:00:59,30 --> 00:01:05,30
And we're going to select Airbnbs.

20
00:01:05,30 --> 00:01:06,60
And now we see that ESLint

21
00:01:06,60 --> 00:01:10,80
created this eslintrc.js file for us.

22
00:01:10,80 --> 00:01:14,00
This file tells ESLint what to look for in our code.

