1
00:00:00,50 --> 00:00:01,70
- [Instructor] The second main concept

2
00:00:01,70 --> 00:00:03,80
of functional programming is the separation

3
00:00:03,80 --> 00:00:05,60
of data and functions.

4
00:00:05,60 --> 00:00:08,10
This is a case where functional programming is directly

5
00:00:08,10 --> 00:00:10,50
in contrast with object-oriented programming,

6
00:00:10,50 --> 00:00:13,70
where data and functions are almost always grouped together.

7
00:00:13,70 --> 00:00:15,80
Let's see what this looks like.

8
00:00:15,80 --> 00:00:17,10
First of all for our purposes,

9
00:00:17,10 --> 00:00:20,40
let's discuss what data and functions actually are.

10
00:00:20,40 --> 00:00:22,50
For our purposes, data might be any values

11
00:00:22,50 --> 00:00:23,80
that a program contains,

12
00:00:23,80 --> 00:00:25,10
people's names, addresses,

13
00:00:25,10 --> 00:00:27,80
and social security numbers in a payroll program,

14
00:00:27,80 --> 00:00:29,30
the models, years, and colors

15
00:00:29,30 --> 00:00:31,60
of cars on a used car website,

16
00:00:31,60 --> 00:00:33,30
the positions, health levels and weapons

17
00:00:33,30 --> 00:00:36,00
of characters in a video game, anything.

18
00:00:36,00 --> 00:00:37,30
All of this is data.

19
00:00:37,30 --> 00:00:39,20
In object-oriented programming,

20
00:00:39,20 --> 00:00:42,00
this data is usually wrapped up inside objects

21
00:00:42,00 --> 00:00:43,20
as member variables,

22
00:00:43,20 --> 00:00:45,40
and the only way we can access it is

23
00:00:45,40 --> 00:00:47,70
using an object's methods.

24
00:00:47,70 --> 00:00:49,80
In functional programming on the other hand,

25
00:00:49,80 --> 00:00:53,20
this data is represented by simple arrays and hashes,

26
00:00:53,20 --> 00:00:56,30
or in this case, JavaScript objects.

27
00:00:56,30 --> 00:00:58,10
And a function is any operation

28
00:00:58,10 --> 00:00:59,40
that we can apply to our data

29
00:00:59,40 --> 00:01:01,90
to convert it into useful information.

30
00:01:01,90 --> 00:01:04,00
For example, if we want to find the average salary

31
00:01:04,00 --> 00:01:06,00
of programmers at our company,

32
00:01:06,00 --> 00:01:07,90
find all the cars on our website

33
00:01:07,90 --> 00:01:10,00
that were made after a certain year,

34
00:01:10,00 --> 00:01:14,20
or find if two characters are colliding in our video game.

35
00:01:14,20 --> 00:01:15,90
In object-oriented programming,

36
00:01:15,90 --> 00:01:18,00
functions are wrapped up inside objects

37
00:01:18,00 --> 00:01:20,40
along with the data that they operate on,

38
00:01:20,40 --> 00:01:24,00
and allow us to access or make changes to that data.

39
00:01:24,00 --> 00:01:26,10
In functional programming on the other hand,

40
00:01:26,10 --> 00:01:28,10
functions are completely separate entities

41
00:01:28,10 --> 00:01:30,20
from the data that they operate on.

42
00:01:30,20 --> 00:01:32,40
In order to operate on given data,

43
00:01:32,40 --> 00:01:34,90
the data must be passed as arguments to the function

44
00:01:34,90 --> 00:01:36,90
instead of using the this keyword

45
00:01:36,90 --> 00:01:39,50
as in object-oriented programming.

46
00:01:39,50 --> 00:01:41,50
And because of the rule of immutability,

47
00:01:41,50 --> 00:01:42,80
they should never make changes

48
00:01:42,80 --> 00:01:44,20
to any of the data they touch.

49
00:01:44,20 --> 00:01:47,00
They only return a modified copy of that data.

