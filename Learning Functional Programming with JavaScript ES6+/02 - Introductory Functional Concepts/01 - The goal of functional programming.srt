1
00:00:00,50 --> 00:00:02,40
- [Instructor] So the big question most people ask

2
00:00:02,40 --> 00:00:05,00
when they hear about how useful functional programming is

3
00:00:05,00 --> 00:00:07,70
is, "What exactly is functional programming?"

4
00:00:07,70 --> 00:00:09,40
Well, to answer this question,

5
00:00:09,40 --> 00:00:11,50
it might be helpful to consider the question

6
00:00:11,50 --> 00:00:14,10
of why we would want to use functional programming

7
00:00:14,10 --> 00:00:15,60
in the first place.

8
00:00:15,60 --> 00:00:17,20
In other words, what are the problems

9
00:00:17,20 --> 00:00:20,20
that functional programming sets out to solve?

10
00:00:20,20 --> 00:00:21,40
Well, if you currently use

11
00:00:21,40 --> 00:00:25,30
an object-oriented programming language such as Java or C++

12
00:00:25,30 --> 00:00:28,70
or especially a procedural programming language such as C,

13
00:00:28,70 --> 00:00:31,30
you've probably run into this problem before.

14
00:00:31,30 --> 00:00:32,90
There are certain bugs that are difficult

15
00:00:32,90 --> 00:00:34,40
to track down and fix,

16
00:00:34,40 --> 00:00:36,30
partly because they're hard to recreate,

17
00:00:36,30 --> 00:00:38,60
it takes a long, convoluted series of steps

18
00:00:38,60 --> 00:00:39,90
to make them show up,

19
00:00:39,90 --> 00:00:41,80
and partly because even when you figure out

20
00:00:41,80 --> 00:00:43,00
how to recreate them,

21
00:00:43,00 --> 00:00:45,00
it's next to impossible for you to keep track

22
00:00:45,00 --> 00:00:49,20
of all the changes that occur while the program is running.

23
00:00:49,20 --> 00:00:51,80
In a typical enterprise-sized program,

24
00:00:51,80 --> 00:00:54,30
thousands of variables are being operated on

25
00:00:54,30 --> 00:00:57,10
in thousands of different places at runtime.

26
00:00:57,10 --> 00:01:00,00
And the application can get itself into buggy states

27
00:01:00,00 --> 00:01:02,70
which can be difficult to recreate.

28
00:01:02,70 --> 00:01:04,60
This is exactly the type of situation

29
00:01:04,60 --> 00:01:07,90
that functional programming aims to avoid.

30
00:01:07,90 --> 00:01:10,70
Once you learn to think and program in a functional style,

31
00:01:10,70 --> 00:01:13,70
this type of situation will become much less frequent.

32
00:01:13,70 --> 00:01:15,30
You'll learn many techniques that can be used

33
00:01:15,30 --> 00:01:17,10
to create very large, powerful,

34
00:01:17,10 --> 00:01:19,90
and virtually bug-free code bases that are composed

35
00:01:19,90 --> 00:01:24,40
of many smaller, self-contained, and easily testable parts.

36
00:01:24,40 --> 00:01:27,50
So then, at its core, functional programming is concerned

37
00:01:27,50 --> 00:01:30,30
with taking a large number of complex ideas present

38
00:01:30,30 --> 00:01:32,30
in any large computer program

39
00:01:32,30 --> 00:01:34,50
and organizing them in a coherent way,

40
00:01:34,50 --> 00:01:36,10
while at the same time making sure

41
00:01:36,10 --> 00:01:39,40
that the code remains easy to test and modify.

42
00:01:39,40 --> 00:01:41,30
Of course, this sort of organization is

43
00:01:41,30 --> 00:01:44,00
what object-oriented programming aims to do as well,

44
00:01:44,00 --> 00:01:46,60
although it achieves it with only varied success.

45
00:01:46,60 --> 00:01:48,30
The difference between object-oriented

46
00:01:48,30 --> 00:01:50,70
and functional programming is in the choices they make

47
00:01:50,70 --> 00:01:53,50
with regards to organizing code.

48
00:01:53,50 --> 00:01:55,00
As you probably know already,

49
00:01:55,00 --> 00:01:58,10
the basic idea behind object-oriented programming is

50
00:01:58,10 --> 00:02:00,00
that humans think in terms of objects

51
00:02:00,00 --> 00:02:02,20
and relationships between these objects,

52
00:02:02,20 --> 00:02:04,60
and therefore computer programs should be organized

53
00:02:04,60 --> 00:02:06,00
in terms of objects

54
00:02:06,00 --> 00:02:08,20
so that we can leverage our existing thought patterns

55
00:02:08,20 --> 00:02:10,80
to write programs more easily.

56
00:02:10,80 --> 00:02:12,20
The problem with this, of course,

57
00:02:12,20 --> 00:02:14,90
is that our thought patterns are often not as well-defined

58
00:02:14,90 --> 00:02:17,00
as we sometimes like to think.

59
00:02:17,00 --> 00:02:19,00
Once we go beyond contrived examples

60
00:02:19,00 --> 00:02:21,80
such as SUV is a subclass of car,

61
00:02:21,80 --> 00:02:23,70
car is a subclass of vehicle,

62
00:02:23,70 --> 00:02:26,70
while demonstrates the concept, but isn't very helpful,

63
00:02:26,70 --> 00:02:28,60
the object-oriented programming doctrine

64
00:02:28,60 --> 00:02:32,40
doesn't provide much guidance regarding code organization.

65
00:02:32,40 --> 00:02:34,70
We're left with a rather daunting array of concepts

66
00:02:34,70 --> 00:02:37,60
like interfaces, subclasses, constructors,

67
00:02:37,60 --> 00:02:39,80
public, private, and protected data,

68
00:02:39,80 --> 00:02:43,40
accessors, mutators, and mother overriding,

69
00:02:43,40 --> 00:02:45,80
all of which form a complicated web of relationships

70
00:02:45,80 --> 00:02:48,40
and are prone to abuse by all but the most experienced

71
00:02:48,40 --> 00:02:51,30
and disciplined programmers.

72
00:02:51,30 --> 00:02:53,20
On the other hand, functional programming aims

73
00:02:53,20 --> 00:02:54,10
to bring the precision

74
00:02:54,10 --> 00:02:57,20
of mathematical functions into computer programs.

75
00:02:57,20 --> 00:02:58,40
While there will always be room

76
00:02:58,40 --> 00:03:00,20
for human error in programming

77
00:03:00,20 --> 00:03:02,40
no matter what paradigm is being used,

78
00:03:02,40 --> 00:03:05,10
adding the provable certainty that comes with mathematics

79
00:03:05,10 --> 00:03:09,10
into computer programming makes it far easier to avoid bugs.

80
00:03:09,10 --> 00:03:11,30
Imagine if we were able to represent all parts

81
00:03:11,30 --> 00:03:13,90
of a computer program as simply as a mathematical function

82
00:03:13,90 --> 00:03:17,10
such as function of x equals x plus one.

83
00:03:17,10 --> 00:03:21,10
Where could a bug possibly be hiding in functions like this?

84
00:03:21,10 --> 00:03:22,50
We'll look further into the details

85
00:03:22,50 --> 00:03:23,50
of functional programming,

86
00:03:23,50 --> 00:03:25,40
along with the many differences between it

87
00:03:25,40 --> 00:03:26,80
and object-oriented programming

88
00:03:26,80 --> 00:03:29,10
throughout the rest of the course.

89
00:03:29,10 --> 00:03:31,10
Since many of you are probably most familiar

90
00:03:31,10 --> 00:03:32,90
with object-oriented programming,

91
00:03:32,90 --> 00:03:35,10
I'll continue to use it in a lot of comparisons

92
00:03:35,10 --> 00:03:36,20
with functional programming

93
00:03:36,20 --> 00:03:38,00
to clarify the concepts I show you.

