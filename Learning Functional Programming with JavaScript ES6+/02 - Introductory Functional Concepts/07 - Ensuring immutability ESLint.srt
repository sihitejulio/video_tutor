1
00:00:00,50 --> 00:00:02,10
- [Instructor] So, I talked in the previous video

2
00:00:02,10 --> 00:00:03,90
about the importance of immutability

3
00:00:03,90 --> 00:00:05,50
in functional programming.

4
00:00:05,50 --> 00:00:07,70
Immutability allows us to avoid the many bugs

5
00:00:07,70 --> 00:00:09,10
that occur in computer programs

6
00:00:09,10 --> 00:00:11,30
as a side effect of state change.

7
00:00:11,30 --> 00:00:12,90
It also means that we don't have to worry

8
00:00:12,90 --> 00:00:14,50
about using private variables

9
00:00:14,50 --> 00:00:17,50
to avoid changes coming from unexpected places,

10
00:00:17,50 --> 00:00:21,20
since we simply can't make changes to any piece of data.

11
00:00:21,20 --> 00:00:23,00
That being said, if we want to ensure

12
00:00:23,00 --> 00:00:25,30
complete immutability in JavaScript,

13
00:00:25,30 --> 00:00:27,60
there's something we need to keep in mind.

14
00:00:27,60 --> 00:00:29,80
While using the const keyword prevents us

15
00:00:29,80 --> 00:00:32,50
from directly changing the value of a piece of data,

16
00:00:32,50 --> 00:00:36,20
for example if we say const x equals five,

17
00:00:36,20 --> 00:00:39,00
we can't simply say x equals six,

18
00:00:39,00 --> 00:00:41,00
since this will throw an error.

19
00:00:41,00 --> 00:00:43,80
However, using const in JavaScript is a little funny

20
00:00:43,80 --> 00:00:46,50
when working with arrays and objects.

21
00:00:46,50 --> 00:00:49,10
For example, if we define an array called numbers,

22
00:00:49,10 --> 00:00:53,00
like this, const numbers,

23
00:00:53,00 --> 00:00:56,00
one two three four five,

24
00:00:56,00 --> 00:00:59,10
we can never directly redefine numbers as a different array

25
00:00:59,10 --> 00:01:06,10
like this, numbers equals hello.

26
00:01:06,10 --> 00:01:08,50
But we can modify the individual elements

27
00:01:08,50 --> 00:01:10,60
of an array like this.

28
00:01:10,60 --> 00:01:15,40
Numbers zero equals 100.

29
00:01:15,40 --> 00:01:17,50
And JavaScript won't complain about this at all,

30
00:01:17,50 --> 00:01:19,30
even though we've defined our numbers array

31
00:01:19,30 --> 00:01:21,60
using the const keyword.

32
00:01:21,60 --> 00:01:23,50
Also, if we're not careful, we can accidentally

33
00:01:23,50 --> 00:01:26,40
call a mutating array method such as reverse,

34
00:01:26,40 --> 00:01:29,60
on a supposedly immutable array like this.

35
00:01:29,60 --> 00:01:34,50
Numbers dot reverse, and this will actually

36
00:01:34,50 --> 00:01:36,00
modify the original array without

37
00:01:36,00 --> 00:01:38,10
any complaint from JavaScript.

38
00:01:38,10 --> 00:01:40,20
And the same is true for objects as well.

39
00:01:40,20 --> 00:01:42,70
If we define an object called person,

40
00:01:42,70 --> 00:01:46,70
const person, with name and age properties,

41
00:01:46,70 --> 00:01:52,90
name John Doe, age 34,

42
00:01:52,90 --> 00:01:54,80
we can never directly redefine person

43
00:01:54,80 --> 00:02:01,60
as another object like this, person equals name Bob,

44
00:02:01,60 --> 00:02:03,90
but if we try and reset a property of our object

45
00:02:03,90 --> 00:02:08,60
like this, person dot name equals Bob,

46
00:02:08,60 --> 00:02:10,50
JavaScript won't complain even though

47
00:02:10,50 --> 00:02:12,90
this is modifying our data.

48
00:02:12,90 --> 00:02:14,40
Now, I'm going to show you an approach

49
00:02:14,40 --> 00:02:17,10
that we can use to prevent mutation in our code,

50
00:02:17,10 --> 00:02:20,20
and this is by using ESLint rules.

51
00:02:20,20 --> 00:02:22,80
If you've never worked with or heard of ESLint before,

52
00:02:22,80 --> 00:02:24,90
it's an extremely helpful tool that helps you

53
00:02:24,90 --> 00:02:28,50
catch errors or stylistic flaws in your code.

54
00:02:28,50 --> 00:02:31,50
ESLint won't add anything extra to your code,

55
00:02:31,50 --> 00:02:34,30
we can simply run it and it looks at our existing code

56
00:02:34,30 --> 00:02:35,90
and tells us any problems it finds,

57
00:02:35,90 --> 00:02:39,80
for example if we're doing accidental mutation.

58
00:02:39,80 --> 00:02:41,80
Now there are many different ESLint plugins

59
00:02:41,80 --> 00:02:44,00
that we can use depending on our coding style

60
00:02:44,00 --> 00:02:46,00
and concerns, there are ones that check

61
00:02:46,00 --> 00:02:48,00
for correct indentation, ones that make sure

62
00:02:48,00 --> 00:02:51,90
you put semicolons at the end of every line, et cetera.

63
00:02:51,90 --> 00:02:53,60
But for the purposes of this course,

64
00:02:53,60 --> 00:02:55,80
the Lint rules we're interested in are the ones

65
00:02:55,80 --> 00:02:57,10
that will automatically tell us

66
00:02:57,10 --> 00:02:59,70
if we try and mutate anything in our code.

67
00:02:59,70 --> 00:03:02,10
There are several existing plugins that already implement

68
00:03:02,10 --> 00:03:04,30
this functionality, and the one we're going to use

69
00:03:04,30 --> 00:03:07,40
is called ESLint plugin immutable.

70
00:03:07,40 --> 00:03:09,00
I'm not going to go through all the details

71
00:03:09,00 --> 00:03:11,10
about ESLint here, but I'll just walk you

72
00:03:11,10 --> 00:03:14,00
through the steps required to get this set up.

