1
00:00:00,50 --> 00:00:01,30
- [Instructor] If you're coming

2
00:00:01,30 --> 00:00:02,80
from an object-oriented background,

3
00:00:02,80 --> 00:00:05,20
it may seem strange to you that functional programming

4
00:00:05,20 --> 00:00:08,50
places an emphasis on keeping data and functions separate,

5
00:00:08,50 --> 00:00:10,10
since one of the central concepts

6
00:00:10,10 --> 00:00:11,50
of object-oriented programming

7
00:00:11,50 --> 00:00:14,40
is that we put data and the functions related to that data

8
00:00:14,40 --> 00:00:16,80
together in objects.

9
00:00:16,80 --> 00:00:18,50
To understand why it makes sense

10
00:00:18,50 --> 00:00:20,10
to keep data and functions separate

11
00:00:20,10 --> 00:00:21,50
in functional programming,

12
00:00:21,50 --> 00:00:23,00
it might be helpful to think about

13
00:00:23,00 --> 00:00:25,60
why we keep them together in object-oriented programming

14
00:00:25,60 --> 00:00:27,60
in the first place.

15
00:00:27,60 --> 00:00:29,00
The main reason that it's useful

16
00:00:29,00 --> 00:00:32,10
to keep data and functions together in the same object

17
00:00:32,10 --> 00:00:33,60
is that this allows us to interact

18
00:00:33,60 --> 00:00:35,20
with the member variables of an object

19
00:00:35,20 --> 00:00:37,20
without touching them directly.

20
00:00:37,20 --> 00:00:40,30
What's wrong with touching variables directly?

21
00:00:40,30 --> 00:00:43,10
Well, if we give programmers direct access to variables,

22
00:00:43,10 --> 00:00:45,40
they can, and in my experience will,

23
00:00:45,40 --> 00:00:47,80
change them in ways that they're not supposed to.

24
00:00:47,80 --> 00:00:50,00
Consider the simple case of a Person object

25
00:00:50,00 --> 00:00:51,60
with publicly accessible properties

26
00:00:51,60 --> 00:00:54,70
firstName, lastName, and initials.

27
00:00:54,70 --> 00:00:56,10
Now, if we allow programmers

28
00:00:56,10 --> 00:00:58,20
to change these properties directly,

29
00:00:58,20 --> 00:01:00,30
they have to remember to reset initials

30
00:01:00,30 --> 00:01:03,30
every time they change either firstName or lastName,

31
00:01:03,30 --> 00:01:05,20
which they're almost certain to forget somewhere,

32
00:01:05,20 --> 00:01:08,00
which introduces bugs in the program.

33
00:01:08,00 --> 00:01:09,50
The most popular solution to this

34
00:01:09,50 --> 00:01:11,30
is to make the variables private,

35
00:01:11,30 --> 00:01:12,90
which in JavaScript is usually done

36
00:01:12,90 --> 00:01:15,60
by just adding an underscore before the variable name

37
00:01:15,60 --> 00:01:18,00
and telling developers not to touch it,

38
00:01:18,00 --> 00:01:20,80
and then we allow programmers access to the variables

39
00:01:20,80 --> 00:01:22,10
only through functions,

40
00:01:22,10 --> 00:01:26,00
which makes sure that the variables are kept up-to-date.

41
00:01:26,00 --> 00:01:27,90
In functional programming, however,

42
00:01:27,90 --> 00:01:30,40
we don't need to worry about making all our data private

43
00:01:30,40 --> 00:01:33,80
because programmers can't alter the data in unwanted ways.

44
00:01:33,80 --> 00:01:35,70
Because of the rule of immutability,

45
00:01:35,70 --> 00:01:37,80
which we discussed in the previous video,

46
00:01:37,80 --> 00:01:40,50
the data remains a constant source of truth.

47
00:01:40,50 --> 00:01:42,80
Note that in JavaScript we need a little extra help

48
00:01:42,80 --> 00:01:45,60
to keep developers from modifying our object properties,

49
00:01:45,60 --> 00:01:48,50
but we'll talk about that in a coming video.

50
00:01:48,50 --> 00:01:50,90
If we want to transform the data in some way,

51
00:01:50,90 --> 00:01:52,80
such as changing someone's name,

52
00:01:52,80 --> 00:01:54,50
we simply define a new constant

53
00:01:54,50 --> 00:01:57,00
that represents the updated data.

54
00:01:57,00 --> 00:01:58,60
We then use this updated constant

55
00:01:58,60 --> 00:02:00,10
in all our future calculations,

56
00:02:00,10 --> 00:02:03,30
knowing exactly what data it represents.

57
00:02:03,30 --> 00:02:04,80
Let's take a look at a simple example

58
00:02:04,80 --> 00:02:06,70
to demonstrate how functional programming

59
00:02:06,70 --> 00:02:08,00
and object-oriented programming

60
00:02:08,00 --> 00:02:10,70
handle functions and data differently.

61
00:02:10,70 --> 00:02:12,20
Let's imagine the classic example

62
00:02:12,20 --> 00:02:14,60
of creating a simple to-do list program

63
00:02:14,60 --> 00:02:18,10
that helps people keep track of tasks they need to complete.

64
00:02:18,10 --> 00:02:19,00
Let's look at

65
00:02:19,00 --> 00:02:21,20
what an object-oriented approach to this program

66
00:02:21,20 --> 00:02:22,40
would look like.

67
00:02:22,40 --> 00:02:26,00
First we'd create a TodoItem class that contains properties

68
00:02:26,00 --> 00:02:28,60
such as the name, category, and deadline for the item,

69
00:02:28,60 --> 00:02:30,50
and whether or not it's been completed,

70
00:02:30,50 --> 00:02:34,80
as well as functions such as changeName or mark as done.

71
00:02:34,80 --> 00:02:35,60
Then, of course,

72
00:02:35,60 --> 00:02:38,70
our getters to access our private variables.

73
00:02:38,70 --> 00:02:41,20
Then we might have another class called TodoList,

74
00:02:41,20 --> 00:02:43,30
which contains an array of TodoItems,

75
00:02:43,30 --> 00:02:45,80
as well as functions for adding or removing items,

76
00:02:45,80 --> 00:02:48,50
or sorting them in different ways.

77
00:02:48,50 --> 00:02:50,60
In functional programming, on the other hand,

78
00:02:50,60 --> 00:02:53,40
we'd keep the data and functions separate.

79
00:02:53,40 --> 00:02:55,40
Remember that in functional programming,

80
00:02:55,40 --> 00:02:57,50
instead of wrapping up our data's properties

81
00:02:57,50 --> 00:03:00,00
as member variables inside classes,

82
00:03:00,00 --> 00:03:01,40
we usually represent data

83
00:03:01,40 --> 00:03:04,50
using constructs such as arrays or hashes,

84
00:03:04,50 --> 00:03:08,30
which in JavaScript, somewhat confusingly, we call objects.

85
00:03:08,30 --> 00:03:09,30
For this program

86
00:03:09,30 --> 00:03:12,40
we might have JavaScript objects that represent todoItems,

87
00:03:12,40 --> 00:03:15,60
with name, category, and deadline properties,

88
00:03:15,60 --> 00:03:18,10
and the list itself, which we would simply represent

89
00:03:18,10 --> 00:03:21,10
with an array that holds a bunch of todoItems.

90
00:03:21,10 --> 00:03:22,60
Then, outside of these classes,

91
00:03:22,60 --> 00:03:23,70
we define the functions

92
00:03:23,70 --> 00:03:25,60
for combining or transforming our data,

93
00:03:25,60 --> 00:03:29,00
such as sorting it or marking an item as done.

