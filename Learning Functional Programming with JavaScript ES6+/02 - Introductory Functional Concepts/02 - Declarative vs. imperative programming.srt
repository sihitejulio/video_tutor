1
00:00:00,50 --> 00:00:01,70
- [Instructor] One of the first things you'll want

2
00:00:01,70 --> 00:00:03,70
to understand about functional programming

3
00:00:03,70 --> 00:00:05,90
is that it's a declarative style of programming

4
00:00:05,90 --> 00:00:08,60
as opposed to things like object or in a programming

5
00:00:08,60 --> 00:00:11,20
and procedural programming which are imperative styles

6
00:00:11,20 --> 00:00:12,60
of programming.

7
00:00:12,60 --> 00:00:14,50
What this means is that functional programming

8
00:00:14,50 --> 00:00:16,40
puts more focus on what things are

9
00:00:16,40 --> 00:00:19,20
as opposed to how to get them.

10
00:00:19,20 --> 00:00:20,70
A simple example of this would be

11
00:00:20,70 --> 00:00:22,70
if we were to talk about houses.

12
00:00:22,70 --> 00:00:25,50
If we wanted to describe a house in a declarative way,

13
00:00:25,50 --> 00:00:27,90
if we wanted to describe what a house is,

14
00:00:27,90 --> 00:00:31,10
we might say that a house is a foundation, four walls

15
00:00:31,10 --> 00:00:33,20
and a roof on top.

16
00:00:33,20 --> 00:00:35,40
On the other hand, if we were describing a house

17
00:00:35,40 --> 00:00:37,00
in an imperative way,

18
00:00:37,00 --> 00:00:39,90
if we wanted to describe how to make a house,

19
00:00:39,90 --> 00:00:42,00
we might say "In order to make a house,

20
00:00:42,00 --> 00:00:44,80
"you need to pour the foundation, build four walls

21
00:00:44,80 --> 00:00:47,30
"and then put a roof on top."

22
00:00:47,30 --> 00:00:48,70
Let's expand on this a bit

23
00:00:48,70 --> 00:00:51,50
and look at a more programming related example.

24
00:00:51,50 --> 00:00:53,10
Let's say that we want to write a program

25
00:00:53,10 --> 00:00:55,90
that tells us the average of an array of numbers.

26
00:00:55,90 --> 00:00:58,60
Imperative programming would specify the steps required

27
00:00:58,60 --> 00:01:00,10
to calculate the result.

28
00:01:00,10 --> 00:01:01,30
Something like this.

29
00:01:01,30 --> 00:01:03,60
First we set x equal to zero.

30
00:01:03,60 --> 00:01:07,90
Second, we add the first number in the array to x

31
00:01:07,90 --> 00:01:10,80
and then we repeat step two for the rest of the numbers

32
00:01:10,80 --> 00:01:12,00
in the array

33
00:01:12,00 --> 00:01:15,80
and finally, we divide x by the length of the array.

34
00:01:15,80 --> 00:01:18,70
Now, unless you've worked with functional programming before

35
00:01:18,70 --> 00:01:21,10
this is is probably how you're used to writing programs

36
00:01:21,10 --> 00:01:23,50
thinking of how to get the result.

37
00:01:23,50 --> 00:01:25,10
On the other hand, if we wanted to solve

38
00:01:25,10 --> 00:01:27,20
the same problem in a declarative way,

39
00:01:27,20 --> 00:01:29,80
we could simply say that x is the sum of all the numbers

40
00:01:29,80 --> 00:01:32,90
in the array divided by the length of the array.

41
00:01:32,90 --> 00:01:35,60
Or if you're more mathematically inclined,

42
00:01:35,60 --> 00:01:39,50
we could rewrite it using mathematical notation like this.

43
00:01:39,50 --> 00:01:41,50
Now, if it never occurred to your that it's possible

44
00:01:41,50 --> 00:01:43,00
to write programs in this way,

45
00:01:43,00 --> 00:01:45,50
by simply stating what something is,

46
00:01:45,50 --> 00:01:46,90
you're not alone.

47
00:01:46,90 --> 00:01:47,80
YOu've probably noticed

48
00:01:47,80 --> 00:01:50,00
that talking about things declaratively,

49
00:01:50,00 --> 00:01:52,90
talking about what they are instead of how to get them,

50
00:01:52,90 --> 00:01:54,10
feels a lot more natural

51
00:01:54,10 --> 00:01:57,40
and well frankly, it's a lot easier for many problems.

52
00:01:57,40 --> 00:01:58,80
This way of talking about things

53
00:01:58,80 --> 00:02:01,30
may also remind some of you of mathematical functions

54
00:02:01,30 --> 00:02:03,10
such as these.

55
00:02:03,10 --> 00:02:04,50
And as I mentioned before,

56
00:02:04,50 --> 00:02:07,30
this is one of the main ideas behind functional programming,

57
00:02:07,30 --> 00:02:09,40
to bring the precision of mathematical functions

58
00:02:09,40 --> 00:02:11,10
into our programs.

59
00:02:11,10 --> 00:02:12,00
As you may have figured,

60
00:02:12,00 --> 00:02:15,70
this is also where functional programming gets its name.

61
00:02:15,70 --> 00:02:18,30
So it's all very easy to say that functional programming

62
00:02:18,30 --> 00:02:21,20
brings the precision of mathematics into programming.

63
00:02:21,20 --> 00:02:23,40
But what does that mean exactly?

64
00:02:23,40 --> 00:02:25,00
Well, there are three main concepts

65
00:02:25,00 --> 00:02:26,70
behind functional programming which allow us

66
00:02:26,70 --> 00:02:28,00
to do just that.

67
00:02:28,00 --> 00:02:29,50
These concepts are:

68
00:02:29,50 --> 00:02:31,30
one, immutability,

69
00:02:31,30 --> 00:02:33,80
two, separating functions and data

70
00:02:33,80 --> 00:02:36,70
and three, first-class functions.

71
00:02:36,70 --> 00:02:38,50
Over the course of the next few videos,

72
00:02:38,50 --> 00:02:41,50
I'll go into depth on what each of these concepts means,

73
00:02:41,50 --> 00:02:43,90
contrast it with how other paradigms do things

74
00:02:43,90 --> 00:02:46,00
and explain its advantages.

