1
00:00:00,50 --> 00:00:02,20
The next thing we're going to do is install the

2
00:00:02,20 --> 00:00:05,30
eslint plugin immutable package for our project.

3
00:00:05,30 --> 00:00:07,60
So in the terminal, we'll type

4
00:00:07,60 --> 00:00:16,30
npm install dash dash save-dev eslint-plugin dash immutable

5
00:00:16,30 --> 00:00:19,80
and hit enter, and then all have to do

6
00:00:19,80 --> 00:00:23,20
is add some of these rules to our eslint rc file.

7
00:00:23,20 --> 00:00:26,70
So we'll open up eslintrc.js

8
00:00:26,70 --> 00:00:29,80
and add a plugins property right above rules.

9
00:00:29,80 --> 00:00:34,00
Plugins, and it's going to be an array,

10
00:00:34,00 --> 00:00:36,70
and then we simply add our immutable package to it.

11
00:00:36,70 --> 00:00:42,70
Like this: immutable, and don't forget to add the commas.

12
00:00:42,70 --> 00:00:44,80
Then, inside the rules object,

13
00:00:44,80 --> 00:00:48,00
we're going to add a rule from immutable.

14
00:00:48,00 --> 00:00:50,40
Specifically, we're going to use the immutable

15
00:00:50,40 --> 00:00:53,60
slash no dash mutation rule.

16
00:00:53,60 --> 00:00:55,50
And we're going to give it the value two

17
00:00:55,50 --> 00:00:57,90
because we want eslint to treat this as an error

18
00:00:57,90 --> 00:01:01,30
if it finds it, not just a warning, which would be one.

19
00:01:01,30 --> 00:01:03,00
And for those of you who are wondering,

20
00:01:03,00 --> 00:01:05,40
if we want to specifically disable a rule,

21
00:01:05,40 --> 00:01:07,80
we could set this value to zero like this.

22
00:01:07,80 --> 00:01:11,00
But we're going to leave it as two.

23
00:01:11,00 --> 00:01:15,70
Let's save our file, as well.

24
00:01:15,70 --> 00:01:17,90
So now, if we delete all this code,

25
00:01:17,90 --> 00:01:19,90
and rewrite some code that we might find

26
00:01:19,90 --> 00:01:23,70
when we're accidentally mutating our data, like this,

27
00:01:23,70 --> 00:01:33,20
const person equals name John Doe, and age equals 34,

28
00:01:33,20 --> 00:01:35,80
and then we try and mutate the name property,

29
00:01:35,80 --> 00:01:41,60
person dot name equals Bob, and then we save our code,

30
00:01:41,60 --> 00:01:46,30
and then if we go into our terminal and type npx eslint,

31
00:01:46,30 --> 00:01:48,60
and then the directory that we want to run lint on,

32
00:01:48,60 --> 00:01:51,30
in this case, just the current directory we were in,

33
00:01:51,30 --> 00:01:55,50
so we'll use dot, and hit enter,

34
00:01:55,50 --> 00:01:59,10
we see that, in addition to a few other lint errors,

35
00:01:59,10 --> 00:02:00,80
eslint also gives us an error saying that

36
00:02:00,80 --> 00:02:03,40
no object mutation is allowed.

37
00:02:03,40 --> 00:02:05,20
Keep in mind also that if you don't want to use

38
00:02:05,20 --> 00:02:07,40
the default rules from Airbnb,

39
00:02:07,40 --> 00:02:10,30
you can go into your eslint rc,

40
00:02:10,30 --> 00:02:15,20
and simply delete extends:'airbnb-base' from the object.

41
00:02:15,20 --> 00:02:17,80
But I'm going to leave it in there.

42
00:02:17,80 --> 00:02:19,00
So there you have it.

43
00:02:19,00 --> 00:02:20,70
When we're creating a functional code base

44
00:02:20,70 --> 00:02:22,90
and want to ensure that we don't accidentally mutate

45
00:02:22,90 --> 00:02:25,10
any of our data, we can set up eslint

46
00:02:25,10 --> 00:02:27,00
to catch any mutations for us.

