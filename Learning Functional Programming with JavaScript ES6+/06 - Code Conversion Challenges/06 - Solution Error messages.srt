1
00:00:00,40 --> 00:00:05,60
(upbeat music)

2
00:00:05,60 --> 00:00:06,50
- [Instructor] So now I'm going to show you

3
00:00:06,50 --> 00:00:08,40
how I solve this challenge.

4
00:00:08,40 --> 00:00:09,60
I want to emphasize, again,

5
00:00:09,60 --> 00:00:11,10
that this is just one of the many ways

6
00:00:11,10 --> 00:00:12,40
that it could've been done.

7
00:00:12,40 --> 00:00:13,50
You might've done it differently,

8
00:00:13,50 --> 00:00:16,00
and that's probably just as good.

9
00:00:16,00 --> 00:00:18,50
So first let's decide what our input criteria

10
00:00:18,50 --> 00:00:19,90
is going to look like.

11
00:00:19,90 --> 00:00:22,10
I decided that when I was solving this problem,

12
00:00:22,10 --> 00:00:26,80
each of these was going to contain a series of functions.

13
00:00:26,80 --> 00:00:29,40
To do an example now, we'll just do one function,

14
00:00:29,40 --> 00:00:31,00
but I decided that this function

15
00:00:31,00 --> 00:00:32,50
was going to take one argument,

16
00:00:32,50 --> 00:00:34,90
which is the current value of this field,

17
00:00:34,90 --> 00:00:38,50
up in our current input values object,

18
00:00:38,50 --> 00:00:41,70
and these functions were going to test something.

19
00:00:41,70 --> 00:00:43,20
So for example, for our first name,

20
00:00:43,20 --> 00:00:45,20
we want to test if the length of the value,

21
00:00:45,20 --> 00:00:46,70
value.length,

22
00:00:46,70 --> 00:00:49,20
is greater than or equal to two.

23
00:00:49,20 --> 00:00:50,90
And then if this was true,

24
00:00:50,90 --> 00:00:53,30
I wanted to return just an empty string,

25
00:00:53,30 --> 00:00:57,80
and if it was false, I wanted to return an error message.

26
00:00:57,80 --> 00:01:05,60
So we can say first name must be at least two characters.

27
00:01:05,60 --> 00:01:07,70
And note that I used the ternary operator here,

28
00:01:07,70 --> 00:01:09,20
but you could've done this just as easily

29
00:01:09,20 --> 00:01:11,60
with an if statement.

30
00:01:11,60 --> 00:01:13,20
So this function here that tests if a string

31
00:01:13,20 --> 00:01:15,60
is at least two characters long,

32
00:01:15,60 --> 00:01:19,30
we're going to copy this

33
00:01:19,30 --> 00:01:22,50
and paste it in our last name criteria as well.

34
00:01:22,50 --> 00:01:24,30
And we're just going to change this here

35
00:01:24,30 --> 00:01:29,20
to say last name must be at least two characters,

36
00:01:29,20 --> 00:01:30,60
and then we're going to do something similar

37
00:01:30,60 --> 00:01:32,90
for the ZIP Code and state.

38
00:01:32,90 --> 00:01:36,10
We're going to copy and paste this in here,

39
00:01:36,10 --> 00:01:37,90
the only difference here is that we want to make sure

40
00:01:37,90 --> 00:01:40,70
the length is exactly equal to five,

41
00:01:40,70 --> 00:01:42,40
which is the length of standard ZIP Codes

42
00:01:42,40 --> 00:01:45,70
in the United States,

43
00:01:45,70 --> 00:01:47,00
and for the state,

44
00:01:47,00 --> 00:01:49,60
we wanted to make sure that the length was equal to two

45
00:01:49,60 --> 00:01:53,00
for the state abbreviations.

46
00:01:53,00 --> 00:01:55,90
And then we want to change the strings here as well.

47
00:01:55,90 --> 00:02:00,80
So we want to say ZIP Code must be exactly

48
00:02:00,80 --> 00:02:05,00
five characters long.

49
00:02:05,00 --> 00:02:08,00
And then we'll say state must be exactly

50
00:02:08,00 --> 00:02:10,90
two characters long.

51
00:02:10,90 --> 00:02:14,10
And let's save it.

52
00:02:14,10 --> 00:02:15,60
Now we have to figure out what the body

53
00:02:15,60 --> 00:02:18,90
of our get error messages function should look like.

54
00:02:18,90 --> 00:02:22,60
Basically we have to take our inputs and criteria

55
00:02:22,60 --> 00:02:26,10
and combine them in such a way that we get out an array

56
00:02:26,10 --> 00:02:31,70
of the error messages as we have here.

57
00:02:31,70 --> 00:02:35,90
So the first thing we're going to do is say return.

58
00:02:35,90 --> 00:02:39,00
We're going to get the keys from our inputs object,

59
00:02:39,00 --> 00:02:42,10
which are going to be the names of the different fields,

60
00:02:42,10 --> 00:02:48,10
like first name, last name, ZIP Code, and state,

61
00:02:48,10 --> 00:02:50,10
and we're going to do reduce here.

62
00:02:50,10 --> 00:02:52,20
And for the function we pass to reduce,

63
00:02:52,20 --> 00:02:54,20
we're going to have our accumulator,

64
00:02:54,20 --> 00:02:55,80
and then each of the elements we're mapping

65
00:02:55,80 --> 00:02:58,10
is going to be called field name.

66
00:02:58,10 --> 00:03:00,50
So since we want our final value to be an array,

67
00:03:00,50 --> 00:03:02,10
it makes sense that our starting value

68
00:03:02,10 --> 00:03:03,80
would be an array as well.

69
00:03:03,80 --> 00:03:05,40
And then for the body of this function

70
00:03:05,40 --> 00:03:07,10
that we pass to reduce,

71
00:03:07,10 --> 00:03:09,40
we're going to return an array here as well

72
00:03:09,40 --> 00:03:11,50
and what we want to do inside this array

73
00:03:11,50 --> 00:03:17,20
is say spread operator, accumulator,

74
00:03:17,20 --> 00:03:20,10
and then we're going to want to do the spread operator again

75
00:03:20,10 --> 00:03:22,50
and we're going to want to get the corresponding array of tests

76
00:03:22,50 --> 00:03:25,30
that we run from our criteria object,

77
00:03:25,30 --> 00:03:26,90
and that's going to look like this.

78
00:03:26,90 --> 00:03:30,80
Criteria, field name,

79
00:03:30,80 --> 00:03:36,10
so a field name is first name, for example, right here,

80
00:03:36,10 --> 00:03:38,80
it'll give us this array,

81
00:03:38,80 --> 00:03:42,00
and we want to map over that array,

82
00:03:42,00 --> 00:03:45,40
and for each of these elements we'll call them test,

83
00:03:45,40 --> 00:03:49,70
we want to call that test on the value of the field name

84
00:03:49,70 --> 00:03:54,40
of our inputs object, which will look like this.

85
00:03:54,40 --> 00:03:55,40
And let's make sure we're using

86
00:03:55,40 --> 00:03:57,50
the right variable names here.

87
00:03:57,50 --> 00:04:00,20
This is called inputs instead of input,

88
00:04:00,20 --> 00:04:03,20
and then if we save our code and run it,

89
00:04:03,20 --> 00:04:08,20
we see that we get all of our error messages correctly.

90
00:04:08,20 --> 00:04:10,40
Let's see what happens if we change some of these values

91
00:04:10,40 --> 00:04:13,50
in our current input values object,

92
00:04:13,50 --> 00:04:18,40
for example, if we had the correct input for first name.

93
00:04:18,40 --> 00:04:21,00
If we run our code again,

94
00:04:21,00 --> 00:04:23,10
we see that it no longer gives us the error message

95
00:04:23,10 --> 00:04:24,00
for the first name field,

96
00:04:24,00 --> 00:04:26,80
which is exactly what we'd expect.

97
00:04:26,80 --> 00:04:30,50
And if we put in the correct ZIP Code,

98
00:04:30,50 --> 00:04:35,00
and run it again,

99
00:04:35,00 --> 00:04:36,80
we can see that it no longer gives us an error

100
00:04:36,80 --> 00:04:39,10
for the ZIP Code either.

101
00:04:39,10 --> 00:04:41,90
So there's just one last thing we want to do.

102
00:04:41,90 --> 00:04:44,20
Instead of having these empty strings in our array,

103
00:04:44,20 --> 00:04:46,70
we only want our array to contain strings

104
00:04:46,70 --> 00:04:49,80
that actually have an error message,

105
00:04:49,80 --> 00:04:51,10
and that's pretty simple to do.

106
00:04:51,10 --> 00:04:54,70
All we have to do

107
00:04:54,70 --> 00:04:57,10
is at the end of this array that we're returning,

108
00:04:57,10 --> 00:04:59,00
type filter,

109
00:04:59,00 --> 00:05:01,60
and we want to make sure that the message exists,

110
00:05:01,60 --> 00:05:05,40
which we can do just like this.

111
00:05:05,40 --> 00:05:08,80
Now if we run our code again,

112
00:05:08,80 --> 00:05:11,00
we see that it got rid of the empty strings,

113
00:05:11,00 --> 00:05:14,50
and this works because empty strings are falsy values.

114
00:05:14,50 --> 00:05:16,90
So when our filter gets to an empty string,

115
00:05:16,90 --> 00:05:21,70
it will see this as falsy and leave the element out.

116
00:05:21,70 --> 00:05:24,10
So that's as far as I'm going to go,

117
00:05:24,10 --> 00:05:27,00
but if you want to improve this answer even more,

118
00:05:27,00 --> 00:05:30,40
notice that these functions are all very similar

119
00:05:30,40 --> 00:05:32,60
and this means that we can probably use the concept

120
00:05:32,60 --> 00:05:34,20
of first class functions

121
00:05:34,20 --> 00:05:36,50
to configure our functions for us

122
00:05:36,50 --> 00:05:39,80
so that we don't have to redefine them like this each time,

123
00:05:39,80 --> 00:05:41,00
but I'll leave that up to you.

