1
00:00:00,00 --> 00:00:05,60
(upbeat electronic music)

2
00:00:05,60 --> 00:00:07,90
- [Instructor] Now let's move on to our final challenge.

3
00:00:07,90 --> 00:00:09,40
This challenge is a lot more open ended

4
00:00:09,40 --> 00:00:11,50
than the previous two and it involves

5
00:00:11,50 --> 00:00:14,30
generating error messages for form values

6
00:00:14,30 --> 00:00:17,40
like we might see on any sign up form on a website.

7
00:00:17,40 --> 00:00:18,70
So up top here we have an object

8
00:00:18,70 --> 00:00:21,00
representing our form values.

9
00:00:21,00 --> 00:00:23,20
So for example we might have a first name input box,

10
00:00:23,20 --> 00:00:28,40
a last name input box, and ZIP code and state input boxes.

11
00:00:28,40 --> 00:00:31,00
And the value that we have for each of these properties

12
00:00:31,00 --> 00:00:35,10
is the current value that's in the corresponding text box.

13
00:00:35,10 --> 00:00:37,60
We then have a separate criteria object

14
00:00:37,60 --> 00:00:39,50
that contains the checks we want to perform

15
00:00:39,50 --> 00:00:42,50
on each of our input boxes.

16
00:00:42,50 --> 00:00:45,00
Now I mentioned that this challenge is pretty open ended,

17
00:00:45,00 --> 00:00:47,60
so I haven't filled any of these in yet.

18
00:00:47,60 --> 00:00:49,60
But the requirements for each of these fields

19
00:00:49,60 --> 00:00:51,40
is up at the top.

20
00:00:51,40 --> 00:00:53,50
So for example, the first name and last name

21
00:00:53,50 --> 00:00:56,20
have to be at least two characters long.

22
00:00:56,20 --> 00:00:59,10
The ZIP code has to be exactly five characters long

23
00:00:59,10 --> 00:01:01,90
and the state has to be exactly two characters long.

24
00:01:01,90 --> 00:01:05,40
So your task is to use these two objects

25
00:01:05,40 --> 00:01:07,00
and then write a function that takes both

26
00:01:07,00 --> 00:01:09,80
the input values as we have up here

27
00:01:09,80 --> 00:01:12,40
and the criteria as we have here

28
00:01:12,40 --> 00:01:15,60
and generates an array of error messages.

29
00:01:15,60 --> 00:01:19,10
I've put an example down below.

30
00:01:19,10 --> 00:01:22,30
Now because this challenge is a lot more open ended,

31
00:01:22,30 --> 00:01:23,40
the answer that I come up with

32
00:01:23,40 --> 00:01:25,70
is certainly not going to be the only one.

33
00:01:25,70 --> 00:01:28,20
But I will give you a hint at how I'll do it.

34
00:01:28,20 --> 00:01:30,80
Inside this input criteria object,

35
00:01:30,80 --> 00:01:33,00
for each of these properties,

36
00:01:33,00 --> 00:01:34,40
I'm going to create an array of functions

37
00:01:34,40 --> 00:01:36,90
that we use to check against the values

38
00:01:36,90 --> 00:01:38,90
up here in this object.

39
00:01:38,90 --> 00:01:40,80
So for example, our first name criteria

40
00:01:40,80 --> 00:01:42,70
might look something like this.

41
00:01:42,70 --> 00:01:45,30
It'll be a function that takes a value argument

42
00:01:45,30 --> 00:01:47,60
and then either returns an empty string

43
00:01:47,60 --> 00:01:50,60
or an error message depending on that value.

44
00:01:50,60 --> 00:01:52,50
Of course you don't have to do it that way.

45
00:01:52,50 --> 00:01:55,30
Any way you can come up with to do this is good.

46
00:01:55,30 --> 00:01:56,40
But just make sure that you follow

47
00:01:56,40 --> 00:01:59,00
the core tenants of functional programming.

48
00:01:59,00 --> 00:02:00,80
For example, you're not allowed to use any kind

49
00:02:00,80 --> 00:02:03,00
of mutation in here at all.

50
00:02:03,00 --> 00:02:04,40
Well, once you think you've figured it out,

51
00:02:04,40 --> 00:02:05,90
feel free to go on to the next video

52
00:02:05,90 --> 00:02:07,00
and see how I did it.

