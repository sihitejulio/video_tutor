1
00:00:01,00 --> 00:00:05,60
(upbeat music)

2
00:00:05,60 --> 00:00:08,00
- So I'm going to show you how I solve this now.

3
00:00:08,00 --> 00:00:10,30
First of all the tricky part here is that we want this

4
00:00:10,30 --> 00:00:13,20
function to be independent of the names that are actually

5
00:00:13,20 --> 00:00:15,10
in the array.

6
00:00:15,10 --> 00:00:18,70
As I mentioned before, we're not allowed to hard code

7
00:00:18,70 --> 00:00:21,90
any of these names into the function.

8
00:00:21,90 --> 00:00:24,70
So what I'm going to do for this function is use reduce.

9
00:00:24,70 --> 00:00:29,30
So we're going to say return votes.reduce and remember

10
00:00:29,30 --> 00:00:32,20
that reduce takes two arguments the accumulator,

11
00:00:32,20 --> 00:00:34,70
which will be the votes that we've tallied so far

12
00:00:34,70 --> 00:00:37,20
and the current element, which will be the name

13
00:00:37,20 --> 00:00:39,50
of a certain candidate.

14
00:00:39,50 --> 00:00:42,00
And now before we write out our function body we have

15
00:00:42,00 --> 00:00:44,10
to think about what we want the starting value to be

16
00:00:44,10 --> 00:00:45,80
for our reduce function.

17
00:00:45,80 --> 00:00:47,90
What do we want it to start out with?

18
00:00:47,90 --> 00:00:51,70
Well since our expected output is an object, it would

19
00:00:51,70 --> 00:00:53,70
be a pretty good guess to say that we want our starting

20
00:00:53,70 --> 00:00:55,80
value to be an object.

21
00:00:55,80 --> 00:00:58,50
And in my case that's exactly what I'm going to do.

22
00:00:58,50 --> 00:01:00,40
So now let's write the body of the function that we're

23
00:01:00,40 --> 00:01:02,00
passing to reduce.

24
00:01:02,00 --> 00:01:04,70
Again since we want the final turn value of this function

25
00:01:04,70 --> 00:01:07,40
to be an object, the thing that we're returning from this

26
00:01:07,40 --> 00:01:11,10
function here is also going to be an object.

27
00:01:11,10 --> 00:01:13,50
And since we're using ero syntax here, we're going to have

28
00:01:13,50 --> 00:01:16,40
to remember to wrap it in parentheses and then the brackets

29
00:01:16,40 --> 00:01:18,20
of our object.

30
00:01:18,20 --> 00:01:23,20
Now at any given point, this accumulator argument

31
00:01:23,20 --> 00:01:27,40
is going to be some earlier stage of our final result.

32
00:01:27,40 --> 00:01:31,00
So when our reduce function first starts out,

33
00:01:31,00 --> 00:01:33,70
the accumulator is going to be an empty object

34
00:01:33,70 --> 00:01:37,60
and the name is going to be the first element in our array.

35
00:01:37,60 --> 00:01:41,00
And then when reduce gets to the second value of our array,

36
00:01:41,00 --> 00:01:43,30
the accumulator is going to be an object that looks

37
00:01:43,30 --> 00:01:46,40
like this.

38
00:01:46,40 --> 00:01:48,70
And our name argument is going to be the second value

39
00:01:48,70 --> 00:01:51,00
in our array.

40
00:01:51,00 --> 00:01:55,30
And so on and so forth until we obtain our final result.

41
00:01:55,30 --> 00:01:58,20
So what we want to do inside this object here

42
00:01:58,20 --> 00:02:01,50
is use the spread operator and spread our accumulator

43
00:02:01,50 --> 00:02:04,60
variable this will put all the names and counts that we

44
00:02:04,60 --> 00:02:08,50
have so far into this object that we're returning.

45
00:02:08,50 --> 00:02:11,70
And then underneath that, we want to add another property

46
00:02:11,70 --> 00:02:12,90
to our object.

47
00:02:12,90 --> 00:02:15,80
Now this property is simply going to be whatever name

48
00:02:15,80 --> 00:02:18,30
that we're currently on in the array.

49
00:02:18,30 --> 00:02:20,40
And what we want to do, let's say that this is the object

50
00:02:20,40 --> 00:02:23,90
we have so far and we get to this same name again

51
00:02:23,90 --> 00:02:26,40
is we want to increment this number.

52
00:02:26,40 --> 00:02:29,70
So what we're going to do is say accumulator and we want

53
00:02:29,70 --> 00:02:33,10
to get this property from our accumulator, so we'll say

54
00:02:33,10 --> 00:02:38,70
name and then we're going to increment it by one like this.

55
00:02:38,70 --> 00:02:43,30
If we run our code now, we can see that the result

56
00:02:43,30 --> 00:02:46,30
that we get is not exactly what we want.

57
00:02:46,30 --> 00:02:48,50
Our object successfully contains all the right people's

58
00:02:48,50 --> 00:02:52,30
names, but the values for these names where the account

59
00:02:52,30 --> 00:02:55,00
is supposed to be is not a number.

60
00:02:55,00 --> 00:02:57,30
Now I'll tell you why this is and it's because of this

61
00:02:57,30 --> 00:02:58,70
line here.

62
00:02:58,70 --> 00:03:01,40
The problem that we're running into is if this acc

63
00:03:01,40 --> 00:03:05,00
variable if our tally so far doesn't contain a person's

64
00:03:05,00 --> 00:03:08,10
name this here is going to return undefined.

65
00:03:08,10 --> 00:03:11,30
So we'll be doing undefined plus one, which is not a number.

66
00:03:11,30 --> 00:03:15,20
And then it will simply propagate like that as we go on.

67
00:03:15,20 --> 00:03:18,30
So what we need to do is define what happens the first

68
00:03:18,30 --> 00:03:20,20
time we see a name.

69
00:03:20,20 --> 00:03:21,90
And the way we're going to do that is by checking

70
00:03:21,90 --> 00:03:24,90
if this accumulator argument already has a value

71
00:03:24,90 --> 00:03:26,70
for this name.

72
00:03:26,70 --> 00:03:28,00
And that'll look like this.

73
00:03:28,00 --> 00:03:32,80
Accumulator, name, and then we'll use the ternary operator,

74
00:03:32,80 --> 00:03:35,40
so we'll just use a question mark.

75
00:03:35,40 --> 00:03:37,90
And so far we're saying that if this accumulator variable

76
00:03:37,90 --> 00:03:40,80
has this name already we're going to return whatever

77
00:03:40,80 --> 00:03:43,90
the number is associated with that name plus one.

78
00:03:43,90 --> 00:03:47,00
So we just need to define what happens if that name

79
00:03:47,00 --> 00:03:48,80
doesn't exist.

80
00:03:48,80 --> 00:03:50,90
Well since this means that this is the first time we've

81
00:03:50,90 --> 00:03:53,60
seen this name, we're going to want the starting value

82
00:03:53,60 --> 00:03:55,10
to just be one like that.

83
00:03:55,10 --> 00:03:59,20
And that should do it for our tally votes function.

84
00:03:59,20 --> 00:04:03,80
If we run our code again, we see that we get the right

85
00:04:03,80 --> 00:04:05,30
vote counts for everything.

86
00:04:05,30 --> 00:04:07,10
You can go back to the array and actually count them

87
00:04:07,10 --> 00:04:10,60
if you want, but just trust me that this is correct.

88
00:04:10,60 --> 00:04:13,40
And that's all there is to it, this is just how I solved it

89
00:04:13,40 --> 00:04:15,60
you might've done something different.

90
00:04:15,60 --> 00:04:18,30
Keep in mind also that since the body of our function

91
00:04:18,30 --> 00:04:22,90
is only one line, we can remove these brackets here

92
00:04:22,90 --> 00:04:28,00
and delete the return keyword and that's all there is to it.

