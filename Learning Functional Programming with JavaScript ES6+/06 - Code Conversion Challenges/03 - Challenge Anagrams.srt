1
00:00:05,60 --> 00:00:07,30
- [Instructor] The next functional programming challenge

2
00:00:07,30 --> 00:00:09,00
we're going to tackle in this course,

3
00:00:09,00 --> 00:00:12,10
is the problem of finding anagrams for a given word.

4
00:00:12,10 --> 00:00:14,30
Now if you don't know what anagrams are,

5
00:00:14,30 --> 00:00:17,00
they're two or more words that have the exact same letters

6
00:00:17,00 --> 00:00:18,50
and number of letters.

7
00:00:18,50 --> 00:00:21,70
For example, iceman, cinema, and anemic.

8
00:00:21,70 --> 00:00:26,50
Each of these has one a, one n, one e, one m, one I,

9
00:00:26,50 --> 00:00:28,00
and one c.

10
00:00:28,00 --> 00:00:30,60
So your challenge is to write the body of this function

11
00:00:30,60 --> 00:00:33,00
that takes a word as an argument,

12
00:00:33,00 --> 00:00:35,10
and finds all the anagrams for that words

13
00:00:35,10 --> 00:00:36,90
in the second argument here.

14
00:00:36,90 --> 00:00:38,30
This second argument will be an array

15
00:00:38,30 --> 00:00:40,80
of all the words in our dictionary.

16
00:00:40,80 --> 00:00:42,20
Now before you start this challenge,

17
00:00:42,20 --> 00:00:45,10
note that we're importing something up top here.

18
00:00:45,10 --> 00:00:47,60
This is an npm package that exports an array

19
00:00:47,60 --> 00:00:50,00
of all the words in the English language.

20
00:00:50,00 --> 00:00:50,80
So in other words,

21
00:00:50,80 --> 00:00:52,80
it's a really big array of strings.

22
00:00:52,80 --> 00:00:53,90
And before we can do anything,

23
00:00:53,90 --> 00:00:55,80
we have to install this package.

24
00:00:55,80 --> 00:00:58,50
So just open up a terminal inside your directory,

25
00:00:58,50 --> 00:01:02,90
and we're going to type npm, install, dash dash save,

26
00:01:02,90 --> 00:01:11,00
and then an dash array dash of dash English dash words,

27
00:01:11,00 --> 00:01:13,20
and then hit enter.

28
00:01:13,20 --> 00:01:16,10
And once that installs, you're free to start working.

29
00:01:16,10 --> 00:01:18,00
And I've given you a little hint here.

30
00:01:18,00 --> 00:01:19,10
Since the word we're passing in

31
00:01:19,10 --> 00:01:20,50
is going to be in the form of a string

32
00:01:20,50 --> 00:01:22,80
instead of directly in the form of an array,

33
00:01:22,80 --> 00:01:25,20
we want to get just an array of the letters in the word,

34
00:01:25,20 --> 00:01:27,80
and the way we do that is by doing this,

35
00:01:27,80 --> 00:01:32,00
by calling word.split with an empty string.

36
00:01:32,00 --> 00:01:34,60
That's just a little hint to get you started off.

37
00:01:34,60 --> 00:01:36,00
Well, when you think you've figured it out,

38
00:01:36,00 --> 00:01:37,50
feel free to go onto the next video

39
00:01:37,50 --> 00:01:40,00
and I'll show you how I've solved this.

40
00:01:40,00 --> 00:01:41,70
And keep in mind that the functional rules

41
00:01:41,70 --> 00:01:43,40
are still in effect here.

42
00:01:43,40 --> 00:01:46,00
You're not allowed to use any kind of mutation,

43
00:01:46,00 --> 00:01:48,90
and you're also not allowed to use regex,

44
00:01:48,90 --> 00:01:52,10
which could theoretically be used to solve this as well.

45
00:01:52,10 --> 00:01:53,60
And that's all you need to know.

46
00:01:53,60 --> 00:01:56,00
You're free to get started whenever you want.

