1
00:00:00,10 --> 00:00:05,60
(upbeat music)

2
00:00:05,60 --> 00:00:07,10
- [Instructor] So I'm going to show you how I solved

3
00:00:07,10 --> 00:00:09,30
this find anagrams challenge.

4
00:00:09,30 --> 00:00:12,00
The way that I thought about it is this.

5
00:00:12,00 --> 00:00:14,60
When we have two words that are anagrams,

6
00:00:14,60 --> 00:00:16,50
that means that they have the exact same count

7
00:00:16,50 --> 00:00:20,70
of the exact same letters, but in different orders.

8
00:00:20,70 --> 00:00:22,50
So if order doesn't matter,

9
00:00:22,50 --> 00:00:23,60
then it might make sense for us

10
00:00:23,60 --> 00:00:26,20
to express our words like this.

11
00:00:26,20 --> 00:00:29,00
As an object that contains each of the letters

12
00:00:29,00 --> 00:00:30,90
that the word contains,

13
00:00:30,90 --> 00:00:34,70
with a count of how many letters it has.

14
00:00:34,70 --> 00:00:36,40
So for the word cinema for example,

15
00:00:36,40 --> 00:00:37,90
it would look like this

16
00:00:37,90 --> 00:00:41,70
we have one c, one i,

17
00:00:41,70 --> 00:00:46,00
one n, one e,

18
00:00:46,00 --> 00:00:51,40
one m and one a.

19
00:00:51,40 --> 00:00:53,70
We could then do the same with all of the other words

20
00:00:53,70 --> 00:00:56,60
and check to see if they have the same sort of signature,

21
00:00:56,60 --> 00:00:59,50
the same count of each of their letters.

22
00:00:59,50 --> 00:01:02,00
So for instance if we formed this same kind of object

23
00:01:02,00 --> 00:01:04,90
with the other words, iceman and anemic

24
00:01:04,90 --> 00:01:08,30
which are anagrams of cinema

25
00:01:08,30 --> 00:01:09,40
we would get two objects

26
00:01:09,40 --> 00:01:12,20
where all of the properties would be equal.

27
00:01:12,20 --> 00:01:14,10
Now those of you who completed the last challenge

28
00:01:14,10 --> 00:01:16,90
might notice that this looks a little familiar.

29
00:01:16,90 --> 00:01:18,70
This is the exact same functionality

30
00:01:18,70 --> 00:01:21,80
that counted the number of occurrences of a given string

31
00:01:21,80 --> 00:01:25,00
in an array.

32
00:01:25,00 --> 00:01:28,20
So the first thing we're going to do is copy and paste

33
00:01:28,20 --> 00:01:29,60
our tally votes function

34
00:01:29,60 --> 00:01:31,90
that we wrote in the previous function

35
00:01:31,90 --> 00:01:35,30
and in order to make this less specific,

36
00:01:35,30 --> 00:01:39,60
let's rename it to count occurrences

37
00:01:39,60 --> 00:01:45,30
and then we'll rename the argument to array, just arr

38
00:01:45,30 --> 00:01:47,60
and we'll change that here as well

39
00:01:47,60 --> 00:01:50,30
and since we're no longer dealing with names,

40
00:01:50,30 --> 00:01:54,50
we'll say string, str

41
00:01:54,50 --> 00:02:00,50
str str and str.

42
00:02:00,50 --> 00:02:03,30
And now that we have this count occurrences function

43
00:02:03,30 --> 00:02:06,00
that we can use to convert an array of letters

44
00:02:06,00 --> 00:02:07,90
into an object that contains the count

45
00:02:07,90 --> 00:02:09,90
of each of those letters,

46
00:02:09,90 --> 00:02:12,30
let's create another helper function.

47
00:02:12,30 --> 00:02:16,30
We'll call this has same letter count.

48
00:02:16,30 --> 00:02:18,20
And what this function is going to do

49
00:02:18,20 --> 00:02:21,50
is compare two words to see if they have the same number

50
00:02:21,50 --> 00:02:23,60
of the same letters.

51
00:02:23,60 --> 00:02:28,50
So we'll say word one and word two for the arguments

52
00:02:28,50 --> 00:02:29,80
and inside this function body

53
00:02:29,80 --> 00:02:31,80
we're going to define two new constants,

54
00:02:31,80 --> 00:02:36,60
we'll say const word one count

55
00:02:36,60 --> 00:02:41,40
equals count occurrences word one

56
00:02:41,40 --> 00:02:44,00
and then since count occurrences expects an array

57
00:02:44,00 --> 00:02:47,50
of letters instead of just a string,

58
00:02:47,50 --> 00:02:52,30
we're going to say word one dot split empty string,

59
00:02:52,30 --> 00:02:55,40
as I showed you down here

60
00:02:55,40 --> 00:02:57,80
and then we're going to do the same thing for word two.

61
00:02:57,80 --> 00:03:01,90
We're going to say const word two count

62
00:03:01,90 --> 00:03:09,20
equals count occurrences word two dot split

63
00:03:09,20 --> 00:03:11,50
empty string

64
00:03:11,50 --> 00:03:14,40
and then our return value is going to look like this.

65
00:03:14,40 --> 00:03:19,30
We're going to do return and we want to do object dot keys

66
00:03:19,30 --> 00:03:22,80
which will give us all the letters in our word one count

67
00:03:22,80 --> 00:03:26,50
word one count and we're going to do dot every

68
00:03:26,50 --> 00:03:29,80
and now for each letter in word one count

69
00:03:29,80 --> 00:03:31,60
we're going to check to see if the associated number

70
00:03:31,60 --> 00:03:34,60
in word one count, which we get like this

71
00:03:34,60 --> 00:03:41,90
is equal to that number in word two count like this.

72
00:03:41,90 --> 00:03:43,60
Now this is still missing something

73
00:03:43,60 --> 00:03:45,50
and to see what it's missing

74
00:03:45,50 --> 00:03:49,20
let's finish our find anagrams function first.

75
00:03:49,20 --> 00:03:50,40
So we don't need this anymore,

76
00:03:50,40 --> 00:03:53,30
I just gave that to you as a hint

77
00:03:53,30 --> 00:03:55,50
and what we're going to do in find anagrams,

78
00:03:55,50 --> 00:03:58,80
we're going to say return all words,

79
00:03:58,80 --> 00:04:00,40
which is our dictionary

80
00:04:00,40 --> 00:04:05,20
dot filter and then for each entry in our dictionary

81
00:04:05,20 --> 00:04:07,50
we're going to check if it has the same letter count

82
00:04:07,50 --> 00:04:10,40
as our word argument here

83
00:04:10,40 --> 00:04:11,30
and that'll look like this.

84
00:04:11,30 --> 00:04:16,20
We'll say has same letter count

85
00:04:16,20 --> 00:04:21,40
word and entry.

86
00:04:21,40 --> 00:04:22,60
Now let's save our code and run it

87
00:04:22,60 --> 00:04:25,40
to see what we have so far

88
00:04:25,40 --> 00:04:26,50
and this is going to take a while,

89
00:04:26,50 --> 00:04:30,30
because our words dictionary contains a lot of words

90
00:04:30,30 --> 00:04:32,10
and once that finishes we'll see that it prints out

91
00:04:32,10 --> 00:04:34,40
an array of things that definitely aren't anagrams

92
00:04:34,40 --> 00:04:36,90
for cinema, but if we look a little closer,

93
00:04:36,90 --> 00:04:40,20
we can see that these words contain all of the same letters

94
00:04:40,20 --> 00:04:42,60
as cinema in the same amounts,

95
00:04:42,60 --> 00:04:48,10
for example they have one c, one i, one n, one e,

96
00:04:48,10 --> 00:04:51,20
one m and one a, but they also have a lot

97
00:04:51,20 --> 00:04:54,30
of other letters as well.

98
00:04:54,30 --> 00:04:57,80
So to fix this so that it only finds true anagrams

99
00:04:57,80 --> 00:04:59,20
we just have to add another condition

100
00:04:59,20 --> 00:05:01,30
to our has same letter count

101
00:05:01,30 --> 00:05:03,90
and for this condition we basically want to check

102
00:05:03,90 --> 00:05:06,60
that word one count and word two count

103
00:05:06,60 --> 00:05:09,00
have the same number of letters in them

104
00:05:09,00 --> 00:05:10,70
and this means that word two count

105
00:05:10,70 --> 00:05:11,90
doesn't have any letters in it

106
00:05:11,90 --> 00:05:13,90
that word one count doesn't have

107
00:05:13,90 --> 00:05:17,10
which is the problem that we were dealing with down here.

108
00:05:17,10 --> 00:05:18,20
So to write that condition

109
00:05:18,20 --> 00:05:22,70
all we have to write is object dot keys

110
00:05:22,70 --> 00:05:26,00
word one count

111
00:05:26,00 --> 00:05:32,40
dot length is equal to object dot keys

112
00:05:32,40 --> 00:05:38,30
word two count dot length

113
00:05:38,30 --> 00:05:40,40
and then we want to add an and in between these

114
00:05:40,40 --> 00:05:44,50
to make sure they're both true

115
00:05:44,50 --> 00:05:47,90
and now if we run our program again,

116
00:05:47,90 --> 00:05:51,20
we see that it correctly gives us all the anagrams

117
00:05:51,20 --> 00:05:53,60
so the only thing that we want to fix now

118
00:05:53,60 --> 00:05:55,60
is we want to make it so that find anagrams

119
00:05:55,60 --> 00:05:58,30
isn't returning the original word

120
00:05:58,30 --> 00:05:59,20
and it's doing that now

121
00:05:59,20 --> 00:06:01,30
because obviously the word that we pass in

122
00:06:01,30 --> 00:06:05,40
has the same number of letters as the word that we pass in.

123
00:06:05,40 --> 00:06:07,10
So in order to get rid of that

124
00:06:07,10 --> 00:06:10,10
all we have to do is on the end of this statement

125
00:06:10,10 --> 00:06:12,70
in our find anagrams function

126
00:06:12,70 --> 00:06:14,60
we're going to write dot

127
00:06:14,60 --> 00:06:16,80
then we're going to say filter

128
00:06:16,80 --> 00:06:19,70
and the anagrams that we return

129
00:06:19,70 --> 00:06:21,40
we want to make sure that it's not equal to the word

130
00:06:21,40 --> 00:06:22,50
that we pass in

131
00:06:22,50 --> 00:06:29,00
so we'll just say anagram does not equal word.

132
00:06:29,00 --> 00:06:30,40
And we'll put some spacing between that

133
00:06:30,40 --> 00:06:36,50
to make it more readable.

134
00:06:36,50 --> 00:06:40,70
And now if we run our code one last time

135
00:06:40,70 --> 00:06:42,80
we see that it gives us our desired output

136
00:06:42,80 --> 00:06:44,10
even though it's not in the same order,

137
00:06:44,10 --> 00:06:45,80
which isn't a big deal

138
00:06:45,80 --> 00:06:47,90
and that's it, that's just how I solved it.

139
00:06:47,90 --> 00:06:50,00
You might have come up with a different way.

