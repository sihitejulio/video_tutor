1
00:00:00,00 --> 00:00:05,60
(upbeat techno music)

2
00:00:05,60 --> 00:00:06,90
- [Instructor] To close out this course,

3
00:00:06,90 --> 00:00:08,90
we're going to do a few challenges that require you

4
00:00:08,90 --> 00:00:12,10
to use functional concepts to solve problems.

5
00:00:12,10 --> 00:00:14,30
And our first challenge is this.

6
00:00:14,30 --> 00:00:17,90
Let's say that an election has happened in our home town,

7
00:00:17,90 --> 00:00:19,60
and we have an array of the votes,

8
00:00:19,60 --> 00:00:22,20
which are just strings with the person's name.

9
00:00:22,20 --> 00:00:24,40
Now first of all please note that none of these names

10
00:00:24,40 --> 00:00:25,90
is in any way meant as a references

11
00:00:25,90 --> 00:00:30,00
to any candidate who's currently running in any election.

12
00:00:30,00 --> 00:00:32,00
So what we want to do is write a function

13
00:00:32,00 --> 00:00:34,80
that takes this array of election votes,

14
00:00:34,80 --> 00:00:36,80
and produces an object like this,

15
00:00:36,80 --> 00:00:38,40
with the candidates' names,

16
00:00:38,40 --> 00:00:42,30
and the number of votes that they had in the array.

17
00:00:42,30 --> 00:00:44,00
Now this might not seem too hard,

18
00:00:44,00 --> 00:00:46,00
but there are a few catches.

19
00:00:46,00 --> 00:00:47,30
The first catch is that you're not allowed

20
00:00:47,30 --> 00:00:50,60
to hard code any of these names into your function.

21
00:00:50,60 --> 00:00:52,00
For all practical purposes,

22
00:00:52,00 --> 00:00:53,20
assume that you don't even know

23
00:00:53,20 --> 00:00:56,00
what names are in this election votes array.

24
00:00:56,00 --> 00:00:56,90
We want to write a function

25
00:00:56,90 --> 00:00:59,80
that in theory could work with any array like this.

26
00:00:59,80 --> 00:01:01,20
The second catch is that

27
00:01:01,20 --> 00:01:03,50
since this is a functional programming course,

28
00:01:03,50 --> 00:01:04,40
you're not allowed to use

29
00:01:04,40 --> 00:01:07,00
any kind of mutation in this function.

30
00:01:07,00 --> 00:01:09,00
So when you think you've got a solution,

31
00:01:09,00 --> 00:01:10,40
feel free to go on to the next video,

32
00:01:10,40 --> 00:01:12,00
where I'll show you how I solved it.

