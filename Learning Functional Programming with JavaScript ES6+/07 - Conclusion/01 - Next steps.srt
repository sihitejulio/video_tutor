1
00:00:00,50 --> 00:00:01,40
- [Instructor] So we've covered quite

2
00:00:01,40 --> 00:00:03,00
a bit of ground in this course.

3
00:00:03,00 --> 00:00:05,00
If you're wondering where to go from here,

4
00:00:05,00 --> 00:00:06,40
a great next step would be to take

5
00:00:06,40 --> 00:00:07,90
whatever programs you've already written

6
00:00:07,90 --> 00:00:10,00
in JavaScript, and try converting them

7
00:00:10,00 --> 00:00:12,80
to make better use of functional concepts.

8
00:00:12,80 --> 00:00:14,60
There's also a book that I'd highly recommend

9
00:00:14,60 --> 00:00:16,70
for those of you looking for an extremely in-depth

10
00:00:16,70 --> 00:00:18,90
look at functional programming.

11
00:00:18,90 --> 00:00:20,90
This book is called Structure and Interpretation

12
00:00:20,90 --> 00:00:22,90
of Computer Programs, and it's available

13
00:00:22,90 --> 00:00:26,80
for free download on MIT's OpenCourseWare website.

14
00:00:26,80 --> 00:00:29,00
Despite its rather dry-sounding title,

15
00:00:29,00 --> 00:00:30,90
this book is a fantastic guide to thinking

16
00:00:30,90 --> 00:00:33,80
about programming in a very flexible way.

17
00:00:33,80 --> 00:00:35,10
And finally, if you want to learn more

18
00:00:35,10 --> 00:00:37,80
about either functional programming or JavaScript,

19
00:00:37,80 --> 00:00:41,10
our library is full of great courses on both subjects.

20
00:00:41,10 --> 00:00:42,50
If you have any interest in seeing how

21
00:00:42,50 --> 00:00:43,90
the concepts we have talked about here

22
00:00:43,90 --> 00:00:46,30
are expressed in Swift, check out my course

23
00:00:46,30 --> 00:00:49,30
about Learning Functional Programming with Swift.

24
00:00:49,30 --> 00:00:50,50
And finally, if you want to see how

25
00:00:50,50 --> 00:00:53,50
to use JavaScript ES6 to write a full-stack web application

26
00:00:53,50 --> 00:00:56,40
using React and Node.js, check out my course

27
00:00:56,40 --> 00:01:00,10
about Creating and Hosting a Full-Stack React Site.

28
00:01:00,10 --> 00:01:01,70
And also, please feel free to connect

29
00:01:01,70 --> 00:01:03,00
with me on LinkedIn where I post

30
00:01:03,00 --> 00:01:06,80
about functional programming and other development topics.

31
00:01:06,80 --> 00:01:07,90
Thanks for watching and I hope this course

32
00:01:07,90 --> 00:01:10,00
has really helped you.

