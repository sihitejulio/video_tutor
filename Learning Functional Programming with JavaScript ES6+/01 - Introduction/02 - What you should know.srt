1
00:00:00,50 --> 00:00:02,40
- [Instructor] To get the most out of this course,

2
00:00:02,40 --> 00:00:04,00
there are few prerequisites that it would

3
00:00:04,00 --> 00:00:06,10
be helpful for you to know.

4
00:00:06,10 --> 00:00:08,30
The first is a basic knowledge of JavaScript,

5
00:00:08,30 --> 00:00:11,60
and ideally some experience with ES6 syntax.

6
00:00:11,60 --> 00:00:14,20
If you're not already comfortable with ES6 syntax,

7
00:00:14,20 --> 00:00:15,90
it's not hard to learn.

8
00:00:15,90 --> 00:00:17,90
If you're interested, I'd recommend checking out one

9
00:00:17,90 --> 00:00:21,00
of the ES6 courses in our library.

10
00:00:21,00 --> 00:00:23,00
It would also be helpful for you to have some experience

11
00:00:23,00 --> 00:00:25,60
with basic command line operations.

12
00:00:25,60 --> 00:00:26,90
All we'll be using for this course

13
00:00:26,90 --> 00:00:30,00
is basic commands like mkdir to make directories,

14
00:00:30,00 --> 00:00:32,40
cd to change directories, and ls to see

15
00:00:32,40 --> 00:00:34,30
the contents of a directory, as well

16
00:00:34,30 --> 00:00:37,30
as a few commands specific to Node.js.

17
00:00:37,30 --> 00:00:39,30
Keep in mind also that if you're on Windows,

18
00:00:39,30 --> 00:00:41,40
the commands might be a little different.

19
00:00:41,40 --> 00:00:43,50
There are articles out there like this one

20
00:00:43,50 --> 00:00:45,40
that show many of the basic Linux commands

21
00:00:45,40 --> 00:00:47,80
and their Windows equivalents.

22
00:00:47,80 --> 00:00:49,00
And finally, since I'll be using

23
00:00:49,00 --> 00:00:50,20
it as a reference point throughout

24
00:00:50,20 --> 00:00:52,00
this course, a basic knowledge

25
00:00:52,00 --> 00:00:53,40
of object-oriented programming

26
00:00:53,40 --> 00:00:55,70
concepts would be helpful to have as well.

27
00:00:55,70 --> 00:00:56,80
And even if you're not familiar

28
00:00:56,80 --> 00:00:58,40
with object-oriented programming,

29
00:00:58,40 --> 00:00:59,40
it's not a big deal.

30
00:00:59,40 --> 00:01:01,30
You should still be able to follow along comfortably

31
00:01:01,30 --> 00:01:03,00
with the examples.

32
00:01:03,00 --> 00:01:04,90
While I highly-recommend that you follow along

33
00:01:04,90 --> 00:01:07,50
with me as I write the code, I've also included

34
00:01:07,50 --> 00:01:09,00
the finished state for all the code

35
00:01:09,00 --> 00:01:11,40
that I write in this course in the exercise files

36
00:01:11,40 --> 00:01:13,60
for your reference.

37
00:01:13,60 --> 00:01:16,40
So basically, this course is for low-intermediate

38
00:01:16,40 --> 00:01:18,10
to professional developers who have

39
00:01:18,10 --> 00:01:20,20
a desire to improve their code by learning

40
00:01:20,20 --> 00:01:22,30
and applying functional concepts.

41
00:01:22,30 --> 00:01:24,10
If you fit that description, then this course

42
00:01:24,10 --> 00:01:25,00
is definitely for you.

