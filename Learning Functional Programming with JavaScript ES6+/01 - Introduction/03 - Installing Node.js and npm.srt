1
00:00:00,50 --> 00:00:02,10
- [Instructor] Let's install the tools necessary

2
00:00:02,10 --> 00:00:03,70
for this course.

3
00:00:03,70 --> 00:00:06,40
Node.js will be really important for running your code

4
00:00:06,40 --> 00:00:07,70
and installing a few libraries

5
00:00:07,70 --> 00:00:09,90
we'll be using to make our programming easy.

6
00:00:09,90 --> 00:00:12,20
If you're not already familiar with Node.js,

7
00:00:12,20 --> 00:00:13,50
all you need to know for this course

8
00:00:13,50 --> 00:00:16,40
is that Node.js is an incredibly popular platform

9
00:00:16,40 --> 00:00:18,20
that allows you to run JavaScript code

10
00:00:18,20 --> 00:00:20,10
outside of the browser.

11
00:00:20,10 --> 00:00:21,50
This is really helpful since it means

12
00:00:21,50 --> 00:00:23,40
your JavaScript code doesn't have to be loaded

13
00:00:23,40 --> 00:00:25,70
and executed as part of a webpage.

14
00:00:25,70 --> 00:00:28,10
In other words, you could write a single JavaScript file

15
00:00:28,10 --> 00:00:30,10
and run it from the command line.

16
00:00:30,10 --> 00:00:33,00
If you don't already have Node.js installed,

17
00:00:33,00 --> 00:00:37,40
you can download it from Node.js's website nodejs.org.

18
00:00:37,40 --> 00:00:41,50
We're going to click on this button here and download it

19
00:00:41,50 --> 00:00:44,50
and once it downloads we're going to click on it to open it.

20
00:00:44,50 --> 00:00:45,80
And this should take you through the steps

21
00:00:45,80 --> 00:00:47,90
required for installation.

22
00:00:47,90 --> 00:00:50,80
Note that if you're using Windows or a Unix distribution,

23
00:00:50,80 --> 00:00:52,40
the steps might be a little different.

24
00:00:52,40 --> 00:00:55,40
But Node.js has instructions for all of these as well.

25
00:00:55,40 --> 00:00:57,50
And once you have Node.js installed,

26
00:00:57,50 --> 00:00:59,20
you'll want to make sure that you have the most recent

27
00:00:59,20 --> 00:01:01,50
version of NPM installed.

28
00:01:01,50 --> 00:01:03,40
We'll be using a few libraries in this course

29
00:01:03,40 --> 00:01:06,10
and NPM is a package manager for Node.js

30
00:01:06,10 --> 00:01:08,50
that allows us to easily install them.

31
00:01:08,50 --> 00:01:11,60
To install the most recent version of NPM,

32
00:01:11,60 --> 00:01:17,80
just open your terminal and type npm install -g

33
00:01:17,80 --> 00:01:28,20
npm@latest and you might need to run this with sudo as well.

34
00:01:28,20 --> 00:01:31,50
After that, type npm -v and it should

35
00:01:31,50 --> 00:01:33,40
show the latest version.

36
00:01:33,40 --> 00:01:35,30
If the NPM and Node versions on your computer

37
00:01:35,30 --> 00:01:37,20
are higher than mine, don't worry about it.

38
00:01:37,20 --> 00:01:39,40
Everything should still run correctly.

39
00:01:39,40 --> 00:01:41,00
But I do highly recommend having at least

40
00:01:41,00 --> 00:01:43,20
these version numbers here.

41
00:01:43,20 --> 00:01:44,60
And that's all there is to it.

42
00:01:44,60 --> 00:01:48,00
Node.js and NPM are now installed on your computer.

