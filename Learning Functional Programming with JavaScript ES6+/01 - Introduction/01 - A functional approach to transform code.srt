1
00:00:00,50 --> 00:00:02,10
- [Shaun] An increasing number of developers

2
00:00:02,10 --> 00:00:04,10
are switching from object-oriented programming

3
00:00:04,10 --> 00:00:06,70
to functional programming as a way to minimize

4
00:00:06,70 --> 00:00:08,60
the potential for bugs in their code

5
00:00:08,60 --> 00:00:12,10
while maximizing its readability and reusability.

6
00:00:12,10 --> 00:00:14,40
Functional programming is an increasingly popular

7
00:00:14,40 --> 00:00:17,20
coding paradigm that's been championed by languages

8
00:00:17,20 --> 00:00:18,80
like Lisp and Haskell,

9
00:00:18,80 --> 00:00:22,20
and its features are natively supported in JavaScript.

10
00:00:22,20 --> 00:00:24,60
Writing code using functional programming concepts

11
00:00:24,60 --> 00:00:26,70
is the single-most important approach you can take

12
00:00:26,70 --> 00:00:30,40
to ensure your applications are maintainable and scalable.

13
00:00:30,40 --> 00:00:31,50
In this course, I'll show you

14
00:00:31,50 --> 00:00:34,10
how to program in the functional paradigm.

15
00:00:34,10 --> 00:00:35,70
We'll start off with the basic concepts

16
00:00:35,70 --> 00:00:36,70
of functional programming

17
00:00:36,70 --> 00:00:39,40
and how it compares to object-oriented programming,

18
00:00:39,40 --> 00:00:41,10
and then move on to first class functions

19
00:00:41,10 --> 00:00:43,50
and how to work with them in JavaScript.

20
00:00:43,50 --> 00:00:45,70
We'll then move on to seeing how functional programming

21
00:00:45,70 --> 00:00:48,60
makes working with data structures very straightforward,

22
00:00:48,60 --> 00:00:50,40
followed by some more advanced concepts

23
00:00:50,40 --> 00:00:53,40
such as recursion and partial application.

24
00:00:53,40 --> 00:00:55,40
Finally, we'll close out with a few interesting challenges

25
00:00:55,40 --> 00:00:57,70
that will help you improve your functional knowledge

26
00:00:57,70 --> 00:00:59,30
even further.

27
00:00:59,30 --> 00:01:00,90
Hi, I'm Shaun Wassell,

28
00:01:00,90 --> 00:01:03,80
and I'm a Senior JavaScript and React Developer.

29
00:01:03,80 --> 00:01:05,70
Join me in my LinkedIn learning course

30
00:01:05,70 --> 00:01:08,80
to learn functional programming with JavaScript ES6

31
00:01:08,80 --> 00:01:12,00
to see how you can take your code to a whole new level.

