1
00:00:00,60 --> 00:00:02,70
- [Instructor] Let's create a very simple file.

2
00:00:02,70 --> 00:00:07,70
We'll call it hello-world.js,

3
00:00:07,70 --> 00:00:09,10
and inside it we'll type some code

4
00:00:09,10 --> 00:00:11,90
with very basic ES6 syntax.

5
00:00:11,90 --> 00:00:16,30
We'll say const sayHello,

6
00:00:16,30 --> 00:00:22,10
and that'll be a function that takes our name,

7
00:00:22,10 --> 00:00:24,50
and with backticks

8
00:00:24,50 --> 00:00:29,20
says hello name.

9
00:00:29,20 --> 00:00:30,80
After we've typed this out,

10
00:00:30,80 --> 00:00:35,80
let's just call our function with our own name.

11
00:00:35,80 --> 00:00:37,20
Now, to run our code,

12
00:00:37,20 --> 00:00:38,80
make sure you're in the right directory

13
00:00:38,80 --> 00:00:43,70
and type npx babel-node,

14
00:00:43,70 --> 00:00:49,60
and then hello-world.js,

15
00:00:49,60 --> 00:00:52,30
and hit Enter, and we should see the result.

16
00:00:52,30 --> 00:00:53,70
That's all there is to it.

17
00:00:53,70 --> 00:00:56,50
That's how we'll be running all the code in this course.

18
00:00:56,50 --> 00:00:57,50
It's also worth noting

19
00:00:57,50 --> 00:00:59,20
that if you want to play around with this code

20
00:00:59,20 --> 00:01:01,30
without writing it in a file first,

21
00:01:01,30 --> 00:01:05,20
you can also just type npx babel-node

22
00:01:05,20 --> 00:01:06,10
without a file name,

23
00:01:06,10 --> 00:01:07,90
and hit Enter,

24
00:01:07,90 --> 00:01:10,20
and it'll open up this read a value print loop

25
00:01:10,20 --> 00:01:11,10
in our terminal

26
00:01:11,10 --> 00:01:20,10
where we can test out different statements.

27
00:01:20,10 --> 00:01:21,00
Keep in mind, also,

28
00:01:21,00 --> 00:01:23,20
that from now on when I run code in the course

29
00:01:23,20 --> 00:01:26,20
I'm going to be running it from the exercise files folder.

30
00:01:26,20 --> 00:01:27,70
The only difference that makes is that

31
00:01:27,70 --> 00:01:31,00
the file paths in my commands might be a little different,

32
00:01:31,00 --> 00:01:32,20
so when you're running your own code

33
00:01:32,20 --> 00:01:33,50
just make sure that the file path

34
00:01:33,50 --> 00:01:35,00
points to the correct file.

