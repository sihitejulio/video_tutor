1
00:00:00,50 --> 00:00:02,70
- Before we get into learning functional programming

2
00:00:02,70 --> 00:00:04,30
with JavaScript ES6,

3
00:00:04,30 --> 00:00:06,40
let's set up an NPM directory that we can use to

4
00:00:06,40 --> 00:00:08,40
run our ES6 code.

5
00:00:08,40 --> 00:00:10,70
Open up a terminal and use cd to navigate

6
00:00:10,70 --> 00:00:12,70
to whatever parent directory you want to store

7
00:00:12,70 --> 00:00:14,80
the directory for this course in.

8
00:00:14,80 --> 00:00:16,80
I'm going to use Documents.

9
00:00:16,80 --> 00:00:18,80
And then us the make directory command to create

10
00:00:18,80 --> 00:00:20,60
a new directory.

11
00:00:20,60 --> 00:00:25,50
For example, functional-ES6.

12
00:00:25,50 --> 00:00:31,30
And then use cd to move into that directory.

13
00:00:31,30 --> 00:00:32,60
And once we're inside here,

14
00:00:32,60 --> 00:00:35,40
we want to set up our directory as an NPM package.

15
00:00:35,40 --> 00:00:41,10
And we'll do that by running NPM init dash y,

16
00:00:41,10 --> 00:00:43,30
and the y flag here sets up an NPM package

17
00:00:43,30 --> 00:00:44,90
with all the default values.

18
00:00:44,90 --> 00:00:46,70
If you want to override the default values

19
00:00:46,70 --> 00:00:49,90
for your directory, just leave off the dash y flag.

20
00:00:49,90 --> 00:00:52,40
And now that we've set up our directory with NPM,

21
00:00:52,40 --> 00:00:54,70
we can install the NPM packages necessary

22
00:00:54,70 --> 00:00:56,60
to be able to execute ES6 code

23
00:00:56,60 --> 00:00:59,30
with node JS.

24
00:00:59,30 --> 00:01:01,60
Currently, node JS doesn't have native support

25
00:01:01,60 --> 00:01:04,20
for ES6 syntax, so we need to use a tool

26
00:01:04,20 --> 00:01:06,10
called babel to act as the middleman

27
00:01:06,10 --> 00:01:07,90
between the modern ES6 syntax

28
00:01:07,90 --> 00:01:09,00
that we're going to write,

29
00:01:09,00 --> 00:01:12,60
and common JS syntax which node JS can run.

30
00:01:12,60 --> 00:01:15,00
This might sound complicated, but it isn't.

31
00:01:15,00 --> 00:01:16,90
There are just two steps we need to take in order

32
00:01:16,90 --> 00:01:18,30
to get it working.

33
00:01:18,30 --> 00:01:20,00
The first thing we need to do is install

34
00:01:20,00 --> 00:01:22,10
the necessary bable packages.

35
00:01:22,10 --> 00:01:25,60
And we do this by simply running NPM install,

36
00:01:25,60 --> 00:01:27,10
dash dash save,

37
00:01:27,10 --> 00:01:30,30
at babel slash core,

38
00:01:30,30 --> 00:01:33,10
at babel slash node,

39
00:01:33,10 --> 00:01:36,90
and at babel slash preset,

40
00:01:36,90 --> 00:01:38,80
dash env.

41
00:01:38,80 --> 00:01:41,10
And hit enter.

42
00:01:41,10 --> 00:01:43,40
And this will run for a little while,

43
00:01:43,40 --> 00:01:44,90
but once it finishes, the second thing

44
00:01:44,90 --> 00:01:48,40
we need to do is create a new file in our directory.

45
00:01:48,40 --> 00:01:49,60
In order to do that,

46
00:01:49,60 --> 00:01:52,10
let's open up our directory in an IDE.

47
00:01:52,10 --> 00:01:53,80
I'm going to use VSCode here,

48
00:01:53,80 --> 00:01:55,50
but feel free to use whatever editor

49
00:01:55,50 --> 00:01:57,50
you're comfortable with.

50
00:01:57,50 --> 00:02:03,20
So, let's open our directory,

51
00:02:03,20 --> 00:02:08,30
mine is in Documents, functional ES6.

52
00:02:08,30 --> 00:02:09,30
And inside this directory,

53
00:02:09,30 --> 00:02:11,10
we're going to create a new file,

54
00:02:11,10 --> 00:02:17,30
and we're going to call it dot babel rc.

55
00:02:17,30 --> 00:02:18,70
I'm going to zoom in a little bit here,

56
00:02:18,70 --> 00:02:20,50
so that you can see it better.

57
00:02:20,50 --> 00:02:23,10
And all we need to do here is create a json object,

58
00:02:23,10 --> 00:02:26,20
with a single property called presets.

59
00:02:26,20 --> 00:02:28,80
And the value of this property will just be an array

60
00:02:28,80 --> 00:02:30,50
with a single string,

61
00:02:30,50 --> 00:02:33,90
that says at babel slash preset,

62
00:02:33,90 --> 00:02:35,50
dash env.

63
00:02:35,50 --> 00:02:36,80
And then we save that file,

64
00:02:36,80 --> 00:02:38,40
and that's all we have to do.

65
00:02:38,40 --> 00:02:40,40
We shouldn't have to worry about this file ever again

66
00:02:40,40 --> 00:02:42,40
for the rest of the course.

67
00:02:42,40 --> 00:02:44,20
Now comes the most important part,

68
00:02:44,20 --> 00:02:45,80
running our code.

69
00:02:45,80 --> 00:02:47,40
From now on, I'm going to be running our code

70
00:02:47,40 --> 00:02:50,20
from VSCode's built-in terminal,

71
00:02:50,20 --> 00:02:53,10
but you're free to use whatever terminal you want.

72
00:02:53,10 --> 00:02:54,80
So, normally in node JS,

73
00:02:54,80 --> 00:02:57,00
we can run our code by typing node,

74
00:02:57,00 --> 00:03:00,10
and then the path to our file, and the file name.

75
00:03:00,10 --> 00:03:04,40
So for example, source slash my file dot js.

76
00:03:04,40 --> 00:03:07,40
But since we're going to be writing our code in ES6 syntax,

77
00:03:07,40 --> 00:03:09,90
which isn't yet natively supported by node,

78
00:03:09,90 --> 00:03:12,50
the command we're going to use instead is

79
00:03:12,50 --> 00:03:15,90
npx babel node,

80
00:03:15,90 --> 00:03:18,00
and then the path to our file.

