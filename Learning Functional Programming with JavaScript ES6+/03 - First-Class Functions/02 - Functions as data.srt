1
00:00:00,50 --> 00:00:01,80
- [Instructor] I mentioned in a previous video

2
00:00:01,80 --> 00:00:03,20
that functional programming treats

3
00:00:03,20 --> 00:00:05,30
functions as first class citizens,

4
00:00:05,30 --> 00:00:06,60
meaning that we can treat functions

5
00:00:06,60 --> 00:00:08,20
in a very similar way to other types

6
00:00:08,20 --> 00:00:10,80
such as strings, numbers or objects.

7
00:00:10,80 --> 00:00:12,10
In this video, we're going to take a look

8
00:00:12,10 --> 00:00:13,90
at what this means for our code,

9
00:00:13,90 --> 00:00:16,40
and a few useful things that we can do with it.

10
00:00:16,40 --> 00:00:18,30
Let's start off with a simple example.

11
00:00:18,30 --> 00:00:21,60
We'll create a function, we'll call it sayHello,

12
00:00:21,60 --> 00:00:25,30
const sayHello, and all this function will do

13
00:00:25,30 --> 00:00:28,10
is print the word Hello to the console.

14
00:00:28,10 --> 00:00:29,90
Equals parentheses,

15
00:00:29,90 --> 00:00:36,50
arrow console dot log Hello.

16
00:00:36,50 --> 00:00:38,50
What we can do now is define another function

17
00:00:38,50 --> 00:00:40,40
we'll call it sayHello2

18
00:00:40,40 --> 00:00:43,50
and set it equal to our function sayHello.

19
00:00:43,50 --> 00:00:48,90
Const sayHello2 equals sayHello.

20
00:00:48,90 --> 00:00:51,10
We can then call our sayHello2 function

21
00:00:51,10 --> 00:00:53,20
in the same way that we can call sayHello,

22
00:00:53,20 --> 00:00:56,60
just by adding parentheses to the end like this.

23
00:00:56,60 --> 00:00:59,00
This is true also when we add arguments to our function,

24
00:00:59,00 --> 00:01:00,80
for example, if we add a name argument

25
00:01:00,80 --> 00:01:03,90
to our sayHello function like this.

26
00:01:03,90 --> 00:01:05,90
And in this case, since we only have one argument,

27
00:01:05,90 --> 00:01:09,40
we can remove the parentheses

28
00:01:09,40 --> 00:01:14,40
and then we'll say plus name, in here.

29
00:01:14,40 --> 00:01:16,50
We can then add this argument to sayHello2

30
00:01:16,50 --> 00:01:18,10
and it'll be just like if we called it

31
00:01:18,10 --> 00:01:21,70
from the original sayHello function.

32
00:01:21,70 --> 00:01:23,20
You may have noticed that when we defined

33
00:01:23,20 --> 00:01:25,70
functions with the ES6 arrow syntax,

34
00:01:25,70 --> 00:01:27,80
we used the const keyword.

35
00:01:27,80 --> 00:01:29,50
This is exactly the same syntax

36
00:01:29,50 --> 00:01:31,00
that we use when defining other types,

37
00:01:31,00 --> 00:01:33,40
such as numbers, strings or objects.

38
00:01:33,40 --> 00:01:34,90
And just like with these other types,

39
00:01:34,90 --> 00:01:37,70
we can do interesting things when assigning.

40
00:01:37,70 --> 00:01:39,60
For example, we can use a ternary operator

41
00:01:39,60 --> 00:01:44,60
to dynamically change a function's definition, like this.

42
00:01:44,60 --> 00:01:49,10
We can say const myFunction equals

43
00:01:49,10 --> 00:01:52,40
and then some condition, we'll just use true here.

44
00:01:52,40 --> 00:01:55,20
And then we can do our ternary operator

45
00:01:55,20 --> 00:01:56,30
and if the condition is true,

46
00:01:56,30 --> 00:02:03,20
we can set it equal to one type of function.

47
00:02:03,20 --> 00:02:04,70
And if it's not, we can set it equal

48
00:02:04,70 --> 00:02:11,50
to another type of function.

49
00:02:11,50 --> 00:02:13,30
Now, one possible application of this

50
00:02:13,30 --> 00:02:16,60
is for mocking out pieces of our code during development.

51
00:02:16,60 --> 00:02:19,20
If we have a piece of code that's especially time intensive,

52
00:02:19,20 --> 00:02:21,60
such as network or database operations

53
00:02:21,60 --> 00:02:23,10
or especially destructive such

54
00:02:23,10 --> 00:02:25,30
as deleting an entire database,

55
00:02:25,30 --> 00:02:28,20
it can really get in the way of our development.

56
00:02:28,20 --> 00:02:30,80
So what we can do is define a mocked out version

57
00:02:30,80 --> 00:02:33,60
of our function that returns some fake data

58
00:02:33,60 --> 00:02:35,80
without doing all the time intensive operations

59
00:02:35,80 --> 00:02:38,80
that the real version of the function does.

60
00:02:38,80 --> 00:02:41,00
Let's see what this looks like.

61
00:02:41,00 --> 00:02:42,40
First, let's define a constant

62
00:02:42,40 --> 00:02:44,00
that tells our application when it's running,

63
00:02:44,00 --> 00:02:46,00
what sort of environment it's in.

64
00:02:46,00 --> 00:02:49,00
We'll call this Development.

65
00:02:49,00 --> 00:02:52,10
And then we can set it equal to either true or false.

66
00:02:52,10 --> 00:02:55,50
Right now we'll just set it equal to true.

67
00:02:55,50 --> 00:02:59,40
And then we'll create a function called fetchDataReal,

68
00:02:59,40 --> 00:03:03,70
const fetchDataReal

69
00:03:03,70 --> 00:03:08,50
and then it will just have no arguments for now.

70
00:03:08,50 --> 00:03:10,10
And this is where the time intensive

71
00:03:10,10 --> 00:03:11,50
operations would take place.

72
00:03:11,50 --> 00:03:15,30
For now we'll just have a comment there.

73
00:03:15,30 --> 00:03:16,80
We can then define another function

74
00:03:16,80 --> 00:03:22,50
called fetchDataFake, const fetchDataFake

75
00:03:22,50 --> 00:03:23,70
and what this function will do is

76
00:03:23,70 --> 00:03:25,30
simply mock out the data that we

77
00:03:25,30 --> 00:03:27,40
expect to get back from our server.

78
00:03:27,40 --> 00:03:28,60
For example, if we're expecting

79
00:03:28,60 --> 00:03:36,30
to get a person object back like this.

80
00:03:36,30 --> 00:03:37,80
And finally we'll define the function

81
00:03:37,80 --> 00:03:40,20
that would be used elsewhere in the app.

82
00:03:40,20 --> 00:03:41,90
This is where we use the ternary operator

83
00:03:41,90 --> 00:03:44,00
to decide which of the function definitions,

84
00:03:44,00 --> 00:03:47,80
the real one or the fake one will be used at runtime.

85
00:03:47,80 --> 00:03:53,00
We'll simply call this function fetchData, const fetchData,

86
00:03:53,00 --> 00:03:54,50
and we're going to use a ternary operator

87
00:03:54,50 --> 00:03:59,30
which depends on Development.

88
00:03:59,30 --> 00:04:01,00
And if Development is true, we want to

89
00:04:01,00 --> 00:04:07,10
use our fetchDataFake function.

90
00:04:07,10 --> 00:04:09,00
And if development is false, in other words,

91
00:04:09,00 --> 00:04:11,50
if our application is running in production mode,

92
00:04:11,50 --> 00:04:17,70
we want to use the real fetchData, so fetchDataReal.

93
00:04:17,70 --> 00:04:19,10
And that's all there is to it.

94
00:04:19,10 --> 00:04:20,80
Now, obviously, in a real application,

95
00:04:20,80 --> 00:04:24,10
we probably use stuff like promises or async or wait

96
00:04:24,10 --> 00:04:25,50
but for illustration purposes,

97
00:04:25,50 --> 00:04:27,90
this example serves its purpose.

98
00:04:27,90 --> 00:04:29,30
There are many other applications

99
00:04:29,30 --> 00:04:31,00
for this sort of function assignment.

100
00:04:31,00 --> 00:04:32,60
For example, if you want to do stuff

101
00:04:32,60 --> 00:04:34,80
like A/B testing different implementations

102
00:04:34,80 --> 00:04:37,20
of a function, for performance reasons,

103
00:04:37,20 --> 00:04:39,00
but I'll leave that to you to experiment with.

