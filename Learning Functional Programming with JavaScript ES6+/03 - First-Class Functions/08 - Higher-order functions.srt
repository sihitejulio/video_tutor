1
00:00:00,50 --> 00:00:01,80
- [Instructor] We've spent quite a bit of time

2
00:00:01,80 --> 00:00:04,20
in previous videos talking about first-class functions

3
00:00:04,20 --> 00:00:05,20
in JavaScript.

4
00:00:05,20 --> 00:00:07,40
We've seen how JavaScript treats functions

5
00:00:07,40 --> 00:00:09,10
in much the same way as strings, numbers,

6
00:00:09,10 --> 00:00:10,20
objects and arrays.

7
00:00:10,20 --> 00:00:12,10
And how this allows us to do things,

8
00:00:12,10 --> 00:00:14,60
like pass functions as arguments to other functions

9
00:00:14,60 --> 00:00:16,80
and return functions from other functions.

10
00:00:16,80 --> 00:00:18,80
Now, there's a term for functions

11
00:00:18,80 --> 00:00:20,90
that either take other functions as arguments

12
00:00:20,90 --> 00:00:22,30
or return functions.

13
00:00:22,30 --> 00:00:25,10
We call them higher-order functions.

14
00:00:25,10 --> 00:00:28,00
Using them in our code can provide amazing flexibility

15
00:00:28,00 --> 00:00:28,80
and re usability.

16
00:00:28,80 --> 00:00:32,10
To demonstrate this fact, we're going to look

17
00:00:32,10 --> 00:00:34,00
at some examples of higher-order functions

18
00:00:34,00 --> 00:00:35,30
and what they can do.

19
00:00:35,30 --> 00:00:38,10
The first thing we're going to use higher-order functions

20
00:00:38,10 --> 00:00:40,50
to solve, is the problem of checking arguments

21
00:00:40,50 --> 00:00:41,70
in JavaScript.

22
00:00:41,70 --> 00:00:44,70
It happens quite often in programming that we want

23
00:00:44,70 --> 00:00:47,40
to make sure our arguments meet certain criteria.

24
00:00:47,40 --> 00:00:49,00
We might want to make sure that our arguments

25
00:00:49,00 --> 00:00:51,30
are the correct types, for example

26
00:00:51,30 --> 00:00:52,80
or we might want to make sure that a number

27
00:00:52,80 --> 00:00:56,40
we pass in is positive or that two arrays that we pass in

28
00:00:56,40 --> 00:00:58,40
have the same number of elements.

29
00:00:58,40 --> 00:01:00,20
There are many different situations like this

30
00:01:00,20 --> 00:01:02,60
that we might run into in our programs.

31
00:01:02,60 --> 00:01:04,20
Now, normally, when we want to perform

32
00:01:04,20 --> 00:01:06,20
these kind of checks, they make their way

33
00:01:06,20 --> 00:01:07,70
into the body of our function.

34
00:01:07,70 --> 00:01:11,10
Let's take a simple example of writing a division function.

35
00:01:11,10 --> 00:01:13,40
A function that takes two arguments

36
00:01:13,40 --> 00:01:16,30
and divides the first argument by the other, like this.

37
00:01:16,30 --> 00:01:24,90
const divide = (x, y) ==> x / y.

38
00:01:24,90 --> 00:01:27,50
Obviously, we'd like to prevent the second argument

39
00:01:27,50 --> 00:01:28,50
from being zero.

40
00:01:28,50 --> 00:01:33,60
So, we add that condition to the function body, like this.

41
00:01:33,60 --> 00:01:36,00
First, we have to add brackets.

42
00:01:36,00 --> 00:01:41,10
And the return statement. (computer keys clicking)

43
00:01:41,10 --> 00:01:43,50
And we add an if statement that looks like this.

44
00:01:43,50 --> 00:01:48,30
If (y === 0)

45
00:01:48,30 --> 00:01:51,10
then we want to log an error to the console.

46
00:01:51,10 --> 00:01:57,60
It'll say something like Error: dividing by zero.

47
00:01:57,60 --> 00:02:01,70
And then it will do something like return null.

48
00:02:01,70 --> 00:02:04,10
This will prevent our function from returning

49
00:02:04,10 --> 00:02:07,20
not a number, when the second argument is zero.

50
00:02:07,20 --> 00:02:09,70
Now, this works and this pattern of argument checking

51
00:02:09,70 --> 00:02:12,00
is quite common in many codebases.

52
00:02:12,00 --> 00:02:14,00
But now we're cluttering up our divide function

53
00:02:14,00 --> 00:02:15,90
with argument checking logic.

54
00:02:15,90 --> 00:02:18,20
The single responsibility principle,

55
00:02:18,20 --> 00:02:20,90
a very important principle to follow in writing clean code,

56
00:02:20,90 --> 00:02:23,00
states that each piece of code should

57
00:02:23,00 --> 00:02:24,60
have only one responsibility.

58
00:02:24,60 --> 00:02:27,80
In other words, if describing what a given piece

59
00:02:27,80 --> 00:02:30,50
of code does requires you to name two or more things

60
00:02:30,50 --> 00:02:33,40
it's a sign that you should refactor.

61
00:02:33,40 --> 00:02:35,90
In our case, our divide function is not only returning

62
00:02:35,90 --> 00:02:38,60
the quotient of two numbers, but it's also worrying

63
00:02:38,60 --> 00:02:42,40
about checking that the arguments fit a specific criteria.

64
00:02:42,40 --> 00:02:44,00
Let's see how we can improve this code

65
00:02:44,00 --> 00:02:47,20
using higher-order functions.

66
00:02:47,20 --> 00:02:49,10
Our basic plan of attack here, is to have

67
00:02:49,10 --> 00:02:51,40
our divide function only worry about returning

68
00:02:51,40 --> 00:02:53,30
the result of division.

69
00:02:53,30 --> 00:02:55,80
So, instead of including the argument checking logic inside

70
00:02:55,80 --> 00:02:58,50
the divide function, what we're going to do

71
00:02:58,50 --> 00:03:00,60
is create another function.

72
00:03:00,60 --> 00:03:02,10
We're going to name this function with

73
00:03:02,10 --> 00:03:10,00
the rather lengthy name of secondArgumentIsntZero.

74
00:03:10,00 --> 00:03:12,10
What this function will do, is take a function

75
00:03:12,10 --> 00:03:15,00
as an argument, we'll call it func.

76
00:03:15,00 --> 00:03:18,00
This func argument will represent the function

77
00:03:18,00 --> 00:03:21,20
that we want to call if a given criteria is met.

78
00:03:21,20 --> 00:03:23,40
So, we'll be returning a function.

79
00:03:23,40 --> 00:03:26,40
For the arguments here, we're going to use

80
00:03:26,40 --> 00:03:28,90
the ES6 spread operator to get all the arguments

81
00:03:28,90 --> 00:03:30,70
we pass in as an array.

82
00:03:30,70 --> 00:03:33,10
Then inside this function that we're returning

83
00:03:33,10 --> 00:03:35,80
we're going to perform the argument checking.

84
00:03:35,80 --> 00:03:38,30
In our case, it's going to be almost exactly

85
00:03:38,30 --> 00:03:41,70
the same as this if statement we defined inside divide.

86
00:03:41,70 --> 00:03:44,60
Except, instead of checking y

87
00:03:44,60 --> 00:03:48,00
we're going to make sure the second argument isn't zero.

88
00:03:48,00 --> 00:03:52,00
And the second argument, of course, has the index of one.

89
00:03:52,00 --> 00:03:54,30
Then, if we get past this if statement

90
00:03:54,30 --> 00:03:58,50
what we're going to do is return our original function argument

91
00:03:58,50 --> 00:04:01,00
that we passed in, called with all the arguments

92
00:04:01,00 --> 00:04:03,00
that we passed in.

93
00:04:03,00 --> 00:04:06,90
Remember here, that we also have to use the spread operator.

94
00:04:06,90 --> 00:04:09,20
And that's what our secondArgumentIsntZero function

95
00:04:09,20 --> 00:04:10,70
is going to look like.

96
00:04:10,70 --> 00:04:12,50
Now, let's see how to use it.

97
00:04:12,50 --> 00:04:14,60
First, let's delete the checking logic from divide

98
00:04:14,60 --> 00:04:16,70
so that all it's doing is returning the quotient

99
00:04:16,70 --> 00:04:18,60
of the two arguments we pass in.

100
00:04:18,60 --> 00:04:24,00
(computer keys clicking)

101
00:04:24,00 --> 00:04:26,50
So using this function is quite simple.

102
00:04:26,50 --> 00:04:28,50
All we have to do is define a new function.

103
00:04:28,50 --> 00:04:31,40
We'll call it, divideSafe.

104
00:04:31,40 --> 00:04:38,20
So, const divideSafe = secondArgumentIsntZero

105
00:04:38,20 --> 00:04:41,60
and we're going to pass the divide function as an argument.

106
00:04:41,60 --> 00:04:43,90
We can now call our divideSafe function

107
00:04:43,90 --> 00:04:46,60
in the exact same way that we called our divide function.

108
00:04:46,60 --> 00:04:48,80
Like this,

109
00:04:48,80 --> 00:04:55,60
console.log(divideSafe(7, 0)).

110
00:04:55,60 --> 00:04:56,70
If we run our code by typing

111
00:04:56,70 --> 00:05:05,70
npx babel-node Ch02/02_08/Start/examples.js

112
00:05:05,70 --> 00:05:08,10
and run our code,

113
00:05:08,10 --> 00:05:11,30
we see that it correctly logs the error and returns null,

114
00:05:11,30 --> 00:05:13,20
just as we wanted it to.

115
00:05:13,20 --> 00:05:15,90
The nice part here, is that both our divide function

116
00:05:15,90 --> 00:05:18,40
and our secondArgumentIsntZero function

117
00:05:18,40 --> 00:05:20,50
only have one responsibility.

118
00:05:20,50 --> 00:05:22,90
Divide's function is to find the quotient of

119
00:05:22,90 --> 00:05:26,30
the two arguments and our secondArgumentIsntZero function,

120
00:05:26,30 --> 00:05:29,10
its job is just to check the second argument

121
00:05:29,10 --> 00:05:31,00
and make sure it isn't zero.

122
00:05:31,00 --> 00:05:35,00
Of course, if we try and call our divideSafe function,

123
00:05:35,00 --> 00:05:37,80
with arguments that will pass the check,

124
00:05:37,80 --> 00:05:41,20
and run our code again,

125
00:05:41,20 --> 00:05:44,10
it will give us the correct result.

126
00:05:44,10 --> 00:05:46,00
Which is exactly what we want.

