1
00:00:00,50 --> 00:00:01,90
- [Instructor] Since throughout the rest of the course

2
00:00:01,90 --> 00:00:05,70
we want to take full advantage of many of ES6's features,

3
00:00:05,70 --> 00:00:06,70
the first thing we're going to do

4
00:00:06,70 --> 00:00:08,40
is really solidify our experience

5
00:00:08,40 --> 00:00:10,40
with the alternative syntax that ES6

6
00:00:10,40 --> 00:00:12,60
provides for defining functions.

7
00:00:12,60 --> 00:00:15,30
In JavaScript prior to ES6 the most common way

8
00:00:15,30 --> 00:00:18,30
of defining functions was to use the function keyword.

9
00:00:18,30 --> 00:00:19,70
And there were two ways of doing this,

10
00:00:19,70 --> 00:00:22,60
either by writing function, the function's name,

11
00:00:22,60 --> 00:00:24,80
and then the arguments like this.

12
00:00:24,80 --> 00:00:27,60
Function myFunction

13
00:00:27,60 --> 00:00:31,90
and the arguments, arg1, arg2, et cetera,

14
00:00:31,90 --> 00:00:36,40
and then brackets followed by the function body.

15
00:00:36,40 --> 00:00:38,30
Or by using var, let, or const

16
00:00:38,30 --> 00:00:40,50
to define our function like this.

17
00:00:40,50 --> 00:00:46,00
Const my0therFunction = function,

18
00:00:46,00 --> 00:00:50,20
and then the arguments, arg1, arg2,

19
00:00:50,20 --> 00:00:54,10
and then brackets followed by the function body.

20
00:00:54,10 --> 00:00:55,90
Now ES6 allows us to get rid

21
00:00:55,90 --> 00:00:57,40
of the function keyword completely

22
00:00:57,40 --> 00:01:01,50
by providing us with a new syntax called arrow functions.

23
00:01:01,50 --> 00:01:03,10
Many of you may already be familiar

24
00:01:03,10 --> 00:01:04,70
with arrow functions or you may not.

25
00:01:04,70 --> 00:01:07,40
But either way, since I'll be using this syntax heavily

26
00:01:07,40 --> 00:01:09,20
throughout the rest of the course,

27
00:01:09,20 --> 00:01:12,50
let's go through some examples to get comfortable with them.

28
00:01:12,50 --> 00:01:14,10
Let's start with a very simple example,

29
00:01:14,10 --> 00:01:15,90
a function that takes two number arguments

30
00:01:15,90 --> 00:01:17,70
and adds them together.

31
00:01:17,70 --> 00:01:20,50
The most long-winded way to write this using arrow notation

32
00:01:20,50 --> 00:01:22,50
would be like this.

33
00:01:22,50 --> 00:01:26,30
Const add, we'll call our function add,

34
00:01:26,30 --> 00:01:30,10
equals the arguments, x and y,

35
00:01:30,10 --> 00:01:34,30
and then the arrow and then brackets,

36
00:01:34,30 --> 00:01:39,80
and then we're going to say const sum = x + y.

37
00:01:39,80 --> 00:01:42,70
And finally we're going to return the sum.

38
00:01:42,70 --> 00:01:44,80
Now as I mentioned, this is a rather long way

39
00:01:44,80 --> 00:01:47,40
of writing this function using arrow syntax,

40
00:01:47,40 --> 00:01:49,90
but it'll give us the chance to see a few nice things

41
00:01:49,90 --> 00:01:52,30
that arrow functions let us do syntax wise

42
00:01:52,30 --> 00:01:54,50
for specific cases.

43
00:01:54,50 --> 00:01:58,00
For example, if we cut out this intermediary sum constant

44
00:01:58,00 --> 00:02:02,80
we can simply return x + y like this.

45
00:02:02,80 --> 00:02:08,40
Delete that and return x + y.

46
00:02:08,40 --> 00:02:09,80
And this is where arrow functions

47
00:02:09,80 --> 00:02:11,40
can start looking really nice

48
00:02:11,40 --> 00:02:13,30
because when the only statement in a function

49
00:02:13,30 --> 00:02:18,40
is its return statement we can drop both the brackets,

50
00:02:18,40 --> 00:02:24,30
delete those, and we can also delete the return keyword.

51
00:02:24,30 --> 00:02:26,70
Like this.

52
00:02:26,70 --> 00:02:29,00
Personally I find this nice compact syntax

53
00:02:29,00 --> 00:02:31,30
to be much more beautiful than using the function keyword

54
00:02:31,30 --> 00:02:34,30
with the brackets and the return keyword.

55
00:02:34,30 --> 00:02:36,70
Once we get into more complex functional concepts,

56
00:02:36,70 --> 00:02:38,30
the arrow function will contribute a lot

57
00:02:38,30 --> 00:02:41,30
toward keeping our code concise and uncluttered.

58
00:02:41,30 --> 00:02:42,60
If at any point you find yourself

59
00:02:42,60 --> 00:02:44,50
getting confused by this syntax,

60
00:02:44,50 --> 00:02:45,90
just remember that it's a shorthand

61
00:02:45,90 --> 00:02:48,60
for the regular function syntax.

62
00:02:48,60 --> 00:02:51,30
There's one more nice thing that the arrow function notation

63
00:02:51,30 --> 00:02:53,60
lets us do syntax wise.

64
00:02:53,60 --> 00:02:56,30
Let's say that we have a function with only one argument.

65
00:02:56,30 --> 00:02:57,90
In this example we'll make a function

66
00:02:57,90 --> 00:03:01,20
that takes a single number and doubles it like this.

67
00:03:01,20 --> 00:03:07,10
Const, we'll call it double = number, which is our argument.

68
00:03:07,10 --> 00:03:10,80
And then it returns number times 2.

69
00:03:10,80 --> 00:03:12,60
Now when we define a function like this

70
00:03:12,60 --> 00:03:13,90
with only one argument,

71
00:03:13,90 --> 00:03:16,90
arrow syntax allows us to completely drop the parentheses

72
00:03:16,90 --> 00:03:20,20
around the argument like this.

73
00:03:20,20 --> 00:03:22,70
Again, don't let this syntax confuse you.

74
00:03:22,70 --> 00:03:25,60
At its core it's just a shorthand version of writing this

75
00:03:25,60 --> 00:03:30,00
using the original non-ES6 syntax like this.

76
00:03:30,00 --> 00:03:35,70
Const double = function number

77
00:03:35,70 --> 00:03:40,40
brackets and then return number times 2.

78
00:03:40,40 --> 00:03:41,50
And what does it look like when we

79
00:03:41,50 --> 00:03:43,60
have a function with zero arguments?

80
00:03:43,60 --> 00:03:46,40
Well we can't just have a blank space with arguments in it,

81
00:03:46,40 --> 00:03:49,10
so when we define a function with no arguments,

82
00:03:49,10 --> 00:03:52,60
we just put two empty parentheses like this.

83
00:03:52,60 --> 00:04:02,40
Const sayHello = parentheses console.log Hello.

84
00:04:02,40 --> 00:04:04,20
One more thing to keep in mind is that if we want

85
00:04:04,20 --> 00:04:06,90
to write an arrow function that returns an object

86
00:04:06,90 --> 00:04:08,70
and we're omitting the brackets,

87
00:04:08,70 --> 00:04:13,20
we need to wrap the function body in parentheses like this.

88
00:04:13,20 --> 00:04:16,80
Let's create a function called getPersonData.

89
00:04:16,80 --> 00:04:22,10
So const getPersonData = empty parentheses,

90
00:04:22,10 --> 00:04:23,70
and then since we're returning an object

91
00:04:23,70 --> 00:04:27,00
we have to put parentheses and then brackets.

92
00:04:27,00 --> 00:04:30,30
And then we can define our object properties.

93
00:04:30,30 --> 00:04:34,80
So we'll say name John Doe again.

94
00:04:34,80 --> 00:04:37,70
Age, 34.

95
00:04:37,70 --> 00:04:42,00
Job, programmer.

96
00:04:42,00 --> 00:04:44,10
Note that here if we didn't wrap the object

97
00:04:44,10 --> 00:04:49,40
we're returning in parentheses like this,

98
00:04:49,40 --> 00:04:51,40
it would look to the interpreter like we wanted

99
00:04:51,40 --> 00:04:54,40
to use the brackets to define the body of our function.

100
00:04:54,40 --> 00:04:55,70
And that's why we have to wrap it

101
00:04:55,70 --> 00:05:00,40
in parentheses in this specific case.

102
00:05:00,40 --> 00:05:02,20
So then let's just review the different ways

103
00:05:02,20 --> 00:05:05,80
we can write the parts of an arrow function.

104
00:05:05,80 --> 00:05:08,70
Let's say const

105
00:05:08,70 --> 00:05:11,00
myArrowFunction.

106
00:05:11,00 --> 00:05:13,20
And if our function has zero arguments

107
00:05:13,20 --> 00:05:15,60
then it's just empty parentheses.

108
00:05:15,60 --> 00:05:17,50
And if we have one argument we can

109
00:05:17,50 --> 00:05:20,10
write it without the parentheses.

110
00:05:20,10 --> 00:05:21,60
Like this.

111
00:05:21,60 --> 00:05:24,20
And if it's two or more arguments we're still required

112
00:05:24,20 --> 00:05:29,20
to wrap the arguments in parentheses like this.

113
00:05:29,20 --> 00:05:31,10
And when we're writing the function body,

114
00:05:31,10 --> 00:05:32,20
if our function contains

115
00:05:32,20 --> 00:05:34,40
more than just the return statement,

116
00:05:34,40 --> 00:05:39,40
we have to wrap it in brackets like this.

117
00:05:39,40 --> 00:05:43,80
And we also have to use the return keyword.

118
00:05:43,80 --> 00:05:47,00
Otherwise, if our function body only has one statement,

119
00:05:47,00 --> 00:05:49,60
we can simply drop the brackets and ES6

120
00:05:49,60 --> 00:05:50,80
will automatically return

121
00:05:50,80 --> 00:05:57,60
whatever the result of that line is.

122
00:05:57,60 --> 00:06:02,10
And we drop the return keyword as well.

123
00:06:02,10 --> 00:06:04,70
And finally, if you're returning an object in this case,

124
00:06:04,70 --> 00:06:09,50
remember to wrap it in parentheses.

125
00:06:09,50 --> 00:06:11,00
Like this.

