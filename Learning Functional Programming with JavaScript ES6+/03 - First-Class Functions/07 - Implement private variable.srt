1
00:00:00,50 --> 00:00:01,40
- [Instructor] So the most common way

2
00:00:01,40 --> 00:00:03,00
that I see programmers implementing

3
00:00:03,00 --> 00:00:04,80
private variables in JavaScript

4
00:00:04,80 --> 00:00:06,60
is by simply adding an underscore

5
00:00:06,60 --> 00:00:07,90
at the beginning of their variable name.

6
00:00:07,90 --> 00:00:11,70
For example, _name.

7
00:00:11,70 --> 00:00:13,70
This is supposed to signify to themselves

8
00:00:13,70 --> 00:00:15,10
and to other developers that you should

9
00:00:15,10 --> 00:00:18,30
never directly use this variable outside the class.

10
00:00:18,30 --> 00:00:20,10
However, in my experience programmers

11
00:00:20,10 --> 00:00:22,70
will always ignore this when it's convenient for them.

12
00:00:22,70 --> 00:00:24,90
So let's look at a better way.

13
00:00:24,90 --> 00:00:27,80
Let's imagine that we want to create a person class.

14
00:00:27,80 --> 00:00:29,60
Now instead of using the class keyword,

15
00:00:29,60 --> 00:00:31,50
as many people are used to doing,

16
00:00:31,50 --> 00:00:33,30
we're just going to define it as a function

17
00:00:33,30 --> 00:00:35,40
that returns a new person object.

18
00:00:35,40 --> 00:00:37,10
And that'll look like this.

19
00:00:37,10 --> 00:00:42,10
We'll say const Person = function.

20
00:00:42,10 --> 00:00:44,30
And then instead of passing multiple arguments

21
00:00:44,30 --> 00:00:47,30
to specify name, age, and job for this person,

22
00:00:47,30 --> 00:00:49,70
we're going to use object destructuring.

23
00:00:49,70 --> 00:00:56,10
And we're going to have name, age, and job properties.

24
00:00:56,10 --> 00:00:58,40
And then we're going to define the function body here,

25
00:00:58,40 --> 00:01:00,00
and at some point inside this function

26
00:01:00,00 --> 00:01:01,60
we're going to return an object.

27
00:01:01,60 --> 00:01:03,80
We'll get to that in a second.

28
00:01:03,80 --> 00:01:06,80
Now let's define our quote, unquote member variables.

29
00:01:06,80 --> 00:01:10,00
Note that since we've used the variables name, age, and job,

30
00:01:10,00 --> 00:01:13,40
up in the arguments, we'll have to rename these a bit.

31
00:01:13,40 --> 00:01:19,40
What we're going to do is say var _name = name,

32
00:01:19,40 --> 00:01:23,10
var _age = age,

33
00:01:23,10 --> 00:01:27,80
var _job = job.

34
00:01:27,80 --> 00:01:29,50
Now comes the fun part.

35
00:01:29,50 --> 00:01:32,10
We're going to use closure to make these variables accessible

36
00:01:32,10 --> 00:01:35,60
to the outside world only through getters and setters.

37
00:01:35,60 --> 00:01:37,30
Let's start by writing the getter function

38
00:01:37,30 --> 00:01:38,80
for the person's name.

39
00:01:38,80 --> 00:01:40,30
We're going to define it as a property

40
00:01:40,30 --> 00:01:43,20
inside the object we're returning.

41
00:01:43,20 --> 00:01:45,60
We'll call this function getName

42
00:01:45,60 --> 00:01:49,10
and define it using the arrow syntax, as usual.

43
00:01:49,10 --> 00:01:50,60
And finally, inside this function,

44
00:01:50,60 --> 00:01:52,40
we're simply going to return the name variable

45
00:01:52,40 --> 00:01:54,30
that we have access to.

46
00:01:54,30 --> 00:01:55,80
Let's stop and take a look at how we can

47
00:01:55,80 --> 00:01:58,90
use this function that we've created so far.

48
00:01:58,90 --> 00:02:02,10
First we'll create a new person object like this.

49
00:02:02,10 --> 00:02:05,80
Const me = Person

50
00:02:05,80 --> 00:02:11,60
and then we'll say name Shaun age 25,

51
00:02:11,60 --> 00:02:15,40
and job developer.

52
00:02:15,40 --> 00:02:19,00
And then if we want to see the value of this person's name,

53
00:02:19,00 --> 00:02:24,00
we can type me.getName.

54
00:02:24,00 --> 00:02:27,60
Let's print this to the console to see the result.

55
00:02:27,60 --> 00:02:31,80
Console.log

56
00:02:31,80 --> 00:02:36,30
and then let's run our code by typing npx babel-node

57
00:02:36,30 --> 00:02:48,00
and then the path to our file, Ch02/02_07/Start/examples.js.

58
00:02:48,00 --> 00:02:50,70
And we see that it correctly prints the person's name.

59
00:02:50,70 --> 00:02:53,80
Now here's the really important part to understand.

60
00:02:53,80 --> 00:02:56,00
This getName function is the only way

61
00:02:56,00 --> 00:02:58,20
we can access the name variable.

62
00:02:58,20 --> 00:03:04,30
If we try to do something like me._name,

63
00:03:04,30 --> 00:03:06,90
this will return undefined.

64
00:03:06,90 --> 00:03:08,30
And this might seem strange at first,

65
00:03:08,30 --> 00:03:09,80
but let's take a look.

66
00:03:09,80 --> 00:03:11,00
The object we're returning here

67
00:03:11,00 --> 00:03:13,70
doesn't have any name property.

68
00:03:13,70 --> 00:03:15,70
The only thing it has is a getName function

69
00:03:15,70 --> 00:03:19,20
that has access to the name variable through closure.

70
00:03:19,20 --> 00:03:21,70
Let's write the getters for the other two properties now.

71
00:03:21,70 --> 00:03:23,90
Here's what that will look like.

72
00:03:23,90 --> 00:03:26,50
getAge,

73
00:03:26,50 --> 00:03:30,30
we're going to have a function that returns our age variable.

74
00:03:30,30 --> 00:03:33,00
And getJob, we're going to have a function

75
00:03:33,00 --> 00:03:37,20
that simply returns our job variable.

76
00:03:37,20 --> 00:03:39,20
And we can then use these in the exact same way

77
00:03:39,20 --> 00:03:42,50
that we used getName.

78
00:03:42,50 --> 00:03:45,60
Now let's write the setters for our private variables.

79
00:03:45,60 --> 00:03:47,40
Just for the sake of illustration,

80
00:03:47,40 --> 00:03:50,00
we're going to leave the name variable without a getter,

81
00:03:50,00 --> 00:03:51,30
signifying that it's the property

82
00:03:51,30 --> 00:03:53,80
of a person that can't be changed.

83
00:03:53,80 --> 00:03:54,80
Let's see what the setter for

84
00:03:54,80 --> 00:03:56,90
our job variable will look like.

85
00:03:56,90 --> 00:03:58,40
We're going to add it to the same object

86
00:03:58,40 --> 00:04:01,40
that we're returning along with all of the getters.

87
00:04:01,40 --> 00:04:04,50
We'll call this function setJob.

88
00:04:04,50 --> 00:04:06,00
This function will take one argument,

89
00:04:06,00 --> 00:04:07,50
which is a string signifying what

90
00:04:07,50 --> 00:04:09,70
we want to change the job variable to.

91
00:04:09,70 --> 00:04:11,90
We'll call it newJob.

92
00:04:11,90 --> 00:04:14,30
And then inside the function we're just going to

93
00:04:14,30 --> 00:04:17,20
set our interior job variable equal

94
00:04:17,20 --> 00:04:19,20
to the argument that we pass in.

95
00:04:19,20 --> 00:04:22,40
_job = newJob.

96
00:04:22,40 --> 00:04:26,50
Now let's see what using this new function will look like.

97
00:04:26,50 --> 00:04:29,80
If we go back to this person object that we defined earlier

98
00:04:29,80 --> 00:04:33,90
we can call getJob and see that we have

99
00:04:33,90 --> 00:04:38,20
access to the job variable through closure

100
00:04:38,20 --> 00:04:41,30
by running our code again.

101
00:04:41,30 --> 00:04:44,70
We can then call our new setJob function,

102
00:04:44,70 --> 00:04:47,50
me.setJob,

103
00:04:47,50 --> 00:04:49,40
with a new value.

104
00:04:49,40 --> 00:04:53,10
We'll say senior developer.

105
00:04:53,10 --> 00:04:56,20
And if we print the value of getJob again,

106
00:04:56,20 --> 00:05:01,50
console.log me.getJob, we'll see that

107
00:05:01,50 --> 00:05:06,80
it prints the updated value.

108
00:05:06,80 --> 00:05:09,10
Again, I really want to stress that these functions

109
00:05:09,10 --> 00:05:12,00
are the only access that we have to this job variable

110
00:05:12,00 --> 00:05:14,20
thanks to the magic of closure.

111
00:05:14,20 --> 00:05:16,80
If we try to access the job variable as a property

112
00:05:16,80 --> 00:05:22,10
of our person object we get undefined like this.

113
00:05:22,10 --> 00:05:26,50
This will be undefined.

114
00:05:26,50 --> 00:05:29,60
As an exercise, why don't you try writing this setAge setter

115
00:05:29,60 --> 00:05:31,00
for our person object.

