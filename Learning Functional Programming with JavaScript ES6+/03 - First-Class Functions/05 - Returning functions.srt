1
00:00:00,50 --> 00:00:01,30
- [Narrator] So far, we've seen

2
00:00:01,30 --> 00:00:02,90
that's it's possible to define functions

3
00:00:02,90 --> 00:00:05,20
in pretty much the same way we define other things,

4
00:00:05,20 --> 00:00:06,80
such as strings or numbers,

5
00:00:06,80 --> 00:00:08,40
and we've also seen that it's possible

6
00:00:08,40 --> 00:00:10,00
to pass functions as arguments,

7
00:00:10,00 --> 00:00:12,20
again, similar to other types.

8
00:00:12,20 --> 00:00:14,40
So the next logical thing for us to look at

9
00:00:14,40 --> 00:00:16,80
with respect to first-class functions

10
00:00:16,80 --> 00:00:20,20
is our ability to return functions from other functions.

11
00:00:20,20 --> 00:00:22,10
Now the usefulness of first-class functions

12
00:00:22,10 --> 00:00:23,80
has been kind of limited up until now,

13
00:00:23,80 --> 00:00:25,10
without this final piece of our

14
00:00:25,10 --> 00:00:27,10
first-class functions puzzle.

15
00:00:27,10 --> 00:00:28,70
This is the place where the flexibility

16
00:00:28,70 --> 00:00:30,20
that first-class functions give us

17
00:00:30,20 --> 00:00:32,70
really starts to shine through.

18
00:00:32,70 --> 00:00:34,50
So first let's try to wrap our heads around

19
00:00:34,50 --> 00:00:37,30
the concept of functions and return functions.

20
00:00:37,30 --> 00:00:39,20
In programming, it's common to hear functions

21
00:00:39,20 --> 00:00:41,10
referred to as a black box.

22
00:00:41,10 --> 00:00:44,50
You put data into the box, you get some different data out.

23
00:00:44,50 --> 00:00:47,30
If we're talking about a function called double for example,

24
00:00:47,30 --> 00:00:49,90
we'd put one number the box, say five,

25
00:00:49,90 --> 00:00:53,30
and we'd get another number out, in this case ten.

26
00:00:53,30 --> 00:00:55,90
Or, if we're talking about a function called upper case,

27
00:00:55,90 --> 00:01:00,00
we put a string into the box, say hello, all in lower case

28
00:01:00,00 --> 00:01:03,40
and we get the same string out of the box in upper case.

29
00:01:03,40 --> 00:01:05,20
Now, since JavaScript treats functions

30
00:01:05,20 --> 00:01:07,10
the same as strings and numbers,

31
00:01:07,10 --> 00:01:09,60
it's possible in JavaScript to have a black box

32
00:01:09,60 --> 00:01:11,70
that returns another black box.

33
00:01:11,70 --> 00:01:14,10
This other black box then behaves the same way

34
00:01:14,10 --> 00:01:15,80
as a regular function.

35
00:01:15,80 --> 00:01:19,00
We put data in, and get some different data out.

36
00:01:19,00 --> 00:01:22,10
This is exactly what we're doing when we return functions.

37
00:01:22,10 --> 00:01:24,70
So when we have a function that returns another function,

38
00:01:24,70 --> 00:01:25,90
it might help to think of it

39
00:01:25,90 --> 00:01:29,60
as a function creator, or a function factory.

40
00:01:29,60 --> 00:01:31,80
Let's look at the syntax for defining a function

41
00:01:31,80 --> 00:01:33,50
that returns another function.

42
00:01:33,50 --> 00:01:35,60
This is the part that unnerves the people the most

43
00:01:35,60 --> 00:01:37,00
the first time they look at it.

44
00:01:37,00 --> 00:01:40,20
But bear with me, once you understand it, it'll be easy.

45
00:01:40,20 --> 00:01:42,80
So let's do the simplest possible example,

46
00:01:42,80 --> 00:01:45,40
let's define a function that returns a function,

47
00:01:45,40 --> 00:01:48,70
that prints something to the console, const.

48
00:01:48,70 --> 00:01:50,70
And I mentioned it's often helpful

49
00:01:50,70 --> 00:01:53,70
to think of these kinds of functions as function creators,

50
00:01:53,70 --> 00:01:56,70
so let's call this one createPrinter.

51
00:01:56,70 --> 00:01:59,00
Now, as with most of our other examples,

52
00:01:59,00 --> 00:02:01,90
we're going to define this function using arrow syntax.

53
00:02:01,90 --> 00:02:03,70
This function doesn't take any arguments,

54
00:02:03,70 --> 00:02:06,30
so we'll put empty parenthesis,

55
00:02:06,30 --> 00:02:08,10
and it returns another function

56
00:02:08,10 --> 00:02:10,60
which we'll also define using arrow syntax,

57
00:02:10,60 --> 00:02:12,60
that doesn't take any arguments,

58
00:02:12,60 --> 00:02:17,50
and prints hello to the console,

59
00:02:17,50 --> 00:02:20,40
console.log('Hello').

60
00:02:20,40 --> 00:02:22,40
The part that confuses people the most

61
00:02:22,40 --> 00:02:24,00
about this sort of definition

62
00:02:24,00 --> 00:02:27,00
is the double parenthesis and arrows.

63
00:02:27,00 --> 00:02:28,40
It might take a little practice

64
00:02:28,40 --> 00:02:30,50
to get used to reading functions like this,

65
00:02:30,50 --> 00:02:31,70
but what you have to remember is

66
00:02:31,70 --> 00:02:33,40
that this arrow function syntax

67
00:02:33,40 --> 00:02:35,00
is just a shorthand way of writing

68
00:02:35,00 --> 00:02:38,90
the much longer version, which would look like this.

69
00:02:38,90 --> 00:02:42,90
Adding in the function keywords,

70
00:02:42,90 --> 00:02:48,00
and putting in the brackets,

71
00:02:48,00 --> 00:02:52,40
and we would want to say return function,

72
00:02:52,40 --> 00:02:58,50
and then put this console.log inside the brackets as well.

73
00:02:58,50 --> 00:02:59,50
You might find that as you're

74
00:02:59,50 --> 00:03:01,00
learning functional programming,

75
00:03:01,00 --> 00:03:03,60
it helps you to write your functions like this first,

76
00:03:03,60 --> 00:03:06,90
and then reduce them down to the arrow function notation.

77
00:03:06,90 --> 00:03:08,30
Pretty soon, you'll get the hang of it,

78
00:03:08,30 --> 00:03:11,60
and be writing functions like this with minimal effort.

79
00:03:11,60 --> 00:03:13,30
Now that we have a slightly better idea

80
00:03:13,30 --> 00:03:15,90
of how this whole returning functions thing works,

81
00:03:15,90 --> 00:03:18,20
let's look at a more useful example.

82
00:03:18,20 --> 00:03:19,50
First, let's say that we have a program

83
00:03:19,50 --> 00:03:21,60
with three functions;

84
00:03:21,60 --> 00:03:24,80
double, const double,

85
00:03:24,80 --> 00:03:28,40
which simply takes a number and doubles it,

86
00:03:28,40 --> 00:03:32,80
const triple, which takes a number and triples it

87
00:03:32,80 --> 00:03:39,20
and quadruple, which takes a number and quadruples it.

88
00:03:39,20 --> 00:03:40,50
Now, the first thing that you'll notice

89
00:03:40,50 --> 00:03:41,70
about these functions,

90
00:03:41,70 --> 00:03:44,40
is that our code has quite a bit of repetition in it,

91
00:03:44,40 --> 00:03:46,20
and this is usually a sign that our code

92
00:03:46,20 --> 00:03:49,60
could use some refactoring to increase code reuse.

93
00:03:49,60 --> 00:03:51,10
So what if, instead of having

94
00:03:51,10 --> 00:03:52,80
to define several different functions

95
00:03:52,80 --> 00:03:55,10
with only slightly different definitions,

96
00:03:55,10 --> 00:03:56,10
we could have a function that

97
00:03:56,10 --> 00:03:59,00
created these variations for us.

98
00:03:59,00 --> 00:04:02,40
Well, this sounds like a job for first-class functions,

99
00:04:02,40 --> 00:04:04,10
what we're picturing here is a function

100
00:04:04,10 --> 00:04:06,40
that takes a number as an argument,

101
00:04:06,40 --> 00:04:10,00
and we'll call this function createMultiplier.

102
00:04:10,00 --> 00:04:13,80
And it takes a number as an argument, we'll call it y,

103
00:04:13,80 --> 00:04:15,00
and then returns a function that

104
00:04:15,00 --> 00:04:17,70
also takes a number as an argument,

105
00:04:17,70 --> 00:04:19,20
and multiplies its own argument

106
00:04:19,20 --> 00:04:21,10
by whatever number we originally passed

107
00:04:21,10 --> 00:04:24,50
into the outer function so x times y.

108
00:04:24,50 --> 00:04:27,50
And now that we have our createMultiplier function,

109
00:04:27,50 --> 00:04:30,10
we can use it to replace our existing implementations,

110
00:04:30,10 --> 00:04:32,70
of our double, triple and quadruple functions,

111
00:04:32,70 --> 00:04:34,40
and that will look like this,

112
00:04:34,40 --> 00:04:38,00
let's move these down here,

113
00:04:38,00 --> 00:04:45,10
and we can replace double with createMultiplier,

114
00:04:45,10 --> 00:04:47,60
with the value two.

115
00:04:47,60 --> 00:04:51,70
And we can replace triple with createMultiplier,

116
00:04:51,70 --> 00:04:53,60
with the argument three.

117
00:04:53,60 --> 00:04:55,90
And finally, we can replace quadruple

118
00:04:55,90 --> 00:05:00,30
with createMultiplier and passing four.

119
00:05:00,30 --> 00:05:02,20
And we can then call these functions

120
00:05:02,20 --> 00:05:04,20
and it'll return exactly the same thing

121
00:05:04,20 --> 00:05:06,70
as our original function definitions,

122
00:05:06,70 --> 00:05:07,80
except now, we're doing it

123
00:05:07,80 --> 00:05:10,50
in a more functional, reusable way.

124
00:05:10,50 --> 00:05:12,00
Now the functions that we replaced here

125
00:05:12,00 --> 00:05:13,80
were very small and simple,

126
00:05:13,80 --> 00:05:15,70
but the same technique can also be used

127
00:05:15,70 --> 00:05:17,30
to replace repeated code,

128
00:05:17,30 --> 00:05:19,10
even when dealing with larger functions

129
00:05:19,10 --> 00:05:21,30
that use much more complex logic.

130
00:05:21,30 --> 00:05:24,00
We'll see some examples of this later on in the course.

