1
00:00:00,50 --> 00:00:01,70
- [Instructor] Now that we've seen a few ways

2
00:00:01,70 --> 00:00:03,40
that we can treat functions in the same way

3
00:00:03,40 --> 00:00:05,20
as other types such as numbers, strings,

4
00:00:05,20 --> 00:00:07,70
and objects, the next step is to look at how

5
00:00:07,70 --> 00:00:11,20
we can pass functions as arguments to other functions.

6
00:00:11,20 --> 00:00:13,40
Up until now you've probably been used to passing

7
00:00:13,40 --> 00:00:15,10
arguments into functions with the purpose

8
00:00:15,10 --> 00:00:18,20
of specifying what the data is that we're operating on.

9
00:00:18,20 --> 00:00:20,80
If we pass two numbers into a function called add

10
00:00:20,80 --> 00:00:23,70
or subtract, for example, we're specifying

11
00:00:23,70 --> 00:00:26,90
the two numbers that we want to add or subtract.

12
00:00:26,90 --> 00:00:28,90
But what if instead of passing arguments

13
00:00:28,90 --> 00:00:31,90
into our function to specify what the data is

14
00:00:31,90 --> 00:00:33,70
we could pass in arguments to specify

15
00:00:33,70 --> 00:00:35,80
what was done to our data.

16
00:00:35,80 --> 00:00:37,20
This is one of the important things

17
00:00:37,20 --> 00:00:40,00
that first-class functions let us do.

18
00:00:40,00 --> 00:00:42,00
To illustrate this point let's take a look

19
00:00:42,00 --> 00:00:44,80
at two very simple functions, add and subtract

20
00:00:44,80 --> 00:00:48,10
defined of course using arrow function syntax.

21
00:00:48,10 --> 00:00:50,00
Each of these functions takes two arguments

22
00:00:50,00 --> 00:00:52,90
and then either adds or subtracts the two numbers.

23
00:00:52,90 --> 00:00:56,90
For example, if we call add with two and three

24
00:00:56,90 --> 00:00:58,60
it'll give us five.

25
00:00:58,60 --> 00:01:01,30
And if we call subtract with two numbers,

26
00:01:01,30 --> 00:01:07,80
say nine and three, it'll give us six.

27
00:01:07,80 --> 00:01:09,90
But what if we had another function where instead

28
00:01:09,90 --> 00:01:12,90
of passing in the two numbers into this function

29
00:01:12,90 --> 00:01:15,90
the numbers were fixed, to say two and three.

30
00:01:15,90 --> 00:01:20,90
So we'll create this function called combine two and three.

31
00:01:20,90 --> 00:01:23,30
And then we pass in a function that specifies

32
00:01:23,30 --> 00:01:25,70
how those numbers should be combined.

33
00:01:25,70 --> 00:01:27,80
For example, should they be added, subtracted,

34
00:01:27,80 --> 00:01:30,40
multiplied, averaged, et cetera?

35
00:01:30,40 --> 00:01:33,50
So we'll have our function argument here.

36
00:01:33,50 --> 00:01:35,10
And then inside our combined two

37
00:01:35,10 --> 00:01:38,90
and three function, we call the function argument

38
00:01:38,90 --> 00:01:42,40
on two fixed numbers, we'll say two and three,

39
00:01:42,40 --> 00:01:44,60
and then return the result, which is done

40
00:01:44,60 --> 00:01:47,70
automatically since this is an arrow function.

41
00:01:47,70 --> 00:01:50,20
Now what we can do is take our add or subtract

42
00:01:50,20 --> 00:01:52,50
functions and pass them into our combined two

43
00:01:52,50 --> 00:01:55,30
and three function like this.

44
00:01:55,30 --> 00:02:05,10
Combine two and three, add or subtract.

45
00:02:05,10 --> 00:02:07,50
Now when we pass the add function, we end up

46
00:02:07,50 --> 00:02:13,00
with two plus three, which of course is five.

47
00:02:13,00 --> 00:02:15,10
And when we pass the subtract function

48
00:02:15,10 --> 00:02:20,60
we end up with two minus three, which is negative one.

49
00:02:20,60 --> 00:02:22,30
And of course we can pass in pretty much

50
00:02:22,30 --> 00:02:24,60
any other function that we want that combines

51
00:02:24,60 --> 00:02:27,10
two numbers and returns a number, and pass that

52
00:02:27,10 --> 00:02:29,10
into our function as well.

53
00:02:29,10 --> 00:02:31,30
For example, we can pass in JavaScript's built-in

54
00:02:31,30 --> 00:02:34,80
math dot min or math dot max functions like this.

55
00:02:34,80 --> 00:02:38,70
Combine two and three, math dot max.

56
00:02:38,70 --> 00:02:40,60
And this would give us the maximum of two

57
00:02:40,60 --> 00:02:43,90
or three, which would be three.

58
00:02:43,90 --> 00:02:45,90
Now, so far we've been defining our functions

59
00:02:45,90 --> 00:02:48,50
separately before passing them as arguments.

60
00:02:48,50 --> 00:02:50,50
While for the sake of readability this is often

61
00:02:50,50 --> 00:02:53,00
considered a good practice, we can also pass

62
00:02:53,00 --> 00:02:55,30
in anonymous functions, which are basically

63
00:02:55,30 --> 00:02:57,80
just unnamed functions that we define on the spot

64
00:02:57,80 --> 00:02:58,90
as we need them.

65
00:02:58,90 --> 00:03:00,90
Using this existing code as an example,

66
00:03:00,90 --> 00:03:02,70
instead of creating a new named function

67
00:03:02,70 --> 00:03:05,30
for each type of combination we could simply

68
00:03:05,30 --> 00:03:07,30
define it in place.

69
00:03:07,30 --> 00:03:09,60
So for example, instead of explicitly defining

70
00:03:09,60 --> 00:03:12,20
and passing add, we could just say combine

71
00:03:12,20 --> 00:03:16,00
two and three and then pass an anonymous function,

72
00:03:16,00 --> 00:03:18,50
which takes two numbers, x and y,

73
00:03:18,50 --> 00:03:22,30
and adds them together like that.

74
00:03:22,30 --> 00:03:24,20
A lot of times anonymous functions can be more

75
00:03:24,20 --> 00:03:26,30
convenient to write since there can be some

76
00:03:26,30 --> 00:03:28,40
special cases where our desired functionality

77
00:03:28,40 --> 00:03:30,20
is very specific and it's not really worth

78
00:03:30,20 --> 00:03:32,00
creating a named function for it.

