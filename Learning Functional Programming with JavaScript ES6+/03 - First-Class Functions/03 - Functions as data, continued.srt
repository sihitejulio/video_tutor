1
00:00:00,50 --> 00:00:01,70
- [Instructor] In this video we're going to look

2
00:00:01,70 --> 00:00:03,60
at another example of an interesting thing

3
00:00:03,60 --> 00:00:06,20
that can be done with first class functions.

4
00:00:06,20 --> 00:00:08,00
Just as we can create an array of numbers,

5
00:00:08,00 --> 00:00:10,50
an array of strings, or an array of objects.

6
00:00:10,50 --> 00:00:12,30
Because functions in JavaScript are first class

7
00:00:12,30 --> 00:00:14,70
we can also create an array of functions.

8
00:00:14,70 --> 00:00:17,80
And why would we want to do something like that exactly?

9
00:00:17,80 --> 00:00:19,60
Well let's say that we have a few different functions

10
00:00:19,60 --> 00:00:22,80
that we've created with our nice es6 arrow syntax.

11
00:00:22,80 --> 00:00:24,40
All of which take a single argument

12
00:00:24,40 --> 00:00:27,10
and make some sort of change to that argument.

13
00:00:27,10 --> 00:00:28,90
Like doubling it or subtracting from it

14
00:00:28,90 --> 00:00:32,10
or tripling it or adding five to it.

15
00:00:32,10 --> 00:00:33,70
Now let's say that we wanted to get the result

16
00:00:33,70 --> 00:00:36,90
of applying all of these functions to some number.

17
00:00:36,90 --> 00:00:39,40
The mechanical way of doing this would be to say something

18
00:00:39,40 --> 00:00:44,90
like const my number equals 42.

19
00:00:44,90 --> 00:00:47,60
And then we would apply our functions one by one

20
00:00:47,60 --> 00:00:48,70
to this number.

21
00:00:48,70 --> 00:00:56,10
So const doubled equals double my number.

22
00:00:56,10 --> 00:01:03,40
Const minus one equals subtract one called on doubled

23
00:01:03,40 --> 00:01:08,60
the result of our last operation and so on.

24
00:01:08,60 --> 00:01:10,00
But what if we have hundreds or even thousands

25
00:01:10,00 --> 00:01:11,90
of these functions?

26
00:01:11,90 --> 00:01:14,70
Well what we can do is define an array

27
00:01:14,70 --> 00:01:17,50
with all of these functions in it like this.

28
00:01:17,50 --> 00:01:22,90
We'll say const functions array equals

29
00:01:22,90 --> 00:01:25,90
and then we'll put each of these functions in this array;

30
00:01:25,90 --> 00:01:34,70
double, subtract one, triple, and add five.

31
00:01:34,70 --> 00:01:36,20
The first thing to notice is that

32
00:01:36,20 --> 00:01:38,60
when we put our functions into the array

33
00:01:38,60 --> 00:01:40,40
we don't put parenthesis after them

34
00:01:40,40 --> 00:01:42,80
as you might be used to seeing.

35
00:01:42,80 --> 00:01:44,00
This is something you'll get used to

36
00:01:44,00 --> 00:01:46,40
while mastering functional programming.

37
00:01:46,40 --> 00:01:48,20
The thing to remember is that when we put parenthesis

38
00:01:48,20 --> 00:01:50,70
after a function we're referring to the result

39
00:01:50,70 --> 00:01:52,40
of calling the function

40
00:01:52,40 --> 00:01:54,60
instead of the function itself.

41
00:01:54,60 --> 00:01:56,50
When we want to refer to the function itself

42
00:01:56,50 --> 00:01:59,40
as in this array we simply use the function's name

43
00:01:59,40 --> 00:02:02,60
without any parenthesis or arguments after it.

44
00:02:02,60 --> 00:02:04,40
So we now have an array of functions

45
00:02:04,40 --> 00:02:05,90
that we just want to apply

46
00:02:05,90 --> 00:02:07,90
one after the other to some number.

47
00:02:07,90 --> 00:02:09,60
The solution now is pretty simple.

48
00:02:09,60 --> 00:02:11,00
We just loop through the functions and apply

49
00:02:11,00 --> 00:02:13,10
each one to our number.

50
00:02:13,10 --> 00:02:15,30
Now keep in mind the code we're going to write for this

51
00:02:15,30 --> 00:02:18,50
isn't the most functional code since it involves mutation.

52
00:02:18,50 --> 00:02:20,90
But in an effort not to give you too much too soon

53
00:02:20,90 --> 00:02:24,60
we're going to use a bit of procedural code to prove a point.

54
00:02:24,60 --> 00:02:25,80
Later on in the course you'll learn

55
00:02:25,80 --> 00:02:27,50
a more functional version of this code,

56
00:02:27,50 --> 00:02:29,20
but for now it's good enough.

57
00:02:29,20 --> 00:02:31,00
So first what we'll do is assign the number

58
00:02:31,00 --> 00:02:33,30
we want to work with to a variable.

59
00:02:33,30 --> 00:02:39,30
So we'll say var number equals 42.

60
00:02:39,30 --> 00:02:40,70
And now that we have the number that

61
00:02:40,70 --> 00:02:42,70
we want to start off with we're going to use

62
00:02:42,70 --> 00:02:45,60
for each to loop over each function in our array,

63
00:02:45,60 --> 00:02:48,20
and apply it to our variable number.

64
00:02:48,20 --> 00:02:53,60
So functions array.forEach.

65
00:02:53,60 --> 00:02:56,80
And then this argument will represent each of our functions.

66
00:02:56,80 --> 00:03:03,50
And we'll say number equals the function called on number.

67
00:03:03,50 --> 00:03:05,80
So after this line runs our number variable

68
00:03:05,80 --> 00:03:06,70
should hold our result.

69
00:03:06,70 --> 00:03:08,50
Let's print that result using

70
00:03:08,50 --> 00:03:11,30
console.log and then run the program.

71
00:03:11,30 --> 00:03:16,10
Console.log number.

72
00:03:16,10 --> 00:03:17,20
And then we'll run our program

73
00:03:17,20 --> 00:03:21,00
by typing npx babel dash node,

74
00:03:21,00 --> 00:03:22,50
and then the path to our file.

75
00:03:22,50 --> 00:03:26,80
Which in this case is Ch zero two slash

76
00:03:26,80 --> 00:03:30,20
zero two underscore zero three slash

77
00:03:30,20 --> 00:03:35,20
start slash examples.js.

78
00:03:35,20 --> 00:03:36,00
And there we see our result.

79
00:03:36,00 --> 00:03:38,00
If you want you can mess

80
00:03:38,00 --> 00:03:39,60
around with the order of these functions

81
00:03:39,60 --> 00:03:42,00
and see how that changes things.

82
00:03:42,00 --> 00:03:43,60
You can also define your own functions

83
00:03:43,60 --> 00:03:44,50
and add them to the array.

84
00:03:44,50 --> 00:03:49,80
with as well that might shed some light of the nature

85
00:03:49,80 --> 00:03:51,50
of first class functions is to

86
00:03:51,50 --> 00:03:53,10
put JavaScript's built in math

87
00:03:53,10 --> 00:03:55,20
functions into our function array.

88
00:03:55,20 --> 00:03:57,50
Let's see what happens if we add the square root function

89
00:03:57,50 --> 00:04:02,20
math.sqrt to our functions array.

90
00:04:02,20 --> 00:04:04,00
Remember to pass it without the parenthesis

91
00:04:04,00 --> 00:04:05,80
since we want to pass the function itself

92
00:04:05,80 --> 00:04:08,30
and not the result of the function.

93
00:04:08,30 --> 00:04:10,20
Let's save our code and then run

94
00:04:10,20 --> 00:04:13,40
it again to see what happens.

95
00:04:13,40 --> 00:04:15,70
And we see that we have our result again.

96
00:04:15,70 --> 00:04:17,10
The point here is that we're not just

97
00:04:17,10 --> 00:04:19,20
limited to functions that we define.

98
00:04:19,20 --> 00:04:21,00
Any function that's defined anywhere can

99
00:04:21,00 --> 00:04:21,90
be used in this way.

100
00:04:21,90 --> 00:04:21,90
Whether it's a built in function,

101
00:04:21,90 --> 00:04:25,20
a function from another library,

102
00:04:25,20 --> 00:04:27,00
or one of your own functions.

