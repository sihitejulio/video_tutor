1
00:00:00,00 --> 00:00:01,80
- [Instructor] In a previous video we talked

2
00:00:01,80 --> 00:00:04,90
about how in java script, it's possible to return functions

3
00:00:04,90 --> 00:00:06,10
from other functions.

4
00:00:06,10 --> 00:00:08,30
And saw some possible applications of doing this

5
00:00:08,30 --> 00:00:09,80
in our code base.

6
00:00:09,80 --> 00:00:11,90
Returning functions from other functions leads us

7
00:00:11,90 --> 00:00:14,70
to another very important concept in java script.

8
00:00:14,70 --> 00:00:17,20
And this is something called closure.

9
00:00:17,20 --> 00:00:19,30
So what is closure exactly?

10
00:00:19,30 --> 00:00:22,00
Well basically, closure means that when we define a function

11
00:00:22,00 --> 00:00:23,60
that returns another function,

12
00:00:23,60 --> 00:00:26,70
the function that we returned still has access

13
00:00:26,70 --> 00:00:29,90
to the internally scope of the function that returned it.

14
00:00:29,90 --> 00:00:31,70
In this case, the function that we returned

15
00:00:31,70 --> 00:00:34,90
still has access to the constant x.

16
00:00:34,90 --> 00:00:37,60
Even after we return it.

17
00:00:37,60 --> 00:00:39,40
To see what I mean a little better,

18
00:00:39,40 --> 00:00:42,60
let's code out an example that demonstrates the closure.

19
00:00:42,60 --> 00:00:45,50
First let's create a function called createPrinter.

20
00:00:45,50 --> 00:00:48,60
Const, createPrinter.

21
00:00:48,60 --> 00:00:51,00
And this function won't take any arguments.

22
00:00:51,00 --> 00:00:52,50
And for illustrative purposes,

23
00:00:52,50 --> 00:00:54,50
let's make a function body with more that one line

24
00:00:54,50 --> 00:00:56,50
using the brackets.

25
00:00:56,50 --> 00:00:58,50
So inside this function body,

26
00:00:58,50 --> 00:01:01,50
let's define a constant called myFavoriteNumber.

27
00:01:01,50 --> 00:01:02,90
And set it equal to what ever you want.

28
00:01:02,90 --> 00:01:05,70
I'm going to use 42.

29
00:01:05,70 --> 00:01:08,20
Now we're going to return a function

30
00:01:08,20 --> 00:01:10,30
that also doesn't take any arguments.

31
00:01:10,30 --> 00:01:12,60
And prints this number to the console.

32
00:01:12,60 --> 00:01:15,50
So we'll say console.log.

33
00:01:15,50 --> 00:01:17,20
And I'm going to use backticks here,

34
00:01:17,20 --> 00:01:19,40
instead of the normal single quotes.

35
00:01:19,40 --> 00:01:23,30
We'll say my favorite number

36
00:01:23,30 --> 00:01:26,80
is dollar sign, brackets,

37
00:01:26,80 --> 00:01:33,60
myFavoriteNumber, the constant.

38
00:01:33,60 --> 00:01:37,70
And then we'll use this by saying, const printer

39
00:01:37,70 --> 00:01:39,80
equals createPrinter.

40
00:01:39,80 --> 00:01:43,30
And then calling printer.

41
00:01:43,30 --> 00:01:47,50
And now if we run our code by typing npx babel-node

42
00:01:47,50 --> 00:01:48,80
and then the path to our file.

43
00:01:48,80 --> 00:01:59,60
In this case, Ch02/02_06/Start/examples.js.

44
00:01:59,60 --> 00:02:01,70
We see that it prints it to the console.

45
00:02:01,70 --> 00:02:03,30
Now at first glance this might not seem

46
00:02:03,30 --> 00:02:04,50
like anything special.

47
00:02:04,50 --> 00:02:06,20
But let's look closer.

48
00:02:06,20 --> 00:02:08,00
When we return our printer function

49
00:02:08,00 --> 00:02:09,30
by calling createPrinter,

50
00:02:09,30 --> 00:02:11,80
and then call the printer function.

51
00:02:11,80 --> 00:02:14,30
We can see that we somehow still have access

52
00:02:14,30 --> 00:02:16,50
to the constant myFavoriteNumber.

53
00:02:16,50 --> 00:02:19,50
That we defined inside the createPrinter function.

54
00:02:19,50 --> 00:02:22,30
Even though if we were to try and use this constant directly

55
00:02:22,30 --> 00:02:27,80
by typing console.log, myFavoriteNumber.

56
00:02:27,80 --> 00:02:29,10
We would get an error saying

57
00:02:29,10 --> 00:02:31,40
that myFavoriteNumber isn't defined.

58
00:02:31,40 --> 00:02:33,80
And this is exactly what closure is.

59
00:02:33,80 --> 00:02:34,90
When we return a function,

60
00:02:34,90 --> 00:02:38,20
it still has access to the scope that it was returned from.

61
00:02:38,20 --> 00:02:40,50
This is a very important concept.

62
00:02:40,50 --> 00:02:41,80
Since if it didn't exist,

63
00:02:41,80 --> 00:02:45,20
returning functions would be pretty useless in most cases.

64
00:02:45,20 --> 00:02:46,50
For example, if it didn't exist,

65
00:02:46,50 --> 00:02:48,60
when we called printer here,

66
00:02:48,60 --> 00:02:54,80
it would just log out myFavoriteNumber is nul or undefined.

67
00:02:54,80 --> 00:02:57,00
Now in order to illustrate closure further,

68
00:02:57,00 --> 00:02:59,10
let's look at an interesting example.

69
00:02:59,10 --> 00:03:00,80
I'm going to show you how to use closure

70
00:03:00,80 --> 00:03:04,00
to implement real private variables in java script.

71
00:03:04,00 --> 00:03:05,30
This may seem a little ironic

72
00:03:05,30 --> 00:03:07,20
and of course devoted to functional programing.

73
00:03:07,20 --> 00:03:09,40
Since functional programing makes every effort

74
00:03:09,40 --> 00:03:11,50
to avoid using these private variables.

75
00:03:11,50 --> 00:03:13,90
Since they usually mean our code is stateful.

76
00:03:13,90 --> 00:03:15,60
But I think this is an excellent example

77
00:03:15,60 --> 00:03:16,90
of closure in java script.

78
00:03:16,90 --> 00:03:18,40
And will help you wrap your head around

79
00:03:18,40 --> 00:03:20,00
first class functions a little better.

