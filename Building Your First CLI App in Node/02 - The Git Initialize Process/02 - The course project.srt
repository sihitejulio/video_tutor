1
00:00:00,50 --> 00:00:02,80
- [Instructor] For this course, you will be making a project

2
00:00:02,80 --> 00:00:05,90
that automates the seven git init steps.

3
00:00:05,90 --> 00:00:09,10
Not only will you learn how to a create command-line app,

4
00:00:09,10 --> 00:00:10,30
but by the end of this course,

5
00:00:10,30 --> 00:00:11,60
you'll have a handy tool

6
00:00:11,60 --> 00:00:13,60
that will prompt you through all of the stages

7
00:00:13,60 --> 00:00:16,60
to create and deploy your future Git projects.

8
00:00:16,60 --> 00:00:18,80
Never forget a step again.

9
00:00:18,80 --> 00:00:20,80
I'm calling our little program Musette.

10
00:00:20,80 --> 00:00:23,30
And she will, create a Git repository

11
00:00:23,30 --> 00:00:25,00
in the folder you're currently in,

12
00:00:25,00 --> 00:00:27,20
create a remote repository.

13
00:00:27,20 --> 00:00:30,00
For this tutorial, we'll be using GitHub for our remote.

14
00:00:30,00 --> 00:00:33,50
And then of course, adding this newly created repository

15
00:00:33,50 --> 00:00:36,50
as a remote to your project locally.

16
00:00:36,50 --> 00:00:39,40
Then Musette will provide a simple interactive wizard,

17
00:00:39,40 --> 00:00:41,20
just like those install windows,

18
00:00:41,20 --> 00:00:43,50
for creating a .gitignore file.

19
00:00:43,50 --> 00:00:46,10
Afterwards, she'll add the contents of the folder

20
00:00:46,10 --> 00:00:49,40
and push it all to the remote repository.

21
00:00:49,40 --> 00:00:51,70
It may not save you hours of your life,

22
00:00:51,70 --> 00:00:54,10
but it will remove some of the initial friction

23
00:00:54,10 --> 00:00:55,70
when starting a new project.

24
00:00:55,70 --> 00:00:59,00
And we all want life to go as smoothly as possible, right?

