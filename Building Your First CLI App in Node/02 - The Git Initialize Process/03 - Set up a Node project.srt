1
00:00:00,50 --> 00:00:02,50
- [Narrator] Before we jump in and get building,

2
00:00:02,50 --> 00:00:04,30
let's take a look at why Node.js

3
00:00:04,30 --> 00:00:07,80
is the best tool to build this command line application.

4
00:00:07,80 --> 00:00:09,40
The first advantage is that,

5
00:00:09,40 --> 00:00:11,00
if you're here in this course,

6
00:00:11,00 --> 00:00:12,70
you've probably heard about Node,

7
00:00:12,70 --> 00:00:15,40
or used it a bit in one of your own projects already.

8
00:00:15,40 --> 00:00:16,60
That's great.

9
00:00:16,60 --> 00:00:18,60
If not, there's no worries.

10
00:00:18,60 --> 00:00:21,20
If you're here, you're definitely familiar with JavaScript,

11
00:00:21,20 --> 00:00:25,50
and Node is a framework written, entirely in JavaScript.

12
00:00:25,50 --> 00:00:28,20
Another advantage we'll see as we work through this project,

13
00:00:28,20 --> 00:00:30,50
is that the Node.js ecosystem of packages

14
00:00:30,50 --> 00:00:33,00
is strong, active, and growing.

15
00:00:33,00 --> 00:00:35,40
Which means there are literally hundreds of thousands

16
00:00:35,40 --> 00:00:37,00
of different packages available,

17
00:00:37,00 --> 00:00:39,30
for all sorts of things you want to build.

18
00:00:39,30 --> 00:00:42,00
In our case, we'll be most particularly interested

19
00:00:42,00 --> 00:00:43,60
in packages designed to help build

20
00:00:43,60 --> 00:00:45,90
command line tools like ours.

21
00:00:45,90 --> 00:00:49,90
Finally, we can use npm, Node Package Manager,

22
00:00:49,90 --> 00:00:52,60
to manage any of our dependencies.

23
00:00:52,60 --> 00:00:55,80
This means, it will be operating system agnostic.

24
00:00:55,80 --> 00:00:58,30
Which is very different from other package managers,

25
00:00:58,30 --> 00:01:02,70
like Aptitude, YUM, or my go to, Homebrew.

26
00:01:02,70 --> 00:01:05,40
Before we can get started installing our dependencies,

27
00:01:05,40 --> 00:01:08,40
we first need a project to install them in.

28
00:01:08,40 --> 00:01:09,90
So if we go to our terminal,

29
00:01:09,90 --> 00:01:12,70
we can create a new directory for the project.

30
00:01:12,70 --> 00:01:14,70
And if we take a look,

31
00:01:14,70 --> 00:01:16,80
you can see that we have a new directory here.

32
00:01:16,80 --> 00:01:18,80
If we go inside of that,

33
00:01:18,80 --> 00:01:21,20
and take a look inside of the directory,

34
00:01:21,20 --> 00:01:23,50
you can see that there's nothing in there yet.

35
00:01:23,50 --> 00:01:25,10
The other thing we're going to have to do,

36
00:01:25,10 --> 00:01:27,50
is to create a new package.json file

37
00:01:27,50 --> 00:01:30,20
for our packages to go into.

38
00:01:30,20 --> 00:01:33,10
So the way we're going to do this is by running npm init,

39
00:01:33,10 --> 00:01:35,10
and we're just going to follow this simple wizard,

40
00:01:35,10 --> 00:01:37,00
and fill in the information here.

41
00:01:37,00 --> 00:01:39,10
What it's doing with the package name in brackets,

42
00:01:39,10 --> 00:01:41,90
is it's saying, this is the default package name

43
00:01:41,90 --> 00:01:43,20
that we would like to give it,

44
00:01:43,20 --> 00:01:45,40
based on what you named your folder.

45
00:01:45,40 --> 00:01:46,80
So I'm just going to hit enter,

46
00:01:46,80 --> 00:01:49,70
because I would like my package name to be called musette.

47
00:01:49,70 --> 00:01:51,50
Again, they're setting a default version,

48
00:01:51,50 --> 00:01:53,10
so you can just hit enter here.

49
00:01:53,10 --> 00:01:55,60
You can optionally put in a description.

50
00:01:55,60 --> 00:02:00,10
So I'm going to say, "This is a cool dev tool

51
00:02:00,10 --> 00:02:07,30
to help automate the initialize phase of your git workflow."

52
00:02:07,30 --> 00:02:09,10
And then I'm going to hit enter again.

53
00:02:09,10 --> 00:02:12,10
The entry point is where your app starts from,

54
00:02:12,10 --> 00:02:14,10
and our app will start from the index.js,

55
00:02:14,10 --> 00:02:16,10
so we can just hit enter again.

56
00:02:16,10 --> 00:02:18,20
We don't need a test command

57
00:02:18,20 --> 00:02:20,80
and we don't have a git repository for it yet.

58
00:02:20,80 --> 00:02:22,00
You could put in some keywords

59
00:02:22,00 --> 00:02:24,50
so other devs could find it in the registry.

60
00:02:24,50 --> 00:02:28,80
So you could have git, node, cli.

61
00:02:28,80 --> 00:02:32,00
Here, you want to put in your name, as the author.

62
00:02:32,00 --> 00:02:34,10
And then again, it chooses a default license.

63
00:02:34,10 --> 00:02:37,20
I'm just going to let it use ISC because it wants to.

64
00:02:37,20 --> 00:02:39,60
But you could choose another license if you wanted to.

65
00:02:39,60 --> 00:02:41,10
So I'm just going to hit enter,

66
00:02:41,10 --> 00:02:42,50
and what it's doing here,

67
00:02:42,50 --> 00:02:46,00
is returning all of the information we just entered.

68
00:02:46,00 --> 00:02:47,50
So, my information looks good,

69
00:02:47,50 --> 00:02:51,10
so I'm going to type yes, and then hit enter.

70
00:02:51,10 --> 00:02:53,70
Oh, looks like we just need to hit enter.

71
00:02:53,70 --> 00:02:56,10
Of course, you don't have to call yours musette,

72
00:02:56,10 --> 00:02:58,80
but I will refer to the program as such throughout.

73
00:02:58,80 --> 00:03:01,00
It's inspired by a favorite desk buddy of mine.

74
00:03:01,00 --> 00:03:05,40
If we just go into the directory that we've just created.

75
00:03:05,40 --> 00:03:08,70
You'll be able to see really clearly, this is our directory.

76
00:03:08,70 --> 00:03:10,40
And then if I double-click and go inside,

77
00:03:10,40 --> 00:03:12,90
we have this package.json file in here.

78
00:03:12,90 --> 00:03:15,30
And if we double-click and take a look,

79
00:03:15,30 --> 00:03:16,90
You'll see, that's all the information

80
00:03:16,90 --> 00:03:18,90
that you just entered from the terminal.

81
00:03:18,90 --> 00:03:21,70
We're going to add a little bit more information to it later.

82
00:03:21,70 --> 00:03:24,30
But it's good to know that this package.json

83
00:03:24,30 --> 00:03:26,40
is created by the npm init.

84
00:03:26,40 --> 00:03:27,60
You can think of this file here

85
00:03:27,60 --> 00:03:29,90
as a sort of blueprint of your project.

86
00:03:29,90 --> 00:03:32,40
As we install dependencies in the next video,

87
00:03:32,40 --> 00:03:34,20
the information about these will also

88
00:03:34,20 --> 00:03:37,00
be automatically added to your package.json file.

