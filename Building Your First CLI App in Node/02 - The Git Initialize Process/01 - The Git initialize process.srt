1
00:00:00,90 --> 00:00:03,10
- Git allows you to have version control

2
00:00:03,10 --> 00:00:05,50
in your projects, both locally and remote,

3
00:00:05,50 --> 00:00:08,40
with your own individual work and with teams.

4
00:00:08,40 --> 00:00:11,40
It's a core tool in the modern dev toolbox.

5
00:00:11,40 --> 00:00:14,40
Online Git hosts like GitHub and Bitbucket

6
00:00:14,40 --> 00:00:17,00
have allowed developer collaboration,

7
00:00:17,00 --> 00:00:20,40
as well as the open source community, to flourish.

8
00:00:20,40 --> 00:00:22,50
These days the first thing you probably do

9
00:00:22,50 --> 00:00:26,00
with any project is to create a Git repository for it.

10
00:00:26,00 --> 00:00:28,40
You can think of a Git repo as a box

11
00:00:28,40 --> 00:00:31,90
where you store a draft of your code on your local machine.

12
00:00:31,90 --> 00:00:34,80
Git init creates this box for you.

13
00:00:34,80 --> 00:00:36,50
If you've pushed a couple of projects

14
00:00:36,50 --> 00:00:37,90
through a remote code host,

15
00:00:37,90 --> 00:00:41,90
git init initializes, or starts the creation of,

16
00:00:41,90 --> 00:00:45,80
a Git repository in your current directory.

17
00:00:45,80 --> 00:00:48,00
To share it with others you will package your code

18
00:00:48,00 --> 00:00:50,90
and send a copy of it to an online storage,

19
00:00:50,90 --> 00:00:53,50
like GitHub or Bitbucket.

20
00:00:53,50 --> 00:00:55,40
When you stop to think about it though,

21
00:00:55,40 --> 00:00:57,00
that's not the only thing you need to do

22
00:00:57,00 --> 00:00:59,00
to get your project pushed online.

23
00:00:59,00 --> 00:01:01,80
Git init is only one of a number of steps

24
00:01:01,80 --> 00:01:03,80
involved in the process.

25
00:01:03,80 --> 00:01:06,60
Overall, the typical Git initialize process

26
00:01:06,60 --> 00:01:08,90
is made up of seven key steps.

27
00:01:08,90 --> 00:01:12,30
First, initialize the local repository.

28
00:01:12,30 --> 00:01:14,80
This is like creating an open box.

29
00:01:14,80 --> 00:01:17,60
We would use the git init command here.

30
00:01:17,60 --> 00:01:20,20
Second, create a remote repository,

31
00:01:20,20 --> 00:01:23,70
which is like getting an address label for your box.

32
00:01:23,70 --> 00:01:28,00
Third, add the remote repository to your project

33
00:01:28,00 --> 00:01:29,30
and this would be like sticking

34
00:01:29,30 --> 00:01:31,50
the address label on the box.

35
00:01:31,50 --> 00:01:33,90
Fourth, we'll create a .git ignore file

36
00:01:33,90 --> 00:01:36,10
and that lets your box know which files

37
00:01:36,10 --> 00:01:38,00
should not be added.

38
00:01:38,00 --> 00:01:40,30
Fifth, add the rest of your project files

39
00:01:40,30 --> 00:01:44,40
and this lets your box know which files should be added.

40
00:01:44,40 --> 00:01:47,30
Sixth, you want to commit your initial set of files,

41
00:01:47,30 --> 00:01:50,00
which is like closing and sealing the box

42
00:01:50,00 --> 00:01:52,60
and seven, you want to push the files

43
00:01:52,60 --> 00:01:54,50
to your remote repository,

44
00:01:54,50 --> 00:01:57,10
which is like sending the box.

45
00:01:57,10 --> 00:02:00,30
Every single time you push code to your remote repo,

46
00:02:00,30 --> 00:02:02,80
you're repeating these seven steps.

47
00:02:02,80 --> 00:02:05,50
There may be a few others you add to the workflow over time,

48
00:02:05,50 --> 00:02:07,00
but no matter what you're doing

49
00:02:07,00 --> 00:02:09,80
you must do these seven steps to complete

50
00:02:09,80 --> 00:02:12,00
a Git initialize process.

