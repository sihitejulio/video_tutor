1
00:00:00,50 --> 00:00:01,90
- [Naomi] Congratulations!

2
00:00:01,90 --> 00:00:04,60
You've just built a complete console application

3
00:00:04,60 --> 00:00:07,20
top to bottom in Node.js.

4
00:00:07,20 --> 00:00:09,50
Not only that, but a console application

5
00:00:09,50 --> 00:00:13,00
that can take user prompts, connects to a restful API,

6
00:00:13,00 --> 00:00:15,20
and has a little bit of color in the UI.

7
00:00:15,20 --> 00:00:17,50
Sadly, that takes us to the end of this course.

8
00:00:17,50 --> 00:00:19,20
I hope you've enjoyed it.

9
00:00:19,20 --> 00:00:21,30
You've come a long way in a short time.

10
00:00:21,30 --> 00:00:22,70
If you're interested in learning more,

11
00:00:22,70 --> 00:00:25,10
I can always recommend O'Reilly series books

12
00:00:25,10 --> 00:00:27,90
like "Web Development with Node & Express."

13
00:00:27,90 --> 00:00:29,70
Of course, we're always adding more courses

14
00:00:29,70 --> 00:00:32,90
here in the LinkedIn Learning library, so check back soon.

15
00:00:32,90 --> 00:00:34,50
If you'd like to connect with me personally,

16
00:00:34,50 --> 00:00:36,40
I'm always happy to chat.

17
00:00:36,40 --> 00:00:38,20
Feel free to connect with me on Twitter

18
00:00:38,20 --> 00:00:40,00
or check out my website to learn more

19
00:00:40,00 --> 00:00:42,80
about upcoming courses and learning resources.

20
00:00:42,80 --> 00:00:44,60
Thanks again for sharing your time with me

21
00:00:44,60 --> 00:00:46,50
so that we could build out this Node.js

22
00:00:46,50 --> 00:00:48,30
command line tool together.

23
00:00:48,30 --> 00:00:49,40
I hope it serves you well

24
00:00:49,40 --> 00:00:52,00
as you continue building code projects.

