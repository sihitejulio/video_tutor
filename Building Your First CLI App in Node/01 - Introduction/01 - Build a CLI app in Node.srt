1
00:00:00,90 --> 00:00:02,70
- Have you ever wondered what it would take

2
00:00:02,70 --> 00:00:05,60
to development an app or tool in a CLI?

3
00:00:05,60 --> 00:00:07,70
Well I did too.

4
00:00:07,70 --> 00:00:10,10
When I first started out as a dev,

5
00:00:10,10 --> 00:00:13,40
CLI apps seemed like a pretty big black box mystery.

6
00:00:13,40 --> 00:00:15,40
I didn't know where to get started.

7
00:00:15,40 --> 00:00:18,10
So I'm going to show you how.

8
00:00:18,10 --> 00:00:21,40
We will leverage node.js and npm's huge ecosystem

9
00:00:21,40 --> 00:00:24,00
of packages and modules to connect

10
00:00:24,00 --> 00:00:27,30
to connect to GitHub's restful API using octokit.

11
00:00:27,30 --> 00:00:30,10
You'll create a practical command line developer tool

12
00:00:30,10 --> 00:00:33,60
for your self to automate your git init process.

13
00:00:33,60 --> 00:00:35,00
Once you build the project,

14
00:00:35,00 --> 00:00:37,40
you'll be able to use this tool over and over again,

15
00:00:37,40 --> 00:00:40,50
to startup and push your git projects.

16
00:00:40,50 --> 00:00:41,50
I'm Naomi Freeman Freeman

17
00:00:41,50 --> 00:00:44,60
and I'm a software engineer and programming instructor.

18
00:00:44,60 --> 00:00:47,30
I love sinking my teeth into backend logic

19
00:00:47,30 --> 00:00:48,70
and systems problems,

20
00:00:48,70 --> 00:00:50,90
and have developed an AI prototype

21
00:00:50,90 --> 00:00:53,70
as well as developed payment systems.

22
00:00:53,70 --> 00:00:56,40
If you're ready to take your node.js, JavaScript,

23
00:00:56,40 --> 00:00:58,90
and API building skills to the next level,

24
00:00:58,90 --> 00:01:01,30
then come join me in my LinkedIn Learning course,

25
00:01:01,30 --> 00:01:04,00
all about building a command line application.

