1
00:00:00,50 --> 00:00:02,70
- We're going to build a Node.js app

2
00:00:02,70 --> 00:00:05,90
using the command-line interface, or CLI.

3
00:00:05,90 --> 00:00:06,90
Before diving in,

4
00:00:06,90 --> 00:00:09,30
there's a few things you may want to know.

5
00:00:09,30 --> 00:00:13,20
I expect you've used some Node.js or JavaScript before,

6
00:00:13,20 --> 00:00:15,00
and you want to expand your knowledge

7
00:00:15,00 --> 00:00:17,70
through a more hands-on project.

8
00:00:17,70 --> 00:00:20,80
Don't worry though, this isn't an advanced course.

9
00:00:20,80 --> 00:00:21,90
If you're just starting out,

10
00:00:21,90 --> 00:00:24,10
you'll still be able to follow along.

11
00:00:24,10 --> 00:00:27,10
It just means that now you're ready to take the next step

12
00:00:27,10 --> 00:00:29,60
into building an app from the ground up.

13
00:00:29,60 --> 00:00:31,20
You'll get the most out of this course

14
00:00:31,20 --> 00:00:34,20
if you're pushed one or two projects to GitHub already,

15
00:00:34,20 --> 00:00:36,20
but this is not necessary.

16
00:00:36,20 --> 00:00:39,90
So if you're comfortable with Node.js or JavaScript basics,

17
00:00:39,90 --> 00:00:41,80
then you have a great foundation

18
00:00:41,80 --> 00:00:44,00
to take on the challenges in this course.

