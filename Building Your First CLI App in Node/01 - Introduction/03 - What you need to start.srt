1
00:00:00,50 --> 00:00:01,30
- [Instructor] You should be able

2
00:00:01,30 --> 00:00:02,80
to follow along with me in this course

3
00:00:02,80 --> 00:00:06,10
whether you're on a Mac, Windows or Linux machine.

4
00:00:06,10 --> 00:00:08,60
In this video I'll show you what you need on your computer

5
00:00:08,60 --> 00:00:09,80
to work with me.

6
00:00:09,80 --> 00:00:12,00
First, you'll need an IDE.

7
00:00:12,00 --> 00:00:15,50
I'm working with Sublime Text 2 in this course.

8
00:00:15,50 --> 00:00:18,70
You can download this at sublimetext.com

9
00:00:18,70 --> 00:00:21,70
or feel free to use your own favorite IDE.

10
00:00:21,70 --> 00:00:24,90
Other popular ones are Atom or Visual Studio.

11
00:00:24,90 --> 00:00:27,60
Next, you'll need Node.js and npm.

12
00:00:27,60 --> 00:00:29,60
In the exercise files for this video,

13
00:00:29,60 --> 00:00:32,70
I've included instructions for both Mac and Windows machines

14
00:00:32,70 --> 00:00:35,20
to help you install what you need.

15
00:00:35,20 --> 00:00:38,40
If you're working on a Mac, you'll install and run Homebrew,

16
00:00:38,40 --> 00:00:40,20
and once Homebrew's installed,

17
00:00:40,20 --> 00:00:43,70
you're going to run brew install node in your terminal.

18
00:00:43,70 --> 00:00:45,40
After that, you'll be able to build

19
00:00:45,40 --> 00:00:47,80
using your terminal and chosen IDE.

20
00:00:47,80 --> 00:00:49,80
For Windows, you will need to take some extra steps,

21
00:00:49,80 --> 00:00:50,80
but no worries,

22
00:00:50,80 --> 00:00:54,00
it's very standard for Windows devs to be using Node.

23
00:00:54,00 --> 00:00:56,10
Make sure you check out the installation instructions

24
00:00:56,10 --> 00:00:57,70
in the exercise files.

25
00:00:57,70 --> 00:01:00,50
But as an overview, what you will do is

26
00:01:00,50 --> 00:01:04,50
install Git, install Git Bash, install Node,

27
00:01:04,50 --> 00:01:06,30
and then restart your computer,

28
00:01:06,30 --> 00:01:10,30
and then you'll install npm and NVM.

29
00:01:10,30 --> 00:01:11,50
As a Windows dev,

30
00:01:11,50 --> 00:01:12,80
all of your build will be done

31
00:01:12,80 --> 00:01:15,70
in your chosen IDE and in Git Bash,

32
00:01:15,70 --> 00:01:17,80
not PowerShell or Command Line.

33
00:01:17,80 --> 00:01:20,00
Now you're ready to start building the project with me.

