1
00:00:00,50 --> 00:00:01,90
- So all of your functionality

2
00:00:01,90 --> 00:00:04,00
is now working in your program.

3
00:00:04,00 --> 00:00:05,60
Right now a user has to call

4
00:00:05,60 --> 00:00:08,30
each of your commander commands separately though.

5
00:00:08,30 --> 00:00:09,20
What we really want

6
00:00:09,20 --> 00:00:11,80
is for them to all run one after the other.

7
00:00:11,80 --> 00:00:14,70
To do that, open your package.json file

8
00:00:14,70 --> 00:00:16,60
and look for scripts.

9
00:00:16,60 --> 00:00:19,10
There should be a test listed in there as a script,

10
00:00:19,10 --> 00:00:21,70
so create a new line immediately after this test script.

11
00:00:21,70 --> 00:00:25,90
Here write start to define your start script.

12
00:00:25,90 --> 00:00:27,90
This start means that when a user runs

13
00:00:27,90 --> 00:00:29,90
npm start in your app,

14
00:00:29,90 --> 00:00:32,90
then these commands following start will run.

15
00:00:32,90 --> 00:00:34,80
Start should run all three of the commands

16
00:00:34,80 --> 00:00:37,20
that have been set in the index.js file.

17
00:00:37,20 --> 00:00:43,00
So begin with node index.js init, and after that,

18
00:00:43,00 --> 00:00:45,40
write in a double ampersand

19
00:00:45,40 --> 00:00:48,40
to make sure the next script runs after this.

20
00:00:48,40 --> 00:00:50,90
The next script is octocheck,

21
00:00:50,90 --> 00:00:55,40
and we have to write node index.js again, octocheck,

22
00:00:55,40 --> 00:00:57,50
one more double ampersand,

23
00:00:57,50 --> 00:00:59,60
and then this final one is going to be

24
00:00:59,60 --> 00:01:04,90
node index.js create_repo.

25
00:01:04,90 --> 00:01:06,30
So let's save this

26
00:01:06,30 --> 00:01:08,40
and go back into your terminal

27
00:01:08,40 --> 00:01:13,00
and now let's just try and run npm start.

28
00:01:13,00 --> 00:01:16,50
So you'll see that first one with the banner was init.

29
00:01:16,50 --> 00:01:19,60
That long string is your credentials from octocheck.

30
00:01:19,60 --> 00:01:22,70
And now it's asking for repository name.

31
00:01:22,70 --> 00:01:25,00
If we haven't already put in our credentials,

32
00:01:25,00 --> 00:01:26,10
it would first prompt us

33
00:01:26,10 --> 00:01:29,00
for our username and password for GitHub.

