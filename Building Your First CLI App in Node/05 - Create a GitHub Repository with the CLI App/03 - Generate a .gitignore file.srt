1
00:00:00,60 --> 00:00:03,10
- [Instructor] In order to generate a .gitignore file

2
00:00:03,10 --> 00:00:05,40
we're going to create a command line wizard.

3
00:00:05,40 --> 00:00:07,10
If the user is running our application

4
00:00:07,10 --> 00:00:09,00
in an existing project directory,

5
00:00:09,00 --> 00:00:11,00
we'll show them a list of files and directories

6
00:00:11,00 --> 00:00:13,20
already in the current working directory,

7
00:00:13,20 --> 00:00:16,50
and then allow them to select which ones to ignore.

8
00:00:16,50 --> 00:00:19,00
We'll be using inquirers check box functionality

9
00:00:19,00 --> 00:00:20,50
to help us with this.

10
00:00:20,50 --> 00:00:22,80
So continuing in create_a_repo,

11
00:00:22,80 --> 00:00:26,20
after the create a repository function,

12
00:00:26,20 --> 00:00:32,60
Let's create a new async function called createGitIgnore.

13
00:00:32,60 --> 00:00:34,50
In here we need to find the files

14
00:00:34,50 --> 00:00:37,80
that our .gitignore file will be composed of,

15
00:00:37,80 --> 00:00:40,30
which are .gitignore files.

16
00:00:40,30 --> 00:00:44,00
So here we're going to make use of lowdash's without method

17
00:00:44,00 --> 00:00:47,70
we're going to set this in a const called filelist

18
00:00:47,70 --> 00:00:49,80
and then here we're going to make use of lowdash,

19
00:00:49,80 --> 00:00:52,80
without and then what we're going to do is

20
00:00:52,80 --> 00:00:55,90
scan the current, and ignore the git folder

21
00:00:55,90 --> 00:00:59,00
and any existing git ignore files.

22
00:00:59,00 --> 00:01:00,70
So we're looking through all of the files

23
00:01:00,70 --> 00:01:01,80
and we're looking specifically

24
00:01:01,80 --> 00:01:09,00
for files ending in .git and .gitignore.

25
00:01:09,00 --> 00:01:11,80
If there is nothing that needs to be added

26
00:01:11,80 --> 00:01:13,10
there's no point in continuing,

27
00:01:13,10 --> 00:01:16,60
so we'll simply touch the current .gitignore file

28
00:01:16,60 --> 00:01:18,40
and exit out of the function.

29
00:01:18,40 --> 00:01:21,00
So that looks like this.

30
00:01:21,00 --> 00:01:22,50
Just going to scroll up a little bit here,

31
00:01:22,50 --> 00:01:28,30
make a little more space.

32
00:01:28,30 --> 00:01:32,40
And we'll just setup the basic structure here first.

33
00:01:32,40 --> 00:01:35,40
So all that's saying is if there's nothing

34
00:01:35,40 --> 00:01:37,40
that has been retrieved,

35
00:01:37,40 --> 00:01:40,50
then we'll just exit this process here.

36
00:01:40,50 --> 00:01:43,00
For now we're just going to keep this structure here

37
00:01:43,00 --> 00:01:46,20
and we're going to save and close our create_a_repo

38
00:01:46,20 --> 00:01:50,00
and we're going to go work in our inquire.js file.

39
00:01:50,00 --> 00:01:52,00
Just to setup the rest of the functionality for this

40
00:01:52,00 --> 00:01:54,80
before we finish this function here.

41
00:01:54,80 --> 00:01:58,40
So in our lib/inqure.js file, we'll add a new

42
00:01:58,40 --> 00:02:07,20
askIgnoreFiles function after askRepositoryDetails.

43
00:02:07,20 --> 00:02:09,80
Again I'm just going to create a little bit more space here

44
00:02:09,80 --> 00:02:11,30
and we're going to start our function here.

45
00:02:11,30 --> 00:02:16,30
This is going to be called askIgnoreFiles.

46
00:02:16,30 --> 00:02:18,70
This is going to take in a filelist const,

47
00:02:18,70 --> 00:02:21,30
which we will setup shortly.

48
00:02:21,30 --> 00:02:24,70
And again we want to setup some questions in here.

49
00:02:24,70 --> 00:02:28,70
These questions are all about our git ignore files.

50
00:02:28,70 --> 00:02:31,20
So here we're going to use a new type

51
00:02:31,20 --> 00:02:34,20
that will generate check boxes to all the user to select

52
00:02:34,20 --> 00:02:37,10
which files or folders they wish to ignore.

53
00:02:37,10 --> 00:02:40,00
So that type is checkbox.

54
00:02:40,00 --> 00:02:43,30
We're going to name this ignore.

55
00:02:43,30 --> 00:02:46,90
We'll set a message for the user

56
00:02:46,90 --> 00:02:50,20
so they know what to do with these check boxes.

57
00:02:50,20 --> 00:02:58,40
Select the files and or folders you wish to ignore.

58
00:02:58,40 --> 00:03:00,90
Our choices for the checkbox are going to come out

59
00:03:00,90 --> 00:03:06,60
of the filelist and the default will be set to ignore,

60
00:03:06,60 --> 00:03:10,40
for example node_modules.

61
00:03:10,40 --> 00:03:13,70
Finally, at the end of this, we need to be sure

62
00:03:13,70 --> 00:03:18,60
to return the values to the questions we've just asked.

63
00:03:18,60 --> 00:03:20,00
That's looking really great,

64
00:03:20,00 --> 00:03:22,00
so let's just make sure to save that.

65
00:03:22,00 --> 00:03:24,80
And now that we've setup our inquirer code,

66
00:03:24,80 --> 00:03:27,90
we're going to go back to our create_a_repo.js file again

67
00:03:27,90 --> 00:03:29,90
to create the git ignore function

68
00:03:29,90 --> 00:03:32,80
that's going to call in these questions.

69
00:03:32,80 --> 00:03:35,90
So inside our if statement that we setup earlier

70
00:03:35,90 --> 00:03:38,80
we want to setup a const answers,

71
00:03:38,80 --> 00:03:40,70
which is going to grab the answers

72
00:03:40,70 --> 00:03:44,90
we just setup in our inquirer file

73
00:03:44,90 --> 00:03:46,20
and then the next thing we want to do

74
00:03:46,20 --> 00:03:49,30
is setup another if statement.

75
00:03:49,30 --> 00:03:56,10
So if the value for the length of answers is not zero

76
00:03:56,10 --> 00:03:58,50
we want to write that information

77
00:03:58,50 --> 00:04:01,50
and join the information,

78
00:04:01,50 --> 00:04:05,20
separating each piece of information with a newline.

79
00:04:05,20 --> 00:04:08,60
So we want to make sure we write

80
00:04:08,60 --> 00:04:13,40
and then every single answer that we have

81
00:04:13,40 --> 00:04:16,20
we want to join them and then this little piece

82
00:04:16,20 --> 00:04:18,80
of regex here is just going to make sure each

83
00:04:18,80 --> 00:04:21,70
of them is delivered back to us on a newline

84
00:04:21,70 --> 00:04:25,20
and not just in one big jumble of information.

85
00:04:25,20 --> 00:04:27,60
And then, as we said before,

86
00:04:27,60 --> 00:04:30,40
otherwise for this one as well,

87
00:04:30,40 --> 00:04:33,50
we're going to have two elses cause we have two ifs,

88
00:04:33,50 --> 00:04:35,50
if the above's not true,

89
00:04:35,50 --> 00:04:41,20
again we just want to be exiting that process.

90
00:04:41,20 --> 00:04:44,40
And then after that we still have the other fail catch

91
00:04:44,40 --> 00:04:46,70
from our first case.

92
00:04:46,70 --> 00:04:50,70
In any case, the end result is that we are guaranteed

93
00:04:50,70 --> 00:04:55,00
to have a .gitignore file for our user's repository.

