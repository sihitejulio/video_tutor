1
00:00:00,50 --> 00:00:02,40
- [Instructor] Before we can use the code we just added

2
00:00:02,40 --> 00:00:04,10
to the inquirer.js file,

3
00:00:04,10 --> 00:00:07,40
we also have to add some create a repository functions

4
00:00:07,40 --> 00:00:10,00
in our create_a_repo.js file.

5
00:00:10,00 --> 00:00:11,50
So let's go into our lib folder

6
00:00:11,50 --> 00:00:14,70
and then into our create_a_repo.js file.

7
00:00:14,70 --> 00:00:16,00
At the beginning of the file,

8
00:00:16,00 --> 00:00:19,30
load all of your dependencies, packages, and modules.

9
00:00:19,30 --> 00:00:21,60
This should include lodash, fs,

10
00:00:21,60 --> 00:00:25,60
and your two files inquirer and github_credentials.

11
00:00:25,60 --> 00:00:28,60
We're also going to make use of simple-git here.

12
00:00:28,60 --> 00:00:32,60
This is a helper for running git commands.

13
00:00:32,60 --> 00:00:39,70
Again, begin by creating module.exports.

14
00:00:39,70 --> 00:00:41,40
And this is going to contain an object

15
00:00:41,40 --> 00:00:44,20
we will name createRemoteRepository,

16
00:00:44,20 --> 00:00:46,20
which will run an async function

17
00:00:46,20 --> 00:00:49,20
with two newly created consts.

18
00:00:49,20 --> 00:00:51,80
The first is going to be github,

19
00:00:51,80 --> 00:00:55,20
and this will retrieve an instance of the octokit

20
00:00:55,20 --> 00:00:59,50
that we set up in our inquirer file.

21
00:00:59,50 --> 00:01:04,20
The other const we need to set up is answers,

22
00:01:04,20 --> 00:01:07,80
and what this will do is just retrieve the answers

23
00:01:07,80 --> 00:01:12,50
we've created in our askRepositoryDetails.

24
00:01:12,50 --> 00:01:16,60
Finally, we'll create a const called data,

25
00:01:16,60 --> 00:01:18,30
and this is going to pull the information

26
00:01:18,30 --> 00:01:23,20
from askRepositoryDetails and set them inside of this.

27
00:01:23,20 --> 00:01:24,10
So we're going to have to pull

28
00:01:24,10 --> 00:01:26,00
a few different things out of here.

29
00:01:26,00 --> 00:01:28,00
We're going to make sure we can pull out

30
00:01:28,00 --> 00:01:32,40
the name, the description,

31
00:01:32,40 --> 00:01:36,00
and then this thing about the private or public repository.

32
00:01:36,00 --> 00:01:40,20
So we're going to set it this way,

33
00:01:40,20 --> 00:01:42,40
with a lot of focus on private,

34
00:01:42,40 --> 00:01:46,10
since the default that we've set is public.

35
00:01:46,10 --> 00:01:48,50
Therefore private is only required

36
00:01:48,50 --> 00:01:53,30
when the user answer is actually equal to private.

37
00:01:53,30 --> 00:01:55,20
Below this, the next thing we're going to do

38
00:01:55,20 --> 00:01:58,00
is create a try function to check the status

39
00:01:58,00 --> 00:02:02,50
of all the user input data about the new repository.

40
00:02:02,50 --> 00:02:03,70
This is connected to GitHub

41
00:02:03,70 --> 00:02:07,20
and will return a response from the GitHub SSH URL

42
00:02:07,20 --> 00:02:10,00
that the new repository will be created at.

43
00:02:10,00 --> 00:02:14,20
So we're going to set that in a const response,

44
00:02:14,20 --> 00:02:18,30
and what we're waiting for is GitHub

45
00:02:18,30 --> 00:02:21,90
to create the authentication.

46
00:02:21,90 --> 00:02:25,00
And it is taking data which is what we've just set up

47
00:02:25,00 --> 00:02:27,20
with all of the name and description

48
00:02:27,20 --> 00:02:30,10
and information about the repository.

49
00:02:30,10 --> 00:02:33,80
Now, the response that GitHub is going to create for that

50
00:02:33,80 --> 00:02:35,90
is an SSH URL.

51
00:02:35,90 --> 00:02:37,60
So if everything is authenticated,

52
00:02:37,60 --> 00:02:40,80
it will send back a URL that we can use

53
00:02:40,80 --> 00:02:45,60
to set as the remote URL for the new repository.

54
00:02:45,60 --> 00:02:48,50
And of course we have to put our catch here.

55
00:02:48,50 --> 00:02:51,00
Just going to make sure that we catch all of the errors

56
00:02:51,00 --> 00:02:52,80
and respond to them.

57
00:02:52,80 --> 00:02:55,30
I think I, again, have too many brackets,

58
00:02:55,30 --> 00:02:57,60
so let's get rid of this one here.

59
00:02:57,60 --> 00:02:59,70
So once we have finished all of this,

60
00:02:59,70 --> 00:03:02,20
once we've prompted information from the user

61
00:03:02,20 --> 00:03:04,60
for the repository description and its status,

62
00:03:04,60 --> 00:03:07,90
we can use the GitHub package to create the repository.

63
00:03:07,90 --> 00:03:09,60
As I said, once this is complete,

64
00:03:09,60 --> 00:03:13,40
GitHub will return a URL for the newly created repository.

65
00:03:13,40 --> 00:03:14,60
With that URL,

66
00:03:14,60 --> 00:03:16,70
we'll then be able to set it up as a remote

67
00:03:16,70 --> 00:03:19,60
for the git repo in our local folder.

68
00:03:19,60 --> 00:03:21,20
That hasn't been done yet here.

69
00:03:21,20 --> 00:03:25,10
This is just GitHub returning the URL that we are going to use.

70
00:03:25,10 --> 00:03:26,80
Before we do that, though,

71
00:03:26,80 --> 00:03:30,20
let's interactively create a .gitignore file.

72
00:03:30,20 --> 00:03:33,30
We can put secrets and API keys in the .gitignore file,

73
00:03:33,30 --> 00:03:35,60
making sure the user is always safe when sending

74
00:03:35,60 --> 00:03:38,00
their information to the newly created repository.

