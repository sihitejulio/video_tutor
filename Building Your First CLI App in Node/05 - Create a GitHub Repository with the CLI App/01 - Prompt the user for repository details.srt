1
00:00:00,50 --> 00:00:02,60
- [Instructor] We now have all of the prompts

2
00:00:02,60 --> 00:00:04,30
to authorize our user,

3
00:00:04,30 --> 00:00:06,50
and the authorization token can be stored

4
00:00:06,50 --> 00:00:08,30
from OAuth for GitHub.

5
00:00:08,30 --> 00:00:10,90
These user credentials will be used to create

6
00:00:10,90 --> 00:00:14,00
a remote repository from our CLI app.

7
00:00:14,00 --> 00:00:16,00
The questions in the prompt will be,

8
00:00:16,00 --> 00:00:18,60
what is the name of the repository to be created,

9
00:00:18,60 --> 00:00:20,90
provide a description of the repository,

10
00:00:20,90 --> 00:00:23,70
which is an optimal input for the user,

11
00:00:23,70 --> 00:00:25,50
and should the repository be created

12
00:00:25,50 --> 00:00:27,90
as a public or a private repository?

13
00:00:27,90 --> 00:00:29,80
So we're again going to use inquirer

14
00:00:29,80 --> 00:00:31,60
to prompt the user here.

15
00:00:31,60 --> 00:00:33,40
Let's close index.js

16
00:00:33,40 --> 00:00:37,60
and open the inquirer.js, which is in our lib folder.

17
00:00:37,60 --> 00:00:40,60
In addition to inquirer, we are going to use minimist

18
00:00:40,60 --> 00:00:43,20
to set some defaults for the prompts.

19
00:00:43,20 --> 00:00:45,50
You can see we already have that set as a require

20
00:00:45,50 --> 00:00:48,00
at the top of the inquirer file.

21
00:00:48,00 --> 00:00:51,70
Then, after the askGitHubCredentials function,

22
00:00:51,70 --> 00:00:56,20
below that, we're going to create a new function

23
00:00:56,20 --> 00:01:00,60
called askRepositoryDetails.

24
00:01:00,60 --> 00:01:02,80
So just like before, you're creating an object here

25
00:01:02,80 --> 00:01:04,40
that contains a function.

26
00:01:04,40 --> 00:01:08,40
And to begin with, inside this askRepositoryDetails,

27
00:01:08,40 --> 00:01:10,30
we're going to call in minimist

28
00:01:10,30 --> 00:01:13,40
and set it in a const called argv.

29
00:01:13,40 --> 00:01:16,70
So, we're setting up a const argv.

30
00:01:16,70 --> 00:01:19,20
And in here, we are requiring minimist.

31
00:01:19,20 --> 00:01:23,40
And then we're going to find everything that is given

32
00:01:23,40 --> 00:01:25,30
as input on the command line.

33
00:01:25,30 --> 00:01:31,80
And we're going to do that by running this process.argv.slice.

34
00:01:31,80 --> 00:01:36,20
And we're going to take it from position two in the array.

35
00:01:36,20 --> 00:01:38,80
So when you run process.argv,

36
00:01:38,80 --> 00:01:41,70
it takes every single thing that is given

37
00:01:41,70 --> 00:01:43,20
as input into the command line.

38
00:01:43,20 --> 00:01:47,00
But this also always includes the first item,

39
00:01:47,00 --> 00:01:49,20
your zero place in the array,

40
00:01:49,20 --> 00:01:53,40
as a path to the node itself in node.js.

41
00:01:53,40 --> 00:01:56,70
And then the second one, which is in the first position,

42
00:01:56,70 --> 00:01:58,80
is the path to your script.

43
00:01:58,80 --> 00:02:00,60
So these are going to live in positions zero

44
00:02:00,60 --> 00:02:02,60
and one in the argv array.

45
00:02:02,60 --> 00:02:06,30
And therefore, to get the input given from the user,

46
00:02:06,30 --> 00:02:07,90
you want to begin checking for items

47
00:02:07,90 --> 00:02:10,60
in the array after those first two.

48
00:02:10,60 --> 00:02:14,30
Using two here discards both of those initial bids

49
00:02:14,30 --> 00:02:17,60
and returns everything else typed into the command line.

50
00:02:17,60 --> 00:02:20,40
The next thing you will do right after the argv line

51
00:02:20,40 --> 00:02:22,50
is set up a const question,

52
00:02:22,50 --> 00:02:25,50
which will store all of the input received from the user,

53
00:02:25,50 --> 00:02:28,20
this time about the repository setup.

54
00:02:28,20 --> 00:02:31,60
Again, just like your questions for user name and password,

55
00:02:31,60 --> 00:02:33,70
you will set up functions for each question

56
00:02:33,70 --> 00:02:35,50
that you will prompt the user for.

57
00:02:35,50 --> 00:02:37,70
This time, you'll have three functions,

58
00:02:37,70 --> 00:02:42,00
for name of the repository, description of the repository,

59
00:02:42,00 --> 00:02:44,60
and whether this is private or public.

60
00:02:44,60 --> 00:02:46,30
Type, name, and message

61
00:02:46,30 --> 00:02:48,90
are just like the earlier questions created.

62
00:02:48,90 --> 00:02:51,20
In this case, type is input.

63
00:02:51,20 --> 00:02:52,70
The name here is name.

64
00:02:52,70 --> 00:02:54,60
And the message or prompt is

65
00:02:54,60 --> 00:02:56,90
enter a name for the repository.

66
00:02:56,90 --> 00:03:01,50
After that, you will set a default value that will be input

67
00:03:01,50 --> 00:03:04,90
in the prompt if the user doesn't set in any input.

68
00:03:04,90 --> 00:03:07,40
So here, you can see we're using default,

69
00:03:07,40 --> 00:03:11,00
and we're calling that const we created earlier argv

70
00:03:11,00 --> 00:03:14,40
and taking it from the zero position.

71
00:03:14,40 --> 00:03:15,70
That's because we already got rid

72
00:03:15,70 --> 00:03:18,30
of those two pieces of information we didn't want

73
00:03:18,30 --> 00:03:21,30
and saved all of that in this const.

74
00:03:21,30 --> 00:03:25,00
Then we're going to set in these double pipes, which means or,

75
00:03:25,00 --> 00:03:29,90
so we can use the argv position zero,

76
00:03:29,90 --> 00:03:33,60
or we can check files

77
00:03:33,60 --> 00:03:38,00
and get the current directory name

78
00:03:38,00 --> 00:03:42,20
and use that instead as the repository name.

79
00:03:42,20 --> 00:03:43,70
What are we going to do if neither

80
00:03:43,70 --> 00:03:46,10
of these is available or set?

81
00:03:46,10 --> 00:03:48,70
Well, first we have to check if one of them's been set,

82
00:03:48,70 --> 00:03:51,60
so we're going to use that validate again.

83
00:03:51,60 --> 00:03:54,60
Again, to see if anything has been input by the user

84
00:03:54,60 --> 00:03:56,60
or is available from these defaults,

85
00:03:56,60 --> 00:03:59,60
we just have value.length.

86
00:03:59,60 --> 00:04:02,00
If it's true that something's been entered,

87
00:04:02,00 --> 00:04:06,90
then we just want to return true.

88
00:04:06,90 --> 00:04:10,70
Otherwise, we want to prompt the user

89
00:04:10,70 --> 00:04:12,50
to enter a name for the repository.

90
00:04:12,50 --> 00:04:18,10
So, return 'Please enter a name.'

91
00:04:18,10 --> 00:04:23,70
We could even say a unique name for the repository.

92
00:04:23,70 --> 00:04:25,70
And close that statement.

93
00:04:25,70 --> 00:04:28,10
And here we're finished with the first

94
00:04:28,10 --> 00:04:30,90
of our three questions, which is,

95
00:04:30,90 --> 00:04:33,00
what is the name of your repository.

96
00:04:33,00 --> 00:04:39,20
So below that, we're going to create our next question.

97
00:04:39,20 --> 00:04:45,90
This second question is also input type.

98
00:04:45,90 --> 00:04:51,70
And this time the name of the question is description.

99
00:04:51,70 --> 00:04:58,20
The default here is the value at argv._[1] or null.

100
00:04:58,20 --> 00:05:00,10
You can set this value here to null

101
00:05:00,10 --> 00:05:03,00
since the description is actually an optional input

102
00:05:03,00 --> 00:05:04,20
for the user.

103
00:05:04,20 --> 00:05:07,30
So let's let them know that in the message.

104
00:05:07,30 --> 00:05:10,20
Let's just say now you can choose

105
00:05:10,20 --> 00:05:16,20
to enter a description of the repository if you like.

106
00:05:16,20 --> 00:05:21,40
And just in brackets, this is not a mandatory field.

107
00:05:21,40 --> 00:05:26,00
And then if we come back across to where our code is here,

108
00:05:26,00 --> 00:05:28,60
we're, again, going to chain this here,

109
00:05:28,60 --> 00:05:30,20
open a third question.

110
00:05:30,20 --> 00:05:32,10
The third question is a little bit different

111
00:05:32,10 --> 00:05:35,10
since this time you have to create options for the user.

112
00:05:35,10 --> 00:05:37,90
There's two options, public or private.

113
00:05:37,90 --> 00:05:42,90
And we can still set the input type to input.

114
00:05:42,90 --> 00:05:45,60
We're going to name this visibility.

115
00:05:45,60 --> 00:05:50,00
And the message is going to be,

116
00:05:50,00 --> 00:05:53,60
Would you like this repository to be set

117
00:05:53,60 --> 00:05:56,20
as public or private?

118
00:05:56,20 --> 00:06:00,90
And here, we're actually going to offer the user choices.

119
00:06:00,90 --> 00:06:03,60
This is going to be set up as an array

120
00:06:03,60 --> 00:06:07,10
with two options, public and private.

121
00:06:07,10 --> 00:06:09,90
And what this does is it will prompt the user,

122
00:06:09,90 --> 00:06:13,40
and if they set in anything that isn't exactly public

123
00:06:13,40 --> 00:06:15,20
or private in all lowercase,

124
00:06:15,20 --> 00:06:18,70
it's not actually going to be valid input.

125
00:06:18,70 --> 00:06:22,40
We're going to set the default to public,

126
00:06:22,40 --> 00:06:24,70
and then we're just going to close our statement here

127
00:06:24,70 --> 00:06:29,90
below this question with this square bracket.

128
00:06:29,90 --> 00:06:31,50
It looks like we've got a duplicate.

129
00:06:31,50 --> 00:06:33,90
Let's just get these ones out of here.

130
00:06:33,90 --> 00:06:37,00
This is going to return all of the information

131
00:06:37,00 --> 00:06:40,10
from the different questions and prompts.

132
00:06:40,10 --> 00:06:41,20
Finally, at the end here,

133
00:06:41,20 --> 00:06:43,00
you see you need a return statement

134
00:06:43,00 --> 00:06:45,90
that returns the value of all of these questions.

135
00:06:45,90 --> 00:06:48,60
We need to make sure that we also have a return

136
00:06:48,60 --> 00:06:51,90
for the GitHub credentials' questions up here.

137
00:06:51,90 --> 00:06:53,60
Whoops, there we are.

138
00:06:53,60 --> 00:06:56,50
Let's just add back those brackets I deleted

139
00:06:56,50 --> 00:06:58,70
and put them up here.

140
00:06:58,70 --> 00:07:02,00
I think they belong with this first question we created.

141
00:07:02,00 --> 00:07:03,90
Looks like the return inquirer.prompt

142
00:07:03,90 --> 00:07:05,20
got moved below by accident,

143
00:07:05,20 --> 00:07:09,20
so we've just got to make sure that it's in here correctly,

144
00:07:09,20 --> 00:07:11,30
which it looks like it is now.

145
00:07:11,30 --> 00:07:13,00
Again, this will return the answers

146
00:07:13,00 --> 00:07:16,00
from the questions for the question above it,

147
00:07:16,00 --> 00:07:18,00
this askGitHubCredentials.

