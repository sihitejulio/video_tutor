1
00:00:00,50 --> 00:00:01,40
- [Instructor] Now that we have built

2
00:00:01,40 --> 00:00:03,90
all the functionality to authenticate the user,

3
00:00:03,90 --> 00:00:04,90
we can call it,

4
00:00:04,90 --> 00:00:07,20
and once we're sure the user is authenticated,

5
00:00:07,20 --> 00:00:09,50
we can begin to call the other Git functionality

6
00:00:09,50 --> 00:00:10,90
we've just built in.

7
00:00:10,90 --> 00:00:15,20
So let's set all of this into a create repository command

8
00:00:15,20 --> 00:00:17,70
in our index.js file.

9
00:00:17,70 --> 00:00:22,30
In index.js add a require to call the create_a_repo file

10
00:00:22,30 --> 00:00:24,10
that was just updated,

11
00:00:24,10 --> 00:00:27,80
and then nearer the bottom after the GitHub command,

12
00:00:27,80 --> 00:00:30,90
well, our octocheck GitHub command,

13
00:00:30,90 --> 00:00:32,70
so after this one here,

14
00:00:32,70 --> 00:00:34,40
let's set up a new command.

15
00:00:34,40 --> 00:00:38,30
Again we begin with musette, .command,

16
00:00:38,30 --> 00:00:46,50
and this one is going to be called create_repo.

17
00:00:46,50 --> 00:00:53,80
The description here is Create a new repository on GitHub.

18
00:00:53,80 --> 00:00:58,70
And then we're going to open an async action.

19
00:00:58,70 --> 00:01:01,30
The very first thing we're going to have to do

20
00:01:01,30 --> 00:01:03,80
is retrieve and set the GitHub token.

21
00:01:03,80 --> 00:01:08,80
So let's set that in const getGitHubToken.

22
00:01:08,80 --> 00:01:13,10
So this again async because we're waiting on things.

23
00:01:13,10 --> 00:01:14,60
And then here we're going to

24
00:01:14,60 --> 00:01:22,50
again let token equal getStoredGitHubToken.

25
00:01:22,50 --> 00:01:30,10
If there is a token, we of course want to return the token.

26
00:01:30,10 --> 00:01:33,30
If no token is found we want to wait for

27
00:01:33,30 --> 00:01:35,80
those credentials to come back to us

28
00:01:35,80 --> 00:01:39,20
before we do anything else.

29
00:01:39,20 --> 00:01:42,80
If the credentials do come back to us,

30
00:01:42,80 --> 00:01:45,40
we want to register them

31
00:01:45,40 --> 00:01:49,00
and we'll set that in the token here,

32
00:01:49,00 --> 00:01:51,90
and just make sure that we return the token.

33
00:01:51,90 --> 00:01:54,20
Once we have authenticated the user,

34
00:01:54,20 --> 00:01:55,90
we want to use those credentials

35
00:01:55,90 --> 00:02:00,80
to create a new remote repository in that account on GitHub.

36
00:02:00,80 --> 00:02:03,10
So we're going to set this in a try statement

37
00:02:03,10 --> 00:02:05,20
and we want to make sure that we have the credentials

38
00:02:05,20 --> 00:02:06,40
before we do anything else.

39
00:02:06,40 --> 00:02:09,20
So again we're setting up this token const

40
00:02:09,20 --> 00:02:14,20
and this one is waiting on the getGitHubToken

41
00:02:14,20 --> 00:02:15,70
that we just set up above.

42
00:02:15,70 --> 00:02:19,40
So it's waiting for that to send it back information.

43
00:02:19,40 --> 00:02:22,80
We want to make sure that we can retrieve and store them.

44
00:02:22,80 --> 00:02:25,50
Then we're going to wait for the URL

45
00:02:25,50 --> 00:02:28,00
that is being returned by GitHub.

46
00:02:28,00 --> 00:02:35,50
So that is in our createRemoteRepository function.

47
00:02:35,50 --> 00:02:39,20
So we're going to wait for that.

48
00:02:39,20 --> 00:02:41,30
We're also going to require information

49
00:02:41,30 --> 00:02:44,40
on the GitIgnore file,

50
00:02:44,40 --> 00:02:47,70
and once we receive all this information,

51
00:02:47,70 --> 00:02:50,20
let's set this const complete

52
00:02:50,20 --> 00:02:59,00
which is going to be equal to setupRepository with this URL.

53
00:02:59,00 --> 00:03:02,80
So, if complete, let's let the user know

54
00:03:02,80 --> 00:03:05,30
that everything went well.

55
00:03:05,30 --> 00:03:08,00
We're just going to print out a statement in the console,

56
00:03:08,00 --> 00:03:09,20
and we're going to use chalk here again

57
00:03:09,20 --> 00:03:13,50
to help us make the command line a little less dry,

58
00:03:13,50 --> 00:03:17,70
and just output this message, All done!

59
00:03:17,70 --> 00:03:19,70
Finally, to close the function

60
00:03:19,70 --> 00:03:23,00
we again need to set up some error catching.

61
00:03:23,00 --> 00:03:24,50
I've just pasted this in here,

62
00:03:24,50 --> 00:03:25,50
but we can go through.

63
00:03:25,50 --> 00:03:28,30
Make sure you close your statement at the end.

64
00:03:28,30 --> 00:03:30,40
In general we want to catch the error.

65
00:03:30,40 --> 00:03:34,80
Inside our error though we've added two cases this time.

66
00:03:34,80 --> 00:03:38,80
One is a 401 and the other is a 422.

67
00:03:38,80 --> 00:03:41,10
401 will be set for authentication failure

68
00:03:41,10 --> 00:03:43,70
and 422 will be used to let the user know

69
00:03:43,70 --> 00:03:45,80
that a repository already exists

70
00:03:45,80 --> 00:03:48,70
so a new one can't be created with the same name.

71
00:03:48,70 --> 00:03:51,30
As you can see, inside each of our cases,

72
00:03:51,30 --> 00:03:53,30
we have the message to the user,

73
00:03:53,30 --> 00:03:54,70
whether they couldn't log in

74
00:03:54,70 --> 00:03:57,90
or a remote repository already exists.

75
00:03:57,90 --> 00:04:01,00
You could of course just display the error code.

76
00:04:01,00 --> 00:04:04,50
However printing out a more human-friendly message as well

77
00:04:04,50 --> 00:04:06,90
provides a much better user experience

78
00:04:06,90 --> 00:04:10,10
reducing frustration and friction when using your app.

79
00:04:10,10 --> 00:04:13,10
Finally, if we switch back to our terminal

80
00:04:13,10 --> 00:04:16,50
we should be able to run this create_repo command.

81
00:04:16,50 --> 00:04:18,70
But first, I just want to let you take a look

82
00:04:18,70 --> 00:04:21,50
at my GitHub profile.

83
00:04:21,50 --> 00:04:22,50
You can see that we don't have

84
00:04:22,50 --> 00:04:24,60
any personal access tokens here.

85
00:04:24,60 --> 00:04:25,70
And you can see the name

86
00:04:25,70 --> 00:04:28,60
of my last repository created is kitty.

87
00:04:28,60 --> 00:04:29,60
Let's check this again

88
00:04:29,60 --> 00:04:32,10
after we've run our commands in our program.

89
00:04:32,10 --> 00:04:34,80
So finally if we switch back to our terminal

90
00:04:34,80 --> 00:04:39,00
we should be able to run this create_repo command.

91
00:04:39,00 --> 00:04:41,30
And if we hit Enter it should prompt you

92
00:04:41,30 --> 00:04:44,10
for your GitHub username or an email address.

93
00:04:44,10 --> 00:04:46,20
Let's make sure to use the correct credentials

94
00:04:46,20 --> 00:04:50,20
for your real GitHub account here.

95
00:04:50,20 --> 00:04:51,80
You will have a deprecation error

96
00:04:51,80 --> 00:04:53,30
because they are updating octokit.

97
00:04:53,30 --> 00:04:56,20
But we talked about that a little bit earlier.

98
00:04:56,20 --> 00:04:57,70
Right after the deprecation error

99
00:04:57,70 --> 00:04:59,50
which you don't need to worry about,

100
00:04:59,50 --> 00:05:03,30
it's going to ask you to enter a name for your repository.

101
00:05:03,30 --> 00:05:05,80
By default mine's going to call it create_repo,

102
00:05:05,80 --> 00:05:08,70
but I'm going to call mine octosnails.

103
00:05:08,70 --> 00:05:11,00
And if we just hit Enter here

104
00:05:11,00 --> 00:05:14,40
we can optionally enter a description of the repository.

105
00:05:14,40 --> 00:05:19,00
I'm going to say slow moving code movements and hit Enter.

106
00:05:19,00 --> 00:05:21,80
Would you like this to be public or private?

107
00:05:21,80 --> 00:05:23,80
Public's the default, so if we hit enter

108
00:05:23,80 --> 00:05:25,80
it should create a public repository.

109
00:05:25,80 --> 00:05:28,00
Then we have this neat little checklist we created

110
00:05:28,00 --> 00:05:29,60
for our GitIgnore file.

111
00:05:29,60 --> 00:05:33,20
So you can use A to select everything, I to invert,

112
00:05:33,20 --> 00:05:35,20
and if you use your spacebar you can go down

113
00:05:35,20 --> 00:05:37,50
and check them individually.

114
00:05:37,50 --> 00:05:39,30
This might be a little bit funny when I push it

115
00:05:39,30 --> 00:05:41,20
but let's hit Enter

116
00:05:41,20 --> 00:05:43,80
and in my case the remote origin already exists

117
00:05:43,80 --> 00:05:45,50
because I've already got GitIgnore

118
00:05:45,50 --> 00:05:47,50
and other Git files in here.

119
00:05:47,50 --> 00:05:50,90
However yours should have pushed to your GitHub profile.

120
00:05:50,90 --> 00:05:52,90
So if we come back to my profile,

121
00:05:52,90 --> 00:05:54,80
and just refresh,

122
00:05:54,80 --> 00:05:57,00
you can see that I have a new token

123
00:05:57,00 --> 00:06:00,60
for musette here in my Personal access tokens.

124
00:06:00,60 --> 00:06:03,10
If we go into Repositories and refresh,

125
00:06:03,10 --> 00:06:05,30
you'll see that I have created octosnails

126
00:06:05,30 --> 00:06:08,00
with the description slow moving code movements,

127
00:06:08,00 --> 00:06:10,00
and that came straight from my console

128
00:06:10,00 --> 00:06:12,00
and the program we just created.

