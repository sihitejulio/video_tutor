1
00:00:00,50 --> 00:00:02,50
- [Instructor] So far we've created code within our app

2
00:00:02,50 --> 00:00:04,80
to receive GitHub credentials,

3
00:00:04,80 --> 00:00:06,60
we've created a way to retrieve and store

4
00:00:06,60 --> 00:00:08,50
an OAuth token from GitHub,

5
00:00:08,50 --> 00:00:09,90
and we've set up the functionality

6
00:00:09,90 --> 00:00:12,00
both for creating a new repository,

7
00:00:12,00 --> 00:00:14,10
and creating a .gitignore file,

8
00:00:14,10 --> 00:00:15,80
we're most of the way there.

9
00:00:15,80 --> 00:00:17,60
Now we're going to add a bit more code

10
00:00:17,60 --> 00:00:20,60
to our lib slash creator repo JS file,

11
00:00:20,60 --> 00:00:23,20
so that we can interact with git even more.

12
00:00:23,20 --> 00:00:25,30
We'll be making use of the simple git package

13
00:00:25,30 --> 00:00:27,40
for most of this functionality.

14
00:00:27,40 --> 00:00:30,00
Simple git provides a set of chainable methods

15
00:00:30,00 --> 00:00:32,90
that can run the git EXE.

16
00:00:32,90 --> 00:00:36,00
Overall the functionality we want to automate is this,

17
00:00:36,00 --> 00:00:40,00
one, run git init, two, add the .gitignore file

18
00:00:40,00 --> 00:00:42,60
we have created for our user's new repository,

19
00:00:42,60 --> 00:00:46,10
three, not only add the .gitignore files,

20
00:00:46,10 --> 00:00:48,10
but also be sure to add anything else

21
00:00:48,10 --> 00:00:51,10
that is in the working or current directory,

22
00:00:51,10 --> 00:00:54,00
you know, code files and things like that.

23
00:00:54,00 --> 00:00:57,40
Four, then we want to perform the initial commit,

24
00:00:57,40 --> 00:01:00,20
five, we will also need to add the newly created

25
00:01:00,20 --> 00:01:03,40
remote repository URL that GitHub sends us,

26
00:01:03,40 --> 00:01:07,40
and six, finally, we will push the local working directory

27
00:01:07,40 --> 00:01:10,10
up to the remote GitHub URL provided.

28
00:01:10,10 --> 00:01:12,90
The code we've written so far creates a support structure

29
00:01:12,90 --> 00:01:14,70
for all of this to run on.

30
00:01:14,70 --> 00:01:18,30
So in our lib slash creator repo JS file,

31
00:01:18,30 --> 00:01:21,60
make sure you have the require for simple git at the top

32
00:01:21,60 --> 00:01:25,80
and then create a new object, set up repository,

33
00:01:25,80 --> 00:01:28,40
after your create git ignore.

34
00:01:28,40 --> 00:01:31,50
So we're just going to make sure we have a comma in here,

35
00:01:31,50 --> 00:01:37,00
then we're going to create this setupRepository,

36
00:01:37,00 --> 00:01:41,10
and this is going to contain an async function

37
00:01:41,10 --> 00:01:45,70
which is going to take in the URL.

38
00:01:45,70 --> 00:01:48,60
All of our git functionality is going to be contained

39
00:01:48,60 --> 00:01:52,80
in a try statement, set that up here,

40
00:01:52,80 --> 00:01:54,90
and what are we going to try and do?

41
00:01:54,90 --> 00:01:56,60
Each of our steps that we want to automate

42
00:01:56,60 --> 00:02:01,10
has its own method, so we can just go, await git,

43
00:02:01,10 --> 00:02:03,90
and then we can chain these, and even though we chain them,

44
00:02:03,90 --> 00:02:06,60
they can be on different lines.

45
00:02:06,60 --> 00:02:11,50
So .init, this is calling git init,

46
00:02:11,50 --> 00:02:17,50
then we want to add the .gitignore file,

47
00:02:17,50 --> 00:02:21,00
we also want to add all of the other files

48
00:02:21,00 --> 00:02:23,40
that need to be added to the repository,

49
00:02:23,40 --> 00:02:25,20
and this is just a little piece of rejects in here

50
00:02:25,20 --> 00:02:28,10
that means add everything.

51
00:02:28,10 --> 00:02:29,60
Typically when we push to GitHub,

52
00:02:29,60 --> 00:02:31,60
it would be really similar commands to these,

53
00:02:31,60 --> 00:02:35,00
git init, git add, commit is a little bit different

54
00:02:35,00 --> 00:02:38,40
because the method is taking care of a lot of the work,

55
00:02:38,40 --> 00:02:43,20
so we simply call .commit instead of using for example,

56
00:02:43,20 --> 00:02:45,60
git dash commit m message,

57
00:02:45,60 --> 00:02:49,70
and we're setting the message here to be initial commit.

58
00:02:49,70 --> 00:02:52,80
Add remote is also doing a little heavy-lifting here.

59
00:02:52,80 --> 00:02:54,60
The arguments we have for this method,

60
00:02:54,60 --> 00:02:59,40
are origin, and URL.

61
00:02:59,40 --> 00:03:01,30
Origin is actually a keyword shorthand

62
00:03:01,30 --> 00:03:04,20
for remote repository, and URL

63
00:03:04,20 --> 00:03:07,90
is the internet HTTP address that repository will live at,

64
00:03:07,90 --> 00:03:10,30
in this case, on GitHub.

65
00:03:10,30 --> 00:03:14,30
Finally, we have .push with the arguments origin

66
00:03:14,30 --> 00:03:18,40
and master, which might look pretty familiar.

67
00:03:18,40 --> 00:03:21,00
In the previous command, .addremote,

68
00:03:21,00 --> 00:03:24,00
we added the remote repository information,

69
00:03:24,00 --> 00:03:26,30
so now in this step, we can simply make use

70
00:03:26,30 --> 00:03:29,50
of simple gits.push command and send our master branch

71
00:03:29,50 --> 00:03:32,10
to the remote repository's master.

72
00:03:32,10 --> 00:03:34,20
Let's make sure to close our statement

73
00:03:34,20 --> 00:03:36,00
here at the end of this one,

74
00:03:36,00 --> 00:03:37,60
and finally to finish this function off,

75
00:03:37,60 --> 00:03:41,10
we need to return the information we've just collected.

76
00:03:41,10 --> 00:03:43,70
Then since we have our try, we also need to add

77
00:03:43,70 --> 00:03:46,00
our error catch.

78
00:03:46,00 --> 00:03:48,40
Again, we're going to throw an error,

79
00:03:48,40 --> 00:03:50,30
and that should finish that off.

80
00:03:50,30 --> 00:03:52,80
So now what we have here is all the functionality

81
00:03:52,80 --> 00:03:57,00
we're going to need to push the repository to its remote.

