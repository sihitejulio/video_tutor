1
00:00:00,60 --> 00:00:02,20
- [Instructor] In our files JavaScript file,

2
00:00:02,20 --> 00:00:04,60
we have two main goals.

3
00:00:04,60 --> 00:00:06,70
The first is to create functionality

4
00:00:06,70 --> 00:00:08,90
to get the name of the current directory

5
00:00:08,90 --> 00:00:10,80
so that we can set a default name

6
00:00:10,80 --> 00:00:15,70
for our user's newly created repository, and the second

7
00:00:15,70 --> 00:00:18,90
is to check if the folder the user is currently in

8
00:00:18,90 --> 00:00:21,50
is already a Git repository.

9
00:00:21,50 --> 00:00:23,40
In our files.js file,

10
00:00:23,40 --> 00:00:26,10
let's being by requiring the two helper modules

11
00:00:26,10 --> 00:00:29,90
we will need here, fs and path.

12
00:00:29,90 --> 00:00:31,30
So, just like before,

13
00:00:31,30 --> 00:00:34,10
we're just going to set them in a const

14
00:00:34,10 --> 00:00:38,60
and require them like so,

15
00:00:38,60 --> 00:00:41,20
and make sure you save your file.

16
00:00:41,20 --> 00:00:45,20
And we're going to use path to help us find filePath names.

17
00:00:45,20 --> 00:00:48,10
Now, there's two ways we could do this.

18
00:00:48,10 --> 00:00:50,00
Because we want this to work globally,

19
00:00:50,00 --> 00:00:53,30
we will use path's basename method,

20
00:00:53,30 --> 00:00:58,00
path.basename, to find the current directory name.

21
00:00:58,00 --> 00:01:02,40
We're also going to use process.cwd.

22
00:01:02,40 --> 00:01:04,40
Passing path process.cwd

23
00:01:04,40 --> 00:01:06,40
will extend its functionality a bit.

24
00:01:06,40 --> 00:01:09,40
Process is a special global JavaScript object,

25
00:01:09,40 --> 00:01:12,80
and the method cwd, current working directory,

26
00:01:12,80 --> 00:01:15,10
therefore can return the working directory

27
00:01:15,10 --> 00:01:18,10
even outside of your current working directory.

28
00:01:18,10 --> 00:01:20,10
There is a second way to do this,

29
00:01:20,10 --> 00:01:23,20
and it's most likely what you'd find if you Google it,

30
00:01:23,20 --> 00:01:25,60
and this is using realpathSync.

31
00:01:25,60 --> 00:01:28,10
So, if we just take a look a that here below,

32
00:01:28,10 --> 00:01:33,80
it would look more like, still using the basename method,

33
00:01:33,80 --> 00:01:36,50
but sending it a directory name,

34
00:01:36,50 --> 00:01:41,50
and making use of this realpathSync.

35
00:01:41,50 --> 00:01:44,00
It works perfectly fine when we're in the same directory

36
00:01:44,00 --> 00:01:48,20
as our application, however, it wouldn't work globally.

37
00:01:48,20 --> 00:01:50,50
So, if you see this, it could be a really useful tool

38
00:01:50,50 --> 00:01:52,00
in another application you're building,

39
00:01:52,00 --> 00:01:53,50
but we're not going to do it this way.

40
00:01:53,50 --> 00:01:56,60
So, let's just delete this,

41
00:01:56,60 --> 00:02:01,40
and keep our process.cwd, which is the better choice here

42
00:02:01,40 --> 00:02:04,20
since it will allow us global functionality.

43
00:02:04,20 --> 00:02:07,20
Now, right now, it's just kind of hanging out on its own here.

44
00:02:07,20 --> 00:02:13,90
So, let's place this inside of a module.exports,

45
00:02:13,90 --> 00:02:17,20
and we're just going to put this in here.

46
00:02:17,20 --> 00:02:19,70
And we're also going to place the whole thing

47
00:02:19,70 --> 00:02:23,20
inside of a variable,

48
00:02:23,20 --> 00:02:26,00
and we can call it something descriptive like,

49
00:02:26,00 --> 00:02:31,30
getCurrentDirectoryBase,

50
00:02:31,30 --> 00:02:37,20
and let's just make sure that it's sitting inside here.

51
00:02:37,20 --> 00:02:41,20
And then the next thing we need to do is right after this,

52
00:02:41,20 --> 00:02:44,50
we need to put in a try-catch fail safe error.

53
00:02:44,50 --> 00:02:47,00
So, what we're doing is we're taking a look to see

54
00:02:47,00 --> 00:02:52,70
if there is this filePath.

55
00:02:52,70 --> 00:02:57,80
If there is,

56
00:02:57,80 --> 00:03:02,50
then we want to know what the filePath is called.

57
00:03:02,50 --> 00:03:06,70
Otherwise, we want to catch the error if this doesn't exist,

58
00:03:06,70 --> 00:03:10,50
and return false here,

59
00:03:10,50 --> 00:03:12,30
and make sure we've got all of our

60
00:03:12,30 --> 00:03:13,80
curly queue brackets accounted for.

61
00:03:13,80 --> 00:03:14,60
So we've got all of these,

62
00:03:14,60 --> 00:03:17,30
and we got to make sure we close this one here.

63
00:03:17,30 --> 00:03:18,50
With all of this together,

64
00:03:18,50 --> 00:03:21,40
we've created a utility module for our app.

65
00:03:21,40 --> 00:03:23,00
The utility module just means

66
00:03:23,00 --> 00:03:26,00
that it handles and transforms filePaths.

67
00:03:26,00 --> 00:03:27,00
Handy to know, though,

68
00:03:27,00 --> 00:03:28,60
in case you're going to make a similar module

69
00:03:28,60 --> 00:03:30,40
in future for another app.

70
00:03:30,40 --> 00:03:32,10
Once we have this together,

71
00:03:32,10 --> 00:03:35,70
we need to go back into our index.js file

72
00:03:35,70 --> 00:03:38,50
and make sure that we include the file

73
00:03:38,50 --> 00:03:42,50
that we have just updated so that it can access it.

74
00:03:42,50 --> 00:03:44,70
So we're going to require that here.

75
00:03:44,70 --> 00:03:47,30
We're just going to call it files,

76
00:03:47,30 --> 00:03:49,10
and then, instead of a node module,

77
00:03:49,10 --> 00:03:52,60
this time we are actually requiring

78
00:03:52,60 --> 00:03:56,20
one of our own folders and files.

79
00:03:56,20 --> 00:03:59,90
So, in the lib folder we created,

80
00:03:59,90 --> 00:04:02,20
that's where we're going to find the files.

81
00:04:02,20 --> 00:04:03,90
And be sure that your filePath here

82
00:04:03,90 --> 00:04:06,30
matches the filePath in your folder structure

83
00:04:06,30 --> 00:04:08,60
if you've done it a little bit differently.

84
00:04:08,60 --> 00:04:10,10
Now that we've got that in place,

85
00:04:10,10 --> 00:04:13,00
we can start building the rest of our app.

