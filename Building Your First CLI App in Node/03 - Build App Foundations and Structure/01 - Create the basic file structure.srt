1
00:00:00,40 --> 00:00:03,10
- [Instructor] So far we have set up our main index.js page

2
00:00:03,10 --> 00:00:05,00
and installed a couple of Node.js modules

3
00:00:05,00 --> 00:00:07,80
to help us run some additional functionality in the app.

4
00:00:07,80 --> 00:00:09,80
You can see this in your file structure

5
00:00:09,80 --> 00:00:13,30
on the left hand sidebar in your Sublime text.

6
00:00:13,30 --> 00:00:15,60
Now we're going to create the rest of the file structure

7
00:00:15,60 --> 00:00:18,30
of our app, via the terminal.

8
00:00:18,30 --> 00:00:19,40
The first thing we're going to do

9
00:00:19,40 --> 00:00:22,80
is begin by creating a new lib folder,

10
00:00:22,80 --> 00:00:25,70
just using that make directory command again.

11
00:00:25,70 --> 00:00:27,40
In this folder, we're going to have files

12
00:00:27,40 --> 00:00:30,30
that contain most of the functionality of our app.

13
00:00:30,30 --> 00:00:33,70
Everything will be called and run by the index.js file,

14
00:00:33,70 --> 00:00:35,60
but the files where the functions are set up

15
00:00:35,60 --> 00:00:37,40
will be in the lib folder.

16
00:00:37,40 --> 00:00:39,40
You can think of it like the index.js file

17
00:00:39,40 --> 00:00:41,40
being the captain of starship

18
00:00:41,40 --> 00:00:43,00
and the files in the lib folder

19
00:00:43,00 --> 00:00:45,00
being the different departments on the ship:

20
00:00:45,00 --> 00:00:48,60
a sickbay, warp core engineering, security.

21
00:00:48,60 --> 00:00:51,40
The captain is in charge of the whole ship and its crew,

22
00:00:51,40 --> 00:00:53,50
but is not personally responsible

23
00:00:53,50 --> 00:00:54,80
for executing any of the tasks

24
00:00:54,80 --> 00:00:57,30
in any of those areas himself.

25
00:00:57,30 --> 00:01:00,50
Similarly, the index.js file calls all the functionality

26
00:01:00,50 --> 00:01:05,10
from different files in the lib folder to run the app.

27
00:01:05,10 --> 00:01:08,70
So we're just going to CD into our lib folder,

28
00:01:08,70 --> 00:01:10,70
change directory.

29
00:01:10,70 --> 00:01:15,70
And in here we're going to create four new files.

30
00:01:15,70 --> 00:01:19,80
The first one is going to be files.

31
00:01:19,80 --> 00:01:24,90
The next one is going to be GitHub credentials.

32
00:01:24,90 --> 00:01:28,30
We're also going to have an inquirer file.

33
00:01:28,30 --> 00:01:33,70
And finally we want a create a repo file.

34
00:01:33,70 --> 00:01:35,80
These four files are going to support the entirety

35
00:01:35,80 --> 00:01:37,60
of our app's functionality.

36
00:01:37,60 --> 00:01:39,50
And the next thing we're going to do after this

37
00:01:39,50 --> 00:01:42,30
is begin getting some code into these files.

38
00:01:42,30 --> 00:01:44,70
We'll begin with our files.js file,

39
00:01:44,70 --> 00:01:48,40
which will be developed into our utility package.

40
00:01:48,40 --> 00:01:49,90
If we just do an LS right now,

41
00:01:49,90 --> 00:01:52,50
you can see that all four of our files

42
00:01:52,50 --> 00:01:55,00
are in our lib directory.

