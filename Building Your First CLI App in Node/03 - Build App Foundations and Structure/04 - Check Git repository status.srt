1
00:00:00,50 --> 00:00:02,90
- [Instructor] To finalize our work from our utility package

2
00:00:02,90 --> 00:00:04,80
we need to add one more piece of code

3
00:00:04,80 --> 00:00:07,80
to check if a Git Repository already exists.

4
00:00:07,80 --> 00:00:09,70
Since our utility package is handling all of

5
00:00:09,70 --> 00:00:12,70
the functionality around file endings and transformations,

6
00:00:12,70 --> 00:00:16,30
we'll also write this in files dot JS.

7
00:00:16,30 --> 00:00:18,40
In our files dot JS file,

8
00:00:18,40 --> 00:00:20,50
after the directory exists function

9
00:00:20,50 --> 00:00:24,70
let's create another function Is Git Repository.

10
00:00:24,70 --> 00:00:27,10
Be sure to add a comma after the

11
00:00:27,10 --> 00:00:31,10
directory exists function to chain these together.

12
00:00:31,10 --> 00:00:34,40
In this function we are checking if a git repository exists.

13
00:00:34,40 --> 00:00:40,80
So let's begin with if and send it.

14
00:00:40,80 --> 00:00:43,90
Files dot directory exists

15
00:00:43,90 --> 00:00:47,20
and then we'll pass that git.

16
00:00:47,20 --> 00:00:49,60
So we're using our directory exists function above

17
00:00:49,60 --> 00:00:52,60
to parse the file path and in this new function

18
00:00:52,60 --> 00:00:57,60
we're telling it to explicitly find files ending in dot git.

19
00:00:57,60 --> 00:00:59,70
So if any file ends in dot git

20
00:00:59,70 --> 00:01:03,00
that must mean a dot git repository already exists

21
00:01:03,00 --> 00:01:06,10
in the directory our user is currently inside of.

22
00:01:06,10 --> 00:01:09,30
What should we do if a git repository already exists?

23
00:01:09,30 --> 00:01:11,40
Let's tell our user it isn't possible

24
00:01:11,40 --> 00:01:14,10
to initialize a new repository since

25
00:01:14,10 --> 00:01:17,50
a repository already exists here.

26
00:01:17,50 --> 00:01:20,80
So, we're going to console dot log back

27
00:01:20,80 --> 00:01:22,90
so the user can read the message.

28
00:01:22,90 --> 00:01:24,40
And we're going to make use of chalk again

29
00:01:24,40 --> 00:01:27,80
to make this output a little more aesthetic in the console.

30
00:01:27,80 --> 00:01:31,00
So I'm just going to use chalk red and then we're going to

31
00:01:31,00 --> 00:01:33,70
pass it the message which is just a string.

32
00:01:33,70 --> 00:01:37,10
The message will output to the user is just

33
00:01:37,10 --> 00:01:41,80
sorry, can't create a new git repo

34
00:01:41,80 --> 00:01:49,10
because this directory is already inside a git repository.

35
00:01:49,10 --> 00:01:50,50
And once we close our string there,

36
00:01:50,50 --> 00:01:53,10
you'll see that our brackets become white again.

37
00:01:53,10 --> 00:01:55,50
We want to make sure we've got that.

38
00:01:55,50 --> 00:01:59,20
You also have to make sure just to close your statement.

39
00:01:59,20 --> 00:02:01,30
And once we've told the user that

40
00:02:01,30 --> 00:02:03,40
they're not able to create the repository,

41
00:02:03,40 --> 00:02:05,50
we'll just go down to a new line here

42
00:02:05,50 --> 00:02:08,90
and all we want to do here is exit the process,

43
00:02:08,90 --> 00:02:11,10
all the processes that are currently going on

44
00:02:11,10 --> 00:02:15,90
so that the user can't create an additional git repository

45
00:02:15,90 --> 00:02:18,60
inside a current git repository.

46
00:02:18,60 --> 00:02:22,00
This will stop the program from going any further.

