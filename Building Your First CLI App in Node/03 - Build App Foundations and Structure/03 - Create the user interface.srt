1
00:00:00,50 --> 00:00:02,40
- [Instructor] If you think about building a house,

2
00:00:02,40 --> 00:00:05,20
so far what we've been doing is building the foundations

3
00:00:05,20 --> 00:00:07,70
and setting in the pipes and things.

4
00:00:07,70 --> 00:00:09,90
Now we get to pretty things up a bit.

5
00:00:09,90 --> 00:00:13,00
In this video, we're going to build a user interface.

6
00:00:13,00 --> 00:00:14,60
Musette is a command line tool,

7
00:00:14,60 --> 00:00:16,90
but we can still make a little colorful.

8
00:00:16,90 --> 00:00:19,40
First, we're going to add one more dependency

9
00:00:19,40 --> 00:00:21,70
in our index.js file.

10
00:00:21,70 --> 00:00:24,90
We're going to set this const here as musette,

11
00:00:24,90 --> 00:00:27,20
the same name as our program,

12
00:00:27,20 --> 00:00:31,40
and we are going to require commander.

13
00:00:31,40 --> 00:00:33,70
What this is going to do is to help us manage

14
00:00:33,70 --> 00:00:35,70
the different commands we will have the user

15
00:00:35,70 --> 00:00:38,40
be able to choose from in the console.

16
00:00:38,40 --> 00:00:41,40
Typically, we would put much of this code in a run function,

17
00:00:41,40 --> 00:00:44,00
in our index.js, with a couple of other functions

18
00:00:44,00 --> 00:00:45,20
to support the app.

19
00:00:45,20 --> 00:00:46,20
With commander though,

20
00:00:46,20 --> 00:00:48,70
we're going to explicitly call dot command

21
00:00:48,70 --> 00:00:50,60
on the object we've defined as musette,

22
00:00:50,60 --> 00:00:52,60
which is going to be the commander

23
00:00:52,60 --> 00:00:55,20
of all of the commands that we define.

24
00:00:55,20 --> 00:00:57,50
Commander has created a really clean structure for us

25
00:00:57,50 --> 00:00:58,80
to work with.

26
00:00:58,80 --> 00:01:01,60
So first, call your const that you set up,

27
00:01:01,60 --> 00:01:04,80
which for me is musette,

28
00:01:04,80 --> 00:01:06,10
and then after that,

29
00:01:06,10 --> 00:01:09,90
we're going to set up a chain of methods.

30
00:01:09,90 --> 00:01:11,80
The first of the methods in the commander chain

31
00:01:11,80 --> 00:01:14,10
is called command.

32
00:01:14,10 --> 00:01:16,20
Here we want to pass an argument

33
00:01:16,20 --> 00:01:18,70
that defines what the command is called.

34
00:01:18,70 --> 00:01:20,40
Let's call this one init,

35
00:01:20,40 --> 00:01:23,90
because it's the first of the commands our program will call

36
00:01:23,90 --> 00:01:27,80
when a user starts up the CLI app.

37
00:01:27,80 --> 00:01:31,40
The next method in our chain is description.

38
00:01:31,40 --> 00:01:32,80
So hit enter to go to a new line,

39
00:01:32,80 --> 00:01:35,10
and type dot description.

40
00:01:35,10 --> 00:01:38,00
Description is also going to take a string.

41
00:01:38,00 --> 00:01:40,20
This should be a description of the functionality

42
00:01:40,20 --> 00:01:41,70
of your command.

43
00:01:41,70 --> 00:01:42,50
Here I'm going to say,

44
00:01:42,50 --> 00:01:45,00
this going to draw the app banner,

45
00:01:45,00 --> 00:01:47,20
and you'll see what that looks like in a minute.

46
00:01:47,20 --> 00:01:50,10
Earlier we installed chalk as a dependency,

47
00:01:50,10 --> 00:01:51,30
we're going to make use of that now

48
00:01:51,30 --> 00:01:53,20
to display a banner in the console,

49
00:01:53,20 --> 00:01:55,30
with the name of the app, musette,

50
00:01:55,30 --> 00:01:57,10
so hit enter once more

51
00:01:57,10 --> 00:02:00,30
and begin with the commander method action.

52
00:02:00,30 --> 00:02:03,70
The action is a function that is going to take a function,

53
00:02:03,70 --> 00:02:05,90
which we will define for our program.

54
00:02:05,90 --> 00:02:08,50
What we want to do here is create some output

55
00:02:08,50 --> 00:02:10,60
the user can see in the console,

56
00:02:10,60 --> 00:02:13,10
and we can use console.log for that.

57
00:02:13,10 --> 00:02:15,90
First though, let's put in a clear command.

58
00:02:15,90 --> 00:02:17,30
This will clear the terminal

59
00:02:17,30 --> 00:02:19,80
of any other things that have been run in it,

60
00:02:19,80 --> 00:02:22,80
and make a really nice clean space for our app to run in.

61
00:02:22,80 --> 00:02:25,70
Then we'll make sure our console log is below that,

62
00:02:25,70 --> 00:02:28,60
and what this is going to do is print the name of the app.

63
00:02:28,60 --> 00:02:32,90
So we're just going to use chalk's magenta method,

64
00:02:32,90 --> 00:02:34,20
and then inside of that,

65
00:02:34,20 --> 00:02:38,20
we're making use of another of our install modules, figlet,

66
00:02:38,20 --> 00:02:40,20
and its method textSync.

67
00:02:40,20 --> 00:02:43,20
You'll see what this all does in just a minute.

68
00:02:43,20 --> 00:02:47,20
Let's make sure all of our statements are closed,

69
00:02:47,20 --> 00:02:49,40
then we save our file,

70
00:02:49,40 --> 00:02:53,50
so figlet's textSync actually takes two arguments,

71
00:02:53,50 --> 00:02:56,80
the first, a string, is the output text

72
00:02:56,80 --> 00:03:01,20
the console will display.

73
00:03:01,20 --> 00:03:02,90
We want our app name here,

74
00:03:02,90 --> 00:03:05,10
and the second is a layout.

75
00:03:05,10 --> 00:03:09,40
Let's set ours to horizontal layout,

76
00:03:09,40 --> 00:03:13,40
and that can take a property full.

77
00:03:13,40 --> 00:03:15,50
Finally, before commander will run,

78
00:03:15,50 --> 00:03:20,00
we need a way for it too understand the command line input,

79
00:03:20,00 --> 00:03:23,30
we must therefore parse the command line arguments received

80
00:03:23,30 --> 00:03:26,50
like so.

81
00:03:26,50 --> 00:03:31,30
Just going to use this parse,

82
00:03:31,30 --> 00:03:35,90
and this process.argv is something you'll see quite often.

83
00:03:35,90 --> 00:03:37,30
So what we want to check

84
00:03:37,30 --> 00:03:39,20
is that there are command line arguments

85
00:03:39,20 --> 00:03:45,40
being sent in to the app.

86
00:03:45,40 --> 00:03:49,40
So we're checking how many arguments are sent in,

87
00:03:49,40 --> 00:03:51,20
and with this exclamation mark,

88
00:03:51,20 --> 00:03:53,30
if no arguments are being sent in,

89
00:03:53,30 --> 00:03:57,20
if there's no value for the length of the list of arguments,

90
00:03:57,20 --> 00:03:59,80
we want to run the help command to help the user

91
00:03:59,80 --> 00:04:03,40
figure out how to better use the app.

92
00:04:03,40 --> 00:04:06,20
Now let's save, and go back to the terminal

93
00:04:06,20 --> 00:04:07,90
you've been working in.

94
00:04:07,90 --> 00:04:11,60
Simply run node index.js init,

95
00:04:11,60 --> 00:04:13,10
and see what happens.

96
00:04:13,10 --> 00:04:14,30
If any errors show up,

97
00:04:14,30 --> 00:04:17,50
you can try running MPM install again.

98
00:04:17,50 --> 00:04:19,20
Now that you've seen what happens,

99
00:04:19,20 --> 00:04:22,00
you can try some other colors out in the command line.

