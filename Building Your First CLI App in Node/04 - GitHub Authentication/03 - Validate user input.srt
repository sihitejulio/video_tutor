1
00:00:00,50 --> 00:00:01,80
- [Instructor] One other thing we can do

2
00:00:01,80 --> 00:00:05,30
is be sure that the user has entered information

3
00:00:05,30 --> 00:00:07,00
in the appropriate fields.

4
00:00:07,00 --> 00:00:09,30
So we can just validate this.

5
00:00:09,30 --> 00:00:12,60
On the line immediately after your enter username line,

6
00:00:12,60 --> 00:00:16,00
create another object called validate.

7
00:00:16,00 --> 00:00:18,40
This will create a function

8
00:00:18,40 --> 00:00:20,70
that checks the user's entered input

9
00:00:20,70 --> 00:00:22,70
just to be sure that an input has been given.

10
00:00:22,70 --> 00:00:26,10
So that argument is going to be value.

11
00:00:26,10 --> 00:00:27,20
And basically what we have to do

12
00:00:27,20 --> 00:00:28,40
is much like what we did earlier.

13
00:00:28,40 --> 00:00:32,20
And we're just going to check the length here

14
00:00:32,20 --> 00:00:34,60
of how many values were passed in.

15
00:00:34,60 --> 00:00:37,80
If more than zero things were entered

16
00:00:37,80 --> 00:00:42,60
then this should return true.

17
00:00:42,60 --> 00:00:45,80
Otherwise if we haven't received any input from the user,

18
00:00:45,80 --> 00:00:47,20
let's return a message to them

19
00:00:47,20 --> 00:00:50,20
that just asks them to try again.

20
00:00:50,20 --> 00:00:52,00
Below our username function,

21
00:00:52,00 --> 00:00:53,60
you're going to build the same structure

22
00:00:53,60 --> 00:00:55,60
for the password question.

23
00:00:55,60 --> 00:00:57,30
Let's just make sure to put a comma here

24
00:00:57,30 --> 00:00:58,90
after this squiggly bracket

25
00:00:58,90 --> 00:01:00,30
that's beside your square bracket.

26
00:01:00,30 --> 00:01:02,90
Bring that square bracket down to the end,

27
00:01:02,90 --> 00:01:05,90
and open new brackets right here.

28
00:01:05,90 --> 00:01:07,90
And in here we're just going to copy and paste

29
00:01:07,90 --> 00:01:09,30
what we already have,

30
00:01:09,30 --> 00:01:11,70
because the structure is going to be identical

31
00:01:11,70 --> 00:01:14,30
for the password as it is for the username.

32
00:01:14,30 --> 00:01:20,60
The difference is that this time our name is password,

33
00:01:20,60 --> 00:01:24,20
and our type is also going to be password.

34
00:01:24,20 --> 00:01:26,90
The types here are the same as those we find in HTML forms.

35
00:01:26,90 --> 00:01:30,50
So update the message and return statement

36
00:01:30,50 --> 00:01:36,00
to prompt the user with enter your password.

37
00:01:36,00 --> 00:01:37,40
Otherwise the function and structure

38
00:01:37,40 --> 00:01:39,70
remain pretty much identical.

39
00:01:39,70 --> 00:01:41,40
If we go to the end here

40
00:01:41,40 --> 00:01:44,20
this return statement is now going to return the values

41
00:01:44,20 --> 00:01:46,50
for both of these.

42
00:01:46,50 --> 00:01:47,40
Let's just take a look

43
00:01:47,40 --> 00:01:50,20
and make sure all of our brackets are closed.

44
00:01:50,20 --> 00:01:51,90
It looks like we're looking good.

45
00:01:51,90 --> 00:01:53,00
Great work.

