1
00:00:00,50 --> 00:00:03,00
- [Instructor] Once you have imported the desired modules,

2
00:00:03,00 --> 00:00:07,90
you can use module.exports to import a code from the modules

3
00:00:07,90 --> 00:00:10,70
into your application.

4
00:00:10,70 --> 00:00:14,00
With these two commands, the module.exports

5
00:00:14,00 --> 00:00:15,90
and the require we just wrote,

6
00:00:15,90 --> 00:00:19,20
what happens is that a module object is created.

7
00:00:19,20 --> 00:00:21,00
Remember that we're working in Node.js

8
00:00:21,00 --> 00:00:23,40
and all of our functions, returns, and objects

9
00:00:23,40 --> 00:00:26,70
work just like Vanilla JavaScript for the most part.

10
00:00:26,70 --> 00:00:29,10
The module.exports is a special object

11
00:00:29,10 --> 00:00:31,10
which is included in every js file

12
00:00:31,10 --> 00:00:34,10
in the Node.js application by default.

13
00:00:34,10 --> 00:00:35,40
In it's most basic structure,

14
00:00:35,40 --> 00:00:37,50
it looks something like this.

15
00:00:37,50 --> 00:00:39,50
So you can imagine a structure like this

16
00:00:39,50 --> 00:00:41,90
being inside any module's file.

17
00:00:41,90 --> 00:00:44,30
Everything is packaged up and made available for use

18
00:00:44,30 --> 00:00:46,00
through the require call.

19
00:00:46,00 --> 00:00:48,10
When you then you module.exports,

20
00:00:48,10 --> 00:00:51,00
you'll attach properties to this object we see here

21
00:00:51,00 --> 00:00:53,70
and return statements to return the desired values

22
00:00:53,70 --> 00:00:56,60
back to the module by the require statement.

23
00:00:56,60 --> 00:00:59,20
Let's see that in action in our own app.

24
00:00:59,20 --> 00:01:01,40
The first thing we're going to do is add another function

25
00:01:01,40 --> 00:01:05,20
called askGitHubCredentials.

26
00:01:05,20 --> 00:01:07,80
Inside this new askGitHubCredentials function,

27
00:01:07,80 --> 00:01:10,90
define a variable or constant questions

28
00:01:10,90 --> 00:01:14,20
in order to receive the information received from the user.

29
00:01:14,20 --> 00:01:17,60
This variable will be set to contain an array.

30
00:01:17,60 --> 00:01:19,90
You will need an array to store the information

31
00:01:19,90 --> 00:01:22,00
since there are two questions you will ask

32
00:01:22,00 --> 00:01:23,40
in your user prompt.

33
00:01:23,40 --> 00:01:27,00
First, the username, and then the password.

34
00:01:27,00 --> 00:01:29,80
Each questions requires its own function and logic

35
00:01:29,80 --> 00:01:32,50
and both will live inside the constant questions

36
00:01:32,50 --> 00:01:35,10
so that the values from both questions are returned

37
00:01:35,10 --> 00:01:36,60
to that constant.

38
00:01:36,60 --> 00:01:38,70
Inside the questions constant right now,

39
00:01:38,70 --> 00:01:43,30
you want to create a new prompt to prompt for the username.

40
00:01:43,30 --> 00:01:45,20
So let's set that in.

41
00:01:45,20 --> 00:01:49,10
Each question is an object, which defines the name

42
00:01:49,10 --> 00:01:51,60
of the field as well as the type.

43
00:01:51,60 --> 00:01:55,90
The name of our field here is username.

44
00:01:55,90 --> 00:01:58,20
And the type

45
00:01:58,20 --> 00:01:59,90
is input.

46
00:01:59,90 --> 00:02:02,80
You also need to define the prompt for the user,

47
00:02:02,80 --> 00:02:05,90
which will be our message to the user.

48
00:02:05,90 --> 00:02:09,10
So, let's just say to the user,

49
00:02:09,10 --> 00:02:12,50
"Enter your GitHub username or

50
00:02:12,50 --> 00:02:16,40
"e-mail address."

51
00:02:16,40 --> 00:02:19,00
Make sure that we close this here.

52
00:02:19,00 --> 00:02:22,00
And then at the end, we want to make sure we have a return

53
00:02:22,00 --> 00:02:24,00
for the information that's been put in here.

