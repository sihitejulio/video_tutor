1
00:00:00,50 --> 00:00:02,10
- [Instructor] What we have completed so far

2
00:00:02,10 --> 00:00:04,60
has created must of the functionality in the program

3
00:00:04,60 --> 00:00:07,10
to receive GitHub credentials.

4
00:00:07,10 --> 00:00:09,30
However, your local computer or program

5
00:00:09,30 --> 00:00:10,50
are not themselves

6
00:00:10,50 --> 00:00:12,50
yet equipped to sign into GitHub,

7
00:00:12,50 --> 00:00:16,50
since the program is not yet connected to the GitHub API.

8
00:00:16,50 --> 00:00:19,60
In order to have the user actually sign in to GitHub,

9
00:00:19,60 --> 00:00:21,90
you will have to retrieve an OAuth token

10
00:00:21,90 --> 00:00:23,80
for the GitHub API.

11
00:00:23,80 --> 00:00:26,30
We will exchange the username and password

12
00:00:26,30 --> 00:00:27,90
we received in our program

13
00:00:27,90 --> 00:00:30,00
for a GitHub token.

14
00:00:30,00 --> 00:00:32,10
Once we have the credentials from the user,

15
00:00:32,10 --> 00:00:33,50
we don't want them to have to sign in

16
00:00:33,50 --> 00:00:35,50
every time they use the tool.

17
00:00:35,50 --> 00:00:38,70
Instead, what we'll do is store the OAuth token

18
00:00:38,70 --> 00:00:40,80
so it's always available to the user.

19
00:00:40,80 --> 00:00:42,60
How do we do that?

20
00:00:42,60 --> 00:00:43,70
At its most basic,

21
00:00:43,70 --> 00:00:46,10
storing config is rather simple.

22
00:00:46,10 --> 00:00:47,50
You could do it without a package

23
00:00:47,50 --> 00:00:51,00
by simply reading and writing to and from a JSON file.

24
00:00:51,00 --> 00:00:52,00
We will, however,

25
00:00:52,00 --> 00:00:54,40
use the configstore package in this project

26
00:00:54,40 --> 00:00:58,30
because it provides a few key advantages.

27
00:00:58,30 --> 00:01:00,00
First, the package determines

28
00:01:00,00 --> 00:01:02,90
the most appropriate location for the file for you,

29
00:01:02,90 --> 00:01:03,90
taking into account

30
00:01:03,90 --> 00:01:06,90
your operating system and the current user.

31
00:01:06,90 --> 00:01:07,70
And second,

32
00:01:07,70 --> 00:01:10,90
there's no need to explicitly read or write to the file.

33
00:01:10,90 --> 00:01:13,20
You simply modify a configstore object

34
00:01:13,20 --> 00:01:15,80
and that's all taken care of in the background.

35
00:01:15,80 --> 00:01:16,60
For the code,

36
00:01:16,60 --> 00:01:19,70
we'll add this configstore in our GitHub credentials file

37
00:01:19,70 --> 00:01:21,10
since the GitHub credentials file

38
00:01:21,10 --> 00:01:25,40
is going to be handling all of our GitHub authentication.

39
00:01:25,40 --> 00:01:27,60
Let's go in there through our lib folder

40
00:01:27,60 --> 00:01:30,70
and open the GitHub credentials file.

41
00:01:30,70 --> 00:01:36,90
Begin by requiring the configstore module.

42
00:01:36,90 --> 00:01:38,20
And then what we're going to do

43
00:01:38,20 --> 00:01:42,00
is create an instance of configstore

44
00:01:42,00 --> 00:01:44,20
and pass it an application identifier.

45
00:01:44,20 --> 00:01:46,90
How is knows what our application is called.

46
00:01:46,90 --> 00:01:50,20
So we're going to set that in this conf const.

47
00:01:50,20 --> 00:01:51,20
And creating this instance

48
00:01:51,20 --> 00:01:54,20
just looks like new configstore,

49
00:01:54,20 --> 00:01:56,30
which is just calling the module,

50
00:01:56,30 --> 00:01:59,60
and then in here we're putting package.name.

51
00:01:59,60 --> 00:02:02,60
This identifier is using a name method on package

52
00:02:02,60 --> 00:02:04,30
to pull out the name of your application

53
00:02:04,30 --> 00:02:06,00
from the package JSON.

54
00:02:06,00 --> 00:02:09,50
Since we want to access something from another file here,

55
00:02:09,50 --> 00:02:11,40
the package name from the package JSON,

56
00:02:11,40 --> 00:02:14,10
we'll also need to require that file.

57
00:02:14,10 --> 00:02:16,90
And we want to make sure that we have that require here

58
00:02:16,90 --> 00:02:20,80
in before we're actually requiring the information we need.

59
00:02:20,80 --> 00:02:23,80
So if we set this const package here

60
00:02:23,80 --> 00:02:25,90
right before the configstore,

61
00:02:25,90 --> 00:02:28,90
this will make sure that the file is accessible

62
00:02:28,90 --> 00:02:33,40
to configstore when it's asking for package.name.

63
00:02:33,40 --> 00:02:35,40
If a configstore file doesn't exist,

64
00:02:35,40 --> 00:02:37,80
you will be returned an empty object

65
00:02:37,80 --> 00:02:40,50
and the file will then be created in the background.

66
00:02:40,50 --> 00:02:42,10
If there is already a configstore file,

67
00:02:42,10 --> 00:02:44,40
the contents will be decoded into JSON

68
00:02:44,40 --> 00:02:46,70
and made available to your application.

69
00:02:46,70 --> 00:02:49,40
Then you can use your variable or constant conf

70
00:02:49,40 --> 00:02:51,00
as a simple object

71
00:02:51,00 --> 00:02:54,00
and get or set properties on it as needed.

72
00:02:54,00 --> 00:02:54,90
As mentioned before,

73
00:02:54,90 --> 00:02:57,50
you won't need to worry about saving it afterwards.

74
00:02:57,50 --> 00:02:59,00
The configstore module

75
00:02:59,00 --> 00:03:02,00
takes care of all that functionality for you.

