1
00:00:00,50 --> 00:00:01,90
- [Instructor] Now that we have the power to determine

2
00:00:01,90 --> 00:00:05,00
a Git repository has not yet been initialized

3
00:00:05,00 --> 00:00:07,30
and the user does, in fact, require a new

4
00:00:07,30 --> 00:00:08,80
Git repository for their project,

5
00:00:08,80 --> 00:00:12,00
let's create some prompts for the user to answer questions

6
00:00:12,00 --> 00:00:16,20
about the details of the Git repository they want to create.

7
00:00:16,20 --> 00:00:18,90
The very first thing we need to do is ask the user

8
00:00:18,90 --> 00:00:20,90
for their GitHub credentials.

9
00:00:20,90 --> 00:00:24,70
We'll use the Inquirer module to create these prompts.

10
00:00:24,70 --> 00:00:26,50
If you've used GitHub before,

11
00:00:26,50 --> 00:00:28,10
you know what this looks like.

12
00:00:28,10 --> 00:00:29,20
When you go to Push,

13
00:00:29,20 --> 00:00:31,10
it immediately asks for your username,

14
00:00:31,10 --> 00:00:33,30
and when that is entered it asks for

15
00:00:33,30 --> 00:00:35,20
your password right after.

16
00:00:35,20 --> 00:00:38,00
We'll have the same functionality in our app.

17
00:00:38,00 --> 00:00:40,40
Inquirer has methods included for a variety

18
00:00:40,40 --> 00:00:42,30
of different kinds of prompts.

19
00:00:42,30 --> 00:00:46,40
It's roughly similar to HTML form controls, in fact.

20
00:00:46,40 --> 00:00:48,90
To collect the user's GitHub username and password,

21
00:00:48,90 --> 00:00:53,00
we'll use Inquirer's input and password types.

22
00:00:53,00 --> 00:00:56,50
Right now this isn't actually connected to GitHub.

23
00:00:56,50 --> 00:00:58,50
All we are doing is setting up a form

24
00:00:58,50 --> 00:01:02,40
inside of the local app to receive the information.

25
00:01:02,40 --> 00:01:04,30
Before we implement these methods,

26
00:01:04,30 --> 00:01:06,20
we need somewhere to actually store them

27
00:01:06,20 --> 00:01:07,70
once we receive them.

28
00:01:07,70 --> 00:01:09,80
We're going to do that in the Inquirer file

29
00:01:09,80 --> 00:01:12,20
in your lib folder.

30
00:01:12,20 --> 00:01:16,60
The file path will be lib/inquirer.js.

31
00:01:16,60 --> 00:01:18,50
In our inquirer.js file,

32
00:01:18,50 --> 00:01:21,70
the first thing we need to do is import the modules

33
00:01:21,70 --> 00:01:27,70
we will be using, inquirer, minimist, and files.

34
00:01:27,70 --> 00:01:31,60
As in any Node.js code, we do this by

35
00:01:31,60 --> 00:01:33,80
requiring the modules and storing them

36
00:01:33,80 --> 00:01:37,50
in a variable or constant.

37
00:01:37,50 --> 00:01:41,40
This require here with files is again requiring files

38
00:01:41,40 --> 00:01:46,00
from our own directories for our app.

