1
00:00:00,50 --> 00:00:01,70
- [Instructor] The next thing we're going to do

2
00:00:01,70 --> 00:00:04,70
is build our registerNewToken function.

3
00:00:04,70 --> 00:00:06,70
Let's use a try statement to check whether

4
00:00:06,70 --> 00:00:09,40
authentication and registering a new oauth token

5
00:00:09,40 --> 00:00:10,90
works or not.

6
00:00:10,90 --> 00:00:14,00
First, we will try to get the token, as defined here,

7
00:00:14,00 --> 00:00:18,20
and we're going to store this inside of response const,

8
00:00:18,20 --> 00:00:20,80
and what we're doing is waiting

9
00:00:20,80 --> 00:00:26,40
for our our octokit to return something to us.

10
00:00:26,40 --> 00:00:28,50
Inside of this createAuthorization,

11
00:00:28,50 --> 00:00:30,50
we also need to set some scopes,

12
00:00:30,50 --> 00:00:33,00
and a note for whoever receives the token

13
00:00:33,00 --> 00:00:35,50
about what app we have.

14
00:00:35,50 --> 00:00:39,70
We're setting these for user, public_repo, repo,

15
00:00:39,70 --> 00:00:42,10
as well as the repo:status.

16
00:00:42,10 --> 00:00:45,40
The last thing we're going to set in here is a note.

17
00:00:45,40 --> 00:00:49,00
This note is going to be registered on all of the tokens

18
00:00:49,00 --> 00:00:51,30
that you place on other users' profiles,

19
00:00:51,30 --> 00:00:53,20
so this is just to let them know

20
00:00:53,20 --> 00:00:55,90
what app has been there with a token.

21
00:00:55,90 --> 00:00:56,90
So I'm going to say

22
00:00:56,90 --> 00:01:02,70
"musette: a cool tool for dev workflow automation."

23
00:01:02,70 --> 00:01:05,40
These scopes are keys which specify the permissions

24
00:01:05,40 --> 00:01:07,20
the application requires.

25
00:01:07,20 --> 00:01:09,30
If you don't specify a scopes value,

26
00:01:09,30 --> 00:01:11,50
your application will only have read access

27
00:01:11,50 --> 00:01:13,30
to the user's public data:

28
00:01:13,30 --> 00:01:16,10
repository user info, et cetera.

29
00:01:16,10 --> 00:01:18,70
So here we're setting some scopes for the user,

30
00:01:18,70 --> 00:01:22,80
public repository, repo, the repo:status,

31
00:01:22,80 --> 00:01:25,10
and in our note, we've just let

32
00:01:25,10 --> 00:01:28,00
whoever receives the token know that musette was there.

33
00:01:28,00 --> 00:01:29,00
So the note is just

34
00:01:29,00 --> 00:01:32,30
"musette: a cool tool for dev workflow automation."

35
00:01:32,30 --> 00:01:34,40
Let's make sure to close our statement here

36
00:01:34,40 --> 00:01:36,10
before we move on,

37
00:01:36,10 --> 00:01:39,40
and now what we need to do is store a token

38
00:01:39,40 --> 00:01:41,40
if it is retrieved.

39
00:01:41,40 --> 00:01:44,20
So we're going to set a const token

40
00:01:44,20 --> 00:01:49,50
and let that equal response.data.token,

41
00:01:49,50 --> 00:01:52,70
which just means that any token data in the response

42
00:01:52,70 --> 00:01:54,80
will be stored here.

43
00:01:54,80 --> 00:01:56,70
Again, we want to know what happens

44
00:01:56,70 --> 00:01:59,20
if the token is retrieved.

45
00:01:59,20 --> 00:02:02,20
In this case, we want to set it

46
00:02:02,20 --> 00:02:06,50
in to that conf we set up earlier,

47
00:02:06,50 --> 00:02:09,50
and what we'll be setting is the token value

48
00:02:09,50 --> 00:02:12,60
that we find in our github_credentials,

49
00:02:12,60 --> 00:02:14,60
which has the type token,

50
00:02:14,60 --> 00:02:16,80
and of course we want to return that token

51
00:02:16,80 --> 00:02:18,60
if it's been retrieved.

52
00:02:18,60 --> 00:02:21,20
What happens if our authentication fails though?

53
00:02:21,20 --> 00:02:23,80
Of course, we hope things go right every single time,

54
00:02:23,80 --> 00:02:25,00
but sometimes they don't,

55
00:02:25,00 --> 00:02:27,20
and our program needs to account for that.

56
00:02:27,20 --> 00:02:29,90
In the else statement for your function,

57
00:02:29,90 --> 00:02:33,80
you will create a fail safe to catch errors.

58
00:02:33,80 --> 00:02:37,70
We just say throw new Error,

59
00:02:37,70 --> 00:02:39,30
and then give the error type,

60
00:02:39,30 --> 00:02:42,80
which, here, is Missing Token,

61
00:02:42,80 --> 00:02:45,40
and then an error message to output to users.

62
00:02:45,40 --> 00:02:52,40
So we could say, "uh oh, GitHub token was not retrieved."

63
00:02:52,40 --> 00:02:54,00
Then the user knows what's going on

64
00:02:54,00 --> 00:02:56,40
and why the program is failing.

65
00:02:56,40 --> 00:02:57,70
I'm going to scroll up a little bit

66
00:02:57,70 --> 00:03:00,00
just so we can take a look at this.

67
00:03:00,00 --> 00:03:01,90
And the final thing we have to do is

68
00:03:01,90 --> 00:03:05,20
set in a catch(error) to catch the entire promise

69
00:03:05,20 --> 00:03:07,00
if it fails.

70
00:03:07,00 --> 00:03:11,40
In this case, all we want to do is just throw an error.

71
00:03:11,40 --> 00:03:13,90
It looks like we might have one extra bracket in here,

72
00:03:13,90 --> 00:03:16,60
so let's just take this one here out,

73
00:03:16,60 --> 00:03:18,20
and that should fix it.

74
00:03:18,20 --> 00:03:19,60
Once you save,

75
00:03:19,60 --> 00:03:21,40
we've completed all of our functions

76
00:03:21,40 --> 00:03:23,00
for our GitHub credentials file.

