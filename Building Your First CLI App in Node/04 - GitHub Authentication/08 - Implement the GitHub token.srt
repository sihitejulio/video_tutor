1
00:00:00,60 --> 00:00:03,50
- [Instructor] Now we need to move into our index.js file

2
00:00:03,50 --> 00:00:05,80
and implement a function to handle the logic

3
00:00:05,80 --> 00:00:08,10
from our ship's main bridge involved

4
00:00:08,10 --> 00:00:10,80
in acquiring the GitHub o off token.

5
00:00:10,80 --> 00:00:11,90
So, if you haven't already,

6
00:00:11,90 --> 00:00:13,70
close the GitHub credentials file

7
00:00:13,70 --> 00:00:16,30
and open up the index.js file.

8
00:00:16,30 --> 00:00:18,50
In order to use the code we just created

9
00:00:18,50 --> 00:00:20,10
at the top of our index file,

10
00:00:20,10 --> 00:00:22,80
let's call a const here GitHub.

11
00:00:22,80 --> 00:00:24,60
Just to create a shorter more readable name

12
00:00:24,60 --> 00:00:27,70
for accessing the GitHub credentials file.

13
00:00:27,70 --> 00:00:29,00
Now, we'll create a new command

14
00:00:29,00 --> 00:00:31,60
for calling up GitHub credentials.

15
00:00:31,60 --> 00:00:34,80
So after your first command here, let's call musette again

16
00:00:34,80 --> 00:00:36,90
because she's in charge of everything.

17
00:00:36,90 --> 00:00:42,60
And set a new command, which I'm going to call octocheck.

18
00:00:42,60 --> 00:00:45,40
Next we need to set the description,

19
00:00:45,40 --> 00:00:52,70
which, as we just said, is to check user GitHub credentials.

20
00:00:52,70 --> 00:00:55,20
Then, we're going to set the action.

21
00:00:55,20 --> 00:01:02,40
This time we want to set an async function

22
00:01:02,40 --> 00:01:03,90
and this is so that we can make use

23
00:01:03,90 --> 00:01:07,00
of the awake functionality that our credentials are using.

24
00:01:07,00 --> 00:01:09,00
What action do we want to be performing

25
00:01:09,00 --> 00:01:11,00
when authenticating users?

26
00:01:11,00 --> 00:01:13,80
What we need to do first is make sure

27
00:01:13,80 --> 00:01:15,90
the token is retrieved from GitHub.

28
00:01:15,90 --> 00:01:20,40
So, we're going to let our token equal

29
00:01:20,40 --> 00:01:28,40
get Stored GitHub Token and if there isn't a token,

30
00:01:28,40 --> 00:01:32,60
we need to wait for the set GitHub credentials again.

31
00:01:32,60 --> 00:01:36,00
Then, once these GitHub credentials are set,

32
00:01:36,00 --> 00:01:40,10
we're going to let the token equal whatever comes back

33
00:01:40,10 --> 00:01:41,70
from that which is going to be coming

34
00:01:41,70 --> 00:01:46,10
through register new token.

35
00:01:46,10 --> 00:01:48,70
Finally, once the token is retrieved,

36
00:01:48,70 --> 00:01:51,10
how will we know that this has happened?

37
00:01:51,10 --> 00:01:53,50
Right now inside of this if statement,

38
00:01:53,50 --> 00:01:59,00
let's just do a console.log of the token to the terminal,

39
00:01:59,00 --> 00:02:02,10
so that we can see that it has been returned.

40
00:02:02,10 --> 00:02:04,30
This will show up as a sort of long,

41
00:02:04,30 --> 00:02:06,90
strange string of numbers and letters.

42
00:02:06,90 --> 00:02:09,70
Make sure to close this here as well.

43
00:02:09,70 --> 00:02:12,80
And just to summarize, since there's a lot going on here,

44
00:02:12,80 --> 00:02:14,90
what we've done is retrieved a token

45
00:02:14,90 --> 00:02:17,40
through the get stored GitHub token method,

46
00:02:17,40 --> 00:02:19,70
which is in our GitHub credentials file

47
00:02:19,70 --> 00:02:21,30
and which is now stored inside

48
00:02:21,30 --> 00:02:23,90
of our GitHub variable here in the index.

49
00:02:23,90 --> 00:02:28,50
If we find a token, then, quite simply, return that token.

50
00:02:28,50 --> 00:02:31,80
Otherwise, we'll need to prompt for GitHub credentials.

51
00:02:31,80 --> 00:02:34,10
Once those credentials are secured,

52
00:02:34,10 --> 00:02:35,60
and set in our token as defined

53
00:02:35,60 --> 00:02:37,50
in the register new token method,

54
00:02:37,50 --> 00:02:40,10
which is also in our GitHub credentials file,

55
00:02:40,10 --> 00:02:42,20
then we're going to return the token.

56
00:02:42,20 --> 00:02:45,40
So if we switch from here and go

57
00:02:45,40 --> 00:02:47,90
into our terminal right now.

58
00:02:47,90 --> 00:02:52,40
Now, if you run node index.js octocheck,

59
00:02:52,40 --> 00:02:54,50
which is the command we just created,

60
00:02:54,50 --> 00:02:57,30
in your console and hit enter,

61
00:02:57,30 --> 00:03:00,00
you'll be prompted to enter your GitHub user name.

62
00:03:00,00 --> 00:03:04,40
Use your real GitHub user name and password here

63
00:03:04,40 --> 00:03:06,90
and you'll see the prompt for your password.

64
00:03:06,90 --> 00:03:09,00
Type that in, but don't hit enter yet.

65
00:03:09,00 --> 00:03:12,20
In future, what this is going to do is generate a token

66
00:03:12,20 --> 00:03:14,70
we can see in our GitHub profile.

67
00:03:14,70 --> 00:03:17,10
But, we haven't finished writing our program yet.

68
00:03:17,10 --> 00:03:22,50
So we don't have any fail safe for this action and command.

69
00:03:22,50 --> 00:03:25,00
Since it is an async function,

70
00:03:25,00 --> 00:03:28,00
it's structured as a promise and it needs that try,

71
00:03:28,00 --> 00:03:31,00
catch, error, fail safe at the end.

