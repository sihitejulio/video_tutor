1
00:00:00,10 --> 00:00:01,50
- [Instructor] Now that your program

2
00:00:01,50 --> 00:00:03,90
has the ability to accept credentials from GitHub,

3
00:00:03,90 --> 00:00:05,80
and you have created a Configstore,

4
00:00:05,80 --> 00:00:08,70
so that received credentials can be saved and reused,

5
00:00:08,70 --> 00:00:11,60
a library for handling the GitHub oauth token itself,

6
00:00:11,60 --> 00:00:13,40
needs to be created.

7
00:00:13,40 --> 00:00:16,20
GitHub oauth allows other apps to login to GitHub

8
00:00:16,20 --> 00:00:20,10
via their API or application programming interface.

9
00:00:20,10 --> 00:00:22,90
This typically involves sending user credentials

10
00:00:22,90 --> 00:00:24,70
for your app to GitHub,

11
00:00:24,70 --> 00:00:26,90
and GitHub then authenticating the credentials

12
00:00:26,90 --> 00:00:28,90
and sending them back a token,

13
00:00:28,90 --> 00:00:31,90
okaying the users access to the program.

14
00:00:31,90 --> 00:00:34,90
Your program then needs to store that token inside your app,

15
00:00:34,90 --> 00:00:37,10
so that the user can access it.

16
00:00:37,10 --> 00:00:39,60
So for now, we're going to go inside the lib folder

17
00:00:39,60 --> 00:00:41,80
and into our GitHub credentials files,

18
00:00:41,80 --> 00:00:44,10
and then we're going to require the necessary modules

19
00:00:44,10 --> 00:00:45,50
for the GitHub credentials file,

20
00:00:45,50 --> 00:00:49,20
work, octokit and lowdash.

21
00:00:49,20 --> 00:00:54,90
So we're just going to require octokit like this.

22
00:00:54,90 --> 00:00:59,00
Adding octokit allows us to access GitHub's rest API.

23
00:00:59,00 --> 00:01:01,80
We will also require lowdash.

24
00:01:01,80 --> 00:01:06,60
We will set it in a const like this.

25
00:01:06,60 --> 00:01:09,40
Lowdash offers a set of general purpose utilities

26
00:01:09,40 --> 00:01:11,10
for JavaScript dubs,

27
00:01:11,10 --> 00:01:13,20
that just allows them to simplify and maintain

28
00:01:13,20 --> 00:01:16,10
a variety of different programming functions.

29
00:01:16,10 --> 00:01:18,80
Once you've required the modules and files needed

30
00:01:18,80 --> 00:01:20,70
in your GitHub credentials file.

31
00:01:20,70 --> 00:01:23,20
Create a new function, module.exports,

32
00:01:23,20 --> 00:01:25,60
just like we did for our username and password prompts

33
00:01:25,60 --> 00:01:28,90
in the inquirer.js file.

34
00:01:28,90 --> 00:01:31,00
Inside the module.exports,

35
00:01:31,00 --> 00:01:32,20
the first thing we're going to do

36
00:01:32,20 --> 00:01:37,80
is create a new instance of octokit for user access.

37
00:01:37,80 --> 00:01:41,80
Inside of here, all we need to do is return octokit

38
00:01:41,80 --> 00:01:46,10
to grab instance for the user access.

39
00:01:46,10 --> 00:01:47,70
Once we have that in place,

40
00:01:47,70 --> 00:01:51,10
we can create a function for authentication.

41
00:01:51,10 --> 00:01:54,20
Let's call it gitHubAuth.

42
00:01:54,20 --> 00:01:58,90
gitHubAuth is going receive a token.

43
00:01:58,90 --> 00:02:00,50
Once it has received the token,

44
00:02:00,50 --> 00:02:05,60
we can make use of this in the octokit.authenticate method.

45
00:02:05,60 --> 00:02:09,10
Octokit's authenticate method takes a couple of params.

46
00:02:09,10 --> 00:02:12,50
Type, which is oauth,

47
00:02:12,50 --> 00:02:17,10
and token, which is set as token.

48
00:02:17,10 --> 00:02:20,10
This is true as of recording time.

49
00:02:20,10 --> 00:02:22,60
I do know that this construction of octokit.auth

50
00:02:22,60 --> 00:02:24,60
is currently being deprecated.

51
00:02:24,60 --> 00:02:27,90
Octokit's official and updated documentation

52
00:02:27,90 --> 00:02:31,20
is located at octokit.github.io.

53
00:02:31,20 --> 00:02:35,40
Other breaking changes in the update to version 16.0.1,

54
00:02:35,40 --> 00:02:38,40
have been updated in this video already.

55
00:02:38,40 --> 00:02:41,00
Next, we need to grab and store this token,

56
00:02:41,00 --> 00:02:43,20
to set the credentials for the user.

57
00:02:43,20 --> 00:02:45,00
Let's create a function for that

58
00:02:45,00 --> 00:02:49,20
called gitStoredGitHubToken.

59
00:02:49,20 --> 00:02:55,80
Inside of that, if a conf object exists,

60
00:02:55,80 --> 00:03:00,70
and it has the GitHub credentials.token property,

61
00:03:00,70 --> 00:03:04,30
this means that there is already a token in storage.

62
00:03:04,30 --> 00:03:06,80
In this case, we return the token value

63
00:03:06,80 --> 00:03:09,30
back to the invoking function.

64
00:03:09,30 --> 00:03:11,10
If no token is detected,

65
00:03:11,10 --> 00:03:13,10
we need to fetch one.

66
00:03:13,10 --> 00:03:15,10
Now we'll set up a basic structure

67
00:03:15,10 --> 00:03:17,00
for setting GitHub credentials

68
00:03:17,00 --> 00:03:19,80
and registering a new oauth token.

69
00:03:19,80 --> 00:03:22,20
So first, we'll just set this structure on here

70
00:03:22,20 --> 00:03:25,50
for setGitHubCredentials.

71
00:03:25,50 --> 00:03:29,70
This is going to be an async function,

72
00:03:29,70 --> 00:03:30,90
and then we'll set up the structure

73
00:03:30,90 --> 00:03:34,70
for one more function down here, called registerNewToken.

74
00:03:34,70 --> 00:03:36,70
We'll be building both of these out

75
00:03:36,70 --> 00:03:39,00
in the next couple of videos.

