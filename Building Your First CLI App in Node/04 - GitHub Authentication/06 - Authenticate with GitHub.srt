1
00:00:00,50 --> 00:00:02,20
- [Instructor] For the purposes of this project,

2
00:00:02,20 --> 00:00:04,90
it's probably the most simple to deactivate

3
00:00:04,90 --> 00:00:07,60
two-factor authentication on your GitHub account

4
00:00:07,60 --> 00:00:11,60
if you have two-factor authentication currently enabled.

5
00:00:11,60 --> 00:00:13,90
Now that we're connected to the GitHub API,

6
00:00:13,90 --> 00:00:16,50
and have some core functions skeleton-outlined,

7
00:00:16,50 --> 00:00:18,50
let's build out the rest of the functionality

8
00:00:18,50 --> 00:00:22,50
to authenticate our users using the GitHub API Octokit.

9
00:00:22,50 --> 00:00:24,60
Some of our GitHub methods are already set up

10
00:00:24,60 --> 00:00:26,20
in our inquirer file.

11
00:00:26,20 --> 00:00:29,00
So let's be sure to require inquirer at the top

12
00:00:29,00 --> 00:00:31,20
of our GitHub credentials file.

13
00:00:31,20 --> 00:00:32,80
Then, we're going to work inside of our

14
00:00:32,80 --> 00:00:36,60
setGitHubCredentials function near the bottom here.

15
00:00:36,60 --> 00:00:38,80
Inside of our setGitHubCredentials,

16
00:00:38,80 --> 00:00:42,90
we're going to create a const called credentials.

17
00:00:42,90 --> 00:00:45,00
This is going to store the information retrieved

18
00:00:45,00 --> 00:00:47,10
from the askGitHubCredentials prompts

19
00:00:47,10 --> 00:00:49,70
we set up in our inquirer file.

20
00:00:49,70 --> 00:00:54,30
So here, we're going to await inquirer

21
00:00:54,30 --> 00:00:58,60
and it's askGitHubCredentials.

22
00:00:58,60 --> 00:01:01,00
When we're using the command await here,

23
00:01:01,00 --> 00:01:03,60
this is an operator used to wait for a promise

24
00:01:03,60 --> 00:01:07,70
and can only be used inside of an async function.

25
00:01:07,70 --> 00:01:11,30
Reading from MDN, the await expression

26
00:01:11,30 --> 00:01:14,40
causes async function execution to pause

27
00:01:14,40 --> 00:01:16,60
until a promise is fulfilled,

28
00:01:16,60 --> 00:01:19,40
that is, that it's resolved or rejected,

29
00:01:19,40 --> 00:01:22,20
and then resumes execution of the async function

30
00:01:22,20 --> 00:01:23,60
after fulfillment.

31
00:01:23,60 --> 00:01:26,50
When resumed, the value of the await expression

32
00:01:26,50 --> 00:01:29,30
is that of a fulfilled promise.

33
00:01:29,30 --> 00:01:31,30
If the promise is rejected,

34
00:01:31,30 --> 00:01:34,70
the await expression throws the rejected value.

35
00:01:34,70 --> 00:01:36,70
That might sound a little bit complicated,

36
00:01:36,70 --> 00:01:38,20
but we are literally just waiting for the

37
00:01:38,20 --> 00:01:41,30
askGitHubCredentials to return us a value

38
00:01:41,30 --> 00:01:44,60
based on the functionality in its own function.

39
00:01:44,60 --> 00:01:46,80
Everything else in this function that we have here

40
00:01:46,80 --> 00:01:50,10
will pause until the value is returned to us.

41
00:01:50,10 --> 00:01:51,60
The next thing we're going to do is,

42
00:01:51,60 --> 00:01:54,20
inside of our getGitHubCredentials function,

43
00:01:54,20 --> 00:01:58,90
is use the octokit.authenticate method.

44
00:01:58,90 --> 00:02:02,90
What this will do is check user input in our app

45
00:02:02,90 --> 00:02:07,50
against GitHub's database via their API.

46
00:02:07,50 --> 00:02:14,00
Most of this is taken straight from their own documentation.

47
00:02:14,00 --> 00:02:17,70
So what we need to tell the Octokit GitHub API

48
00:02:17,70 --> 00:02:20,60
is what type of authentication we're doing.

49
00:02:20,60 --> 00:02:24,80
What we're doing is just a basic authentication.

50
00:02:24,80 --> 00:02:29,00
And we want the values there returned to our credentials.

