1
00:00:00,00 --> 00:00:07,10
(light music)

2
00:00:07,10 --> 00:00:08,70
- [Instructor] Now, in order to get started with

3
00:00:08,70 --> 00:00:12,40
working with CSS, we need to actually create

4
00:00:12,40 --> 00:00:14,80
an actual React project that will allows us

5
00:00:14,80 --> 00:00:16,80
to do our CSS coding.

6
00:00:16,80 --> 00:00:19,00
In the first course, we actually created a custom

7
00:00:19,00 --> 00:00:22,20
react-scripts package that provided expanded support

8
00:00:22,20 --> 00:00:26,80
for various postCSS plugins and all of the CSSNext features.

9
00:00:26,80 --> 00:00:29,60
In this particular course, we're going to create

10
00:00:29,60 --> 00:00:31,50
a new React application using

11
00:00:31,50 --> 00:00:33,70
that custom react-scripts package

12
00:00:33,70 --> 00:00:35,50
so that we'll have the latest and greatest features

13
00:00:35,50 --> 00:00:37,30
made available to us.

14
00:00:37,30 --> 00:00:39,40
Now, in this course, we're going to need

15
00:00:39,40 --> 00:00:42,10
to have Node.js installed because Node.js

16
00:00:42,10 --> 00:00:45,00
is what powers all of our development tooling

17
00:00:45,00 --> 00:00:48,50
in modern JavaScript web development projects

18
00:00:48,50 --> 00:00:51,30
and version eight is the minimum required version

19
00:00:51,30 --> 00:00:54,80
but I'll be doing all the demonstrations using version 10.

20
00:00:54,80 --> 00:00:57,00
So if you're watching this video at some point in the future

21
00:00:57,00 --> 00:00:59,60
and you're using a later version of Node

22
00:00:59,60 --> 00:01:01,60
and it doesn't work, make sure you go back

23
00:01:01,60 --> 00:01:03,40
and install version 10 and that way

24
00:01:03,40 --> 00:01:05,30
you'll be using the same version that I'm using

25
00:01:05,30 --> 00:01:07,00
to do the recordings.

26
00:01:07,00 --> 00:01:10,10
We'll be specifying our custom react-scripts package

27
00:01:10,10 --> 00:01:12,00
using the scripts-version option

28
00:01:12,00 --> 00:01:16,80
when we actually go to create our React application.

29
00:01:16,80 --> 00:01:17,80
Now, in addition to Node,

30
00:01:17,80 --> 00:01:20,60
we're going to need a text editor and a web browser.

31
00:01:20,60 --> 00:01:23,20
I recommend using Visual Studio Code.

32
00:01:23,20 --> 00:01:26,60
You can downloaded that from code.visualstudio.com.

33
00:01:26,60 --> 00:01:29,20
I recommend the Google Chrome web browser.

34
00:01:29,20 --> 00:01:31,10
That's kind of the defacto standard web browser

35
00:01:31,10 --> 00:01:32,00
for web development.

36
00:01:32,00 --> 00:01:34,70
It's got great CSS developer tools

37
00:01:34,70 --> 00:01:35,80
and so I highly encourage you

38
00:01:35,80 --> 00:01:37,00
to make use of that web.

