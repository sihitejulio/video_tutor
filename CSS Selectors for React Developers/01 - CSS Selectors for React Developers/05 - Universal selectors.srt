1
00:00:00,70 --> 00:00:06,10
(dynamic music)

2
00:00:06,10 --> 00:00:08,30
- [Instructor] The universal selector is basically

3
00:00:08,30 --> 00:00:12,50
a special type of type selector, or tag name selector,

4
00:00:12,50 --> 00:00:15,90
that basically allows you to select all of the elements.

5
00:00:15,90 --> 00:00:18,50
It matches all of the element types.

6
00:00:18,50 --> 00:00:21,50
The actual symbol for this is the asterisk.

7
00:00:21,50 --> 00:00:24,00
Now the same selector rules apply to it,

8
00:00:24,00 --> 00:00:25,70
as it does to other type selectors.

9
00:00:25,70 --> 00:00:28,90
You can only have one universal selector

10
00:00:28,90 --> 00:00:30,40
per compound selector,

11
00:00:30,40 --> 00:00:32,00
and it must appear first.

12
00:00:32,00 --> 00:00:33,70
Just like you can only have one type

13
00:00:33,70 --> 00:00:36,80
or tag name selector in a compound selector,

14
00:00:36,80 --> 00:00:39,30
you can only have one universal.

15
00:00:39,30 --> 00:00:42,90
Now, you don't typically use the universal selector.

16
00:00:42,90 --> 00:00:45,20
There's very few situations

17
00:00:45,20 --> 00:00:48,50
where you might want to select every single element.

18
00:00:48,50 --> 00:00:51,10
However, there can be times when using it

19
00:00:51,10 --> 00:00:56,80
helps to improve the syntax readability of your CSS.

20
00:00:56,80 --> 00:00:59,90
For example, take a look at this code here.

21
00:00:59,90 --> 00:01:02,80
You can see that we have what's called a combinator.

22
00:01:02,80 --> 00:01:05,60
We cover combinators in part two,

23
00:01:05,60 --> 00:01:09,10
of this two part section on selectors.

24
00:01:09,10 --> 00:01:13,60
We have a combinator here: div :first-child.

25
00:01:13,60 --> 00:01:18,10
This will select the first child of a div element.

26
00:01:18,10 --> 00:01:23,10
The second one is div:first-child but there is no space.

27
00:01:23,10 --> 00:01:27,40
This means it will select every div that is a first child.

28
00:01:27,40 --> 00:01:28,50
As you can see,

29
00:01:28,50 --> 00:01:31,60
both the first and second selectors look very similar.

30
00:01:31,60 --> 00:01:35,00
The only difference is the actual missing space.

31
00:01:35,00 --> 00:01:38,00
So to clarify which one we're referring to,

32
00:01:38,00 --> 00:01:44,00
we can use the universal selector and say div *:first-child.

33
00:01:44,00 --> 00:01:47,50
And that will indicate to select any element,

34
00:01:47,50 --> 00:01:50,20
which is a first child of a div.

35
00:01:50,20 --> 00:01:52,30
And you can see, it has the same effect

36
00:01:52,30 --> 00:01:54,20
as the first selector,

37
00:01:54,20 --> 00:01:56,40
but it's much easier to read

38
00:01:56,40 --> 00:01:58,50
and it clarifies what our intention is,

39
00:01:58,50 --> 00:02:02,40
in terms of writing our selector.

40
00:02:02,40 --> 00:02:05,20
Now,

41
00:02:05,20 --> 00:02:08,00
for those of you who have done some CSS coding before,

42
00:02:08,00 --> 00:02:12,30
you're probably aware of something called a reset file.

43
00:02:12,30 --> 00:02:15,20
While CSS is an official standard,

44
00:02:15,20 --> 00:02:16,60
or has an official standard,

45
00:02:16,60 --> 00:02:18,50
and the browsers for the most part,

46
00:02:18,50 --> 00:02:21,60
do their best to honor those official standards,

47
00:02:21,60 --> 00:02:23,80
the reality is that different browsers

48
00:02:23,80 --> 00:02:25,30
do things slightly differently

49
00:02:25,30 --> 00:02:28,70
in terms of initializing various values

50
00:02:28,70 --> 00:02:30,60
in the CSS that they,

51
00:02:30,60 --> 00:02:32,90
or the CSS user agent style sheets

52
00:02:32,90 --> 00:02:36,10
that they make available for your webpages.

53
00:02:36,10 --> 00:02:38,90
It can be very beneficial to use something called

54
00:02:38,90 --> 00:02:41,30
a CSS reset file,

55
00:02:41,30 --> 00:02:44,10
which basically resets everything

56
00:02:44,10 --> 00:02:48,40
to some type of standard normalized settings.

57
00:02:48,40 --> 00:02:49,80
Now you might think, hey,

58
00:02:49,80 --> 00:02:53,30
I can use the universal selector to go in and do things like

59
00:02:53,30 --> 00:02:55,40
set the margin to zero across the border,

60
00:02:55,40 --> 00:02:57,70
set the padding to zero across the board.

61
00:02:57,70 --> 00:03:00,90
Things like that are very common for people to do.

62
00:03:00,90 --> 00:03:03,20
But using the universal selector for this

63
00:03:03,20 --> 00:03:06,10
can present all types of additional problems,

64
00:03:06,10 --> 00:03:09,60
and the universal selector should not be thought of as a way

65
00:03:09,60 --> 00:03:13,60
to implement a very simple, easy CSS reset file.

66
00:03:13,60 --> 00:03:15,90
In fact, there are so many really great

67
00:03:15,90 --> 00:03:19,60
reset files out there, it's actually recommended

68
00:03:19,60 --> 00:03:23,80
that you use a battle-hardened proven CSS reset file,

69
00:03:23,80 --> 00:03:25,00
instead of even creating your own.

