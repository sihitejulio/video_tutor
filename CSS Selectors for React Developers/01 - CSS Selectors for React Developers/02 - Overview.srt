1
00:00:00,10 --> 00:00:06,60
(inspirational music)

2
00:00:06,60 --> 00:00:09,70
- [Instructor] So what exactly do selectors do for us?

3
00:00:09,70 --> 00:00:11,50
Selectors are used to connect

4
00:00:11,50 --> 00:00:15,00
style declarations to elements.

5
00:00:15,00 --> 00:00:19,20
Now, CSS supports many kinds of simple selectors,

6
00:00:19,20 --> 00:00:21,20
in the previous course we talked a little bit about

7
00:00:21,20 --> 00:00:22,30
simple selectors,

8
00:00:22,30 --> 00:00:23,60
compound selectors,

9
00:00:23,60 --> 00:00:25,60
and complex selectors.

10
00:00:25,60 --> 00:00:28,90
Well there are many different types of simple selectors,

11
00:00:28,90 --> 00:00:31,20
we've explored before the type selector,

12
00:00:31,20 --> 00:00:32,20
the class selector,

13
00:00:32,20 --> 00:00:34,10
and the ID selector,

14
00:00:34,10 --> 00:00:37,10
but we can also use for example selectors based upon the

15
00:00:37,10 --> 00:00:40,60
attribute or what are known as pseudo-class selectors

16
00:00:40,60 --> 00:00:43,20
as well as pseudo-element selectors.

17
00:00:43,20 --> 00:00:46,50
Now each kind of simple selector has a unique syntax

18
00:00:46,50 --> 00:00:49,50
that enables us to combine it with other simple selectors

19
00:00:49,50 --> 00:00:52,20
to create compound selectors.

20
00:00:52,20 --> 00:00:53,80
Now some selectors select elements

21
00:00:53,80 --> 00:00:55,60
based upon the DOM structure,

22
00:00:55,60 --> 00:00:57,30
while other selectors select elements based

23
00:00:57,30 --> 00:01:00,10
upon information stored outside of the DOM,

24
00:01:00,10 --> 00:01:03,00
this is where those pseudo-class selectors come in.

25
00:01:03,00 --> 00:01:05,30
Through web browser API functions we can actually

26
00:01:05,30 --> 00:01:08,80
use JavaScript to access the CSS selector engine

27
00:01:08,80 --> 00:01:11,70
to help find DOM elements in the DOM tree

28
00:01:11,70 --> 00:01:14,30
to perform JavaScript operations on.

29
00:01:14,30 --> 00:01:17,30
In fact, using these web browser API functions

30
00:01:17,30 --> 00:01:20,50
with CSS selectors actually allows us to stop

31
00:01:20,50 --> 00:01:22,50
using some of the older legacy functions,

32
00:01:22,50 --> 00:01:24,70
such as get element by ID,

33
00:01:24,70 --> 00:01:29,00
or get elements by tag name.

34
00:01:29,00 --> 00:01:32,20
Now in order to work with selectors and apply styling

35
00:01:32,20 --> 00:01:35,30
it's essential that we have valid HTML.

36
00:01:35,30 --> 00:01:38,40
If you are not sure how HTML works

37
00:01:38,40 --> 00:01:40,20
I highly encourage you to go read

38
00:01:40,20 --> 00:01:42,30
the specifications for HTML,

39
00:01:42,30 --> 00:01:45,10
particularly those on content categories.

40
00:01:45,10 --> 00:01:48,00
If you violate the rules of content categories,

41
00:01:48,00 --> 00:01:52,20
even if your selector is technically correct,

42
00:01:52,20 --> 00:01:55,30
according to the way you've coded your HTML,

43
00:01:55,30 --> 00:01:59,10
the browser in many cases will still not apply your styles.

44
00:01:59,10 --> 00:02:03,20
And that's because you have to follow the rules of HTML.

45
00:02:03,20 --> 00:02:07,30
Now HTML and CSS are actually interesting in the sense that

46
00:02:07,30 --> 00:02:10,60
web browsers don't throw errors when you make mistakes.

47
00:02:10,60 --> 00:02:12,60
It just simply tries to do it's best

48
00:02:12,60 --> 00:02:15,20
or it ignores the bad syntax.

49
00:02:15,20 --> 00:02:17,00
So it's important you really understand

50
00:02:17,00 --> 00:02:18,20
how these things work,

51
00:02:18,20 --> 00:02:20,20
CSS and HTML,

52
00:02:20,20 --> 00:02:21,80
otherwise you could write a bunch of code,

53
00:02:21,80 --> 00:02:22,70
think it's going to work,

54
00:02:22,70 --> 00:02:25,50
but then it just doesn't look right

55
00:02:25,50 --> 00:02:27,40
and yet there's no actual error.

56
00:02:27,40 --> 00:02:30,00
So it's really important to go read the specifications

57
00:02:30,00 --> 00:02:32,80
and learn the details.

58
00:02:32,80 --> 00:02:36,90
A great example of this is the div element and a p element.

59
00:02:36,90 --> 00:02:39,30
Now you can have a paragraph element inside

60
00:02:39,30 --> 00:02:41,10
of a div tag no problem,

61
00:02:41,10 --> 00:02:43,10
but if you try to put a div tag inside of a

62
00:02:43,10 --> 00:02:45,80
paragraph element things are not going to work

63
00:02:45,80 --> 00:02:47,10
out well for you.

64
00:02:47,10 --> 00:02:49,90
The browser doesn't complain about it,

65
00:02:49,90 --> 00:02:53,90
but if you try to apply any styles to that div element

66
00:02:53,90 --> 00:02:55,90
inside of the paragraph element,

67
00:02:55,90 --> 00:02:58,50
those styles will not be applied, why?

68
00:02:58,50 --> 00:03:02,00
Because we're violating the content categories.

69
00:03:02,00 --> 00:03:04,70
P elements only allow phrasing content,

70
00:03:04,70 --> 00:03:07,20
think of paragraphs of text,

71
00:03:07,20 --> 00:03:08,80
they don't allow flow content,

72
00:03:08,80 --> 00:03:10,30
which is what a div is.

73
00:03:10,30 --> 00:03:12,70
So if you try to put flow content inside of an element

74
00:03:12,70 --> 00:03:15,00
that only from it's phrasing content,

75
00:03:15,00 --> 00:03:17,60
then you're going to have a problem styling your page.

76
00:03:17,60 --> 00:03:19,60
If you've never heard of content categories,

77
00:03:19,60 --> 00:03:22,70
you've never heard of phrasing content versus flow content

78
00:03:22,70 --> 00:03:24,40
you need to go read up on that,

79
00:03:24,40 --> 00:03:26,90
if you want to ensure that your HTML

80
00:03:26,90 --> 00:03:28,50
a, is structured correctly,

81
00:03:28,50 --> 00:03:31,00
and b, your styles are going be applied.

