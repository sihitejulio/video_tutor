1
00:00:00,00 --> 00:00:01,50
(light music)

2
00:00:01,50 --> 00:00:02,70
- [Instructor] Let's do a demonstration now

3
00:00:02,70 --> 00:00:05,00
of using pseudo-class selectors.

4
00:00:05,00 --> 00:00:06,20
So let's go ahead and try out some

5
00:00:06,20 --> 00:00:08,60
pseudo-class selectors and see how those work.

6
00:00:08,60 --> 00:00:09,80
So we're going to comment out

7
00:00:09,80 --> 00:00:12,80
our attribute selectors from earlier.

8
00:00:12,80 --> 00:00:17,20
We'll come up to here and let's go ahead

9
00:00:17,20 --> 00:00:19,50
and do a pseudo-class selector where we're going to select

10
00:00:19,50 --> 00:00:23,20
a list item that is the first child of its parent.

11
00:00:23,20 --> 00:00:27,00
So, we'll come into here and we'll say

12
00:00:27,00 --> 00:00:32,10
list item first child

13
00:00:32,10 --> 00:00:34,40
and we'll make its color to be,

14
00:00:34,40 --> 00:00:36,60
let's go for Rebecca purple.

15
00:00:36,60 --> 00:00:38,80
Rebecca purple is actually one of the newest colors

16
00:00:38,80 --> 00:00:40,20
that was added.

17
00:00:40,20 --> 00:00:44,30
It's actually named in honor of a person's daughter,

18
00:00:44,30 --> 00:00:46,40
so, Rebecca purple.

19
00:00:46,40 --> 00:00:50,50
So, we'll come over to here

20
00:00:50,50 --> 00:00:52,70
and you'll see our first Menu Item there is purple

21
00:00:52,70 --> 00:00:59,10
because it's the first child of its parent of the UL.

22
00:00:59,10 --> 00:01:02,30
Now, sometimes folks will think if you come in here

23
00:01:02,30 --> 00:01:05,60
and say UL colon first child that we're actually referring

24
00:01:05,60 --> 00:01:07,50
to the first child of the UL.

25
00:01:07,50 --> 00:01:09,10
That is not the case.

26
00:01:09,10 --> 00:01:13,70
This would mean a UL that is the first child of its parent.

27
00:01:13,70 --> 00:01:17,00
So, in the case of App JS UL is the first child

28
00:01:17,00 --> 00:01:19,00
of its parent which is a nav.

29
00:01:19,00 --> 00:01:20,10
So if you come into here you'll see

30
00:01:20,10 --> 00:01:22,90
all the Menu Items are actually purple.

31
00:01:22,90 --> 00:01:28,60
If we come into here and actually say UL space first child

32
00:01:28,60 --> 00:01:30,70
this is a combinator and this will select

33
00:01:30,70 --> 00:01:33,70
the first child of a UL.

34
00:01:33,70 --> 00:01:36,70
So, in that case, this is the list item.

35
00:01:36,70 --> 00:01:39,30
Now, if we leave off, if we just leave it like this,

36
00:01:39,30 --> 00:01:41,50
this is the equivalent of the universal selector

37
00:01:41,50 --> 00:01:45,00
as we learned earlier, or I can put list item here

38
00:01:45,00 --> 00:01:48,70
in front of it and this will be the, a list item

39
00:01:48,70 --> 00:01:51,80
that is the first child of its parent UL,

40
00:01:51,80 --> 00:01:53,80
which in this case is the Menu Item.

41
00:01:53,80 --> 00:01:56,20
Now of course because we can only have list items

42
00:01:56,20 --> 00:01:59,50
inside of ULs we're not really gaining anything here

43
00:01:59,50 --> 00:02:03,50
by specifying list item itself so explicitly,

44
00:02:03,50 --> 00:02:06,20
but you get the idea for how this works.

45
00:02:06,20 --> 00:02:09,30
Now, we can also do N-th child

46
00:02:09,30 --> 00:02:14,10
and the way N-th child works is it actually starts at one.

47
00:02:14,10 --> 00:02:16,20
So it's not zero based, it's one based,

48
00:02:16,20 --> 00:02:17,40
and if you take a look here you'll see

49
00:02:17,40 --> 00:02:20,40
our Menu Item 1 is still the purple one.

50
00:02:20,40 --> 00:02:22,60
But if we come into here and say two,

51
00:02:22,60 --> 00:02:24,60
now you'll see the second one,

52
00:02:24,60 --> 00:02:29,30
then of course if we say three that will be the last one.

53
00:02:29,30 --> 00:02:32,00
We also can do little expressions inside of here.

54
00:02:32,00 --> 00:02:35,30
So, for example we could say two N,

55
00:02:35,30 --> 00:02:37,70
and in this case it's just going to be the second one.

56
00:02:37,70 --> 00:02:41,40
So N actually starts at zero, and so zero times

57
00:02:41,40 --> 00:02:46,10
two would be zero, so that would mean the zeroth one,

58
00:02:46,10 --> 00:02:48,30
which doesn't really exist, would be the one

59
00:02:48,30 --> 00:02:51,20
that was purple, and then when N is one

60
00:02:51,20 --> 00:02:55,00
one times two is two, so the second one is purple.

61
00:02:55,00 --> 00:02:58,20
If we were to come in and say plus one

62
00:02:58,20 --> 00:03:00,90
then come into here you would see that Menu Item 1

63
00:03:00,90 --> 00:03:05,10
is now purple and Menu Item 3 is now purple.

64
00:03:05,10 --> 00:03:08,10
So, if we start with zero zero times two is zero.

65
00:03:08,10 --> 00:03:09,30
Zero plus one is one.

66
00:03:09,30 --> 00:03:11,00
So that would be the first one,

67
00:03:11,00 --> 00:03:14,50
and then N is one, so one times two is two

68
00:03:14,50 --> 00:03:16,30
and two plus one is three,

69
00:03:16,30 --> 00:03:20,40
hence the Menu Item 3 being purple.

70
00:03:20,40 --> 00:03:24,10
So, this is an example of working with something

71
00:03:24,10 --> 00:03:26,40
that's based upon position.

72
00:03:26,40 --> 00:03:28,40
We could also come into here and let's say

73
00:03:28,40 --> 00:03:32,30
we have our article and we wanted to do

74
00:03:32,30 --> 00:03:35,50
inside of our article we'll do an H two

75
00:03:35,50 --> 00:03:38,60
and we'll just say Some Article.

76
00:03:38,60 --> 00:03:40,50
So now, if we come back over to our webpage

77
00:03:40,50 --> 00:03:42,90
you'll see we have Some Article right there,

78
00:03:42,90 --> 00:03:46,00
and if we were to say give us the first child

79
00:03:46,00 --> 00:03:50,60
of article, so if we said

80
00:03:50,60 --> 00:03:55,10
article first child then we said color

81
00:03:55,10 --> 00:03:59,30
Rebecca purple once again, you'll see that's purple.

82
00:03:59,30 --> 00:04:03,20
But what if I wanted to make the first paragraph purple?

83
00:04:03,20 --> 00:04:08,30
Well, what I could do is I could say P

84
00:04:08,30 --> 00:04:12,10
first of type.

85
00:04:12,10 --> 00:04:15,50
So, the first of this type will be Rebecca purple,

86
00:04:15,50 --> 00:04:19,60
and you can see now our paragraph one is now purple.

87
00:04:19,60 --> 00:04:21,90
So now we're actually taking a look at the type

88
00:04:21,90 --> 00:04:25,00
and we're saying if it's the first of that particular type

89
00:04:25,00 --> 00:04:27,40
to go ahead and make that purple.

90
00:04:27,40 --> 00:04:30,40
If we get rid of the P altogether and come in here

91
00:04:30,40 --> 00:04:35,00
you'll see the H two and the paragraph are both purple,

92
00:04:35,00 --> 00:04:37,50
and that's because they're the first of their type.

93
00:04:37,50 --> 00:04:39,90
So, in this case the first H two is that one

94
00:04:39,90 --> 00:04:42,20
and then the first P is this one.

95
00:04:42,20 --> 00:04:45,40
All of the additional P's, and if we had any other H two's,

96
00:04:45,40 --> 00:04:47,90
those would not be purple.

97
00:04:47,90 --> 00:04:50,00
So, all types of interesting selectors

98
00:04:50,00 --> 00:04:52,30
that we can work with here.

99
00:04:52,30 --> 00:04:54,50
There's also an N-th of type.

100
00:04:54,50 --> 00:04:58,10
So, for example we could say two

101
00:04:58,10 --> 00:05:00,70
and in this case the second paragraph will be purple

102
00:05:00,70 --> 00:05:04,10
because it is the N-th of type two.

103
00:05:04,10 --> 00:05:09,80
There is no second H two, so that's not going to be purple.

104
00:05:09,80 --> 00:05:12,30
All right, so, that's an example of doing some styling

105
00:05:12,30 --> 00:05:14,90
based upon position.

106
00:05:14,90 --> 00:05:17,60
We could also take a look at whether or not

107
00:05:17,60 --> 00:05:20,40
something actually is empty.

108
00:05:20,40 --> 00:05:22,60
Does it have no children at all?

109
00:05:22,60 --> 00:05:25,70
So for example, if we came over to here

110
00:05:25,70 --> 00:05:30,30
and added another paragraph tag,

111
00:05:30,30 --> 00:05:33,60
there we go, that has no content inside of it

112
00:05:33,60 --> 00:05:37,50
we could come into here and say P empty

113
00:05:37,50 --> 00:05:40,10
and then we could come into here and we could actually maybe

114
00:05:40,10 --> 00:05:47,50
do a border one pixel solid red

115
00:05:47,50 --> 00:05:48,80
and you could see there's our border.

116
00:05:48,80 --> 00:05:50,50
Now of course there's no content in there,

117
00:05:50,50 --> 00:05:52,70
so there's nothing to give it any height,

118
00:05:52,70 --> 00:05:54,90
but you can see this paragraph tag has nothing in it,

119
00:05:54,90 --> 00:05:56,60
and we're applying some style to it

120
00:05:56,60 --> 00:06:00,60
based upon the fact that it doesn't have anything in it.

121
00:06:00,60 --> 00:06:02,60
So that's another cool example of something

122
00:06:02,60 --> 00:06:04,40
that's kind of based upon the structure

123
00:06:04,40 --> 00:06:06,30
of the actual DOM itself.

124
00:06:06,30 --> 00:06:09,20
Now, let's take a look at some pseudo-class selectors

125
00:06:09,20 --> 00:06:11,20
that deal with the state of things

126
00:06:11,20 --> 00:06:14,50
that's kind of outside of the actual DOM structure.

127
00:06:14,50 --> 00:06:20,70
So for example, we'll comment that out.

128
00:06:20,70 --> 00:06:22,00
And we'll come into here,

129
00:06:22,00 --> 00:06:25,00
and let's do some little input fields.

130
00:06:25,00 --> 00:06:27,60
So I'm going to come into here and I'm going to just set up

131
00:06:27,60 --> 00:06:30,60
a couple of things, so we'll do a little form.

132
00:06:30,60 --> 00:06:35,10
So there's our form, and we'll have a little div tag

133
00:06:35,10 --> 00:06:41,60
and a little label, then we'll say htmlFor

134
00:06:41,60 --> 00:06:49,00
and we'll do first name input,

135
00:06:49,00 --> 00:06:53,00
then we'll say First Name.

136
00:06:53,00 --> 00:06:55,60
And then here we're going to have an input tag.

137
00:06:55,60 --> 00:07:03,20
We'll say input type equal to text required.

138
00:07:03,20 --> 00:07:06,90
There we go.

139
00:07:06,90 --> 00:07:14,90
And of course we'll say ID equal to first name input.

140
00:07:14,90 --> 00:07:18,20
Now, this is going to be a required field,

141
00:07:18,20 --> 00:07:23,60
and let's go ahead and say value.

142
00:07:23,60 --> 00:07:27,00
Actually, let's make that defaultValue

143
00:07:27,00 --> 00:07:29,40
and we'll just make it blank.

144
00:07:29,40 --> 00:07:31,40
Okay, so now if we come back over to here

145
00:07:31,40 --> 00:07:33,10
you'll see here's our First Name

146
00:07:33,10 --> 00:07:36,40
and if we actually Inspect this

147
00:07:36,40 --> 00:07:39,10
there's our actual input field there.

148
00:07:39,10 --> 00:07:41,90
Now, what we want to do is we basically want

149
00:07:41,90 --> 00:07:45,40
to mark this up with something that indicates

150
00:07:45,40 --> 00:07:46,90
that it's invalid.

151
00:07:46,90 --> 00:07:48,40
Because it's a required field,

152
00:07:48,40 --> 00:07:50,50
you actually need to fill it out.

153
00:07:50,50 --> 00:07:51,80
So, we'll come back over to here

154
00:07:51,80 --> 00:07:53,80
and we're going to come into our style sheet

155
00:07:53,80 --> 00:07:56,90
and we're going to say input, and we'll actually do this

156
00:07:56,90 --> 00:08:03,80
for a type equal to text, and if it's invalid

157
00:08:03,80 --> 00:08:07,00
we want to make the border to be red.

158
00:08:07,00 --> 00:08:11,50
So, one pixel solid red.

159
00:08:11,50 --> 00:08:13,70
Let's get back to our webpage now.

160
00:08:13,70 --> 00:08:16,10
You can see it's red because it's invalid

161
00:08:16,10 --> 00:08:18,20
because it's a required field.

162
00:08:18,20 --> 00:08:20,30
If I click it and I type in some text

163
00:08:20,30 --> 00:08:25,10
and I type in Eric now you can see it's no longer invalid,

164
00:08:25,10 --> 00:08:28,60
and now the red color has gone away.

165
00:08:28,60 --> 00:08:31,10
If I want I could also set it up to say

166
00:08:31,10 --> 00:08:36,60
when I'm focused in on it

167
00:08:36,60 --> 00:08:41,20
make the color to be blue,

168
00:08:41,20 --> 00:08:43,20
and of course we could also set it up to be

169
00:08:43,20 --> 00:08:50,90
when it is valid make this to be green.

170
00:08:50,90 --> 00:08:53,00
So now if we head back to our web browser here,

171
00:08:53,00 --> 00:08:55,20
you could see it is green.

172
00:08:55,20 --> 00:08:57,00
When I'm editing it's now blue.

173
00:08:57,00 --> 00:08:59,10
When I click out it's now red.

174
00:08:59,10 --> 00:09:01,90
We're using the pseudo-classes to query

175
00:09:01,90 --> 00:09:05,20
the DOM for information that's not readily apparent

176
00:09:05,20 --> 00:09:07,90
in the actual DOM tree itself,

177
00:09:07,90 --> 00:09:10,60
things like the focus or valid or invalid

178
00:09:10,60 --> 00:09:12,90
and we're able to style these things

179
00:09:12,90 --> 00:09:15,80
so that we can, so that we could indicate to the user

180
00:09:15,80 --> 00:09:17,80
that they need to do something here

181
00:09:17,80 --> 00:09:20,60
in terms of typing text in, or whether or not

182
00:09:20,60 --> 00:09:25,30
the text they've typed in is actually valid itself.

183
00:09:25,30 --> 00:09:28,10
Another cool thing is with links.

184
00:09:28,10 --> 00:09:29,60
Let's come back over to our page here

185
00:09:29,60 --> 00:09:31,90
and we'll actually change our Menu Items

186
00:09:31,90 --> 00:09:33,60
into actual links.

187
00:09:33,60 --> 00:09:39,90
So, we'll just do hashtag,

188
00:09:39,90 --> 00:09:44,40
then we'll wrap this around there,

189
00:09:44,40 --> 00:09:47,90
and let's go ahead and do the same thing here.

190
00:09:47,90 --> 00:09:56,70
We'll get rid of our active attribute, close that off.

191
00:09:56,70 --> 00:10:00,90
And then we'll close this one off here.

192
00:10:00,90 --> 00:10:04,20
Now, what we want to do, you'll see our href

193
00:10:04,20 --> 00:10:07,20
wants this value to be accessible.

194
00:10:07,20 --> 00:10:09,40
This is not really navigable, but it'll work

195
00:10:09,40 --> 00:10:11,00
for our purposes, 'cause we don't actually

196
00:10:11,00 --> 00:10:13,40
want to navigate anywhere at the moment.

197
00:10:13,40 --> 00:10:15,00
But what we do want to do is set it up

198
00:10:15,00 --> 00:10:17,10
so when you hover over this

199
00:10:17,10 --> 00:10:18,90
we can actually change the styling.

200
00:10:18,90 --> 00:10:21,10
So if we go to the actual web browser

201
00:10:21,10 --> 00:10:24,20
you can see our default styling looks like this.

202
00:10:24,20 --> 00:10:28,00
But let's say I don't want this to be underlined by default.

203
00:10:28,00 --> 00:10:30,30
So what I can do is come over to here

204
00:10:30,30 --> 00:10:33,70
and I can say my anchor tag,

205
00:10:33,70 --> 00:10:37,60
and I can say text decoration none.

206
00:10:37,60 --> 00:10:39,10
There we go.

207
00:10:39,10 --> 00:10:42,30
And now you can see there's no more text decoration.

208
00:10:42,30 --> 00:10:47,80
Then what I can do is I could say a hover

209
00:10:47,80 --> 00:10:55,20
text decoration underlined.

210
00:10:55,20 --> 00:10:57,50
So what'll happen now is when I mouse over it

211
00:10:57,50 --> 00:11:00,40
you'll see now I get the actual underline there.

212
00:11:00,40 --> 00:11:01,90
So I'm basically querying the fact that

213
00:11:01,90 --> 00:11:05,00
the person is hovering over the actual anchor tag

214
00:11:05,00 --> 00:11:07,50
and then being able to change the styling,

215
00:11:07,50 --> 00:11:10,20
and notice, we're not using any JavaScript to do this.

216
00:11:10,20 --> 00:11:13,70
This is all just built into CSS to be able

217
00:11:13,70 --> 00:11:21,10
to change these styles that we are in fact changing them.

218
00:11:21,10 --> 00:11:22,60
There you go.

219
00:11:22,60 --> 00:11:24,60
Another cool thing that we can do is

220
00:11:24,60 --> 00:11:26,60
we can also take a look at the read only

221
00:11:26,60 --> 00:11:30,00
or read write state of our input fields as well.

222
00:11:30,00 --> 00:11:31,30
So let's say we came over to here

223
00:11:31,30 --> 00:11:35,20
and let's add another input field,

224
00:11:35,20 --> 00:11:41,60
and this one's going to be for last name.

225
00:11:41,60 --> 00:11:43,00
So, we'll just select this.

226
00:11:43,00 --> 00:11:47,20
We'll say last and then come over here

227
00:11:47,20 --> 00:11:49,10
and say last as well.

228
00:11:49,10 --> 00:11:52,00
But for this, we're going to just go in here

229
00:11:52,00 --> 00:11:53,30
and kind of hard code a value.

230
00:11:53,30 --> 00:11:57,40
So, my last name is Greene, and what we're going to do here is

231
00:11:57,40 --> 00:12:01,90
it's required, but it's really actually going to be read only.

232
00:12:01,90 --> 00:12:04,70
So, we can actually even get rid of the required part here.

233
00:12:04,70 --> 00:12:07,00
It's going to be read only.

234
00:12:07,00 --> 00:12:09,90
So, if you go back over here to our webpage

235
00:12:09,90 --> 00:12:11,70
you'll see we have Greene and if I try

236
00:12:11,70 --> 00:12:14,40
to type in there I can't actually type.

237
00:12:14,40 --> 00:12:18,00
But it looks like I can type, but I really can't.

238
00:12:18,00 --> 00:12:20,90
So, what I want to do is I want to come into here

239
00:12:20,90 --> 00:12:23,60
and I want to style this so that it will have

240
00:12:23,60 --> 00:12:27,40
a special style for a read only input field.

241
00:12:27,40 --> 00:12:30,10
So, what we'll do is we'll come back over to here

242
00:12:30,10 --> 00:12:33,70
and then we're going to say input

243
00:12:33,70 --> 00:12:40,10
type equal to text, and if it is read only

244
00:12:40,10 --> 00:12:46,40
then we're going to set the color to be gray,

245
00:12:46,40 --> 00:12:49,60
and we'll set the border

246
00:12:49,60 --> 00:12:56,00
to be two pixels solid gray.

247
00:12:56,00 --> 00:12:57,30
There we go.

248
00:12:57,30 --> 00:12:58,50
Let's head back over to here.

249
00:12:58,50 --> 00:13:01,50
Now you can see it doesn't look like I can edit

250
00:13:01,50 --> 00:13:02,50
that field anymore.

251
00:13:02,50 --> 00:13:05,10
We've made it, we've made it grayed out.

252
00:13:05,10 --> 00:13:07,40
So you can go in there and style things like this

253
00:13:07,40 --> 00:13:10,10
to indicate things like whether or not it's read only,

254
00:13:10,10 --> 00:13:11,60
or read write.

255
00:13:11,60 --> 00:13:15,30
The pseudo-class for read write if just read-write,

256
00:13:15,30 --> 00:13:17,30
and then you can change the styling based upon

257
00:13:17,30 --> 00:13:20,30
whether or not you can actually modify the data

258
00:13:20,30 --> 00:13:23,00
inside of that particular input.

