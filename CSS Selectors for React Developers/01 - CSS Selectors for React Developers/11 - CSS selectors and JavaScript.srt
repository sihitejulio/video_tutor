1
00:00:00,10 --> 00:00:06,90
(tranquil music)

2
00:00:06,90 --> 00:00:09,40
- [Instructor] In addition to be able to use CSS Selectors

3
00:00:09,40 --> 00:00:11,70
for purely CSS purposes,

4
00:00:11,70 --> 00:00:14,30
we can also access the CSS selector engine

5
00:00:14,30 --> 00:00:18,20
through the web browser API using JavaScript.

6
00:00:18,20 --> 00:00:21,00
Now this is useful because JavaScript's primary purpose

7
00:00:21,00 --> 00:00:23,50
in a web browser is to manipulate the DOM.

8
00:00:23,50 --> 00:00:25,50
In order to do that it needs to get references

9
00:00:25,50 --> 00:00:28,20
to DOM objects or DOM elements.

10
00:00:28,20 --> 00:00:30,10
In order to find those DOM elements,

11
00:00:30,10 --> 00:00:32,70
there is a number of Legacy API functions,

12
00:00:32,70 --> 00:00:36,80
like get element by ID, or get elements by tag name,

13
00:00:36,80 --> 00:00:38,80
or get elements by class name,

14
00:00:38,80 --> 00:00:41,00
but these are very limited, doesn't allow searching

15
00:00:41,00 --> 00:00:44,00
by attributes or searching by pseudo-classes.

16
00:00:44,00 --> 00:00:47,40
And so what you can do is, using the CSS selector API,

17
00:00:47,40 --> 00:00:51,60
you can actually select DOM elements,

18
00:00:51,60 --> 00:00:54,50
using the full range of CSS selectors,

19
00:00:54,50 --> 00:00:56,80
including pseudo-classes that actually allow you

20
00:00:56,80 --> 00:00:58,60
to access information

21
00:00:58,60 --> 00:01:02,40
that you wouldn't be able to access otherwise.

22
00:01:02,40 --> 00:01:04,20
Now, in order to understand how this works,

23
00:01:04,20 --> 00:01:06,00
we first have to remind ourselves

24
00:01:06,00 --> 00:01:09,80
that CSS does not really select HTML elements.

25
00:01:09,80 --> 00:01:12,30
It actually selects DOM elements.

26
00:01:12,30 --> 00:01:15,00
The HTML that we download from the web server

27
00:01:15,00 --> 00:01:18,00
actually gets parsed into a DOM tree.

28
00:01:18,00 --> 00:01:20,30
CSS selectors and their style rules

29
00:01:20,30 --> 00:01:25,00
are applied to the DOM tree, not to the actual HTML text.

30
00:01:25,00 --> 00:01:27,70
While using that fact in using JavaScript,

31
00:01:27,70 --> 00:01:30,80
we can use CSS selectors to select actual elements

32
00:01:30,80 --> 00:01:33,00
from that parsed DOM tree,

33
00:01:33,00 --> 00:01:36,30
and then we can actually modify that DOM tree structure

34
00:01:36,30 --> 00:01:38,20
by changing attributes on elements,

35
00:01:38,20 --> 00:01:41,00
creating new elements, removing elements,

36
00:01:41,00 --> 00:01:43,80
so on and so forth.

37
00:01:43,80 --> 00:01:45,50
Now the two functions that we work with here

38
00:01:45,50 --> 00:01:48,50
to actually access that CSS selector engine

39
00:01:48,50 --> 00:01:52,40
is the querySelector and querySelectorAll functions.

40
00:01:52,40 --> 00:01:54,90
The querySelector function returns the first element

41
00:01:54,90 --> 00:01:56,60
that matches the CSS selector.

42
00:01:56,60 --> 00:01:59,80
The querySelectorAll returns all of the elements

43
00:01:59,80 --> 00:02:02,30
that match that particular selector.

44
00:02:02,30 --> 00:02:05,00
The elements in the querySelect are all actually come back

45
00:02:05,00 --> 00:02:07,00
as a NodeList collection.

46
00:02:07,00 --> 00:02:10,00
That's an array like JavaScript object,

47
00:02:10,00 --> 00:02:12,20
that you can actually convert to an array

48
00:02:12,20 --> 00:02:14,70
that you can then iterate over, stuff like that,

49
00:02:14,70 --> 00:02:17,80
but collection allows you to then work with all the elements

50
00:02:17,80 --> 00:02:19,20
that it actually found.

51
00:02:19,20 --> 00:02:21,90
Now, you can run these two functions

52
00:02:21,90 --> 00:02:24,20
off of the actual root of the document,

53
00:02:24,20 --> 00:02:27,40
or you can run it off any other DOM element.

54
00:02:27,40 --> 00:02:29,10
What that means is that if you have access

55
00:02:29,10 --> 00:02:31,10
to a DOM element deep into the tree,

56
00:02:31,10 --> 00:02:34,40
and you only want to query that one part of the tree,

57
00:02:34,40 --> 00:02:36,50
you can do so using the querySelector

58
00:02:36,50 --> 00:02:38,40
and querySelectorAll functions.

59
00:02:38,40 --> 00:02:42,70
This is known as a scoped selector.

60
00:02:42,70 --> 00:02:43,50
So, here we go.

61
00:02:43,50 --> 00:02:45,00
We've got a little DOM structure here,

62
00:02:45,00 --> 00:02:47,50
the body element, with some paragraph tags,

63
00:02:47,50 --> 00:02:49,70
and we're going to use querySelector P,

64
00:02:49,70 --> 00:02:53,10
and we're actually going to get back the first paragraph.

65
00:02:53,10 --> 00:02:55,10
And then we're going to do querySelectorAll P,

66
00:02:55,10 --> 00:02:57,20
and that'll actually return back all of them,

67
00:02:57,20 --> 00:03:00,50
as an actual NodeList.

68
00:03:00,50 --> 00:03:03,50
Now, why does this matter for a React application?

69
00:03:03,50 --> 00:03:05,70
As you know, with React, we don't do a lot

70
00:03:05,70 --> 00:03:07,10
of direct DOM manipulation,

71
00:03:07,10 --> 00:03:09,90
because React is doing that for us.

72
00:03:09,90 --> 00:03:12,80
What we can do, though, is we can utilize the querySelector

73
00:03:12,80 --> 00:03:14,60
for our React DOM render method.

74
00:03:14,60 --> 00:03:18,60
Instead of using the older Legacy get element

75
00:03:18,60 --> 00:03:20,80
by ID function, we could actually use

76
00:03:20,80 --> 00:03:24,70
document.querySelector to actually use a real CSS selector

77
00:03:24,70 --> 00:03:26,50
to find our element, where we're going

78
00:03:26,50 --> 00:03:29,00
to be bootstrapping our React application from.

