1
00:00:00,10 --> 00:00:06,50
(light piano music)

2
00:00:06,50 --> 00:00:07,90
- [Instructor] So in conclusion we've learned

3
00:00:07,90 --> 00:00:10,80
that there are many types of CSS simple selectors

4
00:00:10,80 --> 00:00:12,80
and that these can be combined with other selectors

5
00:00:12,80 --> 00:00:15,30
to form compound selectors.

6
00:00:15,30 --> 00:00:18,60
CSS selectors only apply to valid HTML as defined

7
00:00:18,60 --> 00:00:23,00
by the HTML element rules pertaining to content categories.

8
00:00:23,00 --> 00:00:26,60
The universal selector is a special type of type selector

9
00:00:26,60 --> 00:00:28,90
which selects all types and it is commonly used

10
00:00:28,90 --> 00:00:32,00
to clarify CSS selector syntax.

11
00:00:32,00 --> 00:00:34,40
Attribute selectors allow the selection of DOM elements

12
00:00:34,40 --> 00:00:36,70
according to their attribute names and values.

13
00:00:36,70 --> 00:00:39,30
Pseudo-classes allow us to select elements based upon

14
00:00:39,30 --> 00:00:43,30
their position in the DOM as well as other information

15
00:00:43,30 --> 00:00:46,50
not captured in the DOM tree itself.

16
00:00:46,50 --> 00:00:49,70
We can use CSS selectors in our JavaScript code

17
00:00:49,70 --> 00:00:51,70
through the use of the querySelector

18
00:00:51,70 --> 00:00:54,10
and querySelector all functions.

19
00:00:54,10 --> 00:00:56,00
In fact we can use the querySelector

20
00:00:56,00 --> 00:00:59,50
as an alternative to the getElementById for actually

21
00:00:59,50 --> 00:01:01,80
rendering out our react component tree

22
00:01:01,80 --> 00:01:04,90
and finally scoped selectors only select elements

23
00:01:04,90 --> 00:01:07,40
from a sub-tree within the overall DOM tree

24
00:01:07,40 --> 00:01:09,30
and that can improve performance

25
00:01:09,30 --> 00:01:10,60
when we're trying to look for elements

26
00:01:10,60 --> 00:01:11,90
within a certain part of the tree

27
00:01:11,90 --> 00:01:19,00
as opposed to having to search the entire tree.

