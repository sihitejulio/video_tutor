1
00:00:00,10 --> 00:00:06,50
(soft instrumental)

2
00:00:06,50 --> 00:00:08,70
- [Instructor] Another type of simple selector

3
00:00:08,70 --> 00:00:11,20
that we can use is the attributes selector.

4
00:00:11,20 --> 00:00:13,80
And this allows us to basically select elements

5
00:00:13,80 --> 00:00:15,90
based upon their attributes.

6
00:00:15,90 --> 00:00:18,60
Now we've already seen a couple of situations

7
00:00:18,60 --> 00:00:21,40
of using attributes for doing selections

8
00:00:21,40 --> 00:00:23,60
such as using classes and IDs.

9
00:00:23,60 --> 00:00:26,10
And you can certainly use attribute selectors

10
00:00:26,10 --> 00:00:31,30
to also select on the class attribute or the ID attribute.

11
00:00:31,30 --> 00:00:34,10
But it's actually best practice to use

12
00:00:34,10 --> 00:00:36,80
the class and ID selectors for those.

13
00:00:36,80 --> 00:00:38,80
And then use the attributes selector

14
00:00:38,80 --> 00:00:41,70
for all of the other attributes that are applied.

15
00:00:41,70 --> 00:00:44,30
Now unlike the type selector where you can only have one,

16
00:00:44,30 --> 00:00:47,60
you can actually have multiple attribute selectors applied

17
00:00:47,60 --> 00:00:50,00
in a compound selector.

18
00:00:50,00 --> 00:00:53,20
The only thing is, they have to come after any type selector

19
00:00:53,20 --> 00:00:57,40
if a type selector is in fact specified.

20
00:00:57,40 --> 00:00:59,40
Now when you do an attributes selector,

21
00:00:59,40 --> 00:01:04,50
you can actually select based upon name or name and value.

22
00:01:04,50 --> 00:01:09,30
HTML 5 does not require attributes to actually have values.

23
00:01:09,30 --> 00:01:11,70
And so you can simply just select on whether or not

24
00:01:11,70 --> 00:01:14,10
the actual attribute is present.

25
00:01:14,10 --> 00:01:17,90
Now technically, according to the HTML 5 specification,

26
00:01:17,90 --> 00:01:23,10
custom HTML attributes should be prefixed with data dash.

27
00:01:23,10 --> 00:01:25,10
Web browsers do not enforce this

28
00:01:25,10 --> 00:01:27,50
and neither does the CSS selector engine.

29
00:01:27,50 --> 00:01:28,60
So even though we talked about

30
00:01:28,60 --> 00:01:32,20
a bunch of valid HTML concerns earlier,

31
00:01:32,20 --> 00:01:34,20
when it comes to having this data dash,

32
00:01:34,20 --> 00:01:37,30
that is not a requirement in the web browser itself

33
00:01:37,30 --> 00:01:42,20
and it's not a requirement for the CSS selector engine.

34
00:01:42,20 --> 00:01:44,80
Now to actually select an attribute,

35
00:01:44,80 --> 00:01:47,00
the special type of syntax that's going to be used

36
00:01:47,00 --> 00:01:48,90
is a square bracket.

37
00:01:48,90 --> 00:01:50,50
And so you have square brackets

38
00:01:50,50 --> 00:01:53,40
and inside of it you can place just the attribute name

39
00:01:53,40 --> 00:01:54,90
to select something based upon

40
00:01:54,90 --> 00:01:57,20
whether or not the attribute is present.

41
00:01:57,20 --> 00:01:58,70
Or you can put in the square brackets

42
00:01:58,70 --> 00:02:01,00
and you can have a simple expression,

43
00:02:01,00 --> 00:02:03,20
takes the attribute name and checks

44
00:02:03,20 --> 00:02:06,80
to see whether or not it's been assigned a certain value.

45
00:02:06,80 --> 00:02:10,00
If that value does not have any spaces in it,

46
00:02:10,00 --> 00:02:12,00
it does not have to be quoted.

47
00:02:12,00 --> 00:02:13,90
If the value does have spaces,

48
00:02:13,90 --> 00:02:16,10
then it does have to be quoted.

49
00:02:16,10 --> 00:02:18,20
Now there's numerous operators that you can use

50
00:02:18,20 --> 00:02:20,50
to do things like the attribute value

51
00:02:20,50 --> 00:02:24,70
starts with, ends with, contains, so on and so forth.

52
00:02:24,70 --> 00:02:27,60
Now, as a best practice, all attribute values

53
00:02:27,60 --> 00:02:30,30
in HTML mark-up should be double quoted,

54
00:02:30,30 --> 00:02:32,30
regardless of whether or not you're using them

55
00:02:32,30 --> 00:02:34,40
for CSS or otherwise.

56
00:02:34,40 --> 00:02:37,00
But all attribute value should always be double quoted.

57
00:02:37,00 --> 00:02:40,50
That includes also your react JSX.

58
00:02:40,50 --> 00:02:43,00
If you're not using a JSX expression,

59
00:02:43,00 --> 00:02:44,90
you should be using double quotes

60
00:02:44,90 --> 00:02:47,70
around your attribute values.

61
00:02:47,70 --> 00:02:49,90
Now, regarding CSS selectors,

62
00:02:49,90 --> 00:02:51,80
if the attribute value contains no spaces,

63
00:02:51,80 --> 00:02:54,10
then you can definitely leave off the quotes.

64
00:02:54,10 --> 00:02:55,40
That is totally fine.

65
00:02:55,40 --> 00:02:56,80
And if it does include a space,

66
00:02:56,80 --> 00:02:59,00
then you can include the quotes if you'd like.

67
00:02:59,00 --> 00:03:03,20
Now one of the newest features added in CSS through CSS next

68
00:03:03,20 --> 00:03:06,90
is the ability to do a case insensitive comparison

69
00:03:06,90 --> 00:03:09,90
between the value in the DOM structure

70
00:03:09,90 --> 00:03:12,00
and the value in the selector.

71
00:03:12,00 --> 00:03:14,30
And you can make use of this by including an I

72
00:03:14,30 --> 00:03:18,00
in the square brackets.

73
00:03:18,00 --> 00:03:20,00
So here is an example of some selectors.

74
00:03:20,00 --> 00:03:22,60
So we have an un-ordered list of items here.

75
00:03:22,60 --> 00:03:24,90
Then we have some paragraph content.

76
00:03:24,90 --> 00:03:27,70
If we want to select based upon simply

77
00:03:27,70 --> 00:03:30,00
having the active attribute,

78
00:03:30,00 --> 00:03:31,90
then in this case, the second list item

79
00:03:31,90 --> 00:03:33,50
in the second paragraph will have

80
00:03:33,50 --> 00:03:35,80
their font weight set to bold.

81
00:03:35,80 --> 00:03:38,40
But if we want to create a compound selector saying,

82
00:03:38,40 --> 00:03:41,10
if the list item has an active attribute,

83
00:03:41,10 --> 00:03:43,50
then in this case, only the second list item

84
00:03:43,50 --> 00:03:46,10
will have a text decoration of underline.

85
00:03:46,10 --> 00:03:49,40
That underline will not apply to the paragraph

86
00:03:49,40 --> 00:03:52,20
because the compound selector only matches list items.

87
00:03:52,20 --> 00:03:56,30
It doesn't match paragraphs.

88
00:03:56,30 --> 00:03:58,70
So let's take a look at some of these selectors.

89
00:03:58,70 --> 00:04:00,00
In the first one, we're going to take a look

90
00:04:00,00 --> 00:04:01,90
at the status being set to active.

91
00:04:01,90 --> 00:04:04,10
So you can see some of our elements have a status

92
00:04:04,10 --> 00:04:06,00
equal to active and some do not.

93
00:04:06,00 --> 00:04:09,00
So the ones that have a lowercase value of active

94
00:04:09,00 --> 00:04:10,60
will be set to bold.

95
00:04:10,60 --> 00:04:13,10
So in this case, red will be bold.

96
00:04:13,10 --> 00:04:14,90
Blue will be bold.

97
00:04:14,90 --> 00:04:17,60
And then the second paragraph will also be bold

98
00:04:17,60 --> 00:04:20,80
as well as the first paragraph.

99
00:04:20,80 --> 00:04:23,20
If we want to do a case insensitive comparison,

100
00:04:23,20 --> 00:04:26,10
we can include the I inside of the square brackets

101
00:04:26,10 --> 00:04:29,10
and that will also cause orange to be bolded.

102
00:04:29,10 --> 00:04:31,80
If we want to check to see if the attribute value starts with,

103
00:04:31,80 --> 00:04:36,80
for example, the word, IN or the letters IN, I-N,

104
00:04:36,80 --> 00:04:40,10
then we can say I-N for inactive.

105
00:04:40,10 --> 00:04:41,30
Or if we want to check

106
00:04:41,30 --> 00:04:44,00
to see if the attribute contains a word,

107
00:04:44,00 --> 00:04:46,00
we can actually say minus equals.

108
00:04:46,00 --> 00:04:49,40
And if it contains the word deprecated

109
00:04:49,40 --> 00:04:51,20
with spaces around it,

110
00:04:51,20 --> 00:04:53,10
then that will also select the element.

111
00:04:53,10 --> 00:04:55,30
So in the case of status deprecated,

112
00:04:55,30 --> 00:04:58,00
it'll set that font style to...

