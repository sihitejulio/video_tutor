1
00:00:01,10 --> 00:00:02,30
- [Instructor] Let's do a demonstration now

2
00:00:02,30 --> 00:00:05,50
of using selectors in JavaScript.

3
00:00:05,50 --> 00:00:08,50
Let's go ahead and take a look at using the

4
00:00:08,50 --> 00:00:11,20
JavaScript API functions

5
00:00:11,20 --> 00:00:14,30
that are available to us inside of the web browser.

6
00:00:14,30 --> 00:00:18,30
Go ahead and open our index.js file here,

7
00:00:18,30 --> 00:00:20,10
and we're just going to write a little bit of JavaScript

8
00:00:20,10 --> 00:00:23,60
code to basically query our DOM structure.

9
00:00:23,60 --> 00:00:26,80
Now, in our DOM structure, we have some paragraph tags,

10
00:00:26,80 --> 00:00:31,90
and I can say document.querySelector,

11
00:00:31,90 --> 00:00:35,90
and then I can actually query the paragraph tags.

12
00:00:35,90 --> 00:00:38,40
If I do this,

13
00:00:38,40 --> 00:00:43,40
and say pElement.

14
00:00:43,40 --> 00:00:46,30
I can say console.log(pElement)

15
00:00:46,30 --> 00:00:48,30
and we can take a look at that element.

16
00:00:48,30 --> 00:00:52,40
So, if I come back over to my web page here,

17
00:00:52,40 --> 00:00:55,30
and I open up my developer tools,

18
00:00:55,30 --> 00:01:01,20
you can see, there's my one paragraph tag.

19
00:01:01,20 --> 00:01:04,10
If I want, I can come into there and say console,

20
00:01:04,10 --> 00:01:06,10
not log, dir.

21
00:01:06,10 --> 00:01:08,80
And, instead of calling to string,

22
00:01:08,80 --> 00:01:10,20
so, now, we'll come over here and actually

23
00:01:10,20 --> 00:01:13,20
take a look at the element, itself.

24
00:01:13,20 --> 00:01:15,40
And you can see, here's the actual element

25
00:01:15,40 --> 00:01:17,80
with all of its various properties.

26
00:01:17,80 --> 00:01:21,90
So, using the CSS selector engine and the JavaScript API,

27
00:01:21,90 --> 00:01:25,40
we're able to query that particular paragraph element.

28
00:01:25,40 --> 00:01:28,60
Now, I could also do a querySelectorAll,

29
00:01:28,60 --> 00:01:30,40
and this'll get me all of the elements,

30
00:01:30,40 --> 00:01:32,50
and it'll actually come back as a NodeList.

31
00:01:32,50 --> 00:01:38,20
So I can say console.log

32
00:01:38,20 --> 00:01:39,90
(pElements

33
00:01:39,90 --> 00:01:41,60
instance

34
00:01:41,60 --> 00:01:43,60
of

35
00:01:43,60 --> 00:01:46,10
NodeList)

36
00:01:46,10 --> 00:01:48,50
and, if I reload this, you'll see it is a NodeList.

37
00:01:48,50 --> 00:01:49,70
And then, there's my NodeList

38
00:01:49,70 --> 00:01:53,40
of my three paragraph elements.

39
00:01:53,40 --> 00:01:54,80
Now, I could actually take this

40
00:01:54,80 --> 00:01:57,00
and convert it to a JavaScript array.

41
00:01:57,00 --> 00:01:59,50
For example, I could do

42
00:01:59,50 --> 00:02:04,50
Array.from(pElements)

43
00:02:04,50 --> 00:02:07,10
.forEach,

44
00:02:07,10 --> 00:02:09,50
and, of course, I'd have each pElement,

45
00:02:09,50 --> 00:02:10,80
and then we can simply just say

46
00:02:10,80 --> 00:02:16,40
console.log(pElement.innertext).

47
00:02:16,40 --> 00:02:17,40
So, if we do that,

48
00:02:17,40 --> 00:02:19,20
this will actually iterate over all of them,

49
00:02:19,20 --> 00:02:22,20
and we can actually look at the inner text of each one.

50
00:02:22,20 --> 00:02:23,80
So, now there is our paragraph one,

51
00:02:23,80 --> 00:02:26,40
paragraph two, and paragraph three.

52
00:02:26,40 --> 00:02:28,40
So, using the CSS selector engine,

53
00:02:28,40 --> 00:02:30,90
we have the ability to query the DOM structure,

54
00:02:30,90 --> 00:02:34,00
and then use these elements in our JavaScript

55
00:02:34,00 --> 00:02:36,00
code to be able to manipulate the DOM.

