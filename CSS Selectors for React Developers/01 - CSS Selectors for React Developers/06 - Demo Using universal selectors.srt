1
00:00:00,90 --> 00:00:02,00
- [Instructor] Let's do a demonstration

2
00:00:02,00 --> 00:00:05,20
of using the universal selector.

3
00:00:05,20 --> 00:00:06,50
Let's go ahead and take a look at how

4
00:00:06,50 --> 00:00:08,60
the universal selector works.

5
00:00:08,60 --> 00:00:10,40
Now, to get started here what were going to do

6
00:00:10,40 --> 00:00:12,20
is we're going to come into App JS

7
00:00:12,20 --> 00:00:14,10
and we're just going to delete all

8
00:00:14,10 --> 00:00:16,50
of the content here inside of App.

9
00:00:16,50 --> 00:00:18,00
Just like that.

10
00:00:18,00 --> 00:00:20,20
We'll also open up the App CSS file

11
00:00:20,20 --> 00:00:22,70
and delete all of the content inside of that.

12
00:00:22,70 --> 00:00:27,40
We'll do all of our coding here inside of the app itself.

13
00:00:27,40 --> 00:00:34,40
Now, what we're going to do is we're going to say react.fragement

14
00:00:34,40 --> 00:00:37,10
and inside of here we're going to place a bunch of tags.

15
00:00:37,10 --> 00:00:39,50
For example, we'll say header

16
00:00:39,50 --> 00:00:41,00
and we'll just have some content.

17
00:00:41,00 --> 00:00:43,70
We'll say header.

18
00:00:43,70 --> 00:00:47,00
And then, here we're going to have footer

19
00:00:47,00 --> 00:00:49,50
and we'll just have some footer content.

20
00:00:49,50 --> 00:00:52,40
And then, here in the middle we'll do article

21
00:00:52,40 --> 00:00:55,30
and we'll say article content.

22
00:00:55,30 --> 00:00:58,60
If we take that and go back over to our webpage.

23
00:00:58,60 --> 00:01:01,60
Let's actually first get rid of that code there.

24
00:01:01,60 --> 00:01:05,00
Alright, so we'll go back over to our webpage

25
00:01:05,00 --> 00:01:08,80
and you'll see we have our header, article and footer.

26
00:01:08,80 --> 00:01:11,00
Let's go ahead and open up the App CSS file

27
00:01:11,00 --> 00:01:12,70
and let's do a universal selector.

28
00:01:12,70 --> 00:01:18,30
We'll just say asterisk, then we'll say color blue.

29
00:01:18,30 --> 00:01:20,10
You can see everything is now blue

30
00:01:20,10 --> 00:01:23,90
because of the universal selector.

31
00:01:23,90 --> 00:01:25,10
Alright.

32
00:01:25,10 --> 00:01:27,70
That's literally going to select every element.

33
00:01:27,70 --> 00:01:32,00
Now, let's say we just had article there.

34
00:01:32,00 --> 00:01:35,40
If we just had article, then only the article would be blue,

35
00:01:35,40 --> 00:01:38,20
but because we have the asterisk,

36
00:01:38,20 --> 00:01:42,30
now all of that is blue.

37
00:01:42,30 --> 00:01:45,20
Now, let's say we came into here

38
00:01:45,20 --> 00:01:46,90
and we wanted to set this up

39
00:01:46,90 --> 00:01:55,00
to where we were going to have inside of our article,

40
00:01:55,00 --> 00:01:57,10
we were going to have some paragraph content.

41
00:01:57,10 --> 00:02:07,20
We'll say Para 1, Para 2, and Para 3

42
00:02:07,20 --> 00:02:09,30
You can see we have our three paragraphs.

43
00:02:09,30 --> 00:02:11,20
Everything is blue.

44
00:02:11,20 --> 00:02:13,00
We're going to take our universal selector

45
00:02:13,00 --> 00:02:14,10
and we're going to set this up

46
00:02:14,10 --> 00:02:17,40
to where we only want to make the first paragraph

47
00:02:17,40 --> 00:02:21,00
that's a child of the article to be blue.

48
00:02:21,00 --> 00:02:27,60
We're going to say article

49
00:02:27,60 --> 00:02:35,00
and then we're going to do our first child.

50
00:02:35,00 --> 00:02:37,20
There we go.

51
00:02:37,20 --> 00:02:41,00
Now, if you see that, our first paragraph there is blue.

52
00:02:41,00 --> 00:02:44,00
We could also do the second child, if we wanted to.

53
00:02:44,00 --> 00:02:47,50
We'll say nth child two.

54
00:02:47,50 --> 00:02:48,50
There we go.

55
00:02:48,50 --> 00:02:51,80
And now our second paragraph is now blue.

56
00:02:51,80 --> 00:02:57,10
Now, what would happen if we to take this same selector,

57
00:02:57,10 --> 00:03:00,30
but modify it to get rid of the space.

58
00:03:00,30 --> 00:03:02,30
Now, it's no longer what we call a combinator.

59
00:03:02,30 --> 00:03:03,90
It's actually going to look for the article

60
00:03:03,90 --> 00:03:07,90
that is the nth child two of it's parent.

61
00:03:07,90 --> 00:03:10,00
So if I save that and reload this,

62
00:03:10,00 --> 00:03:12,90
you'll now see everything is blue.

63
00:03:12,90 --> 00:03:16,10
In fact, we'll go ahead and change this to red.

64
00:03:16,10 --> 00:03:17,70
You can see all of this is red

65
00:03:17,70 --> 00:03:19,70
except for the one paragraph two.

66
00:03:19,70 --> 00:03:20,50
Alright.

67
00:03:20,50 --> 00:03:25,30
Well, what if I really didn't want this to be the case?

68
00:03:25,30 --> 00:03:27,90
What if for what ever reason when I was coding this up

69
00:03:27,90 --> 00:03:29,50
I made a mistake and I meant to

70
00:03:29,50 --> 00:03:31,70
actually have a space in there

71
00:03:31,70 --> 00:03:33,40
or I didn't want to confuse the two?

72
00:03:33,40 --> 00:03:36,50
This is where that universal selector comes in.

73
00:03:36,50 --> 00:03:37,60
I could come up to here

74
00:03:37,60 --> 00:03:40,80
and I could actually put the asterisk there

75
00:03:40,80 --> 00:03:42,70
and then that way if I have this

76
00:03:42,70 --> 00:03:45,30
it better differentiates between these two

77
00:03:45,30 --> 00:03:48,00
by having that asterisk.

78
00:03:48,00 --> 00:03:50,40
That way I make sure I don't make a mistake

79
00:03:50,40 --> 00:03:52,80
in using the wrong selector.

80
00:03:52,80 --> 00:03:55,40
Now if I do that, you'll see I get the same result,

81
00:03:55,40 --> 00:03:58,60
but am more clear that this is pertaining to

82
00:03:58,60 --> 00:04:03,50
the child of the article not to the article itself.

83
00:04:03,50 --> 00:04:05,90
Now of course, if I wanted to I could be really explicit

84
00:04:05,90 --> 00:04:07,70
and say the paragraph.

85
00:04:07,70 --> 00:04:10,20
And now I get the same result there.

86
00:04:10,20 --> 00:04:11,00
There you go.

87
00:04:11,00 --> 00:04:13,40
There's a usefulness for the universal selector,

88
00:04:13,40 --> 00:04:16,00
in terms of clarifying your CSS selector.

