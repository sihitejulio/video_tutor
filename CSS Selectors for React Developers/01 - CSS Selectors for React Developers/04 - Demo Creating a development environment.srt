1
00:00:01,10 --> 00:00:02,20
- [Instructor] Let's do a demonstration now

2
00:00:02,20 --> 00:00:05,80
of creating a development environment.

3
00:00:05,80 --> 00:00:09,10
In order to get started with using CSS in a React project,

4
00:00:09,10 --> 00:00:11,30
we have to be able to first create a React project.

5
00:00:11,30 --> 00:00:14,30
And to do that, we need to have Node.js installed.

6
00:00:14,30 --> 00:00:15,90
I'll be doing all the demonstrations here

7
00:00:15,90 --> 00:00:17,60
with Node version 10.12,

8
00:00:17,60 --> 00:00:20,60
which is actually the latest current version of Node

9
00:00:20,60 --> 00:00:22,40
at the time of this recording.

10
00:00:22,40 --> 00:00:24,40
If you're using a future version of Node

11
00:00:24,40 --> 00:00:27,10
that does not work with this particular project,

12
00:00:27,10 --> 00:00:30,30
I recommend you go back to version 10.12

13
00:00:30,30 --> 00:00:33,00
and install that on your system

14
00:00:33,00 --> 00:00:37,70
to do your development during the demonstrations.

15
00:00:37,70 --> 00:00:39,80
I also recommend using Visual Studio Code

16
00:00:39,80 --> 00:00:42,60
to be able to do the editing of your project.

17
00:00:42,60 --> 00:00:44,50
Visual Studio Code is by far the most popular

18
00:00:44,50 --> 00:00:46,80
JavaScript editor out there.

19
00:00:46,80 --> 00:00:48,30
It's totally cross-platform.

20
00:00:48,30 --> 00:00:50,40
I'm recording this on Linux.

21
00:00:50,40 --> 00:00:52,50
You can see, you can download

22
00:00:52,50 --> 00:00:55,10
Debian and RPM packages for Linux.

23
00:00:55,10 --> 00:00:58,20
But it also installs on Mac and Windows as well

24
00:00:58,20 --> 00:01:00,70
and it's a great development environment.

25
00:01:00,70 --> 00:01:03,10
Now to create our project, we're going to be using

26
00:01:03,10 --> 00:01:06,40
our own custom version of custom-react-scripts.

27
00:01:06,40 --> 00:01:10,80
We created this in the first video of this course series,

28
00:01:10,80 --> 00:01:13,40
to include a bunch of additional PostCSS plugins,

29
00:01:13,40 --> 00:01:16,60
as well as to use stage zero and later plugins.

30
00:01:16,60 --> 00:01:19,00
So I'm going to click on this here.

31
00:01:19,00 --> 00:01:20,80
I'm going to bring up the actual project,

32
00:01:20,80 --> 00:01:23,30
it's actually going to be located here at

33
00:01:23,30 --> 00:01:31,50
https://github.com/wnow-t4d-io/custom-react-scripts.

34
00:01:31,50 --> 00:01:34,70
I'm going to take the URL from here

35
00:01:34,70 --> 00:01:36,00
and we're going to use this with

36
00:01:36,00 --> 00:01:38,00
our scripts version option

37
00:01:38,00 --> 00:01:40,40
to actually create our React application.

38
00:01:40,40 --> 00:01:42,60
So I'm going to copy that,

39
00:01:42,60 --> 00:01:47,00
come over to our terminal window.

40
00:01:47,00 --> 00:01:50,30
I'm going to say npx,

41
00:01:50,30 --> 00:01:53,90
create-react-app.

42
00:01:53,90 --> 00:01:55,50
We'll call this our ...

43
00:01:55,50 --> 00:01:58,20
Actually we'll call it demo app.

44
00:01:58,20 --> 00:01:59,50
And then we're going to come into here

45
00:01:59,50 --> 00:02:04,80
and say scripts-version

46
00:02:04,80 --> 00:02:11,40
and then we'll do our git+ssh

47
00:02:11,40 --> 00:02:13,50
and then paste that in there like that.

48
00:02:13,50 --> 00:02:20,40
And now we'll be able to use our custom-react-scripts.

49
00:02:20,40 --> 00:02:22,30
So now it's using our custom-react-scripts

50
00:02:22,30 --> 00:02:29,30
and downloading all of the packages that we need.

51
00:02:29,30 --> 00:02:31,30
Okay, so now that we have all of that set up,

52
00:02:31,30 --> 00:02:34,20
let's go ahead and open up our project in our editor.

53
00:02:34,20 --> 00:02:36,20
I'm going to be using Visual Studio Code

54
00:02:36,20 --> 00:02:38,50
so I'm just going to type in code and then dot

55
00:02:38,50 --> 00:02:40,70
to open up this particular folder.

56
00:02:40,70 --> 00:02:41,80
But actually before I do that,

57
00:02:41,80 --> 00:02:44,80
let me actually change into the demo app folder.

58
00:02:44,80 --> 00:02:52,60
Then I'll say code dot and we'll fire up Visual Studio Code.

59
00:02:52,60 --> 00:02:54,10
And we'll zoom out a little.

60
00:02:54,10 --> 00:02:55,00
And then what I'm going to do

61
00:02:55,00 --> 00:02:56,60
is actually open up the terminal window.

62
00:02:56,60 --> 00:03:00,60
I can do that by toggling the terminal with Ctrl + `

63
00:03:00,60 --> 00:03:03,10
like this and I'll get a terminal window.

64
00:03:03,10 --> 00:03:08,20
Or if you prefer the menu you can say View, Terminal.

65
00:03:08,20 --> 00:03:11,50
Now once I'm inside of here I can say npm start.

66
00:03:11,50 --> 00:03:14,70
This will fire up my React application.

67
00:03:14,70 --> 00:03:18,70
And I should now get my customized version

68
00:03:18,70 --> 00:03:20,40
of create-react-app loaded up here.

69
00:03:20,40 --> 00:03:21,70
And there we go.

70
00:03:21,70 --> 00:03:24,00
WintellectNOW Custom React Scripts.

