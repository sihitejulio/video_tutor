1
00:00:00,40 --> 00:00:06,10
(light music)

2
00:00:06,10 --> 00:00:07,30
- [Instructor] Another type of selector

3
00:00:07,30 --> 00:00:09,60
is the pseudo-class selector.

4
00:00:09,60 --> 00:00:12,10
This basically allows us to select an element based

5
00:00:12,10 --> 00:00:15,40
upon its position or its state.

6
00:00:15,40 --> 00:00:17,70
Now the state is actually information

7
00:00:17,70 --> 00:00:20,60
that's not tracked in the DOM tree, like, for example,

8
00:00:20,60 --> 00:00:22,80
which element has the current focus.

9
00:00:22,80 --> 00:00:25,90
There is lots of different pseudo-class selectors.

10
00:00:25,90 --> 00:00:28,20
We can have pseudo-class selectors for looking

11
00:00:28,20 --> 00:00:29,90
for elements that are the first child,

12
00:00:29,90 --> 00:00:34,00
the last child, the nth child, the child of a certain type,

13
00:00:34,00 --> 00:00:37,00
whether or not if an element is the only child

14
00:00:37,00 --> 00:00:40,00
or whether or not the element has no children at all.

15
00:00:40,00 --> 00:00:42,50
For selecting based upon state, we have pseudo-classes

16
00:00:42,50 --> 00:00:45,40
that will look for valid and invalid form controls,

17
00:00:45,40 --> 00:00:47,90
the focused element, links that have been visited

18
00:00:47,90 --> 00:00:50,70
or not visited or the root element, the actual

19
00:00:50,70 --> 00:00:54,40
top level element of the page.

20
00:00:54,40 --> 00:00:56,40
So here's an example of a pseudo-class selector.

21
00:00:56,40 --> 00:00:58,50
We have an unordered list of list items,

22
00:00:58,50 --> 00:01:00,10
red, blue and green.

23
00:01:00,10 --> 00:01:03,20
If we want to grab the actual list item

24
00:01:03,20 --> 00:01:05,40
that's the first child, that will grab

25
00:01:05,40 --> 00:01:06,90
the list item with red.

26
00:01:06,90 --> 00:01:09,00
If we want to grab the list item that is the last

27
00:01:09,00 --> 00:01:11,30
child of its parent, that will actually grab

28
00:01:11,30 --> 00:01:13,00
the list item with the content of

