1
00:00:00,00 --> 00:00:03,50
(intensifying suspenseful music)

2
00:00:03,50 --> 00:00:05,10
- [Eric] Welcome to the Mastering CSS

3
00:00:05,10 --> 00:00:07,00
for React Developers series.

4
00:00:07,00 --> 00:00:08,20
In this course, we're going to take

5
00:00:08,20 --> 00:00:10,70
a look at working with CSS selectors.

6
00:00:10,70 --> 00:00:14,60
This particular course is a part one of two,

7
00:00:14,60 --> 00:00:17,60
that's just how big CSS selectors are.

8
00:00:17,60 --> 00:00:19,50
My name is Eric Greene, and this video

9
00:00:19,50 --> 00:00:22,50
is brought to you by WintellectNOW.

10
00:00:22,50 --> 00:00:23,60
So in this course, we're going to take

11
00:00:23,60 --> 00:00:26,80
a look at an overview of working with CSS selectors.

12
00:00:26,80 --> 00:00:28,40
We'll set up a development environment

13
00:00:28,40 --> 00:00:30,80
so we can try out some different selectors.

14
00:00:30,80 --> 00:00:32,10
Then we're going to talk about a number

15
00:00:32,10 --> 00:00:33,70
of different selectors that are available

16
00:00:33,70 --> 00:00:35,80
to us in CSS.

17
00:00:35,80 --> 00:00:38,20
We'll first talk about the universal selector

18
00:00:38,20 --> 00:00:40,60
and why you might want to make use of that.

19
00:00:40,60 --> 00:00:42,60
Then we'll take a look at attribute selectors

20
00:00:42,60 --> 00:00:44,50
and then pseudo-class selectors.

21
00:00:44,50 --> 00:00:47,40
And then finally, we'll talk about using CSS selectors

22
00:00:47,40 --> 00:00:49,00
in JavaScript and how we can actually

23
00:00:49,00 --> 00:00:52,00
directly access that CSS selector engine

24
00:00:52,00 --> 00:00:53,00
through the web browser APO.

