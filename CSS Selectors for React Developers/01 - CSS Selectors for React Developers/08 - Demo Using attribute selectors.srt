1
00:00:00,80 --> 00:00:05,30
- Let's do a demonstration now of using attribute selectors.

2
00:00:05,30 --> 00:00:07,00
Let's go ahead and take a look at how we can use

3
00:00:07,00 --> 00:00:10,40
attribute selectors, so coming over here to App.js

4
00:00:10,40 --> 00:00:14,00
let's go ahead and expand this out a little bit.

5
00:00:14,00 --> 00:00:17,90
We have our article with our three paragraph elements,

6
00:00:17,90 --> 00:00:21,70
we're going to add a nav section here with an unordered list

7
00:00:21,70 --> 00:00:24,10
and we have some list items,

8
00:00:24,10 --> 00:00:29,00
and we'll just call this menu item one,

9
00:00:29,00 --> 00:00:32,70
menu item two and menu item three.

10
00:00:32,70 --> 00:00:35,00
Now let's say for whatever reason we happen to be

11
00:00:35,00 --> 00:00:37,80
on this particular menu item here,

12
00:00:37,80 --> 00:00:42,70
so it's marked as active, and then down here

13
00:00:42,70 --> 00:00:46,80
in our paragraph let's go ahead and add an attribute here

14
00:00:46,80 --> 00:00:54,00
for content we'll just say content is equal to summary,

15
00:00:54,00 --> 00:00:55,90
and then down here we can say

16
00:00:55,90 --> 00:01:02,40
our content is equal to conclusion.

17
00:01:02,40 --> 00:01:10,60
Now, these are custom attributes,

18
00:01:10,60 --> 00:01:15,30
ideally we should prefix them data dash to ensure that

19
00:01:15,30 --> 00:01:19,70
we do not conflict with any current official HTML

20
00:01:19,70 --> 00:01:25,10
attributes and we do not conflict with future attributes,

21
00:01:25,10 --> 00:01:28,00
if you think about it the word active and the word content

22
00:01:28,00 --> 00:01:31,40
here, you know these are things that actually could be

23
00:01:31,40 --> 00:01:34,70
possibly used in the future, and we want to make sure

24
00:01:34,70 --> 00:01:37,90
that our application is going to work today

25
00:01:37,90 --> 00:01:41,80
but also work tomorrow, and so that's why the data dash here

26
00:01:41,80 --> 00:01:44,90
has been added as part of the actual standard.

27
00:01:44,90 --> 00:01:46,70
So let's go ahead and right a little bit of code

28
00:01:46,70 --> 00:01:49,90
to apply some styling based upon the attribute,

29
00:01:49,90 --> 00:01:52,70
so we can come over to here and we're going to get rid of

30
00:01:52,70 --> 00:01:56,30
our code there, just compile that out,

31
00:01:56,30 --> 00:02:00,30
and we're going to say active, and we're going to set the

32
00:02:00,30 --> 00:02:06,10
font weight to be bold.

33
00:02:06,10 --> 00:02:10,40
So now if we load our page up here, you'll see that our

34
00:02:10,40 --> 00:02:13,70
menu item here is not bold, let's take a look at why,

35
00:02:13,70 --> 00:02:17,20
active, font, weight, bold that looks good.

36
00:02:17,20 --> 00:02:20,40
Ah, we put data active, let's put our data dash

37
00:02:20,40 --> 00:02:26,50
in front of that, now you can see menu item two is bolded.

38
00:02:26,50 --> 00:02:30,30
We could come in and we could say for our summary content

39
00:02:30,30 --> 00:02:33,10
we want to make that a little bit bigger in size,

40
00:02:33,10 --> 00:02:36,00
so we can't simply just select data content,

41
00:02:36,00 --> 00:02:39,40
we need to select data content with the word summary,

42
00:02:39,40 --> 00:02:44,10
so we'll say data content equal to summary and then we can

43
00:02:44,10 --> 00:02:47,00
come into here and we can say font size,

44
00:02:47,00 --> 00:02:50,80
and we can go for 20 pixels, now if you reload this

45
00:02:50,80 --> 00:02:53,00
you'll see that this is a little bit bigger

46
00:02:53,00 --> 00:02:55,30
than the text that we have down here.

47
00:02:55,30 --> 00:02:57,60
Let's actually make that a lot bigger and make the level

48
00:02:57,60 --> 00:03:01,90
easier to see, so there's our paragraph one.

49
00:03:01,90 --> 00:03:05,60
Now in this particular case, what if somebody were to

50
00:03:05,60 --> 00:03:11,50
type in capital S, now you can see our styling goes away.

51
00:03:11,50 --> 00:03:14,40
But we actually want it to pay attention to that and be able

52
00:03:14,40 --> 00:03:17,50
to include it, so we'll come into here and just include an I

53
00:03:17,50 --> 00:03:22,10
this is part of cssnext, now if we come into here and reload

54
00:03:22,10 --> 00:03:25,10
now you'll see, boom there's our paragraph

55
00:03:25,10 --> 00:03:28,50
with its larger font size.

56
00:03:28,50 --> 00:03:31,20
Now let's say we wanted to take the conclusion

57
00:03:31,20 --> 00:03:33,40
here and style that a little bit differently,

58
00:03:33,40 --> 00:03:37,30
maybe get a bit of font style of italicized.

59
00:03:37,30 --> 00:03:39,70
Well we could come into here and we could say

60
00:03:39,70 --> 00:03:43,80
data dash content and then we could do for example

61
00:03:43,80 --> 00:03:46,20
a little hat and we could say if it starts off with

62
00:03:46,20 --> 00:03:55,00
conc or conclusion, then go ahead and set the font style

63
00:03:55,00 --> 00:03:59,60
to italic, then we will switch over to here and now

64
00:03:59,60 --> 00:04:02,90
you can see our font style there is italic.

65
00:04:02,90 --> 00:04:05,40
We could also look at the end of the text, we could say

66
00:04:05,40 --> 00:04:10,90
dollar sign in this case we'll say s-i-o-n

67
00:04:10,90 --> 00:04:13,90
for the end of conclusion.

68
00:04:13,90 --> 00:04:16,50
So now if we come back over here we can see it's still

69
00:04:16,50 --> 00:04:19,80
italicized and if we inspect this, you can see it's

70
00:04:19,80 --> 00:04:23,30
actually matching the end there.

71
00:04:23,30 --> 00:04:25,80
Now inside of here we could set this up

72
00:04:25,80 --> 00:04:28,30
to where we had different values,

73
00:04:28,30 --> 00:04:32,20
so for example we could call this one to be for example

74
00:04:32,20 --> 00:04:37,30
print, and then also down here could say data dash content

75
00:04:37,30 --> 00:04:40,50
equal to print, so we only want to select it

76
00:04:40,50 --> 00:04:44,10
if we have the whole word print available to us,

77
00:04:44,10 --> 00:04:47,70
so we could come into here and we could say for example

78
00:04:47,70 --> 00:04:54,20
data dash content and then we could do our dash equals

79
00:04:54,20 --> 00:05:00,00
print, and then for this style we'll say color blue.

80
00:05:00,00 --> 00:05:02,00
So if we flip over here we'll see nothings blue

81
00:05:02,00 --> 00:05:05,20
and that's because I put dash instead of tilde,

82
00:05:05,20 --> 00:05:11,50
so tilde cause that to be blue, matching on the word.

83
00:05:11,50 --> 00:05:13,40
There you go, so different ways we can work with

84
00:05:13,40 --> 00:05:16,00
attributes selector is to select our.

