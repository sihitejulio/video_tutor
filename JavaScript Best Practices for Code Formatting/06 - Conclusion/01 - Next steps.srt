1
00:00:00,80 --> 00:00:02,30
- [Sasha] Thanks so much for joining me

2
00:00:02,30 --> 00:00:03,50
in this course.

3
00:00:03,50 --> 00:00:06,20
You now have experience implementing best practices

4
00:00:06,20 --> 00:00:09,30
for formatting Vanilla JavaScript code.

5
00:00:09,30 --> 00:00:12,60
To dig deeper into coding using Vanilla JavaScript

6
00:00:12,60 --> 00:00:16,20
check out a course on App building with Vanilla JavaScript.

7
00:00:16,20 --> 00:00:18,50
If you want to learn more about fixing logic errors

8
00:00:18,50 --> 00:00:22,40
in your JavaScript code check out a course on debugging.

9
00:00:22,40 --> 00:00:24,80
Feel free to follow me online.

10
00:00:24,80 --> 00:00:27,50
Now take your new skills and build something amazing.

11
00:00:27,50 --> 00:00:29,00
Good luck.

