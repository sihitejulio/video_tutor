1
00:00:00,70 --> 00:00:02,90
- [Instructor] Spaces are optional in comments,

2
00:00:02,90 --> 00:00:04,70
but you can include them to increase

3
00:00:04,70 --> 00:00:07,10
the legibility of your comments.

4
00:00:07,10 --> 00:00:10,20
In particular, it's a common practice to include a space

5
00:00:10,20 --> 00:00:12,20
between the starting comment characters

6
00:00:12,20 --> 00:00:14,00
and the commented code.

7
00:00:14,00 --> 00:00:17,80
I can already see that in the format for my start block.

8
00:00:17,80 --> 00:00:21,10
I'm going to add a single line comment before my variable

9
00:00:21,10 --> 00:00:24,30
just to mark this as a section that contains variables.

10
00:00:24,30 --> 00:00:27,50
So I'll add a blank line to keep some space from U strict,

11
00:00:27,50 --> 00:00:30,80
and then I can just do a single line, slash slash,

12
00:00:30,80 --> 00:00:33,70
and variables.

13
00:00:33,70 --> 00:00:36,00
Now at a glance, it takes a little work

14
00:00:36,00 --> 00:00:39,50
to see where the slashes end and the word variables begins.

15
00:00:39,50 --> 00:00:41,60
But if I click in here and stick a space

16
00:00:41,60 --> 00:00:45,40
after the second slash, it immediately becomes easier

17
00:00:45,40 --> 00:00:47,30
to recognize that as a comment

18
00:00:47,30 --> 00:00:50,40
containing the word variables.

19
00:00:50,40 --> 00:00:53,30
ESLint contains a spaced comment rule

20
00:00:53,30 --> 00:00:54,70
that let's me specify whether

21
00:00:54,70 --> 00:00:57,00
comments should include spaces.

22
00:00:57,00 --> 00:00:59,40
The documentation makes it clear that you can get

23
00:00:59,40 --> 00:01:01,20
fairly detailed for this rule.

24
00:01:01,20 --> 00:01:04,80
It let's you specify exclusions and situations

25
00:01:04,80 --> 00:01:07,50
where spaces are and aren't needed.

26
00:01:07,50 --> 00:01:10,10
I'm going to go with a more basic setting for my needs,

27
00:01:10,10 --> 00:01:11,50
which is to require a space

28
00:01:11,50 --> 00:01:14,30
after the opening comment characters.

29
00:01:14,30 --> 00:01:16,70
In me ESLint RC file,

30
00:01:16,70 --> 00:01:18,70
I'm going to add a new line,

31
00:01:18,70 --> 00:01:23,90
and the rule name is spaced-comment,

32
00:01:23,90 --> 00:01:26,90
and for the value I'll specify an array,

33
00:01:26,90 --> 00:01:30,30
first containing how I want the issue treated,

34
00:01:30,30 --> 00:01:32,30
which is as an error,

35
00:01:32,30 --> 00:01:35,20
and then the style I want applied

36
00:01:35,20 --> 00:01:38,50
which is the string, always.

37
00:01:38,50 --> 00:01:43,40
Save that and then switch back to my app.js file,

38
00:01:43,40 --> 00:01:48,70
and if I take out the space before variables in that comment

39
00:01:48,70 --> 00:01:50,60
I see the error indicator.

40
00:01:50,60 --> 00:01:51,80
And when I hover,

41
00:01:51,80 --> 00:01:54,20
I can see a note that specifically references

42
00:01:54,20 --> 00:01:56,40
the rule that I just put in place,

43
00:01:56,40 --> 00:02:00,20
and if I put that space back in, it fixes it right up.

44
00:02:00,20 --> 00:02:01,90
Note that the space comment rule

45
00:02:01,90 --> 00:02:05,50
also applies to multi-line comments.

46
00:02:05,50 --> 00:02:08,80
Because I'm already enforcing the starred block style,

47
00:02:08,80 --> 00:02:13,30
I automatically have a space enforced after the star

48
00:02:13,30 --> 00:02:15,20
at the beginning of each line.

49
00:02:15,20 --> 00:02:17,40
But if I didn't have starred block enforced,

50
00:02:17,40 --> 00:02:19,50
I'd also have to be sure to include a space

51
00:02:19,50 --> 00:02:24,20
before the content of each line of my comment.

52
00:02:24,20 --> 00:02:26,80
Now I'm enforcing spaces in my comments

53
00:02:26,80 --> 00:02:29,90
which will keep my code even more legible for me

54
00:02:29,90 --> 00:02:32,00
and for other developers.

