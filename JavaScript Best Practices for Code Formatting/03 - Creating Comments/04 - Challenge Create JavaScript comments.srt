1
00:00:00,00 --> 00:00:05,90
(pleasant electronic music)

2
00:00:05,90 --> 00:00:08,00
- [Instructor] Ready for a challenge?

3
00:00:08,00 --> 00:00:10,90
The start files contain the first few lines of code

4
00:00:10,90 --> 00:00:13,70
to create a grocery shopping app.

5
00:00:13,70 --> 00:00:17,00
Edit the files to implement and enforce best practices

6
00:00:17,00 --> 00:00:19,10
for JavaScript comments.

7
00:00:19,10 --> 00:00:22,60
Your code should also use and enforce strict mode.

8
00:00:22,60 --> 00:00:25,60
You'll need to make changes to both the script.js

9
00:00:25,60 --> 00:00:29,20
and eslintrc.js files.

10
00:00:29,20 --> 00:00:31,40
This should take you about five minutes.

11
00:00:31,40 --> 00:00:33,90
When you're done, join me in the next video,

12
00:00:33,90 --> 00:00:36,00
and I'll walk you through how I approached it.

