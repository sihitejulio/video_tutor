1
00:00:00,30 --> 00:00:05,90
(upbeat music)

2
00:00:05,90 --> 00:00:07,80
- [Instructor] The challenge consists

3
00:00:07,80 --> 00:00:11,50
of enforcing appropriate single line

4
00:00:11,50 --> 00:00:14,40
and multiline syntax for comments,

5
00:00:14,40 --> 00:00:18,00
enforcing leading spaces and also Strict Mode.

6
00:00:18,00 --> 00:00:19,30
So the first thing I want to do

7
00:00:19,30 --> 00:00:21,60
is start by building out some rules,

8
00:00:21,60 --> 00:00:23,10
because building out the rules first

9
00:00:23,10 --> 00:00:27,00
will help me then see in my editor what needs to be changed.

10
00:00:27,00 --> 00:00:31,40
So, my ESLintrc File has no rules right now.

11
00:00:31,40 --> 00:00:33,50
I know I want to enforce Strict Mode,

12
00:00:33,50 --> 00:00:37,10
so I'm going to start by adding Strict,

13
00:00:37,10 --> 00:00:39,80
and that value is an array

14
00:00:39,80 --> 00:00:45,10
which starts with Error and then Global.

15
00:00:45,10 --> 00:00:46,90
Then I want to specify

16
00:00:46,90 --> 00:00:51,10
what my multiline comment style should look like.

17
00:00:51,10 --> 00:00:57,00
So that's Multiline-Comment-Style

18
00:00:57,00 --> 00:00:59,80
and an array which is Error

19
00:00:59,80 --> 00:01:04,30
and we want to use Starred-Block

20
00:01:04,30 --> 00:01:07,50
to make this multiline comments really easy to read.

21
00:01:07,50 --> 00:01:11,80
And finally, I want to enforce leading spaces in my comments.

22
00:01:11,80 --> 00:01:17,30
So that's the Spaced-Comment rule

23
00:01:17,30 --> 00:01:25,00
with a value of an array which is Error and Always.

24
00:01:25,00 --> 00:01:27,10
So save my ESLintrc,

25
00:01:27,10 --> 00:01:29,60
immediately I can see all of the red

26
00:01:29,60 --> 00:01:31,70
being indicated for my file,

27
00:01:31,70 --> 00:01:33,90
my Script.JS file.

28
00:01:33,90 --> 00:01:37,70
Switching over there, everything is underlined as an error.

29
00:01:37,70 --> 00:01:41,50
And so, first off, I know I want to make sure

30
00:01:41,50 --> 00:01:43,20
I have Strict Mode going on.

31
00:01:43,20 --> 00:01:46,30
So after this initial multiline comment,

32
00:01:46,30 --> 00:01:49,50
before the single line comment

33
00:01:49,50 --> 00:01:52,50
that seems to be connected with this code down here,

34
00:01:52,50 --> 00:01:57,80
I'm going to add Use Strict, as a string, on it's own line.

35
00:01:57,80 --> 00:02:01,10
And that gets rid of the error that was down here,

36
00:02:01,10 --> 00:02:04,70
because I, my code was not being processed in Strict Mode.

37
00:02:04,70 --> 00:02:07,60
Then I can look up here at the errors

38
00:02:07,60 --> 00:02:09,50
and there's two of them.

39
00:02:09,50 --> 00:02:11,90
I have a multiline block comment

40
00:02:11,90 --> 00:02:14,50
using consecutive line comments

41
00:02:14,50 --> 00:02:19,00
and I'm also missing spaces in those single line comments.

42
00:02:19,00 --> 00:02:21,60
So I'm going to select that all

43
00:02:21,60 --> 00:02:24,20
in my editor, Visual Studio Code,

44
00:02:24,20 --> 00:02:27,50
I can do on my Mac a Command Slash

45
00:02:27,50 --> 00:02:29,50
to remove those single-line comments

46
00:02:29,50 --> 00:02:35,80
and then I'm going to start by creating a bare-block-comment.

47
00:02:35,80 --> 00:02:38,90
To do that, I need to add a blank line before my comment

48
00:02:38,90 --> 00:02:40,70
and then I'm selecting everything

49
00:02:40,70 --> 00:02:44,00
from that blank line before to the blank line after.

50
00:02:44,00 --> 00:02:47,30
And doing Alt + Shift + A on my Mac

51
00:02:47,30 --> 00:02:49,40
which gets me those opening and closing characters

52
00:02:49,40 --> 00:02:51,50
on the surrounding lines,

53
00:02:51,50 --> 00:02:53,30
rather than running into the beginning

54
00:02:53,30 --> 00:02:55,60
or ending lines of a content.

55
00:02:55,60 --> 00:02:59,20
And then, if I check the error again,

56
00:02:59,20 --> 00:03:03,00
now we have the Expected A Star issue.

57
00:03:03,00 --> 00:03:08,00
So creating a new Star-Block, when the editor supports that,

58
00:03:08,00 --> 00:03:10,40
is a lot easier than converting an existing one,

59
00:03:10,40 --> 00:03:11,20
but we can do it.

60
00:03:11,20 --> 00:03:13,10
This one's not that long.

61
00:03:13,10 --> 00:03:17,10
So, again I need to indent with a space

62
00:03:17,10 --> 00:03:20,20
so that the Star that I type lines up vertically

63
00:03:20,20 --> 00:03:22,40
with the Star in the first line

64
00:03:22,40 --> 00:03:25,10
and then one more Space before the content,

65
00:03:25,10 --> 00:03:26,50
and I'll just keep doing the same thing.

66
00:03:26,50 --> 00:03:28,00
I can even copy that pattern,

67
00:03:28,00 --> 00:03:29,80
Space Star Space

68
00:03:29,80 --> 00:03:31,50
copy it to the clipboard,

69
00:03:31,50 --> 00:03:34,60
and just Paste it at the start of each line

70
00:03:34,60 --> 00:03:38,00
within my miltiline-comment.

71
00:03:38,00 --> 00:03:40,30
And now I have a Starred-Block-Comment

72
00:03:40,30 --> 00:03:45,10
and I don't have any error underlining here.

73
00:03:45,10 --> 00:03:46,70
I'm going to create a blank line

74
00:03:46,70 --> 00:03:49,00
just to separate visually that initial comment

75
00:03:49,00 --> 00:03:50,80
from the Use Strict statement.

76
00:03:50,80 --> 00:03:53,60
Then I have one more issue and if I hover that,

77
00:03:53,60 --> 00:03:56,40
this comment is violating my Spaced-Comment rule

78
00:03:56,40 --> 00:03:59,70
because I need a Space after those opening characters

79
00:03:59,70 --> 00:04:01,40
in a single line comment.

80
00:04:01,40 --> 00:04:05,70
So I can click after the second slash, press Space,

81
00:04:05,70 --> 00:04:08,50
and now that content is separate

82
00:04:08,50 --> 00:04:11,50
from the comment characters.

83
00:04:11,50 --> 00:04:13,30
And, I have fulfilled

84
00:04:13,30 --> 00:04:15,90
all of the requirements of this challenge,

85
00:04:15,90 --> 00:04:19,00
and my code is a lot easier to read.

