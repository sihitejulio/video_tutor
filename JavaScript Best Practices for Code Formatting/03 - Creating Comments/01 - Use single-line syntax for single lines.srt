1
00:00:00,70 --> 00:00:03,10
- [Instructor] JavaScript has two different syntaxes

2
00:00:03,10 --> 00:00:04,30
for creating comments.

3
00:00:04,30 --> 00:00:08,50
To create a single line comment, you type two slashes.

4
00:00:08,50 --> 00:00:10,70
Anything after those characters in that line

5
00:00:10,70 --> 00:00:14,80
is considered a comment and is ignored by a parser.

6
00:00:14,80 --> 00:00:17,10
You can theoretically create a single line comment

7
00:00:17,10 --> 00:00:19,40
that's as long as you want and it's readable

8
00:00:19,40 --> 00:00:22,00
if you turn on word-wrap in your editor.

9
00:00:22,00 --> 00:00:24,50
However, it's standard practice to limit

10
00:00:24,50 --> 00:00:27,70
line length in code to ensure that word-wrap

11
00:00:27,70 --> 00:00:29,90
isn't needed to view content.

12
00:00:29,90 --> 00:00:33,30
For longer comments then, you need to use multiple lines.

13
00:00:33,30 --> 00:00:36,00
Multiple line comments have their on syntax,

14
00:00:36,00 --> 00:00:39,60
starting with slash star and ending with star slash.

15
00:00:39,60 --> 00:00:44,00
Anything enclosed by those sets of characters is ignored.

16
00:00:44,00 --> 00:00:46,80
In theory, you could simply mark each line

17
00:00:46,80 --> 00:00:48,20
of a multi-line comment

18
00:00:48,20 --> 00:00:50,80
as a single line comment.

19
00:00:50,80 --> 00:00:53,00
This would have the same practical effect

20
00:00:53,00 --> 00:00:55,30
as creating a multi-line comment.

21
00:00:55,30 --> 00:00:57,80
However, there's a subtle distinction.

22
00:00:57,80 --> 00:01:00,00
Using multiline comment syntax

23
00:01:00,00 --> 00:01:02,90
indicates clearly, from the start,

24
00:01:02,90 --> 00:01:06,20
that the following lines are part of a multiline comment.

25
00:01:06,20 --> 00:01:09,70
Using the appropriate syntax also provides formatting

26
00:01:09,70 --> 00:01:12,20
that makes it easier for you and other developers

27
00:01:12,20 --> 00:01:15,90
to quickly see how long a multi-line comment is,

28
00:01:15,90 --> 00:01:18,90
where it starts, and where it ends.

29
00:01:18,90 --> 00:01:22,20
By contrast, a set of single line comment markers

30
00:01:22,20 --> 00:01:26,20
can more easily blend into the surrounding code.

31
00:01:26,20 --> 00:01:28,60
I'm going to create a multiline comment

32
00:01:28,60 --> 00:01:31,70
at the start of my app.js file.

33
00:01:31,70 --> 00:01:33,40
Now, I want the use strict statement

34
00:01:33,40 --> 00:01:36,30
to come before any statements in my code.

35
00:01:36,30 --> 00:01:38,60
But it's pretty common to include a comment at the

36
00:01:38,60 --> 00:01:41,20
start of the file, giving basic information

37
00:01:41,20 --> 00:01:42,70
about its contents.

38
00:01:42,70 --> 00:01:45,60
And because a comment is ignored by parsers,

39
00:01:45,60 --> 00:01:47,90
putting it before the use strict statement

40
00:01:47,90 --> 00:01:49,90
isn't a problem.

41
00:01:49,90 --> 00:01:51,50
So, I'll add my comment just above

42
00:01:51,50 --> 00:01:53,00
the use strict statement.

43
00:01:53,00 --> 00:01:56,20
So, I'll add my name,

44
00:01:56,20 --> 00:01:57,10
and the name of the course,

45
00:01:57,10 --> 00:02:03,40
(keyboard typing)

46
00:02:03,40 --> 00:02:07,80
and "LinkedIN Learning".

47
00:02:07,80 --> 00:02:09,70
To make this a multiline comment,

48
00:02:09,70 --> 00:02:13,70
I'll add a blank line before and add slash star,

49
00:02:13,70 --> 00:02:15,60
and then on the line after,

50
00:02:15,60 --> 00:02:17,30
I'll add star slash.

51
00:02:17,30 --> 00:02:20,20
And then another blank line.

52
00:02:20,20 --> 00:02:22,40
And I'll add one more touch.

53
00:02:22,40 --> 00:02:24,60
I'll indent the text in that comment

54
00:02:24,60 --> 00:02:27,20
to make it stand out a little bit.

55
00:02:27,20 --> 00:02:31,70
And now I can see really clear delineation between the start

56
00:02:31,70 --> 00:02:33,90
and the end of the comment.

57
00:02:33,90 --> 00:02:37,50
I can enforce this style with eslint as well.

58
00:02:37,50 --> 00:02:41,10
Using the multiline comment style rule.

59
00:02:41,10 --> 00:02:45,10
In my eslintrc file, I'll add a new key value pair

60
00:02:45,10 --> 00:02:48,40
to the rules object.

61
00:02:48,40 --> 00:02:52,00
The name is multiline,

62
00:02:52,00 --> 00:02:55,00
dash comment, dash style.

63
00:02:55,00 --> 00:02:57,30
And notice

64
00:02:57,30 --> 00:02:58,80
after I've typed that,

65
00:02:58,80 --> 00:03:01,40
that I've got a different coloring going on here

66
00:03:01,40 --> 00:03:02,50
than I did for my other key.

67
00:03:02,50 --> 00:03:06,60
I've also got and error here, 'cause it's expecting a comma,

68
00:03:06,60 --> 00:03:09,00
so, I have hyphens in my key name here

69
00:03:09,00 --> 00:03:11,90
and in order for that to work as a key name,

70
00:03:11,90 --> 00:03:15,60
I need to quote it so that it's parsed correctly.

71
00:03:15,60 --> 00:03:19,20
So, that results in a key coloring in my editor

72
00:03:19,20 --> 00:03:21,60
that's different from the other key which isn't quoted,

73
00:03:21,60 --> 00:03:25,40
but they're both interpreted the same, as keys.

74
00:03:25,40 --> 00:03:28,80
So then, for the value of this,

75
00:03:28,80 --> 00:03:31,10
I specify an array.

76
00:03:31,10 --> 00:03:34,50
And first, I want severity level, which is error,

77
00:03:34,50 --> 00:03:37,60
because I want the editor to throw an error.

78
00:03:37,60 --> 00:03:41,20
And then, I need to specify which of the styles I'm using.

79
00:03:41,20 --> 00:03:44,90
So, there's a set of key words for different styles

80
00:03:44,90 --> 00:03:46,70
for multi-line comments

81
00:03:46,70 --> 00:03:49,50
and I've chosen bare-block,

82
00:03:49,50 --> 00:03:51,80
which I just using slash star at the beginning

83
00:03:51,80 --> 00:03:54,60
and star slash at the end and that's it.

84
00:03:54,60 --> 00:04:00,10
So, put comma at the end and we'll save those changes.

85
00:04:00,10 --> 00:04:04,80
And then, back in my app.js file, let's test this out.

86
00:04:04,80 --> 00:04:09,00
So, I want to take out the multiline comment formatting.

87
00:04:09,00 --> 00:04:12,70
In visual studio code, I can select all those lines

88
00:04:12,70 --> 00:04:16,10
and press Alt shift A, on a Mac

89
00:04:16,10 --> 00:04:19,70
to turn that back into plain text

90
00:04:19,70 --> 00:04:22,30
and then, if I select all those lines, I want to see what

91
00:04:22,30 --> 00:04:24,30
happens if I format this as a series of

92
00:04:24,30 --> 00:04:26,10
single line comments.

93
00:04:26,10 --> 00:04:29,20
So, in visual studio code, I can say command slash

94
00:04:29,20 --> 00:04:33,00
on a Mac and it's a comment, I can see in green

95
00:04:33,00 --> 00:04:35,90
but I also have error underlining

96
00:04:35,90 --> 00:04:37,90
and when I hover over that,

97
00:04:37,90 --> 00:04:40,20
it very specifically mentions the multiline

98
00:04:40,20 --> 00:04:43,00
comment style rule.

99
00:04:43,00 --> 00:04:44,20
So, I'm just going to go ahead

100
00:04:44,20 --> 00:04:46,20
and undo the last two things I did;

101
00:04:46,20 --> 00:04:47,10
get back to that

102
00:04:47,10 --> 00:04:50,00
multi-line comment style and now,

103
00:04:50,00 --> 00:04:52,50
I don't have that error anymore.

104
00:04:52,50 --> 00:04:55,30
So, I'm enforcing a code style that ensures

105
00:04:55,30 --> 00:04:57,80
that my multi-line comments stand out more clearly

106
00:04:57,80 --> 00:04:59,40
from the codes that surrounds them.

107
00:04:59,40 --> 00:05:02,50
And that helps me, whenever I work with my code,

108
00:05:02,50 --> 00:05:04,00
and it helps other developers

109
00:05:04,00 --> 00:05:06,00
that might need to read through it, as well.

