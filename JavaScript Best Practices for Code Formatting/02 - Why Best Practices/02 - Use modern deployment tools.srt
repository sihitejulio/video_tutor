1
00:00:00,60 --> 00:00:02,70
- [Instructor] In the early days of JavaScript,

2
00:00:02,70 --> 00:00:06,20
the code a developer wrote was the code that was executed

3
00:00:06,20 --> 00:00:09,80
in every browser, or other user agent.

4
00:00:09,80 --> 00:00:14,10
Over time, JavaScript evolved and added new features.

5
00:00:14,10 --> 00:00:16,70
However, because not every user was using

6
00:00:16,70 --> 00:00:18,50
the newest version of a browser,

7
00:00:18,50 --> 00:00:20,50
developers had to write code targeted

8
00:00:20,50 --> 00:00:22,30
at the lowest common denominator.

9
00:00:22,30 --> 00:00:25,40
That is, the browser with the fewest modern features

10
00:00:25,40 --> 00:00:29,10
that still maintained a critical mass of users.

11
00:00:29,10 --> 00:00:31,10
This meant that developers were constrained

12
00:00:31,10 --> 00:00:32,70
from using the newest features,

13
00:00:32,70 --> 00:00:35,00
or had to write and maintain multiple versions

14
00:00:35,00 --> 00:00:37,50
of the same code.

15
00:00:37,50 --> 00:00:40,20
The modern JavaScript workflow has resolved this issue

16
00:00:40,20 --> 00:00:42,90
by incorporating transpilers.

17
00:00:42,90 --> 00:00:46,50
A transpiler is a utility that accepts Java Script code

18
00:00:46,50 --> 00:00:48,40
written using modern features,

19
00:00:48,40 --> 00:00:52,90
and returns code that does the same job as the original,

20
00:00:52,90 --> 00:00:55,70
but is written without modern features.

21
00:00:55,70 --> 00:00:59,90
Babel is an example of a popular transpiler for JavaScript.

22
00:00:59,90 --> 00:01:02,80
Including a transpiler in your deployment process

23
00:01:02,80 --> 00:01:04,90
means that you can take advantage of modern

24
00:01:04,90 --> 00:01:07,40
and even cutting edge JavaScript features

25
00:01:07,40 --> 00:01:10,30
without worrying that your code won't perform as expected

26
00:01:10,30 --> 00:01:13,50
for users of older browsers.

27
00:01:13,50 --> 00:01:15,80
Not every modern JavaScript feature

28
00:01:15,80 --> 00:01:17,80
can be transpiled however.

29
00:01:17,80 --> 00:01:21,10
But for those that can't, developers use another tool,

30
00:01:21,10 --> 00:01:23,10
the polyfill.

31
00:01:23,10 --> 00:01:25,30
A polyfill is a library that you include

32
00:01:25,30 --> 00:01:27,70
with your code at deployment.

33
00:01:27,70 --> 00:01:30,70
Because a transpiler can't rebuild some code

34
00:01:30,70 --> 00:01:32,20
for older browsers,

35
00:01:32,20 --> 00:01:35,70
the job of a polyfill is to instead add the functionality

36
00:01:35,70 --> 00:01:40,40
of the new feature to the older browsers at runtime.

37
00:01:40,40 --> 00:01:43,40
Promises and Fetch are a widely used example

38
00:01:43,40 --> 00:01:45,70
of a feature that requires a polyfill

39
00:01:45,70 --> 00:01:49,10
for backward compatibility.

40
00:01:49,10 --> 00:01:52,50
With transpiling and polyfills in the developer toolbox

41
00:01:52,50 --> 00:01:54,40
for JavaScript developers,

42
00:01:54,40 --> 00:01:56,70
the group that maintains JavaScript standards

43
00:01:56,70 --> 00:01:58,90
has been free to innovate without worrying

44
00:01:58,90 --> 00:02:02,80
about proposed changes never being widely available.

45
00:02:02,80 --> 00:02:05,60
As a result, many new features of JavaScript

46
00:02:05,60 --> 00:02:07,60
serve to help developers write cleaner,

47
00:02:07,60 --> 00:02:11,10
more manageable code, rather than to add new features

48
00:02:11,10 --> 00:02:13,60
or capabilities to the language.

49
00:02:13,60 --> 00:02:17,10
It's due to the widespread use of Babel and polyfills

50
00:02:17,10 --> 00:02:19,80
that many modern JavaScript best practices

51
00:02:19,80 --> 00:02:23,00
involve use of these developer-focused features.

