1
00:00:00,70 --> 00:00:02,60
- [Instructor] No matter which other practices

2
00:00:02,60 --> 00:00:03,90
are in your style guide,

3
00:00:03,90 --> 00:00:07,30
or what modern JavaScript features you may be using,

4
00:00:07,30 --> 00:00:09,40
your first step when writing code

5
00:00:09,40 --> 00:00:13,00
should be to ensure that you're working in strict mode.

6
00:00:13,00 --> 00:00:15,00
Indicating that code should be interpreted

7
00:00:15,00 --> 00:00:18,70
in strict mode specifies to user agents like browsers

8
00:00:18,70 --> 00:00:21,50
that they should treat code literally as written,

9
00:00:21,50 --> 00:00:26,50
and throw an error if the code doesn't make sense.

10
00:00:26,50 --> 00:00:28,70
Without strict mode turned on,

11
00:00:28,70 --> 00:00:30,80
user agents often go through a series

12
00:00:30,80 --> 00:00:33,10
of modifications to problematic code

13
00:00:33,10 --> 00:00:36,00
in an attempt to get it to make sense.

14
00:00:36,00 --> 00:00:37,70
In the start file for this video,

15
00:00:37,70 --> 00:00:40,80
I'm going to create and then log a variable.

16
00:00:40,80 --> 00:00:43,40
My idea is to build an app to get a user's location

17
00:00:43,40 --> 00:00:46,30
and identify nearby events.

18
00:00:46,30 --> 00:00:48,80
And for a placeholder location,

19
00:00:48,80 --> 00:00:51,60
I'm going to create a const called city,

20
00:00:51,60 --> 00:00:55,80
and set that to Chicago.

21
00:00:55,80 --> 00:01:01,60
And then I'm going to console.log that.

22
00:01:01,60 --> 00:01:03,70
And I'm going to save that file,

23
00:01:03,70 --> 00:01:06,30
and then switching over to my HTML file

24
00:01:06,30 --> 00:01:07,80
for this video,

25
00:01:07,80 --> 00:01:09,50
I'm going to start my live server,

26
00:01:09,50 --> 00:01:11,40
but you can also just open the HTML file

27
00:01:11,40 --> 00:01:14,00
directly in your browser.

28
00:01:14,00 --> 00:01:16,40
I'm going to open my browser console,

29
00:01:16,40 --> 00:01:19,30
and there we go, I've got Chicago logged

30
00:01:19,30 --> 00:01:23,10
to the console, which is the value of the variable

31
00:01:23,10 --> 00:01:24,90
that I console.logged.

32
00:01:24,90 --> 00:01:28,20
Now going back to the editor and to my JS file,

33
00:01:28,20 --> 00:01:31,40
what if I accidentally failed to declare my variable

34
00:01:31,40 --> 00:01:32,90
but just set a value?

35
00:01:32,90 --> 00:01:36,10
So for instance, if I took the const out here,

36
00:01:36,10 --> 00:01:38,40
and just said city equals Chicago?

37
00:01:38,40 --> 00:01:42,10
If I save this and I go back to my browser,

38
00:01:42,10 --> 00:01:43,40
and I reload my page,

39
00:01:43,40 --> 00:01:46,50
I can see Chicago is still logged.

40
00:01:46,50 --> 00:01:48,30
That's because the browser is attempting

41
00:01:48,30 --> 00:01:52,20
to tease out meaning from my malformed code.

42
00:01:52,20 --> 00:01:54,00
So the browser figures that even though

43
00:01:54,00 --> 00:01:55,90
I didn't declare this variable,

44
00:01:55,90 --> 00:01:57,90
I'm setting a value for it, so I must have

45
00:01:57,90 --> 00:01:59,80
just meant to declare it.

46
00:01:59,80 --> 00:02:02,60
On the surface, this can seem like a fine thing,

47
00:02:02,60 --> 00:02:04,90
and indeed, working outside of strict mode

48
00:02:04,90 --> 00:02:07,20
makes it possible for people to get their feet wet

49
00:02:07,20 --> 00:02:10,20
with JavaScript code without having all the details

50
00:02:10,20 --> 00:02:12,10
quite nailed down.

51
00:02:12,10 --> 00:02:15,00
However, as a developer, I don't want to leave

52
00:02:15,00 --> 00:02:17,60
a bug in my code, because I know it could come back

53
00:02:17,60 --> 00:02:20,70
and bite me later on, and I also just want to

54
00:02:20,70 --> 00:02:22,10
write good code.

55
00:02:22,10 --> 00:02:24,30
And that's where strict mode helps out.

56
00:02:24,30 --> 00:02:27,10
So in my editor, I'm going to add a new first line

57
00:02:27,10 --> 00:02:30,00
to my file, and that's going to be the string

58
00:02:30,00 --> 00:02:34,10
use strict.

59
00:02:34,10 --> 00:02:36,70
Now this is technically just a string,

60
00:02:36,70 --> 00:02:38,60
and that's the magic that prevents it

61
00:02:38,60 --> 00:02:41,10
from throwing an error in older browsers.

62
00:02:41,10 --> 00:02:43,90
They'll just see a string here and move on in the code.

63
00:02:43,90 --> 00:02:46,10
But modern browsers recognize the string

64
00:02:46,10 --> 00:02:49,20
as a statement to switch into strict mode.

65
00:02:49,20 --> 00:02:53,00
And saving this and checking back in my browser,

66
00:02:53,00 --> 00:02:55,20
now I'm seeing a reference error.

67
00:02:55,20 --> 00:02:57,30
And this is what I want when I'm trying to

68
00:02:57,30 --> 00:03:00,00
work with a variable that I haven't defined.

69
00:03:00,00 --> 00:03:04,00
And so the console is pointing me to line three,

70
00:03:04,00 --> 00:03:06,80
so going back to my code,

71
00:03:06,80 --> 00:03:10,30
I'm just going to add const.

72
00:03:10,30 --> 00:03:13,00
Save that.

73
00:03:13,00 --> 00:03:14,70
Back in the browser.

74
00:03:14,70 --> 00:03:17,30
Once again I have the value of my variable logged

75
00:03:17,30 --> 00:03:19,70
and I don't have an error.

76
00:03:19,70 --> 00:03:22,50
Although strict mode may seem like it creates more work

77
00:03:22,50 --> 00:03:25,50
on the front end, it's actually a really good trade-off,

78
00:03:25,50 --> 00:03:27,70
because it saves more headaches later on

79
00:03:27,70 --> 00:03:30,50
as you weed out bugs in the code you write.

80
00:03:30,50 --> 00:03:33,60
So make sure the first line in any JavaScript file

81
00:03:33,60 --> 00:03:36,90
you create is use strict.

82
00:03:36,90 --> 00:03:39,50
Now I can also enforce this in my editor

83
00:03:39,50 --> 00:03:41,40
using eslint.

84
00:03:41,40 --> 00:03:46,30
So opening .eslintrc.js, within the rules object,

85
00:03:46,30 --> 00:03:47,80
I'm going to add my first key,

86
00:03:47,80 --> 00:03:51,20
and that's going to be strict.

87
00:03:51,20 --> 00:03:55,40
Now this rule takes an array of two values.

88
00:03:55,40 --> 00:03:58,60
So I can look up the documentation on eslint.org

89
00:03:58,60 --> 00:04:01,00
to read more about this rule.

90
00:04:01,00 --> 00:04:03,10
Now the rule takes an array of two values.

91
00:04:03,10 --> 00:04:06,60
The first is the severity of the issue if it arises,

92
00:04:06,60 --> 00:04:09,30
which can be off, warn, or error.

93
00:04:09,30 --> 00:04:12,40
And the second is when strict mode is required,

94
00:04:12,40 --> 00:04:16,00
which can be only in functions or globally.

95
00:04:16,00 --> 00:04:17,60
Now I want this to trigger an error,

96
00:04:17,60 --> 00:04:20,70
and I want to require a single global strict mode

97
00:04:20,70 --> 00:04:23,50
declaration in my code.

98
00:04:23,50 --> 00:04:26,90
So I'm going to specify an array.

99
00:04:26,90 --> 00:04:31,00
The first value is going to be the string error.

100
00:04:31,00 --> 00:04:34,80
The second value is going to be the string global.

101
00:04:34,80 --> 00:04:37,10
And then saving that,

102
00:04:37,10 --> 00:04:40,00
I'm going to go back to my app.js file,

103
00:04:40,00 --> 00:04:43,10
and I'm going to comment out my use strict statement.

104
00:04:43,10 --> 00:04:47,10
So this is just modeling what this file would be like

105
00:04:47,10 --> 00:04:49,90
if it didn't have a use strict statement in it.

106
00:04:49,90 --> 00:04:53,30
And immediately I can see the file name

107
00:04:53,30 --> 00:04:55,60
in my explorer bar turns red,

108
00:04:55,60 --> 00:04:58,30
and I get these red squiggles underneath

109
00:04:58,30 --> 00:05:00,30
all of my code.

110
00:05:00,30 --> 00:05:03,10
These are both a result of the error setting,

111
00:05:03,10 --> 00:05:06,10
and if I hover over that underlined code,

112
00:05:06,10 --> 00:05:07,90
I see the error message,

113
00:05:07,90 --> 00:05:09,90
which indicates that I've chosen to enforce

114
00:05:09,90 --> 00:05:12,70
the global form of use strict.

115
00:05:12,70 --> 00:05:15,70
So if I go up here and uncomment my use strict statement,

116
00:05:15,70 --> 00:05:17,20
all those errors go away

117
00:05:17,20 --> 00:05:20,40
because now I'm conforming to that rule that I set

118
00:05:20,40 --> 00:05:22,80
in my eslint configuration file.

119
00:05:22,80 --> 00:05:25,80
And I'll just save those changes.

120
00:05:25,80 --> 00:05:28,20
Including this use strict statement

121
00:05:28,20 --> 00:05:30,70
will work in concert with eslint

122
00:05:30,70 --> 00:05:33,70
to ensure that both the editor and the parser

123
00:05:33,70 --> 00:05:37,10
will prompt me about any lazy coding practices,

124
00:05:37,10 --> 00:05:39,40
and the result is going to be an increase

125
00:05:39,40 --> 00:05:42,00
in the overall quality of my code.

