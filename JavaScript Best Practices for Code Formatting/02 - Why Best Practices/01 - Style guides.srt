1
00:00:00,60 --> 00:00:02,20
- [Instructor] If you've already watched one of my

2
00:00:02,20 --> 00:00:05,20
other courses on JavaScript best practices,

3
00:00:05,20 --> 00:00:08,00
you may already have the background under your belt.

4
00:00:08,00 --> 00:00:11,30
If so, feel free to skip ahead to Chapter Two.

5
00:00:11,30 --> 00:00:14,30
If this is your first time exploring best practices,

6
00:00:14,30 --> 00:00:16,90
it's useful to understand where they come from,

7
00:00:16,90 --> 00:00:19,50
and that's style guides.

8
00:00:19,50 --> 00:00:23,00
A style guide is a set of agreements on how to write code

9
00:00:23,00 --> 00:00:25,90
for everyone working on a common code base,

10
00:00:25,90 --> 00:00:28,50
whether that's on an organizational level,

11
00:00:28,50 --> 00:00:32,30
a project level, or even a specific repo.

12
00:00:32,30 --> 00:00:34,30
A style guide often combines approaches

13
00:00:34,30 --> 00:00:36,80
that developers have found useful in working

14
00:00:36,80 --> 00:00:39,50
with that code base, with choices that developers

15
00:00:39,50 --> 00:00:43,60
have made about how to build and organize that code.

16
00:00:43,60 --> 00:00:46,40
Although guidelines can be project-specific,

17
00:00:46,40 --> 00:00:49,40
they may also be drawn from broader, industry-wide

18
00:00:49,40 --> 00:00:52,10
understandings about the best approach to certain

19
00:00:52,10 --> 00:00:56,30
aspects of building code, known as best practices.

20
00:00:56,30 --> 00:00:59,60
Some best practices may be nearly universal.

21
00:00:59,60 --> 00:01:02,20
For instance, most every developer agrees

22
00:01:02,20 --> 00:01:04,90
that using consistent indents is helpful,

23
00:01:04,90 --> 00:01:08,00
both in building and reading code.

24
00:01:08,00 --> 00:01:11,70
However, many best practices come with trade offs.

25
00:01:11,70 --> 00:01:14,40
As a result, managers, project leaders,

26
00:01:14,40 --> 00:01:16,90
or groups of developers may need to choose

27
00:01:16,90 --> 00:01:20,80
which approach is best for a specific code base.

28
00:01:20,80 --> 00:01:23,80
For instance, modern JavaScript provides an almost

29
00:01:23,80 --> 00:01:27,70
overwhelming range of options for creating functions.

30
00:01:27,70 --> 00:01:29,90
When working on code collaboratively,

31
00:01:29,90 --> 00:01:32,90
does it make more sense to use a function declaration,

32
00:01:32,90 --> 00:01:35,70
or an expression with let or with const?

33
00:01:35,70 --> 00:01:40,00
Maybe an arrow function, but with let or with const?

34
00:01:40,00 --> 00:01:42,20
And if you're using a function expression,

35
00:01:42,20 --> 00:01:44,40
should it include a lexical function name,

36
00:01:44,40 --> 00:01:46,90
to help with debugging?

37
00:01:46,90 --> 00:01:49,60
A style guide, based on best practices,

38
00:01:49,60 --> 00:01:51,70
does a couple things.

39
00:01:51,70 --> 00:01:54,20
First, it keeps you coding, rather than needing

40
00:01:54,20 --> 00:01:55,60
to break your flow while you try

41
00:01:55,60 --> 00:01:58,90
to balance the options and choose a syntax.

42
00:01:58,90 --> 00:02:01,30
It also ensures that the code you write

43
00:02:01,30 --> 00:02:03,10
interacts with the rest of the code base,

44
00:02:03,10 --> 00:02:05,10
in a particular way.

45
00:02:05,10 --> 00:02:07,80
For instance, defining a function using const

46
00:02:07,80 --> 00:02:12,00
ensures that it can't be erroneously redefined.

47
00:02:12,00 --> 00:02:14,20
As you build your knowledge of best practices,

48
00:02:14,20 --> 00:02:17,30
keep in mind there's often no right answer.

49
00:02:17,30 --> 00:02:20,50
So it can be useful to understand the trade offs.

50
00:02:20,50 --> 00:02:23,00
Different projects may make different choices

51
00:02:23,00 --> 00:02:25,20
that work best for a specific team,

52
00:02:25,20 --> 00:02:27,60
architecture, or goal.

53
00:02:27,60 --> 00:02:30,30
The Airbnb Style Guide, is one of the most

54
00:02:30,30 --> 00:02:36,00
widely referenced style guides among JavaScript developers.

55
00:02:36,00 --> 00:02:38,70
Another influential JavaScript style guide,

56
00:02:38,70 --> 00:02:42,70
is the Google Style Guide.

57
00:02:42,70 --> 00:02:46,90
Many best practices can and do change over time.

58
00:02:46,90 --> 00:02:50,30
This is especially true in the modern JavaScript landscape,

59
00:02:50,30 --> 00:02:52,80
where new features are regularly introduced

60
00:02:52,80 --> 00:02:56,40
as optimized replacements for previous practices.

61
00:02:56,40 --> 00:03:00,10
So staying on top of changes in popular style guides,

62
00:03:00,10 --> 00:03:02,40
as well as keeping up with other style guides

63
00:03:02,40 --> 00:03:04,50
that colleagues or developers you respect

64
00:03:04,50 --> 00:03:07,60
may be referencing, can be helpful in ensuring

65
00:03:07,60 --> 00:03:12,00
that your code reflects up-to-date formatting practices.

