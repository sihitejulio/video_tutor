1
00:00:00,60 --> 00:00:02,50
- ESLint is a utility that flags

2
00:00:02,50 --> 00:00:05,00
deviations from selected best practices

3
00:00:05,00 --> 00:00:07,80
right in your editor as you code.

4
00:00:07,80 --> 00:00:11,10
ESLint is widely used by JavaScript developers

5
00:00:11,10 --> 00:00:13,20
to catch and correct issues

6
00:00:13,20 --> 00:00:15,70
before testing and deploying.

7
00:00:15,70 --> 00:00:17,70
Although ESLint allows you to load

8
00:00:17,70 --> 00:00:20,40
a default set of rules, one of it's strengths

9
00:00:20,40 --> 00:00:22,90
is it's configurability.

10
00:00:22,90 --> 00:00:25,00
It's unlikely that you'll want to enforce

11
00:00:25,00 --> 00:00:27,90
every one of the default rules in your code

12
00:00:27,90 --> 00:00:29,80
or that you'll make precisely

13
00:00:29,80 --> 00:00:32,50
the same choices as the defaults.

14
00:00:32,50 --> 00:00:34,10
For this reason it's important

15
00:00:34,10 --> 00:00:37,00
to understand how to configure ESLint.

16
00:00:37,00 --> 00:00:39,90
Otherwise, using it can be an exercise in frustration,

17
00:00:39,90 --> 00:00:42,00
as you have to ignore it's warnings

18
00:00:42,00 --> 00:00:46,40
when they're not relevant to your preferences.

19
00:00:46,40 --> 00:00:48,80
With ESLint installed I could run

20
00:00:48,80 --> 00:00:50,80
a configuration utility.

21
00:00:50,80 --> 00:00:53,80
However, this builds out a package.json file

22
00:00:53,80 --> 00:00:57,40
in associated modules that I don't need here.

23
00:00:57,40 --> 00:00:59,90
So instead, ESLint supports rules

24
00:00:59,90 --> 00:01:02,10
written using JavaScript itself,

25
00:01:02,10 --> 00:01:04,70
as well as yamo or Jason.

26
00:01:04,70 --> 00:01:05,90
I'm going with JavaScript,

27
00:01:05,90 --> 00:01:10,60
so the file is .ESLintrc.js.

28
00:01:10,60 --> 00:01:13,20
The file contains a module.export statement

29
00:01:13,20 --> 00:01:16,60
and within that, a couple keys.

30
00:01:16,60 --> 00:01:19,30
One is the ENV or env key,

31
00:01:19,30 --> 00:01:22,20
which specifies the environment I'm using it in.

32
00:01:22,20 --> 00:01:25,40
Because I'll be using es6 santacs in my code,

33
00:01:25,40 --> 00:01:28,70
I specify the es6 key with a value of true

34
00:01:28,70 --> 00:01:31,40
within the env object.

35
00:01:31,40 --> 00:01:33,20
The other key is named rules

36
00:01:33,20 --> 00:01:35,80
and has an object literal as it's value.

37
00:01:35,80 --> 00:01:39,30
Within that object I can simply add key value pairs

38
00:01:39,30 --> 00:01:42,20
that are rule names in the corresponding values

39
00:01:42,20 --> 00:01:43,50
I want for them.

40
00:01:43,50 --> 00:01:46,30
Those rule names are specified in the documentation

41
00:01:46,30 --> 00:01:49,00
at ESLint.org.

42
00:01:49,00 --> 00:01:51,50
In each video in this course, I'll add

43
00:01:51,50 --> 00:01:54,90
the corresponding ESLint rule if one exists.

44
00:01:54,90 --> 00:01:57,50
In general, you can configure a single

45
00:01:57,50 --> 00:02:00,40
.ESLintrc.js file at the root directory

46
00:02:00,40 --> 00:02:03,20
of the files you want it to apply to.

47
00:02:03,20 --> 00:02:05,30
For this course, I'll have a different version

48
00:02:05,30 --> 00:02:07,40
in each folder, as I'll be adding rules

49
00:02:07,40 --> 00:02:10,10
to it as I go along.

50
00:02:10,10 --> 00:02:14,00
Note that .ESLintrc.js starts with a dot.

51
00:02:14,00 --> 00:02:16,30
This marks it as a configuration file

52
00:02:16,30 --> 00:02:18,30
to your operating system.

53
00:02:18,30 --> 00:02:21,40
These are commonly referred to as dot files.

54
00:02:21,40 --> 00:02:23,00
You may find that this file is not

55
00:02:23,00 --> 00:02:24,40
visible in your file manager

56
00:02:24,40 --> 00:02:27,40
even though you can see it in your editor.

57
00:02:27,40 --> 00:02:29,10
This is the default configuration

58
00:02:29,10 --> 00:02:31,40
for both Mac and Windows.

59
00:02:31,40 --> 00:02:32,40
As long as you can see the file

60
00:02:32,40 --> 00:02:36,60
in your editor, you can open it and save changes.

61
00:02:36,60 --> 00:02:38,50
If you wanted to display hidden files

62
00:02:38,50 --> 00:02:40,60
in your operating system like I have here,

63
00:02:40,60 --> 00:02:43,50
I recommend going to stackoverflow.com

64
00:02:43,50 --> 00:02:47,00
and searching for recommended steps.

