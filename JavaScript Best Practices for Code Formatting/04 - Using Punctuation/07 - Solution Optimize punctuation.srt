1
00:00:00,10 --> 00:00:05,70
(upbeat music0

2
00:00:05,70 --> 00:00:07,60
- [Instructor] This challenge was to enforce

3
00:00:07,60 --> 00:00:11,30
best practices around punctuation and line length

4
00:00:11,30 --> 00:00:14,10
in this java script code.

5
00:00:14,10 --> 00:00:15,70
So the way I like to approach these

6
00:00:15,70 --> 00:00:20,60
is to start with my eslintrc file.

7
00:00:20,60 --> 00:00:24,20
Get the rules in place that will highlight

8
00:00:24,20 --> 00:00:27,30
issues with my existing code

9
00:00:27,30 --> 00:00:30,10
and then I can work on it from there.

10
00:00:30,10 --> 00:00:33,20
So I have several eslint rules that will help me

11
00:00:33,20 --> 00:00:35,20
identify issues with this code

12
00:00:35,20 --> 00:00:37,40
and enforce those best practices.

13
00:00:37,40 --> 00:00:40,60
First off is just requiring semi-colons,

14
00:00:40,60 --> 00:00:42,40
and that's the semi rule.

15
00:00:42,40 --> 00:00:47,80
Don't even need quotes on that.

16
00:00:47,80 --> 00:00:50,30
And that is an error,

17
00:00:50,30 --> 00:00:53,90
and I want it always.

18
00:00:53,90 --> 00:00:56,10
And then I also want to make sure

19
00:00:56,10 --> 00:00:58,40
that I don't have a space before my semi-colons

20
00:00:58,40 --> 00:01:02,20
So that's semi-spacing

21
00:01:02,20 --> 00:01:06,70
and that value is simply error.

22
00:01:06,70 --> 00:01:10,80
And I want to disallow extra unneeded semi-colons,

23
00:01:10,80 --> 00:01:15,40
so that's no-extra-semi

24
00:01:15,40 --> 00:01:18,10
and error.

25
00:01:18,10 --> 00:01:21,00
Then in terms of my lines,

26
00:01:21,00 --> 00:01:24,20
I want to make sure that I don't have

27
00:01:24,20 --> 00:01:25,60
unexpected multilines,

28
00:01:25,60 --> 00:01:33,80
so that's the no unexpected multiline key.

29
00:01:33,80 --> 00:01:37,70
And it's value is simply error.

30
00:01:37,70 --> 00:01:41,40
Then I want to enforce a max length on my code.

31
00:01:41,40 --> 00:01:44,20
And that is the max len rule.

32
00:01:44,20 --> 00:01:46,10
And that value is an array.

33
00:01:46,10 --> 00:01:48,10
First we're going to specify that it should

34
00:01:48,10 --> 00:01:49,60
be considered an error.

35
00:01:49,60 --> 00:01:51,70
And then I need an object.

36
00:01:51,70 --> 00:01:54,10
But I only need one key value pair here.

37
00:01:54,10 --> 00:01:58,70
It's code with a value of 80.

38
00:01:58,70 --> 00:02:01,70
And so that's 80 character lines.

39
00:02:01,70 --> 00:02:04,10
And then finally, I just want to deal

40
00:02:04,10 --> 00:02:07,00
with my comma styles.

41
00:02:07,00 --> 00:02:12,00
So first off, the comma style rule.

42
00:02:12,00 --> 00:02:17,40
And that's an array, with error, and last

43
00:02:17,40 --> 00:02:20,40
to specify that I want my commas

44
00:02:20,40 --> 00:02:23,90
to terminate the lines rather than to start them.

45
00:02:23,90 --> 00:02:29,60
And then comma dangle.

46
00:02:29,60 --> 00:02:32,20
And that's an array, with error,

47
00:02:32,20 --> 00:02:36,70
and then always, multiline,

48
00:02:36,70 --> 00:02:40,50
specifying that whenever I have a multiline

49
00:02:40,50 --> 00:02:42,50
series of values, I want to make sure that

50
00:02:42,50 --> 00:02:44,90
the last one has a terminal comma

51
00:02:44,90 --> 00:02:46,80
even though it doesn't need it.

52
00:02:46,80 --> 00:02:50,70
So I'm going to save those changes to eslintrc

53
00:02:50,70 --> 00:02:53,40
and then switch back to my java script file.

54
00:02:53,40 --> 00:02:58,20
You can see here I've got seven errors flagged.

55
00:02:58,20 --> 00:03:00,30
And so just starting from the top,

56
00:03:00,30 --> 00:03:02,50
I can see that my use strict statement

57
00:03:02,50 --> 00:03:04,20
is missing a semi-colon.

58
00:03:04,20 --> 00:03:07,10
That's easy enough to fix.

59
00:03:07,10 --> 00:03:10,00
I can see here that I'm missing a semi-colon

60
00:03:10,00 --> 00:03:11,90
at the end of my const.

61
00:03:11,90 --> 00:03:14,60
And I fix that, but notice that the error

62
00:03:14,60 --> 00:03:16,50
doesn't go away here in the const

63
00:03:16,50 --> 00:03:21,30
because it also has an overly long line.

64
00:03:21,30 --> 00:03:23,50
So since I'm fixing those semi-colons,

65
00:03:23,50 --> 00:03:24,70
let's just go and take care of those`

66
00:03:24,70 --> 00:03:27,40
'cause I got a few more of those for sure.

67
00:03:27,40 --> 00:03:29,30
So this is missing a semi-colon here

68
00:03:29,30 --> 00:03:32,70
in the cart variable.

69
00:03:32,70 --> 00:03:34,70
We'll get back to the return statement,

70
00:03:34,70 --> 00:03:37,40
but here the for each statement

71
00:03:37,40 --> 00:03:41,10
is missing a semi-colon,

72
00:03:41,10 --> 00:03:42,10
so we'll add that.

73
00:03:42,10 --> 00:03:44,10
And then this cosole.log statement,

74
00:03:44,10 --> 00:03:45,00
don't even have to look.

75
00:03:45,00 --> 00:03:47,00
I know that's missing a semi-colon.

76
00:03:47,00 --> 00:03:49,00
So now I'm down to two errors.

77
00:03:49,00 --> 00:03:51,60
So first off, I've got an overly long line here.

78
00:03:51,60 --> 00:03:54,40
So I'm going to break up my array onto multiple lines.

79
00:03:54,40 --> 00:03:56,30
And I'm going to start that right after

80
00:03:56,30 --> 00:03:58,90
the opening punctuation.

81
00:03:58,90 --> 00:04:02,40
So I'll just put in a line break after each comma

82
00:04:02,40 --> 00:04:04,10
at the beginning of each value.

83
00:04:04,10 --> 00:04:10,30
Because that is the style that I specified.

84
00:04:10,30 --> 00:04:13,80
And then, I'm going to break after that final value

85
00:04:13,80 --> 00:04:15,70
before the terminal punctuation.

86
00:04:15,70 --> 00:04:17,90
And now notice I get an error.

87
00:04:17,90 --> 00:04:19,50
And that error is pointing out

88
00:04:19,50 --> 00:04:21,50
that I'm missing my trailing comma.

89
00:04:21,50 --> 00:04:25,60
So, great to notice that that's being called out.

90
00:04:25,60 --> 00:04:27,80
Add my comma in, and now that line is no longer

91
00:04:27,80 --> 00:04:29,60
too long.

92
00:04:29,60 --> 00:04:32,20
It fits within that 80 character limit.

93
00:04:32,20 --> 00:04:33,60
I've got my list broken up

94
00:04:33,60 --> 00:04:34,40
on multiple lines

95
00:04:34,40 --> 00:04:36,90
using that terminal comma style.

96
00:04:36,90 --> 00:04:40,00
And I have my trailing, dangling comma

97
00:04:40,00 --> 00:04:41,90
at the very end.

98
00:04:41,90 --> 00:04:44,20
And then, I can see already

99
00:04:44,20 --> 00:04:45,60
I've got my unreachable code here

100
00:04:45,60 --> 00:04:46,90
after my return statement.

101
00:04:46,90 --> 00:04:49,40
And if I hover over the return,

102
00:04:49,40 --> 00:04:50,70
it's missing a semi-colon.

103
00:04:50,70 --> 00:04:52,10
So that's assuming that return

104
00:04:52,10 --> 00:04:55,00
is just a statement on it's own line.

105
00:04:55,00 --> 00:04:57,60
But I know that I just need to take out

106
00:04:57,60 --> 00:04:59,30
that line break here.

107
00:04:59,30 --> 00:05:01,20
So that now this code is reachable.

108
00:05:01,20 --> 00:05:03,60
This is an entire statement,

109
00:05:03,60 --> 00:05:06,20
and I've gotten rid of all my errors.

110
00:05:06,20 --> 00:05:09,30
And I've implemented all of these best practices

111
00:05:09,30 --> 00:05:12,70
that we've worked with for punctuation

112
00:05:12,70 --> 00:05:14,40
and line length.

113
00:05:14,40 --> 00:05:15,90
So now my code is readable,

114
00:05:15,90 --> 00:05:18,30
and now it works the way that I intended.

115
00:05:18,30 --> 00:05:20,80
And other developers should be able to read it

116
00:05:20,80 --> 00:05:23,00
and understand what' it's doing as well.

