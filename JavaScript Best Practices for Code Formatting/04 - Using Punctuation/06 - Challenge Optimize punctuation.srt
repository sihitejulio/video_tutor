1
00:00:00,00 --> 00:00:05,70
(pleasant electronic music)

2
00:00:05,70 --> 00:00:07,80
- [Instructor] Ready for a challenge?

3
00:00:07,80 --> 00:00:11,70
The start files contain code for a grocery shopping app.

4
00:00:11,70 --> 00:00:15,30
Edit the script.js file to implement and enforce

5
00:00:15,30 --> 00:00:18,30
best practices for punctuation in JavaScript

6
00:00:18,30 --> 00:00:23,60
including semicolons, line length, and commas.

7
00:00:23,60 --> 00:00:26,10
This should take you about five minutes.

8
00:00:26,10 --> 00:00:28,80
When you're done, join me in the next video,

9
00:00:28,80 --> 00:00:31,00
and I'll walk you through how I approached it.

