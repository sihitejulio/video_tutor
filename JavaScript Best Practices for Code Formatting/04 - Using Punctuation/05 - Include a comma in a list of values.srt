1
00:00:00,70 --> 00:00:03,30
- [Instructor] Lists of values like array elements,

2
00:00:03,30 --> 00:00:05,60
key value pairs and object literals,

3
00:00:05,60 --> 00:00:06,90
and function arguments

4
00:00:06,90 --> 00:00:10,00
require comma's to separate values.

5
00:00:10,00 --> 00:00:12,20
No comma is required at the end,

6
00:00:12,20 --> 00:00:15,20
but when you're using a multiline format

7
00:00:15,20 --> 00:00:17,30
for a series of values,

8
00:00:17,30 --> 00:00:20,00
and you're using a terminal comma format

9
00:00:20,00 --> 00:00:24,00
rather than putting commas at the beginning of the line,

10
00:00:24,00 --> 00:00:27,00
then it's a best practice to include a comma

11
00:00:27,00 --> 00:00:29,90
after the last item as well.

12
00:00:29,90 --> 00:00:32,70
There are a couple of solid reasons for this.

13
00:00:32,70 --> 00:00:36,40
First, it makes it harder to omit a comma between items

14
00:00:36,40 --> 00:00:37,90
when you add it to your list,

15
00:00:37,90 --> 00:00:40,70
because the comma that used to be at the end,

16
00:00:40,70 --> 00:00:45,80
automatically becomes the separator before the new item.

17
00:00:45,80 --> 00:00:49,30
The terminal comma also makes diff's cleaner,

18
00:00:49,30 --> 00:00:51,70
as you don't see a separate deletion

19
00:00:51,70 --> 00:00:54,40
and insertion for the added comma.

20
00:00:54,40 --> 00:00:59,40
Instead, the only change is the newly inserted line.

21
00:00:59,40 --> 00:01:04,20
In the fetch code in my app.js file for my local events app,

22
00:01:04,20 --> 00:01:08,20
I'm specifying an object containing a couple of properties.

23
00:01:08,20 --> 00:01:09,70
I need to add another one,

24
00:01:09,70 --> 00:01:11,60
which is the Api key.

25
00:01:11,60 --> 00:01:13,50
Now, I can add a new line,

26
00:01:13,50 --> 00:01:17,10
and simply add that with a placeholder for now.

27
00:01:17,10 --> 00:01:23,40
So, that's X-Api-Key,

28
00:01:23,40 --> 00:01:25,40
the value, and I'm just going to use

29
00:01:25,40 --> 00:01:31,40
INSERT-API-KEY-HERE as the value,

30
00:01:31,40 --> 00:01:34,30
but notice that I failed to include

31
00:01:34,30 --> 00:01:36,60
a comma after the previous line,

32
00:01:36,60 --> 00:01:38,30
so the editor is already flagging

33
00:01:38,30 --> 00:01:40,60
an error in my code here,

34
00:01:40,60 --> 00:01:45,00
and adding that comma after the url value, fixes things.

35
00:01:45,00 --> 00:01:47,70
So, including a comma at the end of this line,

36
00:01:47,70 --> 00:01:52,00
is going to keep me from forgetting that connecting comma

37
00:01:52,00 --> 00:01:55,90
when I add future key value pairs here.

38
00:01:55,90 --> 00:01:58,30
ESLint supports setting a rule

39
00:01:58,30 --> 00:02:01,30
for trailing commas in object literals.

40
00:02:01,30 --> 00:02:04,80
The rule name is comma-dangle.

41
00:02:04,80 --> 00:02:07,30
In my eslintrc file

42
00:02:07,30 --> 00:02:09,10
I'll add a new line,

43
00:02:09,10 --> 00:02:14,50
and I'm going to add comma-dangle,

44
00:02:14,50 --> 00:02:16,80
and the value is an array

45
00:02:16,80 --> 00:02:19,00
with a string error,

46
00:02:19,00 --> 00:02:23,10
and then the style name just chosen from a list of

47
00:02:23,10 --> 00:02:25,20
defined style names for this rule,

48
00:02:25,20 --> 00:02:31,30
the one I'm using is always-multiline.

49
00:02:31,30 --> 00:02:33,00
I'm not so concerned about commas

50
00:02:33,00 --> 00:02:37,00
at the end of a set of values that all fit on a single line,

51
00:02:37,00 --> 00:02:38,70
it's when I need to add a line break

52
00:02:38,70 --> 00:02:42,20
that I'm most likely to forget the comma.

53
00:02:42,20 --> 00:02:44,90
So, I've been including terminal commas

54
00:02:44,90 --> 00:02:48,50
in this rules list in eslintrc,

55
00:02:48,50 --> 00:02:51,00
and I'll add one here.

56
00:02:51,00 --> 00:02:54,70
Then saving and returning to apa.js,

57
00:02:54,70 --> 00:02:56,30
and I can check things out here.

58
00:02:56,30 --> 00:02:58,20
If I take out that terminal comma,

59
00:02:58,20 --> 00:03:00,80
I immediately get that squiggle,

60
00:03:00,80 --> 00:03:04,30
and it calls out the comma-dangle rule,

61
00:03:04,30 --> 00:03:06,50
saying that I'm missing a trailing comma,

62
00:03:06,50 --> 00:03:10,00
and so I'll add that back.

63
00:03:10,00 --> 00:03:13,50
Now, I'm inoculated against forgetting a separator comma,

64
00:03:13,50 --> 00:03:15,80
if I need to add another key value pair

65
00:03:15,80 --> 00:03:17,70
after this one down the road,

66
00:03:17,70 --> 00:03:21,00
and my get diff's will be cleaner as well.

