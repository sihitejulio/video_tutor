1
00:00:00,70 --> 00:00:03,40
- [Instructor] Aside from automatic semicolon insertion,

2
00:00:03,40 --> 00:00:07,10
white space is not significant in JavaScript.

3
00:00:07,10 --> 00:00:09,10
As a result, you can theoretically

4
00:00:09,10 --> 00:00:11,40
write lines of code of any length.

5
00:00:11,40 --> 00:00:13,10
And indeed one of the ways

6
00:00:13,10 --> 00:00:15,60
that Minification reduces character count

7
00:00:15,60 --> 00:00:18,60
is by reducing code to a single line.

8
00:00:18,60 --> 00:00:22,30
However, when formatting code for human usability,

9
00:00:22,30 --> 00:00:26,20
limiting line length is a widely adopted practice.

10
00:00:26,20 --> 00:00:29,10
Shorter lines enable you to fully view each line

11
00:00:29,10 --> 00:00:31,30
without wrapping or scrolling.

12
00:00:31,30 --> 00:00:33,60
In addition, breaking up a long statement

13
00:00:33,60 --> 00:00:36,90
across multiple lines when combined with indentation,

14
00:00:36,90 --> 00:00:39,90
can help you parse what the entire statement is doing

15
00:00:39,90 --> 00:00:44,80
as well as any steps involved in the process.

16
00:00:44,80 --> 00:00:48,60
Code is generally displayed in a fixed width font,

17
00:00:48,60 --> 00:00:50,00
meaning that each character

18
00:00:50,00 --> 00:00:52,60
takes up the same amount of space on the line.

19
00:00:52,60 --> 00:00:54,30
For this reason it's common

20
00:00:54,30 --> 00:00:57,10
to measure line lengths in characters.

21
00:00:57,10 --> 00:01:01,40
Common line lengths include 80 or 100 characters.

22
00:01:01,40 --> 00:01:05,10
In my app.js file, a few lines look long,

23
00:01:05,10 --> 00:01:09,10
but it's hard to tell just how long they are.

24
00:01:09,10 --> 00:01:13,10
There are a few tools that can help us manage this.

25
00:01:13,10 --> 00:01:17,00
First, ESlint includes the max-len rule,

26
00:01:17,00 --> 00:01:19,40
which lets you set a maximum length

27
00:01:19,40 --> 00:01:21,50
and trigger an error when a line exceeds

28
00:01:21,50 --> 00:01:25,00
the number of characters you specify.

29
00:01:25,00 --> 00:01:28,10
In my eslintrc file,

30
00:01:28,10 --> 00:01:33,00
I'm going to add a max-len key.

31
00:01:33,00 --> 00:01:35,10
For the value, I specify an array,

32
00:01:35,10 --> 00:01:38,50
indicating I want to trigger an error message,

33
00:01:38,50 --> 00:01:42,10
and then for the condition I use an object

34
00:01:42,10 --> 00:01:46,50
with a single value, the string 'code',

35
00:01:46,50 --> 00:01:49,00
and the maximum number of characters,

36
00:01:49,00 --> 00:01:53,00
and I'm going to use 80.

37
00:01:53,00 --> 00:01:56,50
There are a number of other configurations you can add here.

38
00:01:56,50 --> 00:01:59,40
For instance, if your code includes long strings

39
00:01:59,40 --> 00:02:02,50
you may not want to break those up over multiple lines,

40
00:02:02,50 --> 00:02:05,90
so you can exclude those from the rule.

41
00:02:05,90 --> 00:02:08,60
For now, I can save these changes

42
00:02:08,60 --> 00:02:11,80
and return to my app.js file,

43
00:02:11,80 --> 00:02:14,60
and I see an error indicator here,

44
00:02:14,60 --> 00:02:16,80
and when I hover over it,

45
00:02:16,80 --> 00:02:21,40
it tells me my line is too long.

46
00:02:21,40 --> 00:02:23,30
Now by default I wouldn't even be able

47
00:02:23,30 --> 00:02:27,10
to see the entire line of code here because it's so long.

48
00:02:27,10 --> 00:02:30,60
So if you haven't already, I recommend turning on word wrap,

49
00:02:30,60 --> 00:02:33,10
so code that's wider than the browser window

50
00:02:33,10 --> 00:02:35,70
will automatically wrap to fit.

51
00:02:35,70 --> 00:02:37,40
In Visual Studio Code,

52
00:02:37,40 --> 00:02:40,90
I access that by clicking view

53
00:02:40,90 --> 00:02:43,20
and then toggle word wrap.

54
00:02:43,20 --> 00:02:45,10
And so notice if I turn that off

55
00:02:45,10 --> 00:02:48,60
I only seem to be getting this one very long line,

56
00:02:48,60 --> 00:02:53,00
and if I toggle it again, I've got it back.

57
00:02:53,00 --> 00:02:55,60
Now notice that the line number doesn't increment

58
00:02:55,60 --> 00:02:57,70
until there's a line break.

59
00:02:57,70 --> 00:02:59,70
So this is a way I can tell

60
00:02:59,70 --> 00:03:02,50
when something is really long and being wrapped,

61
00:03:02,50 --> 00:03:04,00
or whether it's actually code

62
00:03:04,00 --> 00:03:05,70
that's broken up over multiple lines

63
00:03:05,70 --> 00:03:08,90
in which case I would see multiple line numbers.

64
00:03:08,90 --> 00:03:10,70
Now what would be useful at this point

65
00:03:10,70 --> 00:03:12,10
would be some help figuring out

66
00:03:12,10 --> 00:03:14,20
just where the line needs to end

67
00:03:14,20 --> 00:03:17,20
to help me think about how I might break it up,

68
00:03:17,20 --> 00:03:20,20
and here's where the editor itself can help.

69
00:03:20,20 --> 00:03:24,80
Every modern editor, Visual Studio Code, Sublime, and Atom,

70
00:03:24,80 --> 00:03:28,30
every one supports adding rulers to the editor window

71
00:03:28,30 --> 00:03:29,60
to show the length of a line

72
00:03:29,60 --> 00:03:32,40
of a given number of characters.

73
00:03:32,40 --> 00:03:35,30
In VSCode I'm going to open up my preferences,

74
00:03:35,30 --> 00:03:37,20
so that's, clicking on my Mac,

75
00:03:37,20 --> 00:03:39,20
I'm clicking code, preferences,

76
00:03:39,20 --> 00:03:42,70
and I'm going to go to settings.

77
00:03:42,70 --> 00:03:44,70
And then I can search the settings,

78
00:03:44,70 --> 00:03:48,90
so I'm going to search for ruler,

79
00:03:48,90 --> 00:03:50,70
and then scrolling down,

80
00:03:50,70 --> 00:03:54,60
I'm looking specifically for Editor:Rulers.

81
00:03:54,60 --> 00:03:57,50
And here instead of just entering values,

82
00:03:57,50 --> 00:04:00,00
I need to go to my settings.json file,

83
00:04:00,00 --> 00:04:03,70
so I'll click that link and open that up.

84
00:04:03,70 --> 00:04:06,50
Now I already have a few configurations here,

85
00:04:06,50 --> 00:04:10,00
but you may only have one, or two, or none.

86
00:04:10,00 --> 00:04:17,50
I need to specify a rule with the name editor.rulers, so...

87
00:04:17,50 --> 00:04:20,60
adding a comma after my previous last line,

88
00:04:20,60 --> 00:04:23,90
because this is json, and I have to use double quotes.

89
00:04:23,90 --> 00:04:26,20
So editor

90
00:04:26,20 --> 00:04:29,10
.rulers.

91
00:04:29,10 --> 00:04:31,10
And a colon.

92
00:04:31,10 --> 00:04:33,50
And I include an array as the value,

93
00:04:33,50 --> 00:04:37,60
containing one or more line lengths in characters.

94
00:04:37,60 --> 00:04:39,30
So for now I just want 80,

95
00:04:39,30 --> 00:04:43,40
so I'm going to do an array with a single value of 80.

96
00:04:43,40 --> 00:04:45,00
Remember that this is a json file,

97
00:04:45,00 --> 00:04:49,00
so I cannot end my line with a comma.

98
00:04:49,00 --> 00:04:56,00
I will save that change and then I'll go back to my

99
00:04:56,00 --> 00:05:01,30
JavaScript file, and I do not see the ruler on the screen.

100
00:05:01,30 --> 00:05:03,90
I'll try reducing my font size.

101
00:05:03,90 --> 00:05:05,20
Actually, the first thing I'll do

102
00:05:05,20 --> 00:05:07,20
is hide my explorer bar over here.

103
00:05:07,20 --> 00:05:08,00
There we go.

104
00:05:08,00 --> 00:05:10,30
And now

105
00:05:10,30 --> 00:05:13,90
I can see this ruler over here,

106
00:05:13,90 --> 00:05:16,30
at 80 characters,

107
00:05:16,30 --> 00:05:20,60
which gives me some idea of where the lines get too long.

108
00:05:20,60 --> 00:05:24,50
And so now I can break up my code across multiple lines.

109
00:05:24,50 --> 00:05:27,00
So with a fetch statement

110
00:05:27,00 --> 00:05:28,00
I can just go ahead and put

111
00:05:28,00 --> 00:05:30,20
those chain methods on their own lines.

112
00:05:30,20 --> 00:05:33,80
So I will break it .then,

113
00:05:33,80 --> 00:05:36,00
and I've got an if statement here

114
00:05:36,00 --> 00:05:40,40
so that could use its own line.

115
00:05:40,40 --> 00:05:44,40
Contents of the if statement...

116
00:05:44,40 --> 00:05:45,40
That closing curly brace,

117
00:05:45,40 --> 00:05:49,00
an else statement...

118
00:05:49,00 --> 00:05:52,50
Another closing curly brace,

119
00:05:52,50 --> 00:05:56,50
and I've got another chained .then,

120
00:05:56,50 --> 00:05:59,90
and a chained .catch.

121
00:05:59,90 --> 00:06:02,40
And so I've gotten rid of that error,

122
00:06:02,40 --> 00:06:06,20
I can see visually because of my ruler in my editor,

123
00:06:06,20 --> 00:06:08,50
that all of my lines of code

124
00:06:08,50 --> 00:06:11,50
are beneath 80 characters in length.

125
00:06:11,50 --> 00:06:15,00
And I have also gotten rid of those ESlint errors,

126
00:06:15,00 --> 00:06:18,70
so that's another way of verifying that.

127
00:06:18,70 --> 00:06:24,10
And so, using that ruler along with those rules

128
00:06:24,10 --> 00:06:28,30
helps me to break up my code into manageable line lengths

129
00:06:28,30 --> 00:06:33,00
and helps me verify that I've done it when I've done it.

