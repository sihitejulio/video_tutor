1
00:00:00,80 --> 00:00:02,40
- [Instructor] Several types of JavaScript

2
00:00:02,40 --> 00:00:06,00
statements uses commas to separate sequential items.

3
00:00:06,00 --> 00:00:08,20
For instance, elements in a array literals

4
00:00:08,20 --> 00:00:09,90
are separated by commas,

5
00:00:09,90 --> 00:00:13,50
as are key value pairs in object literals.

6
00:00:13,50 --> 00:00:16,50
When a list of values is too long for a single line,

7
00:00:16,50 --> 00:00:18,10
it's common to break the list up

8
00:00:18,10 --> 00:00:20,70
by putting each item on it's own line.

9
00:00:20,70 --> 00:00:25,20
This means that the commas are accompanied by line breaks.

10
00:00:25,20 --> 00:00:28,80
Some developers use commas at the end of each line,

11
00:00:28,80 --> 00:00:31,40
while others place them at the start.

12
00:00:31,40 --> 00:00:34,30
Whichever you choose, consistency is important.

13
00:00:34,30 --> 00:00:36,20
Both to make your code more readable

14
00:00:36,20 --> 00:00:38,30
and to enable you to catch errors,

15
00:00:38,30 --> 00:00:42,40
more importantly, omitted commas.

16
00:00:42,40 --> 00:00:44,90
In my location app, the fetch request

17
00:00:44,90 --> 00:00:48,40
can take a number of parameters using an object literal.

18
00:00:48,40 --> 00:00:50,50
The values don't all fit in a single line

19
00:00:50,50 --> 00:00:52,20
so I already have an error here

20
00:00:52,20 --> 00:00:53,90
because I've specified a maximum

21
00:00:53,90 --> 00:00:57,50
line length of 80 characters.

22
00:00:57,50 --> 00:01:00,60
So I want to break that object up,

23
00:01:00,60 --> 00:01:04,90
and one way to do this is to use the comma first style.

24
00:01:04,90 --> 00:01:09,20
So I have my opening here for my object

25
00:01:09,20 --> 00:01:13,60
and I'm going to put the first key

26
00:01:13,60 --> 00:01:15,70
on it's own line.

27
00:01:15,70 --> 00:01:21,00
And then, I'm going to start a new line with this comma,

28
00:01:21,00 --> 00:01:26,30
so that my second key is on it's own line.

29
00:01:26,30 --> 00:01:31,50
Then I can put my ending for that object literal

30
00:01:31,50 --> 00:01:34,10
on it's own line.

31
00:01:34,10 --> 00:01:36,60
And to make this all line up,

32
00:01:36,60 --> 00:01:39,50
I probably want to indent here

33
00:01:39,50 --> 00:01:41,70
just so that all the keys

34
00:01:41,70 --> 00:01:46,60
and the closing punctuation are all vertically aligned,

35
00:01:46,60 --> 00:01:49,70
meaning that if there's an issue with a missing character

36
00:01:49,70 --> 00:01:51,50
or a missing space,

37
00:01:51,50 --> 00:01:54,30
it's immediately obvious.

38
00:01:54,30 --> 00:01:57,20
My preferred style though is comma last.

39
00:01:57,20 --> 00:01:59,30
I like to see that values first

40
00:01:59,30 --> 00:02:01,70
and so I'm going to switch to that.

41
00:02:01,70 --> 00:02:04,30
And so that's really just going to mean

42
00:02:04,30 --> 00:02:08,20
taking away that line break there

43
00:02:08,20 --> 00:02:12,70
and instead, adding a line break here and likewise,

44
00:02:12,70 --> 00:02:16,20
taking away my line break before that comma

45
00:02:16,20 --> 00:02:20,70
and just moving those closing character onto their own line.

46
00:02:20,70 --> 00:02:26,00
So here, the key names are front and center.

47
00:02:26,00 --> 00:02:28,70
ESLint lets me enforce a comma style

48
00:02:28,70 --> 00:02:31,50
using the comma style rule.

49
00:02:31,50 --> 00:02:34,30
So in my ESLintrc file,

50
00:02:34,30 --> 00:02:38,50
I'm going to add a new line at the end of my rules object

51
00:02:38,50 --> 00:02:43,40
and I'm going to specify the comma-style key

52
00:02:43,40 --> 00:02:45,00
with a value that's in array,

53
00:02:45,00 --> 00:02:50,60
staring with error and then the key word last,

54
00:02:50,60 --> 00:02:54,60
meaning that I want to enforce a comma-last style.

55
00:02:54,60 --> 00:02:57,00
And notice also that I've already been

56
00:02:57,00 --> 00:03:00,60
implementing this style in my ESLintrc file.

57
00:03:00,60 --> 00:03:04,90
So, all of my key value pairs end with a comma

58
00:03:04,90 --> 00:03:09,00
rather than all of these lines starting with a comma.

59
00:03:09,00 --> 00:03:12,30
And then switching back to app.js,

60
00:03:12,30 --> 00:03:14,60
if I take out this comma,

61
00:03:14,60 --> 00:03:16,80
add it to the beginning of this line instead,

62
00:03:16,80 --> 00:03:21,00
I get a squiggle here and it specifically says,

63
00:03:21,00 --> 00:03:23,10
"Comma should be placed last."

64
00:03:23,10 --> 00:03:27,80
So I can undo that change and now I'm all good.

65
00:03:27,80 --> 00:03:32,00
So again, the comma style you pick is a matter of preference

66
00:03:32,00 --> 00:03:34,50
but it's important to be consistent with you commas

67
00:03:34,50 --> 00:03:38,00
either starting or ending your lines of code.

