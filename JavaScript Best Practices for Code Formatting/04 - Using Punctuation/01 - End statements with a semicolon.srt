1
00:00:00,60 --> 00:00:01,90
- [Instructor] You're probably used to seeing

2
00:00:01,90 --> 00:00:05,30
most lines in JavaScript code ended with semicolons.

3
00:00:05,30 --> 00:00:08,10
Although not strictly necessary, it's a best practice

4
00:00:08,10 --> 00:00:11,10
to include a semicolon at the end of every expression,

5
00:00:11,10 --> 00:00:14,90
such as a variable declaration, or a return statement.

6
00:00:14,90 --> 00:00:18,30
Some developers write JavaScript without semicolons.

7
00:00:18,30 --> 00:00:21,10
In a lot of cases, it's obvious to the parser

8
00:00:21,10 --> 00:00:24,20
where one statement ends, and the next begins,

9
00:00:24,20 --> 00:00:26,60
but there are some corner cases that can trip up

10
00:00:26,60 --> 00:00:28,80
even experienced developers.

11
00:00:28,80 --> 00:00:32,10
As a result, including semicolons is a simple way

12
00:00:32,10 --> 00:00:35,70
to make it clear where you intend your statements to end,

13
00:00:35,70 --> 00:00:37,80
and to illustrate the structure of your code

14
00:00:37,80 --> 00:00:41,40
for other developers who might need to work with it as well.

15
00:00:41,40 --> 00:00:43,80
JavaScript statements that end with a code block,

16
00:00:43,80 --> 00:00:47,90
such as an if/else block, don't need semicolons at the end,

17
00:00:47,90 --> 00:00:51,40
because the closing curly brace terminates the statement.

18
00:00:51,40 --> 00:00:53,80
But as a rule, you should terminate any statement

19
00:00:53,80 --> 00:00:57,90
that doesn't end with a curly brace with a semicolon.

20
00:00:57,90 --> 00:01:00,50
And note that even though an object literal

21
00:01:00,50 --> 00:01:04,50
ends with a curly brace, it's not a block, it's data.

22
00:01:04,50 --> 00:01:07,30
An expression with a function literal on the right

23
00:01:07,30 --> 00:01:11,90
needs to end with a semicolon as well.

24
00:01:11,90 --> 00:01:15,70
My app.js file includes code for checking

25
00:01:15,70 --> 00:01:18,50
the user's geolocation, so my app can start

26
00:01:18,50 --> 00:01:20,50
with a nearby view.

27
00:01:20,50 --> 00:01:22,80
But if geolocation isn't available,

28
00:01:22,80 --> 00:01:25,70
or if a user declines to share that data,

29
00:01:25,70 --> 00:01:28,40
we can use fallback data from the user's account

30
00:01:28,40 --> 00:01:29,90
when they set it up.

31
00:01:29,90 --> 00:01:32,30
So I have user preferences, which would come

32
00:01:32,30 --> 00:01:35,10
from my back end, saved as an object.

33
00:01:35,10 --> 00:01:38,50
And then conditional code for checking geolocation.

34
00:01:38,50 --> 00:01:41,00
I've written this without semicolons.

35
00:01:41,00 --> 00:01:43,20
You can still get the basic structure of the code

36
00:01:43,20 --> 00:01:45,00
from my indents and line breaks,

37
00:01:45,00 --> 00:01:47,60
but semicolons will ensure the parser understands

38
00:01:47,60 --> 00:01:49,90
exactly what I intend.

39
00:01:49,90 --> 00:01:53,60
So switching over to my eslintrc file,

40
00:01:53,60 --> 00:01:56,20
I'm going to add three rules.

41
00:01:56,20 --> 00:02:00,40
First is semi, and since it's a single word,

42
00:02:00,40 --> 00:02:03,50
I don't need quotes around it.

43
00:02:03,50 --> 00:02:06,70
And semi specifies that semicolons

44
00:02:06,70 --> 00:02:10,10
can't be omitted where required.

45
00:02:10,10 --> 00:02:12,30
And the value for this is an array,

46
00:02:12,30 --> 00:02:15,40
starting with error, and we're just going to say

47
00:02:15,40 --> 00:02:18,80
we want them always.

48
00:02:18,80 --> 00:02:20,60
I also want to enforce readability

49
00:02:20,60 --> 00:02:24,40
by specifying no space before a semicolon.

50
00:02:24,40 --> 00:02:30,50
That rule is semi-spacing, and I can simply say error,

51
00:02:30,50 --> 00:02:32,80
because I'm saying yes, I want semi-spacing.

52
00:02:32,80 --> 00:02:36,10
And I need a comma there, not a semicolon.

53
00:02:36,10 --> 00:02:40,50
And finally, I can set a flag for no extra semicolons,

54
00:02:40,50 --> 00:02:42,50
just to keep things clean.

55
00:02:42,50 --> 00:02:48,50
So eslint has a no-extra-semi rule,

56
00:02:48,50 --> 00:02:51,60
and there again I can just say error,

57
00:02:51,60 --> 00:02:53,90
which means that if there's an extra semicolon,

58
00:02:53,90 --> 00:02:56,50
it will flag an error.

59
00:02:56,50 --> 00:02:57,70
So I saved those changes.

60
00:02:57,70 --> 00:03:01,30
I immediately see that my editor is flagging

61
00:03:01,30 --> 00:03:03,90
some issues with my app.js file.

62
00:03:03,90 --> 00:03:07,10
And so switching over here,

63
00:03:07,10 --> 00:03:09,60
you can start with that use strict statement,

64
00:03:09,60 --> 00:03:12,00
I hover over that squiggle, I see the missing semicolon,

65
00:03:12,00 --> 00:03:16,60
referencing the semi rule,

66
00:03:16,60 --> 00:03:20,10
and I add that, and that fixes that one.

67
00:03:20,10 --> 00:03:22,40
And notice that I have an error

68
00:03:22,40 --> 00:03:25,10
at the end of my const statement here,

69
00:03:25,10 --> 00:03:27,00
after the curly brace,

70
00:03:27,00 --> 00:03:29,00
but down after my if/else construction,

71
00:03:29,00 --> 00:03:31,60
there's no error flagged.

72
00:03:31,60 --> 00:03:35,20
And if I add a semicolon after the else,

73
00:03:35,20 --> 00:03:39,00
I get a warning, because that's an unnecessary semicolon,

74
00:03:39,00 --> 00:03:41,60
which I specifically said I don't want.

75
00:03:41,60 --> 00:03:44,70
So again, I don't use a semicolon after a code block.

76
00:03:44,70 --> 00:03:45,80
I'm going to take that out.

77
00:03:45,80 --> 00:03:48,00
But I do up here after an object literal,

78
00:03:48,00 --> 00:03:51,50
so I need to add a semicolon there.

79
00:03:51,50 --> 00:03:54,30
And then I just need to add semicolons

80
00:03:54,30 --> 00:04:00,60
after each console.log statement, here and here.

81
00:04:00,60 --> 00:04:04,70
And also here.

82
00:04:04,70 --> 00:04:07,60
And just based on the sizing on my screen,

83
00:04:07,60 --> 00:04:10,40
that terminal semicolon is going onto the next line,

84
00:04:10,40 --> 00:04:11,90
but we can see that's word wrap,

85
00:04:11,90 --> 00:04:14,90
because there's no new line number here in my editor.

86
00:04:14,90 --> 00:04:16,70
So this semicolon's just at the end

87
00:04:16,70 --> 00:04:18,90
of this console.log statement.

88
00:04:18,90 --> 00:04:22,50
And then, finally, I have one more squiggle here,

89
00:04:22,50 --> 00:04:26,80
because this is the end of my getCurrentPosition method.

90
00:04:26,80 --> 00:04:30,20
So I need a semicolon to terminate that statement as well.

91
00:04:30,20 --> 00:04:32,60
And then I want to test out the other rules.

92
00:04:32,60 --> 00:04:36,10
So if I add a space before a semicolon,

93
00:04:36,10 --> 00:04:39,40
I can see that I get a squiggle,

94
00:04:39,40 --> 00:04:43,20
and it says unexpected white space right there.

95
00:04:43,20 --> 00:04:45,20
I can take that out, and I can also,

96
00:04:45,20 --> 00:04:46,80
what happens if I just put a second semicolon?

97
00:04:46,80 --> 00:04:49,30
It shouldn't bother anything.

98
00:04:49,30 --> 00:04:53,20
But again, I get an unnecessary semicolon message.

99
00:04:53,20 --> 00:04:56,10
So I'll take that out, I have no more errors,

100
00:04:56,10 --> 00:04:57,40
I can save that.

101
00:04:57,40 --> 00:05:00,80
And now my code uses semicolons consistently,

102
00:05:00,80 --> 00:05:03,20
but doesn't include them where they're not needed.

103
00:05:03,20 --> 00:05:05,20
And that is going to help me write code

104
00:05:05,20 --> 00:05:07,20
that parses as I expect,

105
00:05:07,20 --> 00:05:10,00
and that's readable for other developers.

