1
00:00:00,70 --> 00:00:02,90
- [Speaker] From a technical standpoint JavaScript

2
00:00:02,90 --> 00:00:06,00
doesn't absolutely require semicolons.

3
00:00:06,00 --> 00:00:10,00
Indeed, some developers write JavaScript without semicolons,

4
00:00:10,00 --> 00:00:13,20
relying on automatic semicolon insertion

5
00:00:13,20 --> 00:00:15,40
which is exactly what it sounds like.

6
00:00:15,40 --> 00:00:18,70
JavaScript's built in process for identifying a logical end

7
00:00:18,70 --> 00:00:21,30
of a statement and the beginning of a new one

8
00:00:21,30 --> 00:00:23,30
based on context.

9
00:00:23,30 --> 00:00:26,50
However, it's best not to rely on this mechanism,

10
00:00:26,50 --> 00:00:30,60
because it can have some unintended consequences.

11
00:00:30,60 --> 00:00:33,90
The classic example of where automatic semicolon insertion

12
00:00:33,90 --> 00:00:35,90
can behave differently than you want

13
00:00:35,90 --> 00:00:38,30
is when you use the return statement.

14
00:00:38,30 --> 00:00:41,10
If a return statement is followed by a line break

15
00:00:41,10 --> 00:00:44,40
parsers will treat it as as if it's followed by a semicolon.

16
00:00:44,40 --> 00:00:47,50
This means any continuation on the following line,

17
00:00:47,50 --> 00:00:51,00
such as a value to be returned will instead be treated

18
00:00:51,00 --> 00:00:53,40
as a separate statement.

19
00:00:53,40 --> 00:00:57,20
In the app.js file for this video lines 10-11

20
00:00:57,20 --> 00:01:00,60
are meant to be a single statement.

21
00:01:00,60 --> 00:01:03,30
It's already clear from the editor here that line 11

22
00:01:03,30 --> 00:01:07,80
is unreachable. I can tell that because my editor

23
00:01:07,80 --> 00:01:09,80
grazed the code out a little bit

24
00:01:09,80 --> 00:01:11,80
and when I hover over that I can see

25
00:01:11,80 --> 00:01:13,80
unreachable code detected.

26
00:01:13,80 --> 00:01:17,20
And the return statement is also marked

27
00:01:17,20 --> 00:01:21,10
with an eslint(semi) error for a missing semicolon,

28
00:01:21,10 --> 00:01:25,50
because I have a semi rule set in eslintrc file.

29
00:01:25,50 --> 00:01:29,00
But just to test things out what should happen here

30
00:01:29,00 --> 00:01:33,40
when I actually execute this code is that I enter a value,

31
00:01:33,40 --> 00:01:36,70
the makeLowerCase function converts my entry to lower case

32
00:01:36,70 --> 00:01:40,30
and it gets added to userPrefs with the key setting.

33
00:01:40,30 --> 00:01:44,30
So, I can open up my index.html file.

34
00:01:44,30 --> 00:01:49,20
I'm going to open that up in my browser and I get a prompt .

35
00:01:49,20 --> 00:01:53,30
I'm going to enter Urban with a capital U.

36
00:01:53,30 --> 00:01:55,10
Click OK.

37
00:01:55,10 --> 00:01:58,40
And then opening up my console I have an error

38
00:01:58,40 --> 00:02:01,60
about unreachable code and then on line 17

39
00:02:01,60 --> 00:02:05,70
that setting value is logged and the value is undefined.

40
00:02:05,70 --> 00:02:08,60
And that's because the function returned without a value,

41
00:02:08,60 --> 00:02:13,30
due to that premature line break.

42
00:02:13,30 --> 00:02:15,60
If I go back to my code

43
00:02:15,60 --> 00:02:18,20
and I remove

44
00:02:18,20 --> 00:02:20,70
that extra line break.

45
00:02:20,70 --> 00:02:22,60
I can save that,

46
00:02:22,60 --> 00:02:25,70
return to my browser.

47
00:02:25,70 --> 00:02:29,60
Prompted again and type in Urban with a capital U.

48
00:02:29,60 --> 00:02:34,40
And now I get urban logged to the console

49
00:02:34,40 --> 00:02:37,90
that has been run through the makLowerCase function

50
00:02:37,90 --> 00:02:39,90
and it's all lower case.

51
00:02:39,90 --> 00:02:42,10
So, everything now works as expected

52
00:02:42,10 --> 00:02:45,80
by taking out that unexpected line break.

53
00:02:45,80 --> 00:02:50,10
ESlint supports a no-unexpected-multiline rule.

54
00:02:50,10 --> 00:02:53,20
This rule looks for omitted semicolons in situations

55
00:02:53,20 --> 00:02:55,10
where consecutive lines could be treated

56
00:02:55,10 --> 00:02:56,60
as single statement.

57
00:02:56,60 --> 00:03:02,40
So, the reverse of the issue with the return statement.

58
00:03:02,40 --> 00:03:07,40
In my eslintrc file I'll add that rule.

59
00:03:07,40 --> 00:03:12,80
So, it's no-unexpected-multiline

60
00:03:12,80 --> 00:03:14,80
(typing)

61
00:03:14,80 --> 00:03:19,70
with a value of error.

62
00:03:19,70 --> 00:03:21,30
ending with a comma.

63
00:03:21,30 --> 00:03:25,80
So saving that, going back to app.js and I have a couple

64
00:03:25,80 --> 00:03:28,20
flags down here.

65
00:03:28,20 --> 00:03:31,10
These are both situations where I included a line break

66
00:03:31,10 --> 00:03:33,20
in code that shouldn't have it.

67
00:03:33,20 --> 00:03:35,50
So, both of these situations are parsed

68
00:03:35,50 --> 00:03:38,60
without automatic semicolon insertion,

69
00:03:38,60 --> 00:03:40,10
and to make that clear,

70
00:03:40,10 --> 00:03:44,80
I'm just going to remove those line breaks.

71
00:03:44,80 --> 00:03:46,20
There

72
00:03:46,20 --> 00:03:48,40
Then here

73
00:03:48,40 --> 00:03:52,40
And that makes the error flags go away.

74
00:03:52,40 --> 00:03:54,00
And I'm going to save this

75
00:03:54,00 --> 00:03:59,70
and I'll go back to my browser and retest that.

76
00:03:59,70 --> 00:04:02,80
(typing)

77
00:04:02,80 --> 00:04:04,90
And again the code is still working,

78
00:04:04,90 --> 00:04:06,50
nothing has changed.

79
00:04:06,50 --> 00:04:09,00
So, again it's possible to write JavaScript

80
00:04:09,00 --> 00:04:10,30
without semicolons,

81
00:04:10,30 --> 00:04:13,30
but it comes with some perils and to avoid that.

82
00:04:13,30 --> 00:04:16,60
You can make sure to terminate every statement appropriately

83
00:04:16,60 --> 00:04:19,00
and we can use ESlint to help us out there.

