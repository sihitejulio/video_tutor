1
00:00:00,70 --> 00:00:02,60
- [Instructor] There's a wide variety of approaches

2
00:00:02,60 --> 00:00:06,10
to spacing after an opening paren bracket or brace,

3
00:00:06,10 --> 00:00:09,30
and before the corresponding closing character.

4
00:00:09,30 --> 00:00:12,40
Developers who pad some or all of these enclosing characters

5
00:00:12,40 --> 00:00:14,70
with spaces may feel that the spaces

6
00:00:14,70 --> 00:00:17,90
make the enclosed values easier to read.

7
00:00:17,90 --> 00:00:20,70
On the other hand, the choice to omit the spaces

8
00:00:20,70 --> 00:00:23,30
has the advantage of creating a clear association

9
00:00:23,30 --> 00:00:26,30
between values and the enclosing characters

10
00:00:26,30 --> 00:00:30,70
which generally indicate what type of data is enclosed.

11
00:00:30,70 --> 00:00:33,80
Many style guides specify an approach to spacing

12
00:00:33,80 --> 00:00:35,80
within enclosing characters.

13
00:00:35,80 --> 00:00:39,10
So if your project or organization has a style guide,

14
00:00:39,10 --> 00:00:40,90
that's a great place to start.

15
00:00:40,90 --> 00:00:42,80
If you're creating your own style,

16
00:00:42,80 --> 00:00:45,80
then you should pick a style for each type of character

17
00:00:45,80 --> 00:00:48,20
and use that style consistently.

18
00:00:48,20 --> 00:00:50,60
Note that the style doesn't need to be the same

19
00:00:50,60 --> 00:00:53,30
among all types of enclosing characters.

20
00:00:53,30 --> 00:00:56,00
In fact, popular style guides often set out

21
00:00:56,00 --> 00:00:58,80
one or more distinct styles for parens,

22
00:00:58,80 --> 00:01:01,70
brackets, and braces.

23
00:01:01,70 --> 00:01:04,60
I want a standardized spacing in the enclosing characters

24
00:01:04,60 --> 00:01:09,10
in my app.js file, and also in my .eslintrc file

25
00:01:09,10 --> 00:01:11,70
which contains a lot of them as well.

26
00:01:11,70 --> 00:01:14,60
ESLint supports a rule to let me specify

27
00:01:14,60 --> 00:01:17,70
the styles I'd like to enforce for parens.

28
00:01:17,70 --> 00:01:20,20
That's the space-in-parens rule.

29
00:01:20,20 --> 00:01:23,20
It also supports a rule that lets me specify spacing

30
00:01:23,20 --> 00:01:29,70
inside of brackets which is the array-bracket-spacing rule.

31
00:01:29,70 --> 00:01:33,60
In addition, it supports a separate rule

32
00:01:33,60 --> 00:01:36,90
specifically for spacing inside braces,

33
00:01:36,90 --> 00:01:41,40
this is known as the object-curly-spacing rule.

34
00:01:41,40 --> 00:01:45,90
In my .eslintrc file I'm going to go ahead and add those rules.

35
00:01:45,90 --> 00:01:49,50
Now I generally don't use spaces within parens or brackets,

36
00:01:49,50 --> 00:01:52,70
but I do use them in braces, and this is the style

37
00:01:52,70 --> 00:01:55,40
that the Airbnb style guide uses.

38
00:01:55,40 --> 00:02:01,60
So, I'm going to add, space in parens

39
00:02:01,60 --> 00:02:06,60
and the value there is going to be, error and never

40
00:02:06,60 --> 00:02:11,00
and then, array bracket spacing

41
00:02:11,00 --> 00:02:16,50
and again, error and never.

42
00:02:16,50 --> 00:02:21,30
And finally, object curly spacing

43
00:02:21,30 --> 00:02:28,00
and that's going to be, error and always

44
00:02:28,00 --> 00:02:31,20
because that's where I do actually include spaces.

45
00:02:31,20 --> 00:02:33,50
So I'm going to save this, and I can see

46
00:02:33,50 --> 00:02:37,60
that I have some errors popping up in the app.js file.

47
00:02:37,60 --> 00:02:42,10
So I'll start there, and right here,

48
00:02:42,10 --> 00:02:44,30
should be no spaces inside this paren,

49
00:02:44,30 --> 00:02:47,70
so I can take that space out, same thing here,

50
00:02:47,70 --> 00:02:50,70
no spaces before the closing paren,

51
00:02:50,70 --> 00:02:56,20
and down here I have an errant space, and here as well.

52
00:02:56,20 --> 00:03:00,20
And so that takes care of any issues in my app.js file.

53
00:03:00,20 --> 00:03:02,60
Now I've chosen to do an .eslintrc file

54
00:03:02,60 --> 00:03:05,20
that is itself a JavaScript file.

55
00:03:05,20 --> 00:03:09,80
But by default, ESLint ignores dot files

56
00:03:09,80 --> 00:03:11,60
which are hidden files.

57
00:03:11,60 --> 00:03:18,00
And so even though I do have a few issues in my .eslint file

58
00:03:18,00 --> 00:03:20,10
for instance, I haven't been putting spaces

59
00:03:20,10 --> 00:03:24,10
inside my braces, my ESLintRC rules

60
00:03:24,10 --> 00:03:27,30
are not being applied to that file itself.

61
00:03:27,30 --> 00:03:29,10
So in the start files for this video,

62
00:03:29,10 --> 00:03:32,00
I've included an .eslintignore file,

63
00:03:32,00 --> 00:03:35,20
and this is simply a configuration file

64
00:03:35,20 --> 00:03:38,30
that I can include in my project setup

65
00:03:38,30 --> 00:03:42,20
that specifies that ESLint should not ignore

66
00:03:42,20 --> 00:03:47,00
the .eslintrc.js file.

67
00:03:47,00 --> 00:03:49,40
So I'm going to switch over to Finder,

68
00:03:49,40 --> 00:03:52,30
and in the folder for this project,

69
00:03:52,30 --> 00:03:54,40
and for this specific video in the Begin folder,

70
00:03:54,40 --> 00:03:57,20
here's my .eslintignore file.

71
00:03:57,20 --> 00:03:59,60
Now on a Mac, this is a hidden file,

72
00:03:59,60 --> 00:04:01,70
and if you can't see it, you can press

73
00:04:01,70 --> 00:04:06,10
the keyboard combination, Command + Shift + Period

74
00:04:06,10 --> 00:04:10,10
to toggle whether or not those hidden files are visible.

75
00:04:10,10 --> 00:04:13,60
So then I want to drag that .eslintignore file

76
00:04:13,60 --> 00:04:18,00
over to my Exercise Files folder.

77
00:04:18,00 --> 00:04:22,50
SO now that .eslintignore file is at the base

78
00:04:22,50 --> 00:04:26,20
of this whole project where the package-lock.json file

79
00:04:26,20 --> 00:04:28,50
is in the node modules that configure

80
00:04:28,50 --> 00:04:30,70
ESLint for this project.

81
00:04:30,70 --> 00:04:33,90
And now when I switch back to VSCode,

82
00:04:33,90 --> 00:04:36,60
I have a whole bunch of errors (laughs)

83
00:04:36,60 --> 00:04:42,70
so let's start at the beginning.

84
00:04:42,70 --> 00:04:45,00
And so it says, Use the global form 'use strict'

85
00:04:45,00 --> 00:04:49,50
so I've overlooked a few rules that I've set globally here,

86
00:04:49,50 --> 00:04:52,40
so I'm going to make sure to add my use strict,

87
00:04:52,40 --> 00:04:55,30
'cause this is after all a JavaScript file.

88
00:04:55,30 --> 00:04:58,20
Here in my true value, I'm missing a trailing comma

89
00:04:58,20 --> 00:05:01,90
which is another standard that I specified

90
00:05:01,90 --> 00:05:04,60
in my ESLint rules.

91
00:05:04,60 --> 00:05:09,50
And now I have a number of little underlines here and there

92
00:05:09,50 --> 00:05:12,80
on enclosing characters,

93
00:05:12,80 --> 00:05:15,50
so no space before that square bracket,

94
00:05:15,50 --> 00:05:17,50
so we'll take that out.

95
00:05:17,50 --> 00:05:20,10
No space after that square bracket, we'll take that out,

96
00:05:20,10 --> 00:05:22,80
likewise over here.

97
00:05:22,80 --> 00:05:26,00
Here, here,

98
00:05:26,00 --> 00:05:28,20
and then down here with my brace,

99
00:05:28,20 --> 00:05:30,40
space is required afterward,

100
00:05:30,40 --> 00:05:33,30
and a space is required before that one,

101
00:05:33,30 --> 00:05:37,90
take out that errant space in my brackets,

102
00:05:37,90 --> 00:05:43,00
and there, another set of curly braces

103
00:05:43,00 --> 00:05:45,20
where I need a space, and another set of brackets

104
00:05:45,20 --> 00:05:46,70
where I don't.

105
00:05:46,70 --> 00:05:49,00
And all that remains is that I've got

106
00:05:49,00 --> 00:05:52,20
a really long line here that I never actually broke up,

107
00:05:52,20 --> 00:05:58,30
so I'm going to start that bracket on its own line,

108
00:05:58,30 --> 00:06:04,30
and I'm going to break up these values

109
00:06:04,30 --> 00:06:12,00
on their own lines.

110
00:06:12,00 --> 00:06:16,30
And here I'm missing a trailing comma in my object literal,

111
00:06:16,30 --> 00:06:19,00
here also I'm missing a trailing comma,

112
00:06:19,00 --> 00:06:22,40
and there I go, now I've got ESLint rules

113
00:06:22,40 --> 00:06:25,00
applying to the .eslint file itself,

114
00:06:25,00 --> 00:06:26,60
and I've got everything ironed out

115
00:06:26,60 --> 00:06:30,20
so that my spacing within all of my enclosing characters

116
00:06:30,20 --> 00:06:32,80
is predictable and consistent.

117
00:06:32,80 --> 00:06:35,40
Another tool that I can use here,

118
00:06:35,40 --> 00:06:39,20
is to actually set consistent approach to these spaces

119
00:06:39,20 --> 00:06:42,60
in the Settings in my Editor.

120
00:06:42,60 --> 00:06:45,90
So I'll open up those Visual Studio Code Settings,

121
00:06:45,90 --> 00:06:50,10
I'm going to search for JavaScript insert space,

122
00:06:50,10 --> 00:06:52,80
and I get a list of several settings,

123
00:06:52,80 --> 00:06:54,50
these all control whether the Editor

124
00:06:54,50 --> 00:06:57,00
automatically adds a space after opening characters

125
00:06:57,00 --> 00:06:59,60
and before closing characters.

126
00:06:59,60 --> 00:07:06,50
So I want spaces within braces,

127
00:07:06,50 --> 00:07:10,60
and so we've got one for JSX expression braces,

128
00:07:10,60 --> 00:07:12,90
I'll go ahead and check that.

129
00:07:12,90 --> 00:07:16,20
Non-empty braces is already checked.

130
00:07:16,20 --> 00:07:21,20
Non-empty brackets, I want to be sure to be unchecked,

131
00:07:21,20 --> 00:07:25,60
and then, close my sidebar just so I can read these,

132
00:07:25,60 --> 00:07:27,80
I do not want spaces in my parenthesis,

133
00:07:27,80 --> 00:07:30,30
so I'm going to keep that unchecked,

134
00:07:30,30 --> 00:07:33,40
and template string braces, we'll go ahead

135
00:07:33,40 --> 00:07:35,90
and check that there as well.

136
00:07:35,90 --> 00:07:37,80
And now when I enter new code,

137
00:07:37,80 --> 00:07:39,90
the Editor will automatically format it

138
00:07:39,90 --> 00:07:42,00
according to my preferred spacing style

139
00:07:42,00 --> 00:07:45,10
within each type of enclosed character.

140
00:07:45,10 --> 00:07:46,90
Spaces within enclosing characters

141
00:07:46,90 --> 00:07:49,90
are a relatively small detail in our code,

142
00:07:49,90 --> 00:07:52,20
but approaching these spaces consistently

143
00:07:52,20 --> 00:07:54,80
makes your code that much easier to read and understand

144
00:07:54,80 --> 00:07:58,00
for you and for other developers who work with it.

