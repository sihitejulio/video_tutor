1
00:00:00,70 --> 00:00:02,80
- Like all white space blank lines

2
00:00:02,80 --> 00:00:05,80
don't effect the meaning of JavaScript code.

3
00:00:05,80 --> 00:00:08,40
As a result, you can make liberal use of them

4
00:00:08,40 --> 00:00:10,10
while writing code.

5
00:00:10,10 --> 00:00:12,50
For instance, you may add several blank lines

6
00:00:12,50 --> 00:00:16,50
below a comment, leaving space for code you need to add.

7
00:00:16,50 --> 00:00:19,10
You may also find that as you delete or remove

8
00:00:19,10 --> 00:00:22,00
sections of your code the blank lines padding a section

9
00:00:22,00 --> 00:00:25,90
are moved together into a larger blank area.

10
00:00:25,90 --> 00:00:28,20
During a quick visual scan of your code,

11
00:00:28,20 --> 00:00:31,30
large blank sections may suggest section breaks

12
00:00:31,30 --> 00:00:33,90
or other meanings that you didn't intend.

13
00:00:33,90 --> 00:00:36,70
To avoid this, it's best to limit your blank space

14
00:00:36,70 --> 00:00:39,20
to a single line and use comments

15
00:00:39,20 --> 00:00:41,20
to indicate sections of your code

16
00:00:41,20 --> 00:00:43,30
or content to add later.

17
00:00:43,30 --> 00:00:46,30
This same rule applies to the end of your document.

18
00:00:46,30 --> 00:00:50,00
Instead of ending with multiple blank lines or none

19
00:00:50,00 --> 00:00:53,10
best practice is to end your file with a single line break

20
00:00:53,10 --> 00:00:55,80
which provides a starting point to add new code

21
00:00:55,80 --> 00:01:01,00
without extending the length of the file unnecessarily.

22
00:01:01,00 --> 00:01:04,70
ESLint can flag deviations from blank space preferences

23
00:01:04,70 --> 00:01:09,40
you specify using the no multiple empty lines rule.

24
00:01:09,40 --> 00:01:13,70
In my eslintrc file I'm going to add that rule.

25
00:01:13,70 --> 00:01:19,40
So I'll say no multiple empty lines

26
00:01:19,40 --> 00:01:21,50
and the value's going to be an array.

27
00:01:21,50 --> 00:01:23,00
I want it to flag an error

28
00:01:23,00 --> 00:01:27,70
and I'm going to specify an object literal: the max keyword

29
00:01:27,70 --> 00:01:29,80
indicating the greatest number of blank lines

30
00:01:29,80 --> 00:01:33,10
that I'm allowing in a row and that's one

31
00:01:33,10 --> 00:01:37,90
and I can also specify the maxEOF keyword

32
00:01:37,90 --> 00:01:40,60
which specifies the greatest number of lines

33
00:01:40,60 --> 00:01:42,70
that I can include at the end of a document

34
00:01:42,70 --> 00:01:44,10
and that's also one.

35
00:01:44,10 --> 00:01:48,00
So there EOF is short for end of form.

36
00:01:48,00 --> 00:01:50,90
Saving that rule I can see a few issues flagged

37
00:01:50,90 --> 00:01:53,90
over in my app, that js file.

38
00:01:53,90 --> 00:01:57,10
So here I left some space after 'use strict'

39
00:01:57,10 --> 00:01:59,60
but I'll take two of those out.

40
00:01:59,60 --> 00:02:01,70
Likewise here I spaced a little extra

41
00:02:01,70 --> 00:02:05,20
between my two consts so I'll take one out

42
00:02:05,20 --> 00:02:08,40
and there's a little extra between the const

43
00:02:08,40 --> 00:02:13,20
and the distanceTo function

44
00:02:13,20 --> 00:02:17,90
and then down here an extra line below that function

45
00:02:17,90 --> 00:02:21,10
and above the console.log statement.

46
00:02:21,10 --> 00:02:24,40
Now I can also see it's not flagged by ESLint

47
00:02:24,40 --> 00:02:27,80
but I don't have a terminating line break here.

48
00:02:27,80 --> 00:02:30,50
And so just to implement that best practice myself,

49
00:02:30,50 --> 00:02:32,10
I'll go ahead and add that in.

50
00:02:32,10 --> 00:02:34,80
So now automatically have an empty line here

51
00:02:34,80 --> 00:02:40,20
ready to go if I open this file up and want to add some code

52
00:02:40,20 --> 00:02:43,30
With these preferences specified in ESLint

53
00:02:43,30 --> 00:02:45,70
I can ensure my code strikes a balance

54
00:02:45,70 --> 00:02:50,00
between compactness and a judicious use of blank lines.

