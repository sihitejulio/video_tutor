1
00:00:00,70 --> 00:00:02,80
- JavaScript has a number of syntaxes

2
00:00:02,80 --> 00:00:04,60
for creating a function signature,

3
00:00:04,60 --> 00:00:08,10
including function declarations, function expressions,

4
00:00:08,10 --> 00:00:12,50
anonymous functions, async functions and arrow functions.

5
00:00:12,50 --> 00:00:13,50
As a result,

6
00:00:13,50 --> 00:00:16,30
standardizing the way you create functions in your code

7
00:00:16,30 --> 00:00:18,00
can be a challenge.

8
00:00:18,00 --> 00:00:19,80
Different developers approach spacing

9
00:00:19,80 --> 00:00:22,40
in function signatures, differently.

10
00:00:22,40 --> 00:00:24,80
One common approach, is to include a space

11
00:00:24,80 --> 00:00:28,00
before the parens in an anonymous function signature,

12
00:00:28,00 --> 00:00:30,90
but not in a named function signature.

13
00:00:30,90 --> 00:00:34,30
This style makes it straightforward to add or remove a name,

14
00:00:34,30 --> 00:00:37,50
without needing to adjust spacing.

15
00:00:37,50 --> 00:00:39,80
Other developers prefer to standardize on spaces

16
00:00:39,80 --> 00:00:43,20
before parens, in all function signatures.

17
00:00:43,20 --> 00:00:45,10
The extra spaces added by the style

18
00:00:45,10 --> 00:00:49,10
can make the code easier to read.

19
00:00:49,10 --> 00:00:52,10
Finally, some developers never include spaces

20
00:00:52,10 --> 00:00:54,60
before parens in function signatures,

21
00:00:54,60 --> 00:00:56,70
preferring to treat the combination of keyword,

22
00:00:56,70 --> 00:01:02,00
or name or arrow symbol, and parens as a single unit.

23
00:01:02,00 --> 00:01:03,60
Whichever style you adopt,

24
00:01:03,60 --> 00:01:07,30
you can use ESLint to help you use it consistently.

25
00:01:07,30 --> 00:01:09,80
For my code, I've chosen to implement a space

26
00:01:09,80 --> 00:01:11,00
in anonymous functions,

27
00:01:11,00 --> 00:01:12,60
but not in named functions,

28
00:01:12,60 --> 00:01:14,70
which is a popular choice.

29
00:01:14,70 --> 00:01:18,40
The ESLint space-before-function-paren rule is customizable

30
00:01:18,40 --> 00:01:21,10
for the different types of function signatures.

31
00:01:21,10 --> 00:01:25,20
I can use the anonymous, named and arrow function options

32
00:01:25,20 --> 00:01:28,20
to customize the rule.

33
00:01:28,20 --> 00:01:32,60
Next, I'll add that rule to my eslintrc file.

34
00:01:32,60 --> 00:01:39,70
So the rule name is space-before-function-paren.

35
00:01:39,70 --> 00:01:41,60
The value is an array,

36
00:01:41,60 --> 00:01:43,00
and I'm going to start by saying

37
00:01:43,00 --> 00:01:44,90
that I want to flag an error,

38
00:01:44,90 --> 00:01:46,90
with a string.

39
00:01:46,90 --> 00:01:50,80
And then I'm going to specify an object literal

40
00:01:50,80 --> 00:01:52,40
that gives key value pairs,

41
00:01:52,40 --> 00:01:54,90
describing how I want each of the different types

42
00:01:54,90 --> 00:01:57,90
of function signatures, to be handled.

43
00:01:57,90 --> 00:02:01,20
So first, anonymous,

44
00:02:01,20 --> 00:02:04,20
and that's going to be always.

45
00:02:04,20 --> 00:02:08,50
Then named, that's going to be never.

46
00:02:08,50 --> 00:02:13,30
And finally, asyncArrow,

47
00:02:13,30 --> 00:02:16,80
and that is always.

48
00:02:16,80 --> 00:02:19,60
So I'll save that.

49
00:02:19,60 --> 00:02:20,90
Switching back to my code,

50
00:02:20,90 --> 00:02:24,20
I have a couple issues flagged here.

51
00:02:24,20 --> 00:02:28,30
So first off, I have an anonymous function signature here,

52
00:02:28,30 --> 00:02:30,60
and there should be a space before the parentheses,

53
00:02:30,60 --> 00:02:31,80
based on my rule,

54
00:02:31,80 --> 00:02:34,60
so I'll add that in.

55
00:02:34,60 --> 00:02:37,00
And, the same thing down here.

56
00:02:37,00 --> 00:02:40,80
So, I will add in that space.

57
00:02:40,80 --> 00:02:44,60
Now, notice that down here, in my event listener,

58
00:02:44,60 --> 00:02:47,00
the function called does not take a space

59
00:02:47,00 --> 00:02:49,40
between the name of the functions and the parens.

60
00:02:49,40 --> 00:02:54,30
And this is pretty standard for a function call.

61
00:02:54,30 --> 00:02:57,40
There is one editor format setting that affects this.

62
00:02:57,40 --> 00:02:59,50
So looking in the VS code settings,

63
00:02:59,50 --> 00:03:06,80
I'm going to look in the JavaScript settings,

64
00:03:06,80 --> 00:03:14,40
and there's an insert space before function parenthesis,

65
00:03:14,40 --> 00:03:16,10
right here.

66
00:03:16,10 --> 00:03:18,30
And I want to make sure that that's unchecked,

67
00:03:18,30 --> 00:03:23,70
because I don't want spaces in all instances.

68
00:03:23,70 --> 00:03:27,00
So now my ESLint settings and my editor settings

69
00:03:27,00 --> 00:03:29,00
support a specific set of choices

70
00:03:29,00 --> 00:03:31,30
around spaces in function signatures,

71
00:03:31,30 --> 00:03:32,90
that balance readability,

72
00:03:32,90 --> 00:03:34,20
with the ability to convert

73
00:03:34,20 --> 00:03:37,00
between function signature types.

