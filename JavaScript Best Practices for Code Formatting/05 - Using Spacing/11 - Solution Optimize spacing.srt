1
00:00:00,20 --> 00:00:05,70
(upbeat music)

2
00:00:05,70 --> 00:00:07,80
- [Instructor] In my Challenge file,

3
00:00:07,80 --> 00:00:09,80
I'm starting with my .eslint file,

4
00:00:09,80 --> 00:00:12,80
and now I notice already in my script.js file,

5
00:00:12,80 --> 00:00:15,40
I have an error, and that's due

6
00:00:15,40 --> 00:00:17,70
to a really long line down here,

7
00:00:17,70 --> 00:00:19,60
clearly this is a bunch of chained methods

8
00:00:19,60 --> 00:00:21,00
and we're going to get to that,

9
00:00:21,00 --> 00:00:24,10
so we'll just leave that be for the moment.

10
00:00:24,10 --> 00:00:27,50
So I'm going to go one by one with my rules,

11
00:00:27,50 --> 00:00:31,50
adding the rules that deal with spacing,

12
00:00:31,50 --> 00:00:35,20
and then using the prompts from ESLint

13
00:00:35,20 --> 00:00:39,30
to identify and fix issues in my files.

14
00:00:39,30 --> 00:00:42,60
So first off is the indent rule,

15
00:00:42,60 --> 00:00:47,50
and I want to flag an error when that's not two,

16
00:00:47,50 --> 00:00:51,10
so I'll save that, back in my script.js file,

17
00:00:51,10 --> 00:00:53,80
I can see up here, I have some right-aligned elements

18
00:00:53,80 --> 00:00:56,70
in an array, and I want those to be left-aligned,

19
00:00:56,70 --> 00:00:59,90
so I'm going to take out spaces

20
00:00:59,90 --> 00:01:06,00
until those all line up at two spaces,

21
00:01:06,00 --> 00:01:07,80
and now I'm back to one which is still

22
00:01:07,80 --> 00:01:13,80
that really long line, so I'm good with the indents.

23
00:01:13,80 --> 00:01:16,80
Next up, I'm going to specify space

24
00:01:16,80 --> 00:01:23,20
around my infix operators, so that's space, infix, ops,

25
00:01:23,20 --> 00:01:25,20
and I'm just going to flag an error there

26
00:01:25,20 --> 00:01:31,20
and rely on the default value for that rule.

27
00:01:31,20 --> 00:01:35,40
And so I can see down here, there's an equal sign

28
00:01:35,40 --> 00:01:39,80
that needs some spaces, a less than sign that needs them,

29
00:01:39,80 --> 00:01:44,30
a plus equals that needs it.

30
00:01:44,30 --> 00:01:48,60
We've got an assignment operator here that needs that,

31
00:01:48,60 --> 00:01:54,70
and so I'm back to just that one long line again.

32
00:01:54,70 --> 00:02:00,90
Next up is space before blocks,

33
00:02:00,90 --> 00:02:06,40
and we'll flag an error on that one.

34
00:02:06,40 --> 00:02:09,60
That one doesn't actually seem to have any errors over here,

35
00:02:09,60 --> 00:02:12,00
so we'll keep on going.

36
00:02:12,00 --> 00:02:15,80
We also have brace style,

37
00:02:15,80 --> 00:02:17,80
and that one we're taking the default on

38
00:02:17,80 --> 00:02:22,40
and just flagging an error.

39
00:02:22,40 --> 00:02:25,90
And there we've got a number of issues.

40
00:02:25,90 --> 00:02:28,40
So we can see here, that we've turned on

41
00:02:28,40 --> 00:02:31,80
the one true brace style and so we have this brace

42
00:02:31,80 --> 00:02:37,70
starting on its own line, which is not part of that style.

43
00:02:37,70 --> 00:02:39,90
And then we've got issues here again,

44
00:02:39,90 --> 00:02:42,60
now part of this is the chain statements,

45
00:02:42,60 --> 00:02:45,40
the chained methods, but some of this

46
00:02:45,40 --> 00:02:50,20
is also about line breaks, and so I'm going to go ahead

47
00:02:50,20 --> 00:02:52,80
and put this fetch on its own line,

48
00:02:52,80 --> 00:02:56,10
going to follow this prompt and put the if on its own line,

49
00:02:56,10 --> 00:02:59,90
here I need a return on its own line,

50
00:02:59,90 --> 00:03:04,80
here the closing curly brace should be on the line

51
00:03:04,80 --> 00:03:07,90
after the block,

52
00:03:07,90 --> 00:03:11,90
likewise here, the throw should be on its own line,

53
00:03:11,90 --> 00:03:14,40
and these curly braces need to be broken out

54
00:03:14,40 --> 00:03:18,00
to close things in the correct places,

55
00:03:18,00 --> 00:03:22,70
and there, a closing curly brace goes at the end.

56
00:03:22,70 --> 00:03:26,70
Now here we have a long line, here we still have a long line

57
00:03:26,70 --> 00:03:30,10
so those we'll get to in a bit, but for now,

58
00:03:30,10 --> 00:03:33,80
we've ironed out the issues with those curly braces.

59
00:03:33,80 --> 00:03:37,00
And then let's look at keyword spacing,

60
00:03:37,00 --> 00:03:42,50
so for keyword dash spacing, we'll trigger an error

61
00:03:42,50 --> 00:03:46,60
if we're not inline with the default settings,

62
00:03:46,60 --> 00:03:49,00
and looks like we have an error here,

63
00:03:49,00 --> 00:03:51,20
and that's the for statement,

64
00:03:51,20 --> 00:03:54,70
expecting a space after the word for,

65
00:03:54,70 --> 00:03:59,70
so a space there, and that's ironed out.

66
00:03:59,70 --> 00:04:03,80
And likewise with arrow spacing,

67
00:04:03,80 --> 00:04:08,30
we'll call an error there.

68
00:04:08,30 --> 00:04:10,30
And looks like we have a couple issues

69
00:04:10,30 --> 00:04:13,30
over in our script.js file.

70
00:04:13,30 --> 00:04:15,30
So we have an arrow right here,

71
00:04:15,30 --> 00:04:19,10
that needs a space before and after,

72
00:04:19,10 --> 00:04:22,10
that gets us ironed out.

73
00:04:22,10 --> 00:04:24,40
And then we want to look at the space

74
00:04:24,40 --> 00:04:29,90
before function paren rule, and the way I like

75
00:04:29,90 --> 00:04:35,10
to specify this is as an error and using,

76
00:04:35,10 --> 00:04:36,90
so this is going to be a long one,

77
00:04:36,90 --> 00:04:39,60
I'm going to put the values on separate lines

78
00:04:39,60 --> 00:04:42,80
so we're going to have an error and then I'm going to use

79
00:04:42,80 --> 00:04:46,40
an object literal,

80
00:04:46,40 --> 00:04:50,70
so for an anonymous function,

81
00:04:50,70 --> 00:04:54,50
I want to always have that space

82
00:04:54,50 --> 00:05:00,00
for a named function, I want to never have that space,

83
00:05:00,00 --> 00:05:02,80
and for an asyncArrow function,

84
00:05:02,80 --> 00:05:06,40
I want to always have that space.

85
00:05:06,40 --> 00:05:10,30
And then I'm going to move my closing square bracket,

86
00:05:10,30 --> 00:05:16,00
I need a closing comma here, closing comma here,

87
00:05:16,00 --> 00:05:22,40
and right here we're looking for a trailing comma as well.

88
00:05:22,40 --> 00:05:27,70
And it looks like that didn't actually catch any errors,

89
00:05:27,70 --> 00:05:30,30
so yeah, we don't actually have any code over here

90
00:05:30,30 --> 00:05:32,90
that violates that rule which is great,

91
00:05:32,90 --> 00:05:34,80
means we're doing something right.

92
00:05:34,80 --> 00:05:41,30
So we'll go ahead to new line per chained call,

93
00:05:41,30 --> 00:05:47,00
and there we want an error if we violate the default.

94
00:05:47,00 --> 00:05:52,00
And this is where we already have a set of chained calls,

95
00:05:52,00 --> 00:05:56,30
I'm going to go ahead and move this then onto its own line,

96
00:05:56,30 --> 00:06:00,00
I'm going to move this then onto its own line,

97
00:06:00,00 --> 00:06:04,80
and this catch, then I'm going to grab all of these

98
00:06:04,80 --> 00:06:13,20
and indent them under the fetch statement.

99
00:06:13,20 --> 00:06:15,90
And I prefer to have that paren indented

100
00:06:15,90 --> 00:06:21,10
underneath the curly brace,

101
00:06:21,10 --> 00:06:30,40
and I'm actually going to indent this whole structure,

102
00:06:30,40 --> 00:06:32,50
like that.

103
00:06:32,50 --> 00:06:36,40
So now I have my then statement

104
00:06:36,40 --> 00:06:41,80
with each set of enclosing characters starting a new line,

105
00:06:41,80 --> 00:06:44,90
I have my then and my catch on separate lines,

106
00:06:44,90 --> 00:06:48,50
I've broken up my long lines and I've got my chained methods

107
00:06:48,50 --> 00:06:53,80
broken up on new lines.

108
00:06:53,80 --> 00:06:58,20
Then we're going to look at space in parens,

109
00:06:58,20 --> 00:07:01,00
and again, this is one of those three,

110
00:07:01,00 --> 00:07:03,00
we've got parens, brackets, and braces

111
00:07:03,00 --> 00:07:04,80
where we can talk about the spacing.

112
00:07:04,80 --> 00:07:09,00
So in the parens, I'm going to use an array,

113
00:07:09,00 --> 00:07:13,40
specify error and never, and I'll go ahead

114
00:07:13,40 --> 00:07:20,40
and do array bracket spacing here as well,

115
00:07:20,40 --> 00:07:27,00
and that's going to be error and never,

116
00:07:27,00 --> 00:07:32,90
and then we'll do object curly spacing,

117
00:07:32,90 --> 00:07:36,10
and that's going to be error and always,

118
00:07:36,10 --> 00:07:38,50
because I always want those spaces

119
00:07:38,50 --> 00:07:41,60
padding out my curly braces.

120
00:07:41,60 --> 00:07:45,60
So then, going back to my .js file,

121
00:07:45,60 --> 00:07:50,40
I've got extra spaces in my parens here that I don't want,

122
00:07:50,40 --> 00:07:54,60
in these parens as well,

123
00:07:54,60 --> 00:08:00,40
and in these parens,

124
00:08:00,40 --> 00:08:04,20
and these parens,

125
00:08:04,20 --> 00:08:13,60
and then down here,

126
00:08:13,60 --> 00:08:17,50
there's extra spaces down here,

127
00:08:17,50 --> 00:08:19,20
this is like when you code in one style

128
00:08:19,20 --> 00:08:21,20
and then decide to change everything,

129
00:08:21,20 --> 00:08:26,30
you have to just go line by line and fix everything.

130
00:08:26,30 --> 00:08:28,10
And I missed a few up here as well,

131
00:08:28,10 --> 00:08:34,20
we got spaces there, and spaces here.

132
00:08:34,20 --> 00:08:37,20
And now we've got that all ironed out.

133
00:08:37,20 --> 00:08:44,00
So just a couple more, I want to check on comma spacing,

134
00:08:44,00 --> 00:08:48,40
that's going to flag an error,

135
00:08:48,40 --> 00:08:52,80
and my key value pairs will be before false

136
00:08:52,80 --> 00:08:58,60
and after true.

137
00:08:58,60 --> 00:09:04,80
I need to pad out my curly braces there.

138
00:09:04,80 --> 00:09:08,00
Saving that, we've got a few issues in the script file,

139
00:09:08,00 --> 00:09:09,90
so we have all of these commas here

140
00:09:09,90 --> 00:09:12,30
that have an extra space before them,

141
00:09:12,30 --> 00:09:16,60
so we'll go ahead and take those out,

142
00:09:16,60 --> 00:09:18,50
and now we've got that comma spacing

143
00:09:18,50 --> 00:09:21,20
implemented consistently.

144
00:09:21,20 --> 00:09:24,50
Then we got one more, that's about the empty lines,

145
00:09:24,50 --> 00:09:30,90
so I'm going to do, no multiple empty lines,

146
00:09:30,90 --> 00:09:37,30
and flag an error and specify an object literal with max

147
00:09:37,30 --> 00:09:43,40
of one in a row, and maxEOF with a value of one,

148
00:09:43,40 --> 00:09:46,60
so one maximum at the end of the file.

149
00:09:46,60 --> 00:09:49,60
Going to pad out my curly braces again,

150
00:09:49,60 --> 00:09:52,80
add a terminal comma and save that.

151
00:09:52,80 --> 00:09:55,90
And in my script.js file, got one spot up here

152
00:09:55,90 --> 00:09:58,80
where I put an extra space below use strict,

153
00:09:58,80 --> 00:10:02,50
put a little extra space here before my itemLookup,

154
00:10:02,50 --> 00:10:04,90
and then at the bottom, this isn't flagged

155
00:10:04,90 --> 00:10:09,20
but I'm going to go ahead and add in that final blank line,

156
00:10:09,20 --> 00:10:12,60
so that I'm ready to go to add more code if I need to.

157
00:10:12,60 --> 00:10:16,60
And so now I have all of these rules implemented

158
00:10:16,60 --> 00:10:22,20
for spacing, and I have my JavaScript code

159
00:10:22,20 --> 00:10:26,20
in my script.js file all ironed out and consistent

160
00:10:26,20 --> 00:10:29,00
in the use of all those rules.

