1
00:00:00,60 --> 00:00:04,20
- Including spaces around keywords like function and if

2
00:00:04,20 --> 00:00:07,10
can make your JavaScript code more readable.

3
00:00:07,10 --> 00:00:09,30
The same is true for spaces around arrows

4
00:00:09,30 --> 00:00:14,90
in arrow functions.

5
00:00:14,90 --> 00:00:17,80
ESLint includes a keyword spacing rule

6
00:00:17,80 --> 00:00:22,20
that lets your specify the format to check for keywords.

7
00:00:22,20 --> 00:00:25,30
ESLint also includes an arrow spacing rule

8
00:00:25,30 --> 00:00:29,70
that's specific to the arrows in arrow functions.

9
00:00:29,70 --> 00:00:32,60
My app.js file contains an arrow function,

10
00:00:32,60 --> 00:00:35,00
which includes if else logic.

11
00:00:35,00 --> 00:00:37,40
And right now, that's written without spaces

12
00:00:37,40 --> 00:00:42,30
before or after the if, else if, or else keywords,

13
00:00:42,30 --> 00:00:46,60
as well as the arrow symbol.

14
00:00:46,60 --> 00:00:50,00
I'll start by adding a couple rules to my ESLintrc file

15
00:00:50,00 --> 00:00:52,70
to flag the lack of spaces.

16
00:00:52,70 --> 00:00:57,50
First off, I'm going to do keyword-spacing

17
00:00:57,50 --> 00:00:59,60
for the value of error.

18
00:00:59,60 --> 00:01:03,20
And then arrow spacing,

19
00:01:03,20 --> 00:01:04,40
also with the value of error.

20
00:01:04,40 --> 00:01:07,50
So accepting the defaults for both of those keywords,

21
00:01:07,50 --> 00:01:10,10
and just instructing ESLint to flag

22
00:01:10,10 --> 00:01:13,60
deviations from those defaults as errors.

23
00:01:13,60 --> 00:01:15,30
And I save that,

24
00:01:15,30 --> 00:01:18,80
and I've got some errors flagged in my app.js file.

25
00:01:18,80 --> 00:01:22,20
And I can see it's the arrow and all the keywords

26
00:01:22,20 --> 00:01:25,00
where I'm seeing those underscores.

27
00:01:25,00 --> 00:01:29,80
So adding those spaces back, first off,

28
00:01:29,80 --> 00:01:32,20
this one is missing a space before the arrow,

29
00:01:32,20 --> 00:01:35,00
that one's going to be missing a space after.

30
00:01:35,00 --> 00:01:38,00
So I will add those in.

31
00:01:38,00 --> 00:01:39,00
Oops. There we go.

32
00:01:39,00 --> 00:01:43,00
Then we got the missing space after if,

33
00:01:43,00 --> 00:01:45,60
space before else,

34
00:01:45,60 --> 00:01:46,40
so all the way through here,

35
00:01:46,40 --> 00:01:52,00
just spacing around those keywords or key phrases.

36
00:01:52,00 --> 00:01:55,00
And that takes care of all my errors.

37
00:01:55,00 --> 00:01:58,30
And so resolving those errors also makes my code

38
00:01:58,30 --> 00:01:59,40
more readable.

39
00:01:59,40 --> 00:02:01,70
Now, I can also check a couple configurations

40
00:02:01,70 --> 00:02:03,40
in my editor to make this style

41
00:02:03,40 --> 00:02:05,00
easier to write next time.

42
00:02:05,00 --> 00:02:08,00
So in my Visual Studio Code settings,

43
00:02:08,00 --> 00:02:14,80
going to search on JavaScript.

44
00:02:14,80 --> 00:02:17,70
And there's a lot of options in the

45
00:02:17,70 --> 00:02:26,10
JavaScript format settings for inserting spaces.

46
00:02:26,10 --> 00:02:28,50
And so I have an option to insert a space

47
00:02:28,50 --> 00:02:31,40
after the function keyword in anonymous functions,

48
00:02:31,40 --> 00:02:34,30
and that's selected.

49
00:02:34,30 --> 00:02:37,30
And then there's another option for inserting a space after

50
00:02:37,30 --> 00:02:39,40
keywords in control flow statements.

51
00:02:39,40 --> 00:02:41,20
That's selected, as well.

52
00:02:41,20 --> 00:02:43,10
Now, this doesn't cover all the cases that the

53
00:02:43,10 --> 00:02:44,80
ESLint rules apply to,

54
00:02:44,80 --> 00:02:46,50
but these are both a good start

55
00:02:46,50 --> 00:02:48,50
in getting my code formatted the right way

56
00:02:48,50 --> 00:02:51,80
from the get go.

57
00:02:51,80 --> 00:02:53,40
All of these configurations will help

58
00:02:53,40 --> 00:02:56,20
make my functions and my control-flow logic

59
00:02:56,20 --> 00:02:59,20
easier to read by ensuring I consistently use spaces

60
00:02:59,20 --> 00:03:02,00
around keywords and around arrow symbols.

