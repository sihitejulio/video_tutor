1
00:00:00,60 --> 00:00:02,90
- [Instructor] Commas separate values in JavaScript

2
00:00:02,90 --> 00:00:05,10
in a number of situations.

3
00:00:05,10 --> 00:00:07,00
It's common to write commas in code

4
00:00:07,00 --> 00:00:09,30
the same way you would use them in a sentence,

5
00:00:09,30 --> 00:00:13,00
with no space before and a space after.

6
00:00:13,00 --> 00:00:16,30
However, other comma styles are used as well,

7
00:00:16,30 --> 00:00:19,70
including a space before but no space after

8
00:00:19,70 --> 00:00:23,30
or no spaces on either side.

9
00:00:23,30 --> 00:00:25,70
I use a space only after commas,

10
00:00:25,70 --> 00:00:28,60
and so far, I've been writing my ESLintRC file

11
00:00:28,60 --> 00:00:30,80
using that format.

12
00:00:30,80 --> 00:00:35,80
I want to add a rule to flag errors if my spacing deviates.

13
00:00:35,80 --> 00:00:38,00
ESLint supports the comma spacing rule,

14
00:00:38,00 --> 00:00:40,90
which lets me specify which style I want to use

15
00:00:40,90 --> 00:00:43,80
and how I want it flagged.

16
00:00:43,80 --> 00:00:47,40
So over in my ESLintRC file I'm going to add a new line,

17
00:00:47,40 --> 00:00:50,70
and that's going to be comma-spacing

18
00:00:50,70 --> 00:00:53,90
and in an array, I going to specify

19
00:00:53,90 --> 00:00:56,10
that I want it to be flagged as an error,

20
00:00:56,10 --> 00:00:58,10
and then I need an object literal.

21
00:00:58,10 --> 00:01:03,10
I'm going to specify the value before with a value of false,

22
00:01:03,10 --> 00:01:08,20
and a value of after, and give that a value of true.

23
00:01:08,20 --> 00:01:13,40
Got to make sure to put spaces to pad out my braces,

24
00:01:13,40 --> 00:01:16,80
terminal comma, and when I save that,

25
00:01:16,80 --> 00:01:19,50
I can see over here in my JavaScript file,

26
00:01:19,50 --> 00:01:21,90
I have an error throw, so here I've got

27
00:01:21,90 --> 00:01:25,10
a space before and a space after my comma,

28
00:01:25,10 --> 00:01:29,70
and the pop-up error here

29
00:01:29,70 --> 00:01:31,80
indicates a comma spacing issue,

30
00:01:31,80 --> 00:01:36,30
so I can simply take out that space, and I'm all good.

31
00:01:36,30 --> 00:01:38,20
I can also check an editor setting

32
00:01:38,20 --> 00:01:40,90
that can help me use commas consistently.

33
00:01:40,90 --> 00:01:45,50
So in my VS Code Settings,

34
00:01:45,50 --> 00:01:50,70
I'm going to do a search on JavaScript and search space,

35
00:01:50,70 --> 00:01:52,00
and right here we've got the

36
00:01:52,00 --> 00:01:56,30
Insert Space After Comma Delimiter Rule.

37
00:01:56,30 --> 00:02:00,10
That is to say a comma that separates values.

38
00:02:00,10 --> 00:02:02,20
I want this formatting rule to be applied

39
00:02:02,20 --> 00:02:04,80
when the auto-formatting feature is used,

40
00:02:04,80 --> 00:02:07,40
and it's already checked, so I'm good.

41
00:02:07,40 --> 00:02:09,20
And now I have the tools in place

42
00:02:09,20 --> 00:02:11,90
to ensure that my comma spacing is uniform,

43
00:02:11,90 --> 00:02:14,00
which makes my code easier to read.

