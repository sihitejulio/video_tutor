1
00:00:00,70 --> 00:00:01,70
- [Instructor] You've likely encountered

2
00:00:01,70 --> 00:00:03,60
a number of different layouts for blocks

3
00:00:03,60 --> 00:00:07,30
such as conditionals and functions in JavaScript code.

4
00:00:07,30 --> 00:00:08,70
You've probably seen code written in the

5
00:00:08,70 --> 00:00:12,10
One True Brace style, with the opening brace

6
00:00:12,10 --> 00:00:15,20
on the same line as the keyword.

7
00:00:15,20 --> 00:00:18,00
You may have also seen or used the Stroustrup variant,

8
00:00:18,00 --> 00:00:21,10
which pushes else statements onto separate lines,

9
00:00:21,10 --> 00:00:25,60
but keeps all other aspects of the One True Brace style.

10
00:00:25,60 --> 00:00:28,30
And you may have also run into the Allman style,

11
00:00:28,30 --> 00:00:32,40
in which starting braces get their own dedicated lines.

12
00:00:32,40 --> 00:00:34,40
As with all types of code formatting,

13
00:00:34,40 --> 00:00:37,70
each of these styles has its advantages and drawbacks.

14
00:00:37,70 --> 00:00:39,40
And it's important to simply pick the one

15
00:00:39,40 --> 00:00:42,40
that works for you and use it consistently.

16
00:00:42,40 --> 00:00:44,00
You can enforce this style in your code

17
00:00:44,00 --> 00:00:48,20
using the ESLint Brace-style rule.

18
00:00:48,20 --> 00:00:50,20
One other variation in writing block code

19
00:00:50,20 --> 00:00:53,60
is the choice of spacing before the opening brace.

20
00:00:53,60 --> 00:00:56,90
Including a space before a brace is a subtle,

21
00:00:56,90 --> 00:00:58,70
but useful best practice

22
00:00:58,70 --> 00:01:01,60
that makes your code easier to read.

23
00:01:01,60 --> 00:01:04,40
You can enforce space before blocks

24
00:01:04,40 --> 00:01:08,70
with ESLint's space before blocks rule.

25
00:01:08,70 --> 00:01:11,00
In my app.js file,

26
00:01:11,00 --> 00:01:15,20
the region function contains an if else structure.

27
00:01:15,20 --> 00:01:17,20
Both the function and the conditional structure

28
00:01:17,20 --> 00:01:20,50
currently use Allman block format.

29
00:01:20,50 --> 00:01:25,70
I'm going to start by adding rules to my eslintrc file.

30
00:01:25,70 --> 00:01:31,20
So I'm going to use brace-style and error,

31
00:01:31,20 --> 00:01:32,50
and this chooses the default,

32
00:01:32,50 --> 00:01:34,60
which is the One True Brace style.

33
00:01:34,60 --> 00:01:38,30
So I'm changing the style that I want to apply to my code.

34
00:01:38,30 --> 00:01:42,60
And then space before blocks,

35
00:01:42,60 --> 00:01:45,90
the default is true and so I'm just going to say error,

36
00:01:45,90 --> 00:01:49,70
to say I want deviations from that standard

37
00:01:49,70 --> 00:01:51,10
to be flagged.

38
00:01:51,10 --> 00:01:54,10
So I save that, now I see a bunch of errors

39
00:01:54,10 --> 00:01:56,40
in my app.js file.

40
00:01:56,40 --> 00:01:59,10
So switching back there, each of my opening braces

41
00:01:59,10 --> 00:02:01,70
has a squiggly underscore.

42
00:02:01,70 --> 00:02:07,70
And this one's flagging the brace style.

43
00:02:07,70 --> 00:02:09,60
So I'm going to put that function brace

44
00:02:09,60 --> 00:02:13,90
back on the same line as the arrow.

45
00:02:13,90 --> 00:02:17,80
And I'll do the same with all of the conditional braces,

46
00:02:17,80 --> 00:02:23,20
moving those up on to the appropriate line.

47
00:02:23,20 --> 00:02:24,80
That's the opening braces.

48
00:02:24,80 --> 00:02:27,80
I'm also going to move these else if statements

49
00:02:27,80 --> 00:02:30,30
and the final else statement up on the line

50
00:02:30,30 --> 00:02:32,80
with the closing brace that precedes them,

51
00:02:32,80 --> 00:02:37,00
and that's another piece of the One True Brace style.

52
00:02:37,00 --> 00:02:38,50
As a side note,

53
00:02:38,50 --> 00:02:41,10
brace style does not affect object literals,

54
00:02:41,10 --> 00:02:43,20
because they're either part of assignments

55
00:02:43,20 --> 00:02:45,50
or they stand on their own as arguments.

56
00:02:45,50 --> 00:02:50,20
So no changes are necessary to the user prefs variable here.

57
00:02:50,20 --> 00:02:53,30
Now, One True Brace style is generally the default style

58
00:02:53,30 --> 00:02:55,40
used by editors that auto format.

59
00:02:55,40 --> 00:02:58,30
But it's worth checking a couple settings.

60
00:02:58,30 --> 00:03:00,30
In Visual Studio code,

61
00:03:00,30 --> 00:03:03,40
in the settings I'm going to search on JavaScript,

62
00:03:03,40 --> 00:03:05,30
and there's a whole bunch of

63
00:03:05,30 --> 00:03:07,70
settings that apply specifically to JavaScript code

64
00:03:07,70 --> 00:03:13,70
that are built into the editor.

65
00:03:13,70 --> 00:03:15,10
And so we have a setting,

66
00:03:15,10 --> 00:03:18,70
place open brace on new line for control blocks.

67
00:03:18,70 --> 00:03:20,80
And that's off by default.

68
00:03:20,80 --> 00:03:21,60
And then there's also

69
00:03:21,60 --> 00:03:23,40
place open brace on new line for functions,

70
00:03:23,40 --> 00:03:25,20
that's off as well.

71
00:03:25,20 --> 00:03:27,10
So if you're typing code in your editor

72
00:03:27,10 --> 00:03:28,70
and you're finding that that new code

73
00:03:28,70 --> 00:03:31,00
isn't implementing the One True Brace style,

74
00:03:31,00 --> 00:03:33,80
make sure that these options are unchecked.

75
00:03:33,80 --> 00:03:36,80
Or if you want to use another style like Stroustrup or Allman,

76
00:03:36,80 --> 00:03:39,00
this is where you can go in your editor

77
00:03:39,00 --> 00:03:45,80
to make the auto formatter implement your style for you.

78
00:03:45,80 --> 00:03:47,40
Now that I have block related styles

79
00:03:47,40 --> 00:03:49,60
set in my eslintrc file,

80
00:03:49,60 --> 00:03:52,00
I'm enforcing insistency in block formatting

81
00:03:52,00 --> 00:03:54,30
throughout my code, making it easier for me

82
00:03:54,30 --> 00:03:58,00
and other developers to read and understand.

