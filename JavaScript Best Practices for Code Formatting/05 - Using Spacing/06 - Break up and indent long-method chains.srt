1
00:00:00,70 --> 00:00:02,60
- [Instructor] Chaining methods together enables you

2
00:00:02,60 --> 00:00:05,50
to write compact code that accomplishes a task

3
00:00:05,50 --> 00:00:07,40
without unnecessary reassignment,

4
00:00:07,40 --> 00:00:10,20
however, the trade off for that compactness

5
00:00:10,20 --> 00:00:14,80
can be dense code that's hard to read.

6
00:00:14,80 --> 00:00:17,00
To make long method chains more legible,

7
00:00:17,00 --> 00:00:20,10
it's useful to break them up onto multiple lines

8
00:00:20,10 --> 00:00:23,90
with a dot preceding a method name starting a new line,

9
00:00:23,90 --> 00:00:26,60
combined with indenting the methods below

10
00:00:26,60 --> 00:00:27,90
the first line of the statement.

11
00:00:27,90 --> 00:00:31,00
This format makes it easy to understand the sequence

12
00:00:31,00 --> 00:00:33,10
of modifications being applied,

13
00:00:33,10 --> 00:00:37,80
as well as where the chain begins and ends.

14
00:00:37,80 --> 00:00:41,70
ESLint includes the newline-per-chained-call rule

15
00:00:41,70 --> 00:00:43,60
which flags errors in your code

16
00:00:43,60 --> 00:00:46,00
if your methods aren't on their own lines.

17
00:00:46,00 --> 00:00:49,10
In my app that identifies points of interest

18
00:00:49,10 --> 00:00:52,70
based on a user's location, I'm building a fetch request

19
00:00:52,70 --> 00:00:55,70
to the US Park's Service API to get information

20
00:00:55,70 --> 00:00:57,60
on national parks.

21
00:00:57,60 --> 00:00:59,60
Notice that I already have one error here,

22
00:00:59,60 --> 00:01:01,80
and that's just because I have a really long line

23
00:01:01,80 --> 00:01:06,70
that includes a couple of chain methods.

24
00:01:06,70 --> 00:01:11,90
In my ESLint RC file, I'm going to add a new rule,

25
00:01:11,90 --> 00:01:17,50
and that is newline-per-chained-call,

26
00:01:17,50 --> 00:01:20,70
and I'm simply going to give it a value of error

27
00:01:20,70 --> 00:01:23,30
which means I'm accepting the default,

28
00:01:23,30 --> 00:01:26,80
and asking for that to be flagged as an error.

29
00:01:26,80 --> 00:01:29,20
So I can add options here if I wanted to,

30
00:01:29,20 --> 00:01:31,90
so I could say one or two chain methods are okay

31
00:01:31,90 --> 00:01:35,00
without line breaks, but for now I'm going to enforce

32
00:01:35,00 --> 00:01:38,00
all chained methods on new lines,

33
00:01:38,00 --> 00:01:41,90
and when I save that and go back to my app, that js file

34
00:01:41,90 --> 00:01:48,50
I have an error here.

35
00:01:48,50 --> 00:01:54,80
So both of these catch statements are expecting line breaks,

36
00:01:54,80 --> 00:01:59,00
and so I'm just going to go ahead and break these up

37
00:01:59,00 --> 00:02:01,80
starting with the dot on both of these,

38
00:02:01,80 --> 00:02:04,10
and for good measure I'm going to break up

39
00:02:04,10 --> 00:02:08,70
the first one as well.

40
00:02:08,70 --> 00:02:11,50
Now I want everything indented

41
00:02:11,50 --> 00:02:13,70
underneath that fetch statement,

42
00:02:13,70 --> 00:02:18,40
and notice here that indeed ESLint is expecting

43
00:02:18,40 --> 00:02:22,60
that two space indent that my file calls for.

44
00:02:22,60 --> 00:02:25,60
So I'm just going to select all of the chained methods.

45
00:02:25,60 --> 00:02:29,90
I'm going to use a tab to indent those two spaces,

46
00:02:29,90 --> 00:02:32,80
that whole if else construction is already

47
00:02:32,80 --> 00:02:35,10
indented under the then,

48
00:02:35,10 --> 00:02:37,40
so everything is indented correctly,

49
00:02:37,40 --> 00:02:42,40
and I have each of my chained methods starting a new line,

50
00:02:42,40 --> 00:02:46,00
and this makes for consistent and easy to read code.

