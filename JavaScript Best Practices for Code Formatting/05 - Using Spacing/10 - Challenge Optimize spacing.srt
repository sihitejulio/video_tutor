1
00:00:00,00 --> 00:00:05,70
(upbeat electronic music)

2
00:00:05,70 --> 00:00:07,70
- [Narrator] Ready for a challenge?

3
00:00:07,70 --> 00:00:10,90
The start files contain code for a grocery shopping app.

4
00:00:10,90 --> 00:00:14,20
Edit the script.js file to implement

5
00:00:14,20 --> 00:00:16,50
and enforce best practices for spacing

6
00:00:16,50 --> 00:00:19,40
in JavaScript, including indents, spacing

7
00:00:19,40 --> 00:00:21,50
around characters and blank lines.

8
00:00:21,50 --> 00:00:24,20
This should take you about five minutes.

9
00:00:24,20 --> 00:00:26,80
When you're done, join me in the next video

10
00:00:26,80 --> 00:00:30,00
and I'll walk you through how I approached it.

