1
00:00:00,70 --> 00:00:02,20
- [Instructor] Although spaces don't change

2
00:00:02,20 --> 00:00:04,80
the meaning of your code in Javascript,

3
00:00:04,80 --> 00:00:07,20
appropriate use of spaces is important

4
00:00:07,20 --> 00:00:10,40
for creating code that's readable by humans.

5
00:00:10,40 --> 00:00:13,20
And when it comes to making your code more readable,

6
00:00:13,20 --> 00:00:17,50
nothing makes a bigger impact than consistent indents.

7
00:00:17,50 --> 00:00:19,50
Whenever the contents of braces, brackets,

8
00:00:19,50 --> 00:00:22,00
or parens don't fit in a single line,

9
00:00:22,00 --> 00:00:23,70
it's common to start a new line

10
00:00:23,70 --> 00:00:25,50
after the opening punctuation,

11
00:00:25,50 --> 00:00:28,70
and indent all nested statements.

12
00:00:28,70 --> 00:00:30,20
As you build out code,

13
00:00:30,20 --> 00:00:32,90
you'll inevitably end up multiple levels deep,

14
00:00:32,90 --> 00:00:34,70
with indented statements nested

15
00:00:34,70 --> 00:00:37,10
within other indented statements.

16
00:00:37,10 --> 00:00:40,50
For this reason, consistently applied indents are key

17
00:00:40,50 --> 00:00:42,30
in being able to read the code

18
00:00:42,30 --> 00:00:47,50
and understand the relationship between statements.

19
00:00:47,50 --> 00:00:50,10
Some developers use spaces for indents

20
00:00:50,10 --> 00:00:52,10
while others prefer tabs

21
00:00:52,10 --> 00:00:55,40
but common style guides tend to favor spaces.

22
00:00:55,40 --> 00:00:57,00
Different developers may also choose

23
00:00:57,00 --> 00:01:00,10
a different number of spaces for each level of indent

24
00:01:00,10 --> 00:01:03,90
but two spaces has become a widely adopted standard.

25
00:01:03,90 --> 00:01:06,20
I can use the ESLint indent rule

26
00:01:06,20 --> 00:01:10,50
to set a preferred style for indents.

27
00:01:10,50 --> 00:01:13,10
In my app that identifies points of interest

28
00:01:13,10 --> 00:01:15,00
based on a user's location,

29
00:01:15,00 --> 00:01:17,00
I'm starting with a set of user prefs

30
00:01:17,00 --> 00:01:19,40
using an object literal as sample data

31
00:01:19,40 --> 00:01:23,70
while I build out the app.

32
00:01:23,70 --> 00:01:27,30
In my eslintrc file, I'll add an indent rule

33
00:01:27,30 --> 00:01:28,70
and in the array value,

34
00:01:28,70 --> 00:01:31,60
I'll specify that I want to flag an error.

35
00:01:31,60 --> 00:01:35,90
I can specify tab for the second value if I want to use tabs

36
00:01:35,90 --> 00:01:37,70
or I can just use a number to indicate

37
00:01:37,70 --> 00:01:41,20
the number of spaces I want so I'll specify two.

38
00:01:41,20 --> 00:01:44,20
I'll save that and then go back to my app.js file

39
00:01:44,20 --> 00:01:46,20
and I have a couple red underlines up here

40
00:01:46,20 --> 00:01:48,80
in my user prefs constant.

41
00:01:48,80 --> 00:01:51,40
My user pref properties are right aligned

42
00:01:51,40 --> 00:01:52,70
instead of left aligned

43
00:01:52,70 --> 00:01:55,20
so they use varying indents.

44
00:01:55,20 --> 00:01:58,30
So I'll add a space here before city

45
00:01:58,30 --> 00:02:02,00
and another space here before lon

46
00:02:02,00 --> 00:02:05,40
and now everything looks good.

47
00:02:05,40 --> 00:02:06,80
When I'm typing new code,

48
00:02:06,80 --> 00:02:10,10
it's useful to have those indents implemented automatically

49
00:02:10,10 --> 00:02:12,70
and editors can help us out there.

50
00:02:12,70 --> 00:02:15,20
I can use a few config settings to get my editor

51
00:02:15,20 --> 00:02:17,20
to do some of the work.

52
00:02:17,20 --> 00:02:20,90
In VS Code, I'm going to hit code and then preferences

53
00:02:20,90 --> 00:02:23,80
and select settings on my Mac

54
00:02:23,80 --> 00:02:27,90
and in the search box, I'm going to type tabsize

55
00:02:27,90 --> 00:02:30,20
which is one of the settings

56
00:02:30,20 --> 00:02:33,10
and I get a few rules that match.

57
00:02:33,10 --> 00:02:36,90
Now, these are settings that apply only to my editor.

58
00:02:36,90 --> 00:02:39,30
If I open my project in another editor,

59
00:02:39,30 --> 00:02:44,10
my ESLint settings go with in my .eslintrc file

60
00:02:44,10 --> 00:02:48,40
but not any changes I make here in my editor settings

61
00:02:48,40 --> 00:02:51,10
but since this is the editor I use for all my coding,

62
00:02:51,10 --> 00:02:53,80
I want to customize it to help me out.

63
00:02:53,80 --> 00:02:58,50
So first off, I can change tab size

64
00:02:58,50 --> 00:03:02,30
from the default of four to two.

65
00:03:02,30 --> 00:03:05,40
This means that any time I press tab,

66
00:03:05,40 --> 00:03:08,30
the editor is going to insert two spaces.

67
00:03:08,30 --> 00:03:11,50
I also have the Detect Indentations setting.

68
00:03:11,50 --> 00:03:13,90
This is checked on by default

69
00:03:13,90 --> 00:03:15,90
and that means that the editor adjusts

70
00:03:15,90 --> 00:03:18,70
for whatever tab length is already used in a file,

71
00:03:18,70 --> 00:03:20,90
overwriting my tab size setting.

72
00:03:20,90 --> 00:03:25,10
I don't want that so I'll uncheck that box

73
00:03:25,10 --> 00:03:28,20
and that means that the editor will enforce two spaces

74
00:03:28,20 --> 00:03:31,40
on every file I open.

75
00:03:31,40 --> 00:03:33,40
Now, by default Visual Studio Code

76
00:03:33,40 --> 00:03:35,20
replaces tabs with spaces

77
00:03:35,20 --> 00:03:39,30
so that works well for my chosen style.

78
00:03:39,30 --> 00:03:42,50
Editors can also offer a little automatic help

79
00:03:42,50 --> 00:03:44,80
when they detect what you're doing.

80
00:03:44,80 --> 00:03:46,70
In Visual Studio Code,

81
00:03:46,70 --> 00:03:51,70
I have the Auto Indent setting

82
00:03:51,70 --> 00:03:54,20
and that's true by default.

83
00:03:54,20 --> 00:03:56,30
This means that when I start a new line,

84
00:03:56,30 --> 00:03:58,70
it's automatically indented the appropriate amount

85
00:03:58,70 --> 00:04:01,60
which is really helpful.

86
00:04:01,60 --> 00:04:05,20
Then I'm going to search for format on

87
00:04:05,20 --> 00:04:09,90
and I have a couple settings here.

88
00:04:09,90 --> 00:04:13,40
The Format On Type setting automatically formats code

89
00:04:13,40 --> 00:04:15,90
after I complete a statement.

90
00:04:15,90 --> 00:04:19,00
And there's also Format On Save,

91
00:04:19,00 --> 00:04:20,20
which can apply formatting

92
00:04:20,20 --> 00:04:23,10
to an entire document when you save it.

93
00:04:23,10 --> 00:04:25,80
These are both really useful tools.

94
00:04:25,80 --> 00:04:28,20
However, for the purposes of this course,

95
00:04:28,20 --> 00:04:30,40
I'm going to leave them off so I can illustrate

96
00:04:30,40 --> 00:04:33,20
both the wrong and right way to format code

97
00:04:33,20 --> 00:04:36,50
without premature corrections from my browser.

98
00:04:36,50 --> 00:04:39,10
I encourage you to use one or both of these settings

99
00:04:39,10 --> 00:04:41,50
after you're done with this course.

100
00:04:41,50 --> 00:04:43,80
You can also choose to turn them on now

101
00:04:43,80 --> 00:04:45,50
but be aware that if you do so,

102
00:04:45,50 --> 00:04:48,50
your code may not look like what you see on the screen

103
00:04:48,50 --> 00:04:52,00
as you enter or save it.

104
00:04:52,00 --> 00:04:54,10
Going back to my app.js file,

105
00:04:54,10 --> 00:04:57,70
I want to add some conditional logic to my region function

106
00:04:57,70 --> 00:05:01,20
which the editor should now help me indent automatically.

107
00:05:01,20 --> 00:05:05,70
So, if city is equal to,

108
00:05:05,70 --> 00:05:11,90
we'll say Buenos Aires,

109
00:05:11,90 --> 00:05:17,60
it can return South America.

110
00:05:17,60 --> 00:05:24,60
Else if city is equal to Los Angeles,

111
00:05:24,60 --> 00:05:27,30
and I'm getting those nice auto indents,

112
00:05:27,30 --> 00:05:29,80
both for the closing brace

113
00:05:29,80 --> 00:05:32,30
and for the new line within the braces,

114
00:05:32,30 --> 00:05:37,30
so I'm going to return North America.

115
00:05:37,30 --> 00:05:42,60
And we'll just finish up with an else, return lookup.

116
00:05:42,60 --> 00:05:46,30
Now my code is consistently indented making it more readable

117
00:05:46,30 --> 00:05:49,40
and both my project and my browser are set up

118
00:05:49,40 --> 00:05:51,40
to make it easy to keep that up

119
00:05:51,40 --> 00:05:54,00
in all the code I work with going forward.

