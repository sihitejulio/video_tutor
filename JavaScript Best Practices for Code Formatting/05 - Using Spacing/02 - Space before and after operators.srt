1
00:00:00,70 --> 00:00:02,30
- [Instructor] You've probably been using operators

2
00:00:02,30 --> 00:00:04,60
like plus, minus and equals

3
00:00:04,60 --> 00:00:07,00
since your earliest math courses.

4
00:00:07,00 --> 00:00:09,20
And it's unlikely your teachers ever required

5
00:00:09,20 --> 00:00:11,90
a particular spacing regimen around them.

6
00:00:11,90 --> 00:00:13,60
And like most of Javascript,

7
00:00:13,60 --> 00:00:16,70
white space isn't significant around operators.

8
00:00:16,70 --> 00:00:18,80
So there's no requirement to write equations

9
00:00:18,80 --> 00:00:21,80
or concatenations in any specific way.

10
00:00:21,80 --> 00:00:24,50
However, spaces help human readability

11
00:00:24,50 --> 00:00:25,80
in many parts of code,

12
00:00:25,80 --> 00:00:28,30
and operators are no exception.

13
00:00:28,30 --> 00:00:30,40
To make your code more readable,

14
00:00:30,40 --> 00:00:34,10
you should include a space before and after every operator.

15
00:00:34,10 --> 00:00:40,50
Including mathematical, logical, and assignment operators.

16
00:00:40,50 --> 00:00:42,00
In the code for my app,

17
00:00:42,00 --> 00:00:44,20
I've coded up a function to calculate the distance

18
00:00:44,20 --> 00:00:47,10
between two sets of geo coordinates.

19
00:00:47,10 --> 00:00:49,40
I didn't use any spaces around operators,

20
00:00:49,40 --> 00:00:53,00
and the statements are pretty dense.

21
00:00:53,00 --> 00:00:55,70
ESLint supports flagging missing spaces

22
00:00:55,70 --> 00:00:58,80
with the space-infix-ops rule.

23
00:00:58,80 --> 00:01:01,40
An infix operator is an operator that's used

24
00:01:01,40 --> 00:01:03,30
between two operands.

25
00:01:03,30 --> 00:01:04,80
That includes mathematical,

26
00:01:04,80 --> 00:01:07,00
logical, and assignment operators,

27
00:01:07,00 --> 00:01:09,80
as well as some others.

28
00:01:09,80 --> 00:01:12,80
I can add this rule to my eslintrc file,

29
00:01:12,80 --> 00:01:15,40
with a simple string to flag it as an error.

30
00:01:15,40 --> 00:01:17,80
It doesn't need to take options.

31
00:01:17,80 --> 00:01:23,20
So, that's going to be space-infix-ops

32
00:01:23,20 --> 00:01:27,10
with a value as a string of error.

33
00:01:27,10 --> 00:01:28,90
And saving that,

34
00:01:28,90 --> 00:01:31,40
you can see I've got lots of errors over there

35
00:01:31,40 --> 00:01:34,30
being flagged in my app.js file.

36
00:01:34,30 --> 00:01:35,60
So switching back,

37
00:01:35,60 --> 00:01:37,00
I can see I have underlines

38
00:01:37,00 --> 00:01:39,60
almost everywhere in my function.

39
00:01:39,60 --> 00:01:41,60
And when I hover over one of these

40
00:01:41,60 --> 00:01:48,70
the error message points out that I'm missing spaces.

41
00:01:48,70 --> 00:01:50,70
So I can just follow all these underlines

42
00:01:50,70 --> 00:01:54,10
to figure out where I need to add spaces.

43
00:01:54,10 --> 00:02:00,40
(keyboard clicks)

44
00:02:00,40 --> 00:02:08,30
Which is around all my operators.

45
00:02:08,30 --> 00:02:09,50
And little by little ...

46
00:02:09,50 --> 00:02:11,80
(keyboard clicks)

47
00:02:11,80 --> 00:02:13,30
Type another one.

48
00:02:13,30 --> 00:02:16,90
(keyboard clicks)

49
00:02:16,90 --> 00:02:19,30
One space at a time.

50
00:02:19,30 --> 00:02:25,70
(keyboard clicks)

51
00:02:25,70 --> 00:02:28,10
Even around this logical operator here.

52
00:02:28,10 --> 00:02:31,70
I want to have those spaces.

53
00:02:31,70 --> 00:02:35,00
And another one.

54
00:02:35,00 --> 00:02:37,90
And a mathematical operator.

55
00:02:37,90 --> 00:02:38,80
And now I'm good,

56
00:02:38,80 --> 00:02:43,30
and now I can see I don't have any red underlines anymore.

57
00:02:43,30 --> 00:02:45,90
Now I can also customize a format setting

58
00:02:45,90 --> 00:02:47,30
in Visual Studio code

59
00:02:47,30 --> 00:02:49,20
to do this for me automatically.

60
00:02:49,20 --> 00:02:53,90
So in my Settings I'm going to search on binary,

61
00:02:53,90 --> 00:02:56,50
and we have both Javascript and Typescript settings

62
00:02:56,50 --> 00:02:58,10
that parallel each other.

63
00:02:58,10 --> 00:03:00,00
In this case I'm looking for the Javascript setting

64
00:03:00,00 --> 00:03:05,30
which is insert space before and after binary operators.

65
00:03:05,30 --> 00:03:07,40
So this rule is turned on by default,

66
00:03:07,40 --> 00:03:10,30
and this means that if I use the editor to format my code,

67
00:03:10,30 --> 00:03:12,90
it will automatically add these spaces in

68
00:03:12,90 --> 00:03:15,20
which can save me some time.

69
00:03:15,20 --> 00:03:17,80
Enforcing these spacing rules around operators

70
00:03:17,80 --> 00:03:19,90
will make any code that uses operators

71
00:03:19,90 --> 00:03:23,00
easy for me and other developers to read.

