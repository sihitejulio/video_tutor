1
00:00:00,70 --> 00:00:02,70
- [Sasha] JavaScript is a forgiving language

2
00:00:02,70 --> 00:00:04,50
when it comes to code formatting,

3
00:00:04,50 --> 00:00:08,70
and JavaScript developers employ a variety of code styles.

4
00:00:08,70 --> 00:00:11,20
But there are some widely-accepted best practices

5
00:00:11,20 --> 00:00:14,20
for code formatting among JavaScript developers

6
00:00:14,20 --> 00:00:17,60
that can make your code easier to read and understand

7
00:00:17,60 --> 00:00:20,30
for team members you work with, other developers

8
00:00:20,30 --> 00:00:22,30
who might work with your code in the future,

9
00:00:22,30 --> 00:00:25,30
and for you, yourself, when you return to your own code

10
00:00:25,30 --> 00:00:27,10
after some time away.

11
00:00:27,10 --> 00:00:29,60
Implementing best practices in your code

12
00:00:29,60 --> 00:00:31,70
and configuring tools to help you

13
00:00:31,70 --> 00:00:34,30
can also sharpen up files that may look sloppy

14
00:00:34,30 --> 00:00:36,80
or poorly organized simply as a result

15
00:00:36,80 --> 00:00:40,80
of your use of spaces, punctuation, and line breaks.

16
00:00:40,80 --> 00:00:42,50
In my LinkedIn Learning course,

17
00:00:42,50 --> 00:00:45,00
I explore best practices as laid out in some

18
00:00:45,00 --> 00:00:48,60
of the industry's most commonly referenced style guides.

19
00:00:48,60 --> 00:00:51,20
I also show you how to set up ESLint rules

20
00:00:51,20 --> 00:00:54,20
to flag deviations from styles that you set

21
00:00:54,20 --> 00:00:56,40
and how to turn on features in your editor

22
00:00:56,40 --> 00:01:00,20
that can help format your code as you write and save it.

23
00:01:00,20 --> 00:01:02,90
I'm Sasha Vodnik, and I've been writing JavaScript

24
00:01:02,90 --> 00:01:05,40
since the browser wars of the '90s.

25
00:01:05,40 --> 00:01:07,50
If you want to put modern code formatting

26
00:01:07,50 --> 00:01:10,50
to work on your team, an open source project,

27
00:01:10,50 --> 00:01:13,00
or just in your own personal work,

28
00:01:13,00 --> 00:01:15,30
I invite you to join me for this course

29
00:01:15,30 --> 00:01:19,00
on JavaScript best practices for code formatting.

