1
00:00:00,70 --> 00:00:03,00
- [Instructor] This course assumes you have some experience

2
00:00:03,00 --> 00:00:08,00
coding in front-end JavaScript including modern ES6 syntax.

3
00:00:08,00 --> 00:00:10,50
If you don't have experience with JavaScript,

4
00:00:10,50 --> 00:00:14,30
a basic JavaScript course would be a great place to start.

5
00:00:14,30 --> 00:00:17,60
To familiarize yourself with modern JavaScript syntax,

6
00:00:17,60 --> 00:00:20,00
explore a course on ES6.

