1
00:00:00,40 --> 00:00:03,60
- To work along with me on the files for this course

2
00:00:03,60 --> 00:00:05,70
you need three applications.

3
00:00:05,70 --> 00:00:07,20
ESLint,

4
00:00:07,20 --> 00:00:08,40
a web browser

5
00:00:08,40 --> 00:00:10,90
and a code editor.

6
00:00:10,90 --> 00:00:14,30
ESLint is a command line utility that you install using the

7
00:00:14,30 --> 00:00:17,60
node package manager or npm.

8
00:00:17,60 --> 00:00:20,90
Npm is part of the node JS run time.

9
00:00:20,90 --> 00:00:23,00
If you don't have node installed,

10
00:00:23,00 --> 00:00:25,10
start by visiting nodejs.org

11
00:00:25,10 --> 00:00:28,70
and installing the LTS version.

12
00:00:28,70 --> 00:00:29,60
With that installed

13
00:00:29,60 --> 00:00:31,40
open your terminal application

14
00:00:31,40 --> 00:00:34,60
and navigate to the exercise files folder

15
00:00:34,60 --> 00:00:39,20
that contains the files for this course.

16
00:00:39,20 --> 00:00:41,70
And we're going to start by initializing this directory

17
00:00:41,70 --> 00:00:44,10
for our use with npm.

18
00:00:44,10 --> 00:00:46,90
So we're going to type npm space init

19
00:00:46,90 --> 00:00:48,50
and press enter.

20
00:00:48,50 --> 00:00:50,20
It's going to ask you a series of questions and you're

21
00:00:50,20 --> 00:00:51,90
just going to press enter for all of them.

22
00:00:51,90 --> 00:00:54,60
For right now we're not concerned about the configuration

23
00:00:54,60 --> 00:00:55,50
here at all.

24
00:00:55,50 --> 00:00:59,00
So we're just accepting the default answers for all these.

25
00:00:59,00 --> 00:01:02,30
And when we get a new command prompt then we're all good.

26
00:01:02,30 --> 00:01:05,70
So now we have this directory initialized for npm

27
00:01:05,70 --> 00:01:08,50
and now we can install ESLint.

28
00:01:08,50 --> 00:01:12,90
SO, that next command is going to be npm space install

29
00:01:12,90 --> 00:01:16,00
space ESLint.

30
00:01:16,00 --> 00:01:19,60
And this will install the files that will enable you

31
00:01:19,60 --> 00:01:22,40
to use ESLint to the command line.

32
00:01:22,40 --> 00:01:24,80
We've got a warning here, and that's not a problem

33
00:01:24,80 --> 00:01:27,20
it's just telling us that if we're really serious about

34
00:01:27,20 --> 00:01:30,00
this later on we can go configure a file with some

35
00:01:30,00 --> 00:01:33,10
more information but for our uses for this course,

36
00:01:33,10 --> 00:01:36,40
this is just fine as is.

37
00:01:36,40 --> 00:01:39,70
You undoubtedly already have a web browser installed in

38
00:01:39,70 --> 00:01:43,40
your machine and any major modern browser, Chrome,

39
00:01:43,40 --> 00:01:47,70
Firefox or Microsoft edge is fine for this course.

40
00:01:47,70 --> 00:01:50,10
I'll be using Firefox for these videos

41
00:01:50,10 --> 00:01:53,60
which includes a powerful sweet of developer tools.

42
00:01:53,60 --> 00:01:56,00
A number of great code editors are available

43
00:01:56,00 --> 00:01:58,50
both free and paid apps.

44
00:01:58,50 --> 00:02:02,10
Any editor that lets you edit and save plain text is fine

45
00:02:02,10 --> 00:02:03,40
for this course.

46
00:02:03,40 --> 00:02:05,60
So if you have a code editor you like

47
00:02:05,60 --> 00:02:07,40
such as sublime text or Adam,

48
00:02:07,40 --> 00:02:09,70
it's fine to use that.

49
00:02:09,70 --> 00:02:12,60
I use visual studio code in these videos.

50
00:02:12,60 --> 00:02:15,90
Which is a version of Microsoft's visual studio code

51
00:02:15,90 --> 00:02:18,70
created specifically for web development.

52
00:02:18,70 --> 00:02:22,40
Visual studio code is free and has Windows, Mac and

53
00:02:22,40 --> 00:02:23,90
Linux releases.

54
00:02:23,90 --> 00:02:26,70
The code is available on GitHub and users can submit

55
00:02:26,70 --> 00:02:28,90
issues there as well.

56
00:02:28,90 --> 00:02:31,30
I've turned on word wrap on my editor.

57
00:02:31,30 --> 00:02:32,80
If you want to do the same,

58
00:02:32,80 --> 00:02:36,90
just click view and then toggle word wrap.

59
00:02:36,90 --> 00:02:39,70
This ensures that long lines of code don't run off

60
00:02:39,70 --> 00:02:41,50
the screen.

61
00:02:41,50 --> 00:02:45,00
I've also installed a couple extensions.

62
00:02:45,00 --> 00:02:48,90
The ESLint extension by Dirk Baeumer enables visual

63
00:02:48,90 --> 00:02:51,80
studio code to highlight code that violates rules

64
00:02:51,80 --> 00:02:57,30
specified in a projects ESLint configuration file.

65
00:02:57,30 --> 00:03:01,50
Live Server by Ritwick Dey is an http server you can

66
00:03:01,50 --> 00:03:04,50
launch with a single click that automatically opens

67
00:03:04,50 --> 00:03:07,80
the current html document in your default browser.

68
00:03:07,80 --> 00:03:10,50
This makes testing code in the browser quick and easy

69
00:03:10,50 --> 00:03:12,20
to do.

70
00:03:12,20 --> 00:03:14,50
If you want to learn more about anything I just used or

71
00:03:14,50 --> 00:03:16,10
talk about in this course,

72
00:03:16,10 --> 00:03:18,10
I encourage you to explore the library

73
00:03:18,10 --> 00:03:20,50
for a deeper dive on that topic.

74
00:03:20,50 --> 00:03:22,00
Now, let's get started.

