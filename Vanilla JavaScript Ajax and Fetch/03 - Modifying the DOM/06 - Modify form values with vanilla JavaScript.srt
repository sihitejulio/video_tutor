1
00:00:00,50 --> 00:00:02,60
- [Instructor] My form has event listeners

2
00:00:02,60 --> 00:00:04,60
and I have references to the data

3
00:00:04,60 --> 00:00:06,30
that I need from the response.

4
00:00:06,30 --> 00:00:09,10
So, at this point, my finals step for the form

5
00:00:09,10 --> 00:00:10,90
is to actually put the nine-digit

6
00:00:10,90 --> 00:00:13,50
zip code data into the form.

7
00:00:13,50 --> 00:00:15,80
To add data to a form field,

8
00:00:15,80 --> 00:00:19,60
I can use the same value property of the element reference

9
00:00:19,60 --> 00:00:23,30
that I used to get the values from the other input elements.

10
00:00:23,30 --> 00:00:25,80
I have a reference to the zip code input element

11
00:00:25,80 --> 00:00:29,30
saved with the variable name zip field.

12
00:00:29,30 --> 00:00:32,70
So, down in my update UI success function,

13
00:00:32,70 --> 00:00:35,60
I can comment out my console.log statements,

14
00:00:35,60 --> 00:00:37,10
and I'm just going to take out both of them

15
00:00:37,10 --> 00:00:39,80
because I don't really need them anymore,

16
00:00:39,80 --> 00:00:45,00
and instead, I can specify zipfield.value

17
00:00:45,00 --> 00:00:46,30
equals

18
00:00:46,30 --> 00:00:49,00
and then I can just copy the same expression

19
00:00:49,00 --> 00:00:51,90
that I console.logged previously,

20
00:00:51,90 --> 00:00:54,90
zip and a hyphen and plus four.

21
00:00:54,90 --> 00:00:57,40
And so, in this case, I'm saying

22
00:00:57,40 --> 00:01:00,40
instead of getting the value from the zip field,

23
00:01:00,40 --> 00:01:02,40
I am specifying a new value,

24
00:01:02,40 --> 00:01:05,30
which is then placed in that form field.

25
00:01:05,30 --> 00:01:08,30
So I'm going to save my changes.

26
00:01:08,30 --> 00:01:12,30
I'm going to go live with my HTML document.

27
00:01:12,30 --> 00:01:15,20
I'm going to open the console up

28
00:01:15,20 --> 00:01:18,60
and then, using my sample address,

29
00:01:18,60 --> 00:01:21,60
tabbing out of that final field and there we go.

30
00:01:21,60 --> 00:01:25,00
So the request is sent, the response is parsed,

31
00:01:25,00 --> 00:01:28,60
and that nine-digit zip code is added to the zip field,

32
00:01:28,60 --> 00:01:31,30
which is exactly what I was aiming to do.

33
00:01:31,30 --> 00:01:32,70
So far, I've been customizing

34
00:01:32,70 --> 00:01:36,70
this update UI success function for my form data,

35
00:01:36,70 --> 00:01:39,90
but code is currently written to use all the same functions

36
00:01:39,90 --> 00:01:43,90
for both my address request and my national park request.

37
00:01:43,90 --> 00:01:45,30
But both of those sets of data

38
00:01:45,30 --> 00:01:48,70
are going to require very different UI updates,

39
00:01:48,70 --> 00:01:51,10
so I need to make a few updates to my code

40
00:01:51,10 --> 00:01:54,70
to keep the main XHR code usable for any request

41
00:01:54,70 --> 00:01:58,10
while ensuring that any response gets a customer handler

42
00:01:58,10 --> 00:02:00,50
and that starts with update UI success,

43
00:02:00,50 --> 00:02:04,10
which is now customized just for the smarty streets request.

44
00:02:04,10 --> 00:02:09,00
So, I'll rename this function smarty update UI success

45
00:02:09,00 --> 00:02:13,20
and then I'll create a new success handler for parks data,

46
00:02:13,20 --> 00:02:17,10
so const park update UI success,

47
00:02:17,10 --> 00:02:20,30
which is a function that takes a data parameter

48
00:02:20,30 --> 00:02:24,30
and we'll simply console.log data.

49
00:02:24,30 --> 00:02:26,60
Right now, the update UI error function

50
00:02:26,60 --> 00:02:29,30
is the same for both, but eventually, I'll want to do

51
00:02:29,30 --> 00:02:31,40
some different things with the UI.

52
00:02:31,40 --> 00:02:37,50
So, I'll change that to smarty update UI error

53
00:02:37,50 --> 00:02:39,70
and then I'll make a copy of that

54
00:02:39,70 --> 00:02:44,10
and I'll change the name to park update UI error.

55
00:02:44,10 --> 00:02:46,50
Then in my response method function,

56
00:02:46,50 --> 00:02:51,90
I want to take a couple more parameters, succeed and fail.

57
00:02:51,90 --> 00:02:53,20
These are going to be the names

58
00:02:53,20 --> 00:02:56,40
of the correct success and failure functions

59
00:02:56,40 --> 00:02:58,60
for the requests that I'm making.

60
00:02:58,60 --> 00:03:01,80
And then, in my if/else structure,

61
00:03:01,80 --> 00:03:03,50
I'll update the function names

62
00:03:03,50 --> 00:03:06,60
to reference those succeed and fail parameters.

63
00:03:06,60 --> 00:03:10,60
So instead of specifically calling the function name,

64
00:03:10,60 --> 00:03:15,00
I will reference the function that was passed as a parameter

65
00:03:15,00 --> 00:03:18,70
and likewise for the fail.

66
00:03:18,70 --> 00:03:22,40
And then, in the create request function,

67
00:03:22,40 --> 00:03:26,20
I also want to add

68
00:03:26,20 --> 00:03:29,30
succeed and fail parameters

69
00:03:29,30 --> 00:03:31,30
and I need to add those as arguments

70
00:03:31,30 --> 00:03:33,90
in the response method function call here.

71
00:03:33,90 --> 00:03:39,50
So we'll just pass those right along.

72
00:03:39,50 --> 00:03:42,80
Now, the check completion function is custom

73
00:03:42,80 --> 00:03:44,70
for the smarty streets request,

74
00:03:44,70 --> 00:03:48,60
so I can customize the create request function call,

75
00:03:48,60 --> 00:03:52,90
passing along smarty update UI success

76
00:03:52,90 --> 00:03:55,80
and smarty update UI error.

77
00:03:55,80 --> 00:04:00,40
That needed to be a success.

78
00:04:00,40 --> 00:04:03,70
So now that should cascade through all my function calls

79
00:04:03,70 --> 00:04:06,20
and all the rest of the functions should work correctly

80
00:04:06,20 --> 00:04:09,90
and end up with the appropriate success and fail callbacks.

81
00:04:09,90 --> 00:04:12,90
And then for the national parks request,

82
00:04:12,90 --> 00:04:14,40
which is currently just a simple

83
00:04:14,40 --> 00:04:16,40
create request function call,

84
00:04:16,40 --> 00:04:21,40
I'll uncomment that statement and I'll add

85
00:04:21,40 --> 00:04:27,70
park update UI success and park update UI error

86
00:04:27,70 --> 00:04:30,30
as additional arguments.

87
00:04:30,30 --> 00:04:32,00
Then I'm going to save my work.

88
00:04:32,00 --> 00:04:33,20
Check in the browser.

89
00:04:33,20 --> 00:04:35,10
And so I have my park data

90
00:04:35,10 --> 00:04:37,40
automatically requested and logged,

91
00:04:37,40 --> 00:04:41,20
so that means the success handler's working just fine there

92
00:04:41,20 --> 00:04:47,10
and then testing out an address request from the form,

93
00:04:47,10 --> 00:04:48,40
that's working as well.

94
00:04:48,40 --> 00:04:52,80
So now, I have custom UI functions for the AJAX requests

95
00:04:52,80 --> 00:04:55,10
while still sharing the main code

96
00:04:55,10 --> 00:04:58,00
to create the request and check the response.

