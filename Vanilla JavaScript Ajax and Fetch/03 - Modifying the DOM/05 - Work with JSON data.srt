1
00:00:00,20 --> 00:00:03,20
- Once you have the response from your AJAX request,

2
00:00:03,20 --> 00:00:05,20
you have to dig into it and find the

3
00:00:05,20 --> 00:00:07,40
specific data you need.

4
00:00:07,40 --> 00:00:10,70
JavaScript Object Notation, or JSON, is

5
00:00:10,70 --> 00:00:13,10
by far the most common format used to

6
00:00:13,10 --> 00:00:15,50
exchange data on the modern web.

7
00:00:15,50 --> 00:00:18,40
JSON data is transmitted as a string, so the

8
00:00:18,40 --> 00:00:20,40
first thing you need to do is parse it

9
00:00:20,40 --> 00:00:22,80
into the data format it encodes.

10
00:00:22,80 --> 00:00:25,30
Although the word object is in the name,

11
00:00:25,30 --> 00:00:27,20
JSON data could parse out either

12
00:00:27,20 --> 00:00:29,80
as an object or as an array,

13
00:00:29,80 --> 00:00:31,40
which is technically just another type

14
00:00:31,40 --> 00:00:33,00
of object in JavaScript.

15
00:00:33,00 --> 00:00:36,10
To parse JSON, you use the parse method

16
00:00:36,10 --> 00:00:40,20
of the JSON object which is built into JavaScript.

17
00:00:40,20 --> 00:00:41,90
Now that we have a string of data

18
00:00:41,90 --> 00:00:43,80
back from our web service,

19
00:00:43,80 --> 00:00:45,80
to go any further in our code

20
00:00:45,80 --> 00:00:47,50
we'll need to parse that.

21
00:00:47,50 --> 00:00:50,80
So, in my updateUISuccess function

22
00:00:50,80 --> 00:00:55,10
I want to create a new variable

23
00:00:55,10 --> 00:00:59,70
called parsedData and I'm going to set that equal

24
00:00:59,70 --> 00:01:03,10
to JSON.parse and I'm going to grab the value of

25
00:01:03,10 --> 00:01:06,50
data, which is that JSON string that came in

26
00:01:06,50 --> 00:01:09,20
from my API call.

27
00:01:09,20 --> 00:01:12,40
So JSON, all caps, is the name of the object

28
00:01:12,40 --> 00:01:16,00
that we use to work with, JSON in JavaScript,

29
00:01:16,00 --> 00:01:19,90
and parse is a method that takes a JSON

30
00:01:19,90 --> 00:01:24,20
string and renders it as a JavaScript object or array.

31
00:01:24,20 --> 00:01:25,90
And then, I just need to replace data

32
00:01:25,90 --> 00:01:29,80
in my console.log statement with parsedData.

33
00:01:29,80 --> 00:01:33,50
And then, saving those changes

34
00:01:33,50 --> 00:01:36,30
and then starting up my live server

35
00:01:36,30 --> 00:01:38,40
from the html document.

36
00:01:38,40 --> 00:01:40,40
Going to open up my console

37
00:01:40,40 --> 00:01:43,10
and then to test this out

38
00:01:43,10 --> 00:01:48,10
we'll do 1600 Pennsylvania Avenue again

39
00:01:48,10 --> 00:01:50,40
in Washington DC

40
00:01:50,40 --> 00:01:52,80
Tab out of DC

41
00:01:52,80 --> 00:01:56,10
And now, notice that this first log statement

42
00:01:56,10 --> 00:01:58,70
is from the National Parks request.

43
00:01:58,70 --> 00:02:03,60
The second one is from my address request.

44
00:02:03,60 --> 00:02:07,50
So now, the response, both from the Park's array

45
00:02:07,50 --> 00:02:11,50
and from the SmartyStreets address request

46
00:02:11,50 --> 00:02:14,30
is no longer just a big string of JSON.

47
00:02:14,30 --> 00:02:18,10
Instead, I get an array containing an object

48
00:02:18,10 --> 00:02:20,80
for my address information, and I get

49
00:02:20,80 --> 00:02:24,10
an object for the parks information.

50
00:02:24,10 --> 00:02:27,10
Now when I open a data URL using the address bar,

51
00:02:27,10 --> 00:02:29,50
the response is displayed in the browser window,

52
00:02:29,50 --> 00:02:30,70
and I can work with that with

53
00:02:30,70 --> 00:02:33,10
a browser extension like JSON formatter.

54
00:02:33,10 --> 00:02:35,10
But modern browsers also make it easy to explore

55
00:02:35,10 --> 00:02:37,40
data in the console.

56
00:02:37,40 --> 00:02:40,50
So, for instance, I can click the triangle

57
00:02:40,50 --> 00:02:43,20
here to expand the next level in this array

58
00:02:43,20 --> 00:02:45,00
and start to explore the structure.

59
00:02:45,00 --> 00:02:46,70
So this array contains a single element

60
00:02:46,70 --> 00:02:48,90
with an index number of zero.

61
00:02:48,90 --> 00:02:53,10
And so I'll click the triangle for that element

62
00:02:53,10 --> 00:02:54,90
to open that up.

63
00:02:54,90 --> 00:02:57,10
And we have a nested data structure, which is

64
00:02:57,10 --> 00:02:59,70
super common in AJAX responses.

65
00:02:59,70 --> 00:03:02,40
An array containing an object or multiple objects,

66
00:03:02,40 --> 00:03:05,70
an object containing an array containing multiple objects,

67
00:03:05,70 --> 00:03:07,80
and so I can keep drilling down by

68
00:03:07,80 --> 00:03:09,70
simply clicking triangles.

69
00:03:09,70 --> 00:03:13,90
And so, for instance, we have a components property.

70
00:03:13,90 --> 00:03:17,10
And this is where I have each of the individual

71
00:03:17,10 --> 00:03:20,10
pieces of the address information

72
00:03:20,10 --> 00:03:23,30
broken out, each with a different property name.

73
00:03:23,30 --> 00:03:25,20
And so here I have the data I need.

74
00:03:25,20 --> 00:03:26,70
We got a zip code property,

75
00:03:26,70 --> 00:03:28,50
which is the five digit zip code.

76
00:03:28,50 --> 00:03:30,90
We've got a plus4 code property,

77
00:03:30,90 --> 00:03:33,50
which gives me those last four digits.

78
00:03:33,50 --> 00:03:36,30
So I put those two together with a dash between them,

79
00:03:36,30 --> 00:03:39,30
I'll have the nine digit zip code that I'm aiming for.

80
00:03:39,30 --> 00:03:41,60
Now one of the challenges in accessing

81
00:03:41,60 --> 00:03:44,00
data return from an AJAX request is building

82
00:03:44,00 --> 00:03:47,30
out the combination of square bracket notations

83
00:03:47,30 --> 00:03:50,20
and dot notation that's necessary to reference

84
00:03:50,20 --> 00:03:52,80
each specific piece of data that you want

85
00:03:52,80 --> 00:03:54,20
from the response.

86
00:03:54,20 --> 00:03:56,00
But this is another place that browser

87
00:03:56,00 --> 00:03:58,20
developer tools can really help.

88
00:03:58,20 --> 00:04:01,20
But notice that when I hover over the name of

89
00:04:01,20 --> 00:04:03,90
the data I want, the developer tools display the

90
00:04:03,90 --> 00:04:08,00
path to that specific piece of data, and I can

91
00:04:08,00 --> 00:04:09,10
even copy this.

92
00:04:09,10 --> 00:04:13,70
So if I right click, I can say copy property path.

93
00:04:13,70 --> 00:04:16,00
Now keep in mind, this is the path through this

94
00:04:16,00 --> 00:04:18,70
chunk of data and this chunk of data in my code

95
00:04:18,70 --> 00:04:20,80
has the name parsedData.

96
00:04:20,80 --> 00:04:23,60
That is the variable that's storing this reference.

97
00:04:23,60 --> 00:04:26,30
So going back to my JavaScript code,

98
00:04:26,30 --> 00:04:30,10
in my updateUISuccess, to create that zip code

99
00:04:30,10 --> 00:04:34,00
I can say let zip equal and then I want to reference

100
00:04:34,00 --> 00:04:38,80
parsedData, parsedData, and then I'm

101
00:04:38,80 --> 00:04:41,10
going to paste in that reference.

102
00:04:41,10 --> 00:04:44,40
So notice that Chrome adds in these extra

103
00:04:44,40 --> 00:04:45,80
quotes which I don't need.

104
00:04:45,80 --> 00:04:47,80
So I'm going to take those out because

105
00:04:47,80 --> 00:04:49,80
in the square brackets here all I need is

106
00:04:49,80 --> 00:04:52,00
the number zero for the index.

107
00:04:52,00 --> 00:04:53,70
So parsedData is an array.

108
00:04:53,70 --> 00:04:55,00
I want the first item in that array

109
00:04:55,00 --> 00:04:56,20
which is an object.

110
00:04:56,20 --> 00:04:58,60
Within that object, I want the components property.

111
00:04:58,60 --> 00:05:00,20
And within that components object

112
00:05:00,20 --> 00:05:02,00
I want the zip code property.

113
00:05:02,00 --> 00:05:04,10
So now I have those first five digits saved

114
00:05:04,10 --> 00:05:06,40
in the zip variable, and I can actually

115
00:05:06,40 --> 00:05:09,70
make that a const cause I'm not going to need to change it.

116
00:05:09,70 --> 00:05:11,10
And then one more constant

117
00:05:11,10 --> 00:05:15,10
I'm going to create plus four as a variable name.

118
00:05:15,10 --> 00:05:19,40
This is also going to be part of parsedData

119
00:05:19,40 --> 00:05:22,80
and I can go back to my browser

120
00:05:22,80 --> 00:05:23,90
plus four code

121
00:05:23,90 --> 00:05:25,50
I'm going to right click here,

122
00:05:25,50 --> 00:05:29,30
copy that property path and back in my editor

123
00:05:29,30 --> 00:05:33,10
I'm going to paste that in, once again, take out those quotes

124
00:05:33,10 --> 00:05:39,10
and then I'm going to create another console.log statement

125
00:05:39,10 --> 00:05:41,70
for the data that I'm actually going to want to

126
00:05:41,70 --> 00:05:44,40
stick in the webpage.

127
00:05:44,40 --> 00:05:49,90
And that's going to be the zip variable plus a string

128
00:05:49,90 --> 00:05:51,90
containing simply a dash, which is how

129
00:05:51,90 --> 00:05:55,20
you separate the two pieces of the zip code, plus

130
00:05:55,20 --> 00:05:57,70
the value of plus four.

131
00:05:57,70 --> 00:06:00,00
And so if I've done this right, I should get

132
00:06:00,00 --> 00:06:02,60
those two pieces of data logged to the console

133
00:06:02,60 --> 00:06:04,00
with a dash between them.

134
00:06:04,00 --> 00:06:07,60
So I'm going to save these changes, go back to my browser

135
00:06:07,60 --> 00:06:09,60
and one more time, we'll try

136
00:06:09,60 --> 00:06:12,70
1600 Pennsylvania Avenue, try the city

137
00:06:12,70 --> 00:06:15,60
of Washington, state DC, tab out of that

138
00:06:15,60 --> 00:06:18,40
and there is my nine digit zip code.

139
00:06:18,40 --> 00:06:20,80
So notice I've got an error up here, and that's

140
00:06:20,80 --> 00:06:22,80
because I've just written very specific code for

141
00:06:22,80 --> 00:06:24,90
SmartyStreets while I still have a

142
00:06:24,90 --> 00:06:28,10
request going out to Parks.

143
00:06:28,10 --> 00:06:30,50
The easiest way to fix that right now is just going to be

144
00:06:30,50 --> 00:06:33,70
to comment out this call to the Parks URL and that

145
00:06:33,70 --> 00:06:35,90
will ensure that we don't get that error anymore.

146
00:06:35,90 --> 00:06:36,80
And so one more time,

147
00:06:36,80 --> 00:06:40,60
1600 Pennsylvania Avenue, Washington, DC.

148
00:06:40,60 --> 00:06:43,20
Tab out of that, so I get my data back, and

149
00:06:43,20 --> 00:06:45,10
then I get that nine digit zip code.

150
00:06:45,10 --> 00:06:48,00
So now I have that data I need, and it happens

151
00:06:48,00 --> 00:06:52,00
in response to a user entering address city and state data.

