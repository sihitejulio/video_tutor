1
00:00:00,10 --> 00:00:06,40
(upbeat electronic music)

2
00:00:06,40 --> 00:00:09,40
- [Narrator] To get the part data into the dom,

3
00:00:09,40 --> 00:00:11,10
the first thing I need is references

4
00:00:11,10 --> 00:00:13,10
to a couple more dom elements,

5
00:00:13,10 --> 00:00:14,80
and we've done this before.

6
00:00:14,80 --> 00:00:18,00
So, after parkSection, I'm going to create

7
00:00:18,00 --> 00:00:21,30
a new const called parkName,

8
00:00:21,30 --> 00:00:28,30
and that's going to be document.querySelector,

9
00:00:28,30 --> 00:00:34,80
and it's going to be #specials h2 a.

10
00:00:34,80 --> 00:00:38,00
And then we also need one for the description,

11
00:00:38,00 --> 00:00:41,10
so I'm going to call this parkDesc.

12
00:00:41,10 --> 00:00:44,60
That's going to be document.querySelector,

13
00:00:44,60 --> 00:00:51,00
and that's just #specials, space, p.

14
00:00:51,00 --> 00:00:54,80
And then, in the parkUpdateUISuccess function,

15
00:00:54,80 --> 00:00:56,40
I want to parse data variable,

16
00:00:56,40 --> 00:00:59,10
storing the parsed version of the data.

17
00:00:59,10 --> 00:01:04,10
So I will start with a const parsedData.

18
00:01:04,10 --> 00:01:07,00
And that's JSON.parse

19
00:01:07,00 --> 00:01:08,00
data.

20
00:01:08,00 --> 00:01:13,50
And then we'll change our console.log to parseData.

21
00:01:13,50 --> 00:01:17,70
And so, just saving this,

22
00:01:17,70 --> 00:01:20,60
starting my viewer,

23
00:01:20,60 --> 00:01:23,40
checking out the console,

24
00:01:23,40 --> 00:01:26,00
and after a moment, I should get a response.

25
00:01:26,00 --> 00:01:27,40
And there we go.

26
00:01:27,40 --> 00:01:29,80
And so now I can see the data,

27
00:01:29,80 --> 00:01:31,50
which is an object,

28
00:01:31,50 --> 00:01:32,90
and if I start digging in,

29
00:01:32,90 --> 00:01:35,40
I can see that object has this property

30
00:01:35,40 --> 00:01:37,30
that has a lot more data in it.

31
00:01:37,30 --> 00:01:40,10
So in the data property, I've got an array.

32
00:01:40,10 --> 00:01:43,90
And each one of these arrays stores an object.

33
00:01:43,90 --> 00:01:45,60
And each one of these objects

34
00:01:45,60 --> 00:01:47,70
contains information about a single park.

35
00:01:47,70 --> 00:01:51,00
This is a super common way that you'll see data come in

36
00:01:51,00 --> 00:01:53,70
from a lot of API's, although not all of them.

37
00:01:53,70 --> 00:01:58,50
An object, containing a property, that contains an array

38
00:01:58,50 --> 00:02:02,30
and each element in that array is itself an object

39
00:02:02,30 --> 00:02:03,80
containing data.

40
00:02:03,80 --> 00:02:06,40
So, the first thing I need is the full name of the park.

41
00:02:06,40 --> 00:02:07,80
And so we're just starting by working

42
00:02:07,80 --> 00:02:09,60
with the first element in the array,

43
00:02:09,60 --> 00:02:12,00
which is index zero.

44
00:02:12,00 --> 00:02:15,60
And so we've got a property called fullName.

45
00:02:15,60 --> 00:02:18,60
If I hover over that, there is the reference.

46
00:02:18,60 --> 00:02:23,00
So I'm going to copy that property path

47
00:02:23,00 --> 00:02:24,90
and back in my editor,

48
00:02:24,90 --> 00:02:26,80
I can use my parkName reference

49
00:02:26,80 --> 00:02:29,00
and set the text content.

50
00:02:29,00 --> 00:02:35,00
So, parkName.textContent equals

51
00:02:35,00 --> 00:02:39,10
and remember I'm working with parsedData

52
00:02:39,10 --> 00:02:42,20
dot and then I can paste in my reference.

53
00:02:42,20 --> 00:02:43,80
Again, I can take out those quotes

54
00:02:43,80 --> 00:02:45,90
that Chrome sticks in there.

55
00:02:45,90 --> 00:02:49,40
And that is a reference to the fullName value,

56
00:02:49,40 --> 00:02:51,40
which I'm now sticking into the element reference

57
00:02:51,40 --> 00:02:55,50
by the parkName variable as its text content.

58
00:02:55,50 --> 00:02:58,30
And then similarly, for the url,

59
00:02:58,30 --> 00:03:00,10
I'm going to go through here and find

60
00:03:00,10 --> 00:03:01,70
there is a url property,

61
00:03:01,70 --> 00:03:03,40
so again I'm going to right-click,

62
00:03:03,40 --> 00:03:05,60
copy that property path

63
00:03:05,60 --> 00:03:11,10
and back in my editor parkName.href,

64
00:03:11,10 --> 00:03:15,30
which is the attribute we use to set the target of a link,

65
00:03:15,30 --> 00:03:17,80
it's going to be parsedData

66
00:03:17,80 --> 00:03:20,50
dot and then paste in my reference,

67
00:03:20,50 --> 00:03:23,50
take out those quotes and I've got the url.

68
00:03:23,50 --> 00:03:25,90
And finally for the description,

69
00:03:25,90 --> 00:03:30,70
that is in the parkDesc variable.

70
00:03:30,70 --> 00:03:34,60
So it's parcDesc again dot textContent,

71
00:03:34,60 --> 00:03:37,50
cause we're just putting in some text

72
00:03:37,50 --> 00:03:40,30
and switching back over to our data,

73
00:03:40,30 --> 00:03:43,90
we have a description property.

74
00:03:43,90 --> 00:03:48,90
So I'm going to copy that property path

75
00:03:48,90 --> 00:03:51,20
and that's going to be parsedData

76
00:03:51,20 --> 00:03:56,30
dot and then take out those quotes.

77
00:03:56,30 --> 00:03:58,90
And we have the description.

78
00:03:58,90 --> 00:04:01,10
And so saving that,

79
00:04:01,10 --> 00:04:03,10
turning to the browser

80
00:04:03,10 --> 00:04:07,30
and now notice we have a heading with a link.

81
00:04:07,30 --> 00:04:08,20
When we hover over it,

82
00:04:08,20 --> 00:04:11,00
we can see that the pointer turns to a hand.

83
00:04:11,00 --> 00:04:14,30
At the bottom of the screen, we have the link,

84
00:04:14,30 --> 00:04:16,90
the remote url that that links to

85
00:04:16,90 --> 00:04:21,00
and then we've also got the description displayed here.

86
00:04:21,00 --> 00:04:22,40
So, we're great!

87
00:04:22,40 --> 00:04:24,20
Now the other thing I want to change is that

88
00:04:24,20 --> 00:04:26,30
I want this ajax request to happen

89
00:04:26,30 --> 00:04:28,70
only after the dom finishes loading.

90
00:04:28,70 --> 00:04:31,40
And that's the dom content loaded event

91
00:04:31,40 --> 00:04:33,80
on the window object.

92
00:04:33,80 --> 00:04:39,10
So, scrolling down, I'm going to comment out

93
00:04:39,10 --> 00:04:42,00
my existing createRequest function call.

94
00:04:42,00 --> 00:04:44,30
And instead I want to use an EventListener.

95
00:04:44,30 --> 00:04:46,70
And so again, instead of that being in response

96
00:04:46,70 --> 00:04:49,00
to something happening on a specific element,

97
00:04:49,00 --> 00:04:51,80
it is on the window object itself.

98
00:04:51,80 --> 00:04:53,80
I can add an EventListener.

99
00:04:53,80 --> 00:04:58,10
And the event is DOMContentLoaded

100
00:04:58,10 --> 00:05:00,30
and that's got to be DOM all caps.

101
00:05:00,30 --> 00:05:03,20
And then, that's going to be a function.

102
00:05:03,20 --> 00:05:05,30
And so, in this case,

103
00:05:05,30 --> 00:05:09,40
I can't simply call my createRequest function

104
00:05:09,40 --> 00:05:11,80
because I need to pass in some arguments.

105
00:05:11,80 --> 00:05:14,60
So I'm going to put in an in-line function

106
00:05:14,60 --> 00:05:17,60
and I'll just use an arrow function for that.

107
00:05:17,60 --> 00:05:20,00
So within this function,

108
00:05:20,00 --> 00:05:23,30
I can call createRequest

109
00:05:23,30 --> 00:05:25,60
and I can actually pass in arguments.

110
00:05:25,60 --> 00:05:28,50
So, parkUrl,

111
00:05:28,50 --> 00:05:32,60
parkUpdateUISuccess

112
00:05:32,60 --> 00:05:33,90
for the success handler

113
00:05:33,90 --> 00:05:37,80
and parkUpdateUIError for the error handler.

114
00:05:37,80 --> 00:05:40,10
And then saving that one more time.

115
00:05:40,10 --> 00:05:48,30
I'm going to check back in my browser.

116
00:05:48,30 --> 00:05:53,00
And my url variable is parksUrl not parkUrl

117
00:05:53,00 --> 00:05:55,90
and that's why that function call didn't work.

118
00:05:55,90 --> 00:05:58,40
Changing that and saving that again,

119
00:05:58,40 --> 00:06:02,50
switching back and there's my data.

120
00:06:02,50 --> 00:06:04,90
So everything works like it did before

121
00:06:04,90 --> 00:06:07,70
but now we're prioritizing other things

122
00:06:07,70 --> 00:06:09,90
before making this ajax request.

123
00:06:09,90 --> 00:06:11,90
And that's a pretty standard practice.

124
00:06:11,90 --> 00:06:14,10
Now for the bonus, I want to randomize

125
00:06:14,10 --> 00:06:18,20
which park has its info displayed.

126
00:06:18,20 --> 00:06:23,80
And so that's up here in the parkUpadateUISuccess function.

127
00:06:23,80 --> 00:06:29,60
And so, I'm going to create a random number.

128
00:06:29,60 --> 00:06:33,80
So I'm going to do const number.

129
00:06:33,80 --> 00:06:35,40
So I want to get the length of the array,

130
00:06:35,40 --> 00:06:39,70
so parseData.data.length,

131
00:06:39,70 --> 00:06:42,70
and I want to multiply that by a random number,

132
00:06:42,70 --> 00:06:45,90
so I can do math.random

133
00:06:45,90 --> 00:06:48,60
times parseData.data.length.

134
00:06:48,60 --> 00:06:52,70
And so that's going to give me a random number

135
00:06:52,70 --> 00:06:57,10
that is between zero and the length of the array.

136
00:06:57,10 --> 00:06:59,00
But, it's not necessarily going to be an integer

137
00:06:59,00 --> 00:07:00,70
and in fact, it probably won't.

138
00:07:00,70 --> 00:07:05,20
So I'm going to stick all of that in parense

139
00:07:05,20 --> 00:07:10,60
and I'm going to use that as an argument for Math.floor.

140
00:07:10,60 --> 00:07:15,20
And that rounds down to the next lowest integer.

141
00:07:15,20 --> 00:07:18,20
And so that neatly takes

142
00:07:18,20 --> 00:07:20,40
numbers based on the length of the array

143
00:07:20,40 --> 00:07:23,00
and rounds them to numbers that correspond

144
00:07:23,00 --> 00:07:24,50
to the index values,

145
00:07:24,50 --> 00:07:26,70
which start at zero rather than one.

146
00:07:26,70 --> 00:07:29,00
So now I have that random number

147
00:07:29,00 --> 00:07:31,40
and so then everywhere I've hard-coded as zero,

148
00:07:31,40 --> 00:07:35,00
I just need to replace that with number.

149
00:07:35,00 --> 00:07:37,30
And so that every time this function runs,

150
00:07:37,30 --> 00:07:40,10
I have a new number

151
00:07:40,10 --> 00:07:44,70
and a new choice of data from the results.

152
00:07:44,70 --> 00:07:46,30
So I've saved that,

153
00:07:46,30 --> 00:07:47,70
I'm going to go back to the browser,

154
00:07:47,70 --> 00:07:49,20
now I've got Muir Woods instead.

155
00:07:49,20 --> 00:07:50,90
And so every time I load,

156
00:07:50,90 --> 00:07:55,00
I should get a different park displayed here as well.

