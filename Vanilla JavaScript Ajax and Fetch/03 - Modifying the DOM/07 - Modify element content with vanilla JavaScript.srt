1
00:00:00,50 --> 00:00:03,20
- [Narrator] Selecting form elements is no different

2
00:00:03,20 --> 00:00:05,90
from selecting other DOM elements.

3
00:00:05,90 --> 00:00:09,10
You can use querySelector(), querySelectorAll(),

4
00:00:09,10 --> 00:00:11,70
or any of the older, more specific methods

5
00:00:11,70 --> 00:00:14,20
to select any element in the DOM.

6
00:00:14,20 --> 00:00:15,90
But getting or setting content

7
00:00:15,90 --> 00:00:17,60
from non-form elements

8
00:00:17,60 --> 00:00:20,10
involves a different set of properties

9
00:00:20,10 --> 00:00:21,60
than the value property

10
00:00:21,60 --> 00:00:24,10
that you use with form elements.

11
00:00:24,10 --> 00:00:27,80
Both textContent and innerHTML allow you

12
00:00:27,80 --> 00:00:30,60
to get and set text content.

13
00:00:30,60 --> 00:00:34,00
It's best to use textContent when you only need text

14
00:00:34,00 --> 00:00:36,30
but if you're providing a string of HTML

15
00:00:36,30 --> 00:00:38,10
to set an element's value

16
00:00:38,10 --> 00:00:41,10
you may find innerHTML useful instead.

17
00:00:41,10 --> 00:00:43,30
You can also reference any attribute

18
00:00:43,30 --> 00:00:46,00
of an element using dot notation.

19
00:00:46,00 --> 00:00:49,10
For instance, you use .src

20
00:00:49,10 --> 00:00:51,90
for the source attribute of an image tag

21
00:00:51,90 --> 00:00:55,00
The one exception is the class attribute

22
00:00:55,00 --> 00:00:58,00
which requires you to use class name instead

23
00:00:58,00 --> 00:00:59,70
to avoid confusion

24
00:00:59,70 --> 00:01:02,80
with the JavaScript class key word.

25
00:01:02,80 --> 00:01:04,30
In some design systems,

26
00:01:04,30 --> 00:01:05,60
it's common for an element

27
00:01:05,60 --> 00:01:07,90
to have multiple class names assigned.

28
00:01:07,90 --> 00:01:09,40
So, it can be more convenient

29
00:01:09,40 --> 00:01:11,50
to use the classList property,

30
00:01:11,50 --> 00:01:13,50
and it's add and remove methods,

31
00:01:13,50 --> 00:01:15,00
to manipulate which classes

32
00:01:15,00 --> 00:01:16,90
are assigned to an element.

33
00:01:16,90 --> 00:01:19,40
For our national park service request,

34
00:01:19,40 --> 00:01:20,80
all of the data is going to go

35
00:01:20,80 --> 00:01:24,40
in the div element with the ID specials.

36
00:01:24,40 --> 00:01:27,60
So, searching on specials,

37
00:01:27,60 --> 00:01:30,20
second hit here is the div.

38
00:01:30,20 --> 00:01:34,20
So we can see that, in addition to an h1,

39
00:01:34,20 --> 00:01:36,70
the specials div contains an h2

40
00:01:36,70 --> 00:01:41,40
with a nested img tag, a nested a tag,

41
00:01:41,40 --> 00:01:45,20
and below that we have a paragraph element.

42
00:01:45,20 --> 00:01:50,00
So, the a tag is going to contain the name of the park,

43
00:01:50,00 --> 00:01:52,20
the p tag is going to contain

44
00:01:52,20 --> 00:01:53,80
the description of the park.

45
00:01:53,80 --> 00:01:56,30
Notice that the specials element

46
00:01:56,30 --> 00:01:59,10
has a class value of hidden.

47
00:01:59,10 --> 00:02:03,10
This is defined in our CSS to display none.

48
00:02:03,10 --> 00:02:05,50
So, once we add content to this section,

49
00:02:05,50 --> 00:02:07,70
we'll want to remove that class name.

50
00:02:07,70 --> 00:02:09,10
Now, first off, I want to add

51
00:02:09,10 --> 00:02:12,30
in the logo in this image element.

52
00:02:12,30 --> 00:02:14,70
The API response doesn't include any sort

53
00:02:14,70 --> 00:02:16,80
of per park image thumbnail,

54
00:02:16,80 --> 00:02:18,50
so instead I'm going to use

55
00:02:18,50 --> 00:02:21,60
the National Park Service logo for all of them.

56
00:02:21,60 --> 00:02:23,80
So, I'm going to start by changing the source

57
00:02:23,80 --> 00:02:26,60
of the image element to the URL,

58
00:02:26,60 --> 00:02:29,60
and this way if we never get to the success handler,

59
00:02:29,60 --> 00:02:32,60
that is to say, if we never get data back on parks,

60
00:02:32,60 --> 00:02:35,40
then this image will never be requested or loaded,

61
00:02:35,40 --> 00:02:37,60
which means we save some bandwidth.

62
00:02:37,60 --> 00:02:40,00
And so, in my JavaScript, the first thing I need

63
00:02:40,00 --> 00:02:42,60
is a reference to that element.

64
00:02:42,60 --> 00:02:44,90
So, down here underneath zipField,

65
00:02:44,90 --> 00:02:48,90
I'm going to add another const for parkThumb,

66
00:02:48,90 --> 00:02:55,00
and that's going to be document.querySelector(),

67
00:02:55,00 --> 00:02:59,70
and that's going to be quote, hash specials,

68
00:02:59,70 --> 00:03:02,90
space, h2, space, img.

69
00:03:02,90 --> 00:03:07,70
And then, in the parkUpdateUISuccess function,

70
00:03:07,70 --> 00:03:10,10
I want to reference parkThumb,

71
00:03:10,10 --> 00:03:12,00
and then I can use .src

72
00:03:12,00 --> 00:03:14,70
to reference the source attribute.

73
00:03:14,70 --> 00:03:16,30
And then, I need to set it

74
00:03:16,30 --> 00:03:19,20
to a very specific URL, as a string,

75
00:03:19,20 --> 00:03:29,30
https://www.nps.gov/common/commonspot/templates/

76
00:03:29,30 --> 00:03:38,00
assetsCT/images/branding, finally /logo.png.

77
00:03:38,00 --> 00:03:40,20
Now, the other thing I need to do is,

78
00:03:40,20 --> 00:03:43,10
unhide the section containing the part content.

79
00:03:43,10 --> 00:03:46,70
So, up in my set of references,

80
00:03:46,70 --> 00:03:51,10
I'm going to create one more constant called parkSection,

81
00:03:51,10 --> 00:03:56,80
and I'm going to reference document.querySelector(),

82
00:03:56,80 --> 00:04:01,10
and that's going to be quote #specials.

83
00:04:01,10 --> 00:04:02,50
So, this is the entire section

84
00:04:02,50 --> 00:04:04,90
which is currently hidden.

85
00:04:04,90 --> 00:04:06,60
Need to fix my source here

86
00:04:06,60 --> 00:04:11,10
and take out that final quote, add a semicolon,

87
00:04:11,10 --> 00:04:14,80
and then after the code that changes that image source,

88
00:04:14,80 --> 00:04:17,80
I'm going to use my parkSection reference,

89
00:04:17,80 --> 00:04:22,20
going to use .classList, and the .remove() method

90
00:04:22,20 --> 00:04:26,20
of the classList to remove the hidden class

91
00:04:26,20 --> 00:04:27,50
from that element.

92
00:04:27,50 --> 00:04:30,90
So, classList is a collection of all the class values,

93
00:04:30,90 --> 00:04:34,00
and the remove method lets me specify a class name

94
00:04:34,00 --> 00:04:36,30
to be removed from that list.

95
00:04:36,30 --> 00:04:38,30
And by removing the hidden class,

96
00:04:38,30 --> 00:04:41,20
I'll be making the section visible once again.

97
00:04:41,20 --> 00:04:46,10
So, saving that, switching to my HTML,

98
00:04:46,10 --> 00:04:48,30
and going live with that.

99
00:04:48,30 --> 00:04:51,70
And opening my console, so I have an error,

100
00:04:51,70 --> 00:04:54,80
and switching back to my code that's in line 24,

101
00:04:54,80 --> 00:04:56,00
parkThumb is undefined.

102
00:04:56,00 --> 00:04:57,60
And if I look in parkThumb, I noticed

103
00:04:57,60 --> 00:05:00,30
I used special here, specials there,

104
00:05:00,30 --> 00:05:02,70
this needs to be specials with an s,

105
00:05:02,70 --> 00:05:05,70
so, just little references like that that are off,

106
00:05:05,70 --> 00:05:07,00
mean that I'm referencing something

107
00:05:07,00 --> 00:05:08,20
that doesn't exist in the DOM,

108
00:05:08,20 --> 00:05:10,10
and I'm not going to get anywhere with that.

109
00:05:10,10 --> 00:05:13,40
So, back to my browser, I got my URL wrong,

110
00:05:13,40 --> 00:05:19,00
I can see right away it's assets, plural, CT.

111
00:05:19,00 --> 00:05:22,60
And then back in my browser, I got my data back.

112
00:05:22,60 --> 00:05:27,10
And I both got my image added to the web page

113
00:05:27,10 --> 00:05:29,00
and I got that whole specials section

114
00:05:29,00 --> 00:05:32,00
displayed by removing the hidden class.

115
00:05:32,00 --> 00:05:33,90
Now, the position of this image is odd,

116
00:05:33,90 --> 00:05:34,80
but that's just because

117
00:05:34,80 --> 00:05:36,80
it's missing the adjacent content.

118
00:05:36,80 --> 00:05:39,20
So, once we finish this up it's going to look fine.

119
00:05:39,20 --> 00:05:41,30
So, again, there's a lot of similarity

120
00:05:41,30 --> 00:05:44,40
in DOM manipulation between forms and other elements,

121
00:05:44,40 --> 00:05:46,60
but those are the basic properties and methods

122
00:05:46,60 --> 00:05:49,00
that are going to get you the most mileage.

