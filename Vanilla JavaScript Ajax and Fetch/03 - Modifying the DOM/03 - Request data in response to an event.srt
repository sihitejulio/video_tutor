1
00:00:00,50 --> 00:00:02,70
- [Instructor] Where DOM manipulation really shines

2
00:00:02,70 --> 00:00:06,60
is when your code changes the view in response to an event

3
00:00:06,60 --> 00:00:09,40
whether that's a user event or a browser event.

4
00:00:09,40 --> 00:00:12,80
My plan for this form is to respond to a user leaving

5
00:00:12,80 --> 00:00:16,20
either the address, city or state field.

6
00:00:16,20 --> 00:00:18,90
Leaving a field triggers the blur event

7
00:00:18,90 --> 00:00:21,40
and when a user leaves one of those fields,

8
00:00:21,40 --> 00:00:24,90
my code is going to check if all three of them have content.

9
00:00:24,90 --> 00:00:26,20
And if they do,

10
00:00:26,20 --> 00:00:29,60
then I want to fire off a request to SmartyStreets,

11
00:00:29,60 --> 00:00:32,00
sending those three pieces of info

12
00:00:32,00 --> 00:00:34,00
and getting back data on that address

13
00:00:34,00 --> 00:00:36,70
including the nine-digit zip code.

14
00:00:36,70 --> 00:00:39,10
So the first thing I'm going to do

15
00:00:39,10 --> 00:00:42,10
is to define a function called checkCompletion

16
00:00:42,10 --> 00:00:46,60
that I'll call each time a user leaves one of those fields.

17
00:00:46,60 --> 00:00:50,30
So down here underneath the createRequest function,

18
00:00:50,30 --> 00:00:58,50
I'm going to say const checkCompletion.

19
00:00:58,50 --> 00:01:00,90
I can check the value of a form field

20
00:01:00,90 --> 00:01:04,80
using the value property of the referenced element.

21
00:01:04,80 --> 00:01:10,90
So I'm going to check if addressField.value

22
00:01:10,90 --> 00:01:15,30
and I want to see if that is anything other than empty.

23
00:01:15,30 --> 00:01:19,70
So I'm going to use bang equal equal

24
00:01:19,70 --> 00:01:22,50
and empty quotes,

25
00:01:22,50 --> 00:01:25,40
so as long as that value is not equal to

26
00:01:25,40 --> 00:01:29,00
an empty string or falsey,

27
00:01:29,00 --> 00:01:31,50
but I also want to check whether city field

28
00:01:31,50 --> 00:01:34,40
and state field are also empty.

29
00:01:34,40 --> 00:01:39,40
So I'm going to use an and logical operator here

30
00:01:39,40 --> 00:01:43,80
and on a new line cityField.value

31
00:01:43,80 --> 00:01:47,80
bang equal equal empty string

32
00:01:47,80 --> 00:01:54,60
and stateField.value bang equal equal empty string.

33
00:01:54,60 --> 00:01:58,00
So if all three of those conditions are true,

34
00:01:58,00 --> 00:01:59,50
that is to say if there is data

35
00:01:59,50 --> 00:02:01,50
in all three of those fields,

36
00:02:01,50 --> 00:02:05,40
then I actually want to build out a request to SmartyStreets

37
00:02:05,40 --> 00:02:08,90
using the information that the user entered.

38
00:02:08,90 --> 00:02:12,50
Now scrolling up to my smartyURL,

39
00:02:12,50 --> 00:02:14,30
I actually need to break this up

40
00:02:14,30 --> 00:02:18,00
because this is a URL that has hard coded

41
00:02:18,00 --> 00:02:21,00
street, city and state information

42
00:02:21,00 --> 00:02:22,80
but it gives me the basic structure

43
00:02:22,80 --> 00:02:25,00
for what that URL should look like.

44
00:02:25,00 --> 00:02:30,70
So I'm just going to duplicate my smartyURL.

45
00:02:30,70 --> 00:02:32,70
I'm going to comment out the original.

46
00:02:32,70 --> 00:02:36,10
So I have it there for reference in case I make a mistake.

47
00:02:36,10 --> 00:02:38,60
And then in the new one,

48
00:02:38,60 --> 00:02:41,90
I want to cut everything after the candidates value.

49
00:02:41,90 --> 00:02:44,70
So starting with the ampersand before street

50
00:02:44,70 --> 00:02:46,70
going all the way through MA

51
00:02:46,70 --> 00:02:48,50
and I'm just going to delete that.

52
00:02:48,50 --> 00:02:52,10
So this is my base URL for this request.

53
00:02:52,10 --> 00:02:55,70
And then back to my checkCompletion function,

54
00:02:55,70 --> 00:02:59,20
I'll create a local request URL variable

55
00:02:59,20 --> 00:03:03,80
that concatenates smartyURL with the values that we got.

56
00:03:03,80 --> 00:03:06,30
So I'm going to say

57
00:03:06,30 --> 00:03:14,40
const requestURL equals smartyURL

58
00:03:14,40 --> 00:03:19,60
plus the string ampersand street equals,

59
00:03:19,60 --> 00:03:21,70
which is the key that we use in the URL

60
00:03:21,70 --> 00:03:23,80
to reference the street value,

61
00:03:23,80 --> 00:03:27,00
plus addressField.value,

62
00:03:27,00 --> 00:03:29,50
just the value that the user entered in the form,

63
00:03:29,50 --> 00:03:34,20
plus the string ampersand city equal

64
00:03:34,20 --> 00:03:38,60
plus cityField.value

65
00:03:38,60 --> 00:03:44,60
plus the string ampersand state equals

66
00:03:44,60 --> 00:03:48,80
plus stateField.value.

67
00:03:48,80 --> 00:03:51,00
And to make this a little more readable,

68
00:03:51,00 --> 00:03:54,90
I'm going to break this up over multiple lines and indent.

69
00:03:54,90 --> 00:03:58,40
So we've got our three key value pairs

70
00:03:58,40 --> 00:04:00,90
each on a separate line.

71
00:04:00,90 --> 00:04:04,00
I'm going to put a semicolon at the end of my const statement.

72
00:04:04,00 --> 00:04:06,30
And then finally after that const,

73
00:04:06,30 --> 00:04:08,90
I just want to call createRequest

74
00:04:08,90 --> 00:04:14,00
and pass it this requestURL that I just created.

