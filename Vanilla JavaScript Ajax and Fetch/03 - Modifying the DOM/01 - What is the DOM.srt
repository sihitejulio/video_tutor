1
00:00:00,10 --> 00:00:03,00
- [Educator] The Document Object Model or DOM

2
00:00:03,00 --> 00:00:05,40
is the way that a browser or other user agent

3
00:00:05,40 --> 00:00:08,00
represents the contents of a web document.

4
00:00:08,00 --> 00:00:10,80
The Dom serves as the structure by which developers

5
00:00:10,80 --> 00:00:12,60
can reference and manipulate elements

6
00:00:12,60 --> 00:00:15,20
in the browser window using JavaScript.

7
00:00:15,20 --> 00:00:18,10
The elements in the DOM are structured in a hierarchy.

8
00:00:18,10 --> 00:00:20,30
It can be useful to think of the structure of the DOM

9
00:00:20,30 --> 00:00:23,70
as a tree with the elements higher in the tree

10
00:00:23,70 --> 00:00:25,60
containing the elements lower down.

11
00:00:25,60 --> 00:00:28,80
Browser developer tools provide an interface for viewing

12
00:00:28,80 --> 00:00:32,00
and interacting with the DOM for the current document.

13
00:00:32,00 --> 00:00:35,10
In the browser window, right-click an item.

14
00:00:35,10 --> 00:00:37,40
On the menu that opens, you'll see an option

15
00:00:37,40 --> 00:00:41,80
like Inspect in Chrome or Inspect Element in Firefox.

16
00:00:41,80 --> 00:00:44,70
When you click that item, the developer tools open

17
00:00:44,70 --> 00:00:48,30
and display the Elements pane which shows the DOM tree

18
00:00:48,30 --> 00:00:49,80
for the current document.

19
00:00:49,80 --> 00:00:52,40
The contents of the Elements pane

20
00:00:52,40 --> 00:00:55,70
look a lot like a standard HTML document

21
00:00:55,70 --> 00:00:59,80
with nested elements attributes and even content.

22
00:00:59,80 --> 00:01:02,70
However, it's important to understand that the DOM

23
00:01:02,70 --> 00:01:05,40
is not the same as the HTML file

24
00:01:05,40 --> 00:01:07,10
that a webpage is based on.

25
00:01:07,10 --> 00:01:10,30
We can use the developer tools to explore the difference.

26
00:01:10,30 --> 00:01:13,90
So here on duckduckgo.com which is a search engine,

27
00:01:13,90 --> 00:01:16,70
I'm inspecting the search box itself,

28
00:01:16,70 --> 00:01:19,30
and the Elements pane shows the code for the input box

29
00:01:19,30 --> 00:01:21,80
I right clicked with a highlight over it

30
00:01:21,80 --> 00:01:26,60
and I can see that it's nested in form element

31
00:01:26,60 --> 00:01:28,50
which is nested in a div.

32
00:01:28,50 --> 00:01:31,20
And if I kept scrolling, I could explore the whole tree.

33
00:01:31,20 --> 00:01:33,50
So keep your eye on the form tag.

34
00:01:33,50 --> 00:01:37,60
And as I move my mouse over the text box,

35
00:01:37,60 --> 00:01:40,90
notice that an additional class name is added

36
00:01:40,90 --> 00:01:42,50
to the form tag.

37
00:01:42,50 --> 00:01:48,00
So we have search dash dash adv and search dash dash focus.

38
00:01:48,00 --> 00:01:49,50
And if I go over it again,

39
00:01:49,50 --> 00:01:52,10
now we also have search dash dash hover

40
00:01:52,10 --> 00:01:54,50
which is indicating that my mouse pointer

41
00:01:54,50 --> 00:01:58,40
is hovering over that input box, over that form.

42
00:01:58,40 --> 00:02:01,20
So changing class names is a pretty common way

43
00:02:01,20 --> 00:02:03,00
to change styles in response

44
00:02:03,00 --> 00:02:06,40
to user interactions using JavaScript.

45
00:02:06,40 --> 00:02:10,00
And so we can see that the code in the DOM tree

46
00:02:10,00 --> 00:02:12,90
is a reflection of exactly what's in the browser window

47
00:02:12,90 --> 00:02:16,30
at current moment and it doesn't necessarily map

48
00:02:16,30 --> 00:02:19,20
to the original HTML that was loaded.

49
00:02:19,20 --> 00:02:23,00
The Elements tab in the browser tools also gives us access

50
00:02:23,00 --> 00:02:26,20
to the CSS that's applied to an element.

51
00:02:26,20 --> 00:02:29,60
So I still have that Input element selected

52
00:02:29,60 --> 00:02:33,00
and I can scroll through the Styles Tab down here

53
00:02:33,00 --> 00:02:36,50
and it shows me each style rule from the CSS

54
00:02:36,50 --> 00:02:40,00
that applies to this element.

55
00:02:40,00 --> 00:02:43,10
And it shows every declaration from those rules

56
00:02:43,10 --> 00:02:45,10
and the declarations that have been superseded

57
00:02:45,10 --> 00:02:48,40
or aren't recognized are crossed out.

58
00:02:48,40 --> 00:02:51,20
So this is a super useful tool for understanding

59
00:02:51,20 --> 00:02:56,00
the current state of a web document in the browser window.

60
00:02:56,00 --> 00:02:59,20
The DOM is especially important when working with Ajax

61
00:02:59,20 --> 00:03:02,00
because we don't want to just receive remote data,

62
00:03:02,00 --> 00:03:03,80
we also want to use that data

63
00:03:03,80 --> 00:03:06,30
as part of the users interaction.

64
00:03:06,30 --> 00:03:08,50
And that often means changing what's shown

65
00:03:08,50 --> 00:03:10,10
in the browser window.

66
00:03:10,10 --> 00:03:13,80
So once we've received data back from an AJAX request,

67
00:03:13,80 --> 00:03:15,90
we'll use Dom properties and methods

68
00:03:15,90 --> 00:03:19,00
to change the contents of the browser window itself.

