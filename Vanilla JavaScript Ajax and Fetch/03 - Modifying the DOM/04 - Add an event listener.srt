1
00:00:00,50 --> 00:00:01,70
- [Instructor] Now I have a function

2
00:00:01,70 --> 00:00:04,40
that will actually check if the address field,

3
00:00:04,40 --> 00:00:07,10
city field and state field are all filled,

4
00:00:07,10 --> 00:00:09,30
and then submit my request.

5
00:00:09,30 --> 00:00:12,30
So the next thing I need to do to finish this up

6
00:00:12,30 --> 00:00:14,70
is to make sure that that function is called

7
00:00:14,70 --> 00:00:19,50
in response to the blur event on each of these three fields.

8
00:00:19,50 --> 00:00:22,50
Now, I want to call my check completion function

9
00:00:22,50 --> 00:00:26,50
in response to the blur event on three different elements.

10
00:00:26,50 --> 00:00:28,20
To do that with JavaScript

11
00:00:28,20 --> 00:00:30,30
I use the add event listener method,

12
00:00:30,30 --> 00:00:34,60
which takes two arguments, an event type as a string,

13
00:00:34,60 --> 00:00:38,80
and either a function name or an inline anonymous function.

14
00:00:38,80 --> 00:00:41,00
So first I want to add an event listener

15
00:00:41,00 --> 00:00:45,80
to the element I referenced with the address field variable.

16
00:00:45,80 --> 00:00:49,70
So addressField.addEventListener,

17
00:00:49,70 --> 00:00:53,30
and I'll pass the event name blur as a string,

18
00:00:53,30 --> 00:00:57,00
and then specify checkCompletion

19
00:00:57,00 --> 00:00:58,30
as the function.

20
00:00:58,30 --> 00:01:01,20
And I want to do the same thing for the other two fields,

21
00:01:01,20 --> 00:01:04,10
because if a user changes info in any of these fields,

22
00:01:04,10 --> 00:01:06,10
the query needs to be resubmitted.

23
00:01:06,10 --> 00:01:07,10
So

24
00:01:07,10 --> 00:01:09,00
I can do

25
00:01:09,00 --> 00:01:12,90
cityField.addEventListener,

26
00:01:12,90 --> 00:01:14,70
blur,

27
00:01:14,70 --> 00:01:18,10
checkCompletion,

28
00:01:18,10 --> 00:01:22,00
and stateField.addEventListener,

29
00:01:22,00 --> 00:01:23,50
blur,

30
00:01:23,50 --> 00:01:25,00
checkCompletion.

31
00:01:25,00 --> 00:01:27,60
Now I'm going to save my work.

32
00:01:27,60 --> 00:01:32,50
Go back to the HTML file and start up my live server.

33
00:01:32,50 --> 00:01:35,80
And in my form, I'm going to enter an address,

34
00:01:35,80 --> 00:01:40,80
and we'll just say 1600 Pennsylvania Avenue.

35
00:01:40,80 --> 00:01:43,70
And city, Washington,

36
00:01:43,70 --> 00:01:45,10
state, DC.

37
00:01:45,10 --> 00:01:48,70
Now, when I tab out, that is the blur event.

38
00:01:48,70 --> 00:01:53,60
And now I'm going to open up my console.

39
00:01:53,60 --> 00:01:56,90
And up here I have my park information

40
00:01:56,90 --> 00:01:58,60
coming in from my other request.

41
00:01:58,60 --> 00:02:01,90
And down here I can see for 1600 Pennsylvania Avenue,

42
00:02:01,90 --> 00:02:04,90
I have data coming back from Smarty Streets.

43
00:02:04,90 --> 00:02:09,10
So I've written code to access data from the DOM,

44
00:02:09,10 --> 00:02:12,00
and then use that to create an Ajax request.

