1
00:00:00,40 --> 00:00:06,50
(techno upbeat music)

2
00:00:06,50 --> 00:00:08,30
- It's time for another challenge.

3
00:00:08,30 --> 00:00:11,70
Ready to try your hand at adding content to the DOM?

4
00:00:11,70 --> 00:00:15,10
To complete this challenge, write code that adds information

5
00:00:15,10 --> 00:00:18,30
for the first park in the return data to the DOM.

6
00:00:18,30 --> 00:00:20,80
You'll add the full park name as the content

7
00:00:20,80 --> 00:00:23,50
of the a element in the special section.

8
00:00:23,50 --> 00:00:27,00
You'll add the park url as its href value.

9
00:00:27,00 --> 00:00:28,70
Then you'll show the park description

10
00:00:28,70 --> 00:00:31,30
as the content of the p element.

11
00:00:31,30 --> 00:00:33,70
Update the function call to respond

12
00:00:33,70 --> 00:00:37,90
to the DOMContentLoaded event on the Window object.

13
00:00:37,90 --> 00:00:41,10
For an extra challenge, update your code to display data

14
00:00:41,10 --> 00:00:43,40
for a random park in the results

15
00:00:43,40 --> 00:00:46,40
rather than always displaying the first one.

16
00:00:46,40 --> 00:00:48,90
This challenge might take you around 20 minutes.

17
00:00:48,90 --> 00:00:50,80
If you don't have time for that right now,

18
00:00:50,80 --> 00:00:52,80
feel free to come back to it later on.

19
00:00:52,80 --> 00:00:55,40
When you're done, join me in the next video

20
00:00:55,40 --> 00:00:58,00
and I'll go over how I approached it.

