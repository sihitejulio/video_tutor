1
00:00:00,50 --> 00:00:01,90
- [Instructor] Before you can make changes

2
00:00:01,90 --> 00:00:03,50
to an element in the DOM,

3
00:00:03,50 --> 00:00:05,70
you first have to select it.

4
00:00:05,70 --> 00:00:08,00
The DOM interface supported by browsers

5
00:00:08,00 --> 00:00:11,60
includes several methods for selecting one or more elements,

6
00:00:11,60 --> 00:00:14,10
but a few methods are particularly common.

7
00:00:14,10 --> 00:00:17,80
querySelector takes a CSS selector as an argument

8
00:00:17,80 --> 00:00:19,60
and selects a single element.

9
00:00:19,60 --> 00:00:22,50
querySelectorAll is similar to querySelector

10
00:00:22,50 --> 00:00:26,20
except that if your CSS selector matches multiple elements,

11
00:00:26,20 --> 00:00:29,10
all of them are returned as a collection.

12
00:00:29,10 --> 00:00:33,10
There are also older methods such as getElementById,

13
00:00:33,10 --> 00:00:37,10
which selects the element with the ID value you pass.

14
00:00:37,10 --> 00:00:40,10
In general, querySelector and querySelectorAll

15
00:00:40,10 --> 00:00:41,50
are more versatile,

16
00:00:41,50 --> 00:00:45,50
but you may see getElementById in legacy code.

17
00:00:45,50 --> 00:00:47,90
Looking at our Explore California form,

18
00:00:47,90 --> 00:00:51,60
remember our first goal is to collect address, city,

19
00:00:51,60 --> 00:00:53,00
and state info,

20
00:00:53,00 --> 00:00:56,30
and then use that to retrieve and display zip info.

21
00:00:56,30 --> 00:00:57,50
So we'll need to reference

22
00:00:57,50 --> 00:01:00,30
those four form fields in the DOM.

23
00:01:00,30 --> 00:01:02,70
Now, when you use the method like querySelector,

24
00:01:02,70 --> 00:01:05,10
the browser needs to do some work to search the DOM,

25
00:01:05,10 --> 00:01:07,50
which is a tiny hit on performance.

26
00:01:07,50 --> 00:01:10,60
But if we know we want to reference an element more than once

27
00:01:10,60 --> 00:01:12,10
while our app is running,

28
00:01:12,10 --> 00:01:14,20
we want to save a reference to the element

29
00:01:14,20 --> 00:01:16,50
so we're only looking it up once.

30
00:01:16,50 --> 00:01:19,60
So in the HTML code for the contact form,

31
00:01:19,60 --> 00:01:23,00
I'll start by searching for address.

32
00:01:23,00 --> 00:01:26,00
And it takes me to the code for the address field

33
00:01:26,00 --> 00:01:30,40
with the city, state, and zip just below.

34
00:01:30,40 --> 00:01:32,10
And the easiest way to get references

35
00:01:32,10 --> 00:01:34,80
to these input elements will be with their ID values,

36
00:01:34,80 --> 00:01:36,30
and each one has one,

37
00:01:36,30 --> 00:01:40,00
address, city, state, and zip.

38
00:01:40,00 --> 00:01:42,60
So back in my JavaScript file,

39
00:01:42,60 --> 00:01:47,20
I'm going to create new constants just below my URL variables.

40
00:01:47,20 --> 00:01:51,30
So const addressField.

41
00:01:51,30 --> 00:01:52,80
Now, the querySelector method

42
00:01:52,80 --> 00:01:55,10
is a method of the document object,

43
00:01:55,10 --> 00:02:01,20
so to use it, we have to say document.querySelector.

44
00:02:01,20 --> 00:02:02,40
And then in the parens,

45
00:02:02,40 --> 00:02:05,60
I pass the CSS selector as a string.

46
00:02:05,60 --> 00:02:12,90
So that's going to be a quote, hash for an ID, address.

47
00:02:12,90 --> 00:02:15,40
So that gives me a reference the input box

48
00:02:15,40 --> 00:02:17,50
where users will type the address.

49
00:02:17,50 --> 00:02:20,60
And likewise, const cityField

50
00:02:20,60 --> 00:02:28,10
equals document.querySelector hash city.

51
00:02:28,10 --> 00:02:29,40
And the same for state.

52
00:02:29,40 --> 00:02:32,40
At this point, I could even just copy that,

53
00:02:32,40 --> 00:02:36,40
paste it, change city to state,

54
00:02:36,40 --> 00:02:38,60
and change city to state.

55
00:02:38,60 --> 00:02:40,20
Now sometimes we have legacy code

56
00:02:40,20 --> 00:02:44,10
that uses a library like jQuery to select elements.

57
00:02:44,10 --> 00:02:46,50
Back in the day, jQuery was a lot easier

58
00:02:46,50 --> 00:02:49,30
than using DOM manipulation with the older methods.

59
00:02:49,30 --> 00:02:50,10
But these days,

60
00:02:50,10 --> 00:02:52,90
if you're only using jQuery for DOM manipulation,

61
00:02:52,90 --> 00:02:54,40
then it's pretty straightforward

62
00:02:54,40 --> 00:02:56,80
to convert everything to vanilla JavaScript

63
00:02:56,80 --> 00:02:58,90
and remove jQuery as a dependency.

64
00:02:58,90 --> 00:03:00,90
So let's first write a jQuery statement

65
00:03:00,90 --> 00:03:02,50
to select the zip field.

66
00:03:02,50 --> 00:03:07,60
So const dollar zipField equals

67
00:03:07,60 --> 00:03:10,50
dollar paren quote hash zip.

68
00:03:10,50 --> 00:03:12,40
Notice it's really similar.

69
00:03:12,40 --> 00:03:14,90
All we're doing is replacing the dollar sign

70
00:03:14,90 --> 00:03:17,20
with document.querySelector

71
00:03:17,20 --> 00:03:20,30
and leaving out the dollar sign in the variable name.

72
00:03:20,30 --> 00:03:21,90
So I'll comment this out

73
00:03:21,90 --> 00:03:23,60
and leave it there for reference,

74
00:03:23,60 --> 00:03:26,60
and then I'll rewrite it using vanilla JavaScript.

75
00:03:26,60 --> 00:03:33,70
So const zipField equals document.querySelector

76
00:03:33,70 --> 00:03:37,10
paren quote hash zip,

77
00:03:37,10 --> 00:03:38,80
and now we have references saved

78
00:03:38,80 --> 00:03:41,00
for all four of the DOM elements

79
00:03:41,00 --> 00:03:43,00
that we'll be working with in the form.

