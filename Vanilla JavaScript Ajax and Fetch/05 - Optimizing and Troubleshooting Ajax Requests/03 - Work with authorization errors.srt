1
00:00:00,50 --> 00:00:02,60
- [Instructor] When errors happen in an API request,

2
00:00:02,60 --> 00:00:06,50
sometimes half the battle is understanding what went wrong.

3
00:00:06,50 --> 00:00:08,50
Often the error code and message returned

4
00:00:08,50 --> 00:00:10,20
from a failed request is enough

5
00:00:10,20 --> 00:00:12,10
to point you in the right direction.

6
00:00:12,10 --> 00:00:14,90
But once you have a general idea of the issue,

7
00:00:14,90 --> 00:00:17,90
it's also important to understand how to fix it.

8
00:00:17,90 --> 00:00:19,70
One common error you might encounter

9
00:00:19,70 --> 00:00:21,50
is an Authorization Error.

10
00:00:21,50 --> 00:00:25,20
Which corresponds with a 401 HTTP code.

11
00:00:25,20 --> 00:00:27,30
When you request results in this error,

12
00:00:27,30 --> 00:00:30,90
it generally has to do with your API key.

13
00:00:30,90 --> 00:00:34,00
A few things can go wrong with an API key.

14
00:00:34,00 --> 00:00:36,10
The first place to check is to ensure

15
00:00:36,10 --> 00:00:37,30
that the key in your code

16
00:00:37,30 --> 00:00:40,50
matches the credential issued by the web service.

17
00:00:40,50 --> 00:00:43,70
A copy and paste error or even erroneously typing

18
00:00:43,70 --> 00:00:46,60
or deleting a single character in the wrong place

19
00:00:46,60 --> 00:00:48,10
can cause issues here.

20
00:00:48,10 --> 00:00:50,90
If the key and your code matches the provided string,

21
00:00:50,90 --> 00:00:54,50
another thing to check is that you're using the correct key.

22
00:00:54,50 --> 00:00:56,90
Some providers such as SmartyStreets

23
00:00:56,90 --> 00:00:59,20
provide multiple authentication strings

24
00:00:59,20 --> 00:01:01,50
and it's up to you to read the documentation

25
00:01:01,50 --> 00:01:04,90
and understand which needs to be used in which scenario.

26
00:01:04,90 --> 00:01:06,90
Ensure you're using the right string.

27
00:01:06,90 --> 00:01:08,40
Also check that you've completed

28
00:01:08,40 --> 00:01:10,50
all necessary configuration.

29
00:01:10,50 --> 00:01:13,20
For instance, SmartyStreets requires you

30
00:01:13,20 --> 00:01:15,80
to associate an app address with your key.

31
00:01:15,80 --> 00:01:19,20
Even using the correct key in your app will result

32
00:01:19,20 --> 00:01:22,10
in a 401 error unless you also associate it

33
00:01:22,10 --> 00:01:24,20
with your address.

34
00:01:24,20 --> 00:01:26,30
Right now my code's working really well.

35
00:01:26,30 --> 00:01:28,80
But let's go ahead and introduce an error

36
00:01:28,80 --> 00:01:32,00
and see what that looks like in the console.

37
00:01:32,00 --> 00:01:35,80
So I'm going to start with an Authentication Error here.

38
00:01:35,80 --> 00:01:40,40
Again, I'm going to copy and paste my Smarty URL

39
00:01:40,40 --> 00:01:44,40
and I'm going to take out a couple digits of that auth ID.

40
00:01:44,40 --> 00:01:50,40
Saving that then starting up my HTML file.

41
00:01:50,40 --> 00:01:53,30
Opening up my console.

42
00:01:53,30 --> 00:01:59,10
And let me just switch to a separate window there.

43
00:01:59,10 --> 00:02:05,30
And now I'm going to run a zip code request.

44
00:02:05,30 --> 00:02:08,90
And so in my console I can see that the error I got

45
00:02:08,90 --> 00:02:10,50
out of my fetch is a 401.

46
00:02:10,50 --> 00:02:12,70
The error I logged is a 401.

47
00:02:12,70 --> 00:02:16,80
And I know that's because I went in and broke that API key.

48
00:02:16,80 --> 00:02:19,40
So again, getting that API key right,

49
00:02:19,40 --> 00:02:21,30
that auth ID, whatever it's called,

50
00:02:21,30 --> 00:02:24,80
is crucial to make sure that your requests work.

51
00:02:24,80 --> 00:02:30,00
And I'm just going to fix my code.

52
00:02:30,00 --> 00:02:34,40
Uncomment the correct URL.

53
00:02:34,40 --> 00:02:38,10
Go back to my page and try one more time

54
00:02:38,10 --> 00:02:40,50
to make a request.

55
00:02:40,50 --> 00:02:42,80
And everything's fixed now so I'm good.

56
00:02:42,80 --> 00:02:47,20
And so making sure that your API key is correct.

57
00:02:47,20 --> 00:02:49,90
Making sure that it matches the one you've been provided.

58
00:02:49,90 --> 00:02:52,30
And making sure that it's correctly configured

59
00:02:52,30 --> 00:02:56,00
are all steps you can take to work with a 401 error.

