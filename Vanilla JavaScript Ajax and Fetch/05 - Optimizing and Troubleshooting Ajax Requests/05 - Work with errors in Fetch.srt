1
00:00:00,50 --> 00:00:02,60
- [Instructor] Fetch is both easier to write

2
00:00:02,60 --> 00:00:06,40
and easier for other developers to read than XHR.

3
00:00:06,40 --> 00:00:09,70
However, because the underlying promises pattern

4
00:00:09,70 --> 00:00:12,10
is a little different than the way JavaScript code

5
00:00:12,10 --> 00:00:14,10
has traditionally been written,

6
00:00:14,10 --> 00:00:17,00
Fetch has a few gotchas that can result in errors,

7
00:00:17,00 --> 00:00:19,70
especially when you're using Fetch for the first time.

8
00:00:19,70 --> 00:00:23,20
It's super common to try to access the data too soon.

9
00:00:23,20 --> 00:00:25,60
Remember that the first .then method

10
00:00:25,60 --> 00:00:27,40
needs to return the payload,

11
00:00:27,40 --> 00:00:29,30
and then the second .then method

12
00:00:29,30 --> 00:00:31,10
can work with the data.

13
00:00:31,10 --> 00:00:32,70
The architecture of Fetch

14
00:00:32,70 --> 00:00:34,80
keeps the data off limits to your code

15
00:00:34,80 --> 00:00:37,20
in the first .then method.

16
00:00:37,20 --> 00:00:39,30
Another place where Fetch can trip you up

17
00:00:39,30 --> 00:00:41,80
is forgotten return statements.

18
00:00:41,80 --> 00:00:45,20
Every chained .then method needs to use return

19
00:00:45,20 --> 00:00:48,50
in order to pass data to the next method.

20
00:00:48,50 --> 00:00:51,70
You can instead manipulate variables in the parent scope,

21
00:00:51,70 --> 00:00:53,10
but it's better practice to simply

22
00:00:53,10 --> 00:00:55,30
pass the data along the chain.

23
00:00:55,30 --> 00:00:57,20
Finally, sometimes you'll see

24
00:00:57,20 --> 00:00:59,30
no result from a Fetch request,

25
00:00:59,30 --> 00:01:02,40
including no errors logged in the consol,

26
00:01:02,40 --> 00:01:04,90
even if you've included a .catch method.

27
00:01:04,90 --> 00:01:07,90
Remember that Fetch considers a request to have failed,

28
00:01:07,90 --> 00:01:10,30
only if there's something like a network error

29
00:01:10,30 --> 00:01:12,40
that prevents it from being sent,

30
00:01:12,40 --> 00:01:14,70
or a response from being received.

31
00:01:14,70 --> 00:01:16,40
In all other cases,

32
00:01:16,40 --> 00:01:17,90
it's important for you to provide

33
00:01:17,90 --> 00:01:20,60
the error handling logic yourself.

34
00:01:20,60 --> 00:01:23,30
Otherwise, responses other than 200

35
00:01:23,30 --> 00:01:25,80
may result in no data to be rendered,

36
00:01:25,80 --> 00:01:27,90
but will not throw an error.

37
00:01:27,90 --> 00:01:29,80
Let's check this out ourselves.

38
00:01:29,80 --> 00:01:32,30
I'm here in the create request function

39
00:01:32,30 --> 00:01:34,10
and I'm going to start

40
00:01:34,10 --> 00:01:37,00
by just commenting out this first then.

41
00:01:37,00 --> 00:01:38,60
This is the first then

42
00:01:38,60 --> 00:01:40,80
that takes that response object,

43
00:01:40,80 --> 00:01:43,20
parses out the response .json

44
00:01:43,20 --> 00:01:47,40
and passes that along to the next .then.

45
00:01:47,40 --> 00:01:48,80
Let's just check out what happens

46
00:01:48,80 --> 00:01:50,20
if we skip that step

47
00:01:50,20 --> 00:01:52,30
and try to parse that response object

48
00:01:52,30 --> 00:01:55,30
with the succeed handler.

49
00:01:55,30 --> 00:01:58,10
Going live in my browser,

50
00:01:58,10 --> 00:02:01,00
and I'm opening up my developer tools.

51
00:02:01,00 --> 00:02:03,40
I have a response object logged here

52
00:02:03,40 --> 00:02:05,80
in my success handler, and again,

53
00:02:05,80 --> 00:02:07,80
when I look at the body property,

54
00:02:07,80 --> 00:02:09,40
it says invoke property getter,

55
00:02:09,40 --> 00:02:11,50
which is the browser's way of reminding me

56
00:02:11,50 --> 00:02:13,90
I need to use response .json

57
00:02:13,90 --> 00:02:16,40
in order to get that data out.

58
00:02:16,40 --> 00:02:20,80
I've got information about that actual response,

59
00:02:20,80 --> 00:02:23,70
but then when I try and use it in my success code,

60
00:02:23,70 --> 00:02:24,70
it's just telling me

61
00:02:24,70 --> 00:02:28,00
that it doesn't actually have access to the data.

62
00:02:28,00 --> 00:02:32,10
Going back to that file, I will uncomment that.

63
00:02:32,10 --> 00:02:37,20
That .then method is calling the handle errors function

64
00:02:37,20 --> 00:02:39,60
and passing along the response object.

65
00:02:39,60 --> 00:02:42,10
What happens if I take out

66
00:02:42,10 --> 00:02:44,50
the error handling code here?

67
00:02:44,50 --> 00:02:48,00
We're simply just returning response .json.

68
00:02:48,00 --> 00:02:51,10
Then, if I go up to my parks URL

69
00:02:51,10 --> 00:02:54,20
and I mess something up.

70
00:02:54,20 --> 00:02:57,80
I'm going to make a copy of that,

71
00:02:57,80 --> 00:02:59,30
comment out the original,

72
00:02:59,30 --> 00:03:01,30
and then we'll try something like

73
00:03:01,30 --> 00:03:03,30
taking out the question mark,

74
00:03:03,30 --> 00:03:05,20
which should result in the service

75
00:03:05,20 --> 00:03:06,70
not recognizing that we've actually

76
00:03:06,70 --> 00:03:09,40
sent along an API key.

77
00:03:09,40 --> 00:03:12,90
Checking back in the browser, I get a 403 error,

78
00:03:12,90 --> 00:03:15,60
but notice that I get an error object logged

79
00:03:15,60 --> 00:03:18,20
in my success handler,

80
00:03:18,20 --> 00:03:19,60
and my success handler

81
00:03:19,60 --> 00:03:22,30
is trying to read the length property.

82
00:03:22,30 --> 00:03:25,10
Even though the browser

83
00:03:25,10 --> 00:03:28,70
has gotten an error message from the remote server,

84
00:03:28,70 --> 00:03:31,50
my code has not actually thrown an error,

85
00:03:31,50 --> 00:03:35,10
and the Fetch method has actually continued

86
00:03:35,10 --> 00:03:38,00
to send that data along the chain.

87
00:03:38,00 --> 00:03:40,40
Leaving that error in my URL,

88
00:03:40,40 --> 00:03:42,90
I'm going to scroll back down

89
00:03:42,90 --> 00:03:45,30
to the handle errors function

90
00:03:45,30 --> 00:03:46,60
and I'm going to put

91
00:03:46,60 --> 00:03:49,20
that error handling content back in.

92
00:03:49,20 --> 00:03:51,10
Again, I'm just checking to see

93
00:03:51,10 --> 00:03:53,50
if there's an okay in that response,

94
00:03:53,50 --> 00:03:54,60
and that'll only happen

95
00:03:54,60 --> 00:03:57,50
if I have a response in the 200 range.

96
00:03:57,50 --> 00:04:00,70
With that saved, going back to my browser,

97
00:04:00,70 --> 00:04:02,40
and now, I've still got that error

98
00:04:02,40 --> 00:04:04,10
being thrown by the browser

99
00:04:04,10 --> 00:04:07,50
and I've also got my code recognizing there's an error

100
00:04:07,50 --> 00:04:09,10
and throwing that exception,

101
00:04:09,10 --> 00:04:10,60
having that exception handled

102
00:04:10,60 --> 00:04:14,10
and having the success callback skipped.

103
00:04:14,10 --> 00:04:17,00
That's exactly what I want my error handling code to do,

104
00:04:17,00 --> 00:04:18,50
and that's exactly why we need

105
00:04:18,50 --> 00:04:21,20
to write our own error handling code.

106
00:04:21,20 --> 00:04:23,40
Finally, I'm going to go back to my URL,

107
00:04:23,40 --> 00:04:26,00
take out the one that I broke,

108
00:04:26,00 --> 00:04:28,00
uncomment the good one

109
00:04:28,00 --> 00:04:29,90
and then go back to the browser

110
00:04:29,90 --> 00:04:31,70
and make sure,

111
00:04:31,70 --> 00:04:33,40
there's the data.

112
00:04:33,40 --> 00:04:35,50
There's the DOM manipulation.

113
00:04:35,50 --> 00:04:37,50
I have everything back to a working state,

114
00:04:37,50 --> 00:04:40,50
and now I have my Fetch request set up

115
00:04:40,50 --> 00:04:42,90
in such a way that I can accommodate

116
00:04:42,90 --> 00:04:44,00
issues that come up.

