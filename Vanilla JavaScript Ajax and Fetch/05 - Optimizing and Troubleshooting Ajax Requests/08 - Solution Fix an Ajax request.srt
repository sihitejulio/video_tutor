1
00:00:00,10 --> 00:00:05,90
(upbeat music)

2
00:00:05,90 --> 00:00:08,40
- [Narrator] So the API requests in this file

3
00:00:08,40 --> 00:00:09,40
for Smarty Streets is broken.

4
00:00:09,40 --> 00:00:15,00
Now, the first thing we need to do is update the API Key.

5
00:00:15,00 --> 00:00:18,20
So again, because this was all in a single URL,

6
00:00:18,20 --> 00:00:20,30
I broke that out into a variable.

7
00:00:20,30 --> 00:00:23,30
Created this smartyAPIKey variable that's going to store

8
00:00:23,30 --> 00:00:24,90
the API Key.

9
00:00:24,90 --> 00:00:28,10
So you want to replace that value, I went ahead and copied

10
00:00:28,10 --> 00:00:31,20
my API KEY, and I'm just going to paste it in there.

11
00:00:31,20 --> 00:00:34,10
And so now, that API Key is concatenated

12
00:00:34,10 --> 00:00:37,60
with the rest of that URL, to create my request URL.

13
00:00:37,60 --> 00:00:41,40
So, I'm going to save that, I'm going to go to my HTML file

14
00:00:41,40 --> 00:00:44,80
and we'll just start by going live and checking out

15
00:00:44,80 --> 00:00:46,50
what things look like.

16
00:00:46,50 --> 00:00:50,60
So, I'm going to run a test query

17
00:00:50,60 --> 00:00:56,20
on Smarty Streets,

18
00:00:56,20 --> 00:00:57,60
and in my console.

19
00:00:57,60 --> 00:01:01,70
So, right here in the console I have an error,

20
00:01:01,70 --> 00:01:06,80
and it's specifically an error that says name not resolved.

21
00:01:06,80 --> 00:01:09,80
Now if I look over in the Network tab,

22
00:01:09,80 --> 00:01:16,90
I go back and I reload my page, and I try this again,

23
00:01:16,90 --> 00:01:21,30
I have a failed request,

24
00:01:21,30 --> 00:01:24,30
and I can see there's a problem with the auth id.

25
00:01:24,30 --> 00:01:28,40
But I'm not even getting an error message here,

26
00:01:28,40 --> 00:01:29,20
so that tells me

27
00:01:29,20 --> 00:01:31,60
that there's something going on a little more upstream.

28
00:01:31,60 --> 00:01:35,10
And this name not resolved suggests that there's an issue

29
00:01:35,10 --> 00:01:37,90
with the endpoint itself.

30
00:01:37,90 --> 00:01:40,50
So, going back to my code,

31
00:01:40,50 --> 00:01:44,50
I want to check out the URL here.

32
00:01:44,50 --> 00:01:46,90
And so normally what I would do in a case like this

33
00:01:46,90 --> 00:01:50,90
is actually go in and look at the documentation, maybe even

34
00:01:50,90 --> 00:01:54,50
copy that URL from the documentation.

35
00:01:54,50 --> 00:01:57,50
One thing I can take advantage of here is that we have

36
00:01:57,50 --> 00:01:59,70
our init variable which gives these headers,

37
00:01:59,70 --> 00:02:02,20
that Smarty Streets wants us to use.

38
00:02:02,20 --> 00:02:04,50
And we have a Host value here,

39
00:02:04,50 --> 00:02:06,20
that shows the host,

40
00:02:06,20 --> 00:02:08,10
the endpoint that we're supposed to be hitting.

41
00:02:08,10 --> 00:02:12,70
So the endpoint is us-street.api.smartystreets.com.

42
00:02:12,70 --> 00:02:15,50
Up here, we've got us-streets, plural,

43
00:02:15,50 --> 00:02:18,30
.api.smartystreets.com.

44
00:02:18,30 --> 00:02:20,00
So this is the sort of thing that can happen,

45
00:02:20,00 --> 00:02:22,10
if you're typing something from memory,

46
00:02:22,10 --> 00:02:24,50
or if you're just getting ahead of yourself,

47
00:02:24,50 --> 00:02:26,50
confusing things while you're typing.

48
00:02:26,50 --> 00:02:29,10
So I'm going to go in here and take this s out,

49
00:02:29,10 --> 00:02:31,80
so that it matches the host value down here.

50
00:02:31,80 --> 00:02:33,20
I'm going to save that,

51
00:02:33,20 --> 00:02:35,20
go back to my browser,

52
00:02:35,20 --> 00:02:42,70
and I'm going to try another request.

53
00:02:42,70 --> 00:02:44,00
And in the console,

54
00:02:44,00 --> 00:02:47,80
this time I do not get an issue with the URL itself,

55
00:02:47,80 --> 00:02:51,30
I get a 401 error which means that my request was actually

56
00:02:51,30 --> 00:02:54,90
submitted to a service, and got a response.

57
00:02:54,90 --> 00:02:56,40
So, that's progress.

58
00:02:56,40 --> 00:02:59,00
Now I'm going to go over to the Network tab again,

59
00:02:59,00 --> 00:03:00,80
and notice I've got my two requests,

60
00:03:00,80 --> 00:03:02,40
the first one went through fine.

61
00:03:02,40 --> 00:03:05,50
We always have that two requests with Smarty Streets.

62
00:03:05,50 --> 00:03:07,70
The second gave me a 401.

63
00:03:07,70 --> 00:03:10,80
If I look at my response it says unauthorized.

64
00:03:10,80 --> 00:03:14,30
So that's the text that's corresponding with this 401.

65
00:03:14,30 --> 00:03:16,30
And so again scrolling down,

66
00:03:16,30 --> 00:03:18,90
I can look at those query string parameters.

67
00:03:18,90 --> 00:03:21,70
And just looking at those here I can see an issue.

68
00:03:21,70 --> 00:03:24,00
Cause I've got my state, my city, my street,

69
00:03:24,00 --> 00:03:25,70
just like I entered in the form.

70
00:03:25,70 --> 00:03:29,70
But my auth-id has candidates equals 10 stuck at the end,

71
00:03:29,70 --> 00:03:34,00
and that's clearly, not part of my auth-id.

72
00:03:34,00 --> 00:03:35,80
So, back in my code,

73
00:03:35,80 --> 00:03:39,50
I have this question mark auth-id equals then the value.

74
00:03:39,50 --> 00:03:43,30
And after each value, before the next key,

75
00:03:43,30 --> 00:03:46,20
I'm supposed to have an ampersand.

76
00:03:46,20 --> 00:03:49,20
So, I'm going to stick that back into my query string,

77
00:03:49,20 --> 00:03:51,80
go back to my browser,

78
00:03:51,80 --> 00:03:55,40
run my query again,

79
00:03:55,40 --> 00:03:56,70
and there's my zip code.

80
00:03:56,70 --> 00:03:59,90
If I switch over and look at my console,

81
00:03:59,90 --> 00:04:01,20
everything looks great.

82
00:04:01,20 --> 00:04:03,70
Got my park API request log there,

83
00:04:03,70 --> 00:04:05,30
and I've got a status of 200

84
00:04:05,30 --> 00:04:10,20
on both of my responses from Smarty Streets.

85
00:04:10,20 --> 00:04:16,40
So, again when we're working with errors in an API request,

86
00:04:16,40 --> 00:04:19,10
they can come from a whole bunch of different directions.

87
00:04:19,10 --> 00:04:22,20
And it's really important to just start with one issue,

88
00:04:22,20 --> 00:04:23,20
chase it down,

89
00:04:23,20 --> 00:04:24,40
go on to the next,

90
00:04:24,40 --> 00:04:26,50
and then finally get everything ironed out,

91
00:04:26,50 --> 00:04:29,00
and get your API request working.

