1
00:00:00,50 --> 00:00:03,00
- [Narrator] As you're first building out your request code,

2
00:00:03,00 --> 00:00:05,50
it's not uncommon to receive errors due

3
00:00:05,50 --> 00:00:07,30
to malformed requests.

4
00:00:07,30 --> 00:00:08,60
This type of error results

5
00:00:08,60 --> 00:00:11,40
from an error in either the data you're sending,

6
00:00:11,40 --> 00:00:13,80
or the syntax of your request.

7
00:00:13,80 --> 00:00:16,00
A few common issues are often at the root

8
00:00:16,00 --> 00:00:17,80
of malformed requests.

9
00:00:17,80 --> 00:00:20,60
One of these is simply an invalid endpoint.

10
00:00:20,60 --> 00:00:24,60
That can include forgetting HTTP or HTTPS at the start

11
00:00:24,60 --> 00:00:28,50
of the URL, or not providing the full path for the endpoint,

12
00:00:28,50 --> 00:00:30,60
or even using an outdated endpoint

13
00:00:30,60 --> 00:00:32,10
for which support has ended.

14
00:00:32,10 --> 00:00:34,80
Another place to look is in the separator characters

15
00:00:34,80 --> 00:00:37,00
in the query section of the URL.

16
00:00:37,00 --> 00:00:38,80
Forgetting the question mark at the start

17
00:00:38,80 --> 00:00:41,50
of the query string, or omitting the ampersand

18
00:00:41,50 --> 00:00:45,50
between key value pairs, or the equal sign separating a key

19
00:00:45,50 --> 00:00:49,10
from a value can all result in un-parsable data,

20
00:00:49,10 --> 00:00:51,80
or in data that's parsed by the endpoint

21
00:00:51,80 --> 00:00:54,10
in a different way than you intended.

22
00:00:54,10 --> 00:00:57,00
Finally it's important to check that the keys

23
00:00:57,00 --> 00:00:59,80
and values you're sending in the query string map

24
00:00:59,80 --> 00:01:01,80
to what's expected by the endpoint.

25
00:01:01,80 --> 00:01:04,40
That may be using an incorrect key,

26
00:01:04,40 --> 00:01:08,00
for instance address one rather than street one,

27
00:01:08,00 --> 00:01:10,80
or sending data that hasn't been parsed correctly

28
00:01:10,80 --> 00:01:13,40
before being added to the query string.

29
00:01:13,40 --> 00:01:15,90
The developer tools can be particularly useful

30
00:01:15,90 --> 00:01:17,90
in troubleshooting this type of error.

31
00:01:17,90 --> 00:01:20,70
The network tab lets you view the malformed request,

32
00:01:20,70 --> 00:01:23,00
including the parsed query parameters.

33
00:01:23,00 --> 00:01:25,50
So let's test this out with our code

34
00:01:25,50 --> 00:01:31,10
I'm going to go to the parks URL and save a good copy.

35
00:01:31,10 --> 00:01:33,90
And comment...

36
00:01:33,90 --> 00:01:36,90
And then I'm going to start by taking out the question mark

37
00:01:36,90 --> 00:01:41,60
that separates the endpoint from the first parameter,

38
00:01:41,60 --> 00:01:42,80
which is stateCode.

39
00:01:42,80 --> 00:01:44,60
So I'm going to save that change,

40
00:01:44,60 --> 00:01:47,50
and go live with my page in the browser.

41
00:01:47,50 --> 00:01:49,50
And then in my developer tools,

42
00:01:49,50 --> 00:01:52,00
I've got a 403 forbidden error,

43
00:01:52,00 --> 00:01:54,60
and so lets dig in on that a little bit.

44
00:01:54,60 --> 00:01:57,70
We want to go to the network tab, I'm going to move my tools

45
00:01:57,70 --> 00:02:01,90
to the bottom of the screen, to make them easier to view.

46
00:02:01,90 --> 00:02:04,80
And I'm going to go ahead and reload this page,

47
00:02:04,80 --> 00:02:08,10
to get all of these requests logged.

48
00:02:08,10 --> 00:02:12,20
So, in red, I see this request that didn't work, 403.

49
00:02:12,20 --> 00:02:15,70
And that looks like my national parks service request,

50
00:02:15,70 --> 00:02:19,30
so I'm going to click that, there's the results.

51
00:02:19,30 --> 00:02:22,70
Here are the response headers, the request headers,

52
00:02:22,70 --> 00:02:24,20
and I don't actually have any

53
00:02:24,20 --> 00:02:26,50
parsed query parameters down here.

54
00:02:26,50 --> 00:02:28,50
I look at the preview, there's an error code

55
00:02:28,50 --> 00:02:30,70
that says the API key is missing.

56
00:02:30,70 --> 00:02:32,40
I can dig in a little bit on that,

57
00:02:32,40 --> 00:02:34,30
and it says it doesn't have an API key.

58
00:02:34,30 --> 00:02:37,30
So going back to my code,

59
00:02:37,30 --> 00:02:39,00
that can be a little hard to determine what's up

60
00:02:39,00 --> 00:02:40,90
if I didn't know what I'd done.

61
00:02:40,90 --> 00:02:43,20
Cause clearly I did provide the API key,

62
00:02:43,20 --> 00:02:45,60
but because I never actually ended,

63
00:02:45,60 --> 00:02:48,10
never actually made a distinction between the endpoint

64
00:02:48,10 --> 00:02:50,30
and the query string, it thinks this is all part

65
00:02:50,30 --> 00:02:52,40
of the end point, and that I haven't actually provided

66
00:02:52,40 --> 00:02:54,10
that query parameter.

67
00:02:54,10 --> 00:02:56,00
So putting that question mark back in

68
00:02:56,00 --> 00:02:58,00
is going to solve that issue.

69
00:02:58,00 --> 00:03:00,80
Now what happens if I take the equals sign out?

70
00:03:00,80 --> 00:03:05,70
Now instead of specifying a value for the state code key,

71
00:03:05,70 --> 00:03:08,70
and saying that I want values for California,

72
00:03:08,70 --> 00:03:10,40
it's going to get this blob of text

73
00:03:10,40 --> 00:03:12,40
so let's see how it deals with that.

74
00:03:12,40 --> 00:03:15,60
So I'm going to save that, back in my browser,

75
00:03:15,60 --> 00:03:18,40
I do have a park showing up.

76
00:03:18,40 --> 00:03:22,80
So let's look at our parks request here,

77
00:03:22,80 --> 00:03:24,60
which is the bottom of my list,

78
00:03:24,60 --> 00:03:27,80
here I can see already that missing equals sign.

79
00:03:27,80 --> 00:03:33,80
And so, I got a bunch more parks returned than I usually do.

80
00:03:33,80 --> 00:03:36,00
This is in the preview tab.

81
00:03:36,00 --> 00:03:39,40
If I go back to headers, everything is okay.

82
00:03:39,40 --> 00:03:42,60
If I scroll to the bottom, my query string parameters.

83
00:03:42,60 --> 00:03:46,40
I have a key called stateCodeca with no value, rather than

84
00:03:46,40 --> 00:03:49,00
stateCode key with a value of ca.

85
00:03:49,00 --> 00:03:52,00
And so it looks like the API has just ignored

86
00:03:52,00 --> 00:03:54,20
that all together, and is giving me information

87
00:03:54,20 --> 00:03:58,30
on every park there is, which is not what I was looking for.

88
00:03:58,30 --> 00:04:00,20
And so sticking that equals sign back

89
00:04:00,20 --> 00:04:01,90
in there is going to fix that.

90
00:04:01,90 --> 00:04:03,80
Finally let's see what happens if I leave out

91
00:04:03,80 --> 00:04:05,20
that ampersand.

92
00:04:05,20 --> 00:04:10,30
So I'll save that change, go back to the browser,

93
00:04:10,30 --> 00:04:12,20
and it looks like I have a 403 error again

94
00:04:12,20 --> 00:04:16,50
at the bottom of my list, so I'm going to take a look at that.

95
00:04:16,50 --> 00:04:20,60
I have a forbidden, and I have a single query string here

96
00:04:20,60 --> 00:04:24,30
stateCode, with this really long value with the API key.

97
00:04:24,30 --> 00:04:26,90
My guess is that the response is going to say

98
00:04:26,90 --> 00:04:29,70
I don't actually have an API key.

99
00:04:29,70 --> 00:04:31,80
Right, because it wasn't provided in a way

100
00:04:31,80 --> 00:04:33,60
that was parsable.

101
00:04:33,60 --> 00:04:36,00
So all of these characters are super important,

102
00:04:36,00 --> 00:04:38,80
and if you miss just one, it can cause all sorts

103
00:04:38,80 --> 00:04:41,10
of havoc with the data you get back.

104
00:04:41,10 --> 00:04:43,30
And so I want to make sure that I get my code back

105
00:04:43,30 --> 00:04:44,40
to a working state.

106
00:04:44,40 --> 00:04:49,50
So I'm going to delete that parks URL that I was messing up.

107
00:04:49,50 --> 00:04:51,60
Going to un-comment the good one that I saved,

108
00:04:51,60 --> 00:04:55,10
and I'm going to save those changes, then back in my browser

109
00:04:55,10 --> 00:04:59,40
I do get park back, and when I look at the data,

110
00:04:59,40 --> 00:05:02,80
I get 33 responses, so, looks like we're all good here,

111
00:05:02,80 --> 00:05:04,30
and the code's working again.

112
00:05:04,30 --> 00:05:06,20
So one of the first places to look

113
00:05:06,20 --> 00:05:07,20
when you're getting an error

114
00:05:07,20 --> 00:05:10,70
when you're first constructing your API request is in

115
00:05:10,70 --> 00:05:13,10
that URL that you're providing to make sure

116
00:05:13,10 --> 00:05:15,00
that everything is formatted correctly.

