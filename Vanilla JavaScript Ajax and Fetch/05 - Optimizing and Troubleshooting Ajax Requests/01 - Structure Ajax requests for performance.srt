1
00:00:00,50 --> 00:00:01,90
- [Narrator] No matter whether you're building

2
00:00:01,90 --> 00:00:05,20
a native XHR object or using Fetch,

3
00:00:05,20 --> 00:00:07,60
the way you structure an Ajax request

4
00:00:07,60 --> 00:00:09,70
impacts its performance.

5
00:00:09,70 --> 00:00:11,70
There are a few important considerations

6
00:00:11,70 --> 00:00:14,30
when creating a request that can help ensure

7
00:00:14,30 --> 00:00:17,10
that your app is as responsive as possible.

8
00:00:17,10 --> 00:00:19,70
First off, you should aim to minimize the number

9
00:00:19,70 --> 00:00:22,20
of Ajax requests in your app.

10
00:00:22,20 --> 00:00:23,90
While it may be tempting to limit yourself

11
00:00:23,90 --> 00:00:26,00
to retrieving only the data you need

12
00:00:26,00 --> 00:00:28,10
in a particular part of your code,

13
00:00:28,10 --> 00:00:29,70
it's important to balance that

14
00:00:29,70 --> 00:00:33,50
against the performance hit you take for each Ajax request.

15
00:00:33,50 --> 00:00:35,30
When you know you'll need additional data

16
00:00:35,30 --> 00:00:38,90
from the same source, or even if it's very likely,

17
00:00:38,90 --> 00:00:40,60
then you should go ahead and get everything

18
00:00:40,60 --> 00:00:43,70
in a single request rather than repeating requests

19
00:00:43,70 --> 00:00:45,10
for smaller bites of data.

20
00:00:45,10 --> 00:00:48,70
Second, GET requests are faster that POST requests.

21
00:00:48,70 --> 00:00:51,30
When you have a choice, use GET.

22
00:00:51,30 --> 00:00:54,30
Each of these verbs has its own specific use case:

23
00:00:54,30 --> 00:00:58,20
GET for retrieving data, and POST for creating a resource.

24
00:00:58,20 --> 00:01:00,80
You may find that you're hitting a single endpoint

25
00:01:00,80 --> 00:01:03,50
using the same method for different requests

26
00:01:03,50 --> 00:01:06,00
even when you're simply retrieving data.

27
00:01:06,00 --> 00:01:08,80
You can improve the performance of your Ajax requests

28
00:01:08,80 --> 00:01:11,50
by identifying these unneeded POST requests

29
00:01:11,50 --> 00:01:13,50
and recoding them as GET.

30
00:01:13,50 --> 00:01:15,50
Finally, you can reduce the impact

31
00:01:15,50 --> 00:01:17,50
of your Ajax requests on performance

32
00:01:17,50 --> 00:01:20,00
by reducing the size of the data you're sending

33
00:01:20,00 --> 00:01:21,40
and receiving.

34
00:01:21,40 --> 00:01:24,80
One way to do this is to use the leanest data format

35
00:01:24,80 --> 00:01:27,40
that your app and your endpoint can deal with.

36
00:01:27,40 --> 00:01:31,00
Obviously, plain text is free of structural characters

37
00:01:31,00 --> 00:01:32,70
and using plain text enables you

38
00:01:32,70 --> 00:01:35,10
to minimize the size of the data stream.

39
00:01:35,10 --> 00:01:37,40
However, most requests and responses

40
00:01:37,40 --> 00:01:40,00
need some sort of structured data.

41
00:01:40,00 --> 00:01:44,60
Among popular formats in use, JSon has the lowest overhead.

42
00:01:44,60 --> 00:01:48,00
XML has much higher overhead because of the high ratio

43
00:01:48,00 --> 00:01:51,60
of structuring characters, tags, to data.

44
00:01:51,60 --> 00:01:55,20
Of course, if XML is required for a specific endpoint

45
00:01:55,20 --> 00:01:58,30
or application, there's not much you can do.

46
00:01:58,30 --> 00:02:00,30
If a data source offers a choice,

47
00:02:00,30 --> 00:02:03,60
JSon is highly preferable over XML.

48
00:02:03,60 --> 00:02:05,70
If your Ajax requests use XML

49
00:02:05,70 --> 00:02:08,20
because your app is written to accept it,

50
00:02:08,20 --> 00:02:11,00
it may be useful to perform a cost-benefit analysis

51
00:02:11,00 --> 00:02:13,60
of rewriting your app to accept a data format

52
00:02:13,60 --> 00:02:15,10
with lower overhead.

53
00:02:15,10 --> 00:02:16,90
Another way to reduce data size

54
00:02:16,90 --> 00:02:19,90
is to make sure you're hitting the right endpoint.

55
00:02:19,90 --> 00:02:22,30
If you're requesting user profile information,

56
00:02:22,30 --> 00:02:24,20
for instance, but the endpoint you're hitting

57
00:02:24,20 --> 00:02:26,40
returns full user information,

58
00:02:26,40 --> 00:02:29,00
including an array of recent activity,

59
00:02:29,00 --> 00:02:31,10
it's worth looking for a different endpoint

60
00:02:31,10 --> 00:02:34,20
that returns a narrower slice containing less data

61
00:02:34,20 --> 00:02:36,20
that you don't need.

62
00:02:36,20 --> 00:02:38,30
App development is a constant tug of war

63
00:02:38,30 --> 00:02:41,50
between adding features and increasing performance.

64
00:02:41,50 --> 00:02:43,20
Following these performance guidelines

65
00:02:43,20 --> 00:02:45,60
in your Ajax requests can help keep them

66
00:02:45,60 --> 00:02:47,30
from adding bloat to your apps

67
00:02:47,30 --> 00:02:49,00
and keep your apps responsive.

