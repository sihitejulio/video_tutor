1
00:00:00,00 --> 00:00:05,80
(peppy music)

2
00:00:05,80 --> 00:00:07,50
- [Instructor] It's time for a challenge.

3
00:00:07,50 --> 00:00:10,80
Ready to try your hand at debugging an Ajax request?

4
00:00:10,80 --> 00:00:14,00
For this challenge, you need to use the begin folder

5
00:00:14,00 --> 00:00:17,80
for this video, which contains a broken API request

6
00:00:17,80 --> 00:00:19,70
for SmartyStreets.

7
00:00:19,70 --> 00:00:22,50
You'll need to troubleshoot and fix the errors.

8
00:00:22,50 --> 00:00:25,00
I've separated out the API key portion

9
00:00:25,00 --> 00:00:28,80
of the URL into its own variable to make it easier

10
00:00:28,80 --> 00:00:31,10
for you to swap in your own key.

11
00:00:31,10 --> 00:00:33,80
Be sure to update this value with your own key,

12
00:00:33,80 --> 00:00:36,00
or the request won't work.

13
00:00:36,00 --> 00:00:38,80
You'll know you're done when the zip code functionality

14
00:00:38,80 --> 00:00:41,10
in the form works.

15
00:00:41,10 --> 00:00:44,00
This challenge could take around 10 minutes.

16
00:00:44,00 --> 00:00:47,00
When you're done, join me in the next video,

17
00:00:47,00 --> 00:00:49,00
and I'll go over how I approached it.

