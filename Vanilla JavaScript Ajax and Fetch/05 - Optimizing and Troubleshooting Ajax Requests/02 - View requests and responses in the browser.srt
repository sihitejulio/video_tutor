1
00:00:00,50 --> 00:00:02,60
- [Instructor] When you're working with Ajax requests,

2
00:00:02,60 --> 00:00:04,00
it can be useful to examine

3
00:00:04,00 --> 00:00:07,30
the actual HTTP request and response headers

4
00:00:07,30 --> 00:00:10,30
that are being sent when your code is executed.

5
00:00:10,30 --> 00:00:12,90
Modern browser developer tools let you record

6
00:00:12,90 --> 00:00:14,70
and review network activity

7
00:00:14,70 --> 00:00:19,10
including each HTTP request for the current page.

8
00:00:19,10 --> 00:00:23,30
Both Chrome and Firefox dev tools include a network tab.

9
00:00:23,30 --> 00:00:27,40
I'm using Chrome so I'll open the dev tools

10
00:00:27,40 --> 00:00:30,80
and then I'm going to click network

11
00:00:30,80 --> 00:00:34,20
and notice that this list is empty.

12
00:00:34,20 --> 00:00:35,30
Browsers want to avoid

13
00:00:35,30 --> 00:00:37,80
doing a bunch of work in the background

14
00:00:37,80 --> 00:00:39,80
and they want to avoid saving information

15
00:00:39,80 --> 00:00:42,70
that takes up memory but that may never be viewed.

16
00:00:42,70 --> 00:00:46,00
So dev tools generally don't log network requests

17
00:00:46,00 --> 00:00:48,70
until you open the tab.

18
00:00:48,70 --> 00:00:54,20
With the tab open, I can go back and reload the page

19
00:00:54,20 --> 00:00:57,70
and now I see all the requests logged for this page

20
00:00:57,70 --> 00:01:02,50
including the CSS and JS files, fonts, images,

21
00:01:02,50 --> 00:01:05,80
and down here is my Ajax request for parks info.

22
00:01:05,80 --> 00:01:07,70
So on my screen this is third from the bottom,

23
00:01:07,70 --> 00:01:11,20
starts with the word parks followed by a question mark.

24
00:01:11,20 --> 00:01:13,20
And when I hover over a request,

25
00:01:13,20 --> 00:01:16,00
I see any additional details.

26
00:01:16,00 --> 00:01:19,10
If it's cut off, I see the full path.

27
00:01:19,10 --> 00:01:21,80
And I can click any request in the name column

28
00:01:21,80 --> 00:01:23,40
to see the details.

29
00:01:23,40 --> 00:01:27,20
So I'll click that National Parks URL in the list

30
00:01:27,20 --> 00:01:30,00
and on the headers tab I have a summary

31
00:01:30,00 --> 00:01:32,60
which tells me the URL that I hit

32
00:01:32,60 --> 00:01:34,80
and the result which is a 200.

33
00:01:34,80 --> 00:01:37,80
Below that is a list of response headers.

34
00:01:37,80 --> 00:01:41,20
These are the key value pairs that my browser received

35
00:01:41,20 --> 00:01:44,40
from the National Parks Service web service.

36
00:01:44,40 --> 00:01:50,00
And then further down, I have a list of request headers.

37
00:01:50,00 --> 00:01:52,10
This is the data that my browser sent

38
00:01:52,10 --> 00:01:54,40
to the National Parks Service web service

39
00:01:54,40 --> 00:01:56,60
when making a request.

40
00:01:56,60 --> 00:01:57,60
At the very bottom,

41
00:01:57,60 --> 00:02:00,30
I have a summary of my query string parameters

42
00:02:00,30 --> 00:02:03,00
which are the key value pairs I encoded into the URL

43
00:02:03,00 --> 00:02:04,40
for the request I made.

44
00:02:04,40 --> 00:02:08,00
So I've got my API key and I've got the state code.

45
00:02:08,00 --> 00:02:10,50
On the preview tab, I can view the response body

46
00:02:10,50 --> 00:02:13,30
which is the data I received back in the response,

47
00:02:13,30 --> 00:02:14,90
a parsed version.

48
00:02:14,90 --> 00:02:16,10
And this is the data

49
00:02:16,10 --> 00:02:18,60
that I've already been working with in my code.

50
00:02:18,60 --> 00:02:19,80
But if I was having a problem

51
00:02:19,80 --> 00:02:21,40
with getting the right data back,

52
00:02:21,40 --> 00:02:23,40
this might be a quicker way to examine that

53
00:02:23,40 --> 00:02:25,70
than doing a bunch of console.logging.

54
00:02:25,70 --> 00:02:28,80
I can watch this in action for my other API.

55
00:02:28,80 --> 00:02:31,40
To do that, I'm going to change my view of dev tools

56
00:02:31,40 --> 00:02:34,10
so it's docked at the bottom of my screen

57
00:02:34,10 --> 00:02:36,60
and I'll drag that up just a little bit

58
00:02:36,60 --> 00:02:38,50
and I want to turn off this overview

59
00:02:38,50 --> 00:02:43,40
so I just have a list of headers over here.

60
00:02:43,40 --> 00:02:45,40
I'm going to scroll to that bottom of that list

61
00:02:45,40 --> 00:02:47,20
and then up here in my webpage

62
00:02:47,20 --> 00:02:51,80
I'm going to go ahead and generate a request for an address.

63
00:02:51,80 --> 00:02:53,20
And as soon as I press Tab,

64
00:02:53,20 --> 00:02:55,80
I've got not one but two requests

65
00:02:55,80 --> 00:02:57,40
and that just has to do with the way

66
00:02:57,40 --> 00:03:01,60
that the remote server is configured for SmartyStreets.

67
00:03:01,60 --> 00:03:03,00
And so it's the second request

68
00:03:03,00 --> 00:03:05,40
that should actually have the data

69
00:03:05,40 --> 00:03:07,30
and I've got that preview tab selected

70
00:03:07,30 --> 00:03:09,20
so here's the data I got back

71
00:03:09,20 --> 00:03:11,50
and again I've got headers for this.

72
00:03:11,50 --> 00:03:13,40
So now that I have this tab open,

73
00:03:13,40 --> 00:03:15,10
any new request gets logged here,

74
00:03:15,10 --> 00:03:18,60
a tool when you're building and working with Ajax requests

75
00:03:18,60 --> 00:03:21,00
to see exactly what's going on behind the scenes.

