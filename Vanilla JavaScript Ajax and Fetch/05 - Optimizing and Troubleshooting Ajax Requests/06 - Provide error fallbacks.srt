1
00:00:00,50 --> 00:00:01,80
- [Instructor] You can make your Ajax

2
00:00:01,80 --> 00:00:04,10
error handling code more useful

3
00:00:04,10 --> 00:00:06,20
by accounting for specific cases

4
00:00:06,20 --> 00:00:09,40
that are significant to your app and ensuring that your code

5
00:00:09,40 --> 00:00:12,70
can deal appropriately with each of them.

6
00:00:12,70 --> 00:00:14,80
Many types of Ajax errors can result

7
00:00:14,80 --> 00:00:16,90
in the requested data not being available

8
00:00:16,90 --> 00:00:19,10
to incorporate into the DOM.

9
00:00:19,10 --> 00:00:21,30
In this case, it's important to consider

10
00:00:21,30 --> 00:00:22,40
how you can meet the needs

11
00:00:22,40 --> 00:00:25,60
of your end users without remote data.

12
00:00:25,60 --> 00:00:27,60
For the Explore California form,

13
00:00:27,60 --> 00:00:30,30
the zip code data is just an extra,

14
00:00:30,30 --> 00:00:34,20
if the zip input box doesn't get autocompleted,

15
00:00:34,20 --> 00:00:37,70
users can still use the form and just fill it out manually,

16
00:00:37,70 --> 00:00:41,40
so there's no extra fallback required there.

17
00:00:41,40 --> 00:00:45,40
Now, the National Park section is already hidden by default

18
00:00:45,40 --> 00:00:48,70
and if the data isn't returned for whatever reason,

19
00:00:48,70 --> 00:00:50,90
that section of the page remains hidden,

20
00:00:50,90 --> 00:00:52,90
so we don't exactly have an issue.

21
00:00:52,90 --> 00:00:56,10
However, we could improve the user experience

22
00:00:56,10 --> 00:00:58,50
by including some default data

23
00:00:58,50 --> 00:01:01,40
that's displayed if we don't get a response.

24
00:01:01,40 --> 00:01:04,70
For instance, we could include the data for one of the parks

25
00:01:04,70 --> 00:01:08,00
in the code for the page and setup our code

26
00:01:08,00 --> 00:01:11,00
to use that data if we can't get live data back

27
00:01:11,00 --> 00:01:13,20
from the National Park service.

28
00:01:13,20 --> 00:01:14,90
This would require someone to check,

29
00:01:14,90 --> 00:01:16,40
maybe, once or twice a year

30
00:01:16,40 --> 00:01:18,90
to ensure that the data is up-to-date,

31
00:01:18,90 --> 00:01:20,70
but it would make this feature of our page

32
00:01:20,70 --> 00:01:23,50
a little more bulletproof.

33
00:01:23,50 --> 00:01:27,30
So, the first thing I want to do, in my error handler,

34
00:01:27,30 --> 00:01:30,50
right now I am throwing some text

35
00:01:30,50 --> 00:01:34,80
and that's getting handled by logging into the console,

36
00:01:34,80 --> 00:01:37,90
but JavaScript actually lets me create an error object

37
00:01:37,90 --> 00:01:40,30
that will capture a little more information.

38
00:01:40,30 --> 00:01:41,90
And so, here on line 66,

39
00:01:41,90 --> 00:01:45,20
all I'm going to do is update this a little bit

40
00:01:45,20 --> 00:01:49,00
to throw a new error, and that's capital E,

41
00:01:49,00 --> 00:01:51,00
and then I'm going to add extra parens

42
00:01:51,00 --> 00:01:52,50
to make sure that it's treating

43
00:01:52,50 --> 00:01:56,00
my concatenated string as a single string.

44
00:01:56,00 --> 00:01:59,10
So then, this text will be included

45
00:01:59,10 --> 00:02:01,50
with a new error object that's generated

46
00:02:01,50 --> 00:02:04,70
and that object will be logged to the console.

47
00:02:04,70 --> 00:02:08,50
So I'm going to go up to my parksUrl,

48
00:02:08,50 --> 00:02:13,50
I'm going to copy, duplicate,

49
00:02:13,50 --> 00:02:17,50
comment out the original and then, break my API key

50
00:02:17,50 --> 00:02:20,80
by deleting a couple characters, I'll save that

51
00:02:20,80 --> 00:02:24,60
and then go back to my browser, open the console.

52
00:02:24,60 --> 00:02:27,00
So, I have a 403 Forbidden error,

53
00:02:27,00 --> 00:02:29,80
and notice here, from line 45,

54
00:02:29,80 --> 00:02:31,50
this is where I consoled out logged

55
00:02:31,50 --> 00:02:33,70
in my error handling function.

56
00:02:33,70 --> 00:02:36,50
But in this case, instead of just getting 403 Forbidden,

57
00:02:36,50 --> 00:02:39,70
which was the text I was passing before,

58
00:02:39,70 --> 00:02:42,70
I have the indication that this is an error object

59
00:02:42,70 --> 00:02:45,50
and I also have a stack trace showing

60
00:02:45,50 --> 00:02:49,00
what was in process when this error happened.

61
00:02:49,00 --> 00:02:52,40
So that can be a little more useful to me, as a developer,

62
00:02:52,40 --> 00:02:55,10
when I actually run into errors.

63
00:02:55,10 --> 00:02:58,50
Now, I'm going to fix my API key,

64
00:02:58,50 --> 00:03:02,00
I'm going to delete the broken one,

65
00:03:02,00 --> 00:03:04,90
put back the good one

66
00:03:04,90 --> 00:03:08,80
and then, I'm getting my request working again.

67
00:03:08,80 --> 00:03:11,00
Now, what I'd like to do is actually grab the data

68
00:03:11,00 --> 00:03:14,70
for, say, the first park that comes through in my request,

69
00:03:14,70 --> 00:03:16,40
copy that into my code

70
00:03:16,40 --> 00:03:20,00
and then, setup some code that uses that as a default

71
00:03:20,00 --> 00:03:23,60
if I don't get data back from a request when the page loads.

72
00:03:23,60 --> 00:03:26,30
So, opening this up, what I'm going to do here

73
00:03:26,30 --> 00:03:32,30
is right-click zero in the data array,

74
00:03:32,30 --> 00:03:35,80
and we have this option Store as global variable.

75
00:03:35,80 --> 00:03:38,30
And so, this is going to grab the contents

76
00:03:38,30 --> 00:03:41,00
of element zero in the data array,

77
00:03:41,00 --> 00:03:42,50
and if I scroll to the bottom,

78
00:03:42,50 --> 00:03:46,30
the browser has created a new variable called temp1

79
00:03:46,30 --> 00:03:50,80
that just stores this single entry in my array.

80
00:03:50,80 --> 00:03:55,20
And so, now I can use the Copy command at the console,

81
00:03:55,20 --> 00:03:59,30
I can pass it the name of that variable, temp1,

82
00:03:59,30 --> 00:04:02,40
and that actually copies that data to the clipboard.

83
00:04:02,40 --> 00:04:07,70
So now, switching back over to my code,

84
00:04:07,70 --> 00:04:14,60
I'm going to create, right after the parksUrl, a new variable

85
00:04:14,60 --> 00:04:20,00
called parksFallback.

86
00:04:20,00 --> 00:04:22,60
And, as the value, I'm simply going to paste in

87
00:04:22,60 --> 00:04:26,40
the code I just copied, that is an object,

88
00:04:26,40 --> 00:04:28,20
going to put a semicolon at the end.

89
00:04:28,20 --> 00:04:31,50
Now, this is the full data for one park

90
00:04:31,50 --> 00:04:35,30
and out of this data, I only need three properties,

91
00:04:35,30 --> 00:04:38,40
I need the description, the full name and the URL.

92
00:04:38,40 --> 00:04:39,90
So, I'm going to delete all the properties

93
00:04:39,90 --> 00:04:44,90
that I will never need, to trim this up a little bit.

94
00:04:44,90 --> 00:04:48,30
So, we've got description, down here is full name,

95
00:04:48,30 --> 00:04:50,50
so I'll delete everything before that,

96
00:04:50,50 --> 00:04:54,10
right after that is URL, and then I can take out the rest

97
00:04:54,10 --> 00:04:56,60
before the closing curly brace.

98
00:04:56,60 --> 00:05:00,50
So now, I have an object containing information that I need

99
00:05:00,50 --> 00:05:05,90
for just one park and that's saved in my code.

100
00:05:05,90 --> 00:05:09,20
And so, to make use of that,

101
00:05:09,20 --> 00:05:13,00
in my parkUpdateUIError,

102
00:05:13,00 --> 00:05:16,30
I have my error being logged to the console,

103
00:05:16,30 --> 00:05:19,30
but I also then want to do a little DOM manipulation,

104
00:05:19,30 --> 00:05:23,40
and, in particular, up here from parkUpdateUISuccess,

105
00:05:23,40 --> 00:05:26,80
I just need a few lines of code.

106
00:05:26,80 --> 00:05:30,40
This is the last, let's see, one, two, three, four,

107
00:05:30,40 --> 00:05:32,90
five statements, I'm going to copy,

108
00:05:32,90 --> 00:05:34,60
I'm going to stick those down here

109
00:05:34,60 --> 00:05:37,50
before the end of parkUpdateUIError

110
00:05:37,50 --> 00:05:40,90
and then, I need to update these references a little bit.

111
00:05:40,90 --> 00:05:42,30
So, all of these should refer

112
00:05:42,30 --> 00:05:46,40
to that new variable I just created, called parksFallback,

113
00:05:46,40 --> 00:05:49,10
so instead of parsedData.data[number],

114
00:05:49,10 --> 00:05:56,40
it's just going to be parksFallback, same for this one

115
00:05:56,40 --> 00:06:00,40
and same for this one,

116
00:06:00,40 --> 00:06:03,20
and these two should be exactly the same.

117
00:06:03,20 --> 00:06:06,60
And so, if I save this, then I scroll up

118
00:06:06,60 --> 00:06:10,80
and once again, I need to break my parksUrl,

119
00:06:10,80 --> 00:06:14,00
so again, I'm going to duplicate that,

120
00:06:14,00 --> 00:06:16,50
I'm going to make one of them a comment,

121
00:06:16,50 --> 00:06:20,60
I'm going to break that API key by deleting a couple characters

122
00:06:20,60 --> 00:06:23,20
and then, switching over to my browser,

123
00:06:23,20 --> 00:06:27,20
I have my error object logged, but looking in the browser,

124
00:06:27,20 --> 00:06:29,60
there's the information for Alcatraz.

125
00:06:29,60 --> 00:06:33,40
So, my error handling code was able to go

126
00:06:33,40 --> 00:06:36,60
and recognize I did not get a successful response,

127
00:06:36,60 --> 00:06:38,40
but then, it was able to use the data

128
00:06:38,40 --> 00:06:42,10
that I'd saved in the file as a backup

129
00:06:42,10 --> 00:06:45,60
to display something to enhance the page.

130
00:06:45,60 --> 00:06:50,90
So, we've implemented a couple useful features

131
00:06:50,90 --> 00:06:56,00
in our code to deal with potential errors in a graceful way.

132
00:06:56,00 --> 00:06:59,90
And then, I just need to make sure I go back to my code

133
00:06:59,90 --> 00:07:02,30
and fix my URL, make sure everything works,

134
00:07:02,30 --> 00:07:05,10
I'm going to take out that broken parksUrl,

135
00:07:05,10 --> 00:07:09,10
I'm going to uncomment the original one, save that change,

136
00:07:09,10 --> 00:07:10,80
and then, going back to my browser,

137
00:07:10,80 --> 00:07:12,10
and now, I've got the data back,

138
00:07:12,10 --> 00:07:15,00
I've even got a different park being displayed.

