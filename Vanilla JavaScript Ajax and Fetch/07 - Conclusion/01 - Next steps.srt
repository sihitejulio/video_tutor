1
00:00:00,50 --> 00:00:03,30
- [Sasha] Thanks so much for joining me in this course.

2
00:00:03,30 --> 00:00:05,30
You now have experience working directly

3
00:00:05,30 --> 00:00:07,80
with the XHR object that forms the foundation

4
00:00:07,80 --> 00:00:11,70
of Ajax on the web, as well as the Fetch API.

5
00:00:11,70 --> 00:00:14,40
To dig deeper into some of the JavaScript mechanics

6
00:00:14,40 --> 00:00:16,20
that underpin Ajax,

7
00:00:16,20 --> 00:00:18,80
check out courses on asynchronous JavaScript

8
00:00:18,80 --> 00:00:21,20
or same-origin policies.

9
00:00:21,20 --> 00:00:24,20
If you want to learn more about working with a backend,

10
00:00:24,20 --> 00:00:27,60
explore courses on Node or Express.js.

11
00:00:27,60 --> 00:00:29,50
If you want to dig into the tools available

12
00:00:29,50 --> 00:00:31,80
for inspecting code in the front-end,

13
00:00:31,80 --> 00:00:34,80
check out courses on browser developer tools.

14
00:00:34,80 --> 00:00:36,80
Feel free to follow me online.

15
00:00:36,80 --> 00:00:39,70
Now take your new skills and build something amazing.

16
00:00:39,70 --> 00:00:41,00
Good luck!

