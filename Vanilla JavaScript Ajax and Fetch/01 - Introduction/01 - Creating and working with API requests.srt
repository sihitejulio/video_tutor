1
00:00:00,50 --> 00:00:02,20
- [Sasha] Ajax is the backbone

2
00:00:02,20 --> 00:00:05,60
of interactive high-performance web apps.

3
00:00:05,60 --> 00:00:07,70
It allows you to incorporate new content

4
00:00:07,70 --> 00:00:10,70
into a web app without reloading the page.

5
00:00:10,70 --> 00:00:13,60
But using Ajax effectively requires understanding

6
00:00:13,60 --> 00:00:17,60
different JavaScript syntaxes, as well as working with data

7
00:00:17,60 --> 00:00:20,40
and manipulating the contents of the browser window.

8
00:00:20,40 --> 00:00:23,70
Building code using both XHR and Fetch

9
00:00:23,70 --> 00:00:26,30
can help you get a handle on how Ajax is implemented

10
00:00:26,30 --> 00:00:28,20
across different code bases.

11
00:00:28,20 --> 00:00:31,40
Examining Ajax requests and responses in the browser

12
00:00:31,40 --> 00:00:34,60
and working with data structures to modify webpage content

13
00:00:34,60 --> 00:00:37,90
can give you practice putting Ajax to practical use.

14
00:00:37,90 --> 00:00:39,70
In my LinkedIn Learning course,

15
00:00:39,70 --> 00:00:42,20
I explore modern coding practices,

16
00:00:42,20 --> 00:00:44,80
as well as those found in some legacy code,

17
00:00:44,80 --> 00:00:47,60
and I also show you how to secure your API keys

18
00:00:47,60 --> 00:00:49,10
in a proxy server.

19
00:00:49,10 --> 00:00:51,80
I'm Sasha Vodnik, and I've been writing JavaScript

20
00:00:51,80 --> 00:00:53,90
since the browser wars of the '90s.

21
00:00:53,90 --> 00:00:55,70
If you want to get in a firmer footing

22
00:00:55,70 --> 00:00:57,30
with making API requests

23
00:00:57,30 --> 00:00:59,70
and incorporating the results into your apps,

24
00:00:59,70 --> 00:01:03,00
I invite you to join me for this course on Ajax and Fetch.

