1
00:00:00,50 --> 00:00:02,20
- [Instructor] To work along with me on the files

2
00:00:02,20 --> 00:00:05,00
for this course, you need two applications:

3
00:00:05,00 --> 00:00:07,50
a web browser and a code editor.

4
00:00:07,50 --> 00:00:10,10
You undoubtedly already have a web browser installed

5
00:00:10,10 --> 00:00:13,10
on your machine, and any major modern browser,

6
00:00:13,10 --> 00:00:17,40
Chrome, Firefox, or Microsoft Edge, is fine for this course.

7
00:00:17,40 --> 00:00:19,20
I'll be using Chrome in these videos,

8
00:00:19,20 --> 00:00:21,50
which is a popular choice among web developers

9
00:00:21,50 --> 00:00:24,10
because of the extensive and powerful developer tools

10
00:00:24,10 --> 00:00:25,40
it has built in.

11
00:00:25,40 --> 00:00:28,70
I've customized Chrome with the JSON Formatter extension

12
00:00:28,70 --> 00:00:32,20
by Callum Locke, which is helpful when examining JSON data

13
00:00:32,20 --> 00:00:34,00
in the browser window.

14
00:00:34,00 --> 00:00:36,40
A number of great code editors are available,

15
00:00:36,40 --> 00:00:38,90
both free and paid apps.

16
00:00:38,90 --> 00:00:42,30
Any editor that lets you edit and save plain text is fine

17
00:00:42,30 --> 00:00:45,60
for this course, so if you have a code editor you like,

18
00:00:45,60 --> 00:00:48,80
such as Sublime Text or Atom, it's fine to use it.

19
00:00:48,80 --> 00:00:51,30
I use Visual Studio Code in these videos,

20
00:00:51,30 --> 00:00:53,80
which is a version of Microsoft's Visual Studio

21
00:00:53,80 --> 00:00:56,10
created specifically for web development.

22
00:00:56,10 --> 00:00:57,80
Visual Studio Code is free

23
00:00:57,80 --> 00:01:00,50
and has Windows, Mac, and Linux releases.

24
00:01:00,50 --> 00:01:02,30
The code is available on GitHub,

25
00:01:02,30 --> 00:01:04,90
and users can submit issues there as well.

26
00:01:04,90 --> 00:01:07,20
I've turned on word wrap in my editor.

27
00:01:07,20 --> 00:01:09,80
If you want to do the same, just click View,

28
00:01:09,80 --> 00:01:13,10
and then Toggle Word Wrap, this ensures that long lines

29
00:01:13,10 --> 00:01:15,30
of code don't run off the screen.

30
00:01:15,30 --> 00:01:18,00
I've also installed a few extensions.

31
00:01:18,00 --> 00:01:22,00
Bracker Pair Colorizer 2 by CoenraadS color codes

32
00:01:22,00 --> 00:01:25,60
each nested pair of parentheses, brackets, and braces

33
00:01:25,60 --> 00:01:28,60
to make nesting levels easier to distinguish.

34
00:01:28,60 --> 00:01:30,60
Note that this is a new, improved version

35
00:01:30,60 --> 00:01:34,80
of a previous extension called simply Bracket Pair Colorizer

36
00:01:34,80 --> 00:01:37,60
by the same creator, so be sure to grab version two

37
00:01:37,60 --> 00:01:40,10
for the most up-to-date code.

38
00:01:40,10 --> 00:01:43,30
Indenticator by SirTori highlights your current level

39
00:01:43,30 --> 00:01:45,20
of indent, which is also helpful

40
00:01:45,20 --> 00:01:49,50
in identifying your location in nested code.

41
00:01:49,50 --> 00:01:54,30
Finally, Live Server by Ritwick Dey is an HTTP server

42
00:01:54,30 --> 00:01:56,30
you can launch with a single click

43
00:01:56,30 --> 00:01:58,70
that automatically opens the current document

44
00:01:58,70 --> 00:02:00,10
in your default browser.

45
00:02:00,10 --> 00:02:02,70
Testing Ajax code during development benefits

46
00:02:02,70 --> 00:02:06,00
from an HTTP server, and this one is really easy

47
00:02:06,00 --> 00:02:07,60
to install and use.

48
00:02:07,60 --> 00:02:09,90
If you want to learn more about anything I use

49
00:02:09,90 --> 00:02:12,20
or talk about in this course, I encourage you

50
00:02:12,20 --> 00:02:15,50
to explore the library for a deeper dive on that topic.

51
00:02:15,50 --> 00:02:17,00
Now let's get started.

