1
00:00:00,50 --> 00:00:03,60
- [Instructor] The dynamic nature of web apps and web pages

2
00:00:03,60 --> 00:00:05,90
is an integral feature of the modern web,

3
00:00:05,90 --> 00:00:08,00
but this was not always the case.

4
00:00:08,00 --> 00:00:12,10
In its early days, the web was composed of static pages.

5
00:00:12,10 --> 00:00:14,20
Each page displayed only the content

6
00:00:14,20 --> 00:00:16,50
coded into its HTML document.

7
00:00:16,50 --> 00:00:18,50
Changing the information displayed meant

8
00:00:18,50 --> 00:00:21,90
requesting and loading a different HTML document.

9
00:00:21,90 --> 00:00:25,10
But new documents take time to load and to render.

10
00:00:25,10 --> 00:00:26,40
What if we could, instead, simply

11
00:00:26,40 --> 00:00:30,60
request content and update the display of the current page?

12
00:00:30,60 --> 00:00:34,70
In fact, this is exactly what AJAX lets us do.

13
00:00:34,70 --> 00:00:36,50
A page that uses AJAX,

14
00:00:36,50 --> 00:00:38,90
which is most everything on the modern web,

15
00:00:38,90 --> 00:00:41,30
requests data in the background,

16
00:00:41,30 --> 00:00:43,60
and then integrates it into the document already

17
00:00:43,60 --> 00:00:45,80
displayed in the browser window.

18
00:00:45,80 --> 00:00:48,30
Originally, AJAX was an acronym,

19
00:00:48,30 --> 00:00:51,60
based on the fact that the process was asynchronous,

20
00:00:51,60 --> 00:00:53,50
was scripted from JavaScript,

21
00:00:53,50 --> 00:00:56,70
and originally used XML as the data format

22
00:00:56,70 --> 00:00:58,50
for the new content.

23
00:00:58,50 --> 00:01:02,30
Today, AJAX describes a more generic process.

24
00:01:02,30 --> 00:01:05,80
Although JavaScript is still a core building block of AJAX,

25
00:01:05,80 --> 00:01:09,20
it's much more common to use JavaScript object notation,

26
00:01:09,20 --> 00:01:11,90
or JSON for data exchange.

27
00:01:11,90 --> 00:01:15,30
AJAX is at work anytime you interact with a webpage,

28
00:01:15,30 --> 00:01:17,60
and the page changes to display data

29
00:01:17,60 --> 00:01:20,10
that was not present when it first loaded.

30
00:01:20,10 --> 00:01:22,50
For instance, in may search engines,

31
00:01:22,50 --> 00:01:25,80
when I start typing a search into the search box,

32
00:01:25,80 --> 00:01:28,80
the app sends what I've typed so far to the server,

33
00:01:28,80 --> 00:01:32,60
receives a set of likely search phrases based on that,

34
00:01:32,60 --> 00:01:36,20
and offers them to me in a context menu.

35
00:01:36,20 --> 00:01:39,50
As I continue to type, these options change,

36
00:01:39,50 --> 00:01:42,80
always based on what I've typed, but without loading a new

37
00:01:42,80 --> 00:01:45,50
HTML document.

38
00:01:45,50 --> 00:01:47,70
Another place we often see AJAX

39
00:01:47,70 --> 00:01:50,80
is in feeds that use infinite scrolling.

40
00:01:50,80 --> 00:01:52,60
This feature enables the feed to load

41
00:01:52,60 --> 00:01:56,30
just enough articles that a user can continue scrolling.

42
00:01:56,30 --> 00:02:00,90
So I can go on this site and grab the scroll bar,

43
00:02:00,90 --> 00:02:04,20
and when I scroll down to the bottom of the feed,

44
00:02:04,20 --> 00:02:08,00
more articles load so I can continue scrolling.

45
00:02:08,00 --> 00:02:10,50
This enables users to continue scrolling through a feed

46
00:02:10,50 --> 00:02:13,60
for as long as they want to, but ensures that the page

47
00:02:13,60 --> 00:02:16,20
does not request a lot of unnecessary data.

48
00:02:16,20 --> 00:02:19,00
Instead, the data is requested in chunks,

49
00:02:19,00 --> 00:02:20,40
and only in response to the

50
00:02:20,40 --> 00:02:23,00
user's intent to continue scrolling.

