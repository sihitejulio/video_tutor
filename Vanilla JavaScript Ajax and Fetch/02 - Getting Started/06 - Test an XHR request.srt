1
00:00:00,50 --> 00:00:02,70
- You can test AJAX code in a browser

2
00:00:02,70 --> 00:00:05,50
just like you would any other front-end code.

3
00:00:05,50 --> 00:00:07,60
However, it's generally best not

4
00:00:07,60 --> 00:00:09,60
to simply open your HTML file

5
00:00:09,60 --> 00:00:12,10
in a browser and see what happens.

6
00:00:12,10 --> 00:00:14,70
This is because by default a browser opens a

7
00:00:14,70 --> 00:00:17,40
local file using the file protocol

8
00:00:17,40 --> 00:00:19,90
which browsers treat differently then pages open

9
00:00:19,90 --> 00:00:22,80
using HTTP requests including

10
00:00:22,80 --> 00:00:24,80
everything you open from the web.

11
00:00:24,80 --> 00:00:28,60
Fortunately, it's easy to run a simple HTTP server

12
00:00:28,60 --> 00:00:30,20
on your development computer,

13
00:00:30,20 --> 00:00:32,40
which lets you more closely match the way your

14
00:00:32,40 --> 00:00:35,60
AJAX code will work after you deploy it.

15
00:00:35,60 --> 00:00:38,60
In addition, some API's require requests

16
00:00:38,60 --> 00:00:41,10
to be linked to a specific URL.

17
00:00:41,10 --> 00:00:43,70
So, using a HTTP server is a must

18
00:00:43,70 --> 00:00:45,30
in a situation like that.

19
00:00:45,30 --> 00:00:48,10
I'm using Visual Studio Code editor with a Live

20
00:00:48,10 --> 00:00:49,60
server extension.

21
00:00:49,60 --> 00:00:52,70
This extension takes care of all the configuration for me.

22
00:00:52,70 --> 00:00:55,40
Similar extensions are available for Sublime Text

23
00:00:55,40 --> 00:00:56,20
and for Atom.

24
00:00:56,20 --> 00:00:58,80
So, whatever environment you're using, I recommend

25
00:00:58,80 --> 00:01:01,70
installing and using an extension like this.

26
00:01:01,70 --> 00:01:04,10
For Live Server, I need to start by opening the

27
00:01:04,10 --> 00:01:07,50
HTML document in my editor.

28
00:01:07,50 --> 00:01:09,90
Then I click the go live button

29
00:01:09,90 --> 00:01:13,00
which starts up a server and then automatically opens

30
00:01:13,00 --> 00:01:15,40
my page in my default browser

31
00:01:15,40 --> 00:01:22,10
using HTTP and the IP address 127.0.0.1

32
00:01:22,10 --> 00:01:25,80
which is a reserved address that maps to my local computer.

33
00:01:25,80 --> 00:01:28,10
It's possible that your Live Server will use a

34
00:01:28,10 --> 00:01:31,00
different IP address because every computer

35
00:01:31,00 --> 00:01:35,40
has a few IP addresses that refer to it at any given moment.

36
00:01:35,40 --> 00:01:38,50
So, know matter what IP address you have showing up here,

37
00:01:38,50 --> 00:01:40,10
you need to configure your SmartyStreets

38
00:01:40,10 --> 00:01:42,20
website key to use it.

39
00:01:42,20 --> 00:01:44,70
So, take note of that address,

40
00:01:44,70 --> 00:01:47,40
and then over on SmartyStreets you can

41
00:01:47,40 --> 00:01:49,60
add a host to your website key.

42
00:01:49,60 --> 00:01:54,80
So, my key has a host already of 127.0.0.1

43
00:01:54,80 --> 00:01:58,10
you may have that but need a different IP address,

44
00:01:58,10 --> 00:02:01,50
and you can do that by simply clicking add host here

45
00:02:01,50 --> 00:02:05,00
then typing in whatever IP address you need.

46
00:02:05,00 --> 00:02:12,00
For example, If I was at 10.0.0.5,

47
00:02:12,00 --> 00:02:16,40
and then you click the checkbox over here.

48
00:02:16,40 --> 00:02:18,90
And now we've got two different hosts

49
00:02:18,90 --> 00:02:20,80
associated with this key.

50
00:02:20,80 --> 00:02:23,20
If one of those hosts is not actually relevant for you,

51
00:02:23,20 --> 00:02:26,10
you can just use the garbage can icon,

52
00:02:26,10 --> 00:02:27,80
confirm that with the delete button,

53
00:02:27,80 --> 00:02:30,20
and just have the host that corresponds

54
00:02:30,20 --> 00:02:32,60
to the server that you're using.

55
00:02:32,60 --> 00:02:35,10
And now that website key will work when

56
00:02:35,10 --> 00:02:39,10
the request comes from the associated address.

57
00:02:39,10 --> 00:02:42,00
Now, over on the page that I opened using Live Server,

58
00:02:42,00 --> 00:02:44,00
I'm going to open up my console,

59
00:02:44,00 --> 00:02:47,20
and notice that I have an error.

60
00:02:47,20 --> 00:02:50,10
So, I never actually swapped in my own authorization

61
00:02:50,10 --> 00:02:52,00
information into the URL,

62
00:02:52,00 --> 00:02:54,10
and I'm opening this from a different source

63
00:02:54,10 --> 00:02:56,10
then the SmartyStreets website,

64
00:02:56,10 --> 00:02:59,40
so that authorization information that they provide

65
00:02:59,40 --> 00:03:02,70
on their website isn't valid here.

66
00:03:02,70 --> 00:03:04,50
Now, checking out the documentation

67
00:03:04,50 --> 00:03:07,60
notice that we have two different sections of

68
00:03:07,60 --> 00:03:11,00
authorization information of API keys.

69
00:03:11,00 --> 00:03:14,10
The first is the website keys for client-side code.

70
00:03:14,10 --> 00:03:17,60
The second is secret keys for server-side code.

71
00:03:17,60 --> 00:03:19,50
We need the client-side credentials,

72
00:03:19,50 --> 00:03:22,60
so since I'm here I'm going to copy that key again,

73
00:03:22,60 --> 00:03:25,60
and then I'm going to follow this client-side code link

74
00:03:25,60 --> 00:03:28,30
to learn more about how to use this.

75
00:03:28,30 --> 00:03:31,50
So, in this documentation page it shows us

76
00:03:31,50 --> 00:03:36,00
a sample request, and notice that I have an auth-id key,

77
00:03:36,00 --> 00:03:39,00
but not an auth-token key because in this case

78
00:03:39,00 --> 00:03:41,10
you only need that one key value pair.

79
00:03:41,10 --> 00:03:44,20
So, I want to change the auth-id key in my request

80
00:03:44,20 --> 00:03:46,50
to my website key,

81
00:03:46,50 --> 00:03:50,00
and I want to get rid of that auth-token key.

82
00:03:50,00 --> 00:03:54,90
So, I'm going to scroll up, I'm going to hit products and APIs,

83
00:03:54,90 --> 00:03:58,40
and then scrolling down, this is the demo once again

84
00:03:58,40 --> 00:04:01,30
for the endpoint we're using.

85
00:04:01,30 --> 00:04:03,90
I'm going to use the Belmont Massachusetts

86
00:04:03,90 --> 00:04:06,10
sample data set, but anyone's going to work here,

87
00:04:06,10 --> 00:04:07,20
scroll back up.

88
00:04:07,20 --> 00:04:10,10
And so first I'm going to select the contents of the auth-id

89
00:04:10,10 --> 00:04:13,80
field, I'm going to paste in my own which I just copied.

90
00:04:13,80 --> 00:04:16,70
And then I can get rid of the auth-token field

91
00:04:16,70 --> 00:04:19,00
because I don't need it for a website request,

92
00:04:19,00 --> 00:04:21,90
so I can just use that minus button over here.

93
00:04:21,90 --> 00:04:25,60
And I'm once again going to get rid of the default street2,

94
00:04:25,60 --> 00:04:28,20
and zip code which don't have a value here.

95
00:04:28,20 --> 00:04:35,20
And now I have a fully ready to use URL for my request.

96
00:04:35,20 --> 00:04:38,90
So I'm going to click in here, I'm going to copy that,

97
00:04:38,90 --> 00:04:41,60
and then back in my code, in my JavaScript,

98
00:04:41,60 --> 00:04:43,70
I'm just going to replace the previous URL

99
00:04:43,70 --> 00:04:46,70
within the quotes with the one I just copied.

100
00:04:46,70 --> 00:04:50,60
So, now I have the auth-id key with my specific key.

101
00:04:50,60 --> 00:04:52,60
I still have all those other key value pairs

102
00:04:52,60 --> 00:04:54,30
for the data I'm looking for,

103
00:04:54,30 --> 00:04:58,10
but I don't have that extraneous auth-token key,

104
00:04:58,10 --> 00:05:03,40
and I'm using my own API keys and not the

105
00:05:03,40 --> 00:05:06,10
sample ones provided by SmartyStreets.

106
00:05:06,10 --> 00:05:10,20
So, I'm going to save my changes here, and my Live preview app

107
00:05:10,20 --> 00:05:13,30
automatically refreshes the view in the browser.

108
00:05:13,30 --> 00:05:15,70
Going back to the browser, I can see in the console

109
00:05:15,70 --> 00:05:17,90
that now I'm all good, I have the data I want.

110
00:05:17,90 --> 00:05:20,50
So, let's take a little closer look at this.

111
00:05:20,50 --> 00:05:25,60
I am going to move this into its own window,

112
00:05:25,60 --> 00:05:28,50
and then I'm going to increase the font size a little bit,

113
00:05:28,50 --> 00:05:30,10
so it's easier to read,

114
00:05:30,10 --> 00:05:33,80
and so I can see that I have amongst all this,

115
00:05:33,80 --> 00:05:39,10
the zip code and the plus four that I was looking for.

116
00:05:39,10 --> 00:05:41,60
And so this is the data, again, for the

117
00:05:41,60 --> 00:05:43,80
Belmont, Massachusetts sample request.

118
00:05:43,80 --> 00:05:46,00
But I know now that I have a starting point

119
00:05:46,00 --> 00:05:47,50
that gets me data back,

120
00:05:47,50 --> 00:05:49,80
and I can then customize this from the data

121
00:05:49,80 --> 00:05:51,00
that I get from my form.

