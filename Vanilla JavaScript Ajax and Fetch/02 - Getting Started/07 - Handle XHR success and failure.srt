1
00:00:00,50 --> 00:00:03,50
- [Instructor] My code, so far, creates and sends a request

2
00:00:03,50 --> 00:00:07,10
and then logs whatever's in the response to the console.

3
00:00:07,10 --> 00:00:10,20
Now, eventually, I want to be working with the response data

4
00:00:10,20 --> 00:00:12,90
to change what users see in the browser window.

5
00:00:12,90 --> 00:00:15,80
But, until I get there, I at least want to update my code

6
00:00:15,80 --> 00:00:18,80
so it differentiates between a successful response

7
00:00:18,80 --> 00:00:21,20
and a response containing an error message.

8
00:00:21,20 --> 00:00:23,30
And I'll break those out into separate functions

9
00:00:23,30 --> 00:00:26,60
to keep my concerns separate so the AJAX request code

10
00:00:26,60 --> 00:00:28,90
can be changed without affecting the code

11
00:00:28,90 --> 00:00:31,20
to work with the response.

12
00:00:31,20 --> 00:00:32,80
So first, I'm going to break out my code

13
00:00:32,80 --> 00:00:35,90
for analyzing the response into a new function

14
00:00:35,90 --> 00:00:41,00
and I'll call that response method.

15
00:00:41,00 --> 00:00:44,90
And that's going to take the HTTP request parameter.

16
00:00:44,90 --> 00:00:47,10
And so, within that function, I want to check

17
00:00:47,10 --> 00:00:49,70
a couple properties of that request.

18
00:00:49,70 --> 00:00:51,60
Now, the ready state property will tell us

19
00:00:51,60 --> 00:00:53,10
if the request is completed.

20
00:00:53,10 --> 00:00:55,10
That's the case when the value's four.

21
00:00:55,10 --> 00:01:00,60
So, I will check if HTTP request .ready state,

22
00:01:00,60 --> 00:01:05,10
triple equals four, and then only if that's the case,

23
00:01:05,10 --> 00:01:07,70
I want my code to examine my status property.

24
00:01:07,70 --> 00:01:10,90
This property gives me the HTTP status code.

25
00:01:10,90 --> 00:01:14,90
That's a three-digit number like 200, 404, 550,

26
00:01:14,90 --> 00:01:17,50
it summarizes the response.

27
00:01:17,50 --> 00:01:19,70
If the code is 200, that corresponds

28
00:01:19,70 --> 00:01:21,60
to a successful response.

29
00:01:21,60 --> 00:01:24,10
Otherwise, there's some issue.

30
00:01:24,10 --> 00:01:29,40
So, I'm going to call an update UI success function

31
00:01:29,40 --> 00:01:31,00
that I'll create in a bit

32
00:01:31,00 --> 00:01:34,80
and I'm going to pass that HTTP request

33
00:01:34,80 --> 00:01:39,60
.response text and that is the text that we receive,

34
00:01:39,60 --> 00:01:43,00
the body of the response.

35
00:01:43,00 --> 00:01:45,40
Now, if the status is not 200,

36
00:01:45,40 --> 00:01:48,40
I'm going to do an else and I'm going to call a different function

37
00:01:48,40 --> 00:01:51,60
that I'm about to create, update UI error,

38
00:01:51,60 --> 00:01:55,00
and I will pass that HTTP request.status,

39
00:01:55,00 --> 00:01:57,30
so that's status value,

40
00:01:57,30 --> 00:02:00,80
and then I will concatenate a colon and a space,

41
00:02:00,80 --> 00:02:03,70
that needs to be a plus.

42
00:02:03,70 --> 00:02:09,70
And then I will concatenate HTTP request.response text

43
00:02:09,70 --> 00:02:11,50
and in this case, when I don't actually have

44
00:02:11,50 --> 00:02:13,30
have a successful response body,

45
00:02:13,30 --> 00:02:15,70
that response text is going to contain the text

46
00:02:15,70 --> 00:02:18,10
of any error and that'll let me work

47
00:02:18,10 --> 00:02:19,80
with exactly what's wrong.

48
00:02:19,80 --> 00:02:23,40
So, now I just need to define these new functions,

49
00:02:23,40 --> 00:02:28,50
so I'll do const update UI success

50
00:02:28,50 --> 00:02:32,20
and that is going to take a parameter we'll call data,

51
00:02:32,20 --> 00:02:35,80
and for now, I'm simply going to console.log data

52
00:02:35,80 --> 00:02:42,60
and likewise, const update UI error

53
00:02:42,60 --> 00:02:46,10
and that's going to take a parameter I'll call error

54
00:02:46,10 --> 00:02:50,90
and we'll console.log that error parameter.

55
00:02:50,90 --> 00:02:56,50
Now, saving my changes, going to switch to the HTML

56
00:02:56,50 --> 00:03:02,40
and go live in my browser and opening up my console,

57
00:03:02,40 --> 00:03:05,10
and I've got a successful response.

58
00:03:05,10 --> 00:03:08,10
And so, with those functions split out,

59
00:03:08,10 --> 00:03:11,20
everything's working like it did before.

60
00:03:11,20 --> 00:03:13,20
So the final change I need to make to my code then

61
00:03:13,20 --> 00:03:16,10
is down here in the create request function.

62
00:03:16,10 --> 00:03:21,80
And I need to take out this if/else section.

63
00:03:21,80 --> 00:03:24,50
Like I mentioned earlier, when I first set things up,

64
00:03:24,50 --> 00:03:27,00
each time you want to view files from a different folder,

65
00:03:27,00 --> 00:03:28,70
you need to make sure you've closed

66
00:03:28,70 --> 00:03:30,50
and restarted live server.

67
00:03:30,50 --> 00:03:32,10
Otherwise, you'll be viewing the files

68
00:03:32,10 --> 00:03:34,20
from the previous folder in the browser

69
00:03:34,20 --> 00:03:36,00
instead of the current files.

70
00:03:36,00 --> 00:03:38,80
So I need to go down here, and the live server icon

71
00:03:38,80 --> 00:03:42,20
has changed to just show the port and the slash symbol

72
00:03:42,20 --> 00:03:45,40
and clicking that will dispose of that server.

73
00:03:45,40 --> 00:03:48,50
Now we've got go live again and so I can click

74
00:03:48,50 --> 00:03:55,30
the HTML file and then click go live to open that back up.

75
00:03:55,30 --> 00:03:58,50
And on ready state change, I simply want to call

76
00:03:58,50 --> 00:04:04,70
response method and a pass it the HTTP request object.

77
00:04:04,70 --> 00:04:08,30
So I'm going to save that,

78
00:04:08,30 --> 00:04:11,30
then switching over to the HTML file,

79
00:04:11,30 --> 00:04:14,80
and going live in the browser,

80
00:04:14,80 --> 00:04:17,00
and I'm going to open up my console

81
00:04:17,00 --> 00:04:19,50
and so I have my response text logged

82
00:04:19,50 --> 00:04:21,50
just like I did before.

83
00:04:21,50 --> 00:04:22,50
Now, going back to my code,

84
00:04:22,50 --> 00:04:25,70
I want to test that error function.

85
00:04:25,70 --> 00:04:27,90
So I'm going to go up to my URL.

86
00:04:27,90 --> 00:04:31,40
I'm going to copy it and duplicate it.

87
00:04:31,40 --> 00:04:32,90
I'm going to comment out the first one

88
00:04:32,90 --> 00:04:35,20
so that I have a known good URL

89
00:04:35,20 --> 00:04:39,90
and then I'm going to hack apart my URL here a little bit.

90
00:04:39,90 --> 00:04:42,70
I'm just going to go to the auth ID key

91
00:04:42,70 --> 00:04:44,80
and for that auth ID value, I'm just going to take out

92
00:04:44,80 --> 00:04:48,60
the last couple digits, so it's going to be a invalid auth ID.

93
00:04:48,60 --> 00:04:52,10
I'm going to save that, then, going back to my browser,

94
00:04:52,10 --> 00:04:57,70
I've got an error and this is my update UI error function

95
00:04:57,70 --> 00:05:02,10
logging the HTTP request status

96
00:05:02,10 --> 00:05:04,20
and the status text.

97
00:05:04,20 --> 00:05:06,80
So that, as a developer, just for this development process,

98
00:05:06,80 --> 00:05:08,50
I can see exactly what error I got

99
00:05:08,50 --> 00:05:11,40
and the error text pretty straightforwardly.

100
00:05:11,40 --> 00:05:14,60
And so, going back and just ironing things out,

101
00:05:14,60 --> 00:05:19,90
I'm going to take out that broken version of my variable.

102
00:05:19,90 --> 00:05:24,40
I'm going to uncomment the good version of the variable.

103
00:05:24,40 --> 00:05:26,40
I'm going to save that.

104
00:05:26,40 --> 00:05:28,90
Back to my console one more time.

105
00:05:28,90 --> 00:05:31,20
I've got my data back, so great.

106
00:05:31,20 --> 00:05:33,90
Now I have success and error code working

107
00:05:33,90 --> 00:05:35,60
and I also have them separated out

108
00:05:35,60 --> 00:05:38,00
into their own independent functions.

