1
00:00:00,10 --> 00:00:06,00
(upbeat music)

2
00:00:06,00 --> 00:00:07,40
- [Instructor] It's time for a challenge.

3
00:00:07,40 --> 00:00:10,30
Ready to try your hand at getting information from an API?

4
00:00:10,30 --> 00:00:12,00
Another aspect that we want to change

5
00:00:12,00 --> 00:00:15,10
on Explore California form page is to add information

6
00:00:15,10 --> 00:00:17,50
about national parks in California.

7
00:00:17,50 --> 00:00:19,60
The National Park Service has an API

8
00:00:19,60 --> 00:00:21,40
that provides park information.

9
00:00:21,40 --> 00:00:25,00
So you can get this data using an Ajax request.

10
00:00:25,00 --> 00:00:28,40
Check out the developer guide and the API documentation

11
00:00:28,40 --> 00:00:30,50
at NPS.gov.

12
00:00:30,50 --> 00:00:33,20
Then create a request that returns information

13
00:00:33,20 --> 00:00:35,30
on national parks in California.

14
00:00:35,30 --> 00:00:37,30
You can reuse the functions you already created

15
00:00:37,30 --> 00:00:40,10
in this chapter, so just focus on actually constructing

16
00:00:40,10 --> 00:00:42,40
and implementing the URL.

17
00:00:42,40 --> 00:00:44,20
This is a longer challenge

18
00:00:44,20 --> 00:00:46,40
that might take you around 30 minutes.

19
00:00:46,40 --> 00:00:49,00
If you don't have time for that right now,

20
00:00:49,00 --> 00:00:51,00
feel free to come back to it later on.

21
00:00:51,00 --> 00:00:53,10
When you're done, join me in the next video,

22
00:00:53,10 --> 00:00:56,00
and I'll go over how I approached it.

