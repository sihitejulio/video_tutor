1
00:00:00,50 --> 00:00:03,50
- [Instructor] Once you've identified the API you want to use,

2
00:00:03,50 --> 00:00:06,20
the next step is to build a request.

3
00:00:06,20 --> 00:00:08,10
Modern web services are based on

4
00:00:08,10 --> 00:00:11,70
an architecture called REST, which uses HTTP requests

5
00:00:11,70 --> 00:00:14,60
and responses to exchange information.

6
00:00:14,60 --> 00:00:16,20
So we need to write a URL

7
00:00:16,20 --> 00:00:18,80
that requests the information we want.

8
00:00:18,80 --> 00:00:22,40
APIs often provide access to multiple types of data

9
00:00:22,40 --> 00:00:24,70
and you need to examine the documentation

10
00:00:24,70 --> 00:00:28,40
to find how to request the specific data you want.

11
00:00:28,40 --> 00:00:31,20
When an API gives different URL paths,

12
00:00:31,20 --> 00:00:33,70
these are known as endpoints and you need to pick

13
00:00:33,70 --> 00:00:36,10
the correct one for the data you want.

14
00:00:36,10 --> 00:00:39,30
The SmartyStreets U.S. street address API

15
00:00:39,30 --> 00:00:41,80
returns nine-digit zip codes.

16
00:00:41,80 --> 00:00:45,00
Even though there's another API called the zip code API,

17
00:00:45,00 --> 00:00:47,90
that actually requires a zip code to start.

18
00:00:47,90 --> 00:00:50,60
So, again, the documentation is super valuable

19
00:00:50,60 --> 00:00:53,70
when you're first picking an API and endpoint

20
00:00:53,70 --> 00:00:55,80
and then figuring out how to use it.

21
00:00:55,80 --> 00:00:57,40
Now here on the overview page,

22
00:00:57,40 --> 00:00:59,80
there's a demo form that creates requests

23
00:00:59,80 --> 00:01:01,80
and shows responses.

24
00:01:01,80 --> 00:01:04,00
There are a lot of fields here that aren't

25
00:01:04,00 --> 00:01:06,20
all that obvious in their names,

26
00:01:06,20 --> 00:01:08,90
but I can click Try a sample data set

27
00:01:08,90 --> 00:01:12,30
and then I'm going to pick the Belmont, Massachusetts address

28
00:01:12,30 --> 00:01:14,40
and the data is auto-populated.

29
00:01:14,40 --> 00:01:18,30
Now, up here to the right, is a sample cURL request.

30
00:01:18,30 --> 00:01:20,30
This is a command line utility

31
00:01:20,30 --> 00:01:23,90
that enables a developer to send an HTTP request.

32
00:01:23,90 --> 00:01:26,20
And the useful thing here is that it shows us

33
00:01:26,20 --> 00:01:29,80
how our form values are translated into key value pairs

34
00:01:29,80 --> 00:01:32,90
in the query portion of the URL.

35
00:01:32,90 --> 00:01:35,10
So we have the endpoint first,

36
00:01:35,10 --> 00:01:39,10
which is the main portion of the URL

37
00:01:39,10 --> 00:01:44,40
and then the auth ID and auth token that are unique to us.

38
00:01:44,40 --> 00:01:48,70
In this case, these are the sample auth ID and auth token

39
00:01:48,70 --> 00:01:51,50
that are specific to the sample website.

40
00:01:51,50 --> 00:01:53,40
And then we have candidates,

41
00:01:53,40 --> 00:01:56,20
which seems likely to indicate

42
00:01:56,20 --> 00:01:58,30
the number of results we want back.

43
00:01:58,30 --> 00:02:01,20
And then we see things like street, city, and state,

44
00:02:01,20 --> 00:02:04,30
along with key value pairs with empty values

45
00:02:04,30 --> 00:02:08,60
for two of the fields, zip code and street two.

46
00:02:08,60 --> 00:02:11,10
And we will be providing values for these.

47
00:02:11,10 --> 00:02:13,00
So we can actually take those out of our search

48
00:02:13,00 --> 00:02:15,90
with this interface just by clicking the minus next to them.

49
00:02:15,90 --> 00:02:18,30
So I'm going to click the minus next to zip code,

50
00:02:18,30 --> 00:02:19,80
the minus next to street two,

51
00:02:19,80 --> 00:02:22,40
and those are removed from our sample URL.

52
00:02:22,40 --> 00:02:24,70
So we have a basic URL structure,

53
00:02:24,70 --> 00:02:26,40
which gives results that include

54
00:02:26,40 --> 00:02:30,10
those nine-digit zip code sections.

55
00:02:30,10 --> 00:02:32,70
So, I want to copy just the URL portion,

56
00:02:32,70 --> 00:02:35,40
so I'm going to click in here first

57
00:02:35,40 --> 00:02:38,00
and then I'm going to select the URL,

58
00:02:38,00 --> 00:02:39,70
but not the quotes around it.

59
00:02:39,70 --> 00:02:42,10
I'm going to copy that to the clipboard.

60
00:02:42,10 --> 00:02:45,30
And then switching over to my files,

61
00:02:45,30 --> 00:02:50,80
I'm going to open up the _scripts folder and the app.js file.

62
00:02:50,80 --> 00:02:52,80
And below that use strict statement,

63
00:02:52,80 --> 00:02:55,70
I want to create a new const

64
00:02:55,70 --> 00:02:59,10
because my app will not be changing this value

65
00:02:59,10 --> 00:03:01,70
and I'm going to call that URL.

66
00:03:01,70 --> 00:03:04,10
I'm going to give it quotes and I'm going to paste in

67
00:03:04,10 --> 00:03:06,30
that URL that I just copied

68
00:03:06,30 --> 00:03:08,60
and I'm going to finish it up with a semicolon.

69
00:03:08,60 --> 00:03:10,70
And then with that value still copied,

70
00:03:10,70 --> 00:03:13,60
and save my file, go back to the browser

71
00:03:13,60 --> 00:03:17,60
and in a new tab, I'm just going to paste in that URL.

72
00:03:17,60 --> 00:03:20,70
And here I get the return JSON.

73
00:03:20,70 --> 00:03:22,50
Now, by default, that's not easy to read

74
00:03:22,50 --> 00:03:24,80
in a browser window.

75
00:03:24,80 --> 00:03:26,50
It looks something like that,

76
00:03:26,50 --> 00:03:29,90
but I have the JSON Formatter browser extension,

77
00:03:29,90 --> 00:03:31,90
which lets me see the parsed view

78
00:03:31,90 --> 00:03:35,10
and, again, I can see that we've got the zip code,

79
00:03:35,10 --> 00:03:37,80
the plus four code, so all the data I want,

80
00:03:37,80 --> 00:03:41,80
and so I now have selected my endpoint.

81
00:03:41,80 --> 00:03:45,50
I have a sample URL that returns the data I want

82
00:03:45,50 --> 00:03:47,90
and so this is enough to get me started

83
00:03:47,90 --> 00:03:50,00
actually building out my app.

