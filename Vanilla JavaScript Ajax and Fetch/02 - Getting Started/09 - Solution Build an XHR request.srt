1
00:00:00,40 --> 00:00:05,90
(upbeat electronic music)

2
00:00:05,90 --> 00:00:08,40
- [Instructor] The National Park Service API is free

3
00:00:08,40 --> 00:00:10,30
for a reasonable volume of requests,

4
00:00:10,30 --> 00:00:12,80
but it requires use of an API key,

5
00:00:12,80 --> 00:00:15,80
so we've got a register link right here,

6
00:00:15,80 --> 00:00:17,90
and I click that, and just have to fill

7
00:00:17,90 --> 00:00:20,50
in first name

8
00:00:20,50 --> 00:00:22,70
and my email.

9
00:00:22,70 --> 00:00:24,50
And you can tell 'em how you want to use it,

10
00:00:24,50 --> 00:00:27,40
but you don't have to, so we'll just sign up.

11
00:00:27,40 --> 00:00:31,40
And so, we get back an API key.

12
00:00:31,40 --> 00:00:35,30
Again, I'm going to copy that and save it.

13
00:00:35,30 --> 00:00:40,30
So then, I'm going to switch over to the documentation.

14
00:00:40,30 --> 00:00:43,90
We have a bunch of endpoints listed on this page,

15
00:00:43,90 --> 00:00:46,80
including a parks endpoint that gives us park-related data,

16
00:00:46,80 --> 00:00:49,20
so that seems like a good candidate.

17
00:00:49,20 --> 00:00:52,30
And so, if I click the blue area here,

18
00:00:52,30 --> 00:00:55,00
we've got some information about the parameters,

19
00:00:55,00 --> 00:00:57,20
and there's a try it out option.

20
00:00:57,20 --> 00:00:59,70
But in order to use that, I need to authorize

21
00:00:59,70 --> 00:01:02,20
on this page using my API key,

22
00:01:02,20 --> 00:01:05,70
so I can just paste in the API key I just got,

23
00:01:05,70 --> 00:01:10,00
and now I am authorized so that any test requests

24
00:01:10,00 --> 00:01:12,70
I make on this page are going to use my API key.

25
00:01:12,70 --> 00:01:15,10
And we see these locks meaning that I'm authorized;

26
00:01:15,10 --> 00:01:16,90
these icons have changed.

27
00:01:16,90 --> 00:01:20,70
So now, I want to try this out and what I want is

28
00:01:20,70 --> 00:01:23,10
Parks for California, so reading through here,

29
00:01:23,10 --> 00:01:27,50
I can specify state code and get back parks from that state.

30
00:01:27,50 --> 00:01:31,80
So I will add item; the two-character state code

31
00:01:31,80 --> 00:01:36,00
for California is CA, and then I'm going to scroll down

32
00:01:36,00 --> 00:01:39,30
and hit this big execute button.

33
00:01:39,30 --> 00:01:45,30
And then the request is submitted, and the results.

34
00:01:45,30 --> 00:01:48,60
First, I get a curl statement with that URL,

35
00:01:48,60 --> 00:01:52,30
and I get a 200 response from the server and a response body

36
00:01:52,30 --> 00:01:54,70
showing all this beautiful information about parks.

37
00:01:54,70 --> 00:01:56,30
So, great!

38
00:01:56,30 --> 00:01:59,10
I can get the data I need and so, I'm going to go ahead

39
00:01:59,10 --> 00:02:04,60
and grab this URL, already includes my API key,

40
00:02:04,60 --> 00:02:08,00
and take everything but the quotes in the URL itself.

41
00:02:08,00 --> 00:02:10,40
I'm going to copy that, and then back

42
00:02:10,40 --> 00:02:13,50
over in my JavaScript file,

43
00:02:13,50 --> 00:02:17,40
I am going to change my existing URL to specifically,

44
00:02:17,40 --> 00:02:22,90
smartyUrl, then I'm going to create a new constant

45
00:02:22,90 --> 00:02:26,30
called parksUrl,

46
00:02:26,30 --> 00:02:28,40
and I am going to paste in

47
00:02:28,40 --> 00:02:31,60
as a string the URL that I just copied.

48
00:02:31,60 --> 00:02:35,10
An so, this includes my API key, it includes my endpoint,

49
00:02:35,10 --> 00:02:38,20
and it includes this key value pair for the state code

50
00:02:38,20 --> 00:02:42,40
to get data specifically about parks in California.

51
00:02:42,40 --> 00:02:45,00
Now, one of the great things about learning XHR is

52
00:02:45,00 --> 00:02:47,60
that once you've written the code once,

53
00:02:47,60 --> 00:02:50,20
you can pretty much reuse the same pattern.

54
00:02:50,20 --> 00:02:51,60
So, you may have discovered here,

55
00:02:51,60 --> 00:02:54,10
you don't really need to write any new code.

56
00:02:54,10 --> 00:02:56,80
You've already created a reusable set of functions,

57
00:02:56,80 --> 00:02:59,20
so all you have to do is pass in the new URL

58
00:02:59,20 --> 00:03:04,60
to the create request function, and you get data handled.

59
00:03:04,60 --> 00:03:06,20
So, I'm going to do a couple things here.

60
00:03:06,20 --> 00:03:11,20
I changed my URL variable to smartyUrl,

61
00:03:11,20 --> 00:03:12,60
so I'm going to change that,

62
00:03:12,60 --> 00:03:14,10
and then I'm going to comment this out.

63
00:03:14,10 --> 00:03:17,20
Since I only have 250 requests this month,

64
00:03:17,20 --> 00:03:20,10
I don't want to submit a request to SmartyStreets

65
00:03:20,10 --> 00:03:21,70
every time I load my page.

66
00:03:21,70 --> 00:03:23,00
So I'm going to comment that out,

67
00:03:23,00 --> 00:03:25,70
and then I'm just going to call createRequest again,

68
00:03:25,70 --> 00:03:29,30
and this time I'm going to pass it parksUrl,

69
00:03:29,30 --> 00:03:31,80
and I'm going to save those changes.

70
00:03:31,80 --> 00:03:34,30
I'm going to go to my html file,

71
00:03:34,30 --> 00:03:37,70
and I'm going to start up my live server,

72
00:03:37,70 --> 00:03:39,90
and open up my console.

73
00:03:39,90 --> 00:03:42,30
So, the parks API can take a few seconds

74
00:03:42,30 --> 00:03:45,40
between when you make the request and you get the data back,

75
00:03:45,40 --> 00:03:47,90
but notice I've got tons of data here.

76
00:03:47,90 --> 00:03:51,00
It is data about parks, so it looks like

77
00:03:51,00 --> 00:03:53,10
we got what we needed.

78
00:03:53,10 --> 00:03:55,80
And then I just want to double-check errors as well,

79
00:03:55,80 --> 00:03:59,30
so going back into my JavaScript file,

80
00:03:59,30 --> 00:04:04,40
I'm going to duplicate that parksUrl variable.

81
00:04:04,40 --> 00:04:07,30
I'm going to comment out one of them, and I'm going to take

82
00:04:07,30 --> 00:04:10,50
out a couple characters from that API key value.

83
00:04:10,50 --> 00:04:13,70
I'm going to save that, and then back in my browser,

84
00:04:13,70 --> 00:04:15,90
I got a 403 forbidden error.

85
00:04:15,90 --> 00:04:19,80
And I can see that my error-handling function

86
00:04:19,80 --> 00:04:24,50
is giving me the error code, followed by the error text,

87
00:04:24,50 --> 00:04:27,70
which is API_KEY_INVALID, and I get a specific message

88
00:04:27,70 --> 00:04:30,80
from NPS telling me exactly what's going on.

89
00:04:30,80 --> 00:04:32,80
That's super helpful.

90
00:04:32,80 --> 00:04:36,50
And so, going back here, deleting that broken URL,

91
00:04:36,50 --> 00:04:39,60
uncommenting the original good URL,

92
00:04:39,60 --> 00:04:41,80
and saving those changes,

93
00:04:41,80 --> 00:04:43,80
and then back in my browser once again,

94
00:04:43,80 --> 00:04:46,40
I get good data back, so we're all good.

95
00:04:46,40 --> 00:04:50,10
We've now got data coming in from the National Parks Service

96
00:04:50,10 --> 00:04:57,00
with just the addition of a new URL and a function call.

97
00:04:57,00 --> 00:04:59,30
So we've now got data from the National Parks Service

98
00:04:59,30 --> 00:05:03,00
using just a new URL and a function call.

