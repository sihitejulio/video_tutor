1
00:00:00,50 --> 00:00:02,50
- [Instructor] A client can experience a lag

2
00:00:02,50 --> 00:00:05,20
between submitting a request for data

3
00:00:05,20 --> 00:00:07,30
and receiving the response.

4
00:00:07,30 --> 00:00:10,00
For this reason, it's important to structure code

5
00:00:10,00 --> 00:00:14,00
to minimize the impact of this potential delay on users.

6
00:00:14,00 --> 00:00:16,40
We can build program flow in JavaScript

7
00:00:16,40 --> 00:00:20,50
in two different ways, synchronously and asynchronously.

8
00:00:20,50 --> 00:00:23,60
Using synchronous program flow, code is executed

9
00:00:23,60 --> 00:00:27,10
in the order it appears in your JavaScript files.

10
00:00:27,10 --> 00:00:29,00
So here, lines one through five

11
00:00:29,00 --> 00:00:31,30
of the code are executed first,

12
00:00:31,30 --> 00:00:33,30
followed by lines six through 10,

13
00:00:33,30 --> 00:00:36,90
and then lines 11 through 15 are executed next,

14
00:00:36,90 --> 00:00:40,30
matching the order of the statements in the code as written.

15
00:00:40,30 --> 00:00:43,40
In synchronous program flow, the browser parses

16
00:00:43,40 --> 00:00:46,40
and executes only one statement at a time.

17
00:00:46,40 --> 00:00:49,70
This results in our statements being executed in order.

18
00:00:49,70 --> 00:00:52,80
Synchronous program flow exhibits a behavior known

19
00:00:52,80 --> 00:00:55,80
as blocking, meaning that later statements are prevented

20
00:00:55,80 --> 00:00:59,00
from executing until earlier statements are finished.

21
00:00:59,00 --> 00:01:01,40
In many cases, this is what we want.

22
00:01:01,40 --> 00:01:04,20
For instance, a value needs to be calculated first

23
00:01:04,20 --> 00:01:06,20
before it can be used.

24
00:01:06,20 --> 00:01:09,10
But if an earlier statement may take a while to execute,

25
00:01:09,10 --> 00:01:11,10
and statements that follow don't rely

26
00:01:11,10 --> 00:01:13,50
on that earlier statement having finished,

27
00:01:13,50 --> 00:01:15,30
then synchronous program flow can

28
00:01:15,30 --> 00:01:17,60
needlessly slow down our apps.

29
00:01:17,60 --> 00:01:20,40
This is often the case with an Ajax request,

30
00:01:20,40 --> 00:01:23,40
which is why JavaScript treats an Ajax request

31
00:01:23,40 --> 00:01:26,20
as an asynchronous process by default.

32
00:01:26,20 --> 00:01:29,60
We want the client to make the Ajax request,

33
00:01:29,60 --> 00:01:32,10
but then be able to do other things,

34
00:01:32,10 --> 00:01:34,80
like respond to user initiated events,

35
00:01:34,80 --> 00:01:37,20
while waiting for the Ajax response.

36
00:01:37,20 --> 00:01:40,10
Otherwise, a slow response would essentially lock up

37
00:01:40,10 --> 00:01:42,50
our apps, which would create a bad experience

38
00:01:42,50 --> 00:01:44,20
that frustrates users.

39
00:01:44,20 --> 00:01:47,60
So this is an example of asynchronous program flow,

40
00:01:47,60 --> 00:01:51,00
also known as asynchronous code, or simply async.

41
00:01:51,00 --> 00:01:54,70
Here, lines one through five of the code start execution,

42
00:01:54,70 --> 00:01:56,10
but these include some activity

43
00:01:56,10 --> 00:01:57,60
that's going to take a while.

44
00:01:57,60 --> 00:02:00,70
Now, technically, JavaScript has only a single thread,

45
00:02:00,70 --> 00:02:03,50
but does have a mechanism that allows some processes

46
00:02:03,50 --> 00:02:07,00
to execute in parallel while other things are going on.

47
00:02:07,00 --> 00:02:09,80
So here, the first set of statements is moved

48
00:02:09,80 --> 00:02:13,20
into that parallel process, which continues executing

49
00:02:13,20 --> 00:02:15,50
even as the main program flow moves

50
00:02:15,50 --> 00:02:18,20
to the next set of statements.

51
00:02:18,20 --> 00:02:22,00
Now, that main program flow continues synchronous execution,

52
00:02:22,00 --> 00:02:24,30
so it keeps moving through the remaining code

53
00:02:24,30 --> 00:02:27,90
while that first block of code is executing in parallel.

54
00:02:27,90 --> 00:02:30,10
Now, remember that synchronous code is executed

55
00:02:30,10 --> 00:02:32,80
only in the main program flow, while we've seen

56
00:02:32,80 --> 00:02:35,10
that asynchronous code is instead moved over

57
00:02:35,10 --> 00:02:36,80
to execute in parallel.

58
00:02:36,80 --> 00:02:38,70
This means that those statements are

59
00:02:38,70 --> 00:02:42,50
essentially executed simultaneously.

60
00:02:42,50 --> 00:02:44,60
The upshot of this architecture is

61
00:02:44,60 --> 00:02:47,70
that asynchronous program flow lets us write code

62
00:02:47,70 --> 00:02:50,50
that is non-blocking, and this is super important

63
00:02:50,50 --> 00:02:53,00
for things like an Ajax request.

