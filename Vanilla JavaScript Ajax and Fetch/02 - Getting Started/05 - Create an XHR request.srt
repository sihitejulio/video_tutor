1
00:00:00,50 --> 00:00:02,50
- [Instructor] Ajax requests in JavaScript

2
00:00:02,50 --> 00:00:06,60
are based on the XMLHttpRequest object.

3
00:00:06,60 --> 00:00:08,40
Now that's a real mouthful.

4
00:00:08,40 --> 00:00:10,20
So this object is often referred to

5
00:00:10,20 --> 00:00:12,60
as the XHR object for short.

6
00:00:12,60 --> 00:00:17,00
You create an XHR object to open a request to a remote URL

7
00:00:17,00 --> 00:00:19,80
using an HTTP method such as get.

8
00:00:19,80 --> 00:00:22,60
When the XHR object was first created,

9
00:00:22,60 --> 00:00:26,50
XML was the standard format for exchanging data on the web.

10
00:00:26,50 --> 00:00:31,30
Fortunately, XML is tied to the XHR object in name only

11
00:00:31,30 --> 00:00:36,10
and XHR works with any data format including JSON.

12
00:00:36,10 --> 00:00:40,20
The XHR object includes quite a few properties and methods

13
00:00:40,20 --> 00:00:43,90
but a few particular features are used in most requests.

14
00:00:43,90 --> 00:00:47,70
You use the open method to specify an HTTP verb

15
00:00:47,70 --> 00:00:49,10
and a URL to hit.

16
00:00:49,10 --> 00:00:51,20
While this method attempts to open a connection

17
00:00:51,20 --> 00:00:53,10
with a specified endpoint,

18
00:00:53,10 --> 00:00:56,70
it doesn't actually send the HTTP request.

19
00:00:56,70 --> 00:00:59,60
To do that, you use the send method.

20
00:00:59,60 --> 00:01:01,70
Once a request has been sent,

21
00:01:01,70 --> 00:01:04,00
the value of the readyState property

22
00:01:04,00 --> 00:01:06,60
indicates the current state of the request

23
00:01:06,60 --> 00:01:09,40
using a number from zero to four.

24
00:01:09,40 --> 00:01:11,00
A state of zero indicates

25
00:01:11,00 --> 00:01:13,80
that the request has not yet been initialized.

26
00:01:13,80 --> 00:01:17,00
One indicates that the request has been set up.

27
00:01:17,00 --> 00:01:18,90
Two that it's been sent.

28
00:01:18,90 --> 00:01:23,40
Three that it's in process and four that it's complete.

29
00:01:23,40 --> 00:01:27,80
The XHR object also specifies a readystatechange event.

30
00:01:27,80 --> 00:01:31,20
XHR code commonly listens for this event to fire

31
00:01:31,20 --> 00:01:35,10
and then uses a conditional to check if the state is four

32
00:01:35,10 --> 00:01:37,20
indicating that the request is complete.

33
00:01:37,20 --> 00:01:40,90
In this case, code to work with the response is called.

34
00:01:40,90 --> 00:01:44,40
In the starting code for the Explore California form,

35
00:01:44,40 --> 00:01:46,00
I'm going to build a function

36
00:01:46,00 --> 00:01:50,60
to create and send an XHR request in the app.js file.

37
00:01:50,60 --> 00:01:53,00
So below my URL constant,

38
00:01:53,00 --> 00:01:58,40
I'm going to create a const called createRequest,

39
00:01:58,40 --> 00:01:59,40
that's going to be a function

40
00:01:59,40 --> 00:02:02,10
that takes a single parameter of URL,

41
00:02:02,10 --> 00:02:04,80
and that will allow us to pass a URL to the function

42
00:02:04,80 --> 00:02:06,90
so that we can customize a request

43
00:02:06,90 --> 00:02:08,10
and use the same function

44
00:02:08,10 --> 00:02:10,20
for multiple different web services.

45
00:02:10,20 --> 00:02:13,60
Now within the function, I want to create an XHR object.

46
00:02:13,60 --> 00:02:21,20
And to do that, I use the new keyword with XMLHttpRequest

47
00:02:21,20 --> 00:02:23,00
as the constructor

48
00:02:23,00 --> 00:02:26,30
and then I'm going to pass that the URL.

49
00:02:26,30 --> 00:02:28,30
Now if I just create this object,

50
00:02:28,30 --> 00:02:31,40
I don't have any way to work with it in my subsequent code.

51
00:02:31,40 --> 00:02:36,20
So I always want to create a variable, I'm going to use const,

52
00:02:36,20 --> 00:02:40,60
I'm going to call it httpRequest

53
00:02:40,60 --> 00:02:44,80
and now I can use that httpRequest variable name

54
00:02:44,80 --> 00:02:48,80
to reference and work with my XHR object.

55
00:02:48,80 --> 00:02:54,00
So I create a variable called httpRequest

56
00:02:54,00 --> 00:02:58,30
that gives me a reference to this XHR object

57
00:02:58,30 --> 00:03:03,00
and allows me to use it later on in my code.

58
00:03:03,00 --> 00:03:07,20
And then I want to add an event listener

59
00:03:07,20 --> 00:03:09,60
for the readystatechange event.

60
00:03:09,60 --> 00:03:15,30
So httpRequest so a reference to my XHR object

61
00:03:15,30 --> 00:03:19,30
.addEventListener and the event we're going to listen for

62
00:03:19,30 --> 00:03:21,10
is readystatechange

63
00:03:21,10 --> 00:03:23,50
and I'm going to use an in line anonymous function

64
00:03:23,50 --> 00:03:26,20
so I'm just going to use an arrow function for that

65
00:03:26,20 --> 00:03:31,50
and so we will pass in URL as a parameter

66
00:03:31,50 --> 00:03:41,30
and we're going to check if httpRequest.readyState

67
00:03:41,30 --> 00:03:45,70
triple equals four.

68
00:03:45,70 --> 00:03:47,40
And if that's the case,

69
00:03:47,40 --> 00:03:51,10
for now we're just going to console.log

70
00:03:51,10 --> 00:03:56,20
httpRequest.responseText.

71
00:03:56,20 --> 00:03:59,40
The responseText property gives us the text content

72
00:03:59,40 --> 00:04:01,50
returned from the remote service

73
00:04:01,50 --> 00:04:03,20
which could be our requested data

74
00:04:03,20 --> 00:04:05,30
or could be an error message.

75
00:04:05,30 --> 00:04:08,40
So this code will let us see in the console

76
00:04:08,40 --> 00:04:10,50
whether our request has succeeded.

77
00:04:10,50 --> 00:04:13,10
To finish the createRequest function,

78
00:04:13,10 --> 00:04:18,20
I need to open a request and I want to use the GET HTTP method

79
00:04:18,20 --> 00:04:20,40
and I'll use that variable I created above.

80
00:04:20,40 --> 00:04:29,00
So httpRequest.open and we're going to pass in GET and the URL.

81
00:04:29,00 --> 00:04:31,90
And remember, just opening it isn't enough.

82
00:04:31,90 --> 00:04:37,40
I need to follow that with httpRequest.send

83
00:04:37,40 --> 00:04:41,30
and that is going to actually send the request.

84
00:04:41,30 --> 00:04:45,10
And then finally outside of that function

85
00:04:45,10 --> 00:04:46,60
I need to call the function

86
00:04:46,60 --> 00:04:52,60
so we'll call createRequest and pass it in URL

87
00:04:52,60 --> 00:04:55,10
and I'll save that.

88
00:04:55,10 --> 00:05:00,20
So now we have code that actually creates an XHR object,

89
00:05:00,20 --> 00:05:04,80
sends a request and logs what it gets back

90
00:05:04,80 --> 00:05:07,40
and we have that function setup

91
00:05:07,40 --> 00:05:10,60
in such a way that we can pass in any URL

92
00:05:10,60 --> 00:05:13,00
and examine what we get back.

