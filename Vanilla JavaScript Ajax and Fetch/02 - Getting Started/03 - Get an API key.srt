1
00:00:00,50 --> 00:00:02,10
- [Instructor] In this course we'll be improving

2
00:00:02,10 --> 00:00:05,80
the usability of the sign up form for Explore California,

3
00:00:05,80 --> 00:00:07,60
a fictitious visitor's bureau.

4
00:00:07,60 --> 00:00:12,30
When a user enters a U.S. address, city and state,

5
00:00:12,30 --> 00:00:14,10
I want the form to automatically look up

6
00:00:14,10 --> 00:00:16,40
the corresponding nine digit zip code

7
00:00:16,40 --> 00:00:18,00
and fill that in for us.

8
00:00:18,00 --> 00:00:23,10
To do this, we'll make use of a web service.

9
00:00:23,10 --> 00:00:26,80
A wide variety of web services make data available.

10
00:00:26,80 --> 00:00:29,40
These include all sorts of subject areas

11
00:00:29,40 --> 00:00:32,90
from weather to Star Wars character references.

12
00:00:32,90 --> 00:00:35,90
Some are free while others require a payment.

13
00:00:35,90 --> 00:00:37,70
Todd Motto is a developer

14
00:00:37,70 --> 00:00:41,40
who maintains this pretty extensive list of free APIs.

15
00:00:41,40 --> 00:00:44,20
So scrolling down there's an index

16
00:00:44,20 --> 00:00:48,80
that gives us a list of subject areas.

17
00:00:48,80 --> 00:00:53,50
And we've got Geocoding which in general is how the data

18
00:00:53,50 --> 00:00:55,70
that we're looking for is described.

19
00:00:55,70 --> 00:00:59,20
So we have possible web services here

20
00:00:59,20 --> 00:01:02,30
from all over the world giving us specific data

21
00:01:02,30 --> 00:01:04,10
on specific parts of the world.

22
00:01:04,10 --> 00:01:06,50
And we are looking for U.S. data

23
00:01:06,50 --> 00:01:09,70
and we are specifically looking for zip code data.

24
00:01:09,70 --> 00:01:13,50
So I've looked through this list

25
00:01:13,50 --> 00:01:17,90
and I do see one that's interesting which is Zippopotam.

26
00:01:17,90 --> 00:01:22,10
And this is clearly a zip code API.

27
00:01:22,10 --> 00:01:26,00
And so we'll give this a go.

28
00:01:26,00 --> 00:01:29,80
So following that, it's zippopotam.us.

29
00:01:29,80 --> 00:01:33,00
So Zippopotamus and it's actually right here

30
00:01:33,00 --> 00:01:35,70
on the front page it's got kind of overview

31
00:01:35,70 --> 00:01:39,10
of what you can ask it and what it can give you.

32
00:01:39,10 --> 00:01:42,20
So it's an API with JSON response format.

33
00:01:42,20 --> 00:01:45,00
And we can send it a zip code

34
00:01:45,00 --> 00:01:49,00
and it's going to give us back some information

35
00:01:49,00 --> 00:01:50,80
about where that zip code is.

36
00:01:50,80 --> 00:01:53,50
Or we can send it a state and a city in the U.S.

37
00:01:53,50 --> 00:01:56,30
and get back that zip code.

38
00:01:56,30 --> 00:01:58,20
Now the thing is for our uses,

39
00:01:58,20 --> 00:02:01,30
we want to be able to send a street address, city and state

40
00:02:01,30 --> 00:02:03,30
and get a nine digit zip code back.

41
00:02:03,30 --> 00:02:06,50
And Zippopotamus doesn't actually do that for us.

42
00:02:06,50 --> 00:02:09,70
And so I've looked at other sources already

43
00:02:09,70 --> 00:02:13,40
in the Geocoding section on Todd Motto's list

44
00:02:13,40 --> 00:02:16,20
and unfortunately none of them are going to do what we want.

45
00:02:16,20 --> 00:02:18,70
So at this point I'm going to go web search

46
00:02:18,70 --> 00:02:21,90
and I'm just going to search on zip code API

47
00:02:21,90 --> 00:02:23,40
and there's a few options.

48
00:02:23,40 --> 00:02:25,00
So at this point, it's a question

49
00:02:25,00 --> 00:02:28,80
of just checking out the options and seeing what might work.

50
00:02:28,80 --> 00:02:33,20
So we've got this one, SmartyStreets that's a zip code API.

51
00:02:33,20 --> 00:02:36,10
I'm going to check that one out.

52
00:02:36,10 --> 00:02:38,30
And so scrolling through this documentation,

53
00:02:38,30 --> 00:02:41,80
I looked through this earlier and what I discovered is

54
00:02:41,80 --> 00:02:45,40
that we actually have a precision down to nine digits.

55
00:02:45,40 --> 00:02:47,70
So we can actually get nine digit zip codes

56
00:02:47,70 --> 00:02:49,60
out of this service.

57
00:02:49,60 --> 00:02:52,00
Now SmartyStreets doesn't appear to be free.

58
00:02:52,00 --> 00:02:53,90
But if we look over here on pricing,

59
00:02:53,90 --> 00:02:55,40
I'm going to United States.

60
00:02:55,40 --> 00:03:00,40
And we've got 250 lookups per month for free.

61
00:03:00,40 --> 00:03:01,70
So that should be enough

62
00:03:01,70 --> 00:03:03,50
to get us through building out this course.

63
00:03:03,50 --> 00:03:04,80
This is obviously a service

64
00:03:04,80 --> 00:03:08,30
where if we're actually implementing this in an app

65
00:03:08,30 --> 00:03:10,90
that has any user base at all,

66
00:03:10,90 --> 00:03:14,20
we're going to need to move up to a paid plan.

67
00:03:14,20 --> 00:03:16,20
But to be able to build out this course,

68
00:03:16,20 --> 00:03:18,50
we should be able to do that on the free plan.

69
00:03:18,50 --> 00:03:20,90
And so from this pricing page,

70
00:03:20,90 --> 00:03:24,50
if I go to Products in the menu and select APIs,

71
00:03:24,50 --> 00:03:27,30
I can see their selection of APIs.

72
00:03:27,30 --> 00:03:30,30
Now U.S. zip code API might seem like the one we want.

73
00:03:30,30 --> 00:03:33,00
It's actually U.S. street address API

74
00:03:33,00 --> 00:03:37,90
that is going to give us back the nine digit zip codes.

75
00:03:37,90 --> 00:03:41,10
And so they even have this demo here

76
00:03:41,10 --> 00:03:45,40
where we can click this arrow for Try a simple data set.

77
00:03:45,40 --> 00:03:48,30
And I'll just click this one in Belmont, Massachusets.

78
00:03:48,30 --> 00:03:50,80
And it's filling in basic information,

79
00:03:50,80 --> 00:03:53,20
street, city and state.

80
00:03:53,20 --> 00:03:56,20
And it shows me the request that it generated

81
00:03:56,20 --> 00:03:58,40
as well as the response.

82
00:03:58,40 --> 00:04:04,10
And so we look here and we have a nine digit zip code.

83
00:04:04,10 --> 00:04:07,00
And it's broken down into a five digit zip code

84
00:04:07,00 --> 00:04:09,00
and a four digit plus four.

85
00:04:09,00 --> 00:04:11,00
So that's exactly the data we need.

86
00:04:11,00 --> 00:04:13,70
So this API can do what we need.

87
00:04:13,70 --> 00:04:14,70
So great.

88
00:04:14,70 --> 00:04:17,50
Now looking in this demo,

89
00:04:17,50 --> 00:04:20,60
we can see that one of the fields is this auth-id.

90
00:04:20,60 --> 00:04:23,10
And this is super common with web services.

91
00:04:23,10 --> 00:04:25,90
And it's generically referred to as an API key.

92
00:04:25,90 --> 00:04:29,60
This is a string of characters associated with your account

93
00:04:29,60 --> 00:04:33,30
that you provide as part of any request you make to the API.

94
00:04:33,30 --> 00:04:34,50
For this sample page,

95
00:04:34,50 --> 00:04:37,40
they've created a general public auth-id

96
00:04:37,40 --> 00:04:40,00
that probably can only be used with this page.

97
00:04:40,00 --> 00:04:42,50
So in order to create my own requests,

98
00:04:42,50 --> 00:04:44,90
I'm going to have to sign up for my own auth-id

99
00:04:44,90 --> 00:04:48,10
on this website and you'll need to do the same.

100
00:04:48,10 --> 00:04:50,10
So scrolling back up,

101
00:04:50,10 --> 00:04:52,90
we have a Sign Up button at the top.

102
00:04:52,90 --> 00:04:55,60
I'm going to click that.

103
00:04:55,60 --> 00:04:58,20
And just going back over to the free section,

104
00:04:58,20 --> 00:04:59,80
click the Start Now.

105
00:04:59,80 --> 00:05:02,80
Okay, and so to sign up, I need to give them my first name,

106
00:05:02,80 --> 00:05:06,00
I need a phone number, we need an email address

107
00:05:06,00 --> 00:05:10,30
and then a password and then I'm going to create an account.

108
00:05:10,30 --> 00:05:12,20
So my account's all set.

109
00:05:12,20 --> 00:05:14,80
I'm going to click Start Using Your Account.

110
00:05:14,80 --> 00:05:19,30
And so scrolling down then we have this API Key section

111
00:05:19,30 --> 00:05:21,10
so I'm going to click that.

112
00:05:21,10 --> 00:05:23,90
And we have a couple different sections here.

113
00:05:23,90 --> 00:05:26,20
We have Website Keys for client-side code.

114
00:05:26,20 --> 00:05:28,80
We have Secret Keys for server-side code.

115
00:05:28,80 --> 00:05:32,60
So we're going to be working primarily with client-side code.

116
00:05:32,60 --> 00:05:35,70
And so I'm going to click this plus next to Create new key

117
00:05:35,70 --> 00:05:37,70
and the host name we use here is going to be dependent

118
00:05:37,70 --> 00:05:39,40
on our testing environment.

119
00:05:39,40 --> 00:05:43,30
For now, I'm going to use 127.0.0.1

120
00:05:43,30 --> 00:05:46,10
which is the IP address used for local testing.

121
00:05:46,10 --> 00:05:48,80
But if we need to adjust that later on we can do that.

122
00:05:48,80 --> 00:05:50,80
I'm going to go ahead and click Okay.

123
00:05:50,80 --> 00:05:53,80
And now I have a website key

124
00:05:53,80 --> 00:05:56,00
which is this string of numbers here.

125
00:05:56,00 --> 00:06:00,40
And I have a host that is associated with that.

126
00:06:00,40 --> 00:06:02,90
Note that this key, by the time you see this video

127
00:06:02,90 --> 00:06:04,40
this key will have been deactivated.

128
00:06:04,40 --> 00:06:07,50
So you'll need to make sure you get and use your own.

129
00:06:07,50 --> 00:06:10,40
So I find using a password manager to be a great way

130
00:06:10,40 --> 00:06:12,50
to stay on top of web credentials.

131
00:06:12,50 --> 00:06:15,30
And if you are using that system,

132
00:06:15,30 --> 00:06:17,80
that is a great place to stick your API key.

133
00:06:17,80 --> 00:06:20,80
For now I'm just going to copy it.

134
00:06:20,80 --> 00:06:23,80
I'm going to open Notes 'cause I'm on a Mac.

135
00:06:23,80 --> 00:06:25,50
And I'm going to create a new note

136
00:06:25,50 --> 00:06:33,60
which is SmartyStreets API key and paste that in.

137
00:06:33,60 --> 00:06:36,20
And now any time I need to grab my API key

138
00:06:36,20 --> 00:06:39,00
if I need to use it in files,

139
00:06:39,00 --> 00:06:42,00
I can simply come to this document and copy and paste it in.

