1
00:00:00,50 --> 00:00:02,30
- [Instructor] I'm going to create and configure

2
00:00:02,30 --> 00:00:06,20
an HX proxy for the Explore California contact page.

3
00:00:06,20 --> 00:00:08,80
I'm going to use Heroku to host my proxy,

4
00:00:08,80 --> 00:00:11,80
which is a popular service for hosting apps in the cloud.

5
00:00:11,80 --> 00:00:13,40
This process is going to draw

6
00:00:13,40 --> 00:00:15,80
on a few more advanced developer skills,

7
00:00:15,80 --> 00:00:18,20
which I'll assume you have some experience with.

8
00:00:18,20 --> 00:00:20,70
I'll be cloning a repo from GitHub.

9
00:00:20,70 --> 00:00:23,20
I'll also be using Git at the command line

10
00:00:23,20 --> 00:00:25,60
to perform some basic commands.

11
00:00:25,60 --> 00:00:29,30
And finally, I'll be working with the Heroku Dashboard.

12
00:00:29,30 --> 00:00:31,40
I'll be walking through each of these

13
00:00:31,40 --> 00:00:33,90
but if you have questions I'm not answering here,

14
00:00:33,90 --> 00:00:36,60
I encourage you to find another course in the library

15
00:00:36,60 --> 00:00:39,80
that focuses on that topic for a deeper dive.

16
00:00:39,80 --> 00:00:42,10
You'll need to have the Git command line utility

17
00:00:42,10 --> 00:00:43,70
installed on your machine,

18
00:00:43,70 --> 00:00:46,80
as well as a free account set up with heroku.com.

19
00:00:46,80 --> 00:00:48,90
So if you need to take care of any of that,

20
00:00:48,90 --> 00:00:50,00
go ahead and do it now.

