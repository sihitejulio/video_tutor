1
00:00:00,50 --> 00:00:02,40
- [Instructor] Next, I need to build out the code

2
00:00:02,40 --> 00:00:04,50
for the proxy itself.

3
00:00:04,50 --> 00:00:06,40
I've created a project on GitHub

4
00:00:06,40 --> 00:00:08,50
that's a basic proxy server.

5
00:00:08,50 --> 00:00:12,00
It's customizable for a wide variety of uses.

6
00:00:12,00 --> 00:00:14,70
For the Explore California contact page,

7
00:00:14,70 --> 00:00:18,20
I'll clone this repo and customize it for my needs,

8
00:00:18,20 --> 00:00:20,10
and then deploy it to Heroku.

9
00:00:20,10 --> 00:00:21,90
To clone a repo from GitHub,

10
00:00:21,90 --> 00:00:25,00
I'd click the green clone or download button,

11
00:00:25,00 --> 00:00:28,30
and then I'll click the copy icon next to the text box,

12
00:00:28,30 --> 00:00:31,50
which copies it to my clipboard.

13
00:00:31,50 --> 00:00:33,60
My next stop is the terminal app.

14
00:00:33,60 --> 00:00:36,00
I'm in a Mac so, I'm using terminal,

15
00:00:36,00 --> 00:00:38,50
and if you're in Linux that's pretty straight forward.

16
00:00:38,50 --> 00:00:41,00
If you are using Windows, you need to make sure

17
00:00:41,00 --> 00:00:42,40
you're in the correct application

18
00:00:42,40 --> 00:00:45,20
that allows you access to Git.

19
00:00:45,20 --> 00:00:49,80
I want to cd to the exercise files folder for this course,

20
00:00:49,80 --> 00:00:52,00
so, I'm going to type cd,

21
00:00:52,00 --> 00:00:54,20
then I can switch over to finder,

22
00:00:54,20 --> 00:00:56,00
grab that folder and drag it up here,

23
00:00:56,00 --> 00:00:57,80
and it'll give me that path.

24
00:00:57,80 --> 00:01:00,60
Just a super helpful little shortcut.

25
00:01:00,60 --> 00:01:06,00
So, I'm going to clone this repo into my exercise files folder,

26
00:01:06,00 --> 00:01:09,20
but you can clone the project to any location you like.

27
00:01:09,20 --> 00:01:12,10
The command is git clone, followed by a space,

28
00:01:12,10 --> 00:01:16,10
and then I'm going to paste in that address that I copied.

29
00:01:16,10 --> 00:01:17,30
And then I press enter.

30
00:01:17,30 --> 00:01:21,20
And the project is cloned into a sub-folder called ajaxproxy

31
00:01:21,20 --> 00:01:24,80
which is the name of the GitHub project that we're cloning.

32
00:01:24,80 --> 00:01:27,50
And now if I do LS to list my contents,

33
00:01:27,50 --> 00:01:29,80
I've got my chapter folders,

34
00:01:29,80 --> 00:01:32,10
as well as the ajaxproxy folder.

35
00:01:32,10 --> 00:01:35,20
The next thing I want to do is open the project in my editor.

36
00:01:35,20 --> 00:01:37,10
With visual studio code, I've enabled

37
00:01:37,10 --> 00:01:38,40
the command line shortcut.

38
00:01:38,40 --> 00:01:43,00
So I can just type code ajaxproxy/,

39
00:01:43,00 --> 00:01:45,30
can auto complete that with a tab,

40
00:01:45,30 --> 00:01:47,60
and then press enter and that will open

41
00:01:47,60 --> 00:01:51,10
up the project in visual studio code.

42
00:01:51,10 --> 00:01:53,40
The project includes several files,

43
00:01:53,40 --> 00:01:55,10
but to customize it, the only one

44
00:01:55,10 --> 00:01:59,20
I need to edit is index.js.

45
00:01:59,20 --> 00:02:00,60
This is a node file.

46
00:02:00,60 --> 00:02:03,80
Which means it's a file intended to run in the backend,

47
00:02:03,80 --> 00:02:07,10
but written in JavaScript.

48
00:02:07,10 --> 00:02:10,40
It includes two variables that I want to customize.

49
00:02:10,40 --> 00:02:13,20
Filter and apiOptions.

50
00:02:13,20 --> 00:02:16,60
For now, I want to focus on apiOptions.

51
00:02:16,60 --> 00:02:18,80
So, here in the filter variable,

52
00:02:18,80 --> 00:02:21,90
I'm going to comment out the return statement,

53
00:02:21,90 --> 00:02:26,70
and then, at the beginning of the function,

54
00:02:26,70 --> 00:02:28,20
I'm going to just create a statement

55
00:02:28,20 --> 00:02:30,50
that says return true.

56
00:02:30,50 --> 00:02:32,00
And I'll come back and work

57
00:02:32,00 --> 00:02:36,00
with this filter variable later on.

58
00:02:36,00 --> 00:02:38,50
Then, in the apiOptions object,

59
00:02:38,50 --> 00:02:40,30
I need to change a few properties.

60
00:02:40,30 --> 00:02:43,80
First, target needs to store the value

61
00:02:43,80 --> 00:02:47,50
of the remote api I want to make a request to.

62
00:02:47,50 --> 00:02:51,50
For the National Park Service, that's https,

63
00:02:51,50 --> 00:02:54,70
so that part's correct, and then I need

64
00:02:54,70 --> 00:03:00,20
developer.nps.gov.

65
00:03:00,20 --> 00:03:04,30
Next, the path rewrite value needs to specify

66
00:03:04,30 --> 00:03:07,90
what name I'm going to use for my proxy endpoint.

67
00:03:07,90 --> 00:03:11,20
One way to think of a proxy is as your own

68
00:03:11,20 --> 00:03:14,50
custom api that forwards requests.

69
00:03:14,50 --> 00:03:18,30
You can specify multiple endpoints just like any other api

70
00:03:18,30 --> 00:03:20,60
to provide different types of responses.

71
00:03:20,60 --> 00:03:24,20
The endpoint for my NPS requests is going to be nps,

72
00:03:24,20 --> 00:03:27,20
all lower case, so following the pattern here

73
00:03:27,20 --> 00:03:31,20
in the comment, I'm going to change

74
00:03:31,20 --> 00:03:36,10
the first slash to carrot nps.

75
00:03:36,10 --> 00:03:37,40
And I need to make sure there's a slash

76
00:03:37,40 --> 00:03:38,70
at the beginning as well.

77
00:03:38,70 --> 00:03:42,00
So it's carrot, slash, nps, slash, in the quotes,

78
00:03:42,00 --> 00:03:45,10
and I'm going to leave the second slash as it is.

79
00:03:45,10 --> 00:03:47,10
So this is just a pattern that recognizes

80
00:03:47,10 --> 00:03:50,30
that nps path does not need to be forwarded on

81
00:03:50,30 --> 00:03:52,20
to the National Park Service api.

82
00:03:52,20 --> 00:03:56,20
Finally, the code in the on Proxy request method,

83
00:03:56,20 --> 00:03:58,70
is what appends the key and value for my

84
00:03:58,70 --> 00:04:01,20
api credentials to the request.

85
00:04:01,20 --> 00:04:04,20
So first, I need to replace KEYNAME

86
00:04:04,20 --> 00:04:07,50
with the name of the key that the National Park Service

87
00:04:07,50 --> 00:04:11,70
expects in the query stream, to indicate the api key.

88
00:04:11,70 --> 00:04:15,10
And that's api_key.

89
00:04:15,10 --> 00:04:19,20
And then I need to replace KEYVALUE with the name

90
00:04:19,20 --> 00:04:22,10
I gave to my config var on Heroku.

91
00:04:22,10 --> 00:04:27,10
And that's NPS_APIKEY.

92
00:04:27,10 --> 00:04:30,80
So, anytime we reference a config var

93
00:04:30,80 --> 00:04:35,50
in our backend code, we use process.env dot

94
00:04:35,50 --> 00:04:38,50
and then the name of the config var.

95
00:04:38,50 --> 00:04:40,30
And then near the bottom of the app,

96
00:04:40,30 --> 00:04:42,70
there's one more thing to tweak.

97
00:04:42,70 --> 00:04:46,70
The app.use statement includes /api

98
00:04:46,70 --> 00:04:49,30
as a default proxy endpoint.

99
00:04:49,30 --> 00:04:51,80
But, I decided to use /nps,

100
00:04:51,80 --> 00:04:54,80
and I specified that above in my path rewrite statement.

101
00:04:54,80 --> 00:05:00,00
So here, I need to change /api to /nps.

102
00:05:00,00 --> 00:05:02,40
Those are all the changes I need to make,

103
00:05:02,40 --> 00:05:05,60
so I'll save, and now my proxy code is configured

104
00:05:05,60 --> 00:05:09,00
for the needs of the Explore California contact page.

