1
00:00:00,50 --> 00:00:01,80
- [Narrator] Now that my proxy code

2
00:00:01,80 --> 00:00:03,50
is configured for my needs.

3
00:00:03,50 --> 00:00:06,50
I need to Heroku, which will build,

4
00:00:06,50 --> 00:00:09,80
execute and host my proxy.

5
00:00:09,80 --> 00:00:13,40
So first off, I want to open up the dashboard for my app.

6
00:00:13,40 --> 00:00:15,00
So if you're not already there,

7
00:00:15,00 --> 00:00:20,00
click your app in your list of apps and then,

8
00:00:20,00 --> 00:00:22,80
I'm going to go over and choose deploy.

9
00:00:22,80 --> 00:00:25,10
And the deploy section actually gives

10
00:00:25,10 --> 00:00:28,10
me step by step instructions for how

11
00:00:28,10 --> 00:00:31,60
to deploy using the command line tools.

12
00:00:31,60 --> 00:00:33,60
So the first step, if you haven't already done it,

13
00:00:33,60 --> 00:00:36,10
is to download and install those command line tools.

14
00:00:36,10 --> 00:00:37,60
There is a link right there.

15
00:00:37,60 --> 00:00:40,20
I already have those installed, but if you don't,

16
00:00:40,20 --> 00:00:42,30
then go ahead and do that now.

17
00:00:42,30 --> 00:00:44,50
Now, I'm pretty sure I'm already logged in,

18
00:00:44,50 --> 00:00:45,50
but let me check.

19
00:00:45,50 --> 00:00:48,00
So that's a Heroku log in,

20
00:00:48,00 --> 00:00:52,90
and so back in my terminal, Heroku log in.

21
00:00:52,90 --> 00:00:55,10
And so I'm going to press a key.

22
00:00:55,10 --> 00:00:57,80
It's going to launch my browser,

23
00:00:57,80 --> 00:01:00,90
and since I'm already logged in to Heroku in my browser.

24
00:01:00,90 --> 00:01:03,70
All I have to do is click the log in button

25
00:01:03,70 --> 00:01:06,00
and that's going to make sure

26
00:01:06,00 --> 00:01:09,90
that I'm logged in my command line as well.

27
00:01:09,90 --> 00:01:15,00
And then I need to switch to the folder containing my,

28
00:01:15,00 --> 00:01:16,00
proxy code.

29
00:01:16,00 --> 00:01:17,60
So that's,

30
00:01:17,60 --> 00:01:18,70
cd,

31
00:01:18,70 --> 00:01:21,00
ajaxproxy.

32
00:01:21,00 --> 00:01:26,70
And then back on that deploy page on my Heroku dashboard.

33
00:01:26,70 --> 00:01:30,30
My cloned project is already initializes

34
00:01:30,30 --> 00:01:33,80
as you get repository because I downloaded it from GitHub.

35
00:01:33,80 --> 00:01:37,00
So all you need to do is copy this command,

36
00:01:37,00 --> 00:01:38,70
make sure you don't grab that dollar sign.

37
00:01:38,70 --> 00:01:42,40
That's just showing you that there's a command prompt there.

38
00:01:42,40 --> 00:01:46,20
And I'm going to paste that in, at my prompt.

39
00:01:46,20 --> 00:01:50,10
And so now, this repository has it's remote set

40
00:01:50,10 --> 00:01:52,10
to that project,

41
00:01:52,10 --> 00:01:52,90
on Heroku.

42
00:01:52,90 --> 00:01:56,60
On so next it's just a standard git work flow.

43
00:01:56,60 --> 00:01:59,90
First I do a git add .

44
00:01:59,90 --> 00:02:02,00
Then I do a git commit,

45
00:02:02,00 --> 00:02:03,30
- m,

46
00:02:03,30 --> 00:02:08,10
and in quotes I'll say 'initial commit'.

47
00:02:08,10 --> 00:02:10,50
And if this is your first time using git on your machine,

48
00:02:10,50 --> 00:02:12,10
you may see something like this if not,

49
00:02:12,10 --> 00:02:16,00
you won't see it, it's fine.

50
00:02:16,00 --> 00:02:18,00
And then finally, I just want

51
00:02:18,00 --> 00:02:22,70
to do a git push heroku master.

52
00:02:22,70 --> 00:02:29,70
And that is going to push the entire project, up to Heroku.

53
00:02:29,70 --> 00:02:32,80
Now once the files have been uploaded Heroku starts building

54
00:02:32,80 --> 00:02:35,60
the app and we're seeing the progress of that here.

55
00:02:35,60 --> 00:02:37,40
This can take a little bit.

56
00:02:37,40 --> 00:02:40,10
Now if there's any errors in your index that GS code,

57
00:02:40,10 --> 00:02:42,80
that may cause the build process to fail.

58
00:02:42,80 --> 00:02:45,50
So if that happens, you need to go back to that index

59
00:02:45,50 --> 00:02:47,30
that GS file in your editor.

60
00:02:47,30 --> 00:02:50,00
Find and fix those errors and then just go through

61
00:02:50,00 --> 00:02:53,30
the git add, git commit and git push again.

62
00:02:53,30 --> 00:02:56,00
To actually push those changes up to Heroku.

63
00:02:56,00 --> 00:03:00,30
So once we see that bash prompt, the dollar sign.

64
00:03:00,30 --> 00:03:02,50
I've got build succeeded right here.

65
00:03:02,50 --> 00:03:06,20
So that tells me I'm all good, and so know my custom code is

66
00:03:06,20 --> 00:03:08,70
hosted and running on Heroku

67
00:03:08,70 --> 00:03:11,40
and it's listening for HTTP requests.

68
00:03:11,40 --> 00:03:14,40
To actually test my code, I need to make a change

69
00:03:14,40 --> 00:03:17,50
to my front end ajax code.

70
00:03:17,50 --> 00:03:21,00
Right now, my front end code includes a URL,

71
00:03:21,00 --> 00:03:24,50
that makes a direct request to the NPSAPI

72
00:03:24,50 --> 00:03:26,30
and includes the API key.

73
00:03:26,30 --> 00:03:28,90
And that's starred right here in the parksUrl variable.

74
00:03:28,90 --> 00:03:31,20
And I need to change both of those things.

75
00:03:31,20 --> 00:03:35,40
So, I'm going to grab that parksUrl, variable,

76
00:03:35,40 --> 00:03:37,10
copy it,

77
00:03:37,10 --> 00:03:38,90
duplicate it,

78
00:03:38,90 --> 00:03:40,10
comment out the original,

79
00:03:40,10 --> 00:03:42,70
to keep that for reference.

80
00:03:42,70 --> 00:03:46,40
And first off, I need to replace the base URL,

81
00:03:46,40 --> 00:03:47,70
the end point.

82
00:03:47,70 --> 00:03:48,90
I'll take out,

83
00:03:48,90 --> 00:03:52,20
developer.nps.gov,

84
00:03:52,20 --> 00:03:56,30
and instead I need to insert the URL from my Heroku app.

85
00:03:56,30 --> 00:03:59,90
So just that the name of the app.herokuapp.com.

86
00:03:59,90 --> 00:04:05,70
So for me that's, ecajaxproxy.herokuapp.com.

87
00:04:05,70 --> 00:04:09,20
Bur make sure that you get the name of your app,

88
00:04:09,20 --> 00:04:13,00
that you created in Heroku and use that here instead.

89
00:04:13,00 --> 00:04:16,80
And then after that, I need to put /nps

90
00:04:16,80 --> 00:04:20,40
which is the name of the end point that I created.

91
00:04:20,40 --> 00:04:23,30
Finally, I need to take out the API key.

92
00:04:23,30 --> 00:04:26,50
Remember that my back end proxy code running on Heroku

93
00:04:26,50 --> 00:04:27,60
will append,

94
00:04:27,60 --> 00:04:29,80
api_key,

95
00:04:29,80 --> 00:04:32,40
plus the value to the end of my URL

96
00:04:32,40 --> 00:04:35,90
before forwarding it to National Park Service.

97
00:04:35,90 --> 00:04:36,70
So,

98
00:04:36,70 --> 00:04:39,50
here, I'll delete, and append,

99
00:04:39,50 --> 00:04:41,30
api_key=

100
00:04:41,30 --> 00:04:44,90
and that entire API key string.

101
00:04:44,90 --> 00:04:48,00
So I got stateCode=ca and my closing quote.

102
00:04:48,00 --> 00:04:52,80
And then save this changes and then the moment of truth.

103
00:04:52,80 --> 00:04:57,70
I'll start up, live server for contact at HTM.

104
00:04:57,70 --> 00:05:00,90
And there's a pause, which is a good sign.

105
00:05:00,90 --> 00:05:04,40
Waiting to get data back.

106
00:05:04,40 --> 00:05:06,80
And it looks like a good I got a random park,

107
00:05:06,80 --> 00:05:10,30
rather than that hard coded fallback, so that a good sign.

108
00:05:10,30 --> 00:05:13,50
And, opening up the developer tools,

109
00:05:13,50 --> 00:05:17,50
and I'm going to check out the network tab.

110
00:05:17,50 --> 00:05:19,10
Go ahead and reload the page to get

111
00:05:19,10 --> 00:05:21,90
that network information logged.

112
00:05:21,90 --> 00:05:27,10
And I will go full-screen, with my developer tools.

113
00:05:27,10 --> 00:05:29,30
And down here, in my parks request,

114
00:05:29,30 --> 00:05:32,60
you can see that I have a 200.

115
00:05:32,60 --> 00:05:34,20
And if I open this up this up,

116
00:05:34,20 --> 00:05:36,70
there is my request to my proxy.

117
00:05:36,70 --> 00:05:38,30
And looking all they way through here,

118
00:05:38,30 --> 00:05:42,80
nowhere in this data is my API key.

119
00:05:42,80 --> 00:05:45,20
Here is the data, I got back.

120
00:05:45,20 --> 00:05:48,50
Here is that response, and so,

121
00:05:48,50 --> 00:05:50,70
the client, which is the browser.

122
00:05:50,70 --> 00:05:55,00
Never got the API key, and yet, it did get data back

123
00:05:55,00 --> 00:05:58,00
and that's exactly what a proxy is suppose to do.

