1
00:00:00,50 --> 00:00:03,30
- [Instructor] Setting up a proxy to store your API keys

2
00:00:03,30 --> 00:00:05,10
and append them to a request

3
00:00:05,10 --> 00:00:07,40
keeps your credentials out of the front end

4
00:00:07,40 --> 00:00:10,50
which is a major step forward for keeping them secret.

5
00:00:10,50 --> 00:00:13,30
However, what if an enterprising user

6
00:00:13,30 --> 00:00:16,60
examined your HTTP request to your proxy

7
00:00:16,60 --> 00:00:19,30
and duplicated it in their own app?

8
00:00:19,30 --> 00:00:23,50
As long as their request URL matched what the proxy expects,

9
00:00:23,50 --> 00:00:27,10
the proxy would do its job, append your API key,

10
00:00:27,10 --> 00:00:30,30
forward the request and then send along the response

11
00:00:30,30 --> 00:00:32,40
back to the app that had asked for it.

12
00:00:32,40 --> 00:00:34,20
Although other developers wouldn't have

13
00:00:34,20 --> 00:00:37,00
your actual credentials, they'd still be able

14
00:00:37,00 --> 00:00:40,20
to take advantage of your access to the target web service,

15
00:00:40,20 --> 00:00:42,30
and if you're paying for that service

16
00:00:42,30 --> 00:00:45,00
or trying to stay under a small free limit,

17
00:00:45,00 --> 00:00:47,60
you'd eventually get a higher bill than you expected

18
00:00:47,60 --> 00:00:50,30
or you'd have your requests cut off more quickly

19
00:00:50,30 --> 00:00:51,40
when you hit that limit.

20
00:00:51,40 --> 00:00:54,70
You can prevent this situation by configuring your proxy

21
00:00:54,70 --> 00:00:57,50
to check the origin of each request.

22
00:00:57,50 --> 00:01:02,50
In HTTP, the origin identifies where a request came from.

23
00:01:02,50 --> 00:01:05,90
An origin includes the protocol, any subdomain,

24
00:01:05,90 --> 00:01:08,30
the domain and the port number.

25
00:01:08,30 --> 00:01:10,10
A difference between two origins

26
00:01:10,10 --> 00:01:13,30
in even one of these factors makes them distinct.

27
00:01:13,30 --> 00:01:16,60
Every HTTP request includes the origin,

28
00:01:16,60 --> 00:01:19,60
and this value cannot be altered programmatically

29
00:01:19,60 --> 00:01:21,10
from a browser.

30
00:01:21,10 --> 00:01:23,60
This means you can use the origin as a mechanism

31
00:01:23,60 --> 00:01:26,80
for securing your proxy server by including logic

32
00:01:26,80 --> 00:01:28,90
to check the origin of each request

33
00:01:28,90 --> 00:01:31,10
against a list you provide.

34
00:01:31,10 --> 00:01:33,60
This ensures that requests from your app,

35
00:01:33,60 --> 00:01:36,60
whose origin you've specified in the backend code,

36
00:01:36,60 --> 00:01:40,90
are allowed while requests from other origins are blocked.

37
00:01:40,90 --> 00:01:44,20
The Ajax proxy app includes a filter variable

38
00:01:44,20 --> 00:01:46,80
that allows you to specify one or more origins

39
00:01:46,80 --> 00:01:48,90
that should receive access.

40
00:01:48,90 --> 00:01:51,10
The origin of every request is checked

41
00:01:51,10 --> 00:01:54,20
against the origin or origins you specify,

42
00:01:54,20 --> 00:01:58,20
and all requests from origins not on your list are rejected.

43
00:01:58,20 --> 00:02:02,10
The code contains two versions of the return statement.

44
00:02:02,10 --> 00:02:04,30
Both are commented out right now.

45
00:02:04,30 --> 00:02:10,70
I am currently running the file locally from 127.0.0.1:5500

46
00:02:10,70 --> 00:02:12,90
which is the port, and I want to allow

47
00:02:12,90 --> 00:02:17,10
both HTTP and HTTPS versions of that.

48
00:02:17,10 --> 00:02:19,60
So first, I'm going to delete the return true statement

49
00:02:19,60 --> 00:02:21,80
that I added earlier because I only need

50
00:02:21,80 --> 00:02:24,70
one return statement, and I'm going to uncomment

51
00:02:24,70 --> 00:02:27,60
this last line which is the return statement

52
00:02:27,60 --> 00:02:30,80
that allows me to specify multiple origins.

53
00:02:30,80 --> 00:02:33,10
And then for the first origin, I'm going to take out

54
00:02:33,10 --> 00:02:42,40
that example URL, and I'm going to enter HTTP 127.0.0.1:5500,

55
00:02:42,40 --> 00:02:44,50
and then I'm going to copy that.

56
00:02:44,50 --> 00:02:47,20
I'm going to paste it in for the second one,

57
00:02:47,20 --> 00:02:52,50
but I'm going to change that HTTP to HTTPS because remember

58
00:02:52,50 --> 00:02:55,30
the protocol is part of the origin,

59
00:02:55,30 --> 00:02:58,10
and so each of these URLs is considered

60
00:02:58,10 --> 00:03:00,60
a distinct and different origin.

61
00:03:00,60 --> 00:03:02,10
I'm going to save that.

62
00:03:02,10 --> 00:03:04,50
I'm going to go back to my terminal.

63
00:03:04,50 --> 00:03:07,60
We'll do a git status just to see where we are,

64
00:03:07,60 --> 00:03:10,50
and I do have that one change I just made,

65
00:03:10,50 --> 00:03:14,80
so I'll do a git add, do a git add dot.

66
00:03:14,80 --> 00:03:15,70
Git is really useful.

67
00:03:15,70 --> 00:03:19,20
It's very good at noticing what you may have meant

68
00:03:19,20 --> 00:03:21,50
when you make an error and suggesting it.

69
00:03:21,50 --> 00:03:22,70
I did a git add dot.

70
00:03:22,70 --> 00:03:25,50
Then, I want a git commit -m,

71
00:03:25,50 --> 00:03:29,90
and I'm going to say added origins to filter

72
00:03:29,90 --> 00:03:36,60
and then a git push heroku master.

73
00:03:36,60 --> 00:03:38,80
Again, we're building out that app,

74
00:03:38,80 --> 00:03:41,20
getting to watch in real time as that happens,

75
00:03:41,20 --> 00:03:43,60
and as long as I don't have any errors pop up here.

76
00:03:43,60 --> 00:03:47,60
It says the build succeeded, so I'm all good

77
00:03:47,60 --> 00:03:50,80
once I get my Bash prompt back, and there it is.

78
00:03:50,80 --> 00:03:53,30
Now I know I didn't break anything.

79
00:03:53,30 --> 00:03:56,00
Now I want to test whether this really works.

80
00:03:56,00 --> 00:03:59,20
One easy way to change the origin while I'm testing is

81
00:03:59,20 --> 00:04:02,40
to change the port that my live server serves from.

82
00:04:02,40 --> 00:04:05,30
So, I'm going to go back to Visual Studio Code,

83
00:04:05,30 --> 00:04:08,40
and I've killed my live server already.

84
00:04:08,40 --> 00:04:11,00
I'm going to go into the configuration

85
00:04:11,00 --> 00:04:13,30
and specify a different port.

86
00:04:13,30 --> 00:04:15,50
So, I want to open the command palette,

87
00:04:15,50 --> 00:04:18,10
on a Mac that's Command + Shift + P,

88
00:04:18,10 --> 00:04:21,30
and then I'm going to type settings and json,

89
00:04:21,30 --> 00:04:24,30
and that gives me the two options for JSON files

90
00:04:24,30 --> 00:04:25,60
where I can enter settings.

91
00:04:25,60 --> 00:04:26,90
I don't want the default settings.

92
00:04:26,90 --> 00:04:29,80
I want just open settings

93
00:04:29,80 --> 00:04:32,50
These are a few things that I've set.

94
00:04:32,50 --> 00:04:34,80
After this last line, I'm going to add a comma,

95
00:04:34,80 --> 00:04:36,90
go to the next line, and I have to use double quotes

96
00:04:36,90 --> 00:04:39,80
'cause this is JSON, and I want liveServer,

97
00:04:39,80 --> 00:04:46,10
with capital S, .settings.port: 6500.

98
00:04:46,10 --> 00:04:49,40
Remember, no comma at the end because this is JSON.

99
00:04:49,40 --> 00:04:51,90
I'm going to save that and then switching over

100
00:04:51,90 --> 00:04:56,00
to contact.htm in my front end code,

101
00:04:56,00 --> 00:05:00,20
and I need to kill my live server over here,

102
00:05:00,20 --> 00:05:02,60
and I'm going to go live.

103
00:05:02,60 --> 00:05:06,60
And, I can see that I'm serving now from port 6500.

104
00:05:06,60 --> 00:05:09,30
I can see that I pretty quickly got my backup data display

105
00:05:09,30 --> 00:05:11,70
which suggests the request didn't work,

106
00:05:11,70 --> 00:05:14,10
so I'm going to check in my console,

107
00:05:14,10 --> 00:05:17,70
and I got a 404 error from my proxy,

108
00:05:17,70 --> 00:05:20,20
and that is exactly what I expect.

109
00:05:20,20 --> 00:05:21,60
My request didn't go through,

110
00:05:21,60 --> 00:05:23,90
and that's because my origin now

111
00:05:23,90 --> 00:05:26,80
with that different port number doesn't match the origins

112
00:05:26,80 --> 00:05:30,30
that I specified in my proxy server configuration.

113
00:05:30,30 --> 00:05:32,60
This is exactly what I want to see.

114
00:05:32,60 --> 00:05:35,40
So to get my origin back to the correct one,

115
00:05:35,40 --> 00:05:37,40
I'm going to go back to my editor.

116
00:05:37,40 --> 00:05:41,20
I'm going to end that live server session,

117
00:05:41,20 --> 00:05:44,10
and back in my settings.json file,

118
00:05:44,10 --> 00:05:46,70
I'm just going to delete that configuration line

119
00:05:46,70 --> 00:05:49,30
for liveServer.settings.port,

120
00:05:49,30 --> 00:05:51,40
take that comma out of the previous line

121
00:05:51,40 --> 00:05:54,90
because, again, JSON doesn't like it if you do that.

122
00:05:54,90 --> 00:05:59,50
I'm going to save that file and then back in contact.htm,

123
00:05:59,50 --> 00:06:01,70
I'm going to go live with that again.

124
00:06:01,70 --> 00:06:03,70
Now, we're at port 5500.

125
00:06:03,70 --> 00:06:06,50
I do not have data immediately which is a good sign,

126
00:06:06,50 --> 00:06:10,00
and then when it shows up, it is randomized data

127
00:06:10,00 --> 00:06:14,10
from the park service, and I have no errors here.

128
00:06:14,10 --> 00:06:17,50
I have my data object logged, and so I'm good.

129
00:06:17,50 --> 00:06:21,90
So, now I have my proxy secured against casual misuse.

130
00:06:21,90 --> 00:06:25,80
Obviously, 127.0.0.1 is an address

131
00:06:25,80 --> 00:06:27,90
that anyone could run locally,

132
00:06:27,90 --> 00:06:30,50
so this works for me in testing only.

133
00:06:30,50 --> 00:06:32,00
But when my app is finished

134
00:06:32,00 --> 00:06:34,50
and I'm ready to deploy to a domain,

135
00:06:34,50 --> 00:06:38,00
I'll change my proxy settings to use that domain name.

