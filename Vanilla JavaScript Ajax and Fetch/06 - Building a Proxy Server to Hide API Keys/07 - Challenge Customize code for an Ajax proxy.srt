1
00:00:00,30 --> 00:00:05,80
(funky music)

2
00:00:05,80 --> 00:00:07,50
- [Instructor] Ready for a challenge?

3
00:00:07,50 --> 00:00:08,90
It's time to try your hand

4
00:00:08,90 --> 00:00:11,40
at configuring an Ajax proxy server.

5
00:00:11,40 --> 00:00:13,60
The Explore California contact page

6
00:00:13,60 --> 00:00:17,10
includes a second Ajax call to Smarty Streets.

7
00:00:17,10 --> 00:00:19,30
Now the API credentials for this service

8
00:00:19,30 --> 00:00:22,90
are already locked only to the origins you specify.

9
00:00:22,90 --> 00:00:26,30
So it's actually okay to make them available to the client.

10
00:00:26,30 --> 00:00:29,50
But because you already have an Ajax proxy running,

11
00:00:29,50 --> 00:00:32,10
and you have one set of credentials stored there,

12
00:00:32,10 --> 00:00:34,10
it makes some sense organizationally

13
00:00:34,10 --> 00:00:36,80
to store your other credentials there as well.

14
00:00:36,80 --> 00:00:38,60
Then you always know where to look

15
00:00:38,60 --> 00:00:41,10
for the API credentials used in your app.

16
00:00:41,10 --> 00:00:44,10
To complete this challenge, you'll add another endpoint

17
00:00:44,10 --> 00:00:46,20
to your Ajax proxy server

18
00:00:46,20 --> 00:00:48,10
for the Smarty Streets API.

19
00:00:48,10 --> 00:00:51,00
You'll need to duplicate the apiOptions variable

20
00:00:51,00 --> 00:00:53,00
with a unique name to store the values

21
00:00:53,00 --> 00:00:54,80
for the Smarty Streets request.

22
00:00:54,80 --> 00:00:57,60
Then you'll need to duplicate the apiProxy variable

23
00:00:57,60 --> 00:00:59,00
with another unique name

24
00:00:59,00 --> 00:01:02,80
and swap in the name of your new apiOptions duplicate.

25
00:01:02,80 --> 00:01:06,10
Finally, you'll need to duplicate the app.use statement

26
00:01:06,10 --> 00:01:08,20
to reference a unique endpoint

27
00:01:08,20 --> 00:01:09,90
and then call the unique duplicate

28
00:01:09,90 --> 00:01:12,40
you made of apiProxy earlier.

29
00:01:12,40 --> 00:01:15,40
This challenge might take you about 10 to 15 minutes.

30
00:01:15,40 --> 00:01:18,20
When you're done, join me in the next video

31
00:01:18,20 --> 00:01:20,00
and I'll work you through how I approached it.

