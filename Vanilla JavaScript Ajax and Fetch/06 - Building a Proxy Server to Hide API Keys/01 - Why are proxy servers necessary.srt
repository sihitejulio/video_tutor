1
00:00:00,50 --> 00:00:02,70
- [Instructor] Building out a basic Ajax request

2
00:00:02,70 --> 00:00:04,90
in front end code is a common practice

3
00:00:04,90 --> 00:00:06,60
when developing an app.

4
00:00:06,60 --> 00:00:10,30
It's a quick and easy process for trying out the request,

5
00:00:10,30 --> 00:00:12,40
writing code to work with the response

6
00:00:12,40 --> 00:00:14,60
and building out your app around that.

7
00:00:14,60 --> 00:00:18,50
But in many cases before you deploy an app for public use,

8
00:00:18,50 --> 00:00:21,00
it's necessary to set up a proxy server

9
00:00:21,00 --> 00:00:23,20
to handle your Ajax requests.

10
00:00:23,20 --> 00:00:26,20
So why are proxy servers necessary?

11
00:00:26,20 --> 00:00:30,70
A lot of APIs require the use of credentials like API keys.

12
00:00:30,70 --> 00:00:33,20
Without including a key in your request,

13
00:00:33,20 --> 00:00:35,40
the server returns a 401 error

14
00:00:35,40 --> 00:00:37,40
indicating that your app isn't authorized

15
00:00:37,40 --> 00:00:39,60
to access the requested data.

16
00:00:39,60 --> 00:00:41,00
To get authorization,

17
00:00:41,00 --> 00:00:44,10
you include your API key as part of your request

18
00:00:44,10 --> 00:00:46,80
either in the query string or as a header.

19
00:00:46,80 --> 00:00:49,30
This lets the web service associate your request

20
00:00:49,30 --> 00:00:50,60
with your account.

21
00:00:50,60 --> 00:00:52,40
As long as you're still below any quota

22
00:00:52,40 --> 00:00:53,90
or limit on your account,

23
00:00:53,90 --> 00:00:57,50
the service sends an OK response with the data included.

24
00:00:57,50 --> 00:00:59,20
The problem with this setup

25
00:00:59,20 --> 00:01:02,70
is that when you include your API key in the front end code,

26
00:01:02,70 --> 00:01:06,20
an end user who understands the basics of HTTP

27
00:01:06,20 --> 00:01:08,50
and knows how to open developer tools

28
00:01:08,50 --> 00:01:12,50
can access your API key and use it for their own apps.

29
00:01:12,50 --> 00:01:16,00
This means their requests will count against your quota

30
00:01:16,00 --> 00:01:17,40
potentially costing you money

31
00:01:17,40 --> 00:01:19,40
if you're paying for the web service.

32
00:01:19,40 --> 00:01:22,50
It also means that if other users misuse your keys,

33
00:01:22,50 --> 00:01:24,40
for instance incorporating the data

34
00:01:24,40 --> 00:01:27,20
in an app that's outside the terms of service,

35
00:01:27,20 --> 00:01:28,60
the service would look to you

36
00:01:28,60 --> 00:01:31,90
as the owner of that credential as the responsible party.

37
00:01:31,90 --> 00:01:34,20
At a minimum, your credential would be revoked

38
00:01:34,20 --> 00:01:36,80
and your account might even be shut down.

39
00:01:36,80 --> 00:01:39,70
And this is where proxies come into the picture.

40
00:01:39,70 --> 00:01:43,40
A proxy is simply a web server that receives the request,

41
00:01:43,40 --> 00:01:47,30
makes some modification to it and then forwards it along.

42
00:01:47,30 --> 00:01:49,90
You can set up and configure your own proxy server

43
00:01:49,90 --> 00:01:52,00
to store your API credentials

44
00:01:52,00 --> 00:01:55,30
and then append them to requests that it receives.

45
00:01:55,30 --> 00:01:57,50
You can then rewrite the Ajax requests

46
00:01:57,50 --> 00:02:00,70
in your front end code to target your proxy server

47
00:02:00,70 --> 00:02:02,20
which will add your credentials

48
00:02:02,20 --> 00:02:05,50
and redirect a request to the target API.

49
00:02:05,50 --> 00:02:08,70
The proxy also then receives the response from the API

50
00:02:08,70 --> 00:02:10,80
and forwards it back to the client.

51
00:02:10,80 --> 00:02:12,00
Throughout this process,

52
00:02:12,00 --> 00:02:15,00
your private credentials never pass through the browser

53
00:02:15,00 --> 00:02:18,90
so users are unable to view and possibly misuse them.

54
00:02:18,90 --> 00:02:20,60
Proxies are super common

55
00:02:20,60 --> 00:02:25,00
and an important tool for web apps that incorporate Ajax.

56
00:02:25,00 --> 00:02:29,10
Note that not all APIs require the use of a proxy.

57
00:02:29,10 --> 00:02:31,30
Instead, some services require you

58
00:02:31,30 --> 00:02:34,60
to associate your credentials with one or more domains.

59
00:02:34,60 --> 00:02:36,50
The service then checks each request

60
00:02:36,50 --> 00:02:38,10
that includes your API key

61
00:02:38,10 --> 00:02:40,60
to ensure that it originated at your domain

62
00:02:40,60 --> 00:02:45,90
and fulfills your request when the API and the origin match.

63
00:02:45,90 --> 00:02:49,90
However, the service rejects requests from other origins.

64
00:02:49,90 --> 00:02:52,00
Users of your app do have access

65
00:02:52,00 --> 00:02:55,30
to the API key in their browsers if they dig into the code.

66
00:02:55,30 --> 00:02:58,10
However, they would be unable to reuse your key

67
00:02:58,10 --> 00:03:01,00
in their own projects because the origin of the request

68
00:03:01,00 --> 00:03:02,60
wouldn't match the authorized list

69
00:03:02,60 --> 00:03:05,00
you associated with your account.

