1
00:00:00,00 --> 00:00:06,10
(electronic music)

2
00:00:06,10 --> 00:00:08,10
- [Instructor] To create my proxy end point

3
00:00:08,10 --> 00:00:12,30
for SmartyStreets, I need to do three things.

4
00:00:12,30 --> 00:00:15,20
In my index.js file, the first thing I need to do is

5
00:00:15,20 --> 00:00:19,00
duplicate the apiOptions variable.

6
00:00:19,00 --> 00:00:21,60
And then I need to rename the new copy,

7
00:00:21,60 --> 00:00:24,80
because I need a different copy for each of my end points.

8
00:00:24,80 --> 00:00:26,80
So I'm going to, instead of saying API,

9
00:00:26,80 --> 00:00:29,30
I'm going to say smartyOptions.

10
00:00:29,30 --> 00:00:31,00
And then to configure this,

11
00:00:31,00 --> 00:00:34,40
I need to change the target value

12
00:00:34,40 --> 00:00:36,40
so it's not for the National Park Service,

13
00:00:36,40 --> 00:00:37,90
but it's for SmartyStreets,

14
00:00:37,90 --> 00:00:44,80
and that's us-street.api.smartystreets.com.

15
00:00:44,80 --> 00:00:49,20
Then I need to come up with a different API path,

16
00:00:49,20 --> 00:00:54,20
and so instead of NPS, I'm going to use streets.

17
00:00:54,20 --> 00:00:57,40
Finally, I need an API key.

18
00:00:57,40 --> 00:01:00,90
So I know that in my SmartyStreets url,

19
00:01:00,90 --> 00:01:04,30
if I take a look at that code,

20
00:01:04,30 --> 00:01:06,80
the key in the query string

21
00:01:06,80 --> 00:01:10,70
that SmartyStreets is expecting is auth-id,

22
00:01:10,70 --> 00:01:12,60
so I know I can swap that in here

23
00:01:12,60 --> 00:01:15,90
as the key name, auth-id.

24
00:01:15,90 --> 00:01:19,50
And then I need to replace this

25
00:01:19,50 --> 00:01:21,60
with whatever name I choose in Heroku.

26
00:01:21,60 --> 00:01:24,50
So right now, I'm just going to decide

27
00:01:24,50 --> 00:01:28,20
to call this STREETS_APIKEY,

28
00:01:28,20 --> 00:01:33,20
STREETS_APIKEY, and I'm actually going to copy that,

29
00:01:33,20 --> 00:01:35,80
and then over in the dashboard for my app

30
00:01:35,80 --> 00:01:40,60
I'm going to go to settings, I'm going to reveal my config vars,

31
00:01:40,60 --> 00:01:43,40
I copied that new key name and I'm going to paste it in here,

32
00:01:43,40 --> 00:01:47,70
STREETS_APIKEY, and then for the value

33
00:01:47,70 --> 00:01:52,00
I'm going to grab that key which I've saved,

34
00:01:52,00 --> 00:01:55,70
paste it in here, click the add button to save it.

35
00:01:55,70 --> 00:01:58,10
So now I have that SmartyStreets API key

36
00:01:58,10 --> 00:02:01,80
saved on the back end as a config var in Heroku.

37
00:02:01,80 --> 00:02:04,90
And so now, I have my query string key,

38
00:02:04,90 --> 00:02:08,40
I have a reference to the key value that's saved on Heroku,

39
00:02:08,40 --> 00:02:10,70
and so that takes care of smarty options.

40
00:02:10,70 --> 00:02:12,50
There's two more things I need to do.

41
00:02:12,50 --> 00:02:14,60
I need to actually create the proxy,

42
00:02:14,60 --> 00:02:18,30
and so I'm just going to copy this apiProxy variable,

43
00:02:18,30 --> 00:02:20,40
and again I need to rename it.

44
00:02:20,40 --> 00:02:22,90
So I'm going to call this streetsProxy,

45
00:02:22,90 --> 00:02:27,00
and I need to reference smartyOptions,

46
00:02:27,00 --> 00:02:31,40
so I'll paste that variable name in here.

47
00:02:31,40 --> 00:02:33,40
And then the last thing I need to do

48
00:02:33,40 --> 00:02:36,40
is duplicate this app.use statement.

49
00:02:36,40 --> 00:02:42,80
So in my smartyOptions I chose /streets as my endpoint,

50
00:02:42,80 --> 00:02:47,50
so down here I need to say /streets,

51
00:02:47,50 --> 00:02:52,50
and the name of my proxy is streetsProxy.

52
00:02:52,50 --> 00:02:55,20
So what this means is that when a request comes in,

53
00:02:55,20 --> 00:02:58,80
the endpoint /streets, it gets routed to streetsProxy,

54
00:02:58,80 --> 00:03:01,50
streetsProxy uses this framework

55
00:03:01,50 --> 00:03:04,20
to create a proxy using smartyOptions,

56
00:03:04,20 --> 00:03:06,60
and smartyOptions defines how

57
00:03:06,60 --> 00:03:10,20
requests that come in to that endpoint should be handled.

58
00:03:10,20 --> 00:03:11,80
So I'm going to save my changes,

59
00:03:11,80 --> 00:03:13,40
now I need to do one more thing,

60
00:03:13,40 --> 00:03:16,50
I need to actually change my front end code.

61
00:03:16,50 --> 00:03:18,80
And so over in app.js,

62
00:03:18,80 --> 00:03:24,90
first what I want to do is copy and paste my smartyUrl,

63
00:03:24,90 --> 00:03:29,00
keep it around for reference,

64
00:03:29,00 --> 00:03:31,40
and then I'm going to copy

65
00:03:31,40 --> 00:03:35,60
the beginning of that parksUrl up through the .com,

66
00:03:35,60 --> 00:03:39,20
and I'm going to swap that in through the .com here.

67
00:03:39,20 --> 00:03:44,00
Remember the endpoint that I chose was /streets,

68
00:03:44,00 --> 00:03:46,90
so I need to append that here.

69
00:03:46,90 --> 00:03:52,40
And then I don't need my auth-id or my API key anymore,

70
00:03:52,40 --> 00:03:55,10
so I can take off all of that,

71
00:03:55,10 --> 00:03:57,70
can take off the end of that quote,

72
00:03:57,70 --> 00:03:59,30
and then I don't need that ampersand

73
00:03:59,30 --> 00:04:00,90
because I've got a question mark here.

74
00:04:00,90 --> 00:04:04,50
So I'm just sending the key value pair candidates=10,

75
00:04:04,50 --> 00:04:06,80
my other key value pairs are getting appended

76
00:04:06,80 --> 00:04:09,30
once that form is completed,

77
00:04:09,30 --> 00:04:12,10
and then again the proxy will take care of

78
00:04:12,10 --> 00:04:15,10
adding the API key.

79
00:04:15,10 --> 00:04:17,60
So I'm saving that updated URL.

80
00:04:17,60 --> 00:04:19,50
Just for proof of concept I can also

81
00:04:19,50 --> 00:04:23,10
comment out my API key in the front end code.

82
00:04:23,10 --> 00:04:24,90
So I'm going to save that,

83
00:04:24,90 --> 00:04:27,20
and so switching to my terminal,

84
00:04:27,20 --> 00:04:30,30
I'm going to do a git status,

85
00:04:30,30 --> 00:04:33,20
and I have made some changes to index.js,

86
00:04:33,20 --> 00:04:35,50
so I'm going to do a git add .,

87
00:04:35,50 --> 00:04:43,20
git commit -m 'added streets endpoint',

88
00:04:43,20 --> 00:04:48,30
and then a git push heroku master.

89
00:04:48,30 --> 00:04:50,50
And again as long as I haven't made any

90
00:04:50,50 --> 00:04:53,50
syntax errors in my code, any logic errors,

91
00:04:53,50 --> 00:04:55,80
this should build correctly.

92
00:04:55,80 --> 00:04:57,70
Build did succeed, I see right there,

93
00:04:57,70 --> 00:05:00,40
which is always a good thing to see.

94
00:05:00,40 --> 00:05:02,60
Got my bash prompt back.

95
00:05:02,60 --> 00:05:05,70
So now I will go back to Visual Studio Code,

96
00:05:05,70 --> 00:05:09,20
go to contact.htm, and go live.

97
00:05:09,20 --> 00:05:14,10
And this time I need to actually submit an address,

98
00:05:14,10 --> 00:05:16,00
and I get my zip code back.

99
00:05:16,00 --> 00:05:18,70
So opening my console up,

100
00:05:18,70 --> 00:05:27,40
and let me go back and reload that...

101
00:05:27,40 --> 00:05:29,80
And I can see my street address request here.

102
00:05:29,80 --> 00:05:32,20
Again, I have a request to the proxy

103
00:05:32,20 --> 00:05:34,10
with all of my data there,

104
00:05:34,10 --> 00:05:36,60
and looking through all of this,

105
00:05:36,60 --> 00:05:38,90
the API key is nowhere to be seen,

106
00:05:38,90 --> 00:05:41,80
but I got my data back just like I did before,

107
00:05:41,80 --> 00:05:47,90
and so I have successfully extended my proxy server

108
00:05:47,90 --> 00:05:49,80
with a second endpoint,

109
00:05:49,80 --> 00:05:51,80
so that it can handle requests

110
00:05:51,80 --> 00:05:56,00
for both National Park Service data and SmartyStreets data.

