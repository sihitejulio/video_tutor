1
00:00:00,50 --> 00:00:02,20
- [Instructor] Heroku allows you to create

2
00:00:02,20 --> 00:00:06,90
and deploy apps which it hosts, builds, and executes.

3
00:00:06,90 --> 00:00:09,20
A free account on Heroku isn't appropriate

4
00:00:09,20 --> 00:00:12,30
for an app that needs to be available 24-7,

5
00:00:12,30 --> 00:00:14,80
but it will work fine for our purposes.

6
00:00:14,80 --> 00:00:18,10
To get started, I'm already logged into Heroku

7
00:00:18,10 --> 00:00:21,40
and on my dashboard, I'm going to create a new app.

8
00:00:21,40 --> 00:00:23,70
To do that I'm going to click the New button,

9
00:00:23,70 --> 00:00:25,80
and I want an app rather than a pipeline

10
00:00:25,80 --> 00:00:27,80
so I click Create New App.

11
00:00:27,80 --> 00:00:29,70
Now any app you create on Heroku

12
00:00:29,70 --> 00:00:32,00
has to have a unique name throughout

13
00:00:32,00 --> 00:00:34,10
all the apps that are on Heroku already.

14
00:00:34,10 --> 00:00:36,50
So anything obvious like Ajax Proxy's

15
00:00:36,50 --> 00:00:38,30
probably not going to be available.

16
00:00:38,30 --> 00:00:41,60
I'm going to try out ec for explore California

17
00:00:41,60 --> 00:00:44,20
followed by ajaxproxy.

18
00:00:44,20 --> 00:00:46,00
And it tells me it is available,

19
00:00:46,00 --> 00:00:48,00
that's great, I'm in the US so I'm going to use that

20
00:00:48,00 --> 00:00:50,30
and I'm going to create that.

21
00:00:50,30 --> 00:00:54,00
So now I have an app setup and I have a number

22
00:00:54,00 --> 00:00:56,30
of different tabs here that I can select

23
00:00:56,30 --> 00:01:00,30
to see different options and settings for this app.

24
00:01:00,30 --> 00:01:03,00
So the next thing I need to do is actually store

25
00:01:03,00 --> 00:01:05,70
my National Park Service API credentials

26
00:01:05,70 --> 00:01:07,80
in Heroku cause the whole point here

27
00:01:07,80 --> 00:01:09,90
is to get those out of the front end code

28
00:01:09,90 --> 00:01:12,00
and in to the back end code.

29
00:01:12,00 --> 00:01:13,50
So to do that, I'm going to create

30
00:01:13,50 --> 00:01:15,00
what's called a config var

31
00:01:15,00 --> 00:01:18,20
which is short for configuration variable.

32
00:01:18,20 --> 00:01:21,50
This let's me store a value and give it a name

33
00:01:21,50 --> 00:01:24,30
just like a variable in any other Java script code.

34
00:01:24,30 --> 00:01:27,40
And then I can reference that name in my apps.

35
00:01:27,40 --> 00:01:30,20
So I'll go to the Settings tab,

36
00:01:30,20 --> 00:01:33,40
and there's this button here Reveal Config Vars.

37
00:01:33,40 --> 00:01:35,20
So those are hidden by default

38
00:01:35,20 --> 00:01:36,60
in case there's somebody just happens

39
00:01:36,60 --> 00:01:39,30
to be walking behind you when you open the screen,

40
00:01:39,30 --> 00:01:40,70
you can make sure there's no one around

41
00:01:40,70 --> 00:01:45,00
who shouldn't see your actual config var values

42
00:01:45,00 --> 00:01:47,10
before you actually reveal them.

43
00:01:47,10 --> 00:01:48,90
So I'm going to click that button,

44
00:01:48,90 --> 00:01:51,40
has no config vars yet and we've got a key

45
00:01:51,40 --> 00:01:53,00
and a value box here.

46
00:01:53,00 --> 00:01:54,40
So I'm going to click in the Key box.

47
00:01:54,40 --> 00:01:59,90
I'm going to name my new variable NPS_APIKEY.

48
00:01:59,90 --> 00:02:02,70
So it's really common to name these in all caps

49
00:02:02,70 --> 00:02:05,30
because they are essentially constants

50
00:02:05,30 --> 00:02:08,60
and then in the value box I've copied

51
00:02:08,60 --> 00:02:11,50
my National Park Service API key to the clipboard

52
00:02:11,50 --> 00:02:13,70
and I'm just going to paste that in here.

53
00:02:13,70 --> 00:02:17,30
And then click the Add button, and now that's saved.

54
00:02:17,30 --> 00:02:19,70
So now my back end code can reference

55
00:02:19,70 --> 00:02:24,30
this variable name and this API key value

56
00:02:24,30 --> 00:02:26,20
will get swapped into my code.

57
00:02:26,20 --> 00:02:28,00
And this has the extra advantage

58
00:02:28,00 --> 00:02:31,40
that if I ever change or replace my API key,

59
00:02:31,40 --> 00:02:33,70
I don't have to dig into my code to do that.

60
00:02:33,70 --> 00:02:35,50
I can come right here to my dashboard

61
00:02:35,50 --> 00:02:37,00
and just swap it in.

