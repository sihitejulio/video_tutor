1
00:00:00,50 --> 00:00:01,90
- [Insructor] Fetch allows you to create

2
00:00:01,90 --> 00:00:05,30
a well-organized request with a minimum of code.

3
00:00:05,30 --> 00:00:08,70
But sometimes APIs require a more finely grained

4
00:00:08,70 --> 00:00:12,30
HTTP request than what fits in a URL.

5
00:00:12,30 --> 00:00:14,80
By far the most common instance of this,

6
00:00:14,80 --> 00:00:18,70
is APIs that require one or more HTTP headers

7
00:00:18,70 --> 00:00:20,60
as part of your request.

8
00:00:20,60 --> 00:00:24,00
Fetch supports a second argument after the URL,

9
00:00:24,00 --> 00:00:26,60
which is an initialization object containing one

10
00:00:26,60 --> 00:00:28,40
or more arguments.

11
00:00:28,40 --> 00:00:32,10
This can include the HTTP method, your credentials,

12
00:00:32,10 --> 00:00:35,80
or a number of other options, including headers.

13
00:00:35,80 --> 00:00:38,80
To include headers, you use the headers properties name

14
00:00:38,80 --> 00:00:40,40
within the init object.

15
00:00:40,40 --> 00:00:43,10
And then specify an object as its value.

16
00:00:43,10 --> 00:00:46,10
This object includes each header name and value

17
00:00:46,10 --> 00:00:49,00
as a key value pair.

18
00:00:49,00 --> 00:00:53,10
Our HTTP request to SmartyStreets has been working fine,

19
00:00:53,10 --> 00:00:55,00
but if you read through the documentation,

20
00:00:55,00 --> 00:00:56,90
you'll notice that SmartyStreets prefers

21
00:00:56,90 --> 00:01:00,60
each request to include two specific HTTP headers.

22
00:01:00,60 --> 00:01:03,00
If they decide to enforce this down the line,

23
00:01:03,00 --> 00:01:05,70
my request might result in an error in the future,

24
00:01:05,70 --> 00:01:08,00
so I want to add these headers to my request,

25
00:01:08,00 --> 00:01:11,40
just to square everything away as neatly as I can.

26
00:01:11,40 --> 00:01:14,50
At the top of my app.js file, I'm going to create

27
00:01:14,50 --> 00:01:18,80
a new constant called smartyInit.

28
00:01:18,80 --> 00:01:20,60
This will store the object will pass

29
00:01:20,60 --> 00:01:23,10
as the second parameter to Fetch,

30
00:01:23,10 --> 00:01:26,20
so I'm going to create an object data type.

31
00:01:26,20 --> 00:01:28,30
Go ahead and close that now.

32
00:01:28,30 --> 00:01:31,00
The only custom option I need is headers,

33
00:01:31,00 --> 00:01:33,80
so I'll add a headers property,

34
00:01:33,80 --> 00:01:38,00
then as its value, I need to specify an object

35
00:01:38,00 --> 00:01:41,80
with each of those two specified headers as properties,

36
00:01:41,80 --> 00:01:43,90
and I can just copy those straight from the website

37
00:01:43,90 --> 00:01:46,90
to ensure that I don't make any typing mistakes.

38
00:01:46,90 --> 00:01:48,30
So there's the first one.

39
00:01:48,30 --> 00:01:50,40
Now notice that because this property name

40
00:01:50,40 --> 00:01:53,30
has a hyphen in it, I actually need to treat

41
00:01:53,30 --> 00:01:54,70
that as a string here.

42
00:01:54,70 --> 00:01:57,50
And then of course, I need to treat the value

43
00:01:57,50 --> 00:01:59,80
as a string as well.

44
00:01:59,80 --> 00:02:03,50
So then the other header is host.

45
00:02:03,50 --> 00:02:05,20
So again, pasting that in.

46
00:02:05,20 --> 00:02:08,10
This time I only need to quote that value.

47
00:02:08,10 --> 00:02:12,00
I always like to end with a comma, and there we go.

48
00:02:12,00 --> 00:02:15,80
So now, I have these two headers saved in a variable,

49
00:02:15,80 --> 00:02:18,10
and then I can use that variable

50
00:02:18,10 --> 00:02:20,10
to customize my Fetch request.

51
00:02:20,10 --> 00:02:24,20
To implement this, first I'll go down to create request,

52
00:02:24,20 --> 00:02:28,80
and I'll add a fourth parameter, which will be init.

53
00:02:28,80 --> 00:02:32,00
Then I'm going to update my Fetch invocation

54
00:02:32,00 --> 00:02:35,80
to add that parameter, so now I'm passing the URL

55
00:02:35,80 --> 00:02:39,70
and init parameters when I call Fetch.

56
00:02:39,70 --> 00:02:42,50
Then when I move down to check completion,

57
00:02:42,50 --> 00:02:45,60
I need to add my variable as the fourth argument

58
00:02:45,60 --> 00:02:48,10
in the create request function call.

59
00:02:48,10 --> 00:02:54,20
So after smartyUpdateUIError, I want to specify smartyInit.

60
00:02:54,20 --> 00:02:57,40
So I'll save my work,

61
00:02:57,40 --> 00:02:59,70
fire up my live server,

62
00:02:59,70 --> 00:03:03,40
open my console.

63
00:03:03,40 --> 00:03:06,50
And notice that my park request still works

64
00:03:06,50 --> 00:03:10,50
because it's got a null value for that init object,

65
00:03:10,50 --> 00:03:14,00
and it doesn't make a difference in the request.

66
00:03:14,00 --> 00:03:20,00
But I will go ahead and try out a Smartystreets request

67
00:03:20,00 --> 00:03:21,50
And it looks like I have an error,

68
00:03:21,50 --> 00:03:25,90
and that's pointing me to line 23 in my code.

69
00:03:25,90 --> 00:03:28,70
And that's where I am trying to, again,

70
00:03:28,70 --> 00:03:31,40
like I did down in my parkUpdateUISuccess,

71
00:03:31,40 --> 00:03:34,80
I'm trying to treat JSON as something I need

72
00:03:34,80 --> 00:03:35,90
to parse into JSON.

73
00:03:35,90 --> 00:03:41,20
So I'm going to comment out that parse data declaration,

74
00:03:41,20 --> 00:03:43,00
and I'm going to change my parameter name

75
00:03:43,00 --> 00:03:47,50
to parseData, which is used throughout the function

76
00:03:47,50 --> 00:03:49,70
and that should get all the data

77
00:03:49,70 --> 00:03:51,80
that I'm providing aligned with the data

78
00:03:51,80 --> 00:03:53,70
that's expected in this function.

79
00:03:53,70 --> 00:03:57,60
So going back to my page, let's try

80
00:03:57,60 --> 00:04:01,00
this request one more time.

81
00:04:01,00 --> 00:04:02,30
So there's the zip code.

82
00:04:02,30 --> 00:04:05,70
So I'm all good, I have my Fetch request customized

83
00:04:05,70 --> 00:04:08,80
to meet the criteria laid out in the API documentation

84
00:04:08,80 --> 00:04:11,00
by using that init object.

