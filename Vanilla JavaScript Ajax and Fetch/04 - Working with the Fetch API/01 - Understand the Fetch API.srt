1
00:00:00,70 --> 00:00:02,60
- [Instructor] Libraries like jQuery include

2
00:00:02,60 --> 00:00:05,00
convenience methods that abstract a way

3
00:00:05,00 --> 00:00:08,50
the need to work with XHR objects directly.

4
00:00:08,50 --> 00:00:10,90
But until recently, a streamlined solution

5
00:00:10,90 --> 00:00:13,40
was missing from Vanilla JavaScript.

6
00:00:13,40 --> 00:00:17,00
Today, the Fetch API fills this void.

7
00:00:17,00 --> 00:00:20,00
One of the biggest challenges that XHR presents

8
00:00:20,00 --> 00:00:22,50
in all but the most basic use cases

9
00:00:22,50 --> 00:00:25,60
is what's known as, colloquially, as callback hell.

10
00:00:25,60 --> 00:00:27,60
This is the situation where you want

11
00:00:27,60 --> 00:00:30,20
a series of things to happen in sequence.

12
00:00:30,20 --> 00:00:33,00
The callback-based solution for this in JavaScript

13
00:00:33,00 --> 00:00:36,20
is generally to create a series of nested function calls

14
00:00:36,20 --> 00:00:38,30
each calling another so the result

15
00:00:38,30 --> 00:00:41,20
of the deepest nested function must be determined

16
00:00:41,20 --> 00:00:44,80
before the next function in the chain is executed and so on,

17
00:00:44,80 --> 00:00:46,70
up to the stop of the stack.

18
00:00:46,70 --> 00:00:49,60
Nested callbacks can be a challenge to read in code

19
00:00:49,60 --> 00:00:51,10
and can create a call stack

20
00:00:51,10 --> 00:00:53,40
that presents debugging challenges.

21
00:00:53,40 --> 00:00:55,90
The Fetch API does two things.

22
00:00:55,90 --> 00:00:59,40
First, it performs some of the basic AJAX functions

23
00:00:59,40 --> 00:01:01,30
without requiring you to code them.

24
00:01:01,30 --> 00:01:03,80
You pass a URL to the Fetch method

25
00:01:03,80 --> 00:01:06,90
and then opening the connection and sending the request

26
00:01:06,90 --> 00:01:08,90
are performed behind the scenes

27
00:01:08,90 --> 00:01:11,50
without the need for additional statements.

28
00:01:11,50 --> 00:01:14,60
You can pass the Fetch method an optional second argument

29
00:01:14,60 --> 00:01:17,10
containing parameters, but by default,

30
00:01:17,10 --> 00:01:21,40
it assumes basic settings, such as get, for the HTTP method.

31
00:01:21,40 --> 00:01:23,00
The result of the Fetch statement

32
00:01:23,00 --> 00:01:25,90
is returned in a standardized response object.

33
00:01:25,90 --> 00:01:27,50
Once the request has happened

34
00:01:27,50 --> 00:01:29,80
and that response object is generated,

35
00:01:29,80 --> 00:01:31,90
you can easily specify what happens

36
00:01:31,90 --> 00:01:35,40
in the next and in subsequent steps asynchronously

37
00:01:35,40 --> 00:01:37,40
using a syntax first introduced

38
00:01:37,40 --> 00:01:40,40
in another recent JavaScript feature, promises.

39
00:01:40,40 --> 00:01:43,30
Promises provide a way to easily specify code

40
00:01:43,30 --> 00:01:45,20
that should run in sequence.

41
00:01:45,20 --> 00:01:47,70
Rather than create nested callbacks,

42
00:01:47,70 --> 00:01:50,90
promises allow you to chain the results of running code

43
00:01:50,90 --> 00:01:52,80
using the keyword, then.

44
00:01:52,80 --> 00:01:55,90
Promises also pass along standardized objects

45
00:01:55,90 --> 00:02:00,00
and even include their own streamlined way to handle errors.

46
00:02:00,00 --> 00:02:02,30
You can use the promise constructor in your code

47
00:02:02,30 --> 00:02:04,90
in a wide variety of situations.

48
00:02:04,90 --> 00:02:07,70
The Fetch API uses the fetch keyword,

49
00:02:07,70 --> 00:02:10,00
but adapts the syntax of promises

50
00:02:10,00 --> 00:02:13,00
including the super useful then method.

51
00:02:13,00 --> 00:02:15,60
This allows us to simplify XHR code

52
00:02:15,60 --> 00:02:18,00
into something as straightforward as this.

53
00:02:18,00 --> 00:02:21,10
And in particular, while more complex XHR code

54
00:02:21,10 --> 00:02:24,30
often requires you to interpret nesting in the code,

55
00:02:24,30 --> 00:02:27,00
you can read Fetch code in a linear fashion

56
00:02:27,00 --> 00:02:30,10
from top to bottom and you can understand the order

57
00:02:30,10 --> 00:02:32,80
in which all of the code will be executed.

58
00:02:32,80 --> 00:02:35,60
The modern jQuery syntax for AJAX requests

59
00:02:35,60 --> 00:02:39,00
is also based on promises, so if you've seen that,

60
00:02:39,00 --> 00:02:41,50
you'll notice the similarities right off.

61
00:02:41,50 --> 00:02:45,30
In fact, you can rewrite jQuery dollar.get code

62
00:02:45,30 --> 00:02:48,10
by replacing dollar.get with Fetch,

63
00:02:48,10 --> 00:02:52,70
then replacing each instance of .done with .then,

64
00:02:52,70 --> 00:02:56,00
replacing the fail method with the catch method,

65
00:02:56,00 --> 00:02:58,80
and then adding an error-handling step.

66
00:02:58,80 --> 00:03:01,40
Each step in a Fetch chain takes a function

67
00:03:01,40 --> 00:03:04,20
as an argument and returns a value.

68
00:03:04,20 --> 00:03:07,30
For this reason, it's super common to see Fetch code

69
00:03:07,30 --> 00:03:10,60
written even more concisely using arrow functions.

70
00:03:10,60 --> 00:03:14,40
This code is identical to the Fetch chain shown previously,

71
00:03:14,40 --> 00:03:16,50
but uses fewer lines of code.

72
00:03:16,50 --> 00:03:19,30
Fetch is supported by all modern browsers.

73
00:03:19,30 --> 00:03:21,30
We can look on caniuse.com to see

74
00:03:21,30 --> 00:03:23,20
its current level of support.

75
00:03:23,20 --> 00:03:25,50
Can I use is a great resource for understanding

76
00:03:25,50 --> 00:03:28,20
the level of browser support for newer features

77
00:03:28,20 --> 00:03:31,60
in HTML, CSS, and JavaScript.

78
00:03:31,60 --> 00:03:35,70
So, in the search box, I can simply type in fetch

79
00:03:35,70 --> 00:03:38,70
and get a chart showing the current level

80
00:03:38,70 --> 00:03:40,20
of support for Fetch.

81
00:03:40,20 --> 00:03:43,80
I find it more useful to click the usage relative button

82
00:03:43,80 --> 00:03:46,40
which shows the weighted bars that indicate

83
00:03:46,40 --> 00:03:48,30
how much traffic comes from browsers

84
00:03:48,30 --> 00:03:52,30
that support this feature and those are indicated in green

85
00:03:52,30 --> 00:03:55,60
and in red, we see that percentage of traffic

86
00:03:55,60 --> 00:03:57,70
from browsers that are not supported.

87
00:03:57,70 --> 00:04:00,70
And so, we can see here, the biggest culprit is IE 11,

88
00:04:00,70 --> 00:04:02,80
the last version of Internet Explorer,

89
00:04:02,80 --> 00:04:06,40
which has still got over 2% global use in this chart

90
00:04:06,40 --> 00:04:09,50
and then little dribs and drabs of older IE,

91
00:04:09,50 --> 00:04:14,80
some older versions of Safari are still out there,

92
00:04:14,80 --> 00:04:19,30
both on Mac OS and iOS.

93
00:04:19,30 --> 00:04:21,90
And we have Opera Mini, which does have

94
00:04:21,90 --> 00:04:23,80
a significant user base.

95
00:04:23,80 --> 00:04:27,10
The Android browser is still out there a little bit.

96
00:04:27,10 --> 00:04:28,70
And so we have a summary up here

97
00:04:28,70 --> 00:04:33,10
that shows us that not quite 92% of traffic,

98
00:04:33,10 --> 00:04:36,30
by their reckoning, supports Fetch.

99
00:04:36,30 --> 00:04:38,80
That means that we have a hole of 8%

100
00:04:38,80 --> 00:04:41,80
if we don't support alternatives.

101
00:04:41,80 --> 00:04:45,30
So, as with a few other modern web development tools,

102
00:04:45,30 --> 00:04:49,00
we need to use a polyfill for full backward compatibility.

103
00:04:49,00 --> 00:04:52,20
So Fetch is pretty exciting and makes AJAX code

104
00:04:52,20 --> 00:04:56,00
easier to create and maintain using Vanilla JavaScript.

