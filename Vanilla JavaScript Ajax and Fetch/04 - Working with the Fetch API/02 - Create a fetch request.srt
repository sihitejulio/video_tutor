1
00:00:00,50 --> 00:00:02,80
- [Instructor] My form page for Explore California

2
00:00:02,80 --> 00:00:06,90
currently uses an XHR request to get remote data.

3
00:00:06,90 --> 00:00:08,40
I want to update that to fetch

4
00:00:08,40 --> 00:00:11,40
to make it easier to read and to maintain.

5
00:00:11,40 --> 00:00:13,80
To do this, I need to replace two functions,

6
00:00:13,80 --> 00:00:17,60
createRequest creates an XHR object

7
00:00:17,60 --> 00:00:20,60
and responseMethod works with the properties

8
00:00:20,60 --> 00:00:22,80
of the XHR object.

9
00:00:22,80 --> 00:00:25,90
I'll start by commenting these out.

10
00:00:25,90 --> 00:00:31,80
So there's responseMethod, and there's createRequest.

11
00:00:31,80 --> 00:00:33,80
And next, I want to recreate

12
00:00:33,80 --> 00:00:37,90
the createRequest function using fetch.

13
00:00:37,90 --> 00:00:45,30
So that'll be const createRequest = function takes url,

14
00:00:45,30 --> 00:00:48,70
succeed and fail as parameters,

15
00:00:48,70 --> 00:00:53,30
got to spell that right, C-C-E-E-D,

16
00:00:53,30 --> 00:00:59,40
and then I'm going to call fetch, pass it my URL,

17
00:00:59,40 --> 00:01:02,50
which is a parameter, and because I'm going to be chaining

18
00:01:02,50 --> 00:01:07,70
then to this, I'm going to do that on a separate line.

19
00:01:07,70 --> 00:01:10,70
So this is pretty common so that it's clear

20
00:01:10,70 --> 00:01:14,80
that .then is chained to the fetch that's above it.

21
00:01:14,80 --> 00:01:17,00
And then I need to pass in a function,

22
00:01:17,00 --> 00:01:19,60
and I'm going to use arrow function syntax.

23
00:01:19,60 --> 00:01:25,50
So, the parameter for this function is (response)

24
00:01:25,50 --> 00:01:35,10
and I'm going to start by simply console.log.(response).

25
00:01:35,10 --> 00:01:37,60
I'm using the same function name as before,

26
00:01:37,60 --> 00:01:40,60
so this will still get called for the National Parks API

27
00:01:40,60 --> 00:01:42,40
when the dom finishes loading,

28
00:01:42,40 --> 00:01:45,10
but all the function does for now is log the response.

29
00:01:45,10 --> 00:01:47,80
So we're taking a first step and we'll check it out.

30
00:01:47,80 --> 00:01:50,10
So I'm going to save those changes,

31
00:01:50,10 --> 00:01:53,90
start my live server and open my console.

32
00:01:53,90 --> 00:01:57,00
So I have the response logged to the console,

33
00:01:57,00 --> 00:02:02,20
and it's an object, so I can dig into it a little bit.

34
00:02:02,20 --> 00:02:05,40
And we have some details here,

35
00:02:05,40 --> 00:02:08,10
we have some header information

36
00:02:08,10 --> 00:02:12,80
telling us about the response, that it was okay.

37
00:02:12,80 --> 00:02:16,20
But looking at the body, not actually able

38
00:02:16,20 --> 00:02:19,50
to access the data there automatically.

39
00:02:19,50 --> 00:02:23,60
So fetch doesn't actually allow me to write code

40
00:02:23,60 --> 00:02:26,20
to access the value of that body,

41
00:02:26,20 --> 00:02:28,60
which is the response data.

42
00:02:28,60 --> 00:02:32,70
So instead, I need to use a method, .json,

43
00:02:32,70 --> 00:02:34,80
on the response object itself,

44
00:02:34,80 --> 00:02:36,30
and I have to return that value

45
00:02:36,30 --> 00:02:39,40
and pass it to another .then method.

46
00:02:39,40 --> 00:02:43,70
So this is part of the underlying architecture of Promises.

47
00:02:43,70 --> 00:02:48,30
And so back in my file, I'm still getting

48
00:02:48,30 --> 00:02:50,50
a response parameter, and then,

49
00:02:50,50 --> 00:02:53,50
since I want to return response.json,

50
00:02:53,50 --> 00:02:56,50
I'm just going to take advantage of the implicit return

51
00:02:56,50 --> 00:03:01,30
in an arrow function, and simply say response.json,

52
00:03:01,30 --> 00:03:04,20
and in this case .json is all lower case,

53
00:03:04,20 --> 00:03:09,40
and then, another .then, which is getting that data

54
00:03:09,40 --> 00:03:11,50
as a parameter, so I'm just going to use data

55
00:03:11,50 --> 00:03:16,30
as my parameter name, and this time,

56
00:03:16,30 --> 00:03:21,10
I will console.log value of that data parameter.

57
00:03:21,10 --> 00:03:24,40
I'll save that, and back in my browser,

58
00:03:24,40 --> 00:03:29,50
I now have the data being logged, which is actual park data.

59
00:03:29,50 --> 00:03:31,00
So with those two steps,

60
00:03:31,00 --> 00:03:33,70
first simply returning response.json,

61
00:03:33,70 --> 00:03:36,20
and then actually working with that data,

62
00:03:36,20 --> 00:03:39,90
I now have access to the data just like I did with HXR.

63
00:03:39,90 --> 00:03:43,10
And so now, instead of simply console.logging,

64
00:03:43,10 --> 00:03:47,90
I can actually return the succeed function call

65
00:03:47,90 --> 00:03:49,90
and pass it data, so I don't need

66
00:03:49,90 --> 00:03:51,60
those curly braces here anymore.

67
00:03:51,60 --> 00:03:56,90
I'm going to save that change, and back in my browser,

68
00:03:56,90 --> 00:04:00,00
so I have an error, and back in my browser,

69
00:04:00,00 --> 00:04:04,10
I am sending parsed data to the succeed method,

70
00:04:04,10 --> 00:04:08,00
but if I look in the succeed method for my parkUpdateUI,

71
00:04:08,00 --> 00:04:10,30
it is starting by parsing the data.

72
00:04:10,30 --> 00:04:12,40
But I already have parsed data.

73
00:04:12,40 --> 00:04:14,50
So in this case, I'm simply going to

74
00:04:14,50 --> 00:04:17,00
comment out parsedData here,

75
00:04:17,00 --> 00:04:20,30
and I'm going to change my parameter name to parsedData

76
00:04:20,30 --> 00:04:22,80
to match the data that I actually have coming in.

77
00:04:22,80 --> 00:04:24,90
So now all the rest of my code is expecting

78
00:04:24,90 --> 00:04:26,90
to work with parsed data, and that's

79
00:04:26,90 --> 00:04:31,10
what is actually being supplied as the parameter here.

80
00:04:31,10 --> 00:04:35,10
And now, back on my page, we can see

81
00:04:35,10 --> 00:04:36,30
that the data's been received,

82
00:04:36,30 --> 00:04:40,20
and we have the dom actually being manipulated,

83
00:04:40,20 --> 00:04:45,50
and that data for a random park being added to the dom.

84
00:04:45,50 --> 00:04:47,10
Recall that fetch is a feature

85
00:04:47,10 --> 00:04:51,30
that's supported by modern browsers, but not by older ones.

86
00:04:51,30 --> 00:04:53,30
And although we can use a tool like Babble

87
00:04:53,30 --> 00:04:55,70
to transpile a number of Yes Six features

88
00:04:55,70 --> 00:04:58,50
into Yes Five compatible code,

89
00:04:58,50 --> 00:05:00,30
that's not the case with fetch.

90
00:05:00,30 --> 00:05:03,30
Instead, we need to include a polyfill in our code,

91
00:05:03,30 --> 00:05:05,80
which is a separate set of JavaScript functions

92
00:05:05,80 --> 00:05:08,30
that check for the feature and recreate it

93
00:05:08,30 --> 00:05:10,00
as a method of the Window object

94
00:05:10,00 --> 00:05:12,30
if it's not already present.

95
00:05:12,30 --> 00:05:15,30
Github makes a popular and concise fetch polyfill.

96
00:05:15,30 --> 00:05:18,10
Now, as you look through the documentation here,

97
00:05:18,10 --> 00:05:23,60
one thing they make clear is that in order to use this,

98
00:05:23,60 --> 00:05:27,20
you need to also include a Promise polyfill,

99
00:05:27,20 --> 00:05:29,70
which reproduces that underlying architecture

100
00:05:29,70 --> 00:05:31,40
that fetch is based on.

101
00:05:31,40 --> 00:05:34,60
I've already included the files for both this fetch polyfill

102
00:05:34,60 --> 00:05:37,70
and the recommended Promise polyfill

103
00:05:37,70 --> 00:05:40,10
in the start files for this course.

104
00:05:40,10 --> 00:05:43,70
So, returning to my contact.html file,

105
00:05:43,70 --> 00:05:47,20
I need to go to bottom where the script element

106
00:05:47,20 --> 00:05:50,10
referencing app.js is located.

107
00:05:50,10 --> 00:05:52,30
Now, because my code uses the fetch keyword,

108
00:05:52,30 --> 00:05:54,00
I need to include the fetch polyfill

109
00:05:54,00 --> 00:05:57,90
in my .html document before my app.js file.

110
00:05:57,90 --> 00:06:00,60
And because the fetch polyfill uses Promises,

111
00:06:00,60 --> 00:06:02,70
I need to include the Promises polyfill

112
00:06:02,70 --> 00:06:04,40
before the fetch polyfill.

113
00:06:04,40 --> 00:06:07,50
If these are out of order they won't work in older browsers.

114
00:06:07,50 --> 00:06:10,00
So, before the app.js script element,

115
00:06:10,00 --> 00:06:12,50
I'm going to add another script element,

116
00:06:12,50 --> 00:06:21,90
src="_scripts/promise.min.js"

117
00:06:21,90 --> 00:06:25,00
and then after that promise polyfill,

118
00:06:25,00 --> 00:06:30,20
another script element, srs="_scripts/

119
00:06:30,20 --> 00:06:33,60
and this time, fetch.umd.js"

120
00:06:33,60 --> 00:06:40,10
and now saving this, and going back to my browser,

121
00:06:40,10 --> 00:06:42,90
and everything still works just like it did.

122
00:06:42,90 --> 00:06:45,60
But if I were to transpile, meaning that, for instance,

123
00:06:45,60 --> 00:06:48,10
my const would be replaced with vars,

124
00:06:48,10 --> 00:06:49,90
and if I opened this in an older browser

125
00:06:49,90 --> 00:06:52,50
like Internet Explorer, now I'd be sure

126
00:06:52,50 --> 00:06:55,00
that that fetch request would work, no problem.

