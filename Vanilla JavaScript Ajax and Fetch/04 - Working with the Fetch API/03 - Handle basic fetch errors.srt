1
00:00:00,50 --> 00:00:02,40
- [Instructor] Each method in a fetch chain finishes

2
00:00:02,40 --> 00:00:05,00
by either resolving or rejecting.

3
00:00:05,00 --> 00:00:08,00
Resolving means the process executed successfully

4
00:00:08,00 --> 00:00:10,10
while rejecting means there was an issue

5
00:00:10,10 --> 00:00:11,50
and it throws an error.

6
00:00:11,50 --> 00:00:15,80
You might expect that receiving an HTTP response like 404

7
00:00:15,80 --> 00:00:18,60
would result in a fetch request rejecting.

8
00:00:18,60 --> 00:00:21,50
However, fetch rejects only when something

9
00:00:21,50 --> 00:00:22,90
like a network issue

10
00:00:22,90 --> 00:00:25,80
prevents the request from being executed.

11
00:00:25,80 --> 00:00:29,70
However, the response object returned by the fetch method

12
00:00:29,70 --> 00:00:32,30
includes a Boolean OK property

13
00:00:32,30 --> 00:00:37,30
that is true only if the HTTP response is in the 200 range.

14
00:00:37,30 --> 00:00:40,40
Otherwise, this property is set to false

15
00:00:40,40 --> 00:00:43,70
even though the fetch itself doesn't throw an error.

16
00:00:43,70 --> 00:00:46,20
It's a common pattern when using fetch

17
00:00:46,20 --> 00:00:48,90
to check the OK value first

18
00:00:48,90 --> 00:00:51,60
before attempting to work with the data.

19
00:00:51,60 --> 00:00:54,10
This way, you can intercept any errors

20
00:00:54,10 --> 00:00:55,80
and handle them differently

21
00:00:55,80 --> 00:00:58,50
than you would a successful response.

22
00:00:58,50 --> 00:00:59,80
Back in my code,

23
00:00:59,80 --> 00:01:02,60
I could add error checking to the first then method.

24
00:01:02,60 --> 00:01:04,40
But anything more than a return statement

25
00:01:04,40 --> 00:01:06,60
I prefer to move into a helper function

26
00:01:06,60 --> 00:01:09,50
so I can keep my fetch structure lean and readable.

27
00:01:09,50 --> 00:01:13,60
So I'm going to create a new handleErrors function

28
00:01:13,60 --> 00:01:21,30
before my createRequest function.

29
00:01:21,30 --> 00:01:24,20
That's going to take response as a parameter

30
00:01:24,20 --> 00:01:25,60
and then I'm going to use an if statement

31
00:01:25,60 --> 00:01:27,70
to check if it's not the case

32
00:01:27,70 --> 00:01:30,60
that response.ok is true.

33
00:01:30,60 --> 00:01:32,90
So remember, response.ok is a Boolean

34
00:01:32,90 --> 00:01:34,90
so it's going to be either true or false

35
00:01:34,90 --> 00:01:37,20
and not response.ok will be true

36
00:01:37,20 --> 00:01:39,50
if that OK value is false.

37
00:01:39,50 --> 00:01:41,00
And so in that case,

38
00:01:41,00 --> 00:01:42,80
I want to use the throw keyword

39
00:01:42,80 --> 00:01:46,10
to send along some debugging data.

40
00:01:46,10 --> 00:01:53,00
And so I'm going to concatenate response.status

41
00:01:53,00 --> 00:01:56,00
and a colon and a space

42
00:01:56,00 --> 00:02:01,60
and then response.statusText.

43
00:02:01,60 --> 00:02:03,70
Now I don't need an else statement here

44
00:02:03,70 --> 00:02:08,70
because obviously if response.ok is true,

45
00:02:08,70 --> 00:02:11,10
then I want something else to happen.

46
00:02:11,10 --> 00:02:12,60
So I'll just finish here

47
00:02:12,60 --> 00:02:17,20
with the line I was previously using in my then statement,

48
00:02:17,20 --> 00:02:19,00
spelling out return,

49
00:02:19,00 --> 00:02:21,90
and then response.json.

50
00:02:21,90 --> 00:02:25,70
And then down here in createRequest in my then statement

51
00:02:25,70 --> 00:02:29,50
instead of returning response.json,

52
00:02:29,50 --> 00:02:33,80
I want to return handleErrors

53
00:02:33,80 --> 00:02:39,10
passing it response as an argument.

54
00:02:39,10 --> 00:02:42,20
So now that response object is handed off to handleErrors

55
00:02:42,20 --> 00:02:45,20
which decides whether an error needs to be thrown

56
00:02:45,20 --> 00:02:49,20
or otherwise it returns back that response.json

57
00:02:49,20 --> 00:02:51,60
which goes into the next .then.

58
00:02:51,60 --> 00:02:53,70
If I throw an exception up in handleErrors,

59
00:02:53,70 --> 00:02:57,30
then my method rejects and no further thens are executed.

60
00:02:57,30 --> 00:02:59,80
However, fetch supports an additional method

61
00:02:59,80 --> 00:03:02,50
to work with errors and that's catch.

62
00:03:02,50 --> 00:03:07,80
So generally we chain .catch after the last .then

63
00:03:07,80 --> 00:03:11,30
and I'm simply going to grab the text

64
00:03:11,30 --> 00:03:13,40
or object that's passed in,

65
00:03:13,40 --> 00:03:16,40
give it the parameter name error

66
00:03:16,40 --> 00:03:20,60
and then I simply want to return

67
00:03:20,60 --> 00:03:22,90
a call to the fail handler

68
00:03:22,90 --> 00:03:26,00
and pass that the error parameter.

69
00:03:26,00 --> 00:03:29,90
So I'm going to save those changes.

70
00:03:29,90 --> 00:03:33,20
I have to restart my live server

71
00:03:33,20 --> 00:03:35,60
and open my console

72
00:03:35,60 --> 00:03:39,80
and we can see that the parks request has worked.

73
00:03:39,80 --> 00:03:41,90
And then going back to my code,

74
00:03:41,90 --> 00:03:44,40
I'm going to simulate an error.

75
00:03:44,40 --> 00:03:50,20
So one more time I will grab that parksUrl,

76
00:03:50,20 --> 00:03:52,00
I'm going to duplicate it,

77
00:03:52,00 --> 00:03:54,20
I'm going to comment the original out,

78
00:03:54,20 --> 00:03:58,50
take out a couple characters from my API key, save that,

79
00:03:58,50 --> 00:04:00,30
and then back in the browser

80
00:04:00,30 --> 00:04:04,10
I have the error generated by the browser

81
00:04:04,10 --> 00:04:08,10
and I have my own error message that I logged to the console

82
00:04:08,10 --> 00:04:10,70
as a result of my .catch method.

83
00:04:10,70 --> 00:04:13,10
So I have ensured that my program flow

84
00:04:13,10 --> 00:04:18,50
treats HTTP response codes outside the 200 range as errors.

85
00:04:18,50 --> 00:04:20,70
And now I just need to clean my code up.

86
00:04:20,70 --> 00:04:22,20
So back in the editor,

87
00:04:22,20 --> 00:04:25,90
I'm going to take out that broken version of the URL

88
00:04:25,90 --> 00:04:28,50
and uncomment the original one.

89
00:04:28,50 --> 00:04:30,20
I'm going to save that.

90
00:04:30,20 --> 00:04:32,40
So I have my data back successfully.

91
00:04:32,40 --> 00:04:34,30
I have my DOM manipulation done

92
00:04:34,30 --> 00:04:38,40
and so I have rewritten that XHR request using fetch

93
00:04:38,40 --> 00:04:41,40
and everything still works just the way it did before

94
00:04:41,40 --> 00:04:45,00
but the code is easier to read and easier to maintain.

