1
00:00:00,50 --> 00:00:01,70
- [Narrator] In this course, we learned

2
00:00:01,70 --> 00:00:04,40
several important functional concepts.

3
00:00:04,40 --> 00:00:06,60
We started off with a discussion of first-class functions

4
00:00:06,60 --> 00:00:09,20
and how they're expressed in JavaScript.

5
00:00:09,20 --> 00:00:11,60
We then moved on to a series of functions that are meant

6
00:00:11,60 --> 00:00:14,60
for working with arrays, such as Map and Reduce.

7
00:00:14,60 --> 00:00:16,40
After that, we learned how JavaScript's treatment

8
00:00:16,40 --> 00:00:18,60
of functions allows us to more easily work

9
00:00:18,60 --> 00:00:20,50
with asynchronous code.

10
00:00:20,50 --> 00:00:22,40
And, finally, we covered some more advanced concepts

11
00:00:22,40 --> 00:00:24,00
in functional programming, such as

12
00:00:24,00 --> 00:00:27,40
Partial Application and Recursion.

13
00:00:27,40 --> 00:00:28,90
A great next step for those of you

14
00:00:28,90 --> 00:00:30,40
who are looking to increase your understanding

15
00:00:30,40 --> 00:00:32,40
of functional programming further,

16
00:00:32,40 --> 00:00:33,40
is to go through your own code

17
00:00:33,40 --> 00:00:35,40
and identify areas where you can convert it

18
00:00:35,40 --> 00:00:38,40
from a procedural style to a functional style.

19
00:00:38,40 --> 00:00:40,20
This will not only make functional programming

20
00:00:40,20 --> 00:00:42,10
more relevant to you personally,

21
00:00:42,10 --> 00:00:46,00
but it also has the potential to greatly improve your code.

22
00:00:46,00 --> 00:00:47,40
And if you're looking for more courses

23
00:00:47,40 --> 00:00:50,30
on similar subjects such as Node.js and JavaScript,

24
00:00:50,30 --> 00:00:52,90
our library contains some very good ones.

25
00:00:52,90 --> 00:00:54,50
My personal recommendations would be

26
00:00:54,50 --> 00:00:57,10
Node.js Essential Training by Alex Banks

27
00:00:57,10 --> 00:01:00,40
and JavaScript Basic Training by Simon Allardice.

28
00:01:00,40 --> 00:01:02,70
The Become a Full-Stack Web Developer path

29
00:01:02,70 --> 00:01:07,10
found in our website could also be very helpful for you.

30
00:01:07,10 --> 00:01:09,60
And if you're looking for a good book on the subject,

31
00:01:09,60 --> 00:01:12,10
"Structure and Interpretation of Computer Programs",

32
00:01:12,10 --> 00:01:14,60
available through MIT OpenCourseWare,

33
00:01:14,60 --> 00:01:16,60
is a great book on the subject.

34
00:01:16,60 --> 00:01:18,60
It contains a lot of theory and exercises

35
00:01:18,60 --> 00:01:20,20
that can really help you improve your knowledge

36
00:01:20,20 --> 00:01:22,60
of functional programming.

37
00:01:22,60 --> 00:01:24,00
And finally, if you have a Twitter account,

38
00:01:24,00 --> 00:01:25,40
feel free to follow me.

39
00:01:25,40 --> 00:01:28,60
My username is @shaun_wassell.

40
00:01:28,60 --> 00:01:30,00
Thanks again, and best of luck.

