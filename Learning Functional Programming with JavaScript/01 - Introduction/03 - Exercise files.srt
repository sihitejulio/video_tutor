1
00:00:00,40 --> 00:00:02,20
- [Instructor] If you have access to the exercise files

2
00:00:02,20 --> 00:00:04,90
for this course, you can download them to your desktop

3
00:00:04,90 --> 00:00:07,20
or anywhere else that you like.

4
00:00:07,20 --> 00:00:10,00
In my case I've put them in the documents directory.

5
00:00:10,00 --> 00:00:12,60
The exercise files are organized by video.

6
00:00:12,60 --> 00:00:15,50
With the exception of chapter one video two which contains

7
00:00:15,50 --> 00:00:17,80
a test file, the rest of the folders contain

8
00:00:17,80 --> 00:00:20,80
both a begin folder, which represents the state of our code

9
00:00:20,80 --> 00:00:23,30
at the beginning of the video, and an end folder,

10
00:00:23,30 --> 00:00:26,40
which contains the completed code for each video.

11
00:00:26,40 --> 00:00:28,50
And if you don't have access to the exercises files,

12
00:00:28,50 --> 00:00:29,60
that's okay.

13
00:00:29,60 --> 00:00:32,40
You can still follow along as we go through the course.

14
00:00:32,40 --> 00:00:34,00
Now let's get started.

