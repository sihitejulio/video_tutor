1
00:00:00,60 --> 00:00:02,20
- [Narrator] There are a few basic prerequisites

2
00:00:02,20 --> 00:00:03,40
for this course.

3
00:00:03,40 --> 00:00:05,70
The first, is a basic knowledge of navigating using

4
00:00:05,70 --> 00:00:07,00
the command line.

5
00:00:07,00 --> 00:00:09,50
If you know how to use ls to list the contents

6
00:00:09,50 --> 00:00:10,50
of a directory,

7
00:00:10,50 --> 00:00:17,50
and cd to change directories, that's enough.

8
00:00:17,50 --> 00:00:20,10
The second is a basic knowledge of JavaScript Syntax.

9
00:00:20,10 --> 00:00:22,90
But even if you've never worked with JavaScript before,

10
00:00:22,90 --> 00:00:24,90
the examples I've included throughout this course

11
00:00:24,90 --> 00:00:26,90
contain relatively simple syntax,

12
00:00:26,90 --> 00:00:29,90
so you should have no problem picking it up.

13
00:00:29,90 --> 00:00:31,50
Some knowledge of object-oriented programming

14
00:00:31,50 --> 00:00:33,80
may also be helpful for some of the comparisons I'll make

15
00:00:33,80 --> 00:00:39,10
between this paradigm and functional programming.

16
00:00:39,10 --> 00:00:41,50
A basic knowledge of nodejs will also be helpful for

17
00:00:41,50 --> 00:00:43,60
running code on your local machine,

18
00:00:43,60 --> 00:00:46,20
but we'll go over how to do that as well.

19
00:00:46,20 --> 00:00:49,20
Basically, this course is meant for two kinds of people.

20
00:00:49,20 --> 00:00:51,10
The first, is those who have a knowledge of JavaScript,

21
00:00:51,10 --> 00:00:52,60
and want to learn how to incorporate

22
00:00:52,60 --> 00:00:55,50
functional programming into their code.

23
00:00:55,50 --> 00:00:57,50
The second, is those who have a basic knowledge of

24
00:00:57,50 --> 00:00:58,80
functional programming,

25
00:00:58,80 --> 00:01:00,50
and want to learn how its concepts are expressed

26
00:01:00,50 --> 00:01:01,00
in JavaScript.

