1
00:00:00,70 --> 00:00:02,00
- [Shaun] The world of software development

2
00:00:02,00 --> 00:00:03,70
is changing in a big way.

3
00:00:03,70 --> 00:00:05,40
Many developers are beginning to move away

4
00:00:05,40 --> 00:00:08,10
from the typical procedural style of programming

5
00:00:08,10 --> 00:00:10,40
and toward another style that continues to show itself

6
00:00:10,40 --> 00:00:13,50
to be more flexible, clean, and robust.

7
00:00:13,50 --> 00:00:15,90
This other style is called functional programming,

8
00:00:15,90 --> 00:00:18,80
and I'm really looking forward to teaching you about it.

9
00:00:18,80 --> 00:00:21,30
My name is Shaun Wassell, and I've recently been doing

10
00:00:21,30 --> 00:00:24,10
a lot of work with full stack JavaScript development,

11
00:00:24,10 --> 00:00:27,00
which gave me the desire to figure out a more flexible way

12
00:00:27,00 --> 00:00:29,50
of organizing all my code.

13
00:00:29,50 --> 00:00:31,70
That's how I discovered functional programming

14
00:00:31,70 --> 00:00:35,10
and how easily it can be incorporated into JavaScript.

15
00:00:35,10 --> 00:00:37,30
In this course, we'll start off talking about

16
00:00:37,30 --> 00:00:38,90
what functional programming is

17
00:00:38,90 --> 00:00:42,10
and how it contrasts with object-oriented programming.

18
00:00:42,10 --> 00:00:44,00
We'll then move on to how the basic concepts

19
00:00:44,00 --> 00:00:46,60
of functional programming are expressed in JavaScript

20
00:00:46,60 --> 00:00:48,80
and how we can use them in our code.

21
00:00:48,80 --> 00:00:51,00
Finally, we'll move on to some more advanced concepts

22
00:00:51,00 --> 00:00:52,60
in functional programming.

23
00:00:52,60 --> 00:00:54,00
Let's jump right in.

