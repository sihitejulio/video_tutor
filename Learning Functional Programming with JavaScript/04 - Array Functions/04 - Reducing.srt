1
00:00:00,70 --> 00:00:02,20
- [Instructor] There's one more important function

2
00:00:02,20 --> 00:00:04,40
that we're going to go over called Reduce.

3
00:00:04,40 --> 00:00:05,50
As its name suggests,

4
00:00:05,50 --> 00:00:07,20
Reduce takes an array and based

5
00:00:07,20 --> 00:00:08,60
on the function we give it,

6
00:00:08,60 --> 00:00:11,50
reduces the array down to a single value.

7
00:00:11,50 --> 00:00:13,40
For example it might take an array of numbers

8
00:00:13,40 --> 00:00:16,50
and reduce it down to a sum or an average.

9
00:00:16,50 --> 00:00:18,40
The syntax of Reduce is a little different

10
00:00:18,40 --> 00:00:20,10
from the ones we've seen before.

11
00:00:20,10 --> 00:00:21,80
And the other functions we've seen,

12
00:00:21,80 --> 00:00:23,50
the function we pass in generally needs

13
00:00:23,50 --> 00:00:25,00
only one argument representing

14
00:00:25,00 --> 00:00:27,00
each element of the array.

15
00:00:27,00 --> 00:00:28,30
With Reduce however,

16
00:00:28,30 --> 00:00:31,50
the function we pass in needs two arguments.

17
00:00:31,50 --> 00:00:33,30
The first argument represents the value

18
00:00:33,30 --> 00:00:34,80
accumulated so far,

19
00:00:34,80 --> 00:00:37,90
and the second represents the current element.

20
00:00:37,90 --> 00:00:39,80
Each time the function is called,

21
00:00:39,80 --> 00:00:41,80
its return value becomes the first argument

22
00:00:41,80 --> 00:00:43,80
for the next time it's called.

23
00:00:43,80 --> 00:00:45,20
So if we wanted to find the sum

24
00:00:45,20 --> 00:00:47,40
of all the numbers in an array,

25
00:00:47,40 --> 00:00:49,20
it would look like this.

26
00:00:49,20 --> 00:00:52,20
Note that we can also pass a third argument into Reduce.

27
00:00:52,20 --> 00:00:55,90
This value then becomes starting value for our accumulator.

28
00:00:55,90 --> 00:00:58,40
By default the starting value of a cumulator

29
00:00:58,40 --> 00:01:02,40
is the first element of the array we pass in.

30
00:01:02,40 --> 00:01:04,80
Let's see how Reduce is used.

31
00:01:04,80 --> 00:01:06,60
Let's say that we have an array representing

32
00:01:06,60 --> 00:01:08,70
our shopping list and we want to find

33
00:01:08,70 --> 00:01:11,50
the total cost of all the items in it.

34
00:01:11,50 --> 00:01:14,10
To do this we can use Reduce.

35
00:01:14,10 --> 00:01:20,20
First let's include lodash.

36
00:01:20,20 --> 00:01:24,10
Then we can create another variable called total cost.

37
00:01:24,10 --> 00:01:30,90
To which we assign our final reduced value.

38
00:01:30,90 --> 00:01:32,40
Then we simply return the value

39
00:01:32,40 --> 00:01:34,40
that we've accumulated so far,

40
00:01:34,40 --> 00:01:36,70
plus the price of the next item.

41
00:01:36,70 --> 00:01:38,90
Note that since by default the initial value

42
00:01:38,90 --> 00:01:40,30
of our accumulator is set

43
00:01:40,30 --> 00:01:42,00
to the first element of the array,

44
00:01:42,00 --> 00:01:43,60
in this case an object,

45
00:01:43,60 --> 00:01:47,10
we have to specify a different starting value.

46
00:01:47,10 --> 00:01:49,60
Otherwise the first time our was function called,

47
00:01:49,60 --> 00:01:52,70
we would be returning an object plus a number.

48
00:01:52,70 --> 00:01:54,40
What we want is for the initial value

49
00:01:54,40 --> 00:01:56,10
of a cumulator to be zero,

50
00:01:56,10 --> 00:01:58,50
so that the first time our function's called,

51
00:01:58,50 --> 00:02:03,40
it returns zero plus the price of the first item.

52
00:02:03,40 --> 00:02:08,30
Finally let's log the result.

53
00:02:08,30 --> 00:02:11,40
Now let's run our program.

54
00:02:11,40 --> 00:02:13,10
And we see that we have the result,

55
00:02:13,10 --> 00:02:15,00
which is the total price of all the items in our list.

