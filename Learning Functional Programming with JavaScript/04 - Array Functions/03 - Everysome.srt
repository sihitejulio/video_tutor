1
00:00:00,70 --> 00:00:03,20
- [Narrator] We now move onto two functions that behave

2
00:00:03,20 --> 00:00:05,20
very similarly to each other.

3
00:00:05,20 --> 00:00:08,10
These functions are called some and every.

4
00:00:08,10 --> 00:00:10,80
Some and every are similar to filter but instead of

5
00:00:10,80 --> 00:00:13,30
giving us an array's output, they simply return

6
00:00:13,30 --> 00:00:17,10
true or false, some returns true if the function

7
00:00:17,10 --> 00:00:19,60
we give it returns true for at least one of the elements

8
00:00:19,60 --> 00:00:22,70
in the array, every returns true only if

9
00:00:22,70 --> 00:00:24,50
the function we give it returns true for all

10
00:00:24,50 --> 00:00:28,20
of the elements in the array and false otherwise.

11
00:00:28,20 --> 00:00:31,20
So for example, if we have an array of all even numbers

12
00:00:31,20 --> 00:00:34,10
and we pass that into every along with a function

13
00:00:34,10 --> 00:00:37,10
that checks if each element is even, every would

14
00:00:37,10 --> 00:00:40,90
return true because all of the elements are even.

15
00:00:40,90 --> 00:00:44,10
If we pass this array into some with the same criteria,

16
00:00:44,10 --> 00:00:45,90
it will return true also because at

17
00:00:45,90 --> 00:00:48,60
least one of the elements is even.

18
00:00:48,60 --> 00:00:50,60
If we change one of the elements in our array

19
00:00:50,60 --> 00:00:52,90
so that it's odd and then pass it into every,

20
00:00:52,90 --> 00:00:56,70
every now returns false since all of the elements

21
00:00:56,70 --> 00:01:00,10
are not even, however if we pass this same array into some,

22
00:01:00,10 --> 00:01:03,00
it still returns true since at least

23
00:01:03,00 --> 00:01:05,10
one of the elements is even.

24
00:01:05,10 --> 00:01:07,00
If we pass an array of all odd numbers into every,

25
00:01:07,00 --> 00:01:09,70
it of course, returns false.

26
00:01:09,70 --> 00:01:13,00
And if we pass this array of all odd numbers into some,

27
00:01:13,00 --> 00:01:18,20
it also returns false since none of the elements are even.

28
00:01:18,20 --> 00:01:21,90
Now let's see what this previous example looks like in code.

29
00:01:21,90 --> 00:01:24,00
Once more, we show the typical procedural way of

30
00:01:24,00 --> 00:01:27,50
performing these checks, using a four loop to cycle

31
00:01:27,50 --> 00:01:29,90
through all the elements in an array and mutate

32
00:01:29,90 --> 00:01:32,50
a bulion variable that we create.

33
00:01:32,50 --> 00:01:35,80
Let's change this over to use every and some instead.

34
00:01:35,80 --> 00:01:38,20
Remember to include low dash so that we can use these

35
00:01:38,20 --> 00:01:43,30
functions and then redefine our

36
00:01:43,30 --> 00:01:47,40
variables in terms of every and some.

37
00:01:47,40 --> 00:01:49,60
Once more, we can simply take the condition that

38
00:01:49,60 --> 00:01:51,60
we were checking for in our four loop

39
00:01:51,60 --> 00:01:58,50
and return that from the function that we pass in.

40
00:01:58,50 --> 00:01:59,70
Now let's define this variable

41
00:01:59,70 --> 00:02:06,40
array as all even in terms of every.

42
00:02:06,40 --> 00:02:08,60
Notice that the condition that we return in this case

43
00:02:08,60 --> 00:02:10,50
is the same one that we return from some

44
00:02:10,50 --> 00:02:13,40
since we want to return true if a number is even.

45
00:02:13,40 --> 00:02:22,10
Finally, let's delete these four loops.

46
00:02:22,10 --> 00:02:36,50
We can now log our results.

47
00:02:36,50 --> 00:02:41,00
Now let's run our program.

48
00:02:41,00 --> 00:02:44,10
And we see that since our original array was all evens,

49
00:02:44,10 --> 00:02:47,30
both of these functions return true.

50
00:02:47,30 --> 00:02:51,30
Let's change one of these numbers to see what happens.

51
00:02:51,30 --> 00:02:55,80
Let's insert an odd number into our array.

52
00:02:55,80 --> 00:02:57,40
Running our code again, we see that every now

53
00:02:57,40 --> 00:02:59,80
returns false because not all of

54
00:02:59,80 --> 00:03:02,20
the elements in our array were even.

55
00:03:02,20 --> 00:03:04,20
We also see that some still returns true

56
00:03:04,20 --> 00:03:07,20
since at least one of the elements is even.

57
00:03:07,20 --> 00:03:14,40
Now let's change our array to all odds.

58
00:03:14,40 --> 00:03:16,00
Running our code one last time, we see that

59
00:03:16,00 --> 00:03:18,40
both our functions now return false since

60
00:03:18,40 --> 00:03:20,00
none of the elements are even.

