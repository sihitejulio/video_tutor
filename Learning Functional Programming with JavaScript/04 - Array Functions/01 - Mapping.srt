1
00:00:00,70 --> 00:00:01,80
- [Instructor] The first of these functions we'll be

2
00:00:01,80 --> 00:00:03,60
talking about is called map.

3
00:00:03,60 --> 00:00:05,60
Map is used when you want to take all the elements

4
00:00:05,60 --> 00:00:08,30
in an array and convert them to some other value.

5
00:00:08,30 --> 00:00:10,90
For example, if you wanted to double all the elements

6
00:00:10,90 --> 00:00:13,50
in an array or convert an array of inch measurements into

7
00:00:13,50 --> 00:00:16,10
an array of centimeter measurements.

8
00:00:16,10 --> 00:00:18,20
The way we do this is by passing map an array and

9
00:00:18,20 --> 00:00:21,80
some function to apply to each element in the array.

10
00:00:21,80 --> 00:00:24,30
Map then returns another array that contains the return

11
00:00:24,30 --> 00:00:26,90
values of the function for each element.

12
00:00:26,90 --> 00:00:29,30
So in other words, it takes each element and maps it to

13
00:00:29,30 --> 00:00:31,70
the return value of the function we give it.

14
00:00:31,70 --> 00:00:34,50
If the function we pass is something like square,

15
00:00:34,50 --> 00:00:37,20
for example, map returns an array where each of the

16
00:00:37,20 --> 00:00:39,10
numbers has been squared.

17
00:00:39,10 --> 00:00:42,00
And of course, you can pass in an anonymous function

18
00:00:42,00 --> 00:00:44,60
to make it do essentially whatever you want.

19
00:00:44,60 --> 00:00:46,90
There is one important thing to remember with the

20
00:00:46,90 --> 00:00:49,90
map function and the rest of these functions.

21
00:00:49,90 --> 00:00:52,00
In typical functional fashion, they don't actually change

22
00:00:52,00 --> 00:00:53,80
the array we pass it.

23
00:00:53,80 --> 00:00:55,70
They only return a copy.

24
00:00:55,70 --> 00:00:58,00
So don't expect them to make any changes to the

25
00:00:58,00 --> 00:01:00,80
original array, and remember to save the copy to another

26
00:01:00,80 --> 00:01:04,20
variable if you're going to need it later.

27
00:01:04,20 --> 00:01:06,70
Next I'm going to show you a few examples of how

28
00:01:06,70 --> 00:01:08,90
map is used.

29
00:01:08,90 --> 00:01:10,80
Let's say that as I said before you have an array

30
00:01:10,80 --> 00:01:13,00
of numbers, and you want to make some sort of change to

31
00:01:13,00 --> 00:01:15,10
all of the elements in the array.

32
00:01:15,10 --> 00:01:17,20
The typical procedural way of doing this is

33
00:01:17,20 --> 00:01:18,80
something like this.

34
00:01:18,80 --> 00:01:20,30
You start off with your array.

35
00:01:20,30 --> 00:01:22,20
You create another empty array so that you don't

36
00:01:22,20 --> 00:01:24,90
change the original one, and then write a for loop that

37
00:01:24,90 --> 00:01:27,50
cycles through each element in the array and then does

38
00:01:27,50 --> 00:01:30,60
something to it and pushes the results onto the new array.

39
00:01:30,60 --> 00:01:33,10
This works, and for most cases it's not necessarily

40
00:01:33,10 --> 00:01:35,90
a bad way of doing it, but it can be prone to certain

41
00:01:35,90 --> 00:01:39,20
hard to find bugs such as off by one errors.

42
00:01:39,20 --> 00:01:41,80
It's often much easier for us to think about problems

43
00:01:41,80 --> 00:01:44,10
like this in terms of what needs to happen to each element

44
00:01:44,10 --> 00:01:47,10
individually without even considering that the element

45
00:01:47,10 --> 00:01:49,80
is part of an array, as you have to when using a

46
00:01:49,80 --> 00:01:51,90
for loop like this.

47
00:01:51,90 --> 00:01:54,40
I'm going to show you how to rewrite this type of construct

48
00:01:54,40 --> 00:01:56,90
much more easily using the map function.

49
00:01:56,90 --> 00:01:59,50
First remember to include lodash by including the line

50
00:01:59,50 --> 00:02:04,50
that I showed you before.

51
00:02:04,50 --> 00:02:07,50
Next we can remove the for loop, and finally we're going to

52
00:02:07,50 --> 00:02:14,10
define our new array, numbersCubed, by using map.

53
00:02:14,10 --> 00:02:16,60
Note that the argument of this function that we pass map

54
00:02:16,60 --> 00:02:19,00
represents each individual element.

55
00:02:19,00 --> 00:02:23,70
Since we want to cube each element, we simply return

56
00:02:23,70 --> 00:02:30,60
the element times itself times itself.

57
00:02:30,60 --> 00:02:33,90
If we add a console log statement and then run our code,

58
00:02:33,90 --> 00:02:37,20
we can see that this has the same effect as the for loop,

59
00:02:37,20 --> 00:02:39,00
but looks much cleaner.

