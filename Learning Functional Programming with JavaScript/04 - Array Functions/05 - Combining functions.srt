1
00:00:00,60 --> 00:00:01,50
- [Instructor] We've seen that the functions

2
00:00:01,50 --> 00:00:04,90
map, filter, and reduce can be very useful on their own.

3
00:00:04,90 --> 00:00:06,30
But combining these functions

4
00:00:06,30 --> 00:00:08,70
can make them even more useful.

5
00:00:08,70 --> 00:00:12,50
Let's go through an example to see how.

6
00:00:12,50 --> 00:00:15,80
Imagine that we have an array of employee data.

7
00:00:15,80 --> 00:00:17,70
We want to find out how the average age of males

8
00:00:17,70 --> 00:00:20,20
compares with that of females.

9
00:00:20,20 --> 00:00:22,70
By chaining the map, filter, and reduce functions together

10
00:00:22,70 --> 00:00:28,40
we can neatly perform this calculation.

11
00:00:28,40 --> 00:00:30,40
The first thing we'll do is filter our array

12
00:00:30,40 --> 00:00:38,90
so that we end up with an array of only the male employees.

13
00:00:38,90 --> 00:00:40,60
To do this we call filter with a function

14
00:00:40,60 --> 00:00:42,50
that checks if each employee is male

15
00:00:42,50 --> 00:00:46,90
and returns true if they are.

16
00:00:46,90 --> 00:00:48,90
This will give us an array of all the male employees

17
00:00:48,90 --> 00:00:51,60
from our original array.

18
00:00:51,60 --> 00:00:53,90
We then want to convert this array of male employees

19
00:00:53,90 --> 00:01:01,40
into an array of the ages of the male employees.

20
00:01:01,40 --> 00:01:03,40
To do this we use map with a function that takes

21
00:01:03,40 --> 00:01:06,60
a male employee object and changes it into a number

22
00:01:06,60 --> 00:01:10,70
representing his age.

23
00:01:10,70 --> 00:01:13,30
Now that we have the ages of all the male employees

24
00:01:13,30 --> 00:01:14,90
we want to find out what the total is

25
00:01:14,90 --> 00:01:20,10
so that we can get the average.

26
00:01:20,10 --> 00:01:21,90
To do this we use reduce with a function

27
00:01:21,90 --> 00:01:27,00
that finds the total of all the male ages.

28
00:01:27,00 --> 00:01:31,20
This is similar to our previous example using reduce.

29
00:01:31,20 --> 00:01:32,60
But notice that since male ages

30
00:01:32,60 --> 00:01:34,40
is simply an array of numbers

31
00:01:34,40 --> 00:01:38,30
we don't have to specify a custom starting value.

32
00:01:38,30 --> 00:01:39,60
The starting value will simply be set

33
00:01:39,60 --> 00:01:46,30
to the first number in the array of male ages.

34
00:01:46,30 --> 00:01:47,30
Now that we have the total

35
00:01:47,30 --> 00:01:49,40
of the ages of all the male employees,

36
00:01:49,40 --> 00:01:52,40
we can find the average by simply dividing that number

37
00:01:52,40 --> 00:01:55,10
by the total number of male employees.

38
00:01:55,10 --> 00:02:00,70
Let's log this result for future reference.

39
00:02:00,70 --> 00:02:04,90
Now let's do the same thing with the female employees.

40
00:02:04,90 --> 00:02:06,70
Just like before we can use filter to convert

41
00:02:06,70 --> 00:02:12,70
our original array into an array of only female employees.

42
00:02:12,70 --> 00:02:14,00
The only difference is that this time

43
00:02:14,00 --> 00:02:20,30
we want to return true if the employee's gender is female.

44
00:02:20,30 --> 00:02:22,80
Now that we have an array of all the female employees,

45
00:02:22,80 --> 00:02:25,30
we want to convert those objects into simple numbers

46
00:02:25,30 --> 00:02:30,00
that represent their age.

47
00:02:30,00 --> 00:02:32,40
As before we simply use map with a function that returns

48
00:02:32,40 --> 00:02:36,80
the age of each female in the array.

49
00:02:36,80 --> 00:02:38,30
Now that we have the ages,

50
00:02:38,30 --> 00:02:47,10
we want to find the total of all those ages.

51
00:02:47,10 --> 00:02:49,00
Again we simply use reduce with a function

52
00:02:49,00 --> 00:02:55,40
that adds each age to the accumulated total.

53
00:02:55,40 --> 00:02:57,00
Now that we have the total age,

54
00:02:57,00 --> 00:02:59,20
we can find the average age by dividing that number

55
00:02:59,20 --> 00:03:02,30
by the number of female employees.

56
00:03:02,30 --> 00:03:09,90
Let's log this result as well.

57
00:03:09,90 --> 00:03:11,30
Let's just add in some simple labels

58
00:03:11,30 --> 00:03:12,30
so that we can better see

59
00:03:12,30 --> 00:03:26,20
what we're printing to the console.

60
00:03:26,20 --> 00:03:33,60
Now let's run our program to see the results.

61
00:03:33,60 --> 00:03:35,90
And here we have the average ages of the male employees

62
00:03:35,90 --> 00:03:39,60
and the female employees.

63
00:03:39,60 --> 00:03:40,70
One thing to notice is that

64
00:03:40,70 --> 00:03:42,50
in order to solve this problem

65
00:03:42,50 --> 00:03:46,00
we used a series of transformations.

66
00:03:46,00 --> 00:03:48,40
We started with our original array of employees,

67
00:03:48,40 --> 00:03:50,10
and then gradually applied transformations

68
00:03:50,10 --> 00:03:52,70
to get our final reuslt.

69
00:03:52,70 --> 00:03:56,10
This is a pretty common pattern in functional programming.

70
00:03:56,10 --> 00:03:57,70
Notice also that we never actually mutated

71
00:03:57,70 --> 00:04:00,00
the original data.

72
00:04:00,00 --> 00:04:01,90
We assigned each transformation we wanted to apply

73
00:04:01,90 --> 00:04:03,50
to a new variable.

74
00:04:03,50 --> 00:04:05,50
And this way we maintained the integrity

75
00:04:05,50 --> 00:04:07,00
of our original data.

