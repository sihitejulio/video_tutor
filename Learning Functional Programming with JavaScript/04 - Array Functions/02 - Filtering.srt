1
00:00:00,60 --> 00:00:03,00
- [Narrator] The next function is called filter.

2
00:00:03,00 --> 00:00:04,80
Filter is used when you want only the elements

3
00:00:04,80 --> 00:00:07,70
in an array that fits some kind of criteria.

4
00:00:07,70 --> 00:00:10,00
For example, if you wanted only the even numbers

5
00:00:10,00 --> 00:00:12,20
from an array of numbers or only the employees

6
00:00:12,20 --> 00:00:15,20
that make more than a certain amount per year.

7
00:00:15,20 --> 00:00:17,60
The syntax of filter is similar to map.

8
00:00:17,60 --> 00:00:19,50
The only difference is that instead of passing

9
00:00:19,50 --> 00:00:21,10
at function that returns a value

10
00:00:21,10 --> 00:00:22,80
for each element in an array,

11
00:00:22,80 --> 00:00:24,20
you pass at a function that returns

12
00:00:24,20 --> 00:00:26,70
either true of false for each element.

13
00:00:26,70 --> 00:00:28,60
If the function that you pass returns true

14
00:00:28,60 --> 00:00:29,80
for a given element,

15
00:00:29,80 --> 00:00:32,60
then that element is included in the final array.

16
00:00:32,60 --> 00:00:35,70
Otherwise, it's left out.

17
00:00:35,70 --> 00:00:39,00
Now, let's take a look at how filter is used.

18
00:00:39,00 --> 00:00:41,40
So, let's use the example I just mentioned.

19
00:00:41,40 --> 00:00:43,10
Let's say that we have a simple array of numbers

20
00:00:43,10 --> 00:00:46,50
and we want only the even numbers from the array.

21
00:00:46,50 --> 00:00:48,80
Again, here's the typical procedural way to do this

22
00:00:48,80 --> 00:00:52,60
using a for-loop to push elements on to another array.

23
00:00:52,60 --> 00:00:56,10
Let's convert this to use filter instead.

24
00:00:56,10 --> 00:00:57,90
First, let's remember to include lodash,

25
00:00:57,90 --> 00:01:03,30
so that we can use it.

26
00:01:03,30 --> 00:01:09,90
Now, let's define our new array by using filter.

27
00:01:09,90 --> 00:01:11,30
Remember, that this argument represents

28
00:01:11,30 --> 00:01:15,20
each individual element of the array we pass in.

29
00:01:15,20 --> 00:01:17,50
Since we want to return true if an element is even

30
00:01:17,50 --> 00:01:19,10
and false if it's odd,

31
00:01:19,10 --> 00:01:24,90
we can simply return this condition from our for-loop.

32
00:01:24,90 --> 00:01:28,10
Finally, let's delete our for-loop.

33
00:01:28,10 --> 00:01:29,40
Let's add a console.log so that

34
00:01:29,40 --> 00:01:33,40
we can see the result of our code.

35
00:01:33,40 --> 00:01:37,60
Finally, let's run our code.

36
00:01:37,60 --> 00:01:39,80
And we see that the array that we get out of filter

37
00:01:39,80 --> 00:01:44,10
contains only the even numbers from our original array.

38
00:01:44,10 --> 00:01:46,20
Now, let's look at another example.

39
00:01:46,20 --> 00:01:48,20
Let's say that we have an array of employees

40
00:01:48,20 --> 00:01:49,60
and we want to find out which ones

41
00:01:49,60 --> 00:01:52,10
make less than 70,000 per year.

42
00:01:52,10 --> 00:01:54,60
Let's change this code from the procedural way

43
00:01:54,60 --> 00:01:58,20
to use a more functional way using filter.

44
00:01:58,20 --> 00:02:03,70
As usual, let's include lodash, so that we can use filter.

45
00:02:03,70 --> 00:02:07,90
Now, let's define our new array with filter.

46
00:02:07,90 --> 00:02:09,90
It's worth noting that for the function we pass in

47
00:02:09,90 --> 00:02:12,50
we can name the argument whatever we want.

48
00:02:12,50 --> 00:02:13,90
Let's call it employee in this case

49
00:02:13,90 --> 00:02:17,10
to better represent what we're doing.

50
00:02:17,10 --> 00:02:18,90
Again, we can simply return this condition

51
00:02:18,90 --> 00:02:20,60
found in our for-loop.

52
00:02:20,60 --> 00:02:22,70
In this case, it returns true

53
00:02:22,70 --> 00:02:28,50
if an employee's salary is less than 70,000.

54
00:02:28,50 --> 00:02:31,00
Now, let's delete our for-loop.

55
00:02:31,00 --> 00:02:32,60
And finally, we'll add a console.log

56
00:02:32,60 --> 00:02:37,70
to see what our results are.

57
00:02:37,70 --> 00:02:41,30
Let's run our code.

58
00:02:41,30 --> 00:02:42,80
And we see that we have our desired result,

59
00:02:42,80 --> 00:02:45,50
which was all of the employees from our original array

60
00:02:45,50 --> 00:02:48,00
that make less than 70,000 per year.

