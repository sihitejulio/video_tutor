1
00:00:00,60 --> 00:00:01,40
- [Narrator] Let's install the tools

2
00:00:01,40 --> 00:00:03,70
necessary for this course.

3
00:00:03,70 --> 00:00:06,40
Nodejs will be really important for running our code

4
00:00:06,40 --> 00:00:08,30
and installing a few libraries we'll be using

5
00:00:08,30 --> 00:00:10,40
to make our programming easier.

6
00:00:10,40 --> 00:00:12,60
If your not already familiar with nodejs,

7
00:00:12,60 --> 00:00:14,00
all you need to know for this course

8
00:00:14,00 --> 00:00:15,30
is that nodejs allows you

9
00:00:15,30 --> 00:00:18,50
to run JavaScript code outside of the browser.

10
00:00:18,50 --> 00:00:19,60
This can be really helpful,

11
00:00:19,60 --> 00:00:21,20
since it means your JavaScript code

12
00:00:21,20 --> 00:00:22,90
doesn't have to be loaded and executed

13
00:00:22,90 --> 00:00:25,10
as part of a web page.

14
00:00:25,10 --> 00:00:27,50
In other words, you can write a single JavaScript file

15
00:00:27,50 --> 00:00:29,50
and run it from the command line.

16
00:00:29,50 --> 00:00:31,80
If you don't already have nodejs installed,

17
00:00:31,80 --> 00:00:33,10
you can download the installer

18
00:00:33,10 --> 00:00:36,80
from nodejs's website, nodejs.org.

19
00:00:36,80 --> 00:00:37,90
On the website, you should see a link

20
00:00:37,90 --> 00:00:41,50
to download nodejs for your operating system.

21
00:00:41,50 --> 00:00:43,20
Once you've downloaded the installer,

22
00:00:43,20 --> 00:00:44,70
just double-click it and it should take you

23
00:00:44,70 --> 00:00:47,60
through the steps required for installation.

24
00:00:47,60 --> 00:00:49,30
Once you have nodejs installed,

25
00:00:49,30 --> 00:00:50,30
you'll want to make sure that you have

26
00:00:50,30 --> 00:00:53,50
the most recent version of npm installed.

27
00:00:53,50 --> 00:00:55,40
Npm is a program that nodejs uses

28
00:00:55,40 --> 00:00:58,10
to keep track of libraries.

29
00:00:58,10 --> 00:00:59,90
We'll be using a few libraries in this course

30
00:00:59,90 --> 00:01:02,30
and npm is a package manager for node

31
00:01:02,30 --> 00:01:05,00
that allows us to easily install them.

32
00:01:05,00 --> 00:01:07,20
To install the most recent version of npm,

33
00:01:07,20 --> 00:01:08,10
just open your terminal

34
00:01:08,10 --> 00:01:16,10
and type npm install dash g npm.

35
00:01:16,10 --> 00:01:19,50
Then, type npm dash v.

36
00:01:19,50 --> 00:01:21,60
And it should show the latest version.

37
00:01:21,60 --> 00:01:24,00
Don't worry if you don't have the same version code as me,

38
00:01:24,00 --> 00:01:26,50
everything should still run correctly.

39
00:01:26,50 --> 00:01:28,80
If at any point while going through these steps

40
00:01:28,80 --> 00:01:31,70
you get an error saying something like, "Permission denied,"

41
00:01:31,70 --> 00:01:34,80
just run the above commands using sudo before everything.

42
00:01:34,80 --> 00:01:36,70
Sudo is S-U-D-O.

43
00:01:36,70 --> 00:01:38,00
And then the command.

