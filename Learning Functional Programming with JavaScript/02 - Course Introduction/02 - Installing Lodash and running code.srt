1
00:00:00,50 --> 00:00:01,40
- [Instructor] Let's take a look

2
00:00:01,40 --> 00:00:03,40
at how to run the sample code.

3
00:00:03,40 --> 00:00:05,50
First navigate to your exercise files folder

4
00:00:05,50 --> 00:00:07,20
using the command line.

5
00:00:07,20 --> 00:00:08,50
For example, if you saved the

6
00:00:08,50 --> 00:00:10,50
exercise files in the documents folder,

7
00:00:10,50 --> 00:00:15,10
you would type cd tilda slash documents

8
00:00:15,10 --> 00:00:19,10
slash exercise underscore files.

9
00:00:19,10 --> 00:00:20,90
Remember that you can see the contents of the

10
00:00:20,90 --> 00:00:23,80
folder you're currently in by typing ls.

11
00:00:23,80 --> 00:00:24,70
Here we see that we have the

12
00:00:24,70 --> 00:00:27,40
exercise file folders for each chapter.

13
00:00:27,40 --> 00:00:29,70
Now, once we're inside the exercise files folder,

14
00:00:29,70 --> 00:00:32,50
we're going to create a package.json file.

15
00:00:32,50 --> 00:00:34,80
What this file does is help the node package manager

16
00:00:34,80 --> 00:00:38,10
keep track of the npm packages we've installed.

17
00:00:38,10 --> 00:00:39,50
And, to create this file we're going to

18
00:00:39,50 --> 00:00:43,20
type npm init dash y.

19
00:00:43,20 --> 00:00:46,20
And we see that everything has been created properly.

20
00:00:46,20 --> 00:00:48,20
In order for most of our code to work for this course,

21
00:00:48,20 --> 00:00:50,90
we'll need to install an npm package called low dash

22
00:00:50,90 --> 00:00:51,90
which contains a lot of useful

23
00:00:51,90 --> 00:00:54,30
methods for functional programming.

24
00:00:54,30 --> 00:00:56,40
To install this package for our project,

25
00:00:56,40 --> 00:01:03,40
simply type npm install, low dash, dash dash save.

26
00:01:03,40 --> 00:01:04,60
Now, once you've done that,

27
00:01:04,60 --> 00:01:06,40
I've created a simple hello world program

28
00:01:06,40 --> 00:01:08,40
to check that everything's installed correctly

29
00:01:08,40 --> 00:01:10,40
and to show you how to run code.

30
00:01:10,40 --> 00:01:12,20
This program is in the first folder

31
00:01:12,20 --> 00:01:13,90
of the exercise files you downloaded

32
00:01:13,90 --> 00:01:18,80
and is called hello-world.js.

33
00:01:18,80 --> 00:01:20,30
To run this code, all you need to do

34
00:01:20,30 --> 00:01:22,30
is type node and then the file name,

35
00:01:22,30 --> 00:01:25,60
in this case hello dash world .js.

36
00:01:25,60 --> 00:01:27,50
And if everything is setup correctly,

37
00:01:27,50 --> 00:01:30,20
you should see hello world printed to the console.

38
00:01:30,20 --> 00:01:31,90
If you get an error, make sure you followed

39
00:01:31,90 --> 00:01:34,80
all the steps from this and the previous videos.

40
00:01:34,80 --> 00:01:36,00
And, there you have it, that's how

41
00:01:36,00 --> 00:01:38,00
we'll be running all our programs for this course.

