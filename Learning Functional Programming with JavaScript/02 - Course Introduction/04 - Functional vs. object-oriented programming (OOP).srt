1
00:00:00,10 --> 00:00:01,50
- [Narrator] To get a feel for

2
00:00:01,50 --> 00:00:03,50
what functional programming is like, it helps to compare it

3
00:00:03,50 --> 00:00:05,60
with object-oriented programming,

4
00:00:05,60 --> 00:00:08,50
which many of you may be familiar with.

5
00:00:08,50 --> 00:00:10,80
Let's take a look at how these two different paradigms

6
00:00:10,80 --> 00:00:13,60
might solve a simple programming problem.

7
00:00:13,60 --> 00:00:15,00
So, let's say that we want to write

8
00:00:15,00 --> 00:00:17,20
a Shopping List program that helps us keep track

9
00:00:17,20 --> 00:00:19,60
of what we need to buy at the store.

10
00:00:19,60 --> 00:00:21,80
In object-oriented programming, the main units

11
00:00:21,80 --> 00:00:25,00
of computer programs are Classes and Objects.

12
00:00:25,00 --> 00:00:27,70
So, a simple object-oriented implementation

13
00:00:27,70 --> 00:00:29,60
might look something like this:

14
00:00:29,60 --> 00:00:33,20
you have a Shopping List class and a Shopping Item class.

15
00:00:33,20 --> 00:00:34,40
Each of these has their own

16
00:00:34,40 --> 00:00:36,50
member variables which hold our data,

17
00:00:36,50 --> 00:00:39,80
and internal methods to operate on that data.

18
00:00:39,80 --> 00:00:42,20
In contrast, the only thing we need to define initially

19
00:00:42,20 --> 00:00:45,90
for a functional approach, is a single function.

20
00:00:45,90 --> 00:00:48,10
Since functional programming only operates

21
00:00:48,10 --> 00:00:49,80
on simple structures, we can assume

22
00:00:49,80 --> 00:00:52,70
that our Shopping List will be represented by an array.

23
00:00:52,70 --> 00:00:55,00
Therefore, we can define our "add item" function

24
00:00:55,00 --> 00:00:56,80
by simply returning a copy of the list

25
00:00:56,80 --> 00:00:58,30
with a new item attached,

26
00:00:58,30 --> 00:01:01,30
since List Docking CAT returns a copy.

27
00:01:01,30 --> 00:01:05,20
Creating our objects in each paradigm looks like this.

28
00:01:05,20 --> 00:01:07,40
Notice how for the object-oriented approach,

29
00:01:07,40 --> 00:01:10,60
we create a new object for each piece of data.

30
00:01:10,60 --> 00:01:11,90
In other words, we have to define

31
00:01:11,90 --> 00:01:15,20
what sort of object each piece of data represents.

32
00:01:15,20 --> 00:01:18,30
In functional programming, things look a bit different.

33
00:01:18,30 --> 00:01:21,00
Our data is contained in simple structures,

34
00:01:21,00 --> 00:01:25,00
in this case, a simple hash with name and value keys.

35
00:01:25,00 --> 00:01:27,50
In functional programming, we're not so concerned

36
00:01:27,50 --> 00:01:30,90
with defining what everything is and what it can do.

37
00:01:30,90 --> 00:01:33,60
Instead, we're more concerned with the raw data itself

38
00:01:33,60 --> 00:01:35,60
and what operations and transformations

39
00:01:35,60 --> 00:01:37,70
we can perform on it.

40
00:01:37,70 --> 00:01:39,90
And this is what adding new objects to our Shopping List

41
00:01:39,90 --> 00:01:42,80
will look like in both paradigms.

42
00:01:42,80 --> 00:01:44,40
Notice that the object-oriented way

43
00:01:44,40 --> 00:01:46,40
actually mutates the original data.

44
00:01:46,40 --> 00:01:50,60
It changes the state of my Shopping List's inner array.

45
00:01:50,60 --> 00:01:53,10
As I mentioned before, for programs as small as this one,

46
00:01:53,10 --> 00:01:55,50
it's not hard to keep track of all the changes

47
00:01:55,50 --> 00:01:58,30
that we might make to an object throughout its lifetime.

48
00:01:58,30 --> 00:02:00,40
But, once programs get bigger,

49
00:02:00,40 --> 00:02:02,40
keeping track of all the state changes

50
00:02:02,40 --> 00:02:06,20
can be a difficult task and lead to hard-to-find bugs.

51
00:02:06,20 --> 00:02:08,60
On the other hand, in typical functional style,

52
00:02:08,60 --> 00:02:11,30
we create a new variable with a name representing

53
00:02:11,30 --> 00:02:15,80
what we need it for so our original data remains intact.

54
00:02:15,80 --> 00:02:18,50
So, in this example, even though both paradigms

55
00:02:18,50 --> 00:02:20,80
are adding a new item for the same reason,

56
00:02:20,80 --> 00:02:22,60
the object-oriented way gives no hint

57
00:02:22,60 --> 00:02:24,50
of why we're adding the new item,

58
00:02:24,50 --> 00:02:26,20
whereas the functional way tells us

59
00:02:26,20 --> 00:02:28,70
through the name we give our new list.

60
00:02:28,70 --> 00:02:31,10
As I mentioned before, this is actually

61
00:02:31,10 --> 00:02:34,20
an interesting side-effect of functional programming.

62
00:02:34,20 --> 00:02:36,20
Since we have to come up with a new name

63
00:02:36,20 --> 00:02:38,40
for each transformation we perform,

64
00:02:38,40 --> 00:02:40,10
it forces us to think carefully

65
00:02:40,10 --> 00:02:42,30
about what why we're performing that transformation

66
00:02:42,30 --> 00:02:44,20
and what we'll use it for.

67
00:02:44,20 --> 00:02:46,20
This can greatly improve our code's readability

68
00:02:46,20 --> 00:02:48,90
and therefore further reduce bugs.

69
00:02:48,90 --> 00:02:51,70
For example, take a look at the object-oriented code

70
00:02:51,70 --> 00:02:52,70
on the left.

71
00:02:52,70 --> 00:02:54,30
In this code, we're going through a series

72
00:02:54,30 --> 00:02:57,00
of "if" statements, and actually mutating the internal array

73
00:02:57,00 --> 00:03:01,00
of my Shopping List depending on certain conditions.

74
00:03:01,00 --> 00:03:03,80
And after all this happens, the array remains mutated,

75
00:03:03,80 --> 00:03:05,80
so that we have to know which conditions were true

76
00:03:05,80 --> 00:03:09,00
in order to know which changes took place.

77
00:03:09,00 --> 00:03:11,30
In contrast, the functional way,

78
00:03:11,30 --> 00:03:13,70
since we create a new variable for each transformation,

79
00:03:13,70 --> 00:03:16,30
our original data remains intact.

80
00:03:16,30 --> 00:03:18,00
So that afterward, we don't have to worry

81
00:03:18,00 --> 00:03:20,00
about which conditions were actually true or not.

