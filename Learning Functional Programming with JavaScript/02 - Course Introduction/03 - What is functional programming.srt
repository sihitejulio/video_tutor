1
00:00:00,50 --> 00:00:01,30
- [Instructor] First, we'll talk about some

2
00:00:01,30 --> 00:00:04,20
of the core concepts of functional programming.

3
00:00:04,20 --> 00:00:06,40
Now, there are many different possible definitions

4
00:00:06,40 --> 00:00:08,60
of functional programming, depending on who you ask,

5
00:00:08,60 --> 00:00:11,60
but most of them agree on the following concepts.

6
00:00:11,60 --> 00:00:13,60
First of all, functional programming keeps

7
00:00:13,60 --> 00:00:15,60
functions and data separate.

8
00:00:15,60 --> 00:00:17,60
It avoids state change and mutable data,

9
00:00:17,60 --> 00:00:21,00
and it treats functions as first-class citizens.

10
00:00:21,00 --> 00:00:22,30
Let's go through each of these briefly

11
00:00:22,30 --> 00:00:24,90
so you can see what I mean.

12
00:00:24,90 --> 00:00:27,30
The first concept, keeping data and functions separate,

13
00:00:27,30 --> 00:00:29,00
is best illustrated by a comparison

14
00:00:29,00 --> 00:00:30,80
with object-oriented programming,

15
00:00:30,80 --> 00:00:33,20
which does not follow this concept.

16
00:00:33,20 --> 00:00:34,40
Object-oriented programming,

17
00:00:34,40 --> 00:00:37,10
as we see in this simple class representing a person,

18
00:00:37,10 --> 00:00:38,80
likes to group together its data

19
00:00:38,80 --> 00:00:42,10
with the functions in charge of operating on that data.

20
00:00:42,10 --> 00:00:45,00
For instance, we have this variable, age,

21
00:00:45,00 --> 00:00:46,60
and then we have a function in charge

22
00:00:46,60 --> 00:00:49,50
of incrementing the variable.

23
00:00:49,50 --> 00:00:51,80
In contrast, functional programming stores its data

24
00:00:51,80 --> 00:00:54,30
in simple constructs like arrays or hashes

25
00:00:54,30 --> 00:00:56,90
and makes separate functions that take the data

26
00:00:56,90 --> 00:00:58,80
as an argument and return some sort

27
00:00:58,80 --> 00:01:01,50
of transformed version of the data.

28
00:01:01,50 --> 00:01:03,10
Notice that in this function, increaseAge,

29
00:01:03,10 --> 00:01:05,60
we're transforming a copy of the object,

30
00:01:05,60 --> 00:01:08,30
not the original object itself.

31
00:01:08,30 --> 00:01:11,10
In the next concept, we'll see why we do this.

32
00:01:11,10 --> 00:01:12,90
The main benefit of doing things this way,

33
00:01:12,90 --> 00:01:15,10
instead of using classes, is that it gives us

34
00:01:15,10 --> 00:01:18,50
immediate polymorphism with regards to our data.

35
00:01:18,50 --> 00:01:20,00
For example, the function we've created

36
00:01:20,00 --> 00:01:22,00
works out of the box with any piece of data

37
00:01:22,00 --> 00:01:23,90
that has an age field.

38
00:01:23,90 --> 00:01:26,00
Normally, we would have had to create classes

39
00:01:26,00 --> 00:01:29,10
for each type of object and add in an increaseAge

40
00:01:29,10 --> 00:01:31,60
function for each, or mess around with having

41
00:01:31,60 --> 00:01:35,00
a Superclass for all things with age.

42
00:01:35,00 --> 00:01:37,10
Now we come to our second concept.

43
00:01:37,10 --> 00:01:39,60
When we talk about mutability and state change,

44
00:01:39,60 --> 00:01:41,20
this is what we mean.

45
00:01:41,20 --> 00:01:42,90
As we can see in this simple example,

46
00:01:42,90 --> 00:01:46,40
the variable greeting contains several different values

47
00:01:46,40 --> 00:01:48,10
throughout its lifetime.

48
00:01:48,10 --> 00:01:49,50
In a small program like this one,

49
00:01:49,50 --> 00:01:51,50
this isn't really a problem, but in a full-sized program,

50
00:01:51,50 --> 00:01:54,20
a variable like this could hold many different values,

51
00:01:54,20 --> 00:01:57,10
depending on when and where it's referenced.

52
00:01:57,10 --> 00:01:58,70
This means that when modifying the program,

53
00:01:58,70 --> 00:02:00,50
we have to keep track of all the state changes

54
00:02:00,50 --> 00:02:03,30
this variable goes through, which can be very difficult

55
00:02:03,30 --> 00:02:06,60
for even an experienced programmer.

56
00:02:06,60 --> 00:02:08,50
The solution to this problem and the pattern

57
00:02:08,50 --> 00:02:10,20
often found in functional programming

58
00:02:10,20 --> 00:02:12,50
is to make all of our data immutable,

59
00:02:12,50 --> 00:02:14,40
either through the use of constants

60
00:02:14,40 --> 00:02:16,30
or simply through treating our data

61
00:02:16,30 --> 00:02:20,60
in an immutable way and not making unnecessary changes.

62
00:02:20,60 --> 00:02:23,20
This forces us to create a new variable

63
00:02:23,20 --> 00:02:27,10
to represent each change we want to make to the original.

64
00:02:27,10 --> 00:02:29,40
Notice how, just by looking at the name of each constant,

65
00:02:29,40 --> 00:02:33,60
we can infer what it contains and what it might be used for.

66
00:02:33,60 --> 00:02:36,10
This makes debugging much easier and is one

67
00:02:36,10 --> 00:02:39,00
of the big benefits of writing functional code.

68
00:02:39,00 --> 00:02:40,90
Since in functional programming, all of our data

69
00:02:40,90 --> 00:02:42,70
is contained in simple structures,

70
00:02:42,70 --> 00:02:45,30
as I mentioned before, the cost of copying

71
00:02:45,30 --> 00:02:47,20
and transforming the data in this manner

72
00:02:47,20 --> 00:02:49,60
is relatively inexpensive.

73
00:02:49,60 --> 00:02:52,10
And finally, functional programming treats

74
00:02:52,10 --> 00:02:54,70
functions as first-class citizens.

75
00:02:54,70 --> 00:02:57,50
What this means is that just as we can assign values,

76
00:02:57,50 --> 00:03:00,30
such as numbers or strings, to a variable,

77
00:03:00,30 --> 00:03:02,90
we can also assign functions.

78
00:03:02,90 --> 00:03:05,20
In the eyes of JavaScript, the third declaration

79
00:03:05,20 --> 00:03:08,90
here is virtually no different from the other two.

80
00:03:08,90 --> 00:03:11,00
This has some important implications for the ways

81
00:03:11,00 --> 00:03:13,80
in which we can organize our programs.

82
00:03:13,80 --> 00:03:15,50
First class functions give us the ability

83
00:03:15,50 --> 00:03:18,20
to both pass functions as arguments to other functions

84
00:03:18,20 --> 00:03:20,30
and to return functions.

85
00:03:20,30 --> 00:03:22,30
As we'll see later, this can give our programs

86
00:03:22,30 --> 00:03:24,10
amazing flexibility and allow us

87
00:03:24,10 --> 00:03:27,00
to significantly increase code reusability, in some cases.

