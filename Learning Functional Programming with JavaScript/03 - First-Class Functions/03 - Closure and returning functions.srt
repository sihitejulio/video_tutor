1
00:00:00,60 --> 00:00:01,80
- [Instructor] We've already seen that functions

2
00:00:01,80 --> 00:00:04,20
can be assigned to variables and, like variables

3
00:00:04,20 --> 00:00:07,40
can be passed as arguments to other functions.

4
00:00:07,40 --> 00:00:10,30
As it happens, this means that we can also return functions

5
00:00:10,30 --> 00:00:12,60
from other functions.

6
00:00:12,60 --> 00:00:14,90
This concept when combined with assigning functions

7
00:00:14,90 --> 00:00:17,80
to variables, and passing functions as arguments,

8
00:00:17,80 --> 00:00:21,50
can enable us to write some incredibly flexible code.

9
00:00:21,50 --> 00:00:23,60
This is a simple example showing JavaScript's

10
00:00:23,60 --> 00:00:27,00
basic syntax for returning functions.

11
00:00:27,00 --> 00:00:29,50
Syntax-wise, returning functions really is just like

12
00:00:29,50 --> 00:00:32,90
returning values or other variables.

13
00:00:32,90 --> 00:00:34,70
Notice that since putting parentheses after

14
00:00:34,70 --> 00:00:37,60
a function name transforms it into its return value,

15
00:00:37,60 --> 00:00:39,80
we can do this as well.

16
00:00:39,80 --> 00:00:41,70
While this may look a little weird, it's perfectly

17
00:00:41,70 --> 00:00:44,50
valid syntax in JavaScript.

18
00:00:44,50 --> 00:00:46,70
The first set of parentheses transforms

19
00:00:46,70 --> 00:00:49,30
give me a function into its return value,

20
00:00:49,30 --> 00:00:51,80
which in this case is another function.

21
00:00:51,80 --> 00:00:53,90
The next set of parentheses transforms

22
00:00:53,90 --> 00:00:56,30
this other function into its return value,

23
00:00:56,30 --> 00:00:59,70
in the process logging a message to the console.

24
00:00:59,70 --> 00:01:01,80
It may help to think of each pair of parentheses

25
00:01:01,80 --> 00:01:04,10
as moving inward one block.

26
00:01:04,10 --> 00:01:07,60
For example, if you have several nested function blocks,

27
00:01:07,60 --> 00:01:09,90
each subsequent call returns the function

28
00:01:09,90 --> 00:01:13,00
or value one block inward.

29
00:01:13,00 --> 00:01:15,20
Note also that although it's common practice

30
00:01:15,20 --> 00:01:18,00
to return anonymous functions, it is possible

31
00:01:18,00 --> 00:01:20,70
to return named functions as well.

32
00:01:20,70 --> 00:01:22,40
Just know that the inner function won't actually

33
00:01:22,40 --> 00:01:25,60
keep its name once it's returned.

34
00:01:25,60 --> 00:01:28,00
It's also possible to return multiple functions

35
00:01:28,00 --> 00:01:30,90
in the form of a JavaScript object.

36
00:01:30,90 --> 00:01:34,70
These functions can then be called like this.

37
00:01:34,70 --> 00:01:36,60
The ability to return functions leads us back

38
00:01:36,60 --> 00:01:39,20
to a very important concept in JavaScript.

39
00:01:39,20 --> 00:01:41,50
The concept of scope.

40
00:01:41,50 --> 00:01:43,80
As you may know, normal variable in JavaScript

41
00:01:43,80 --> 00:01:46,00
are function scoped, meaning that variables

42
00:01:46,00 --> 00:01:48,60
defined within a function are available

43
00:01:48,60 --> 00:01:50,40
only within the opening and closing brackets

44
00:01:50,40 --> 00:01:53,20
of that function.

45
00:01:53,20 --> 00:01:55,10
When returning functions, this scope has

46
00:01:55,10 --> 00:01:57,70
some interesting implications.

47
00:01:57,70 --> 00:01:59,80
If we take a look at this code snippet,

48
00:01:59,80 --> 00:02:01,30
we can see that even though the function

49
00:02:01,30 --> 00:02:04,80
printValueOfX is outside the scope where x was defined,

50
00:02:04,80 --> 00:02:07,70
it still has access to x.

51
00:02:07,70 --> 00:02:10,90
The culprit here is a concept called, closure.

52
00:02:10,90 --> 00:02:13,60
Here we say that the function printValueOfX

53
00:02:13,60 --> 00:02:16,70
captures the variable x.

54
00:02:16,70 --> 00:02:18,70
This can be used to implement a wide variety

55
00:02:18,70 --> 00:02:20,40
of useful features.

56
00:02:20,40 --> 00:02:24,90
Let's take a look at how it can be put to use.

57
00:02:24,90 --> 00:02:27,10
Let's say that we have a simple Javascript object

58
00:02:27,10 --> 00:02:30,10
like this, that represents a counter.

59
00:02:30,10 --> 00:02:32,80
It has a variable that keeps track of the count,

60
00:02:32,80 --> 00:02:34,60
and methods for incrementing the count,

61
00:02:34,60 --> 00:02:37,60
and retrieving the value of the count.

62
00:02:37,60 --> 00:02:39,80
Working with this object is fairly straightforward

63
00:02:39,80 --> 00:02:42,30
until somewhere in the code, someone manually

64
00:02:42,30 --> 00:02:46,80
sets the count by referring directly to the count property.

65
00:02:46,80 --> 00:02:48,90
If you're familiar with Object Oriented Programming,

66
00:02:48,90 --> 00:02:52,50
then you're aware of this need for private variables.

67
00:02:52,50 --> 00:02:55,70
There's currently no private keyword in native Javascript.

68
00:02:55,70 --> 00:02:57,50
So, let's use the concepts of closures

69
00:02:57,50 --> 00:03:01,70
and returning functions to implement the private variables.

70
00:03:01,70 --> 00:03:04,20
What we want is a function that returns two functions,

71
00:03:04,20 --> 00:03:06,60
each of which captures an inner variable.

72
00:03:06,60 --> 00:03:09,00
Using just this simple description, we can guess

73
00:03:09,00 --> 00:03:10,90
that the structure of our function is going

74
00:03:10,90 --> 00:03:15,70
to look like this.

75
00:03:15,70 --> 00:03:17,90
Let's call our function createCounter,

76
00:03:17,90 --> 00:03:20,00
because as you'll see later, this is exactly

77
00:03:20,00 --> 00:03:21,70
what it's doing.

78
00:03:21,70 --> 00:03:24,00
So inside our createCounter function, we're going to

79
00:03:24,00 --> 00:03:26,30
create a variable called, count, that will hold

80
00:03:26,30 --> 00:03:28,80
the current value of our counter.

81
00:03:28,80 --> 00:03:31,40
Then we're going to be returning two functions.

82
00:03:31,40 --> 00:03:33,50
Remember, the way we do this is by returning

83
00:03:33,50 --> 00:03:37,00
a JavaScript object with functions inside of it.

84
00:03:37,00 --> 00:03:38,90
Remember that since these functions we're returning

85
00:03:38,90 --> 00:03:40,90
are defined within the scope of the function

86
00:03:40,90 --> 00:03:43,70
that returns them, they have access to all the variables

87
00:03:43,70 --> 00:03:45,90
defined within that function.

88
00:03:45,90 --> 00:03:49,40
So we can simply reference count inside of them.

89
00:03:49,40 --> 00:03:51,40
So the way we'll write our increment function

90
00:03:51,40 --> 00:03:55,00
is simply by incrementing count.

91
00:03:55,00 --> 00:03:57,40
And for our currentValue function,

92
00:03:57,40 --> 00:04:00,20
we'll simply return count.

93
00:04:00,20 --> 00:04:02,60
Once all of that's in place, we can create a new counter

94
00:04:02,60 --> 00:04:05,20
simply by calling createCounter and assigning

95
00:04:05,20 --> 00:04:08,50
the return value, in this case a Javascript object

96
00:04:08,50 --> 00:04:12,20
with our two functions, to a new variable.

97
00:04:12,20 --> 00:04:14,10
We can then call increment and get value

98
00:04:14,10 --> 00:04:17,00
whenever we want to.

99
00:04:17,00 --> 00:04:20,40
Notice that we can no longer reference myCounter.count,

100
00:04:20,40 --> 00:04:22,50
and if we try to, we'll get an error saying

101
00:04:22,50 --> 00:04:30,90
that myCounter does not have a property called count.

102
00:04:30,90 --> 00:04:34,10
And if we take a look at what myCounter is exactly,

103
00:04:34,10 --> 00:04:36,80
this object here, we see that it really doesn't have

104
00:04:36,80 --> 00:04:39,20
a property called, count.

105
00:04:39,20 --> 00:04:42,50
It's only captured in the two functions here.

106
00:04:42,50 --> 00:04:45,20
Another thing to notice is that if the outer function

107
00:04:45,20 --> 00:04:48,20
has arguments, then the inner functions have access

108
00:04:48,20 --> 00:04:50,20
to those as well.

109
00:04:50,20 --> 00:04:52,90
So what if we wanted to revise our previous example

110
00:04:52,90 --> 00:04:55,10
to allow us to specify a starting value

111
00:04:55,10 --> 00:04:57,10
for our counter by passing in an argument

112
00:04:57,10 --> 00:04:59,00
to createCounter?

113
00:04:59,00 --> 00:05:01,50
Well, our first instinct, especially if we're used to

114
00:05:01,50 --> 00:05:04,10
languages that use constructors, might be

115
00:05:04,10 --> 00:05:06,30
to assign this argument to another variable

116
00:05:06,30 --> 00:05:08,10
within the function.

117
00:05:08,10 --> 00:05:10,20
And this will work perfectly well, but there's a

118
00:05:10,20 --> 00:05:12,50
cleaner way to do it.

119
00:05:12,50 --> 00:05:14,80
Since we have access to the arguments passed

120
00:05:14,80 --> 00:05:17,10
into the outer function, as well as the variables

121
00:05:17,10 --> 00:05:19,40
defined within the outer function,

122
00:05:19,40 --> 00:05:21,70
we can simply skip this middle step

123
00:05:21,70 --> 00:05:23,30
and reference the argument directly

124
00:05:23,30 --> 00:05:28,00
from the inner functions.

125
00:05:28,00 --> 00:05:29,60
This will work exactly the same way as

126
00:05:29,60 --> 00:05:32,70
our previous example, except now we can specify

127
00:05:32,70 --> 00:05:40,10
what the counter starts at.

128
00:05:40,10 --> 00:05:41,90
Now, note that what I've just shown you,

129
00:05:41,90 --> 00:05:44,40
although it's built on functional concepts,

130
00:05:44,40 --> 00:05:47,60
is really more object oriented in nature.

131
00:05:47,60 --> 00:05:49,40
For one, it has this mutable variable that can

132
00:05:49,40 --> 00:05:51,50
really be changed from anywhere that has a reference

133
00:05:51,50 --> 00:05:54,60
to the counter by calling increment.

134
00:05:54,60 --> 00:05:56,80
Secondly, it wraps up our data in the functions

135
00:05:56,80 --> 00:05:59,90
we used to operate on it into one package.

136
00:05:59,90 --> 00:06:02,40
The reason I've shown you this example is, one,

137
00:06:02,40 --> 00:06:04,40
because many of you are probably coming from

138
00:06:04,40 --> 00:06:07,70
an object oriented background, and so this example

139
00:06:07,70 --> 00:06:11,40
can serve as a sort of bridge into the functional world,

140
00:06:11,40 --> 00:06:13,70
and two, so that you can get the hang of closures

141
00:06:13,70 --> 00:06:17,10
and returning functions, and appreciate how closely linked

142
00:06:17,10 --> 00:06:19,00
the two concepts are.

143
00:06:19,00 --> 00:06:22,00
In fact, there are very few times in functional programming

144
00:06:22,00 --> 00:06:25,00
where we return a function without making use of closures.

