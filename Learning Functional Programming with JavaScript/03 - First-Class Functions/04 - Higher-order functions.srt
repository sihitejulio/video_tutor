1
00:00:00,10 --> 00:00:02,20
- [Narrator] So, we've seen how functions can be passed

2
00:00:02,20 --> 00:00:04,90
as arguments, and return from other functions.

3
00:00:04,90 --> 00:00:07,30
There's a name for functions that deal with other functions

4
00:00:07,30 --> 00:00:10,50
in this way, and that's higher-order functions.

5
00:00:10,50 --> 00:00:12,30
A higher-order function is, simply, a function that

6
00:00:12,30 --> 00:00:14,40
takes another function as an argument,

7
00:00:14,40 --> 00:00:17,30
returns another function, or both.

8
00:00:17,30 --> 00:00:19,40
These kind of functions are called higher-order functions

9
00:00:19,40 --> 00:00:22,00
because, in contrast with basic functions,

10
00:00:22,00 --> 00:00:24,20
which work with just data, these functions

11
00:00:24,20 --> 00:00:26,80
work with other functions as well.

12
00:00:26,80 --> 00:00:29,90
Let's cut straight to the examples.

13
00:00:29,90 --> 00:00:32,40
We create a function where we pass in the two arguments

14
00:00:32,40 --> 00:00:34,50
we want checked, along with the function that

15
00:00:34,50 --> 00:00:38,40
we want called if the two arguments fit our criteria.

16
00:00:38,40 --> 00:00:42,80
We'll call this function do if safe.

17
00:00:42,80 --> 00:00:44,80
Now, this function will perform the validation

18
00:00:44,80 --> 00:00:47,90
checks for us, on the arguments that we pass in.

19
00:00:47,90 --> 00:00:50,70
And then, if those pass, it will call our function

20
00:00:50,70 --> 00:00:53,80
with those two arguments.

21
00:00:53,80 --> 00:00:56,20
Note that since some of our functions have a return

22
00:00:56,20 --> 00:00:59,10
value, we want to pass this along.

23
00:00:59,10 --> 00:01:00,70
The way we do this is simply by putting a

24
00:01:00,70 --> 00:01:03,40
return statement in front of this.

25
00:01:03,40 --> 00:01:05,00
This allows us to remove the validation

26
00:01:05,00 --> 00:01:07,60
logic from inside of our functions,

27
00:01:07,60 --> 00:01:14,10
keeping them focused on their original purpose.

28
00:01:14,10 --> 00:01:16,70
We can then switch over our calls to this do if safe

29
00:01:16,70 --> 00:01:19,30
function, which will give us the same result,

30
00:01:19,30 --> 00:01:21,10
but we'll first check the arguments

31
00:01:21,10 --> 00:01:27,30
to make sure they fit our criteria.

32
00:01:27,30 --> 00:01:29,80
Notice how this function can be used with a variety

33
00:01:29,80 --> 00:01:32,70
of different functions that look similar to each other.

34
00:01:32,70 --> 00:01:34,80
This is really the essence of higher-order functions,

35
00:01:34,80 --> 00:01:36,40
working with classes of functions

36
00:01:36,40 --> 00:01:43,30
that have similar needs and usages.

37
00:01:43,30 --> 00:01:46,00
Let's take this a step further, and incorporate returning

38
00:01:46,00 --> 00:01:48,80
functions into this validation.

39
00:01:48,80 --> 00:01:50,70
So, currently, when we call do if safe,

40
00:01:50,70 --> 00:01:52,40
it immediately checks the arguments

41
00:01:52,40 --> 00:01:55,40
and calls the function we give it.

42
00:01:55,40 --> 00:01:57,30
What we really want is a function that converts

43
00:01:57,30 --> 00:01:59,20
our functions into another function

44
00:01:59,20 --> 00:02:02,30
that will automatically check the parameters we pass it.

45
00:02:02,30 --> 00:02:04,80
In order to do this, we need to both take a function

46
00:02:04,80 --> 00:02:07,70
as an argument, and return a function.

47
00:02:07,70 --> 00:02:09,50
First, let's change the name of this function

48
00:02:09,50 --> 00:02:14,40
from do if safe to create safe version.

49
00:02:14,40 --> 00:02:16,90
Now, instead of passing all our arguments at once,

50
00:02:16,90 --> 00:02:20,70
we only want to pass one of them, the function.

51
00:02:20,70 --> 00:02:22,80
Once we do this, we want our function to return

52
00:02:22,80 --> 00:02:25,60
another function that will take the two arguments

53
00:02:25,60 --> 00:02:29,30
we want to call the original function with.

54
00:02:29,30 --> 00:02:32,30
This is going to look like this.

55
00:02:32,30 --> 00:02:34,80
In essence, we're delaying the execution of our

56
00:02:34,80 --> 00:02:37,00
original function until we have all the arguments

57
00:02:37,00 --> 00:02:38,80
required to call it.

58
00:02:38,80 --> 00:02:41,70
Let's see how we use this function.

59
00:02:41,70 --> 00:02:44,40
Since create safe version returns a function,

60
00:02:44,40 --> 00:02:49,30
we want to assign that function to another variable,

61
00:02:49,30 --> 00:02:55,80
and that looks like this.

62
00:02:55,80 --> 00:02:58,10
We can then call these functions with two arguments,

63
00:02:58,10 --> 00:03:00,50
and it performs the validation for us,

64
00:03:00,50 --> 00:03:09,80
automatically, just as we wanted.

65
00:03:09,80 --> 00:03:11,70
If we try and call one of these functions with

66
00:03:11,70 --> 00:03:13,80
a argument that's not the correct type,

67
00:03:13,80 --> 00:03:16,90
we'll simply get a null value returned.

68
00:03:16,90 --> 00:03:18,50
As you can see from this example,

69
00:03:18,50 --> 00:03:20,10
functions like this allow us to construct

70
00:03:20,10 --> 00:03:23,00
more complex functions out of simpler functions,

71
00:03:23,00 --> 00:03:24,50
which can be incredibly useful in keeping

72
00:03:24,50 --> 00:03:26,00
our code readable and flexible.

