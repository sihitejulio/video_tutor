1
00:00:00,70 --> 00:00:01,50
- [Instructor] First we'll talk about

2
00:00:01,50 --> 00:00:04,40
how we assign functions to variables.

3
00:00:04,40 --> 00:00:07,20
As you can see from this example, it's quite simple.

4
00:00:07,20 --> 00:00:08,40
We just write it the same way

5
00:00:08,40 --> 00:00:09,80
as assigning any other value,

6
00:00:09,80 --> 00:00:13,60
except we put a function after the equal sign.

7
00:00:13,60 --> 00:00:16,30
These variables can then be called exactly like functions

8
00:00:16,30 --> 00:00:17,80
by adding parentheses after them

9
00:00:17,80 --> 00:00:20,50
with the correct number of arguments.

10
00:00:20,50 --> 00:00:22,10
Let's take a moment to notice a difference

11
00:00:22,10 --> 00:00:25,20
between a function variable with parentheses after it

12
00:00:25,20 --> 00:00:28,80
and a function variable without parentheses.

13
00:00:28,80 --> 00:00:31,80
Essentially, putting parentheses after a function variable

14
00:00:31,80 --> 00:00:35,30
transforms it into whatever its return value is.

15
00:00:35,30 --> 00:00:36,90
Writing the value without parentheses

16
00:00:36,90 --> 00:00:40,30
simply represents the function itself.

17
00:00:40,30 --> 00:00:41,80
You may be wondering what the difference is

18
00:00:41,80 --> 00:00:43,90
between defining functions the normal way

19
00:00:43,90 --> 00:00:46,00
and assigning functions to variables.

20
00:00:46,00 --> 00:00:49,80
After all, they do look and work very similarly.

21
00:00:49,80 --> 00:00:52,40
However, there is one important difference.

22
00:00:52,40 --> 00:00:54,50
When functions are defined the normal way,

23
00:00:54,50 --> 00:00:56,30
it doesn't matter whether we use them

24
00:00:56,30 --> 00:01:00,30
before or after their declaration in the code.

25
00:01:00,30 --> 00:01:03,40
This is because functions defined this way are hoisted,

26
00:01:03,40 --> 00:01:05,20
meaning that the compiler automatically

27
00:01:05,20 --> 00:01:08,40
moves them to the top of the file.

28
00:01:08,40 --> 00:01:10,90
When we assign functions to variables, however,

29
00:01:10,90 --> 00:01:14,10
we can only use them after they've been declared.

30
00:01:14,10 --> 00:01:16,50
This makes sense, since, with all other variables,

31
00:01:16,50 --> 00:01:19,70
we can only use them after they've been defined, as well.

32
00:01:19,70 --> 00:01:21,50
Now let's go over a few potential uses

33
00:01:21,50 --> 00:01:25,00
for assigning functions to variables.

34
00:01:25,00 --> 00:01:26,70
First you'll want to use your terminal

35
00:01:26,70 --> 00:01:29,70
to navigate to this video's exercise files,

36
00:01:29,70 --> 00:01:31,00
and then to the Begin folder

37
00:01:31,00 --> 00:01:32,20
so that we can run our code

38
00:01:32,20 --> 00:01:34,60
to verify that our changes work.

39
00:01:34,60 --> 00:01:45,30
To do this, simply type cd ~/Documents/Exercise_Files/,

40
00:01:45,30 --> 00:01:47,60
and then navigate to our video,

41
00:01:47,60 --> 00:01:55,10
which is 02_02, and then Begin.

42
00:01:55,10 --> 00:02:00,20
And we see that we have our exercise files here.

43
00:02:00,20 --> 00:02:01,40
The first use of this concept

44
00:02:01,40 --> 00:02:03,00
is to rename builtin functions

45
00:02:03,00 --> 00:02:05,40
to improve code readability.

46
00:02:05,40 --> 00:02:07,10
For example, if we find that we're calling

47
00:02:07,10 --> 00:02:09,90
console.log a lot, we can use this concept

48
00:02:09,90 --> 00:02:14,00
to rename console.log to something shorter, such as line.

49
00:02:14,00 --> 00:02:18,30
The way we do this is by simply creating a new variable,

50
00:02:18,30 --> 00:02:22,30
and assigning the function console.log to it.

51
00:02:22,30 --> 00:02:24,50
Once we've done that, we can simply replace all the

52
00:02:24,50 --> 00:02:35,20
occurrences of console.log with our new variable, line.

53
00:02:35,20 --> 00:02:38,60
And if we now run our code using node,

54
00:02:38,60 --> 00:02:41,00
we see that it has the exact same functionality,

55
00:02:41,00 --> 00:02:43,90
while the code is more concise.

56
00:02:43,90 --> 00:02:47,40
Notice that when we assign console.log to another variable,

57
00:02:47,40 --> 00:02:49,90
we leave the parentheses off of it.

58
00:02:49,90 --> 00:02:55,10
Now what would happen if we put the parentheses on it?

59
00:02:55,10 --> 00:02:58,00
Well, as we see here, we get this error

60
00:02:58,00 --> 00:03:00,70
saying that line is not a function.

61
00:03:00,70 --> 00:03:03,60
What does it mean by that, exactly?

62
00:03:03,60 --> 00:03:04,90
Well, as I mentioned before,

63
00:03:04,90 --> 00:03:06,60
adding parentheses after a function

64
00:03:06,60 --> 00:03:10,30
translates that function into its return value.

65
00:03:10,30 --> 00:03:12,40
So instead of saying that our variable line

66
00:03:12,40 --> 00:03:15,30
is equal to the function console.log,

67
00:03:15,30 --> 00:03:18,80
we say that it's equal to the return value of console.log,

68
00:03:18,80 --> 00:03:21,50
which, as it happens, is undefined.

69
00:03:21,50 --> 00:03:23,60
So, essentially, what we're doing in this case is,

70
00:03:23,60 --> 00:03:25,30
every time we call line,

71
00:03:25,30 --> 00:03:29,30
we're actually trying to call undefined as a function,

72
00:03:29,30 --> 00:03:32,70
which really doesn't make a whole lot of sense.

73
00:03:32,70 --> 00:03:34,90
This is just something you should keep in mind while you're

74
00:03:34,90 --> 00:03:37,40
learning functional programming with JavaScript.

75
00:03:37,40 --> 00:03:40,80
Parentheses can make all the difference.

76
00:03:40,80 --> 00:03:42,10
Another use for this concept

77
00:03:42,10 --> 00:03:43,60
is to make functions behave differently

78
00:03:43,60 --> 00:03:46,60
depending on certain environment criteria.

79
00:03:46,60 --> 00:03:48,60
I'll show you what I mean by that.

80
00:03:48,60 --> 00:03:50,30
Quite often in our code, we want to print

81
00:03:50,30 --> 00:03:53,50
statements to the console for debugging purposes.

82
00:03:53,50 --> 00:03:55,50
But we only want these messages to show up

83
00:03:55,50 --> 00:03:57,20
when we're actually debugging.

84
00:03:57,20 --> 00:03:59,60
Otherwise, they can clutter up the console.

85
00:03:59,60 --> 00:04:01,80
And we certainly don't want to have to go in and delete

86
00:04:01,80 --> 00:04:03,60
or comment out all the debug statements

87
00:04:03,60 --> 00:04:06,20
and re-add them when we need them again.

88
00:04:06,20 --> 00:04:08,80
So what we can do here is have an environment variable

89
00:04:08,80 --> 00:04:10,20
that we can change to indicate

90
00:04:10,20 --> 00:04:12,90
whether we're in debug mode or not.

91
00:04:12,90 --> 00:04:15,00
Then, depending on this variable,

92
00:04:15,00 --> 00:04:17,20
we either assign the variable debug

93
00:04:17,20 --> 00:04:18,90
a function that prints the message,

94
00:04:18,90 --> 00:04:29,80
or a function that does nothing.

95
00:04:29,80 --> 00:04:33,20
So if we run this program when debug enabled is true,

96
00:04:33,20 --> 00:04:36,80
we see that it prints our debug message.

97
00:04:36,80 --> 00:04:41,50
And if we set it to false, we see that it does nothing.

98
00:04:41,50 --> 00:04:44,10
Which is exactly what we wanted.

99
00:04:44,10 --> 00:04:46,10
You may be wondering why we bother assigning

100
00:04:46,10 --> 00:04:49,20
different functions to this debug variable when we could

101
00:04:49,20 --> 00:04:57,60
just add the logic into the function itself, like this.

102
00:04:57,60 --> 00:04:58,90
Well, at the moment, both of these

103
00:04:58,90 --> 00:05:00,80
work just as well as each other.

104
00:05:00,80 --> 00:05:03,10
But once we get into passing functions as arguments

105
00:05:03,10 --> 00:05:05,00
and returning them from other functions,

106
00:05:05,00 --> 00:05:07,50
we'll see that the ability to dynamically assign

107
00:05:07,50 --> 00:05:10,40
different functions at runtime can be a huge benefit,

108
00:05:10,40 --> 00:05:13,00
both in terms of flexibility and readability.

