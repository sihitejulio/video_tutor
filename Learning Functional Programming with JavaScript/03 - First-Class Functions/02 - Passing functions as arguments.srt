1
00:00:00,60 --> 00:00:02,10
- [Narrator] Now let's how functions can be passed

2
00:00:02,10 --> 00:00:03,80
as arguments and some of the ways

3
00:00:03,80 --> 00:00:06,00
that this can be used in our code.

4
00:00:06,00 --> 00:00:09,10
You're already familiar with passing values to functions.

5
00:00:09,10 --> 00:00:11,10
When we pass values as arguments,

6
00:00:11,10 --> 00:00:13,20
essentially what they do is change the data

7
00:00:13,20 --> 00:00:15,30
that the function operates on.

8
00:00:15,30 --> 00:00:17,60
For example, if we have some simple functions

9
00:00:17,60 --> 00:00:20,10
that combine two numbers using some operations,

10
00:00:20,10 --> 00:00:24,60
the arguments we pass in specify what those numbers are.

11
00:00:24,60 --> 00:00:26,60
But what if instead of passing arguments

12
00:00:26,60 --> 00:00:28,60
to specify what our data is,

13
00:00:28,60 --> 00:00:30,20
we passed in arguments to specify

14
00:00:30,20 --> 00:00:32,70
what was done to that data.

15
00:00:32,70 --> 00:00:35,10
JavaScript let's us do just that by allowing us

16
00:00:35,10 --> 00:00:37,70
to pass functions as arguments.

17
00:00:37,70 --> 00:00:39,70
In other words instead of passing values

18
00:00:39,70 --> 00:00:41,50
we're passing actions.

19
00:00:41,50 --> 00:00:43,80
This is likely a somewhat strange concept

20
00:00:43,80 --> 00:00:45,90
if you're coming from object oriented programming

21
00:00:45,90 --> 00:00:47,40
so let's go over a few examples

22
00:00:47,40 --> 00:00:51,70
showing how this can be used.

23
00:00:51,70 --> 00:00:54,90
Just for the sake of an example, take a look at this code.

24
00:00:54,90 --> 00:00:57,50
In this simple program we have this repeated pattern

25
00:00:57,50 --> 00:01:01,00
where if a condition met we call some function.

26
00:01:01,00 --> 00:01:03,00
What if we wanted to abstract away this pattern

27
00:01:03,00 --> 00:01:04,70
into its own function?

28
00:01:04,70 --> 00:01:07,30
In other words, we want a function that takes a condition

29
00:01:07,30 --> 00:01:09,50
and a function as its arguments

30
00:01:09,50 --> 00:01:12,80
and if the condition is true, calls the function.

31
00:01:12,80 --> 00:01:15,70
We'll call this function doif.

32
00:01:15,70 --> 00:01:19,00
This function will have the two arguments condition

33
00:01:19,00 --> 00:01:22,30
and a function to call if that condition is true.

34
00:01:22,30 --> 00:01:25,60
Inside this function we want to do exactly as I just said.

35
00:01:25,60 --> 00:01:28,30
First we want to check if the condition is true

36
00:01:28,30 --> 00:01:32,30
and if it is then we want to call the function.

37
00:01:32,30 --> 00:01:33,80
Now that we have this function in place,

38
00:01:33,80 --> 00:01:38,60
let's use it as a substitute for all these if statements.

39
00:01:38,60 --> 00:01:49,70
That's going to look something like this.

40
00:01:49,70 --> 00:01:52,70
Notice again the lack of parentheses.

41
00:01:52,70 --> 00:01:54,70
Up until now we've been naming functions

42
00:01:54,70 --> 00:01:57,20
before passing them as arguments.

43
00:01:57,20 --> 00:01:58,90
While for the sake of readability this is

44
00:01:58,90 --> 00:02:01,30
generally considered a good practice,

45
00:02:01,30 --> 00:02:03,30
there's a very common practice in JavaScript

46
00:02:03,30 --> 00:02:05,80
called anonymous functions.

47
00:02:05,80 --> 00:02:08,90
An anonymous function is just a function without a name.

48
00:02:08,90 --> 00:02:10,30
This is what we used when assigning

49
00:02:10,30 --> 00:02:13,10
functions to variables earlier.

50
00:02:13,10 --> 00:02:15,00
Using this code as an example,

51
00:02:15,00 --> 00:02:17,20
instead of creating a new named function

52
00:02:17,20 --> 00:02:19,30
for each action we want it to perform,

53
00:02:19,30 --> 00:02:24,00
we can simply do the following.

54
00:02:24,00 --> 00:02:26,00
This can often be more convenient to write,

55
00:02:26,00 --> 00:02:28,00
especially if the functionality you're looking for

56
00:02:28,00 --> 00:02:48,30
is very specific to a certain instance as it is here.

57
00:02:48,30 --> 00:02:50,60
Going back to our simple add and subtract functions

58
00:02:50,60 --> 00:02:53,50
earlier in the video you can also pass in functions

59
00:02:53,50 --> 00:02:55,80
that take arguments.

60
00:02:55,80 --> 00:02:57,60
For example we can create a simple function

61
00:02:57,60 --> 00:03:00,70
that takes two values and a function as arguments

62
00:03:00,70 --> 00:03:04,60
and then calls that function with those two values.

63
00:03:04,60 --> 00:03:11,60
Anonymous functions can be used here as well.

64
00:03:11,60 --> 00:03:14,00
You can even pass in multiple functions.

65
00:03:14,00 --> 00:03:16,30
Let's say that we want to take our doif

66
00:03:16,30 --> 00:03:17,80
and change its functionality to be

67
00:03:17,80 --> 00:03:20,40
more like an if else statement.

68
00:03:20,40 --> 00:03:23,60
In other words, we want our function to take a condition

69
00:03:23,60 --> 00:03:25,50
and two functions as arguments.

70
00:03:25,50 --> 00:03:28,90
If the condition is true, it should call the first one,

71
00:03:28,90 --> 00:03:31,30
otherwise it should call the second one.

72
00:03:31,30 --> 00:03:33,00
All we have to do is add another argument

73
00:03:33,00 --> 00:03:35,00
onto our do if function

74
00:03:35,00 --> 00:03:36,30
and then add an else block

75
00:03:36,30 --> 00:03:39,70
where the second function is called.

76
00:03:39,70 --> 00:03:43,60
Finally let's change the function's name to ifElse.

77
00:03:43,60 --> 00:03:45,70
This function can then be called very similarly

78
00:03:45,70 --> 00:03:47,80
to an if statement, except now we're doing it

79
00:03:47,80 --> 00:03:50,60
in a functional way.

80
00:03:50,60 --> 00:03:52,80
Now some of you may have noticed in these examples

81
00:03:52,80 --> 00:03:55,30
that there's still a lot of repeated code.

82
00:03:55,30 --> 00:03:57,20
Mainly we're creating a lot of functions

83
00:03:57,20 --> 00:04:01,30
that do exactly the same thing with slight variations.

84
00:04:01,30 --> 00:04:04,70
All these functions for example, call console.log

85
00:04:04,70 --> 00:04:07,80
with a different message about the state of x.

86
00:04:07,80 --> 00:04:09,20
And it seems like we should be able to

87
00:04:09,20 --> 00:04:11,60
abstract this out into a function.

88
00:04:11,60 --> 00:04:13,10
For that, we'll need to take a look at

89
00:04:13,10 --> 00:04:16,00
our third and final concept, returning functions.

