1
00:00:00,30 --> 00:00:02,70
- [Narrator] First we'll talk about partial application.

2
00:00:02,70 --> 00:00:05,90
Now what partial application is is this.

3
00:00:05,90 --> 00:00:07,20
Let's say that we have a function

4
00:00:07,20 --> 00:00:09,60
with a certain number of arguments.

5
00:00:09,60 --> 00:00:11,90
What partial application is is taking one

6
00:00:11,90 --> 00:00:14,30
of those arguments and fixing the value.

7
00:00:14,30 --> 00:00:17,90
This gives us a function with one less argument.

8
00:00:17,90 --> 00:00:20,50
We can then call this function and it will call

9
00:00:20,50 --> 00:00:23,00
our original function with both the argument

10
00:00:23,00 --> 00:00:27,70
that we fixed and the new argument that we pass in.

11
00:00:27,70 --> 00:00:29,40
Let's change this example around a little bit

12
00:00:29,40 --> 00:00:33,10
so that we can get a better idea of how this works.

13
00:00:33,10 --> 00:00:35,50
So, what if instead of having an add function that takes

14
00:00:35,50 --> 00:00:41,40
only two arguments, we had one that took three?

15
00:00:41,40 --> 00:00:44,50
If we wanted to partially apply only the first argument,

16
00:00:44,50 --> 00:00:51,80
we could change it like this.

17
00:00:51,80 --> 00:00:53,90
Notice that now when we call partiallyApply,

18
00:00:53,90 --> 00:00:56,80
we get a function that takes two arguments.

19
00:00:56,80 --> 00:00:59,80
These two arguments represent the last two arguments

20
00:00:59,80 --> 00:01:02,30
of our add function.

21
00:01:02,30 --> 00:01:06,50
Now we can call add5 with another argument.

22
00:01:06,50 --> 00:01:08,50
Notice that this gives us exactly the same result

23
00:01:08,50 --> 00:01:10,20
as if we had simply called add

24
00:01:10,20 --> 00:01:15,00
with the arguments 5, 2, and 3.

25
00:01:15,00 --> 00:01:17,10
So in essence, partial application works

26
00:01:17,10 --> 00:01:19,50
by delaying the execution of a function

27
00:01:19,50 --> 00:01:24,30
until we have all the required arguments.

28
00:01:24,30 --> 00:01:26,10
Now what if we wanted to partially apply

29
00:01:26,10 --> 00:01:28,80
two of these arguments?

30
00:01:28,80 --> 00:01:31,10
All we have to do is simply change the number of arguments

31
00:01:31,10 --> 00:01:34,30
that we pass into partiallyApply and then return a function

32
00:01:34,30 --> 00:01:36,90
with only one argument representing the last argument

33
00:01:36,90 --> 00:01:39,50
that we haven't partially applied.

34
00:01:39,50 --> 00:01:40,70
Now when we create a new function

35
00:01:40,70 --> 00:01:47,50
through partially application, we pass in two arguments.

36
00:01:47,50 --> 00:01:51,60
This new function can then be called like this.

37
00:01:51,60 --> 00:01:53,80
Again note that this function gives us the exact same result

38
00:01:53,80 --> 00:01:56,50
as add simply called with those three arguments,

39
00:01:56,50 --> 00:01:58,10
the two that we partially applied

40
00:01:58,10 --> 00:02:05,00
and the one that we passed in at the end.

