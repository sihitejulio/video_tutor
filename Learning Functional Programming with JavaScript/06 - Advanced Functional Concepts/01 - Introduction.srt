1
00:00:00,60 --> 00:00:02,20
- [Instructor] So now that we've gone over the basics

2
00:00:02,20 --> 00:00:03,80
of functional programming,

3
00:00:03,80 --> 00:00:05,40
there are two more advanced concepts

4
00:00:05,40 --> 00:00:07,40
that I'd like to go over with you.

5
00:00:07,40 --> 00:00:11,20
These two concepts are partial application and recursion.

6
00:00:11,20 --> 00:00:14,00
Keep in mind since that these are more advanced concepts,

7
00:00:14,00 --> 00:00:15,80
you don't have to worry if they don't make sense

8
00:00:15,80 --> 00:00:17,00
for you right away.

