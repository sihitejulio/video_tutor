1
00:00:00,60 --> 00:00:02,00
- [Narrator] The next advanced topic we're going

2
00:00:02,00 --> 00:00:04,10
to talk about is recursion.

3
00:00:04,10 --> 00:00:06,80
Now a recursive function is basically just a function

4
00:00:06,80 --> 00:00:10,00
that calls itself somewhere within its definition.

5
00:00:10,00 --> 00:00:11,30
While for reasons of efficiency,

6
00:00:11,30 --> 00:00:14,00
you generally want to avoid using recursive functions,

7
00:00:14,00 --> 00:00:17,30
they can make certain programming problems a lot easier.

8
00:00:17,30 --> 00:00:19,50
This includes problems such as tree traversal,

9
00:00:19,50 --> 00:00:21,20
which using a regular, procedural approach,

10
00:00:21,20 --> 00:00:24,40
can be quite clumsy sometimes.

11
00:00:24,40 --> 00:00:26,60
Let's go over some examples.

12
00:00:26,60 --> 00:00:28,90
Using recursion, it's possible to create structures

13
00:00:28,90 --> 00:00:30,60
that behave like a loop.

14
00:00:30,60 --> 00:00:32,50
The way we do this is simply by calling the function

15
00:00:32,50 --> 00:00:35,00
within itself with different arguments,

16
00:00:35,00 --> 00:00:37,10
and then defining conditions under which the functions

17
00:00:37,10 --> 00:00:40,40
should stop calling itself.

18
00:00:40,40 --> 00:00:42,60
In this case, we want our function to stop calling itself

19
00:00:42,60 --> 00:00:47,00
as soon as I reaches zero.

20
00:00:47,00 --> 00:00:49,30
Let's log the value of I so that we can see what happens

21
00:00:49,30 --> 00:00:53,20
when we call this function.

22
00:00:53,20 --> 00:00:57,90
Finally, lets call loop with a value of ten.

23
00:00:57,90 --> 00:01:02,20
Now let's run our code.

24
00:01:02,20 --> 00:01:04,20
As we can see, our loop function behaves a bit

25
00:01:04,20 --> 00:01:05,90
like a countdown.

26
00:01:05,90 --> 00:01:08,00
Except instead of using a four loop,

27
00:01:08,00 --> 00:01:10,30
it simply calls itself for the lower value each time,

28
00:01:10,30 --> 00:01:12,70
until it reaches zero.

29
00:01:12,70 --> 00:01:15,90
Let's change this function so that it counts up instead.

30
00:01:15,90 --> 00:01:17,60
The way we do this is by having the function

31
00:01:17,60 --> 00:01:19,50
call itself with a higher value each time,

32
00:01:19,50 --> 00:01:23,30
instead of a lower value.

33
00:01:23,30 --> 00:01:28,90
And then we redefine the value that we want it to stop at.

34
00:01:28,90 --> 00:01:32,40
Let's call our function with a value of zero this time.

35
00:01:32,40 --> 00:01:34,80
Once again, let's run our code.

36
00:01:34,80 --> 00:01:38,00
Now we see that our value counts up instead of down.

37
00:01:38,00 --> 00:01:39,80
This is because each time it calls itself with a

38
00:01:39,80 --> 00:01:43,00
higher value, instead of a lower value as before.

