1
00:00:00,40 --> 00:00:02,30
- [Instructor] The simplest example of using a callback

2
00:00:02,30 --> 00:00:06,40
in JavaScript is the built in function setTimeout.

3
00:00:06,40 --> 00:00:07,50
The purpose of this function

4
00:00:07,50 --> 00:00:10,10
is simply to delay the execution of another function

5
00:00:10,10 --> 00:00:12,70
for a certain number of milliseconds.

6
00:00:12,70 --> 00:00:14,70
In practice this function is used mainly for

7
00:00:14,70 --> 00:00:16,30
user interface tweaks,

8
00:00:16,30 --> 00:00:19,20
such as having pop-ups appear after several seconds

9
00:00:19,20 --> 00:00:22,10
or cycling through slideshows.

10
00:00:22,10 --> 00:00:23,60
Let's start with a simple example

11
00:00:23,60 --> 00:00:27,40
to show you how this function works.

12
00:00:27,40 --> 00:00:30,00
All we have to do is pass setTimeout a function

13
00:00:30,00 --> 00:00:31,80
and a duration.

14
00:00:31,80 --> 00:00:35,90
Remember that this duration is expressed in milliseconds.

15
00:00:35,90 --> 00:00:38,50
Inside this function we can do whatever we want.

16
00:00:38,50 --> 00:00:44,40
For now, we'll just print to the console.

17
00:00:44,40 --> 00:00:46,20
And for sake of demonstration,

18
00:00:46,20 --> 00:00:48,10
we're going to log something to the console

19
00:00:48,10 --> 00:00:53,60
before we call setTimeout.

20
00:00:53,60 --> 00:00:58,50
Let's run our code to see what happens.

21
00:00:58,50 --> 00:00:59,70
Now as we can see,

22
00:00:59,70 --> 00:01:01,90
the function that we passed to setTimeout

23
00:01:01,90 --> 00:01:04,90
isn't executed until after 3,000 milliseconds,

24
00:01:04,90 --> 00:01:07,20
or three seconds.

25
00:01:07,20 --> 00:01:13,40
Now what happens if we execute a statement After setTimeout?

26
00:01:13,40 --> 00:01:15,80
Now you might expect this line After setTimeout

27
00:01:15,80 --> 00:01:18,50
to be printed to the console after the line

28
00:01:18,50 --> 00:01:21,50
"The function has been executed".

29
00:01:21,50 --> 00:01:23,30
But when we run the code again, we see

30
00:01:23,30 --> 00:01:27,40
that both the lines before setTimeout and After setTimeout

31
00:01:27,40 --> 00:01:30,30
have been executed before the function that we passed

32
00:01:30,30 --> 00:01:32,70
to setTimeout.

33
00:01:32,70 --> 00:01:35,50
That's because setTimeout is an asynchronous function,

34
00:01:35,50 --> 00:01:39,10
meaning that the execution of the program continues

35
00:01:39,10 --> 00:01:41,60
while it's waiting.

36
00:01:41,60 --> 00:01:42,60
Let's go through another example

37
00:01:42,60 --> 00:01:44,00
so that you can get a better idea

38
00:01:44,00 --> 00:01:47,90
of how asynchronous functions work.

39
00:01:47,90 --> 00:01:50,20
Let's say that we have a variable called X,

40
00:01:50,20 --> 00:01:55,80
and we set it equal to 1, initially.

41
00:01:55,80 --> 00:02:01,70
Let's log the original value of X to the console.

42
00:02:01,70 --> 00:02:03,30
Now what if we call setTimeout

43
00:02:03,30 --> 00:02:05,10
with a function that changes the value of X

44
00:02:05,10 --> 00:02:09,50
after three seconds?

45
00:02:09,50 --> 00:02:17,50
Let's log the value of X here, as well.

46
00:02:17,50 --> 00:02:19,40
Now what will happen when we log the value of X

47
00:02:19,40 --> 00:02:24,80
underneath the setTimeout function?

48
00:02:24,80 --> 00:02:26,00
Running our program, we see that

49
00:02:26,00 --> 00:02:28,20
outside of our setTimeout function,

50
00:02:28,20 --> 00:02:30,70
the value of X is still 1.

51
00:02:30,70 --> 00:02:32,80
In fact, the value of X doesn't change

52
00:02:32,80 --> 00:02:34,10
until after three seconds,

53
00:02:34,10 --> 00:02:39,00
when the function we passed to setTimeout is called.

54
00:02:39,00 --> 00:02:40,60
This can be one of the challenges of using

55
00:02:40,60 --> 00:02:43,70
asynchronous functions since the value of X changes

56
00:02:43,70 --> 00:02:46,70
depending on time.

57
00:02:46,70 --> 00:02:49,60
Note also that if we have several asynchronous functions,

58
00:02:49,60 --> 00:02:52,50
each of which sets X to a different value,

59
00:02:52,50 --> 00:02:56,30
there's no guarantee what order they'll be executed in.

60
00:02:56,30 --> 00:02:59,10
This is especially true for other asynchronous tasks,

61
00:02:59,10 --> 00:03:06,90
such as reading files or performing network operations.

62
00:03:06,90 --> 00:03:09,60
Keep in mind that if you have several asynchronous tasks

63
00:03:09,60 --> 00:03:12,10
and you need them to happen in a specific order,

64
00:03:12,10 --> 00:03:31,00
you need to nest them like this.

