1
00:00:00,40 --> 00:00:02,70
- [Narrator] With our simple setTimeout example,

2
00:00:02,70 --> 00:00:05,80
the function we passed was a function with no arguments.

3
00:00:05,80 --> 00:00:09,00
This is actually very rarely the case.

4
00:00:09,00 --> 00:00:11,50
Most asynchronous functions that we perform

5
00:00:11,50 --> 00:00:13,30
pass the results of the operation

6
00:00:13,30 --> 00:00:15,30
to our function as an argument.

7
00:00:15,30 --> 00:00:17,80
For example, it might pass the results that we obtain

8
00:00:17,80 --> 00:00:21,10
when reading from a database or reading a file.

9
00:00:21,10 --> 00:00:23,50
In Node.js especially, the standard form is

10
00:00:23,50 --> 00:00:25,80
to pass the result of the asynchronous function

11
00:00:25,80 --> 00:00:27,90
as the second argument, and any errors

12
00:00:27,90 --> 00:00:31,80
the function encounters as the first argument.

13
00:00:31,80 --> 00:00:33,60
As an example, let's take a look

14
00:00:33,60 --> 00:00:37,40
at how we read a file asynchronously in Node.js.

15
00:00:37,40 --> 00:00:39,90
Fs is a standard library included with Node.js

16
00:00:39,90 --> 00:00:42,60
that allows us to work with the file system.

17
00:00:42,60 --> 00:00:48,20
Note that we don't have to use npm to install it.

18
00:00:48,20 --> 00:00:51,30
Now, when we read a file using fs.readFile,

19
00:00:51,30 --> 00:00:55,20
the results of opening the file are passed to our function.

20
00:00:55,20 --> 00:00:56,90
The first argument will contain any errors

21
00:00:56,90 --> 00:00:58,50
that occurred during the operation

22
00:00:58,50 --> 00:01:00,70
such as if the file didn't exist,

23
00:01:00,70 --> 00:01:04,10
and the second one will contain the data from the file.

24
00:01:04,10 --> 00:01:05,80
If we run our program, we can see

25
00:01:05,80 --> 00:01:08,50
that the contents of the file are printed to the console

26
00:01:08,50 --> 00:01:11,10
after the operation is completed.

27
00:01:11,10 --> 00:01:14,20
Now, as I mentioned before, the concept of callbacks

28
00:01:14,20 --> 00:01:17,00
can be a bit tricky at first for some programmers.

29
00:01:17,00 --> 00:01:32,30
It can be very tempting to write something like this.

30
00:01:32,30 --> 00:01:34,70
However, when we run the program,

31
00:01:34,70 --> 00:01:37,60
we see that it logs undefined.

32
00:01:37,60 --> 00:01:38,70
The reason for this is that

33
00:01:38,70 --> 00:01:42,00
the fs.readFile function takes some time to complete,

34
00:01:42,00 --> 00:01:44,60
and since the call is asynchronous, the program

35
00:01:44,60 --> 00:01:48,30
continues on before fs.readFile completes.

36
00:01:48,30 --> 00:01:51,00
This means that when our last line is called,

37
00:01:51,00 --> 00:01:55,00
our variable fileContents has not yet been assigned a value.

38
00:01:55,00 --> 00:01:56,50
For this reason, it's important to remember that

39
00:01:56,50 --> 00:01:58,80
if any line of code uses a variable

40
00:01:58,80 --> 00:02:01,00
that's affected by an asynchronous call,

41
00:02:01,00 --> 00:02:03,50
as our variable fileContents is here,

42
00:02:03,50 --> 00:02:08,80
that line must be placed inside the callback as well.

43
00:02:08,80 --> 00:02:12,00
Now if we run our code again, we can see that it worked.

