1
00:00:00,50 --> 00:00:02,80
- [Narrator] The ability to pass functions in JavaScript

2
00:00:02,80 --> 00:00:04,50
has some very useful implications

3
00:00:04,50 --> 00:00:06,70
for performing asynchronous tasks,

4
00:00:06,70 --> 00:00:08,50
such as reading or writing files,

5
00:00:08,50 --> 00:00:09,60
working with databases,

6
00:00:09,60 --> 00:00:11,80
or making network requests.

7
00:00:11,80 --> 00:00:13,80
The normal synchronous way of reading files, for example,

8
00:00:13,80 --> 00:00:16,50
requires that the file operation be fully completed

9
00:00:16,50 --> 00:00:19,20
before any other code is executed.

10
00:00:19,20 --> 00:00:20,10
So in this example,

11
00:00:20,10 --> 00:00:21,40
the second line won't be called

12
00:00:21,40 --> 00:00:23,40
until the first line completes.

13
00:00:23,40 --> 00:00:25,50
In many cases this can take several seconds

14
00:00:25,50 --> 00:00:27,50
or even several minutes.

15
00:00:27,50 --> 00:00:28,60
This can of course

16
00:00:28,60 --> 00:00:31,20
have detrimental effects on user experience,

17
00:00:31,20 --> 00:00:32,60
since it holds up the entire program

18
00:00:32,60 --> 00:00:34,50
until it's finished.

19
00:00:34,50 --> 00:00:36,60
The answer is to use asynchronous tasks,

20
00:00:36,60 --> 00:00:39,50
tasks that don't hold up the rest of the program.

21
00:00:39,50 --> 00:00:42,10
Lucky for us JavaScript provides a very convenient way

22
00:00:42,10 --> 00:00:44,00
of handling asynchronous tasks.

23
00:00:44,00 --> 00:00:46,20
This is through the use of callbacks.

24
00:00:46,20 --> 00:00:48,60
A callback is simply a function that you pass

25
00:00:48,60 --> 00:00:51,00
as an argument to an asynchronous function.

26
00:00:51,00 --> 00:00:53,40
When the asynchronous function finishes running,

27
00:00:53,40 --> 00:00:56,40
it simply calls the function we passed it.

28
00:00:56,40 --> 00:00:59,00
Often times the asynchronous function passes its result

29
00:00:59,00 --> 00:01:01,60
as an argument to the function we give it.

30
00:01:01,60 --> 00:01:02,50
We're going to look at

31
00:01:02,50 --> 00:01:04,00
a few simple examples of this behavior.

