1
00:00:00,50 --> 00:00:02,20
- [Instructor] This course was a good introduction

2
00:00:02,20 --> 00:00:05,30
to how to do SSR with React and leveraging library

3
00:00:05,30 --> 00:00:09,30
such as Next JS, Express and Redux.

4
00:00:09,30 --> 00:00:11,10
If the approach interests you,

5
00:00:11,10 --> 00:00:13,70
you can take a look at React's all SSR techniques

6
00:00:13,70 --> 00:00:16,40
leveraging React DOM Server API's.

7
00:00:16,40 --> 00:00:19,90
But I decided to focus on Next JS for its simplicity.

8
00:00:19,90 --> 00:00:22,30
Feel free to explore these other libraries.

9
00:00:22,30 --> 00:00:25,30
Also, the number of Next JS scenarios for the

10
00:00:25,30 --> 00:00:28,60
example code is quite extensive on their GitHub repo.

11
00:00:28,60 --> 00:00:31,20
So I would strongly suggest you take a look at the list.

12
00:00:31,20 --> 00:00:33,30
You might find your specific need for SSR

13
00:00:33,30 --> 00:00:36,40
and be able to get a quick sample project going.

14
00:00:36,40 --> 00:00:41,10
Next JS with type script, MobX, various styling libraries,

15
00:00:41,10 --> 00:00:42,80
and just for testing are just a

16
00:00:42,80 --> 00:00:45,20
few examples of what you'll find there.

17
00:00:45,20 --> 00:00:47,00
If you ever run into any issues,

18
00:00:47,00 --> 00:00:50,50
you can use Stack Overflow, the GitHub issues,

19
00:00:50,50 --> 00:00:53,50
and finally take a look at their docs for further help.

20
00:00:53,50 --> 00:00:56,10
Next JS community, scroll in everyday and

21
00:00:56,10 --> 00:00:59,10
you'll quickly find an answer to your questions.

22
00:00:59,10 --> 00:01:01,10
Thanks very much for taking my course.

23
00:01:01,10 --> 00:01:03,00
I'll see you in a bit.

