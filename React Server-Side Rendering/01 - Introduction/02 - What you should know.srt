1
00:00:00,60 --> 00:00:01,60
- [Instructor] This course is focused

2
00:00:01,60 --> 00:00:04,50
on building a server side application with React.

3
00:00:04,50 --> 00:00:06,90
So some experience with React is a must.

4
00:00:06,90 --> 00:00:09,60
If you aren't familiar with creating React components,

5
00:00:09,60 --> 00:00:12,00
using props and importing modules,

6
00:00:12,00 --> 00:00:14,30
I'd recommend that you take a basic React course

7
00:00:14,30 --> 00:00:15,60
from our library.

8
00:00:15,60 --> 00:00:17,60
Also, experience with JavaScript,

9
00:00:17,60 --> 00:00:21,40
especially the ES6 syntax is a must for this course

10
00:00:21,40 --> 00:00:24,50
as this is the basis of a React programmer.

11
00:00:24,50 --> 00:00:27,80
In this course I will be using the ES2015 version

12
00:00:27,80 --> 00:00:29,50
of the syntax.

13
00:00:29,50 --> 00:00:32,10
Additionally, being familiar with terminal commands

14
00:00:32,10 --> 00:00:35,40
and NPM isn't a must but it will definitely help you

15
00:00:35,40 --> 00:00:37,30
when we use these tools.

16
00:00:37,30 --> 00:00:39,50
In this course I'll be using a Mac

17
00:00:39,50 --> 00:00:41,50
but you can follow along on a PC,

18
00:00:41,50 --> 00:00:45,00
as the tools work exactly the same on both platforms.

19
00:00:45,00 --> 00:00:47,80
I'll also be using VS Code as my code editor.

20
00:00:47,80 --> 00:00:50,30
It doesn't cost anything so I recommend it

21
00:00:50,30 --> 00:00:53,00
but feel free to use whichever editor you prefer,

22
00:00:53,00 --> 00:00:55,60
although I recommend using an editor

23
00:00:55,60 --> 00:00:57,00
with a built in terminal.

