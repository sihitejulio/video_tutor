1
00:00:00,50 --> 00:00:01,50
- [Instructor] You can follow along

2
00:00:01,50 --> 00:00:04,50
with this course using the provided exercise files.

3
00:00:04,50 --> 00:00:06,10
Just download the ZIP file, and extract

4
00:00:06,10 --> 00:00:08,30
the folder to a place of your choice.

5
00:00:08,30 --> 00:00:11,80
I extracted my copy here on the desktop for easy access.

6
00:00:11,80 --> 00:00:13,00
You will see that the exercise files

7
00:00:13,00 --> 00:00:15,40
are organized by chapter and lesson

8
00:00:15,40 --> 00:00:18,10
to make it easy for you to follow along with the videos.

9
00:00:18,10 --> 00:00:21,50
Keep in mind that not every chapter has exercise files.

10
00:00:21,50 --> 00:00:23,40
Each folder has a copy of our project

11
00:00:23,40 --> 00:00:25,40
as it looks at both the beginning

12
00:00:25,40 --> 00:00:27,50
and the end of each video.

13
00:00:27,50 --> 00:00:28,70
If you want to jump ahead and see how

14
00:00:28,70 --> 00:00:31,00
the code will look at the end of our video,

15
00:00:31,00 --> 00:00:34,90
you can always peek inside the End folder for that video.

16
00:00:34,90 --> 00:00:37,30
So, if you wanted to actually load these files

17
00:00:37,30 --> 00:00:43,30
inside of VS Code, click on Explore, Open Folder,

18
00:00:43,30 --> 00:00:45,10
then get to the actual folder questions.

19
00:00:45,10 --> 00:00:47,20
So, let's go to the Exercise Files,

20
00:00:47,20 --> 00:00:53,40
Chapter Two, 02_02, and then simply open those files.

21
00:00:53,40 --> 00:00:56,60
And then at that point, it's a simple npm install,

22
00:00:56,60 --> 00:01:00,10
so you can do View, Integrated Terminal,

23
00:01:00,10 --> 00:01:04,90
and then do npm install to install all the dependencies.

24
00:01:04,90 --> 00:01:06,90
And then once you have the dependencies installed,

25
00:01:06,90 --> 00:01:10,10
you can simply do an npm on that, like so.

26
00:01:10,10 --> 00:01:11,80
Please keep in mind that sometimes

27
00:01:11,80 --> 00:01:13,90
the code is a work in progress,

28
00:01:13,90 --> 00:01:16,90
and if you run npm run dev, the application

29
00:01:16,90 --> 00:01:19,50
won't run and may return an error.

30
00:01:19,50 --> 00:01:20,40
This is expected and won't affect

31
00:01:20,40 --> 00:01:23,00
your ability to follow along.

