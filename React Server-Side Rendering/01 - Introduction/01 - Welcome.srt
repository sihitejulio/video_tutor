1
00:00:00,40 --> 00:00:02,60
- Hi, I'm Manny Henri and I've been

2
00:00:02,60 --> 00:00:04,50
working with React since it's release.

3
00:00:04,50 --> 00:00:08,40
And often use SSR in my own applications.

4
00:00:08,40 --> 00:00:12,00
Server-Side Rendering, universal or ISO morphic applications

5
00:00:12,00 --> 00:00:14,60
can be an intimidating subject for some.

6
00:00:14,60 --> 00:00:17,20
And even understanding what these terms are can

7
00:00:17,20 --> 00:00:20,00
be confusing if you've never explored the subject.

8
00:00:20,00 --> 00:00:23,50
I'm here to clarify and demystify the subject for you.

9
00:00:23,50 --> 00:00:29,00
While exploring using SSR with Next JS, ReadX and Express.

10
00:00:29,00 --> 00:00:31,60
First, we'll explore the basics of SSR.

11
00:00:31,60 --> 00:00:35,00
What it is and tools we need and install them.

12
00:00:35,00 --> 00:00:37,40
Then, we'll focus our attention on using

13
00:00:37,40 --> 00:00:40,20
a template project and build our first application

14
00:00:40,20 --> 00:00:44,30
with SSR React and leveraging Next JS.

15
00:00:44,30 --> 00:00:46,90
Also, we'll take a look at Express and

16
00:00:46,90 --> 00:00:49,80
how we can use it with SSR approach.

17
00:00:49,80 --> 00:00:52,40
Finally, we'll take a look at code splitting and

18
00:00:52,40 --> 00:00:55,30
how it impacts our applications' performance.

19
00:00:55,30 --> 00:00:58,10
If you're ready to learn all about SSR with React,

20
00:00:58,10 --> 00:01:02,00
fire up your favorite editor and let's get started.

