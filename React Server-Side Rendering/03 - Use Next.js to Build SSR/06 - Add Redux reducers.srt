1
00:00:00,50 --> 00:00:02,00
- [Instructor] Next up to complete Redux

2
00:00:02,00 --> 00:00:03,80
is to add the reducers.

3
00:00:03,80 --> 00:00:06,20
Basically the function that will take the action dispatch

4
00:00:06,20 --> 00:00:08,60
by the application and then update the store

5
00:00:08,60 --> 00:00:09,70
with the new information

6
00:00:09,70 --> 00:00:11,30
and therefore the application will be able

7
00:00:11,30 --> 00:00:12,70
to update its views

8
00:00:12,70 --> 00:00:14,30
with the updated data.

9
00:00:14,30 --> 00:00:18,40
If that didn't make any sense, it will in a few seconds.

10
00:00:18,40 --> 00:00:20,90
Okay, so let's go ahead and code this,

11
00:00:20,90 --> 00:00:25,00
so let's create inside of our store.js file

12
00:00:25,00 --> 00:00:27,10
a section called reducers,

13
00:00:27,10 --> 00:00:31,70
so let me put it just after line 24

14
00:00:31,70 --> 00:00:34,50
and just before creating our store.

15
00:00:34,50 --> 00:00:37,60
So, we'll call this section reducers

16
00:00:37,60 --> 00:00:39,40
and let's go ahead and create it.

17
00:00:39,40 --> 00:00:45,90
So, let's create an export of a variable called reducer

18
00:00:45,90 --> 00:00:50,80
and again, make sure that if you did reducers plural here,

19
00:00:50,80 --> 00:00:53,80
then you make sure that you also do plural there

20
00:00:53,80 --> 00:00:56,90
'cause this is basically grabbing this variable here

21
00:00:56,90 --> 00:00:59,50
and loading it inside of your store,

22
00:00:59,50 --> 00:01:02,80
so let's go and continue creating this

23
00:01:02,80 --> 00:01:05,30
and we're passing the state

24
00:01:05,30 --> 00:01:08,60
as the initial state

25
00:01:08,60 --> 00:01:11,20
and then we're passing the actions

26
00:01:11,20 --> 00:01:13,50
to our reducer

27
00:01:13,50 --> 00:01:16,10
because basically it will take the state

28
00:01:16,10 --> 00:01:17,80
and then take the actions

29
00:01:17,80 --> 00:01:19,80
and then do something with it

30
00:01:19,80 --> 00:01:23,10
and this is why we're doing a switch statement

31
00:01:23,10 --> 00:01:25,00
because we're going to have the possibility

32
00:01:25,00 --> 00:01:29,00
of having multiple actions type passed to it.

33
00:01:29,00 --> 00:01:34,20
So, for example, we'll do an action.type

34
00:01:34,20 --> 00:01:36,50
and then inside of a switch statement

35
00:01:36,50 --> 00:01:39,20
we'll do a case, so basically if the case,

36
00:01:39,20 --> 00:01:42,20
so if the type of this specific action

37
00:01:42,20 --> 00:01:46,50
is the INITIALCARDS,

38
00:01:46,50 --> 00:01:51,30
then what we'll do is return the cards

39
00:01:51,30 --> 00:01:54,60
with the action.cards

40
00:01:54,60 --> 00:01:57,60
and that's as simple as it gets.

41
00:01:57,60 --> 00:02:00,80
So, basically if the type of the action

42
00:02:00,80 --> 00:02:03,40
is INITIALCARDS and let me make sure

43
00:02:03,40 --> 00:02:06,60
I've done the exact same, yeah, so we're good,

44
00:02:06,60 --> 00:02:09,60
so if we're passing this specific action type

45
00:02:09,60 --> 00:02:11,30
in this reducer,

46
00:02:11,30 --> 00:02:13,30
then pass the cards,

47
00:02:13,30 --> 00:02:16,20
so the action cards, so this object

48
00:02:16,20 --> 00:02:18,00
or the value of this object

49
00:02:18,00 --> 00:02:22,00
to the cards and then which will become part of our state

50
00:02:22,00 --> 00:02:24,90
and therefore that's how we're going to update our state

51
00:02:24,90 --> 00:02:27,50
with the cards or the data

52
00:02:27,50 --> 00:02:31,30
that we got from the data.json file.

53
00:02:31,30 --> 00:02:34,40
Okay, so we have a second case.

54
00:02:34,40 --> 00:02:38,80
So, if we hit return after line 32,

55
00:02:38,80 --> 00:02:40,40
we can have a second case.

56
00:02:40,40 --> 00:02:42,50
So, we have a second action type,

57
00:02:42,50 --> 00:02:43,90
so we have a second action

58
00:02:43,90 --> 00:02:47,00
that could be dispatched by the application

59
00:02:47,00 --> 00:02:48,80
which is type add

60
00:02:48,80 --> 00:02:54,50
so if we have a case where the type is ADD,

61
00:02:54,50 --> 00:02:56,20
so I'm going on a limb

62
00:02:56,20 --> 00:02:58,50
and I'm going to basically code

63
00:02:58,50 --> 00:02:59,50
what this case would be

64
00:02:59,50 --> 00:03:02,40
if we had anything to add to our store.

65
00:03:02,40 --> 00:03:05,80
So, in this case we would basically extend our state

66
00:03:05,80 --> 00:03:07,00
which we've passed here,

67
00:03:07,00 --> 00:03:09,10
so grab our state as it is

68
00:03:09,10 --> 00:03:13,00
and then update cards

69
00:03:13,00 --> 00:03:18,70
with what we just have in our state.cards

70
00:03:18,70 --> 00:03:21,70
plus the new item that we got.

71
00:03:21,70 --> 00:03:23,30
So, basically we passing an item

72
00:03:23,30 --> 00:03:24,90
from our action here

73
00:03:24,90 --> 00:03:25,90
and it will be passed

74
00:03:25,90 --> 00:03:28,40
and then we'll basically grab the state,

75
00:03:28,40 --> 00:03:30,80
extend it, and then from the state

76
00:03:30,80 --> 00:03:33,70
we'll create a new card section

77
00:03:33,70 --> 00:03:36,40
and therefore we'll create a new object

78
00:03:36,40 --> 00:03:39,00
with the new item inside of it.

79
00:03:39,00 --> 00:03:41,40
So, one of the things that you'll hear a lot

80
00:03:41,40 --> 00:03:45,50
in Redux is basically we don't mutate the state

81
00:03:45,50 --> 00:03:48,50
which basically means that we are not changing the state.

82
00:03:48,50 --> 00:03:51,00
We are creating a new object

83
00:03:51,00 --> 00:03:53,60
of the state, adding the new items to it

84
00:03:53,60 --> 00:03:55,50
and then continuing down the road

85
00:03:55,50 --> 00:03:57,30
with this approach.

86
00:03:57,30 --> 00:03:59,60
You can traverse and then go back

87
00:03:59,60 --> 00:04:01,50
in history of the state

88
00:04:01,50 --> 00:04:04,90
and we'll see this once we use the Redux DevTools.

89
00:04:04,90 --> 00:04:07,90
Okay, and then you always have a default

90
00:04:07,90 --> 00:04:10,40
when you do a switch statement,

91
00:04:10,40 --> 00:04:12,90
so you have a default

92
00:04:12,90 --> 00:04:17,50
which will return the state as it is.

93
00:04:17,50 --> 00:04:19,90
So, that's what a reducer looks like

94
00:04:19,90 --> 00:04:23,50
and basically if we recap very quickly,

95
00:04:23,50 --> 00:04:25,50
we have actions that will be dispatched

96
00:04:25,50 --> 00:04:27,20
from the application.

97
00:04:27,20 --> 00:04:29,70
Once we have an action dispatched,

98
00:04:29,70 --> 00:04:32,30
it will basically run this type

99
00:04:32,30 --> 00:04:33,70
and have this data

100
00:04:33,70 --> 00:04:36,10
and then pass it to our reducer

101
00:04:36,10 --> 00:04:37,40
which will basically, okay,

102
00:04:37,40 --> 00:04:39,30
so what is the type of action

103
00:04:39,30 --> 00:04:40,50
you're passing to me?

104
00:04:40,50 --> 00:04:42,90
Okay, so I'm passing the initial cards,

105
00:04:42,90 --> 00:04:44,80
all right, so then we need to return

106
00:04:44,80 --> 00:04:48,30
the cards with whatever you have in your cards

107
00:04:48,30 --> 00:04:51,50
in your action in your cards object

108
00:04:51,50 --> 00:04:53,50
and you're returning the state this way

109
00:04:53,50 --> 00:04:56,90
and that's the state of our state.

110
00:04:56,90 --> 00:04:59,50
If you have an action that's dispatched,

111
00:04:59,50 --> 00:05:01,80
that's called of type add,

112
00:05:01,80 --> 00:05:03,90
then we'll run to this case here,

113
00:05:03,90 --> 00:05:05,80
then return our state,

114
00:05:05,80 --> 00:05:08,70
with our state we'll add the new action item

115
00:05:08,70 --> 00:05:09,70
and then return it

116
00:05:09,70 --> 00:05:12,10
and then we have a new copy of the state

117
00:05:12,10 --> 00:05:15,30
with this new item inside of our cards object.

118
00:05:15,30 --> 00:05:17,70
And that's pretty much how it works.

119
00:05:17,70 --> 00:05:19,90
So, now we have our Redux implementation completed.

120
00:05:19,90 --> 00:05:22,10
Now we just need to call the action

121
00:05:22,10 --> 00:05:23,50
when the application first loads

122
00:05:23,50 --> 00:05:26,00
and implement the views to work with Redux.

123
00:05:26,00 --> 00:05:28,00
Let's do that next.

