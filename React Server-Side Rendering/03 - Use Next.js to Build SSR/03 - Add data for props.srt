1
00:00:00,60 --> 00:00:02,50
- [Instructor] So far, we've used stateless components

2
00:00:02,50 --> 00:00:04,50
or functions, but like most applications,

3
00:00:04,50 --> 00:00:07,30
we need to populate our application with data.

4
00:00:07,30 --> 00:00:09,50
And as with any regular React applications,

5
00:00:09,50 --> 00:00:12,00
we get the state populated with data

6
00:00:12,00 --> 00:00:15,10
and then pass these as props to our components.

7
00:00:15,10 --> 00:00:17,40
Next.js is a bit different in its approach,

8
00:00:17,40 --> 00:00:19,90
but conceptually the same as React.

9
00:00:19,90 --> 00:00:21,10
Let me demonstrate.

10
00:00:21,10 --> 00:00:23,90
So the first thing we need to do is some type of data,

11
00:00:23,90 --> 00:00:27,10
so let's grab this from the exercise files.

12
00:00:27,10 --> 00:00:29,60
In the exercise files and resources,

13
00:00:29,60 --> 00:00:31,60
you have a data.json file.

14
00:00:31,60 --> 00:00:34,60
Click, command-C, copy,

15
00:00:34,60 --> 00:00:37,00
or right-click and copy.

16
00:00:37,00 --> 00:00:42,10
And then, in the SSR folder, create a new folder called

17
00:00:42,10 --> 00:00:46,40
data, and then paste the data.json file.

18
00:00:46,40 --> 00:00:49,40
All right, let's go back to VS Code.

19
00:00:49,40 --> 00:00:51,00
So now, we'll do a little bit of code.

20
00:00:51,00 --> 00:00:54,00
So bring up your index.js file

21
00:00:54,00 --> 00:00:56,00
and we'll change a few things here.

22
00:00:56,00 --> 00:00:59,50
So what we need to do first is to make sure that this is now

23
00:00:59,50 --> 00:01:02,40
a class-based or a state-full component,

24
00:01:02,40 --> 00:01:04,80
so we can use some lifecycle components,

25
00:01:04,80 --> 00:01:06,20
pretty much like React.

26
00:01:06,20 --> 00:01:08,20
So let's go ahead and do that.

27
00:01:08,20 --> 00:01:11,60
Let's call it a class, export default class.

28
00:01:11,60 --> 00:01:13,00
Index

29
00:01:13,00 --> 00:01:14,90
extends

30
00:01:14,90 --> 00:01:19,00
React.Component.

31
00:01:19,00 --> 00:01:21,70
Now, let's remove all this.

32
00:01:21,70 --> 00:01:26,80
Let's open up our curly braces and just open

33
00:01:26,80 --> 00:01:28,60
and then close it here.

34
00:01:28,60 --> 00:01:32,20
So remove the parentheses and then close it here.

35
00:01:32,20 --> 00:01:34,80
So you'll have some errors right now, that's fine.

36
00:01:34,80 --> 00:01:36,10
It's normal.

37
00:01:36,10 --> 00:01:40,40
Now, what we need to do to use React is to import React.

38
00:01:40,40 --> 00:01:43,30
So we can't go without it.

39
00:01:43,30 --> 00:01:46,50
So React from react.

40
00:01:46,50 --> 00:01:49,40
And again, if you did only a stateless component,

41
00:01:49,40 --> 00:01:51,00
you wouldn't have to import React.

42
00:01:51,00 --> 00:01:53,20
You could just do an export default

43
00:01:53,20 --> 00:01:56,40
and then return the actual index here

44
00:01:56,40 --> 00:01:58,10
and next.js would know enough

45
00:01:58,10 --> 00:02:01,80
that this is the index.js route that we need to create

46
00:02:01,80 --> 00:02:05,50
and you don't need to create the naming for it.

47
00:02:05,50 --> 00:02:08,30
But in this case, if we're doing a state-full component,

48
00:02:08,30 --> 00:02:10,80
we actually need to import React

49
00:02:10,80 --> 00:02:13,70
and then be explicit about the actual class

50
00:02:13,70 --> 00:02:15,60
that we're creating.

51
00:02:15,60 --> 00:02:18,40
Okay, the next thing we need to do is actually

52
00:02:18,40 --> 00:02:20,70
import the data

53
00:02:20,70 --> 00:02:23,90
from our data folder.

54
00:02:23,90 --> 00:02:26,10
So I'm getting out of this folder pages

55
00:02:26,10 --> 00:02:28,20
then going back in data.

56
00:02:28,20 --> 00:02:31,80
Then /data.json.

57
00:02:31,80 --> 00:02:36,90
Okay, so return on line six at the end, like so.

58
00:02:36,90 --> 00:02:42,30
And we'll need first to create a static async

59
00:02:42,30 --> 00:02:46,20
and get the InitialProps

60
00:02:46,20 --> 00:02:48,90
and this is how you would initialize your state

61
00:02:48,90 --> 00:02:51,50
using next.js.

62
00:02:51,50 --> 00:02:54,50
And basically, return,

63
00:02:54,50 --> 00:02:59,10
in our case, we're going to create something called cards

64
00:02:59,10 --> 00:03:00,60
with the data that we have.

65
00:03:00,60 --> 00:03:02,50
So basically, all the data that we're going to get

66
00:03:02,50 --> 00:03:05,10
from the data.json.

67
00:03:05,10 --> 00:03:07,80
So all these here.

68
00:03:07,80 --> 00:03:09,60
And this is data from the same course

69
00:03:09,60 --> 00:03:12,50
that I just referred to earlier.

70
00:03:12,50 --> 00:03:17,30
And then we're going to pass it to a cards prop

71
00:03:17,30 --> 00:03:20,50
that we'll create in the state like so.

72
00:03:20,50 --> 00:03:21,80
So this is good.

73
00:03:21,80 --> 00:03:24,40
Now, the only thing that we need to do at this point is

74
00:03:24,40 --> 00:03:27,70
to basically finish the rest of our class here,

75
00:03:27,70 --> 00:03:30,90
'cause this is not regular React.

76
00:03:30,90 --> 00:03:34,70
So whenever we create a class or a state-full component,

77
00:03:34,70 --> 00:03:38,70
we need to use the render and return

78
00:03:38,70 --> 00:03:42,10
like usual with React.

79
00:03:42,10 --> 00:03:47,10
Then open up the curly braces and then return

80
00:03:47,10 --> 00:03:48,30
like so.

81
00:03:48,30 --> 00:03:50,90
And then inside of it, we'll put that code, so.

82
00:03:50,90 --> 00:03:54,50
Highlight from 16 to 27,

83
00:03:54,50 --> 00:03:56,70
Command-X or cut,

84
00:03:56,70 --> 00:03:58,20
right-click, cut.

85
00:03:58,20 --> 00:04:01,20
And then paste it inside of your return statement

86
00:04:01,20 --> 00:04:02,40
here like so.

87
00:04:02,40 --> 00:04:06,80
And let's just indent this properly.

88
00:04:06,80 --> 00:04:09,50
Okay, so everything is good here.

89
00:04:09,50 --> 00:04:11,10
So we have data.

90
00:04:11,10 --> 00:04:14,90
Now, instead of doing the cards manually like this,

91
00:04:14,90 --> 00:04:19,00
I want to basically do a map method from JavaScript,

92
00:04:19,00 --> 00:04:22,70
so from ES6 JavaScript and I want to make sure to return

93
00:04:22,70 --> 00:04:25,40
as many cards as I have items in my array

94
00:04:25,40 --> 00:04:27,10
from the data here.

95
00:04:27,10 --> 00:04:30,10
So let's delete that.

96
00:04:30,10 --> 00:04:32,60
And let's do some JavaScript here.

97
00:04:32,60 --> 00:04:35,20
So when you want to do JavaScript in next.js,

98
00:04:35,20 --> 00:04:39,40
it's pretty much the same as when you do it inside of React.

99
00:04:39,40 --> 00:04:42,00
So we're going to it, this.props.

100
00:04:42,00 --> 00:04:45,80
And because we're actually returning these cards here

101
00:04:45,80 --> 00:04:48,40
and we're initializing the state,

102
00:04:48,40 --> 00:04:50,70
this is becoming a prop for this.

103
00:04:50,70 --> 00:04:53,50
I know this is a little bit different than the usual way

104
00:04:53,50 --> 00:04:57,20
you would do this in React, but all the components

105
00:04:57,20 --> 00:05:01,60
are now passing props as props and not as a state

106
00:05:01,60 --> 00:05:04,60
if you have declared a state in the same file.

107
00:05:04,60 --> 00:05:07,90
So just to note to self, so this is how it works.

108
00:05:07,90 --> 00:05:10,80
And while I'm on it, I actually just realized that there's

109
00:05:10,80 --> 00:05:12,90
a little typo error here.

110
00:05:12,90 --> 00:05:16,80
So it's async and not asyn.

111
00:05:16,80 --> 00:05:18,70
So please make sure that you correct that

112
00:05:18,70 --> 00:05:22,30
if you basically followed word-by-word what I just typed.

113
00:05:22,30 --> 00:05:24,10
All right, so let's go back here.

114
00:05:24,10 --> 00:05:25,60
So this.props

115
00:05:25,60 --> 00:05:27,30
.cards

116
00:05:27,30 --> 00:05:29,30
.map

117
00:05:29,30 --> 00:05:33,10
is the function, which takes an argument.

118
00:05:33,10 --> 00:05:35,30
And we'll use a card as the argument

119
00:05:35,30 --> 00:05:38,40
and then we are returning

120
00:05:38,40 --> 00:05:41,40
the card component

121
00:05:41,40 --> 00:05:47,00
as many times as there are items inside of our data.

122
00:05:47,00 --> 00:05:49,70
And we need to pass the keys, so that doesn't change.

123
00:05:49,70 --> 00:05:51,50
So if you're familiar with React,

124
00:05:51,50 --> 00:05:55,10
you'll always need to have a unique key pass to it.

125
00:05:55,10 --> 00:05:59,70
So we're going to pass the card, that id

126
00:05:59,70 --> 00:06:00,50
to this,

127
00:06:00,50 --> 00:06:01,80
basically our card

128
00:06:01,80 --> 00:06:04,20
and then it's going to run through the array.

129
00:06:04,20 --> 00:06:06,80
So if you take a look at the files here,

130
00:06:06,80 --> 00:06:09,90
so let's go back to our data here.

131
00:06:09,90 --> 00:06:12,10
We have an id for each one and they are unique,

132
00:06:12,10 --> 00:06:13,20
so we're good there.

133
00:06:13,20 --> 00:06:16,20
It's going to iterate through the array for each object

134
00:06:16,20 --> 00:06:18,80
and create a card every time there is one.

135
00:06:18,80 --> 00:06:21,90
So I believe there's 18 of them, yeah.

136
00:06:21,90 --> 00:06:25,50
So 17 plus one, because this starts at zero.

137
00:06:25,50 --> 00:06:27,00
An array starts at zero.

138
00:06:27,00 --> 00:06:31,70
And this function is going to iterate through every item

139
00:06:31,70 --> 00:06:32,90
in our data.

140
00:06:32,90 --> 00:06:35,40
Okay, so let's save that.

141
00:06:35,40 --> 00:06:36,50
No errors here.

142
00:06:36,50 --> 00:06:38,30
So let's go back to the browser.

143
00:06:38,30 --> 00:06:41,70
All right, so now we have our application

144
00:06:41,70 --> 00:06:44,80
and if you're seeing an error because I had a typo earlier,

145
00:06:44,80 --> 00:06:45,80
just do a refresh.

146
00:06:45,80 --> 00:06:48,00
It will actually refresh the application

147
00:06:48,00 --> 00:06:49,20
and everything's going to be good.

148
00:06:49,20 --> 00:06:52,80
All right, so you can see there's a cards array of 18

149
00:06:52,80 --> 00:06:55,20
that has been passed to that particular application,

150
00:06:55,20 --> 00:06:57,80
so it looks like everything is good.

151
00:06:57,80 --> 00:07:01,80
So let's go ahead and drop down until we see the cards

152
00:07:01,80 --> 00:07:04,00
on our here.

153
00:07:04,00 --> 00:07:07,50
So now, you can see in this particular index here

154
00:07:07,50 --> 00:07:10,30
that we have the 18 cards passed as props.

155
00:07:10,30 --> 00:07:13,20
So we have all the data that we need.

156
00:07:13,20 --> 00:07:14,50
So we're good to go.

157
00:07:14,50 --> 00:07:16,90
We initialized our state with the props,

158
00:07:16,90 --> 00:07:18,50
so we're good to go.

159
00:07:18,50 --> 00:07:20,00
And now, basically, what it did

160
00:07:20,00 --> 00:07:24,40
it iterated through all 18 elements inside of our array

161
00:07:24,40 --> 00:07:27,40
and created a card inside of our application.

162
00:07:27,40 --> 00:07:30,50
So if you scroll down, there should be 18 of them.

163
00:07:30,50 --> 00:07:32,90
And we got them here as well.

164
00:07:32,90 --> 00:07:36,40
So you can see the key, zero, one, two, three, four, five,

165
00:07:36,40 --> 00:07:37,50
and so on and so forth.

166
00:07:37,50 --> 00:07:38,80
Okay.

167
00:07:38,80 --> 00:07:41,70
You could also fetch data from an external API

168
00:07:41,70 --> 00:07:45,20
and there are multiple examples of this in the documentation

169
00:07:45,20 --> 00:07:47,30
so let me show you an example.

170
00:07:47,30 --> 00:07:49,50
So if you click on a second page

171
00:07:49,50 --> 00:07:55,90
and go to github.com/zeit/next.js

172
00:07:55,90 --> 00:07:58,70
and then as you get to here,

173
00:07:58,70 --> 00:08:01,70
let me grab the top level of where you should be right now,

174
00:08:01,70 --> 00:08:05,00
click on, scroll down to examples

175
00:08:05,00 --> 00:08:09,30
and then scroll down again til you see data-fetch

176
00:08:09,30 --> 00:08:14,20
and then click on pages and then click on index.js.

177
00:08:14,20 --> 00:08:16,40
And basically, you recognize

178
00:08:16,40 --> 00:08:17,90
some of the code that we just did.

179
00:08:17,90 --> 00:08:20,90
So we just did a simple async get initial props

180
00:08:20,90 --> 00:08:25,70
and then return what we got from our import of the data.

181
00:08:25,70 --> 00:08:28,20
But if you had an external library,

182
00:08:28,20 --> 00:08:31,50
first of all, you would need to install isomorphic-unfetch

183
00:08:31,50 --> 00:08:36,20
so you would do an npm install isomorphic-unfetch.

184
00:08:36,20 --> 00:08:38,40
And then basically do the exact same code

185
00:08:38,40 --> 00:08:39,60
as you're seeing here.

186
00:08:39,60 --> 00:08:41,90
So create, basically, a variable.

187
00:08:41,90 --> 00:08:44,30
Call the response and then await fetch

188
00:08:44,30 --> 00:08:47,30
the particular URL you're fetching.

189
00:08:47,30 --> 00:08:49,60
And then create a json.

190
00:08:49,60 --> 00:08:52,50
So once you get a response from your fetch,

191
00:08:52,50 --> 00:08:54,60
you create a json variable,

192
00:08:54,60 --> 00:08:58,20
which you then pass to the actual state

193
00:08:58,20 --> 00:09:01,20
or basically the props that you want to add that data to.

194
00:09:01,20 --> 00:09:05,60
So in this case, he's passing stargazers_count to stars

195
00:09:05,60 --> 00:09:07,60
and then it would be available to render

196
00:09:07,60 --> 00:09:09,60
inside of your component.

197
00:09:09,60 --> 00:09:12,00
So that's as simple as it is

198
00:09:12,00 --> 00:09:15,10
to actually fetch external APIs.

199
00:09:15,10 --> 00:09:16,90
And while I'm here,

200
00:09:16,90 --> 00:09:22,20
if you ever wonder about any specific scenarios of next.js,

201
00:09:22,20 --> 00:09:25,60
you can click on the examples and you'll see there's, like,

202
00:09:25,60 --> 00:09:30,00
30 or so examples of scenarios that you could use.

203
00:09:30,00 --> 00:09:33,90
For example, router, preact, a whole bunch of libraries.

204
00:09:33,90 --> 00:09:36,30
One was about redux and so on and so forth.

205
00:09:36,30 --> 00:09:38,70
So feel free to browse this is if you want

206
00:09:38,70 --> 00:09:40,40
and see some examples of code.

207
00:09:40,40 --> 00:09:43,00
There's multiple ways to use next.js.

208
00:09:43,00 --> 00:09:45,40
And the dev team was kind enough

209
00:09:45,40 --> 00:09:47,60
to do a lot of these examples.

210
00:09:47,60 --> 00:09:49,50
So as you can see, it's not really hard to add data

211
00:09:49,50 --> 00:09:53,20
to our state and get our data going into our application.

212
00:09:53,20 --> 00:09:57,70
And remember, this is all happening in the server.

213
00:09:57,70 --> 00:09:59,00
Let's move on.

