1
00:00:00,70 --> 00:00:03,30
- [Instructor] A redux implementation requires actions

2
00:00:03,30 --> 00:00:06,10
that will be dispatched and then will call a reducer

3
00:00:06,10 --> 00:00:08,70
so we can update the store with the new data available

4
00:00:08,70 --> 00:00:10,60
for the application to feed from.

5
00:00:10,60 --> 00:00:12,30
So let's work on this.

6
00:00:12,30 --> 00:00:14,30
So the first thing I want you to notice is

7
00:00:14,30 --> 00:00:16,60
where I've put the notes, or the comments,

8
00:00:16,60 --> 00:00:18,50
and I will continue doing that as we build

9
00:00:18,50 --> 00:00:20,70
the redux implementation.

10
00:00:20,70 --> 00:00:23,30
Basically everywhere I have a note,

11
00:00:23,30 --> 00:00:27,60
which basically means that this is a specific part of redux

12
00:00:27,60 --> 00:00:30,40
you can actually put in its own file.

13
00:00:30,40 --> 00:00:32,40
So we'll continue with this tradition

14
00:00:32,40 --> 00:00:35,00
and now we'll create the actions.

15
00:00:35,00 --> 00:00:38,20
So just return on line nine like so,

16
00:00:38,20 --> 00:00:41,90
and this will be the actions and we'll create

17
00:00:41,90 --> 00:00:43,40
basically two actions.

18
00:00:43,40 --> 00:00:44,90
You can create more if you want,

19
00:00:44,90 --> 00:00:47,70
if you want to play with redux feel free to continue

20
00:00:47,70 --> 00:00:50,30
adding more as you can see the example here

21
00:00:50,30 --> 00:00:52,90
you'll be able to determine what are the other things

22
00:00:52,90 --> 00:00:54,40
that you need to put together.

23
00:00:54,40 --> 00:00:56,70
But let me just do two of them here.

24
00:00:56,70 --> 00:01:02,30
So the first one we'll create is called initialCards

25
00:01:02,30 --> 00:01:06,00
and basically this action will return an object.

26
00:01:06,00 --> 00:01:08,80
So actions, so while we're on the subject,

27
00:01:08,80 --> 00:01:11,80
you're going to seek different ways of doing actions,

28
00:01:11,80 --> 00:01:15,10
action types on redux implementation.

29
00:01:15,10 --> 00:01:17,60
I'd like to simplify things as much as possible,

30
00:01:17,60 --> 00:01:21,90
so my limitation is basically to export a function

31
00:01:21,90 --> 00:01:24,60
that returns the type as well.

32
00:01:24,60 --> 00:01:27,50
So when we are dispatching that specific function here,

33
00:01:27,50 --> 00:01:31,00
we are passing the type and then whatever object

34
00:01:31,00 --> 00:01:34,20
we need to pass to the reducers and then the reducers

35
00:01:34,20 --> 00:01:38,10
actually make a change to our store or our state.

36
00:01:38,10 --> 00:01:39,80
And that's my approach to it,

37
00:01:39,80 --> 00:01:41,70
there's multiple ways to approach it.

38
00:01:41,70 --> 00:01:44,30
Feel free to do it whichever way you prefer.

39
00:01:44,30 --> 00:01:46,10
But this is my approach.

40
00:01:46,10 --> 00:01:48,10
So let's go ahead and do it.

41
00:01:48,10 --> 00:01:52,10
So basically we have a type for this specific.

42
00:01:52,10 --> 00:01:54,60
So we have a type for this action,

43
00:01:54,60 --> 00:02:00,70
and then this specific types passes a specific item.

44
00:02:00,70 --> 00:02:03,30
So specifically in this case, I'm passing the data

45
00:02:03,30 --> 00:02:05,80
that we have in our JSON file here.

46
00:02:05,80 --> 00:02:10,00
So when we call the initialCards or the initial load,

47
00:02:10,00 --> 00:02:13,40
it will basically go through the reducer

48
00:02:13,40 --> 00:02:16,30
and then update the cards with the data

49
00:02:16,30 --> 00:02:18,90
that we have from our data.json.

50
00:02:18,90 --> 00:02:20,80
So that's the first action.

51
00:02:20,80 --> 00:02:23,60
Then I'm going to create a fixive second action

52
00:02:23,60 --> 00:02:25,80
that we're not going to use in this course,

53
00:02:25,80 --> 00:02:28,60
but to give you an idea of a second one.

54
00:02:28,60 --> 00:02:30,90
So let's go head and do that.

55
00:02:30,90 --> 00:02:35,00
The second one will be called addItem

56
00:02:35,00 --> 00:02:37,50
and we're passing an item to it.

57
00:02:37,50 --> 00:02:40,70
So when we are dispatching the specific action,

58
00:02:40,70 --> 00:02:42,50
we are passing an item to it

59
00:02:42,50 --> 00:02:46,00
which we'll use inside of our reducer.

60
00:02:46,00 --> 00:02:51,80
And this type is ADD, and we're passing the time.

61
00:02:51,80 --> 00:02:55,30
So we have our two actions, so once we are actually

62
00:02:55,30 --> 00:02:58,50
dispatching those actions, we'll send those actions

63
00:02:58,50 --> 00:03:01,80
to the reducers based off what we are calling here.

64
00:03:01,80 --> 00:03:06,20
So once we call initialCards, we'll pass this specific type,

65
00:03:06,20 --> 00:03:08,40
and then the data, and then the reducer will actually

66
00:03:08,40 --> 00:03:11,30
do the changes to our store that we need to

67
00:03:11,30 --> 00:03:13,00
and then the same thing with an item.

68
00:03:13,00 --> 00:03:15,70
If we would use them, then we would pass an item

69
00:03:15,70 --> 00:03:18,90
to that function and then it would call onto the actual

70
00:03:18,90 --> 00:03:21,40
reducer the add type, and then pass

71
00:03:21,40 --> 00:03:22,80
the item to make changes.

72
00:03:22,80 --> 00:03:27,20
Again, to the actual reducer slash store slash statement.

73
00:03:27,20 --> 00:03:28,00
Okay?

74
00:03:28,00 --> 00:03:31,40
So we have one more piece of the puzzle for redux.

75
00:03:31,40 --> 00:03:33,00
Let's move on.

