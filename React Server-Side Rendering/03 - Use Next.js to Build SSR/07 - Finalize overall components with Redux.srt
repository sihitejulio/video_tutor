1
00:00:00,60 --> 00:00:02,40
- [Instructor] Now that we've got all the files ready

2
00:00:02,40 --> 00:00:05,40
for our Redux store, actions and reducers,

3
00:00:05,40 --> 00:00:06,70
now it's time to implement this

4
00:00:06,70 --> 00:00:08,80
into our application and do the final code

5
00:00:08,80 --> 00:00:10,30
into an index file.

6
00:00:10,30 --> 00:00:11,60
Let's get to it.

7
00:00:11,60 --> 00:00:13,20
So, the first thing we need to do

8
00:00:13,20 --> 00:00:14,80
if you don't have it open,

9
00:00:14,80 --> 00:00:17,10
open index.js

10
00:00:17,10 --> 00:00:21,30
and then what we'll do is remove the import data first

11
00:00:21,30 --> 00:00:22,70
because we no longer need it,

12
00:00:22,70 --> 00:00:25,50
it's going to be part of our state already

13
00:00:25,50 --> 00:00:28,40
and then what we'll do is at the top,

14
00:00:28,40 --> 00:00:30,90
so after react, so click here,

15
00:00:30,90 --> 00:00:34,40
and then return, we need to import a few files.

16
00:00:34,40 --> 00:00:43,40
And we need to first import bindActionCreators from redux.

17
00:00:43,40 --> 00:00:47,00
Then we need to import from our store,

18
00:00:47,00 --> 00:00:50,40
so from the store file that we just created a few things.

19
00:00:50,40 --> 00:00:53,20
So, we need to import initStore,

20
00:00:53,20 --> 00:00:56,30
we need to import initialCards

21
00:00:56,30 --> 00:00:59,00
and we need to import addItem

22
00:00:59,00 --> 00:01:02,00
and that is coming from our store

23
00:01:02,00 --> 00:01:03,60
and the last thing we need to import

24
00:01:03,60 --> 00:01:07,20
is withRedux

25
00:01:07,20 --> 00:01:11,40
from next-redux-wrapper

26
00:01:11,40 --> 00:01:14,80
which allow us to do Redux with next.

27
00:01:14,80 --> 00:01:16,20
So, the next thing we need to do

28
00:01:16,20 --> 00:01:18,10
is remove the export default

29
00:01:18,10 --> 00:01:20,60
and you'll understand why in a few seconds

30
00:01:20,60 --> 00:01:23,20
while we actually export that from somewhere else

31
00:01:23,20 --> 00:01:24,30
in this file

32
00:01:24,30 --> 00:01:26,20
and then everything else stays the same

33
00:01:26,20 --> 00:01:28,80
and then our getInitialProps function

34
00:01:28,80 --> 00:01:30,00
will change a little bit,

35
00:01:30,00 --> 00:01:33,20
so let's remove the return cards: data

36
00:01:33,20 --> 00:01:36,40
and actually and then do store.dispatch

37
00:01:36,40 --> 00:01:38,60
and this is where we're dispatching

38
00:01:38,60 --> 00:01:40,10
the actual function

39
00:01:40,10 --> 00:01:41,70
that will initially run

40
00:01:41,70 --> 00:01:45,40
when we first get our application together here,

41
00:01:45,40 --> 00:01:47,90
so initialCards

42
00:01:47,90 --> 00:01:49,80
and this is where we'll load

43
00:01:49,80 --> 00:01:53,00
the initial cards inside of our application

44
00:01:53,00 --> 00:01:55,70
and that's pretty much it for this section

45
00:01:55,70 --> 00:01:57,10
and then what I want to do

46
00:01:57,10 --> 00:02:00,50
is right at the bottom of this class,

47
00:02:00,50 --> 00:02:03,50
so below this actual code here,

48
00:02:03,50 --> 00:02:04,70
we'll create a few things.

49
00:02:04,70 --> 00:02:06,60
So, the first thing we'll create

50
00:02:06,60 --> 00:02:09,20
is a variable which we'll call

51
00:02:09,20 --> 00:02:11,50
and if you've ever done Redux before

52
00:02:11,50 --> 00:02:13,00
you'll be familiar with this,

53
00:02:13,00 --> 00:02:18,40
it's called mapDispatchToProps,

54
00:02:18,40 --> 00:02:20,30
so basically what we're doing here

55
00:02:20,30 --> 00:02:23,30
is basically mapping the props

56
00:02:23,30 --> 00:02:25,30
to the store's props

57
00:02:25,30 --> 00:02:27,50
and we'll be able to use them in here

58
00:02:27,50 --> 00:02:28,70
because of that

59
00:02:28,70 --> 00:02:30,80
and we'll do the same for the store.

60
00:02:30,80 --> 00:02:33,50
So, the props that we're matching are actually

61
00:02:33,50 --> 00:02:38,10
the two functions that we want to have access to,

62
00:02:38,10 --> 00:02:40,90
so the first one is initialCards

63
00:02:40,90 --> 00:02:44,20
which is coming from our store

64
00:02:44,20 --> 00:02:47,10
and we need to bindActionCreators

65
00:02:47,10 --> 00:02:48,70
with initialCards

66
00:02:48,70 --> 00:02:50,40
and then dispatch.

67
00:02:50,40 --> 00:02:53,20
So, basically what I'm doing with this single line here

68
00:02:53,20 --> 00:02:57,50
is I'm mapping the actual initial card's function

69
00:02:57,50 --> 00:02:59,10
that comes from our store

70
00:02:59,10 --> 00:03:01,10
to the initial cards

71
00:03:01,10 --> 00:03:03,90
on this particular component here.

72
00:03:03,90 --> 00:03:06,40
So, this is required so we have access to it

73
00:03:06,40 --> 00:03:08,80
and let's do the same even though we're not using it

74
00:03:08,80 --> 00:03:13,90
for addItem and let me just copy and paste this line here,

75
00:03:13,90 --> 00:03:18,20
so, copy line 36 up to the parentheses

76
00:03:18,20 --> 00:03:19,80
and then paste it

77
00:03:19,80 --> 00:03:24,40
and then just change initialCards to additem.

78
00:03:24,40 --> 00:03:25,80
And let me make sure it's not plural,

79
00:03:25,80 --> 00:03:28,60
addItems, no it's item, singular.

80
00:03:28,60 --> 00:03:29,90
Okay, so we're good.

81
00:03:29,90 --> 00:03:30,90
Perfect.

82
00:03:30,90 --> 00:03:33,30
Okay, we need to create the exact same thing

83
00:03:33,30 --> 00:03:35,50
to map our state to the props,

84
00:03:35,50 --> 00:03:37,00
so let's go ahead and do that,

85
00:03:37,00 --> 00:03:42,00
so mapStateToProps

86
00:03:42,00 --> 00:03:45,40
which will take the state as an argument

87
00:03:45,40 --> 00:03:47,80
and then we'll return

88
00:03:47,80 --> 00:03:51,20
the cards in state.cards.

89
00:03:51,20 --> 00:03:52,50
So, this way we'll make sure

90
00:03:52,50 --> 00:03:54,50
that our cards are actually showing up

91
00:03:54,50 --> 00:03:56,50
in this particular file.

92
00:03:56,50 --> 00:03:57,90
And then the last thing we need to do

93
00:03:57,90 --> 00:03:59,60
is export that component,

94
00:03:59,60 --> 00:04:02,20
so export default

95
00:04:02,20 --> 00:04:04,80
withRedux, so what we're doing here

96
00:04:04,80 --> 00:04:06,60
is making sure that Redux works

97
00:04:06,60 --> 00:04:08,90
inside of that file.

98
00:04:08,90 --> 00:04:11,50
So, we'll pass initStore,

99
00:04:11,50 --> 00:04:13,60
so we're initializing the store

100
00:04:13,60 --> 00:04:16,20
with this argument here.

101
00:04:16,20 --> 00:04:20,00
We need to pass the mapStateToProps

102
00:04:20,00 --> 00:04:22,90
in order to be able to use the state

103
00:04:22,90 --> 00:04:27,10
and also map our dispatchToProps

104
00:04:27,10 --> 00:04:29,20
and finally, all you need to do

105
00:04:29,20 --> 00:04:33,00
is basically add index

106
00:04:33,00 --> 00:04:36,20
or the actual class that we created.

107
00:04:36,20 --> 00:04:39,00
Okay, so semicolon to finish this

108
00:04:39,00 --> 00:04:41,30
and then let's make sure that our index

109
00:04:41,30 --> 00:04:43,10
is also a capital I

110
00:04:43,10 --> 00:04:44,40
and let's go back up.

111
00:04:44,40 --> 00:04:47,60
The class is capital I, we're good.

112
00:04:47,60 --> 00:04:49,40
While I'm here I just realized one thing.

113
00:04:49,40 --> 00:04:50,60
I'm actually using the store,

114
00:04:50,60 --> 00:04:53,10
I'm not passing it inside of that function.

115
00:04:53,10 --> 00:04:54,30
That needs to happen,

116
00:04:54,30 --> 00:04:57,20
so let's go and do that and fix that right away

117
00:04:57,20 --> 00:04:59,00
and then save all this

118
00:04:59,00 --> 00:05:01,60
and then let's run our application,

119
00:05:01,60 --> 00:05:05,90
so npm run dev, we're good to go.

120
00:05:05,90 --> 00:05:09,30
Perfect and then let's go to our application

121
00:05:09,30 --> 00:05:11,40
and refresh this

122
00:05:11,40 --> 00:05:12,90
and we are good to go.

123
00:05:12,90 --> 00:05:15,20
So, right now if you see the cards,

124
00:05:15,20 --> 00:05:18,30
that means that your implementation of Redux worked.

125
00:05:18,30 --> 00:05:20,00
So, if we bring up the DevTools,

126
00:05:20,00 --> 00:05:22,70
so option command I for Mac,

127
00:05:22,70 --> 00:05:25,00
control shift I for Windows

128
00:05:25,00 --> 00:05:26,80
and then click on React

129
00:05:26,80 --> 00:05:28,20
and then let's scroll down,

130
00:05:28,20 --> 00:05:31,60
so click on Container, click on AppContainer,

131
00:05:31,60 --> 00:05:34,80
you see the WrappedCom isServerTrue

132
00:05:34,80 --> 00:05:36,40
and then you see the initial state

133
00:05:36,40 --> 00:05:38,50
with the cards here.

134
00:05:38,50 --> 00:05:41,70
If we actually drop down a notch,

135
00:05:41,70 --> 00:05:46,70
and then again up until we actually see the index,

136
00:05:46,70 --> 00:05:48,30
then you should see in the props

137
00:05:48,30 --> 00:05:50,60
the cards that are being passed to it like so.

138
00:05:50,60 --> 00:05:51,90
And we're good to go.

139
00:05:51,90 --> 00:05:54,00
Now, one thing that I want to show you

140
00:05:54,00 --> 00:05:56,50
is the Redux DevTools

141
00:05:56,50 --> 00:05:57,30
and this is something

142
00:05:57,30 --> 00:05:58,90
that's going to help you tremendously

143
00:05:58,90 --> 00:06:00,40
if you work with Redux.

144
00:06:00,40 --> 00:06:02,40
Let me do a quick search on a new tab,

145
00:06:02,40 --> 00:06:05,00
so Redux DevTools

146
00:06:05,00 --> 00:06:07,30
and I'm going to go for Chrome.

147
00:06:07,30 --> 00:06:09,40
If you want to use the Redux DevTools

148
00:06:09,40 --> 00:06:11,50
with another browser

149
00:06:11,50 --> 00:06:13,50
or use the actual standalone app,

150
00:06:13,50 --> 00:06:16,50
you can click on Redux DevTools here

151
00:06:16,50 --> 00:06:17,60
and go there.

152
00:06:17,60 --> 00:06:20,60
In my case I'm going to go for the Redux DevTools extension

153
00:06:20,60 --> 00:06:23,10
on Chrome like so

154
00:06:23,10 --> 00:06:25,50
and then Add to Chrome

155
00:06:25,50 --> 00:06:26,90
and if it's added to Chrome,

156
00:06:26,90 --> 00:06:28,40
then you're good to go

157
00:06:28,40 --> 00:06:29,30
and let's make sure

158
00:06:29,30 --> 00:06:31,00
that's it's running,

159
00:06:31,00 --> 00:06:35,10
so let me go inside of my extensions in here,

160
00:06:35,10 --> 00:06:37,90
so click on Windows, Extensions

161
00:06:37,90 --> 00:06:39,40
and let's scroll all the way

162
00:06:39,40 --> 00:06:42,00
to the actual extension Redux

163
00:06:42,00 --> 00:06:43,60
and make sure it's running.

164
00:06:43,60 --> 00:06:45,30
So, it's doesn't seem to be here

165
00:06:45,30 --> 00:06:46,80
and it says that I have it installed,

166
00:06:46,80 --> 00:06:50,20
that is weird.

167
00:06:50,20 --> 00:06:52,30
Okay, so now we got Redux DevTools.

168
00:06:52,30 --> 00:06:56,10
In theory now we should actually see our Redux.

169
00:06:56,10 --> 00:06:58,60
So, if you don't see the tab here,

170
00:06:58,60 --> 00:07:01,30
make sure the extension is installed properly.

171
00:07:01,30 --> 00:07:03,30
If you see the tab, click on it

172
00:07:03,30 --> 00:07:07,00
and I'm going to increase my window here

173
00:07:07,00 --> 00:07:10,30
and this is the really cool thing about Redux

174
00:07:10,30 --> 00:07:12,20
and the Redux DevTool.

175
00:07:12,20 --> 00:07:15,10
So, right now I can click on State

176
00:07:15,10 --> 00:07:16,70
and see my current state

177
00:07:16,70 --> 00:07:17,60
with my cards,

178
00:07:17,60 --> 00:07:18,80
so I see all the cards,

179
00:07:18,80 --> 00:07:20,80
I can see all the information.

180
00:07:20,80 --> 00:07:24,30
As you do new things with your state,

181
00:07:24,30 --> 00:07:26,20
you're actually going to see

182
00:07:26,20 --> 00:07:29,20
the history of your state showing up here

183
00:07:29,20 --> 00:07:32,60
and then you can actually roll back in the history

184
00:07:32,60 --> 00:07:34,10
of your state.

185
00:07:34,10 --> 00:07:36,60
You can also see it as a chart

186
00:07:36,60 --> 00:07:40,20
which is pretty cool when you have lots of information.

187
00:07:40,20 --> 00:07:42,60
You can actually see your cards like that.

188
00:07:42,60 --> 00:07:44,30
So, once you refresh your browser,

189
00:07:44,30 --> 00:07:47,60
you can see the type that has the init in here

190
00:07:47,60 --> 00:07:49,30
and then as you do more actions,

191
00:07:49,30 --> 00:07:51,90
you'll see basically in your inspector

192
00:07:51,90 --> 00:07:54,00
the actions, what were the differences

193
00:07:54,00 --> 00:07:55,60
in between of those two

194
00:07:55,60 --> 00:07:57,40
which are none for now

195
00:07:57,40 --> 00:07:59,80
but we can actually see all these things

196
00:07:59,80 --> 00:08:02,80
inside of our DevTool.

197
00:08:02,80 --> 00:08:05,60
And voila, you've got server side rendering

198
00:08:05,60 --> 00:08:07,50
implemented with Redux.

199
00:08:07,50 --> 00:08:09,90
Although it's a minimal implementation,

200
00:08:09,90 --> 00:08:11,00
it's a good starting point

201
00:08:11,00 --> 00:08:13,20
to build more complex applications.

202
00:08:13,20 --> 00:08:15,00
So, let's move on.

