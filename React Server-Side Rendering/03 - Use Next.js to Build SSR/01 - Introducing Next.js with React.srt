1
00:00:00,50 --> 00:00:02,80
- If you've never heard of Next.js,

2
00:00:02,80 --> 00:00:04,80
it's a great library that allows you to quickly

3
00:00:04,80 --> 00:00:07,10
set up server-side, rendered applications

4
00:00:07,10 --> 00:00:09,40
with just a few lines of code.

5
00:00:09,40 --> 00:00:11,70
I will demonstrate this in a few seconds.

6
00:00:11,70 --> 00:00:15,40
It's easy to get started with but also highly extensible,

7
00:00:15,40 --> 00:00:18,00
and works great with libraries like Redox,

8
00:00:18,00 --> 00:00:20,50
which we'll also demonstrate in this course.

9
00:00:20,50 --> 00:00:22,30
If you want to take a look at the repo,

10
00:00:22,30 --> 00:00:29,60
you can go to github.com/zeit/next.js.

11
00:00:29,60 --> 00:00:32,20
Behind the curtain it uses webpack,

12
00:00:32,20 --> 00:00:34,50
which you may or may not be familiar with.

13
00:00:34,50 --> 00:00:37,00
But in any case, this is what builds your application

14
00:00:37,00 --> 00:00:40,00
into bundle to be used in the server.

15
00:00:40,00 --> 00:00:42,50
Let's get started with Next.js.

16
00:00:42,50 --> 00:00:44,30
You can close this.

17
00:00:44,30 --> 00:00:46,70
And let's open a terminal.

18
00:00:46,70 --> 00:00:48,60
Let's check where we are.

19
00:00:48,60 --> 00:00:50,60
Okay, so we want to be on the desktop,

20
00:00:50,60 --> 00:00:54,10
so I'm actually going to change directory to the desktop.

21
00:00:54,10 --> 00:00:55,20
I'm good.

22
00:00:55,20 --> 00:00:57,80
And what I'm going to do is, do a few commands.

23
00:00:57,80 --> 00:01:00,00
I'm going to first make a directory,

24
00:01:00,00 --> 00:01:02,90
which I'll call SSR.

25
00:01:02,90 --> 00:01:06,20
And if you've never seen this command here is basically,

26
00:01:06,20 --> 00:01:08,30
I'm going to execute this command,

27
00:01:08,30 --> 00:01:09,30
and once I'm done,

28
00:01:09,30 --> 00:01:11,70
I'm going to execute with the other command.

29
00:01:11,70 --> 00:01:13,80
So if that command isn't successful,

30
00:01:13,80 --> 00:01:15,80
then it won't do the second one.

31
00:01:15,80 --> 00:01:18,00
So, once I actually created this,

32
00:01:18,00 --> 00:01:21,50
let's change directory into SSR.

33
00:01:21,50 --> 00:01:26,80
Then let's create a new package.json.

34
00:01:26,80 --> 00:01:28,90
And let's start with this.

35
00:01:28,90 --> 00:01:31,40
And yes we want to name it SSR.

36
00:01:31,40 --> 00:01:33,30
Yep, yep, yep, yep.

37
00:01:33,30 --> 00:01:34,40
Good.

38
00:01:34,40 --> 00:01:36,50
So, one note about Git repository,

39
00:01:36,50 --> 00:01:39,30
and you actually hear me saying this over and over

40
00:01:39,30 --> 00:01:40,40
in my courses,

41
00:01:40,40 --> 00:01:43,20
if you have a Git repository, add it there.

42
00:01:43,20 --> 00:01:46,00
If you don't, it will actually tell you

43
00:01:46,00 --> 00:01:49,20
when you are actually doing NPM commands.

44
00:01:49,20 --> 00:01:51,20
Just a quick note for yourself.

45
00:01:51,20 --> 00:01:53,50
And I'm going to put my name.

46
00:01:53,50 --> 00:01:55,30
Yep, we're good with that.

47
00:01:55,30 --> 00:01:57,40
And, we are good to go.

48
00:01:57,40 --> 00:01:59,00
Now that we have all this,

49
00:01:59,00 --> 00:02:00,80
I'm going to get out of this terminal

50
00:02:00,80 --> 00:02:03,40
and actually continue in VS code.

51
00:02:03,40 --> 00:02:06,60
I've got nothing here so let's open

52
00:02:06,60 --> 00:02:09,00
in Explorer the new folders.

53
00:02:09,00 --> 00:02:12,10
Open folder, then let's go on the desktop,

54
00:02:12,10 --> 00:02:15,10
and select SSR, like so.

55
00:02:15,10 --> 00:02:16,40
Perfect.

56
00:02:16,40 --> 00:02:17,30
The next thing I want to do

57
00:02:17,30 --> 00:02:20,40
is create a quick Git ignore file here.

58
00:02:20,40 --> 00:02:22,00
Let's go ahead and do that.

59
00:02:22,00 --> 00:02:25,50
Create new file, .Gitignore.

60
00:02:25,50 --> 00:02:26,70
This is actually done for you

61
00:02:26,70 --> 00:02:29,90
when you actually create a new React project,

62
00:02:29,90 --> 00:02:33,60
but because we're actually going to start this from scratch.

63
00:02:33,60 --> 00:02:36,00
So let's make sure we don't include the node modules

64
00:02:36,00 --> 00:02:37,60
in our Git commits.

65
00:02:37,60 --> 00:02:39,10
So we're good.

66
00:02:39,10 --> 00:02:41,80
The next thing I want to do is create a new folder,

67
00:02:41,80 --> 00:02:44,20
and I will call this folder Pages.

68
00:02:44,20 --> 00:02:46,90
And inside of that folder, I'll create a new file,

69
00:02:46,90 --> 00:02:50,50
which will be our index.js file.

70
00:02:50,50 --> 00:02:52,70
And let's quickly do a stateless component

71
00:02:52,70 --> 00:02:55,50
inside of that particular file.

72
00:02:55,50 --> 00:03:00,80
I won't need to import React for now, if I'm using Next.js,

73
00:03:00,80 --> 00:03:02,20
and I'll show you in a second,

74
00:03:02,20 --> 00:03:05,10
it's really cool to use Next.js

75
00:03:05,10 --> 00:03:07,00
when you want to do React stuff.

76
00:03:07,00 --> 00:03:12,70
I'm going to simply do P1 that says "hello"

77
00:03:12,70 --> 00:03:14,80
and close my tag.

78
00:03:14,80 --> 00:03:17,30
And I'm good to go with that.

79
00:03:17,30 --> 00:03:19,80
Now let's just put a semicolon here,

80
00:03:19,80 --> 00:03:21,30
and let me save that.

81
00:03:21,30 --> 00:03:22,80
Then all I need to do at this point

82
00:03:22,80 --> 00:03:26,00
is actually install all the required dependencies

83
00:03:26,00 --> 00:03:30,00
that I need, and then get started with Next.js.

84
00:03:30,00 --> 00:03:31,40
I'm going to bring up my terminal,

85
00:03:31,40 --> 00:03:34,90
so click on View, Integrated Terminal.

86
00:03:34,90 --> 00:03:36,00
And inside of my terminal,

87
00:03:36,00 --> 00:03:38,40
I am going to install a few things.

88
00:03:38,40 --> 00:03:44,00
First, I'm going to NPM install, dash dash save.

89
00:03:44,00 --> 00:03:51,10
And I'm going to install Next, React, and React dom.

90
00:03:51,10 --> 00:03:53,20
Once I have those three installed,

91
00:03:53,20 --> 00:03:56,90
then all I have to do is go to the package.json file.

92
00:03:56,90 --> 00:03:58,40
Let me just close this for now.

93
00:03:58,40 --> 00:04:00,50
And actually add a few scripts here.

94
00:04:00,50 --> 00:04:02,40
The first script that I'm going to do

95
00:04:02,40 --> 00:04:04,80
is change this one here,

96
00:04:04,80 --> 00:04:10,70
and I'm going to call this one dev and basically do next.

97
00:04:10,70 --> 00:04:12,60
And then let's add the other ones as well,

98
00:04:12,60 --> 00:04:15,20
'cause there's a few that we can use with Next.

99
00:04:15,20 --> 00:04:17,50
We're going to have a build script,

100
00:04:17,50 --> 00:04:20,90
so next build that we could use.

101
00:04:20,90 --> 00:04:28,40
And we also have a start that we could use with next start.

102
00:04:28,40 --> 00:04:30,80
Like so, okay?

103
00:04:30,80 --> 00:04:33,60
What I'm going to do now is use the dev one,

104
00:04:33,60 --> 00:04:35,60
so let's open up our terminal.

105
00:04:35,60 --> 00:04:37,00
So View, Integrated Terminal.

106
00:04:37,00 --> 00:04:40,40
You could do control gravity if you want,

107
00:04:40,40 --> 00:04:45,40
and then we'll do an NPM run dev.

108
00:04:45,40 --> 00:04:47,60
And now we got our project started.

109
00:04:47,60 --> 00:04:49,40
We are good to go.

110
00:04:49,40 --> 00:04:55,70
We could go into our browser and open up the localhost3000.

111
00:04:55,70 --> 00:04:58,10
And we got our page rendered.

112
00:04:58,10 --> 00:04:59,80
You may be wondering, "What's happening here?

113
00:04:59,80 --> 00:05:01,20
Is this SSR?

114
00:05:01,20 --> 00:05:03,20
Is this server-side rendering?"

115
00:05:03,20 --> 00:05:05,10
Well, the answer is yes.

116
00:05:05,10 --> 00:05:08,00
If you've never worked with Next.js before,

117
00:05:08,00 --> 00:05:09,50
and have some experience with React,

118
00:05:09,50 --> 00:05:10,80
you may wondering what kind

119
00:05:10,80 --> 00:05:12,80
of voodoo magic just happened here.

120
00:05:12,80 --> 00:05:14,90
Well, that is the beauty of Next.js.

121
00:05:14,90 --> 00:05:17,60
It does a lot of the background work for you,

122
00:05:17,60 --> 00:05:20,00
including code splitting, which we'll take a look at

123
00:05:20,00 --> 00:05:21,40
further in this course.

124
00:05:21,40 --> 00:05:23,60
And as we have more code to this project,

125
00:05:23,60 --> 00:05:26,70
you'll see just how intuitive the library is.

126
00:05:26,70 --> 00:05:27,60
Oh, and yes.

127
00:05:27,60 --> 00:05:30,50
This is all running in the server.

128
00:05:30,50 --> 00:05:32,00
So let's move on.

