1
00:00:00,30 --> 00:00:02,20
- [Instructor] So, we need to add a bit more meat

2
00:00:02,20 --> 00:00:03,80
to our application.

3
00:00:03,80 --> 00:00:06,20
So, let's go to our desktop

4
00:00:06,20 --> 00:00:07,80
and in the exercise files

5
00:00:07,80 --> 00:00:11,10
I included a project I created in another course.

6
00:00:11,10 --> 00:00:12,40
If you click on Resources

7
00:00:12,40 --> 00:00:14,40
you'll find the sample here.

8
00:00:14,40 --> 00:00:15,60
We'll use some of those files

9
00:00:15,60 --> 00:00:16,60
to build our application

10
00:00:16,60 --> 00:00:18,20
leveraging server side rendering

11
00:00:18,20 --> 00:00:19,50
and show how different it is

12
00:00:19,50 --> 00:00:22,40
from building a straight React application.

13
00:00:22,40 --> 00:00:24,10
So, let's build this.

14
00:00:24,10 --> 00:00:25,80
So, keep that open

15
00:00:25,80 --> 00:00:28,90
and open your SSR folder

16
00:00:28,90 --> 00:00:30,90
and what I'm going to do is create a new folder

17
00:00:30,90 --> 00:00:35,00
called Static and this is where with Next.js

18
00:00:35,00 --> 00:00:36,60
you put all your static files

19
00:00:36,60 --> 00:00:40,60
such as pictures, anything that you want to download

20
00:00:40,60 --> 00:00:42,50
or whatever is the static file

21
00:00:42,50 --> 00:00:45,40
that you used to put in public for React,

22
00:00:45,40 --> 00:00:47,20
it goes in Static here.

23
00:00:47,20 --> 00:00:53,00
So, in this case we need to have background, juice and logo

24
00:00:53,00 --> 00:00:54,60
and copy those three

25
00:00:54,60 --> 00:00:57,70
and put them in Static

26
00:00:57,70 --> 00:00:59,00
and then from our sample,

27
00:00:59,00 --> 00:01:00,80
copy those three files

28
00:01:00,80 --> 00:01:02,30
like so

29
00:01:02,30 --> 00:01:05,50
and place them in Pages here.

30
00:01:05,50 --> 00:01:07,20
Okay, perfect.

31
00:01:07,20 --> 00:01:08,40
So, the next thing we need to do

32
00:01:08,40 --> 00:01:10,80
because Next.js doesn't come built in

33
00:01:10,80 --> 00:01:13,90
with CSS support, it needs to be added.

34
00:01:13,90 --> 00:01:15,10
Let's go ahead and do that.

35
00:01:15,10 --> 00:01:17,60
So, jump to VS Code,

36
00:01:17,60 --> 00:01:20,60
shut down your server by doing control C

37
00:01:20,60 --> 00:01:33,80
and we're going to do an npm install --save @zeit/next-css.

38
00:01:33,80 --> 00:01:35,30
What you need to do next

39
00:01:35,30 --> 00:01:37,50
is to do an actual config,

40
00:01:37,50 --> 00:01:39,40
so we need to add a new file,

41
00:01:39,40 --> 00:01:42,10
so go to the top directory

42
00:01:42,10 --> 00:01:43,80
and click New File

43
00:01:43,80 --> 00:01:50,10
and call this file next.config.js

44
00:01:50,10 --> 00:01:51,60
and then this file will create a variable

45
00:01:51,60 --> 00:02:03,10
called withCSS require @zeit/next-css.

46
00:02:03,10 --> 00:02:07,60
Like so and then the next one is module.export,

47
00:02:07,60 --> 00:02:10,20
so we're exporting that specific module

48
00:02:10,20 --> 00:02:11,70
that we just created or variable

49
00:02:11,70 --> 00:02:16,00
we just created with CSS like so.

50
00:02:16,00 --> 00:02:18,70
Okay, so now we're good.

51
00:02:18,70 --> 00:02:20,80
With that simple config now we'll be able

52
00:02:20,80 --> 00:02:23,10
to use CSS in our code.

53
00:02:23,10 --> 00:02:24,70
So, what I want to do next

54
00:02:24,70 --> 00:02:26,10
is to do a few things

55
00:02:26,10 --> 00:02:28,30
in our index.js

56
00:02:28,30 --> 00:02:30,80
because right now I have CSS styles

57
00:02:30,80 --> 00:02:33,20
that I have imported from another project,

58
00:02:33,20 --> 00:02:35,20
same thing, I've got a card,

59
00:02:35,20 --> 00:02:37,30
that's CSS with styles

60
00:02:37,30 --> 00:02:38,90
and so on and so forth,

61
00:02:38,90 --> 00:02:40,40
so I want to leverage those

62
00:02:40,40 --> 00:02:42,60
and I want to be able to create an application.

63
00:02:42,60 --> 00:02:43,80
So, there's a few things

64
00:02:43,80 --> 00:02:45,00
that we're going to change here.

65
00:02:45,00 --> 00:02:46,60
So, first we want to make sure

66
00:02:46,60 --> 00:02:50,60
that we're importing the CSS file

67
00:02:50,60 --> 00:02:54,90
that we just added to our project like so.

68
00:02:54,90 --> 00:02:57,80
Then we want to import the card

69
00:02:57,80 --> 00:02:59,80
that we just added to the project as well,

70
00:02:59,80 --> 00:03:03,30
so import card from card

71
00:03:03,30 --> 00:03:04,80
and then we'll change a few things here.

72
00:03:04,80 --> 00:03:06,70
So, what I'm going to do first

73
00:03:06,70 --> 00:03:09,80
is change this to a div,

74
00:03:09,80 --> 00:03:13,10
like so, closing tag

75
00:03:13,10 --> 00:03:16,20
and then I'm going to add a header

76
00:03:16,20 --> 00:03:18,30
and then I'm going to have my grid here,

77
00:03:18,30 --> 00:03:22,70
so I'm basically typing all the divs that I need

78
00:03:22,70 --> 00:03:25,50
and then then inside here of that div

79
00:03:25,50 --> 00:03:31,00
we'll actually load our card component like so.

80
00:03:31,00 --> 00:03:34,30
And let's add three for now

81
00:03:34,30 --> 00:03:37,70
and then we'll do basically when we load the data,

82
00:03:37,70 --> 00:03:39,90
I will use a map function

83
00:03:39,90 --> 00:03:42,00
to actually map out through the data

84
00:03:42,00 --> 00:03:45,00
and repeat the number of cards that we need.

85
00:03:45,00 --> 00:03:47,70
Okay, so now we need to add a few more things,

86
00:03:47,70 --> 00:03:50,10
so let me just add an image here,

87
00:03:50,10 --> 00:03:52,80
so an image and this image

88
00:03:52,80 --> 00:03:57,20
has a source of /static

89
00:03:57,20 --> 00:03:59,80
because my file is there

90
00:03:59,80 --> 00:04:03,60
and I need the logo.png.

91
00:04:03,60 --> 00:04:06,10
So, if you've done the React Interaction course

92
00:04:06,10 --> 00:04:07,50
that I've done recently,

93
00:04:07,50 --> 00:04:09,40
you will recognize what I'm doing here

94
00:04:09,40 --> 00:04:10,80
or some of it

95
00:04:10,80 --> 00:04:12,70
and then let's just make sure

96
00:04:12,70 --> 00:04:15,50
that I have a className for this guy here

97
00:04:15,50 --> 00:04:17,80
and the className that I have

98
00:04:17,80 --> 00:04:20,60
in my CSS is the static-logo

99
00:04:20,60 --> 00:04:23,20
and let's just add an alt

100
00:04:23,20 --> 00:04:28,30
otherwise I'm going to get some warnings from React.

101
00:04:28,30 --> 00:04:30,30
The header, well, let's go at the top here,

102
00:04:30,30 --> 00:04:33,40
so we have a className here as well

103
00:04:33,40 --> 00:04:35,80
and this one is App.

104
00:04:35,80 --> 00:04:41,10
The header has a className as well

105
00:04:41,10 --> 00:04:44,30
and let's go to the grid here,

106
00:04:44,30 --> 00:04:50,90
so this one has a className of Grid like so,

107
00:04:50,90 --> 00:04:52,10
so we have our grid here,

108
00:04:52,10 --> 00:04:53,50
so let me quickly check

109
00:04:53,50 --> 00:04:55,10
if everything checks out.

110
00:04:55,10 --> 00:04:58,40
ClassName App, className App-header,

111
00:04:58,40 --> 00:04:59,60
we're good to go here,

112
00:04:59,60 --> 00:05:01,80
so we have our static logo here.

113
00:05:01,80 --> 00:05:03,10
We're good to go

114
00:05:03,10 --> 00:05:04,80
and we have our grid.

115
00:05:04,80 --> 00:05:06,70
Okay, so we're good.

116
00:05:06,70 --> 00:05:08,60
Let's save that

117
00:05:08,60 --> 00:05:10,20
and now we should be good to go

118
00:05:10,20 --> 00:05:12,80
to actually load this in our browser,

119
00:05:12,80 --> 00:05:15,70
so let's go ahead and bring up our terminal,

120
00:05:15,70 --> 00:05:17,50
so View, Integrated Terminal

121
00:05:17,50 --> 00:05:21,40
and npm run dev

122
00:05:21,40 --> 00:05:23,10
and we have no errors.

123
00:05:23,10 --> 00:05:25,60
Let's go and check in our browser

124
00:05:25,60 --> 00:05:28,10
and refresh that page.

125
00:05:28,10 --> 00:05:30,50
And we have our little application.

126
00:05:30,50 --> 00:05:32,60
So, if you've done the React Interaction course,

127
00:05:32,60 --> 00:05:33,90
you'll recognize this here,

128
00:05:33,90 --> 00:05:36,00
so all the animations and interactions

129
00:05:36,00 --> 00:05:37,30
that we have in the other course

130
00:05:37,30 --> 00:05:38,40
I'm not going to be apparent here

131
00:05:38,40 --> 00:05:41,50
because this is not a course about interactions,

132
00:05:41,50 --> 00:05:44,40
this is a course about server rendering.

133
00:05:44,40 --> 00:05:46,20
This is all rendered in the server

134
00:05:46,20 --> 00:05:48,10
and presented to the client,

135
00:05:48,10 --> 00:05:49,40
so let me show you,

136
00:05:49,40 --> 00:05:51,90
so what does Next do

137
00:05:51,90 --> 00:05:55,90
is look inside of Pages for every JS file

138
00:05:55,90 --> 00:05:58,50
and considers this a route

139
00:05:58,50 --> 00:06:00,60
and therefore you can actually link it

140
00:06:00,60 --> 00:06:02,90
if you use the link element.

141
00:06:02,90 --> 00:06:04,80
You can actually link a page

142
00:06:04,80 --> 00:06:06,00
that sits in here.

143
00:06:06,00 --> 00:06:08,10
So, everything that sits in that folder

144
00:06:08,10 --> 00:06:10,30
is considered a route.

145
00:06:10,30 --> 00:06:12,40
Well, actually all the JS files.

146
00:06:12,40 --> 00:06:14,80
So, therefore it creates the route for you,

147
00:06:14,80 --> 00:06:16,50
so it does HMR for you,

148
00:06:16,50 --> 00:06:18,00
so it reloads the application

149
00:06:18,00 --> 00:06:20,80
as soon as you change anything in there.

150
00:06:20,80 --> 00:06:23,80
Now that we have installed the Next CSS,

151
00:06:23,80 --> 00:06:26,50
it does CSS by imports

152
00:06:26,50 --> 00:06:28,30
and if you put any files

153
00:06:28,30 --> 00:06:31,20
in the Static folder, this is available to load.

154
00:06:31,20 --> 00:06:34,20
As you can see, we actually loaded our background.

155
00:06:34,20 --> 00:06:38,00
You have our juice images that are actually loaded

156
00:06:38,00 --> 00:06:40,20
into each cards like so,

157
00:06:40,20 --> 00:06:41,70
so you have it here

158
00:06:41,70 --> 00:06:44,20
and I actually did the static already

159
00:06:44,20 --> 00:06:46,40
in the sample files for you

160
00:06:46,40 --> 00:06:47,90
so you don't have to do that

161
00:06:47,90 --> 00:06:50,40
but again, if you want to load a file,

162
00:06:50,40 --> 00:06:51,30
you want to make sure

163
00:06:51,30 --> 00:06:53,20
that it is in the static.

164
00:06:53,20 --> 00:06:55,80
So, all the classes are applied here

165
00:06:55,80 --> 00:06:58,60
because I've imported my CSS

166
00:06:58,60 --> 00:07:01,10
and because I have Next CSS installed,

167
00:07:01,10 --> 00:07:05,80
it applies all these styles to the actual cart.

168
00:07:05,80 --> 00:07:07,20
So, let's go back to our browser

169
00:07:07,20 --> 00:07:10,00
and let's take a look at what it does.

170
00:07:10,00 --> 00:07:13,20
So, basically this is the rendered project

171
00:07:13,20 --> 00:07:16,30
after it has been rendered on the server.

172
00:07:16,30 --> 00:07:18,80
So, right now this is the HTML

173
00:07:18,80 --> 00:07:20,40
that is presented to the client,

174
00:07:20,40 --> 00:07:23,00
so the client has nothing to do,

175
00:07:23,00 --> 00:07:25,50
it actually reads from this file

176
00:07:25,50 --> 00:07:28,00
that was created on the server

177
00:07:28,00 --> 00:07:31,50
by Next.js, so if you take a look at the grid,

178
00:07:31,50 --> 00:07:35,00
all these elements have been created on the server,

179
00:07:35,00 --> 00:07:38,90
also all the classes and all the static files

180
00:07:38,90 --> 00:07:40,30
that have been applied to all those

181
00:07:40,30 --> 00:07:43,40
have been basically rendered on the server as well.

182
00:07:43,40 --> 00:07:45,30
So, everything that you see

183
00:07:45,30 --> 00:07:47,30
is actually rendered on the server

184
00:07:47,30 --> 00:07:48,90
and if you go back to VS Code,

185
00:07:48,90 --> 00:07:52,10
you'll see how fast it actually has been rendered

186
00:07:52,10 --> 00:07:53,90
on the last compile.

187
00:07:53,90 --> 00:07:56,20
Okay, so with this we have a base project

188
00:07:56,20 --> 00:07:57,40
we can start working with,

189
00:07:57,40 --> 00:07:59,00
so let's move on.

