1
00:00:00,50 --> 00:00:02,10
- When doing state management,

2
00:00:02,10 --> 00:00:04,10
Redux is often the first place to go to

3
00:00:04,10 --> 00:00:05,90
when working with React.

4
00:00:05,90 --> 00:00:09,70
So how would you go about doing Redux with SSR and XJS?

5
00:00:09,70 --> 00:00:12,20
We'll explore this in the next few videos.

6
00:00:12,20 --> 00:00:13,90
If you're not familiar with Redux,

7
00:00:13,90 --> 00:00:16,80
it basically makes the management of your estate centralized

8
00:00:16,80 --> 00:00:18,40
but also prevents spreading your estate

9
00:00:18,40 --> 00:00:20,60
through all your trial components.

10
00:00:20,60 --> 00:00:22,30
Something that becomes frustrating

11
00:00:22,30 --> 00:00:25,20
for any developer as your application grows.

12
00:00:25,20 --> 00:00:27,60
If you have no interest of using Redux,

13
00:00:27,60 --> 00:00:30,40
feel free to skip to the next chapter where we revert back

14
00:00:30,40 --> 00:00:32,80
to where our application currently is,

15
00:00:32,80 --> 00:00:34,70
and work further on it.

16
00:00:34,70 --> 00:00:37,30
Okay, so let's work on building the code to make

17
00:00:37,30 --> 00:00:39,00
Redux work into your environment.

18
00:00:39,00 --> 00:00:41,60
But please be aware that our application won't be working

19
00:00:41,60 --> 00:00:44,90
until we complete all the required Redux items.

20
00:00:44,90 --> 00:00:47,90
Okay, so get into VS code first.

21
00:00:47,90 --> 00:00:52,10
And open up your integrated terminal, so click on view,

22
00:00:52,10 --> 00:00:53,90
integrated terminal.

23
00:00:53,90 --> 00:00:57,10
And first, let me just clear that up,

24
00:00:57,10 --> 00:00:58,80
we'll install a few things.

25
00:00:58,80 --> 00:01:01,30
We need a few dependencies to work with Redux so let's

26
00:01:01,30 --> 00:01:06,70
go ahead and do that, so MPM install dash dash save.

27
00:01:06,70 --> 00:01:10,10
And we'll install first of all Redux.

28
00:01:10,10 --> 00:01:12,20
Then, Redux

29
00:01:12,20 --> 00:01:14,10
dev tools

30
00:01:14,10 --> 00:01:15,60
extension.

31
00:01:15,60 --> 00:01:19,10
And that will allow us to actually use a dev tool

32
00:01:19,10 --> 00:01:21,00
for Redux specifically.

33
00:01:21,00 --> 00:01:24,80
And I'm going to show you how to use that in one of our videos.

34
00:01:24,80 --> 00:01:27,50
Then we'll install Redux thunk

35
00:01:27,50 --> 00:01:30,50
and React Redux,

36
00:01:30,50 --> 00:01:35,10
and finally Next Redux

37
00:01:35,10 --> 00:01:36,80
wrapper.

38
00:01:36,80 --> 00:01:39,60
Okay, so we are good to go here.

39
00:01:39,60 --> 00:01:41,90
So what we need to do next is create a file that will

40
00:01:41,90 --> 00:01:44,40
hold all the code for Redux.

41
00:01:44,40 --> 00:01:46,20
We'll call this store JS.

42
00:01:46,20 --> 00:01:49,60
Sometimes, and most often than not in bigger and larger

43
00:01:49,60 --> 00:01:52,00
projects, you would have all this different code

44
00:01:52,00 --> 00:01:55,10
in different files, but for the purpose of learning

45
00:01:55,10 --> 00:01:57,00
and because this is a small project,

46
00:01:57,00 --> 00:01:59,10
I'm going to put it inside of one file.

47
00:01:59,10 --> 00:02:02,20
So let's go ahead and create inside of our project

48
00:02:02,20 --> 00:02:05,40
a new file called store,

49
00:02:05,40 --> 00:02:07,40
store JS,

50
00:02:07,40 --> 00:02:09,10
and then inside of that file we'll start

51
00:02:09,10 --> 00:02:11,10
importing and creating our code.

52
00:02:11,10 --> 00:02:16,20
So, first we'll import create store

53
00:02:16,20 --> 00:02:19,90
and apply middleware

54
00:02:19,90 --> 00:02:22,80
from Redux.

55
00:02:22,80 --> 00:02:26,70
Then we'll import compose

56
00:02:26,70 --> 00:02:29,50
with dev tools.

57
00:02:29,50 --> 00:02:32,50
Which will allow us to basically see the store

58
00:02:32,50 --> 00:02:36,10
and Redux inside of our dev tools.

59
00:02:36,10 --> 00:02:39,50
From Redux dev tools extension.

60
00:02:39,50 --> 00:02:44,60
Then we'll import thunk middleware

61
00:02:44,60 --> 00:02:47,40
from redux thunk.

62
00:02:47,40 --> 00:02:49,70
And we'll also import our data.

63
00:02:49,70 --> 00:02:55,70
From our data folder, so data dot json, like so.

64
00:02:55,70 --> 00:02:58,50
Okay, now we to create our initial state,

65
00:02:58,50 --> 00:03:02,60
so let me put a note here so this is the initial state.

66
00:03:02,60 --> 00:03:06,60
We'll create a variable which we'll call start state.

67
00:03:06,60 --> 00:03:10,20
And inside of that state we'll initialize with MT array

68
00:03:10,20 --> 00:03:13,10
for cards, like so.

69
00:03:13,10 --> 00:03:13,90
Perfect.

70
00:03:13,90 --> 00:03:16,40
And then the last thing that we need to do in this

71
00:03:16,40 --> 00:03:18,90
specific video is to create the store itself.

72
00:03:18,90 --> 00:03:22,30
So let's go ahead and do that, so create store,

73
00:03:22,30 --> 00:03:24,90
and we'll export

74
00:03:24,90 --> 00:03:27,80
a variable called

75
00:03:27,80 --> 00:03:29,50
init store

76
00:03:29,50 --> 00:03:33,20
and we'll pass the initial state

77
00:03:33,20 --> 00:03:36,60
as the start state.

78
00:03:36,60 --> 00:03:41,10
Then it's a function that returns create store

79
00:03:41,10 --> 00:03:44,00
that we just imported from Redux.

80
00:03:44,00 --> 00:03:47,60
Then we'll pass the reducers which we'll create soon.

81
00:03:47,60 --> 00:03:51,40
The initial state and finally,

82
00:03:51,40 --> 00:03:55,80
what we'll do, compose with dev tools

83
00:03:55,80 --> 00:04:01,90
and we'll apply middleware with thunk middleware.

84
00:04:01,90 --> 00:04:05,20
Alright so, let me just regurgitate all this to make

85
00:04:05,20 --> 00:04:07,70
sure that you understand what's happening here.

86
00:04:07,70 --> 00:04:10,40
So basically what we've done here,

87
00:04:10,40 --> 00:04:13,00
we've imported the tools from Redux

88
00:04:13,00 --> 00:04:14,80
to be able to use Redux.

89
00:04:14,80 --> 00:04:17,90
And then we've imported compose with dev tools from

90
00:04:17,90 --> 00:04:21,30
redux dev tools extension and also thunk middleware,

91
00:04:21,30 --> 00:04:24,70
which will allow us to apply some middleware to Redux.

92
00:04:24,70 --> 00:04:28,00
And basically we're creating a store, an initial store,

93
00:04:28,00 --> 00:04:30,70
with our start state as the initial state.

94
00:04:30,70 --> 00:04:33,10
And then we'll create the store with the reducers

95
00:04:33,10 --> 00:04:36,70
which we'll do shortly past the initial state.

96
00:04:36,70 --> 00:04:39,60
And also make sure that we can use the dev tools

97
00:04:39,60 --> 00:04:43,60
by applying some middleware with compose with dev tools.

98
00:04:43,60 --> 00:04:46,80
And that's pretty much it for this video.

99
00:04:46,80 --> 00:04:48,20
So before we actually move on,

100
00:04:48,20 --> 00:04:51,30
let's make sure we save all this, command S.

101
00:04:51,30 --> 00:04:53,60
And now that we have our base store

102
00:04:53,60 --> 00:04:57,00
and Redux dependencies, let's move on.

