1
00:00:00,60 --> 00:00:01,40
- [Instructor] Okay so now we have

2
00:00:01,40 --> 00:00:03,70
a Base Express Server with next js.

3
00:00:03,70 --> 00:00:05,80
So let's add again our project files from our project

4
00:00:05,80 --> 00:00:09,00
before we added Redux to keep things simple.

5
00:00:09,00 --> 00:00:11,50
But you could also add the Redux version if you'd like

6
00:00:11,50 --> 00:00:14,00
and challenge yourself, but for the purpose

7
00:00:14,00 --> 00:00:17,00
of keeping the conversation focused, I won't.

8
00:00:17,00 --> 00:00:18,20
So let's get to it.

9
00:00:18,20 --> 00:00:21,50
So go to your desktop and open Exercise Files

10
00:00:21,50 --> 00:00:23,60
and then click on resource.

11
00:00:23,60 --> 00:00:24,50
And the first thing we'll do

12
00:00:24,50 --> 00:00:27,90
is basically copy the files inside of sample

13
00:00:27,90 --> 00:00:30,20
so let's highlight all these.

14
00:00:30,20 --> 00:00:31,50
Command C.

15
00:00:31,50 --> 00:00:33,10
And then click on ssr2.

16
00:00:33,10 --> 00:00:36,10
Make sure you are in ssr2 and not the first one,

17
00:00:36,10 --> 00:00:39,10
or whatever is the name of your second project.

18
00:00:39,10 --> 00:00:42,30
And then we'll create a new folder called pages.

19
00:00:42,30 --> 00:00:45,70
And then paste those files so Command V.

20
00:00:45,70 --> 00:00:49,40
And then we'll create the static folder

21
00:00:49,40 --> 00:00:53,80
like we've done before, inside of our ssr.

22
00:00:53,80 --> 00:00:57,00
And then go on the sample or the resources

23
00:00:57,00 --> 00:00:58,80
inside of the Exercise Files

24
00:00:58,80 --> 00:01:03,30
and copy logo, juice, background

25
00:01:03,30 --> 00:01:04,60
like so,

26
00:01:04,60 --> 00:01:09,40
and paste them inside of our static folder, like so.

27
00:01:09,40 --> 00:01:11,10
Okay, perfect.

28
00:01:11,10 --> 00:01:12,90
So the one thing we're missing in our project

29
00:01:12,90 --> 00:01:17,10
is the actual dependency of next css so we'll install that.

30
00:01:17,10 --> 00:01:20,40
So Control C to get out of your server

31
00:01:20,40 --> 00:01:22,70
and let's do an npm install.

32
00:01:22,70 --> 00:01:31,50
- -save @zeit/next-css

33
00:01:31,50 --> 00:01:32,90
And then the one thing we're missing as well

34
00:01:32,90 --> 00:01:34,90
is the config file so we can grab that

35
00:01:34,90 --> 00:01:39,50
from our previous project so let's go in our desktop

36
00:01:39,50 --> 00:01:43,50
and then click on ssr and then copy the next config js.

37
00:01:43,50 --> 00:01:45,40
So simply copy that file

38
00:01:45,40 --> 00:01:49,10
and paste it inside of ssr2, like so,

39
00:01:49,10 --> 00:01:50,90
on the main directory.

40
00:01:50,90 --> 00:01:52,80
So you should have next config,

41
00:01:52,80 --> 00:01:55,40
you should have pages with those three files,

42
00:01:55,40 --> 00:01:59,10
and then you should have static with those three images.

43
00:01:59,10 --> 00:02:01,80
So with all this, we should be good to go.

44
00:02:01,80 --> 00:02:03,80
Now we need to add the index

45
00:02:03,80 --> 00:02:05,60
and our new application should be running

46
00:02:05,60 --> 00:02:08,20
exactly the same as the previous one.

47
00:02:08,20 --> 00:02:12,60
So let's create an index inside of pages.

48
00:02:12,60 --> 00:02:14,00
And again, like I told you before,

49
00:02:14,00 --> 00:02:17,50
the reason why we've created these server.js

50
00:02:17,50 --> 00:02:20,10
and not index for the main server file

51
00:02:20,10 --> 00:02:24,60
is because next js is looking for an index.js file here,

52
00:02:24,60 --> 00:02:27,60
as the main and default file to load

53
00:02:27,60 --> 00:02:29,80
when we first load our application.

54
00:02:29,80 --> 00:02:32,90
So that's why I created this one with this name

55
00:02:32,90 --> 00:02:34,90
and now we'll create the index one.

56
00:02:34,90 --> 00:02:38,70
So click inside of pages, right click New File.

57
00:02:38,70 --> 00:02:40,10
And then let's call this one, well

58
00:02:40,10 --> 00:02:42,70
you guess it, index.js.

59
00:02:42,70 --> 00:02:45,10
And in this file we'll start importing a few things

60
00:02:45,10 --> 00:02:46,20
and you can copy and paste

61
00:02:46,20 --> 00:02:49,20
the code from the previous project if you want to

62
00:02:49,20 --> 00:02:50,40
and let me type it.

63
00:02:50,40 --> 00:02:52,30
It's going to take a few seconds.

64
00:02:52,30 --> 00:02:54,80
Index.css

65
00:02:54,80 --> 00:02:57,70
And now we need to import the card

66
00:02:57,70 --> 00:03:00,40
from the card.

67
00:03:00,40 --> 00:03:03,00
And if you do copy from the previous project

68
00:03:03,00 --> 00:03:06,60
make sure that you remove all the stuff from Redux

69
00:03:06,60 --> 00:03:10,00
otherwise you're going to have issues here.

70
00:03:10,00 --> 00:03:14,20
So let's go and export default.

71
00:03:14,20 --> 00:03:18,60
And then basically type our div stuff.

72
00:03:18,60 --> 00:03:20,30
So inside of that div

73
00:03:20,30 --> 00:03:22,60
I'm going to have a header

74
00:03:22,60 --> 00:03:23,70
and this particular code

75
00:03:23,70 --> 00:03:26,80
is a good opportunity to start copying code

76
00:03:26,80 --> 00:03:29,50
from your previous project if you want.

77
00:03:29,50 --> 00:03:33,20
Header and then after our header

78
00:03:33,20 --> 00:03:36,80
we have a div for our grid.

79
00:03:36,80 --> 00:03:38,10
And then we'll have our card.

80
00:03:38,10 --> 00:03:42,10
So let me add one card to start with,

81
00:03:42,10 --> 00:03:45,10
and then let's add the second one,

82
00:03:45,10 --> 00:03:47,50
and then a third one,

83
00:03:47,50 --> 00:03:49,60
and then let's copy this,

84
00:03:49,60 --> 00:03:52,30
those three lines or eight through ten,

85
00:03:52,30 --> 00:03:55,40
and paste it until we get six,

86
00:03:55,40 --> 00:03:57,60
so at least we can populate our application a little bit.

87
00:03:57,60 --> 00:03:59,90
So let's add the classes here.

88
00:03:59,90 --> 00:04:04,20
So className, this one is App.

89
00:04:04,20 --> 00:04:06,60
If you remember the header

90
00:04:06,60 --> 00:04:11,20
had a className of App-header

91
00:04:11,20 --> 00:04:12,50
and if you're wondering

92
00:04:12,50 --> 00:04:14,10
where I'm getting those classes

93
00:04:14,10 --> 00:04:17,80
you can check the actual index.css.

94
00:04:17,80 --> 00:04:19,00
And then inside of that guy

95
00:04:19,00 --> 00:04:23,00
we had the image source

96
00:04:23,00 --> 00:04:27,00
of static/logo.png

97
00:04:27,00 --> 00:04:32,90
and this guy had the className of static logo

98
00:04:32,90 --> 00:04:36,30
and make sure to enter an alt,

99
00:04:36,30 --> 00:04:39,80
because otherwise React will scream at you if you don't.

100
00:04:39,80 --> 00:04:42,30
And then close our element here.

101
00:04:42,30 --> 00:04:43,70
So we're good for that.

102
00:04:43,70 --> 00:04:47,50
And then let's add the class for this guy here.

103
00:04:47,50 --> 00:04:49,80
And this is a class of Grid.

104
00:04:49,80 --> 00:04:52,00
And that is pretty much it.

105
00:04:52,00 --> 00:04:55,50
So if we save that and then run our server,

106
00:04:55,50 --> 00:04:57,90
so npm run dev

107
00:04:57,90 --> 00:05:00,80
then it's going to look for that index.js file

108
00:05:00,80 --> 00:05:03,90
and then that index.js file will look for the card,

109
00:05:03,90 --> 00:05:07,90
and all the files that belong to that application.

110
00:05:07,90 --> 00:05:09,80
And it should run properly.

111
00:05:09,80 --> 00:05:12,00
So let's go and take a look.

112
00:05:12,00 --> 00:05:14,10
So we're good, everything is good here.

113
00:05:14,10 --> 00:05:15,60
Let's see if it renders properly

114
00:05:15,60 --> 00:05:19,40
and let's go inside of localhost.

115
00:05:19,40 --> 00:05:21,00
And we got our application.

116
00:05:21,00 --> 00:05:24,80
The only thing that I seem to be missing here is the logo,

117
00:05:24,80 --> 00:05:28,10
so let's make sure that I didn't make a typo on this one.

118
00:05:28,10 --> 00:05:32,00
I did, so let's make a dash as opposed to an equal sign.

119
00:05:32,00 --> 00:05:33,70
Save that.

120
00:05:33,70 --> 00:05:35,00
Get back to our application

121
00:05:35,00 --> 00:05:37,80
and then the logo shows up the way it should.

122
00:05:37,80 --> 00:05:40,50
Okay, great, so now we've got an Express Server

123
00:05:40,50 --> 00:05:44,00
with next js, ssr and a Base Rep.

124
00:05:44,00 --> 00:05:45,00
Let's move on.

