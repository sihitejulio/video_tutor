1
00:00:00,50 --> 00:00:02,30
- [Instructor] In many cases, you might already have

2
00:00:02,30 --> 00:00:04,10
an express web server running

3
00:00:04,10 --> 00:00:07,00
or would like to get a bit more control over the routes.

4
00:00:07,00 --> 00:00:09,50
Well, you can build an express server with next.js

5
00:00:09,50 --> 00:00:12,50
and still do SSR with the power of the two libraries.

6
00:00:12,50 --> 00:00:14,10
Let's work on this.

7
00:00:14,10 --> 00:00:17,40
So if you go to VS Code, the first thing I want to do is

8
00:00:17,40 --> 00:00:21,20
remove the SSR, so we're going to create a brand new project.

9
00:00:21,20 --> 00:00:26,50
So let's basically click on File and then Close Folder.

10
00:00:26,50 --> 00:00:28,30
And then we're actually going to start straight

11
00:00:28,30 --> 00:00:31,70
from a terminal as opposed to a terminal inside of VS Code.

12
00:00:31,70 --> 00:00:33,60
So let's get out of there

13
00:00:33,60 --> 00:00:36,50
and let's open a terminal window.

14
00:00:36,50 --> 00:00:38,20
And let me make it super big,

15
00:00:38,20 --> 00:00:40,20
so you guys can actually see it.

16
00:00:40,20 --> 00:00:44,50
Perfect, so what I'm going to do first is get to the desktop.

17
00:00:44,50 --> 00:00:47,10
So let's see where we are.

18
00:00:47,10 --> 00:00:50,10
Let's do a change directory to the desktop.

19
00:00:50,10 --> 00:00:53,10
And what I'm going to do is create a brand new directory,

20
00:00:53,10 --> 00:00:56,10
so make dir, so mkdir,

21
00:00:56,10 --> 00:00:57,40
call ssr2.

22
00:00:57,40 --> 00:00:59,30
So this is going to be our brand new project

23
00:00:59,30 --> 00:01:01,30
and I'll call it ssr2.

24
00:01:01,30 --> 00:01:03,40
Then what we need to do is make sure

25
00:01:03,40 --> 00:01:05,30
that we move inside of that folder,

26
00:01:05,30 --> 00:01:08,00
so I'm going to do double-&,

27
00:01:08,00 --> 00:01:10,80
which means that whenever I'm done with this command,

28
00:01:10,80 --> 00:01:12,60
then do the next one.

29
00:01:12,60 --> 00:01:17,60
And this one is going to be basically cd into ssr2.

30
00:01:17,60 --> 00:01:20,50
And then I'm going to do double-& again

31
00:01:20,50 --> 00:01:23,40
and I'm going to do an npm in it,

32
00:01:23,40 --> 00:01:26,00
which will create our brand new file.

33
00:01:26,00 --> 00:01:29,00
So if you recap what we're doing here is

34
00:01:29,00 --> 00:01:31,00
we're creating a brand new folder

35
00:01:31,00 --> 00:01:32,70
and then once this command is done,

36
00:01:32,70 --> 00:01:34,60
it's going to move on to the next one.

37
00:01:34,60 --> 00:01:37,90
We're going to move into that directory and once this is done,

38
00:01:37,90 --> 00:01:41,60
then we're going to initialize a package.json file.

39
00:01:41,60 --> 00:01:44,00
So let's go ahead and return on that

40
00:01:44,00 --> 00:01:47,10
and let's hit Return, Return, Return.

41
00:01:47,10 --> 00:01:49,90
And again, I'm repeating myself.

42
00:01:49,90 --> 00:01:52,20
If you have a git repository, add it.

43
00:01:52,20 --> 00:01:54,60
If not, then you can continue.

44
00:01:54,60 --> 00:01:58,10
Going to add my name here, there you go.

45
00:01:58,10 --> 00:02:02,10
Yes, okay, so we got our package.json file created.

46
00:02:02,10 --> 00:02:04,70
We can go back inside of VS Code now.

47
00:02:04,70 --> 00:02:09,90
So let me turn this off and go back inside VS Code.

48
00:02:09,90 --> 00:02:10,90
And

49
00:02:10,90 --> 00:02:13,50
go to File.

50
00:02:13,50 --> 00:02:15,00
Open.

51
00:02:15,00 --> 00:02:17,70
And then select ssr2.

52
00:02:17,70 --> 00:02:20,40
So there's two things that I want to do before we continue.

53
00:02:20,40 --> 00:02:23,70
I want to first create a gitignore file.

54
00:02:23,70 --> 00:02:30,20
So do a new file or right-click and then do a gitignore.

55
00:02:30,20 --> 00:02:34,30
And in this file, we'll add node_modules.

56
00:02:34,30 --> 00:02:36,10
So if you work with React quite a bit,

57
00:02:36,10 --> 00:02:39,60
usually these are done for you, but because this is a fresh,

58
00:02:39,60 --> 00:02:41,60
new project that we'll create from scratch,

59
00:02:41,60 --> 00:02:44,20
we kind of need to be more explicit about which files

60
00:02:44,20 --> 00:02:47,50
we don't want to include in our git commits.

61
00:02:47,50 --> 00:02:49,30
So let's save that.

62
00:02:49,30 --> 00:02:50,70
We can close that.

63
00:02:50,70 --> 00:02:52,60
Let me sip on water.

64
00:02:52,60 --> 00:02:55,60
Okay, and then let's bring up our integrated terminals.

65
00:02:55,60 --> 00:02:58,70
So click on View, Integrated Terminal.

66
00:02:58,70 --> 00:03:00,60
And now, we'll install a few things.

67
00:03:00,60 --> 00:03:03,40
So we need to install express.

68
00:03:03,40 --> 00:03:09,00
So npm install --save, we need to install express first.

69
00:03:09,00 --> 00:03:14,40
We need to install next, react, and react-dom.

70
00:03:14,40 --> 00:03:16,80
Okay, so now that we have the dependencies installed,

71
00:03:16,80 --> 00:03:18,50
let's change the scripts here.

72
00:03:18,50 --> 00:03:20,70
So let me remove that line.

73
00:03:20,70 --> 00:03:24,00
I'm going to change it to dev

74
00:03:24,00 --> 00:03:27,70
and add node server.js.

75
00:03:27,70 --> 00:03:30,10
And then the main entry file for this one

76
00:03:30,10 --> 00:03:32,30
is going to be server.

77
00:03:32,30 --> 00:03:34,30
And the reason why I'm changing this to server

78
00:03:34,30 --> 00:03:37,90
and not keeping the index default file is because

79
00:03:37,90 --> 00:03:41,60
index is the default file for next.

80
00:03:41,60 --> 00:03:45,10
So basically, whatever code or whatever component we build

81
00:03:45,10 --> 00:03:48,70
inside of index is going to be the first component

82
00:03:48,70 --> 00:03:49,80
to be rendered on screen.

83
00:03:49,80 --> 00:03:51,70
So if we use index for this server,

84
00:03:51,70 --> 00:03:54,70
then we can't use it for our component.

85
00:03:54,70 --> 00:03:57,10
So it's very important to create a separate file

86
00:03:57,10 --> 00:04:00,90
for your server and then index as your entry point

87
00:04:00,90 --> 00:04:02,50
for your views.

88
00:04:02,50 --> 00:04:04,20
So let's save that.

89
00:04:04,20 --> 00:04:10,00
Click on the new file plus here and call this server.js.

90
00:04:10,00 --> 00:04:12,40
And then we'll create our server code here.

91
00:04:12,40 --> 00:04:15,50
So let's start by doing express.

92
00:04:15,50 --> 00:04:19,80
And you may be wondering why is he doing express this way.

93
00:04:19,80 --> 00:04:23,60
Well, because we're using next.js and express together,

94
00:04:23,60 --> 00:04:26,50
we need to change our approach, how we're doing it

95
00:04:26,50 --> 00:04:28,40
inside of the initial file.

96
00:04:28,40 --> 00:04:30,80
So let me code all this

97
00:04:30,80 --> 00:04:34,30
and I'll explain what everything means here after.

98
00:04:34,30 --> 00:04:36,60
Then we need to import next.

99
00:04:36,60 --> 00:04:41,20
And then let's create a constant for our port

100
00:04:41,20 --> 00:04:43,50
like usual, 3000.

101
00:04:43,50 --> 00:04:46,80
Then let's create a dev

102
00:04:46,80 --> 00:04:51,80
variable called process.env.

103
00:04:51,80 --> 00:04:54,80
And it's process not porcess.

104
00:04:54,80 --> 00:04:59,30
.node_env,

105
00:04:59,30 --> 00:05:02,60
otherwise, do production.

106
00:05:02,60 --> 00:05:04,10
Now, let's create the app

107
00:05:04,10 --> 00:05:05,30
and this is where you're going to

108
00:05:05,30 --> 00:05:06,80
start making sense of all this.

109
00:05:06,80 --> 00:05:10,10
So we created a variable called express

110
00:05:10,10 --> 00:05:12,50
and we're going to wrap express

111
00:05:12,50 --> 00:05:16,50
inside of our next.js application.

112
00:05:16,50 --> 00:05:19,60
Then let's create a handler,

113
00:05:19,60 --> 00:05:24,00
which will be the app.getRequestHandler.

114
00:05:24,00 --> 00:05:27,70
All right, so now, the app, which is

115
00:05:27,70 --> 00:05:30,80
next.js will prepare

116
00:05:30,80 --> 00:05:32,40
our server

117
00:05:32,40 --> 00:05:37,00
with express and this is how we do it.

118
00:05:37,00 --> 00:05:42,40
So now, we've actually started the app, the next.js app.

119
00:05:42,40 --> 00:05:45,70
And then we wrapped the express server

120
00:05:45,70 --> 00:05:48,20
inside of that next.js app,

121
00:05:48,20 --> 00:05:51,50
so we're basically able to now create routes

122
00:05:51,50 --> 00:05:56,10
and use express functions inside of a next.js build.

123
00:05:56,10 --> 00:06:00,20
And for example, now we're creating our first route,

124
00:06:00,20 --> 00:06:01,90
which is the default route.

125
00:06:01,90 --> 00:06:05,60
Or all routes that are not explicit.

126
00:06:05,60 --> 00:06:09,30
Use this one, so request response,

127
00:06:09,30 --> 00:06:11,60
and for this specific route, do this.

128
00:06:11,60 --> 00:06:15,00
So return handle,

129
00:06:15,00 --> 00:06:17,90
request response.

130
00:06:17,90 --> 00:06:23,40
Perfect, now, let's create the, so server.listen,

131
00:06:23,40 --> 00:06:26,20
so we have a port to listen to.

132
00:06:26,20 --> 00:06:29,60
So port, which is port 3000.

133
00:06:29,60 --> 00:06:32,00
If there's an error,

134
00:06:32,00 --> 00:06:37,20
throw an error.

135
00:06:37,20 --> 00:06:42,20
Otherwise, console.log and we'll use template strings.

136
00:06:42,20 --> 00:06:43,90
Ready

137
00:06:43,90 --> 00:06:46,80
on http,

138
00:06:46,80 --> 00:06:50,20
local host, home port,

139
00:06:50,20 --> 00:06:52,10
and we'll use curly braces,

140
00:06:52,10 --> 00:06:57,60
so we can insert the value of our port in here like so.

141
00:06:57,60 --> 00:06:59,10
And

142
00:06:59,10 --> 00:07:00,30
perfect.

143
00:07:00,30 --> 00:07:03,20
Okay, so you can save this.

144
00:07:03,20 --> 00:07:05,20
And then you can actually start the server

145
00:07:05,20 --> 00:07:07,50
and we're going to get the 404 error,

146
00:07:07,50 --> 00:07:11,10
basically, which is the default template that next.js

147
00:07:11,10 --> 00:07:14,00
will actually render if we don't have any pages done,

148
00:07:14,00 --> 00:07:15,50
but we'll do that soon.

149
00:07:15,50 --> 00:07:18,60
In the meantime, let's go ahead and actually test this.

150
00:07:18,60 --> 00:07:20,90
So everything works properly.

151
00:07:20,90 --> 00:07:23,90
Our server is compiled, everything is working.

152
00:07:23,90 --> 00:07:27,20
Let's go to our webpage

153
00:07:27,20 --> 00:07:30,80
and let's go to port 3000

154
00:07:30,80 --> 00:07:32,10
like so.

155
00:07:32,10 --> 00:07:35,20
And this page could not be found, error 404.

156
00:07:35,20 --> 00:07:36,90
This is perfect.

157
00:07:36,90 --> 00:07:40,10
Okay, so we got our express next.js server running.

158
00:07:40,10 --> 00:07:42,00
Let's move on.

