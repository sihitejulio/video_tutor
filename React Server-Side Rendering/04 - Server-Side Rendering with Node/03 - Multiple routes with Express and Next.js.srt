1
00:00:00,60 --> 00:00:02,80
- [Instructor] Most applications require more than one route

2
00:00:02,80 --> 00:00:05,80
so let's add more to show you how easy it is

3
00:00:05,80 --> 00:00:08,00
to create a full SSR application

4
00:00:08,00 --> 00:00:11,00
leveraging Express and Next.js.

5
00:00:11,00 --> 00:00:12,40
So, the first thing we'll do is create

6
00:00:12,40 --> 00:00:14,30
the two components inside of Pages,

7
00:00:14,30 --> 00:00:18,10
so right click on Pages, create a new file,

8
00:00:18,10 --> 00:00:22,50
and then name the first one page2.js

9
00:00:22,50 --> 00:00:25,30
and then in this one we'll create a simple component,

10
00:00:25,30 --> 00:00:30,20
so default, export default function

11
00:00:30,20 --> 00:00:33,80
and then return a simple h1

12
00:00:33,80 --> 00:00:37,70
and we'll say I'm page 2

13
00:00:37,70 --> 00:00:39,70
and that's for page two

14
00:00:39,70 --> 00:00:42,40
and then let's create a new file,

15
00:00:42,40 --> 00:00:45,20
so right click, New File

16
00:00:45,20 --> 00:00:49,40
and then we'll name this ohyeah.js

17
00:00:49,40 --> 00:00:52,40
and the reason why I'm not calling this one page3

18
00:00:52,40 --> 00:00:54,90
is because I want to show you the power of Express

19
00:00:54,90 --> 00:00:57,40
inside of Next.js where you can define

20
00:00:57,40 --> 00:01:02,10
specific crowds and then render whatever page you want.

21
00:01:02,10 --> 00:01:04,90
So, let's go ahead and create this one as well,

22
00:01:04,90 --> 00:01:08,00
export default

23
00:01:08,00 --> 00:01:09,90
and then function

24
00:01:09,90 --> 00:01:13,60
and then simple h2 as well

25
00:01:13,60 --> 00:01:17,40
and in this one we'll say I'm page 3

26
00:01:17,40 --> 00:01:22,00
trapped inside of oh yeah

27
00:01:22,00 --> 00:01:23,90
and save this.

28
00:01:23,90 --> 00:01:25,90
We're good to go there.

29
00:01:25,90 --> 00:01:27,80
Okay, so here's the idea.

30
00:01:27,80 --> 00:01:30,40
So, I'll go to server.js

31
00:01:30,40 --> 00:01:31,70
and here's the idea.

32
00:01:31,70 --> 00:01:34,40
We'll first create a route

33
00:01:34,40 --> 00:01:36,40
that will bring us to page2.

34
00:01:36,40 --> 00:01:38,60
Usually you wouldn't need to create that route

35
00:01:38,60 --> 00:01:39,70
because with Next.js

36
00:01:39,70 --> 00:01:42,80
usually it creates them automatically for you.

37
00:01:42,80 --> 00:01:45,00
So, if you had created those pages

38
00:01:45,00 --> 00:01:47,00
and you want to maintain the ohyeah

39
00:01:47,00 --> 00:01:49,50
as the route and page2 as the route,

40
00:01:49,50 --> 00:01:51,40
then you wouldn't need to create them

41
00:01:51,40 --> 00:01:52,60
in the server here

42
00:01:52,60 --> 00:01:54,00
but just for the example,

43
00:01:54,00 --> 00:01:58,00
I'm going to purposefully and explicitly create

44
00:01:58,00 --> 00:02:00,10
the routes for page2

45
00:02:00,10 --> 00:02:03,10
and then I will create a route for page3

46
00:02:03,10 --> 00:02:06,40
which will bring us to the page ohyeah.

47
00:02:06,40 --> 00:02:08,20
So, let's go ahead and do this.

48
00:02:08,20 --> 00:02:12,60
So, the first one is server.get

49
00:02:12,60 --> 00:02:16,30
and this is for the route page2

50
00:02:16,30 --> 00:02:18,90
which takes a request

51
00:02:18,90 --> 00:02:22,70
and responds the usual stuff for Express

52
00:02:22,70 --> 00:02:26,90
and then basically returns a render from app

53
00:02:26,90 --> 00:02:29,40
and this is syntax that may be familiar to you

54
00:02:29,40 --> 00:02:32,00
if you've done Express before.

55
00:02:32,00 --> 00:02:35,10
Request response

56
00:02:35,10 --> 00:02:39,80
and we're rendering page2 like so.

57
00:02:39,80 --> 00:02:42,30
So, for this one this is how it would end

58
00:02:42,30 --> 00:02:45,80
and if we needed just to have that route

59
00:02:45,80 --> 00:02:48,30
and no other specific routes,

60
00:02:48,30 --> 00:02:50,30
then we may not need to type this

61
00:02:50,30 --> 00:02:53,30
because we are actually getting it from Next.js.

62
00:02:53,30 --> 00:02:54,40
So, let's save this

63
00:02:54,40 --> 00:02:56,30
and I'm going to show you

64
00:02:56,30 --> 00:02:59,50
and one thing that you may need to do sometimes

65
00:02:59,50 --> 00:03:01,30
when you create new pages,

66
00:03:01,30 --> 00:03:03,60
and something is off or something's not working

67
00:03:03,60 --> 00:03:05,50
as it's supposed to,

68
00:03:05,50 --> 00:03:10,10
what you could do is simply stop the Next.js server

69
00:03:10,10 --> 00:03:11,30
and then restart it again

70
00:03:11,30 --> 00:03:13,30
and by restarting it again,

71
00:03:13,30 --> 00:03:15,30
it will actually rerender everything

72
00:03:15,30 --> 00:03:16,90
that you have in your project

73
00:03:16,90 --> 00:03:19,40
and most likely if you had any issues,

74
00:03:19,40 --> 00:03:21,40
then those will go away.

75
00:03:21,40 --> 00:03:24,50
So, let's go back to localhost,

76
00:03:24,50 --> 00:03:26,20
refresh our page

77
00:03:26,20 --> 00:03:29,60
and then let's do page2

78
00:03:29,60 --> 00:03:31,90
and we got our page two.

79
00:03:31,90 --> 00:03:33,80
So, we're all good.

80
00:03:33,80 --> 00:03:37,80
Now, let's create the specific route for page3

81
00:03:37,80 --> 00:03:42,20
but render the ohyeah as opposed to the page3

82
00:03:42,20 --> 00:03:44,10
which we don't have actually.

83
00:03:44,10 --> 00:03:46,80
So, let's do a get page3

84
00:03:46,80 --> 00:03:50,00
so when we call route page3,

85
00:03:50,00 --> 00:03:54,30
then pass the request and response arguments

86
00:03:54,30 --> 00:03:55,90
to a function

87
00:03:55,90 --> 00:03:57,10
and inside of that function

88
00:03:57,10 --> 00:04:00,00
we want to return this function

89
00:04:00,00 --> 00:04:04,40
from Express which takes the request

90
00:04:04,40 --> 00:04:08,40
and response and then renders

91
00:04:08,40 --> 00:04:12,00
the ohyeah as opposed to a page3

92
00:04:12,00 --> 00:04:14,50
because if we kept it page3,

93
00:04:14,50 --> 00:04:17,80
page3 it would be looking for page3

94
00:04:17,80 --> 00:04:19,70
and we don't have that.

95
00:04:19,70 --> 00:04:21,80
So, let's go ahead and test that

96
00:04:21,80 --> 00:04:24,90
and let's go back to our initial page

97
00:04:24,90 --> 00:04:27,60
and call for page3

98
00:04:27,60 --> 00:04:29,50
and right now like I said before,

99
00:04:29,50 --> 00:04:30,80
we have an error,

100
00:04:30,80 --> 00:04:35,10
so let's go ahead and actually rerender this.

101
00:04:35,10 --> 00:04:37,50
So, you do npm run dev.

102
00:04:37,50 --> 00:04:41,30
It will basically reestablish everything,

103
00:04:41,30 --> 00:04:44,80
recompile and now we are good.

104
00:04:44,80 --> 00:04:47,60
So, if we go from 3000,

105
00:04:47,60 --> 00:04:52,60
page3, then we are on the right component.

106
00:04:52,60 --> 00:04:54,60
One of the things that I could show you as well

107
00:04:54,60 --> 00:04:56,90
while we're actually talking about links

108
00:04:56,90 --> 00:04:59,00
and routes and things like that

109
00:04:59,00 --> 00:05:02,20
is how to do a link inside of a page.

110
00:05:02,20 --> 00:05:03,10
So, let's say for example

111
00:05:03,10 --> 00:05:04,60
you wanted to click or tap

112
00:05:04,60 --> 00:05:05,90
on the actual logo

113
00:05:05,90 --> 00:05:10,20
and be brought to a page or page2 or page3,

114
00:05:10,20 --> 00:05:13,00
you can do this with a link component

115
00:05:13,00 --> 00:05:14,70
inside of Next.js.

116
00:05:14,70 --> 00:05:16,60
Let me show you how that works.

117
00:05:16,60 --> 00:05:19,80
So, go inside of index here

118
00:05:19,80 --> 00:05:20,60
and the first thing you need to do

119
00:05:20,60 --> 00:05:24,00
is actually importing link

120
00:05:24,00 --> 00:05:28,90
from next/link

121
00:05:28,90 --> 00:05:31,60
and then simply wrap around the specific elements

122
00:05:31,60 --> 00:05:34,10
you want to be used as a link,

123
00:05:34,10 --> 00:05:37,70
the component link like so.

124
00:05:37,70 --> 00:05:40,60
And you want to specify inside of the props

125
00:05:40,60 --> 00:05:45,00
of that component what is the href.

126
00:05:45,00 --> 00:05:47,70
In this case, you want to push it to page2

127
00:05:47,70 --> 00:05:51,00
and then let's grab our code from line nine to 11,

128
00:05:51,00 --> 00:05:54,60
so basically the image, cut that, so command X

129
00:05:54,60 --> 00:05:57,40
and then paste it in between the link,

130
00:05:57,40 --> 00:06:01,00
command V and let's return on that

131
00:06:01,00 --> 00:06:03,60
and now you got the image as a link,

132
00:06:03,60 --> 00:06:05,70
so let's save that,

133
00:06:05,70 --> 00:06:07,30
let's go to our page,

134
00:06:07,30 --> 00:06:10,50
let's make sure it refreshes

135
00:06:10,50 --> 00:06:13,80
and now we got our logo as a link

136
00:06:13,80 --> 00:06:16,20
which brings us to page2.

137
00:06:16,20 --> 00:06:19,10
So, as you can see, using Express with Next.js

138
00:06:19,10 --> 00:06:21,00
allows us to fully customize the routes

139
00:06:21,00 --> 00:06:25,10
and get all the benefits of Express with Next.js.

140
00:06:25,10 --> 00:06:27,80
A win-win-win-win.

141
00:06:27,80 --> 00:06:29,00
Let's move on.

