1
00:00:00,50 --> 00:00:01,70
- [Instructor] Maybe you've heard of

2
00:00:01,70 --> 00:00:04,30
some of these different terms, Isomorphic,

3
00:00:04,30 --> 00:00:06,70
Universal and SSR.

4
00:00:06,70 --> 00:00:10,40
These are all different terms meaning the exact same thing.

5
00:00:10,40 --> 00:00:13,70
Which is that your obligation is rendered on the server

6
00:00:13,70 --> 00:00:15,60
instead of the client.

7
00:00:15,60 --> 00:00:17,40
So to better understand what happens

8
00:00:17,40 --> 00:00:19,30
when running your application on the server,

9
00:00:19,30 --> 00:00:20,80
let me first explain what happens

10
00:00:20,80 --> 00:00:24,10
in your regular React client side application.

11
00:00:24,10 --> 00:00:26,80
Your back end provides APIs or data

12
00:00:26,80 --> 00:00:29,60
to your front end side or client.

13
00:00:29,60 --> 00:00:32,90
Then React with the data on hand renders the HTML

14
00:00:32,90 --> 00:00:35,00
of the application in the browser.

15
00:00:35,00 --> 00:00:37,50
Whereas, in server side rendering,

16
00:00:37,50 --> 00:00:41,80
the server renders the HTML and then sends it to the client,

17
00:00:41,80 --> 00:00:45,20
eliminating the need of the client to do these tasks.

18
00:00:45,20 --> 00:00:47,40
Depending on the size of your application,

19
00:00:47,40 --> 00:00:50,10
that could be beneficial and there are some advantages

20
00:00:50,10 --> 00:00:52,90
to running your application in the server.

21
00:00:52,90 --> 00:00:56,70
For starters, if you're running a pretty large application

22
00:00:56,70 --> 00:00:59,90
or expected to grow quite large, rendering on the server

23
00:00:59,90 --> 00:01:01,70
will have a performance improvement

24
00:01:01,70 --> 00:01:05,10
in terms of the response time of your application.

25
00:01:05,10 --> 00:01:07,80
Consider this fact, the browser on which

26
00:01:07,80 --> 00:01:09,80
your application renders the application,

27
00:01:09,80 --> 00:01:13,10
is impacted by the machine it runs on.

28
00:01:13,10 --> 00:01:16,00
Therefore, if people are looking at your application

29
00:01:16,00 --> 00:01:19,60
on older hardware such as an old phone, laptop

30
00:01:19,60 --> 00:01:23,40
or desktop, they will see a difference in performance

31
00:01:23,40 --> 00:01:26,10
to those running your hardware.

32
00:01:26,10 --> 00:01:29,50
However, by rendering the application in the server

33
00:01:29,50 --> 00:01:32,50
you control the performance of the render.

34
00:01:32,50 --> 00:01:35,40
Additionally, if you're concerned if your site

35
00:01:35,40 --> 00:01:38,80
or application gets found in the top results of Google,

36
00:01:38,80 --> 00:01:40,90
Bing and other search engines,

37
00:01:40,90 --> 00:01:43,30
that might be the second benefit of rendering

38
00:01:43,30 --> 00:01:45,40
an application on the server.

39
00:01:45,40 --> 00:01:48,20
JavaScript doesn't play well with search engines

40
00:01:48,20 --> 00:01:50,10
and unfortunately when Google, for instance,

41
00:01:50,10 --> 00:01:53,30
sends it's robots to index your pages,

42
00:01:53,30 --> 00:01:57,10
all it sees is JavaScript and not HTML.

43
00:01:57,10 --> 00:02:00,10
Over the past few years the situation has improved

44
00:02:00,10 --> 00:02:02,90
but you are better positioned to be indexed properly

45
00:02:02,90 --> 00:02:06,40
if what that robot sees is HTML.

46
00:02:06,40 --> 00:02:09,30
Keep all these items in mind when you consider SSR

47
00:02:09,30 --> 00:02:11,50
and whether you should make the move.

48
00:02:11,50 --> 00:02:14,30
So now that we've explored the basics of SSR,

49
00:02:14,30 --> 00:02:18,00
let's get going on working with this great approach.

