1
00:00:00,60 --> 00:00:01,90
- [Instructor] Any type of web development,

2
00:00:01,90 --> 00:00:03,90
our first tool of choice is the dev tools

3
00:00:03,90 --> 00:00:05,80
that come with your own browser.

4
00:00:05,80 --> 00:00:08,50
Whether you use Chrome, Safari, Firefox

5
00:00:08,50 --> 00:00:10,50
or Edge, you have the tools needed

6
00:00:10,50 --> 00:00:14,20
to inspect your code, do performance checks and more.

7
00:00:14,20 --> 00:00:15,60
In this course we'll specifically

8
00:00:15,60 --> 00:00:18,00
use Chrome's internal tools.

9
00:00:18,00 --> 00:00:21,40
I highly recommend that you use either Chrome or Firefox,

10
00:00:21,40 --> 00:00:23,10
as they offer solid performance

11
00:00:23,10 --> 00:00:25,60
and work with extensions we'll use.

12
00:00:25,60 --> 00:00:27,60
As a developer, you've probably used

13
00:00:27,60 --> 00:00:30,70
Chrome's developer tools, but if not you can access them

14
00:00:30,70 --> 00:00:34,00
on Mac with option command I,

15
00:00:34,00 --> 00:00:37,90
and on Windows with control shift I, like so.

16
00:00:37,90 --> 00:00:40,80
And then you see the actual dev tools.

17
00:00:40,80 --> 00:00:42,80
So now let's install the React developer tools

18
00:00:42,80 --> 00:00:44,10
which you can find on the

19
00:00:44,10 --> 00:00:51,50
github.com/facebook/react-devtools.

20
00:00:51,50 --> 00:00:54,50
Once you get to that page, scroll all the way down

21
00:00:54,50 --> 00:00:57,90
to the installation section and you can find the extension

22
00:00:57,90 --> 00:01:00,80
for Chrome or the Firefox extension

23
00:01:00,80 --> 00:01:02,90
or if you don't have any of those two browsers

24
00:01:02,90 --> 00:01:05,40
you can actually install these tiny little app

25
00:01:05,40 --> 00:01:08,20
but I would recommend using one of the extensions here.

26
00:01:08,20 --> 00:01:12,30
So in this case I have Chrome, so I would click here.

27
00:01:12,30 --> 00:01:14,20
And then as you can see, I already have it,

28
00:01:14,20 --> 00:01:17,20
so if you don't just click on the button here

29
00:01:17,20 --> 00:01:19,20
to add it to Chrome.

30
00:01:19,20 --> 00:01:21,30
Once it's done let's go ahead and check

31
00:01:21,30 --> 00:01:23,40
if it is properly installed.

32
00:01:23,40 --> 00:01:25,90
So let's bring up our developer tools.

33
00:01:25,90 --> 00:01:29,30
So again, option command I for Mac

34
00:01:29,30 --> 00:01:31,90
and control shift I for Windows.

35
00:01:31,90 --> 00:01:34,60
And right now we don't have a tab that says React.

36
00:01:34,60 --> 00:01:36,80
So let's go, for example, to Netflix.

37
00:01:36,80 --> 00:01:42,10
I know Netflix has React all over the place.

38
00:01:42,10 --> 00:01:45,90
And as you can see right now, if I click on my name,

39
00:01:45,90 --> 00:01:49,20
let's mute tab, thank you very much,

40
00:01:49,20 --> 00:01:53,00
and then let's click on React

41
00:01:53,00 --> 00:01:55,10
and then you'll see all the components.

42
00:01:55,10 --> 00:01:57,30
You'll see the state and so on and so forth.

43
00:01:57,30 --> 00:02:02,00
So we are good to go, React has been installed.

44
00:02:02,00 --> 00:02:05,00
And now we have all the tools we need, let's move on.

