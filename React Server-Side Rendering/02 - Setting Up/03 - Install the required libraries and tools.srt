1
00:00:00,60 --> 00:00:02,50
- So at the bare minimum, if you want to

2
00:00:02,50 --> 00:00:05,50
do this course you will need node and npm.

3
00:00:05,50 --> 00:00:07,30
And everything else is going to be installed

4
00:00:07,30 --> 00:00:09,30
as we actually work on the projects.

5
00:00:09,30 --> 00:00:12,60
So, go to nodejs.org.

6
00:00:12,60 --> 00:00:14,00
Now once you get to that page,

7
00:00:14,00 --> 00:00:16,90
basically click on other downloads

8
00:00:16,90 --> 00:00:18,80
and then select the operating system

9
00:00:18,80 --> 00:00:19,60
that you're on.

10
00:00:19,60 --> 00:00:21,40
So if you're on mac, click here,

11
00:00:21,40 --> 00:00:23,50
if you're on Windows, click here,

12
00:00:23,50 --> 00:00:27,10
and if you're on lynx, click on this lynx file here.

13
00:00:27,10 --> 00:00:30,30
So basically, I recommend using the LTS version

14
00:00:30,30 --> 00:00:32,40
and not the current because the LTS version

15
00:00:32,40 --> 00:00:34,10
is the one that is supported;

16
00:00:34,10 --> 00:00:36,00
the current is with the latest features

17
00:00:36,00 --> 00:00:38,60
but not necessarily supported at this point.

18
00:00:38,60 --> 00:00:42,60
So use the LTS version, that's the safest place to start

19
00:00:42,60 --> 00:00:44,20
and in my case, I'm going to use

20
00:00:44,20 --> 00:00:47,40
the macOS installer like so.

21
00:00:47,40 --> 00:00:49,90
Once it's ready, you double-click on it

22
00:00:49,90 --> 00:00:51,60
or you click in the browser

23
00:00:51,60 --> 00:00:53,30
and then you install it this way.

24
00:00:53,30 --> 00:00:55,30
There's also other ways to install node,

25
00:00:55,30 --> 00:00:59,20
you can use nvm and you can also use homebrew.

26
00:00:59,20 --> 00:01:00,90
This is not needed for this course

27
00:01:00,90 --> 00:01:04,30
but if you'd like to or if you have those tools already,

28
00:01:04,30 --> 00:01:06,10
feel free to install node that way,

29
00:01:06,10 --> 00:01:08,70
I'm basically installing it the simplest way

30
00:01:08,70 --> 00:01:11,60
because I don't need the other stuff for this course.

31
00:01:11,60 --> 00:01:13,50
So let's go ahead and do that

32
00:01:13,50 --> 00:01:15,80
and the installation for Windows is roughly the same,

33
00:01:15,80 --> 00:01:20,00
so basically do all this and then enter your password,

34
00:01:20,00 --> 00:01:23,00
and it will install node and npm.

35
00:01:23,00 --> 00:01:25,50
So once this is done, there's a quick way to check

36
00:01:25,50 --> 00:01:28,60
if it is installed, let's close this,

37
00:01:28,60 --> 00:01:33,20
and let's open a terminal or a console on Windows,

38
00:01:33,20 --> 00:01:35,60
and let me just make this super big

39
00:01:35,60 --> 00:01:38,00
and the best way to check is to basically do

40
00:01:38,00 --> 00:01:41,90
node dash version and it will tell you the version;

41
00:01:41,90 --> 00:01:43,60
if it doesn't tell you the version,

42
00:01:43,60 --> 00:01:46,50
then there was a problem with your installation,

43
00:01:46,50 --> 00:01:47,60
try again.

44
00:01:47,60 --> 00:01:50,00
And then the same thing with npm.

45
00:01:50,00 --> 00:01:53,50
And if you see a version, you are good to go.

46
00:01:53,50 --> 00:01:56,00
So now we've got npm and node installed,

47
00:01:56,00 --> 00:01:58,00
let's move on.

