1
00:00:00,60 --> 00:00:01,90
- [Narrator] So you may be wondering,

2
00:00:01,90 --> 00:00:04,80
what is this code splitting thing that you heard about?

3
00:00:04,80 --> 00:00:05,80
What are the benefits?

4
00:00:05,80 --> 00:00:08,90
And how does a library like Next.js support that?

5
00:00:08,90 --> 00:00:10,50
So code splitting in a nutshell

6
00:00:10,50 --> 00:00:14,60
is the ability to split your code into smaller bundles

7
00:00:14,60 --> 00:00:17,60
so the application can load faster when rendered.

8
00:00:17,60 --> 00:00:19,50
It's all about speed.

9
00:00:19,50 --> 00:00:20,70
Normally, with some libraries,

10
00:00:20,70 --> 00:00:22,60
you'd have to be explicit about which part

11
00:00:22,60 --> 00:00:25,20
of your application you want to be bundled together.

12
00:00:25,20 --> 00:00:28,00
With Next.js that's not the case.

13
00:00:28,00 --> 00:00:30,20
It comes with code splitting out of the box

14
00:00:30,20 --> 00:00:32,30
and it's pretty intelligent about deciding

15
00:00:32,30 --> 00:00:34,80
which part of your code should be split.

16
00:00:34,80 --> 00:00:37,60
Off the shelf, Next.js splits your code

17
00:00:37,60 --> 00:00:39,40
into components or whatever is

18
00:00:39,40 --> 00:00:43,20
in your pages directory and its own imports.

19
00:00:43,20 --> 00:00:46,30
So for example, if you have a page component

20
00:00:46,30 --> 00:00:49,50
that imports a .css file, a dependency,

21
00:00:49,50 --> 00:00:51,90
then it will bundle all these three together

22
00:00:51,90 --> 00:00:53,80
into its own bundle.

23
00:00:53,80 --> 00:00:55,70
There are also advanced features

24
00:00:55,70 --> 00:00:58,80
like dynamic imports and dynamic React components

25
00:00:58,80 --> 00:01:00,40
which basically allows you to delay

26
00:01:00,40 --> 00:01:03,40
or load specific features on demand

27
00:01:03,40 --> 00:01:06,60
and maintain full control of your application load times.

28
00:01:06,60 --> 00:01:09,10
But this is a bit beyond the scope of this course,

29
00:01:09,10 --> 00:01:11,30
so I'd suggest you take a look

30
00:01:11,30 --> 00:01:14,00
at these features on Next.js documentation.

31
00:01:14,00 --> 00:01:15,30
So if you decide to do nothing,

32
00:01:15,30 --> 00:01:17,60
you don't have to worry about code splitting,

33
00:01:17,60 --> 00:01:19,30
as mentioned before.

34
00:01:19,30 --> 00:01:22,00
Next.js does it automatically for you.

